
<!-- BEGIN FOOTER -->
<div class="footer">
	<footer>
		<div class="fluid-yellow">
			<div class="container">
				<div class="row">
					<!--  <div class="col-sm-9">
                                    <ul class="left gap">
                                        <li><a href="faq.html"><i class="fa fa-question"></i>FAQ</a></li>
                                        <li><a href="help.html"><i class="fa fa-medkit"></i>Help</a></li>
                                        <li><a href="contactus.html"><i class="fa fa-envelope"></i>Contact Us</a></li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div> -->
					<div class="col-sm-3">
						<div class="text-right gap">
							<img class=""
								src="${pageContext.request.contextPath}/assets/images/member-my.png"
								alt="logo">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="fluid-grey">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="gap">&copy; 2016 Etiqa. All Rights Reserved</div>
					</div>
					<!--  <div class="col-sm-6">
                                    <ul class="right">
                                        <li><a href="terms.html">Terms</a></li>
                                        <li><a href="privacy.html">Privacy</a></li>
                                        <li><a href="disclaimer.html">Disclaimer</a></li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div> -->
				</div>
			</div>
		</div>
	</footer>
</div>
<!-- END FOOTER -->

