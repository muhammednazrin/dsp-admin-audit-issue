<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@ page import="com.etiqa.model.report.TransactionalReport"%>
<% java.text.DateFormat df=new java.text.SimpleDateFormat("dd/MM/yyyy"); %>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}
</style>

</head>

<body>
	<%
        
       
   	 DecimalFormat formatter = new DecimalFormat("#,###.00");
        
 if (request.getMethod().equals("GET") && request.getParameter("startRow") == null){
			
			session.setAttribute("ProductType", "");
		
			session.setAttribute("Status", "");
			session.setAttribute("PolicyCertificateNo", "");
			session.setAttribute("NRIC","");
			session.setAttribute("ReceiptNo", "");
			session.setAttribute("dateFrom","");
			session.setAttribute("dateTo","");
			session.setAttribute("DateFrom", "");
			session.setAttribute("DateTo","");
			session.setAttribute("PlateNo","");
			session.setAttribute("ProductEntity","");
			
 }
			
        int totalRecord = 0;
        int recordPerPage =10;
        int pageCount =0;
        int startRow=0;
        int endRow=0;
        //temporary 
        endRow=recordPerPage;
        
       int start=0;       
       int end=0;
       double lastPage=0;
       String textColor="";
       String status="";
       String totalAmount="0.00";
       double commAmount= 0.00 ;
       double commisson=10.00;
       
       String transactionStatus="";
       String lastStep="";
       String nextStep="";
       String reason="";
       int i=0;
       int rowLine=0;
       String txtColor="";
       double netAmt= 0.00 ;
       double nettAmount=10.00;
       
       
       
        
        %>



	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Agent &amp; Analytics</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Agent Performance Report</h4>
														</div>
													</div>
													<!-- Start Form -->
													<form:form name="txnreport" action="agentReport"
														method="post" onsubmit="return validateForm()">
														<input type="hidden" name="action" value="list" />
														<input type="hidden" name="startRow" value="0" />
														<input type="hidden" name="endRow" value="10" />
														<input type="hidden" id="productType" name="productType"
															value="" />
														<input type="hidden" id="productTypeValSession"
															name="productTypeValSession"
															value="<c:out value="${sessionScope.ProductType}" />" />
														<input type="hidden" id="productEntityValSession"
															name="productEntityValSession"
															value="<c:out value="${sessionScope.ProductEntity}" />" />

														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Search
																				by Date</label>
																			<%--  <div class="col-sm-4">
																				<input type="text" placeholder="Date From" class="form-control" id="datepicker1" name="dateFrom" value="${sessionScope.dateFrom}" />
																					
																					
																			</div>
																			<div class="col-sm-5">
																				<input type="text" placeholder="Date To"
																					class="form-control" id="datepicker2" name="dateTo"
																					value="${sessionScope.dateTo}">
																			</div>  --%>
																			<div class="col-sm-4">

																				<fmt:parseDate value="${now}" var="parsedEmpDate"
																					pattern="dd/MM/yyyy" />


																				<input type="text" placeholder="Date From"
																					class="form-control" id="datepicker1"
																					name="dateFrom"
																					<c:choose>
																					    <c:when test="${not empty sessionScope.dateFrom }">
																					         value="<c:out value="${sessionScope.dateFrom}" />"																					     
																					    </c:when>
																					    <c:otherwise>
																					          value="<%= df.format(new java.util.Date())%>"
																					    </c:otherwise>
																					</c:choose> />
																			</div>
																			<div class="col-sm-5">
																				<input type="text" placeholder="Date To"
																					class="form-control" id="datepicker2" name="dateTo"
																					<c:choose>
																					    <c:when test="${not empty sessionScope.dateTo }">
																					         value="<c:out value="${sessionScope.dateTo}" />"																					     
																					    </c:when>
																					    <c:otherwise>
																					          value="<%= df.format(new java.util.Date())%>"
																					    </c:otherwise>
																					</c:choose> />

																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Search
																				by Product Entity</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="productEntity"
																					id="productEntity">
																					<option value="" selected>-View All-</option>
																					<option
																						<c:if test="${sessionScope.ProductEntity == 'EIB'}" >selected</c:if>
																						value="EIB">EIB</option>
																					<option
																						<c:if test="${sessionScope.ProductEntity == 'ETB'}" >selected</c:if>
																						value="ETB">ETB</option>
																				</select>
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Product
																				Type</label>


																			<div class="col-sm-9" id="eib-list">

																				<select class="form-control" name="productTypeEIB"
																					id="productTypeEIB">
																					<option value=""
																						<c:if test="${sessionScope.ProductType == ''}" >selected</c:if>>-View
																						All-</option>
																					<option value="WTC"
																						<c:if test="${sessionScope.ProductType == 'WTC'}" >selected</c:if>>
																						World Traveller Care</option>
																					<option value="MI"
																						<c:if test="${sessionScope.ProductType == 'MI'}" >selected</c:if>>
																						Motor Insurance</option>
																					<option value="TL"
																						<c:if test="${sessionScope.ProductType == 'TL'}" >selected</c:if>>
																						Ezy-Life Secure</option>
																					<option value="building_content"
																						<c:if test="${sessionScope.ProductType == 'building_content'}" >selected</c:if>>
																						Houseowner Householder</option>

																					<option value="building"
																						<c:if test="${sessionScope.ProductType == 'building'}" >selected</c:if>>
																						Houseowner</option>
																					<option value="content"
																						<c:if test="${sessionScope.ProductType == 'content'}" >selected</c:if>>
																						Householder</option>

																				</select>


																			</div>

																			<div class="col-sm-9" id="etb-list">

																				<select class="form-control" name="productTypeETB"
																					id="productTypeETB">
																					<option value=""
																						<c:if test="${sessionScope.ProductType == ''}" >selected</c:if>>-View
																						All-</option>
																					<option value="TPT"
																						<c:if test="${sessionScope.ProductType == 'TPT'}" >selected</c:if>>
																						World Traveller Care Takaful</option>
																					<option value="TPT"
																						<c:if test="${sessionScope.ProductType == 'MT'}" >selected</c:if>>
																						Motor Takaful</option>
																					<option value="content"
																						<c:if test="${sessionScope.ProductType == 'building_content'}" >selected</c:if>>
																						Householder Takaful</option>
																					<option value="building"
																						<c:if test="${sessionScope.ProductType == 'building'}" >selected</c:if>>
																						Houseowner Takaful</option>
																					<option value="building_content"
																						<c:if test="${sessionScope.ProductType == 'content'}" >selected</c:if>>
																						Houseowner Householder Takaful</option>
																				</select>


																			</div>


																			<div class="col-sm-9" id="all-list">

																				<select class="form-control" name="productTypeALL"
																					id="productTypeALL">
																					<option value=""
																						<c:if test="${sessionScope.ProductType == ''}" >selected</c:if>>-View
																						All-</option>
																					<option value="WTC"
																						<c:if test="${sessionScope.ProductType == 'WTC'}" >selected</c:if>>
																						World Traveller Care</option>
																					<option value="MI"
																						<c:if test="${sessionScope.ProductType == 'MI'}" >selected</c:if>>
																						Motor Insurance</option>
																					<option value="TL"
																						<c:if test="${sessionScope.ProductType == 'TL'}" >selected</c:if>>
																						Ezy-Life Secure</option>
																					<option value="building_content"
																						<c:if test="${sessionScope.ProductType == 'building_content'}" >selected</c:if>>
																						Houseowner Householder</option>
																					<option value="building"
																						<c:if test="${sessionScope.ProductType == 'building'}" >selected</c:if>>
																						Houseowner</option>
																					<!-- 	<option value="building_content"
																						<c:if test="${sessionScope.ProductType == 'HOHHT'}" >selected</c:if>>
																						Houseowner Householder Takaful</option> 
																					<option value="building"
																						<c:if test="${sessionScope.ProductType == 'HO'}" >selected</c:if>>
																						Houseowner</option>-->
																					<option value="TPT"
																						<c:if test="${sessionScope.ProductType == 'TPT'}" >selected</c:if>>
																						World Traveller Care Takaful</option>
																					<option value="TPT"
																						<c:if test="${sessionScope.ProductType == 'MT'}" >selected</c:if>>
																						Motor Takaful</option>


																				</select>


																			</div>



																		</div>




																		<div class="form-group">
																			<label class="col-sm-3 control-label">Sales
																				Entity</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="salesEntity"
																					id="salesEntity">
																					<option value="" selected>-View All-</option>
																					<option
																						<c:if test="${sessionScope.salesEntity == 'Cyber Agent'}" >selected</c:if>
																						value="Cyber Agent">Cyber Agent</option>
																					<option
																						<c:if test="${sessionScope.salesEntity == 'Marketer'}" >selected</c:if>
																						value="Marketer">Marketer</option>
																				</select>
																			</div>
																		</div>


																	</div>
																</div>

																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Agent
																				Code</label>
																			<div class="col-sm-9">
																				<input type="text" placeholder=""
																					class="form-control" name="NRIC"
																					value="<c:out value="${sessionScope.NRIC}" />" />
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Agent
																				Name</label>

																			<div class="col-sm-9">
																				<input type="text" placeholder=""
																					class="form-control" name="policyNo"
																					value="<c:out value="${sessionScope.PolicyCertificateNo}" />" />
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Transaction
																				Status</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="status">
																					<option value=""
																						<c:if test="${sessionScope.Status == ''}" >selected</c:if>>-View
																						All-</option>
																					<option value="S"
																						<c:if test="${sessionScope.Status == 'S'}" >selected</c:if>>Succesful</option>
																					<option value="F"
																						<c:if test="${sessionScope.Status == 'F'}">selected</c:if>>Fail
																						Payment</option>
																					<option value="AP"
																						<c:if test="${sessionScope.Status == 'AP'}" >selected</c:if>>Attempt
																						Payment</option>
																					<option value="O"
																						<c:if test="${sessionScope.Status == 'O'}" >selected</c:if>>Incomplete</option>
																					<option value="C"
																						<c:if test="${sessionScope.Status == 'C'}" >selected</c:if>>Cancelled</option>
																					<option value="R"
																						<c:if test="${sessionScope.Status == 'R'}"  >selected</c:if>>Rejection</option>
																				</select>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Marketer
																				Code</label>
																			<div class="col-sm-9">
																				<input type="text" placeholder=""
																					class="form-control" name="receiptNo"
																					value="<c:out value="${sessionScope.ReceiptNo}" />" />
																			</div>
																		</div>

																	</div>
																</div>


																<div class="col-sm-12 text-right">
																	<button type="submit" class="btn btn-default"
																		name="SEARCH">Search</button>
																</div>
															</div>
														</div>
													</form:form>
													<!-- END FORM -->
												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="row">
																<div class="col-xs-12 form-inline">
																	<div class="col-xs-6">
																		<!--   <div class="row">
                                                                        <label>
                                                                            <select class="form-control" disabled>
                                                                              <option value="5">5</option>
                                                                              <option value="10" selected>10</option>
                                                                              <option value="25">25</option>
                                                                              <option value="50">50</option>
                                                                              <option value="100">100</option>
                                                                            </select>
                                                                            records per page
                                                                        </label>
                                                                        </div>-->
																	</div>
																	<div class="col-xs-6">
																		<div class="row">
																			<div class="pull-right" id='btnhere'>
																				<!--  <button class="btn btn-warning btn-sm">
																					Print <i class="fa fa-print" aria-hidden="true"></i>
																				</button>
																				<button class="btn btn-warning btn-sm">
																					Export to XLS <i class="fa fa-download"
																						aria-hidden="true"></i>-->
																				</button>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="table-responsive" id="MyTable">
																<table
																	class="table table-striped table-warning table-hover"
																	id="admin-datatable-second_admin">
																	<thead>
																		<tr>
																			<th colspan="2">Gross Amount</th>
																			<th colspan="2">Nett Amount</th>
																			<th colspan="2">Total Transaction Amount</th>
																			<th colspan="2">Discount</th>
																		</tr>
																		<td colspan="2">
																			<%        
                                                                if (request.getAttribute("data") != null) {
														            List<TransactionalReport> list = (List<TransactionalReport>) request.getAttribute("data");
														           
														            
														            if(list.size() > 0 ){
													            	   	
														            	totalAmount = list.get(0).getTotalAmount();
														            	commAmount = (Double.parseDouble(totalAmount))*commisson/100;
														            	String commAmount1 = String.format("%.02f", commAmount);
														            	totalAmount = list.get(0).getTotalAmount();
														            	System.out.println("netAmt  " + totalAmount );
														            	netAmt = (Double.parseDouble(totalAmount))*(0.1);
														            	System.out.println("netAmt  " + netAmt);
														            	netAmt= Double.parseDouble(totalAmount)-netAmt;
														            	System.out.println("netAmt  " + netAmt);
														            	String netAmt1 = String.format("%.02f", netAmt);
														            	
														            }
														           
																	
														            if(session.getAttribute("ProductType").equals("TL") && session.getAttribute("Status").equals("")){
														            	
														            	
														            	totalAmount = request.getAttribute("totalAmount").toString();
														            	
														            	
														            	
														            	
														            	
														            }

                                                                } %> <b>RM</b>
																			<%=totalAmount %></td>
																		<td colspan="2"><b>RM</b> <%
                          %> <% if(session.getAttribute("ProductType").equals("TL")){
	                                                                    	%>
																			- <%  } else{
	                                                                    %>
																			<%-- <%=totalAmount %> --%><%=String.format("%.02f", netAmt) %>
																			<% } %></td>
																		<td colspan="2"><b>RM</b> <%=totalAmount %></td>
																		<td colspan="2"><b>RM <%=String.format("%.02f", commAmount) %></b></td>

																		<tr>
																			<th>No</th>
																			<th style="width: 40px;">Txn Date</th>
																			<th>Pol/Cert No</th>
																			<th>Customer</th>
																			<th>Product</th>
																			<th>Payment</th>
																			<th>Status</th>
																			<th>Amount(RM)</th>
																		</tr>


																	</thead>
																	<tbody>


																		<%
                                                                 
                                                                 //System.out.println(request.getAttribute("data"));
                                                                 if (request.getAttribute("data") != null) {
														            List<TransactionalReport> list = (List<TransactionalReport>) request.getAttribute("data");
														           
														           
														            if(list.size() > 0 ){
														            	   	
														            	totalRecord = list.get(0).getRecordCount();
														            }
														            
														            
														            System.out.println(totalRecord);
														            if(totalRecord >0) {
														            	startRow= list.get(0).getId();
														            	endRow=(startRow)+recordPerPage;
														            	
														            	if (endRow > totalRecord ){
														            		
														            		endRow=totalRecord;//at last page
														            		
														            	}
														            	
														            	
														                for(TransactionalReport txn : list) {
														                	
						                		rowLine++;
														                		
														                
														                	
														                	if(txn.getPolicyNo()!=null && txn.getPolicyNo()!="" && !txn.getStatus().equals("S") && !txn.getStatus().equals("SUCCESS"))
														                	{
														                		
														                		txtColor="red";
														                		
														                	}
														                	
														                	
														        %>
																		<tr>
																			<td align="center"><%=txn.getDSPQQID()%></td>
																			<td>
																				<% if(txn.getTransactionDate()!=null && txn.getTransactionDate()!="" ) { %><%=txn.getTransactionDate()%>
																				<%} %>
																			</td>
																			<td>
																				<% if(txn.getPolicyNo()!=null && txn.getPolicyNo()!="" ) { %><%=txn.getPolicyNo()%></span></a>
																				<%} %> <% if(txn.getCAPSPolicyNo()!=null && txn.getCAPSPolicyNo()!="" ) { %><br /><%=txn.getCAPSPolicyNo()%></span></a>
																				<%} %> <br> <span> <% if(txn.getVehicleNo()!=null && txn.getVehicleNo()!="") { %>
																					<%=txn.getVehicleNo()%> <%} %>
																			</span> <%-- <td><b>Name :</b> <% if(txn.getTransactionID()!=null && txn.getTransactionID()!="" ) { %>
																				<a
																				href="TransactionalReport?action=detail&id=<%=txn.getTransactionID()%>"><%=txn.getCustomerName()%></a>
																				<%} else { %><%=txn.getCustomerName()%> <% }%><br /> <b>ID
																					No : </b><%=txn.getCustomerNRIC()%><br>
																				<% if(txn.getProductEntity() == "ETB"){ %>
																				ETIQA TAKAFUL BERHAD 
																	   <%}else{ %>ETIQA INSURANCE BERHAD
																	              <%} %>- E CHANNEL<%=txn.getAgentCode()%> --%>
																			<td><b>Name :</b> <% if(txn.getTransactionID()!=null && txn.getTransactionID()!="" ) { %>
																				<%=txn.getCustomerName()%></a> <%} else { %><%=txn.getCustomerName()%>
																				<% }%><br /> <b>ID No : </b><%=txn.getCustomerNRIC()%><br><%=txn.getAgentName()%>
																				<%if(txn.getAgentCode()!=""){%>(<%=txn.getAgentCode()%>)<%} %>

																				<% if (txn.getProductCode().equals("TL"))
                                                                    
							                                                                    {
							                                                                    	 
							                                                                    	 String coverage = txn.getCoverage();
							                                                                    	 
							                                                                    	 if(coverage != null ||  coverage !=""){
							                                                                    	 
							                                                                    		 double fmtCoverage = Double.parseDouble(coverage);
							                                                                    	 coverage = formatter.format(fmtCoverage);
							
							                                                                    	 System.out.println(coverage);
							                                                                    	 }
							                                                                    	 
							                                                                    	%>
																				<br> <b>Term:</b> <%=txn.getTerm()%> Years <br>
																				<b>Coverage:</b> RM <%=coverage%> <% 
							                                                                    }
                                                                     %>


																			</td>

																			<td>
																				<% if(txn.getProductType()!="" || txn.getProductType()!=null)   { 
																				 
																				
																				 
																				 if(txn.getProductType().indexOf("H")!=-1){
																					 
																				if(txn.getHomeCoverage().equals("content")){
																					%> Householder <% 
																				}
																					else if(txn.getHomeCoverage().equals("building")){
																					%> Houseowner <% 
																				}
																				
																					else if(txn.getHomeCoverage().equals("building_content")){
																						%> Houseowner/Householder <% 
																					}
																				
																				
																				
																				} 
																				
																				else
																					
																				{ %> <%=txn.getProductType()%> <% }
																				 
																				}
																				
                                                                    	%>
																				<br>
																				<% if(txn.getProductEntity()!=null) {%>(<%=txn.getProductEntity()%>)
																				<% } %> <!-- 	<br><%=txn.getHomeCoverage()%> -->

																			</td>

																			<td><b><%=txn.getPaymentChannel()%></b> <% if (txn.getPaymentChannel().equals("MPAY"))
                                                                    
                                                                    {
                                                                    	%>
																				<br> <b>Ref No:</b><%=txn.getMPAY_RefNo()%> <br>
																				<b>Auth Code:</b> <%=txn.getMPAY_AuthCode()%> <br>
																				<b>Mode:</b> <%=txn.getPremiumMode()%> <% 
                                                                    }
                                                                    
                                                                    else if(txn.getPaymentChannel().equals("EBPG"))
                                                                   
                                                                    	
                                                                    {
                                                                    	%>
																				<br> <b>Ref No:</b><%=txn.getEBPG_RefNo()%> <br>
																				<b>Auth Code:</b> <%=txn.getEBPG_AuthCode()%> <br>

																				<b>Tax Inv:</b> <%=txn.getInvoiceNo() %> <% 	
                                                                    	
                                                                    }
 																	else if(txn.getPaymentChannel().equals("M2U"))
                                                                   
                                                                    	
                                                                    {
                                                                    	%>
																				<br> <b>Ref No:</b><%=txn.getM2U_RefNo()%> <br>
																				<b>App Code:</b> <%=txn.getM2U_AuthCode()%> <br>

																				<b>Tax Inv:</b> <%=txn.getInvoiceNo() %> <% 	
                                                                    	
                                                                    }
                                                                    
	                                                                else if(txn.getPaymentChannel().equals("FPX"))
                                                                   
                                                                    	
                                                                    {
                                                                    	%>
																				<br> <b>Ref No:</b><%=txn.getFPX_RefNo()%> <br>
																				<b>FPX Status:</b> <%
                                                                    	System.out.println("txn.getFPX_AuthCode() "+txn.getFPX_AuthCode());
                                                                    	
                                                                    	if (txn.getFPX_AuthCode().equals("00"))
                                                            			{
                                                            				
                                                            				transactionStatus= txn.getFPX_AuthCode()+"-Successful";
                                                            					
                                                            			}
                                                            			else if (!txn.getFPX_AuthCode().equals("00") && !txn.getFPX_AuthCode().equals("") )
                                                            			{
                                                            				transactionStatus= txn.getFPX_AuthCode()+"-Unsuccessful";
                                                            				
                                                            			}
                                                            			else
                                                            			{
                                                            				
                                                            				transactionStatus="";
                                                            				
                                                            			}
                                                            			
                                                                    	
                                                                    	%>

																				<%=transactionStatus%> <br> <b>Tax Inv:</b> <%=txn.getInvoiceNo() %>
																				<% 	
                                                                    	
                                                                    }
                                                                    
                                                                    %></td>

																			<!-- start: temporary -->
																			<% if (txn.getStatus().equals("SUCCESS") || txn.getStatus().equals("S") ){
                                                                    	 
                                                                    	 status = "Successful";
                                                                    	 textColor="green";
                                                                    	 
                                                                     }
                                                                     
                                                                     if (txn.getStatus().equals("Incomplete") || txn.getStatus().equals("O"))
                                                                    	 
                                                                     {
                                                                    	
                                                                    		 
                                                                    		 status="Incomplete";
                                                                    		 textColor="";
                                                                    		 
                                                                    		
                                                                    		 System.out.println("txn.getDoneStep1() = " +txn.getDoneStep1());
                                                                    		 System.out.println("txn.getDoneStep2() =" +txn.getDoneStep2());
                                                                    		 
                                                                    		 
                                                                    		 if(txn.getDoneStep1() != "" || txn.getDoneStep1()  !=null)
                                                                    		 {
                                                                    			 
                                                                    			 
                                                                    				if(txn.getDoneStep2() == "" || txn.getDoneStep2()  ==null){
                                                                    				 
                                                                    			      lastStep = "Step 1: Get A Quote";
                                                                    			 //nextStep = "Detailed Information"; 
                                                                    			 
                                                                    			 }
                                                                    				 else if(txn.getDoneStep2() != "" || txn.getDoneStep2()  !=null){
                                                                    					 
                                                                    					 
                                                                    						 
                                                                    						 lastStep = "Step 2 : Detailed Information";	 
                                                                    						 
                                                                    					 }
                                                                    				
                                                                    			 
                                                                    		 }
                                                                    				 else{

                                                                    					 lastStep = "Step 1: Get A Quote";
                                                                    				 
                                                                    				 }
                                                                    			 
                                                                    		 
                                                                    	 
                                                                    	  
                                                                    	 
                                                                     }
                                                                    
                                                                     if (txn.getStatus().equals("Pending") || txn.getStatus().equals("AP"))
                                                                    	 
                                                                     {
                                                                    	
                                                                    		 
                                                                    		 status="Attempt Payment";
                                                                    		 textColor="";
                                                                    	 
                                                                    	  
                                                                    	 
                                                                     }
                                                                     
																	if (txn.getStatus().equals("Failed") || txn.getStatus().equals("F"))
                                                                    	 
                                                                     {
                                                                    	
                                                                    		 
                                                                    		 status="Failed";
                                                                    		 textColor="red";
                                                                    	 
                                                                    	  
                                                                    	 
                                                                     }
																	
																	if (txn.getStatus().equals("R"))
                                                                   	 
                                                                    {
                                                                   	   
																		if(txn.getReason() == null ||txn.getReason().trim() == "" ){
                                                                   		 
                                                                   		 reason="";
                                                                  
																		}
																		
																		else
																			
																		{
																		
																			reason=txn.getReason();
																			
																		}
																		
																		if(txn.getUWReason() != null &&  txn.getUWReason().trim() != ""){
																			
																			if(reason ==""){
	                                                                   		 
	                                                                   		 reason=txn.getUWReason();
	                                                                   		 
																			}
																			else
																			{
																				
																				System.out.println(reason);
																				System.out.println(txn.getUWReason());
																				if(!reason.trim().equals(txn.getUWReason().trim()) ){
																				 reason=reason+"("+txn.getUWReason()+")";	
																				}
																				
																				
																			}
	                                                                  
																			}
																			
																			
																		 status="Rejection : "+reason;
                                                                   		 textColor="red";
                                                                   		 
                                                                   	  
                                                                   	 
                                                                    }
                                                                    
																	
                                                                   if (txn.getStatus().equals("CANCELLED"))
                                                                    	 
                                                                     {
                                                                	   status="Cancelled";
                                                                    	 textColor="red";
                                                                    	 
                                                                    	 
                                                                     }
                                                                     
                                                                     
                                                                     %>
																			<!-- end: temporary -->
																			<td><font color=<%=textColor %>><%=status%>
																					<br><%=lastStep %> </font></td>
																			<td align="center"><%=txn.getPremiumAmount()%></td>
																		</tr>
																		<%
                                                                 
														                }
														                status="";
																           textColor="";
																           
														            }
																	else
																	{
																	%>
																		<tr>
																			<td colspan="8"><center>No Record
																					Registered</center></td>
																		</tr>
																		<% 	
																	
																	
														            }
                                                                 }
														            
														            else{
														            	
														            	%>
																		<tr>
																			<td colspan="8"><center>No Record
																					Registered</center></td>
																		</tr>
																		<% 	
														            }
														           
														        %>
																	</tbody>
																</table>
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>
											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->

	<script type="text/javascript">
         	$(document).ready(function() {
         		
         		
         		 $('#datepicker1').datepicker({
  				    format: 'dd/mm/yyyy',
  			}).on('changeDate', function(e){
  			    $(this).datepicker('hide');
  			});
         		 
            $('#datepicker2').datepicker({
   				    format: 'dd/mm/yyyy',
   			}).on('changeDate', function(e){
   			    $(this).datepicker('hide');
   			});
         		
                $('#eib-list').hide();
				$('#etb-list').hide();
				$('#all-list').show();
			
				
				var productTypeSelected = $('#productTypeValSession').val();
				var productEntitySelected = $('#productEntityValSession').val();
				
				if(productTypeSelected =="MI")
					{
					
					$('#mi-option').show();	
					
					}
				
				else{
					
					$('#mi-option').hide();	
					
				}
				
				if(productEntitySelected =="EIB")
				{
				
					$('#eib-list').show();
    				$('#etb-list').hide();
    				$('#all-list').hide();
    				
    				
				
				}
				
				if(productEntitySelected =="ETB")
				{
				
					$('#eib-list').hide();
    				$('#etb-list').show();
    				$('#all-list').hide();
    				
				
				}
			
				if(productEntitySelected =="")
				{
				
					$('#eib-list').hide();
    				$('#etb-list').hide();
    				$('#all-list').show();
    				
				
				}
			
			
		
				
				 $("#productEntity").on('change', function() {	
	        			//alert(this.value);
	        			if (this.value == 'ETB')
	        			{
	        				$('#eib-list').hide();
	        				$('#etb-list').show();
	        				$('#all-list').hide();
	        				$('#mi-option').hide();	
	        				//$('#mi-option').css("display","none");
	        				
	        			} 
	        			
	        			else if (this.value == 'EIB')
	        				{
	        				
	        				$('#eib-list').show();
	        				$('#etb-list').hide();
	        				$('#all-list').hide();
	        				$('#mi-option').hide();	
	        				//$('#mi-option').css("display","none");
	        				
	        				}
	        			
	        			else {
	        				$('#eib-list').hide();
	        				$('#etb-list').hide();
	        				$('#all-list').show();
	        				$('#mi-option').hide();	
	        				//$('#mi-option').css("display","none");
	        				
	        			}
	        		});
				 
				 
				 $('#productTypeEIB').on('change', function() {	
		         		//alert("ETB = "+this.value);
		         		productType.value = this.value;
		         		
		         		if(this.value == "MI"){
		         			
		         			$('#mi-option').show();	
		         			
		         			
		         		}
		         		else{
		         			
		         			$('#mi-option').hide();
		         		}
		         			
		         		});	
		         	
		         	$('#productTypeETB').on('change', function() {	
		         		//alert("EIB = "+this.value);
		         		productType.value = this.value;
                       
		         			
		         		});	
		         	
		        	$('#productTypeALL').on('change', function() {	
		         		//alert("ALL ="+this.value);
		         		productType.value = this.value;
		         			
							if(this.value == "MI"){
									         			
							$('#mi-option').show();	
									         			
							}
							else{
									
									$('#mi-option').hide();
								}
									         			
		         		});	
		         	
         		
		        	 var t = $('#admin-datatable-second_admin').DataTable( {
	         			 
		         			dom: 'Bfrtip',
		         			  buttons: [	
		{
		    extend: 'pageLength',
		    className: 'btn btn-secondary btn-sm active'
		},
		            {
		                extend: 'print',
		                className: 'btn btn-warning btn-sm',
		                title : 'Etiqa Transaction Report',
		                text: 'Print <i class="fa fa-print" aria-hidden="true"></i>'
		            },
		            {
		                extend: 'excel',
		                filename: 'Etiqa Transaction Report',
		                className: 'btn btn-warning btn-sm',
		                text:'Export to XLS <i class="fa fa-download" aria-hidden="true"></i>'
		            }
		         			  ],
		         		      "searching" :false, 
		         	          "columnDefs": [ {
		         	          "searchable": false,
		         	          "orderable": false,
		         	          "targets": [1],
		         	          "bSortable": false
		         	         
		         	        } ],
		         	        "order": [[ 0, 'desc' ]]
		         	    } );
		         	 
		         	    t.on( 'order.dt search.dt', function () {
		         	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		         	            cell.innerHTML = i+1;
		         	        } );
		         	    } ).draw();
		         	           	
		         	  
		         	   t.buttons( 0, null ).containers().appendTo( '#btnhere' );
		         	   
		         	});
		         
         
         	
    
        function validateForm() {
        	   var x = document.forms["txnreport"]["dateFrom"].value;
        	   var y = document.forms["txnreport"]["dateTo"].value;
        	   var z=  document.forms["txnreport"]["status"].value;
        	   var w=  document.forms["txnreport"]["productType"].value;
        	   
        	   if ( x == null || x == "" ) {
        		   if(y !=""){
        	      alert("Please choose Date From");
        	      document.forms["txnreport"]["dateFrom"].focus();
        	      
        	      return false;
        	   } 
        		  
        	   }
        	   
        	   if(z=="R"|| z =="O"){
        		   
        		   if(w =="")
        			   {
        			   
        			   alert ("Please choose Product Type");
        			   document.forms["txnreport"]["productType"].focus();
        			   return false;	   
        			   }      		   
        		   
        	   }
        	   return true;
        	  
        	}
        
        
        </script>
</body>
</html>