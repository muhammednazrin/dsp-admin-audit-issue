<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.math.BigDecimal"%>
<%@ page import="java.math.RoundingMode"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="com.etiqa.model.report.TransactionalReport"%>
<% java.text.DateFormat df=new java.text.SimpleDateFormat("dd/MM/yyyy"); %>


<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

.errorText {
	color: green
}

.modal-body {
	overflow-x: auto;
	overflow-y: auto;
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>

</head>

<body>


	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Payment Gateway</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Connection Status</h4>
														</div>
													</div>


													<div class="content-inner">
														<div class="the-box">
															<div class="col-sm-6">
																<div class="form-horizontal">
																	<div class="form-group">
																		<label class="col-sm-3 control-label">Payment
																			Gateway</label>
																		<form:form name="gatewayStatusForm"
																			action="checkGatewayStatus" method="post">
																			<div class="col-sm-9">
																				<select class="form-control" name="gateway"
																					id="gateway">
																					<option value="fpx">FPX</option>
																					<option value="ebpg">EBPG</option>
																					<option value="m2u">M2U</option>
																					<option value="mpay">MPAY</option>
																				</select>
																			</div>
																			<div class="col-sm-3">
																				<button type="submit" class="btn btn-default"
																					name="checkStatusBtn" id="checkStatusBtn">Check
																					Status</button>
																			</div>
																		</form:form>

																	</div>


																	<div class="row">
																		<div class="col-sm-12">

																			<ul>
																				<c:if test="${not empty gateway}">
																					<li class="errorText">Gateway: <c:out
																							value="${gateway}" /></li>
																				</c:if>
																				<c:if test="${not empty url}">
																					<li class="errorText">URL: <c:out
																							value="${url}" /></li>
																				</c:if>
																				<c:if test="${not empty errorMessage}">
																					<li class="errorText">Result: <c:out
																							value="${errorMessage}" /></li>
																				</c:if>
																				<c:if test="${not empty status}">
																					<li class="errorText">Result : <c:out
																							value="${status}" /></li>
																				</c:if>
																			</ul>

																		</div>
																	</div>

																</div>

															</div>
														</div>






													</div>

												</div>
												<%-- <c:if test="${empty motorReportList}">

			<div class="row">
			<c:out value="${emptySearch}" />
			</div>
		
			
</c:if>												
<c:if test="${not empty motorReportList}"> --%>
												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="row">
																<div class="col-xs-12 form-inline">
																	<div class="col-xs-6"></div>
																	<div class="col-xs-6">
																		<div class="row"></div>
																	</div>
																</div>
															</div>
															<div class="table-responsive" id="MyTable">
																<form:form name="changeGatewayStatus"
																	action="updateGatewayStatus" method="post">
																	<table
																		class="table table-striped table-warning table-hover"
																		id="admin-datatable-second_admin">
																		<thead>
																			<tr>
																				<th>Payment Gateway</th>
																				<th>Availability</th>
																				<th align="center">Log</th>
																			</tr>
																		</thead>
																		<tbody>
																			<c:set var="fpx" value="false" />
																			<c:set var="m2u" value="false" />
																			<c:set var="ebpg" value="false" />
																			<c:set var="mpay" value="false" />

																			<c:if test="${not empty GatewayStatusList}">
																				<c:forEach items="${GatewayStatusList}"
																					var="element" varStatus="theCount">
																					<c:if
																						test="${element.pmntGatewayCode == 'FPX' && fpx=='false'}">
																						<c:set var="fpx" value="true" />
																						<tr>
																							<td>FPX</td>
																							<td><input type="checkbox" name="showFPX"
																								id="showFPX" value="1"
																								<c:if test="${element.status == '1'}" >checked </c:if>>Yes&nbsp;&nbsp;<input
																								type="checkbox" name="showFPX" id="showFPX"
																								value="0"
																								<c:if test="${element.status == '0'}" >checked </c:if>>
																								No</td>
																							<td><a href="#" data-toggle="modal"
																								data-target="#fpxlogDetail"
																								data-value="${element.pmntGatewayCode}">View</a></td>
																						</tr>
																					</c:if>
																					<c:if
																						test="${element.pmntGatewayCode == 'EBPG' && ebpg=='false'}">
																						<c:set var="ebpg" value="true" />
																						<tr>
																							<td>EBPG</td>
																							<td><input type="checkbox" name="showEBPG"
																								id="showEBPG" value="1"
																								<c:if test="${element.status == '1'}" >checked </c:if>>Yes&nbsp;&nbsp;<input
																								type="checkbox" name="showEBPG" id="showEBPG"
																								value="0"
																								<c:if test="${element.status == '0'}" >checked </c:if>>
																								No</td>
																							<td><a href="#" data-toggle="modal"
																								data-target="#ebpglogDetail"
																								data-value="${element.pmntGatewayCode}">View</a></td>
																						</tr>
																					</c:if>
																					<c:if
																						test="${element.pmntGatewayCode == 'M2U' && m2u=='false'}">
																						<c:set var="m2u" value="true" />
																						<tr>
																							<td>M2U</td>
																							<td><input type="checkbox" name="showM2U"
																								id="showM2U" value="1"
																								<c:if test="${element.status == '1'}" >checked </c:if>>Yes&nbsp;&nbsp;<input
																								type="checkbox" name="showM2U" id="showM2U"
																								value="0"
																								<c:if test="${element.status == '0'}" >checked </c:if>>No</td>
																							<td><a href="#" data-toggle="modal"
																								data-target="#m2ulogDetail"
																								data-value="${element.pmntGatewayCode}">View</a></td>
																						</tr>
																					</c:if>
																					<c:if
																						test="${element.pmntGatewayCode == 'MPAY' && mpay=='false'}">
																						<c:set var="mpay" value="true" />
																						<tr>
																							<td>MPAY</td>
																							<td><input type="checkbox" name="showMPAY"
																								id="showMPAY" value="1"
																								<c:if test="${element.status == '1'}" >checked </c:if>>Yes&nbsp;&nbsp;<input
																								type="checkbox" name="showMPAY" id="showMPAY"
																								value="0"
																								<c:if test="${element.status == '0'}" >checked </c:if>>No</td>
																							<td><a href="#" data-toggle="modal"
																								data-target="#mpaylogDetail"
																								data-value="${element.pmntGatewayCode}">View</a></td>
																						</tr>
																					</c:if>
																				</c:forEach>
																			</c:if>
																		</tbody>
																	</table>
																	<div class="col-sm-3">
																		<button type="submit" class="btn btn-default"
																			name="save">UPDATE</button>
																	</div>
																</form:form>
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>
												<%-- </c:if> --%>
											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->


	<!-- start: modal -->
	<div class="modal fade" id="fpxlogDetail" tabindex="-1" role="dialog"
		aria-labelledby="detaillLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="reportDetailLabel">
						Connection Status Log : <span id="paymentGatewayText"></span>
					</h4>
				</div>
				<div class="modal-body">

					<div id="result"></div>
					<div class="box-body">

						<table id="example1" class="table table-bordered table-striped">
							<thead>

								<tr>

									<th>Log Date Time</th>
									<th>Status</th>
									<th>Log Data</th>
									<!-- <th>Email Sent</th> -->
									<th>Checked By</th>
								</tr>

								<c:forEach items="${FPXLogList}" var="log" varStatus="theCount"
									begin="0" end="9">

									<tr>
										<td>${log.logDatetime}</td>
										<td>${log.statusCode}</td>
										<td>${log.logData}</td>
										<%--   <td>${log.emailSent}</td> --%>
										<td>${log.createdBy}</td>
									</tr>
								</c:forEach>



							</thead>


						</table>
					</div>
					<!-- /.box-body -->



				</div>
				<div class="modal-footer">
					<!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</div>
	</div>


	<!-- end: modal -->

	<!-- start: modal -->
	<div class="modal fade" id="m2ulogDetail" tabindex="-1" role="dialog"
		aria-labelledby="detaillLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="reportDetailLabel">
						Connection Status Log : <span id="paymentGatewayText"></span>
					</h4>
				</div>
				<div class="modal-body">

					<div id="result"></div>
					<div class="box-body">

						<table id="example1" class="table table-bordered table-striped">
							<thead>

								<tr>

									<th>Log Date Time</th>
									<th>Status</th>
									<th>Log Data</th>
									<th>Email Sent</th>
									<th>Checked By</th>
								</tr>

								<c:forEach items="${M2ULogList}" var="log" varStatus="theCount"
									begin="0" end="9">

									<tr>

										<td>${log.logDatetime}</td>
										<td>${log.statusCode}</td>
										<td>${log.logData}</td>
										<td>${log.emailSent}</td>
										<td>${log.createdBy}</td>
									</tr>
								</c:forEach>



							</thead>


						</table>
					</div>
					<!-- /.box-body -->



				</div>
				<div class="modal-footer">
					<!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</div>
	</div>


	<!-- end: modal -->

	<!-- start: modal -->
	<div class="modal fade" id="ebpglogDetail" tabindex="-1" role="dialog"
		aria-labelledby="detaillLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="reportDetailLabel">
						Connection Status Log : <span id="paymentGatewayText"></span>
					</h4>
				</div>
				<div class="modal-body">

					<div id="result"></div>
					<div class="box-body">

						<table id="example1" class="table table-bordered table-striped">
							<thead>

								<tr>

									<th>Log Date Time</th>
									<th>Status</th>
									<th>Log Data</th>
									<th>Email Sent</th>
									<th>Checked By</th>
								</tr>

								<c:forEach items="${EBPGLogList}" var="log" varStatus="theCount"
									begin="0" end="9">

									<tr>

										<td>${log.logDatetime}</td>
										<td>${log.statusCode}</td>
										<td>${log.logData}</td>
										<td>${log.emailSent}</td>
										<td>${log.createdBy}</td>
									</tr>
								</c:forEach>



							</thead>


						</table>
					</div>
					<!-- /.box-body -->



				</div>
				<div class="modal-footer">
					<!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</div>
	</div>


	<!-- end: modal -->

	<!-- start: modal -->
	<div class="modal fade" id="mpaylogDetail" tabindex="-1" role="dialog"
		aria-labelledby="detaillLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="reportDetailLabel">
						Connection Status Log : <span id="paymentGatewayText"></span>
					</h4>
				</div>
				<div class="modal-body">

					<div id="result"></div>
					<div class="box-body">

						<table id="example1" class="table table-bordered table-striped">
							<thead>

								<tr>

									<th>Log Date Time</th>
									<th>Status</th>
									<th>Log Data</th>
									<th>Email Sent</th>
									<th>Checked By</th>
								</tr>

								<c:forEach items="${MPAYLogList}" var="log" varStatus="theCount"
									begin="0" end="9">

									<tr>

										<td>${log.logDatetime}</td>
										<td>${log.statusCode}</td>
										<td>${log.logData}</td>
										<td>${log.emailSent}</td>
										<td>${log.createdBy}</td>
									</tr>
								</c:forEach>



							</thead>


						</table>
					</div>
					<!-- /.box-body -->



				</div>
				<div class="modal-footer">
					<!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</div>
	</div>


	<!-- end: modal -->
	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%-- <jsp:include page="siteFooter.jsp" /> --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->
	<script
		src="${pageContext.request.contextPath}/assets/js/jquery.min.bk.js"
		type="text/javascript"></script>
	<!-- <script src="${pageContext.request.contextPath}/assets/js/jquery.min.js" type="text/javascript"></script>  -->
	<!--  <script src="assets/jAlert/jquery-1.11.3.min.js"></script> -->
	<script
		src="${pageContext.request.contextPath}/assets/jAlert/jAlert.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/jAlert/jAlert-functions.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/bootstrap.js"
		type="text/javascript"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/retina/retina.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/backstretch/jquery.backstretch.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/apps.js"></script>

	<!-- PLUGINS -->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/prettify/prettify.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<!-- <script src="assets/plugins/chosen/chosen.jquery.min.js"></script> -->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/icheck/icheck.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/mask/jquery.mask.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/summernote/summernote.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/to-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/slider/bootstrap-slider.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/toastr/toastr.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/knob.js"></script>

	<script
		src="${pageContext.request.contextPath}/assets/js/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/dataTables.bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/dataTables.buttons.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/jszip.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/pdfmake.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/vfs_fonts.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.html5.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.print.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.colVis.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/toastr.min.js"></script>


	<script type="text/javascript">
	
	// to allow only one checkbox checked
		$('input[type="checkbox"]').on('change', function() {
	    $('input[name="' + this.name + '"]').not(this).prop('checked', false);
	});

        </script>

	<script>
    
    $('#logDetail').on('show.bs.modal', function (event) {
    	
    	 var modal = $(this);

    	  var button = $(event.relatedTarget) // Button that triggered the modal
    	  var gateway = button.data('value') // Extract info from data-* attributes
    	 
  	    	   $('#paymentGatewayText').html(gateway);
  	    
  	    	
  	    	
  		  });

    </script>


</body>
</html>