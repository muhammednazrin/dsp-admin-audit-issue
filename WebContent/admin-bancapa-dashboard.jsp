<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="com.spring.admin.BPDashBoardController"%>
<%@ page import="com.spring.admin.DashBoardController"%>
<%@ page import="java.io.*,java.util.*"%>
<%@ page import="javax.servlet.*,java.text.*"%>
<%--  <%if (session.getAttribute("user") == null) {  response.sendRedirect(request.getContextPath() + "/admin-login.jsp"); }%> --%>


<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />

<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

.ddwidth {
	max-width: 170px;
	margin: 0 auto;
}

.width10 {
	max-width: 1%;
}

.errorText {
	color: red
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<%--  <link href="${pageContext.request.contextPath}/plugins/c3-chart/c3.min.css" rel="stylesheet"> --%>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th, .dataTable tr td {
	text-align: center;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>


<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link href="css/bootstrap.min.css" rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/plugins/c3-chart/c3.min.css"
	rel="stylesheet">

</head>
<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">

		<jsp:include page="bancaPA/bancaHeader.jsp" />
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<jsp:include page="bancaPA/menu-banca.jsp" />

					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="getBPDashboard">Dashboard</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>









							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<table width="100%">
																<tr>
																	<td>
																		<h4>Daily New Business Summary
																			(${bpdashBoard.fromdate})</h4>
																	</td>
																	<td>
																		<form name="rptForm" id="rptForm" method="post"
																			onsubmit="return validateForm()">
																			<%-- <input type="hidden" name="CSRFToken"  value="<%=session.getAttribute("CSRFToken") %>" />
																		 --%>
																			<div class="col-sm-6">
																				<table width="450px">
																					<tr>
																						<td><h4>Select Date :</h4></td>
																						<td><input type="text" readonly id="fromdate"
																							required value="${bpdashBoard.fromdate}" required
																							name="fromdate" class="form-control"></td>
																						<td><input value="Search"
																							class="btn btn-default" type="submit" /></td>
																					</tr>
																				</table>
																		</form>
																	</td>
																</tr>

															</table>




														</div>
													</div>
													<div class="content-inner">
														<div class="row">
															<!-- row -->
															<div class="col-sm-12">
																<!-- group 2 -->
																<div class="row">
																	<div class="col-sm-6">
																		<div class="the-box">
																			<p class="small-title cat-head-2 text-center">Banca
																				Personal Accident</p>

																			<div id="renewalretention" style="height: 200px;"></div>

																		</div>
																	</div>
																	<!-- /.col-sm-12 -->
																	<div class="col-sm-3">
																		<!-- BEGIN TODAY VISITOR TILES -->
																		<div
																			class="panel panel-danger panel-square panel-no-border">
																			<div class="panel-body">
																				<p class="cat-head text-center">EIB</p>
																				<h1 class="bolded tiles-number text-center">
																					<c:choose>
																						<c:when
																							test="${bpdashBoard.f_BPA_TotalPoliciesSold>'0'}">
																							<c:out
																								value="${bpdashBoard.f_BPA_TotalPoliciesSold}" />
																						</c:when>
																						<c:otherwise>
																							<c:out value="0" />
																						</c:otherwise>
																					</c:choose>
																				</h1>
																				<p class="text-center text20">
																					<strong>RM <c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_Sold_Amount>'0'}">
																								<c:out value="${bpdashBoard.f_BPA_Sold_Amount}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose>
																					</strong>
																				</p>
																				<div class="gap gap-mini"></div>
																				<p>
																					Total Policy (MTD) <span class="pull-right">
																						<c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_TotalPoliciesSold_PM>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_TotalPoliciesSold_PM}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose>
																					</span>
																				</p>
																				<p>
																					Total Policy (YTD) <span class="pull-right">

																						<c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_TotalPoliciesSold_PY>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_TotalPoliciesSold_PY}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose>
																					</span>
																				</p>
																				<div class="gap gap-mini"></div>
																				<p class="small">
																					Premium (MTD) <span class="pull-right"> <c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_Sold_Amount_M>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_Sold_Amount_M}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose>
																					</span>
																				</p>
																				<p class="small">
																					Premium (YTD) <span class="pull-right">RM <c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_Sold_Amount_Y>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_Sold_Amount_Y}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose>
																					</span>
																				</p>
																			</div>
																			<!-- /.panel-body -->
																		</div>
																		<!-- /.panel panel-success panel-block-color -->
																		<!-- END TODAY VISITOR TILES -->

																	</div>
																	<!-- /.col-sm-3 -->
																	<div class="col-sm-3">
																		<!-- BEGIN TODAY VISITOR TILES -->
																		<div
																			class="panel panel-warning panel-square panel-no-border">
																			<div class="panel-body">
																				<p class="cat-head text-center">ETB</p>
																				<h1 class="bolded tiles-number text-center">
																					<c:choose>
																						<c:when
																							test="${bpdashBoard.f_BPA_ETB_TotalPoliciesSold>'0'}">
																							<c:out
																								value="${bpdashBoard.f_BPA_ETB_TotalPoliciesSold}" />
																						</c:when>
																						<c:otherwise>
																							<c:out value="0" />
																						</c:otherwise>
																					</c:choose>
																				</h1>
																				<p class="text-center text20">
																					<strong>RM <c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_ETB_Sold_Amount>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_ETB_Sold_Amount}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose></strong>
																				</p>
																				<div class="gap gap-mini"></div>
																				<p>
																					Total Policy (MTD) <span class="pull-right">
																						<c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_ETB_TotalPoliciesSold_PM>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_ETB_TotalPoliciesSold_PM}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose>
																					</span>
																				</p>
																				<p>
																					Total Policy (YTD) <span class="pull-right">
																						<c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_ETB_TotalPoliciesSold_PY>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_ETB_TotalPoliciesSold_PY}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose>
																					</span>
																				</p>
																				<div class="gap gap-mini"></div>
																				<p class="small">
																					Premium (MTD) <span class="pull-right">RM <c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_ETB_Sold_Amount_M>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_ETB_Sold_Amount_M}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose></span>
																				</p>
																				<p class="small">
																					Premium (YTD) <span class="pull-right">RM <c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_ETB_Sold_Amount_Y>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_ETB_Sold_Amount_Y}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose></span>
																				</p>
																			</div>
																			<!-- /.panel-body -->
																		</div>
																		<!-- /.panel panel-success panel-block-color -->
																		<!-- END TODAY VISITOR TILES -->

																	</div>
																	<!-- /.col-sm-3 -->

																</div>
															</div>
															<!-- group 1 -->

															<div class="col-sm-12">
																<!-- group 1 -->
																<div class="row">
																	<div class="col-sm-3">
																		<!-- BEGIN TODAY VISITOR TILES -->
																		<div
																			class="panel panel-warning panel-square panel-no-border">
																			<div class="panel-body">
																				<p class="cat-head text-center">Premier PA Plus</p>
																				<h1 class="bolded tiles-number text-center">
																					<c:choose>
																						<c:when
																							test="${bpdashBoard.f_BPA_EIB_PPA_TPoliciesSold>'0'}">
																							<c:out
																								value="${bpdashBoard.f_BPA_EIB_PPA_TPoliciesSold}" />
																						</c:when>
																						<c:otherwise>
																							<c:out value="0" />
																						</c:otherwise>
																					</c:choose>
																				</h1>
																				<p class="text-center text20">
																					<strong>RM <c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_EIB_PPA_Sold_Amount>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_EIB_PPA_Sold_Amount}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose></strong>
																				</p>
																				<div class="gap gap-mini"></div>
																				<p>
																					Total Policy (MTD) <span class="pull-right">
																						<c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_ETB_PPA_TPoliciesSold_PM>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_ETB_PPA_TPoliciesSold_PM}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose>
																					</span>
																				</p>
																				<p>
																					Total Policy (YTD) <span class="pull-right">
																						<c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_ETB_PPA_TPoliciesSold_PY>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_ETB_PPA_TPoliciesSold_PY}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose> <span>
																				</p>
																				<div class="gap gap-mini"></div>
																				<p class="small">
																					Premium (MTD) <span class="pull-right">RM <c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_EIB_PPA_Sold_Amount_M>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_EIB_PPA_Sold_Amount_M}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose></span>
																				</p>
																				<p class="small">
																					Premium (YTD) <span class="pull-right">RM <c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_EIB_PPA_Sold_Amount_Y>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_EIB_PPA_Sold_Amount_Y}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose></span>
																				</p>
																			</div>
																			<!-- /.panel-body -->
																		</div>
																		<!-- /.panel panel-success panel-block-color -->
																		<!-- END TODAY VISITOR TILES -->
																	</div>
																	<!-- /.col-sm-3 -->
																	<div class="col-sm-3">
																		<!-- BEGIN TODAY VISITOR TILES -->
																		<div
																			class="panel panel-success panel-square panel-no-border">
																			<div class="panel-body">
																				<p class="cat-head text-center">Takaful PA Plus</p>
																				<h1 class="bolded tiles-number text-center">
																					<c:choose>
																						<c:when
																							test="${bpdashBoard.f_BPA_ETB_PPA_TPoliciesSold>'0'}">
																							<c:out
																								value="${bpdashBoard.f_BPA_ETB_PPA_TPoliciesSold}" />
																						</c:when>
																						<c:otherwise>
																							<c:out value="0" />
																						</c:otherwise>
																					</c:choose>
																				</h1>
																				<p class="text-center text20">
																					<strong>RM <c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_ETB_PPA_Sold_Amount>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_ETB_PPA_Sold_Amount}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose></strong>
																				</p>
																				<div class="gap gap-mini"></div>
																				<p>
																					Total Policy (MTD) <span class="pull-right">
																						<c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_ETB_PPA_TPoliciesSold_PM>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_ETB_PPA_TPoliciesSold_PM}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose>
																					</span>
																				</p>
																				<p>
																					Total Policy (YTD) <span class="pull-right">
																						<c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_ETB_PPA_TPoliciesSold_PY>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_ETB_PPA_TPoliciesSold_PY}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose>
																					</span>
																				</p>
																				<div class="gap gap-mini"></div>
																				<p class="small">
																					Premium (MTD) <span class="pull-right">RM <c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_ETB_PPA_Sold_Amount_M>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_ETB_PPA_Sold_Amount_M}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose></span>
																				</p>
																				<p class="small">
																					Premium (YTD) <span class="pull-right">RM <c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_ETB_PPA_Sold_Amount_Y>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_ETB_PPA_Sold_Amount_Y}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose>
																					</span>
																				</p>
																			</div>
																			<!-- /.panel-body -->
																		</div>
																		<!-- /.panel panel-success panel-block-color -->
																		<!-- END TODAY VISITOR TILES -->

																	</div>
																	<!-- /.col-sm-3 -->
																	<div class="col-sm-3">
																		<!-- BEGIN TODAY VISITOR TILES -->
																		<div
																			class="panel panel-danger panel-square panel-no-border">
																			<div class="panel-body">
																				<p class="cat-head text-center">Takaful
																					PesonaLady</p>
																				<h1 class="bolded tiles-number text-center">
																					<c:choose>
																						<c:when
																							test="${bpdashBoard.f_BPA_ETB_Lady_TPolSold>'0'}">
																							<c:out
																								value="${bpdashBoard.f_BPA_ETB_Lady_TPolSold}" />
																						</c:when>
																						<c:otherwise>
																							<c:out value="0" />
																						</c:otherwise>
																					</c:choose>
																				</h1>
																				<p class="text-center text20">
																					<strong>RM <c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_ETB_Lady_Sold_Amount>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_ETB_Lady_Sold_Amount}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose></strong>
																				</p>
																				<div class="gap gap-mini"></div>
																				<p>
																					Total Policy (MTD) <span class="pull-right">
																						<c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_ETB_Lady_TPolSold_PM>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_ETB_Lady_TPolSold_PM}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose>

																					</span>
																				</p>
																				<p>
																					Total Policy (YTD) <span class="pull-right">
																						<c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_ETB_Lady_TPolSold_PY>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_ETB_Lady_TPolSold_PY}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose>

																					</span>
																				</p>
																				<div class="gap gap-mini"></div>
																				<p class="small">
																					Premium (MTD) <span class="pull-right">RM <c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_ETB_Lady_Sold_Amount_M>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_ETB_Lady_Sold_Amount_M}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose>
																					</span>
																				</p>
																				<p class="small">
																					Premium (YTD) <span class="pull-right">RM <c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_ETB_Lady_Sold_Amount_Y>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_ETB_Lady_Sold_Amount_Y}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose>
																					</span>
																				</p>
																			</div>
																			<!-- /.panel-body -->
																		</div>
																		<!-- /.panel panel-success panel-block-color -->
																		<!-- END TODAY VISITOR TILES -->

																	</div>
																	<!-- /.col-sm-3 -->
																	<div class="col-sm-3">
																		<!-- BEGIN TODAY VISITOR TILES -->
																		<div
																			class="panel panel-primary panel-square panel-no-border">
																			<div class="panel-body">
																				<p class="cat-head text-center">Takaful Hero 15</p>
																				<h1 class="bolded tiles-number text-center">
																					<c:choose>
																						<c:when
																							test="${bpdashBoard.f_BPA_ETB_Hero_TPolSold>'0'}">
																							<c:out
																								value="${bpdashBoard.f_BPA_ETB_Hero_TPolSold}" />
																						</c:when>
																						<c:otherwise>
																							<c:out value="0" />
																						</c:otherwise>
																					</c:choose>
																				</h1>
																				<p class="text-center text20">
																					<strong>RM <c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_ETB_Hero_Sold_Amount>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_ETB_Hero_Sold_Amount}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose></strong>
																				</p>
																				<div class="gap gap-mini"></div>
																				<p>
																					Total Policy (MTD) <span class="pull-right">

																						<c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_ETB_Hero_TPolSold_PM>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_ETB_Hero_TPolSold_PM}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose>
																					</span>
																				</p>
																				<p>
																					Total Policy (YTD) <span class="pull-right">
																						<c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_ETB_Hero_TPolSold_PY>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_ETB_Hero_TPolSold_PY}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose>
																					</span>
																				</p>
																				<div class="gap gap-mini"></div>
																				<p class="small">
																					Premium (MTD) <span class="pull-right">RM <c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_ETB_Hero_Sold_Amount_M>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_ETB_Hero_Sold_Amount_M}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose></span>
																				</p>
																				<p class="small">
																					Premium (YTD) <span class="pull-right">RM <c:choose>
																							<c:when
																								test="${bpdashBoard.f_BPA_ETB_Hero_Sold_Amount_Y>'0'}">
																								<c:out
																									value="${bpdashBoard.f_BPA_ETB_Hero_Sold_Amount_Y}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose></span>
																				</p>
																			</div>
																			<!-- /.panel-body -->
																		</div>
																		<!-- /.panel panel-success panel-block-color -->
																		<!-- END TODAY VISITOR TILES -->

																	</div>
																	<!-- /.col-sm-3 -->
																</div>
															</div>
															<!--end group 2 -->
														</div>
														<!--end row -->
													</div>
												</div>

											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
















						</div>
					</div>
				</div>
				<!-- /.container -->

			</div>
			<!-- /.page-content -->
			<!-- BEGIN FOOTER -->
			<jsp:include page="pageFooter.jsp" />

			<!-- END FOOTER -->
		</div>
		<!-- /.wrapper -->
		<!-- END PAGE CONTENT -->



		<!-- BEGIN BACK TO TOP BUTTON -->
		<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
		<!-- END BACK TO TOP -->


		<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

		<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

		<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
		<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
		<script
			src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/js/bootstrap-select.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/js/bootstrap-tabcollapse.js"></script>

		<script
			src="${pageContext.request.contextPath}/assets/plugins/retina/retina.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/nicescroll/jquery.nicescroll.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/backstretch/jquery.backstretch.min.js"></script>


		<!-- PLUGINS -->
		<script
			src="${pageContext.request.contextPath}/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/prettify/prettify.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

		<script
			src="${pageContext.request.contextPath}/assets/plugins/chosen/chosen.jquery.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/icheck/icheck.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/mask/jquery.mask.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/validator/bootstrapValidator.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.highlight.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/summernote/summernote.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/markdown/markdown.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/markdown/to-markdown.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/markdown/bootstrap-markdown.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/slider/bootstrap-slider.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/toastr/toastr.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/placeholder/jquery.placeholder.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/footable-3/js/footable.js"></script>

		<!-- C3 JS -->
		<script
			src="${pageContext.request.contextPath}/assets/plugins/c3-chart/d3.v3.min.js"
			charset="utf-8"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/c3-chart/c3.min.js"></script>

		<!-- highcharts JS -->
		<script
			src="${pageContext.request.contextPath}/assets/plugins/highcharts/highcharts.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/highcharts/modules/funnel.js"></script>

		<!-- KNOB JS -->
		<!--[if IE]>
        <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
		<script
			src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/jquery.knob.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/knob.js"></script>

		<!-- MAIN APPS JS -->
		<script src="${pageContext.request.contextPath}/assets/js/apps.js"></script>




		<script>
			function validateForm() {
				var x = document.forms["rptForm"]["fromdate"].value;

				if (x == null || x == "") {

					alert("Please choose Date From");
					document.forms["rptForm"]["fromdate"].focus();

					return false;
				}

				return true;

			}
		</script>

		<script type="text/javascript">
			$(document).ready(
					function() {
						//chart for Renewal Retention(MTD)
						var chart = c3.generate({
							bindto : '#renewalretention',
							data : {
								// iris data from R
								columns : [ [ 'Takaful Hero 15', ${bpdashBoard.f_BPA_ETB_Hero_TPolSold} ], [ 'Premier PA Plus', ${bpdashBoard.f_BPA_EIB_PPA_TPoliciesSold} ],
										[ 'Takaful PA Plus', ${bpdashBoard.f_BPA_ETB_PPA_TPoliciesSold} ], [ 'Takaful PesonaLady', ${bpdashBoard.f_BPA_ETB_Lady_TPolSold} ], ],
								type : 'pie',
								colors : {
									WTC : '#337ab7',
									HOHH : '#d9534f',
									Motor : '#f0ad4e',
									TermLife : '#5cb85c',
								},
							},

							legend : {
								show : true
							},

						});
					});
		</script>

		<script>
		$(document).ready(function() {

			$('#admin-datatable-second_admin').DataTable({
				"order" : [],
				dom : 'Bfrtip',
				title : ' Agent List',

				buttons : [ 'copy', 'csv', 'print', {
					extend : 'excelHtml5',
					title : 'Audit Trail - Payments'
				}, {
					extend : 'pdfHtml5',
					title : 'Audit Trail - Payments'
				} ]
			});

			// $('#date1').datepicker();
			$("#fromdate").datepicker({
				format : 'dd/mm/yyyy',
				autoclose : true,
			}).on('changeDate', function(ev) {
				$(this).datepicker('hide');
			});
			$("#todate").datepicker({
				format : 'dd/mm/yyyy',
				autoclose : true,
			}).on('changeDate', function(ev) {
				$(this).datepicker('hide');
			});

		
		});
	</script>


		<script>
		function validateForm() {
			var x = document.forms["rptForm"]["fromdate"].value;
			var y = document.forms["rptForm"]["todate"].value;

			if (x == null || x == "") {

				alert("Please choose Date From");
				document.forms["rptForm"]["fromdate"].focus();

				return false;
			}

			return true;

		}
	</script>
</body>
</html>