
<%@ page import="java.util.List"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="com.etiqa.model.report.TransactionalReport"%>
<%@ page import="java.io.*,java.util.*"%>
<%@ page import="javax.servlet.*,java.text.*"%>
<jsp:useBean id="obj" class="com.etiqa.utils.SecurityUtil" />

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="/eosuam/assets/js/jquery.dataTables.min.css" rel="stylesheet"
	type="text/css">
<link href="/eosuam/assets/js/buttons.dataTables.min.css" rel="stylesheet"
	type="text/css">
<!-- <link href="/eosuam/assets/js/bootstrap.min.css" rel="stylesheet"
			type="text/css">
		<link href="/eosuam/assets/js/dataTables.bootstrap.min.css" rel="stylesheet"
			type="text/css"> -->
<link href="/eosuam/assets/js/buttons.bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link rel="stylesheet" href="/eosuam/assets/css/toastr.min.css">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}
</style>


<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}
</style>
<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<!-- <link href="/eosuam/assets/css/bootstrap.min.css" rel="stylesheet"> -->
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet"
	href="/eosuam/assets/plugins/datepicker/datepicker.min.css">
<!-- PLUGINS CSS -->
<link href="/eosuam/assets/plugins/owl-carousel/owl.carousel.min.css"
	rel="stylesheet">
<link href="/eosuam/assets/plugins/owl-carousel/owl.theme.min.css"
	rel="stylesheet">
<link href="/eosuam/assets/plugins/owl-carousel/owl.transitions.min.css"
	rel="stylesheet">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="/eosuam/assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="/eosuam/assets/css/owl.css" rel="stylesheet">
<link href="/eosuam/assets/css/bootstrap.css" rel="stylesheet">
<link href="/eosuam/assets/css/bootstrap-select.css" rel="stylesheet">
<link href="/eosuam/assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="/eosuam/assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="/eosuam/assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" href="/eosuam/assets/jAlert/jAlert.css">
<!-- <link rel="stylesheet" type="text/css" href="/eosuam/assets/plugins/all.min.css" /> -->


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
		        <script src="libs/html5shiv.js"></script>
		        <script src="libs/respond.min.js"></script>
		        <![endif]-->


<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

form#agentE, form#agentD {
	display: inline-block;
}

form#agentE input[type=submit], form#agentD input[type=submit] {
	padding: 5px;
}
</style>


</head>

<body>

	<!--
		        ===========================================================
		        BEGIN PAGE
		        ===========================================================
		        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">- Agent Product Management</a></li>
											</ol>
											<!-- End breadcrumb -->

										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">


												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">

															<div class="content-inner">
																<div class="the-box">
																	<form name="rptForm" id="rptForm" method="post"
																		action="${pageContext.request.contextPath}/getApprovalList">
																		<%-- <input type="hidden" name="CSRFToken"  value="<%=session.getAttribute("CSRFToken") %>" />
			 --%>
																		<div class="col-sm-6">
																			<div class="form-horizontal">
																				<div class="form-group">
																					<label class="col-sm-4 control-label">Application
																						Type <span class="text-danger">*</span>
																					</label> <select class="form-control" name="appType">
																						<option value="" selected>-View All-</option>
																						<c:forEach items="${applicationTypeList}"
																							var="element">
																							<option value="<c:out value="${element.id}"/>">
																								<c:out value="${element.description}" />
																							</option>
																						</c:forEach>
																					</select>

																				</div>
																			</div>
																		</div>

																		<div class="col-sm-6">
																			<div class="form-horizontal">
																				<div class="form-group">
																					<label class="col-sm-4 control-label">Status
																						<span class="text-danger">*</span>
																					</label> <select class="form-control" name="status">
																						<option value="" selected>-View All-</option>
																						<option value="1">Pending</option>
																						<option value="2">Rejected</option>
																						<option value="3">Approved</option>
																					</select>

																				</div>
																			</div>
																		</div>


																		<div class="col-sm-12 text-right">
																			<input value="Search" class="btn btn-default"
																				type="submit" />
																		</div>
																	</form>
																</div>




															</div>


															<div class="col-sm-12">

																<!--No record found Invisible  -->
																<div id="noRecordFound"
																	class="alert alert-danger fade in alert-dismissable hidediv">
																	<!--   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X—</button> -->
																	<strong><i class="fa fa-exclamation"></i> No
																		record found</strong>
																</div>
																<!--No record found Invisible  -->
																<div class="content-inner">
																	<div class="the-box static">
																		<div class="title-second">
																			<div class="sub">
																				<h4>Agent Product Approval From ${fromdate} -
																					${todate}</h4>
																			</div>
																		</div>

																		<div>
																			<div class="form-horizontal" id="MyTable">
																				<div id="dvData">
																					<!-- class="table table-striped table-hover" -->
																					<!--  <button id="exportButton" class="btn btn-lg btn-danger clearfix"><span class="fa fa-file-excel-o"></span> Export to Excel</button>-->

																					<table id="admin-datatable-second_admin"
																						width="900px"
																						class="table table-striped table-hover">
																						<thead>
																							<tr>
																								<th>No</th>
																								<th>Maker</th>
																								<th>Checker</th>
																								<th>Application Type</th>
																								<th>Created Date</th>
																								<th>Status</th>
																								<th>Action</th>
																							</tr>
																						</thead>
																						<tbody>
																							<c:forEach items="${approvalLogList}"
																								var="element" varStatus="loop">
																								<c:set var="approveStatus"
																									value='${element.status}' scope="request" />
																								<%
																									
																									String approveStatusVal = request.getAttribute("approveStatus").toString();
																									String encryptMsg =obj.enciptString(approveStatusVal); 
																									 pageContext.setAttribute("encryptMsg", encryptMsg);
																									%>


																								<%-- <c:set var="encryptMsgVal" value='${encryptMsg}' scope="request"/> --%>

																								<tr>
																									<td></td>
																									<td align='left'>${element.maker}</td>

																									<td align='left'>${element.checker}</td>
																									<td><c:choose>
																											<c:when test="${element.appType == '2'}">
																										          MOTOR INSURANCE - CHANGE PRODUCT RATE
																										         </c:when>
																											<c:when test="${element.appType == '4'}">
																										          MOTOR TAKFUL - CHANGE PRODUCT RATE
																										         </c:when>
																											<c:when test="${element.appType == '7'}">
																										         AGENT PRODUCT MAPPING
																										         </c:when>
																											<c:when test="${element.appType == '8'}">
																										         HOHH INSURANCE - CHANGE PRODUCT RATE
																										         </c:when>
																											<c:when test="${element.appType == '6'}">
																										         HOHH TAKAFUL - CHANGE PRODUCT RATE
																										         </c:when>
																											<c:when test="${element.appType == '1'}">
																										        MOTOR INSURANCE - CHANGE PRODUCT INFORMATION
																										         </c:when>
																											<c:when test="${element.appType == '9'}">
																										        MOTOR INSURANCE -RULES MANAGEMENT
																										         </c:when>
																											<c:when test="${element.appType == '3'}">
																										        MOTOR TAKAFUL - CHANGE PRODUCT INFORMATION
																										         </c:when>
																											<c:when test="${element.appType == '11'}">
																										        EASY LIFE SECURE - CHANGE PRODUCT INFORMATION
																										         </c:when>
																											<c:when test="${element.appType == '10'}">
																										        EZY-LIFE SECURE - CHANGE PRODUCT RATE
																										         </c:when>
																											<c:when test="${element.appType == '15'}">
																										        HOHH - RULES MANAGEMENT
																										         </c:when>
																											<c:when test="${element.appType == '13'}">
																										        EZY-LIFE SECURE - RULES MANAGEMENT
																										         </c:when>

																											<c:when test="${element.appType == '14'}">
																										        I-DOUBLE SECURE - RULES MANAGEMENT
																										         </c:when>


																											<c:when test="${element.appType == '12'}">
																										        WTC/T - RULES MANAGEMENT
																										         </c:when>
																											<c:when test="${element.appType == '5'}">
																										        WTC/T - CHANGE PRODUCT RATE
																										         </c:when>
																											<c:when test="${element.appType == '16'}">
																										        WTC/T - CHANGE  PRODUCT  INFORMATION
																										         </c:when>
																											<c:when test="${element.appType == '17'}">
																										        HOHH - CHANGE PRODUCT INFORMATION
																										         </c:when>

																											<c:when test="${element.appType == '4'}">
		                                                                                                         MOTOR TAKFUL - CHANGE PRODUCT RATE
		                                                                                                          </c:when>
																											<c:when test="${element.appType == '18'}">
		                                                                                                         BUDDY PA - CHANGE PLAN INFORMATION
		                                                                                                          </c:when>

																											<c:when test="${element.appType == '19'}">
		                                                                                                         BUDDY PA - CHANGE COMBO INFORMATION
		                                                                                                          </c:when>

																											<c:when test="${element.appType == '20'}">
		                                                                                                         BUDDY PA - CHANGE BENEFIT INFORMATION
		                                                                                                          </c:when>

																											<c:when test="${element.appType == '21'}">
		                                                                                                         BUDDY PA - CHANGE BENEFIT GROUPING INFORMATION
		                                                                                                          </c:when>

																											<c:when test="${element.appType == '22'}">
		                                                                                                         BUDDY PA - CHANGE PRODUCT INFORMATION
		                                                                                                          </c:when>

																											<c:when test="${element.appType == '23'}">
		                                                                                                         TRAVEL EZY - CHANGE PRODUCT INFORMATION
		                                                                                                          </c:when>

																											<c:when test="${element.appType == '25'}">
																										        TRIPCARE360 - CHANGE PRODUCT RATE
																										         </c:when>
																											<c:when test="${element.appType == '24'}">
																										        TRIPCARE360 - CHANGE PRODUCT INFORMATION
																										         </c:when>
																										</c:choose></td>
																									<td align='left'>${element.createDate}</td>

																									<td align='left'><c:choose>
																											<c:when test="${element.status == '1'}">
																												<font color="Blue"><b> Pending </b></font>
																											</c:when>
																											<c:when test="${element.status == '2'}">
																												<font color="Red"><b> Rejected</b></font>
																											</c:when>
																											<c:when test="${element.status == '3'}">
																												<font color="Green"> <b> Approved
																												</b></font>
																											</c:when>
																										</c:choose></td>



																									<td align='center'><c:if
																											test="${element.appType eq '7'}">
																											<a
																												href="approvalAgentProductMap?id=${element.id}&appType=${element.appType}&appstatus=${encryptMsg}"><button
																													class="btn btn-warning btn-sm">
																													View <i class="fa fa-edit"
																														aria-hidden="true"></i>
																												</button></a>
																										</c:if> <c:if test="${element.appType eq '2'}">
																											<a
																												href="approvalMIProduct?id=${element.id}&appType=${element.appType}&appstatus=${encryptMsg}"><button
																													class="btn btn-warning btn-sm">
																													View <i class="fa fa-edit"
																														aria-hidden="true"></i>
																												</button></a>
																										</c:if> <c:if test="${element.appType eq '8'}">
																											<a
																												href="approvalHOHHInsProduct?id=${element.id}&appType=${element.appType}&appstatus=${encryptMsg}"><button
																													class="btn btn-warning btn-sm">
																													View <i class="fa fa-edit"
																														aria-hidden="true"></i>
																												</button></a>
																										</c:if> <c:if test="${element.appType eq '6'}">
																											<a
																												href="approvalHOHHTakafulProduct?id=${element.id}&appType=${element.appType}&appstatus=${encryptMsg}"><button
																													class="btn btn-warning btn-sm">
																													View <i class="fa fa-edit"
																														aria-hidden="true"></i>
																												</button></a>
																										</c:if> <c:if test="${element.appType eq '1'}">
																											<a
																												href="approvalMIProductInfo?id=${element.id}&appType=${element.appType}&appstatus=${encryptMsg}"><button
																													class="btn btn-warning btn-sm">
																													View <i class="fa fa-edit"
																														aria-hidden="true"></i>
																												</button></a>
																										</c:if> <c:if
																											test="${element.appType eq '9' || element.appType eq '15' || element.appType eq '13' || element.appType eq '14'}">
																											<a
																												href="approvalMIRulesManagement?id=${element.id}&appType=${element.appType}&appstatus=${encryptMsg}"><button
																													class="btn btn-warning btn-sm">
																													View <i class="fa fa-edit"
																														aria-hidden="true"></i>
																												</button></a>
																										</c:if> <c:if test="${element.appType eq '3'}">
																											<a
																												href="approvalMTProductInfo?id=${element.id}&appType=${element.appType}&appstatus=${encryptMsg}"><button
																													class="btn btn-warning btn-sm">
																													View <i class="fa fa-edit"
																														aria-hidden="true"></i>
																												</button></a>
																										</c:if> <c:if test="${element.appType eq '11'}">
																											<a
																												href="approvalEasyLifeProductInfo?id=${element.id}&appType=${element.appType}&appstatus=${encryptMsg}"><button
																													class="btn btn-warning btn-sm">
																													View <i class="fa fa-edit"
																														aria-hidden="true"></i>
																												</button></a>
																										</c:if> <c:if test="${element.appType eq '10'}">
																											<a
																												href="approvalEasyLifeProductRate?id=${element.id}&appType=${element.appType}&appstatus=${encryptMsg}"><button
																													class="btn btn-warning btn-sm">
																													View <i class="fa fa-edit"
																														aria-hidden="true"></i>
																												</button></a>
																										</c:if> <c:if test="${element.appType eq '12'}">
																											<a
																												href="approvalWTCRulesManagement?id=${element.id}&appType=${element.appType}&appstatus=${encryptMsg}"><button
																													class="btn btn-warning btn-sm">
																													View <i class="fa fa-edit"
																														aria-hidden="true"></i>
																												</button></a>
																										</c:if> <c:if test="${element.appType eq '5'}">
																											<a
																												href="approvalWTCProductrate?id=${element.id}&appType=${element.appType}&appstatus=${encryptMsg}"><button
																													class="btn btn-warning btn-sm">
																													View <i class="fa fa-edit"
																														aria-hidden="true"></i>
																												</button></a>
																										</c:if> <c:if test="${element.appType eq '16'}">
																											<a
																												href="approvalWTCProductInfo?id=${element.id}&appType=${element.appType}&appstatus=${encryptMsg}"><button
																													class="btn btn-warning btn-sm">
																													View <i class="fa fa-edit"
																														aria-hidden="true"></i>
																												</button></a>
																										</c:if> <c:if test="${element.appType eq '17'}">
																											<a
																												href="approvalHohhProductInfo?id=${element.id}&appType=${element.appType}&appstatus=${encryptMsg}"><button
																													class="btn btn-warning btn-sm">
																													View <i class="fa fa-edit"
																														aria-hidden="true"></i>
																												</button></a>
																										</c:if> <c:if test="${element.appType eq '4'}">
																											<a
																												href="approvalMTProduct?id=${element.id}&appType=${element.appType}&appstatus=${encryptMsg}"><button
																													class="btn btn-warning btn-sm">
																													View <i class="fa fa-edit"
																														aria-hidden="true"></i>
																												</button></a>
																										</c:if> <c:if test="${element.appType eq '18'}">
																											<a
																												href="approvalBuddyPAPlan?id=${element.id}&appType=${element.appType}&appstatus=${encryptMsg}">
																												<button class="btn btn-warning btn-sm">
																													View <i class="fa fa-edit"
																														aria-hidden="true"></i>
																												</button>
																											</a>
																										</c:if> <c:if test="${element.appType eq '19'}">
																											<a
																												href="approvalBuddyPACombo?id=${element.id}&appType=${element.appType}&appstatus=${encryptMsg}">
																												<button class="btn btn-warning btn-sm">
																													View <i class="fa fa-edit"
																														aria-hidden="true"></i>
																												</button>
																											</a>
																										</c:if> <c:if test="${element.appType eq '20'}">
																											<a
																												href="approvalBuddyPABenefit?id=${element.id}&appType=${element.appType}&appstatus=${encryptMsg}">
																												<button class="btn btn-warning btn-sm">
																													View <i class="fa fa-edit"
																														aria-hidden="true"></i>
																												</button>
																											</a>
																										</c:if> <c:if test="${element.appType eq '21'}">
																											<a
																												href="approvalBuddyPABenefitGrp?id=${element.id}&appType=${element.appType}&appstatus=${encryptMsg}">
																												<button class="btn btn-warning btn-sm">
																													View <i class="fa fa-edit"
																														aria-hidden="true"></i>
																												</button>
																											</a>
																										</c:if> <c:if test="${element.appType eq '22'}">
																											<a
																												href="approvalBuddyPAProduct?id=${element.id}&appType=${element.appType}&appstatus=${encryptMsg}">
																												<button class="btn btn-warning btn-sm">
																													View <i class="fa fa-edit"
																														aria-hidden="true"></i>
																												</button>
																											</a>
																										</c:if> <c:if test="${element.appType eq '23'}">
																											<a
																												href="approvalTravelEzyProduct?id=${element.id}&appType=${element.appType}&appstatus=${encryptMsg}">
																												<button class="btn btn-warning btn-sm">
																													View <i class="fa fa-edit"
																														aria-hidden="true"></i>
																												</button>
																											</a>
																										</c:if> <c:if test="${element.appType eq '25'}">
																											<a
																												href="approvalTCProductRate?id=${element.id}&appType=${element.appType}&appstatus=${encryptMsg}">
																												<button class="btn btn-warning btn-sm">
																													View <i class="fa fa-edit"
																														aria-hidden="true"></i>
																												</button>
																											</a>
																										</c:if> <c:if test="${element.appType eq '24'}">
																											<a
																												href="approvalTCProductInfo?id=${element.id}&appType=${element.appType}&appstatus=${encryptMsg}">
																												<button class="btn btn-warning btn-sm">
																													View <i class="fa fa-edit"
																														aria-hidden="true"></i>
																												</button>
																											</a>
																										</c:if></td>



																								</tr>
																							</c:forEach>
																						</tbody>
																					</table>


																				</div>

																			</div>
																		</div>
																	</div>
																</div>
																<!--                                                    
		                                                    -->
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>


											</div>
										</div>
									</div>
									<!--row -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->

	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
		        <div id="back-top">
		            <a href="#top"><i class="fa fa-chevron-up"></i></a>
		        </div>
		-->
	<!-- END BACK TO TOP -->


	<!--
		        ===========================================================
		        END PAGE
		        ===========================================================
		        -->

	<!--
		        ===========================================================
		        Placed at the end of the document so the pages load faster
		        ===========================================================
		        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%--    <jsp:include page="siteFooter.jsp" />   --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->


	<!-- JAVA Script Goes Here -->

	<script
		src="<c:url value="/eosuam/resources/assets/js/jquery-2.2.4.min.js" />"></script>
	<!--  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
	<script src="/eosuam/assets/js/apps.js"></script>
	<script src="/eosuam/assets/plugins/retina/retina.min.js"></script>
	<script src="/eosuam/assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="/eosuam/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="/eosuam/assets/plugins/backstretch/jquery.backstretch.min.js"></script>


	<!-- PLUGINS -->
	<script src="/eosuam/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="/eosuam/assets/plugins/prettify/prettify.js"></script>
	<script
		src="/eosuam/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="/eosuam/assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="/eosuam/assets/plugins/icheck/icheck.min.js"></script>
	<script src="/eosuam/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="/eosuam/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="/eosuam/assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="/eosuam/assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="/eosuam/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="/eosuam/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="/eosuam/assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="/eosuam/assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="/eosuam/assets/plugins/summernote/summernote.min.js"></script>
	<script src="/eosuam/assets/plugins/markdown/markdown.js"></script>
	<script src="/eosuam/assets/plugins/markdown/to-markdown.js"></script>
	<script src="/eosuam/assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="/eosuam/assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="/eosuam/assets/plugins/toastr/toastr.js"></script>
	<script src="/eosuam/assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="/eosuam/assets/plugins/placeholder/jquery.placeholder.js"></script>
	<!-- 	<script
				src="/eosuam/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
			<script src="/eosuam/assets/plugins/jquery-validation/dist/jquery.validate.js"></script> -->
	<script type="text/javascript" src="/eosuam/assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript" src="/eosuam/assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
		       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
		       <![endif]-->
	<script src="/eosuam/assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="/eosuam/assets/plugins/jquery-knob/knob.js"></script>
	<script src="/eosuam/assets/js/jquery.dataTables.min.js"></script>
	<script src="/eosuam/assets/js/dataTables.bootstrap.min.js"></script>
	<script src="/eosuam/assets/js/dataTables.buttons.min.js"></script>
	<script src="/eosuam/assets/js/buttons.bootstrap.min.js"></script>
	<script src="/eosuam/assets/js/jszip.min.js"></script>
	<script src="/eosuam/assets/js/pdfmake.min.js"></script>
	<script src="/eosuam/assets/js/vfs_fonts.js"></script>
	<script src="/eosuam/assets/js/buttons.html5.min.js"></script>
	<script src="/eosuam/assets/js/buttons.print.min.js"></script>
	<script src="/eosuam/assets/js/buttons.colVis.min.js"></script>
	<script src="/eosuam/assets/js/toastr.min.js"></script>
	<script src="/eosuam/assets/jAlert/jAlert.min.js"></script>
	<script src="/eosuam/assets/jAlert/jAlert-functions.min.js"></script>
	<script src="plugins/jquery-knob/knob.js"></script>


	<!--
		        ===========================================================
		        Placed at the end of the document so the pages load faster
		        ===========================================================
		        -->

	<script>
			
			
				$(document).ready(function() {
					
					
					 var rejectmessage="<c:out value="${rejectmessage}"/>";
				       // show when the button is clicked
				       if (rejectmessage.length) {
				    	 // alert(updatemessage);
				    	   successAlert('Success!', rejectmessage);
				       }
				       
				       var approvemessage="<c:out value="${approvemessage}"/>";
				       // show when the button is clicked
				       if (approvemessage.length) {
				    	 // alert(updatemessage);
				    	   successAlert('Success!', approvemessage);
				       }
					
		
					$('#admin-datatable-second_admin').DataTable({
						"order" : [],
						dom : 'Bfrtip',
						title : ' Agent List',
		
						buttons : [ 'copy', 'csv', 'print', {
							extend : 'excelHtml5',
							title : 'Audit Trail - Payments'
						}, {
							extend : 'pdfHtml5',
							title : 'Audit Trail - Payments'
						} ]
					});
		
					// $('#date1').datepicker();
					$("#fromdate").datepicker({
						format : 'dd/mm/yyyy',
						autoclose : true,
					}).on('changeDate', function(ev) {
						$(this).datepicker('hide');
					});
					$("#todate").datepicker({
						format : 'dd/mm/yyyy',
						autoclose : true,
					}).on('changeDate', function(ev) {
						$(this).datepicker('hide');
					});
		
					

				});
			</script>


	<script>
				function validateForm() {
					var x = document.forms["rptForm"]["fromdate"].value;
					var y = document.forms["rptForm"]["todate"].value;
		
					if (x == null || x == "") {
		
						alert("Please choose Date From");
						document.forms["rptForm"]["fromdate"].focus();
		
						return false;
					} else if (y == null || y == "") {
		
						alert("Please choose Date To");
						document.forms["rptForm"]["todate"].focus();
						return false;
					}
		
					return true;
		
				}
			</script>






</body>
</html>