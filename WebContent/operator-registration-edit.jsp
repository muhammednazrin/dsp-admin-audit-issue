<%@ page import="java.io.*,java.util.*"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>

<%@ page import="com.etiqa.model.operator.Operator"%>
<%@ page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<link rel="stylesheet" href="assets/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/menu.css">
<script src="assets/js/jquery.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.js" type="text/javascript"></script>
</head>

<body>
	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->


	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Operator Management</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">

											<% Operator operator =new Operator(); 
                                               SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy");
                                              
                                               %>


											<form action="operator" method="post" id="operatorEd"
												name="operatorEd">
												<input type="hidden" name="action" value="update"> <input
													type="hidden" name="id" value="${dataoperator.id}" />
												<div class="row">
													<div class="col-sm-12">
														<div class="title">
															<div class="sub">
																<h4>Edit Register Operator</h4>
															</div>
														</div>
														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-12">
																	<div class="title">
																		<div class="sub">
																			<h4>Operator Information Details</h4>
																		</div>
																	</div>
																</div>
																<div class="gap gap-mini"></div>
																<div class="col-sm-6">
																	<div class="form-horizontal info-meor">


																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Registration
																					Date</label>
																			</div>
																			<div class="col-sm-9">
																				<input type="text" placeholder="Date"
																					class="form-control" required
																					name="registrationDate" id="datepicker1"
																					value="${dataoperator.registrationDate}"> <span
																					id="msg_date" class="hidden"
																					style="color: red; text-align: left">Please
																					select registration Date</span>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Annual Sales
																					Target (RM)</label>
																			</div>
																			<div class="col-sm-9">
																				<input type="text" name="annualSalesTarget"
																					class="form-control" required
																					id="annualSalesTarget"
																					value="${dataoperator.annualSalesTarget}">
																				<span id="msg_salesTarget" class="hidden"
																					style="color: red; text-align: left">Please
																					enter your Annual Sales Target</span>
																			</div>
																		</div>

																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-horizontal info-meor">


																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Operator
																					Category</label>
																			</div>
																			<div class="col-sm-9">
																				<select class="form-control" name="operatorCategory"
																					required id="operatorCategory"
																					onblur="checkOnBlur(this,'Please Select Your Category');"
																					onchange="checkOnChange(this);">


																					<option value="Individual"
																						<c:if test="${dataoperator.operatorCategory == 'Individual'}"> <c:out value= "selected=selected"/></c:if>>
																						Individual</option>
																					<option value="Company/Organization"
																						<c:if test="${dataoperator.operatorCategory == 'Company/Organization'}"> <c:out value= "selected=selected"/></c:if>>
																						Company/Organization</option>

																				</select>

																			</div>
																		</div>

																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Entity</label>
																			</div>
																			<div class="col-sm-9">
																				<select class="form-control" name="entityType"
																					required id="entityType"
																					onblur="checkOnBlur(this,'Please Select Your Category');"
																					onchange="checkOnChange(this);">


																					<option value="Insurance"
																						<c:if test="${dataoperator.entity == 'Insurance'}"> <c:out value= "selected=selected"/></c:if>>
																						Insurance</option>
																					<option value="Takaful"
																						<c:if test="${dataoperator.entity == 'Takaful'}"> <c:out value= "selected=selected"/></c:if>>
																						Takaful</option>

																				</select>

																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Operator Type</label>
																			</div>
																			<div class="col-sm-9">
																				<select class="form-control" name="operatorType"
																					required id="operatorType"
																					onblur="checkOnBlur(this,'Please Select Your Category');"
																					onchange="checkOnChange(this);">

																					<option value="D"
																						<c:if test="${dataoperator.operatorType == 'D'}"> <c:out value= "selected=selected"/></c:if>>Direct</option>
																					<option value="ND"
																						<c:if test="${dataoperator.operatorType == 'ND'}"> <c:out value= "selected=selected"/></c:if>>Non
																						Direct</option>

																				</select>

																			</div>
																		</div>



																	</div>
																</div>



															</div>





														</div>

														<div class="the-box">
															<div class="col-sm-12">
																<div class="title">
																	<div class="sub">
																		<h4>Personal Details</h4>
																	</div>
																</div>
															</div>
															<div class="gap gap-mini"></div>
															<div class="col-sm-6">
																<div class="form-horizontal info-meor">
																	<div class="form-group">
																		<div class="col-sm-3 column text-right">
																			<label class="control-label">Operator Name</label>
																		</div>
																		<div class="col-sm-9">
																			<input type="text" required class="form-control"
																				name="operatorName" id="operatorName"
																				value="${dataoperator.operatorName}" /> <span
																				id="msg_agentname" class="hidden"
																				style="color: red; text-align: left">Please
																				select to proceed</span>
																		</div>
																	</div>

																	<div class="form-group">
																		<div class="col-sm-3 column text-right">
																			<label class="control-label">ID Type</label>
																		</div>
																		<div class="col-sm-9 column">
																			<div class="row">
																				<div class="col-sm-4 column">
																					<select class="form-control" name="idType" required
																						onblur="checkOnBlur(this,'Please Select Your ID Type');"
																						onchange="checkOnChange(this);">
																						<option value="Non-Civilian"
																							<c:if test="${dataoperator.idType == 'Non-Civilian'}"> <c:out value= "selected=selected"/></c:if>>Non-Civilian</option>
																						<option value="MyKad"
																							<c:if test="${dataoperator.idType == 'MyKad'}"> <c:out value= "selected=selected"/></c:if>>MyKad</option>
																					</select> <span id="msg_idtype" class="hidden"
																						style="color: red; text-align: left">Please
																						select ID Type</span>
																				</div>
																				<div class="col-sm-8 column">
																					<input type="text" required class="form-control"
																						name="idNo" id="idNo" value="${dataoperator.idNo}">
																					<span id="msg_idno" class="hidden"
																						style="color: red; text-align: left">Please
																						key in your ID number</span>
																				</div>
																			</div>
																		</div>
																	</div>


																	<div class="form-group">
																		<div class="col-sm-3 column text-right">
																			<label class="control-label">Phone No</label>
																		</div>
																		<div class="col-sm-9">
																			<div class="row">
																				<div class="col-sm-4">
																					<select class="form-control" name="phoneType"
																						required
																						onblur="checkOnBlur(this,'Please Select Your Phone Type');"
																						onchange="checkOnChange(this);">
																						<option value="Mobile"
																							<c:if test="${dataoperator.phoneType == 'Mobile'}"> <c:out value= "selected=selected"/></c:if>>
																							Mobile</option>
																						<option value="Office"
																							<c:if test="${dataoperator.phoneType == 'Office'}"> <c:out value= "selected=selected"/></c:if>>
																							Office</option>
																					</select> <span id="msg_phonetype" class="hidden"
																						style="color: red; text-align: left">Please
																						select your Phone Type</span>
																				</div>
																				<div class="col-sm-8">
																					<input type="text" required class="form-control"
																						name="phoneNO" id="phoneNo"
																						value="${dataoperator.phoneNO}"> <span
																						id="msg_phoneno" class="hidden"
																						style="color: red; text-align: left">Please
																						key in your phone number</span>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="form-group">
																		<div class="col-sm-3 column text-right">
																			<label class="control-label">Email</label>
																		</div>
																		<div class="col-sm-9">
																			<input type="text" required placeholder="Email"
																				class="form-control" name="email" id="email"
																				value="${dataoperator.email}"> <span
																				id="msg_email" class="hidden"
																				style="color: red; text-align: left">Please
																				key in your Email ID</span> <span id="msg_validmail"
																				class="hidden" style="color: red; font-size: bold">Please
																				Enter valid email ID</span>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-horizontal info-meor">
																	<div class="form-group">
																		<div class="col-sm-3 column text-right">
																			<label class="control-label">Address</label>
																		</div>
																		<div class="col-sm-9">
																			<div class="form-group">
																				<div class="col-sm-12">
																					<input type="text" required class="form-control"
																						name="address1" id="address1"
																						value="${dataoperator.address1}"> <span
																						id="msg_address" class="hidden"
																						style="color: red; text-align: left">Please
																						key in your Address</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<div class="col-sm-12">
																					<input type="text" class="form-control"
																						name="address2" value="">
																				</div>
																			</div>
																			<div class="form-group">
																				<div class="col-sm-12">
																					<input type="text" class="form-control"
																						name="address3" value="">
																				</div>
																			</div>
																			<div class="form-group">
																				<div class="col-sm-5">
																					<input type="text" class="form-control"
																						placeholder="Postcode" required name="postcode"
																						id="postcode" value="${dataoperator.postcode}">
																					<span id="msg_postcode" class="hidden"
																						style="color: red; text-align: left">Please
																						key in your Postcode</span>
																				</div>
																				<div class="col-sm-7">
																					<select class="form-control" name="city" id="city">
																						<option>Select</option>
																						<option>Petaling Jaya</option>
																						<option>Bangsar</option>
																					</select> <span id="msg_city" class="hidden"
																						style="color: red; text-align: left">Please
																						select your city</span>
																				</div>
																			</div>

																			<div class="form-group">
																				<div class="col-sm-12">
																					<select class="form-control" name="state"
																						id="state">
																						<option value="">Select</option>
																						<option value="Selangor">Selangor</option>
																						<option value="WP Labuan">WP Labuan</option>
																						<option value="Negeri Sembilan">Negeri
																							Sembilan</option>
																						<option value="Perlis">Perlis</option>
																						<option value="Kelantan">Kelantan</option>
																						<option value="Terengganu">Terengganu</option>
																						<option value="Pahang">Pahang</option>
																						<option value="Kedah">Kedah</option>
																						<option value="WP Kuala Lumpur">WP Kuala
																							Lumpur</option>
																						<option value="WP Putrajaya">WP Putrajaya</option>
																						<option value="Sarawak">Sarawak</option>
																						<option value="Johor">Johor</option>
																						<option value="Perak">Perak</option>
																						<option value="Pulau Pinang">Pulau Pinang</option>
																					</select> <span id="msg_state" class="hidden"
																						style="color: red; text-align: left">Please
																						select your state</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<div class="col-sm-12">
																					<select class="form-control" name="country">
																						<option value="Malaysia" selected="">Malaysia</option>
																					</select>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>

													</div>
													<!--content inner -->
												</div>
												<!--col-sm-12 -->

												<div class="col-sm-12">
													<div class="text-right">
														<input class="btn btn-warning btn-sm"
															onClick="submitRegistration();" type="submit"
															value="Submit" />
													</div>
												</div>
												<!--col-sm-12 -->
										</div>
										<!--row -->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->

	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
            <div id="back-top">
                <a href="#top"><i class="fa fa-chevron-up"></i></a>
            </div>
            -->
	<!-- END BACK TO TOP -->


	<!--
            ===========================================================
            END PAGE
            ===========================================================
            -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->

	<script type="text/javascript">
            function submitRegistration(){
                result=true;
                if(!$('#operatorCode').val().length) {
                    $('#msg_opcode').removeClass('hidden');
                    $('#msg_opcode').addClass('visible');
                    $('#operatorCode').focus();
                    result=false;
                }
                if(!$('#agentCode').val().length) {
                    $('#msg_agentcode').removeClass('hidden');
                    $('#msg_agentcode').addClass('visible');
                    $('#agentCode').focus();
                    result=false;
                }
                if(!$('#datepicker1').val().length) {
                    $('#msg_date').removeClass('hidden');
                    $('#msg_date').addClass('visible');
                    $('#datepicker1').focus();
                    result=false;
                }
                if(!$('#annualSalesTarget').val().length) {
                    $('#msg_salesTarget').removeClass('hidden');
                    $('#msg_salesTarget').addClass('visible');
                    $('#annualSalesTarget').focus();
                    result=false;
                }
                if($('#agentCategory').index()== 0) {
                    $('#msg_operatorcat').removeClass('hidden');
                    $('#msg_operatorcat').addClass('visible');
                    $('#operatorCategory').focus();
                    result=false;
                }
                if($('#entitytype').index()== 0) {
                    $('#msg_entype').removeClass('hidden');
                    $('#msg_entype').addClass('visible');
                    $('#entitytype').focus();
                    result=false;
                }
               
                if(!$('input[name=products]:checked').val()) {                     
                    $('#msg_productselect').removeClass('hidden');
                    $('#msg_productselect').addClass('visible');
                    $('#products').focus();
                    result=false;
                }
                if(!$('input[name=registrationDocument]:checked').val()) {                     
                    $('#msg_agentregdoc').removeClass('hidden');
                    $('#msg_agentregdoc').addClass('visible');
                    $('#registrationDocument').focus();
                    result=false;
                }
                if(!$('#operatorName').val().length) {
                    $('#msg_agentname').removeClass('hidden');
                    $('#msg_agentname').addClass('visible');
                    $('#agentName').focus();
                    result=false;
                }
                if($('#idType').index()== 0) {
                    $('#msg_idtype').removeClass('hidden');
                    $('#msg_idtype').addClass('visible');
                    $('#idType').focus();
                    result=false;
                }
                if(!$('#idNo').val().length) {
                    $('#msg_idno').removeClass('hidden');
                    $('#msg_idno').addClass('visible');
                    $('#idNo').focus();
                    result=false;
                }
                if($('#phoneType').index()== 0) {
                    $('#msg_phonetype').removeClass('hidden');
                    $('#msg_phonetype').addClass('visible');
                    $('#phoneType').focus();
                    result=false;
                }
                if(!$('#phoneNo').val().length) {
                    $('#msg_phoneno').removeClass('hidden');
                    $('#msg_phoneno').addClass('visible');
                    $('#phoneNo').focus();
                    result=false;
                }
                if(!$('#email').val().length) {
                    $('#msg_email').removeClass('hidden');
                    $('#msg_email').addClass('visible');
                    $('#email').focus();
                    result=false;
                }
                if(!$('#address1').val().length) {
                    $('#msg_address').removeClass('hidden');
                    $('#msg_address').addClass('visible');
                    $('#address1').focus();
                    result=false;
                }
                if(!$('#postcode').val().length) {
                    $('#msg_postcode').removeClass('hidden');
                    $('#msg_postcode').addClass('visible');
                    $('#postcode').focus();
                    result=false;
                }
             //   if($('#city').index()== 0) {
              //      $('#msg_city').removeClass('hidden');
              ///      $('#msg_city').addClass('visible');
               //     $('#city').focus();
               //     result=false;
              //  }
             //   if($('#state').index()== 0) {
                //    $('#msg_state').removeClass('hidden');
                //    $('#msg_state').addClass('visible');
                 //   $('#state').focus();
                 //   result=false;
               // }
            }

            $('#operatorCode').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_opcode').removeClass('visible');
                    $('#msg_opcode').addClass('hidden');
                }
            });
            $('#agentCode').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_agentcode').removeClass('visible');
                    $('#msg_agentcode').addClass('hidden');
                }
            });
            $('#datepicker1').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_date').removeClass('visible');
                    $('#msg_date').addClass('hidden');
                }
            });
            $('#annualSalesTarget').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_salesTarget').removeClass('visible');
                    $('#msg_salesTarget').addClass('hidden');
                }
            });
            $('#operatorCategory').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_operatorcat').removeClass('visible');
                    $('#msg_operatorcat').addClass('hidden');
                }
            });
            $('#entitytype').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_entype').removeClass('visible');
                    $('#msg_entype').addClass('hidden');
                }
            });
           
            $('#products').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_productselect').removeClass('visible');
                    $('#msg_productselect').addClass('hidden');
                }
            });
           
            $('#agentName').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_agentname').removeClass('visible');
                    $('#msg_agentname').addClass('hidden');
                }
            });
            $('#idType').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_idtype').removeClass('visible');
                    $('#msg_idtype').addClass('hidden');
                }
            });
            $('#idNo').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_idno').removeClass('visible');
                    $('#msg_idno').addClass('hidden');
                }
            });
            $('#phoneType').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_phonetype').removeClass('visible');
                    $('#msg_phonetype').addClass('hidden');
                }
            });
            $('#phoneNo').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_phoneno').removeClass('visible');
                    $('#msg_phoneno').addClass('hidden');
                }
            });
            $('#email').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_email').removeClass('visible');
                    $('#msg_email').addClass('hidden');
                }
                if (validateEmail(this.value)) {
                    $('#msg_validmail').removeClass('visible');
                    $('#msg_validmail').addClass('hidden');
                } 
            });
            $('#address1').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_address').removeClass('visible');
                    $('#msg_address').addClass('hidden');
                }
            });
            $('#postcode').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_postcode').removeClass('visible');
                    $('#msg_postcode').addClass('hidden');
                }
            });
            $('#city').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_city').removeClass('visible');
                    $('#msg_city').addClass('hidden');
                }
            });
            $('#state').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_state').removeClass('visible');
                    $('#msg_state').addClass('hidden');
                }
            });

            

            $(document).ready(function() {
                $('#postcode').blur(function(e) {
                    if (validatePhone('postcode')) {
                       //alert('valid');
                        $('#msg_postcode').removeClass('visible');
                        $('#msg_postcode').addClass('hidden');
                    }
                    else {
                    //  alert('invalid');
                        $('#msg_postcode').removeClass('hidden');
                        $('#msg_postcode').addClass('visible');
                    }
                });
            });
            // Force Numeric Only 
            jQuery.fn.ForceNumericOnly = function() {
                return this.each(function() {
                    $(this).keydown(function(e) {
                        var key = e.charCode || e.keyCode || 0;
                        // allow backspace, tab, delete, arrows, letters, numbers and keypad numbers ONLY
                        return (
                            key == 8 || 
                            key == 9 ||
                            key == 46 ||
                            key == 32 ||
                            (key >= 48 && key <= 57));                
                    })
                })
            };
            // Function Numbers
            //for (var i=1;i<=counter;i++){
            /*$("#email").validateEmail();*/
            $("#referenceNo").ForceNumericOnly();
           // $("#agentCode").ForceNumericOnly();
            $("#annualSalesTarget").ForceNumericOnly();
            $("#idNo").ForceNumericOnly();
            $("#phoneNo").ForceNumericOnly();
            $("#postcode").ForceNumericOnly();
            //}

            /////////////// Fetching State from Postcode ////////////////  
            /*function fetch_state() { 
                //alert(document.getElementById("postcode").value);
                var obj;
                var arr = [];
                var formData = {postcode:document.getElementById("postcode").value}; //Array 
                if (document.getElementById("postcode").value.length != 0 && document.getElementById("postcode").value.length >= 4) {
                    $.ajax({
                        type: "post", 
                        url: "/getonline/sites/REST/controller/TravellerController/fetch_State",
                        data: formData,
                        success: function (data) {
                            if(data) {
                                document.getElementById("state").value=data;
                            }

                        },
                        error: function (request, status, error) {
                            alert(request.responseText);
                        }
                    }); 
                } 
            }*/
            function validateEmail(sEmail) {
                var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if (filter.test(sEmail)) {
                    return true;
                }
                else {
                    return false;
                }
            }

            function validatePhone(postcode) {
                var a = document.getElementById(postcode).value;
                var filter = /^[0-9-+]+$/;
                if (filter.test(a)) {
                    return true;
                }
                else {
                    return false;
                }
            }

            function checkOnBlur(me, message) {
                var  e_id = $(me).attr('id');
                if (!(me.value).length) {
                    var exist= document.getElementById(e_id+ '_e');
                    if (exist == null) {
                        var htmlString="";
                        htmlString+='<label id='+ e_id+ '_e' + '  ' + 'class="error">'+ message + '</label>';
                        $(htmlString).insertAfter("#"+ e_id);
                        $('#'+ e_id).focus();
                    }
                }
            }
                //////////////////////////////////////////////////////////////////////////////////////////////  
            /*function checkKeyUp(me) {
                var  e_id = $(me).attr('id');
                if($.trim(me.value).length) {
                    var exist= document.getElementById(e_id+ '_e');
                    if (exist != null) {
                        $('#'+ e_id + '_e').remove();
                    }
                }
            }*/
            //////////////////////////////////////////////////////////////////////////////////////////////
            function checkOnChange(me) {
                var  e_id = $(me).attr('id');
                if($.trim(me.value).length) {
                    var exist= document.getElementById(e_id+ '_e');
                    if (exist != null) {
                        $('#'+ e_id + '_e').remove();
                    }
                }
            }

            /*var js = jQuery.noConflict();
            js(document).ready(function() {
                js("#registration").validate({
                    rules: {
                        phoneType: {
                            required: true
                        }
                    },
                    messages: {
                        phoneType: {
                            required: "You must select a timezone"
                        }
                    }

                });
            });*/

        </script>
</body>
</html>