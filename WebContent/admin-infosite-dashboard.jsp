<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.math.BigDecimal"%>
<%@ page import="java.math.RoundingMode"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="com.etiqa.model.report.TransactionalReport"%>
<% java.text.DateFormat df=new java.text.SimpleDateFormat("dd/MM/yyyy"); %>


<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" /> 
  <link rel="stylesheet" type="text/css" href="assets/daterangepicker/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="assets/daterangepicker/main.css">
  <link rel="stylesheet" type="text/css" href="assets/daterangepicker/daterangepicker.css" />
  <style type="text/css">
  	.btn-black{padding: .8rem 3.5rem}
  </style>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}
.nav-tabs>li {
    width: 100%;
    padding: 1rem;
    font-size: 16px;
}
.nav-tabs>li.active{font-weight:bold;}
.nav-tabs>li>div{cursor:pointer;}
select.form-control{height:34px !important;}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>

</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Report &amp; Analytics</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
							
								<section>
		<div class="container mb-4">
			<div class="row">
				<div class="col-md-12 mt-4 text-center">
					<h3 class="section-title mb-3">Infosite Date Download</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
				
			
			<div id="MyAccountsTab" class="tabbable tabs-left">
                
                
			  <div class="col-sm-3 border-right">
			  	<ul  class="nav nav-tabs">
                     <li class="active">
                         <div data-target="#v-enquiry" data-toggle="tab"><i class="fas fa-envelope pr-2"></i> Enquiry</div>
                     </li>
                     <li>
                         <div data-target="#v-contact" data-toggle="tab"><i class="fas fa-phone pr-2"></i> Contact Me</div>
                     </li>
                     <li>
                         <div data-target="#v-newsletter" data-toggle="tab"><i class="fas fa-paper-plane pr-2"></i> Newsletter</div>
                     </li>
                      
                </ul>
                </div>
			  <div class="col-sm-9">
			  	<div class="tab-content">
                    <div class="tab-pane active" id="v-enquiry">
                        <form:form name="txnreport" id = "frm" action="generateInfositeEnquiryExcel" method="post">
					      	<div class="container-fluid mt-5">
					      		<div class="row">
					      			<div class="col-sm-8">
					      				<div class="row">
					      					<label class="col-sm-4 mb-4 pt-2">Select Date Range1: </label>
													<div class="col-sm-8 mb-4">
													<input type="text" name="enquirydate" value="" class="selectbox pull-right form-control"/>
														<!--<div id="daterange" class="selectbox pull-right form-control">
															<i class="fa fa-calendar"></i>
															<span >September 11, 2018 - October 10, 2018</span> <b class="caret"></b>
														</div>-->
													</div>
												</div>
												<div class="row">
													<label class="col-sm-4 mb-5 pt-2">Select File Format: </label>
													<div class="col-sm-8 mb-5">
														<select class="form-control">
															<option>Excel (.xls)</option>
															<option>Comma seperated(.csv)</option>
														</select>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-6 text-center">
														<a href="#" class="btn-black btn text-color bold animate fadeInUp">Cancel</a>
													</div>
													<div class="col-sm-6 text-center">
													<button type="submit" class="btn-primary btn text-color bold animate fadeInUp"
																				name="Download">Download</button>
														<!-- <a href="" class="btn-primary btn text-color bold animate fadeInUp">Download</a> -->
													</div>
												</div>
					      					</div>
					      				</div>
									</div>
								</form:form>
                    </div>
                    <div class="tab-pane" id="v-contact">
                        <form:form name="txnreport" id="frm1" action="generateInfositecontactExcel" method="post">
					      	<div class="container-fluid mt-5">
					      		<div class="row">
					      			<div class="col-sm-8">
					      				<div class="row">
													<label class="col-sm-4 mb-4 pt-2">Select Date Range2: </label>
													<div class="col-sm-8 mb-4">
													<input type="text" name="contactdate" value="" class="selectbox pull-right form-control"/>
														<!--<div id="daterange" class="selectbox pull-right form-control">
															<i class="fa fa-calendar"></i>
															<span>September 11, 2018 - October 10, 2018</span> <b class="caret"></b>
														</div>-->
													</div>
												</div>
												<div class="row">
													<label class="col-sm-4 mb-5 pt-2">Select File Format: </label>
													<div class="col-sm-8 mb-5">
														<select class="form-control">
															<option>Excel (.xls)</option>
															<option>Comma seperated(.csv)</option>
														</select>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-6 text-center">
														<a href="#" class="btn-black btn text-color bold animate fadeInUp">Cancel</a>
													</div>
													<div class="col-sm-6 text-center">
														<button type="submit" class="btn-primary btn text-color bold animate fadeInUp"name="Download">Download</button>
														
													</div>
												</div>
					      					</div>
					      				</div>
									</div>
								</form:form>
                    </div>
                    <div class="tab-pane" id="v-newsletter">
                        <form:form name="txnreport" id ="frm2" action="generateInfositeNewsletterExcel" method="post">
					      	<div class="container-fluid mt-5">
					      		<div class="row">
					      			<div class="col-sm-8">
					      				<div class="row">
													<label class="col-sm-4 mb-4 pt-2">Select Date Range3: </label>
													<div class="col-sm-8 mb-4">
													<input type="text" name="newsletterdate" value="" class="selectbox pull-right form-control"/>
														<!--<div id="daterange" class="selectbox pull-right form-control">
															<i class="fa fa-calendar"></i>
															<span>September 11, 2018 - October 10, 2018</span> <b class="caret"></b>
														</div>-->
													</div>
												</div>
												<div class="row">
													<label class="col-sm-4 mb-5 pt-2">Select File Format: </label>
													<div class="col-sm-8 mb-5">
														<select class="form-control">
															<option>Excel (.xls)</option>
															<option>Comma seperated(.csv)</option>
														</select>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-6 text-center">
														<a href="#" class="btn-black btn text-color bold animate fadeInUp">Cancel</a>
													</div>
													<div class="col-sm-6 text-center">
														<button type="submit" class="btn-primary btn text-color bold animate fadeInUp" name="Download">Download</button>
												
													</div>
												</div>
					      					</div>
					      				</div>
									</div>
								</form:form>
                   </div>
                </div>
			    </div>
			</div>
		</div>
	</div>
		</section>
							
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%-- <jsp:include page="siteFooter.jsp" /> --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->
	<script
		src="${pageContext.request.contextPath}/assets/js/jquery.min.bk.js"
		type="text/javascript"></script>
	<!-- <script src="${pageContext.request.contextPath}/assets/js/jquery.min.js" type="text/javascript"></script>  -->
	<!--  <script src="assets/jAlert/jquery-1.11.3.min.js"></script> -->
	<script
		src="${pageContext.request.contextPath}/assets/jAlert/jAlert.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/jAlert/jAlert-functions.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/bootstrap.js"
		type="text/javascript"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/retina/retina.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/backstretch/jquery.backstretch.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/apps.js"></script>

	<!-- PLUGINS -->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/prettify/prettify.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<!-- <script src="assets/plugins/chosen/chosen.jquery.min.js"></script> -->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/icheck/icheck.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/mask/jquery.mask.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/summernote/summernote.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/to-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/slider/bootstrap-slider.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/toastr/toastr.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/knob.js"></script>

	<script
		src="${pageContext.request.contextPath}/assets/js/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/dataTables.bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/dataTables.buttons.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/jszip.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/pdfmake.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/vfs_fonts.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.html5.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.print.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.colVis.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/toastr.min.js"></script>

	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/daterangepicker/fontawesome-all.min.css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/momentjs/moment.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/daterangepicker/daterangepicker.min.js"></script>

	<script type="text/javascript">
         
        $(function() {
		 
		   $('input[name="enquirydate"]').daterangepicker({
		    opens: 'left',
		     ranges: {
	        'Today': [moment(), moment()],
	        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
	        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	        'This Month': [moment().startOf('month'), moment().endOf('month')],
	        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    		},
    		"alwaysShowCalendars": true
		  }, function(start, end, label) {
		    console.log("A new date selection was made: " + start.format('MMMM D, YYYY') + ' to ' + end.format('MMMM D, YYYY'));
		  });
		  
		  $('input[name="contactdate"]').daterangepicker({
		    opens: 'left',
		     ranges: {
	        'Today': [moment(), moment()],
	        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
	        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	        'This Month': [moment().startOf('month'), moment().endOf('month')],
	        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    		},
    		"alwaysShowCalendars": true
		  }, function(start, end, label) {
		    console.log("A new date selection was made: " + start.format('MMMM D, YYYY') + ' to ' + end.format('MMMM D, YYYY'));
		  });
		  
		  $('input[name="newsletterdate"]').daterangepicker({
		    opens: 'left',
		     ranges: {
	        'Today': [moment(), moment()],
	        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
	        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	        'This Month': [moment().startOf('month'), moment().endOf('month')],
	        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    		},
    		"alwaysShowCalendars": true
		  }, function(start, end, label) {
		    console.log("A new date selection was made: " + start.format('MMMM D, YYYY') + ' to ' + end.format('MMMM D, YYYY'));
		  });
		});
        </script>




</body>
</html>