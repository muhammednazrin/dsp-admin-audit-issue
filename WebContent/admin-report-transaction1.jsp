<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.math.BigDecimal"%>
<%@ page import="java.math.RoundingMode"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="com.etiqa.model.report.TransactionalReport"%>
<% java.text.DateFormat df=new java.text.SimpleDateFormat("dd/MM/yyyy"); %>


<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="https://protect2.fireeye.com/url?k=fb6529fa17c4dc3d.fb65f363-4e51064eee243e8f&u=assets/plugins/all.min.css" />
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

.errorText {
	color: red
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>

</head>

<body>
	<%
        
       
   	 DecimalFormat formatter = new DecimalFormat("#,###.00");
        
 if (request.getMethod().equals("GET")){
			
			session.setAttribute("ProductType", "");		
			session.setAttribute("Status", "");
			session.setAttribute("PolicyCertificateNo", "");
			session.setAttribute("NRIC","");
			session.setAttribute("ReceiptNo", "");
			session.setAttribute("dateFrom","");
			session.setAttribute("dateTo","");
			session.setAttribute("DateFrom", "");
			session.setAttribute("DateTo","");
			session.setAttribute("PlateNo","");
			session.setAttribute("ProductEntity","");
			
 }
			
 
    
       String textColor="";
       String status="";
       String transactionStatus="";
       String lastStep="";
       String nextStep="";
       String reason="";
       int i=0;
       int rowLine=0;
       String txtColor="";


        %>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Report &amp; Analytics</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Transactional Report</h4>
														</div>
													</div>
													<c:if test="${not empty errorMessages}">
														<div class="row">
															<div class="col-sm-12">
																<c:forEach items="${errorMessages}" var="element"
																	varStatus="theCount">
																	<ul>
																		<li class="errorText"><c:out value="${element}" /></li>
																	</ul>
																</c:forEach>
															</div>
														</div>
													</c:if>

													<!-- Start Form -->
													<form:form name="txnreport" action="searchReportDone"
														method="post">
														<input type="hidden" name="action" value="list" />
														<input type="hidden" name="startRow" value="0" />
														<input type="hidden" name="endRow" value="10" />
														<input type="hidden" id="productType" name="productType"
															value="<c:out value="${sessionScope.ProductType}" />" />
														<input type="hidden" id="productTypeValSession"
															name="productTypeValSession"
															value="<c:out value="${sessionScope.ProductType}" />" />
														<input type="hidden" id="productEntityValSession"
															name="productEntityValSession"
															value="<c:out value="${sessionScope.ProductEntity}" />" />

														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-6">
																	<div class="form-horizontal">

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Search
																				by Date</label>
																			<div class="col-sm-4">

																				<fmt:parseDate value="${now}" var="parsedEmpDate"
																					pattern="dd/MM/yyyy" />

																				<input type="text" placeholder="Date From"
																					class="form-control" id="datepicker1"
																					name="dateFrom" autocomplete="off"
																					<c:choose>
																					    <c:when test="${not empty sessionScope.dateFrom}">
																					         value="<c:out value="${sessionScope.dateFrom}" />"																					     
																					    </c:when>
																					    <c:otherwise>
																					          value="<%= df.format(new java.util.Date())%>"
																					    </c:otherwise>
																					</c:choose> />
																			</div>
																			<div class="col-sm-5">
																				<input type="text" placeholder="Date To"
																					autocomplete="off" class="form-control"
																					id="datepicker2" name="dateTo"
																					<c:choose>
																					    <c:when test="${not empty sessionScope.dateTo}">
																					         value="<c:out value="${sessionScope.dateTo}" />"																					     
																					    </c:when>
																					    <c:otherwise>
																					          value="<%= df.format(new java.util.Date())%>"
																					    </c:otherwise>
																					</c:choose> />

																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Search
																				by Product Entity</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="productEntity"
																					id="productEntity">
																					<option value="" selected>-View All-</option>
																					<option
																						<c:if test="${sessionScope.ProductEntity == 'EIB'}" >selected</c:if>
																						value="EIB">EGIB</option>
																					<option
																						<c:if test="${sessionScope.ProductEntity == 'ETB'}" >selected</c:if>
																						value="ETB">EGTB</option>
																				</select>
																			</div>
																		</div>

																		<div class="form-group" id="eib-pr" name="eib-pr">
																			<label class="col-sm-3 control-label">Product
																				Type</label>

																			<div class="col-sm-9" id="eib-list">
																				<select class="form-control" name="productTypeEIB"
																					id="productTypeEIB">
																					<option value=""
																						<c:if test="${sessionScope.ProductType == ''}" >selected</c:if>>-View
																						All-</option>
																					<option value="WTC"
																						<c:if test="${sessionScope.ProductType == 'WTC'}" >selected</c:if>>
																						World Traveller Care</option>
																					<option value="MI"
																						<c:if test="${sessionScope.ProductType == 'MI'}" >selected</c:if>>
																						Motor Insurance</option>
																					<option value="TL"
																						<c:if test="${sessionScope.ProductType == 'TL'}" >selected</c:if>>
																						Ezy-Life Secure</option>
																					<option value="PCCA01"
																						<c:if test="${sessionScope.ProductType == 'PCCA01'}" >selected</c:if>>
																						 e-CancerCare</option>
																					<option value="MP"
																						<c:if test="${sessionScope.ProductType == 'MP'}" >selected</c:if>>
																						 e-Medical Pass </option>
																					<option value="EZYTL"
																						<c:if test="${sessionScope.ProductType == 'EZYTL'}" >selected</c:if>>
																						Ezy-Secure</option>
																					<option value="HOHH"
																						<c:if test="${sessionScope.ProductType == 'HOHH'}" >selected</c:if>>
																						Houseowner Householder</option>
																					<option value="CPP"
																						<c:if test="${sessionScope.ProductType == 'CPP'}" >selected</c:if>>
																						Buddy PA</option>
																					<option value="BPT"
																						<c:if test="${sessionScope.ProductType == 'BPT'}" >selected</c:if>>
																						Travel Ezy</option>
																					<option value="TCI"
																						<c:if test="${sessionScope.ProductType == 'TCI'}" >selected</c:if>>
																						TripCare360</option>
																				</select>


																			</div>

																			<div class="col-sm-9" id="etb-list">

																				<select class="form-control" name="productTypeETB"
																					id="productTypeETB">
																					<option value=""
																						<c:if test="${sessionScope.ProductType == ''}" >selected</c:if>>-View
																						All-</option>
																					<option value="TPT"
																						<c:if test="${sessionScope.ProductType == 'TPT'}" >selected</c:if>>
																						World Traveller Care Takaful</option>
																					<option value="MT"
																						<c:if test="${sessionScope.ProductType == 'MT'}" >selected</c:if>>
																						Motor Takaful</option>
																					<option value="HOHH-ETB"
																						<c:if test="${sessionScope.ProductType == 'HOHH-ETB'}" >selected</c:if>>
																						Houseowner Householder Takaful</option>

																					<option value="IDS"
																						<c:if test="${sessionScope.ProductType == 'IDS'}" >selected</c:if>>
																						I Double Secure</option>
																					<option value="ISCTL"
																						<c:if test="${sessionScope.ProductType == 'ISCTL'}" >selected</c:if>>
																						I-Secure</option>
																					<option value="PTCA01"
																						<c:if test="${sessionScope.ProductType == 'PTCA01'}" >selected</c:if>>
																						 e-CancerCare Takaful</option>
																					<option value="MPT"
																						<c:if test="${sessionScope.ProductType == 'MPT'}" >selected</c:if>>
																						 e-Medical Pass Takaful</option>
																					<option value="TPP"
																						<c:if test="${sessionScope.ProductType == 'TPP'}" >selected</c:if>>
																						Buddy PA</option>
																					<option value="TZT"
																						<c:if test="${sessionScope.ProductType == 'TZT'}" >selected</c:if>>
																						i-Travel Ezy</option>
																					<option value="TCT"
																						<c:if test="${sessionScope.ProductType == 'TCT'}" >selected</c:if>>
																						TripCare360</option>
																				</select>
																			</div>

																		</div>


																		<div id="mi-option">
																			<div class="form-horizontal">
																				<div class="form-group">
																					<label class="col-sm-3 control-label">Vehicle
																						Registration No.</label>
																					<div class="col-sm-9">
																						<input type="text" id="plateNo" name="plateNo"
																							placeholder="" class="form-control"
																							value="<c:out value="${sessionScope.PlateNo}" />" />
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>

																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">NRIC/ID
																				No.</label>
																			<div class="col-sm-9">
																				<input type="text" placeholder=""
																					class="form-control" name="NRIC"
																					<%-- value="${sessionScope.NRIC}" --%>
																					 value="<c:out value="${sessionScope.NRIC}" />" />
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Policy/Certificate
																				No</label>

																			<div class="col-sm-9">
																				<input type="text" placeholder=""
																					class="form-control" name="policyNo"
																					value="<c:out value="${sessionScope.PolicyCertificateNo}" />" />
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Transaction
																				Status</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="status">
																					<option value=""
																						<c:if test="${sessionScope.Status == ''}" >selected</c:if>>-View
																						All-</option>
																					<option value="S"
																						<c:if test="${sessionScope.Status == 'S'}" >selected</c:if>>Successful</option>
																					<option value="F"
																						<c:if test="${sessionScope.Status == 'F'}">selected</c:if>>Fail
																						Payment</option>
																					<option value="AP"
																						<c:if test="${sessionScope.Status == 'AP'}" >selected</c:if>>Attempt
																						Payment</option>
																					<option value="O"
																						<c:if test="${sessionScope.Status == 'O'}" >selected</c:if>>Incomplete</option>
																					<option value="C"
																						<c:if test="${sessionScope.Status == 'C'}" >selected</c:if>>Cancelled</option>
																					<option value="R"
																						<c:if test="${sessionScope.Status == 'R'}"  >selected</c:if>>Rejection</option>
																				</select>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Tax
																				Invoice/Receipt No.</label>
																			<div class="col-sm-9">
																				<input type="text" placeholder=""
																					class="form-control" name="receiptNo"
																					value="<c:out value="${sessionScope.ReceiptNo}" />" />
																			</div>
																		</div>
																	</div>
																</div>


																<div class="col-sm-12 text-right">
																	<button type="submit" class="btn btn-default"
																		name="SEARCH">Search</button>
																</div>
															</div>
														</div>
													</form:form>
													<!-- END FORM -->
												</div>
												<%-- <c:if test="${empty motorReportList}">

			<div class="row">
			<c:out value="${emptySearch}" />
			</div>
		
			
</c:if>												
<c:if test="${not empty motorReportList}"> --%>
												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="row">
																<div class="col-xs-12 form-inline">
																	<div class="col-xs-6"></div>
																	<div class="col-xs-6">
																		<div class="row">



																			<c:if test="${not empty reportResult}">
																				<div class="pull-right" id='btnhere'>
																					<!--  <button class="btn btn-warning btn-sm">
																					Print <i class="fa fa-print" aria-hidden="true"></i>
																				</button>-->
																					<a href="generateExcel"><button
																							class="btn btn-warning btn-sm">
																							Export to XLS <i class="fa fa-download"
																								aria-hidden="true"></i>
																						</button></a>

																				</div>
																			</c:if>
																		</div>
																	</div>
																</div>
															</div>
															<div class="table-responsive" id="MyTable">
																<table
																	class="table table-striped table-warning table-hover"
																	id="admin-datatable-second_admin">
																	<thead>
																		<tr>
																			<th colspan="2">Gross Amount</th>
																			<th colspan="2">Nett Amount</th>
																			<th colspan="4">Total Transaction Amount</th>
																		</tr>


																		<c:if test="${not empty totalGross}">
																			<fmt:formatNumber var="totalGross" type="number"
																				groupingUsed="false" minFractionDigits="2"
																				maxFractionDigits="2" value="${totalGross}" />

																		</c:if>

																		<c:if test="${not empty totalNet}">
																			<fmt:formatNumber var="totalNet" type="number"
																				groupingUsed="false" minFractionDigits="2"
																				maxFractionDigits="2" value="${totalNet}" />

																		</c:if>

																		<c:if test="${empty totalNet}">
																			<c:set var="totalNet" value="-" />
																		</c:if>


																		<c:if test="${not empty totalAmount}">

																			<fmt:formatNumber var="totalAmount" type="number"
																				groupingUsed="false" minFractionDigits="2"
																				maxFractionDigits="2" value="${totalAmount}" />

																		</c:if>
																		<td colspan="2"><b>RM</b> <c:out
																				value="${totalNet}" /></td>
																		<td colspan="2"><b>RM</b> <c:out
																				value="${totalGross}" /></td>
																		<td colspan="4"><b>RM</b> <c:out
																				value="${totalAmount}" /></td>

																		<tr>
																			<th>No</th>
																			<th style="width: 40px;">Txn Date</th>
																			<th>Pol/Cert No</th>
																			<th>Customer</th>
																			<th>Product</th>
																			<th>Payment</th>
																			<th>Status</th>
																			<th>Amount(RM)</th>
																		</tr>
																	</thead>
																	<tbody>
																		<c:if test="${not empty reportResult}">
																			<c:forEach items="${reportResult}" var="element"
																				varStatus="theCount">

																				<c:if test="${not empty element.amount}">
																					<fmt:formatNumber var="newAmount" type="number"
																						groupingUsed="false" minFractionDigits="2"
																						maxFractionDigits="2" value="${element.amount}" />
																				</c:if>

																				<c:if test="${empty element.amount}">
																					<c:set var="newAmount" value="-" />
																				</c:if>




																				<c:if
																					test="${element.product_code eq 'MI' || element.product_code eq 'MT'}">
																					<tr>
																						<td align="center"><c:out
																								value="${theCount.count}" /></td>
																						<td><c:out
																								value="${element.transaction_datetime}" /></td>
																						<td><c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.policy_number}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.policy_number}" />
																								</c:otherwise>
																							</c:choose> <br /> <c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.caps_dppa}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.caps_dppa}" />
																								</c:otherwise>
																							</c:choose> <br /> <c:out
																								value="${element.registration_number}" /></td>
																						<td><span style="font-weight: bold">Name:</span>
																							<c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.customer_name}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.customer_name}" />
																								</c:otherwise>
																							</c:choose> <br /> <span style="font-weight: bold">ID
																								No:</span> <c:out value="${element.customer_nric_id}" />
																							<br /> <c:out value="${element.agent_name}" /> (<c:out
																								value="${element.agent_code}" />)</td>
																						<td><c:out value="${element.product_code}" />
																							<br /> <c:choose>
																								<c:when test="${element.product_code eq 'MI'}">																		   
																			           (<c:out value="EGIB" />)
																			          </c:when>
																								<c:otherwise>
																			          (<c:out value="EGTB" />)
																			          </c:otherwise>
																							</c:choose></td>
																						<td><c:choose>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'Maybank2U MOTOR' || fn:trim(element.pmnt_gateway_code) eq 'Maybank2U'}">
																									<span style="font-weight: bold"> <c:out
																											value="M2U" />
																									</span>
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Ref No:" />
																									</span>
																									<c:out value="${element.bankrefno}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="App Code:" />
																									</span>
																									<c:out value="${element.approvalcode}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />

																								</c:when>


																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'EBPG' || fn:trim(element.pmnt_gateway_code) eq 'EBPG 2D'}">
																									<span style="font-weight: bold"> <c:out
																											value="EBPG" />
																									</span>
																									<br />
																									<span style="font-weight: bold"><c:out
																											value="Ref No:" /> </span>
																									<c:out value="${element.transaction_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Auth Code:" />
																									</span>
																									<c:out value="${element.auth_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />
																								</c:when>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'FPXMOTOR' || fn:trim(element.pmnt_gateway_code) eq 'FPX' || fn:trim(element.pmnt_gateway_code) eq 'ETB FPX' }">
																									<span style="font-weight: bold"> <c:out
																											value="FPX" />
																									</span>
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Ref No:" />
																									</span>
																									<c:out value="${element.fpx_fpxtxnid}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="FPX Status:" />
																									</span>
																									<c:out value="${element.fpx_debitauthcode}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />
																								</c:when>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'AMEX' || fn:trim(element.pmnt_gateway_code) eq 'AMEX 2D' }">
																									<span style="font-weight: bold"> <c:out
																											value="AMEX" />
																									</span>
																									<br />
																									<span style="font-weight: bold"><c:out
																											value="Ref No:" /> </span>
																									<c:out value="${element.transaction_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Auth Code:" />
																									</span>
																									<c:out value="${element.auth_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />
																								</c:when>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'API'}">
																									<span style="font-weight: bold"> <c:out
																											value="MOTORAPI" />
																									</span>
																									<br />
																									<span style="font-weight: bold"><c:out
																											value="Ref No:" /> </span>
																									<c:out value="${element.REFNUM}" />
																									<br />
																									<%-- <span style="font-weight:bold"> <c:out value="Auth Code:"/> </span> <c:out value="${element.auth_id}"/> <br /> --%>
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />
																								</c:when>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'EZYPAY'}">
																									<span style="font-weight: bold"> <c:out
																											value="EZYPAY" /> <c:if
																											test="${element.recurringTerm ne null}">
																											<c:out
																												value="-${element.recurringCardType}-${element.recurringTerm}" />
																										</c:if>
																									</span>
																									<br />
																									<span style="font-weight: bold"><c:out
																											value="Ref No:" /> </span>
																									<c:out value="${element.transaction_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Auth Code:" />
																									</span>
																									<c:out value="${element.auth_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />
																								</c:when>
																								<c:otherwise>
																								</c:otherwise>
																							</c:choose> <c:choose>
																								<c:when test="${element.modeStatus eq 'Auto'}">
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Mode:" />
																									</span>
																									<c:out value="Auto" />
																								</c:when>
																							</c:choose></td>
																						<td><c:if
																								test="${not empty element.pmnt_status}">
																								<c:choose>
																									<c:when test="${element.pmnt_status eq 'S'}">
																										<span style="color: green"> <c:out
																												value="Successful" />
																										</span>
																										<c:if
																											test="${element.refundStatus eq 'SUCCESS'}">
																											<br>
																											<span style="color: red"> RT Cancel </span>
																										</c:if>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'AP'}">
																										<span> <c:out value="Attempt Payment" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'F'}">
																										<span style="color: red"> <c:out
																												value="Fail" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'C'}">
																										<span style="color: black"> <c:out
																												value="Cancel" />
																										</span>
																									</c:when>
																									<%-- <c:otherwise>
																			          <c:out value="Other1"/>
																			          </c:otherwise>	 --%>
																								</c:choose>
																							</c:if> <c:if
																								test="${not empty element.quotation_status}">
																								<c:choose>
																									<c:when
																										test="${element.quotation_status eq 'O'}">
																										<span style="color: black"> <c:out
																												value="Incomplete" /> <c:choose>
																												<c:when
																													test="${element.last_page eq '1' || element.last_page eq '0' || element.last_page eq ''}">
																													<br />
																													<c:out value="Step 1 : Get A Quote" />
																												</c:when>
																												<c:when
																													test="${element.last_page eq '2' || element.last_page eq '3' }">
																													<br />
																													<c:out
																														value="Step 2 : Detailed Information" />
																												</c:when>
																												<c:when test="${element.last_page eq '6'}">
																													<br />
																													<c:out value="Step 3 : Confirmation" />
																												</c:when>
																												<c:otherwise>
																													<br />
																													<c:out value="" />
																												</c:otherwise>
																											</c:choose>
																										</span>
																									</c:when>
																									<c:when
																										test="${element.quotation_status eq 'R'}">
																										<span style="color: red"> <c:out
																												value="Rejection" />: <c:out
																												value="${element.reason}" />
																										</span>
																									</c:when>
																									<%-- <c:otherwise>
																		           <c:out value="Other2"/>
																		          </c:otherwise> --%>
																								</c:choose>
																							</c:if></td>
																						<td><c:choose>
																								<c:when
																									test="${not empty element.motor_net_premium}">
																									<fmt:formatNumber var="newMotorNetPremium"
																										type="number" groupingUsed="false"
																										minFractionDigits="2" maxFractionDigits="2"
																										value="${element.motor_net_premium}" />
																								</c:when>
																								<c:otherwise>
																									<c:set var="newMotorNetPremium" value="" />
																								</c:otherwise>
																							</c:choose> <c:choose>
																								<c:when
																									test="${not empty element.passenger_pa_premium_payable}">
																									<fmt:formatNumber var="newCAPPDPPA"
																										type="number" groupingUsed="false"
																										minFractionDigits="2" maxFractionDigits="2"
																										value="${element.passenger_pa_premium_payable}" />
																								</c:when>
																								<c:otherwise>
																									<c:set var="newCAPPDPPA" value="" />
																								</c:otherwise>
																							</c:choose> Motor: <c:out value="${newMotorNetPremium}" /> <br />
																							<c:if test="${element.product_code eq 'MT'}">
																		        DPPA: <c:out value="${newCAPPDPPA}" />
																								<br />
																							</c:if> <c:if test="${element.product_code eq 'MI'}">
																		         CAPS: <c:out value="${newCAPPDPPA}" />
																								<br />
																							</c:if> RT: <c:out value="${element.rttotalpayable}" />
																							<br /> Total: <c:out value="${newAmount}" /></td>
																					</tr>
																				</c:if>
																				<c:if
																					test="${element.product_code eq 'WTC' || element.product_code eq 'TPT'}">
																					<tr>
																						<td align="center"><c:out
																								value="${theCount.count}" /></td>
																						<td><c:out
																								value="${element.transaction_datetime}" /></td>
																						<td><c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.policy_number}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.policy_number}" />
																								</c:otherwise>
																							</c:choose></td>
																						<td><span style="font-weight: bold">Name:</span>
																							<c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.customer_name}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.customer_name}" />
																								</c:otherwise>
																							</c:choose> <br /> <span style="font-weight: bold">ID
																								No:</span> <c:out value="${element.customer_nric_id}" />
																							<br /> <c:out value="${element.agent_name}" /> (<c:out
																								value="${element.agent_code}" />)</td>
																						<td><c:out value="${element.product_code}" />
																							<br /> <c:choose>
																								<c:when test="${element.product_code eq 'WTC'}">																		   
																			           (<c:out value="EGIB" />)
																			          </c:when>
																								<c:otherwise>
																			          (<c:out value="EGTB" />)
																			          </c:otherwise>
																							</c:choose> <br> <b>Travel: </b>
																							${element.travel_area_type} <c:choose>
																								<c:when
																									test="${element.travel_area_type eq 'International' and element.last_page ne '1' and element.last_page ne '0'  }">																		   
																			           (<c:out
																										value="${element.offered_plan_name}" />)
																			          </c:when>
																								<c:otherwise>
																								</c:otherwise>
																							</c:choose> <br> <b>Scheme: </b>${element.travlling_with}




																							<br> <b>Days:</b> ${element.travel_duration}
																							days</td>
																						<td><c:choose>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'Maybank2U MOTOR' || fn:trim(element.pmnt_gateway_code) eq 'Maybank2U'}">
																									<span style="font-weight: bold"> <c:out
																											value="M2U" />
																									</span>
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Ref No:" />
																									</span>
																									<c:out value="${element.bankrefno}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="App Code:" />
																									</span>
																									<c:out value="${element.approvalcode}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />

																								</c:when>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'EBPG' || fn:trim(element.pmnt_gateway_code) eq 'EBPG 2D' || fn:trim(element.pmnt_gateway_code) eq 'ETB VISA 2D'}">
																									<span style="font-weight: bold"> <c:out
																											value="EBPG" />
																									</span>
																									<br />
																									<span style="font-weight: bold"><c:out
																											value="Ref No:" /> </span>
																									<c:out value="${element.transaction_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Auth Code:" />
																									</span>
																									<c:out value="${element.auth_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />
																								</c:when>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'FPXMOTOR' || fn:trim(element.pmnt_gateway_code) eq 'FPX' || fn:trim(element.pmnt_gateway_code) eq 'ETB FPX'  || fn:trim(element.pmnt_gateway_code) eq 'EGTB FPX' }">
																									<span style="font-weight: bold"> <c:out
																											value="FPX" />
																									</span>
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Ref No:" />
																									</span>
																									<c:out value="${element.fpx_fpxtxnid}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="FPX Status:" />
																									</span>
																									<c:out value="${element.fpx_debitauthcode}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />
																								</c:when>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'AMEX' || fn:trim(element.pmnt_gateway_code) eq 'AMEX 2D'}">
																									<span style="font-weight: bold"> <c:out
																											value="AMEX" />
																									</span>
																									<br />
																									<span style="font-weight: bold"><c:out
																											value="Ref No:" /> </span>
																									<c:out value="${element.transaction_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Auth Code:" />
																									</span>
																									<c:out value="${element.auth_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />
																								</c:when>
																								<c:otherwise>
																								</c:otherwise>
																							</c:choose> <c:choose>
																								<c:when test="${element.modeStatus eq 'Auto'}">
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Mode:" />
																									</span>
																									<c:out value="Auto" />
																								</c:when>
																							</c:choose></td>
																						<td><c:if
																								test="${not empty element.pmnt_status}">
																								<c:choose>
																									<c:when test="${element.pmnt_status eq 'S'}">
																										<span style="color: green"> <c:out
																												value="Successful" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'AP'}">
																										<span> <c:out value="Attempt Payment" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'F'}">
																										<span style="color: red"> <c:out
																												value="Fail" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'C'}">
																										<span style="color: black"> <c:out
																												value="Cancel" />
																										</span>
																									</c:when>

																								</c:choose>
																							</c:if> <c:if
																								test="${not empty element.quotation_status}">
																								<c:choose>
																									<c:when
																										test="${element.quotation_status eq 'O'}">
																										<span style="color: black"> <c:out
																												value="Incomplete" /> <c:choose>
																												<c:when
																													test="${element.last_page eq '1' || element.last_page eq '0' || element.last_page eq ''}">
																													<br />
																													<c:out value="Step 1 : Get A Quote" />
																												</c:when>
																												<c:when test="${element.last_page eq '2'}">
																													<br />
																													<c:out
																														value="Step 2 : Detailed Information" />
																												</c:when>
																												<c:when test="${element.last_page eq '3'}">
																													<br />
																													<c:out value="Step 3 : Confirmation" />
																												</c:when>
																											</c:choose>
																										</span>
																									</c:when>
																									<c:when
																										test="${element.quotation_status eq 'R'}">
																										<span style="color: red"> <c:out
																												value="Rejection" /> <c:if
																												test="${not empty element.reason}">
																		               : <c:out value="${element.reason}" />
																											</c:if>
																										</span>
																									</c:when>

																								</c:choose>
																							</c:if></td>
																						<td><c:out value="${newAmount}" /></td>
																					</tr>
																				</c:if>
																				<c:if
																					test="${element.product_code eq 'HOHH' || element.product_code eq 'HOHH-ETB'}">
																					<tr>
																						<td align="center"><c:out
																								value="${theCount.count}" /></td>
																						<td><c:out
																								value="${element.transaction_datetime}" /></td>
																						<td><c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.policy_number}" /></a>
																								</c:when>
																								<c:otherwise>

																								</c:otherwise>
																							</c:choose></td>
																						<td><span style="font-weight: bold">Name:</span>
																							<c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.customer_name}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.customer_name}" />
																								</c:otherwise>
																							</c:choose> <br /> <span style="font-weight: bold">ID
																								No:</span> <c:out value="${element.customer_nric_id}" />
																							<br /> <c:out value="${element.agent_name}" /> (<c:out
																								value="${element.agent_code}" />)</td>
																						<td><c:out value="${element.product_code}" />
																							<br /> <c:choose>
																								<c:when test="${element.product_code eq 'HOHH'}">																		   
																			           (<c:out value="EGIB" />)
																			          </c:when>
																								<c:otherwise>
																			          (<c:out value="EGTB" />)
																			          </c:otherwise>
																							</c:choose> <c:choose>
																								<c:when
																									test="${element.home_sum_insured ne null and element.home_sum_insured ne '0' and element.home_sum_insured ne ''}">
																									<br>
																									<b>Building : </b>RM<c:out
																										value="${element.home_sum_insured }" />
																								</c:when>
																							</c:choose> <c:choose>
																								<c:when
																									test="${element.content_sum_insured ne null and element.content_sum_insured ne '0' and element.content_sum_insured ne ''}">
																									<br>
																									<b>Content : </b>RM<c:out
																										value="${element.content_sum_insured }" />
																								</c:when>
																							</c:choose> <c:choose>
																								<c:when
																									test="${element.add_ben_riot_strike eq '1'}">
																									<br>+ RSMD
																		</c:when>
																							</c:choose> <c:choose>
																								<c:when
																									test="${element.add_ben_extended_theft eq '1' and element.home_coverage eq 'building'}">
																									<br>+ Extended Theft Cover
																		</c:when>
																							</c:choose></td>
																						<td><c:choose>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'Maybank2U MOTOR' || fn:trim(element.pmnt_gateway_code) eq 'Maybank2U'}">
																									<span style="font-weight: bold"> <c:out
																											value="M2U" />
																									</span>
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Ref No:" />
																									</span>
																									<c:out value="${element.bankrefno}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="App Code:" />
																									</span>
																									<c:out value="${element.approvalcode}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />

																								</c:when>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'EBPG' || fn:trim(element.pmnt_gateway_code) eq 'EBPG 2D'}">
																									<span style="font-weight: bold"> <c:out
																											value="EBPG" />
																									</span>
																									<br />
																									<span style="font-weight: bold"><c:out
																											value="Ref No:" /> </span>
																									<c:out value="${element.transaction_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Auth Code:" />
																									</span>
																									<c:out value="${element.auth_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />
																								</c:when>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'FPXMOTOR' || fn:trim(element.pmnt_gateway_code) eq 'FPX'  || fn:trim(element.pmnt_gateway_code) eq 'ETB FPX' }">
																									<span style="font-weight: bold"> <c:out
																											value="FPX" />
																									</span>
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Ref No:" />
																									</span>
																									<c:out value="${element.fpx_fpxtxnid}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="FPX Status:" />
																									</span>
																									<c:out value="${element.fpx_debitauthcode}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />
																								</c:when>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'AMEX' || fn:trim(element.pmnt_gateway_code) eq 'AMEX 2D'}">
																									<span style="font-weight: bold"> <c:out
																											value="AMEX" />
																									</span>
																									<br />
																									<span style="font-weight: bold"><c:out
																											value="Ref No:" /> </span>
																									<c:out value="${element.transaction_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Auth Code:" />
																									</span>
																									<c:out value="${element.auth_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />
																								</c:when>
																								<c:otherwise>
																								</c:otherwise>
																							</c:choose> <c:choose>
																								<c:when test="${element.modeStatus eq 'Auto'}">
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Mode:" />
																									</span>
																									<c:out value="Auto" />
																								</c:when>
																							</c:choose></td>
																						<td><c:if
																								test="${not empty element.pmnt_status}">
																								<c:choose>
																									<c:when test="${element.pmnt_status eq 'S'}">
																										<span style="color: green"> <c:out
																												value="Successful" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'AP'}">
																										<span> <c:out value="Attempt Payment" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'F'}">
																										<span style="color: red"> <c:out
																												value="Fail" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'C'}">
																										<span style="color: black"> <c:out
																												value="Cancel" />
																										</span>
																									</c:when>
																								</c:choose>
																							</c:if> <c:if
																								test="${not empty element.quotation_status}">
																								<c:choose>
																									<c:when
																										test="${element.quotation_status eq 'O'}">
																										<span style="color: black"> <c:out
																												value="Incomplete" /> <c:choose>
																												<c:when
																													test="${element.last_page eq '1' || element.last_page eq '0' || element.last_page eq '' || element.last_page eq '2'}">
																													<br />
																													<c:out value="Step 1 : Get A Quote" />
																												</c:when>
																												<c:when test="${element.last_page eq '3'}">
																													<br />
																													<c:out
																														value="Step 2 : Detailed Information" />
																												</c:when>
																												<c:when test="${element.last_page eq '4'}">
																													<br />
																													<c:out value="Step 3 : Confirmation" />
																												</c:when>
																												<c:otherwise>
																													<br />
																													<c:out value="" />
																												</c:otherwise>
																											</c:choose>
																										</span>
																									</c:when>
																									<c:when
																										test="${element.quotation_status eq 'R'}">
																										<span style="color: red"> <c:out
																												value="Rejection" /> <c:if
																												test="${not empty element.reason}">
																		               : <c:out value="${element.reason}" />
																											</c:if>
																										</span>
																									</c:when>

																								</c:choose>
																							</c:if></td>
																						<td><c:out value="${newAmount}" /></td>
																					</tr>
																				</c:if>
																				<c:if test="${element.product_code eq 'TL'}">
																					<tr>
																						<td align="center"><c:out
																								value="${theCount.count}" /></td>
																						<td><c:out
																								value="${element.transaction_datetime}" /></td>
																						<td><c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.policy_number}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.policy_number}" />
																								</c:otherwise>
																							</c:choose></td>
																						<td><span style="font-weight: bold">Name:</span>
																							<c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.customer_name}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.customer_name}" />
																								</c:otherwise>
																							</c:choose> <br /> <span style="font-weight: bold">ID
																								No:</span> <c:out value="${element.customer_nric_id}" />

																							<br> <b>Term:</b> ${element.coverage_term}
																							Years <br> <b>Coverage:</b> RM
																							${element.coverage_amount} <br /> <c:out
																								value="${element.agent_name}" /> (<c:out
																								value="${element.agent_code}" />)</td>
																						<td><c:out value="${element.product_code}" />
																							<br /> <c:choose>
																								<c:when test="${element.product_code eq 'TL'}">																		   
																			           (<c:out value="EGIB" />)
																			          </c:when>
																								<c:otherwise>
																			          (<c:out value="EGTB" />)
																			          </c:otherwise>
																							</c:choose></td>
																						<td><c:choose>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'CREDIT CARD'}">
																									<span style="font-weight: bold"> <c:out
																											value="MPAY" />
																									</span>
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Ref No:" />
																									</span>
																									<c:out value="${element.txn_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Auth Code:" />
																									</span>
																									<c:out value="${element.auth_code}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Mode" />
																									</span>
																									<c:out value="${element.premium_mode}" />
																								</c:when>
																								<c:otherwise>
																								</c:otherwise>
																							</c:choose></td>
																						<td><c:if
																								test="${not empty element.pmnt_status}">
																								<c:choose>
																									<c:when test="${element.pmnt_status eq 'S'}">
																										<span style="color: green"> <c:out
																												value="Successful" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'AP'}">
																										<span> <c:out value="Attempt Payment" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'F'}">
																										<span style="color: red"> <c:out
																												value="Fail" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'C'}">
																										<span style="color: black"> <c:out
																												value="Cancel" />
																										</span>
																									</c:when>
																									<%-- <c:otherwise>
																			          <c:out value="Other1"/>
																			          </c:otherwise>	 --%>
																								</c:choose>
																							</c:if> <c:if
																								test="${not empty element.quotation_status}">
																								<c:choose>
																									<c:when
																										test="${element.quotation_status eq 'O'}">
																										<span style="color: black"> <c:out
																												value="Incomplete" /> <c:choose>
																												<c:when
																													test="${element.last_page eq '1' || element.last_page eq '0' || element.last_page eq ''}">
																													<br />
																													<c:out value="Step 1 : Get A Quote" />
																												</c:when>
																												<c:when test="${element.last_page eq '2' }">
																													<br />
																													<c:out
																														value="Step 2 : Detailed Information" />
																												</c:when>
																												<c:when test="${element.last_page eq '3'}">
																													<br />
																													<c:out value="Step 3 : Confirmation" />
																												</c:when>
																											</c:choose>
																										</span>
																									</c:when>
																									<c:when
																										test="${element.quotation_status eq 'R'}">
																										<span style="color: red"> <c:out
																												value="Rejection" /> <c:if
																												test="${not empty element.reason}">
																		               : <c:out value="${element.reason}" />
																											</c:if> <c:if
																												test="${not empty element.UWReason and element.UWReason ne element.reason}">
																		                (<c:out value="${element.UWReason}" /> )
																		            </c:if>
																										</span>
																									</c:when>


																								</c:choose>
																							</c:if></td>
																						<td><c:out value="${newAmount}" /></td>
																					</tr>
																				</c:if>
																				<c:if test="${element.product_code eq 'PCCA01'|| element.product_code eq 'PTCA01'}">
																					<tr>
																						<td align="center"><c:out
																								value="${theCount.count}" /></td>
																						<td><c:out
																								value="${element.transaction_datetime}" /></td>
																						<td><c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.policy_number}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.policy_number}" />
																								</c:otherwise>
																							</c:choose></td>
																						<td><span style="font-weight: bold">Name:</span>
																							<c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.customer_name}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.customer_name}" />
																								</c:otherwise>
																							</c:choose> <br /> <span style="font-weight: bold">ID
																								No:</span> <c:out value="${element.customer_nric_id}" />

																							<br> <b>Term:</b> ${element.coverage_term}
																							Years <br> <b>Coverage:</b> RM
																							${element.coverage_amount} <br /> <c:out value="${element.agent_name}" /> (<c:out
																								value="${element.agent_code}" />)</td>
																						<td><c:out value="${element.product_code}" />
																							<br /> <c:choose>
																					<c:when test="${element.product_code eq 'PCCA01'}">																		   
																			           (<c:out value="EGIB" />)
																			          </c:when>
																								<c:otherwise>
																			          (<c:out value="EGTB" />)
																			          </c:otherwise>
																							</c:choose></td>
																						<td><c:choose>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'CREDIT CARD'}">
																									<span style="font-weight: bold"> <c:out
																											value="MPAY" />
																									</span>
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Ref No:" />
																									</span>
																									<c:out value="${element.txn_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Auth Code:" />
																									</span>
																									<c:out value="${element.auth_code}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Mode" />
																									</span>
																									<c:out value="${element.premium_mode}" />
																								</c:when>
																								<c:otherwise>
																								</c:otherwise>
																							</c:choose></td>
																						<td><c:if
																								test="${not empty element.pmnt_status}">
																								<c:choose>
																									<c:when test="${element.pmnt_status eq 'S'}">
																										<span style="color: green"> <c:out
																												value="Successful" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'AP'}">
																										<span> <c:out value="Attempt Payment" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'F'}">
																										<span style="color: red"> <c:out
																												value="Fail" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'C'}">
																										<span style="color: black"> <c:out
																												value="Cancel" />
																										</span>
																									</c:when>
																									<%-- <c:otherwise>
																			          <c:out value="Other1"/>
																			          </c:otherwise>	 --%>
																								</c:choose>
																							</c:if> <c:if
																								test="${not empty element.quotation_status}">
																								<c:choose>
																									<c:when
																										test="${element.quotation_status eq 'O'}">
																										<span style="color: black"> <c:out
																												value="Incomplete" /> <c:choose>
																												<c:when
																													test="${element.last_page eq '1' || element.last_page eq '0' || element.last_page eq ''}">
																													<br />
																													<c:out value="Step 1 : Get A Quote" />
																												</c:when>
																												<c:when test="${element.last_page eq '2' }">
																													<br />
																													<c:out
																														value="Step 2 : Detailed Information" />
																												</c:when>
																												<c:when test="${element.last_page eq '3'}">
																													<br />
																													<c:out value="Step 3 : Confirmation" />
																												</c:when>
																											</c:choose>
																										</span>
																									</c:when>
																									<c:when
																										test="${element.quotation_status eq 'R'}">
																										<span style="color: red"> <c:out
																												value="Rejection" /> <c:if
																												test="${not empty element.reason}">
																		               : <c:out value="${element.reason}" />
																											</c:if> <c:if
																												test="${not empty element.UWReason and element.UWReason ne element.reason}">
																		                (<c:out value="${element.UWReason}" /> )
																		            </c:if>
																										</span>
																									</c:when>


																								</c:choose>
																							</c:if></td>
																						<td><c:out value="${newAmount}" /></td>
																					</tr>
																				</c:if>
																				
																				
																				<c:if test="${element.product_code eq 'PCTM01'  || element.product_code eq 'PCTM02'  ||element.product_code eq 'PCTM03'}">
																					<tr>
																						<td align="center"><c:out
																								value="${theCount.count}" /></td>
																						<td><c:out
																								value="${element.transaction_datetime}" /></td>
																						<td><c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.policy_number}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.policy_number}" />
																								</c:otherwise>
																							</c:choose></td>
																						<td><span style="font-weight: bold">Name:</span>
																							<c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.customer_name}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.customer_name}" />
																								</c:otherwise>
																							</c:choose> <br /> <span style="font-weight: bold">ID
																								No:</span> <c:out value="${element.customer_nric_id}" />

																							<br> <b>Term:</b> ${element.coverage_term}
																							Years <br> <b>Coverage:</b> 
																							${element.coverage_amount} <br /> <c:out
																								value="${element.agent_name}" /> (<c:out
																								value="${element.agent_code}" />)</td>
																						<td><c:out value="${element.product_code}" /> 
																							<br /> <c:choose>
																					<c:when test="${element.product_code eq 'PCTM01'  || element.product_code eq 'PCTM02'  ||element.product_code eq 'PCTM03'}">																		   
																			           (<c:out value="EGIB" />)
																			          </c:when>
																								<c:otherwise>
																			          (<c:out value="EGTB" />)
																			          </c:otherwise>
																							</c:choose></td>
																						<td><c:choose>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'CREDIT CARD'}">
																									<span style="font-weight: bold"> <c:out
																											value="MPAY" />
																									</span>
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Ref No:" />
																									</span>
																									<c:out value="${element.txn_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Auth Code:" />
																									</span>
																									<c:out value="${element.auth_code}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Mode" />
																									</span>
																									<c:out value="${element.premium_mode}" />
																								</c:when>
																								<c:otherwise>
																								</c:otherwise>
																							</c:choose></td>
																						<td><c:if
																								test="${not empty element.pmnt_status}">
																								<c:choose>
																									<c:when test="${element.pmnt_status eq 'S'}">
																										<span style="color: green"> <c:out
																												value="Successful" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'AP'}">
																										<span> <c:out value="Attempt Payment" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'F'}">
																										<span style="color: red"> <c:out
																												value="Fail" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'C'}">
																										<span style="color: black"> <c:out
																												value="Cancel" />
																										</span>
																									</c:when>
																									<%-- <c:otherwise>
																			          <c:out value="Other1"/>
																			          </c:otherwise>	 --%>
																								</c:choose>
																							</c:if> <c:if
																								test="${not empty element.quotation_status}">
																								<c:choose>
																									<c:when
																										test="${element.quotation_status eq 'O'}">
																										<span style="color: black"> <c:out
																												value="Incomplete" /> <c:choose>
																												<c:when
																													test="${element.last_page eq '1' || element.last_page eq '0' || element.last_page eq ''}">
																													<br />
																													<c:out value="Step 1 : Get A Quote" />
																												</c:when>
																												<c:when test="${element.last_page eq '2' }">
																													<br />
																													<c:out
																														value="Step 2 : Detailed Information" />
																												</c:when>
																												<c:when test="${element.last_page eq '3'}">
																													<br />
																													<c:out value="Step 3 : Confirmation" />
																												</c:when>
																											</c:choose>
																										</span>
																									</c:when>
																									<c:when
																										test="${element.quotation_status eq 'R'}">
																										<span style="color: red"> <c:out
																												value="Rejection" /> <c:if
																												test="${not empty element.reason}">
																		               : <c:out value="${element.reason}" />
																											</c:if> <c:if
																												test="${not empty element.UWReason and element.UWReason ne element.reason}">
																		                (<c:out value="${element.UWReason}" /> )
																		            </c:if>
																										</span>
																									</c:when>


																								</c:choose>
																							</c:if></td>
																						<td><c:out value="${newAmount}" /></td>
																					</tr>
																				</c:if>
														
														
																				<c:if test="${element.product_code eq 'EZYTL'}">
																					<tr>
																						<td align="center"><c:out
																								value="${theCount.count}" /></td>
																						<td><c:out
																								value="${element.transaction_datetime}" /></td>
																						<td><c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.policy_number}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.policy_number}" />
																								</c:otherwise>
																							</c:choose></td>
																						<td><span style="font-weight: bold">Name:</span>
																							<c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.customer_name}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.customer_name}" />
																								</c:otherwise>
																							</c:choose> <br /> <span style="font-weight: bold">ID
																								No:</span> <c:out value="${element.customer_nric_id}" />

																							<br> <b>Term:</b> ${element.coverage_term}
																							Years <br> <b>Coverage:</b> RM
																							${element.coverage_amount} <br /> <c:out
																								value="${element.agent_name}" /> (<c:out
																								value="${element.agent_code}" />)</td>
																						<td><c:out value="${element.product_code}" />
																							<br /> <c:choose>
																								<c:when test="${element.product_code eq 'EZYTL'}">																		   
																			           (<c:out value="EGIB" />)
																			          </c:when>
																								<c:otherwise>
																			          (<c:out value="EGTB" />)
																			          </c:otherwise>
																							</c:choose></td>
																						<td><c:choose>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'CREDIT CARD'}">
																									<span style="font-weight: bold"> <c:out
																											value="MPAY" />
																									</span>
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Ref No:" />
																									</span>
																									<c:out value="${element.txn_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Auth Code:" />
																									</span>
																									<c:out value="${element.auth_code}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Mode" />
																									</span>
																									<c:out value="${element.premium_mode}" />
																								</c:when>
																								<c:otherwise>
																								</c:otherwise>
																							</c:choose></td>
																						<td><c:if
																								test="${not empty element.pmnt_status}">
																								<c:choose>
																									<c:when test="${element.pmnt_status eq 'S'}">
																										<span style="color: green"> <c:out
																												value="Successful" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'AP'}">
																										<span> <c:out value="Attempt Payment" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'F'}">
																										<span style="color: red"> <c:out
																												value="Fail" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'C'}">
																										<span style="color: black"> <c:out
																												value="Cancel" />
																										</span>
																									</c:when>
																									<%-- <c:otherwise>
																			          <c:out value="Other1"/>
																			          </c:otherwise>	 --%>
																								</c:choose>
																							</c:if> <c:if
																								test="${not empty element.quotation_status}">
																								<c:choose>
																									<c:when
																										test="${element.quotation_status eq 'O'}">
																										<span style="color: black"> <c:out
																												value="Incomplete" /> <c:choose>
																												<c:when
																													test="${element.last_page eq '1' || element.last_page eq '0' || element.last_page eq ''}">
																													<br />
																													<c:out value="Step 1 : Get A Quote" />
																												</c:when>
																												<c:when test="${element.last_page eq '2' }">
																													<br />
																													<c:out
																														value="Step 2 : Detailed Information" />
																												</c:when>
																												<c:when test="${element.last_page eq '3'}">
																													<br />
																													<c:out value="Step 3 : Confirmation" />
																												</c:when>
																											</c:choose>
																										</span>
																									</c:when>
																									<c:when
																										test="${element.quotation_status eq 'R'}">
																										<span style="color: red"> <c:out
																												value="Rejection" /> <c:if
																												test="${not empty element.reason}">
																		               : <c:out value="${element.reason}" />
																											</c:if> <c:if
																												test="${not empty element.UWReason and element.UWReason ne element.reason}">
																		                (<c:out value="${element.UWReason}" /> )
																		            </c:if>
																										</span>
																									</c:when>


																								</c:choose>
																							</c:if></td>
																						<td><c:out value="${newAmount}" /></td>
																					</tr>
																				</c:if>


																				<c:if test="${element.product_code eq 'ISCTL'}">
																					<tr>
																						<td align="center"><c:out
																								value="${theCount.count}" /></td>
																						<td><c:out
																								value="${element.transaction_datetime}" /></td>
																						<td><c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.policy_number}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.policy_number}" />
																								</c:otherwise>
																							</c:choose></td>
																						<td><span style="font-weight: bold">Name:</span>
																							<c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.customer_name}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.customer_name}" />
																								</c:otherwise>
																							</c:choose> <br /> <span style="font-weight: bold">ID
																								No:</span> <c:out value="${element.customer_nric_id}" />

																							<br> <b>Term:</b> ${element.coverage_term}
																							Years <br> <b>Coverage:</b> RM
																							${element.coverage_amount} <br /> <c:out
																								value="${element.agent_name}" /> (<c:out
																								value="${element.agent_code}" />)</td>
																						<td><c:out value="${element.product_code}" />
																							<br /> <c:choose>
																								<c:when
																									test="${element.product_code eq 'ISCTL'}">																		   
																			           (<c:out value="EGTB" />)
																			          </c:when>
																								<c:otherwise>
																			          (<c:out value="EGIB" />)
																			          </c:otherwise>
																							</c:choose></td>
																						<td><c:choose>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'CREDIT CARD'}">
																									<span style="font-weight: bold"> <c:out
																											value="MPAY" />
																									</span>
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Ref No:" />
																									</span>
																									<c:out value="${element.txn_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Auth Code:" />
																									</span>
																									<c:out value="${element.auth_code}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Mode" />
																									</span>
																									<c:out value="${element.premium_mode}" />
																								</c:when>
																								<c:otherwise>
																								</c:otherwise>
																							</c:choose></td>
																						<td><c:if
																								test="${not empty element.pmnt_status}">
																								<c:choose>
																									<c:when test="${element.pmnt_status eq 'S'}">
																										<span style="color: green"> <c:out
																												value="Successful" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'AP'}">
																										<span> <c:out value="Attempt Payment" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'F'}">
																										<span style="color: red"> <c:out
																												value="Fail" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'C'}">
																										<span style="color: black"> <c:out
																												value="Cancel" />
																										</span>
																									</c:when>
																									<%-- <c:otherwise>
																			          <c:out value="Other1"/>
																			          </c:otherwise>	 --%>
																								</c:choose>
																							</c:if> <c:if
																								test="${not empty element.quotation_status}">
																								<c:choose>
																									<c:when
																										test="${element.quotation_status eq 'O'}">
																										<span style="color: black"> <c:out
																												value="Incomplete" /> <c:choose>
																												<c:when
																													test="${element.last_page eq '1' || element.last_page eq '0' || element.last_page eq ''}">
																													<br />
																													<c:out value="Step 1 : Get A Quote" />
																												</c:when>
																												<c:when test="${element.last_page eq '2' }">
																													<br />
																													<c:out
																														value="Step 2 : Detailed Information" />
																												</c:when>
																												<c:when test="${element.last_page eq '3'}">
																													<br />
																													<c:out value="Step 3 : Confirmation" />
																												</c:when>
																											</c:choose>
																										</span>
																									</c:when>
																									<c:when
																										test="${element.quotation_status eq 'R'}">
																										<span style="color: red"> <c:out
																												value="Rejection" /> <c:if
																												test="${not empty element.reason}">
																		               : <c:out value="${element.reason}" />
																											</c:if> <c:if
																												test="${not empty element.UWReason and element.UWReason ne element.reason}">
																		                (<c:out value="${element.UWReason}" /> )
																		            </c:if>
																										</span>
																									</c:when>


																								</c:choose>
																							</c:if></td>
																						<td><c:out value="${newAmount}" /></td>
																					</tr>
																				</c:if>

																				<!-- FOR IDS -->
																				<c:if test="${element.product_code eq 'IDS'}">
																					<tr>
																						<td align="center"><c:out
																								value="${theCount.count}" /></td>
																						<td><c:out
																								value="${element.transaction_datetime}" /></td>
																						<td><c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.policy_number}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.policy_number}" />
																								</c:otherwise>
																							</c:choose></td>
																						<td><span style="font-weight: bold">Name:</span>
																							<c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.customer_name}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.customer_name}" />
																								</c:otherwise>
																							</c:choose> <br /> <span style="font-weight: bold">ID
																								No:</span> <c:out value="${element.customer_nric_id}" />

																							<br> <b>Term:</b> ${element.coverage_term}
																							Years <br> <b>Coverage:</b> RM
																							${element.coverage_amount} <br /> <c:out
																								value="${element.agent_name}" /> (<c:out
																								value="${element.agent_code}" />)</td>
																						<td><c:out value="${element.product_code}" />
																							<br /> <c:choose>
																								<c:when test="${element.product_code eq 'IDS'}">																		   
																			           (<c:out value="EGTB" />)
																			          </c:when>
																								<c:otherwise>
																			          (<c:out value="EGIB" />)
																			          </c:otherwise>
																							</c:choose></td>
																						<td><c:choose>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'CREDIT CARD'}">
																									<span style="font-weight: bold"> <c:out
																											value="MPAY" />
																									</span>
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Ref No:" />
																									</span>
																									<c:out value="${element.txn_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Auth Code:" />
																									</span>
																									<c:out value="${element.auth_code}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Mode" />
																									</span>
																									<c:out value="${element.premium_mode}" />
																								</c:when>
																								<c:otherwise>
																								</c:otherwise>
																							</c:choose></td>
																						<td><c:if
																								test="${not empty element.pmnt_status}">
																								<c:choose>
																									<c:when test="${element.pmnt_status eq 'S'}">
																										<span style="color: green"> <c:out
																												value="Successful" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'AP'}">
																										<span> <c:out value="Attempt Payment" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'F'}">
																										<span style="color: red"> <c:out
																												value="Fail" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'C'}">
																										<span style="color: black"> <c:out
																												value="Cancel" />
																										</span>
																									</c:when>
																									<%-- <c:otherwise>
																			          <c:out value="Other1"/>
																			          </c:otherwise>	 --%>
																								</c:choose>
																							</c:if> <c:if
																								test="${not empty element.quotation_status}">
																								<c:choose>
																									<c:when
																										test="${element.quotation_status eq 'O'}">
																										<span style="color: black"> <c:out
																												value="Incomplete" /> <c:choose>
																												<c:when
																													test="${element.last_page eq '1' || element.last_page eq '0' || element.last_page eq ''}">
																													<br />
																													<c:out value="Step 1 : Get A Quote" />
																												</c:when>
																												<c:when test="${element.last_page eq '2' }">
																													<br />
																													<c:out
																														value="Step 2 : Detailed Information" />
																												</c:when>
																												<c:when test="${element.last_page eq '3'}">
																													<br />
																													<c:out value="Step 3 : Confirmation" />
																												</c:when>
																											</c:choose>
																										</span>
																									</c:when>
																									<c:when
																										test="${element.quotation_status eq 'R'}">
																										<span style="color: red"> <c:out
																												value="Rejection" /> <c:if
																												test="${not empty element.reason}">
																		               : <c:out value="${element.reason}" />
																											</c:if> <c:if
																												test="${not empty element.UWReason and element.UWReason ne element.reason}">
																		                (<c:out value="${element.UWReason}" /> )
																		            </c:if>
																										</span>
																									</c:when>


																								</c:choose>
																							</c:if></td>
																						<td><c:out value="${newAmount}" /></td>
																					</tr>
																				</c:if>
																				
																				<!-- FOR PTCA01 -->
																				<c:if test="${element.product_code eq 'PTCA01'}">
																					<tr>
																						<td align="center"><c:out
																								value="${theCount.count}" /></td>
																						<td><c:out
																								value="${element.transaction_datetime}" /></td>
																						<td><c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.policy_number}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.policy_number}" />
																								</c:otherwise>
																							</c:choose></td>
																						<td><span style="font-weight: bold">Name:</span>
																							<c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.customer_name}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.customer_name}" />
																								</c:otherwise>
																							</c:choose> <br /> <span style="font-weight: bold">ID
																								No:</span> <c:out value="${element.customer_nric_id}" />

																							<br> <b>Term:</b> ${element.coverage_term}
																							Years <br> <b>Coverage:</b> RM
																							${element.coverage_amount} <br /> <c:out
																								value="${element.agent_name}" /> (<c:out
																								value="${element.agent_code}" />)</td>
																						<td><c:out value="${element.product_code}" />
																							<br /> <c:choose>
																								<c:when test="${element.product_code eq 'PTCA01'}">																		   
																			           (<c:out value="EGTB" />)
																			          </c:when>
																								<c:otherwise>
																			          (<c:out value="EGIB" />)
																			          </c:otherwise>
																							</c:choose></td>
																						<td><c:choose>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'CREDIT CARD IDS'}">
																									<span style="font-weight: bold"> <c:out
																											value="MPAY" />
																									</span>
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Ref No:" />
																									</span>
																									<c:out value="${element.txn_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Auth Code:" />
																									</span>
																									<c:out value="${element.auth_code}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Mode" />
																									</span>
																									<c:out value="${element.premium_mode}" />
																								</c:when>
																								<c:otherwise>
																								</c:otherwise>
																							</c:choose></td>
																						<td><c:if
																								test="${not empty element.pmnt_status}">
																								<c:choose>
																									<c:when test="${element.pmnt_status eq 'S'}">
																										<span style="color: green"> <c:out
																												value="Successful" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'AP'}">
																										<span> <c:out value="Attempt Payment" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'F'}">
																										<span style="color: red"> <c:out
																												value="Fail" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'C'}">
																										<span style="color: black"> <c:out
																												value="Cancel" />
																										</span>
																									</c:when>
																									<%-- <c:otherwise>
																			          <c:out value="Other1"/>
																			          </c:otherwise>	 --%>
																								</c:choose>
																							</c:if> <c:if
																								test="${not empty element.quotation_status}">
																								<c:choose>
																									<c:when
																										test="${element.quotation_status eq 'O'}">
																										<span style="color: black"> <c:out
																												value="Incomplete" /> <c:choose>
																												<c:when
																													test="${element.last_page eq '1' || element.last_page eq '0' || element.last_page eq ''}">
																													<br />
																													<c:out value="Step 1 : Get A Quote" />
																												</c:when>
																												<c:when test="${element.last_page eq '2' }">
																													<br />
																													<c:out
																														value="Step 2 : Detailed Information" />
																												</c:when>
																												<c:when test="${element.last_page eq '3'}">
																													<br />
																													<c:out value="Step 3 : Confirmation" />
																												</c:when>
																											</c:choose>
																										</span>
																									</c:when>
																									<c:when
																										test="${element.quotation_status eq 'R'}">
																										<span style="color: red"> <c:out
																												value="Rejection" /> <c:if
																												test="${not empty element.reason}">
																		               : <c:out value="${element.reason}" />
																											</c:if> <c:if
																												test="${not empty element.UWReason and element.UWReason ne element.reason}">
																		                (<c:out value="${element.UWReason}" /> )
																		            </c:if>
																										</span>
																									</c:when>


																								</c:choose>
																							</c:if></td>
																						<td><c:out value="${newAmount}" /></td>
																					</tr>
																				</c:if>
																				
																				<c:if test="${element.product_code eq 'PTTM01'  || element.product_code eq 'PTTM02'  ||element.product_code eq 'PTTM03'}">
																					<tr>
																						<td align="center"><c:out
																								value="${theCount.count}" /></td>
																						<td><c:out
																								value="${element.transaction_datetime}" /></td>
																						<td><c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.policy_number}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.policy_number}" />
																								</c:otherwise>
																							</c:choose></td>
																						<td><span style="font-weight: bold">Name:</span>
																							<c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.customer_name}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.customer_name}" />
																								</c:otherwise>
																							</c:choose> <br /> <span style="font-weight: bold">ID
																								No:</span> <c:out value="${element.customer_nric_id}" />

																							<br> <b>Term:</b> ${element.coverage_term}
																							Years <br> <b>Coverage:</b> 
																							${element.coverage_amount} <br /> <c:out
																								value="${element.agent_name}" /> (<c:out
																								value="${element.agent_code}" />)</td>
																						<td><c:out value="${element.product_code}" />
																							<br /> <c:choose>
																								<c:when test="${element.product_code eq 'PTTM01'  || element.product_code eq 'PTTM02'  ||element.product_code eq 'PTTM03'}">																		   
																			           (<c:out value="EGTB" />)
																			          </c:when>
																								<c:otherwise>
																			          (<c:out value="EGIB" />)
																			          </c:otherwise>
																							</c:choose></td>
																						<td><c:choose>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'CREDIT CARD IDS'}">
																									<span style="font-weight: bold"> <c:out
																											value="MPAY" />
																									</span>
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Ref No:" />
																									</span>
																									<c:out value="${element.txn_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Auth Code:" />
																									</span>
																									<c:out value="${element.auth_code}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Mode" />
																									</span>
																									<c:out value="${element.premium_mode}" />
																								</c:when>
																								<c:otherwise>
																								</c:otherwise>
																							</c:choose></td>
																						<td><c:if
																								test="${not empty element.pmnt_status}">
																								<c:choose>
																									<c:when test="${element.pmnt_status eq 'S'}">
																										<span style="color: green"> <c:out
																												value="Successful" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'AP'}">
																										<span> <c:out value="Attempt Payment" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'F'}">
																										<span style="color: red"> <c:out
																												value="Fail" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'C'}">
																										<span style="color: black"> <c:out
																												value="Cancel" />
																										</span>
																									</c:when>
																									<%-- <c:otherwise>
																			          <c:out value="Other1"/>
																			          </c:otherwise>	 --%>
																								</c:choose>
																							</c:if> <c:if
																								test="${not empty element.quotation_status}">
																								<c:choose>
																									<c:when
																										test="${element.quotation_status eq 'O'}">
																										<span style="color: black"> <c:out
																												value="Incomplete" /> <c:choose>
																												<c:when
																													test="${element.last_page eq '1' || element.last_page eq '0' || element.last_page eq ''}">
																													<br />
																													<c:out value="Step 1 : Get A Quote" />
																												</c:when>
																												<c:when test="${element.last_page eq '2' }">
																													<br />
																													<c:out
																														value="Step 2 : Detailed Information" />
																												</c:when>
																												<c:when test="${element.last_page eq '3'}">
																													<br />
																													<c:out value="Step 3 : Confirmation" />
																												</c:when>
																											</c:choose>
																										</span>
																									</c:when>
																									<c:when
																										test="${element.quotation_status eq 'R'}">
																										<span style="color: red"> <c:out
																												value="Rejection" /> <c:if
																												test="${not empty element.reason}">
																		               : <c:out value="${element.reason}" />
																											</c:if> <c:if
																												test="${not empty element.UWReason and element.UWReason ne element.reason}">
																		                (<c:out value="${element.UWReason}" /> )
																		            </c:if>
																										</span>
																									</c:when>


																								</c:choose>
																							</c:if></td>
																						<td><c:out value="${newAmount}" /></td>
																					</tr>
																				</c:if>
																				
																				
																				<!-- Buddy PA -->
																				<c:if
																					test="${element.product_code eq 'CPP' || element.product_code eq 'TPP'}">
																					<tr>
																						<td align="center"><c:out
																								value="${theCount.count}" /></td>
																						<td><c:out
																								value="${element.transaction_datetime}" /></td>
																						<td><c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.policy_number}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.policy_number}" />
																								</c:otherwise>
																							</c:choose> <br /></td>
																						<td><span style="font-weight: bold">Name:</span>
																							<c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.customer_name}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.customer_name}" />
																								</c:otherwise>
																							</c:choose> <br /> <span style="font-weight: bold">ID
																								No:</span> <c:out value="${element.customer_nric_id}" />
																							<br /> <c:out value="${element.agent_name}" /> (<c:out
																								value="${element.agent_code}" />)</td>
																						<td><c:out value="${element.product_code}" />
																							<br /> Combo : <c:out
																								value="${element.buddy_combo}" /> <br /> Plan
																							Name : <c:out value="${element.plan_name}" /> <br />
																							<c:choose>
																								<c:when test="${element.product_code eq 'CPP'}">																		   
																			           (<c:out value="EGIB" />)
																			          </c:when>
																								<c:otherwise>
																			          (<c:out value="EGTB" />)
																			          </c:otherwise>
																							</c:choose></td>
																						<td><c:choose>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'Maybank2U AAD' || fn:trim(element.pmnt_gateway_code) eq 'Maybank2U AAE' || fn:trim(element.pmnt_gateway_code) eq 'Maybank2U'}">
																									<span style="font-weight: bold"> <c:out
																											value="M2U" />
																									</span>
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Ref No:" />
																									</span>
																									<c:out value="${element.bankrefno}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="App Code:" />
																									</span>
																									<c:out value="${element.approvalcode}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />

																								</c:when>


																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'EIB EBPG VISA'|| fn:trim(element.pmnt_gateway_code) eq 'ETB EBPG VISA'||fn:trim(element.pmnt_gateway_code) eq 'EGIB EBPG VISA'|| fn:trim(element.pmnt_gateway_code) eq 'EGTB EBPG VISA'}">
																									<span style="font-weight: bold"> <c:out
																											value="EBPG" />
																									</span>
																									<br />
																									<span style="font-weight: bold"><c:out
																											value="Ref No:" /> </span>
																									<c:out value="${element.transaction_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Auth Code:" />
																									</span>
																									<c:out value="${element.auth_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />
																								</c:when>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'EIB FPX' || fn:trim(element.pmnt_gateway_code) eq 'ETB FPX'||fn:trim(element.pmnt_gateway_code) eq 'EGIB FPX' || fn:trim(element.pmnt_gateway_code) eq 'EGTB FPX'}">
																									<span style="font-weight: bold"> <c:out
																											value="FPX" />
																									</span>
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Ref No:" />
																									</span>
																									<c:out value="${element.fpx_fpxtxnid}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="FPX Status:" />
																									</span>
																									<c:out value="${element.fpx_debitauthcode}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />
																								</c:when>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'EIB EBPG AMEX'|| fn:trim(element.pmnt_gateway_code) eq 'ETB EBPG AMEX'||fn:trim(element.pmnt_gateway_code) eq 'EGIB EBPG AMEX'|| fn:trim(element.pmnt_gateway_code) eq 'EGTB EBPG AMEX'}">
																									<span style="font-weight: bold"> <c:out
																											value="AMEX" />
																									</span>
																									<br />
																									<span style="font-weight: bold"><c:out
																											value="Ref No:" /> </span>
																									<c:out value="${element.transaction_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Auth Code:" />
																									</span>
																									<c:out value="${element.auth_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />
																								</c:when>
																								<c:otherwise>
																								</c:otherwise>
																							</c:choose></td>
																						<td><c:if
																								test="${not empty element.pmnt_status}">
																								<c:choose>
																									<c:when test="${element.pmnt_status eq 'S'}">
																										<span style="color: green"> <c:out
																												value="Successful" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'AP'}">
																										<span> <c:out value="Attempt Payment" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'F'}">
																										<span style="color: red"> <c:out
																												value="Fail" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'C'}">
																										<span style="color: black"> <c:out
																												value="Cancel" />
																										</span>
																									</c:when>
																									<%-- <c:otherwise>
																			          <c:out value="Other1"/>
																			          </c:otherwise>	 --%>
																								</c:choose>
																							</c:if> <c:if
																								test="${not empty element.quotation_status}">
																								<c:choose>
																									<c:when
																										test="${element.quotation_status eq 'O'}">
																										<span style="color: black"> <c:out
																												value="Incomplete" /> <c:choose>
																												<c:when
																													test="${element.last_page eq '1' || element.last_page eq '0' || element.last_page eq ''}">
																													<br />
																													<c:out value="Step 1 : Get A Quote" />
																												</c:when>
																												<c:when
																													test="${element.last_page eq '2' || element.last_page eq '3' }">
																													<br />
																													<c:out
																														value="Step 2 : Detailed Information" />
																												</c:when>
																												<c:when test="${element.last_page eq '6'}">
																													<br />
																													<c:out value="Step 3 : Confirmation" />
																												</c:when>
																												<c:otherwise>
																													<br />
																													<c:out value="" />
																												</c:otherwise>
																											</c:choose>
																										</span>
																									</c:when>
																									<c:when
																										test="${element.quotation_status eq 'R'}">
																										<span style="color: red"> <c:out
																												value="Rejection" />: <c:out
																												value="${element.reason}" />
																										</span>
																									</c:when>
																									<%-- <c:otherwise>
																		           <c:out value="Other2"/>
																		          </c:otherwise> --%>
																								</c:choose>
																							</c:if></td>
																						<td><c:out value="${element.amount}" /></td>
																					</tr>
																				</c:if>
																				<!-- END Buddy PA -->
																				<!-- START Travel Ezy -->
																				<c:if
																					test="${element.product_code eq 'BPT' || element.product_code eq 'TZT'}">
																					<tr>
																						<td align="center"><c:out
																								value="${theCount.count}" /></td>
																						<td><c:out
																								value="${element.transaction_datetime}" /></td>
																						<td><c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.policy_number}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.policy_number}" />
																								</c:otherwise>
																							</c:choose></td>
																						<td><span style="font-weight: bold">Name:</span>
																							<c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.customer_name}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.customer_name}" />
																								</c:otherwise>
																							</c:choose> <br /> <span style="font-weight: bold">ID
																								No:</span> <c:out value="${element.customer_nric_id}" />
																							<br /> <c:out value="${element.agent_name}" /> (<c:out
																								value="${element.agent_code}" />)</td>
																						<td><c:out value="${element.product_code}" />
																							<br /> <c:choose>
																								<c:when test="${element.product_code eq 'BPT'}">																		   
																			           (<c:out value="EGIB" />)
																			          </c:when>
																								<c:otherwise>
																			          (<c:out value="EGTB" />)
																			          </c:otherwise>
																							</c:choose> <br> <b>Trip Type: </b>
																							${element.trip_type} <br> <b>Departure
																								Date: </b>${element.d_flight_dt} <br> <b>Return
																								Date: </b>${element.r_flight_dt} <br> <b>Companions:</b>
																							${element.number_of_companion}</td>
																						<td><c:choose>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'Maybank2U AAD' || fn:trim(element.pmnt_gateway_code) eq 'Maybank2U AAE' || fn:trim(element.pmnt_gateway_code) eq 'Maybank2U'}">
																									<span style="font-weight: bold"> <c:out
																											value="M2U" />
																									</span>
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Ref No:" />
																									</span>
																									<c:out value="${element.bankrefno}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="App Code:" />
																									</span>
																									<c:out value="${element.approvalcode}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />

																								</c:when>


																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'EIB EBPG VISA'|| fn:trim(element.pmnt_gateway_code) eq 'ETB EBPG VISA'||fn:trim(element.pmnt_gateway_code) eq 'EGIB EBPG VISA'|| fn:trim(element.pmnt_gateway_code) eq 'EGTB EBPG VISA'}">
																									<span style="font-weight: bold"> <c:out
																											value="EBPG" />
																									</span>
																									<br />
																									<span style="font-weight: bold"><c:out
																											value="Ref No:" /> </span>
																									<c:out value="${element.transaction_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Auth Code:" />
																									</span>
																									<c:out value="${element.auth_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />
																								</c:when>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'EIB FPX' || fn:trim(element.pmnt_gateway_code) eq 'ETB FPX'||fn:trim(element.pmnt_gateway_code) eq 'EGIB FPX' || fn:trim(element.pmnt_gateway_code) eq 'EGTB FPX'}">
																									<span style="font-weight: bold"> <c:out
																											value="FPX" />
																									</span>
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Ref No:" />
																									</span>
																									<c:out value="${element.fpx_fpxtxnid}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="FPX Status:" />
																									</span>
																									<c:out value="${element.fpx_debitauthcode}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />
																								</c:when>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'EIB EBPG AMEX'|| fn:trim(element.pmnt_gateway_code) eq 'ETB EBPG AMEX'||fn:trim(element.pmnt_gateway_code) eq 'EGIB EBPG AMEX'|| fn:trim(element.pmnt_gateway_code) eq 'EGTB EBPG AMEX'}">
																									<span style="font-weight: bold"> <c:out
																											value="AMEX" />
																									</span>
																									<br />
																									<span style="font-weight: bold"><c:out
																											value="Ref No:" /> </span>
																									<c:out value="${element.transaction_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Auth Code:" />
																									</span>
																									<c:out value="${element.auth_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />
																								</c:when>
																								<c:otherwise>
																								</c:otherwise>
																							</c:choose></td>
																						<td><c:if
																								test="${not empty element.pmnt_status}">
																								<c:choose>
																									<c:when test="${element.pmnt_status eq 'S'}">
																										<span style="color: green"> <c:out
																												value="Successful" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'AP'}">
																										<span> <c:out value="Attempt Payment" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'F'}">
																										<span style="color: red"> <c:out
																												value="Fail" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'C'}">
																										<span style="color: black"> <c:out
																												value="Cancel" />
																										</span>
																									</c:when>

																								</c:choose>
																							</c:if> <c:if
																								test="${not empty element.quotation_status}">
																								<c:choose>
																									<c:when
																										test="${element.quotation_status eq 'O'}">
																										<span style="color: black"> <c:out
																												value="Incomplete" /> <c:choose>
																												<c:when
																													test="${element.last_page eq '1' || element.last_page eq '0' || element.last_page eq ''}">
																													<br />
																													<c:out value="Step 1 : Get A Quote" />
																												</c:when>
																												<c:when test="${element.last_page eq '2'}">
																													<br />
																													<c:out
																														value="Step 2 : Detailed Information" />
																												</c:when>
																												<c:when test="${element.last_page eq '3'}">
																													<br />
																													<c:out value="Step 3 : Confirmation" />
																												</c:when>
																											</c:choose>
																										</span>
																									</c:when>
																									<c:when
																										test="${element.quotation_status eq 'R'}">
																										<span style="color: red"> <c:out
																												value="Rejection" /> <c:if
																												test="${not empty element.reason}">
																		               : <c:out value="${element.reason}" />
																											</c:if>
																										</span>
																									</c:when>

																								</c:choose>
																							</c:if></td>
																						<td><c:out value="${newAmount}" /></td>
																					</tr>
																				</c:if>
																				<!-- END Travel Ezy -->
																				<!-- START TRIPCARE  -->
																				<c:if
																					test="${element.product_code eq 'TCI' || element.product_code eq 'TCT'}">
																					<tr>
																						<td align="center"><c:out
																								value="${theCount.count}" /></td>
																						<td><c:out
																								value="${element.transaction_datetime}" /></td>
																						<td><c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.policy_number}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.policy_number}" />
																								</c:otherwise>
																							</c:choose></td>
																						<td><span style="font-weight: bold">Name:</span>
																							<c:choose>
																								<c:when
																									test="${element.pmnt_status eq 'S' || element.pmnt_status eq 'AP' || element.pmnt_status eq 'F' || element.pmnt_status eq 'C'}">
																									<a href=TransactionalReport?action=detail&id=<c:out value="${element.paymentTrxID}"/>><c:out value="${element.customer_name}" /></a>
																								</c:when>
																								<c:otherwise>
																									<c:out value="${element.customer_name}" />
																								</c:otherwise>
																							</c:choose> <br /> <span style="font-weight: bold">ID
																								No:</span> <c:out value="${element.customer_nric_id}" />
																							<br /> <c:out value="${element.agent_name}" /> (<c:out
																								value="${element.agent_code}" />)</td>
																						<td><c:out value="${element.product_code}" />
																							<br /> <c:choose>
																								<c:when test="${element.product_code eq 'TCI'}">																		   
																			           (<c:out value="EGIB" />)
																			          </c:when>
																								<c:otherwise>
																			          (<c:out value="EGTB" />)
																			          </c:otherwise>
																							</c:choose> <br> <b>Travel: </b>
																							${element.travel_area_type} <c:choose>
																								<c:when
																									test="${element.travel_area_type eq 'International' and element.last_page ne '1' and element.last_page ne '0'  }">																		   
																			           (<c:out
																										value="${element.offered_plan_name}" />)
																			          </c:when>
																								<c:otherwise>
																								</c:otherwise>
																							</c:choose> <br> <b>Scheme: </b>${element.travlling_with}

																							<br> <b>Days:</b> ${element.travel_duration}
																							days</td>
																						<td>
																						<c:choose>

																						
																				
																							<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'EBPG' || fn:trim(element.pmnt_gateway_code) eq 'EBPG 2D' || fn:trim(element.pmnt_gateway_code) eq 'ETB VISA 2D'}">
																									<span style="font-weight: bold"> <c:out
																											value="EBPG" />
																									</span>
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Ref No:" />
																									</span>
																									<c:out value="${element.bankrefno}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="App Code:" />
																									</span>
																									<c:out value="${element.approvalcode}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />

																								</c:when>
																				
																				
																						<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'Maybank2U AAD' || fn:trim(element.pmnt_gateway_code) eq 'Maybank2U AAE' || fn:trim(element.pmnt_gateway_code) eq 'Maybank2U'}">
																									<span style="font-weight: bold"> <c:out
																											value="M2U" />
																									</span>
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Ref No:" />
																									</span>
																									<c:out value="${element.bankrefno}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="App Code:" />
																									</span>
																									<c:out value="${element.approvalcode}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />

																								</c:when>
																								
																								
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'EBPG' || fn:trim(element.pmnt_gateway_code) eq 'EBPG 2D' || fn:trim(element.pmnt_gateway_code) eq 'ETB VISA 2D'}">
																									<span style="font-weight: bold"> <c:out
																											value="EBPG" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Ref No:" />
																									</span>
																									<c:out value="${element.bankrefno}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="App Code:" />
																									</span>
																									<c:out value="${element.approvalcode}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />

																								</c:when>


																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'EIB EBPG VISA'|| fn:trim(element.pmnt_gateway_code) eq 'ETB EBPG VISA'||fn:trim(element.pmnt_gateway_code) eq 'EGIB EBPG VISA'|| fn:trim(element.pmnt_gateway_code) eq 'EGTB EBPG VISA'}">
																									<span style="font-weight: bold"> <c:out
																											value="EBPG" />
																									</span>
																									<br />
																									<span style="font-weight: bold"><c:out
																											value="Ref No:" /> </span>
																									<c:out value="${element.transaction_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Auth Code:" />
																									</span>
																									<c:out value="${element.auth_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />
																								</c:when>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'EIB FPX' || fn:trim(element.pmnt_gateway_code) eq 'ETB FPX'||fn:trim(element.pmnt_gateway_code) eq 'EGIB FPX' || fn:trim(element.pmnt_gateway_code) eq 'EGTB FPX'}">
																									<span style="font-weight: bold"> <c:out
																											value="FPX" />
																									</span>
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Ref No:" />
																									</span>
																									<c:out value="${element.fpx_fpxtxnid}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="FPX Status:" />
																									</span>
																									<c:out value="${element.fpx_debitauthcode}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />
																								</c:when>
																								<c:when
																									test="${fn:trim(element.pmnt_gateway_code) eq 'EIB EBPG AMEX'|| fn:trim(element.pmnt_gateway_code) eq 'ETB EBPG AMEX'||fn:trim(element.pmnt_gateway_code) eq 'EGIB EBPG AMEX'|| fn:trim(element.pmnt_gateway_code) eq 'EGTB EBPG AMEX'}">
																									<span style="font-weight: bold"> <c:out
																											value="AMEX" />
																									</span>
																									<br />
																									<span style="font-weight: bold"><c:out
																											value="Ref No:" /> </span>
																									<c:out value="${element.transaction_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Auth Code:" />
																									</span>
																									<c:out value="${element.auth_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />
																								</c:when>
																								

																										<c:when test="${fn:trim(element.pmnt_gateway_code) eq 'AMEX 2D'|| fn:trim(element.pmnt_gateway_code) eq 'ETB AMEX 2D'||fn:trim(element.pmnt_gateway_code) eq 'EIB AMEX 2D'|| fn:trim(element.pmnt_gateway_code) eq 'EIB AMEX 2D'}">
																									<span style="font-weight: bold"> <c:out
																											value="AMEX" />
																									</span>
																									<br />
																									<span style="font-weight: bold"><c:out
																											value="Ref No:" /> </span>
																									<c:out value="${element.transaction_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Auth Code:" />
																									</span>
																									<c:out value="${element.auth_id}" />
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Tax Inv" />
																									</span>
																									<c:out value="${element.invoice_no}" />
																								</c:when>
																								
																								<c:otherwise>
																								</c:otherwise>
																							</c:choose> 
																							<c:choose>
																								<c:when test="${element.modeStatus eq 'Auto'}">
																									<br />
																									<span style="font-weight: bold"> <c:out
																											value="Mode:" />
																									</span>
																									<c:out value="Auto" />
																								</c:when>
																							</c:choose></td>
																						<td><c:if
																								test="${not empty element.pmnt_status}">
																								<c:choose>
																									<c:when test="${element.pmnt_status eq 'S'}">
																										<span style="color: green"> <c:out
																												value="Successful" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'AP'}">
																										<span> <c:out value="Attempt Payment" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'F'}">
																										<span style="color: red"> <c:out
																												value="Fail" />
																										</span>
																									</c:when>
																									<c:when test="${element.pmnt_status eq 'C'}">
																										<span style="color: black"> <c:out
																												value="Cancel" />
																										</span>
																									</c:when>

																								</c:choose>
																							</c:if> <c:if
																								test="${not empty element.quotation_status}">
																								<c:choose>
																									<c:when
																										test="${element.quotation_status eq 'O'}">
																										<span style="color: black"> <c:out
																												value="Incomplete" /> <c:choose>
																												<c:when
																													test="${element.last_page eq '1' || element.last_page eq '0' || element.last_page eq ''}">
																													<br />
																													<c:out value="Step 1 : Get A Quote" />
																												</c:when>
																												<c:when test="${element.last_page eq '2'}">
																													<br />
																													<c:out
																														value="Step 2 : Detailed Information" />
																												</c:when>
																												<c:when test="${element.last_page eq '3'}">
																													<br />
																													<c:out value="Step 3 : Confirmation" />
																												</c:when>
																											</c:choose>
																										</span>
																									</c:when>
																									<c:when
																										test="${element.quotation_status eq 'R'}">
																										<span style="color: red"> <c:out
																												value="Rejection" /> <c:if
																												test="${not empty element.reason}">
																		               : <c:out value="${element.reason}" />
																											</c:if>
																										</span>
																									</c:when>

																								</c:choose>
																							</c:if></td>
																						<td><c:out value="${newAmount}" /></td>
																					</tr>
																				</c:if>
																				<!--  END TRIPCARE -->
																			</c:forEach>
																		</c:if>


																	</tbody>
																</table>
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>
												<%-- </c:if> --%>
											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%-- <jsp:include page="siteFooter.jsp" /> --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->
	<script
		src="${pageContext.request.contextPath}/assets/js/jquery.min.bk.js"
		type="text/javascript"></script>
	<!-- <script src="${pageContext.request.contextPath}/assets/js/jquery.min.js" type="text/javascript"></script>  -->
	<!--  <script src="assets/jAlert/jquery-1.11.3.min.js"></script> -->
	<script
		src="${pageContext.request.contextPath}/assets/jAlert/jAlert.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/jAlert/jAlert-functions.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/bootstrap.js"
		type="text/javascript"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/retina/retina.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/backstretch/jquery.backstretch.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/apps.js"></script>

	<!-- PLUGINS -->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/prettify/prettify.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<!-- <script src="assets/plugins/chosen/chosen.jquery.min.js"></script> -->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/icheck/icheck.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/mask/jquery.mask.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/summernote/summernote.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/to-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/slider/bootstrap-slider.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/toastr/toastr.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/knob.js"></script>

	<script
		src="${pageContext.request.contextPath}/assets/js/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/dataTables.bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/dataTables.buttons.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/jszip.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/pdfmake.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/vfs_fonts.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.html5.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.print.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.colVis.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/toastr.min.js"></script>


	<script type="text/javascript">
         	$(document).ready(function() {
         		
         		 $("#date_pretty").datepicker({ 
         	    });
         	    var myDate = new Date();
         	    var month = myDate.getMonth() + 1;
         	    var prettyDate =  month + '/' + myDate.getDate()  + '/' + myDate.getFullYear();
         	    $("#date_pretty").val(prettyDate);
         		
         		        		
         		$('#eib-pr').hide();
                $('#eib-list').hide();
				$('#etb-list').hide();
				$('#all-list').hide();
			
				
				var productTypeSelected = $('#productTypeValSession').val();
				var productEntitySelected = $('#productEntityValSession').val();
				
				if(productTypeSelected =="MI" || productTypeSelected =="MT")
					{
					
					$('#mi-option').show();	
					
					}
				
				else{
					
					$('#mi-option').hide();	
					
				}
				
				
				
				if(productEntitySelected =="EIB")
				{
					$('#eib-pr').show();
					$('#eib-list').show();
    				$('#etb-list').hide();
    				$('#all-list').hide();
    				
    				
				
				}
				
				if(productEntitySelected =="ETB")
				{
					$('#eib-pr').show();
					$('#eib-list').hide();
    				$('#etb-list').show();
    				$('#all-list').hide();
    				
				
				}
			
				if(productEntitySelected =="")
				{
					
					$('#eib-list').hide();
    				$('#etb-list').hide();
    				$('#all-list').hide();
    				
				
				}
			
			
		
				
				 $("#productEntity").on('change', function() {	
	        			alert(this.value);
	        			if (this.value == 'ETB')
	        			{
	        				$('#eib-pr').show();
	        				$('#eib-list').hide();
	        				$('#etb-list').show();
	        				$('#all-list').hide();
	        				$('#mi-option').hide();	
	        				
	        				plateNo.value = null;
	        				productType.value=null;
	        			
	        				//$('#mi-option').css("display","none");
	        				
	        			} 
	        			
	        			else if (this.value == 'EIB')
	        				{
	        				$('#eib-pr').show();
	        				$('#eib-list').show();
	        				$('#etb-list').hide();
	        				$('#all-list').hide();
	        				$('#mi-option').hide();	
	        				
	        				plateNo.value = null;
	        				productType.value=null;
	        			
	        				//$('#mi-option').css("display","none");
	        				
	        				}
	        			
	        			else {
	        				$('#eib-pr').hide();
	        				$('#eib-list').hide();
	        				$('#etb-list').hide();
	        				$('#all-list').hide();
	        				$('#mi-option').hide();	
	        				
	        				plateNo.value = null;
	        				productType.value=null;
	        		
	        				//$('#mi-option').css("display","none");
	        				
	        			}
	        		});
				 
				 
				 $('#productTypeEIB').on('change', function() {	
		         		//alert("ETB = "+this.value);
		         		productType.value = this.value;
		         		
		         		if(this.value == "MI"){
		         			
		         			$('#mi-option').show();	
		         			
		         			
		         		}
		         		else{
		         			
		         			$('#mi-option').hide();
		         			plateNo.value = null;
		         			
		         		}
		         		
		         	
		         			
		         		});	
		         	
		         	$('#productTypeETB').on('change', function() {	
		         		//alert("EIB = "+this.value);
		         		productType.value = this.value;
                       
		         			
						if(this.value == "MT"){
		         			
		         			$('#mi-option').show();	
		         			
		         			
		         		}
		         		else{
		         			
		         			$('#mi-option').hide();
		         			plateNo.value = null;
		         		}
		         	
		         			
		         		});	
		         	
		        	$('#productTypeALL').on('change', function() {	
		         		//alert("ALL ="+this.value);
		         		productType.value = this.value;
		         			
							if(this.value == "MI" ){
									         			
							$('#mi-option').show();	
									         			
							}
							else{ 
									
									$('#mi-option').hide();
									plateNo.value = null;
								}
								
					
									         			
		         		});	
		         	
         		
		        	 var t = $('#admin-datatable-second_admin').DataTable( {
	         			 
		         			dom: 'Bfrtip',
		         			  buttons: [	
		{
		    extend: 'pageLength',
		    className: 'btn btn-secondary btn-sm active'
		},
		            {
		                extend: 'print',
		                className: 'btn btn-warning btn-sm',
		                title : 'Etiqa Transaction Report',
		                text: 'Print <i class="fa fa-print" aria-hidden="true"></i>'
		            }
		//,
		            //{
		               // extend: 'excel',
		                //filename: 'Etiqa Transaction Report',
		               // className: 'btn btn-warning btn-sm',
		                //text:'Export to XLS <i class="fa fa-download" aria-hidden="true"></i>'
		           // }
		         			  ],
		         		      "searching" :false, 
		         	          "columnDefs": [ {
		         	          "searchable": false,
		         	          "orderable": false,
		         	          "targets": [0,1,2,3,4,5,6,7],
		         	          "bSortable": false
		         	         
		         	        } ],
		         	        "order": [[ 0, 'asc' ]]
		         	    } );
		         	 
		         	    t.on( 'order.dt search.dt', function () {
		         	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		         	            cell.innerHTML = i+1;
		         	        } );
		         	    } ).draw();
		         	           	
		         	  
		         	   t.buttons( 0, null ).containers().appendTo( '#btnhere' );
		         	   
		         	});
		         
         
         	
   
        
        </script>




</body>
</html>