<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<%@ page import="com.etiqa.model.agent.Agent"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- PLUGINS CSS -->
<link href="plugins/owl-carousel/owl.carousel.min.css" rel="stylesheet">
<link href="plugins/owl-carousel/owl.theme.min.css" rel="stylesheet">
<link href="plugins/owl-carousel/owl.transitions.min.css"
	rel="stylesheet">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="plugins/footable-3/css/footable.bootstrap.css"
	rel="stylesheet">
<link href="css/owl.css" rel="stylesheet">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-select.css" rel="stylesheet">
<link href="css/menu.css" rel="stylesheet">
<link href="plugins/datepicker/datepicker.min.css" rel="stylesheet">
<!--<link href="css/style.css" rel="stylesheet">-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!-- header -->
		<div class="header">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="visible-xs-block">
							<!-- Begin button sidebar left toggle -->
							<div class="btn-collapse-sidebar-left logo">
								<i class="fa fa-bars"></i>
							</div>
							<!-- /.btn-collapse-sidebar-left -->
							<!-- End button sidebar left toggle -->
						</div>
						<div class="logo">
							<img class="" src="images/logo.png" alt="logo">
						</div>
						<div class="title">Administration</div>
						<div class="clearfix"></div>
					</div>
					<div class="col-sm-6">
						<!-- BEGIN TOP NAV -->
						<div class="top-navbar">
							<div class="top-navbar-inner">

								<div class="top-nav-content">

									<!-- Begin Collapse menu nav -->
									<div class="collapse navbar-collapse" id="main-fixed-nav">
										<!-- Begin user session nav -->
										<ul class="nav-user navbar-right">
											<li class="dropdown"><a href="#fakelink"
												class="dropdown-toggle" data-toggle="dropdown"> <i
													class="fa fa-user"></i> Welcome <strong>Siti
														Amirah Umar</strong> <i class="fa fa-angle-down fa-lg"></i>
											</a>
												<ul
													class="dropdown-menu square primary margin-list-rounded with-triangle">
													<li><a href="security-setting.html">Security
															setting</a></li>
													<li class="divider"></li>
													<li><a href="logout.html">Log out</a></li>
												</ul></li>
										</ul>
										<!-- End user session nav -->
										<ul class="nav navbar-nav alert navbar-right">
											<!-- Begin nav notification -->
											<li class="dropdown"><a href="#fakelink"
												class="dropdown-toggle" data-toggle="dropdown"> <span
													class="badge badge-warning icon-count">7</span> <i
													class="fa fa-bell"></i>
											</a>
												<ul
													class="dropdown-menu square margin-list-rounded with-triangle">
													<li>
														<div class="nav-dropdown-heading">Notifications</div>
														<!-- /.nav-dropdown-heading -->
														<div class="nav-dropdown-content scroll-nav-dropdown">
															<ul>
																<li class="unread"><a href="#fakelink">
																		<div class="absolute-left-content icon-task progress">I</div>
																		Creating documentation <span class="small-caps">Completed
																			: Yesterday</span>
																</a></li>
																<li><a href="#fakelink">
																		<div class="absolute-left-content icon-task progress">G</div>
																		Eating sands <span class="small-caps">Deadline
																			: Tomorrow</span>
																</a></li>
																<li><a href="#fakelink">
																		<div class="absolute-left-content icon-task progress">I</div>
																		Sending payment <span class="small-caps">Deadline
																			: Next week</span>
																</a></li>
																<li class="unread"><a href="#fakelink">
																		<div class="absolute-left-content icon-task progress">G</div>
																		Sleeping under bridge <span class="small-caps">Completed
																			: Dec 31, 2013</span>
																</a></li>
																<li class="unread"><a href="#fakelink">
																		<div class="absolute-left-content icon-task progress">G</div>
																		Buying some cigarettes <span class="small-caps">Completed
																			: 2 days ago</span>
																</a></li>
															</ul>
														</div>
														<!-- /.nav-dropdown-content scroll-nav-dropdown -->
														<form action="notification.html">
															<button class="btn btn-warning btn-square btn-block">See
																all task</button>
														</form>
													</li>
												</ul></li>
											<!-- End nav notification -->
										</ul>
										<!-- Begin nav search form -->
										<div class="navbar-form navbar-right" role="search">
											<div class="form-group">
												<div id="custom-search-input">
													<div class="input-group col-md-12">
														<input type="text" class="  search-query form-control"
															placeholder="Search" /> <span class="input-group-btn">
															<button class="btn btn-danger" type="button">
																<span class=" glyphicon glyphicon-search"></span>
															</button>
														</span>
													</div>
												</div>
											</div>
										</div>
										<!-- End nav search form -->

									</div>
									<!-- /.navbar-collapse -->
									<!-- End Collapse menu nav -->
								</div>
								<!-- /.top-nav-content -->
							</div>
							<!-- /.top-navbar-inner -->
						</div>
						<!-- /.top-navbar -->
						<!-- END TOP NAV -->
						<div class="clearfix"></div>
					</div>

					<!-- search mobile view-->
					<div class="col-xs-11 visible-xs">
						<div class="navbar-collapse searching"
							id="bs-example-navbar-collapse-1">
							<form class="navbar-form" role="search">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Search..">
									<span class="input-group-btn">
										<button type="reset" class="btn btn-default">
											<span class="glyphicon glyphicon-remove"> <span
												class="sr-only">Close</span>
											</span>
										</button>
										<button type="submit" class="btn btn-default">

											<span class="glyphicon glyphicon-search"> <span
												class="sr-only">Search</span>
											</span>
										</button>
									</span>
								</div>
							</form>
						</div>
						<!-- /.navbar-collapse -->
					</div>
					<!-- search mobile view-->

				</div>
				<!-- /.row -->
			</div>
		</div>
		<!-- end header -->
		<!-- header second-->
		<div class="header-second">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 hidden-xs">
						<!-- Begin breadcrumb -->
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i></li>
							<li><a href="admin-system-user-role.html">View Profile</a></li>
						</ol>
						<!-- End breadcrumb -->
					</div>
					<div class="col-sm-6 hidden-xs">
						<div class="col-sm-8">
							<div class="text-right last-login">Your last login was on
								01 Feb 2016 10:01:01</div>
						</div>
						<div class="col-sm-4">
							<div class="language">
								<select class="selectpicker">
									<option value="United States">English</option>
									<option value="United Kingdom">Malay</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-xs-12 visible-xs-block">
						<div class="mobile-view">
							<ul class="nav-user navbar-right">
								<li class="dropdown"><a href="#fakelink"
									class="dropdown-toggle text-right" data-toggle="dropdown">
										<i class="fa fa-user"></i> Welcome <strong>Siti
											Amirah Umar</strong> <i class="fa fa-angle-down fa-lg"></i>
								</a>
									<ul
										class="dropdown-menu square primary margin-list-rounded with-triangle pull-right">
										<li><a href="security-setting.html">Security setting</a></li>
										<li class="divider"></li>
										<li><a href="logout.html">Log out</a></li>
									</ul></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- header second-->
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<div class="col-sm-3 col-md-2 fluid-menu">
						<div class="sidebar-left sidebar-nicescroller admin">
							<!-- desktop menu -->
							<ul class="sidebar-menu admin">
								<li><a href="admin-dashboard.html"> <span
										class="icon-sidebar icon dashboard"></span> Dashboard
								</a></li>
								<li><a href="#"> <span
										class="icon-sidebar icon services"></span> <i
										class="fa fa-angle-right chevron-icon-sidebar"></i> System
										Administration
								</a>
									<ul class="submenu">
										<li><a href="admin-system-user-role.html">User Roles
												&amp; Permission</a></li>
										<li><a href="admin-system-audit-log.html">Audit Log
												Maintenance</a></li>
										<li><a href="admin-system-audit-trail.html">Audit
												Trail/Log Report</a></li>
										<li><a href="admin-system-log-report.html">Integration
												Log Report</a></li>
									</ul></li>
								<li class="submenu"><a href="#"> <span
										class="icon-sidebar icon services"></span> <i
										class="fa fa-angle-right chevron-icon-sidebar"></i> Business
										Administration
								</a>
									<ul class="submenu">
										<li class=""><a href="#"> <i
												class="fa fa-angle-right chevron-icon-sidebar2"></i> Product
												Management
										</a>
											<ul class="submenu">
												<li><a href="#"> <i
														class="fa fa-angle-right chevron-icon-sidebar3"></i> Motor
														Insurance
												</a>
													<ul class="submenu">
														<li><a
															href="admin-business-product-management-motor-product-infomation.html">Product
																Information</a></li>
														<li><a
															href="admin-business-product-management-motor-product-rate.html">Product
																Rates</a></li>
														<li><a
															href="admin-business-product-management-motor-excess.html">Excess</a></li>
														<li><a
															href="admin-business-product-management-motor-loading.html">Loading</a></li>
														<li><a
															href="admin-business-product-management-motor-nvic.html">NVIC
																Listing</a></li>
														<li><a
															href="admin-business-product-management-motor-payment.html">Payment
																Method</a></li>
													</ul></li>
												<li><a href="#"> <i
														class="fa fa-angle-right chevron-icon-sidebar3"></i>
														Houseowner/Householder
												</a>
													<ul class="submenu visible">
														<li><a
															href="admin-business-product-management-house-product-infomation.html">Product
																Information</a></li>
														<li><a
															href="admin-business-product-management-house-product-rate.html">Product
																Rates</a></li>
														<li class="active selected"><a
															href="admin-business-product-management-house-payment.html">Payment
																Method</a></li>
													</ul></li>
												<li><a href="#"> <i
														class="fa fa-angle-right chevron-icon-sidebar3"></i> World
														Traveller Care
												</a>
													<ul class="submenu">
														<li><a
															href="admin-business-product-management-traveller-product-infomation.html">Product
																Information</a></li>
														<li><a
															href="admin-business-product-management-traveller-product-rate.html">Product
																Rates</a></li>
														<li><a
															href="admin-business-product-management-traveller-payment.html">Payment
																Method</a></li>
													</ul></li>
												<li><a href="#"> <i
														class="fa fa-angle-right chevron-icon-sidebar3"></i> Term
														Life
												</a>
													<ul class="submenu">
														<li><a
															href="admin-business-product-management-term-product-infomation.html">Product
																Information</a></li>
														<li><a
															href="admin-business-product-management-term-product-rate.html">Product
																Rates</a></li>
														<li><a
															href="admin-business-product-management-term-payment.html">Payment
																Method</a></li>
													</ul></li>
											</ul></li>
										<li><a href="admin-business-list-member.html">List of
												Customer</a></li>
										<li><a href="admin-business-segmentation.html">Segmentation</a></li>
										<li><a href="admin-business-campaign-analysis.html">Campaign
												Analysis Setup</a></li>
										<li><a href="admin-business-straight-through.html">Rules
												Management</a></li>
										<li><a href="admin-business-discount.html">Discount
												Management</a></li>
									</ul></li>
								<li><a href="#"> <span
										class="icon-sidebar icon services"></span> <i
										class="fa fa-angle-right chevron-icon-sidebar"></i> Agent
										Management
								</a>
									<ul class="submenu">
										<li><a href="agent-view-list-agent.html">List of
												Agent</a></li>
										<li><a href="agent-registration.html">Register Agent</a></li>
									</ul></li>
								<li class="active selected"><a href="#"> <span
										class="icon-sidebar icon services"></span> <i
										class="fa fa-angle-right chevron-icon-sidebar"></i> Cyber
										Agent
								</a>
									<ul class="submenu visible">
										<li class="active"><a href="agent-view-profile.html">View
												Profile</a></li>
										<li><a href="agent-list-of-perator.html">List of
												Operator</a></li>
										<li><a href="agent-operatorregistration.html">Register
												Operator</a></li>
									</ul></li>
								<li><a href="#"> <span
										class="icon-sidebar icon services"></span> <i
										class="fa fa-angle-right chevron-icon-sidebar"></i> Leads
										Management &amp; Campaign
								</a>
									<ul class="submenu">
										<li><a href="lead-sales-lead.html">Sales Leads Portal
												Registration</a></li>
										<li><a href="lead-sales-tools.html">Sales Tools</a></li>
										<li><a href="lead-manual-upload.html">Manual Upload</a></li>
										<li><a href="lead-abandonment-repository.html">Abandonment
												Repository</a></li>
										<li><a href="lead-campaign-banner.html">Campaign
												Banner</a></li>
										<li><a href="lead-data-download.html">Leads Data
												Download</a></li>
									</ul></li>
								<li><a href="#"> <span class="icon-sidebar icon policy"></span>
										<i class="fa fa-angle-right chevron-icon-sidebar"></i> Report
										&amp; Analytics
								</a>
									<ul class="submenu">
										<li><a href="admin-report-operator.html">Operator
												Performance</a></li>
										<li><a href="admin-report-transaction.html">Transactional
												Report</a></li>
									</ul></li>
							</ul>
						</div>
						<!-- /.sidebar-left -->
					</div>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Cyber Agent</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>View Profile</h4>
														</div>
													</div>
													<div class="content-inner">
														<div class="the-box">
															<div class="col-sm-12">
																<div class="title">
																	<div class="sub">
																		<h4>Agent Information Details</h4>
																	</div>
																</div>
															</div>
															<div class="gap gap-mini"></div>
															<div class="col-sm-6">
																<div class="form-horizontal info-meor">
																	<div class="form-group">
																		<label class="col-sm-3 control-label">Reference
																			No</label>
																		<div class="col-sm-9">
																			<p class="form-control-static">VS975765</p>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label">Agent
																			Code</label>
																		<div class="col-sm-9">
																			<p class="form-control-static">A46342</p>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label">Registration
																			Date</label>
																		<div class="col-sm-9">
																			<p class="form-control-static">01-Jan-2020</p>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label">Annual
																			Sales Target</label>
																		<div class="col-sm-9">
																			<p class="form-control-static">RM 2131,23</p>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="control-label col-sm-3">Product
																			Selection</label>
																		<div class="col-sm-9">
																			<p class="form-control-static">Motor Insurance /
																				Term Life Insurance</p>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-horizontal info-meor">
																	<div class="form-group">
																		<label class="col-sm-3 control-label">Agent
																			Category</label>
																		<div class="col-sm-9">
																			<p class="form-control-static">Company/Organisation</p>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label">Insurance
																			Type</label>
																		<div class="col-sm-9">
																			<p class="form-control-static">Insurance</p>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label">Status</label>
																		<div class="col-sm-9">
																			<p class="form-control-static">Permohonan Dilulus</p>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label">Agent
																			Alias Name</label>
																		<div class="col-sm-9">
																			<p class="form-control-static">
																				Samab <span class="right tooltips"> <a
																					id="tooltips-2"> <i
																						class="fa fa-question-circle fa-lg"></i>
																				</a>
																				</span>
																			</p>

																			<!-- Popover 2 hidden title -->
																			<div id="tooltips-2Title" style="display: none">
																				Agent referral ID is the link management solution to
																				shorten the product referral link<br> e.g.
																				http://www.etiqa.com.my/agentname<br>
																			</div>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label">Agent
																			Logo</label>
																		<div class="col-sm-9">
																			<div class="logo-upload">
																				<img src="images/default-image2.png">
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-horizontal">
																	<div class="form-group">
																		<label class="control-label">Product referral
																			URL :</label>
																	</div>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-horizontal">
																	<div class="form-group">
																		<label class="col-sm-3 control-label">Motor
																			Insurance</label>
																		<div class="col-sm-7">
																			<p class="form-control-static">http://www.etiqa.com.my/samad_saif/prod_id01</p>
																		</div>
																		<div class="col-sm-2">
																			<a href="#" class="btn btn-warning btn-xs">
																				Preview</a>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-horizontal">
																	<div class="form-group">
																		<label class="col-sm-3 control-label">Term
																			Life Insurance</label>
																		<div class="col-sm-7">
																			<p class="form-control-static">http://www.etiqa.com.my/samad_saif/prod_id01</p>
																		</div>
																		<div class="col-sm-2">
																			<a href="#" class="btn btn-warning btn-xs">
																				Preview</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<div class="the-box">
															<div class="col-sm-12">
																<div class="title">
																	<div class="sub">
																		<h4>Company Details</h4>
																	</div>
																</div>
															</div>
															<div class="gap gap-mini"></div>
															<div class="col-sm-6">
																<div class="form-horizontal info-meor">
																	<div class="form-group">
																		<label class="col-sm-3 control-label">Agent
																			Name</label>
																		<div class="col-sm-9">
																			<p class="form-control-static">Samad Saif
																				Enterprise</p>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label">Company
																			Registration No</label>
																		<div class="col-sm-9">
																			<p class="form-control-static">123122-x</p>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label">Phone No</label>
																		<div class="col-sm-9">
																			<p class="form-control-static">Office 033420292</p>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label">Email</label>
																		<div class="col-sm-9">
																			<p class="form-control-static">samad_saif@gmail.com</p>
																		</div>
																	</div>


																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-horizontal info-meor">
																	<div class="form-group">
																		<label class="col-sm-3 control-label">Address</label>
																		<div class="col-sm-9">
																			<div class="form-group">
																				<div class="col-sm-12">
																					<p class="form-control-static">
																						B-1-22 Blok Bouganvilla<br> 10 Boulevard<br>
																						Lebuh Raya Sprint PJU 6A<br> 47400 Petaling
																						Jaya<br> Selangor Malaysia
																					</p>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>

														<div class="the-box">
															<div class="col-sm-12">
																<div class="title">
																	<div class="sub">
																		<h4>Claim Notices</h4>
																	</div>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="table-responsive">
																	<table
																		class="table table-striped table-warning table-hover">
																		<thead>
																			<tr>
																				<th style="width: 30px;">No</th>
																				<th width="20%">Date</th>
																				<th>Item</th>
																			</tr>
																		</thead>
																		<tbody>
																			<tr>
																				<td>1</td>
																				<td>01-Jan-2020</td>
																				<td><a href="#">Claim Notice 5</a></td>
																			</tr>
																		</tbody>
																	</table>
																</div>
															</div>
														</div>

													</div>
													<!--content inner -->
												</div>
												<!--col-sm-12 -->

												<div class="col-sm-12">
													<div class="text-right">
														<a href="agent-view-list-agent-edit.html"
															class="btn btn-warning btn-sm"><i class="fa fa-edit"></i>
															Update</a>
													</div>
												</div>
												<!--col-sm-12 -->
											</div>
											<!--row -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.container -->

			</div>
			<!-- /.page-content -->
			<!-- BEGIN FOOTER -->
			<div class="footer">
				<footer>
					<div class="fluid-yellow">
						<div class="container">
							<div class="row">
								<div class="col-sm-9">
									<ul class="left gap">
										<li><a href="faq.html"><i class="fa fa-question"></i>FAQ</a></li>
										<li><a href="help.html"><i class="fa fa-medkit"></i>Help</a></li>
										<li><a href="contactus.html"><i
												class="fa fa-envelope"></i>Contact Us</a></li>
									</ul>
									<div class="clearfix"></div>
								</div>
								<div class="col-sm-3">
									<div class="text-right gap">
										<img class="" src="images/member-my.png" alt="logo">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="fluid-grey">
						<div class="container">
							<div class="row">
								<div class="col-sm-6">
									<div class="gap">&copy; 2016 Etiqa. All Rights Reserved</div>
								</div>
								<div class="col-sm-6">
									<ul class="right">
										<li><a href="terms.html">Terms</a></li>
										<li><a href="privacy.html">Privacy</a></li>
										<li><a href="disclaimer.html">Disclaimer</a></li>
									</ul>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</footer>
			</div>
			<!-- END FOOTER -->
		</div>
		<!-- /.wrapper -->
		<!-- END PAGE CONTENT -->



		<!-- BEGIN BACK TO TOP BUTTON -->
		<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
		<!-- END BACK TO TOP -->


		<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

		<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
		<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/bootstrap-select.min.js"></script>
		<script src="js/bootstrap-tabcollapse.js"></script>

		<script src="plugins/retina/retina.min.js"></script>
		<script src="plugins/nicescroll/jquery.nicescroll.js"></script>
		<script src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="plugins/backstretch/jquery.backstretch.min.js"></script>


		<!-- PLUGINS -->
		<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
		<script src="plugins/prettify/prettify.js"></script>
		<script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

		<script src="plugins/chosen/chosen.jquery.min.js"></script>
		<script src="plugins/icheck/icheck.min.js"></script>
		<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
		<script src="plugins/timepicker/bootstrap-timepicker.js"></script>
		<script src="plugins/mask/jquery.mask.min.js"></script>
		<script src="plugins/validator/bootstrapValidator.min.js"></script>
		<script src="plugins/datatable/js/jquery.dataTables.min.js"></script>
		<script src="plugins/datatable/js/bootstrap.datatable.js"></script>
		<script src="plugins/datatable/js/jquery.highlight.js"></script>
		<script src="plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
		<script src="plugins/summernote/summernote.min.js"></script>
		<script src="plugins/markdown/markdown.js"></script>
		<script src="plugins/markdown/to-markdown.js"></script>
		<script src="plugins/markdown/bootstrap-markdown.js"></script>
		<script src="plugins/slider/bootstrap-slider.js"></script>
		<script src="plugins/toastr/toastr.js"></script>
		<script src="plugins/newsticker/jquery.newsTicker.min.js"></script>
		<script src="plugins/placeholder/jquery.placeholder.js"></script>
		<script src="plugins/footable-3/js/footable.js"></script>

		<!-- KNOB JS -->
		<!--[if IE]>
        <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
		<script src="plugins/jquery-knob/jquery.knob.js"></script>
		<script src="plugins/jquery-knob/knob.js"></script>

		<!-- MAIN APPS JS -->
		<script src="js/apps.js"></script>
</body>
</html>