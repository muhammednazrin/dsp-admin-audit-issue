<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List"%>
<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="com.spring.VO.roadtaxVO.RoadTaxTxnReport"%>


<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="${pageContext.request.contextPath}/assets/css/menu.css"
	rel="stylesheet">

<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

.width10 {
	max-width: 1%;
}

.errorText {
	color: red
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="../../siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>

<style>
.table-responsive {
	overflow-x: inherit;
}
</style>

</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!-- header -->
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- end header -->
		<!-- header second-->
		<!-- header second-->
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menuRoadTax.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Roadtax Renewal</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-10">
													<div class="title">
														<div class="sub">
															<h4>Add New Rate</h4>
														</div>
													</div>
													<div class="content-inner">
														<div class="the-box">
															<form action="" method="post" id="my_form">
																<div class="col-sm-4">
																	<div class="col-sm-12">
																		<div class="pull-left form-horizontal">

																			<div class="form-group">
																				<label class="col-sm-5 control-label">Region:
																				</label>
																				<div class="col-sm-6">
																					<select id='region' name='region'
																						class="form-control">
																						<option value="">Please Select</option>
																						<option value="0">Peninsular</option>
																						<option value="1">Sabah and Sarawak</option>
																						<option value="3">Labuan</option>
																						<option value="2">Langkawi</option>
																					</select>
																				</div>

																			</div>
																			<div class="form-group">
																				<label class="col-sm-5 control-label">Car
																					Type: </label>
																				<div class="col-sm-6">
																					<select id='cartype' name='cartype'
																						class="form-control">
																						<option value="">Please Select</option>
																						<option value="Y">Saloon</option>
																						<option value="N">Non Saloon</option>
																					</select>
																				</div>

																			</div>

																		</div>
																	</div>
																</div>
																<div class="table-responsive table-custom">
																	<table class="table">
																		<thead class="thead-custom">
																			<tr>
																				<th>Engine Capacity (CC) (From)</th>
																				<th>Engine Capacity (CC) (To)</th>
																				<th>Base Rate (RM)</th>
																				<th>Progressive Rate (Per CC)</th>
																			</tr>
																		</thead>
																		<tbody id="addccregion">

																		</tbody>
																	</table>
																</div>
																<div class="gap gap-mini"></div>
																<div class="col-sm-12">
																	<div class="pull-right">
																		<button id="add_row" class="btn btn-default">Add
																			New Row</button>
																		<button type="submit" class="btn btn-default">Save</button>
																	</div>
																</div>
															</form>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->
		<!-- BEGIN FOOTER -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="../../pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
		<!-- END FOOTER -->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
	<script
		src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/bootstrap-select.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/bootstrap-tabcollapse.js"></script>

	<script
		src="${pageContext.request.contextPath}/assets/plugins/retina/retina.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/backstretch/jquery.backstretch.min.js"></script>


	<!-- PLUGINS -->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/prettify/prettify.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/icheck/icheck.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/mask/jquery.mask.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/summernote/summernote.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/to-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/slider/bootstrap-slider.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/toastr/toastr.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/footable-3/js/footable.js"></script>

	<!-- KNOB JS -->
	<!--[if IE]>
        <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/knob.js"></script>

	<!-- MAIN APPS JS -->
	<script src="${pageContext.request.contextPath}/assets/js/apps.js"></script>


	<script type="text/javascript">
		var fromrange_values = new Array();
		var torange_values = new Array();
		var baserate_values = new Array();
		var progressrate_values = new Array();
		
		$(function(){
		    var $select = $(".1-31");
		    for (i=1;i<=31;i++){
		        $select.append($('<option></option>').val(i).html(i))
		    }
		});
		
		
		$(document).ready(function() {
			  var i = 1;
			  $("#add_row").click(function() {
			//  $('tr').find('input').prop('disabled',true)
			    //$('#addr' + i).html("<td><input type='text' name='fromrange'  placeholder='From Range(cc)' class='form-control input-md'/></td><td><input type='text' name='torange'  placeholder='To Range(cc)' class='form-control input-md'/></td><td><input type='text' name='baserate' placeholder='Base Rate(RM)' class='form-control input-md'/></td><td><input type='text' name='progressiverate' placeholder='Progressive Rate(RM)' class='form-control input-md'/></td>");

			    $('#addccregion').append('<tr id="addr' + i + '"><td><input type="number" onkeypress="return isNumberKey(event)" name="fromrange"  placeholder="From Range(cc)" class="form-control input-md" required/></td><td><input type="text" name="torange"  placeholder="To Range(cc)" type="number" onkeypress="return isNumberKey(event)" class="form-control input-md" required/></td><td><input type="text" name="baserate" placeholder="Base Rate(RM)" type="number" onkeypress="return isNumberKey(event)" class="form-control input-md" required></td><td><input type="text" name="progressiverate" placeholder="Progressive Rate(RM)" type="number" onkeypress="return isNumberKey(event)"  class="form-control input-md" required/></td></tr>');
			    i++;
			  });
			  
			});
		
		$("#my_form").submit(function(e) {
		//function save_row(evt){
			e.preventDefault();
			var region = $("#region").val();
			var cartype = $("#cartype").val();
			var flag = 0;
			
			$("#addccregion tr").each(function(){
				 var fromrange=$(this).find('[name="fromrange"]').val();
				 var torange=$(this).find('[name="torange"]').val();
				 var baserate=$(this).find('[name="baserate"]').val();
				 var progressiverate=$(this).find('[name="progressiverate"]').val(); 
				 if(fromrange!='' && torange!='' && baserate!='' && progressiverate!='')
				 {
					 fromrange_values.push(fromrange);
					 torange_values.push(torange);
					 baserate_values.push(baserate);
					 progressrate_values.push(progressiverate);
				 }
				 else
					 {
					 
					 }
			});
				
			if(region!='' && cartype!= '')
			 {
				
				if(fromrange_values.length > 0){
			
		$.ajax({
			url : '${pageContext.request.contextPath}/addRateMaintenanceForRegion',
			type: "post",
			dataType: "json",
			beforeSend : function() {
				$('.loader').addClass("is-active");
			},
			data:{regionidvalue: region,
				cartypeidvalue: cartype,
				fromrangearray:fromrange_values,
				torangearray: torange_values,
				baseratearray: baserate_values,
				progressratearray: progressrate_values},
			success : function(data) {
				
				var status = data[0].status;
				$("#addccregion tr").remove();
				if(status=="1"){
					$('.loader').removeClass(
					"is-active");
					obj = data;
					alert("Added Successfully");
				}else{
					alert(status);
				}
			},
			complete : function() {
				$('.loader').removeClass(
				"is-active");
			},
			error : function(request, status, error) {
				alert(request.responseText);
				alert(error);
			}
			});
		
				}else{
					 alert("Please add data");
				}
		
			 }
			 else
				 {
				    alert("Please Select Region and Car Type");
				 }
		});
		
		
		function isNumberKey(evt){
		    var charCode = (evt.which) ? evt.which : event.keyCode
		    		if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57)))
		        return false;
		    return true;
		}
		
</script>

</body>
</html>