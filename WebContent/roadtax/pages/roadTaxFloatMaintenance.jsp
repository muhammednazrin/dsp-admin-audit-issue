<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%
	java.text.DateFormat df = new java.text.SimpleDateFormat("dd/MM/yyyy");
%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="${pageContext.request.contextPath}/assets/css/rtx-menu.css"
	rel="stylesheet">
rel="stylesheet">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

.ddwidth {
	max-width: 170px;
	margin: 0 auto;
}

.width10 {
	max-width: 1%;
}

.errorText {
	color: red
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="../../siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>

</head>

<body>
	<%
		DecimalFormat formatter = new DecimalFormat("#,###.00");

		if (request.getMethod().equals("POST")) {

			/* 		session.setAttribute("ProductType", "");
					session.setAttribute("Status", "");
					session.setAttribute("PolicyCertificateNo", "");
					session.setAttribute("NRIC", "");
					session.setAttribute("ReceiptNo", "");
					session.setAttribute("dateFrom", "");
					session.setAttribute("dateTo", "");
					session.setAttribute("DateFrom", "");
					session.setAttribute("DateTo", "");
					session.setAttribute("PlateNo", "");
					session.setAttribute("ProductEntity", "");
			*/
		}

		String textColor = "";
		String status = "";
		String transactionStatus = "";
		String lastStep = "";
		String nextStep = "";
		String reason = "";
		int i = 0;
		int rowLine = 0;
		String txtColor = "";
	%>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menuRoadTax.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Road Tax Renewal</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Float Maintenance</h4>
														</div>
													</div>
													<c:if test="${not empty errorMessages}">
														<div class="row">
															<div class="col-sm-12">
																<c:forEach items="${errorMessages}" var="element"
																	varStatus="theCount">
																	<ul>
																		<li class="errorText"><c:out value="${element}" /></li>
																	</ul>
																</c:forEach>
															</div>
														</div>
													</c:if>

													<!-- Start Form -->
													<form:form action="#" id="txnFloatForm"
														name="txnreportForm" method="POST">
														<input type="hidden" name="action" value="list" />
														<input type="hidden" name="startRow" value="0" />
														<input type="hidden" name="endRow" value="10" />

														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-6">
																	<div class="form-horizontal">

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Select
																				Entity</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="roadTaxStatus"
																					id="roadTaxStatus">
																					<option value="" selected>-View All-</option>
																					<option value="A">ETB</option>
																					<option value="B">EIB</option>
																				</select>
																			</div>
																		</div>
																	</div>
																</div>

																<div class="col-sm-12 text-right">
																	<button type="submit" class="btn btn-default"
																		name="SEARCH">Search</button>
																</div>
															</div>
														</div>

														<!-- END FORM -->
												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="row">
																<div class="col-xs-12 form-inline">
																	<div class="col-xs-6"></div>
																	<div class="col-xs-6">
																		<%-- <div class="row">
																			<c:if test="${not empty roadTaxTxnReportList}">
																				<div class="pull-right" id='btnhere'>
																					<a href="generateExcelTxnReport"><button
																							class="btn btn-warning btn-sm">
																							Export to XLS <i class="fa fa-download"
																								aria-hidden="true"></i>
																						</button></a>
																				</div>
																			</c:if>
																		</div> --%>
																	</div>
																</div>
															</div>
															<div class="table-responsive" id="MyTable">
																<table
																	class="table table-striped table-warning table-hover"
																	id="admin-datatable-second_admin">
																	<thead>
																		<tr>
																			<th>Float Replenished (RM)</th>
																			<th>Entity</th>
																			<th>Date and Time</th>
																			<th>Action</th>
																		</tr>
																	</thead>
																	<tbody>
																		<c:if test="${not empty roadTaxFloat}">
																			<c:forEach items="${roadTaxFloat}" var="element"
																				varStatus="theCount">
																				<tr>
																					<td align="left">&nbsp; <input type="text"
																						class="floatReplenishedVal"
																						name="floatReplenishedVal"
																						id="floatReplenishedVal" style="width: 82px"
																						maxlength="10" size="2" width="1" height="1"
																						value="${element.dispFloatAmount}" disabled="true" />

																						<input type="hidden" id="curFloatAmount"
																						name="curFloatAmount"
																						value="<c:out value="${element.dispCurrent}"/>" />

																					</td>

																					<td align="left">&nbsp;<c:out
																							value="${element.entity}" /> <input
																						type="hidden" id="entity" name="entity"
																						value="<c:out value="${element.entity}"/>" />

																					</td>

																					<td align="left">&nbsp;<c:out
																							value="${element.dispDateandtime}" />
																					</td>

																					<td align="left">
																						<!-- <body link="blue">
																			 			<p><a href="##">Edit</a></p>
																						</body> --> <input type="button"
																						class="btn btn-default edit_actamt" value="Edit"
																						id="edit_btn" /> &nbsp;&nbsp; <input
																						type="button"
																						class="btn btn-default updateFloatReplenished"
																						value="Save" id="saveFloat_btn" />






																					</td>
																				</tr>
																			</c:forEach>
																		</c:if>
																	</tbody>
																	<%-- 	</form:form> --%>
																</table>
															</div>


															<!-- 		<input type="button"
																class="btn btn-default saveFloatReplenishedTest"
																value="Save" id="saveFloat_btn" /> -->


															</form:form>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>
												<%-- </c:if> --%>
											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="../../pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>

	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->

	<script type="text/javascript">
		/* ------------------------------------------------------------------ */
		/* Start - Edit the Float Replenished amout and change the value Info */
		/*------------------------------------------------------------------  */
		$(document).ready(
				function() {
					$('.edit_actamt').click(
							function() {
								$(this).parents('tr').find(
										'#floatReplenishedVal').prop(
										'disabled', false);
							});
				});
		/* ------------------------------------------------------------------ */
		/* End - Edit the Float Replenished amout and change the value Info   */
		/*------------------------------------------------------------------ */

		/* ------------------------------------------------------------------ */
		/* Start - Update the Float Replenished amout Info                    */
		/*------------------------------------------------------------------ */

		$('.updateFloatReplenished')
				.click(
						function() {
							//successAlert('Success!', 'Actual Value Updated Successfully');

							var dd = $(this).val();
							var $tr = $(this).closest('tr');
							var myRow = $tr.index();
							//alert("myRow:" + myRow);

							var floatAmount = $tr.find('td:eq(0)')
									.find('#floatReplenishedVal').val();
							//alert("floatAmount:" + floatAmount);
							
							var curFloatAmount = $tr.find('td:eq(0)')
							.find('#curFloatAmount').val();
							//alert("curFloatAmount:" + curFloatAmount);						
				
							
							var entity = $tr.find('td:eq(1)').find('#entity')
									.val();
							//alert("entity:" + entity);

							$
									.ajax({
										type : "post",
										beforeSend : function() {
											$('.loader').addClass("is-active");
										},
										url : '${pageContext.request.contextPath}/updateFloatReplenishedValue',
										data : {
											floatAmount : floatAmount,
											curFloatAmount : curFloatAmount,
											entity : entity
										},
										success : function(data) {
											obj = JSON.parse(data);
											if (obj.result = '1') {

												//successAlert('Success!','FloatReplenished Value Updated Successfully');
												//alert("Sucess");
												window.location.href = '${pageContext.request.contextPath}/roadTaxFloatMaintenance';
												//$tr.find('td:eq(7)').find('#saveConsign_btn').hide();
											}
										},
										complete : function() {
											$('.loader').removeClass(
													"is-active");
										},
										error : function(request, status, error) {
											alert(request.responseText);
										}
									});
						});

		/* ------------------------------------------------------------------ */
		/* End - Update the Float Replenished amout Info                     */
		/*------------------------------------------------------------------ */

		/* 	
		 $('.saveFloatReplenishedTest').click(function(){
		
		
		 var floatVal = $("#floatReplenishedValhidden").val(); 
		 alert(floatVal);
		 alert($("[type='hidden']", this).val());
		
		 var currentId = $(this).attr('id');
		 var hiddenval = $('#'+currentId).find('input[type=hidden]').val();
		 alert(hiddenval); 
		 }) */

		/* ------------------------------------------------------------------*/
		/* Start - Column sorting with table header Info                     */
		/*-------------------------------------------------------------------*/

		$(document)
				.ready(
						function() {
							var t = $('#admin-datatable-second_admin')
									.DataTable(
											{
												dom : 'Bfrtip',
												buttons : [
														{
															extend : 'pageLength',
															className : 'btn btn-secondary btn-sm active'
														},
														{
															extend : 'print',
															className : 'btn btn-warning btn-sm',
															title : 'Etiqa Transaction Report',
															text : 'Print <i class="fa fa-print" aria-hidden="true"></i>'
														}
												//,
												//{
												// extend: 'excel',
												//filename: 'Etiqa Transaction Report',
												// className: 'btn btn-warning btn-sm',
												//text:'Export to XLS <i class="fa fa-download" aria-hidden="true"></i>'
												// }
												],
												"searching" : false,
												"columnDefs" : [ {
													"searchable" : false,
													"orderable" : false,
													"targets" : [ 0, 1, 2, 3,
															4, 5, 6, 7 ],
													"bSortable" : false
												} ],
												"order" : [ [ 0, 'asc' ] ]
											});

							t.on('order.dt search.dt', function() {
								t.column(0, {
									search : 'applied',
									order : 'applied'
								}).nodes().each(function(cell, i) {
									cell.innerHTML = i + 1;
								});
							}).draw();

							t.buttons(0, null).containers()
									.appendTo('#btnhere');
						});
		/* ------------------------------------------------------------------*/
		/* End - Column sorting with table header Info                       */
		/*-------------------------------------------------------------------*/
	</script>

</body>
</html>