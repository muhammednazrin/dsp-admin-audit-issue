<%@ page import="java.util.List"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.text.DecimalFormat"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%
	java.text.DateFormat df = new java.text.SimpleDateFormat("dd/MM/yyyy");
	//DecimalFormat formatter = new DecimalFormat("#,###.00");
	//NumberFormat formatter = new DecimalFormat("#0,000.00");
%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="${pageContext.request.contextPath}/roadtax/css/menu.css"
	rel="stylesheet">

<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

.width10 {
	max-width: 1%;
}

.errorText {
	color: red
}
</style>


<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="../../siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}

#spinner {
	position: absolute;
	top: 0;
	left: 0;
	bottom: 0;
	right: 0;
	z-index: 99999;
}

#spinner span {
	position: fixed;
	top: 50%;
	left: 50%;
	display: block;
	margin-top: -25px;
	margin-left: -25px;
	width: 50px;
	height: 50px;
	background:
		url(${pageContext.request.contextPath}/roadtax/images/spinner.gif)
		center no-repeat #f6f6f6;
	border-radius: 25px;
	-webkit-border-radius: 25px;
	-moz-border-radius: 25px;
	-o-border-radius: 25px;
	-khtml-border-radius: 25px;
	-webkit-box-shadow: 0 0 5px rgba(0, 0, 0, 0.25);
	-moz-box-shadow: 0 0 5px rgba(0, 0, 0, 0.25);
	box-shadow: 0 0 5px rgba(0, 0, 0, 0.25);
}
</style>

</head>

<body>
	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->

	<div id="spinner">
		<span></span>
	</div>
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menuRoadTax.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Road Tax Renewal</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-md-12 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Printing and Delivery</h4>
														</div>
													</div>

													<c:if test="${not empty errorMessages}">
														<div class="row">
															<div class="col-sm-12">
																<c:forEach items="${errorMessages}" var="element"
																	varStatus="theCount">
																	<ul>
																		<li class="errorText"><c:out value="${element}" /></li>
																	</ul>
																</c:forEach>
															</div>
														</div>
													</c:if>

													<!-- Start Form -->
													<!-- END FORM -->
												</div>
												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<!-- <form class="form-inline"> -->
															<form:form modelAttribute="customer"
																action="${pageContext.request.contextPath}/updatePrinting"
																id="saveRemarkForm" name="saveRemarkForm" method="POST">
															</form:form>

															<div class="form-group">
																<c:choose>
																	<c:when test="${callMethod eq 'MI'}">
																		<h3 class="txt-custom">Motor Insurance(EIB)</h3>
																	</c:when>
																	<c:otherwise>
																		<h3 class="txt-custom">Motor Takaful (ETB)</h3>
																	</c:otherwise>
																</c:choose>
															</div>

															<div class="row">
																<div class="col-xs-12 form-inline">
																	<div class="col-xs-6"></div>
																	<div class="col-xs-6">
																		<div class="row">
																			<div class="pull-right custom-lbl">
																				<label>Road Tax to be Processed:&nbsp; <c:choose>
																						<c:when test="${callMethod eq 'MI'}">
																							<c:out value="${countMI}" />
																						</c:when>
																						<c:otherwise>
																							<c:out value="${countMT}" />
																						</c:otherwise>
																					</c:choose>
																				</label>
																				<!-- <label>Road Tax to Deliver :0</label>  -->
																				<label>Float Amount&nbsp;(RM):&nbsp;<c:out
																						value="${currentFloatValue}" /></label>
																				<c:if test="${not empty rdtaxDeliveryAddList}">
																					<c:forEach items="${rdtaxDeliveryAddList}"
																						var="element">
																					</c:forEach>
																				</c:if>
																			</div>
																		</div>
																	</div>
																	<div class="col-xs-6"></div>
																	<div class="col-xs-6">
																		<div class="row">
																			<c:if test="${not empty rdtaxDeliveryAddList}">
																				<div class="pull-right" id='btnhere'>
																					<!--  <button class="btn btn-warning btn-sm">
																					Print <i class="fa fa-print" aria-hidden="true"></i>
																				</button>-->
																				</div>
																			</c:if>
																		</div>
																	</div>
																</div>
															</div>
															<div class="gap gap-mini"></div>
															<div class="table-responsive">
																<table id="admin-datatable-second_admin"
																	class="table table-striped table-warning table-hover">
																	<thead class="thead-custom" align="left">
																		<tr>
																			<th style="width: 1px;">No</th>
																			<th style="width: 115px;">Insured Info</th>
																			<th style="width: 92px;">Road Tax Period (Month)</th>
																			<th style="width: 136px;">Recipient Details</th>
																			<th style="width: 73px;">Transaction Date</th>
																			<th style="width: 1px;">Aging (Days)</th>
																			<th style="width: 96px;">Road Tax Amount (RM)</th>
																			<th style="width: 73px;">Road Tax Printing</th>
																			<th style="width: 111px;">Consignment Notes
																				Tracking No.</th>
																		</tr>
																	</thead>
																	<tbody>
																		<c:if test="${empty floatAmoutCheck}">
																			<c:if test="${not empty rdtaxDeliveryAddList}">
																				<c:forEach items="${rdtaxDeliveryAddList}"
																					var="element">
																					<tr>
																						<td align="center" class="width10"></td>
																						<td align="left"><B>Name:&nbsp;</B> <c:out
																								value="${element.customerName}" /><br> <B>ID
																								No:&nbsp;</B> <c:out
																								value="${element.customerNricId}" /><br> <B>Vehicle
																								Reg.No:&nbsp;</B> <c:out value="${element.regNo}" /><br>
																							<B>Policy No :&nbsp;</B> <c:out
																								value="${element.mainPolicyNo}" /><br> <B>JPJ
																								Status:&nbsp;</B> <c:choose>
																								<c:when test="${empty element.message}">
																								N/A
																							</c:when>
																								<c:otherwise>
																									<c:out value="${element.message}" />
																								</c:otherwise>
																							</c:choose></td>

																						<td class="width10" align="center">&nbsp;<c:out
																								value="${element.period}" /></td>
																						<td align="left"><B>Name:&nbsp;</B> <c:out
																								value="${element.deliveryName}" /><br> <B>Address:&nbsp;</B>
																							<c:out value="${element.address1}" /><br> <c:out
																								value="${element.address2}" /><br> <c:out
																								value="${element.address3}" /><br> <B>Postcode:&nbsp;</B>
																							<c:out value="${element.postcode}" /><br> <B>State:&nbsp;</B>
																							<c:out value="${element.state}" /><br> <B>Phone
																								No:&nbsp;</B> <c:out value="${element.mobile}" /><br>

																							<B>Delivery:</B>&nbsp;NDD <%-- <c:choose>
																							<c:when test="${element.deliveryTime eq 'S'}">
																								<B>Delivery:</B>&nbsp;SDD
																				</c:when>
																							<c:otherwise>
																								<B>Delivery:</B>&nbsp;NDD
																				</c:otherwise>
																						</c:choose> --%></td>

																						<td class="width10" align="center">&nbsp;<c:out
																								value="${element.dateAndTime}" /></td>
																						<td align="center"><c:out
																								value="${element.aging}" /></td>

																						<td align="left"><B>Collected:&nbsp;</B> <%-- <c:out value="${element.roadTaxAmount}" /><br>  --%>
																							<fmt:formatNumber type="number"
																								pattern="#,###.00"
																								value="${element.roadTaxAmount}" /><br> <B>Actual:&nbsp;</B>
																							<input id="target" type="text"
																							class="actualValue" name="actVal" id="actVal"
																							style="width: 42px" maxlength="6" size="2"
																							width="1" height="1"
																							value="${element.actualAmount}" disabled="true" /><br>
																							<br> <input type="button"
																							class="btn btn-default edit_actamt" value="Edit"
																							id="edit_btn" /> <input type="hidden" id="qqid"
																							name="qqid"
																							value="<c:out value="${element.qqId_Delivery}"/>" />
																							<!-- <input
																						type="button" class="btn btn-default save_actamt"
																						style="display: none" value="Save" name="save_btn"
																						id="save_btn"><br><br> --> <br> <br> <a
																							href="downloadFile?policyNo=<c:out value="${element.mainPolicyNo}" />&term=TaxInvoiceForm"
																							target="_blank" id="TaxInvoiceForm">Download
																								Tax Invoice</a></td>

																						<td><select class="Options">
																								<option value="A">Please Select</option>
																								<option value="B">Printed</option>
																								<option value="C">Unable to Print</option>
																						</select> <br> <br> <input type="hidden"
																							id="qqId" name="qqId"
																							value="<c:out value="${element.qqId_Delivery}"/>" />
																							<input type="hidden" id="dspQqId" name="dspQqId"
																							value="<c:out value="${element.dspQqId}"/>" /> <input
																							type="hidden" id="mainPolicyNo"
																							name="mainPolicyNo"
																							value="<c:out value="${element.mainPolicyNo}"/>" />
																							<input type="hidden" id="rtpf" name="rtpf"
																							value="<c:out value="${element.rtpf}"/>" /> <input
																							type="hidden" id="rtpfgst" name="rtpfgst"
																							value="<c:out value="${element.rtpfgst}"/>" /> <input
																							type="hidden" id="rtprf" name="rtprf"
																							value="<c:out value="${element.rtprf}"/>" /> <input
																							type="hidden" id="rtprfgst" name="rtprfgst"
																							value="<c:out value="${element.rtprfgst}"/>" />
																							<input type="hidden" id="rtdf" name="rtdf"
																							value="<c:out value="${element.rtdf}"/>" /> <input
																							type="hidden" id="rtdfgst" name="rtdfgst"
																							value="<c:out value="${element.rtdfgst}"/>" /> <input
																							type="hidden" id="callMethod" name="callMethod"
																							value="<c:out value="${callMethod}"/>" /> <input
																							type="hidden" id="address1" name="address1"
																							value="<c:out value="${element.address1}"/>" />
																							<input type="hidden" id="address2"
																							name="address2"
																							value="<c:out value="${element.address2}"/>" />
																							<input type="hidden" id="address3"
																							name="address3"
																							value="<c:out value="${element.address3}"/>" />

																							<input type="hidden" id="postcode"
																							name="postcode"
																							value="<c:out value="${element.postcode}"/>" />

																							<input type="hidden" id="deliveryTime"
																							name="deliveryTime"
																							value="<c:out value="${element.deliveryTime}"/>" />
																							<input type="hidden" id="expDeliveryDtSDD"
																							name="expDeliveryDtSDD"
																							value="<c:out value="${element.dateAndTime}"/>" />

																							<input type="hidden" id="expDeliveryDtNDD"
																							name="expDeliveryDtNDD"
																							value="<c:out value="${element.expDeliveryDtNDD}"/>" />
																							<input type="hidden" id="totalTax"
																							name="totalTax"
																							value="<c:out value="${element.totalTax}"/>" />

																							<input type="button"
																							class="btn btn-default saveConsign"
																							style="display: none" value="Save"
																							id="saveConsign_btn" />

																							<div class="form-group" id="remark"
																								style="display: none; margin-left: -15px">
																								<label class="col-sm-3 control-label">Remarks:</label>
																							</div> <br> <select class="ddwidth" id="delivery"
																							style="display: none">
																								<option value="A" selected="selected">Refer
																									to JPJ</option>
																								<!-- <option value="A">Remarks</option>  -->
																								<!-- <option value="B">Unsuccessful due to invalid cover period</option>
																				<option value="C">Unsuccessful due to pending summons payment</option>
																				<option value="D">Unsuccessful due to vehicle being blacklisted</option>  -->
																						</select> <br> <br> <input type="button"
																							class="btn btn-default refundStatus"
																							style="display: none" value="Save"
																							id="unable_btn" /></td>

																						<td><span class="conNo" id="conNo"></span> <br>
																							<!-- <td> <label id="conNo"> </label> <br> --> <!-- <a href="downloadConsignment?Id=Chandra" target="_blank" id="ePolicyInformationPageLink"><span id="conNo"></span></a> 	 -->
																							<!--  <a href="downloadConsignment?Id='<span id="conNo"></span> View Member</a>  -->

																							<input type="button"
																							class="btn btn-default printStatus"
																							style="display: none" value="Print"
																							id="printConsign_btn" /></td>

																					</tr>
																				</c:forEach>
																			</c:if>
																		</c:if>


																	</tbody>
																</table>
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>
												<%-- </c:if> --%>
											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="../../pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%-- <jsp:include page="siteFooter.jsp" /> --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->
	<script
		src="${pageContext.request.contextPath}/assets/js/jquery.min.bk.js"
		type="text/javascript"></script>
	<%--  <script src="${pageContext.request.contextPath}/assets/jAlert/jAlert.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/jAlert/jAlert-functions.min.js"></script>  --%>
	<script src="${pageContext.request.contextPath}/assets/js/bootstrap.js"
		type="text/javascript"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/retina/retina.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/backstretch/jquery.backstretch.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/apps.js"></script>
	<!-- PLUGINS -->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/prettify/prettify.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
	<!-- <script src="assets/plugins/chosen/chosen.jquery.min.js"></script> -->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/icheck/icheck.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/mask/jquery.mask.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/summernote/summernote.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/to-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/slider/bootstrap-slider.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/toastr/toastr.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/assets/js/shieldui-all.min.js"></script>
	<!--  Table Export -->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/knob.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/dataTables.bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/dataTables.buttons.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/jszip.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/pdfmake.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/vfs_fonts.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.html5.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.print.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.colVis.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/toastr.min.js"></script>


	<script type="text/javascript">
		/* ------------------------------------------------------------------ */
		/* Start - Edit the Actual amout and change the value Info            */
		/*------------------------------------------------------------------  */

		$(document).ready(
				
				
				function() {
					
					$('#spinner').hide();
					
					$('.edit_actamt').click(
							function() {

								var $tr = $(this).closest('tr');
								var myRow = $tr.index();

								var amt = $(this).closest('tr')
										.find('td:eq(6)').find('input').val();
								//alert(amt);

								//$tr.find('td:eq(6)').find('#save_btn').show();
								$tr.find('td:eq(6)').find('#edit_btn').hide();

								$(this).parents('tr').find('#target').prop(
										'disabled', false);

								// $('#target').attr('readonly', false);
								//$("#target").prop("disabled",false);
								// $('#target').click(function() {}		
							});
				});
		/* ------------------------------------------------------------------ */
		/* End - Edit the Actual amout and change the value Info             */
		/*------------------------------------------------------------------ */

		/* ------------------------------------------------------------------ */
		/* Start - Save Consignment               		     			      */
		/*------------------------------------------------------------------- */

		$('.saveConsign').click(
				function() {
					//successAlert('Success!', 'Actual Value Updated Successfully');

					var $tr = $(this).closest('tr');
					var myRow = $tr.index();
					//console.log($tr);
					//console.log(myRow);
					//var no = "EC81000001" + myRow + "MY";

					// Save 
					var qqId = $tr.find('td:eq(7)').find('#qqId').val();
					var dspQqId = $tr.find('td:eq(7)').find('#dspQqId').val();
					var mainPolicyNo = $tr.find('td:eq(7)').find(
							'#mainPolicyNo').val();
					var callMethod = $tr.find('td:eq(7)').find('#callMethod')
							.val();
					
					var address1 = $tr.find('td:eq(7)').find('#address1').val();
					var address2 = $tr.find('td:eq(7)').find('#address2').val();
					var address3 = $tr.find('td:eq(7)').find('#address3').val();
					var postcode = $tr.find('td:eq(7)').find('#postcode').val();
					
					
					
					if ( address1 != "" && address2 !="" && address3 != "")
						var address = address1+','+address2+','+address3;
					else if ( address1 != "" && address2 != "")
						var address = address1+','+address2;
					else
						var address = address1;
					
					var address = address+', postcode: '+postcode;
					
					var deliveryTime = $tr.find('td:eq(7)').find('#deliveryTime').val();
					var expDeliveryDtSDD = $tr.find('td:eq(7)').find('#expDeliveryDtSDD').val();
					var expDeliveryDtNDD = $tr.find('td:eq(7)').find('#expDeliveryDtNDD').val();
					
					
					$('#spinner').show();
		$('#spinner').fadeIn();
					$.ajax({
						type : "post",
						beforeSend : function() {
							$('.loader').addClass("is-active");
						},
						url : '${pageContext.request.contextPath}/saveConsign',
						data : {
							qqId : qqId,
							dspQqId : dspQqId,
							mainPolicyNo : mainPolicyNo,
							callMethod : callMethod,
							address : address,
							deliveryTime : deliveryTime,
							expDeliveryDtSDD : expDeliveryDtSDD,
							expDeliveryDtNDD : expDeliveryDtNDD
						},
						success : function(data) {
							obj = JSON.parse(data);
							//alert("obj.result:"+obj.result);
							if (obj.result != 'Error') {
								//var no1 = obj.result;
								//console.log("Hello");
								//successAlert('Success!', 'Actual Value Updated Successfully');
								//alert("Sucess");
								//window.location.href  = '${pageContext.request.contextPath}/updateActualAmount';
								//window.location('${pageContext.request.contextPath}/updateActualAmount/<c:out value="${tranDetail.mainPolicyNo}" />');
								$tr.find('td:eq(7)').find('#saveConsign_btn')
										.hide();
								//$tr.find('td:eq(8)').find('#printConsign_btn').css('visibility', 'visible');
								$tr.find('td:eq(8)').find('#conNo').text(
										obj.result);

								$tr.find('td:eq(8)').find('#printConsign_btn')
										.show();
								
		
								//successAlert('Email Successfully Sent');
								//alert('Email Successfully Sent');
								
								$('#spinner').delay(100).fadeOut();
		
							}
							else {
								//errorAlert('Could Not Send Email');
								//alert('Could Not Send Email');
								alert("Consignment numbers has been fully used. \n Please upload new set of numbers");
								//window.location.href = '${pageContext.request.contextPath}/updatePrinting?callMethod='+ callMethod;
								
								$('#spinner').delay(100).fadeOut();
							}
						},
						complete : function() {
							$('.loader').removeClass("is-active");
						},
						error : function(request, status, error) {
							alert(request.responseText);
						}
					});
				});
		/* ------------------------------------------------------------------ */
		/* End - Save Consignment               		     			      */
		/*------------------------------------------------------------------- */

		/* ------------------------------------------------------------------ */
		/* Start - Print Consign and Update the print status Info             */
		/*------------------------------------------------------------------- */

		$('.printStatus')
				.click(
						function() {

							//alert('printStatus');
							/* 	if (confirm("Are you sure!") == true) {
									alert('Hi'); 
									confirm(function(e,btn){
										e.preventDefault();
										successAlert('Confirmed!'); */

							if (confirm("This record will be remove from the list. \nYou can find this record in 'Road Tax Report' submenu!")) {

								var dd = $(this).val();
								var $tr = $(this).closest('tr');
								var myRow = $tr.index();
								//console.log(myRow);

								// upable to print 
								var qqId = $tr.find('td:eq(7)').find('#qqId')
										.val();
								var dspQqId = $tr.find('td:eq(7)').find(
										'#dspQqId').val();
								var mainPolicyNo = $tr.find('td:eq(7)').find(
										'#mainPolicyNo').val();
								var callMethod = $tr.find('td:eq(7)').find(
										'#callMethod').val();
								var remarks = "Printed";

								var actVal = 0.0;
								var rtpf = 0.0;
								var rtpfgst = 0.0;
								var rtprf = 0.0;
								var rtprfgst = 0.0;
								var rtdf = 0.0;
								var rtdfgst = 0.0;
								var totalTax = 0.0;
								var totGST = 0.0;

								actVal = $tr.find('td:eq(6)').find(
										'.actualValue').val();
								rtpf = $tr.find('td:eq(7)').find('#rtpf').val();
								rtpfgst = $tr.find('td:eq(7)').find('#rtpfgst')
										.val();
								rtprf = $tr.find('td:eq(7)').find('#rtprf')
										.val();
								rtprfgst = $tr.find('td:eq(7)').find(
										'#rtprfgst').val();								
								rtdf = $tr.find('td:eq(7)').find('#rtdf').val();
								rtdfgst = $tr.find('td:eq(7)').find('#rtdfgst')
										.val();
										totalTax== $tr.find('td:eq(7)').find('#totalTax').val();
										
								console.log(rtpf+'rtpf');
								console.log(rtpfgst+'rtpfgst');
								console.log(rtprf+'rtprf');
								console.log(rtprfgst+'rtpf');
								console.log(rtdf+'rtdf');
								console.log(rtdfgst+'rtdfgst');
								console.log(totalTax+'totalTax');
								//alert(actVal+' '+rtpf+' '+rtpfgst+' '+rtprf+' '+rtprfgst+' '+rtdf+' '+rtdfgst);				
								/* totGST = parseFloat(rtpf) + parseFloat(rtpfgst)
										+ parseFloat(rtprf)
										+ parseFloat(rtprfgst) + parseFloat(rtdf) + parseFloat(rtdfgst); */
									totGST =	parseFloat(rtpf) +parseFloat(rtprf) + parseFloat(rtdf) + parseFloat(totalTax);
									console.log(totGST+'totGST');
								//alert(totGST);
								//var no="EC81000001"+myRow+"MY";
								//var Cosignment = "sample-consignment"+".pdf";

								//var Cosignment = $tr.find('td:eq(8)').find('#conNo').val();
								//var Cosignment = $('#conNo').html();
								//alert("Cosignment No > "+Cosignment); 
								
							/* 	var Cosignment = $('span#conNo').html();
								alert("Cosignment11 > "+Cosignment); */
								
								/* var cons= $tr.find('td:eq(8)').find('span#conNo').val();
								alert("cnsignment22 "+cons); */
								
								// working all the cases
								var Cosignment = $(this).closest('td').find('span#conNo').text();
								//alert("Cosignment No > "+Cosignment); 
								
								//var id = $(this).closest('td').find('span.customerId').text();
								//<span class="customerId">1</span>
								
								//var spans = $(this).closest("tr").find("span");
								//alert("spans No > "+spans);
								
								//var Cosignment1 = spans.eq(8).text();
								//alert("Cosignment1 No > "+Cosignment1);

								$.ajax({
											type : "post",
											beforeSend : function() {
												$('.loader').addClass(
														"is-active");
											},
											url : '${pageContext.request.contextPath}/printStatus',
											data : {
												qqId : qqId,
												dspQqId : dspQqId,
												mainPolicyNo : mainPolicyNo,
												remarks : remarks,
												actVal : actVal,
												totGST : totGST,
												callMethod : callMethod
											},
											success : function(data) {
												obj = JSON.parse(data);
												//alert(obj);
												//alert("Result:"+obj.result);

												if (obj.result == '1') {

													//alert("before consingment");
													//window.open("${pageContext.request.contextPath}/roadtax/images/sample-consignment.pdf", '_blank')
													//window.location.href  = '${pageContext.request.contextPath}/roadtax/images/sample-consignment.pdf';
													//window.open('https://support.wwf.org.uk/earth_hour/index.php?type=individual','_blank');							
													//window.open('${pageContext.request.contextPath}/roadtax/images/sample-consignment.pdf', '_blank');

													//console.log("Hello");
													//successAlert('Success!', 'Actual Value Updated Successfully');
													//alert("Sucess");
													//window.location.href  = '${pageContext.request.contextPath}/updatePrinting';							

													//window.open('${pageContext.request.contextPath}/downloadConsignment', '_blank');	
													window.location.href = '${pageContext.request.contextPath}/updatePrinting?callMethod='+ callMethod;
													$('#spinner').fadeIn(3000);	
													//alert("after success");
													//window.open('${pageContext.request.contextPath}/roadtax/images/sample-consignment.pdf', '_blank');

													//window.location.href  = '${pageContext.request.contextPath}/roadtax/pages/printingUpdate.jsp';
													//window.location('${pageContext.request.contextPath}/updateActualAmount/<c:out value="${tranDetail.mainPolicyNo}" />');
												}
											},
											complete : function() {
												$('.loader').removeClass(
														"is-active");
											},
											error : function(request, status,
													error) {
												alert(request.responseText);
											}
										});

								// second Ajax call for downloading Consignment
								//window.location.href = '${pageContext.request.contextPath}/updatePrinting?callMethod='+ callMethod;
								//$('#spinner').delay(10000).fadeOut();
							//commenting line as per request starting 		
								//window.location.href = '${pageContext.request.contextPath}/downloadConsignment?ConsignmentId='+Cosignment;
								//commenting line as per request ending	
								//window.open('${pageContext.request.contextPath}/images/sample-consignment.pdf','_blank');
								// window.open('${pageContext.request.contextPath}/roadtax/images/'+obj.result+'.pdf','_blank');
								//window.open('/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs/'+obj.result+'.pdf','_blank');

							}
							/*  else {
								// Do nothing!
								alert("Else");
								}
							 */

							/* 		 }, function(e,btn){
										e.preventDefault();
										errorAlert('Denied!');
										alert("cancel");
									}); 
								}  */

						});

		/* ------------------------------------------------------------------ */
		/* End - Print Consign and Update the print status Info             */
		/*------------------------------------------------------------------- */

		/* ------------------------------------------------------------------ */
		/* Start - Update Refund status and Remarks		     			      */
		/*------------------------------------------------------------------- */
		$('.refundStatus')
				.click(
						function() {

							if (confirm("This record will be remove from the list. \nYou can find this record in 'Road Tax Report' submenu!")) {

								var dd = $(this).val();
								var $tr = $(this).closest('tr');
								var myRow = $tr.index();
								//console.log(myRow);

								// Unable to print 
								var qqId = $tr.find('td:eq(7)').find('#qqId')
										.val();
								var dspQqId = $tr.find('td:eq(7)').find(
										'#dspQqId').val();
								var mainPolicyNo = $tr.find('td:eq(7)').find(
										'#mainPolicyNo').val();
								var uPrint = $tr.find('td:eq(7)').find(
										'#delivery').val();
								var callMethod = $tr.find('td:eq(7)').find(
										'#callMethod').val();

								if (uPrint == "A") {
									var remarks = "Unable to Print";
								}

								$
										.ajax({
											type : "post",
											beforeSend : function() {
												$('.loader').addClass(
														"is-active");
											},
											url : '${pageContext.request.contextPath}/callRefundStatus',
											data : {
												qqId : qqId,
												dspQqId : dspQqId,
												mainPolicyNo : mainPolicyNo,
												remarks : remarks
											},
											success : function(data) {
												obj = JSON.parse(data);
												//if (obj.result == 'Done') {
												if (obj.result > 0 ) {
													//console.log("Hello");
													//successAlert('Success!', 'Actual Value Updated Successfully');
													//alert("Success");
													//successAlert('Email Successfully Sent');
													//alert('Email Successfully Sent');

													$tr.find('td:eq(7)').find(
															'#unable_btn')
															.hide();
													window.location.href = '${pageContext.request.contextPath}/updatePrinting?callMethod='
															+ callMethod;
													//window.location('${pageContext.request.contextPath}/updatePrinting/<c:out value="${callMethod}" />');
												}
												else {
													//errorAlert('Could Not Send Email');
													//alert('Could Not Send Email');
												}
												
											},
											complete : function() {
												$('.loader').removeClass(
														"is-active");
											},
											error : function(request, status,
													error) {
												alert(request.responseText);
											}
										});
							}
						});
		/* ------------------------------------------------------------------ */
		/* Start - Update Refund status and Remarks		     			      */
		/*------------------------------------------------------------------- */

		//var x="";
		$('.Options').change(function() {
			var unprint = $(this).val();
			var $tr = $(this).closest('tr');
			var myRow = $tr.index();

			if (unprint == "A") {
				$tr.find('td:eq(8) ').text("");
				$tr.find('td:eq(7)').find('#delivery').hide();
				$tr.find('td:eq(7)').find('#remark').hide();
				$tr.find('td:eq(7)').find('#save_btn').hide();
				$tr.find('td:eq(7)').find('#saveConsign_btn').hide();
				$tr.find('td:eq(7)').find('#unable_btn').hide();
			}

			if (unprint == "B") {
				$tr.find('td:eq(7)').find('#delivery').hide();
				$tr.find('td:eq(7)').find('#remark').hide();
				$tr.find('td:eq(7)').find('#save_btn').hide();
				$tr.find('td:eq(7)').find('#saveConsign_btn').show();
				$tr.find('td:eq(7)').find('#unable_btn').hide();
			}

			if (unprint == "C") {
				$tr.find('td:eq(8) ').text("");
				$tr.find('td:eq(7)').find('#delivery').val("A")
				$tr.find('td:eq(7)').find('#delivery').show();
				$tr.find('td:eq(7)').find('#remark').show();
				$tr.find('td:eq(7)').find('#saveConsign_btn').hide();
				$tr.find('td:eq(7)').find('#unable_btn').show();
			}
		});

		/* ------------------------------------------------------------------*/
		/* Start - Column sorting with table header Info                     */
		/*-------------------------------------------------------------------*/
		$(document)
				.ready(
						function() {
							/* 	$("#date_pretty").datepicker({});
								var myDate = new Date();
								var month = myDate.getMonth() + 1;
								var prettyDate = month + '/' + myDate.getDate()
										+ '/' + myDate.getFullYear();
								$("#date_pretty").val(prettyDate); */

							/* 		$('#eib-pr').hide();
									$('#eib-list').hide();
									$('#etb-list').hide();
									$('#all-list').hide(); */

							/* 	var productTypeSelected = $('#productTypeValSession').val();
								var productEntitySelected = $('#productEntityValSession').val();									
								if (productTypeSelected == "NCI" || productTypeSelected == "NCT") {
									$('#mi-option').show();
								}

								else {
									$('#mi-option').hide();
								} */
							var t = $('#admin-datatable-second_admin')
									.DataTable(
											{

												dom : 'Bfrtip',
												buttons : [
														{
															extend : 'pageLength',
															className : 'btn btn-secondary btn-sm active'
														},
														{
															extend : 'print',
															className : 'btn btn-warning btn-sm',
															title : 'Etiqa Transaction Report',
															text : 'Print <i class="fa fa-print" aria-hidden="true"></i>'
														}
												//,
												//{
												// extend: 'excel',
												//filename: 'Etiqa Transaction Report',
												// className: 'btn btn-warning btn-sm',
												//text:'Export to XLS <i class="fa fa-download" aria-hidden="true"></i>'
												// }
												],
												"searching" : false,
												"columnDefs" : [ {
													"searchable" : false,
													"orderable" : false,
													"targets" : [ 0, 1, 2, 3,
															4, 5, 6, 7, 8 ],
													"bSortable" : false

												} ],
												"order" : [ [ 0, 'asc' ] ],
											});

							t.on('order.dt search.dt', function() {
								t.column(0, {
									search : 'applied',
									order : 'applied'
								}).nodes().each(function(cell, i) {
									cell.innerHTML = i + 1;
								});
							}).draw();

							t.buttons(0, null).containers()
									.appendTo('#btnhere');

						});
		/* ------------------------------------------------------------------ */
		/* End - Column sorting with table header Info                        */
		/*-------------------------------------------------------------------*/
	</script>
</body>
</html>