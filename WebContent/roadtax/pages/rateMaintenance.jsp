<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="com.spring.VO.roadtaxVO.RdtaxRateMaintenance"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="${pageContext.request.contextPath}/assets/css/rtx-menu.css"
	rel="stylesheet">

<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

.width10 {
	max-width: 1%;
}

.errorText {
	color: red
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="../../siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>

</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!-- header -->
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>

		<!-- end header -->
		<!-- header second-->
		<!-- header second-->
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menuRoadTax.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Rate Maintenance</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Rate Maintenance</h4>
														</div>
													</div>

												</div>

												<div class="col-sm-12">

													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="col-sm-12">

																<form method="post" action="" id="my_form">
																	<div class="col-sm-4">
																		<div class="form-group">
																			<label for="exampleInputName2">Search Region
																				: </label> <select class="selectpicker" name="getregionval"
																				required id="getregionval"
																				onblur="checkOnBlur(this,'Please select region');"
																				onchange="checkOnChange(this);">
																				<option value="">Please Select</option>
																				<option value="0">Peninsular</option>
																				<option value="1">Sabah and Sarawak</option>
																				<option value="2">Labuan</option>
																				<option value="3">Langkawi</option>
																			</select>
																			<%--   <select class="selectpicker" name="getRigionlist"
																					id="getRigionlist">
																					<option value="" selected>-View All-</option>
																					 <c:forEach items="${regionlist}" var="element"> 
						                                                                  <option						                                                                  
																									value="<c:out value="${element.paramCode}"/>">
																									<c:out value="${element.paramName}" />
																							</option>																																				
																						</c:forEach> 
				        			    </select> --%>



																		</div>
																	</div>
																	<div class="col-sm-4">
																		<div class="form-group">
																			<label for="exampleInputName2">Car Type : </label> <select
																				class="selectpicker" name="getcartype" required
																				id="getcartype"
																				onblur="checkOnBlur(this,'Please select type');"
																				onchange="checkOnChange(this);">

																				<option value="Y">Saloon</option>
																				<option value="N">Non Saloon</option>
																			</select>
																		</div>
																	</div>
																	<div class="col-sm-4">
																		<!-- <button type="sybmit" class="btn btn-default">Search</button> -->
																		<button type="submit" class="btn btn-default"
																			style="margin-top: 2.1rem;">Search</button>
																	</div>
																</form>
															</div>
															<div class="gap gap-mid"></div>
															<div class="row">
																<div class="col-xs-12 form-inline">
																	<div class="col-xs-6">
																		<div class="row"></div>
																	</div>
																	<div class="col-xs-6">
																		<div class="row">
																			<div class="pull-right">
																				<a href="redirectAddRateMaintenance"
																					class="btn btn-default">Add New</a>
																			</div>

																		</div>
																	</div>
																</div>
															</div>
															<div class="gap gap-mini"></div>
															<div class="table-responsive table-custom">
																<table class="table" id="ratetable">
																	<thead class="thead-custom">
																		<tr>
																			<th>Engine Capacity (CC) (From)</th>
																			<th>Engine Capacity (CC) (To)</th>
																			<th>Base Rate (RM)</th>
																			<th>Progressive Rate (Per CC)</th>
																			<th>Action</th>
																		</tr>
																	</thead>

																	<tbody id="allrows">

																	</tbody>

																</table>
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>
												<div class="row">
													<div class="col-xs-12"></div>
												</div>
											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->
		<!-- BEGIN FOOTER -->
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="../../pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
		<!-- END FOOTER -->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->

	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
	<script
		src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/bootstrap-select.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/bootstrap-tabcollapse.js"></script>

	<script
		src="${pageContext.request.contextPath}/assets/plugins/retina/retina.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/backstretch/jquery.backstretch.min.js"></script>


	<!-- PLUGINS -->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/prettify/prettify.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/icheck/icheck.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/mask/jquery.mask.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/summernote/summernote.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/to-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/slider/bootstrap-slider.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/toastr/toastr.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/footable-3/js/footable.js"></script>
	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->

	<!-- KNOB JS -->
	<!--[if IE]>
        <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/knob.js"></script>

	<!-- MAIN APPS JS -->
	<script src="${pageContext.request.contextPath}/assets/js/apps.js"></script>

	<script type="text/javascript">  

 function checkOnBlur(me, message) {
     var  e_id = $(me).attr('id');
     if (!(me.value).length) {
         var exist= document.getElementById(e_id+ '_e');
         if (exist == null) {
             var htmlString="";
             htmlString+='<label id='+ e_id+ '_e' + '  ' + 'class="error">'+ message + '</label>';
             $(htmlString).insertAfter("#"+ e_id);
             $('#'+ e_id).focus();
         }
     }
 }
 
 
 //////////////////////////////////////////////////////////////////////////////////////////////
 function checkOnChange(me) {
     var  e_id = $(me).attr('id');
     if($.trim(me.value).length) {
         var exist= document.getElementById(e_id+ '_e');
         if (exist != null) {
             $('#'+ e_id + '_e').remove();
         }
     }
 }
 
 //////////////////////////////////////////////////////////////////////////////////////////////
$("#my_form").submit(function(e) {
			e.preventDefault();
//function get_all_details(){
var a = $('[name="getregionval"]').val();
var b = $('[name="getcartype"]').val();
if(a == ""){
alert('Please select rigion.');
return false;
}else if(b == ""){
alert('Please select carttype.');
return false;
}else{
$.ajax({
	
url : '${pageContext.request.contextPath}/getRatemaintenenaceReport',
type: "post",
beforeSend : function() {
	$('.loader').addClass("is-active");
},
dataType: "json",
data:
{rigionid: a,
	carttype: b},// in controler use rigionid to get a value and carttype to get b value
success : function(data) {
	obj = data;
	var tr = '';
	if(obj.length > 0){
	for(var i =0;i<obj.length;i++){
		/**************************##################################################*****************************************/
	tr += '<tr data-id="'+obj[i].cal_id+'"><td data-text="fromrange">'+obj[i].fromrange+'</td><td data-text="torange">'+obj[i].torange+'</td><td data-text="base_rate">'+obj[i].base_rate+'</td><td data-text="progressive_rate">'+obj[i].progressive_rate+'</td><td><span class="cc-color1" onclick="makeeditablerow(this)" style="cursor:pointer">Edit</span>&nbsp;|&nbsp;<span class="cc-color2" onclick="deleterow(this)" style="cursor:pointer" >Delete</span></td></tr>';
	}
	}else{
	tr += '<tr><td colspan="10">No record found.</td></tr>';
	} 
	$("#allrows").html(tr);
	
},
complete : function() {
	$('.loader').removeClass(
			"is-active");
},
error : function(request, status, error) {
	alert(request.responseText);
}
});
}
});
 
 
 
function makeeditablerow(dis){
	$(dis).attr('onclick','updaterow(this)');
	$(dis).text('Update');
	var i = $(dis).parents('tr').find('td').length;
	var j = 0;
	$(dis).parents('tr').find('td').each(function(){
		if(j < i){
			var text = $(this).text();
			if(text!="Update | Delete"){
				$(this).text('');
				var textbox = '<input type="text" value="'+text+'" />';
				$(this).html(textbox);
				j++;
			}
		}
	});
} 


function updaterow(dis){
	var values = new Array();
	var fields = new Array();
	var rowid = $(dis).parents('tr').data('id');
	values.push(rowid);
	fields.push('cal_id');
	var j = 0;
	var i = ($(dis).parents('tr').find('td').length)-1;
	$(dis).parents('tr').find('td').each(function(){
		if(j < i){
			var text = $(this).find('[type="text"]').val();
			//var text1 = $(this).text();
			//if(text1!="Update | Delete"){
			var datatext = $(this).data('text');
			values.push(text);
			fields.push(datatext);
			$(this).html('');
			$(this).text(text);
			j++;
			//}
		}
	});
	$.ajax({
		url : '${pageContext.request.contextPath}/updateRateMaintenanceForRegion',
		type: "post",
		beforeSend : function() {
			$('.loader').addClass("is-active");
		},
		dataType: "json",
		data:{rowidvalue: rowid,
			datafieldArray:fields,
			dataArray: values},//  in controler use rigionid to get a value and cartype to get b value
		    success : function(data) {
			obj = data;
			var tr = '';
			if(obj.length > 0){
				for(var i =0;i<obj.length;i++){
					var status = obj[i].status;
					if(status=="success"){
						$(dis).attr('onclick','makeeditablerow(this)');
						$(dis).text('Edit');
						alert("Updated Successfully");
					}
				}
			}
			
		},
		complete : function() {
			$('.loader').removeClass(
					"is-active");
		},
		error : function(request, status, error) {
			//alert(request.responseText);
			$(dis).attr('onclick','makeeditablerow(this)');
			$(dis).text('Edit');
		}
	});
}


function deleterow(dis){
	var rowid = $(dis).parents('tr').data('id');
	
	$.ajax({
		url : '${pageContext.request.contextPath}/deleteRateMaintenanceRow',
		type: "post",
		beforeSend : function() {
		//	var $loading = $('#progress').show();
			$('.loader').addClass("is-active");
		},
		dataType: "json",
		data:{rowidvalue: rowid},//  in controler use rigionid to get a value and cartype to get b value
		success : function(data) {
			obj = data;
			//alert(obj);
					var status = obj.status;
					if(status=="1"){
						alert("Deleted Successfully");
						$(dis).parents('tr').remove();
						
					}
		},
		complete : function() {
			$('.loader').removeClass(
					"is-active");
			
		},
		error : function(request, status, error) {
			alert("Some error occured. Please try again..");
			
		}
	});
}

 </script>

</body>

</html>




