<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>



<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />

<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

.ddwidth {
	max-width: 170px;
	margin: 0 auto;
}

.width10 {
	max-width: 1%;
}

.errorText {
	color: red
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="../../siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th, .dataTable tr td {
	text-align: center;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>

</head>

<body>


	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menuRoadTax.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li>Road Tax Renewal</li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Add New Post Office</h4>
														</div>
													</div>
													<c:if test="${not empty errorMessages}">
														<div class="row">
															<div class="col-sm-12">
																<c:forEach items="${errorMessages}" var="element"
																	varStatus="theCount">
																	<ul>
																		<li class="errorText"><c:out value="${element}" /></li>
																	</ul>
																</c:forEach>
															</div>
														</div>
													</c:if>

													<!-- Start Form -->
													<form:form modelAttribute="roadTaxPos" name="roadTaxPos"
														action="addroadTaxPosDone" method="post">



														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-6">
																	<div class="form-horizontal">


																		<div class="form-group">
																			<label class="col-sm-3 control-label">Select
																				State</label>
																			<div class="col-sm-9">
																				<spring:bind path="roadTaxPos.stateCoverage">
																					<select class="form-control"
																						name="${status.expression}"
																						id="${status.expression}">
																						<option value="" selected>-View All-</option>
																						<option
																							<c:if test="${status.value eq 'Selangor' }" >selected</c:if>
																							value="Selangor">Selangor</option>
																						<option
																							<c:if test="${status.value eq 'Kuala Lumpur' }" >selected</c:if>
																							value="Kuala Lumpur">Kuala Lumpur</option>

																					</select>
																				</spring:bind>
																			</div>
																		</div>


																		<div class="form-group">
																			<label class="col-sm-3 control-label">Area </label>
																			<div class="col-sm-9">
																				<spring:bind path="roadTaxPos.postcodeArea">
																					<input type="text" placeholder=""
																						class="form-control" name="${status.expression}"
																						id="${status.expression}" value="${status.value}"
																						maxlength="20" />
																				</spring:bind>
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Post
																				office Name </label>
																			<div class="col-sm-9">
																				<spring:bind path="roadTaxPos.postofficeName">
																					<input type="text" placeholder=""
																						class="form-control" name="${status.expression}"
																						id="${status.expression}" value="${status.value}"
																						maxlength="50" />
																				</spring:bind>
																			</div>
																		</div>


																		<div class="form-group">
																			<label class="col-sm-3 control-label">Postcode
																				Coverage(From) </label>
																			<div class="col-sm-9">
																				<spring:bind path="roadTaxPos.postcodeFrom">
																					<input type="number" placeholder=""
																						class="form-control" name="${status.expression}"
																						id="${status.expression}" value="${status.value}"
																						maxlength="10" />
																				</spring:bind>
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Postcode
																				Coverage(To) </label>
																			<div class="col-sm-9">
																				<spring:bind path="roadTaxPos.postcodeTo">
																					<input type="number" placeholder=""
																						class="form-control" name="${status.expression}"
																						id="${status.expression}" value="${status.value}"
																						maxlength="10" />
																				</spring:bind>
																			</div>
																		</div>


																		<div class="form-group">
																			<label class="col-sm-3 control-label">Printing
																				Fees(RM) </label>
																			<div class="col-sm-9">
																				<spring:bind path="roadTaxPos.printingCharge">
																					<input type="decimal" placeholder=""
																						class="form-control" name="${status.expression}"
																						id="${status.expression}" value="${status.value}"
																						maxlength="10" />
																				</spring:bind>
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Delivery
																				Fees(RM) </label>
																			<div class="col-sm-9">
																				<spring:bind path="roadTaxPos.deliveryCharge">
																					<input type="decimal" placeholder=""
																						class="form-control" name="${status.expression}"
																						id="${status.expression}" value="${status.value}"
																						maxlength="10" />
																				</spring:bind>
																			</div>
																		</div>


																		<div class="form-group">
																			<label class="col-sm-3 control-label">Processing
																				Fees(RM) </label>
																			<div class="col-sm-9">
																				<spring:bind path="roadTaxPos.processingCharge">
																					<input type="decimal" placeholder=""
																						class="form-control" name="${status.expression}"
																						id="${status.expression}" value="${status.value}"
																						maxlength="10" />
																				</spring:bind>
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">GST(%)
																			</label>
																			<div class="col-sm-9">
																				<spring:bind path="roadTaxPos.gst">
																					<input type="number" placeholder=""
																						class="form-control" name="${status.expression}"
																						id="${status.expression}" value="${status.value}"
																						maxlength="10" />
																				</spring:bind>
																			</div>
																		</div>


																		<div class="form-group">
																			<label class="col-sm-3 control-label">Delivery
																				Service Offered</label>
																			<div class="col-sm-9">
																				<spring:bind path="roadTaxPos.deliveryService">
																					<select class="form-control"
																						name="${status.expression}"
																						id="${status.expression}">
																						<option value="" selected>-View All-</option>
																						<option
																							<c:if test="${status.value eq 'N' }" >selected</c:if>
																							value="N">Next Working Day Delivery</option>
																						<option
																							<c:if test="${status.value eq 'S' }" >selected</c:if>
																							value="S">Same Day Delivery</option>
																					</select>
																				</spring:bind>
																			</div>
																		</div>



																		<div class="col-sm-12 text-right">
																			<button type="submit" class="btn btn-warning btn-sm"
																				name="SUBMIT">Submit</button>

																			<input type="button" class="btn btn-warning btn-sm"
																				onclick="cancelAddPos()" value="Cancel" />
																		</div>

																	</div>
																</div>



															</div>
														</div>
													</form:form>
													<!-- END FORM -->
												</div>


												<%-- </c:if> --%>
											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="../../pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%-- <jsp:include page="siteFooter.jsp" /> --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->
	<script
		src="${pageContext.request.contextPath}/assets/js/jquery.min.bk.js"
		type="text/javascript"></script>
	<script
		src="${pageContext.request.contextPath}/assets/jAlert/jAlert.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/jAlert/jAlert.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/jAlert/jAlert-functions.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/bootstrap.js"
		type="text/javascript"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/retina/retina.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/backstretch/jquery.backstretch.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/apps.js"></script>
	<!-- PLUGINS -->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/prettify/prettify.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
	<!-- <script src="assets/plugins/chosen/chosen.jquery.min.js"></script> -->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/icheck/icheck.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/mask/jquery.mask.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/summernote/summernote.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/to-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/slider/bootstrap-slider.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/toastr/toastr.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/knob.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/dataTables.bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/dataTables.buttons.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/jszip.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/pdfmake.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/vfs_fonts.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.html5.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.print.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.colVis.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/toastr.min.js"></script>


	<script type="text/javascript">
	function cancelAddPos() {
		
		location.href="${pageContext.request.contextPath}/roadTaxPostCode";
		
	}
	
	</script>




</body>
</html>