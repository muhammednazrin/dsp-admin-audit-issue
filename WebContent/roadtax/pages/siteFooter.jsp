
<!-- JAVA Script Goes Here -->
<script
	src="${pageContext.request.contextPath}/assets/js/jquery.min.bk.js"
	type="text/javascript"></script>
<script
	src="${pageContext.request.contextPath}/assets/jAlert/jAlert.min.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/jAlert/jAlert-functions.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/bootstrap.js"
	type="text/javascript"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/retina/retina.min.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/nicescroll/jquery.nicescroll.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/backstretch/jquery.backstretch.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/apps.js"></script>
<!-- PLUGINS -->
<script
	src="${pageContext.request.contextPath}/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/prettify/prettify.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
<!-- <script src="assets/plugins/chosen/chosen.jquery.min.js"></script> -->
<script
	src="${pageContext.request.contextPath}/assets/plugins/icheck/icheck.min.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/mask/jquery.mask.min.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/validator/bootstrapValidator.min.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.highlight.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/summernote/summernote.min.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/markdown/markdown.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/markdown/to-markdown.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/markdown/bootstrap-markdown.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/slider/bootstrap-slider.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/toastr/toastr.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/placeholder/jquery.placeholder.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/assets/js/shieldui-all.min.js"></script>
<!--  Table Export -->
<script
	src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/jquery.knob.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/knob.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/js/jquery.dataTables.min.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/js/dataTables.bootstrap.min.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/js/dataTables.buttons.min.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/js/buttons.bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/jszip.min.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/js/pdfmake.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/vfs_fonts.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/js/buttons.html5.min.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/js/buttons.print.min.js"></script>
<script
	src="${pageContext.request.contextPath}/assets/js/buttons.colVis.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/toastr.min.js"></script>