<%@taglib prefix="botDetect" uri="https://captcha.com/java/jsp"%>
<%@ page import="java.io.*,java.util.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link
	href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css"
	rel="stylesheet">

<!-- PLUGINS CSS -->
<link
	href="${pageContext.request.contextPath}/assets/plugins/owl-carousel/owl.carousel.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/assets/plugins/owl-carousel/owl.theme.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/assets/plugins/owl-carousel/owl.transitions.min.css"
	rel="stylesheet">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link
	href="${pageContext.request.contextPath}/assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/assets/css/owl.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/assets/css/bootstrap-select.css"
	rel="stylesheet">
<!--<link href="assets/css/test.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="assets/assets/libs/html5shiv.js"></script>
        <script src="assets/assets/libs/respond.min.js"></script>
        <![endif]-->
</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper dashboard">
		<!-- header second-->
		<div class="header-admin">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h2>Administration</h2>
					</div>
				</div>
			</div>
		</div>
		<!-- header second-->


		<!-- BEGIN PAGE CONTENT -->
		<div class="page-content">
			<div class="container">
				<!-- /.container-->
				<div class="row">
					<div class="col-sm-offset-1 col-sm-9">
						<div class="panel panel-default login">
							<div class="panel-heading">
								<h4>Etiqa Administration Login</h4>
							</div>
							<div class="panel-body">
								<div class="col-sm-6">
									<div class="text-center">
										<img width="115" src="assets/images/lock.png" alt="logo">
										<div class="strong">
											<h5>Use a valid PF Number and Password to gain access to
												the Administrator Back-end.</h5>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<form:form action="rtxloginDone" method="post">
										<div class="panel panel-default">
											<div class="panel-body">
												<div class="col-sm-12">
													<div class="logo form-group">
														<a href="index.html"><img width="80"
															src="assets/images/logo.png" alt="logo"></a>
													</div>
												</div>

												<div class="col-xs-12">
													<div
														class="form-group has-feedback lg left-feedback no-label">
														<input type="text" class="form-control no-border rounded"
															name="username" placeholder="PF Number" autofocus>
														<span class="fa fa-user form-control-feedback fa-lg"></span>
													</div>
												</div>
												<div class="col-xs-12">
													<div
														class="form-group has-feedback lg left-feedback no-label">
														<input type="password"
															class="form-control no-border rounded" autocomplete="off"
															name="password" placeholder="Password"> <span
															class="fa fa-unlock-alt form-control-feedback fa-lg"></span>
													</div>
												</div>
												<div class="col-xs-12">
													<div
														class="form-group has-feedback lg left-feedback no-label"
														id="result">
														<%
                                                    if (request.getParameter("result")!=null) {
                                                  %>
														<span id="LoginError" class="error">Invalid
															Credentials. Please Verify PF No and Password</span>
														<%
                                                    }
                                                  %>
													</div>
													<div
														class="text-right form-group has-feedback lg left-feedback no-label">
														<input type="submit" class="btn btn-warning" value="Login">
													</div>
													<div class="text-right">
														<a
															href="https://www.etiqapartner.com.my/_layouts/15/EPSPartner/CustomPages/Security/Admin/ResetPassword.aspx">Forgot
															Password ?</a>
													</div>
												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</div>
						<!--
                            <div class="the-box">
                                <div>Etiqa Customer Web Portal! Administration Login</div>
                                <div class="col-sm-6">asdas</div>
                                <div class="col-sm-6">
                                    <div class="the-box">asdasd</div>
                                </div>
                            </div>
-->
					</div>
				</div>
				<!-- /.row -->
			</div>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="../../pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->

	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
	<script
		src="${pageContext.request.contextPath}/assets/assets/js/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/js/bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/js/bootstrap-select.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/retina/retina.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/backstretch/jquery.backstretch.min.js"></script>


	<!-- PLUGINS -->
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/prettify/prettify.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/icheck/icheck.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/mask/jquery.mask.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/summernote/summernote.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/markdown/markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/markdown/to-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/slider/bootstrap-slider.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/toastr/toastr.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/footable-3/js/footable.js"></script>

	<!-- KNOB JS -->
	<!--[if IE]>
        <script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/assets/plugins/jquery-knob/knob.js"></script>

	<!-- MAIN APPS JS -->
	<script
		src="${pageContext.request.contextPath}/assets/assets/js/apps.js"></script>

	<script type="text/javascript" language="javascript">   
function disableBackButton()
{
window.history.forward()


}  
disableBackButton();  
window.onload=disableBackButton();  
window.onpageshow=function(evt) { if(evt.persisted) disableBackButton() }  
window.onunload=function() { void(0) }  
if(location.href!="https://www.etiqa.com.my/MyAccount/login"){
history.pushState(null, document.title, location.href);
window.addEventListener('popstate', function (event)
{
history.pushState(null, document.title, location.href);
});
}
</script>

</body>
</html>