<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.math.BigDecimal"%>
<%@ page import="java.math.RoundingMode"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="com.etiqa.model.report.TransactionalReport"%>
<%
	java.text.DateFormat df = new java.text.SimpleDateFormat(
			"dd/MM/yyyy");
%>


<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/assets/css/rtx-menu.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/roadtax/css/bootstrap.min.css"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

.errorText {
	color: red
}

#container {
	text-align: center;
}

button {
	display: inline-block;
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="../../siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.ddwidth {
	max-width: 170px;
	margin: 0 auto;
}

.width10 {
	max-width: 10%;
}

table th, table td {
	padding: 8px !important;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>

</head>

<body>
	<%
		DecimalFormat formatter = new DecimalFormat("#,###.00");

		if (request.getMethod().equals("GET")) {

			session.setAttribute("ProductType", "");
			session.setAttribute("Status", "");
			session.setAttribute("PolicyCertificateNo", "");
			session.setAttribute("NRIC", "");
			session.setAttribute("ReceiptNo", "");
			session.setAttribute("dateFrom", "");
			session.setAttribute("dateTo", "");
			session.setAttribute("DateFrom", "");
			session.setAttribute("DateTo", "");
			session.setAttribute("PlateNo", "");
			session.setAttribute("ProductEntity", "");

		}

		String textColor = "";
		String status = "";
		String transactionStatus = "";
		String lastStep = "";
		String nextStep = "";
		String reason = "";
		int i = 0;
		int rowLine = 0;
		String txtColor = "";
	%>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="../../pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page=menuRoadTax.jsp " />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Road Tax Renewal</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-md-12 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Printing and Delivery</h4>
														</div>
													</div>


													<!-- Start Form -->
													<!-- END FORM -->
												</div>
												<%-- <c:if test="${empty motorReportList}">

			<div class="row">
			<c:out value="${emptySearch}" />
			</div>
		
			
</c:if>												
<c:if test="${not empty motorReportList}"> --%>
												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<form class="form-inline">
																<div class="form-group">
																	<h3 class="txt-custom2">Motor Takaful(ETB)</h3>
																</div>
															</form>

															<div class="row">
																<div class="col-xs-12 form-inline">
																	<div class="col-xs-6"></div>
																	<div class="col-xs-6">
																		<div class="row">
																			<div class="pull-right custom-lbl">
																				<label>Road Tax to Print :<%=session.getAttribute("countMT")%></label>
																				<label>Road Tax to Deliver :0</label>
																			</div>

																		</div>
																	</div>
																	<div class="col-xs-6"></div>
																	<div class="col-xs-6">
																		<div class="row">



																			<c:if test="${not empty rdtaxDeliveryAddList}">
																				<div class="pull-right" id='btnhere'>
																					<!--  <button class="btn btn-warning btn-sm">
																					Print <i class="fa fa-print" aria-hidden="true"></i>
																				</button>-->



																				</div>
																			</c:if>
																		</div>
																	</div>
																</div>
															</div>
															<div class="gap gap-mini"></div>
															<div class="table-responsive table-custom">
																<table id="admin-datatable-second_admin"
																	class="table table-striped table-warning table-hover">

																	<thead class="thead-custom" align="left">

																		<tr>
																			<th>No</th>
																			<th>Insured Info</th>
																			<th style="width: 44px;">Road Tax Period (Month)</th>
																			<th>Recipient Details</th>
																			<th style="width: 40px;">Aging (Days)</th>
																			<th>Road Tax Printing</th>
																			<th>Road Tax Amount (RM)</th>

																			<th>Remarks (For failed Road Tax Printing)</th>
																			<th>Consignment Notes Tracking No.</th>
																			<th>Action</th>
																		</tr>
																	</thead>
																	<tbody>
																		<c:if test="${not empty rdtaxDeliveryAddList}">
																			<c:forEach items="${rdtaxDeliveryAddList}"
																				var="element">


																				<tr>
																					<td align="center" class="width10"></td>
																					<td align="left">&nbsp;<B>Name:</B> <c:out
																							value="${element.customerName}" /><br>&nbsp;<B>ID
																							No:</B> <c:out value="${element.customerNricId}" /><br>&nbsp;<B>Vehicle
																							Reg.No:</B> <c:out value="${element.regNo}" /></td>

																					<td class="width10" align="center">&nbsp;<c:out
																							value="${element.period}" /></td>
																					<td align="left">&nbsp;<B>Name:&nbsp;</B> <c:out
																							value="${element.deliveryName}" /><br> <B>&nbsp;Address:&nbsp;</B>
																						<c:out value="${element.address1}" /><br> <B>&nbsp;PostCode:&nbsp;</B>
																						<c:out value="${element.postcode}" /><br> <B>&nbsp;Phone
																							No:&nbsp;</B> <c:out value="${element.mobile}" /></td>


																					<td align="center">1</td>
																					<td><select class="Options">
																							<option value="A">Please Select</option>
																							<option value="B">Printed</option>
																							<option value="C">Unable to Print</option></td>
																					<td align="left">&nbsp;<B>Collected:</B> <c:out
																							value="${element.totalRoadTax}" /><br>
																						&nbsp;<B>Actual:</B> <c:out
																							value="${element.roadTaxAmount}" /></td>
																					<!--  <td><input  name="{element.roadTaxAmount}" type="text" value=<%=request.getParameter("{element.roadTaxAmount}") %>></td> -->

																					<!--  <td align="left" >&nbsp;<c:out value="${element.roadTaxAmount}" /> </td> -->

																					<td><select class="ddwidth">
																							<option value="Please Select">Please
																								Select</option>
																							<option value="Same Day Delivery">Unsuccessful
																								due to invalid cover period</option>
																							<option value="Same Day Delivery">Unsuccessful
																								due to pending summons payment</option>
																							<option value="Same Day Delivery">Unsuccessful
																								due to vehicle being blacklisted</option>
																					</select></td>

																					<td id="cons" align="center">&nbsp;</td>

																					<td align="center">
																						<!-- 	<button type="button" align="center">Save</button>  -->
																						<input type="button" name="btnSubmit" value="Save"
																						align="center" onclick="showAlert();"> <!-- <button type="button" id="input">Print</button>  -->


																					</td>
																					<!-- <td><span class="cc-color1">Save</span>&nbsp;&nbsp;<span class="cc-color1">Print</span></td> -->
																					<!-- <td><button>Save</button><button>Print</button></td> -->
																				</tr>
																			</c:forEach>
																		</c:if>


																	</tbody>
																</table>
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>
												<%-- </c:if> --%>
											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="../../pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%-- <jsp:include page="siteFooter.jsp" /> --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->
	<script
		src="${pageContext.request.contextPath}/assets/js/jquery.min.bk.js"
		type="text/javascript"></script>
	<!-- <script src="${pageContext.request.contextPath}/assets/js/jquery.min.js" type="text/javascript"></script>  -->
	<!--  <script src="assets/jAlert/jquery-1.11.3.min.js"></script> -->
	<script
		src="${pageContext.request.contextPath}/assets/jAlert/jAlert.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/jAlert/jAlert-functions.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/bootstrap.js"
		type="text/javascript"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/retina/retina.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/backstretch/jquery.backstretch.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/apps.js"></script>

	<!-- PLUGINS -->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/prettify/prettify.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<!-- <script src="assets/plugins/chosen/chosen.jquery.min.js"></script> -->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/icheck/icheck.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/mask/jquery.mask.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/summernote/summernote.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/to-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/slider/bootstrap-slider.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/toastr/toastr.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/knob.js"></script>

	<script
		src="${pageContext.request.contextPath}/assets/js/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/dataTables.bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/dataTables.buttons.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/jszip.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/pdfmake.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/vfs_fonts.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.html5.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.print.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.colVis.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/toastr.min.js"></script>


	<script type="text/javascript">
	$(document).ready(function () {
	    // jquery button:
	    $("#demo_button").button({
	        icons: {
	            secondary: "ui-icon-info"
	        }
	    });
	    // jquery selectmenu:
	    $("#demoSelect").addClass("ui-selectmenu-button ui-widget ui-state-default ui-corner-all");
	    // variable for select option value:
	    var demoSelectValue = $("#demoSelect").val();
	    // disable the button if value is zero:
	    if (demoSelectValue == "0") {
	        $("#demo_button").button("disable")
	    }
	    // disable the button if the selection is changed:
	    $("#demoSelect").change(function () {
	        if ($(this).val() == "0") {
	            $("#demo_button").button("disable")
	        } else {
	            // enable button
	            $("#demo_button").button("enable")
	        }
	    });
	    // end $(document).ready(function():
	});
	
		$(document)
				.ready(
						function() {

							$("#date_pretty").datepicker({});
							var myDate = new Date();
							var month = myDate.getMonth() + 1;
							var prettyDate = month + '/' + myDate.getDate()
									+ '/' + myDate.getFullYear();
							$("#date_pretty").val(prettyDate);

							$('#eib-pr').hide();
							$('#eib-list').hide();
							$('#etb-list').hide();
							$('#all-list').hide();

							var productTypeSelected = $('#productTypeValSession').val();
							var productEntitySelected = $('#productEntityValSession').val();									
							if (productTypeSelected == "NCI" || productTypeSelected == "NCT") {
								$('#mi-option').show();
							}

							else {

								$('#mi-option').hide();

							}

							if (productEntitySelected == "EIB") {
								$('#eib-pr').show();
								$('#eib-list').show();
								$('#etb-list').hide();
								$('#all-list').hide();

							}

							if (productEntitySelected == "ETB") {
								$('#eib-pr').show();
								$('#eib-list').hide();
								$('#etb-list').show();
								$('#all-list').hide();

							}

							if (productEntitySelected == "") {

								$('#eib-list').hide();
								$('#etb-list').hide();
								$('#all-list').hide();

							}

							$("#productEntity").on('change', function() {
								alert(this.value);
								if (this.value == 'ETB') {
									$('#eib-pr').show();
									$('#eib-list').hide();
									$('#etb-list').show();
									$('#all-list').hide();
									$('#mi-option').hide();
									plateNo.value = null;
									$("#productType").val("NCT");
									

									//$('#mi-option').css("display","none");
								}

								else if (this.value == 'EIB') {
									$('#eib-pr').show();
									$('#eib-list').show();
									$('#etb-list').hide();
									$('#all-list').hide();
									$('#mi-option').hide();
									plateNo.value = null;
									$("#productType").val("NCI");
									//$('#mi-option').css("display","none");

								}

								else {
									$('#eib-pr').hide();
									$('#eib-list').hide();
									$('#etb-list').hide();
									$('#all-list').hide();
									$('#mi-option').hide();
									plateNo.value = null;
									productType.value = null;

									//$('#mi-option').css("display","none");

								}
							});

							$('#productTypeEIB').on('change', function() {
								//alert("ETB = "+this.value);
								productType.value = this.value;

								if (this.value == "NCI") {

									$('#mi-option').show();

								} else {

									$('#mi-option').hide();
									plateNo.value = null;

								}

							});
							
							

							$('#productTypeETB').on('change', function() {
								//alert("EIB = "+this.value);
								productType.value = this.value;

								if (this.value == "NCT") {

									$('#mi-option').show();

								} else {

									$('#mi-option').hide();
									plateNo.value = null;
								}

							});

							$('#productTypeALL').on('change', function() {
								//alert("ALL ="+this.value);
								productType.value = this.value;

								if (this.value == "NCI") {

									$('#mi-option').show();

								} else {

									$('#mi-option').hide();
									plateNo.value = null;
								}

							});

							var t = $('#admin-datatable-second_admin')
									.DataTable(
											{

												dom : 'Bfrtip',
												buttons : [
														{
															extend : 'pageLength',
															className : 'btn btn-secondary btn-sm active'
														},
														{
															extend : 'print',
															className : 'btn btn-warning btn-sm',
															title : 'Etiqa Transaction Report',
															text : 'Print <i class="fa fa-print" aria-hidden="true"></i>'
														}
												//,
												//{
												// extend: 'excel',
												//filename: 'Etiqa Transaction Report',
												// className: 'btn btn-warning btn-sm',
												//text:'Export to XLS <i class="fa fa-download" aria-hidden="true"></i>'
												// }
												],
												"searching" : false,
												"columnDefs" : [ {
													"searchable" : false,
													"orderable" : false,
													"targets" : [ 0, 1, 2, 3,
															4, 5, 6, 7 ],
													"bSortable" : false

												} ],
												"order" : [ [ 0, 'asc' ] ]
											});

							t.on('order.dt search.dt', function() {
								t.column(0, {
									search : 'applied',
									order : 'applied'
								}).nodes().each(function(cell, i) {
									cell.innerHTML = i + 1;
								});
							}).draw();

							t.buttons(0, null).containers()
									.appendTo('#btnhere');

						});
		/* function checkOption() {
		    /*var input = document.getElementById("input");
		    var Index = document.getElementById("Options").selectedIndex;
		    if(document.getElementById("Options")[Index].value=="C"){
		        input.disabled=true;
		    }else{
		        input.disabled=false;
		    }
		    if($(#Options').val()=='C'){
		    	$(''#input').css("display", "none");
		    }
		} */
		$('.Options').change(function(){
			var unprint = $(this).val();
			var $tr = $(this).closest('tr');	 
			var myRow = $tr.index();
			if(unprint == "B"){
				if (myRow == 0) {
					 $tr.find('td:eq(8) ').text("EC810000010MY");
					 var email = $tr.find('td:eq(8)').text();
					 /* <button type="button" id="input">Print</button> */
					 $tr.find('td:eq(8)').append('<br/> <button type="button" id="input"><a href="${pageContext.request.contextPath}/roadtax/images/sample-consignment.pdf"> Print </a></button>');
					}
				if (myRow == 1) {
				 $tr.find('td:eq(8) ').text("EC810000011MY");
				 var email = $tr.find('td:eq(8)').text();
				 $tr.find('td:eq(8)').append('<br/> <button type="button" id="input"><a href="${pageContext.request.contextPath}/roadtax/images/sample-consignment.pdf"> Print </a></button>');
				}
				if (myRow == 2) {
					var no="EC81000001"+myRow+"MY";
					 $tr.find('td:eq(8)').text(no);
					 var email = $tr.find('td:eq(8)').text();
					 $tr.find('td:eq(8)').append('<br/> <button type="button" id="input"><a href="${pageContext.request.contextPath}/roadtax/images/sample-consignment.pdf"> Print </a></button>');
					}
				
				if (myRow == 3) {
					var no="EC81000001"+myRow+"MY";
					 $tr.find('td:eq(8)').text(no);
					 var email = $tr.find('td:eq(8)').text();
					 $tr.find('td:eq(8)').append('<br/> <button type="button" id="input"><a href="${pageContext.request.contextPath}/roadtax/images/sample-consignment.pdf"> Print </a></button>');
					}
				if (myRow == 4) {
					var no="EC81000001"+myRow+"MY";
					 $tr.find('td:eq(8)').text(no);
					 var email = $tr.find('td:eq(8)').text();
					 $tr.find('td:eq(8)').append('<br/> <button type="button" id="input"><a href="${pageContext.request.contextPath}/roadtax/images/sample-consignment.pdf"> Print </a></button>');
					}
				if (myRow == 5) {
					var no="EC81000001"+myRow+"MY";
					 $tr.find('td:eq(8)').text(no);
					 var email = $tr.find('td:eq(8)').text();
					 $tr.find('td:eq(8)').append('<br/> <button type="button" id="input"><a href="${pageContext.request.contextPath}/roadtax/images/sample-consignment.pdf"> Print </a></button>');
					}
				if (myRow == 6) {
					var no="EC81000001"+myRow+"MY";
					 $tr.find('td:eq(8)').text(no);
					 var email = $tr.find('td:eq(8)').text();
					 $tr.find('td:eq(8)').append('<br/> <button type="button" id="input"><a href="${pageContext.request.contextPath}/roadtax/images/sample-consignment.pdf"> Print </a></button>');
					}
				if (myRow == 7) {
					var no="EC81000001"+myRow+"MY";
					 $tr.find('td:eq(8)').text(no);
					 var email = $tr.find('td:eq(8)').text();
					 $tr.find('td:eq(8)').append('<br/> <button type="button" id="input"><a href="${pageContext.request.contextPath}/roadtax/images/sample-consignment.pdf"> Print </a></button>');
					}
				if (myRow == 8) {
					var no="EC81000001"+myRow+"MY";
					 $tr.find('td:eq(8)').text(no);
					 var email = $tr.find('td:eq(8)').text();
					 $tr.find('td:eq(8)').append('<br/> <button type="button" id="input"><a href="${pageContext.request.contextPath}/roadtax/images/sample-consignment.pdf"> Print </a></button>');
					}
			}
			
			
			if(unprint == "C"){
				$("#input").hide();
			} else{
				$("#input").show();
			}
		});

		
		$('#Options1').change(function(){
			var unprint = $("#Options1").val();
			
			if(unprint == "B"){
				
				$('#cons').text("cons");
			}
			
			
			if(unprint == "C"){
				$("#input1").hide();
			} else{
				$("#input1").show();
			}
		});
function showAlert() {
	console.log("sddsdsds");
	alert("Hello");
}

/* ------------------------------------------------------------------*/
/* Start - Column sorting with table header Info                     */
/*-------------------------------------------------------------------*/
$(document)
		.ready(
				function() {
					/* 	$("#date_pretty").datepicker({});
						var myDate = new Date();
						var month = myDate.getMonth() + 1;
						var prettyDate = month + '/' + myDate.getDate()
								+ '/' + myDate.getFullYear();
						$("#date_pretty").val(prettyDate); */

					/* 		$('#eib-pr').hide();
							$('#eib-list').hide();
							$('#etb-list').hide();
							$('#all-list').hide(); */

					/* 	var productTypeSelected = $('#productTypeValSession').val();
						var productEntitySelected = $('#productEntityValSession').val();									
						if (productTypeSelected == "NCI" || productTypeSelected == "NCT") {
							$('#mi-option').show();
						}

						else {
							$('#mi-option').hide();
						} */
					var t = $('#admin-datatable-second_admin')
							.DataTable(
									{

										dom : 'Bfrtip',
										buttons : [
												{
													extend : 'pageLength',
													className : 'btn btn-secondary btn-sm active'
												},
												{
													extend : 'print',
													className : 'btn btn-warning btn-sm',
													title : 'Etiqa Transaction Report',
													text : 'Print <i class="fa fa-print" aria-hidden="true"></i>'
												}
										//,
										//{
										// extend: 'excel',
										//filename: 'Etiqa Transaction Report',
										// className: 'btn btn-warning btn-sm',
										//text:'Export to XLS <i class="fa fa-download" aria-hidden="true"></i>'
										// }
										],
										"searching" : false,
										"columnDefs" : [ {
											"searchable" : false,
											"orderable" : false,
											"targets" : [ 0, 1, 2, 3,
													4, 5, 6, 7, 8 ],
											"bSortable" : false

										} ],
										"order" : [ [ 0, 'asc' ] ],
									});

					t.on('order.dt search.dt', function() {
						t.column(0, {
							search : 'applied',
							order : 'applied'
						}).nodes().each(function(cell, i) {
							cell.innerHTML = i + 1;
						});
					}).draw();

					t.buttons(0, null).containers()
							.appendTo('#btnhere');

				});
/* ------------------------------------------------------------------ */
/* End - Column sorting with table header Info                        */
/*-------------------------------------------------------------------*/
	
	</script>




</body>
</html>