<%@ page import="java.util.List"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.text.DecimalFormat"%>
<%@ page import="com.spring.VO.roadtaxVO.RoadTaxTxnReport"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%
	java.text.DateFormat df = new java.text.SimpleDateFormat("dd/MM/yyyy");
%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="${pageContext.request.contextPath}/assets/css/rtx-menu.css"
	rel="stylesheet">

<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

.width10 {
	max-width: 1%;
}

.errorText {
	color: red
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="../../siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>

</head>

<body>
	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menuRoadTax.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink"> <!-- Road Tax Renewal -->
												</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>
																<!-- Road Tax Transactional Report -->
															</h4>
														</div>
													</div>
													<c:if test="${not empty errorMessages}">
														<div class="row">
															<div class="col-sm-12">
																<c:forEach items="${errorMessages}" var="element"
																	varStatus="theCount">
																	<ul>
																		<li class="errorText"><c:out value="${element}" /></li>
																	</ul>
																</c:forEach>
															</div>
														</div>
													</c:if>

													<!-- Start Form -->
													<form:form action="searchRoadTaxTxnReport"
														id="txnreportForm" name="txnreportForm" method="POST">
														<input type="hidden" name="action" value="list" />
														<input type="hidden" name="startRow" value="0" />
														<input type="hidden" name="endRow" value="10" />
														<input type="hidden" id="policyNumber"
															name="productTypeSession"
															value="<c:out value="${element.policyNumber}" />" />
														<input type="hidden" id="customerNricId"
															name="customerNricIdSession"
															value="<c:out value="${element.ProductType}" />" />
														<input type="hidden" id="printStatus"
															name="printStatusSession"
															value="<c:out value="${element.printStatus}" />" />

														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-6">
																	<div class="form-horizontal">

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Search
																				by Date</label>
																			<div class="col-sm-4">

																				<fmt:parseDate value="${now}" var="parsedEmpDate"
																					pattern="dd/MM/yyyy" />

																				<input type="text" placeholder="Date From"
																					class="form-control" id="datepicker1"
																					name="dateFrom"
																					<c:choose>
																					    <c:when test="${not empty sessionScope.dateFrom}">
																					         value="<c:out value="${sessionScope.dateFrom}" />"																					     
																					    </c:when>
																					    <c:otherwise>
																					          value="<%= df.format(new java.util.Date())%>"
																					    </c:otherwise>
																					</c:choose> />
																			</div>
																			<div class="col-sm-5">
																				<input type="text" placeholder="Date To"
																					class="form-control" id="datepicker2" name="dateTo"
																					<c:choose>
																					    <c:when test="${not empty sessionScope.dateTo}">
																					         value="<c:out value="${sessionScope.dateTo}" />"																					     
																					    </c:when>
																					    <c:otherwise>
																					          value="<%= df.format(new java.util.Date())%>"
																					    </c:otherwise>
																					</c:choose> />
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Vehicle
																				Registration No.</label>
																			<div class="col-sm-9">
																				<input type="text" name="plateNo" placeholder=""
																					class="form-control" value="<c:out value="" />" />
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Road
																				Tax Status</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="roadTaxStatus"
																					id="roadTaxStatus">
																					<option value="" selected>-View All-</option>
																					<option value="Printed">Printed</option>
																					<option value="UnableToPrint">Unable to
																						Print</option>
																				</select>
																			</div>
																		</div>
																	</div>
																</div>

																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">NRIC/ID
																				No.</label>
																			<div class="col-sm-9">
																				<input type="text" placeholder=""
																					class="form-control" name="NRIC"
																					value="<c:out value="" />" />
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Policy/Certificate
																				No</label>

																			<div class="col-sm-9">
																				<input type="text" placeholder=""
																					class="form-control" name="policyNo" value="" />
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col-sm-12 text-right">
																	<button type="submit" class="btn btn-default"
																		name="SEARCH">Search</button>
																</div>
															</div>
														</div>
													</form:form>
													<!-- END FORM -->
												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="row">
																<div class="col-xs-12 form-inline">
																	<div class="col-xs-6"></div>
																	<div class="col-xs-6">
																		<div class="row">
																			<%-- <c:if test="${not empty roadTaxTxnReportList}">
																				<div class="pull-right" id='btnhere'>
																					<a href="generateExcelTxnReport"><button
																							class="btn btn-warning btn-sm">
																							Export to XLS <i class="fa fa-download"
																								aria-hidden="true"></i>
																						</button></a>
																				</div>
																			</c:if> --%>
																		</div>
																	</div>
																</div>
															</div>
															<div class="table-responsive" id="MyTable">
																<table
																	class="table table-striped table-warning table-hover"
																	id="admin-datatable-second_admin">
																	<thead>
																		<tr>
																			<!-- 	<th style="width: 1px;">No</th>
																			<th style="width: 50px;">Transaction Date</th>
																			<th style="width: 150px;">Insured Info</th>
																			<th style="width: 55px;">Road Tax Period (Month)</th>
																			<th style="width: 120px;">Recipient Details</th>
																			<th>Aging (Days)</th>
																			<th style="width: 75px;">Road Tax Printing</th>
																			<th style="width: 130px;">Road Tax Amount (RM)</th>
																			<th style="width: 150px;">Consignment Note
																				Tracking No.</th> -->
																		</tr>
																	</thead>
																	<tbody>
																		<%-- <c:if test="${not empty roadTaxTxnReportList}">
																			<c:forEach items="${roadTaxTxnReportList}"
																				var="element" varStatus="theCount">
																				<tr>
																					<td align="center" class="width10"></td>
																					<td align="left"><c:out
																							value="${element.dateAndTime}" /></td>
																					<!-- <td align="left"></td>  -->
																					<td align="left"><B>Name:</B>&nbsp;<c:out
																							value="${element.customerName}" /><br> <B>ID
																							No:</B>&nbsp;<c:out value="${element.customerNricId}" /><br>
																						<B>Vehicle Reg. No:</B>&nbsp;<c:out
																							value="${element.regNo}" /><br> <B>Policy
																							No:</B>&nbsp;<c:out value="${element.policyNumber}" /><br>
																						<B>JPJ Status:</B>&nbsp; <c:choose>
																							<c:when test="${empty element.message}">
																								N/A
																							</c:when>
																							<c:otherwise>
																								<c:out value="${element.message}" />
																							</c:otherwise>
																						</c:choose></td>
																					<td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<c:out
																							value="${element.period}" /></td>

																					<td align="left"><B>Name:</B>&nbsp;<c:out
																							value="${element.deliveryName}" /><br> <B>Address:</B>&nbsp;<c:out
																							value="${element.address1}" /><br> <c:out
																							value="${element.address2}" /><br> <c:out
																							value="${element.address3}" /><br> <B>Postcode:</B>&nbsp;<c:out
																							value="${element.postcode}" /><br> <B>State:&nbsp;</B>
																						<c:out value="${element.rtstate}" /><br> <B>Phone
																							No:</B>&nbsp;<c:out value="${element.mobile}" /><br>
																					</td>

																					<td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<c:out
																							value="${element.aging}" /></td>

																					<td align="left"><c:if
																							test="${element.printStatus eq 'SUCCESS'}">
																							<c:out value="${element.remarks}">
																							</c:out>
																						</c:if> <c:if test="${element.refundStatus eq 'SUCCESS'}">
																							<br>
																							<c:out value="${element.remarks}"></c:out>
																							<c:out value="${element.jpjStatus}"></c:out>
																						</c:if></td>

																					<td align="left"><B>Collected:</B>&nbsp;<c:out
																							value="${element.roadTaxAmount}" /> <fmt:formatNumber
																							type="number" pattern="#,###.00"
																							value="${element.roadTaxAmount}" /><br> <B>Actual:</B>&nbsp;


																						<input id="target" type="text" class="actualValue"
																						name="actVal" id="actVal" style="width: 42px"
																						maxlength="6" size="2" width="1" height="1"
																						value="${element.actualAmount}" disabled="true" /><br>
																						<br> <input type="button"
																						class="btn btn-default edit_actamt" value="Edit"
																						id="edit_btn" /> <input type="hidden" id="qqid"
																						name="qqid"
																						value="<c:out value="${element.qqId}"/>" /> <input
																						type="button" class="btn btn-default save_actamt"
																						style="display: none" value="Save" name="save_btn"
																						id="save_btn"></td>

																					<td align="left"><c:choose>
																							<c:when
																								test="${element.printStatus eq 'SUCCESS'}">
																								<c:out value="${element.trackingNo}" />
																								<button type="button" id="input">
																									<a target="_blank"
																										href="${pageContext.request.contextPath}/roadtax/images/sample-consignment.pdf">
																										Re-print </a>																										
																								<a
																						href="downloadConsignment?Id=<c:out value="${element.trackingNo}" />"
																						target="_blank" >Re-print</a>		
																										
																								</button>
																							</c:when>
																							<c:otherwise></c:otherwise>
																						</c:choose></td>
																				</tr>
																			</c:forEach>
																		</c:if> --%>
																	</tbody>
																</table>
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>
												<%-- </c:if> --%>
											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="../../pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>

	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->


</body>
</html>