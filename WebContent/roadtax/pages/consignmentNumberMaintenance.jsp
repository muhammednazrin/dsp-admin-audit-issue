<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="${pageContext.request.contextPath}/assets/css/menu.css"
	rel="stylesheet">

<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

.width10 {
	max-width: 1%;
}

.errorText {
	color: red
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="../../siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}

.custom-file-input {
	display: inline-block;
	overflow: hidden;
	position: relative;
}

.custom-file-input input[type="file"] {
	width: 100%;
	height: 100%;
	opacity: 0;
	filter: alpha(opacity = 0);
	zoom: 1; /* Fix for IE7 */
	position: absolute;
	top: 0;
	left: 0;
	z-index: 999;
}
</style>



</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!-- header -->
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>

		<!-- end header -->
		<!-- header second-->
		<!-- header second-->
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menuRoadTax.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>

					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Consignment Number</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Consignment Number Maintenance</h4>
														</div>
													</div>
												</div>

												<div class="col-sm-12">

													<div class="sub">
														<label>Used No: <c:out value="${isUsed}" /> / <c:out
																value="${total}" />
														</label>
													</div>

													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="gap gap-mid"></div>
															<div class="row">
																<form:form action="getConsignmentRate" method="post"
																	enctype="multipart/form-data" id="fileUploadForm">
																	<div class="col-xs-12 form-inline">
																		<div class="col-xs-6">
																			<div class="row">
																				<label class="custom-file-input"> Upload
																					File: <input type="text"> <input
																					type="file" id="uploadFileConsignment"
																					accept=".xls,.xlsx"
																					class="form-control no-border rounded"
																					placeholder="Input File"
																					name="uploadFileConsignment"
																					data-url="consignmentFileUpload"> <input
																					type="button" class="btn btn-default"
																					value="Browse">
																				</label>
																			</div>
																		</div>
																		<div class="row">
																			<button id="uploadFIle" class="btn btn-default"
																				type="submit">Upload</button>
																		</div>

																	</div>
																</form:form>
															</div>
															<div class="gap gap-mini"></div>
															<div class="table-responsive table-custom">
																<table class="table" id="consignmentNumbertable">
																	<thead class="thead-custom">
																		<tr>
																			<th>No.</th>
																			<th>Files</th>
																			<th>Upload Date</th>
																			<th>Status</th>
																			<th>Action</th>
																		</tr>
																	</thead>
																	<tbody id="allrows">
																	</tbody>
																</table>
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>
												<div class="row"></div>
											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->
		<!-- BEGIN FOOTER -->
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="../../pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
		<!-- END FOOTER -->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->

	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
	<script
		src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/assets/plugins/retina/retina.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/backstretch/jquery.backstretch.min.js"></script>


	<!-- PLUGINS -->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/prettify/prettify.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/icheck/icheck.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/mask/jquery.mask.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/summernote/summernote.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/to-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/slider/bootstrap-slider.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/toastr/toastr.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/footable-3/js/footable.js"></script>
	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->

	<!-- KNOB JS -->
	<!--[if IE]>
        <script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/knob.js"></script>

	<!-- MAIN APPS JS -->
	<script src="${pageContext.request.contextPath}/assets/js/apps.js"></script>

	<script type="text/javascript">  
	
	 $.ajax({
		url : '${pageContext.request.contextPath}/fetchConsignmentFiles',
		type: "post",
		beforeSend : function() {
		//	var $loading = $('#progress').show();
			$('.loader').addClass("is-active");
		},
		dataType: "json",
		data: '',//  in controler use rigionid to get a value and cartype to get b value
		success : function(data) {
			//	alert(data[0].fileName);
			//console.info(data);
			var result = '';
			for(var i=0; i<data.length; i++)
				{
				   result = result+"<tr><td>"+i+"</td><td>"+data[i].fileName+"</td><td>"+data[i].fileCreationDate+"</td><td>"+data[i].fileStatus+"</td><td><a href='javascript:void(0)' class='dlefile' data-filename='"+data[i].fileName+"' onclick='deleterow(this)'>Delete</a></td></tr>"
				}
			$("#allrows").empty();
			$("#allrows").html(result);
		},
		complete : function() {
			$('.loader').removeClass(
					"is-active");
			
		},
		error : function(request, status, error) {
			alert(request.responseText);
			
		}
	}); 
	
	
//////////////////////////////////////////////////////////////////////////////////////////////
$(document).ready(function() {
			  $("#uploadFIle").click(function() {
				 $('.custom-file-input input[type="file"]').change(function(e){
				        $(this).siblings('input[type="text"]').val(e.target.files[0].name);
				    });
		    	});
			  
			});
 
 
 
$(document).ready(function(){
    $('.custom-file-input input[type="file"]').change(function(e){
        $(this).siblings('input[type="text"]').val(e.target.files[0].name);
    });
}); 
 


function deleterow(dis){
	var consignment_filename = $(dis).attr('data-filename');
	
	$.ajax({
		url : '${pageContext.request.contextPath}/deleteConsignmentFile',
		type: "post",
		beforeSend : function() {
		//	var $loading = $('#progress').show();
			$('.loader').addClass("is-active");
		},
		dataType: "json",
		data:{consignmentFile: consignment_filename},// 
		success : function(data) {
			obj = data;
			//alert(obj);
			console.info(data);
					var status = obj[0].status;
					if(status=="1"){
						alert("Deleted Successfully");
						$(dis).parents('tr').remove();
					}else{
						alert("Deleted UnSuccessfully");
					}
		},
		complete : function() {
			$('.loader').removeClass(
					"is-active");
			
		},
		error : function(request, status, error) {
			alert(request.responseText);
			
		}
	});
}

 </script>

</body>

</html>




