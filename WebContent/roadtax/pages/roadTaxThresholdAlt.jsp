<%@ page import="java.util.List"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.text.DecimalFormat"%>
<%@ page import="com.spring.VO.roadtaxVO.RoadTaxTxnReport"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%
	java.text.DateFormat df = new java.text.SimpleDateFormat("dd/MM/yyyy");
%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="${pageContext.request.contextPath}/assets/css/rtx-menu.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/assets/jAlert/jAlert.css">
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

.width10 {
	max-width: 1%;
}

.successText {
	color: #FFA500
}

.orangeColor {
	color: #FFA500
}

.errorText {
	color: red
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="../../siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>

</head>

<body>
	<%
		if (request.getMethod().equals("GET")) {

			//session.setAttribute("interval", "");
			//session.setAttribute("email", "");
			//session.setAttribute("limit", "");
			//session.setAttribute("entity", "");
		}
	%>
	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menuRoadTax.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Road Tax Renewal</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Float Threshold Alert</h4>
														</div>
													</div>
													<c:if test="${not empty errorMessages}">
														<div class="row">
															<div class="col-sm-12">
																<c:forEach items="${errorMessages}" var="element"
																	varStatus="theCount">
																	<ul>
																		<li class="errorText"><c:out value="${element}" /></li>
																	</ul>
																</c:forEach>
															</div>
														</div>
													</c:if>

													<c:if test="${not empty dbErrorMessages}">
														<div class="row">
															<div class="col-sm-12">
																<c:forEach items="${dbErrorMessages}" var="element"
																	varStatus="theCount">
																	<ul>
																		<li class="successText"><c:out value="${element}" /></li>
																	</ul>
																</c:forEach>
															</div>
														</div>
													</c:if>

													<!-- Start Form -->
													<form:form action="saveThresholdAlt" id="thresholdForm"
														name="thresholdForm" method="POST">

														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-6">
																	<div class="form-horizontal">

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Interval
																				By Hours</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="interval"
																					id="interval"
																					value="<c:out value="${sessionScope.interval}" />">
																					<option value="" selected>-Select
																						Interval-</option>
																					<option value="1">Every 1 Hour</option>
																					<option value="2">Every 2 Hour</option>
																					<option value="3">Every 3 Hour</option>

																				</select>
																			</div>
																		</div>

																		<!-- 																		<div class="form-group">
																			<label class="col-sm-3 control-label">Email
																				To</label>
																			<div class="col-sm-9">
																				<input type="text" id="email" name="email"
																					placeholder="" class="form-control" value=""
																					maxlength="50" onblur="validateRoadTaxEmail();" />
																			</div>
																		</div> -->

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Threshold
																				Limit [RM]</label>
																			<div class="col-sm-9">
																				<input type="text" name="limit"
																					placeholder="<c:out value="${sessionScope.intervalByHours}" />"
																					class="form-control" value="" />
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Entity</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="entity"
																					id="entity"
																					value="<c:out value="${sessionScope.entity}" />">
																					<option value="" selected>-View All-</option>
																					<option value="EIB">EIB</option>
																					<option value="ETB">ETB</option>
																				</select>
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Email
																				To</label>
																			<div class="col-sm-9">
																				<input type="text" id="email" name="email"
																					placeholder="" class="form-control" value=""
																					maxlength="50" onblur="validateRoadTaxEmail();" />
																			</div>
																		</div>

																	</div>
																</div>

																<div class="col-sm-12 text-center">

																	<!-- Pramain Start  -->

																	<c:set var="MIRatepervalue"
																		value="${fn:split(param.pid,'$')}" />
																	<c:out value="${MIRatepervalue[0]}"></c:out>
																	<c:if
																		test="${(MIRatepervalue[0] eq 1) || (MIRatepervalue[0] eq 2 )}">
																		<button type="save" class="btn btn-default"
																			name="SAVE">Save</button>
																	</c:if>

																	<!-- Pramain end -->
																	<!-- <button type="save" class="btn btn-default" name="SAVE">Save</button> -->



																</div>

															</div>
														</div>

													</form:form>
													<!-- END FORM -->
												</div>


												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">

															<div class="table-responsive" id="MyTable">
																<table
																	class="table table-striped table-warning table-hover"
																	id="admin-datatable-second_admin">
																	<thead>
																		<tr>
																			<!-- <th style="width: 1px;">No</th> -->
																			<th style="width: 50px;">Interval By Hours</th>
																			<th style="width: 150px;">Email To</th>
																			<th style="width: 55px;">Threshold Limit [RM]</th>
																			<th style="width: 120px;">Entity</th>
																		</tr>
																	</thead>
																	<tbody>
																		<c:if test="${not empty RoadTaxThreshold}">
																			<c:forEach items="${RoadTaxThreshold}" var="element"
																				varStatus="theCount">
																				<tr>
																					<!-- <td align="center" class="width10"></td> -->
																					<td align="left"><c:out
																							value="${element.intervalByHours}" /></td>

																					<td align="left"><c:out
																							value="${element.emailTo}" /></td>

																					<td align="left"><c:out
																							value="${element.thresholdLimit}" /></td>

																					<td align="left"><c:out
																							value="${element.entity}" /></td>


																				</tr>
																			</c:forEach>
																		</c:if>
																	</tbody>
																</table>
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>
												<%-- </c:if> --%>


											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="../../pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>

	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->

	<script type="text/javascript">
		/* ------------------------------------------------------------------ */
		/* Start - validation Email                                           */
		/*------------------------------------------------------------------- */

		function validateRoadTaxEmail() {
			//emailExist = "n";
			var entity = document.getElementById("entity").value;
			var email = document.getElementById("email").value;			
			
			//alert(email);

			if (email.length > 0) {
				$
						.ajax({
							//url : 'validateAgentEmail.html',
							url : '${pageContext.request.contextPath}/validateRoadTaxEmail',
							type : 'GET',
							async : false,
							data : ({
								entity : entity,
								email : email
							}),

							success : function(data) {
								var obj = jQuery.parseJSON(data);

								if (obj.validateEmailFlag == 'y') {
									$('#email').addClass("inputError");
									//$('#msg_validagentmail').text("Email Already Registered, Please Choose Another Email!");
									//$('#msg_validagentmail').removeClass("hidden");
									//$('#msg_validagentmail').addClass("visible");
									//emailExist="y";
									errorAlert(
											"Error",
											email
													+ " Already Registered, Please Choose Another Email!")
									//alert(email +" Already Registered, Please Choose Another Email!");

									//document.getElementById("email").value="";
								}

							}
						});

			}
			//return emailExist;
		}
	</script>

</body>
</html>