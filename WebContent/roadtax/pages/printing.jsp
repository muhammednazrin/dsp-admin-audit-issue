<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.math.BigDecimal"%>
<%@ page import="java.math.RoundingMode"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="com.etiqa.model.report.TransactionalReport"%>
<%
	java.text.DateFormat df = new java.text.SimpleDateFormat("dd/MM/yyyy");
	//session.setAttribute("un", session.getAttribute("un"));
	//System.out.println("roadTax Printing Jsp: ============>"+ session.getAttribute("un"));
%>


<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/assets/css/rtx-menu.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/roadtax/css/bootstrap.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

/* .errorText {
	color: red
}

.orangeColor {
	color: #FFA500
} */
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="../../siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>

</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menuRoadTax.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Road Tax Renewal</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Printing and Delivery</h4>
														</div>
													</div>

													<div class="col-sm-12">

														<c:if test="${not empty errorMessages}">
															<div class="row">
																<div class="col-sm-12">
																	<c:forEach items="${errorMessages}" var="element"
																		varStatus="theCount">
																		<ul>
																			<li class="errorText"><c:out value="${element}" /></li>
																		</ul>
																	</c:forEach>
																</div>
															</div>
														</c:if>

														<!-- Begin  table -->
														<div class="content-inner">
															<div class="the-box full no-border">
																<div class="custom-bx">
																	<div class="col-sm-6">

																		<form class="form-inline">
																			<div class="form-group">
																				<h3 class="orangeColor">Motor
																					Insurance&nbsp;(EIB)</h3>
																			</div>
																			<div class="custom-lbl">
																				<label>Road Tax to be Processed : <c:out
																						value="${countMI}" />
																				</label><br>

																				<!--  <label>Road Tax to Deliver :0</label>  -->
																			</div>
																			<div class="gap gap-mini"></div>
																			<div class="">
																				<a href="updatePrinting?callMethod=MI"
																					class="btn btn-default">Update</a>
																			</div>
																		</form>

																	</div>
																	<div class="col-sm-6">
																		<form class="form-inline">
																			<div class="form-group">
																				<h3 class="txt-custom2">Motor
																					Takaful&nbsp;(ETB)</h3>
																			</div>
																			<div class="custom-lbl">
																				<label>Road Tax to be Processed :&nbsp;<c:out
																						value="${countMT}" /></label><br>
																				<!-- <label>Road Tax to Deliver :0</label>  -->
																			</div>
																			<div class="gap gap-mini"></div>
																			<div class="">
																				<a href="updatePrinting?callMethod=MT"
																					class="btn btn-default" disable="true">Update</a>
																			</div>
																		</form>
																	</div>
																</div>
															</div>
															<!-- /.the-box -->
														</div>

													</div>

													<div class="col-sm-12">
														<!-- Begin  table -->
														<div class="content-inner"></div>
														<!-- End warning color table -->
													</div>
													<%-- </c:if> --%>
												</div>
											</div>
											<!--row -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.container -->

			</div>
			<!-- /.page-content -->
			<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
			<jsp:include page="../../pageFooter.jsp" />
			<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
		</div>

		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="siteFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
		<!-- JAVA Script Goes Here -->

		<script type="text/javascript">
			
		</script>
</body>
</html>