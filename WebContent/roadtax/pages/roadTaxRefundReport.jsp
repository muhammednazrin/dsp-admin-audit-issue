<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="com.spring.VO.roadtaxVO.RoadTaxTxnReport"%>
<%
	java.text.DateFormat df = new java.text.SimpleDateFormat("dd/MM/yyyy");
	//session.setAttribute("un", session.getAttribute("un"));
	//System.out.println("roadTax Transaction Jsp: ============>"+ session.getAttribute("un"));
%>


<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="${pageContext.request.contextPath}/assets/css/rtx-menu.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/assets/jAlert/jAlert.css">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

.ddwidth {
	max-width: 170px;
	margin: 0 auto;
}

.width10 {
	max-width: 1%;
}

.errorText {
	color: red
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="../../siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>

</head>

<body>
	<%
		DecimalFormat formatter = new DecimalFormat("#,###.00");

		if (request.getMethod().equals("GET")) {

			/* 		session.setAttribute("ProductType", "");
					session.setAttribute("Status", "");
					session.setAttribute("PolicyCertificateNo", "");
					session.setAttribute("NRIC", "");
					session.setAttribute("ReceiptNo", "");
					session.setAttribute("dateFrom", "");
					session.setAttribute("dateTo", "");
					session.setAttribute("DateFrom", "");
					session.setAttribute("DateTo", "");
					session.setAttribute("PlateNo", "");
					session.setAttribute("ProductEntity", "");
			*/
		}

		String textColor = "";
		String status = "";
		String transactionStatus = "";
		String lastStep = "";
		String nextStep = "";
		String reason = "";
		int i = 0;
		int rowLine = 0;
		String txtColor = "";
	%>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menuRoadTax.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Road Tax Renewal</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Road Tax Refund Report</h4>
														</div>
													</div>
													<c:if test="${not empty errorMessages}">
														<div class="row">
															<div class="col-sm-12">
																<c:forEach items="${errorMessages}" var="element"
																	varStatus="theCount">
																	<ul>
																		<li class="errorText"><c:out value="${element}" /></li>
																	</ul>
																</c:forEach>
															</div>
														</div>
													</c:if>

													<!-- Start Form -->
													<form:form name="refundreport"
														action="searchRoadTaxRefundReport?refundReport=SUCCESS;"
														method="post">
														<input type="hidden" name="action" value="list" />
														<input type="hidden" name="startRow" value="0" />
														<input type="hidden" name="endRow" value="10" />
														<input type="hidden" id="policyNumber"
															name="productTypeSession"
															value="<c:out value="${element.policyNumber}" />" />
														<input type="hidden" id="customerNricId"
															name="customerNricIdSession"
															value="<c:out value="${element.ProductType}" />" />
														<input type="hidden" id="printStatus"
															name="printStatusSession"
															value="<c:out value="${element.printStatus}" />" />

														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-6">
																	<div class="form-horizontal">

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Search
																				by Date</label>
																			<div class="col-sm-4">

																				<fmt:parseDate value="${now}" var="parsedEmpDate"
																					pattern="dd/MM/yyyy" />

																				<input type="text" placeholder="Date From"
																					class="form-control" id="datepicker1"
																					name="dateFrom"
																					<c:choose>
																					    <c:when test="${not empty sessionScope.dateFrom}">
																					         value="<c:out value="${sessionScope.dateFrom}" />"																					     
																					    </c:when>
																					    <c:otherwise>
																					          value="<%= df.format(new java.util.Date())%>"
																					    </c:otherwise>
																					</c:choose> />
																			</div>
																			<div class="col-sm-5">
																				<input type="text" placeholder="Date To"
																					class="form-control" id="datepicker2" name="dateTo"
																					<c:choose>
																					    <c:when test="${not empty sessionScope.dateTo}">
																					         value="<c:out value="${sessionScope.dateTo}" />"																					     
																					    </c:when>
																					    <c:otherwise>
																					          value="<%= df.format(new java.util.Date())%>"
																					    </c:otherwise>
																					</c:choose> />

																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Vehicle
																				Registration No.</label>
																			<div class="col-sm-9">
																				<input type="text" name="plateNo" placeholder=""
																					class="form-control" value="<c:out value="" />" />
																			</div>
																		</div>
																		<div class="form-group">
																			<!-- <label class="col-sm-3 control-label">Refund
																				Status</label>
																			  <div class="col-sm-9">
																				<select class="form-control" name="refundStatus"
																					id="refundStatus">
																					<option value="" selected>-View All-</option>
																					<option value="Printed">Refund Processed</option>
																					<option value="Unable to Print">Refund Not Processed</option>
																				</select>
																			</div>
																		 -->
																		</div>
																	</div>
																</div>

																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">NRIC/ID
																				No.</label>
																			<div class="col-sm-9">
																				<input type="text" placeholder=""
																					class="form-control" name="NRIC"
																					value="<c:out value="" />" />
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Policy/Certificate
																				No</label>

																			<div class="col-sm-9">
																				<input type="text" placeholder=""
																					class="form-control" name="policyNo" value="" />
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col-sm-12 text-right">
																	<button type="submit" class="btn btn-default"
																		name="SEARCH">Search</button>
																</div>
															</div>
														</div>
													</form:form>
													<!-- END FORM -->
												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="row">
																<div class="col-xs-12 form-inline">
																	<div class="col-xs-6"></div>
																	<div class="col-xs-6">
																		<div class="row">
																			<c:if test="${not empty roadTaxTxnReportList}">
																				<div class="pull-right" id='btnhere'>
																					<a href="generateExcelTxnReport"><button
																							class="btn btn-warning btn-sm">
																							Export to XLS <i class="fa fa-download"
																								aria-hidden="true"></i>
																						</button></a>
																				</div>
																			</c:if>
																		</div>
																	</div>
																</div>
															</div>
															<div class="table-responsive" id="MyTable">
																<table
																	class="table table-striped table-warning table-hover"
																	id="admin-datatable-second_admin">
																	<thead>
																		<tr>
																			<th>No</th>
																			<th>Date and Time</th>
																			<!-- <th>Policy No</th> -->
																			<!--  <th>Insured Name</th>
																			<th>NRIC</th>
																			<th>Car Reg.No</th> -->
																			<th>Insured Info</th>
																			<th>Road Tax Period (Month)</th>
																			<!-- <th>Name of Recipient</th>
																			<th>Delivery Address</th>
																			<th>Postcode</th>
																			<th>Mobile No.</th>  -->
																			<th>Recipient Details</th>
																			<th>Aging (Days)</th>
																			<th>Road Tax Printing</th>
																			<!-- <th>Road Tax Amount Collected (RM)</th>
																			<th>Actual Road Tax Amount (RM)</th>  -->
																			<th>Road Tax Amount (RM)</th>
																			<!-- <th>Remarks (For failed road tax printing)</th> -->
																			<th>Action</th>
																		</tr>
																	</thead>
																	<tbody>
																		<c:if test="${not empty roadTaxTxnReportList}">
																			<c:forEach items="${roadTaxTxnReportList}"
																				var="element" varStatus="theCount">
																				<tr>
																					<td align="center" class="width10"></td>
																					<td align="left"><c:out
																							value="${element.dateAndTime}" /></td>
																					<%-- <td align="left"><c:out
																							value="${element.policyNumber}" /></td> --%>

																					<td align="left"><B>Name:</B>&nbsp;<c:out
																							value="${element.customerName}" /><br> <B>ID
																							No:</B>&nbsp;<c:out value="${element.customerNricId}" /><br>
																						<B>Vehicle Reg. No:</B>&nbsp;<c:out
																							value="${element.regNo}" /><br> <B>Policy
																							No:</B>&nbsp;<c:out value="${element.policyNumber}" /><br>
																						<B>JPJ Status:</B>&nbsp; <c:choose>
																							<c:when test="${empty element.message}">
																								N/A
																							</c:when>
																							<c:otherwise>
																								<c:out value="${element.message}" />
																							</c:otherwise>
																						</c:choose> <input type="hidden" id="policyNumber"
																						name="policyNumber"
																						value="<c:out value="${element.policyNumber}"/>" />


																					</td>

																					<td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<c:out
																							value="${element.period}" /></td>

																					<td align="left"><B>Name:</B>&nbsp;<c:out
																							value="${element.deliveryName}" /><br> <B>Address:</B>&nbsp;<c:out
																							value="${element.address1}" /><br> &nbsp;<c:out
																							value="${element.address2}" /><br> &nbsp;<c:out
																							value="${element.address3}" /><br> <B>State:&nbsp;</B>
																						<c:out value="${element.rtstate}" /><br> <B>Postcode:</B>&nbsp;<c:out
																							value="${element.postcode}" /><br> <B>Phone
																							No:</B>&nbsp;<c:out value="${element.mobile}" /><br>
																					</td>

																					<td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<c:out
																							value="${element.aging}" /></td>

																					<td align="left"><c:if
																							test="${element.printStatus eq 'SUCCESS'}">
																							<c:out value="${element.remarks}"></c:out>
																						</c:if> <c:if test="${element.refundStatus eq 'SUCCESS'}">
																							<br>
																							<c:out value="${element.remarks}"></c:out>
																							<c:out value="${element.jpjStatus}"></c:out>
																						</c:if></td>
																					<td align="left"><B>Collected:</B>&nbsp;<%-- <c:out
																							value="${element.roadTaxAmount}" /> --%> <%-- <fmt:formatNumber
																							type="number" pattern="#,###.00"
																							value="${element.roadTaxAmount}" /> --%> <fmt:formatNumber
																							type="number" pattern="#,###.00"
																							value="${element.toatalRefundAmount}" /><br>
																						<B>Actual:</B>&nbsp;<%-- <c:out
																							value="${element.actualAmount}" /> --%> <fmt:formatNumber
																							type="number" pattern="#,###.00"
																							value="${element.actualAmount}" /><br></td>

																					<%--  <td align="left"><c:out value="${element.remarks}"/></td>	 --%>

																					<td align="center">
																						<body link="blue">
																							<p>
																								<a href="#" class="sendEmail">Send Refund
																									Email</a>
																							</p>

																							<%-- <p>
																								<a
																									href="generateRefundEmail/${element.policyNumber}">Send
																									Refund Email</a>
																							</p>  --%>
																							<!-- <p>
																								<a class="sendEmail">Send Refund Email</a>
																							</p> -->
																						</body>
																					</td>
																				</tr>
																			</c:forEach>
																		</c:if>
																	</tbody>
																</table>
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>
												<%-- </c:if> --%>
											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="../../pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>

	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->

	<script type="text/javascript">
		/* ------------------------------------------------------------------*/
		/* Start - Email Info                                                */
		/*-------------------------------------------------------------------*/

		$('.sendEmail')
				.click(
						function() {

							var $tr = $(this).closest('tr');
							var myRow = $tr.index();
							var policyNumber = $tr.find('td:eq(2)').find(
									'#policyNumber').val();
							//console.log(policyNumber);

							$
									.ajax({

										type : "post",
										beforeSend : function() {
											$('.loader').addClass("is-active");
										},
										url : '${pageContext.request.contextPath}/generateRefundEmail',
										data : {
											policyNumber : policyNumber
										},

										success : function(data) {
											console.log("val");

											obj = JSON.parse(data);
											if (obj.statusResponse == "Done") {
												successAlert('Email Successfully Sent');
											} else {
												errorAlert('Could Not Send Email');
											}
										},
										complete : function() {
											$('.loader').removeClass(
													"is-active");
										},
										error : function(request, status, error) {
											alert(request.responseText);
										}
									});

						});
		/* ------------------------------------------------------------------*/
		/* End - Email Info                                                */
		/*-------------------------------------------------------------------*/

		/* ------------------------------------------------------------------*/
		/* Start - Column sorting with table header Info                     */
		/*-------------------------------------------------------------------*/
		$(document)
				.ready(
						function() {
							var t = $('#admin-datatable-second_admin')
									.DataTable(
											{

												dom : 'Bfrtip',
												buttons : [
														{
															extend : 'pageLength',
															className : 'btn btn-secondary btn-sm active'
														},
														{
															extend : 'print',
															className : 'btn btn-warning btn-sm',
															title : 'Etiqa Transaction Report',
															text : 'Print <i class="fa fa-print" aria-hidden="true"></i>'
														}
												//,
												//{
												// extend: 'excel',
												//filename: 'Etiqa Transaction Report',
												// className: 'btn btn-warning btn-sm',
												//text:'Export to XLS <i class="fa fa-download" aria-hidden="true"></i>'
												// }
												],
												"searching" : false,
												"columnDefs" : [ {
													"searchable" : false,
													"orderable" : false,
													"targets" : [ 0, 2, 3, 4,
															5, 6, 7, 8 ],
													"bSortable" : false

												} ],
												"order" : [ [ 0, 'asc' ] ]
											});

							t.on('order.dt search.dt', function() {
								t.column(0, {
									search : 'applied',
									order : 'applied'
								}).nodes().each(function(cell, i) {
									cell.innerHTML = i + 1;
								});
							}).draw();

							t.buttons(0, null).containers()
									.appendTo('#btnhere');

						});
		/* ------------------------------------------------------------------ */
		/* End - Column sorting with table header Info                        */
		/*-------------------------------------------------------------------*/
	</script>




</body>
</html>