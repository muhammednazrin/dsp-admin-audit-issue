
<div class="col-sm-3 col-md-2 fluid-menu">
	<div class="sidebar-left sidebar-nicescroller admin">
		<!-- desktop menu -->
		<ul class="sidebar-menu admin">

			<!--  <li>
                                        <a href="#">
                                            <span class="icon-sidebar icon services"></span>
                                            <i class="fa fa-angle-right chevron-icon-sidebar"></i>
                                            System Administration
                                        </a>
                                        <ul class="submenu" >
                                            <li><a href="admin-system-user-role.html">User Roles &amp; Permission</a></li>
                                            <li><a href="admin-system-audit-log.html">Audit Log Maintenance</a></li>
                                            <li class="active"><a href="admin-system-audit-trail.html">Audit Trail/Log Report</a></li>
                                            <li><a href="admin-system-log-report.html">Integration Log Report</a></li>
                                        </ul>
                                    </li> -->

			<li class="submenu">
			<li><a href="#"> <span class="icon-sidebar icon policy"></span>
					<i class="fa fa-angle-right chevron-icon-sidebar"></i> Road Tax
					Renewal
			</a>
				<ul class="submenu">
					<!--  <li><a href="#fakelink">Postcode Maintenance</a></li> 
                                                                                    
                                           <li><a href="#fakelink">Rate Maintenance</a></li>   -->

					<li><a href="${pageContext.request.contextPath}/printing">Printing
							and Delivery</a></li>
					<li><a
						href="${pageContext.request.contextPath}/roadTaxTxnReport">Road
							Tax Report</a></li>
					<li><a
						href="${pageContext.request.contextPath}/roadTaxRefReport">Refund
							Report</a></li>

				</ul></li>

		</ul>
	</div>
	<!-- /.sidebar-left -->
</div>