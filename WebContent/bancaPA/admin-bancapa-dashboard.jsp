<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="com.etiqa.model.report.TransactionalReport"%>
<%@ page import="java.io.*,java.util.*"%>
<%@ page import="javax.servlet.*,java.text.*"%>
<%--  <%if (session.getAttribute("user") == null) {  response.sendRedirect(request.getContextPath() + "/admin-login.jsp"); }%> --%>



<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="../siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<%--  <link href="${pageContext.request.contextPath}/plugins/c3-chart/c3.min.css" rel="stylesheet"> --%>


</head>
<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">

		<jsp:include page="bancaHeader.jsp" />
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<jsp:include page="menu-banca.jsp" />

					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Dashboard</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">

												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<div class="col-sm-6">
																<h4>
																	Daily New Business Summary (<%
															
															 Date dNow = new Date();
															SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
															out.print(ft.format(dNow)); 
														%>)
																</h4>
															</div>

														</div>


														<div class="content-inner">
															<div class="row">
																<!-- row -->
																<div class="col-sm-6">
																	<!-- group 1 -->
																	<div class="row">
																		<div class="col-sm-6">
																			<div class="row">
																				<div class="col-sm-12">
																					<!-- BEGIN TODAY VISITOR TILES -->
																					<div
																						class="panel panel-primary panel-square panel-no-border">
																						<div class="panel-body">
																							<p class="cat-head text-center">Premier PA
																								Plus</p>
																							<h1 class="bolded tiles-number text-center">
																								<c:choose>
																									<c:when
																										test="${dashBoard.f_WTC_TotalPoliciesSold>'0'}">
																										<c:out
																											value="${dashBoard.f_WTC_TotalPoliciesSold}" />
																									</c:when>
																									<c:otherwise>
																										<c:out value="0" />
																									</c:otherwise>
																								</c:choose>
																							</h1>
																							<p class="text-center text20">
																								<strong>RM <c:choose>
																										<c:when
																											test="${dashBoard.f_WTC_Sold_Amount>'0'}">
																											<c:out value="${dashBoard.f_WTC_Sold_Amount}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>
																								</strong>
																							</p>
																							<div class="gap gap-mini"></div>
																							<p>
																								Total Policy(MTD) <span class="pull-right">
																									<c:choose>
																										<c:when
																											test="${dashBoard.f_WTC_TotalPoliciesSold_PM>'0'}">
																											<c:out
																												value="${dashBoard.f_WTC_TotalPoliciesSold_PM}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>
																								</span>
																							</p>
																							<p>
																								Total Policy(YTD) <span class="pull-right">

																									<c:choose>
																										<c:when
																											test="${dashBoard.f_WTC_TotalPoliciesSold_PY>'0'}">
																											<c:out
																												value="${dashBoard.f_WTC_TotalPoliciesSold_PY}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>
																								</span>
																							</p>
																							<div class="gap gap-mini"></div>
																							<p class="small">
																								Prem/Cont. (MTD) <span class="pull-right">RM

																									<c:choose>
																										<c:when
																											test="${dashBoard.f_WTC_Sold_Amount_M>'0'}">
																											<c:out
																												value="${dashBoard.f_WTC_Sold_Amount_M}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>
																								</span>
																							</p>
																							<p class="small">
																								Prem/Cont. (YTD) <span class="pull-right">RM

																									<c:choose>
																										<c:when
																											test="${dashBoard.f_WTC_Sold_Amount_Y>'0'}">
																											<c:out
																												value="${dashBoard.f_WTC_Sold_Amount_Y}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>
																								</span>
																							</p>
																						</div>
																						<!-- /.panel-body -->
																					</div>
																					<!-- /.panel panel-success panel-block-color -->
																					<!-- END TODAY VISITOR TILES -->
																				</div>
																				<div class="col-sm-12">
																					<!-- BEGIN TODAY VISITOR TILES -->
																					<div
																						class="panel panel-primary panel-square panel-no-border">
																						<div class="panel-body">
																							<p class="cat-head text-center">EIB</p>
																							<h1 class="bolded tiles-number text-center">

																								<c:choose>
																									<c:when
																										test="${dashBoard.f_WTC_TotalPoliciesSold_EIB>'0'}">
																										<c:out
																											value="${dashBoard.f_WTC_TotalPoliciesSold_EIB}" />
																									</c:when>
																									<c:otherwise>
																										<c:out value="0" />
																									</c:otherwise>
																								</c:choose>
																							</h1>
																							<p class="text-center text20">
																								<strong>RM <c:choose>
																										<c:when
																											test="${dashBoard.f_WTC_Sold_Amount_EIB>'0'}">
																											<c:out
																												value="${dashBoard.f_WTC_Sold_Amount_EIB}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>
																								</strong>
																							</p>
																						</div>
																						<!-- /.panel-body -->
																					</div>
																					<!-- /.panel panel-success panel-block-color -->
																					<!-- END TODAY VISITOR TILES -->
																				</div>
																				<div class="col-sm-12">
																					<!-- BEGIN TODAY VISITOR TILES -->
																					<div
																						class="panel panel-primary panel-square panel-no-border">
																						<div class="panel-body">
																							<p class="cat-head text-center">ETB</p>
																							<h1 class="bolded tiles-number text-center">

																								<c:choose>
																									<c:when
																										test="${dashBoard.f_WTC_TotalPoliciesSold_ETB>'0'}">
																										<c:out
																											value="${dashBoard.f_WTC_TotalPoliciesSold_ETB}" />
																									</c:when>
																									<c:otherwise>
																										<c:out value="0" />
																									</c:otherwise>
																								</c:choose>
																							</h1>
																							<p class="text-center text20">
																								<strong>RM <c:choose>
																										<c:when
																											test="${dashBoard.f_WTC_Sold_Amount_ETB>'0'}">
																											<c:out
																												value="${dashBoard.f_WTC_Sold_Amount_ETB}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>

																								</strong>
																							</p>
																						</div>
																						<!-- /.panel-body -->
																					</div>
																					<!-- /.panel panel-success panel-block-color -->
																					<!-- END TODAY VISITOR TILES -->
																				</div>
																			</div>
																		</div>
																		<!-- /.col-sm-6 -->
																		<div class="col-sm-6">
																			<div class="row">
																				<div class="col-sm-12">
																					<!-- BEGIN TODAY VISITOR TILES -->
																					<div
																						class="panel panel-danger panel-square panel-no-border">
																						<div class="panel-body">
																							<p class="cat-head text-center">Takaful PA
																								Plus</p>
																							<h1 class="bolded tiles-number text-center">

																								<c:choose>
																									<c:when
																										test="${dashBoard.f_HOHH_TotalPoliciesSold>=0}">
																										<c:out
																											value="${dashBoard.f_HOHH_TotalPoliciesSold}" />
																									</c:when>
																									<c:otherwise>
																										<c:out value="0" />
																									</c:otherwise>
																								</c:choose>
																							</h1>
																							<p class="text-center text20">
																								<strong>RM <c:choose>
																										<c:when
																											test="${dashBoard.f_HOHH_Sold_Amount>'0'}">
																											<c:out
																												value="${dashBoard.f_HOHH_Sold_Amount}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>

																								</strong>
																							</p>
																							<div class="gap gap-mini"></div>
																							<p>
																								Total Policy(MTD) <span class="pull-right">
																									<c:choose>
																										<c:when
																											test="${dashBoard.f_HOHH_TotalPoliciesSold_PM>'0'}">
																											<c:out
																												value="${dashBoard.f_HOHH_TotalPoliciesSold_PM}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>

																								</span>
																							</p>
																							<p>
																								Total Policy(YTD) <span class="pull-right">
																									<c:choose>
																										<c:when
																											test="${dashBoard.f_HOHH_TotalPoliciesSold_PY>'0'}">
																											<c:out
																												value="${dashBoard.f_HOHH_TotalPoliciesSold_PY}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>
																								</span>
																							</p>
																							<div class="gap gap-mini"></div>
																							<p class="small">
																								Prem./Cont. (MTD) <span class="pull-right">RM
																									<c:choose>
																										<c:when
																											test="${dashBoard.f_HOHH_Sold_Amount_M>'0'}">
																											<c:out
																												value="${dashBoard.f_HOHH_Sold_Amount_M}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>
																								</span>
																							</p>
																							<p class="small">
																								Prem./Cont. (YTD) <span class="pull-right">RM
																									<c:choose>
																										<c:when
																											test="${dashBoard.f_HOHH_Sold_Amount_Y>'0'}">
																											<c:out
																												value="${dashBoard.f_HOHH_Sold_Amount_Y}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>
																								</span>
																							</p>
																						</div>
																						<!-- /.panel-body -->
																					</div>
																					<!-- /.panel panel-success panel-block-color -->
																					<!-- END TODAY VISITOR TILES -->
																				</div>
																				<div class="col-sm-12">
																					<!-- BEGIN TODAY VISITOR TILES -->
																					<div
																						class="panel panel-danger panel-square panel-no-border">
																						<div class="panel-body">
																							<p class="cat-head text-center">EIB</p>
																							<h1 class="bolded tiles-number text-center">
																								<c:choose>
																									<c:when
																										test="${dashBoard.f_HOHH_TotalPoliciesSold_EIB>'0'}">
																										<c:out
																											value="${dashBoard.f_HOHH_TotalPoliciesSold_EIB}" />
																									</c:when>
																									<c:otherwise>
																										<c:out value="0" />
																									</c:otherwise>
																								</c:choose>

																							</h1>
																							<p class="text-center text20">
																								<strong>RM <c:choose>
																										<c:when
																											test="${dashBoard.f_HOHH_Sold_Amount_EIB>'0'}">
																											<c:out
																												value="${dashBoard.f_HOHH_Sold_Amount_EIB}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>
																								</strong>
																							</p>
																						</div>
																						<!-- /.panel-body -->
																					</div>
																					<!-- /.panel panel-success panel-block-color -->
																					<!-- END TODAY VISITOR TILES -->
																				</div>
																				<div class="col-sm-12">
																					<!-- BEGIN TODAY VISITOR TILES -->
																					<div
																						class="panel panel-danger panel-square panel-no-border">
																						<div class="panel-body">
																							<p class="cat-head text-center">ETB</p>
																							<h1 class="bolded tiles-number text-center">

																								<c:choose>
																									<c:when
																										test="${dashBoard.f_HOHH_TotalPoliciesSold_ETB>'0'}">
																										<c:out
																											value="${dashBoard.f_HOHH_TotalPoliciesSold_ETB}" />
																									</c:when>
																									<c:otherwise>
																										<c:out value="0" />
																									</c:otherwise>
																								</c:choose>
																							</h1>
																							<p class="text-center text20">
																								<strong>RM <c:choose>
																										<c:when
																											test="${dashBoard.f_HOHH_Sold_Amount_ETB>'0'}">
																											<c:out
																												value="${dashBoard.f_HOHH_Sold_Amount_ETB}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>




																								</strong>
																							</p>
																						</div>
																						<!-- /.panel-body -->
																					</div>
																					<!-- /.panel panel-success panel-block-color -->
																					<!-- END TODAY VISITOR TILES -->
																				</div>
																			</div>

																		</div>
																		<!-- /.col-sm-6 -->
																	</div>
																</div>
																<div class="col-sm-6">
																	<!-- group 2 -->
																	<div class="row">
																		<div class="col-sm-6">
																			<!-- BEGIN TODAY VISITOR TILES -->
																			<div
																				class="panel panel-warning panel-square panel-no-border">
																				<div class="panel-body">
																					<p class="cat-head text-center">Takaful
																						PesonaLady</p>
																					<h1 class="bolded tiles-number text-center">

																						<c:choose>
																							<c:when
																								test="${dashBoard.f_MI_TotalPoliciesSold>'0'}">
																								<c:out
																									value="${dashBoard.f_MI_TotalPoliciesSold}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose>
																					</h1>
																					<p class="text-center text20">
																						<strong>RM <c:choose>
																								<c:when
																									test="${dashBoard.f_Motor_Sold_Amount>'0'}">
																									<c:out value="${dashBoard.f_Motor_Sold_Amount}" />
																								</c:when>
																								<c:otherwise>
																									<c:out value="0" />
																								</c:otherwise>
																							</c:choose>

																						</strong>
																					</p>
																					<div class="gap gap-mini"></div>
																					<p>
																						Total Policy(MTD) <span class="pull-right">
																							<c:choose>
																								<c:when
																									test="${dashBoard.f_MI_TotalPoliciesSold_PM>'0'}">
																									<c:out
																										value="${dashBoard.f_MI_TotalPoliciesSold_PM}" />
																								</c:when>
																								<c:otherwise>
																									<c:out value="0" />
																								</c:otherwise>
																							</c:choose>

																						</span>
																					</p>
																					<p>
																						Total Policy(YTD) <span class="pull-right">
																							<c:choose>
																								<c:when
																									test="${dashBoard.f_MI_TotalPoliciesSold_PY>'0'}">
																									<c:out
																										value="${dashBoard.f_MI_TotalPoliciesSold_PY}" />
																								</c:when>
																								<c:otherwise>
																									<c:out value="0" />
																								</c:otherwise>
																							</c:choose>

																						</span>


																					</p>
																					<div class="gap gap-mini"></div>
																					<p class="small">
																						Prem./Cont. (MTD) <span class="pull-right">RM
																							<c:choose>
																								<c:when
																									test="${dashBoard.f_Motor_Sold_Amount_M>'0'}">
																									<c:out
																										value="${dashBoard.f_Motor_Sold_Amount_M}" />
																								</c:when>
																								<c:otherwise>
																									<c:out value="0" />
																								</c:otherwise>
																							</c:choose>

																						</span>
																					</p>
																					<p class="small">
																						Prem./Cont. (YTD) <span class="pull-right">RM
																							<c:choose>
																								<c:when
																									test="${dashBoard.f_Motor_Sold_Amount_Y>'0'}">
																									<c:out
																										value="${dashBoard.f_Motor_Sold_Amount_Y}" />
																								</c:when>
																								<c:otherwise>
																									<c:out value="0" />
																								</c:otherwise>
																							</c:choose>
																						</span>
																					</p>
																				</div>
																				<!-- /.panel-body -->
																			</div>
																			<!-- /.panel panel-success panel-block-color -->
																			<!-- END TODAY VISITOR TILES -->
																			<div
																				class="panel panel-warning panel-square panel-no-border">
																				<div class="panel-body">
																					<p class="cat-head text-center">EIB</p>
																					<h1 class="bolded tiles-number text-center">
																						<c:choose>
																							<c:when
																								test="${dashBoard.f_MI_TotalPoliciesSold_EIB>'0'}">
																								<c:out
																									value="${dashBoard.f_MI_TotalPoliciesSold_EIB}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose>

																					</h1>
																					<p class="text-center text20">
																						<strong>RM <c:choose>
																								<c:when
																									test="${dashBoard.f_MI_Sold_Amount_EIB>'0'}">
																									<c:out
																										value="${dashBoard.f_MI_Sold_Amount_EIB}" />
																								</c:when>
																								<c:otherwise>
																									<c:out value="0" />
																								</c:otherwise>
																							</c:choose>
																						</strong>
																					</p>
																				</div>
																				<!-- /.panel-body -->
																			</div>
																			<div
																				class="panel panel-warning panel-square panel-no-border">
																				<div class="panel-body">
																					<p class="cat-head text-center">ETB</p>
																					<h1 class="bolded tiles-number text-center">
																						<c:choose>
																							<c:when
																								test="${dashBoard.f_MT_TotalPoliciesSold_ETB>'0'}">
																								<c:out
																									value="${dashBoard.f_MT_TotalPoliciesSold_ETB}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose>

																					</h1>
																					<p class="text-center text20">
																						<strong>RM <c:choose>
																								<c:when
																									test="${dashBoard.f_MT_Sold_Amount_ETB>'0'}">
																									<c:out
																										value="${dashBoard.f_MT_Sold_Amount_ETB}" />
																								</c:when>
																								<c:otherwise>
																									<c:out value="0" />
																								</c:otherwise>
																							</c:choose>
																						</strong>
																					</p>
																				</div>
																				<!-- /.panel-body -->
																			</div>
																		</div>
																		<!-- /.col-sm-6 -->
																		<div class="col-sm-6">
																			<!-- BEGIN TODAY VISITOR TILES -->
																			<div
																				class="panel panel-success panel-square panel-no-border">
																				<div class="panel-body">
																					<p class="cat-head text-center">Takaful Hero 15</p>
																					<h1 class="bolded tiles-number text-center">

																						<c:choose>
																							<c:when
																								test="${dashBoard.f_TL_TotalPoliciesSold>'0'}">
																								<c:out
																									value="${dashBoard.f_TL_TotalPoliciesSold}" />
																							</c:when>
																							<c:otherwise>
																								<c:out value="0" />
																							</c:otherwise>
																						</c:choose>

																					</h1>
																					<p class="text-center text20">
																						<strong>RM <c:choose>
																								<c:when test="${dashBoard.f_TL_Sold_Amount>'0'}">
																									<c:out value="${dashBoard.f_TL_Sold_Amount}" />
																								</c:when>
																								<c:otherwise>
																									<c:out value="0" />
																								</c:otherwise>
																							</c:choose>

																						</strong>
																					</p>
																					<div class="gap gap-mini"></div>
																					<p>
																						Total Policy(MTD) <span class="pull-right">
																							<c:choose>
																								<c:when
																									test="${dashBoard.f_TL_TotalPoliciesSold_PM>'0'}">
																									<c:out
																										value="${dashBoard.f_TL_TotalPoliciesSold_PM}" />
																								</c:when>
																								<c:otherwise>
																									<c:out value="0" />
																								</c:otherwise>
																							</c:choose>



																						</span>
																					</p>
																					<p>
																						Total Policy(YTD) <span class="pull-right">
																							<c:choose>
																								<c:when
																									test="${dashBoard.f_TL_TotalPoliciesSold_PY>'0'}">
																									<c:out
																										value="${dashBoard.f_TL_TotalPoliciesSold_PY}" />
																								</c:when>
																								<c:otherwise>
																									<c:out value="0" />
																								</c:otherwise>
																							</c:choose>
																						</span>
																					</p>
																					<div class="gap gap-mini"></div>
																					<p class="small">
																						Prem./Cont. (MTD) <span class="pull-right">RM
																							<c:choose>
																								<c:when
																									test="${dashBoard.f_TL_Sold_Amount_M>'0'}">
																									<c:out value="${dashBoard.f_TL_Sold_Amount_M}" />
																								</c:when>
																								<c:otherwise>
																									<c:out value="0" />
																								</c:otherwise>
																							</c:choose>

																						</span>
																					</p>
																					<p class="small">
																						Prem./Cont. (YTD) <span class="pull-right">RM
																							<c:choose>
																								<c:when
																									test="${dashBoard.f_TL_Sold_Amount_Y>'0'}">
																									<c:out value="${dashBoard.f_TL_Sold_Amount_Y}" />
																								</c:when>
																								<c:otherwise>
																									<c:out value="0" />
																								</c:otherwise>
																							</c:choose>
																						</span>
																					</p>
																				</div>
																				<!-- /.panel-body -->
																			</div>
																			<!-- /.panel panel-success panel-block-color -->
																			<!-- END TODAY VISITOR TILES -->

																		</div>

																		<!-- /.col-sm-6 -->
																	</div>

																	<!--end row -->


																</div>
																<div class="row">
																	<!-- 								<div id="chartContainer" style="height: 150px; width: 100%;"></div>
 											    <div id="chartContainerYear" style="height: 150px; width: 100%;"></div> -->

																	<div class="col-sm-12">
																		<div class="row">
																			<div class="col-sm-8">
																				<div class="the-box">
																					<div class="row">
																						<div class="col-sm-6">
																							<p class="small-title cat-head-2">Performance
																								by Entity</p>
																						</div>
																						<div class="col-sm-6">
																							<select id="DataType" class="form-control">
																								<option value="dataMonth">Premium/Contribution
																									(MTD)</option>
																								<option value="dataYear">Premium/Contribution
																									(YTD)</option>
																							</select>
																						</div>
																					</div>
																					<div id="entity" style="height: 300px;"></div>
																				</div>
																			</div>
																			<!-- /.col-sm-9 -->
																			<div class="col-sm-4">
																				<div class="the-box">
																					<p class="small-title cat-head-2 text-center">Product
																						Campaign Analysis (MTD)</p>
																					<div id="productcampaign" style="height: 295px;"></div>
																				</div>
																			</div>
																			<!-- /.col-sm-6 -->
																		</div>
																	</div>
																</div>
																<!--end row -->

																<div class="row">
																	<div class="col-sm-12">
																		<div>
																			<div class="the-box">
																				<p class="small-title cat-head-2 text-center">Renewal
																					Retention(MTD)</p>
																				<div id="renewalretention" style="height: 295px;"></div>
																			</div>
																		</div>

																	</div>
																</div>

																<!-- <div class="col-sm-12">
                                                                <div class="row">
                                                                    <div class="col-sm-8">
                                                                        <div class="the-box">
                                                                            <div class="row">
                                                                                <div class="col-sm-6">
                                                                                    <p class="small-title cat-head-2">Performance by Entity</p>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <select id="DataType" class="form-control">
                                                                                        <option value="dataMonth">Premium/Contribution (MTD)</option>
                                                                                        <option value="dataYear">Premium/Contribution (YTD)</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div id="entity" style="height: 300px;"></div>
                                                                        </div>
                                                                    </div>/.col-sm-9
                                                                    <div class="col-sm-4">
                                                                            <div class="the-box">
                                                                                <p class="small-title cat-head-2 text-center">Product Campaign Analysis (MTD)</p>
                                                                                <div id="productcampaign" style="height: 295px;"></div>
                                                                            </div>
                                                                    </div>/.col-sm-6
                                                                </div>
                                                            </div> -->
																<!-- /.col-sm-12 -->
															</div>
															<!--end row -->
															<div class="row">
																<div class="col-sm-12">
																	<div class="the-box">
																		<p class="small-title cat-head-2">Current Month
																			Performance Trend</p>
																		<div id="cmpt" style="height: 300px;"></div>
																	</div>
																</div>
																<!-- /.col-sm-12 -->
															</div>
															<!--end row -->
															<div class="row">
																<div class="col-sm-12">
																	<div class="the-box">
																		<div class="row">
																			<div class="col-sm-6">
																				<p class="small-title cat-head-2">Sales Funnel</p>
																			</div>
																			<div class="col-sm-6">
																				<select id="FunnelType" class="form-control">
																					<option value="dataf">Motor Insurance</option>
																					<option value="datata">WTC Insurance</option>
																				</select>
																			</div>
																		</div>
																		<div id="funel2" style="height: 330px;"></div>
																		<div class="row">
																			<div class="col-sm-4">
																				<div
																					class="panel panel-success panel-square panel-no-border">
																					<div class="panel-body">
																						<p class="cat-head text-center">50 Successful
																							Transactions</p>
																					</div>
																					<!-- /.panel-body -->
																				</div>
																			</div>
																			<div class="col-sm-8">
																				<table class="table table-bordered">
																					<thead>
																						<tr class="active">
																							<th>Stage</th>
																							<th>Prospect</th>
																							<th>Lost</th>
																						</tr>
																					</thead>
																					<tbody>
																						<tr>
																							<td>Step 1</td>
																							<td>200</td>
																							<td>30</td>
																						</tr>
																						<tr>
																							<td>Step 2</td>
																							<td>200</td>
																							<td>30</td>
																						</tr>
																						<tr>
																							<td>Step 3</td>
																							<td>200</td>
																							<td>30</td>
																						</tr>
																					</tbody>
																				</table>
																			</div>
																		</div>
																	</div>
																</div>
																<!-- /.col-sm-8 -->
																<div class="col-sm-12">
																	<div class="row">
																		<div class="col-sm-12">
																			<div class="the-box">
																				<p class="small-title cat-head-2 text-center">Actual
																					vs Budget(MTD)</p>
																				<div id="actualbmtd" style="height: 200px;"></div>
																			</div>
																		</div>
																		<div class="col-sm-12">
																			<div class="the-box">
																				<p class="small-title cat-head-2 text-center">Actual
																					vs Budget(YTD)</p>
																				<div id="actualbytd" style="height: 200px;"></div>
																			</div>
																		</div>
																	</div>
																</div>
																<!-- /.col-sm-4 -->
															</div>
															<!--end row -->
															<div class="row">
																<div class="col-sm-12">
																	<div class="the-box">
																		<p class="small-title cat-head-2">Cyber Agent
																			Performance (YTD)</p>
																		<div id="cyberap" style="height: 300px;"></div>
																	</div>
																</div>
																<!-- /.col-sm-12 -->
															</div>
															<!--end row -->
															<div class="row">
																<div class="col-sm-12">
																	<div class="the-box">
																		<div class="row">
																			<div class="col-sm-6">
																				<p class="small-title cat-head-2">Product
																					Cross-Sell Ratio(MTD)</p>
																			</div>
																			<div class="col-sm-6">
																				<select id="Datacross" class="form-control">
																					<option value="CrossSell">Cross-Sell Ratio</option>
																					<option value="CrossUp">Up-Sell Ratio</option>
																				</select>
																			</div>
																		</div>
																		<div id="CrossRatio" style="height: 300px;"></div>
																	</div>
																</div>
																<!-- /.col-sm-12 -->
															</div>
															<!--end row -->
															<div class="row">
																<div class="col-sm-12">
																	<div class="the-box">
																		<p class="small-title cat-head-2">MOTOR :
																			Attachment Ratio(MTD)</p>
																		<div id="motorar" style="height: 300px;"></div>
																	</div>
																</div>
																<!-- /.col-sm-12 -->
															</div>
															<!--end row -->
															<div class="row">
																<div class="col-sm-8">
																	<div class="the-box">
																		<p class="small-title cat-head-2">Source of Leads
																			(MTD)</p>
																		<div class="row">
																			<div class="col-sm-4">
																				<table class="table table-bordered">
																					<thead>
																						<tr class="active">
																							<th>Point Of Sales</th>
																							<th>Total</th>
																						</tr>
																					</thead>
																					<tbody>
																						<tr>
																							<td>Direct</td>
																							<td>${dashBoard.f_SOL_Direct}</td>
																						</tr>
																						<tr>
																							<td>CWP</td>
																							<td>${dashBoard.f_SOL_CWP}</td>
																						</tr>
																						<tr>
																							<td>Email</td>
																							<td>${dashBoard.f_SOL_MAIL}</td>
																						</tr>
																						<tr>
																							<td>Cyber Agent Website</td>
																							<td>${dashBoard.f_SOL_NonDirect}</td>
																						</tr>
																					</tbody>
																				</table>
																			</div>
																			<div class="col-sm-8">
																				<div id="sol" style="height: 300px;"></div>
																			</div>
																		</div>
																	</div>
																</div>
																<!-- /.col-sm-6 -->
																<div class="col-sm-4">
																	<div class="the-box">
																		<p class="small-title cat-head-2">Abandonment vs
																			Follow up Rate(MTD)</p>
																		<div id="afur" style="height: 300px;"></div>
																	</div>
																</div>
																<!-- /.col-sm-6 -->
															</div>
															<!--end row -->
														</div>




													</div>

												</div>
											</div>
											<!--row -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.container -->

			</div>
			<!-- /.page-content -->
			<!-- BEGIN FOOTER -->
			<jsp:include page="../pageFooter.jsp" />

			<!-- END FOOTER -->
		</div>
		<!-- /.wrapper -->
		<!-- END PAGE CONTENT -->



		<!-- BEGIN BACK TO TOP BUTTON -->
		<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
		<!-- END BACK TO TOP -->


		<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

		<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
		<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
		<script
			src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/js/bootstrap-select.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/js/bootstrap-tabcollapse.js"></script>

		<script
			src="${pageContext.request.contextPath}/assets/plugins/retina/retina.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/nicescroll/jquery.nicescroll.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/backstretch/jquery.backstretch.min.js"></script>


		<!-- PLUGINS -->
		<script
			src="${pageContext.request.contextPath}/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/prettify/prettify.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

		<script
			src="${pageContext.request.contextPath}/assets/plugins/chosen/chosen.jquery.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/icheck/icheck.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/mask/jquery.mask.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/validator/bootstrapValidator.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.highlight.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/summernote/summernote.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/markdown/markdown.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/markdown/to-markdown.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/markdown/bootstrap-markdown.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/slider/bootstrap-slider.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/toastr/toastr.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/placeholder/jquery.placeholder.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/footable-3/js/footable.js"></script>

		<!-- C3 JS -->
		<script
			src="${pageContext.request.contextPath}/assets/plugins/c3-chart/d3.v3.min.js"
			charset="utf-8"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/c3-chart/c3.min.js"></script>

		<!-- highcharts JS -->
		<script
			src="${pageContext.request.contextPath}/assets/plugins/highcharts/highcharts.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/highcharts/modules/funnel.js"></script>

		<!-- KNOB JS -->
		<!--[if IE]>
        <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
		<script
			src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/jquery.knob.js"></script>
		<script
			src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/knob.js"></script>

		<!-- MAIN APPS JS -->
		<script src="${pageContext.request.contextPath}/assets/js/apps.js"></script>




		<script type="text/javascript">
$(document).ready(function() {
    
    // bar for Product Campaign Analysis (MTD)
    var chart = c3.generate({
    bindto: '#productcampaign',
    data: {
      columns: [
        ['Expenses', 32000],
        ['Revenue', 37000]
      ],
      types: {
        Expenses: 'bar',
        Revenue: 'bar'
      },
        
    },
        
    axis: {
      x: {
        type: 'categorized',
      }
    },
        
    });
    
    
    //chart for Renewal Retention(MTD)
    var chart = c3.generate({
    bindto: '#renewalretention',
    data: {
        // iris data from R
        columns: [
            ['WTC', 30],
            ['HOHH', 120],
            ['Motor', 120],
            ['TermLife', 120],
        ],
        type : 'pie',
        colors: {
            WTC: '#337ab7',
            HOHH: '#d9534f',
            Motor: '#f0ad4e',
            TermLife: '#5cb85c',
        },
    },
        
    legend: {
        show: true
        },
        
    });
    
    // Actual vs Budget(MTD)
  /*   var chart = c3.generate({
    bindto: '#actualbmtd',
        data: {
            x: 'x',
            columns: [
                ['x', 'WTC', 'HOHH', 'Motor', 'Term Life'],
                ['Actual', ${dashBoard.f_WTC_Sold_Amount_M}, ${dashBoard.f_HOHH_Sold_Amount_M}, ${dashBoard.f_Motor_Sold_Amount_M}, ${dashBoard.f_TL_Sold_Amount_M}],
                ['Budget', ${dashBoard.f_AVB_WTC_Budget}/12, ${dashBoard.f_AVB_HOHH_Budget}/12, ${dashBoard.f_AVB_Motor_Budget}/12, ${dashBoard.f_AVB_TL_Budget}/12],
                ],
            type: 'bar',
        },
        axis: {
            rotated: true,
          x: {
            type: 'categorized',
          }
        },
    }); */
    
    // Actual vs Budget(MTD)
   /*  var chart = c3.generate({
    bindto: '#actualbytd',
        data: {
            x: 'x',
            columns: [
                ['x', 'WTC', 'HOHH', 'Motor', 'Term Life'],
                 ['Actual', ${dashBoard.f_WTC_Sold_Amount_Y}, ${dashBoard.f_HOHH_Sold_Amount_Y}, ${dashBoard.f_Motor_Sold_Amount_Y}, ${dashBoard.f_TL_Sold_Amount_Y}],
                ['Budget', ${dashBoard.f_AVB_WTC_Budget}, ${dashBoard.f_AVB_HOHH_Budget}, ${dashBoard.f_AVB_Motor_Budget}, ${dashBoard.f_AVB_TL_Budget}],
                ],
            type: 'bar',
            scale:1000,
        },
        axis: {
            rotated: true,
          x: {
            type: 'categorized',
          }
        },
    }); */
    
    // Actual vs Budget(MTD)
   /*  var chart = c3.generate({
    bindto: '#cyberap',
        data: {
            x: 'x',
            columns: [
                ['x', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                ['Performance',${dashBoard.f_AgentPer_JAN},${dashBoard.f_AgentPer_FEB},${dashBoard.f_AgentPer_MAR},
                ${dashBoard.f_AgentPer_APR},
                ${dashBoard.f_AgentPer_MAY},
                ${dashBoard.f_AgentPer_JUN},
                ${dashBoard.f_AgentPer_JUL},
                ${dashBoard.f_AgentPer_AUG},
                ${dashBoard.f_AgentPer_SEP},
                ${dashBoard.f_AgentPer_OCT},
                ${dashBoard.f_AgentPer_NOV},
                ${dashBoard.f_AgentPer_DEC}],
                ],
        },
        axis: {
          x: {
            type: 'categorized',
          }
        },
    }); */
    
    // MOTOR : Attachment Ratio(MTD)
   /*  var chart = c3.generate({
    bindto: '#motorar',
        data: {
            x: 'x',
            columns: [
                ['x', 
                'Passenger Legal Liability', 
                'Liability for Acts of Negligence', 
                'Extended Flood Cover',
                'Basic Flood Cover',
                'NCD Relief',
                'Windscreen Coverage',
                'Vehicle Accessories',
                'NGV Gas',
                'Compensation for Assessed Repair Time',
                'ADD NAMED DRIVER',
                'Strike, Riot & Civil Commotion'],
                ['Performance', ${dashBoard.f_Motor_A_Ratio_01},${dashBoard.f_Motor_A_Ratio_02},
                ${dashBoard.f_Motor_A_Ratio_03},${dashBoard.f_Motor_A_Ratio_04},
                ${dashBoard.f_Motor_A_Ratio_05},${dashBoard.f_Motor_A_Ratio_06},
                ${dashBoard.f_Motor_A_Ratio_07},${dashBoard.f_Motor_A_Ratio_08},
                ${dashBoard.f_Motor_A_Ratio_09},${dashBoard.f_Motor_A_Ratio_10},
                ${dashBoard.f_Motor_A_Ratio_11}],
                ],
            type: 'bar',
        },
        axis: {
          x: {
            type: 'categorized',
          }
        },
    });
     */
    
    // Source of Leads (MTD)
   /*  var chart = c3.generate({
    bindto: '#sol',
        data: {
            columns: [
                ['Direct', ${dashBoard.f_SOL_Direct}],
                ['CWP', ${dashBoard.f_SOL_CWP}],
                ['Email', ${dashBoard.f_SOL_MAIL}],
                ['Cyber Agent Website', ${dashBoard.f_SOL_NonDirect}],
                ],
            type: 'donut',
        },
        axis: {
          x: {
            type: 'categorized',
          }
        },
        donut: {
            title: "Point Of Sale"
        }
    }); */
    
    // Abandonment vs Follow up Rate(MTD)
   /*  var colors = ['#e7e7e6', '#ffbf00'];
    var chart = c3.generate({
    bindto: '#afur',
        data: {
            x: 'x',
            columns: [
                ['x', 'Abandonment', 'Follow up'],
                ['MTD',${dashBoard.f_Aband_TotalCount} , ${dashBoard.f_Aband_TotalSuccess}],
                ],
            type: 'bar',
            color: function (color, d) {
                return colors[d.index];
            },
            labels: true
        },
        axis: {
          x: {
            type: 'category',
            categories: ['cat1', 'cat2', 'cat3', 'cat4', 'cat5', 'cat6', 'cat7', 'cat8', 'cat9']
          }
        },
        legend: {
            show: false
        }
    }); */
    
    /*  ['x', '2016-01-01', '2016-01-02', '2016-01-03', '2016-01-04', '2016-01-05', '2016-01-06', '2016-01-07', '2016-01-08'
                , '2016-01-09', '2016-01-10', '2016-01-11', '2016-01-12', '2016-01-13', '2016-01-14', '2016-01-15', '2016-01-16'
                , '2016-01-17', '2016-01-18', '2016-01-19', '2016-01-20', '2016-01-21', '2016-01-22', '2016-01-23'], */
    
    var chart = c3.generate({
          bindto: '#cmpt',
          data: {
            x: 'x',
            columns: [
                ['x', '2016-01-01', '2016-01-02', '2016-01-03', '2016-01-04', '2016-01-05', '2016-01-06', '2016-01-07', '2016-01-08'
                , '2016-01-09', '2016-01-10', '2016-01-11', '2016-01-12', '2016-01-13', '2016-01-14', '2016-01-15', '2016-01-16'
                , '2016-01-17', '2016-01-18', '2016-01-19', '2016-01-20', '2016-01-21', '2016-01-22', '2016-01-23'],
                ['WTC', 30, 200, 200, 400, 150, 250],
                ['HOHH', 10, 300, 300, 400, 150, 250],
                ['Motor', 40, 100, 300, 400, 150, 250],
                ['TermLife', 70, 500, 100, 400, 150, 250],
            ],
            //type: 'bar'
            colors: {
                WTC: '#337ab7',
                HOHH: '#d9534f',
                Motor: '#f0ad4e',
                TermLife: '#5cb85c',
            },
          },
          axis: {
            x: {
              type: 'timeseries',
              tick: {
                format: '%e %b %Y',
                //fit: false
              }
            }
          }
        });

});


//Performance by Entity

 /*  var dataMonth = [
    ['x', 'WTC', 'HOHH', 'Motor', 'Term Life'],
    ['EIB', ${dashBoard.f_Entity_WTC_TPS_PM}, ${dashBoard.f_Entity_HOHH_TPS_PM}, ${dashBoard.f_Entity_MI_TPS_PM}, ${dashBoard.f_Entity_TL_TPS_PM}],
    ['ETB', ${dashBoard.f_Entity_WTCT_TPS_PM}, ${dashBoard.f_Entity_HOHHT_TPS_PM}, ${dashBoard.f_Entity_MT_TPS_PM}, ${dashBoard.f_Entity_TLT_TPS_PM}],
    ];

    var dataYear = [
    ['x', 'WTC', 'HOHH', 'Motor', 'Term Life'],
    ['EIB', ${dashBoard.f_Entity_WTC_TPS_PY}, ${dashBoard.f_Entity_HOHH_TPS_PY}, ${dashBoard.f_Entity_MI_TPS_PY}, ${dashBoard.f_Entity_TL_TPS_PY}],
    ['ETB', ${dashBoard.f_Entity_WTCT_TPS_PY}, ${dashBoard.f_Entity_HOHHT_TPS_PY}, ${dashBoard.f_Entity_MT_TPS_PY}, ${dashBoard.f_Entity_TLT_TPS_PY}],
    ];
 */
 
/* $(function () {

    // Initial chart
    var chart = c3.generate({
        bindto: '#entity',
        data: {
            x: 'x',
            columns: dataMonth,
            type: 'bar',
        },
        axis: {
            rotated: true,
          x: {
            type: 'categorized',
          }
        },
    }); 

    // Redraw chart depending on which option is selected
    $("#DataType").change(function (evt) {
        var timeSelection = eval($("#DataType").val());
        var chart = c3.generate({
            bindto: '#entity',
            data: {
                x: 'x',
                columns: timeSelection,
                type: 'bar',
            },
            axis: {
                rotated: true,
              x: {
                type: 'categorized',
              }
            },
        });
    });

}); */


//Performance by Entity
    var CrossSell = [
    ['x', 'WTC', 'HOHH', 'Motor', 'Term'],
    ['WTC', 77, 100, 140, 100],
    ['HOHH', 40, 77, 140, 20],
    ['Motor', 40, 120, 77, 20],
    ['Term Life', 40, 120, 140, 77],
    ];

    var CrossUp = [
    ['x', 'WTC', 'HOHH', 'Motor', 'Term'],
    ['WTC', 77, 50, 50, 50],
    ['HOHH', 40, 77, 50, 20],
    ['Motor', 40, 10, 77, 20],
    ['Term Life', 40, 20, 10, 77],
    ];

$(function () {

    // Initial chart
    var chart = c3.generate({
        bindto: '#CrossRatio',
        data: {
            x: 'x',
            rows: CrossSell,
            type: 'bar',
            colors: {
                WTC: '#337ab7',
                HOHH: '#d9534f',
                Motor: '#f0ad4e',
                Term: '#5cb85c',
            },
            names: {
                WTC: 'WTC',
                HOHH: 'HOHH',
                Motor: 'Motor',
                Term: 'Term Life'
            }
        },
        axis: {
          x: {
            type: 'categorized',
          }
        },
    });

    // Redraw chart depending on which option is selected
    $("#Datacross").change(function (evt) {
        var timeSelection = eval($("#Datacross").val());
        var chart = c3.generate({
            bindto: '#CrossRatio',
            data: {
                x: 'x',
                rows: timeSelection,
                type: 'bar',
                colors: {
                    WTC: '#337ab7',
                    HOHH: '#d9534f',
                    Motor: '#f0ad4e',
                    Term: '#5cb85c',
                },
                names: {
                    WTC: 'WTC',
                    HOHH: 'HOHH',
                    Motor: 'Motor',
                    Term: 'Term Life'
                }
            },
            axis: {
              x: {
                type: 'categorized',
              }
            },
        });
    });

});




// funnel Sale
var dataf = [
        ['Step 1', 15654],
        ['Step 2', 4064],
        ['Step 3', 1987],
    ];
var datata = [
        ['Step 1', 5654],
        ['Step 2', 4064],
        ['Step 3', 1987],
    ];

//$(function () {
//
//    $('#funnel2').highcharts({
//        chart: {
//            type: 'funnel',
//            marginRight: 100
//        },
//        title: false,
//        plotOptions: {
//            series: {
//                dataLabels: {
//                    enabled: true,
//                    format: '<b>{point.percentage:.1f}% </b> <b>{point.name}</b>',
//                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
//                    softConnector: true
//                },
//                neckWidth: '30%',
//                neckHeight: '25%',
//                width: '64%',
//                //-- Other available options
//                // height: pixels or percent
//                // width: pixels or percent
//
//            },
//            area: {
//                stacking: 'percent',
//                lineColor: '#ffffff',
//                lineWidth: 1,
//                marker: {
//                    lineWidth: 1,
//                    lineColor: '#ffffff'
//                }
//            }
//        },
//        legend: {
//            enabled: false
//        },
//        series: [{
//            name: 'Sales Funnel',
//            data: dataf
//        }]
//    });
//});

$(function () {

    // Initial chart
    var funel =$("#funel2").highcharts({
        chart: {
            type: 'funnel',
            marginRight: 100
        },
        title: false,
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.percentage:.1f}% </b> <b>{point.name}</b>',
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                    softConnector: true
                },
                neckWidth: '30%',
                neckHeight: '25%',
                width: '64%',
                //-- Other available options
                // height: pixels or percent
                // width: pixels or percent

            },
            area: {
                stacking: 'percent',
                lineColor: '#ffffff',
                lineWidth: 1,
                marker: {
                    lineWidth: 1,
                    lineColor: '#ffffff'
                }
            }
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Sales Funnel',
            data: dataf
        }]
    });

    // Redraw chart depending on which option is selected
    $("#FunnelType").change(function (evt) {
        var timeSelection2 = eval($("#FunnelType").val());
        var funel =$("#funel2").highcharts({
            chart: {
                type: 'funnel',
                marginRight: 100
            },
            title: false,
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.percentage:.1f}% </b> <b>{point.name}</b>',
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                        softConnector: true
                    },
                    neckWidth: '10%',
                    neckHeight: '70%',
                    width: '5%',
                    //-- Other available options
                    // height: pixels or percent
                    // width: pixels or percent

                },
                area: {
                    stacking: 'percent',
                    lineColor: '#ffffff',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#ffffff'
                    }
                }
            },
            legend: {
                enabled: false
            },
            series: [{
                name: 'Sales Funnel',
                data: timeSelection2
            }]
        });
    });

});


</script>

		<script>
		$(document).ready(function() {
      
			$('#admin-datatable-second_admin').DataTable({
				"order" : [],
				dom : 'Bfrtip',
				title : ' Agent List',

				buttons : [ 'copy', 'csv', 'print', {
					extend : 'excelHtml5',
					title : 'Abandonment Dashboard'
				}, {
					extend : 'pdfHtml5',
					title : 'Abandonment Dashboard'
				} ]
			});

			$('#admin-datatable-second_admin1').DataTable({
				"order" : [],
				dom : 'Bfrtip',
				title : ' Agent List',

				buttons : [ 'copy', 'csv', 'print', {
					extend : 'excelHtml5',
					title : 'Abandonment Dashboard'
				}, {
					extend : 'pdfHtml5',
					title : 'Abandonment Dashboard'
				} ]
			});

			$('#admin-datatable-second_admin2').DataTable({
				"order" : [],
				dom : 'Bfrtip',
				title : ' Agent List',

				buttons : [ 'copy', 'csv', 'print', {
					extend : 'excelHtml5',
					title : 'Abandonment Dashboard'
				}, {
					extend : 'pdfHtml5',
					title : 'Abandonment Dashboard'
				} ]
			});

			// $('#date1').datepicker();
			$("#fromdate").datepicker({
				format : 'dd/mm/yyyy',
				autoclose : true,
			}).on('changeDate', function(ev) {
				$(this).datepicker('hide');
			});
			$("#todate").datepicker({
				format : 'dd/mm/yyyy',
				autoclose : true,
			}).on('changeDate', function(ev) {
				$(this).datepicker('hide');
			});
	
		});
	</script>


		<script>
	
		 function validateForm() {
        	   var x = document.forms["rptForm"]["fromdate"].value;
        	   
        	   if ( x == null || x == "" ) {
        		  
        	      alert("Please choose Date From");
        	      document.forms["rptForm"]["fromdate"].focus();
        	      
        	      return false;
        	   }
        	  
        	   
        	   return true;
        	  
        	}
	</script>
</body>
</html>