<%--     			 <div class="col-sm-3 col-md-2 fluid-menu">
                            <div class="sidebar-left sidebar-nicescroller admin">
                                <!-- desktop menu -->
                                <ul class="sidebar-menu admin">
                                      <li>
                                        <a href="#">
                                            <span class="icon-sidebar icon dashboard"></span>
                                            Dashboard
                                        </a>
                                    </li> 
                                 
               
                                   
                                       <li>
                                        <a href="${pageContext.request.contextPath}/bancaBranchCode">
                                            <span class="icon-sidebar icon services"></span>
                                            <i class="fa fa-angle-right chevron-icon-sidebar"></i>
                                            Branch Code
                                        </a>
                                        
                                    </li>
                                </ul>
                            </div><!-- /.sidebar-left -->
                        </div> --%>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page session="true"%>

<div class="col-sm-3 col-md-2 fluid-menu">
	<div class="sidebar-left sidebar-nicescroller admin">
		<!-- desktop menu -->
		<ul class="sidebar-menu admin">

			<% System.out.println("check level 1     : "+session.getAttribute("level_1")); %>
			<% System.out.println("check level 2     : "+session.getAttribute("level_2")); %>
			<% System.out.println("check level 3     : "+session.getAttribute("level_3")); %>
			<% System.out.println("check level 4     : "+session.getAttribute("level_4")); %>

			<c:forEach var="paramItem1" items="${sessionScope.level_1}">
				<li><a
					href="<c:choose> 
                                             <c:when test = "${paramItem1.url!=null}">
										           ${pageContext.request.contextPath}${paramItem1.url}?id=${sessionScope.uamReqParamSession}
										         </c:when>  
										         <c:otherwise>
										           #
										         </c:otherwise>
                                                 </c:choose>">
						<span class="icon-sidebar icon services"></span> <c:out
							value='${paramItem1.name}' /> <c:if
							test="${paramItem1.id==paramItem2.parentId}">
							<c:if test="${sessionScope.level_2.size()>0}">
								<i class="fa fa-angle-right chevron-icon-sidebar"></i>
							</c:if>
						</c:if>
				</a> <c:forEach var="paramItem2" items="${sessionScope.level_2}">
						<c:if test="${paramItem1.id==paramItem2.parentId}">
							<ul class="submenu">
								<li><a
									href="<c:choose> 
                                             <c:when test = "${paramItem2.url!=null}">
										           ${pageContext.request.contextPath}${paramItem2.url}
										         </c:when> 
										         <c:otherwise>
										           #
										         </c:otherwise>
                                                 </c:choose>
                                            ">

										<c:if test="${sessionScope.level_3.size()>0}">
											<i class="fa fa-angle-right chevron-icon-sidebar""></i>
										</c:if> <c:out value='${paramItem2.name}' />
								</a> <c:forEach var="paramItem3" items="${sessionScope.level_3}">
										<c:if test="${paramItem2.id==paramItem3.parentId}">
											<ul class="submenu">
												<li><a href="#"> <c:if
															test="${sessionScope.level_4.size()>0}">
															<i class="fa fa-angle-right chevron-icon-sidebar""></i>
														</c:if> <c:out value='${paramItem3.name}' />
												</a> <c:forEach var="paramItem4" items="${sessionScope.level_4}">
														<c:if test="${paramItem3.id==paramItem4.parentId}">
															<ul class="submenu">
																<li><a
																	href="${pageContext.request.contextPath}${paramItem4.url}">
																		<c:out value='${paramItem4.name}' />
																</a></li>
															</ul>
														</c:if>
													</c:forEach>
											</ul>
										</c:if>
									</c:forEach></li>
							</ul>
						</c:if>
					</c:forEach>
			</c:forEach>
		</ul>
	</div>
</div>
