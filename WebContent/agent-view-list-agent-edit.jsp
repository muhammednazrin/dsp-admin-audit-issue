<%@ page import="java.util.List"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<%@ page import="com.etiqa.model.agent.Agent"%>

<%
	java.text.DateFormat df = new java.text.SimpleDateFormat("dd/MM/yyyy");
%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<link rel="stylesheet" href="assets/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/menu.css">
<link rel="stylesheet" href="assets/jAlert/jAlert.css">

<style>
.col-sm-12.buttons {
	text-align: right;
}

.col-sm-12.buttons>div {
	display: inline-block;
}

.pad15 {
	padding-left: 15px !important;
	padding-right: 15px !important;
}
</style>
</head>
<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>

					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Cyber Agent Management</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">

											<form:form modelAttribute="agentProfile"
												action="updateAgentDone" id="updateAgentForm"
												name="updateAgentForm" method="POST">
												<input type="hidden" name="id"
													value="<c:out value="${agentProfile.id}" />">
												<div class="row">
													<div class="col-sm-12">
														<div class="title">
															<div class="sub">
																<h4>Cyber Agent Management</h4>
															</div>
														</div>
														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-12">
																	<div class="title">
																		<div class="sub">
																			<h4>Agent Information Details</h4>
																		</div>
																	</div>
																</div>
																<div class="gap gap-mini"></div>
																<div class="col-sm-6">
																	<div class="form-horizontal info-meor">
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Reference No</label>
																			</div>
																			<div class="col-sm-9">

																				<spring:bind path="agentProfile.referenceNumber">
																					<input type="text" class="form-control"
																						name="${status.expression}"
																						id="${status.expression}" value="${status.value}"
																						required maxlength="30" />
																				</spring:bind>
																				<span id="msg_refno" class="hidden"
																					style="color: red; text-align: left">Please
																					enter your Reference No</span>
																			</div>
																		</div>
																		<%-- <fmt:parseDate value="${agentProfile.registrationDate}" var="parsedEmpDate" 
																	                              pattern="dd/MM/yyyy HH:mm:ss" />
																	                              
																	<fmt:parseDate value="${parsedEmpDate}" var="parsedDate" 
																	                              pattern="dd/MM/yyyy" /> --%>
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Registration
																					Date</label>
																			</div>
																			<div class="col-sm-9">
																				<spring:bind path="agentProfile.registrationDate">
																					<%-- <input type="text" placeholder="Date" class="form-control" required name="${status.expression}" value="<c:out value="${s}" />" id="date1" /> --%>
																					<input type="text" placeholder="Date"
																						class="form-control" readonly
																						name="${status.expression}"
																						value="<c:out value="${s}" />" id="date1" />
																					<%-- <input type="text" placeholder="Date" class="form-control" readonly name="${status.expression}" value="<%= df.format(new java.util.Date())%>" id="date1" /> --%>
																				</spring:bind>
																				<span id="msg_date" class="hidden"
																					style="color: red; text-align: left">Please
																					select registration Date</span>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Annual Sales
																					Target (RM)</label>
																			</div>
																			<div class="col-sm-9">
																				<spring:bind path="agentProfile.annualSalesTarget">
																					<input type="text" name="${status.expression}"
																						value="${status.value}" class="form-control"
																						required id="annualSalesTarget" maxlength="10" />
																				</spring:bind>
																				<span id="msg_salesTarget" class="hidden"
																					style="color: red; text-align: left">Please
																					enter your Annual Sales Target</span>
																			</div>
																		</div>

																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Branch Code</label>
																			</div>
																			<div class="col-sm-9">
																				<spring:bind path="agentProfile.branchCode">
																					<input type="text" name="${status.expression}"
																						value="${status.value}" class="form-control"
																						id="branchCode" maxlength="50" />
																				</spring:bind>
																				<!--   <input type="text" name="annualSalesTarget" class="form-control"  id="annualSalesTarget" maxlength="10"> -->
																				<span id="msg_branchCode" class="hidden"
																					style="color: red; text-align: left">Please
																					enter Branch Code</span>
																			</div>
																		</div>


																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Maya Agent</label>
																			</div>
																			<div class="col-sm-9">
																				<spring:bind path="agentProfile.isMayaAgent"> 
																				YES &nbsp;<input type="radio"
																						name="${status.expression}"
																						<c:if test="${status.value eq 'y'}" >checked</c:if>
																						value="y" id="${status.expression}" />
																				NO &nbsp;<input type="radio"
																						name="${status.expression}" value="n"
																						<c:if test="${status.value ne 'y'}" >checked</c:if>
																						id="${status.expression}" />
																				</spring:bind>

																			</div>
																		</div>

																		<!-- Added by Chandra for Simplified Motor Cyber Agent on 20171030 -->
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Sell Simplified
																					Motor</label>
																			</div>
																			<div class="col-sm-9">
																				<spring:bind path="agentProfile.simplifiedMotor"> 
																				YES &nbsp;<input type="radio"
																						name="${status.expression}"
																						<c:if test="${status.value ne 'N'}" >checked</c:if>
																						value="Y" id="${status.expression}" />
																				NO &nbsp;<input type="radio"
																						name="${status.expression}" value="N"
																						<c:if test="${status.value eq 'N'}" >checked</c:if>
																						id="${status.expression}" />
																				</spring:bind>
																			</div>
																		</div>

																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-horizontal info-meor">


																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Agent Category</label>
																			</div>
																			<div class="col-sm-9">
																				<spring:bind path="agentProfile.agentCategory">
																					<select class="form-control" name="agentCategory"
																						required id="agentCategory"
																						onblur="checkOnBlur(this,'Please Select Your Category');"
																						onchange="checkOnChange(this);">
																						<option value=" -Please Select-">-Please
																							Select-</option>
																						<c:forEach items="${agentCategoryList}"
																							var="element">
																							<option
																								<c:if test="${status.value eq element.paramCode}" >selected</c:if>
																								value="<c:out value="${element.paramCode}"/>">
																								<c:out value="${element.paramName}" />
																							</option>
																						</c:forEach>
																					</select>
																				</spring:bind>
																			</div>
																		</div>

																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Agent Type</label>
																			</div>
																			<div class="col-sm-9">
																				<spring:bind path="agentProfile.agentType">
																					<select class="form-control" required
																						name="${status.expression}"
																						id="${status.expression}"
																						onblur="checkOnBlur(this,'Please Select Your Insurance Type');"
																						onchange="checkOnChange(this);">
																						<option value=" -Please Select-">-Please
																							Select-</option>
																						<c:forEach items="${agentTypeList}" var="element">
																							<option
																								<c:if test="${status.value eq element.paramCode}" >selected</c:if>
																								value="<c:out value="${element.paramCode}"/>">
																								<c:out value="${element.paramName}" />
																							</option>
																						</c:forEach>

																					</select>
																				</spring:bind>
																			</div>
																		</div>


																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Status</label>
																			</div>
																			<div class="col-sm-9">
																				<spring:bind path="agentProfile.status">
																					<select class="form-control" required
																						name="${status.expression}" id="agentStatus"
																						onblur="checkOnBlur(this,'Please Select Your Status');"
																						onchange="checkOnChange(this);">
																						<option value=" -Please Select-">-Please
																							Select-</option>
																						<c:forEach items="${agentStatusList}"
																							var="element">
																							<option
																								<c:if test="${status.value eq element.paramCode}" >selected</c:if>
																								value="<c:out value="${element.paramCode}"/>">
																								<c:out value="${element.paramName}" />
																							</option>
																						</c:forEach>
																					</select>
																				</spring:bind>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Agent Alias
																					Name</label>
																			</div>
																			<div class="col-sm-8">
																				<spring:bind path="agentProfile.agentAliasName">
																					<input type="text" required
																						name="${status.expression}"
																						id="${status.expression}" value="${status.value}"
																						class="form-control required"
																						name="agentAliasName" id="agentAliasName"
																						maxlength="20" />
																				</spring:bind>
																				<span id="msg_agentaliasname" class="hidden"
																					style="color: red; text-align: left">Please
																					key in Agent Name</span>
																			</div>
																			<span class="right tooltips"> <a
																				id="tooltips-2"> <i
																					class="fa fa-question-circle fa-lg"></i>
																			</a>
																			</span>
																			<!-- Popover 2 hidden title -->
																			<div id="tooltips-2Title" style="display: none">
																				Agent referral ID is the link management solution to
																				shorten the product referral link<br> e.g.
																				http://www.etiqa.com.my/agentname<br>
																			</div>
																		</div>

																	</div>
																</div>




																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Agent Logo</label>
																			</div>
																			<div class="col-sm-9">
																				<div id="fine-uploader-gallery">
																					<input type="file">
																				</div>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-3 column">
																				<label class="control-label"></label>
																			</div>
																			<div class="col-sm-9">Please upload logo with
																				this format only .jpg, jpeg, .gif and .png. Do not
																				exceed 345px width X 100px height of image size</div>
																		</div>
																	</div>
																</div>


																<div class="col-sm-12">
																	<div class="form-inline info-meor">
																		<div class="form-group" name="registrationDocument">
																			<label class="control-label col-sm-12">Agent
																				Registration Document</label>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-12">
																				<div class="checkbox">

																					<label> <input type="checkbox"
																						id="registrationDocument"
																						name="registrationDocument"
																						<c:forEach items="${agentDocumentList}" var="agentDocuments">
		                                                                              		<c:if test="${agentDocuments.documentCode == 'Application Form'}" >		                                                                           		
		                                                                              			checked="checked"
		                                                                              		</c:if>
		                                                                              	</c:forEach>
																						value="Application Form"> Application Form
																						<!-- <span id="msg_agentregdoc" class="hidden" style="color:red;text-align:left">Please select to proceed</span> -->
																					</label> <label> <input type="checkbox"
																						id="registrationDocument"
																						name="registrationDocument"
																						<c:forEach items="${agentDocumentList}" var="agentDocuments">
		                                                                              		<c:if test="${agentDocuments.documentCode == 'Cyber Agent Agreement'}" >		                                                                           		
		                                                                              			checked="checked"
		                                                                              		</c:if>
		                                                                              	</c:forEach>
																						value="Cyber Agent Agreement"> Cyber Agent
																						Agreement <!-- <span id="msg_agentregdoc" class="hidden" style="color:red;text-align:left">Please select to proceed</span> -->
																					</label>

																				</div>
																			</div>
																		</div>


																	</div>
																</div>
															</div>

															<div class="the-box">
																<div class="col-sm-12">
																	<div class="title">
																		<div class="sub">
																			<h4>Personal Details</h4>
																		</div>
																	</div>
																</div>
																<div class="gap gap-mini"></div>
																<div class="col-sm-6">
																	<div class="form-horizontal info-meor">
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Agent Name</label>
																			</div>
																			<div class="col-sm-9">
																				<spring:bind path="agentProfile.agentName">
																					<input type="text" required class="form-control"
																						maxlength="50" name="${status.expression}"
																						id="${status.expression}" value="${status.value}" />
																				</spring:bind>
																				<span id="msg_agentname" class="hidden"
																					style="color: red; text-align: left">Please
																					select to proceed</span>
																			</div>
																		</div>

																		<%-- <div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">User Id</label>
																			</div>
																			<div class="col-sm-9">
																			 <spring:bind
														       					 path="agentProfile.agentUsername">
																				<input type="text" required class="form-control"
																					name="${status.expression}" id="${status.expression}" value="${status.value}" />
																				</spring:bind>
																			</div>
																		</div> --%>
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Login ID</label>
																			</div>
																			<div class="col-sm-9 column pad15">
																				<div class="row">
																					<%-- <div class="col-sm-4 column">
																					<spring:bind
														       					       path="agentProfile.idTypeNo">
																						<select class="form-control" name="idType"
																							required
																							onblur="checkOnBlur(this,'Please Select Your ID Type');"
																							onchange="checkOnChange(this);">
																							<c:forEach items="${agentIdTypeList}" var="element"> 
						                                                                     <option						                                                                  
																									<c:if test="${status.value eq element.paramCode}" >selected</c:if> 
																									value="<c:out value="${element.paramCode}"/>">
																									<c:out value="${element.paramName}" />
																							</option>																																				
																						</c:forEach> 
																						</select> 
																						</spring:bind>
																						<span id="msg_idtype" class="hidden"
																							style="color: red; text-align: left">Please
																							select ID Type</span>
																					</div> --%>
																					<div class="col-sm-12 column">
																						<spring:bind path="agentProfile.idNo">
																							<input type="text" required class="form-control"
																								maxlength="12" name="${status.expression}"
																								id="${status.expression}"
																								value="${status.value}" readonly />
																						</spring:bind>
																						<span id="msg_idno" class="hidden"
																							style="color: red; text-align: left">Please
																							key in your ID number</span>
																					</div>
																				</div>
																			</div>
																		</div>


																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Phone No</label>
																			</div>
																			<div class="col-sm-9 pad15">
																				<div class="row">
																					<div class="col-sm-4">
																						<spring:bind path="agentProfile.phoneType">
																							<select class="form-control" name="phoneType"
																								required
																								onblur="checkOnBlur(this,'Please Select Your Phone Type');"
																								onchange="checkOnChange(this);">
																								<c:forEach items="${agentPhoneTypeList}"
																									var="element">
																									<option
																										<c:if test="${status.value eq element.paramCode}" >selected</c:if>
																										value="<c:out value="${element.paramCode}"/>">
																										<c:out value="${element.paramName}" />
																									</option>
																								</c:forEach>
																							</select>
																						</spring:bind>
																						<span id="msg_phonetype" class="hidden"
																							style="color: red; text-align: left">Please
																							select your Phone Type</span>
																					</div>
																					<div class="col-sm-8">
																						<spring:bind path="agentProfile.phoneNumber">
																							<input type="text" required class="form-control"
																								maxlength="20" name="${status.expression}"
																								id="${status.expression}"
																								value="${status.value}" />
																						</spring:bind>
																						<span id="msg_phoneno" class="hidden"
																							style="color: red; text-align: left">Please
																							key in your phone number</span>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Email</label>
																			</div>
																			<div class="col-sm-9">

																				<spring:bind path="agentProfile.email">
																					<input type="text" required placeholder="Email"
																						class="form-control" name="${status.expression}"
																						id="${status.expression}" value="${status.value}"
																						maxlength="50" />
																				</spring:bind>

																				<span id="msg_email" class="hidden"
																					style="color: red; text-align: left">Please
																					key in your Email ID</span> <span id="msg_validmail"
																					class="hidden" style="color: red; font-size: bold">Please
																					Enter valid email ID</span>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-horizontal info-meor">
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Address</label>
																			</div>
																			<div class="col-sm-9">
																				<div class="form-group">
																					<div class="col-sm-12">
																						<spring:bind path="agentProfile.address1">
																							<input type="text" required class="form-control"
																								name="${status.expression}"
																								id="${status.expression}"
																								value="${status.value}" maxlength="150" />
																						</spring:bind>
																						<span id="msg_address" class="hidden"
																							style="color: red; text-align: left">Please
																							key in your Address</span>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="col-sm-12">
																						<spring:bind path="agentProfile.address2">
																							<input type="text" class="form-control"
																								name="${status.expression}"
																								id="${status.expression}"
																								value="${status.value}" maxlength="150" />
																						</spring:bind>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="col-sm-12">
																						<spring:bind path="agentProfile.address3">
																							<input type="text" class="form-control"
																								name="${status.expression}"
																								id="${status.expression}"
																								value="${status.value}" maxlength="150" />
																						</spring:bind>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="col-sm-5">
																						<spring:bind path="agentProfile.postCode">
																							<input type="text" class="form-control"
																								placeholder="Postcode" required
																								name="${status.expression}"
																								id="${status.expression}"
																								value="${status.value}" maxlength="6" />
																						</spring:bind>
																						<span id="msg_postcode" class="hidden"
																							style="color: red; text-align: left">Please
																							key in your Postcode</span>
																					</div>
																					<!-- <div class="col-sm-7">
																						<select class="form-control" name="city" id="city">
																							<option>Select</option>
																							<option>Petaling Jaya</option>
																							<option>Bangsar</option>

																						</select> <span id="msg_city" class="hidden"
																							style="color: red; text-align: left">Please
																							select your city</span>
																					</div> -->
																				</div>

																				<div class="form-group">
																					<div class="col-sm-12">
																						<spring:bind path="agentProfile.state">
																							<select class="form-control" name="state"
																								id="state">
																								<option value="">Select</option>
																								<c:forEach items="${agentStateList}"
																									var="element">
																									<option
																										<c:if test="${status.value eq element.paramCode}" >selected</c:if>
																										value="<c:out value="${element.paramCode}"/>">
																										<c:out value="${element.paramName}" />
																									</option>
																								</c:forEach>

																							</select>
																						</spring:bind>
																						<span id="msg_state" class="hidden"
																							style="color: red; text-align: left">Please
																							select your state</span>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="col-sm-12">
																						<spring:bind path="agentProfile.country">
																							<select class="form-control" name="country">
																								<option value="Malaysia" selected="">Malaysia</option>
																							</select>
																						</spring:bind>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>

														</div>
														<!--content inner -->
													</div>
													<!--col-sm-12 -->

													<div class="col-sm-12 buttons">
														<div>
															<input class="btn btn-warning btn-sm" id="updateAgent"
																onClick="submitRegistration();" type="button"
																value="Update" />
														</div>
														<div>
															<input class="btn btn-warning btn-sm" id="resendEmail"
																type="button" value="Resend Email" />
															<%-- <a href="resendEmail?email=<c:out value="${agentProfile.email}"/>" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i>Resend Email</a> --%>
														</div>
													</div>
													<!--col-sm-12 -->
												</div>
												<!--row -->
											</form:form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->
		</div>
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->
	</div>


	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%-- <jsp:include page="siteFooter.jsp" /> --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->

	<script src="assets/js/jquery.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.js" type="text/javascript"></script>
	<script src="assets/jAlert/jAlert.min.js"></script>
	<script src="assets/jAlert/jAlert-functions.min.js"></script>
	<script src="assets/plugins/retina/retina.min.js"></script>
	<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>
	<script src="assets/js/apps.js"></script>

	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script type="text/javascript" src="assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript" src="assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/buttons.print.min.js"></script>
	<script src="assets/js/buttons.colVis.min.js"></script>
	<script type="text/javascript">
         
         
         
         
         </script>
	<script>
	
	$('#resendEmail').on("click", function(e){
	    e.preventDefault();
	    $('#updateAgentForm').attr('action', "resendEmail").submit();
	});
	
	
	 function submitRegistration(){
         result=true;           
         if(!$('#referenceNumber').val().length) {
             $('#msg_refno').removeClass('hidden');
             $('#msg_refno').addClass('visible');
             /* $('#referenceNo').css("border-color", "red"); */
             $('#referenceNo').focus();
             result=false;
         }
         if(!$('#date1').val().length) {
             $('#msg_date').removeClass('hidden');
             $('#msg_date').addClass('visible');
             $('#date1').focus();
             result=false;
         }
         if(!$('#annualSalesTarget').val().length) {
             $('#msg_salesTarget').removeClass('hidden');
             $('#msg_salesTarget').addClass('visible');
             $('#annualSalesTarget').focus();
             result=false;
         }
        
         
         if(!$('#agentCategory').val()) {
             $('#msg_agentcat').removeClass('hidden');
             $('#msg_agentcat').addClass('visible');
             $('#agentCategory').focus();
             result=false;
         }
         if(!$('#agentType').val()) {
             $('#msg_agenttype').removeClass('hidden');
             $('#msg_agenttype').addClass('visible');
             $('#agentType').focus();
             result=false;
         }
         if(!$('#agentStatus').val()) {
             $('#msg_agentstatus').removeClass('hidden');
             $('#msg_agentstatus').addClass('visible');
             $('#agentStatus').focus();
             result=false;
         }
         if(!$('#agentAliasName').val().length) {
             $('#msg_agentaliasname').removeClass('hidden');
             $('#msg_agentaliasname').addClass('visible');
             $('#agentAliasName').focus();
             result=false;
         }
        
         if(!$('#agentName').val().length) {
             $('#msg_agentname').removeClass('hidden');
             $('#msg_agentname').addClass('visible');
             $('#agentName').focus();
             result=false;
         }
         if($('#phoneType').index() == 0) {
             $('#msg_phonetype').removeClass('hidden');
             $('#msg_phonetype').addClass('visible');
             $('#phoneType').focus();
             result=false;
         }
         if(isEmpty ($('#phoneNumber').val())) {
             $('#msg_phoneno').removeClass('hidden');
             $('#msg_phoneno').addClass('visible');
             $('#phoneNo').focus();
             result=false;
         }
         
         if(!$('#address1').val().length) {
             $('#msg_address').removeClass('hidden');
             $('#msg_address').addClass('visible');
             $('#address1').focus();
             result=false;
         }
         if(isEmpty ($('#postCode').val())) {
             $('#msg_postcode').removeClass('hidden');
             $('#msg_postcode').addClass('visible');
             $('#postcode').focus();
             result=false;
         }

         if (result==true) {
         	$('#updateAgentForm').submit();   
         }
         
     }
	
        function validateForm() {
        	   var x = document.forms["agentList"]["dateFrom"].value;
        	   var y = document.forms["agentList"]["dateTo"].value;
        	   if ( x == null || x == "" ) {
        		   if(y !=""){
        	      alert("Please choose Date From");
        	      return false;
        	   } 
        		  
        	   }
        	   return true;
        	  
        	}
        
           function isEmpty(value) {
        	  return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
        	}
        </script>
	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->

	<script>
	/*	$(document).ready(function() {
			 $('#date1').datepicker({
				    format: 'dd/mm/yyyy',
			}).on('changeDate', function(e){
			    $(this).datepicker('hide');
			});
		}); */
	</script>


</body>
</html>