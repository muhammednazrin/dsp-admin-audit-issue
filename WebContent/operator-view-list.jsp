<%@ page import="java.util.List"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ page import="com.etiqa.model.operator.Operator"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}
</style>


<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}
</style>
<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet"
	href="assets/plugins/datepicker/datepicker.min.css">
<!-- PLUGINS CSS -->
<link href="assets/plugins/owl-carousel/owl.carousel.min.css"
	rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.theme.min.css"
	rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.transitions.min.css"
	rel="stylesheet">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
</head>


<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Operator Management</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>List of Operator</h4>
														</div>
													</div>

													<!-- Start Form -->
													<form name="operatorList" action="operator" method="post"
														onsubmit="return validateForm()">
														<input type="hidden" name="action" value="list" />

														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Search
																				by Registration Date</label>
																			<div class="col-sm-4">
																				<input type="text" placeholder="Date From"
																					class="form-control" name="dateFrom"
																					id="datepicker1">
																			</div>
																			<div class="col-sm-5">
																				<input type="text" placeholder="Date To"
																					class="form-control" name="dateTo" id="datepicker2">
																			</div>
																			<div>
																				<span id="errmsg"></span>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Category</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="operatorCategory">
																					<option value="">-View All-</option>
																					<option value="Individual">Individual</option>
																					<option value="Company/Organisation">Company/Organisation</option>
																				</select>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Entity</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="entityType">
																					<option value="Insurance">Insurance</option>
																					<option value="Takaful">Takaful</option>
																				</select>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Operator
																				Name</label>
																			<div class="col-sm-9">
																				<input type="text" placeholder=""
																					class="form-control" name="operatorName">
																			</div>
																		</div>
																		<!--  <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Operator Code</label>
                                                                    <div class="col-sm-9">
                                                                        <input type="text" placeholder="" class="form-control" name="operatorCode">
                                                                    </div>
                                                                  </div> -->


																	</div>
																</div>
																<div class="col-sm-12 text-right">
																	<button type="submit" class="btn btn-default">Search</button>
																</div>
															</div>
														</div>
													</form>
													<!-- END FORM -->
												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="row">
																<div class="col-xs-12 form-inline">
																	<div class="col-xs-6">
																		<div class="row"></div>
																	</div>
																	<div class="col-xs-6">
																		<div class="row">
																			<div class="pull-right">
																				<div class="col-xs-6">
																					<div class="row">
																						<div class="pull-right">
																							<button class="btn btn-warning btn-sm">
																								<i class="fa fa-print" aria-hidden="false"></i>
																							</button>
																							<button class="btn btn-warning btn-sm">
																								<i class="fa fa-download" aria-hidden="false"></i>
																							</button>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="table-responsive" id="MyTable">
																<table
																	class="table table-striped table-warning table-hover"
																	id="admin-datatable-second_admin">
																	<thead>
																		<tr>
																			<th style="width: 20px;">No</th>
																			<!--  <th>Operator Code</th>-->
																			<th>Operator Name</th>
																			<th>Registration Date</th>
																			<th>Entity</th>
																			<th>Phone No</th>
																			<th>Email</th>
																			<th>Action</th>
																		</tr>
																	</thead>
																	<tbody>


																		<%  
                                                                if (request.getAttribute("data") != null) {
														            List<Operator> list = (List<Operator>) request.getAttribute("data");
														            
														            if(!list.isEmpty()) {
														                for(Operator operator : list) {
														        %>


																		<tr>
																			<td><%=operator.getId()+1%></td>
																			<!--  <td></td>-->
																			<td><%=operator.getOperatorName()%></td>

																			<td><%=operator.getRegistrationDate()%></td>
																			<td><%=operator.getEntity()%></td>
																			<td><%=operator.getPhoneNO()%></td>
																			<td><%=operator.getEmail() %></td>

																			<td>
																				<form action="operator" method="post"
																					name="operatorE" id="operatorE">
																					<input type="hidden" name="action" value="record" />
																					<input type="hidden" name="id"
																						value="<%=operator.getId()%>" /> <input
																						class="btn btn-warning btn-sm" type="submit"
																						name="submit" value="Edit">
																				</form>
																				<form action="operator" method="post"
																					name="operatorD" id="operatorD">
																					<input type="hidden" name="action" value="delete" />
																					<input type="hidden" name="id"
																						value="<%=operator.getId()%>" /> <input
																						class="btn btn-warning btn-sm" type="submit"
																						name="submit" value="Delete">
																				</form>
																			</td>
																		</tr>
																		<%
														                }
														                }
														            }
														        %>
																		<!-- <tr><td>1</td><td>V0006</td><td>Samad Saif Enterprise</td><td>01-Jan-2020</td><td>Takaful</td><td>012-3829828</td><td>my@email.com</td><td>Permohonan Dilulus</td><td><a class="btn btn-warning btn-sm" href="agent-view-list-agent-edit.html"><i class="fa fa-edit"></i> Edit</a></td></tr> -->

																	</tbody>
																</table>
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>

											</div>
										</div>
									</div>
									<!--row -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->

	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->

	<script type="text/javascript">
         
         
         
         </script>
	<script>
        function validateForm() {
       
        	   var x = document.getElementById('datepicker1').value;
        	   var y = document.getElementById('datepicker2').value;
        	 
        	      $('#errmsg').css("color", "black");
        	      $('#errmsg').html("");
        	   if ( x == null || x == "" || y == "" ) {
        		  // if(y !=""){
        	      alert("Please choose Registration Date");
        	      $('#errmsg').css("color", "red");
        	      $('#errmsg').html("Please choose Registration Date");
        	      
        	      return false;
        	  // } 
        		  
        	   }
        	   return true;
        	  
        	}
        
        
        $('#datepicker2').change(function() {
           
        });
        
        </script>


	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->

	<script>
		$(document).ready(function() {
			$('#admin-datatable-second_admin').DataTable({
				dom : 'Bfrtip',
				title : 'Operator List',

				buttons : [ 'copy', 'csv', 'print', {
					extend : 'excelHtml5',
					title : 'Operator List'
				}, {
					extend : 'pdfHtml5',
					title :  'Operator List Report'
				} ]
			});
		});
	</script>


	<script src="assets/plugins/retina/retina.min.js"></script>
	<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>


	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script type="text/javascript" src="assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript" src="assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>

	<link href="assets/js/jquery.dataTables.min.css" rel="stylesheet"
		type="text/css">
	<link href="assets/js/buttons.dataTables.min.css" rel="stylesheet"
		type="text/css">
	<link href="assets/js/bootstrap.min.css" rel="stylesheet"
		type="text/css">
	<link href="assets/js/dataTables.bootstrap.min.css" rel="stylesheet"
		type="text/css">
	<link href="assets/js/buttons.bootstrap.min.css" rel="stylesheet"
		type="text/css">

	<script src="assets/js/jquery-1.12.3.js"></script>
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/buttons.print.min.js"></script>
	<script src="assets/js/buttons.colVis.min.js"></script>

</body>
</html>
