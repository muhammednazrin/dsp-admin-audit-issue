<%@ page import="java.io.*,java.util.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<link rel="stylesheet" href="assets/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/menu.css">
<link rel="stylesheet" href="assets/select2/dist/css/select2.min.css">

<script src="assets/js/jquery.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.js" type="text/javascript"></script>
<style>
.inputError {
	border: 1px solid red;
}

select2.minimal {
	background-image: linear-gradient(45deg, transparent 50%, gray 50%),
		linear-gradient(135deg, gray 50%, transparent 50%),
		linear-gradient(to right, #ccc, #ccc);
	background-position: calc(100% - 20px) calc(1em + 2px),
		calc(100% - 15px) calc(1em + 2px), calc(100% - 2.5em) 0.5em;
	background-size: 5px 5px, 5px 5px, 1px 1.5em;
	background-repeat: no-repeat;
}

select2.minimal:focus {
	background-image: linear-gradient(45deg, green 50%, transparent 50%),
		linear-gradient(135deg, transparent 50%, green 50%),
		linear-gradient(to right, #ccc, #ccc);
	background-position: calc(100% - 15px) 1em, calc(100% - 20px) 1em,
		calc(100% - 2.5em) 0.5em;
	background-size: 5px 5px, 5px 5px, 1px 1.5em;
	background-repeat: no-repeat;
	border-color: green;
	outline: 0;
}
</style>
</head>

<body>
	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->


	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Cyber Agent Management</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="the-box">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Product Management</h4>
														</div>
													</div>
												</div>
												<div class="gap gap-mini"></div>
												<form:form modelAttribute="agentProfile"
													id="productSearchForm" action="agentProductSubmitDone"
													method="POST">
													<div class="col-sm-12">
														<!-- Begin  table -->

														<div class="col-sm-6">


															<div class="form-group">
																<label class="col-sm-3 control-label">Agent : </label>
																<div class="col-sm-7">
																	<spring:bind path="agentProfile.id">
																		<select class="form-control js-example-basic-single"
																			name="${status.expression}" id="${status.expression}"
																			onchange="getAgentType(this);">
																			<option value="">Select Agent</option>
																			<c:forEach items="${agentList}" var="element">
																				<option
																					<c:if test="${status.value eq element.id}" >selected</c:if>
																					value="<c:out value="${element.id}"/>">
																					<c:out value="${element.agentName}" />
																				</option>
																			</c:forEach>
																		</select>
																	</spring:bind>
																</div>
															</div>
															<br> <br>
															<%-- <div class="form-group">
                                                         <label class="col-sm-3 control-label">Type : </label>
                                                        <div class="col-sm-9">
                                                         <spring:bind path="agentProfile.agentType">
                                                        <input class="form-control"  type="text" name="${status.expression}" 
                                                        id="${status.expression}"  value="${status.value}"                                                      
                                                         />
                                                        </spring:bind>                                                                                                               
                                                        <!-- <select class="form-control" required name="agentType" id="agentType" onblur="checkOnBlur(this,'Please Select Your Insurance Type');"  onchange="checkOnChange(this);">                                                                                      
	                                                        <option value="D">Direct</option>
	                                                        <option value="ND">Non Direct</option>                                                                                   
                                                        </select>     -->                                                                               
                                                        </div>
                                                      </div> --%>

														</div>

														<div class="col-sm-6">
															<div class="form-group">
																<label class="col-sm-3 control-label">Entity : </label>
																<div class="col-sm-7">
																	<spring:bind path="agentProfile.insuranceType">
																		<select class="form-control"
																			name="${status.expression}" id="${status.expression}">
																			<option value="">Select Entity</option>
																			<option
																				<c:if test="${status.value eq 'EIB'}" >selected</c:if>
																				value="EIB">EIB</option>
																			<option
																				<c:if test="${status.value eq 'ETB'}" >selected</c:if>
																				value="ETB">ETB</option>
																		</select>
																	</spring:bind>
																</div>
																<div class="col-sm-2"></div>
															</div>
														</div>
													</div>
												</form:form>
												<button class="btn btn-warning btn-sm pull-right"
													onclick="submitProductSearchForm();">
													<i class="fa fa-edit"></i> Submit
												</button>
												<!--  <a href="#" onclick="submitProductSearchForm();" class="btn btn-warning btn-sm pull-right"><i class="fa fa-edit"></i> Submit</a> -->
												<!-- End warning color table -->
											</div>
										</div>

										<div class="content-inner">
											<c:if
												test="${(not empty agentProfile.id) &&  (not empty agentProfile.insuranceType)}">
												<div class="the-box">
													<div class="col-sm-12">
														<div class="title">
															<div class="sub">
																<h4>Select Product</h4>
															</div>
														</div>
													</div>
													<div class="gap gap-mini"></div>
													<c:if test="${(not empty  errorMessage)}">
														<div style="color: red">
															<c:out value="${errorMessage}" />
														</div>
													</c:if>
													<div class="col-sm-12">
														<!-- Begin  table -->
														<div class="content-inner">
															<div class="the-box full no-border">
																<%-- <form:form id="productMapForm" action="agentProductSaveDone" method="POST"> --%>
																<form:form id="productMapForm" action="agentProductSave"
																	method="POST">
																	<input type="hidden" name="id"
																		value="<c:out value="${agentProfile.id}" />" />
																	<input type="hidden" name="agentType"
																		value="<c:out value="${agentProfile.agentType}" />" />
																	<input type="hidden" name="insuranceType"
																		value="<c:out value="${agentProfile.insuranceType}" />" />



																	<div class="table-responsive text-center">

																		<table
																			class="table table-striped table-warning table-hover">
																			<thead>
																				<tr>
																					<th style="width: 30px;"></th>
																					<th>Product</th>
																					<th>Type</th>
																					<th>Entity</th>
																					<th>Agent Code</th>
																					<th>Discount<br>(%)
																					</th>
																					<th>Commission<br>(%)
																					</th>
																				</tr>
																			</thead>
																		<tbody>
																				<c:choose>
																					<c:when test="${not empty agentProdMapList}">
																						<c:forEach items="${productsList}" var="element"
																							varStatus="counter">
																							<input type="hidden" id="productCodeCheckValue"
																								name="productCodeCheckValue"
																								value="<c:out value="${element.dspProductCode}" />" />
																							<c:set var="found" value="n" />
																							<c:forEach items="${agentProdMapList}"
																								var="apm_item">

																								<c:if test="${found eq 'n'}">
																									<c:choose>
																										<c:when
																											test="${apm_item.productCode eq element.dspProductCode}">
																											<c:set var="found" value="y" />
																											<c:set var="checkcounter" value="checked" />
																											<c:set var="agentCodeVar"
																												value="${apm_item.agentCode}" />
																											<c:set var="discountVar"
																												value="${apm_item.discount}" />
																											<c:set var="commissionVar"
																												value="${apm_item.commission}" />
																											<c:set var="agentTypeVar"
																												value="${apm_item.agentType}" />
																											<c:if test="${agentTypeVar eq 'D'}">
																												<c:set var="commision_readonly"
																													value="readonly" />
																												<c:set var="discount_readonly" value="" />
																											</c:if>
																											<c:if test="${agentTypeVar eq 'ND'}">
																												<c:set var="discount_readonly"
																													value="readonly" />
																												<c:set var="commision_readonly" value="" />
																											</c:if>

																										</c:when>
																										<c:otherwise>
																											<c:set var="checkcounter" value="" />
																											<c:set var="agentCodeVar" value="" />
																											<c:set var="discountVar" value="" />
																											<c:set var="commissionVar" value="" />
																											<c:set var="agentTypeVar" value="" />
																											<c:set var="commision_readonly" value="" />
																											<c:set var="discount_readonly" value="" />
																										</c:otherwise>
																									</c:choose>
																								</c:if>

																							</c:forEach>
																							<tr>
																								<td><c:choose>

																										<c:when test="${checkcounter== 'checked' }">
																											<a href="#"
																												onclick="deleteProduct('<c:out value='${agentCodeVar}' />','<c:out value='${element.dspProductCode}' />');"
																												class="btn btn-warning btn-sm pull-right"><i
																												class="fa fa-edit"></i> Delete</a>
																											<input type="checkbox" style="display: none"
																												id="productCodeCheckCounter"
																												name="productCodeCheckCounter" checked
																												value="<c:out value="${counter.count}" />">
																											<%-- <button class="btn btn-warning btn-sm pull-right"  onclick="deleteProduct('<c:out value="${agentProfile.id}" />','<c:out value="${agentCodeVar}" />','<c:out value="${element.dspProductCode}" />');"><i class="fa fa-edit"></i> Delete</button> --%>
																										</c:when>
																										<c:otherwise>
																											<label><input type="checkbox"
																												id="productCodeCheckCounter"
																												name="productCodeCheckCounter"
																												<c:out value="${checkcounter}" />
																												value="<c:out value="${counter.count}" />">
																											</label>
																										</c:otherwise>
																									</c:choose></td>
																								<td><c:out
																										value="${element.dspProductCodeName}" /></td>
																								<td><select class="form-control"
																									name="agentBusinessType" id="agentBusinessType">
																										<option value="">Select Type</option>
																										<option
																											<c:if test="${agentTypeVar eq 'D'}" >selected</c:if>
																											value="D">Direct</option>
																										<option
																											<c:if test="${agentTypeVar eq 'ND'}" >selected</c:if>
																											value="ND">Non Direct</option>
																								</select></td>
																								<td><c:out value="${element.productEntity}" /></td>
																								<td><input id="in_agentCode"
																									name="in_agentCode" type="text"
																									value="<c:out value="${agentCodeVar}" />"
																									class="form-control" size="10"></td>
																								<td><input id="in_discount"
																									name="in_discount" type="text"
																									value="<c:out value="${discountVar}" />"
																									<c:out value="${discount_readonly}" /> size="5"
																									pattern="\d{1,5}" maxlength="3"
																									class="form-control"></td>
																								<td><input id="in_commission"
																									name="in_commission" type="text"
																									value="<c:out value="${commissionVar}" />"
																									<c:out value="${commision_readonly}" />
																									size="5" pattern="\d{1,5}" maxlength="3"
																									class="form-control"></td>
																							</tr>

																						</c:forEach>
																					</c:when>


																					<c:otherwise>
																						<c:forEach items="${productsList}" var="element"
																							varStatus="counter">
																							<input type="hidden" id="productCodeCheckValue"
																								name="productCodeCheckValue"
																								value="<c:out value="${element.dspProductCode}" />" />
																							<tr>
																								<td><label><input type="checkbox"
																										id="productCodeCheckCounter"
																										name="productCodeCheckCounter"
																										value="<c:out value="${counter.count}" />"></label>
																								</td>
																								<td><c:out
																										value="${element.dspProductCodeName}" /></td>
																								<td><select class="form-control"
																									name="agentBusinessType" id="agentBusinessType">
																										<option value="">Select Type</option>
																										<option value="D">Direct</option>
																										<option value="ND">Non Direct</option>
																								</select></td>
																								<td><c:out value="${element.productEntity}" /></td>
																								<td><input id="in_agentCode"
																									name="in_agentCode" type="text"
																									class="form-control" size="10"></td>
																								<td><input id="in_discount"
																									name="in_discount" type="text"
																									pattern="\d{1,5}" maxlength="3"
																									class="form-control" size="5"></td>
																								<td><input id="in_commission"
																									name="in_commission" type="text"
																									pattern="\d{1,5}" maxlength="3"
																									class="form-control" size="5"></td>
																							</tr>
																						</c:forEach>
																					</c:otherwise>
																				</c:choose>
																				<!--  <tr><td><label><input type="checkbox" value=""></label></td><td>Car Accident Protection Special<br>(CAPS)</td><td>Non Direct</td><td>EIB</td><td> <input type="text" class="form-control" id="usr"></td>
                                                                    <td><input type="text" class="form-control" id="usr"></td>
                                                                    <td><input type="text" class="form-control" id="usr"></td>
                                                                    </tr>
                                                                    <tr><td><label><input type="checkbox" value=""></label></td><td>HouseOwner/HouseHolder</td><td>Non Direct</td><td>EIB</td><td> <input type="text" class="form-control" id="usr"></td>
                                                                    <td><input type="text" class="form-control" id="usr"></td>
                                                                    <td><input type="text" class="form-control" id="usr"></td>
                                                                    </tr>
                                                                    <tr><td><label><input type="checkbox" value=""></label></td><td>World Traveller Care</td><td>Non Direct</td><td>EIB</td><td> <input type="text" class="form-control" id="usr"></td>
                                                                    <td><input type="text" class="form-control" id="usr"></td>
                                                                    <td><input type="text" class="form-control" id="usr"></td>
                                                                    </tr>
																	<tr><td><label><input type="checkbox" value=""></label></td><td>Ezy-Life Secure</td><td>Non Direct</td><td>EIB</td><td> <input type="text" class="form-control" id="usr"></td>
																	<td><input type="text" class="form-control" id="usr"></td>
																	<td><input type="text" class="form-control" id="usr"></td>
																	</tr> -->
																			</tbody>
																		</table>

																	</div>
																	<!-- /.table-responsive -->

																	<a href="#" onclick="submitProductMapForm();"
																		class="btn btn-warning btn-sm pull-right"><i
																		class="fa fa-edit"></i> Submit</a>
																</form:form>
															</div>
															<!-- /.the-box -->

														</div>
														<!-- End warning color table -->

													</div>

												</div>
											</c:if>


											<c:if test="${not empty agentProdMapList}">
												<div class="the-box">
													<div class="col-sm-12">
														<div class="title">
															<div class="sub">
																<h4>Product Referral Url</h4>
															</div>
														</div>
													</div>
													<div class="gap gap-mini"></div>
													<div class="col-lg-12">
														<div class="form-horizontal info-meor">
															<c:choose>
																<c:when test="${not empty agentProdMapList}">
																	<c:forEach items="${productsList}" var="element"
																		varStatus="counter">
																		<c:if
																			test="${element.dspProductCode ne 'CAPS' && element.dspProductCode ne 'DPPA'}">
																			<c:set var="found" value="n" />
																			<c:forEach items="${agentProdMapList}" var="apm_item">
																				<c:if test="${found eq 'n'}">
																					<c:choose>
																						<c:when
																							test="${apm_item.productCode eq element.dspProductCode}">
																							<c:set var="found" value="y" />
																							<c:set var="prodRefUrlVar"
																								value="${apm_item.prodRefUrl}" />
																							<div class="form-group">
																								<label class="col-sm-3 control-label"><c:out
																										value="${element.dspProductCodeName}" /></label>
																								<div class="col-sm-7">
																									<input type="text"
																										value="<c:out value="${prodRefUrlVar}" />"
																										class="form-control">
																								</div>
																								<div class="col-sm-2">
																									<a href="<c:out value="${prodRefUrlVar}" />"
																										target="_blank" class="btn btn-warning btn-sm"><i
																										class="fa fa-edit"></i> Preview</a>
																								</div>
																							</div>
																						</c:when>
																						<c:otherwise>
																							<%--   <c:set var="prodRefUrlVar" value=""/> --%>
																						</c:otherwise>
																					</c:choose>
																				</c:if>

																			</c:forEach>

																		</c:if>
																	</c:forEach>
																</c:when>
															</c:choose>



															<!-- <div class="form-group">
                                                                <label class="col-sm-3 control-label">WTC</label>
                                                                <div class="col-sm-7">
                                                                    <input type="text" class="form-control">
																	</div> 
																	<div class="col-sm-2"><a href="agent-list-of-perator.html" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i> Preview</a>
                                                                </div>
                                                              </div> -->

														</div>
													</div>

												</div>
											</c:if>
										</div>
										<!--content inner -->
									</div>
									<!--col-sm-12 -->
								</div>
								<div class="col-sm-12">
									<div class="text-right">
										<!-- <input class="btn btn-warning btn-sm" onClick="submitRegistration();" type="submit" value="Submit"/> -->
									</div>
								</div>
								<!--col-sm-12 -->
							</div>
							<!--row -->
							</form>
							<form:form id="deleteProductForm" name="deleteProductForm"
								action="deleteAgentProductMap" method="POST">
								<input type="hidden" name="agentCode_delete"
									id="agentCode_delete" value="" />
								<input type="hidden" name="productCode_delete"
									id="productCode_delete" value="" />
								<input type="hidden" name="id"
									value="<c:out value="${agentProfile.id}" />" />
								<input type="hidden" name="agentType"
									value="<c:out value="${agentProfile.agentType}" />" />
								<input type="hidden" name="insuranceType"
									value="<c:out value="${agentProfile.insuranceType}" />" />
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>
	<!-- /.container -->
	</div>
	<!-- /.page-content -->



	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->

	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
            <div id="back-top">
                <a href="#top"><i class="fa fa-chevron-up"></i></a>
            </div>
            -->
	<!-- END BACK TO TOP -->


	<!--
            ===========================================================
            END PAGE
            ===========================================================
            -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%--  <jsp:include page="siteFooter.jsp" /> --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->
	<script src="assets/js/apps.js"></script>
	<script src="assets/plugins/retina/retina.min.js"></script>
	<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>


	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script type="text/javascript" src="assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript" src="assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>

	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/buttons.print.min.js"></script>
	<script src="assets/js/buttons.colVis.min.js"></script>
	<script src="assets/select2/dist/js/select2.min.js"
		type="text/javascript"></script>
	<script type="text/javascript">
        
         var capsSelected="n";
         var miSelected="n";
         var dppaSelected="n";
         var mtSelected="n";
         var midSelected="n"
// --------Start-Delete Products        
         function deleteProduct(agentCode_delete,productCode_delete) {        	
        	//alert(agentCode_delete);
        	//alert(productCode_delete);
        	document.getElementById("agentCode_delete").value=agentCode_delete;
        	document.getElementById("productCode_delete").value=productCode_delete;
        	/*  $('#agentID_delete').val(agentID);
        	 $('#agentCode_delete').val(agentCode);
        	 $('#productCode_delete').val(productCode); */
        	 
        	 var r = confirm("Are You Sure You Want to Delete this Product!");
        	 if (r == true) {
        		 $('#deleteProductForm').submit();  
        	 } 
          }

// --------End-Delete Products   


// --------Start-Save Products         
	       function submitProductMapForm() {
	    	   var result=true;
	    	   var countSelected=0;
	    	   // Check if no product(s) selected
	    	   $('[name="productCodeCheckCounter"]').each(function() {	    		    
		             if($(this).is(':checked') ){
		            	  countSelected=countSelected+1;
						}	  
	    	      });
	    	     
	    	      if (countSelected==0) {
	 	    		  alert("No Product(s) Selected!"); 
	 	    		  return
	 	    	   }
	    	   // Check if type and discount/commission entered but no product(s) selected 
	    	   $('[name="productCodeCheckCounter"]').each(function() {	    	   
	    	     // if($(this).not(':checked')) {	 	    	    	  
	    	    	  var agentCode= $(this).closest("tr").find('[name="in_agentCode"]');   
			             if((agentCode.val().length) && !$(this).is(':checked') ){
			            	 $(this).addClass("inputError");
			            	 //alert("1");
							  result=false;
							}	  
			               			               
						var d_nd= $(this).closest("tr").find('[name="agentBusinessType"]'); 
						//alert (d_nd.val());
						if((d_nd.val().length && !$(this).is(':checked'))){
							 $(this).addClass("inputError");
							// alert("2");
							result=false;
						}	
						
						if (d_nd.val()=='D') {
							var in_discount= $(this).closest("tr").find("input[name=in_discount]");
							if((in_discount.val().length && !$(this).is(':checked'))){								
								 $(this).addClass("inputError");
								// alert("3");
								result=false;
							}	
						}
						if (d_nd.val()=='ND') {
							var in_commission= $(this).closest("tr").find("input[name=in_commission]");
							if((in_commission.val().length && !$(this).is(':checked'))){								
								 $(this).addClass("inputError");
								// alert("3");
								result=false;
							}	
						}
	     
	    	   });
	    	   
	    	   if (result==false ) {
 	    		  alert("Product(s) Not Selected!"); 
 	    		 return
 	    	   }
	    	   // Check if product(s) selected  but no  type and discount/commission entered 
	    	   $('[name="productCodeCheckCounter"]').each(function() {					 
	    		      if($(this).is(":checked")){
			               // alert("Checkbox is checked.");
			             var agentCode= $(this).closest("tr").find('[name="in_agentCode"]');   
			             if(!(agentCode.val().length)){			            	
			            	  agentCode.addClass("inputError");
								result=false;
							}	  
			               			               
						var d_nd= $(this).closest("tr").find('[name="agentBusinessType"]'); 
						//alert (d_nd.val());
						if(!(d_nd.val().length)){
							d_nd.addClass("inputError");
							result=false;
						}	
						
						if (d_nd.val()=='D') {
							var in_discount= $(this).closest("tr").find("input[name=in_discount]");
							if(!(in_discount.val().length)){								
								in_discount.addClass("inputError");	
								result=false;
							}	
						}
						if (d_nd.val()=='ND') {
							var in_commission= $(this).closest("tr").find("input[name=in_commission]");
							if(!(in_commission.val().length)){								
								in_commission.addClass("inputError");
								result=false;
							}	
						}
						 var td2 = $(this).closest("tr").find("td").eq(1).text();
						  if (td2=="CAPS") {
							  capsSelected="y";
						  }						  
						  var td2 = $(this).closest("tr").find("td").eq(1).text();
						  if (td2=="Motor Insurance") {
							  miSelected="y";
						  }

						  if (td2=="Motor Insurance Detariff") {
							  midSelected="y";
						  }
						  
						  var td2 = $(this).closest("tr").find("td").eq(1).text();
						  if (td2=="DPPA") {
							  dppaSelected="y";
						  }						  
						  var td2 = $(this).closest("tr").find("td").eq(1).text();
						  if (td2=="Motor Takaful") {
							  mtSelected="y";
						  }

						  						  
			            } // End- if($(this).is(":checked")){							    		   

		    	   });
	    	   // check if CAPS selected without adding MOTOR INSURANCE
	    	   if (capsSelected=="y" && miSelected=="n") {
					 alert ("please select 'Motor Insurance' Product first");
					 result=false;
				  }
	    	   if (dppaSelected=="y" && mtSelected=="n") {
					 alert ("please select 'Motor Takaful' Product first");
					 result=false;
				  }
	    	 // Check agentCode uniqueness
	    	/*  $('[name="in_agentCode"]').each(function() {	
	    		 
	    	          var code= $(this).val();
	    		      var agentExist=validateAgentCode($(this));
	    		      if (agentExist == 'y') {
	    		    	  $(this).addClass("inputError");	    		    	 
	    		    	   result=false;
	    		    	   alert ("Agent Code " + code  +" Already Exist");
	    		    	   $(this).focus();
	    		      }
	    	 }); */

	    	if (result==true) {	    		
	    		  var agentProdMapListSize="<c:out value="${agentProdMapListSize}"/>";	
	    		  if (parseInt(agentProdMapListSize) > 0) {
	    			 // Confirm Update
	    			 var r = confirm("Are You Sure You Want to Update!");
	            	 if (r == true) {
	            		 $('#productMapForm').submit(); // Update
	            	 } else {
	            		  $('#productSearchForm').submit();  // To referesh the page and flush the entered values
	            	 }
	            	 // Confirm Update
	            	 } else {
	            		$('#productMapForm').submit();  // Add 
	            	 }//if  agentProdMapListSize      	    		  
	    	}   
	    	
	    	   
	       }
// --------End-Save Products  
//-------------------------------------Change discont/comission to read only-------------------------------------
  $('[name="agentBusinessType"]').on('change', function() {
            	$(this).removeClass("inputError");
				 if (this.value == 'D') {
					 $(this).closest("tr").find("input[name=in_commission]").attr('readonly','readonly');//attr("disabled", "disabled"); 
					 $(this).closest("tr").find("input[name=in_commission]").removeClass("inputError");
					 $(this).closest("tr").find("input[name=in_commission]").val("");
					 $(this).closest("tr").find("input[name=in_discount]").removeAttr('readonly');//attr('disabled',false); 
					 
				  }
				  if (this.value == 'ND') {
					  $(this).closest("tr").find("input[name=in_discount]").attr('readonly','readonly');//attr("disabled", "disabled");
					  $(this).closest("tr").find("input[name=in_discount]").removeClass("inputError");
					  $(this).closest("tr").find("input[name=in_discount]").val("");
					  $(this).closest("tr").find("input[name=in_commission]").removeAttr('readonly');//attr('disabled',false); 
					 
				  } 
				 
				});	
//----------------------------------------------------------------------------------------------  
$('[name="in_agentCode"]').keyup(function(){
	$(this).removeClass("inputError");
});
$('[name="in_discount"]').keyup(function(){
	$(this).removeClass("inputError");
});
$('[name="in_commission"]').keyup(function(){
	$(this).removeClass("inputError");
});

//-----------------------Start-Show Products ------------------------------------------------  
	       function  submitProductSearchForm() {
	    	   var result=true;	    	   
	    	   if(!( $('.js-example-basic-single').val().length)){
	    		   //$('.js-example-basic-single').addClass("inputError");
	    		    $(".select2-container.form-control").css("border", "1px solid red");  
					result=false;
				}
	    	   if(!( $('#insuranceType').val().length)){
	    		   $('#insuranceType').addClass("inputError");
					result=false;
				}	
	    	   if (result==true) {
		    		//alert ("Submit");
	    		   $('#productSearchForm').submit();   
		    	}   
	    			       	   
	       }
//----------------------End-Show Products--------------------------------------------------------

 $('.js-example-basic-single').on('change', function() {
	 if( $(this).val().length){
		 $(".select2-container.form-control").css("border", "1px solid #ccc");
		  
		}
	});	 

 $('#insuranceType').on('change', function() {
 if( $('#insuranceType').val().length){
	   $('#insuranceType').removeClass("inputError");
		
	}
 });	 
//----------------------------------------------------------------------------------------------


//No space for agentCode
$('[name="in_agentCode"]').on("keydown", function (e) {
    return e.which !== 32;
});

            $(document).ready(function() {
            	 $(".js-example-basic-single").select2();
            	 $(".select2").addClass("form-control");            	
            	 $(".select2").removeClass("select2-container--default");
            	
            	 
            });
            // Force Numeric Only 
            jQuery.fn.ForceNumericOnly = function() {
                return this.each(function() {
                    $(this).keydown(function(e) {
                        var key = e.charCode || e.keyCode || 0;
                        // allow backspace, tab, delete, arrows, letters, numbers and keypad numbers ONLY
                        return (
                            key == 8 || 
                            key == 9 ||
                            key == 46 ||                          
                            (key >= 48 && key <= 57));                
                    })
                })
            };
            jQuery.fn.ForceNoSpace = function() {
                return this.each(function() {
                    $(this).keydown(function(e) {
                        var key = e.charCode || e.keyCode || 0;
                        // allow backspace, tab, delete, arrows, letters, numbers and keypad numbers ONLY
                        return (
                            key == 32 
                            );                
                    })
                })
            };
            // Function Numbers
            //for (var i=1;i<=counter;i++){
            /*$("#email").validateEmail();*/
           // $("#in_agentCode").ForceNoSpace();
           // $("#agentCode").ForceNumericOnly();
            $('[name="in_discount"]').ForceNumericOnly();
            $('[name="in_commission"]').ForceNumericOnly();
            $("#phoneNo").ForceNumericOnly();
            $("#postcode").ForceNumericOnly();
            //}

            /////////////// Fetching State from Postcode ////////////////  
            /*function fetch_state() { 
                //alert(document.getElementById("postcode").value);
                var obj;
                var arr = [];
                var formData = {postcode:document.getElementById("postcode").value}; //Array 
                if (document.getElementById("postcode").value.length != 0 && document.getElementById("postcode").value.length >= 4) {
                    $.ajax({
                        type: "post", 
                        url: "/getonline/sites/REST/controller/TravellerController/fetch_State",
                        data: formData,
                        success: function (data) {
                            if(data) {
                                document.getElementById("state").value=data;
                            }

                        },
                        error: function (request, status, error) {
                            alert(request.responseText);
                        }
                    }); 
                } 
            }*/
            function validateEmail(sEmail) {
                var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if (filter.test(sEmail)) {
                    return true;
                }
                else {
                    return false;
                }
            }

            function validatePhone(postcode) {
                var a = document.getElementById(postcode).value;
                var filter = /^[0-9-+]+$/;
                if (filter.test(a)) {
                    return true;
                }
                else {
                    return false;
                }
            }

            function checkOnBlur(me, message) {
                var  e_id = $(me).attr('id');
                if (!(me.value).length) {
                    var exist= document.getElementById(e_id+ '_e');
                    if (exist == null) {
                        var htmlString="";
                        htmlString+='<label id='+ e_id+ '_e' + '  ' + 'class="error">'+ message + '</label>';
                        $(htmlString).insertAfter("#"+ e_id);
                        $('#'+ e_id).focus();
                    }
                }
            }
            //////////////////////////////////////////////////////////////////////////////////////////////  
            /*function checkKeyUp(me) {
                var  e_id = $(me).attr('id');
                if($.trim(me.value).length) {
                    var exist= document.getElementById(e_id+ '_e');
                    if (exist != null) {
                        $('#'+ e_id + '_e').remove();
                    }
                }
            }*/
            //////////////////////////////////////////////////////////////////////////////////////////////
            function checkOnChange(me) {
                var  e_id = $(me).attr('id');
                if($.trim(me.value).length) {
                    var exist= document.getElementById(e_id+ '_e');
                    if (exist != null) {
                        $('#'+ e_id + '_e').remove();
                    }
                }
            }

            function getAgentType(agent) {
		    	
    		    var txt=agent.value;
    		    $.ajax({
    	        url: 'getAgentType.html',
    	        type: 'GET',
    	        data: ({agentID : txt}),
    	        success: function(data) {
    	        	var obj = jQuery.parseJSON( data );
    	        	if(obj.agentType == 'D') {
    	        		$("#agentType").val("Direct");
    	        	}    	        	
					if(obj.agentType == 'ND') {
						$("#agentType").val("Non Direct");	
    	        	}
    	        	
    	        }
    	      });
    		    }
           
            
            // Check Agent Code uniqueness
            function validateAgentCode(agCode) {	
			agentCodeExist="n";		
			var agentCodeEntered=agCode.val();
			if(agentCodeEntered.length){					 		   
			    $.ajax({
		        url: 'validateAgentCode.html',
		        type: 'GET',
		        async: false,
		        data: ({agentCode : agentCodeEntered}),
		        success: function(data) {
		        	var obj = jQuery.parseJSON( data );
		        	if(obj.validateAgentCodeFlag == 'y') {
		        		 agCode.addClass("inputError");	        		
		        		 agentCodeExist="y";	        		        		  
		        	}    	        	
					  	        	
		            }
		        });
			 } 
				return 	agentCodeExist; 
 		    }
           
        </script>
</body>
</html>