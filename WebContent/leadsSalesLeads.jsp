<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>Etiqa</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}
</style>
<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}
</style>

<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet"
	href="assets/plugins/datepicker/datepicker.min.css">
<!-- PLUGINS CSS -->
<link href="assets/plugins/owl-carousel/owl.carousel.min.css"
	rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.theme.min.css"
	rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.transitions.min.css"
	rel="stylesheet">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />

<!--<link href="css/style.css" rel="stylesheet">-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
.bbdot {
	border-bottom: 1px dotted #333;
	padding-bottom: 10px;
	margin-bottom: 10px;
}

.text-right {
	text-align: right;
}

.btn-primary {
	background-color: #ffcf43;
	border-color: #ffcf43;
	color: #333;
}

.btn-primary:hover, .btn-primary:active {
	background-color: #ffcf43;
	border-color: #ffcf43;
	color: #fff;
}

table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

form#agentE, form#agentD {
	display: inline-block;
}

form#agentE input[type=submit], form#agentD input[type=submit] {
	padding: 5px;
}
</style>
</head>
<script>
    function validateForm() {
    	var x = document.forms["leadsreport"]["fromDate"].value;
  	    var y = document.forms["leadsreport"]["toDate"].value;
  	   if ( x == null || x == "" ) {
  		   if(y !=""){
  	      alert("Please choose Date From");
  	      return false;
  	   } 
  		  
  	   }
  	   return true;
    }
 
 </script>
<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>

		<!-- header -->

		<!-- end header -->
		<!-- header second-->
		<!--             <div class="header-second">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 hidden-xs">
                            Begin breadcrumb
                            <ol class="breadcrumb">
                                <li><i class="fa fa-home"></i></li>
                                <li><a href="admin-system-user-role.html">Sales Leads Portal Registration</a></li>
                            </ol>
                            End breadcrumb
                        </div>
                        <div class="col-sm-6 hidden-xs">
                            <div class="col-sm-8">
                                <div class="text-right last-login">
                                    Your last login was on 01 Feb 2016  10:01:01
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="language">
                                    <select class="selectpicker">
                                        <option value="United States">English</option>
                                        <option value="United Kingdom">Malay</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 visible-xs-block">
                            <div class="mobile-view">
                                <ul class="nav-user navbar-right">
                                    <li class="dropdown">
                                      <a href="#fakelink" class="dropdown-toggle text-right" data-toggle="dropdown">
                                        <i class="fa fa-user"></i>
                                        Welcome <strong>Siti Amirah Umar</strong>
                                        <i class="fa fa-angle-down fa-lg"></i>
                                      </a>
                                      <ul class="dropdown-menu square primary margin-list-rounded with-triangle pull-right">
                                        <li><a href="security-setting.html">Security setting</a></li>
                                        <li class="divider"></li>
                                        <li><a href="logout.html">Log out</a></li>
                                      </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
		<!-- header second-->
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>

					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Leads Management &amp;
														Campaign</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">

												<div class="col-sm-12">

													<div class="content-inner">
														<div class="the-box">
															<div>
																<div class="bbdot">
																	<div class="row">
																		<div class="col-xs-6">
																			<div class="newtitle">
																				<div class="sub">
																					<h4>Sales Leads</h4>
																				</div>
																			</div>

																			<input type="hidden" name="startRow" value="0" /> <input
																				type="hidden" name="endRow" value="10" /> <input
																				type="hidden" id="productType" name="productType"
																				value="MI" /> <input type="hidden"
																				id="productTypeValSession"
																				name="productTypeValSession"
																				value="${sessionScope.ProductType}" /> <input
																				type="hidden" id="productEntityValSession"
																				name="productEntityValSession"
																				value="${sessionScope.ProductEntity}" />
																		</div>
																		<form:form action="uploadCSVFile" method="post"
																			enctype="multipart/form-data" id="registration">
																			<div class="col-xs-10 text-right">
																				<div class="col-sm-6">
																					<input type="file" name="CSV" id="browsecsv"
																						accept=".csv">
																				</div>
																				<div class="col-sm-4">
																					<label class="btn btn-default disabled"
																						id="uploadcsv"> Upload <input
																						type="submit" value="submit"
																						style="display: none;">
																					</label>
																				</div>
																			</div>
																		</form:form>
																	</div>
																</div>
															</div>
															<form:form name="leadsreport" action="searchSalesLeads"
																id="leadsreport" method="post">
																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Date :
																			</label>
																			<div class="col-sm-4">
																				<input type="text" placeholder="Date From"
																					class="form-control required" name="fromDate"
																					id="fromDate" value="${sessionScope.fromDate}">
																				<span id="msg_fromdate" class="hidden"
																					style="color: red; text-align: left">Please
																					select to proceed</span>
																			</div>

																			<div class="col-sm-5">
																				<input type="text" placeholder="Date To"
																					class="form-control" name="toDate" id="toDate"
																					value="${sessionScope.toDate}"> <span
																					id="msg_todate" class="hidden"
																					style="color: red; text-align: left">Please
																					select to proceed</span>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Agent
																				Code : </label>
																			<div class="col-sm-9">
																				<input type="text" placeholder="Agent Code"
																					id="agentCode" class="form-control"
																					name="agentCode" value="${sessionScope.agentCode}">
																				<span id="msg_agentcode" class="hidden"
																					style="color: red; text-align: left">Please
																					select to proceed</span>
																			</div>

																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Agent
																				Name : </label>
																			<div class="col-sm-9">
																				<input type="text" placeholder="Agent Name"
																					id="agentName" class="form-control"
																					name="agentName" value="${sessionScope.agentName}">
																				<span id="msg_agentname" class="hidden"
																					style="color: red; text-align: left">Please
																					select to proceed</span>
																			</div>
																		</div>

																	</div>
																	<div class="col-sm-6 text-right">
																		<!--     <a href="searchSalesLeads" class="btn btn-primary" onClick="submitLeads();">Submit</a> -->
																		<button type="submit" class="btn btn-primary">Submit</button>
																	</div>
																	<!-- <div class="form-horizontal">
                                                                  <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Select Product</label>
                                                                    <div class="col-sm-9">
                                                                        <select class="form-control">
                                                                            <option value="-View All-">-View All-</option>
                                                                              <option value="Individual">Individual</option>
                                                                              <option value="Company/Organisation">Company/Organisation</option>
                                                                        </select>
                                                                    </div>
                                                                  </div>
                                                                </div> -->
																</div>
															</form:form>
															<div class="col-sm-6"></div>


														</div>
													</div>
												</div>

												<!-- END FORM -->
												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="row">
																<div class="col-xs-12 form-inline">
																	<div class="col-xs-6">
																		<div class="row"></div>
																	</div>
																	<!--  <div class="col-xs-6">
                                                                        <div class="row">
                                                                            <div class="pull-right">
                                                                                <button class="btn btn-warning btn-sm">Print <i class="fa fa-print" aria-hidden="true"></i></button>
                                                                                <button class="btn btn-warning btn-sm">Export to XLS <i class="fa fa-download" aria-hidden="true"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div> -->
																</div>
															</div>


															<div class="text-right" id='btnhere'>

																<a href="createExcelForSalesLeads"><button
																		class="btn btn-warning btn-sm">
																		Export to XLS <i class="fa fa-download"
																			aria-hidden="true"></i>
																	</button></a>


															</div>
															<br />
															<div class="table-responsive" id="MyTable">
																<table
																	class="table table-striped table-warning table-hover"
																	id="salesLeadsTable">
																	<thead>
																		<tr>
																			<th style="width: 30px;">No</th>
																			<th>Date</th>
																			<th>Name</th>
																			<th>Vehicle Reg No.</th>
																			<th>Details</th>
																			<th>Agent</th>
																	</thead>
																	<tbody>
																		<c:forEach items="${mtRoadTaxRenewalList}"
																			var="element" varStatus="theCount">
																			<tr>
																				<td style="width: 30px;"><c:out
																						value="${theCount.count}" /></td>
																				<c:set var="createDate"
																					value="${element.createDate}" />
																				<%-- <td> <fmt:formatDate type="both" value="${createDate}" pattern="dd-MMM-yyyy hh:mm:ss"  /></td> --%>
																				<td><c:out value="${element.createDate_STR}" /></td>
																				<td><c:out value="${element.customerName}" /></td>
																				<td><c:out value="${element.vehregno}" /></td>
																				<c:set var="expDate" value="${element.rdtaxexpiry}" />
																				<td><b>Email : </b>
																				<c:out value="${element.email}" /><br /> <b>Mobile
																						No : </b>
																				<c:out value="${element.phoneno}" /><br /> <%--  <b>Expiry Month : </b><fmt:formatDate type="both" value="${expDate}" pattern="MMMM"  /> --%>
																					<b>Expiry Month : </b> <c:out
																						value="${element.rdtaxexpiry}" /><br /></td>
																				<td><b>Agent Code : </b>
																				<c:out value="${element.agentCode}" /><br /> <b>Agent
																						Name : </b>
																				<c:out value="${element.agentName}" /></td>
																			</tr>
																		</c:forEach>
																	</tbody>
																</table>
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>
												<!--   <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="col-sm-6">
                                                        Showing 1 to 5 of 15 entries
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <ul class="pagination pull-right">
                                                          <li class="disabled"><a href="#">&laquo;</a></li>
                                                          <li class="active"><a href="#">1</a></li>
                                                          <li><a href="#">2</a></li>
                                                          <li><a href="#">3</a></li>
                                                          <li><a href="#">4</a></li>
                                                          <li><a href="#">5</a></li>
                                                          <li><a href="#">&raquo;</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                </div> -->

											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->
		<!-- BEGIN FOOTER -->

		<!-- END FOOTER -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->

	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->

	<script>
	     function submitLeads(){
	         //result=true;     
	         if(!$('#fromDate').val().length) {
	             $('#msg_fromdate').removeClass('hidden');
	             $('#msg_fromdate').addClass('visible');
	             /* $('#referenceNo').css("border-color", "red"); */
	             $('#fromDate').focus();
	             result=false;
	         }
	         if(!$('#toDate').val().length) {
	             $('#msg_todate').removeClass('hidden');
	             $('#msg_todate').addClass('visible');
	             $('#toDate').focus();
	             result=false;
	         }

	         if (result==true) {
	         	$('#leadsreport').submit();   
	         }
	         
	     }
	     
	  //   $('#browsecsv').val()
	 
		$(document).ready(function() {
			// validation for restricting only csv files to upload
			$('#browsecsv').change(
                function () {
                    var fileExtension = ['csv'];
                    if($('#browsecsv').val() != ""){
         				  $('#uploadcsv').removeClass('disabled');
         			  }else{
         				  $('#uploadcsv').addClass('disabled');
         			  }
                    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                        alert("Only '.csv' format is allowed.");
                        this.value = ''; // Clean field
                        return false;
                    }
                   
                });
					var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();

			if(dd<10) {
			    dd='0'+dd
			} 

			if(mm<10) {
			    mm='0'+mm
			} 

			today = dd+'/'+mm+'/'+yyyy;
			
			$('#salesLeadsTable').DataTable({
				 "order": [],
				dom : 'Bfrtip',
				title : ' Agent List',
			

				buttons : [  'print',  {
					extend : 'pdfHtml5',
					title :  'Sales_Leads_'+today
				} ]
			});
			 $('#fromDate').datepicker({
				    format: 'dd/mm/yyyy',
			         }).on('changeDate', function(e){
	 			    $(this).datepicker('hide');
	 			
				});
		       
			 $('#toDate').datepicker({
				    format: 'dd/mm/yyyy',
			        }).on('changeDate', function(e){
	 			    $(this).datepicker('hide');
	 			    });
			 
	    });
		
	        
	        
	
	
</script>

</body>
</html>