<%@ page import="java.io.*,java.util.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<link rel="stylesheet" href="assets/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/menu.css">
<script src="assets/js/jquery.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.js" type="text/javascript"></script>
</head>

<body>
	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->


	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Cyber Operator Management</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<form action="emailsend" method="post"
												enctype="multipart/form-data" id="updatepassword">
												<!--  <input type="hidden" name="action" value="changepassword">-->
												<div class="row">
													<div class="col-sm-12">
														<div class="title">
															<div class="sub">
																<h4>Cyber Operator Management</h4>
															</div>
														</div>
														<div class="content-inner">
															<div class="the-box">
																<div class="row">
																	<div class="col-sm-12">
																		<div class="title">
																			<div class="sub">
																				<h4>Operator Forget Password</h4>
																			</div>
																		</div>
																	</div>
																	<div class="gap gap-mini"></div>
																</div>

																<div class="row">
																	<div class="col-md-8">
																		<div class="form-horizontal info-meor">
																			<div class="col-xs-12">
																				<c:if test="${(not empty  forgetpw_error)}">
																					<div style="color: red">
																						<c:out value="${changepw_error}" />
																					</div>
																				</c:if>
																			</div>
																			<div class="col-xs-12">
																				<c:if test="${(not empty  forgetpw_success)}">
																					<div style="color: green">
																						<c:out value="${changepw_success}" />
																					</div>
																				</c:if>
																			</div>
																			<div class="form-group">
																				<div class="col-sm-6 column text-right">
																					<label class="control-label"> Email Address</label>
																				</div>
																				<div class="col-sm-6">
																					<input type="text" class="form-control"
																						name="email" required id="email"> <span
																						id="msg_email" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your email address</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<div class="col-sm-6 column text-right">
																					<label class="control-label">User Name </label>
																				</div>
																				<div class="col-sm-6">
																					<input type="text" class="form-control"
																						name="username" required id="username"> <span
																						id="msg_curuser" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your user name</span>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>



															</div>


														</div>
														<!--content inner -->
													</div>
													<!--col-sm-12 -->

													<div class="col-sm-12">
														<div class="text-right">
															<input class="btn btn-warning btn-sm"
																onClick="submitForm();" type="submit" value="Submit" />
														</div>
													</div>
													<!--col-sm-12 -->
												</div>
												<!--row -->
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->
		</div>
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->

	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
            <div id="back-top">
                <a href="#top"><i class="fa fa-chevron-up"></i></a>
            </div>
            -->
	<!-- END BACK TO TOP -->


	<!--
            ===========================================================
            END PAGE
            ===========================================================
            -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->

	<script>
        $('#fine-uploader-gallery').fineUploader({
            template: 'qq-template-gallery',
            request: {
                endpoint: '/server/uploads'
            },
            thumbnails: {
                placeholders: {
                    waitingPath: '/source/placeholders/waiting-generic.png',
                    notAvailablePath: '/source/placeholders/not_available-generic.png'
                }
            },
            validation: {
                allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
            }
        });
    </script>

	<script type="text/javascript">
            function submitForm(){
                result=true;
                if(!$('#email').val().length) {
                    $('#msg_email').removeClass('hidden');
                    $('#msg_email').addClass('visible');
                    $('#email').focus();
                    result=false;
                }
                if(!$('#username').val().length) {
                    $('#msg_curuser').removeClass('hidden');
                    $('#msg_curuser').addClass('visible');
                    $('#username').focus();
                    result=false;
                }
               
             
            }

            $('#email').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_email').removeClass('visible');
                    $('#msg_email').addClass('hidden');
                }
            });
            $('#username').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_curuser').removeClass('visible');
                    $('#msg_curuser').addClass('hidden');
                }
            });
           
            


        </script>
</body>
</html>