
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>ETIQA</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}
</style>
<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}
</style>
</head>
<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->

	<!-- header -->
	<div class="header">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="visible-xs-block">
						<!-- Begin button sidebar left toggle -->
						<div class="btn-collapse-sidebar-left logo">
							<i class="fa fa-bars"></i>
						</div>
						<!-- /.btn-collapse-sidebar-left -->
						<!-- End button sidebar left toggle -->
					</div>
					<div class="logo">
						<img class=""
							src="${pageContext.request.contextPath}/assets/images/logo.png"
							alt="logo">
					</div>
					<div class="title">Cyber Agent</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-sm-6">
					<!-- BEGIN TOP NAV -->
					<p align="right" valign="top">
						Welcome! <b><c:out value="<%=session.getAttribute("userloginName")%>" /></b>dhana <b>(</b><a
							href="agentLogout">Logout</a><b>)</b>
					</p>
					<!-- END TOP NAV -->
					<div class="clearfix"></div>
				</div>
			</div>
			<!-- /.row -->
		</div>
	</div>
	<!-- end header -->
</body>
</html>
