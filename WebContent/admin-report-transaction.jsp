<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.math.BigDecimal"%>
<%@ page import="java.math.RoundingMode"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="com.etiqa.model.report.TransactionalReport"%>
<% java.text.DateFormat df=new java.text.SimpleDateFormat("dd/MM/yyyy"); %>


<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>

</head>

<body>
	<%
        
       
   	 DecimalFormat formatter = new DecimalFormat("#,###.00");
        
 if (request.getMethod().equals("GET")){
			
			session.setAttribute("ProductType", "");		
			session.setAttribute("Status", "");
			session.setAttribute("PolicyCertificateNo", "");
			session.setAttribute("NRIC","");
			session.setAttribute("ReceiptNo", "");
			session.setAttribute("dateFrom","");
			session.setAttribute("dateTo","");
			session.setAttribute("DateFrom", "");
			session.setAttribute("DateTo","");
			session.setAttribute("PlateNo","");
			session.setAttribute("ProductEntity","");
			
 }
			
        int totalRecord = 0;
        int recordPerPage =10;
        int pageCount =0;
        int startRow=0;
        int endRow=0;
        //temporary 
        endRow=recordPerPage;
        
       int start=0;       
       int end=0;
       double lastPage=0;
       String textColor="";
       String status="";
       String totalAmount="0.00";
       String transactionStatus="";
       String lastStep="";
       String nextStep="";
       String reason="";
       int i=0;
       int rowLine=0;
       String txtColor="";
       
double totalGrossAmount=0.00;
double totalNettAmount=0.00;

      
        
        %>



	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Report &amp; Analytics</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Transactional Report</h4>

														</div>
													</div>
													<!-- Start Form -->
													<form:form name="txnreport" action="TransactionalReport"
														method="post" onsubmit="return validateForm()">
														<input type="hidden" name="action" value="list" />
														<input type="hidden" name="startRow" value="0" />
														<input type="hidden" name="endRow" value="10" />
														<input type="hidden" id="productType" name="productType"
															value="<c:out value="${sessionScope.ProductType}" />" />
														<input type="hidden" id="productTypeValSession"
															name="productTypeValSession"
															value="<c:out value="${sessionScope.ProductType}" />" />
														<input type="hidden" id="productEntityValSession"
															name="productEntityValSession"
															value="<c:out value="${sessionScope.ProductEntity}" />" />

														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-6">
																	<div class="form-horizontal">

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Search
																				by Date</label>
																			<div class="col-sm-4">

																				<fmt:parseDate value="${now}" var="parsedEmpDate"
																					pattern="dd/MM/yyyy" />

																				<input type="text" placeholder="Date From"
																					class="form-control" id="datepicker1"
																					name="dateFrom"
																					<c:choose>
																					    <c:when test="${not empty sessionScope.dateFrom }">
																					         value="<c:out value="${sessionScope.dateFrom}" />"																					     
																					    </c:when>
																					    <c:otherwise>
																					          value="<%= df.format(new java.util.Date())%>"
																					    </c:otherwise>
																					</c:choose> />
																			</div>
																			<div class="col-sm-5">
																				<input type="text" placeholder="Date To"
																					class="form-control" id="datepicker2" name="dateTo"
																					<c:choose>
																					    <c:when test="${not empty sessionScope.dateTo }">
																					         value="<c:out value="${sessionScope.dateTo}" />"																					     
																					    </c:when>
																					    <c:otherwise>
																					          value="<%= df.format(new java.util.Date())%>"
																					    </c:otherwise>
																					</c:choose> />

																			</div>
																		</div>

																		<!-- 
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Search
																				by Date</label>
																			<div class="col-sm-4">
																				<input type="text" placeholder="Date From"
																					class="form-control" id="datepicker1"
																					name="dateFrom" value="${sessionScope.dateFrom}">
																			</div>
																			<div class="col-sm-5">
																				<input type="text" placeholder="Date To"
																					class="form-control" id="datepicker2" name="dateTo"
																					value="${sessionScope.dateTo}">
																			</div>
																		</div>
																		 -->

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Search
																				by Product Entity</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="productEntity"
																					id="productEntity">
																					<option value="" selected>-View All-</option>
																					<option
																						<c:if test="${sessionScope.ProductEntity == 'EIB'}" >selected</c:if>
																						value="EIB">EIB</option>
																					<option
																						<c:if test="${sessionScope.ProductEntity == 'ETB'}" >selected</c:if>
																						value="ETB">ETB</option>
																				</select>
																			</div>
																		</div>

																		<div class="form-group" id="eib-pr" name="eib-pr">
																			<label class="col-sm-3 control-label">Product
																				Type</label>


																			<div class="col-sm-9" id="eib-list">

																				<select class="form-control" name="productTypeEIB"
																					id="productTypeEIB">
																					<option value=""
																						<c:if test="${sessionScope.ProductType == ''}" >selected</c:if>>-View
																						All-</option>
																					<option value="WTC"
																						<c:if test="${sessionScope.ProductType == 'WTC'}" >selected</c:if>>
																						World Traveller Care</option>
																					<option value="MI"
																						<c:if test="${sessionScope.ProductType == 'MI'}" >selected</c:if>>
																						Motor Insurance</option>
																					<option value="TL"
																						<c:if test="${sessionScope.ProductType == 'TL'}" >selected</c:if>>
																						Ezy-Life Secure</option>
																					<option value="building_content"
																						<c:if test="${sessionScope.ProductType == 'building_content'}" >selected</c:if>>
																						Houseowner Householder</option>
																				</select>


																			</div>

																			<div class="col-sm-9" id="etb-list">

																				<select class="form-control" name="productTypeETB"
																					id="productTypeETB">
																					<option value=""
																						<c:if test="${sessionScope.ProductType == ''}" >selected</c:if>>-View
																						All-</option>
																					<option value="TPT"
																						<c:if test="${sessionScope.ProductType == 'TPT'}" >selected</c:if>>
																						World Traveller Care Takaful</option>
																					<option value="MT"
																						<c:if test="${sessionScope.ProductType == 'MT'}" >selected</c:if>>
																						Motor Takaful</option>
																					<option value="building_content"
																						<c:if test="${sessionScope.ProductType == 'building_content'}" >selected</c:if>>
																						Houseowner Householder Takaful</option>
																				</select>


																			</div>


																			<div class="col-sm-9" id="all-list">

																				<select class="form-control" name="productTypeALL"
																					id="productTypeALL">
																					<option value=""
																						<c:if test="${sessionScope.ProductType == ''}" >selected</c:if>>-View
																						All-</option>
																					<option value="WTC"
																						<c:if test="${sessionScope.ProductType == 'WTC'}" >selected</c:if>>
																						World Traveller Care</option>
																					<option value="MI"
																						<c:if test="${sessionScope.ProductType == 'MI'}" >selected</c:if>>
																						Motor Insurance</option>
																					<option value="TL"
																						<c:if test="${sessionScope.ProductType == 'TL'}" >selected</c:if>>
																						Ezy-Life Secure</option>
																					<option value="building_content"
																						<c:if test="${sessionScope.ProductType == 'building_content'}" >selected</c:if>>
																						Houseowner Householder</option>
																					<option value="building"
																						<c:if test="${sessionScope.ProductType == 'building'}" >selected</c:if>>
																						Houseowner</option>
																					<!-- 	<option value="building_content"
																						<c:if test="${sessionScope.ProductType == 'HOHHT'}" >selected</c:if>>
																						Houseowner Householder Takaful</option> 
																					<option value="building"
																						<c:if test="${sessionScope.ProductType == 'HO'}" >selected</c:if>>
																						Houseowner</option>-->
																					<option value="TPT"
																						<c:if test="${sessionScope.ProductType == 'TPT'}" >selected</c:if>>
																						World Traveller Care Takaful</option>
																					<option value="MT"
																						<c:if test="${sessionScope.ProductType == 'MT'}" >selected</c:if>>
																						Motor Takaful</option>


																				</select>


																			</div>



																			<!--  <div class="col-sm-9">
                                                                         <select class="form-control" name="productType" id="productType">
                                                                            <option value=""  <c:if test="${sessionScope.ProductType == ''}" >selected</c:if>>-View All-</option>   
                                                                            <option  value="WTC" <c:if test="${sessionScope.ProductType == 'WTC'}" >selected</c:if>> WORLD TRAVEL CARE</option>                                             
                                                                            <option  value="MI" <c:if test="${sessionScope.ProductType == 'MI'}" >selected</c:if>> MOTOR INSURANCE</option>
                                                           <option  value="TL" <c:if test="${sessionScope.ProductType == 'TL'}" >selected</c:if>> EZY LIFE</option>                                                   
                                                                            <option  value="HOHH" <c:if test="${sessionScope.ProductType == 'HOHH'}" >selected</c:if>> HOME INSURANCE</option>
                                                                     
                                                                        </select>

                                                                        
                                                                    </div>-->






																		</div>


																		<div id="mi-option">
																			<div class="form-horizontal">
																				<!--  <label class="col-sm-3 control-label">Ecovernote Status</label>
                                                                    <div class="col-sm-9">
                                                                        <select class="form-control">
                                                                            <option value="-View All-" selected="">-View All-</option>
                                                                              <option value="a">All</option>
                                                                              <option value="b">Digunakan</option>
                                                                              <option value="c">Dibatalkan</option>
                                                                        </select>
                                                                    </div>
                                                                    -->

																				<div class="form-group">
																					<label class="col-sm-3 control-label">Vehicle
																						Registration No.</label>
																					<div class="col-sm-9">
																						<input type="text" id="plateNo" name="plateNo"
																							placeholder="" class="form-control"
																							value="<c:out value="${sessionScope.PlateNo}" />" />
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>

																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">NRIC/ID
																				No.</label>
																			<div class="col-sm-9">
																				<input type="text" placeholder=""
																					class="form-control" name="NRIC"
																					<%-- value="${sessionScope.NRIC}" --%>
																					 value="<c:out value="${sessionScope.NRIC}" />" />
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Policy/Certificate
																				No</label>

																			<div class="col-sm-9">
																				<input type="text" placeholder=""
																					class="form-control" name="policyNo"
																					value="<c:out value="${sessionScope.PolicyCertificateNo}" />" />
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Transaction
																				Status</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="status">
																					<option value=""
																						<c:if test="${sessionScope.Status == ''}" >selected</c:if>>-View
																						All-</option>
																					<option value="S"
																						<c:if test="${sessionScope.Status == 'S'}" >selected</c:if>>Successful</option>
																					<option value="F"
																						<c:if test="${sessionScope.Status == 'F'}">selected</c:if>>Fail
																						Payment</option>
																					<option value="AP"
																						<c:if test="${sessionScope.Status == 'AP'}" >selected</c:if>>Attempt
																						Payment</option>
																					<option value="O"
																						<c:if test="${sessionScope.Status == 'O'}" >selected</c:if>>Incomplete</option>
																					<option value="C"
																						<c:if test="${sessionScope.Status == 'C'}" >selected</c:if>>Cancelled</option>
																					<option value="R"
																						<c:if test="${sessionScope.Status == 'R'}"  >selected</c:if>>Rejection</option>
																				</select>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Tax
																				Invoice/Receipt No.</label>
																			<div class="col-sm-9">
																				<input type="text" placeholder=""
																					class="form-control" name="receiptNo"
																					value="<c:out value="${sessionScope.ReceiptNo}" />" />
																			</div>
																		</div>
																	</div>
																</div>


																<div class="col-sm-12 text-right">
																	<button type="submit" class="btn btn-default"
																		name="SEARCH">Search</button>
																</div>
															</div>
														</div>
													</form:form>
													<!-- END FORM -->
												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="row">
																<div class="col-xs-12 form-inline">
																	<div class="col-xs-6">
																		<!--   <div class="row">
                                                                        <label>
                                                                            <select class="form-control" disabled>
                                                                              <option value="5">5</option>
                                                                              <option value="10" selected>10</option>
                                                                              <option value="25">25</option>
                                                                              <option value="50">50</option>
                                                                              <option value="100">100</option>
                                                                            </select>
                                                                            records per page
                                                                        </label>
                                                                        </div>-->
																	</div>
																	<div class="col-xs-6">
																		<div class="row">
																			<% if (request.getAttribute("data") != null) {
														            List<TransactionalReport> list = (List<TransactionalReport>) request.getAttribute("data");
														           
														            
														            if(list.size() > 0 ){ %>
																			<div class="pull-right" id='btnhere'>
																				<!--  <button class="btn btn-warning btn-sm">
																					Print <i class="fa fa-print" aria-hidden="true"></i>
																				</button>-->
																				<a href="createExcel"><button
																						class="btn btn-warning btn-sm">
																						Export to XLS <i class="fa fa-download"
																							aria-hidden="true"></i>
																					</button></a>
																			</div>
																			<% } 
																		}%>


																		</div>
																	</div>
																</div>
															</div>
															<div class="table-responsive" id="MyTable">
																<table
																	class="table table-striped table-warning table-hover"
																	id="admin-datatable-second_admin">
																	<thead>
																		<tr>
																			<th colspan="2">Gross Amount</th>
																			<th colspan="2">Nett Amount</th>
																			<th colspan="4">Total Transaction Amount</th>

																		</tr>

																		<%        
                                                                if (request.getAttribute("data") != null) {
														            List<TransactionalReport> list = (List<TransactionalReport>) request.getAttribute("data");
														           										           
														           
														          
 if(list.size() > 0 ){
	 
	

	 for(TransactionalReport txn : list) {
		 
		   double gross=0.00;
	       double discount=0.00;
	       double nettAmount=0.00; 
	       double grossAmount=0.00;
	       double gstAmount=0.00;
	       double stampDuty=10.00; 
	       double capAmount=0.00;
	    		   
		
		 if(null !=txn.getGrossPremiumFinal() && ""!=txn.getGrossPremiumFinal()){
		 gross = Double.parseDouble(txn.getGrossPremiumFinal());
		 
		 }
		 else if (null !=txn.getPremiumAmount() && ""!=txn.getPremiumAmount()){
		 gross=  Double.parseDouble(txn.getPremiumAmount()); // ezylife
			 
		 }
		  
		 //System.out.println("***************************************");
		 //System.out.println("gross----"+gross);
		 
		 if(null !=txn.getDiscount() && "" !=txn.getDiscount()){
		 discount= Double.parseDouble(txn.getDiscount());

		//System.out.println("discount----"+discount);
		
		if(discount == 0.00){
			
			
			if(null !=txn.getTotalPremiumPayable() && "" !=txn.getTotalPremiumPayable()){
			
			gross=  Double.parseDouble(txn.getTotalPremiumPayable()); 
			}
			
				
			}
			
		
			
		}
		
		 
		 
		 else{
			 
			 if(null !=txn.getTotalPremiumPayable() && "" !=txn.getTotalPremiumPayable()){
			 
			gross= Double.parseDouble(txn.getTotalPremiumPayable());
			stampDuty=0.00;
					
			 }
		 }
		 
		 
		 if(txn.getProductCode().equals("WTC") || txn.getProductCode().equals("TPT") ){	
			 
			 if(null!=txn.getAreaCode()){
					 nettAmount=gross;
			 }
			 else{
				 
				 nettAmount= gross-discount;
			 }
		 }
		 else
		 
		 {	
		 nettAmount= gross-discount;
		 }
		 
		 //System.out.println("nettAmount----"+nettAmount);
		 
		 
		 if(null !=txn.getGST()&& ""!=txn.getGST() && txn.getGST().indexOf("-")==-1 ){
		 gstAmount=Double.parseDouble(txn.getGST());
		//System.out.println("gst----"+gstAmount);
		 }
		 
		 if(gross != 0.00){
			 
			 if(txn.getProductCode().equals("TL")){				 
				 stampDuty =0.00; 
			 }
			 
			 
			 if(null !=txn.getTotalPremiumPayable() && "" !=txn.getTotalPremiumPayable())
			 
			{
				 if(gross ==Double.parseDouble(txn.getTotalPremiumPayable())){
				 grossAmount=  gross;
				 }
				 else{
					 grossAmount=  nettAmount+gstAmount+stampDuty;	 
					 
				 }
			}else
			{
			
				 //System.out.println("grossAmount before "+nettAmount);
		     grossAmount=  nettAmount+gstAmount+stampDuty;
		     //System.out.println("stampDuty "+stampDuty);
		     
            }
		     //System.out.println("grossAmount----"+grossAmount);
		 }
		 
		 if(null !=txn.getCapsAmount() && "" !=txn.getCapsAmount()){

				 capAmount= Double.parseDouble(txn.getCapsAmount()); 			 
				 grossAmount=grossAmount+capAmount;
				 
			
				  if(null !=txn.getTotalPremiumPayable() && "" !=txn.getTotalPremiumPayable()){
					  
					  if(gross==  Double.parseDouble(txn.getTotalPremiumPayable()) ){
						  grossAmount=gross;
						 
					 
				 }
				  }
				  
				 
			 
		 }
		 
		 //System.out.println("grossAmount----"+grossAmount);
		 
		 totalGrossAmount=totalGrossAmount+grossAmount;
		 totalNettAmount=totalNettAmount+nettAmount;
	      
     }
														            	
	 totalAmount = list.get(0).getTotalAmount();
	 
	          	
														            	
														            }
														           
																	
														          

                                                                } %>
																		<td colspan="2"><b>RM</b> <%=String.format("%.02f", totalGrossAmount) %></td>
																		<td colspan="2"><b>RM</b> <%
                          %> <% if(session.getAttribute("ProductType").equals("TL")){
	                                                                    	%>
																			- <%  } else{
	                                                                    %>
																			<%=String.format("%.02f", totalNettAmount) %> <% } %></td>
																		<td colspan="4"><b>RM</b> <%=totalAmount %></td>

																		<tr>
																			<th>No</th>
																			<th style="width: 40px;">Txn Date</th>
																			<th>Pol/Cert No</th>
																			<th>Customer</th>
																			<th>Product</th>
																			<th>Payment</th>
																			<th>Status</th>
																			<th>Amount(RM)</th>
																		</tr>


																	</thead>
																	<tbody>


																		<%
                                                                 
                                                                 //System.out.println(request.getAttribute("data"));
                                                                 if (request.getAttribute("data") != null) {
														            List<TransactionalReport> list = (List<TransactionalReport>) request.getAttribute("data");
														           
														           
														            if(list.size() > 0 ){
														            	   	
														            	totalRecord = list.get(0).getRecordCount();
														            }
														            
														            
														           // System.out.println(totalRecord);
														            if(totalRecord >0) {
														            	startRow= list.get(0).getId();
														            	endRow=(startRow)+recordPerPage;
														            	
														            	if (endRow > totalRecord ){
														            		
														            		endRow=totalRecord;//at last page
														            		
														            	}
														            	
														            	
														                for(TransactionalReport txn : list) {
														                	
														                	 textColor="";
																	            status="";
																	           	transactionStatus="";
																	            lastStep="";
																	            reason="";
																	           //lastStep=txn.getLastPage();
						                		rowLine++;
														                		
														                
														                	
														                	if(txn.getPolicyNo()!=null && txn.getPolicyNo()!="" && !txn.getStatus().equals("S") && !txn.getStatus().equals("SUCCESS"))
														                	{
														                		
														                		txtColor="red";
														                		
														                	}
														                	
														                	
														        %>
																		<tr>
																			<td align="center"><%=txn.getId()%></td>
																			<td>
																				<% if(txn.getTransactionDate()!=null && txn.getTransactionDate()!="" ) { %><%=txn.getTransactionDate()%>
																				<%}
																				%>
																			</td>
																			<td>
																				<% if(txn.getPolicyNo()!=null && txn.getPolicyNo()!="" && txn.getStatus().equals("S")) { %><a
																				color="<%=txtColor%>"
																				href="TransactionalReport?action=detail&id=<%=txn.getTransactionID()%>"><span
																					color="<%=txtColor%>"><%=txn.getPolicyNo()%>
																						<%if(txn.getTLCheckDigit()!=null){%><%=txn.getTLCheckDigit()%>
																						<%} %></span></a> <%} %> <% if(txn.getCAPSPolicyNo()!=null && txn.getCAPSPolicyNo()!="" ) { %><a
																				color="<%=txtColor%>"
																				href="TransactionalReport?action=detail&id=<%=txn.getTransactionID()%>"><span
																					color="<%=txtColor%>"><br /><%=txn.getCAPSPolicyNo()%></span></a>
																				<%} %> <br> <span> <% if(txn.getVehicleNo()!=null && txn.getVehicleNo()!="") { %>
																					<%=txn.getVehicleNo()%> <%} %>
																			</span>
																			<td><b>Name :</b> <% if(txn.getTransactionID()!=null && txn.getTransactionID()!="" ) { %>
																				<a
																				href="TransactionalReport?action=detail&id=<%=txn.getTransactionID()%>"><%=txn.getCustomerName()%></a>
																				<%} else { %><%=txn.getCustomerName()%> <% }%><br /> <b>ID
																					No : </b><%=txn.getCustomerNRIC()%><br><%=txn.getAgentName()%>
																				<%if(txn.getAgentCode()!=""){%>(<%=txn.getAgentCode()%>)<%} %>
																				<% if (txn.getProductCode().equals("TL"))
                                                                    
                                                                    {
                                                                    	 
                                                                    	 String coverage = txn.getCoverage();
                                                                    	 String term =txn.getTerm();
                                                                    	 
                                                                    	 if(coverage != null && coverage !=""){
                                                                    	 
                                                                    		double fmtCoverage = Double.parseDouble(coverage);
                                                                    	 coverage = formatter.format(fmtCoverage);

                                                                    	 //System.out.println(coverage);
                                                                    	 }
                                                                    	 
                                                                    	
                                                                    	
                                                                    	if(coverage != null && coverage !="" && term !=null && term!=""){%>
																				<br> <b>Term:</b> <%=txn.getTerm()%> Years <br>
																				<b>Coverage:</b> RM <%=coverage%> <% }
                                                                    }
                                                                     %>

																			</td>

																			<td><%=txn.getProductType()%> <br>
																				<% if(txn.getProductEntity()!=null) {%>(<%=txn.getProductEntity()%>)
																				<% } %> <%  if(txn.getProductCode().indexOf("H")!=-1){ 
																		    	   
																		      if(!txn.getHomeSumInsured().equals(null) && !txn.getHomeSumInsured().equals("0") && !txn.getHomeSumInsured().equals("")){   
																		    	  double homeSumInsured = Double.parseDouble(txn.getHomeSumInsured());                                               	
																		      %><br> <b>Building :</b> <br>RM<%=formatter.format(homeSumInsured)%>
																				<%} 
																		       if(!txn.getContentSumInsured().equals(null) && !txn.getContentSumInsured().equals("0")&& !txn.getHomeSumInsured().equals("") ){
																		       double contentSumInsured = Double.parseDouble(txn.getContentSumInsured());   
																		       %><br> <b>Content :</b> <br>RM<%=formatter.format(contentSumInsured)%>
																				<%} 
 
																		      if(txn.getAddBenRiotStrike().equals("1")){
																		    	 %> <br>+ RSMD <%  }
																		       if(txn.getAddBenExtendedTheft().equals("1")&& !txn.getHomeCoverage().equals("building") ){
																		    	 %> <br>+ Extended Theft Cover <% }
																		      
																		     
																		     
																		      } %> <%  if(txn.getProductType().indexOf("W")!=-1){ 
																		     %> <br> <b>Travel:</b> <%=txn.getTravelAreaType()%>
																				<% if(txn.getTravelAreaType().equals("International")){ %>(<%=txn.getOfferedPlanName()%>)
																				<%} %> <br> <b>Scheme: </b><%=txn.getTravellingWith()%>
																				<br> <b>Days:</b> <%=txn.getTravelDuration()%>
																				days <% 
																		     
																		      } %></td>

																			<td><b><%=txn.getPaymentChannel()%></b> <% if (txn.getPaymentChannel().equals("MPAY"))
                                                                    
                                                                    {
                                                                    	%>
																				<br> <b>Ref No:</b><%=txn.getMPAY_RefNo()%> <br>
																				<b>Auth Code:</b> <%=txn.getMPAY_AuthCode()%> <br>
																				<b>Mode:</b> <%=txn.getPremiumMode()%> <% 
                                                                    }
                                                                    
                                                                    else if(txn.getPaymentChannel().equals("EBPG") || txn.getPaymentChannel().equals("AMEX") )
                                                                   
                                                                    	
                                                                    {
                                                                    	%>
																				<br> <b>Ref No:</b><%=txn.getEBPG_RefNo()%> <br>
																				<b>Auth Code:</b> <%=txn.getEBPG_AuthCode()%> <br>

																				<b>Tax Inv:</b> <%=txn.getInvoiceNo() %> <% 	
                                                                    	
                                                                    }
 																	else if(txn.getPaymentChannel().equals("M2U"))
                                                                   
                                                                    	
                                                                    {
                                                                    	%>
																				<br> <b>Ref No:</b><%=txn.getM2U_RefNo()%> <br>
																				<b>App Code:</b> <%=txn.getM2U_AuthCode()%> <br>

																				<b>Tax Inv:</b> <%=txn.getInvoiceNo() %> <% 	
                                                                    	
                                                                    }
                                                                    
	                                                                else if(txn.getPaymentChannel().equals("FPX"))
                                                                   
                                                                    	
                                                                    {
                                                                    	%>
																				<br> <b>Ref No:</b><%=txn.getFPX_RefNo()%> <br>
																				<b>FPX Status:</b> <%
                                                                    	//System.out.println("txn.getFPX_AuthCode() "+txn.getFPX_AuthCode());
                                                                    	
                                                                    	if (txn.getFPX_AuthCode().equals("00"))
                                                            			{
                                                            				
                                                            				transactionStatus= txn.getFPX_AuthCode()+"-Successful";
                                                            					
                                                            			}
                                                            			else if (!txn.getFPX_AuthCode().equals("00") && !txn.getFPX_AuthCode().equals("") )
                                                            			{
                                                            				transactionStatus= txn.getFPX_AuthCode()+"-Unsuccessful";
                                                            				
                                                            			}
                                                            			else
                                                            			{
                                                            				
                                                            				transactionStatus="";
                                                            				
                                                            			}
                                                            			
                                                                    	
                                                                    	%>

																				<%=transactionStatus%> <br> <b>Tax Inv:</b> <%=txn.getInvoiceNo() %>
																				<% 	
                                                                    	
                                                                    }
                                                                    
                                                                    %></td>

																			<!-- start: temporary -->
																			<% if (txn.getStatus().equals("SUCCESS") || txn.getStatus().equals("S") ){
                                                                    	 
                                                                    	 status = "Successful";
                                                                    	 textColor="green";
                                                                    	 
                                                                    	 
                                                                     }
                                                                     
																			else if (txn.getStatus().equals("Incomplete") || txn.getStatus().equals("O") || txn.getStatus().equals(null))
                                                                    	 
                                                                     {
                                                                    	
                                                                    		 
                                                                    		 status="Incomplete";
                                                                    		 textColor="";
                                                                    
                                                                    		 lastStep=txn.getLastPage();
           	  
                                                                    	 
                                                                     }
                                                                    
																			else if (txn.getStatus().equals("Pending") || txn.getStatus().equals("AP"))
                                                                    	 
                                                                     {
                                                                    	
                                                                    		 
                                                                    		 status="Attempt Payment";
                                                                    		 textColor="";
                                                                    	 
                                                                    	  
                                                                    	 
                                                                     }
                                                                     
																			else if (txn.getStatus().equals("Failed") || txn.getStatus().equals("F"))
                                                                    	 
                                                                     {
                                                                    	
                                                                    		 
                                                                    		 status="Failed";
                                                                    		 textColor="red";
                                                                    	 
                                                                    	  
                                                                    	 
                                                                     }
																	
																			else if (txn.getStatus().equals("R"))
                                                                   	 
                                                                    {
                                                                   	   
																		if(txn.getReason() == null ||txn.getReason().trim() == "" ){
                                                                   		 
                                                                   		 reason="";
                                                                  
																		}
																		
																		else
																			
																		{
																		
																			reason=txn.getReason();
																			
																		}
																		
																		if(txn.getUWReason() != null &&  txn.getUWReason().trim() != ""){
																			
																			if(reason ==""){
	                                                                   		 
	                                                                   		 reason=txn.getUWReason();
	                                                                   		 
																			}
																			else
																			{
																				
																				//System.out.println(reason);
																				//System.out.println(txn.getUWReason());
																				if(!reason.trim().equals(txn.getUWReason().trim()) ){
																				 reason=reason+"("+txn.getUWReason()+")";	
																				}
																				
																				
																			}
	                                                                  
																			}
																			
																			
																		 status="Rejection : "+reason;
                                                                   		 textColor="red";
                                                                   		 
                                                                   	  
                                                                   	 
                                                                    }
                                                                    
																	
																			else if (txn.getStatus().equals("CANCELLED"))
                                                                    	 
                                                                     {
                                                                	   status="Cancelled";
                                                                    	 textColor="red";
                                                                    	 
                                                                    	 
                                                                     }
                                                                     
                                                                     
                                                                     %>
																			<!-- end: temporary -->

																			<td><font color=<%=textColor %>><%=status%>
																					<br><%=lastStep %> </font></td>


																			<%  if(txn.getProductType().indexOf("M")!=-1){ %>


																			<% 
																	//System.out.println("txn.getMotorAmount()"+txn.getMotorAmount());
																	
																	BigDecimal tmp= new BigDecimal(10.00);
																	
																	if(null !=txn.getMotorAmount() && txn.getMotorAmount().compareTo(tmp) !=0){
																		%>
																			<td align="left">Motor : <%=txn.getMotorAmount()%>
																				<br> <% if(txn.getProductType().indexOf("MI")!=-1) {%>Caps<% } else { %>
																				DPPA <% }
																			
																			%> : <%=txn.getCapsAmount()%><br> Total : <%
																					if(null !=txn.getPremiumAmount() && ""!=txn.getPremiumAmount())
																					{
	
																						 DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(Locale.US);
																			    		 format.setParseBigDecimal(true);
																			    		 BigDecimal totalMAmount=(BigDecimal)format.parse(txn.getPremiumAmount());
																			    		 
																			    		 
																						%><%=totalMAmount.setScale(2, RoundingMode.CEILING)%>
																				<%
																					}
																		
																					else{%> <%=txn.getPremiumAmount()%> <% }
																					
																					%>

																			</td>

																			<% 
																	}else{
																		%>
																			<td align="center"><%=txn.getPremiumAmount()%> <% if (txn.getPremiumAmount().equals("")){%>
																				0 <% }
																		
																		
																		%></td>

																			<% }
																	 	
																		} else{ %>

																			<td align="center"><%=txn.getPremiumAmount()%></td>



																			<%} %>
																		</tr>
																		<%
                                                                 
														                }
														                status="";
																           textColor="";
																           
														            }
																	else
																	{
																	%>
																		<tr>
																			<td colspan="8"><center>No Record
																					Registered</center></td>
																		</tr>
																		<% 	
																	
																	
														            }
                                                                 }
														            
														            else{
														            	
														            	%>
																		<tr>
																			<td colspan="8"><center>No Record
																					Registered</center></td>
																		</tr>
																		<% 	
														            }
														           
														        %>
																	</tbody>
																</table>
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>
											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%-- <jsp:include page="siteFooter.jsp" /> --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->
	<script
		src="${pageContext.request.contextPath}/assets/js/jquery.min.bk.js"
		type="text/javascript"></script>
	<!-- <script src="${pageContext.request.contextPath}/assets/js/jquery.min.js" type="text/javascript"></script>  -->
	<!--  <script src="assets/jAlert/jquery-1.11.3.min.js"></script> -->
	<script
		src="${pageContext.request.contextPath}/assets/jAlert/jAlert.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/jAlert/jAlert-functions.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/bootstrap.js"
		type="text/javascript"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/retina/retina.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/backstretch/jquery.backstretch.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/apps.js"></script>

	<!-- PLUGINS -->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/prettify/prettify.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<!-- <script src="assets/plugins/chosen/chosen.jquery.min.js"></script> -->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/icheck/icheck.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/mask/jquery.mask.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/summernote/summernote.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/to-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/slider/bootstrap-slider.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/toastr/toastr.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/knob.js"></script>

	<script
		src="${pageContext.request.contextPath}/assets/js/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/dataTables.bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/dataTables.buttons.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/jszip.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/pdfmake.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/vfs_fonts.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.html5.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.print.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/buttons.colVis.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/toastr.min.js"></script>


	<script type="text/javascript">
         	$(document).ready(function() {
         		
         		 $("#date_pretty").datepicker({ 
         	    });
         	    var myDate = new Date();
         	    var month = myDate.getMonth() + 1;
         	    var prettyDate =  month + '/' + myDate.getDate()  + '/' + myDate.getFullYear();
         	    $("#date_pretty").val(prettyDate);
         		
         		        		
         		$('#eib-pr').hide();
                $('#eib-list').hide();
				$('#etb-list').hide();
				$('#all-list').hide();
			
				
				var productTypeSelected = $('#productTypeValSession').val();
				var productEntitySelected = $('#productEntityValSession').val();
				
				if(productTypeSelected =="MI" || productTypeSelected =="MT")
					{
					
					$('#mi-option').show();	
					
					}
				
				else{
					
					$('#mi-option').hide();	
					
				}
				
				if(productEntitySelected =="EIB")
				{
					$('#eib-pr').show();
					$('#eib-list').show();
    				$('#etb-list').hide();
    				$('#all-list').hide();
    				
    				
				
				}
				
				if(productEntitySelected =="ETB")
				{
					$('#eib-pr').show();
					$('#eib-list').hide();
    				$('#etb-list').show();
    				$('#all-list').hide();
    				
				
				}
			
				if(productEntitySelected =="")
				{
					
					$('#eib-list').hide();
    				$('#etb-list').hide();
    				$('#all-list').hide();
    				
				
				}
			
			
		
				
				 $("#productEntity").on('change', function() {	
	        			alert(this.value);
	        			if (this.value == 'ETB')
	        			{
	        				$('#eib-pr').show();
	        				$('#eib-list').hide();
	        				$('#etb-list').show();
	        				$('#all-list').hide();
	        				$('#mi-option').hide();	
	        				plateNo.value = null;
	        				productType.value=null;
	        				
	        				//$('#mi-option').css("display","none");
	        				
	        			} 
	        			
	        			else if (this.value == 'EIB')
	        				{
	        				$('#eib-pr').show();
	        				$('#eib-list').show();
	        				$('#etb-list').hide();
	        				$('#all-list').hide();
	        				$('#mi-option').hide();	
	        				plateNo.value = null;
	        				productType.value=null;
	        				//$('#mi-option').css("display","none");
	        				
	        				}
	        			
	        			else {
	        				$('#eib-pr').hide();
	        				$('#eib-list').hide();
	        				$('#etb-list').hide();
	        				$('#all-list').hide();
	        				$('#mi-option').hide();	
	        				plateNo.value = null;
	        				productType.value=null;
	        				
	        				//$('#mi-option').css("display","none");
	        				
	        			}
	        		});
				 
				 
				 $('#productTypeEIB').on('change', function() {	
		         		//alert("ETB = "+this.value);
		         		productType.value = this.value;
		         		
		         		if(this.value == "MI"){
		         			
		         			$('#mi-option').show();	
		         			
		         			
		         		}
		         		else{
		         			
		         			$('#mi-option').hide();
		         			plateNo.value = null;
		         			
		         		}
		         			
		         		});	
		         	
		         	$('#productTypeETB').on('change', function() {	
		         		//alert("EIB = "+this.value);
		         		productType.value = this.value;
                       
		         			
if(this.value == "MT"){
		         			
		         			$('#mi-option').show();	
		         			
		         			
		         		}
		         		else{
		         			
		         			$('#mi-option').hide();
		         			plateNo.value = null;
		         		}
		         			
		         		});	
		         	
		        	$('#productTypeALL').on('change', function() {	
		         		//alert("ALL ="+this.value);
		         		productType.value = this.value;
		         			
							if(this.value == "MI" ){
									         			
							$('#mi-option').show();	
									         			
							}
							else{ 
									
									$('#mi-option').hide();
									plateNo.value = null;
								}
									         			
		         		});	
		         	
         		
		        	 var t = $('#admin-datatable-second_admin').DataTable( {
	         			 
		         			dom: 'Bfrtip',
		         			  buttons: [	
		{
		    extend: 'pageLength',
		    className: 'btn btn-secondary btn-sm active'
		},
		            {
		                extend: 'print',
		                className: 'btn btn-warning btn-sm',
		                title : 'Etiqa Transaction Report',
		                text: 'Print <i class="fa fa-print" aria-hidden="true"></i>'
		            }
		//,
		            //{
		               // extend: 'excel',
		                //filename: 'Etiqa Transaction Report',
		               // className: 'btn btn-warning btn-sm',
		                //text:'Export to XLS <i class="fa fa-download" aria-hidden="true"></i>'
		           // }
		         			  ],
		         		      "searching" :false, 
		         	          "columnDefs": [ {
		         	          "searchable": false,
		         	          "orderable": false,
		         	          "targets": [0,1,2,3,4,5,6,7],
		         	          "bSortable": false
		         	         
		         	        } ],
		         	        "order": [[ 0, 'asc' ]]
		         	    } );
		         	 
		         	    t.on( 'order.dt search.dt', function () {
		         	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		         	            cell.innerHTML = i+1;
		         	        } );
		         	    } ).draw();
		         	           	
		         	  
		         	   t.buttons( 0, null ).containers().appendTo( '#btnhere' );
		         	   
		         	});
		         
         
         	
    
        function validateForm() {
        	   var x = document.forms["txnreport"]["dateFrom"].value;
        	   var y = document.forms["txnreport"]["dateTo"].value;
        	   var z=  document.forms["txnreport"]["status"].value;
        	   var w=  document.forms["txnreport"]["productType"].value;        	   
        	   
        	   if ( x == null || x == "" ) {
        		   if(y !=""){
        	      alert("Please choose Date From");
        	      document.forms["txnreport"]["dateFrom"].focus();
        	      
        	      return false;
        	   } 
        		  
        	   }
        	   
        	   if(z=="R"|| z =="O"){
        		   
        		   if(w =="")
        			   {
        			   
        			   alert ("Please choose Product Type");
        			   document.forms["txnreport"]["productType"].focus();
        			   return false;	   
        			   }      		   
        		   
        	   }
        	   return true;
        	  
        	}
        
        
        </script>




</body>
</html>