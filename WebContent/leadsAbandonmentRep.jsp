<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Etiqa</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/js/jquery.dataTables.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/buttons.dataTables.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/dataTables.bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/buttons.bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link rel="stylesheet" href="assets/css/toastr.min.css">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}
</style>
<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}
</style>

<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet"
	href="assets/plugins/datepicker/datepicker.min.css">
<!-- PLUGINS CSS -->
<link href="assets/plugins/owl-carousel/owl.carousel.min.css"
	rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.theme.min.css"
	rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.transitions.min.css"
	rel="stylesheet">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
.bbdot {
	border-bottom: 1px dotted #333;
	padding-bottom: 10px;
	margin-bottom: 10px;
}

.text-right {
	text-align: right;
}

.btn-primary {
	background-color: #ffcf43;
	border-color: #ffcf43;
	color: #333;
}

.btn-primary:hover, .btn-primary:active {
	background-color: #ffcf43;
	border-color: #ffcf43;
	color: #fff;
}

table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

form#agentE, form#agentD {
	display: inline-block;
}

form#agentE input[type=submit], form#agentD input[type=submit] {
	padding: 5px;
}

#abandonmentTable tr th, #abandonmentTable tr td {
	text-align: center;
}
</style>
</head>
<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!-- header -->
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>

		<!-- end header -->
		<!-- header second-->

		<!-- header second-->
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>


					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Leads Management &amp;
														Campaign</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Abandonment Repository</h4>
															<br>(Admin are able to view a drop case (by each
															step) from DSP. Prospect record are based on the follow
															up email sent to the customer and lost record are based
															on the DSP)
														</div>
													</div>
													<div class="content-inner">
														<div class="the-box">
															<form:form name="abandonmentreport"
																id="abandonmentreport" action="searchAbandonment"
																onsubmit="return validateForm()" method="post">
																<input type="hidden" id="productType" name="productType"
																	value="<c:out value="${sessionScope.ProductType}" />" />
																<input type="hidden" id="lblLeadsAbdEntity"
																	name="lblLeadsAbdEntity"
																	value="<c:out value="${sessionScope.LeadsAbdEntity}" />" />


																<div class="col-sm-6 space-top">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Entity
																				: </label>
																			<div class="col-sm-9">
																				<select class="form-control" name="leadsAbdEntity"
																					id="leadsAbdEntity">
																					<c:choose>
																						<c:when
																							test="${sessionScope.LeadsAbdEntity == 'EIB'}">
																							<option value="">Select Entity</option>
																							<option
																								<c:if test="${sessionScope.LeadsAbdEntity == 'EIB'}" >selected</c:if>
																								value="EIB">Insurance</option>
																							<option value="EIT">Takaful</option>
																						</c:when>
																						<c:when
																							test="${sessionScope.LeadsAbdEntity == 'EIT'}">
																							<option value="">Select Entity</option>
																							<option value="EIB">Insurance</option>
																							<option
																								<c:if test="${sessionScope.LeadsAbdEntity == 'EIT'}" >selected</c:if>
																								value="EIT">Takaful</option>
																						</c:when>
																						<c:otherwise>
																							<option value="" selected>Select Entity</option>
																							<option value="EIB">Insurance</option>
																							<option value="EIT">Takaful</option>
																						</c:otherwise>
																					</c:choose>

																				</select>
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Product
																				: </label>
																			<div class="col-sm-9">
																				<select class="form-control" name="leadsAbdProduct"
																					id="leadsAbdProduct">
																					<c:choose>
																						<c:when
																							test="${sessionScope.LeadsAbdEntity == 'EIB'}">
																							<option value="">Select Product</option>
																							<option
																								<c:if test="${sessionScope.ProductType=='TL'}" >selected</c:if>
																								value="TL">Ezy-Life Secure</option>
																							<option
																								<c:if test="${sessionScope.ProductType=='HOHH'}" >selected</c:if>
																								value="HOHH">Houseowner / Householder</option>
																							<option
																								<c:if test="${sessionScope.ProductType=='MI'}" >selected</c:if>
																								value="MI">Motor</option>
																							<option
																								<c:if test="${sessionScope.ProductType=='WTC'}" >selected</c:if>
																								value="WTC">World Traveller Care</option>
																						</c:when>
																						<c:when
																							test="${sessionScope.LeadsAbdEntity == 'EIT'}">
																							<option value="">Select Product</option>
																							<option
																								<c:if test="${sessionScope.ProductType == 'TPT'}" >selected</c:if>
																								value="TPT">World Traveller Care
																								Takaful</option>
																							<option
																								<c:if test="${sessionScope.ProductType == 'HOHH-ETB'}" >selected</c:if>
																								value="HOHH-ETB">Houseowner/Householder
																								Takaful</option>
																							<option
																								<c:if test="${sessionScope.ProductType == 'MT'}" >selected</c:if>
																								value="MT">Motor Takaful</option>
																						</c:when>
																						<c:otherwise>
																							<option value="" selected>Select Product</option>
																						</c:otherwise>
																					</c:choose>
																				</select>
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Agent
																				Name : </label>
																			<div class="col-sm-9">
																				<input type="text" placeholder="Agent Name"
																					name="leadsAbdAgent"
																					value='${sessionScope.leadsAbdAgent}'
																					id="leadsAbdAgent" class="form-control">
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Agent
																				Code : </label>
																			<div class="col-sm-9">
																				<input type="text" placeholder="Agent Code"
																					value='${sessionScope.leadsAbdAgentCode}'
																					name="leadsAbdAgentCode" id="leadsAbdAgentCode"
																					class="form-control">
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Status
																				: </label>
																			<div class="col-sm-9">
																				<select class="form-control" name="leadsAbdStatus"
																					id="leadsAbdStatus">
																					<option
																						<c:if test="${sessionScope.leadsAbdStatus == ' '}" >selected</c:if>
																						value="">Select Status</option>
																					<option
																						<c:if test="${sessionScope.leadsAbdStatus == 'C'}" >selected</c:if>
																						value="C">Close</option>
																					<option
																						<c:if test="${sessionScope.leadsAbdStatus == 'O'}" >selected</c:if>
																						value="O">Open</option>
																				</select>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Step :
																			</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="leadsAbdStep"
																					id="leadsAbdStep">
																					<option
																						<c:if test="${sessionScope.leadsAbdStep == ' '}" >selected</c:if>
																						value="">Select Step</option>
																					<option
																						<c:if test="${sessionScope.leadsAbdStep == '1'}" >selected</c:if>
																						value="1">Step 1</option>
																					<option
																						<c:if test="${sessionScope.leadsAbdStep == '2'}" >selected</c:if>
																						value="2">Step 2</option>
																					<option
																						<c:if test="${sessionScope.leadsAbdStep == '3'}" >selected</c:if>
																						value="3">Step 3</option>
																				</select>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Date :
																			</label>
																			<div class="col-sm-4">
																				<input type="text" placeholder="Date From"
																					value='${sessionScope.leadsAbdFromDate}'
																					class="form-control" name="leadsAbdFromDate"
																					id="leadsAbdFromDate"> <span
																					id="msg_fromdate" class="hidden"
																					style="color: red; text-align: left">Please
																					select to proceed</span>
																			</div>
																			<div class="col-sm-5">
																				<input type="text" placeholder="Date To"
																					value='${sessionScope.leadsAbdToDate}'
																					class="form-control" name="leadsAbdToDate"
																					id="leadsAbdToDate"> <span id="msg_todate"
																					class="hidden" style="color: red; text-align: left">Please
																					select to proceed</span>
																			</div>
																		</div>

																		<div class="form-group text-center">
																			<button type="submit" class="btn btn-primary">Submit</button>
																			<!--   <a href="searchAbandonment" class="btn btn-primary" onClick="submitLeadsAbd();">Submit</a> -->
																		</div>
																	</div>
																</div>
															</form:form>
															<div class="col-sm-6">

																<div class="row">
																	<div id="chartContainer" style="height: 270px;"></div>

																	<div class="col-sm-12">
																		<table class="table table-bordered">
																			<thead>
																				<tr class="active">
																					<th>Stage</th>
																					<th>Prospect</th>
																					<th>Lost</th>
																				</tr>
																			</thead>
																			<tbody>
																				<c:forEach items="${leadsAbdReportList}"
																					var="element1" varStatus="status1">
																					<c:if test="${status1.last}">
																						<tr>
																							<td>Step 1</td>
																							<td><c:out value="${element1.step1_total}" /></td>
																							<td><c:out value="${element1.step1_lost}" /></td>
																						</tr>
																						<tr class="warning bold">
																							<td>Step 2</td>
																							<td><c:out value="${element1.step2_total}" /></td>
																							<td><c:out value="${element1.step2_lost}" /></td>
																						</tr>
																						<tr>
																							<td>Step 3</td>
																							<td><c:out value="${element1.step3_total}" /></td>
																							<td><c:out value="${element1.step3_lost}" /></td>
																						</tr>
																					</c:if>
																				</c:forEach>
																			</tbody>
																		</table>
																	</div>

																	<div class="col-sm-12">

																		<div
																			class="panel panel-success panel-square panel-no-border">
																			<div class="panel-body">
																				<c:forEach items="${leadsAbdReportList}"
																					var="element2" varStatus="status2">
																					<p class="text-center">
																						<c:if test="${status2.last}">
																							<b><c:out value="${element2.totalSuccess}" />
																								Successful Transactions</b>
																						</c:if>
																					</p>
																				</c:forEach>
																			</div>
																			<!-- /.panel-body -->
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="row">
																<div class="col-xs-12 form-inline">
																	<div class="col-xs-6">
																		<div class="row">
																			<!--      <label>
                                                                            <select class="form-control">
                                                                              <option>5</option>
                                                                              <option>10</option>
                                                                              <option>25</option>
                                                                              <option>50</option>
                                                                              <option>100</option>
                                                                            </select>
                                                                            records per page
                                                                        </label> -->
																		</div>
																	</div>
																	<!--      <div class="col-xs-6">
                                                                        <div class="row">
                                                                            <div class="pull-right">
                                                                                <button class="btn btn-warning btn-sm">Print <i class="fa fa-print" aria-hidden="true"></i></button>
                                                                                <button class="btn btn-warning btn-sm">Export to XLS <i class="fa fa-download" aria-hidden="true"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div> -->
																</div>
															</div>
															<div class="table-responsive">
																<table
																	class="table table-striped table-warning table-hover"
																	id='abandonmentTable'>
																	<thead>
																		<tr>
																			<th><p align="left">No</p></th>
																			<th><p align="left">Date</p></th>
																			<th><p align="left">Product</p></th>
																			<th><p align="left">Name</p></th>
																			<th><p align="left">Details</p></th>
																			<th><p align="left">Step</p></th>
																			<th><p align="left">Status</p></th>
																	</thead>
																	<tbody>
																		<c:forEach items="${leadsAbdReportList}" var="element"
																			varStatus="theCount">
																			<tr>
																				<td><c:out value="${theCount.count}" /></td>

																				<%
																					try {
																				%>
																				<fmt:parseDate value="${element.CREATE_DATE}"
																					pattern="yyyy-MM-dd HH:mm:ss"
																					var="convertCreateDate" />
																				<td><fmt:formatDate
																						value="${convertCreateDate}" pattern="dd-MMM-yyyy" /></td>
																				<%
																					} catch (Exception e) {
																				%>
																				<td><p align="left">
																						<c:out value="${element.CREATE_DATE}" />
																					</p></td>
																				<%
																					}
																				%>
																				<td><c:out value="${element.PRODUCT_CODE}" /></td>
																				<td>
																					<p align="left">
																						<c:out value="${element.FULL_NAME}" />
																					</p>
																				</td>
																				<td><p align="left">
																						<b>Email : </b>
																						<c:out value="${element.LEADS_EMAIL_ID}" />
																						<br />
																						<c:if
																							test="${not empty element.CUSTOMER_MOBILE_NO}">
																							<b>Mobile No : </b>
																							<c:out value="${element.CUSTOMER_MOBILE_NO}" />
																						</c:if>
																					</p></td>


																				<td><p align="left">
																						<c:if test="${element.QQ_STEPS == '0'}">
																							<c:out value="Step 1" />
																						</c:if>
																						<c:if test="${element.QQ_STEPS == '1'}">
																							<c:out value="Step 1" />
																						</c:if>
																						<c:if test="${element.QQ_STEPS == '2'}">
																							<c:out value="Step 2" />
																						</c:if>
																						<c:if test="${element.QQ_STEPS == '3'}">
																							<c:out value="Step 3" />
																						</c:if>
																						<c:if test="${element.QQ_STEPS == '4'}">
																							<c:out value="Step 4" />
																						</c:if>
																					</p></td>
																				<td><p align="left">
																						<c:if test="${element.QQ_STATUS == 'O'}">
																							<c:out value="Open" />
																						</c:if>
																						<c:if test="${element.QQ_STATUS == 'C'}">
																							<c:out value="Cancel" />
																						</c:if>
																						<c:if test="${element.QQ_STATUS == 'R'}">
																							<c:out value="Reject" />
																						</c:if>
																					</p></td>
																			</tr>
																		</c:forEach>
																	</tbody>
																</table>
																</p>
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>
												<!--                           <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="col-sm-6">
                                                        Showing 1 to 5 of 15 entries
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <ul class="pagination pull-right">
                                                          <li class="disabled"><a href="#">&laquo;</a></li>
                                                          <li class="active"><a href="#">1</a></li>
                                                          <li><a href="#">2</a></li>
                                                          <li><a href="#">3</a></li>
                                                          <li><a href="#">4</a></li>
                                                          <li><a href="#">5</a></li>
                                                          <li><a href="#">&raquo;</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                </div> -->

											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->
		<!-- BEGIN FOOTER -->

		<!-- END FOOTER -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->

	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->


	<!--   <!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
	<!--  <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-select.min.js"></script>
        <script src="js/bootstrap-tabcollapse.js"></script>

        <script src="plugins/retina/retina.min.js"></script>
        <script src="plugins/nicescroll/jquery.nicescroll.js"></script>
        <script src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
        <script src="plugins/backstretch/jquery.backstretch.min.js"></script>

 
        PLUGINS
        <script src="plugins/owl-carousel/owl.carousel.min.js"></script>
        <script src="plugins/prettify/prettify.js"></script>
        <script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
        
        <script src="plugins/chosen/chosen.jquery.min.js"></script>
        <script src="plugins/icheck/icheck.min.js"></script>
        <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
        <script src="plugins/timepicker/bootstrap-timepicker.js"></script>
        <script src="plugins/mask/jquery.mask.min.js"></script>
        <script src="plugins/validator/bootstrapValidator.min.js"></script>
        <script src="plugins/datatable/js/jquery.dataTables.min.js"></script>
        <script src="plugins/datatable/js/bootstrap.datatable.js"></script>
        <script src="plugins/datatable/js/jquery.highlight.js"></script>
        <script src="plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
        <script src="plugins/summernote/summernote.min.js"></script>
        <script src="plugins/markdown/markdown.js"></script>
        <script src="plugins/markdown/to-markdown.js"></script>
        <script src="plugins/markdown/bootstrap-markdown.js"></script>
        <script src="plugins/slider/bootstrap-slider.js"></script>
        <script src="plugins/toastr/toastr.js"></script>
        <script src="plugins/newsticker/jquery.newsTicker.min.js"></script>
        <script src="plugins/placeholder/jquery.placeholder.js"></script>
        <script src="plugins/footable-3/js/footable.js"></script>
        
        
        highcharts JS
		<script src="plugins/highcharts/highcharts.js"></script>
		<script src="plugins/highcharts/modules/funnel.js"></script>
        KNOB JS
        [if IE]>
        <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
        <![endif]
        <script src="plugins/jquery-knob/jquery.knob.js"></script>
        <script src="plugins/jquery-knob/knob.js"></script> -->
	-->
	<!-- MAIN APPS JS -->
	<!--  <script src="js/apps.js"></script> -->
	<!--  <script src="js/chart-funnel.js"></script> -->

	<script
		src="${pageContext.request.contextPath}/assets/js/canvasjs.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/jquery.canvasjs.min.js"></script>
	<script type="text/javascript">
		function submitLeadsAbd() {
			//result=true;      leadsAbdFromDate leadsAbdToDate
			if (!$('#leadsAbdFromDate').val().length) {
				$('#msg_fromdate').removeClass('hidden');
				$('#msg_fromdate').addClass('visible');
				/* $('#referenceNo').css("border-color", "red"); */
				$('#leadsAbdFromDate').focus();
				result = false;
			}
			if (!$('#leadsAbdToDate').val().length) {
				$('#msg_todate').removeClass('hidden');
				$('#msg_todate').addClass('visible');
				$('#leadsAbdToDate').focus();
				result = false;
			}

			if (result == true) {
				$('#leadsreport').submit();
			}

		}
	</script>
	<script type="text/javascript">
		$('#leadsAbdEntity')
				.on(
						'change',
						function() {
							console.log($('#leadsAbdEntity').val());
							$('#leadsAbdProduct').html('');
							if ($('#leadsAbdEntity').val() == 'EIB') {
								$('#leadsAbdProduct')
										.append(
												'<option value="TL">Ezy-Life Secure</option>');
								$('#leadsAbdProduct').append(
										'<option value="MI">Motor</option>');
								$('#leadsAbdProduct')
										.append(
												'<option value="WTC">World Traveller Care</option>');
								$('#leadsAbdProduct')
										.append(
												'<option value="HOHH">Houseowner / Householder</option>');
							} 
							else if ($('#leadsAbdEntity').val() == '') {
								$('#leadsAbdProduct')
										.append(
												'<option value="">Select Product</option>');
								
							} 
							else {
								$('#leadsAbdProduct')
										.append(
												'<option value="TPT">World Traveller Care Takaful</option>');
								$('#leadsAbdProduct')
										.append(
												'<option value="HOHH-ETB">Houseowner/Householder Takaful</option>');
								$('#leadsAbdProduct')
										.append(
												'<option value="MT">Motor Takaful</option>');

							}
						});
		$(document).ready(function() {
			console.log('load function');
			$('.button').click(function() {
				// if ($('#AccountText').val() == ""){
				alert('Please complete the field');
				// }
			});
			$('#abandonmentTable').DataTable({
				"order" : [],
				dom : 'Bfrtip',
				title : ' Agent List',

				buttons : [ 'copy', 'csv', 'print', {
					extend : 'excelHtml5',
					title : ' Abandonemnt Repository '
				}, {
					extend : 'pdfHtml5',
					title : '  Abandonemnt Repository '
				} ]
			});

			// $('#date1').datepicker();
			$('#leadsAbdFromDate').datepicker({
				format : 'dd/mm/yyyy',
			}).on('changeDate', function(e) {
				$(this).datepicker('hide');

			});
			$('#leadsAbdToDate').datepicker({
				format : 'dd/mm/yyyy',
			}).on('changeDate', function(e) {
				$(this).datepicker('hide');
			});

		});
	</script>
	<script>
		function validateForm() {
			var x = document.forms["rptForm"]["leadsAbdFromDate"].value;
			var y = document.forms["rptForm"]["leadsAbdToDate"].value;

			if (x == null || x == "") {

				alert("Please choose Date From");
				document.forms["rptForm"]["leadsAbdFromDate"].focus();

				return false;
			} else if (y == null || y == "") {

				alert("Please choose Date To");
				document.forms["rptForm"]["leadsAbdToDate"].focus();
				return false;
			}

			return true;

		}
	</script>

	<script type="text/javascript">
		window.onload = function() {
			CanvasJS.addColorSet("OrangeShades",
                [//colorSet Array
                "red",
                  "Orange",
                  "yellow",
                ]);
			
			var chart = new CanvasJS.Chart("chartContainer", {
				colorSet: "OrangeShades",
				title : {
					text : "Performance"
					
				},
				data : [ {
					type : "bar",
					indexLabel: "{y}%",
					dataPoints : [ {
						y : ${sessionScope.step_3_percentage},
						label : "STEP 3",
						
						
					}, {
						y : ${sessionScope.step_2_percentage},
						label : "STEP 2"
					}, {
						y : ${sessionScope.step_1_percentage},
						label : "STEP 1"
					}, ]
				} ]
			});
			chart.render();

		}
	</script>
</body>
</html>