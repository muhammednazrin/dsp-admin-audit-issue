<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<% java.text.DateFormat df=new java.text.SimpleDateFormat("dd/MM/yyyy"); %>


<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="<c:url value="/resources/assets/jAlert/jAlert.css"/>"
	rel="stylesheet">
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>

</head>

<body>


	<!--
	
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1__.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">System Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>New Menu Creation
														</div>
													</div>


													<!-- Start Form -->
													<form name="menuForm"
														action="${pageContext.request.contextPath}/uam/createMenuAction"
														id="menuForm" method="post">

														<input type="hidden" name="action" value="search" />

														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Menu
																				Name</label>
																			<div class="col-sm-9">
																				<input type="text" name="name" maxlength="50"
																					placeholder="" class="form-control">
																			</div>
																		</div>

																	</div>
																</div>

																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Module</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="module">
																					<option value="" selected>-View All-</option>
																					<c:forEach items="${moduleList}" var="element">
																						<option value="<c:out value="${element.id}"/>">
																							<c:out value="${element.name}" />
																						</option>
																					</c:forEach>
																				</select>
																			</div>
																		</div>

																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Parent
																				Id</label>
																			<div class="col-sm-9">
																				<input type="text" name="parentId" maxlength="50"
																					placeholder="" class="form-control">
																				<%-- 
                                                                             <select class="form-control" name="parentId">
                                                                             <option value="" selected>-View All-</option>
                                                                              <c:forEach items="${UserRoleSearch}" var="element"> 
						                                                                  <option						                                                                  
																									value="<c:out value="${element.id}"/>">
																									<c:out value="${element.name}" />
																							</option>																																				
																						</c:forEach>    --%>
																				</select>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Level</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="level">
																					<option value="" selected>-View All-</option>
																					<option value="1">1</option>
																					<option value="2">2</option>
																					<option value="3">3</option>
																					<option value="4">4</option>

																				</select>
																			</div>

																		</div>
																	</div>
																</div>

																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">URL</label>
																			<div class="col-sm-9">
																				<input type="text" name="url" maxlength="50"
																					placeholder="" class="form-control">
																			</div>
																		</div>

																	</div>
																</div>
																<!-- Button -->
																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">

																			<div class="col-sm-9">
																				<br> <br> <br>

																				<!-- Button -->
																				<div class="controls">
																					<input type="submit" class="btn btn-warning"
																						id="createRoleNameBtn">&nbsp; <input
																						type="reset" class="btn btn-danger"
																						data-dismiss="modal" aria-label="Close">
																					</button>
																				</div>
																			</div>
																		</div>

																	</div>
																</div>
															</div>
														</div>
													</form>
													<!-- END FORM -->

												</div>

												<%-- 												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="">
															<div class="row">
																<div class="col-xs-12 form-inline">
																							</div>
																</div>
															</div>
															
															
															<div class="table-responsive" id="MyTable">
																<table
																	class="table table-striped table-warning table-hover"
																	id="admin-datatable-second_admin">
																	<thead>
																		
																		
																		<tr>
																			<th>No</th>
																			<th>Menu</th>
																			<th>Module</th>
																			<th>Status</th>
																			<th>Created By</th>
																			<th>Created Date</th>
																			<th>Updated By</th>
																			<th>Updated Date</th>
																			<th>Action</th> 
																		</tr> 
																	</thead>
																	<tbody>

 <c:forEach items="${UserList}" var="element"  varStatus="loop">
						  <tr>
						   <td align="center"><c:out value="${element.id}" /></td>
						   <td align="left">${element.name}</td>
						   <td align="left">${element.moduleId}</td>
						   <td align="center"><c:if test="${element.status eq 1}" > <span class="label label-success">ACTIVE</span></c:if><c:if test="${element.status eq 0}" > <span class="label label-danger">INACTIVE</span></c:if></td>				  
						   <td align="center">${element.createBy}</td>
						   <td align="center">${element.createDate}</td>
						   <td align="center">${element.updateBy}</td>
						   <td align="center">${element.updateDate}</td>
						   <td align="center">
						   <a href="#" data-toggle="modal"   data-backdrop="static" data-keyboard="false" data-target="#updateUser" data-value="<c:out value="${element.id}" />">
						  <button class="btn btn-warning">
																					Edit <i class="fa fa-edit"
																						aria-hidden="true"></i>
																				</button> 	</a></td>
						  </tr>
						</c:forEach>
						
																		
																	</tbody>
																</table>
															</div>
															
															
															
															
															
															
															
															
															
															
															
															
															
															
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div> --%>
												<!-- End warning color table -->
											</div>
										</div>
									</div>
									<!--row -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->

	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>


	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->



	<script type="text/javascript">
	

         	$(document).ready(function() {
         		
         		
        		
         	    var savemessage="<c:out value="${savemessage}"/>";
	 		       // show when the button is clicked
	 		       if (savemessage.length) {
	 		    	// alert(updatemessage);
	 		    successAlert('Success!', savemessage);
	 		       }  
         		
         		
         		$('#messagePF').hide();
          		$('#messageName').hide();
          		$('#messageManagerPF').hide();
          		$('#messageemail').hide();
          		$('#messageRole').hide();
          		$('#recordFound').hide();
         		$('#pfNumber').keyup(function() {
             	    if($(this).val() == ''){
             	        $('#messagePF').show();		             	     
             	      
             	    }else{
             	        $('#messagePF').hide();
             	        
             	    }
             	});
         	   
         		$('#name').keyup(function() {
             	    if($(this).val() == ''){
             	        $('#messageName').show();
             	    }else{
             	        $('#messageName').hide();
             	    }
             	});
         		$('#managerPF').keyup(function() {
             	    if($(this).val() == ''){
             	        $('#messageManagerPF').show();
             	    }else{
             	        $('#messageManagerPF').hide();
             	    }
             	});
         		$('#email').keyup(function() {
             	    if($(this).val() == ''){
             	        $('#messageemail').show();
             	    }else{
             	        $('#messageemail').hide();
             	    }
             	});
         		
         		
         		$('#groupID').change(function() {
         			if($(this).val() == ''){
         			    $('#messageRole').show();
         			}else{
         			    $('#messageRole').hide();
         			}
         		});
         		
         		
         		$('#status').on('change', function() {
         			alert('test');
         			if($(this).val() == ''){
         			    $('#messageStatusUpdate').show();
         			}else{
         			    $('#messageStatusUpdate').hide();
         			}

               	});

         		//$('#recordFound').hide();
		         	
         		
		        	 var t = $('#admin-datatable-second_admin').DataTable( {
	         			 
		         			dom: 'Bfrtip',
		         			  buttons: [	
		         			          
		{
		    extend: 'pageLength',
		    className: 'btn btn-secondary btn-sm active'
		},
		
		{
             extend: 'excel',
             filename: 'Etiqa User List',
             className: 'btn btn-warning btn-sm',
             text:'Export to XLS <i class="fa fa-download" aria-hidden="true"></i>',
             
            	 exportOptions: {
                     columns: [ 1,2,3,4,5,6,7,8 ]
             
                 }
             
             
             
             
        }
		           
		         			  ],
		         		      "searching" :false, 
		         	          "columnDefs": [ {
		         	          "searchable": false,
		         	          "orderable": false,
		         	          "targets": [0,1,2,3,4,5,6,7,8],
		         	          "bSortable": false
		         	         
		         	        } ],
		         	        "order": [[ 0, 'asc' ]]
		         	    } );
		         	 
		         	    t.on( 'order.dt search.dt', function () {
		         	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		         	            cell.innerHTML = i+1;
		         	        } );
		         	    } ).draw();
		         	           	
		         	  
		         	   //t.buttons( 0, null ).containers().appendTo( '#btnhere' );
		         	   
		         	   

		         		
		         	   
		         	  $("#createUserProfileBtn").on("click", function(event) {
		         		  
		         		$('#recordFound').hide();
		          		$('#messagePF').hide();
		          		$('#messageName').hide();
		          		$('#messageManagerPF').hide();
		          		$('#messageemail').hide();
		          		$('#messageRole').hide();
							event.preventDefault();
							$.ajax({
								url : 'validateCreateUser',
								type : "POST",
								data : $("#createProfileForm").serialize(),
								dataType : 'json',
								success : function(response) {
									var obj = JSON.stringify(response);
							    	 console.log(response);
													
							    	 var result = $.parseJSON(obj);
												  		  
									 
									 $.each(result, function() {		  			 

										console.log(result.message);
										
										if(result.requiredPF == true){
											
											$('#messagePF').show();
										}
										
										if(result.requiredManagerPF == true){
											$('#messageManagerPF').show();
										}
										if(result.requiredEmail == true){
											$('#messageemail').show();
										}
										if(result.requiredName == true){
											$('#messageName').show();
										}
										
										if(result.requiredRole == true){
											
											$('#messageRole').show();
										}
										
										
									
										if(result.recordFound == false){
											
											if(result.requiredPF == false && result.requiredName == false && result.requiredRole == false){
											
											$('#createProfileForm').attr('action', 'createUserProfileAction');
						                    $('#createProfileForm').submit();
										}
											
										}
										
										if(result.recordFound == true && result.count !=-1){
										
											$('#recordFound').show();
										}
						  	    
							  	    	
							  		  });
									 
							  			  
							  			 		
									
								},
								error : function(data, status, er) {
									//alert(data + "_" + status + "_" + er);
								}
							});
						});
		    
		    
		 
       	  $("#updateUserProfileBtn").on("click", function(event) {
       		  
       		   $('#recordFoundUpdate').hide();
        		$('#messagePFUpdate').hide();
        		$('#messageNameUpdate').hide();
        		$('#messageManagerPF').hide();
        		$('#messageemail').hide();
        		$('#messageRoleUpdate').hide();
        		$('#messageStatusUpdate').hide();
					event.preventDefault();
					$.ajax({
						url : 'validateUpdateUser',
						type : "POST",
						data : $("#updateProfileForm").serialize(),
						dataType : 'json',
						success : function(response) {
							var obj = JSON.stringify(response);
					    	 console.log(response);
											
					    	 var result = $.parseJSON(obj);
										  		  
							 
							 $.each(result, function() {		  			 

								console.log(result.message);
								
								if(result.requiredPF == true){
									
									$('#messagePFUpdate').show();
								}
								
								if(result.requiredName == true){
									
									
									$('#messageNameUpdate').show();
								}
								
								if(result.requiredRole == true){
									
									$('#messageRoleUpdate').show();
								}
								
                                if(result.requiredStatus == true){
									
									$('#messageStatusUpdate').show();
								}
								
                              
							
								if(result.recordFound == false){
							
									if(result.requiredPF == false && result.requiredName == false && result.requiredRole == false && result.requiredStatus == false){
									
									$('#updateProfileForm').attr('action', 'updateUserProfileAction');
				                    $('#updateProfileForm').submit();
								}
									
								}
								
								if(result.recordFound == true && result.count !=-1){
								
									$('#recordFoundUpdate').show();
								}
				  	    
					  	    	
					  		  });
							  
						},
						error : function(data, status, er) {
							//alert(data + "_" + status + "_" + er);
						}
					});
				});
       	   
       	   
       	   
       	});
		         
         	//to reset form after close modal
         $('#createUser').on('hidden.bs.modal', function(){
         	    $(this).find('form')[0].reset();
         		$('#recordFound').hide();
         		$('#messagePF').hide();
         		$('#messageName').hide();
         		$('#messageManagerPF').hide();
         		$('#messageemail').hide();
         		$('#messageRole').hide();
         		$('#recordFoundUpdate').hide();
         		$('#messagePFUpdate').hide();
         		$('#messageNameUpdate').hide();
         		$('#messageRoleUpdate').hide();
         		$('#messageStatusUpdate').hide();
         		
         		//Update
         		$('#messageNameUpdate').hide(); 
          		$('#messagePFUpdate').hide();
          		$('#recordFoundUpdate').hide();
          		$('#messageRoleUpdate').hide();
          		 
         	}); 
                 	
      
     //Edit User Profile before show modal
       	 $('#updateUser').on('show.bs.modal', function (event) {
       	 var modal = $(this);	
     
       	var button = $(event.relatedTarget) // Button that triggered the modal
  	    var recordID = button.data('value') // Extract info from data-* attributes
 
  		$('#recordFound').hide(); 
  		$('#messagePF').hide();
  		$('#messageName').hide();
  		$('#messageManagerPF').hide();
  		$('#messageemail').hide();
  		$('#messageRole').hide();
  	    
  		//update 
  				$('#messageNameUpdate').hide(); 
          		$('#messagePFUpdate').hide();
          		$('#recordFoundUpdate').hide();
          		$('#messageRoleUpdate').hide();
        		$('#messageStatusUpdate').hide();
          		
 
     	modal.find('#usereditid').val(recordID);
  	 $.ajax({  
		     type : "Get",   
		     url : "userDetail", //see in UserController.java
		     data : "id=" + recordID, 
		   dataType: "json", //set to JSON   
		     success : function(response) {
		    	var obj = JSON.stringify(response);
		    	 console.log(response);
		
		
		    	 var result = $.parseJSON(obj);
		  		  $.each(result, function() {	
		  		 // alert(result);	  			 
                   
		  			  // modal.find('.modal-title').text("Transaction Record : "+this['transactionid'] );
		  	    	  //modal.find('.modal-body input').val(result.name);
		  	    	//  alert(result.pfNumber);
		  	    	  modal.find('#pfNumber').val(result.pfNumber);
		  	    	  modal.find('#name').val(result.name);
		  	    	 modal.find('#status').val(result.status);
		  	    	 modal.find('#groupID').val(result.groupID);
		  	    	  //modal.find('.modal-body select').val(result.status);
		  	    	  modal.find('#id').val(result.id);	  
	  	    
		  	    	
		  		  });
		     },  
		     error : function(e) {  
		      alert('Error: ' + e);   
		     }  
		    });   
 
       	})
       	
       	
 
       	
       	
       	  $('#createUser').on('show.bs.modal', function (event) {        	
          	    $(this).find('form')[0].reset(); 
          	 	$('#recordFound').hide(); 
          		$('#messagePF').hide();
          		$('#messageName').hide();
          		$('#messageManagerPF').hide();
          		$('#messageemail').hide();
          		$('#messageRole').hide();
          		
          		
          		//Update
          		$('#messageNameUpdate').hide(); 
          		$('#messagePFUpdate').hide();
          		$('#recordFoundUpdate').hide();
          		$('#messageRoleUpdate').hide();
          		$('#messageStatusUpdate').hide(); 
	    	 
         }); 
		         		
      
        </script>


</body>
</html>