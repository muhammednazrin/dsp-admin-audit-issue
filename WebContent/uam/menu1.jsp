
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<div class="col-sm-3 col-md-2 fluid-menu">
	<div class="sidebar-left sidebar-nicescroller admin">
		<!-- desktop menu -->
		<ul class="sidebar-menu admin">
			<c:forEach var="paramItem" items="${sessionScope.ModuleMainList}"
				varStatus="loop">
				<c:set var="subCount1" value="0" />
				<c:set var="subCount2" value="0" />
				<c:set var="subCount3" value="0" />
				<c:set var="accessRightModule" value="0" />
				<c:set var="NoAccessModule" value="0" />

				<!--<c:forEach var="module" items="${sessionScope.UserGroupModuleDetail}" varStatus="loop">
  		<c:if test="${module.moduleId==paramItem.id}">
  <c:set var="accessRightModule" value="${module.accessRight}"/>
  	</c:if>
</c:forEach>-->

				<c:set var="total" value="${0}" />



				<!-- start : validate no access main menu-->
				<c:forEach var="module"
					items="${sessionScope.UserGroupModuleDetail}" varStatus="loop">
					<c:if test="${paramItem.id==module.moduleId}">
						<c:if test="${module.accessRight=='NA'}">
							<c:set var="NoAccessModule" value="${NoAccessModule + 1}" />
						</c:if>

					</c:if>

					<!-- end -->
				</c:forEach>


				<c:forEach var="subModule" items="${sessionScope.ModuleList}">
					<c:if test="${subModule.parentId==paramItem.id}">

						<c:set var="subCount1" value="${subModule.parentId}" />

						<!-- start 1 -->
						<c:forEach var="module"
							items="${sessionScope.UserGroupModuleDetail}" varStatus="loop">

							<c:if test="${subModule.id==module.moduleId}">
								<c:set var="total" value="${total + 1}" />
								<c:if test="${module.accessRight=='NA'}">
									<c:set var="NoAccessModule" value="${NoAccessModule + 1}" />
								</c:if>


							</c:if>
						</c:forEach>
						<!-- end 1 -->

						<!-- start 2 -->
						<c:forEach var="subModule3" items="${sessionScope.ModuleList}">

							<c:if test="${subModule3.parentId==subModule.id}">

								<c:forEach var="module2"
									items="${sessionScope.UserGroupModuleDetail}" varStatus="loop">

									<c:if test="${subModule3.id==module2.moduleId}">
										<c:set var="total" value="${total + 1}" />
										<c:if test="${module2.accessRight=='NA'}">
											<c:set var="NoAccessModule" value="${NoAccessModule + 1}" />
										</c:if>

										<!--  module = ${paramItem.name} 
module = ${submodule.name} 
module2 = ${submodule3.name} 
total = ${total} 
NoAccessModule = ${NoAccessModule}
accessRightModule= ${accessRightModule} <br><br>-->

									</c:if>
								</c:forEach>
							</c:if>

						</c:forEach>
						<!-- end 2 -->

					</c:if>

				</c:forEach>


				<c:if test="${total == 0}">
					<c:set var="total" value="1" />
				</c:if>

				<c:if test="${NoAccessModule !=total}">
					<li><c:choose>
							<c:when test="${empty paramItem.url }">
								<a href="#">
							</c:when>
							<c:otherwise>
								<a href="${paramItem.url}">
							</c:otherwise>
						</c:choose> <span class="${paramItem.css}"></span> <c:if
							test="${subCount1!=0}">
							<i class="fa fa-angle-right chevron-icon-sidebar"></i>
						</c:if> ${paramItem.name} </a>


						<ul class="submenu">
							<c:forEach var="subModule" items="${sessionScope.ModuleList}">
								<c:set var="subCount2" value="0" />
								<c:forEach var="subModule2" items="${sessionScope.ModuleList}">
									<c:if test="${subModule2.parentId==subModule.id}">
										<c:set var="subCount2" value="${subModule2.parentId}" />

										<c:forEach var="subModule3" items="${sessionScope.ModuleList}">
											<c:if test="${subModule3.parentId==subModule2.id}">
												<c:set var="subCount3" value="${subModule3.parentId}" />
											</c:if>
										</c:forEach>

									</c:if>
								</c:forEach>


								<c:if test="${subModule.parentId==paramItem.id}">

									<c:forEach var="module"
										items="${sessionScope.UserGroupModuleDetail}" varStatus="loop">
										<c:if test="${module.moduleId==subModule.id}">
											<c:set var="accessRightModule" value="${module.accessRight}" />
										</c:if>
									</c:forEach>

									<li><c:if test="${accessRightModule != 'NA'}">
											<c:choose>
												<c:when test="${empty subModule.url }">
													<a href="#">
												</c:when>
												<c:otherwise>
													<a href="${subModule.url}">
												</c:otherwise>
											</c:choose>
											<c:if test="${subCount2!=0}">
												<i class="fa fa-angle-right chevron-icon-sidebar"></i>
											</c:if>${subModule.name}</a>
											<ul class="submenu">
												<c:forEach var="subModule2"
													items="${sessionScope.ModuleList}">
													<c:if test="${subModule2.parentId==subModule.id}">

														<c:forEach var="module"
															items="${sessionScope.UserGroupModuleDetail}"
															varStatus="loop">
															<c:if test="${module.moduleId==subModule2.id}">
																<c:set var="accessRightModule"
																	value="${module.accessRight}" />
															</c:if>
														</c:forEach>

														<li><c:if test="${accessRightModule != 'NA'}">
																<c:choose>
																	<c:when test="${empty subModule2.url }">
																		<a href="#">
																	</c:when>
																	<c:otherwise>
																		<a href="${subModule2.url}">
																	</c:otherwise>
																</c:choose>
																<c:if test="${subCount3!=0}">
																	<i class="fa fa-angle-right chevron-icon-sidebar"></i>
																</c:if>${subModule2.name}</a>

																<ul class="submenu">
																	<c:forEach var="subModule3"
																		items="${sessionScope.ModuleList}">
																		<c:if test="${subModule3.parentId==subModule2.id}">
																			<li><c:choose>
																					<c:when test="${empty subModule3.url }">
																						<a href="#">
																					</c:when>
																					<c:otherwise>
																						<a href="${subModule3.url}">
																					</c:otherwise>
																				</c:choose> ${subModule3.name}</a></li>
																		</c:if>
																	</c:forEach>
																</ul>

															</c:if></li>
													</c:if>
												</c:forEach>
											</ul>
										</c:if></li>
								</c:if>


							</c:forEach>
						</ul></li>
				</c:if>


			</c:forEach>


		</ul>
	</div>
	<!-- /.sidebar-left -->
</div>