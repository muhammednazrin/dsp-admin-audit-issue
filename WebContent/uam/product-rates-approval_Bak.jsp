<%@ page import="java.util.List"%>
<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<% java.text.DateFormat df=new java.text.SimpleDateFormat("dd/MM/yyyy"); %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

.mb-3 {
	margin-bottom: 1rem;
}

.mb-5 {
	margin-bottom: 5rem;
}
</style>
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>
</head>
<body>
	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">System Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-12 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Motor Approver</h4>
														</div>
													</div>
													<!-- Start Form -->
													<form name="userGroup" action="userRole" id="userGroup"
														method="post">
														<section class="content bg-white">
															<div class="row">
																<div class="col-md-6 mb-3">
																	<h4 class="text-center">Original</h4>
																	<table
																		class="table table-striped table-warning table-hover table-outline">
																		<tbody>
																			<tr>
																				<td width="400" align="right">Sum Covered
																					Limit(Min.)</td>
																				<td width="5">:</td>
																				<td width="400">RM0</td>
																			</tr>
																			<tr>
																				<td align="right">Sum Covered Limit(Max.)</td>
																				<td width="5">:</td>
																				<td>RM500000</td>
																			</tr>
																			<tr>
																				<td align="right">Direct Discount (Online)</td>
																				<td width="5">:</td>
																				<td>10</td>
																			</tr>
																			<tr>
																				<td align="right">GST</td>
																				<td width="5">:</td>
																				<td>6</td>
																			</tr>

																			<tr>
																				<td align="right">5 seater incl. driver</td>
																				<td width="5">:</td>
																				<td>RM61.00</td>
																			</tr>
																			<tr>
																				<td align="right">7 seater incl. driver</td>
																				<td width="5">:</td>
																				<td>RM85.00</td>
																			</tr>
																			<tr>
																				<td align="right">Stamp Duty</td>
																				<td width="5">:</td>
																				<td>RM10</td>
																			</tr>
																		</tbody>
																	</table>
																</div>
																<div class="col-md-6 mb-3">
																	<h4 class="text-center">Changes</h4>
																	<table
																		class="table table-striped table-warning table-hover">
																		<tbody>
																			<tr>
																				<td align="right">Sum Covered Limit(Min.)</td>
																				<td width="5">:</td>
																				<td>RM0</td>
																			</tr>
																			<tr>
																				<td align="right">Sum Covered Limit(Max.)</td>
																				<td width="5">:</td>
																				<td>RM500000</td>
																			</tr>
																			<tr>
																				<td align="right">Direct Discount (Online)</td>
																				<td width="5">:</td>
																				<td>12</td>
																			</tr>
																			<tr>
																				<td align="right">GST</td>
																				<td width="5">:</td>
																				<td>6</td>
																			</tr>

																			<tr>
																				<td align="right">5 seater incl. driver</td>
																				<td width="5">:</td>
																				<td>RM68.00</td>
																			</tr>
																			<tr>
																				<td align="right">7 seater incl. driver</td>
																				<td width="5">:</td>
																				<td>RM85.00</td>
																			</tr>
																			<tr>
																				<td align="right">Stamp Duty</td>
																				<td width="5">:</td>
																				<td>RM10</td>
																			</tr>
																		</tbody>
																	</table>
																</div>
															</div>
															<div class="row">
																<div class="col-sm-12 text-center mb-5">
																	<div class="controls">
																		<button class="btn btn-success">Approve</button>
																		&nbsp;&nbsp;
																		<button class="btn btn-danger" data-dismiss="modal"
																			aria-label="Close">Reject</button>
																	</div>
																</div>
															</div>
														</section>
													</form>
													<!-- END FORM -->
												</div>

											</div>
										</div>
									</div>
									<!--row -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->
	<!-- ********************************************CREATE GROUP****************************************** -->
	<!-- start: modal Create-->

	<!-- end: modal Create-->
	<!-- ********************************************UPDATE GROUP****************************************** -->
	<!-- start: modal Update-->
	<div class="modal fade" id="updateUserGroup" tabindex="-1"
		role="dialog" aria-labelledby="updateUserGroupLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="reportDetailLabel">Motor Insurance</h4>
				</div>
				<div class="modal-body">
					<section class="content">
						<div class="row">
							<div class="col-md-6 mb-3">
								<h4 class="tex-center">Original</h4>
								<table class="table table-striped table-warning table-hover">
									<tbody>
										<tr>
											<td align="right">Sum Covered Limit(Min.)</td>
											<td width="5">:</td>
											<td>RM0</td>
										</tr>
										<tr>
											<td align="right">Sum Covered Limit(Max.)</td>
											<td width="5">:</td>
											<td>RM500000</td>
										</tr>
										<tr>
											<td align="right">Direct Discount (Online)</td>
											<td width="5">:</td>
											<td>10</td>
										</tr>
										<tr>
											<td align="right">GST</td>
											<td width="5">:</td>
											<td>6</td>
										</tr>
										<hr>
										<tr>
											<td align="right">5 seater incl. driver</td>
											<td width="5">:</td>
											<td>RM61.00</td>
										</tr>
										<tr>
											<td align="right">7 seater incl. driver</td>
											<td width="5">:</td>
											<td>RM85.00</td>
										</tr>
										<tr>
											<td align="right">Stamp Duty</td>
											<td width="5">:</td>
											<td>RM10</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="col-md-6 mb-3">
								<h4 class="tex-center">Changes</h4>
								<table class="table table-striped table-warning table-hover">
									<tbody>
										<tr>
											<td align="right">Sum Covered Limit(Min.)</td>
											<td width="5">:</td>
											<td>RM0</td>
										</tr>
										<tr>
											<td align="right">Sum Covered Limit(Max.)</td>
											<td width="5">:</td>
											<td>RM500000</td>
										</tr>
										<tr>
											<td align="right">Direct Discount (Online)</td>
											<td width="5">:</td>
											<td>12</td>
										</tr>
										<tr>
											<td align="right">GST</td>
											<td width="5">:</td>
											<td>6</td>
										</tr>
										<hr>
										<tr>
											<td align="right">5 seater incl. driver</td>
											<td width="5">:</td>
											<td>RM68.00</td>
										</tr>
										<tr>
											<td align="right">7 seater incl. driver</td>
											<td width="5">:</td>
											<td>RM85.00</td>
										</tr>
										<tr>
											<td align="right">Stamp Duty</td>
											<td width="5">:</td>
											<td>RM10</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="controls tex-center">
									<button class="btn btn-success">Approve</button>
									&nbsp;&nbsp;
									<button class="btn btn-danger" data-dismiss="modal"
										aria-label="Close">Reject</button>
								</div>
							</div>
						</div>
					</section>
				</div>
				<div class="modal-footer">
					<!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</div>
	</div>
	<!-- end: modal -->
	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->
	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->
	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->
	<script type="text/javascript">
          $(document).ready(function() {
               
              });
          $('#name').keyup(function() {
              if($(this).val() == ''){
                 $('#messageName').show();
              }else{
                 $('#messageName').hide();
                 $('#recordFound').hide();
              }
          });
          
        
        </script>
</body>
</html>