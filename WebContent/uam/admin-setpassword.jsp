<%@ page import="java.io.*,java.util.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>

</head>

<body>
	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->


	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->

		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->

					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back" style="width: 100%;">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">System Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<form:form
												action="${pageContext.request.contextPath}/setPasswordDone"
												method="post" id="updatepassword">
												<!--  <input type="hidden" name="action" value="changepassword">-->
												<div class="row">
													<div class="col-sm-12">
														<div class="title">
															<div class="sub">
																<h4>System Administration</h4>
															</div>
														</div>
														<div class="content-inner">
															<div class="the-box">
																<div class="row">
																	<div class="col-sm-12">
																		<div class="title">
																			<div class="sub">
																				<h4>Set Password</h4>
																			</div>
																		</div>
																	</div>
																	<div class="gap gap-mini"></div>
																</div>

																<div class="row">
																	<div class="col-md-8">
																		<div class="form-horizontal info-meor">
																			<div class="col-xs-12">
																				<c:if test="${(not empty  setpw_error)}">
																					<div style="color: red">
																						<c:out value="${setpw_error}" />
																					</div>
																				</c:if>
																			</div>
																			<div class="col-xs-12">
																				<c:if test="${(not empty  setpw_success)}">
																					<div style="color: green">
																						<c:out value="${setpw_success}" />
																					</div>
																				</c:if>
																			</div>

																			<div class="form-group">
																				<div class="col-sm-6 column text-right">
																					<label class="control-label">User Name </label>
																				</div>
																				<div class="col-sm-6">
																					<input type="text" class="form-control"
																						name="username" id="username" readonly required
																						value="<c:out value="${userID}" />"> <span
																						id="msg_user" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your user name</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<div class="col-sm-6 column text-right">
																					<label class="control-label"> Password</label>
																				</div>
																				<div class="col-sm-6">
																					<input type="password" class="form-control"
																						name="newpassword" id="newpassword"
																						autocomplete="off" required> <span
																						id="msg_newpw" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your password</span> <span id="msg_newpw8"
																						class="hidden"
																						style="color: red; text-align: left">Your
																						password must be at least 8 characters including
																						at least one uppercase letter, one special
																						character and numeric characters</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<div class="col-sm-6 column text-right">
																					<label class="control-label"> Confirm
																						Password</label>
																				</div>
																				<div class="col-sm-6">
																					<input type="password" class="form-control"
																						name="confpassword" id="confpassword"
																						autocomplete="off" required> <span
																						id="msg_confpw" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your confirm password</span> <span id="msg_confpw8"
																						class="hidden"
																						style="color: red; text-align: left">Your
																						password must be at least 8 characters including
																						at least one uppercase letter, one special
																						character and numeric characters</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<div class="col-sm-6 column text-right"></div>
																				<div class="col-sm-6">
																					<span id="passMatch" class="hidden"
																						style="color: red; text-align: left">Passwords
																						are Not Matching</span> <label class="control-label">
																						**Password min 8 characters, must contain at least
																						one capital letter, number and special character</label>
																				</div>


																			</div>
																		</div>
																	</div>
																</div>



															</div>


														</div>
														<!--content inner -->
													</div>
													<!--col-sm-12 -->

													<div class="col-sm-12">
														<div class="text-right">
															<a href="#" class="btn btn-warning btn-sm"
																onClick="submitForm();"><i class="fa fa-edit"></i>
																Submit</a>
															<!--  <input class="btn btn-warning btn-sm"  type="submit" value="Submit"/> -->
														</div>
													</div>
													<!--col-sm-12 -->
												</div>
												<!--row -->
											</form:form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->
		</div>
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<%--   <jsp:include page="pageFooter.jsp" /> --%>
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->

	<!-- END PAGE CONTENT -->

	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
            <div id="back-top">
                <a href="#top"><i class="fa fa-chevron-up"></i></a>
            </div>
            -->
	<!-- END BACK TO TOP -->


	<!--
            ===========================================================
            END PAGE
            ===========================================================
            -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->

	<script>
       
    </script>

	<script type="text/javascript">
        var regex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/
            function submitForm(){
                result=true;
                if(!$('#username').val().length) {
                    $('#msg_user').removeClass('hidden');
                    $('#msg_user').addClass('visible');
                    $('#username').focus();
                    result=false;
                }
                if(!$('#newpassword').val().length) {
                    $('#msg_newpw').removeClass('hidden');
                    $('#msg_newpw').addClass('visible');
                    $('#newpassword').focus();
                    result=false;
                }
                if(!$('#confpassword').val().length) {
                    $('#msg_cnfpw').removeClass('hidden');
                    $('#msg_cnfpw').addClass('visible');
                    $('#confirmpassword').focus();
                    result=false;
                }
                
                if (! regex.test ($('#newpassword').val())) {
                	 $('#msg_newpw8').removeClass('hidden');
                     $('#msg_newpw8').addClass('visible');
                	 result=false;
                }
                if (!regex.test ($('#confpassword').val())) {
                	$('#msg_cnfpw8').removeClass('hidden');
                    $('#msg_cnfpw8').addClass('visible');
               	result=false;
               }
                
               if (!( new String($('#newpassword').val()).valueOf() == new String($('#confpassword').val()).valueOf()) ) {
            	   $('#passMatch').removeClass('hidden');
                   $('#passMatch').addClass('visible');
            	   alert ("Passwords are Not Maching");
            	   result=false;
               }
               
               if (result==true) {
               	$('#updatepassword').submit();   
               }
             
            }

            $('#username').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_user').removeClass('visible');
                    $('#msg_user').addClass('hidden');
                }
            });
            $('#newpassword').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_newpw').removeClass('visible');
                    $('#msg_newpw').addClass('hidden');
                    $('#msg_newpw8').removeClass('visible');
                    $('#msg_newpw8').addClass('hidden');
                }
            });
            $('#confirmpassword').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_cnfpw').removeClass('visible');
                    $('#msg_cnfpw').addClass('hidden');
                    $('#msg_cnfpw8').removeClass('visible');
                    $('#msg_cnfpw8').addClass('hidden');
                }
            });
          
            


        </script>
</body>
</html>