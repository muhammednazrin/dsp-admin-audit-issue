<%@ page import="java.util.List"%>
<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<% java.text.DateFormat df=new java.text.SimpleDateFormat("dd/MM/yyyy"); %>



<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->

<link
	href="<c:url value="/resources/assets/plugins/font-awesome/css/font-awesome.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/css/owl.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/css/bootstrap.css" />"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/css/bootstrap-select.css" />"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/plugins/datatable/css/bootstrap.datatable.min.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/css/menu.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/jAlert/jAlert.css"/>"
	rel="stylesheet">

<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

select#status {
	width: auto;
	height: 34px;
	display: inline-block
}

.pull-right {
	float: right;
	clear: both;
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>

</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1__.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">System Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>


								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Update User Roles</h4>

														</div>
													</div>
													<!-- Start Form -->
													<form name="userRole"
														action="${pageContext.request.contextPath}/uam/userRoleUpdateAction"
														id="userRole" method="post">
														<input type="hidden" name="firstid" value="${firstid}" />
														<input type="hidden" name="lastId" value=" ${lastId}" />

														<c:forEach items="${RoleModuleList}" var="element"
															varStatus="loop">

															<input type="hidden" name="roleModuleid"
																value=" ${element.id}" />
															<input type="hidden" name="roleId"
																value=" ${element.roleId}" />
															<input type="hidden" name="moduleId"
																value=" ${element.moduleId}" />

															<div class="content-inner">
																<div class="the-box">
																	<div class="col-sm-12">
																		<div class="form-horizontal">
																			<div class="form-group">

																				<div class="col-sm-4">
																					<label class="control-label">Module: </label>
																					${element.module_Name}
																				</div>

																				<div class="col-sm-4">
																					<label class="control-label">Role : </label>

																					${element.role_Name}

																				</div>


																				<div class="col-sm-4">
																					<label for="status">Status</label> <select
																						class="form-control" id="status" name="status">

																						<c:forEach items="${StatusList}" var="element2">

																							<option
																								value="<c:out value="${element2.paramCode}"/>"
																								<c:if test="${element.status eq  element2.paramCode }" >selected</c:if>>
																								<c:out value="${element2.paramName}" />
																							</option>

																						</c:forEach>
																					</select>
																				</div>
																			</div>

																		</div>

																	</div>
																</div>
															</div>
														</c:forEach>
														<!-- Menu List Display Part pramaiyan -->

														<input type="hidden" name="totalMenuCount"
															value="<c:out value="${totalMenuCount}"/>">

														<div class="container">
															<div class="row">
																<div class="col-lg-12">

																	<c:forEach var="m1" items="${level1}">
																		<!-- Level 1  -->

																		<div class="row">
																			<div class="col-sm-8">
																				<ul>
																					<li><c:out value="${m1.name}" /> <c:set
																							var="contains" value="false" /> <c:forEach
																							var="item" items="${level2}">
																							<c:if test="${item.parentId eq m1.id}">
																								<c:set var="contains" value="true" />
																							</c:if>
																						</c:forEach> <c:if test="${contains eq false}">
																							<div class="pull-right">
																								<c:forEach var="modulePrmssn"
																									items="${modulePrmssnList}">

																									<label class="checkbox-inline"> <input
																										type="checkbox"
																										name="accessRight<c:out value="${m1.id}"/>"
																										value="<c:out value="${modulePrmssn.prmssnId}"/>" />
																										${modulePrmssn.prmssn_Desc}
																									</label>
																								</c:forEach>
																							</div>
																						</c:if> <c:out value="${modulePrmssn.prmssnId}"></c:out>

																						<c:forEach var="m2" items="${level2}">
																							<!-- Level 2  -->
																							<c:if test="${m1.id eq m2.parentId}">
																								<div class="row">
																									<div class="col-sm-12">
																										<ul>
																											<li><c:out value="${m2.name}" /> <c:set
																													var="contains" value="false" /> <c:forEach
																													var="item" items="${level3}">
																													<c:if test="${item.parentId eq m2.id}">
																														<c:set var="contains" value="true" />
																													</c:if>
																												</c:forEach> <c:if test="${contains eq false}">
																													<div class="pull-right">

																														<c:forEach var="modulePrmssn"
																															items="${modulePrmssnList}">

																															<%--   <c:forEach var="roleMenu" items="${roleMenuList}"> 	 --%>
																															<label class="checkbox-inline"> <input
																																type="checkbox"
																																name="accessRight<c:out value="${m2.id}"/>"
																																value="<c:out value="${modulePrmssn.prmssnId}"/>" <%--  <c:if test="${modulePrmssn.prmssnId eq roleMenu.prmssnId && m2.id eq roleMenu.menuId }">checked="checked"</c:if> --%>
				              />
																																${modulePrmssn.prmssn_Desc}
																															</label>

																															<%--  </c:forEach> --%>



																														</c:forEach>
																													</div>
																												</c:if> <c:forEach var="m3" items="${level3}">
																													<!-- Level 3  -->
																													<c:if test="${m2.id eq m3.parentId}">
																														<ul>
																															<li><c:out value="${m3.name}" /> <c:set
																																	var="contains" value="false" /> <c:forEach
																																	var="item" items="${level4}">
																																	<c:if test="${item.parentId eq m3.id}">
																																		<c:set var="contains" value="true" />
																																	</c:if>
																																</c:forEach> <c:if test="${contains eq false}">
																																	<div class="pull-right">

																																		<c:forEach var="modulePrmssn"
																																			items="${modulePrmssnList}">
																																			<%--  <c:forEach var="roleMenu" items="${roleMenuList}"> --%>
																																			<label class="checkbox-inline">
																																				<input type="checkbox"
																																				name="accessRight<c:out value="${m3.id}"/>"
																																				value="<c:out value="${modulePrmssn.prmssnId}"/>" <%--   <c:if test="${modulePrmssn.prmssnId eq roleMenu.prmssnId && m3.id eq roleMenu.menuId }">checked="checked"</c:if> --%>
				              />
																																				${modulePrmssn.prmssn_Desc}
																																			</label>
																																			<%-- </c:forEach> --%>
																																		</c:forEach>
																																	</div>

																																</c:if> <c:forEach var="m4" items="${level4}">
																																	<!-- Level 4  -->
																																	<c:if test="${m3.id eq m4.parentId}">
																																		<ul>
																																			<li><c:out value="${m4.name}" />
																																				<div class="pull-right">

																																					<c:forEach var="modulePrmssn"
																																						items="${modulePrmssnList}">
																																						<%--  <c:forEach var="roleMenu" items="${roleMenuList}"> --%>
																																						<label class="checkbox-inline">
																																							<input type="checkbox"
																																							name="accessRight<c:out value="${m4.id}"/>"
																																							value="<c:out value="${modulePrmssn.prmssnId}"/>" <%--  <c:if test="${modulePrmssn.prmssnId eq roleMenu.prmssnId && m4.id eq roleMenu.menuId }">checked="checked"</c:if> --%>
				                />
																																							${modulePrmssn.prmssn_Desc}
																																						</label>
																																						<%-- </c:forEach> --%>
																																					</c:forEach>

																																				</div></li>
																																		</ul>
																																	</c:if>
																																</c:forEach> <!-- Level 4  --></li>
																														</ul>
																													</c:if>
																												</c:forEach> <!-- Level 3  --></li>
																										</ul>
																									</div>
																								</div>
																							</c:if>
																						</c:forEach> <!-- Level 2  --></li>
																				</ul>
																			</div>
																		</div>

																	</c:forEach>

																</div>
															</div>
														</div>
														<center>

															<input type="submit" value="Update"
																class="btn btn-warning">&nbsp; <a
																href="userRole" class="btn btn-danger btn" role="button"
																aria-pressed="true">Cancel</a>
														</center>

													</form>

													<br>
												</div>
												<!-- /.the-box -->
											</div>
											<!-- End warning color table -->
										</div>
									</div>
								</div>
								<!--row -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.container -->

	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->

	<!-- ********************************************CREATE GROUP****************************************** -->
	<!-- start: modal Create-->
	<div class="modal fade" id="createUserGroup" tabindex="-1"
		role="dialog" aria-labelledby="createUserGroupLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="reportDetailLabel">Create User
						Role</h4>
				</div>
				<div class="modal-body">

					<div class="box-body">
						<div class="content-wrapper">

							<section class="content">
								<div class="container">
									<div class="row">
										<div class="col-md-6">

											<form class="form-horizontal"
												action="${pageContext.request.contextPath}/uam/createUserGroupProfileAction"
												method="POST" id="createGroupProfileForm"
												name="createGroupProfileForm">
												<fieldset>
													<input type="hidden" name="status" id="status" value="1">

													<%-- <input type="hidden" name="totalModule" id="totalModule" value="${TotalModule}"> --%>

													<!-- Start Pramaiyan -->

													<div class="control-group">
														<label class="control-label" for="username">Module</label>
														<div class="controls">
															<select class="form-control" name="moduleDesc"
																id=moduleSelect>
																<option value="" selected>-Select Module-</option>
																<c:forEach items="${ModuleList}" var="element">
																	<option value="<c:out value="${element.id}"/>">
																		<c:out value="${element.name}" />
																	</option>
																</c:forEach>
															</select>

														</div>
													</div>

													<div class="control-group">
														<label class="control-label" for="username">Role
															Name </label>
														<div class="controls">
															<select class="form-control" name="role">
																<option value="" selected>-Select Role-</option>
																<c:forEach items="${RoleList}" var="element">
																	<option value="<c:out value="${element.id}"/>">
																		<c:out value="${element.name}" />
																	</option>
																</c:forEach>
															</select>

														</div>
													</div>
													<!-- End Pramaiyan -->
													<!-- <div class="control-group">
<div class="controls">
<div id="recordFound" class="alert alert-danger fade in alert-dismissable hidediv">
<strong><i class="fa fa-exclamation"></i> Role Already Exist</strong>
</div>
</div>
</div> -->

													<!-- <div class="control-group">
<div class="controls">
<div id="messageName" class="alert alert-danger fade in alert-dismissable hidediv">
<strong><i class="fa fa-exclamation"></i> Please enter Role Name</strong>
</div>
</div>
</div> -->

													<!--  <div class="control-group">
  <label class="control-label" for="username">Menu Access</label>
  <div class="controls">
    &nbsp;
  </div>
</div>
-->


													<!-- <div class="control-group">
<div class="controls">
<div id="messageModule" class="alert alert-danger fade in alert-dismissable hidediv">
<strong><i class="fa fa-exclamation"></i> Please select module access</strong>
</div>
</div>
</div> -->


													<div class="control-group">
														<div class="controls"></div>
													</div>


													<div class="control-group">
														<!-- Button -->
														<div class="controls">
															<button class="btn btn-warning"
																id="createGroupProfileBtn">Create User Role</button>
															&nbsp;
															<button class="btn btn-danger" data-dismiss="modal"
																aria-label="Close">Cancel</button>
														</div>
													</div>
												</fieldset>
											</form>

										</div>
									</div>
								</div>
							</section>
						</div>
					</div>
					<!-- /.box-body -->



				</div>
				<div class="modal-footer">
					<!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</div>
	</div>


	<!-- end: modal Create-->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->

	<script type="text/javascript">
	
	        	$(document).ready(function() {
         		
         	     var savemessage="<c:out value="${savemessage}"/>";
         	      // show when the button is clicked
         	      if (savemessage.length) {
         	   	// alert(updatemessage);
         	   successAlert('Success!', savemessage);
         	      }  
         		
         	});
	 		   
        	$(document).ready(function() {
         	   //set  the  check box  status value from Data base  
         			var list = ${roleMenuList};
         			$.each(list, function( index, value ) {
         				console.log("Before Permission Value data   :  "+index +":"+ value);
         				var arr = 	value.split('$');
         				console.log("Before Permission Value data   :"+arr);
         				console.log("array length   :"+arr.length);
         				var arraylength=parseInt(arr.length);
         				if(arraylength>0){
         				for(var i=0; i<=arraylength-1; i++)
         					{
         					var value=arr[i];
         					if(value>0){
         					$('input[type=checkbox][name="accessRight'+index+'"]').filter('[value='+arr[i]+']').prop('checked', true);
         					}
         					}
         			} 		

         		});               	  
		         	

		         	});
        	


    	      
    	      $('input[type="checkbox"]').on('change', function() {

    	    	  // uncheck sibling checkboxes (checkboxes on the same row)
    	    	  //  $(this).siblings().prop('checked', false);
    	  		   $('input[type=checkbox][name="accessRight"]').prop('checked', true);
    	    	  // uncheck checkboxes in the same column
    	    	   // $('div').find('input[type="checkbox"]:eq(' + $(this).index() + ')').not(this).prop('checked', false);

    	    	});
        	
        	
    	  
         	
        </script>
</body>
</html>