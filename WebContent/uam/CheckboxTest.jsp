<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<% java.text.DateFormat df=new java.text.SimpleDateFormat("dd/MM/yyyy"); %>



<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link
	href="<c:url value="/resources/assets/plugins/font-awesome/css/font-awesome.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/css/owl.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/css/bootstrap.css" />"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/css/bootstrap-select.css" />"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/plugins/datatable/css/bootstrap.datatable.min.css"/>"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/plugins/datatable/css/dataTables.searchHighlight.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/css/menu.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/plugins/all.min.css"/>"
	rel="stylesheet" type="text/css" />
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>

</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1__.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">System Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Create Roles</h4>
														</div>
													</div>

													<!-- Start Form  Create Role on 05032018-->


													<form class="form-horizontal" action="createRoleNameAction"
														method="POST" id="createUserRoleForm"
														name="createUserRoleForm">
														<fieldset>
															<div class="control-group">
																<label class="control-label" for="username">
																	Enter Role Name</label>
																<div class="controls">
																	<input type="text" id="roleName" name="roleName"
																		placeholder="" class="form-control input-lg">
																</div>
															</div>

															<div class="control-group">
																<label class="control-label" for="username">
																	Enter Role Name</label>
																<div class="controls">
																	<ul class="chk-container">
																		<li><input type="checkbox" id="selecctall"
																			name="selecctall" value="All" /></li>
																		<li><input type="checkbox"
																			name="data[InventoryMaster][error]" value="error"
																			id="InventoryMasterError" /></li>
																		<li><input type="checkbox" name="checkid[]"
																			class="checkbox1" value="1" id="InventoryMasterId" /></li>
																		<li><input type="checkbox" name="checkid[]"
																			class="checkbox1" value="2" id="InventoryMasterId" /></li>

																	</ul>





																</div>
															</div>

															<div class="modal fade" id="createRoleName" tabindex="-1"
																role="dialog" aria-labelledby="createUserGroupLabel">
																<div class="control-group">
																	<div class="controls">
																		<div id="recordFound"
																			class="alert alert-danger fade in alert-dismissable hidediv">
																			<strong><i class="fa fa-exclamation"></i>
																				Role Name Already Exist</strong>
																		</div>
																	</div>
																</div>
															</div>


															<div class="control-group">
																<div class="controls">&nbsp;</div>
															</div>

															<div class="control-group">
																<!-- Button -->
																<div class="controls">
																	<input type="submit" class="btn btn-warning"
																		id="createRoleNameBtn">&nbsp;
																	<button class="btn btn-danger" data-dismiss="modal"
																		aria-label="Close">Cancel</button>
																</div>
															</div>
														</fieldset>
													</form>
													<!-- END FORM  End by pramaiyan 25022018 -->


												</div>
												<!-- End warning color table -->
											</div>
										</div>
									</div>
									<!--row -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->

	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->



	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->


	<script type="text/javascript">
         	$(document).ready(function() {
		        	 var t = $('#admin-datatable-second_admin').DataTable( {
	         				dom: 'Bfrtip',
		         			  buttons: [	
		{
		    extend: 'pageLength',
		    className: 'btn btn-secondary btn-sm active'
		},
		{
            extend: 'excel',
            filename: 'Etiqa Group List',
            className: 'btn btn-warning btn-sm',
            text:'Export to XLS <i class="fa fa-download" aria-hidden="true"></i>',
            
           	 exportOptions: {
                    columns: [1,2,3,4,5,6]
                }
       }
		           
		         			  ],
		         		      "searching" :false, 
		         	          "columnDefs": [ {
		         	          "searchable": false,
		         	          "orderable": false,
		         	          "targets": [0,1,2,3,4,5,6],
		         	          "bSortable": false
		         	         
		         	        } ],
		         	        "order": [[ 0, 'asc' ]]
		         	    } );
		         	 
		         	    t.on( 'order.dt search.dt', function () {
		         	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		         	            cell.innerHTML = i+1;
		         	        } );
		         	    } ).draw();
		         	           	
		         	  
		         	  // t.buttons( 0, null ).containers().appendTo( '#btnhere' );
		         	   
		         	});
         	
         	
         	 $("#createRoleNameBtn").on("click", function(event) { 
	         		$('#recordFound').hide();	 
	          			event.preventDefault();
						$.ajax({
							url : 'validateCreateRoleName',
							type : "POST",
							data : $("#createUserRoleForm").serialize(),
							dataType : 'json',
							success : function(response) {
								var obj = JSON.stringify(response);
						    	 
						    	 var result = $.parseJSON(obj); 
								 
								 $.each(result, function() {		  			 
  
									if(result.recordFound == true && result.count !=-1){
									
										$('#recordFound').show();
									} 
									 
                                     if(result.recordFound == false){   
										$('#createUserRoleForm').attr('action', 'createRoleNameAction');
					                    $('#createUserRoleForm').submit();
									} 
						  		  }); 
							},
							error : function(data, status, er) {
								
							}
						});
					});
					         	 
		         
         	//to reset form after close modal
         $('.modal').on('hidden.bs.modal', function(){
         	    $(this).find('form')[0].reset();  
         	   //$('input:checkbox').removeAttr('checked');
        		//$('#messageName').hide();       		
         	  
         	}); 
         
         	
         $(document).ready(function() {
        	    $('#selecctall').click(function(event) { 
        	        if(this.checked) { // check select status
        	            $('.permission').each(function() { 
        	                this.checked = true;  //select all 
        	            });
        	        }else{
        	            $('.permission').each(function() { 
        	                this.checked = false; //deselect all             
        	            });        
        	        }
        	    });
        	   
        	});
		 
        </script>

</body>
</html>