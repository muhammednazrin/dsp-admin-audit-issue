
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
<script src="<c:url value="/resources/assets/js/jquery.min.js" />"
	type="text/javascript"></script>
<script src="<c:url value="/resources/assets/jAlert/jAlert.min.js" />"></script>
<script
	src="<c:url value="/resources/assets/jAlert/jAlert-functions.min.js" />"></script>
<script src="<c:url value="/resources/assets/js/bootstrap.js" />"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/assets/plugins/retina/retina.min.js" />"></script>
<script
	src="<c:url value="/resources/assets/plugins/nicescroll/jquery.nicescroll.js" />"></script>
<script
	src="<c:url value="/resources/assets/plugins/slimscroll/jquery.slimscroll.min.js" />"></script>
<script
	src="<c:url value="/resources/assets/plugins/backstretch/jquery.backstretch.min.js" />"></script>
<script src="<c:url value="/resources/assets/js/apps.js" />"></script>

<!-- PLUGINS -->
<script
	src="<c:url value="/resources/assets/plugins/owl-carousel/owl.carousel.min.js" />"></script>
<script
	src="<c:url value="/resources/assets/plugins/prettify/prettify.js" />"></script>
<script
	src="<c:url value="/resources/assets/plugins/magnific-popup/jquery.magnific-popup.min.js" />"></script>


<script
	src="<c:url value="/resources/assets/plugins/icheck/icheck.min.js" />"></script>
<script
	src="<c:url value="/resources/assets/plugins/datepicker/bootstrap-datepicker.js" />"></script>
<script
	src="<c:url value="/resources/assets/plugins/timepicker/bootstrap-timepicker.js" />"></script>
<script
	src="<c:url value="/resources/assets/plugins/mask/jquery.mask.min.js" />"></script>
<script
	src="<c:url value="/resources/assets/plugins/validator/bootstrapValidator.min.js" />"></script>
<script
	src="<c:url value="/resources/assets/plugins/datatable/js/jquery.dataTables.min.js" />"></script>
<script
	src="<c:url value="/resources/assets/plugins/datatable/js/bootstrap.datatable.js" />"></script>
<script
	src="<c:url value="/resources/assets/plugins/datatable/js/jquery.highlight.js" />"></script>
<script
	src="<c:url value="/resources/assets/plugins/datatable/js/dataTables.searchHighlight.min.js" />"></script>
<script
	src="<c:url value="/resources/assets/plugins/summernote/summernote.min.js" />"></script>
<script
	src="<c:url value="/resources/assets/plugins/markdown/markdown.js" />"></script>
<script
	src="<c:url value="/resources/assets/plugins/markdown/to-markdown.js" />"></script>
<script
	src="<c:url value="/resources/assets/plugins/markdown/bootstrap-markdown.js" />"></script>
<script
	src="<c:url value="/resources/assets/plugins/slider/bootstrap-slider.js" />"></script>
<script
	src="<c:url value="/resources/assets/plugins/toastr/toastr.js" />"></script>
<script
	src="<c:url value="/resources/assets/plugins/newsticker/jquery.newsTicker.min.js" />"></script>
<script
	src="<c:url value="/resources/assets/plugins/placeholder/jquery.placeholder.js" />"></script>
<script src="<c:url value="/resources/assets/js/shieldui-all.min.js" />"
	type="text/javascript"></script>
<script src="<c:url value="/resources/assets/js/jszip.min.js" />"
	type="text/javascript"></script>

<script
	src="<c:url value="/resources/assets/plugins/jquery-knob/jquery.knob.js" />"></script>
<script
	src="<c:url value="/resources/assets/plugins/jquery-knob/knob.js" />"></script>

<script
	src="<c:url value="/resources/assets/js/jquery.dataTables.min.js"/>"></script>
<script
	src="<c:url value="/resources/assets/js/dataTables.bootstrap.min.js"/>"></script>
<script
	src="<c:url value="/resources/assets/js/dataTables.buttons.min.js"/>"></script>
<script
	src="<c:url value="/resources/assets/js/buttons.bootstrap.min.js"/>"></script>
<script src="<c:url value="/resources/assets/js/jszip.min.js"/>"></script>
<script src="<c:url value="/resources/assets/js/pdfmake.min.js"/>"></script>
<script src="<c:url value="/resources/assets/js/vfs_fonts.js"/>"></script>
<script src="<c:url value="/resources/assets/js/buttons.html5.min.js"/>"></script>
<script src="<c:url value="/resources/assets/js/buttons.print.min.js"/>"></script>
<script
	src="<c:url value="/resources/assets/js/buttons.colVis.min.js"/>"></script>
<script src="<c:url value="/resources/assets/js/toastr.min.js"/>"></script>


