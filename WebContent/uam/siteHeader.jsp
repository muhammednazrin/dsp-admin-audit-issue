
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link
	href="<c:url value="/resources/assets/css/bootstrap.min.css" /> rel="stylesheet">

<!-- PLUGINS CSS -->
<link
	href="<c:url value="/resources/assets/plugins/owl-carousel/owl.carousel.min.css" />"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/plugins/owl-carousel/owl.theme.min.css" />"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/plugins/owl-carousel/owl.transitions.min.css" />"
	rel="stylesheet">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link
	href="<c:url value="/resources/assets/plugins/font-awesome/css/font-awesome.min.css" />"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/plugins/footable-3/css/footable.bootstrap.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/css/owl.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/css/bootstrap.css" />"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/css/bootstrap-select.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/css/menu.css" />"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/plugins/datepicker/datepicker.min.css" />"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/plugins/c3-chart/c3.min.css" />"
	rel="stylesheet">
<!-- Select2 -->
<link
	href="<c:url value="/resources/assets/select2/dist/css/select2.css" />"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/select2/dist/css/select2.min.css" />"
	rel="stylesheet">


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="assets/libs/html5shiv.js"></script>
        <script src="assets/libs/respond.min.js"></script>
        <![endif]-->

<link
	href="<c:url value="/resources/assets/js/jquery.dataTables.min.css" />"
	rel="stylesheet" type="text/css">
<link
	href="<c:url value="/resources/assets/js/buttons.dataTables.min.css" />"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/js/dataTables.bootstrap.min.css" />"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/js/buttons.bootstrap.min.css" />"
	rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/assets/css/toastr.min.css" />"
	rel="stylesheet">
<script type="text/javascript">
function idleTimer() {
//alert('Site Header Normal in UAM ');
    var t;
    //window.onload = resetTimer;
    window.onmousemove = resetTimer; // catches mouse movements
    window.onmousedown = resetTimer; // catches mouse movements
    window.onclick = resetTimer;     // catches mouse clicks
    window.onscroll = resetTimer;    // catches scrolling
    window.onkeypress = resetTimer;  //catches keyboard actions

    function logout() {
    alert('Your session is Expired');
        window.location.href = '${pageContext.request.contextPath}/doLogout';  //Adapt to actual logout script
    }

   function reload() {
    
          window.location = self.location.href;  //Reloads the current page
   }

   function resetTimer() {
        clearTimeout(t);
        t = setTimeout(logout, 900000);  // time is in milliseconds (1000 is 1 second)
        t= setTimeout(reload,  900000);  // time is in milliseconds (1000 is 1 second)
    }
}
idleTimer();
</script>
