
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page session="true"%>

<div class="col-sm-3 col-md-2 fluid-menu">
	<div class="sidebar-left sidebar-nicescroller admin">
		<!-- desktop menu -->
		<ul class="sidebar-menu admin">
			<c:if test="${sessionScope.accessRight0 != 'NA'}">
				<li><a href="getDashboard"> <span
						class="icon-sidebar icon dashboard"></span> Dashboard
				</a></li>
			</c:if>
			<c:if
				test="${sessionScope.accessRight1 != 'NA' || sessionScope.accessRight2 != 'NA' }">
				<li><a href="#"> <span class="icon-sidebar icon services"></span>
						<i class="fa fa-angle-right chevron-icon-sidebar"></i> System
						Administration
				</a>
					<ul class="submenu">
						<li><a href="#"><i
								class="fa fa-angle-right chevron-icon-sidebar""></i>User Roles
								&amp; Permission</a> <c:if
								test="${sessionScope.accessRight1 != 'NA'}">
								<ul class="submenu">
									<li><a href="userProfile">User Profile</a>
								</ul>
							</c:if> <c:if test="${sessionScope.accessRight2 != 'NA'}">
								<ul class="submenu">
									<li><a href="userGroup">User Group Profile</a>
								</ul>
							</c:if></li>
						<!-- <li><a href="admin-system-audit-log.html">Audit Log Maintenance</a></li>-->
						<!-- <li class="active"><a href="auditLogList">Audit Trail/Log Report</a></li>-->
						<!--  <li><a href="admin-system-log-report.html">Integration Log Report</a></li>-->
					</ul></li>
			</c:if>
			<!-- 
                                    <li class="submenu">
                                        <a href="#">
                                            <span class="icon-sidebar icon services"></span>
                                            <i class="fa fa-angle-right chevron-icon-sidebar"></i>
                                            Business Administration
                                        </a>
                                        <ul class="submenu">
                                           
                                            <li class="">
                                                <a href="#">
                                                    <i class="fa fa-angle-right chevron-icon-sidebar2"></i>
                                                    Product Management
                                                </a>
                                                <ul class="submenu">
                                                    <li>
                                                        <a href="#">
                                                        <i class="fa fa-angle-right chevron-icon-sidebar3"></i>
                                                        Motor Insurance
                                                        </a>
                                                        <ul class="submenu">
                                                            <li><a href="${pageContext.request.contextPath}/products/motor/admin-business-product-management-motor-product-infomation.jsp">Product Information</a></li>
                                                            <li><a href="admin-business-product-management-motor-product-rate.html">Product Rates</a></li>
                                                            <li><a href="admin-business-product-management-motor-excess.html">Excess</a></li>
                                                            <li><a href="admin-business-product-management-motor-loading.html">Loading</a></li>
                                                            <li><a href="admin-business-product-management-motor-nvic.html">NVIC Listing</a></li>
                                                            <li><a href="admin-business-product-management-motor-payment.html">Payment Method</a></li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                        <i class="fa fa-angle-right chevron-icon-sidebar3"></i>
                                                        Houseowner/Householder
                                                        </a>
                                                        <ul class="submenu visible">
                                                            <li><a href="${pageContext.request.contextPath}/products/hohh/admin-business-product-management-house-product-infomation.jsp">Product Information</a></li>
                                                            <li><a href="admin-business-product-management-house-product-rate.html">Product Rates</a></li>
                                                            <li class="active selected"><a href="admin-business-product-management-house-payment.html">Payment Method</a></li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                        <i class="fa fa-angle-right chevron-icon-sidebar3"></i>
                                                        World Traveller Care
                                                        </a>
                                                        <ul class="submenu">
                                                            <li><a target="_parent" href="${pageContext.request.contextPath}/products/travel/admin-business-product-management-traveller-product-infomation.jsp">Product Information</a></li>
                                                            <li><a href="admin-business-product-management-traveller-product-rate.html">Product Rates</a></li>
                                                            <li><a href="admin-business-product-management-traveller-payment.html">Payment Method</a></li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                        <i class="fa fa-angle-right chevron-icon-sidebar3"></i>
                                                        Term Life
                                                        </a>
                                                        <ul class="submenu">
                                                            <li><a target="_parent" href="${pageContext.request.contextPath}/products/termLife/admin-business-product-management-term-product-infomation.jsp">Product Information</a></li>
                                                            <li><a href="admin-business-product-management-term-product-rate.html">Product Rates</a></li>
                                                            <li><a href="admin-business-product-management-term-payment.html">Payment Method</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                             <li><a href="business_admin/admin-business-list-member.jsp">List of Customer</a></li>
                                            <li><a href="business_admin/admin-business-segmentation.jsp">Segmentation</a></li>
                                            <li><a href="business_admin/admin-business-campaign-analysis.jsp">Campaign Analysis Setup</a></li>
                                            <li><a href="business_admin/admin-business-straight-through.jsp">Rules Management</a></li>
                                            <li><a href="business_admin/admin-business-discount.jsp">Discount Management</a></li>
                                       
                                        </ul>
                                    </li>
              
                                   -->
			<c:if
				test="${sessionScope.accessRight3 != 'NA' || sessionScope.accessRight4 != 'NA' || sessionScope.accessRight5 != 'NA'}">

				<li><a href="#"> <span class="icon-sidebar icon services"></span>
						<i class="fa fa-angle-right chevron-icon-sidebar"></i> Cyber Agent
						Management
				</a>
					<ul class="submenu">

						<%--  <li><a target="_parent" href="${pageContext.request.contextPath}/operator-view-list.jsp"> View Profile</a></li>   --%>
						<c:if test="${sessionScope.accessRight3 != 'NA'}">
							<li><a target="_parent" href="agentRegistration">Register
									Agent</a></li>
						</c:if>
						<c:if test="${sessionScope.accessRight4 != 'NA'}">
							<li><a target="_parent" href="showAgentProductMgm">Agent
									Product Management</a></li>
						</c:if>
						<c:if test="${sessionScope.accessRight5 != 'NA'}">
							<li><a target="_parent" href="listAgents"> Agent List</a></li>
						</c:if>
						<%-- <li><a target="_parent" href="${pageContext.request.contextPath}/operator-registration.jsp"> Register Operator</a></li> --%>
						<%-- <li><a target="_parent" href="${pageContext.request.contextPath}/operator-product-management.jsp">Operator Product Management</a></li>
                                            <li><a target="_parent" href="${pageContext.request.contextPath}/operator-view-list.jsp"> Operator List</a></li> --%>




					</ul></li>
			</c:if>

			<c:if test="${sessionScope.accessRight6 != 'NA'}">
				<li><a href="#"> <span class="icon-sidebar icon services"></span>
						<i class="fa fa-angle-right chevron-icon-sidebar"></i> Leads
						Management &amp; Campaign
				</a>
					<ul class="submenu">
						<li><a href="salesLeadsReport">Sales Leads</a></li>
						<li><a
							href="${pageContext.request.contextPath}/leadsAbandonmentRep.jsp">Abandonment
								Repository</a></li>
						<!--     <li><a href="lead-manual-upload.html">Manual Upload</a></li>
                                            <li><a href="lead-abandonment-repository.html">Abandonment Repository</a></li>
                                            <li><a href="lead-campaign-banner.html">Campaign Banner</a></li>
                                            <li><a href="lead-data-download.html">Leads Data Download</a></li>-->
					</ul></li>
			</c:if>

			<c:if
				test="${sessionScope.accessRight7 != 'NA' || sessionScope.accessRight8 != 'NA'}">
				<li><a href="#"> <span class="icon-sidebar icon policy"></span>
						<i class="fa fa-angle-right chevron-icon-sidebar"></i> Report
						&amp; Analytics
				</a>
					<ul class="submenu">
						<c:if test="${sessionScope.accessRight7 != 'NA'}">
							<li><a
								href="${pageContext.request.contextPath}/admin-report-agent.jsp">Agent
									Performance</a></li>
						</c:if>

						<c:if test="${sessionScope.accessRight8 != 'NA'}">
							<li><a
								href="${pageContext.request.contextPath}/admin-report-transaction.jsp">Transactional
									Report</a></li>
						</c:if>
					</ul></li>
			</c:if>
		</ul>
	</div>
	<!-- /.sidebar-left -->
</div>