<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<% java.text.DateFormat df=new java.text.SimpleDateFormat("dd/MM/yyyy"); %>



<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link
	href="<c:url value="/resources/assets/plugins/font-awesome/css/font-awesome.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/css/owl.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/css/bootstrap.css" />"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/css/bootstrap-select.css" />"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/plugins/datatable/css/bootstrap.datatable.min.css"/>"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/plugins/datatable/css/dataTables.searchHighlight.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/css/menu.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/plugins/all.min.css"/>"
	rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/assets/jAlert/jAlert.css"/>"
	rel="stylesheet">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>

</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1__.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">System Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>
																Roles To Modules Access <a href="#" data-toggle="modal"
																	data-backdrop="static" data-keyboard="false"
																	data-target="#createUserGroup">
																	<button class="btn btn-warning btn-default pull-right">
																		Create Role - Access <i class="fa fa-users"
																			aria-hidden="true"></i>
																	</button>
																</a>

															</h4>

														</div>
													</div>


													<!-- Start Form  search crietria by pramaiyan 25022018-->
													<form name="userGroup"
														action="${pageContext.request.contextPath}/uam/userRole"
														id="userGroup" method="post">

														<input type="hidden" name="action" value="search" />

														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-12">
																	<div class="form-horizontal">
																		<div class="form-group">

																			<div class="col-sm-4">
																				<label class="control-label">Module</label> <select
																					class="form-control" name="module">
																					<option value="" selected>-View All-</option>
																					<c:forEach items="${ModuleList}" var="element">
																						<option value="<c:out value="${element.id}"/>">
																							<c:out value="${element.name}" />
																						</option>
																					</c:forEach>
																				</select>
																			</div>

																			<div class="col-sm-4">
																				<label class="control-label">Role</label> <select
																					class="form-control" name="role">
																					<option value="" selected>-View All-</option>
																					<c:forEach items="${RoleList}" var="element">
																						<option value="<c:out value="${element.id}"/>">
																							<c:out value="${element.name}" />
																						</option>
																					</c:forEach>
																				</select>
																			</div>


																			<div class="col-sm-4">
																				<label class=" control-label">Status</label> <select
																					class="form-control" name="status">
																					<option value="" selected>-View All-</option>
																					<c:forEach items="${StatusList}" var="element">
																						<option
																							value="<c:out value="${element.paramCode}"/>">
																							<c:out value="${element.paramName}" />
																						</option>
																					</c:forEach>
																				</select>
																			</div>
																		</div>


																	</div>



																	<div class="col-sm-12 text-right">
																		<button type="submit" class="btn btn-default">Search</button>
																	</div>
																</div>
															</div>
														</div>
													</form>
													<!-- END FORM  End by pramaiyan 25022018 -->

												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="table-responsive" id="MyTable">
															<a href="generateMenuExcel"><button
																	class="btn btn-warning btn-sm">
																	Report <i class="fa fa-edit" aria-hidden="true"></i>
																</button></a>
															<table
																class="table table-striped table-warning table-hover"
																id="admin-datatable-second_admin">
																<thead>
																	<tr>
																		<th>No</th>
																		<th>Role Name</th>
																		<th>Module</th>
																		<th>Status</th>
																		<th>Created By</th>
																		<th>Created Date</th>
																		<th>Updated By</th>
																		<th>Updated Date</th>
																		<th>Action</th>
																	</tr>
																</thead>
																<tbody>

																	<c:forEach items="${RoleModuleList}" var="element"
																		varStatus="loop">
																		<tr>

																			<td align="center">${element.id}</td>
																			<td align="left">${element.roleName}</td>
																			<td align="left">${element.moduleName}</td>
																			<td align="center"><c:if
																					test="${element.status eq 1}">
																					<span class="label label-success">ACTIVE</span>
																				</c:if> <c:if test="${element.status eq 0}">
																					<span class="label label-danger">INACTIVE</span>
																				</c:if></td>
																			<td align="center">${element.createBy}</td>
																			<td align="center"><c:if
																					test="${not empty element.createDate}">
																					<fmt:formatDate value="${element.createDate}"
																						pattern="dd MMM yyyy hh:mm a" />
																				</c:if></td>
																			<td align="center">${element.updateBy}</td>
																			<td align="center"><c:if
																					test="${not empty element.updateDate}">
																					<fmt:formatDate value="${element.updateDate}"
																						pattern="dd MMM yyyy hh:mm a" />
																				</c:if></td>
																			<td align="center"><a
																				href="updateUserRole?id=${element.id}"><button
																						class="btn btn-warning btn-sm">
																						Edit <i class="fa fa-edit" aria-hidden="true"></i>
																					</button></a></td>
																		</tr>
																	</c:forEach>

																</tbody>
															</table>
														</div>
														<!-- /.table-responsive -->
													</div>
													<!-- /.the-box -->
												</div>
												<!-- End warning color table -->
											</div>
										</div>
									</div>
									<!--row -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->

	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->

	<!-- ********************************************CREATE GROUP****************************************** -->
	<!-- start: modal Create-->
	<div class="modal fade" id="createUserGroup" tabindex="-1"
		role="dialog" aria-labelledby="createUserGroupLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="reportDetailLabel">Create Role
						Access Controls</h4>
				</div>
				<div class="modal-body">

					<div class="box-body">
						<div class="content-wrapper">

							<section class="content">
								<div class="container">
									<div class="row">
										<div class="col-md-6">
											<!-- start by pramaiyan -->


											<form class="form-horizontal"
												action="${pageContext.request.contextPath}/uam/createUserRole"
												method="POST" id="createGroupProfileForm"
												name="createGroupProfileForm">
												<fieldset>
													<input type="hidden" name="status" id="status" value="1">

													<!--   <input type="hidden" name="totalModule" id="totalModule" value="${TotalModule}">-->

													<!-- Start Pramaiyan -->

													<div class="control-group">
														<label class="control-label" for="username">Module</label>
														<div class="controls">
															<select class="form-control" name="moduleDesc"
																id=moduleSelect>
																<option value="" selected>-Select Module-</option>
																<c:forEach items="${ModuleList}" var="element">
																	<option value="<c:out value="${element.id}"/>">
																		<c:out value="${element.name}" />
																	</option>
																</c:forEach>
															</select>
															<!--   <input type="text" id="name" name="name" placeholder="" class="form-control input-lg" required>-->
														</div>
													</div>
													<div class="control-group">
														<div class="controls">
															<div id="messageModule"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i>
																	Please select Module</strong>
															</div>
														</div>
													</div>


													<div class="control-group">
														<label class="control-label" for="username">Role
															Name </label>
														<div class="controls">
															<select class="form-control" name="role">
																<option value="" selected>-Select Role-</option>
																<c:forEach items="${RoleList}" var="element">
																	<option value="<c:out value="${element.id}"/>">
																		<c:out value="${element.name}" />
																	</option>
																</c:forEach>
															</select>
															<!--   <input type="text" id="name" name="name" placeholder="" class="form-control input-lg" required>-->
														</div>
													</div>
													<!-- End Pramaiyan -->

													<div class="control-group">
														<div class="controls">
															<div id="messageRole"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i>
																	Please select Role</strong>
															</div>
														</div>
													</div>
													<div class="control-group">
														<div class="controls">
															<div id="recordFound"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i> Role
																	Already Exist</strong>
															</div>
														</div>
													</div>

													<div class="control-group">
														<!-- Button -->
														<div class="controls">
															<!-- <input type="submit" class="btn btn-warning">&nbsp; -->
															<input type="submit" class="btn btn-warning"
																id="createRoleModuleBtn">&nbsp;
															<button class="btn btn-danger" data-dismiss="modal"
																aria-label="Close">Cancel</button>
														</div>
													</div>

												</fieldset>
											</form>
										</div>
									</div>

								</div>
						</div>
					</div>

					</section>



				</div>
				<div class="modal-footer">
					<!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</div>
	</div>


	<!-- end: modal Create-->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->



	<script type="text/javascript">
         	$(document).ready(function() {
         		
         		  var updatemessage="<c:out value="${updatemessage}"/>";
	 		       // show when the button is clicked
	 		       if (updatemessage.length) {
	 		    	// alert(updatemessage);
	 		    successAlert('Success!', updatemessage);
	 		       }  
	 		       
	 		   
	 		      var savemessage="<c:out value="${savemessage}"/>";
	 		       // show when the button is clicked
	 		       if (savemessage.length) {
	 		    	// alert(updatemessage);
	 		    successAlert('Success!', savemessage);
	 		       }  
         		
		        	 var t = $('#admin-datatable-second_admin').DataTable( {
	         			 
		         			dom: 'Bfrtip',
		         			  buttons: [	
		{
		    extend: 'pageLength',
		    className: 'btn btn-secondary btn-sm active'
		},
		{
            extend: 'excel',
            filename: 'Etiqa Group List',
            className: 'btn btn-warning btn-sm',
            text:'Export to XLS <i class="fa fa-download" aria-hidden="true"></i>',
            
           	 exportOptions: {
                    columns: [1,2,3,4,5,6,7]
                }
       }
		           
		         			  ],
		         		      "searching" :false, 
		         	          "columnDefs": [ {
		         	          "searchable": false,
		         	          "orderable": false,
		         	          "targets": [0,1,2,3,4,5,6],
		         	          "bSortable": false
		         	         
		         	        } ],
		         	        "order": [[ 0, 'asc' ]]
		         	    } );
		         	 
		         	    t.on( 'order.dt search.dt', function () {
		         	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		         	            cell.innerHTML = i+1;
		         	        } );
		         	    } ).draw();
		         	           	
		         	  
		         	  // t.buttons( 0, null ).containers().appendTo( '#btnhere' );
		         	   
		         	});
         	
         	
         	
         	$('#name').keyup(function() {
         	    if($(this).val() == ''){
         	       $('#messageName').show();
         	    }else{
         	       $('#messageName').hide();
         	       $('#recordFound').hide();
         	       
         	    }
         	});
         	 
         	
         	 $("#createRoleModuleBtn").on("click", function(event) { 
	         		$('#recordFound').hide();	          		
	          		$('#messageName').hide();	
                     $('#messageModule').hide();						
	          		
						event.preventDefault();
						$.ajax({
							url : 'validateCreateRoleModule',
							type : "POST",
							data : $("#createGroupProfileForm").serialize(),
							dataType : 'json',
							success : function(response) {
								var obj = JSON.stringify(response);
						    	 
						    	 var result = $.parseJSON(obj); 
								 
								 $.each(result, function() {		  			 
  
									if(result.recordFound == true && result.count !=-1){
									
										$('#recordFound').show();
									}
									
									if(result.requiredModule == true){
										
										$('#messageModule').show();
									}
									
									
									if(result.requiredRole == true){
										
										$('#"messageRole"').show();
									}
									 
                                     if(result.recordFound == false){  
										if(result.requiredModule == false){ 
											if(result.requiredRole ==false){ 
										$('#createGroupProfileForm').attr('action', 'createUserRole');
					                    $('#createGroupProfileForm').submit();
									}
										}
										
									} 
						  		  }); 
							},
							error : function(data, status, er) {
								//alert(data + "_" + status + "_" + er);
							}
						});
					});
					 
         	//to reset form after close modal
         $('.modal').on('hidden.bs.modal', function(){
         	    $(this).find('form')[0].reset();  
         	   //$('input:checkbox').removeAttr('checked');
        		//$('#messageName').hide();        		
        		
        		//$('#messageModule').hide();
        		//$('#messageNameUpdate').hide();
        		//$('#recordFoundUpdate').hide();
        		//$('#messageStatusUpdate').hide();
        		//$('#messageModuleUpdate').hide();
        		
         	}); 
         
         $('#createUserGroup').on('show.bs.modal', function (event) {        	
          	    $(this).find('form')[0].reset(); 
          	 	$('#recordFound').hide(); 
	          	$('#messageRole').hide();
	    		$('#messageModule').hide();
	    		$('#recordFound').hide();
         });
       	
       	 $('#updateUserGroup').on('show.bs.modal', function (event) {
       	 var modal = $(this);	
       	
       	 
       	var button = $(event.relatedTarget) // Button that triggered the modal
  	  var recordID = button.data('value') // Extract info from data-* attributes
  	  
     	
  	 $.ajax({  
		     type : "Get",   
		     url : "userGroupDetail", //see in UserController.java
		     data : "id=" + recordID, 
		   dataType: "json", //set to JSON   
		     success : function(response) {
		    	var obj = JSON.stringify(response);
		    	 console.log(response);
		
		
		    	 var result = $.parseJSON(obj);
		  		  $.each(result, function() {		  			 

		  			// modal.find('.modal-title').text("Transaction Record : "+this['mptransactionid'] );
		  	    	  //modal.find('.modal-body input').val(result.name);
		  	    	  modal.find('#name').val(result.name);
		  	    	  modal.find('.modal-body select').val(result.status);
		  	    	  modal.find('#id').val(result.id);	  
		  	    	  
		  	    
		  	    	for (var i=0;i<result.count;i++){
		  	    		//checkbox checked based on result
		  	    		$('input[type=checkbox][name="accessRight'+result.moduleID[i]+'"]').filter('[value='+result.accessRight[i]+']').prop('checked', true);		  	    		  	    		
		  	    		
		  	    	}
		  	    	
		  	    	
		  		  });
		     },  
		     error : function(e) {  
		      alert('Error: ' + e);   
		     }  
		    });  	
	  

  	  
  	  
  	  
  	  
       	 
       	})
       	 
       	// to allow only one checkbox checked
       		$('input[type="checkbox"]').on('change', function() {
       	    $('input[name="' + this.name + '"]').not(this).prop('checked', false);
       	});
       	 
      	//Start by Pramaiyan 
    	/*  $("#moduleSelect").on("change", function(event) { 
					event.preventDefault();
					$.ajax({
						url : 'fetchPermissionDesc',
						type : "POST",
						data : $("#createGroupProfileForm").serialize(),
						dataType : 'json',
						success : function(response) {
							alert("ok");
							 var obj = JSON.stringify(response);
					    	 console.log(response);
					    	 var result = $.parseJSON(obj);	 
							 $.each(result, function() {		  			 
								console.log(result.message);
                              if(result.recordFound == false){
									if(result.requiredName == false){
										if(result.requiredModule ==false){
									$('#createGroupProfileForm').attr('action', 'createUserGroupProfileAction');
				                    $('#createGroupProfileForm').submit();
								}
									}
								}  
					  		  });
						},
						error : function(data, status, er) {
							 
						}
					});
				}); */
		 //End By Pramaiyan
		 
        </script>



</body>
</html>