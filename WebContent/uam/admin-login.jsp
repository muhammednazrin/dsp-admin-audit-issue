
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<!--<link href="<c:url value="/resources/assets/css/bootstrap.min.css"/> rel="stylesheet">-->

<!-- PLUGINS CSS -->

<link
	href="<c:url value="/resources/assets/plugins/owl-carousel/owl.carousel.min.css"/>"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/plugins/owl-carousel/owl.theme.min.css"/>"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/plugins/owl-carousel/owl.transitions.min.css"/>"
	rel="stylesheet">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link
	href="<c:url value="/resources/assets/plugins/font-awesome/css/font-awesome.min.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/css/owl.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/css/bootstrap.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/css/bootstrap-select.css"/>"
	rel="stylesheet">
<%-- <link href="assets/css/test.css" rel="stylesheet">
        <link href="<c:url value="resources/assets/css/style.css" rel="stylesheet"> --%>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="assets/assets/libs/html5shiv.js"></script>
        <script src="assets/assets/libs/respond.min.js"></script>
        <![endif]-->
</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper dashboard">
		<!-- header second-->
		<div class="header-admin">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h2>Administration</h2>
					</div>
				</div>
			</div>
		</div>
		<!-- header second-->


		<!-- BEGIN PAGE CONTENT -->
		<div class="page-content">
			<div class="container">
				<!-- /.container-->
				<div class="row">
					<div class="col-sm-offset-1 col-sm-9">
						<div class="panel panel-default login">
							<div class="panel-heading">
								<h4>Etiqa Administration Login (UAM)</h4>
							</div>
							<div class="panel-body">
								<div class="col-sm-6">
									<div class="text-center">
										<img width="115"
											src="${pageContext.request.contextPath}/resources/assets/img/lock.png"
											alt="logo">
										<div class="strong">
											<h5>Use a valid PF Number and Password to gain access to
												the Administrator Back-end.</h5>
										</div>

									</div>
								</div>
								<c:if test="${not empty alertMessage}">
									<div class="col-sm-6" style="color: red">
										<c:out value="${alertMessage} " />
									</div>
								</c:if>
								<div class="col-sm-6">

									<form action="${pageContext.request.contextPath}/doLogin"
										method="post">
										<div class="panel panel-default">
											<div class="panel-body">
												<div class="col-sm-12">
													<div class="logo form-group">
														<a href="index.html"><img width="80"
															src="${pageContext.request.contextPath}/resources/assets/img/logo.png"
															alt="logo"></a>
													</div>
												</div>

												<div class="col-xs-12">
													<div
														class="form-group has-feedback lg left-feedback no-label">
														<input type="text" class="form-control no-border rounded"
															name="username" placeholder="PF Number" autofocus>
														<span class="fa fa-user form-control-feedback fa-lg"></span>
													</div>
												</div>
												<div class="col-xs-12">
													<div
														class="form-group has-feedback lg left-feedback no-label">
														<input type="password"
															class="form-control no-border rounded" name="password"
															placeholder="Password"> <span
															class="fa fa-unlock-alt form-control-feedback fa-lg"></span>
													</div>
												</div>
												<div class="col-xs-12">
													<div
														class="form-group has-feedback lg left-feedback no-label"
														id="result">
														<%
                                                    if (request.getParameter("result")!=null) {
                                                  %>
														<span id="LoginError" class="error">Invalid
															Credentials. Please Verify PF No and Password</span>
														<%
                                                    }
                                                  %>
													</div>
													<div class="text-right">
														<input type="submit" class="btn btn-warning" value="Login">

													</div>
													<div class="text-right">
														<a
															href="${pageContext.request.contextPath}/uam/forgotPassword">Forgot
															Password</a>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						<!--
                            <div class="the-box">
                                <div>Etiqa Customer Web Portal! Administration Login</div>
                                <div class="col-sm-6">asdas</div>
                                <div class="col-sm-6">
                                    <div class="the-box">asdasd</div>
                                </div>
                            </div>
-->
					</div>
				</div>
				<!-- /.row -->
				<h6 class="text-center">Use of this system is restricted to
					individuals and activities authorized by management of Maybank
					Group. Unauthorized use may result in appropriate disciplinary
					action and/or legal prosecution.</h6>
			</div>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->

	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->




	<script type="text/javascript" language="javascript">   
			function disableBackButton()
			{
			window.history.forward()
			}  
			disableBackButton();  
			window.onload=disableBackButton();  
			window.onpageshow=function(evt) { if(evt.persisted) disableBackButton() }  
			window.onunload=function() { void(0) }  
			if(location.href!="https://www.etiqa.com.my/MyAccount/login"){
			history.pushState(null, document.title, location.href);
			window.addEventListener('popstate', function (event)
			{
			history.pushState(null, document.title, location.href);
			});
			}



</script>

</body>
</html>