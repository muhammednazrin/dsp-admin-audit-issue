<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="java.io.*,java.util.*"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="javax.servlet.*,java.text.*"%>
<%@ page session="true"%>
<%--  <%if (session.getAttribute("user") == null) {  response.sendRedirect(request.getContextPath() + "/admin-login.jsp"); }%> --%>

<!DOCTYPE html>
<html lang="en">
<head>
<style type="text/css">
.cc-selector input {
	margin: 0;
	padding: 0;
	-webkit-appearance: none;
	-moz-appearance: none;
	appearance: none;
}

.payment-selector {
	cursor: pointer;
	background-size: contain;
	background-repeat: no-repeat;
	display: inline-block;
	width: 160px;
	height: 160px;
	-webkit-transition: all 100ms ease-in;
	-moz-transition: all 100ms ease-in;
	transition: all 100ms ease-in;
	background-color: #FFBF00;
	border-radius: 50%;
}

.payment-selector:hover {
	-webkit-filter: opacity(1);
	-moz-filter: opacity(1);
	filter: opacity(1);
	color: white;
}

input[type="radio"] {
	display: none;
}

input[type="radio"]+label {
	color: #333;
	font-weight: bold;
	font-size: 18px;
	padding: 6rem 0;
}

.icon-label {
	background-position: center;
}

.p-content {
	display: inline-block;
	position: relative;
	vertical-align: top;
	font-size: 24px;
	font-weight: normal;
	color: #333;
}

.cc-selector.car-content p {
	font-size: 18px;
	text-align: center;
	position: absolute;
	left: 50%;
	width: 100%;
	transform: translate(-50%, -50%);
	-webkit-transform: translate(-50%, -50%);
	top: 50%;
}

.cc-selector-3 input:active+.payment-selector, .cc-selector-2 input:active+.payment-selector,
	.cc-selector input:active+.payment-selector {
	opacity: .9;
}

.cc-selector-3 input:checked+.payment-selector, .cc-selector-2 input:checked+.payment-selector,
	.cc-selector input:checked+.payment-selector {
	-webkit-filter: none;
	-moz-filter: none;
	filter: none;
	color: white;
}

.btn-yellow {
	color: #ffffff !important;
	background: #FFBF00;
	border-radius: 3px;
}

.space-top-2x {
	margin-top: 2rem;
}

.space-bottom {
	margin-bottom: 1rem;
}

.white {
	color: white;
}

a.logout {
	color: white;
}

a.logout:hover {
	color: white;
	text-decoration: underline;
}
</style>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>


</head>
<body>
	<%
		 String fn = "";
		/* if (session != null) { */
			if (session.getAttribute("user") != null) {
				String name = (String) session.getAttribute("user");
				System.out.println("from page header " + name);
				fn = (String) session.getAttribute("FullName");
			} else {
				response.sendRedirect("/admin-login.jsp");
				
			}
		/* }  */		
	  %>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper dashboard">
		<!-- header second-->
		<div class="header-admin">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<h2>Administration</h2>
					</div>
					<div class="col-sm-6 text-right white">
						<p align="right" valign="top">
							Welcome! <b><%=fn%></b> <b>(</b><a
								href="${pageContext.request.contextPath}/doLogout"
								class="logout">Logout</a><b>)</b>
						</p>
						<h2>
							Welcome! ,<%=session.getAttribute("userloginName")%></h2>
						<h5 class="white">
							Last Login on :
							<%=session.getAttribute("lastLoginTime")%></h5>
					</div>
				</div>
			</div>
		</div>
		<!-- header second-->


		<!-- BEGIN PAGE CONTENT -->
		<div class="page-content">
			<div class="container">
				<!-- /.container-->
				<div class="row">
					<div class="col-sm-offset-1 col-sm-9">
						<div class="panel panel-default login">
							<div class="panel-heading">
								<h4>Modules</h4>
							</div>
							<div class="panel-body">
								<form class="cc-selector car-content text-center"
									action="${pageContext.request.contextPath}/submitModule"
									id="moduleForm">
									<div class="row">
										<c:forEach var="paramItem1"
											items="${sessionScope.roleModule_all}">
											<div class="p-content col-sm-3">
												<input id="operation<c:out value='${paramItem1.moduleId}'/>"
													name="operation"
													value="<c:out value='${paramItem1.moduleId}'/>$<c:out value='${paramItem1.moduleURL}'/>"
													type="radio"> <label
													class="payment-selector uam-icon icon-label"
													for="operation<c:out value='${paramItem1.moduleId}'/>">
													<c:out value='${paramItem1.moduleName}' />
												</label>
											</div>
										</c:forEach>

									</div>
									<!--    <div class="row">
                                    <div class="col-sm-12 text-center space-top-2x space-bottom">
                                      <button  id="reset" class="btn btn-yellow2">Reset</button>
                                      <a href="javascript:void(0);" id="submit" type="button" class="btn btn-yellow">Next</a>
                                    </div> </div>  -->
								</form>


							</div>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</div>
		<!-- /.page-content -->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->
	<jsp:include page="pageFooter.jsp" />

	<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->

	

	<script src="<c:url value="/resources/assets/js/jquery.min.js"/>"
		type="text/javascript"></script>
	<script type="text/javascript">
        $("input[name='operation']").click(function(){
            $(this).closest("form").submit();
        
         
      
         
         
           
          });
        </script>
</body>
</html>