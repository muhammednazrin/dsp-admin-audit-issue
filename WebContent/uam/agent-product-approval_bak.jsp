<%@ page import="java.util.List"%>
<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<% java.text.DateFormat df=new java.text.SimpleDateFormat("dd/MM/yyyy"); %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

.mb-3 {
	margin-bottom: 1rem;
}

.mb-5 {
	margin-bottom: 5rem;
}
</style>
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>
</head>
<body>
	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">System Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-12 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Agent Product Approver</h4>
														</div>
													</div>
													<!-- Start Form -->
													<section class="content bg-white">
														<div class="row">
															<div class="col-md-6 mb-3">
																<h4 class="text-center">Original</h4>
																<table
																	class="table table-striped table-warning table-hover table-outline">
																	<tbody align="center">
																		<tr>
																			<td align="left"><strong>Product</strong></td>
																			<td><strong>Type</strong></td>
																			<td><strong>Entity</strong></td>
																			<td><strong>Agent Code</strong></td>
																			<td><strong>Discount (%)</strong></td>
																			<td><strong>Commission (%)</strong></td>
																		</tr>
																		<tr>
																			<td align="left">Ezy Secure</td>
																			<td>Direct</td>
																			<td>EIB</td>
																			<td>123456</td>
																			<td>10</td>
																			<td>5</td>
																		</tr>
																		<tr>
																			<td align="left">Motor Insurance Detariff</td>
																			<td>Non Direct</td>
																			<td>EIB</td>
																			<td>000211</td>
																			<td>5</td>
																			<td>2</td>
																		</tr>
																		<tr>
																			<td align="left">New Car Insurance</td>
																			<td>Direct</td>
																			<td>EIB</td>
																			<td>000222</td>
																			<td>10</td>
																			<td>5</td>
																		</tr>
																		<tr>
																			<td align="left">Houseowner Householder</td>
																			<td>Direct</td>
																			<td>EIB</td>
																			<td>000111</td>
																			<td>10</td>
																			<td>5</td>
																		</tr>
																		<tr>
																			<td align="left">Ezy Life Secure</td>
																			<td>Non Direct</td>
																			<td>EIB</td>
																			<td>000112</td>
																			<td>15</td>
																			<td>5</td>
																		</tr>
																		<tr>
																			<td align="left">Motor Insurance</td>
																			<td>Direct</td>
																			<td>EIB</td>
																			<td>000222</td>
																			<td>10</td>
																			<td>5</td>
																		</tr>
																		<tr>
																			<td align="left">World Traveller Care</td>
																			<td>Non Direct</td>
																			<td>EIB</td>
																			<td>000333</td>
																			<td>20</td>
																			<td>8</td>
																		</tr>
																		<tr>
																			<td align="left">CAPS</td>
																			<td>Non Direct</td>
																			<td>EIB</td>
																			<td>000444</td>
																			<td>10</td>
																			<td>5</td>
																		</tr>
																		<tr>
																			<td align="left">HOHH Takaful</td>
																			<td>Non Direct</td>
																			<td>ETB</td>
																			<td>000555</td>
																			<td>20</td>
																			<td>10</td>
																		</tr>
																		<tr>
																			<td align="left">DPPA</td>
																			<td>Direct</td>
																			<td>ETB</td>
																			<td>000666</td>
																			<td>10</td>
																			<td>5</td>
																		</tr>
																		<tr>
																			<td align="left">Motor Takaful</td>
																			<td>Direct</td>
																			<td>ETB</td>
																			<td>000777</td>
																			<td>10</td>
																			<td>5</td>
																		</tr>
																		<tr>
																			<td align="left">WTC Takaful</td>
																			<td>Direct</td>
																			<td>ETB</td>
																			<td>000888</td>
																			<td>10</td>
																			<td>5</td>
																		</tr>
																		<tr>
																			<td align="left">i Secure</td>
																			<td>Non Direct</td>
																			<td>ETB</td>
																			<td>000999</td>
																			<td>10</td>
																			<td>5</td>
																		</tr>
																		<tr>
																			<td align="left">New Car Takaful</td>
																			<td>Non Direct</td>
																			<td>ETB</td>
																			<td>000998</td>
																			<td>10</td>
																			<td>5</td>
																		</tr>
																		<tr>
																			<td align="left">i Double Secure</td>
																			<td>Non Direct</td>
																			<td>ETB</td>
																			<td>000988</td>
																			<td>10</td>
																			<td>5</td>
																		</tr>
																	</tbody>
																</table>
															</div>
															<div class="col-md-6 mb-3">
																<h4 class="text-center">Changes</h4>
																<table
																	class="table table-striped table-warning table-hover table-outline">
																	<tbody align="center">
																		<tr>
																			<td align="left"><strong>Product</strong></td>
																			<td><strong>Type</strong></td>
																			<td><strong>Entity</strong></td>
																			<td><strong>Agent Code</strong></td>
																			<td><strong>Discount (%)</strong></td>
																			<td><strong>Commission (%)</strong></td>
																		</tr>
																		<tr>
																			<td align="left">Ezy Secure</td>
																			<td>Direct</td>
																			<td>EIB</td>
																			<td>123456</td>
																			<td>20</td>
																			<td>10</td>
																		</tr>
																		<tr>
																			<td align="left">Motor Insurance Detariff</td>
																			<td>Direct</td>
																			<td>EIB</td>
																			<td>001211</td>
																			<td>5</td>
																			<td>2</td>
																		</tr>
																		<tr>
																			<td align="left">New Car Insurance</td>
																			<td>Direct</td>
																			<td>EIB</td>
																			<td>000222</td>
																			<td>14</td>
																			<td>6</td>
																		</tr>
																		<tr>
																			<td align="left">Houseowner Householder</td>
																			<td>Non Direct</td>
																			<td>EIB</td>
																			<td>001111</td>
																			<td>10</td>
																			<td>5</td>
																		</tr>
																		<tr>
																			<td align="left">Ezy Life Secure</td>
																			<td>Direct</td>
																			<td>EIB</td>
																			<td>001112</td>
																			<td>15</td>
																			<td>5</td>
																		</tr>
																		<tr>
																			<td align="left">Motor Insurance</td>
																			<td>Direct</td>
																			<td>EIB</td>
																			<td>001222</td>
																			<td>10</td>
																			<td>5</td>
																		</tr>
																		<tr>
																			<td align="left">World Traveller Care</td>
																			<td>Non Direct</td>
																			<td>EIB</td>
																			<td>001333</td>
																			<td>20</td>
																			<td>10</td>
																		</tr>
																		<tr>
																			<td align="left">CAPS</td>
																			<td>Non Direct</td>
																			<td>EIB</td>
																			<td>001444</td>
																			<td>10</td>
																			<td>5</td>
																		</tr>
																		<tr>
																			<td align="left">HOHH Takaful</td>
																			<td>Direct</td>
																			<td>ETB</td>
																			<td>000555</td>
																			<td>20</td>
																			<td>10</td>
																		</tr>
																		<tr>
																			<td align="left">DPPA</td>
																			<td>Direct</td>
																			<td>ETB</td>
																			<td>000666</td>
																			<td>10</td>
																			<td>6</td>
																		</tr>
																		<tr>
																			<td align="left">Motor Takaful</td>
																			<td>Non Direct</td>
																			<td>ETB</td>
																			<td>000777</td>
																			<td>10</td>
																			<td>2</td>
																		</tr>
																		<tr>
																			<td align="left">WTC Takaful</td>
																			<td>Non Direct</td>
																			<td>ETB</td>
																			<td>000888</td>
																			<td>10</td>
																			<td>5</td>
																		</tr>
																		<tr>
																			<td align="left">i Secure</td>
																			<td>Non Direct</td>
																			<td>ETB</td>
																			<td>000999</td>
																			<td>14</td>
																			<td>5</td>
																		</tr>
																		<tr>
																			<td align="left">New Car Takaful</td>
																			<td>Non Direct</td>
																			<td>ETB</td>
																			<td>000998</td>
																			<td>12</td>
																			<td>5</td>
																		</tr>
																		<tr>
																			<td align="left">i Double Secure</td>
																			<td>Non Direct</td>
																			<td>ETB</td>
																			<td>000988</td>
																			<td>11</td>
																			<td>5</td>
																		</tr>
																	</tbody>
																</table>
															</div>
														</div>
														<div class="row">
															<div class="col-sm-12 text-center mb-5">
																<div class="controls">
																	<button class="btn btn-success">Approve</button>
																	&nbsp;&nbsp;
																	<button class="btn btn-danger" data-dismiss="modal"
																		aria-label="Close">Reject</button>
																</div>
															</div>
														</div>
													</section>
													<!-- END FORM -->
												</div>

											</div>
										</div>
									</div>
									<!--row -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->
	<!-- ********************************************CREATE GROUP****************************************** -->
	<!-- start: modal Create-->
	<div class="modal fade" id="createUserGroup" tabindex="-1"
		role="dialog" aria-labelledby="createUserGroupLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="reportDetailLabel">Create User
						Role</h4>
				</div>
				<div class="modal-body">
					<div class="box-body">
						<div class="content-wrapper">
							<section class="content">
								<div class="container">
									<div class="row">
										<div class="col-md-6">
											<form class="form-horizontal"
												action="${pageContext.request.contextPath}/uam/createUserGroupProfileAction"
												method="POST" id="createGroupProfileForm"
												name="createGroupProfileForm">
												<fieldset>
													<input type="hidden" name="status" id="status" value="1">
													<input type="hidden" name="totalModule" id="totalModule"
														value="${TotalModule}">
													<div class="control-group mb-3">
														<label class="control-label" for="username">Module</label>
														<div class="controls">
															<select id="moduleselect" class="form-control" required>
																<option>CWP</option>
																<option>DSP</option>
																<option>BancaPA</option>
																<option>Admin</option>
															</select>
														</div>
													</div>
													<div class="control-group mb-3">
														<label class="control-label" for="username">Role
															Name</label>
														<div class="controls">
															<select id="moduleselect" class="form-control" required>
																<option>Super Admin</option>
																<option>Admin</option>
																<option>support</option>
																<option>Content author</option>
															</select>
														</div>
													</div>
													<div class="control-group">
														<div class="controls">
															<div id="recordFound"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i> Role
																	Already Exist</strong>
															</div>
														</div>
													</div>
													<div class="control-group">
														<div class="controls">
															<div id="messageName"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i>
																	Please enter Role Name</strong>
															</div>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="username">Module
															Access</label>
														<div class="controls">&nbsp;</div>
													</div>
													<div class="control-group">
														<div class="controls">
															<table
																class="table table-striped table-warning table-hover"
																id="admin-datatable-second_admin">
																<thead>
																	<tr>
																		<th align="center">Module</th>
																		<th align="center">Full Access</th>
																		<th align="center">Read Only</th>
																		<th align="center">No Access</th>
																	</tr>
																</thead>
																<tbody>
																	<c:forEach var="paramItem" items="${ModuleMainList}"
																		varStatus="loop">
																		<c:set var="sublevel1" value="0" />
																		<c:set var="sublevel2" value="0" />
																		<c:set var="sublevel3" value="0" />
																		<tr>
																			<td>
																				<div>
																					<b>${paramItem.name}</b>
																				</div> <c:forEach var="subModule" items="${ModuleList}">
																					<c:if test="${subModule.parentId==paramItem.id}">
																						<div>
																							<ul style="list-style-type: square">
																								<li>${subModule.name}</li>
																							</ul>
																						</div>
																						<c:forEach var="subModule2" items="${ModuleList}">
																							<c:if test="${subModule2.parentId==subModule.id}">
																								<div>
																									<ul>
																										<li style="list-style-type: none"><ul
																												style="list-style-type: disc">
																												<li>${subModule2.name}</li>
																											</ul></li>
																									</ul>
																								</div>
																							</c:if>
																						</c:forEach>
																					</c:if>
																				</c:forEach>
																			</td>
																			<!-- start display check box  -->
																			<td align="center"><c:forEach var="subModule"
																					items="${ModuleList}">
																					<c:if test="${subModule.parentId==paramItem.id}">
																						<c:set var="sublevel1"
																							value="${subModule.parentId}" />
																						<!-- start: to configure div top of sub menu -->
																						<c:set var="tmpVal" value="0" />
																						<c:forEach var="subModule2" items="${ModuleList}">
																							<c:if
																								test="${subModule2.parentId==subModule.id && tmpVal == '0'}">
																								<div>&nbsp;</div>
																								<div>&nbsp;</div>
																								<c:set var="tmpVal" value="1" />
																							</c:if>
																						</c:forEach>
																						<!-- end: to configure div top of sub menu -->
																						<c:forEach var="subModule2" items="${ModuleList}">
																							<c:if test="${subModule2.parentId==subModule.id}">
																								<c:set var="sublevel2"
																									value="${subModule2.parentId}" />
																								<input type="hidden"
																									name="checkboxFlag<c:out value="${subModule2.id}"/>"
																									value="1">
																								<div class="checkbox">
																									<input type="checkbox" value="FA"
																										name="accessRight<c:out value="${subModule2.id}"/>"
																										id="accessRight<c:out value="${subModule2.id}"/>">
																								</div>
																							</c:if>
																						</c:forEach>
																						<c:if test="${sublevel1 !='0' && sublevel2=='0'}">
																							<input type="hidden"
																								name="checkboxFlag<c:out value="${subModule.id}"/>"
																								value="1">
																							<div class="checkbox">
																								<input type="checkbox" value="FA"
																									name="accessRight<c:out value="${subModule.id}"/>"
																									id="accessRight<c:out value="${subModule.id}"/>">
																							</div>
																						</c:if>
																						<c:if
																							test="${sublevel1 !='0' && sublevel2!='0' &&  sublevel2!=subModule.id}">
																							<input type="hidden"
																								name="checkboxFlag<c:out value="${subModule.id}"/>"
																								value="1">
																							<div class="checkbox">
																								<input type="checkbox" value="FA"
																									name="accessRight<c:out value="${subModule.id}"/>"
																									id="accessRight<c:out value="${subModule.id}"/>">
																							</div>
																						</c:if>
																					</c:if>
																				</c:forEach> <c:if test="${sublevel1 =='0'}">
																					<input type="hidden"
																						name="checkboxFlag<c:out value="${paramItem.id}"/>"
																						value="1">
																					<div class="checkbox">
																						<input type="checkbox" value="FA"
																							name="accessRight<c:out value="${paramItem.id}"/>"
																							id="accessRight<c:out value="${paramItem.id}"/>">
																					</div>
																				</c:if></td>
																			<td align="center"><c:forEach var="subModule"
																					items="${ModuleList}">
																					<c:if test="${subModule.parentId==paramItem.id}">
																						<c:set var="sublevel1"
																							value="${subModule.parentId}" />
																						<!-- start: to configure div top of sub menu -->
																						<c:set var="tmpVal" value="0" />
																						<c:forEach var="subModule2" items="${ModuleList}">
																							<c:if
																								test="${subModule2.parentId==subModule.id && tmpVal == '0'}">
																								<div>&nbsp;</div>
																								<div>&nbsp;</div>
																								<c:set var="tmpVal" value="1" />
																							</c:if>
																						</c:forEach>
																						<!-- end: to configure div top of sub menu -->
																						<c:forEach var="subModule2" items="${ModuleList}">
																							<c:if test="${subModule2.parentId==subModule.id}">
																								<c:set var="sublevel2"
																									value="${subModule2.parentId}" />
																								<input type="hidden"
																									name="checkboxFlag<c:out value="${subModule2.id}"/>"
																									value="1">
																								<div class="checkbox">
																									<input type="checkbox" value="RO"
																										name="accessRight<c:out value="${subModule2.id}"/>"
																										id="accessRight<c:out value="${subModule2.id}"/>">
																								</div>
																							</c:if>
																						</c:forEach>
																						<c:if test="${sublevel1 !='0' && sublevel2=='0'}">
																							<input type="hidden"
																								name="checkboxFlag<c:out value="${subModule.id}"/>"
																								value="1">
																							<div class="checkbox">
																								<input type="checkbox" value="RO"
																									name="accessRight<c:out value="${subModule.id}"/>"
																									id="accessRight<c:out value="${subModule.id}"/>">
																							</div>
																						</c:if>
																						<c:if
																							test="${sublevel1 !='0' && sublevel2!='0' &&  sublevel2!=subModule.id}">
																							<input type="hidden"
																								name="checkboxFlag<c:out value="${subModule.id}"/>"
																								value="1">
																							<div class="checkbox">
																								<input type="checkbox" value="RO"
																									name="accessRight<c:out value="${subModule.id}"/>"
																									id="accessRight<c:out value="${subModule.id}"/>">
																							</div>
																						</c:if>
																					</c:if>
																				</c:forEach> <c:if test="${sublevel1 =='0'}">
																					<input type="hidden"
																						name="checkboxFlag<c:out value="${paramItem.id}"/>"
																						value="1">
																					<div class="checkbox">
																						<input type="checkbox" value="RO"
																							name="accessRight<c:out value="${paramItem.id}"/>"
																							id="accessRight<c:out value="${paramItem.id}"/>">
																					</div>
																				</c:if></td>
																			<td align="center"><c:forEach var="subModule"
																					items="${ModuleList}">
																					<c:if test="${subModule.parentId==paramItem.id}">
																						<c:set var="sublevel1"
																							value="${subModule.parentId}" />
																						<!-- start: to configure div top of sub menu -->
																						<c:set var="tmpVal" value="0" />
																						<c:forEach var="subModule2" items="${ModuleList}">
																							<c:if
																								test="${subModule2.parentId==subModule.id && tmpVal == '0'}">
																								<div>&nbsp;</div>
																								<div>&nbsp;</div>
																								<c:set var="tmpVal" value="1" />
																							</c:if>
																						</c:forEach>
																						<!-- end: to configure div top of sub menu -->
																						<c:forEach var="subModule2" items="${ModuleList}">
																							<c:if test="${subModule2.parentId==subModule.id}">
																								<c:set var="sublevel2"
																									value="${subModule2.parentId}" />
																								<input type="hidden"
																									name="checkboxFlag<c:out value="${subModule2.id}"/>"
																									value="1">
																								<div class="checkbox">
																									<input type="checkbox" value="NA"
																										name="accessRight<c:out value="${subModule2.id}"/>"
																										id="accessRight<c:out value="${subModule2.id}"/>">
																								</div>
																							</c:if>
																						</c:forEach>
																						<c:if test="${sublevel1 !='0' && sublevel2=='0'}">
																							<input type="hidden"
																								name="checkboxFlag<c:out value="${subModule.id}"/>"
																								value="1">
																							<div class="checkbox">
																								<input type="checkbox" value="NA"
																									name="accessRight<c:out value="${subModule.id}"/>"
																									id="accessRight<c:out value="${subModule.id}"/>">
																							</div>
																						</c:if>
																						<c:if
																							test="${sublevel1 !='0' && sublevel2!='0' &&  sublevel2!=subModule.id}">
																							<input type="hidden"
																								name="checkboxFlag<c:out value="${subModule.id}"/>"
																								value="1">
																							<div class="checkbox">
																								<input type="checkbox" value="NA"
																									name="accessRight<c:out value="${subModule.id}"/>"
																									id="accessRight<c:out value="${subModule.id}"/>">
																							</div>
																						</c:if>
																					</c:if>
																				</c:forEach> <c:if test="${sublevel1 =='0'}">
																					<input type="hidden"
																						name="checkboxFlag<c:out value="${paramItem.id}"/>"
																						value="1">
																					<div class="checkbox">
																						<input type="checkbox" value="NA"
																							name="accessRight<c:out value="${paramItem.id}"/>"
																							id="accessRight<c:out value="${paramItem.id}"/>">
																					</div>
																				</c:if></td>
																		</tr>
																	</c:forEach>
																</tbody>
															</table>
														</div>
													</div>
													<div class="control-group">
														<div class="controls">
															<div id="messageModule"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i>
																	Please select module access</strong>
															</div>
														</div>
													</div>
													<div class="control-group">
														<div class="controls">&nbsp;</div>
													</div>
													<div class="control-group">
														<!-- Button -->
														<div class="controls">
															<button class="btn btn-warning"
																id="createGroupProfileBtn">Create User Role</button>
															&nbsp;
															<button class="btn btn-danger" data-dismiss="modal"
																aria-label="Close">Cancel</button>
														</div>
													</div>
												</fieldset>
											</form>
										</div>
									</div>
								</div>
							</section>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<div class="modal-footer">
					<!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</div>
	</div>
	<!-- end: modal Create-->
	<!-- ********************************************UPDATE GROUP****************************************** -->
	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->
	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->
	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->
	<script type="text/javascript">
          $(document).ready(function() {
               
              });
          $('#name').keyup(function() {
              if($(this).val() == ''){
                 $('#messageName').show();
              }else{
                 $('#messageName').hide();
                 $('#recordFound').hide();
              }
          });
          
        
        </script>
</body>
</html>