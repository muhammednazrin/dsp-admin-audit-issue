<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<% java.text.DateFormat df=new java.text.SimpleDateFormat("dd/MM/yyyy"); %>


<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">



<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link
	href="<c:url value="/resources/assets/css/bootstrap.min.css"/> rel="stylesheet">

<!-- PLUGINS CSS -->

<link
	href="<c:url value="/resources/assets/plugins/owl-carousel/owl.carousel.min.css"/>"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/plugins/owl-carousel/owl.theme.min.css"/>"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/plugins/owl-carousel/owl.transitions.min.css"/>"
	rel="stylesheet">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link
	href="<c:url value="/resources/assets/plugins/font-awesome/css/font-awesome.min.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/css/owl.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/css/bootstrap.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/css/bootstrap-select.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/jAlert/jAlert.css"/>"
	rel="stylesheet">


<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>

</head>

<body>


	<!--
	
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1__.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">System Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>
																User Profile <a href="#" data-toggle="modal"
																	data-backdrop="static" data-keyboard="false"
																	data-target="#createUser"><button
																		class="btn btn-warning btn-default pull-right">
																		Create User Profile <i class="fa fa-users"
																			aria-hidden="true"></i>
																	</button></a>
															</h4>

														</div>
													</div>


													<!-- Start Form -->
													<form name="userProfile"
														action="${pageContext.request.contextPath}/uam/userProfile"
														id="userProfile" method="post">

														<input type="hidden" name="action" value="search" />

														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">PF
																				Number</label>
																			<div class="col-sm-9">
																				<input type="text" name="pfNumber" maxlength="50"
																					placeholder="" class="form-control">
																			</div>
																		</div>

																	</div>
																</div>

																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Role</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="selectroleID">
																					<option value="" selected>-View All-</option>
																					<c:forEach items="${UserRoleSearch}" var="element">
																						<option value="<c:out value="${element.id}"/>">
																							<c:out value="${element.name}" />
																						</option>
																					</c:forEach>
																				</select>
																			</div>
																		</div>




																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Name</label>
																			<div class="col-sm-9">
																				<input type="text" name="name" maxlength="50"
																					placeholder="" class="form-control">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Status</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="status">
																					<option value="" selected>-View All-</option>
																					<c:forEach items="${StatusList}" var="element">
																						<option
																							value="<c:out value="${element.paramCode}"/>">
																							<c:out value="${element.paramName}" />
																						</option>
																					</c:forEach>
																				</select>
																			</div>

																		</div>





																	</div>
																</div>
																<div class="col-sm-12 text-right">
																	<button type="submit" class="btn btn-default">Search</button>
																</div>
															</div>
														</div>
													</form>
													<!-- END FORM -->

												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="">
															<div class="row">
																<div class="col-xs-12 form-inline"></div>
															</div>
														</div>


														<div class="table-responsive" id="MyTable">
															<table
																class="table table-striped table-warning table-hover"
																id="admin-datatable-second_admin">
																<thead>


																	<tr>
																		<th>No</th>
																		<th>PF Number</th>
																		<th>Name</th>
																		<th>Role Name</th>
																		<th>Status</th>
																		<th>Created By</th>
																		<th>Created Date</th>
																		<th>Updated By</th>
																		<th>Updated Date</th>
																		<th>Last Loged Date</th>
																		<th>Action</th>

																	</tr>


																</thead>
																<tbody>

																	<c:forEach items="${UserList}" var="element"
																		varStatus="loop">
																		<tr>
																			<td align="center"><c:out value="${element.id}" /></td>
																			<td align="left">${element.pfNumber}</td>
																			<td align="left">${element.name}</td>
																			<td align="left">${element.userrole}</td>
																			<td align="center"><c:if
																					test="${element.status eq 1}">
																					<span class="label label-success">ACTIVE</span>
																				</c:if> <c:if test="${element.status eq 0}">
																					<span class="label label-danger">INACTIVE</span>
																				</c:if></td>
																			<td align="center">${element.createBy}</td>
																			<td align="center">${element.createDate}</td>
																			<td align="center">${element.updateBy}</td>
																			<td align="center">${element.updateDate}</td>
																			<td align="center">${element.lastLogin}</td>
																			<td align="center"><a href="#"
																				data-toggle="modal" data-backdrop="static"
																				data-keyboard="false" data-target="#updateUser"
																				data-value="<c:out value="${element.id}" />">


																					<button class="btn btn-warning">
																						Edit <i class="fa fa-edit" aria-hidden="true"></i>
																					</button>
																			</a></td>
																		</tr>
																	</c:forEach>


																</tbody>
															</table>
														</div>
														<!-- /.table-responsive -->
													</div>
													<!-- /.the-box -->
												</div>
												<!-- End warning color table -->
											</div>
										</div>
									</div>
									<!--row -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->

	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->

	<!-- ********************************************CREATE USER PROFILE****************************************** -->
	<!-- start: modal Create-->
	<div class="modal fade" id="createUser" tabindex="-1" role="dialog"
		aria-labelledby="createUserLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="reportDetailLabel">Create User
						Profile</h4>
				</div>
				<div class="modal-body">

					<div class="box-body">
						<div class="content-wrapper">

							<section class="content">
								<div class="container">
									<div class="row">
										<div class="col-md-6">

											<form class="form-horizontal"
												action="${pageContext.request.contextPath}/uam/createUserProfileAction"
												method="POST" id="createProfileForm"
												name="createUserProfileAction">
												<fieldset>
													<input type="hidden" name="status" id="status" value="1">


													<div class="control-group">
														<label class="control-label" for="username">PF
															Number</label>

														<div class="controls">
															<input type="text" id="pfNumber" name="pfNumber"
																placeholder="" class="form-control input-lg" required>
														</div>
													</div>

													<div class="control-group">
														<div class="controls">
															<div id="recordFound"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i> PF
																	Number Already Exist</strong>
															</div>
														</div>
													</div>

													<div class="control-group">
														<div class="controls">
															<div id="messagePF"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i>
																	Please enter PF Number</strong>
															</div>
														</div>
													</div>



													<div class="control-group">
														<label class="control-label" for="username">Name</label>
														<div class="controls">
															<input type="text" id="name" name="name" placeholder=""
																class="form-control input-lg" required>
														</div>
													</div>

													<div class="control-group">
														<div class="controls">
															<div id="messageName"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i>
																	Please enter Name</strong>
															</div>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="username">Manager
															PF Number</label>




														<div class="controls">
															<input type="text" id="managerPF" name="managerPF"
																placeholder="" class="form-control input-lg" required>
														</div>
														<div id="mngPFNamediv"
															class="alert alert-danger fade in alert-dismissable hidediv">
															<strong> Manager Name : <span id="mngPFName"></span>
															</strong>
														</div>
													</div>
													<div class="control-group">
														<div class="controls">
															<div id="MngPFNumberFound"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i> PF
																	Number Already Exist </strong>
															</div>

															<div id="messageManagerPF"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i>
																	Please enter Manager PF Number</strong>
															</div>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label" for="username">E-mail
															Address</label>
														<div class="controls">
															<input type="text" id="email" name="email" placeholder=""
																class="form-control input-lg" required>
														</div>
													</div>
													<div class="control-group">
														<div class="controls">
															<div id="messageemail"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i>
																	Please enter E-mail Address</strong>
															</div>
														</div>
													</div>

													<div class="control-group">
														<label class="control-label" for="username">Remarks</label>
														<div class="controls">
															<input type="text" id="remarks" name="remarks"
																placeholder="" class="form-control input-lg" required>
														</div>
													</div>
													<div class="control-group">
														<div class="controls">
															<div id="messageremarks"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i>
																	Please enter Remarks</strong>
															</div>
														</div>
													</div>

													<div class="control-group">
														<label class="control-label" for="username">Role</label>
														<div class="controls">
															<select class="form-control" name="roleId" id="roleId"
																multiple required>
																<option value="" selected>-View All-</option>
																<c:forEach items="${UserRoleSearch}" var="element">
																	<option value="<c:out value="${element.id}"/>">
																		<c:out value="${element.name}" />
																	</option>
																</c:forEach>


															</select>


															<%--   <table>
    <c:forEach items="${UserRoleSearch}" var="userrole" varStatus="rowCounter">
    <c:if test="${rowCounter.count % 3 == 1}">
     <tr>
    </c:if>
    <td style="padding-left:15px;">
    <input type="checkbox"  name="roleId" value="${userrole.id}">&nbsp;<c:out value="${userrole.name}" /></td>
    <c:if test="${rowCounter.count % 3 == 0||rowCounter.count == fn:length(values)}">
      </tr>
    </c:if>
  </c:forEach >
  </table>
 
    <!--  <table>
    <c:forEach items="${UserRoleSearch}" var="userrole" varStatus="rowCounter">
    <c:if test="${rowCounter.count % 3 == 1}">
     <tr>
    </c:if>
    <td style="padding-left:15px;">
    <input type="checkbox"  name="roleId" value="${userrole.id}">&nbsp;<c:out value="${userrole.name}" /></td>
    <c:if test="${rowCounter.count % 3 == 0||rowCounter.count == fn:length(values)}">
      </tr>
    </c:if>
  </c:forEach >
  </table>--> --%>

														</div>
													</div>




													<div class="control-group">
														<div class="controls">
															<div id="messageRole"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i>
																	Please select Role</strong>
															</div>
														</div>
													</div>

													<div class="control-group">
														<div class="controls">&nbsp;</div>
													</div>

													<div class="control-group">
														<!-- Button -->
														<div class="controls">
															<button class="btn btn-warning" id="createUserProfileBtn">Create
																User Profile</button>
															&nbsp;
															<button class="btn btn-danger" data-dismiss="modal"
																aria-label="Close">Cancel</button>
														</div>
													</div>
												</fieldset>
											</form>

										</div>
									</div>
								</div>

							</section>


						</div>
					</div>
					<!-- /.box-body -->



				</div>
				<div class="modal-footer">
					<!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</div>
	</div>


	<!-- end: modal Create-->

	<!-- ********************************************UPDATE USER PROFILE****************************************** -->
	<!-- start: modal Update-->
	<div class="modal fade" id="updateUser" tabindex="-1" role="dialog"
		aria-labelledby="updateUserLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="reportDetailLabel">Update User
						Profile123</h4>
				</div>
				<div class="modal-body">

					<div class="box-body">
						<div class="content-wrapper">

							<section class="content">
								<div class="container">
									<div class="row">
										<div class="col-md-6">

											<form class="form-horizontal"
												action="${pageContext.request.contextPath}/uam/updateUserProfileAction"
												method="POST" id="updateProfileForm"
												name="updateUserProfileAction">
												<fieldset>

													<input type="hidden" name="totalModule" id="totalModule"
														value="${TotalModule}"> <input type="hidden"
														name="id" id="id" value="">

													<div class="control-group">
														<label class="control-label" for="username">PF
															Number</label>
														<div class="controls">
															<input type="text" id="pfNumber" name="pfNumber"
																placeholder="" class="form-control input-lg" required
																readonly>
														</div>
													</div>
													<div class="control-group">
														<div class="controls">
															<div id="recordFoundUpdate"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i> PF
																	Number Already Exist</strong>
															</div>
														</div>
													</div>

													<div class="control-group">
														<div class="controls">
															<div id="messagePFUpdate"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i>
																	Please enter PF Number</strong>
															</div>
														</div>
													</div>

													<div class="control-group">
														<label class="control-label" for="username">Name</label>
														<div class="controls">
															<input type="text" id="name" name="name" placeholder=""
																class="form-control input-lg" required>
														</div>
													</div>
													<div class="control-group">
														<div class="controls">
															<div id="messageNameUpdate"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i>
																	Please enter Name</strong>
															</div>
														</div>
													</div>


													<div class="control-group">
														<label class="control-label" for="mngPFNumber">Manager
															PF Number</label>
														<div class="controls">
															<input type="text" id="mngPFNumber" name="mngPFNumber"
																placeholder="" class="form-control input-lg" required>
														</div>
														<div id="mngPFNamedivUpdate"
															class="alert alert-danger fade in alert-dismissable hidediv">
															<strong> Manager Name : <span
																id="mngPFNameUpdate"></span>
															</strong>
														</div>

													</div>
													<div class="control-group">
														<div class="controls">
															<div id="messageMngPFNumberUpdate"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i>
																	Please enter Manager PF Numbers</strong>
															</div>
														</div>
													</div>




													<div class="control-group">
														<label class="control-label" for="username">E-mail
															Address</label>
														<div class="controls">
															<input type="text" id="email" name="email" placeholder=""
																class="form-control input-lg" required>
														</div>
													</div>
													<div class="control-group">
														<div class="controls">
															<div id="messageEmailUpdate"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i>
																	Please enter E-mail Address</strong>
															</div>
														</div>
													</div>


													<div class="control-group">
														<label class="control-label" for="remarks">Remarks</label>
														<div class="controls">
															<input type="text" id="remarks" name="remarks"
																placeholder="" class="form-control input-lg" required>
														</div>
													</div>
													<div class="control-group">
														<div class="controls">
															<div id="messageUpdateRemarks"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i>
																	Please Enter Remarks</strong>
															</div>
														</div>
													</div>



													<div class="control-group">
														<label class="control-label" for="username">Role</label>
														<div class="controls">
															<select class="form-control" name="groupID" id="groupID"
																multiple required>
																<option value="" selected>-View All-</option>
																<c:forEach items="${UserRoleSearch}" var="element">
																	<option value="<c:out value="${element.id}"/>">
																		<c:out value="${element.name}" />
																	</option>
																</c:forEach>
															</select>
														</div>
													</div>


													<div class="control-group">
														<div class="controls">
															<div id="messageRoleUpdate"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i>
																	Please select Role</strong>
															</div>
														</div>
													</div>


													<div class="control-group">
														<label class="control-label" for="status">Status</label>
														<div class="controls">

															<select class="form-control" name="status" id="status"
																required>
																<option value="">-- Select Status--</option>


																<c:forEach var="statusItem" items="${StatusList}">
																	<option
																		value="<c:out value="${statusItem.paramCode}"/>">
																		<c:out value="${statusItem.paramName}" />
																	</option>
																</c:forEach>
															</select>
														</div>
													</div>

													<div class="control-group">
														<div class="controls">
															<input type="hidden" name="usereditid" id="usereditid"
																value="">
															<div id="messageStatusUpdate"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i>
																	Please select Status</strong>
															</div>
														</div>
													</div>

													<div class="control-group">
														<div class="controls">&nbsp;</div>
													</div>



													<div class="control-group">
														<!-- Button -->
														<div class="controls">
															<button class="btn btn-warning" id="updateUserProfileBtn">Update
																User Group Profile</button>
															&nbsp;
															<button class="btn btn-danger" data-dismiss="modal"
																aria-label="Close">Cancel</button>
														</div>
													</div>
												</fieldset>
											</form>

										</div>
									</div>
								</div>

							</section>


						</div>
					</div>
					<!-- /.box-body -->



				</div>
				<div class="modal-footer">
					<!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</div>
	</div>


	<!-- end: modal -->

	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->

	<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->

	<!-- 	 <script src="assets/jAlert/jAlert.min.js"></script>
    <script src="assets/jAlert/jAlert-functions.min.js"></script>
         -->

	<script type="text/javascript">
	


         	$(document).ready(function() {
         		
         		
         	  var updatemessage="<c:out value="${updatemessage}"/>";
	 		       // show when the button is clicked
	 		       if (updatemessage.length) {
	 		    	// alert(updatemessage);
	 		    successAlert('Success!', updatemessage);
	 		       }  
	 		       
	 		   
	 		      var savemessage="<c:out value="${savemessage}"/>";
	 		       // show when the button is clicked
	 		       if (savemessage.length) {
	 		    	// alert(updatemessage);
	 		    successAlert('Success!', savemessage);
	 		       }  
         		
         		
         		$('#messagePF').hide();
          		$('#messageName').hide();
          		$('#messageManagerPF').hide();
          		$('#messageemail').hide();
          		$('#messageRole').hide();
          		$('#recordFound').hide();
          		$('#recordFound').hide();
          	   	$('#MngPFNumberFound').hide();
          		$('#messageremarks').hide();
          		$('#mngPFNamediv').hide();
         		$('#pfNumber').keyup(function() {
             	    if($(this).val() == ''){
             	        $('#messagePF').show();		             	     
             	      
             	    }else{
             	        $('#messagePF').hide();
             	        
             	    }
             	});
         	   
         		$('#name').keyup(function() {
             	    if($(this).val() == ''){
             	        $('#messageName').show();
             	    }else{
             	        $('#messageName').hide();
             	    }
             	});
         		$('#managerPF').keyup(function() {
             	    if($(this).val() == ''){
             	        $('#messageManagerPF').show();
             	    }else{
             	        $('#messageManagerPF').hide();
             	    }
             	});
         		$('#email').keyup(function() {
             	    if($(this).val() == ''){
             	        $('#messageemail').show();
             	    }else{
             	        $('#messageemail').hide();
             	    }
             	});
         		
         		$('#remarks').keyup(function() {
             	    if($(this).val() == ''){
             	        $('#messageremarks').show();
             	    }else{
             	        $('#messageremarks').hide();
             	    }
             	});
         		
         		
         		$('#groupID').change(function() {
         			if($(this).val() == ''){
         			    $('#messageRole').show();
         			}else{
         			    $('#messageRole').hide();
         			}
         		});
         		
         		
         		$('#status').on('change', function() {
         			alert('test');
         			if($(this).val() == ''){
         			    $('#messageStatusUpdate').show();
         			}else{
         			    $('#messageStatusUpdate').hide();
         			}

               	});

         		//$('#recordFound').hide();
		         	
         		
		        	 var t = $('#admin-datatable-second_admin').DataTable( {
	         			 
		         			dom: 'Bfrtip',
		         			  buttons: [	
		         			          
		{
		    extend: 'pageLength',
		    className: 'btn btn-secondary btn-sm active'
		},
		
		{
             extend: 'excel',
             filename: 'Etiqa User List',
             className: 'btn btn-warning btn-sm',
             text:'Export to XLS <i class="fa fa-download" aria-hidden="true"></i>',
             
            	 exportOptions: {
                     columns: [ 1,2,3,4,5,6,7,8,9]
             
                 }
             
             
             
             
        }
		           
		         			  ],
		         		      "searching" :false, 
		         	          "columnDefs": [ {
		         	          "searchable": false,
		         	          "orderable": false,
		         	          "targets": [0,1,2,3,4,5,6,7,8],
		         	          "bSortable": false
		         	         
		         	        } ],
		         	        "order": [[ 0, 'asc' ]]
		         	    } );
		         	 
		         	    t.on( 'order.dt search.dt', function () {
		         	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		         	            cell.innerHTML = i+1;
		         	        } );
		         	    } ).draw();
		         	           	
		         	  
		         	   //t.buttons( 0, null ).containers().appendTo( '#btnhere' );
		         	   
		         	   

		         		
		         	   
		         	  $("#createUserProfileBtn").on("click", function(event) {
		         		  
		         		$('#recordFound').hide();
		          		$('#messagePF').hide();
		          		$('#messageName').hide();
		          	   	$('#MngPFNumberFound').hide();
		          		$('#messageManagerPF').hide();
		          		$('#messageemail').hide();
		          		$('#messageRole').hide();
		          		$('#messageremarks').hide();
							event.preventDefault();
							$.ajax({
								url : 'validateCreateUser',
								type : "POST",
								data : $("#createProfileForm").serialize(),
								dataType : 'json',
								success : function(response) {
									var obj = JSON.stringify(response);
							    	 console.log(response);
													
							    	 var result = $.parseJSON(obj);
												  		  
									 
									 $.each(result, function() {		  			 

										console.log(result.message);
										
										if(result.requiredPF == true){
											
											$('#messagePF').show();
										}
										
										if(result.requiredManagerPF == true){
											$('#messageManagerPF').show();
										}
										if(result.requiredEmail == true){
											$('#messageemail').show();
										}
										
										if(result.requiredRemarks == true){
											$('#messageremarks').show();
										}
										
										if(result.requiredName == true){
											$('#messageName').show();
										}
										
										if(result.requiredRole == true){
											
											$('#messageRole').show();
										}
										
										
										if(result.recordFound == false){
											
											if(result.requiredPF == false && result.requiredName == false && result.requiredRole == false){
											
											$('#createProfileForm').attr('action', 'createUserProfileAction');
						                    $('#createProfileForm').submit();
										}
											
										}
										
										if(result.recordFound == true && result.count !=-1){
										
											$('#recordFound').show();
										}
						  	    
							  	    	
							  		  });
									 
							  			  
							  			 		
									
								},
								error : function(data, status, er) {
									//alert(data + "_" + status + "_" + er);
								}
							});
						});
		    
		    
		 
       	  $("#updateUserProfileBtn").on("click", function(event) {
       		  
       		   $('#recordFoundUpdate').hide();
        		$('#messagePFUpdate').hide();
        		$('#messageNameUpdate').hide();
        		 		$('#messageMngPFNumberUpdate').hide();
        		 		$('#messageUpdateRemarks').hide();
        		$('#messageEmailUpdate').hide();        		
        		$('#messageManagerPF').hide();
        		$('#messageemail').hide();
        		$('#messageremarks').hide();
        		$('#messageRoleUpdate').hide();
        		$('#messageStatusUpdate').hide();
					event.preventDefault();
					$.ajax({
						url : 'validateUpdateUser',
						type : "POST",
						data : $("#updateProfileForm").serialize(),
						dataType : 'json',
						success : function(response) {
							var obj = JSON.stringify(response);
					    	 console.log(response);
											
					    	 var result = $.parseJSON(obj);
										  		  
							 
							 $.each(result, function() {		  			 

								console.log(result.message);
								
								if(result.requiredPF == true){
									
									$('#messagePFUpdate').show();
								}
								
								if(result.requiredName == true){
									
									$('#messageNameUpdate').show();
								}
								
								if(result.requiredmngPFNumber == true){
									
									$('#messageMngPFNumberUpdate').show();
								}
								
									if(result.requiredRemarks == true){
									
										$('#messageUpdateRemarks').show();
								}
								
								
								
								if(result.requiredEmail == true){									
									
									$('#messageEmailUpdate').show();
								}
								
								
								
								if(result.requiredRole == true){
									
									$('#messageRoleUpdate').show();
								}
								
								
                                if(result.requiredStatus == true){
									
									$('#messageStatusUpdate').show();
								}
								
                              
							
								if(result.recordFound == false){
							
									if(result.requiredPF == false && result.requiredName == false && result.requiredRole == false && result.requiredStatus == false && result.requiredEmail == false && result.requiredmngPFNumber == false&& result.requiredRemarks == false){
									
									$('#updateProfileForm').attr('action', 'updateUserProfileAction');
				                    $('#updateProfileForm').submit();
								}
									
								}
								
								if(result.recordFound == true && result.count !=-1){
								
									$('#recordFoundUpdate').show();
								}
				  	    
					  	    	
					  		  });
							  
						},
						error : function(data, status, er) {
							//alert(data + "_" + status + "_" + er);
						}
					});
				}); 
       	});
		         
         	//to reset form after close modal
         $('#createUser').on('hidden.bs.modal', function(){
         	    $(this).find('form')[0].reset();
         		$('#recordFound').hide();
         		$('#messagePF').hide();
         		$('#messageName').hide();
         		$('#messageManagerPF').hide();
         		$('#messageemail').hide();
         		$('#messageremarks').hide();
         		$('#messageRole').hide();
         		$('#recordFoundUpdate').hide();
         		$('#messagePFUpdate').hide();
         		$('#messageNameUpdate').hide();
         		$('#messageUpdateRemarks').hide();
         		$('#messageMngPFNumberUpdate').hide();
         		$('#messageEmailUpdate').hide();
         		
         		$('#messageRoleUpdate').hide();
         		$('#messageStatusUpdate').hide();
         		
         		//Update
         		$('#messageNameUpdate').hide(); 
          		$('#messagePFUpdate').hide();
          		$('#messageUpdateRemarks').hide();
          		$('#recordFoundUpdate').hide();
          		$('#messageRoleUpdate').hide();
          		 
         	}); 
                 	
      
        
         	
         	
     //Edit User Profile before show modal
       	 $('#updateUser').on('show.bs.modal', function (event) {
       	 var modal = $(this);	
     
       	var button = $(event.relatedTarget) // Button that triggered the modal
  	    var recordID = button.data('value') // Extract info from data-* attributes
 
  		$('#recordFound').hide(); 
       	$('#MngPFNumberFound').hide(); 
  		$('#messagePF').hide();
  		$('#messageName').hide();
  		$('#messageManagerPF').hide();
  		$('#messageemail').hide();
  		$('#messageremarks').hide();
  		$('#messageRole').hide();
  	    
  		//update by pramaiyan
  				$('#messageNameUpdate').hide(); 
  				$('#messageMngPFNumberUpdate').hide();
  				$('#messageNameUpdate').hide(); 
          		$('#messagePFUpdate').hide();
          		$('#recordFoundUpdate').hide();
          		$('#messageRoleUpdate').hide();
        		$('#messageEmailUpdate').hide();
        		$('#messageStatusUpdate').hide();
        		$('#messageUpdateRemarks').hide();
        		
          		
 
     	modal.find('#usereditid').val(recordID);
  	 $.ajax({  
		     type : "Get",   
		     url : "userDetail", //see in UserController.java
		     data : "id=" + recordID, 
		   dataType: "json", //set to JSON   
		     success : function(response) {
		    	var obj = JSON.stringify(response);
		    	 console.log(response);
		
		
		    	 var result = $.parseJSON(obj);
		  		  $.each(result, function() {	
		  		 // alert(result);	  			 
                   
		  			  // modal.find('.modal-title').text("Transaction Record : "+this['transactionid'] );
		  	    	  //modal.find('.modal-body input').val(result.name);
		  	    	//  alert(result.pfNumber);
		  	    	 modal.find('#pfNumber').val(result.pfNumber);
		  	    	 modal.find('#name').val(result.name);
		  	    	 modal.find('#mngPFNumber').val(result.mngPFNumber);
		  	    	 modal.find('#remarks').val(result.remarks);
		  	    	 modal.find('#email').val(result.email);
		  	    	 modal.find('#status').val(result.status);
		  	    	 modal.find('#groupID').val(result.groupID);
		  	    	  //modal.find('.modal-body select').val(result.status);
		  	    	  modal.find('#id').val(result.id);	  
	  	    
		  	    	
		  		  });
		     },  
		     error : function(e) {  
		      alert('Error: ' + e);   
		     }  
		    });   
 
       	})
       	
       	
       	  $('#createUser').on('show.bs.modal', function (event) {        	
          	    $(this).find('form')[0].reset(); 
          	 	$('#recordFound').hide(); 
          		$('#messagePF').hide();
          		$('#messageName').hide();
          		$('#messageManagerPF').hide();
          		$('#messageemail').hide();
          		$('#messageremarks').hide();
          		$('#messageRole').hide();
          	   	$('#MngPFNumberFound').hide();
          	  	$('#mngPFNamediv').hide();
          	   	 
          		//Update
          		$('#messageNameUpdate').hide(); 
          		$('#messageMngPFNumberUpdate').hide();
          		$('#messageEmailUpdate').hide(); 
          		$('#messagePFUpdate').hide();
          		$('#recordFoundUpdate').hide();
          		$('#messageRoleUpdate').hide();
          		$('#messageStatusUpdate').hide();
          		$('#messageUpdateRemarks').hide();
          		 
         });
		         		 
       	// to allow only one checkbox checked
       		$('input[type="checkbox"]').on('change', function() {
       	    $('input[name="' + this.name + '"]').not(this).prop('checked', false);
       	});
       	
       		 $("#managerPF").focusout(function () {
       			var str = $("#createProfileForm").serialize();
       			var urllink='${pageContext.request.contextPath}/uam/validateMngPFNumber';
   				console.log(urllink);
       			$.ajax({
       					type: "post",
       					url: urllink,
       					data: str,
       					success: function(data) {
       					console.log(data);
       					//
       					if(data)
       						{ 
       						$('#mngPFName').text(data);
       						$('#mngPFNamediv').show();
       						$('#mngPFNamediv').removeClass('alert-danger');
       						$('#mngPFNamediv').addClass('alert-success');
       						$('#createUserProfileBtn').show();
       						}
       					else{
       						$('#mngPFName').text("Manager Not Found");
       						$('#mngPFNamediv').show();
       						$('#createUserProfileBtn').hide();
       						$('#mngPFNamediv').removeClass('alert-success');
       						$('#mngPFNamediv').addClass('alert-danger');
       					}
       					},
       					error: function(request, status, error) {
       						alert(request.responseText);
       					}
       				});
       		}); 
       		 
       		 
       		$("#mngPFNumber").focusout(function () {
       			var str = $("#updateProfileForm").serialize();
       			var urllink='${pageContext.request.contextPath}/uam/validateMngPFNumberUpdate';
   				console.log(urllink);
       			$.ajax({
       					type: "post",
       					url: urllink,
       					data: str,
       					success: function(data) {
       					console.log(data);
       					//
       					if(data)
       						{ 
       						$('#mngPFNameUpdate').text(data);
       						$('#mngPFNamedivUpdate').show();
       						$('#mngPFNamedivUpdate').removeClass('alert-danger');
       						$('#mngPFNamedivUpdate').addClass('alert-success');
       						$('#updateUserProfileBtn').show();
       						}
       					else{
       						$('#mngPFNameUpdate').text("Manager Not Found");
       						$('#mngPFNamedivUpdate').show();
       						$('#updateUserProfileBtn').hide();
       						$('#mngPFNamedivUpdate').removeClass('alert-success');
       						$('#mngPFNamedivUpdate').addClass('alert-danger');
       					}
       					},
       					error: function(request, status, error) {
       						alert(request.responseText);
       					}
       				});
       		}); 
       		
       		
    
        </script>
</body>
</html>