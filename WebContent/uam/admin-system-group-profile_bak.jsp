<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<% java.text.DateFormat df=new java.text.SimpleDateFormat("dd/MM/yyyy"); %>



<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>

</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">System Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>
																User Group Profile <a href="#" data-toggle="modal"
																	data-backdrop="static" data-keyboard="false"
																	data-target="#createUserGroup"><button
																		class="btn btn-warning btn-default pull-right">
																		Create User Group Profile <i class="fa fa-users"
																			aria-hidden="true"></i>
																	</button></a>

															</h4>

														</div>
													</div>


													<!-- Start Form -->
													<form name="userGroup"
														action="${pageContext.request.contextPath}/uam/userGroup"
														id="userGroup" method="post">

														<input type="hidden" name="action" value="search" />

														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Group
																				Name</label>
																			<div class="col-sm-9">
																				<select class="form-control" name=id>
																					<option value="" selected>-View All-</option>
																					<c:forEach items="${UserGroupSearch}" var="element">
																						<option value="<c:out value="${element.id}"/>">
																							<c:out value="${element.name}" />
																						</option>
																					</c:forEach>
																				</select>
																			</div>
																		</div>




																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Status</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="status">
																					<option value="" selected>-View All-</option>
																					<c:forEach items="${StatusList}" var="element">
																						<option
																							value="<c:out value="${element.paramCode}"/>">
																							<c:out value="${element.paramName}" />
																						</option>
																					</c:forEach>
																				</select>
																			</div>
																		</div>




																	</div>
																</div>
																<div class="col-sm-12 text-right">
																	<button type="submit" class="btn btn-default">Search</button>
																</div>
															</div>
														</div>
													</form>
													<!-- END FORM -->

												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="">
															<div class="row">
																<div class="col-xs-12 form-inline"></div>
															</div>
														</div>
														<div class="table-responsive" id="MyTable">
															<table
																class="table table-striped table-warning table-hover"
																id="admin-datatable-second_admin">
																<thead>


																	<tr>
																		<th>No</th>
																		<th>Group Name</th>
																		<th>Status</th>
																		<th>Created By</th>
																		<th>Created Date</th>
																		<th>Updated By</th>
																		<th>Updated Date</th>
																		<th>Action</th>

																	</tr>


																</thead>
																<tbody>

																	<c:forEach items="${UserGroupList}" var="element"
																		varStatus="loop">
																		<tr>
																			<td align="center">${element.id}</td>
																			<td align="left">${element.name}</td>
																			<td align="center"><c:if
																					test="${element.status eq 1}">
																					<span class="label label-success">ACTIVE</span>
																				</c:if> <c:if test="${element.status eq 0}">
																					<span class="label label-danger">INACTIVE</span>
																				</c:if></td>
																			<td align="center">${element.createBy}</td>
																			<td align="center">${element.createDate}</td>
																			<td align="center">${element.updateBy}</td>
																			<td align="center">${element.updateDate}</td>
																			<td align="center"><a href="#"
																				data-toggle="modal" data-backdrop="static"
																				data-keyboard="false" data-target="#updateUserGroup"
																				data-value="${element.id}"><button
																						class="btn btn-warning btn-sm">
																						Edit <i class="fa fa-edit" aria-hidden="true"></i>
																					</button></a></td>
																		</tr>
																	</c:forEach>

																</tbody>
															</table>
														</div>
														<!-- /.table-responsive -->
													</div>
													<!-- /.the-box -->
												</div>
												<!-- End warning color table -->
											</div>
										</div>
									</div>
									<!--row -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->

	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->

	<!-- ********************************************CREATE GROUP****************************************** -->
	<!-- start: modal Create-->
	<div class="modal fade" id="createUserGroup" tabindex="-1"
		role="dialog" aria-labelledby="createUserGroupLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="reportDetailLabel">Create User
						Group Profile</h4>
				</div>
				<div class="modal-body">

					<div class="box-body">
						<div class="content-wrapper">

							<section class="content">
								<div class="container">
									<div class="row">
										<div class="col-md-6">

											<form class="form-horizontal"
												action="${pageContext.request.contextPath}/uam/createUserGroupProfileAction"
												method="POST" id="createGroupProfileForm"
												name="createGroupProfileForm">
												<fieldset>
													<input type="hidden" name="status" id="status" value="1">

													<input type="hidden" name="totalModule" id="totalModule"
														value="${TotalModule}">

													<div class="control-group">
														<label class="control-label" for="username">Group
															Name</label>
														<div class="controls">
															<input type="text" id="name" name="name" placeholder=""
																class="form-control input-lg" required>
														</div>
													</div>

													<div class="control-group">
														<div class="controls">
															<div id="recordFound"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i> Group
																	Already Exist</strong>
															</div>
														</div>
													</div>

													<div class="control-group">
														<div class="controls">
															<div id="messageName"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i>
																	Please enter Group Name</strong>
															</div>
														</div>
													</div>

													<div class="control-group">
														<label class="control-label" for="username">Module
															Access</label>
														<div class="controls">&nbsp;</div>
													</div>


													<div class="control-group">
														<div class="controls">





															<table
																class="table table-striped table-warning table-hover"
																id="admin-datatable-second_admin">
																<thead>
																	<tr>
																		<th align="center">Module</th>
																		<th align="center">Full Access</th>
																		<th align="center">Read Only</th>
																		<th align="center">No Access</th>
																	</tr>
																</thead>
																<tbody>

																	<c:forEach var="paramItem" items="${ModuleMainList}"
																		varStatus="loop">
																		<c:set var="sublevel1" value="0" />
																		<c:set var="sublevel2" value="0" />
																		<c:set var="sublevel3" value="0" />

																		<tr>
																			<td><div>
																					<b>${paramItem.name} ${paramItem.id}</b>
																				</div> <c:forEach var="subModule" items="${ModuleList}">

																					<c:if test="${subModule.parentId==paramItem.id}">
																						<div>
																							<ul style="list-style-type: square">
																								<li>${subModule.name}${subModule.id}</li>
																							</ul>
																						</div>

																						<c:forEach var="subModule2" items="${ModuleList}">
																							<c:if test="${subModule2.parentId==subModule.id}">


																								<div>
																									<ul>
																										<li style="list-style-type: none"><ul
																												style="list-style-type: disc">
																												<li>${subModule2.name}${subModule2.id}</li>
																											</ul></li>
																									</ul>
																								</div>

																								<c:forEach var="subModule3"
																									items="${ModuleList}">
																									<c:if
																										test="${subModule3.parentId==subModule2.id}">


																										<div>
																											<ul>
																												<li style="list-style-type: none"><ul>
																														<li style="list-style-type: none"><ul
																																style="list-style-type: disc">
																																<li>${subModule3.name}
																																	${subModule3.id}</li>
																															</ul></li>
																													</ul></li>
																											</ul>
																										</div>
																									</c:if>


																								</c:forEach>




																							</c:if>


																						</c:forEach>

																					</c:if>

																				</c:forEach></td>

																			<!-- start display check box  -->
																			<td align="center">
																				<div>&nbsp;</div> <c:forEach var="subModule"
																					items="${ModuleList}">

																					<c:if test="${subModule.parentId==paramItem.id}">
																						<c:set var="sublevel1"
																							value="${subModule.parentId}" />
																						<div>
																							<ul style="list-style-type: square">
																								<li>${subModule.name}${subModule.id}</li>
																							</ul>
																						</div>

																						<c:forEach var="subModule2" items="${ModuleList}">
																							<c:if test="${subModule2.parentId==subModule.id}">
																								<div>
																									<ul style="list-style-type: square">
																										<li>${subModule2.name}${subModule2.id}</li>
																									</ul>
																								</div>


																								<c:set var="sublevel2"
																									value="${subModule2.parentId}" />



																								<c:forEach var="subModule3"
																									items="${ModuleList}">
																									<c:if
																										test="${subModule3.parentId==subModule2.id}">
																										<div>
																											<ul style="list-style-type: square">
																												<li>${subModule3.name}${subModule3.id}</li>
																											</ul>
																										</div>


																										<c:set var="sublevel3"
																											value="${subModule3.parentId}" />

																										<!--  <input type="hidden" name="checkboxFlag<c:out value="${subModule3.id}"/>" value="1">
									 <div class="checkbox"><c:out value="${subModule3.id}"/><input type="checkbox" value="FA" name="accessRight<c:out value="${subModule3.id}"/>"  id="accessRight<c:out value="${subModule3.id}"/>"></div>								
									-->
																									</c:if>


																								</c:forEach>

																								<!--  <c:if test="${sublevel2 !='0' && sublevel3=='0'}">
							    <input type="hidden" name="checkboxFlag<c:out value="${subModule2.id}"/>" value="1">
							    <div class="checkbox"><c:out value="${subModule2.id}"/><input type="checkbox" value="FA" name="accessRight<c:out value="${subModule2.id}"/>" id="accessRight<c:out value="${subModule2.id}"/>"></div>
							    					</c:if>		
							    					
									<c:if test="${sublevel2 !='0' && sublevel3!='0' && sublevel3!=subModule2.id}">
							       <input type="hidden" name="checkboxFlag<c:out value="${subModule2.id}"/>" value="1">
							    					<div class="checkbox"><input type="checkbox" value="FA" name="accessRight<c:out value="${subModule2.id}"/>" id="accessRight<c:out value="${subModule2.id}"/>"></div>
							    					</c:if>		-->




																							</c:if>


																						</c:forEach>

																					</c:if>








																				</c:forEach> <!--<c:forEach var="subModule" items="${ModuleList}">
									<c:if test="${subModule.parentId==paramItem.id}">									
									
									<c:set var="sublevel1" value="${subModule.parentId}"/>								
									<c:forEach var="subModule2" items="${ModuleList}">
									
									<c:if test="${subModule2.parentId==subModule.id}">						
									<c:set var="sublevel2" value="${subModule2.parentId}"/>
									
									
									</c:if>
																		
							    </c:forEach>	
							    
							     
							    					
							    					 <c:if test="${sublevel1 !='0' && sublevel2=='0' && sublevel1!=subModule.id && sublevel3=='0'}">
							    					<div class="checkbox"><input type="checkbox" value="FA" name="accessRight<c:out value="${subModule.id}"/>" id="accessRight<c:out value="${subModule.id}"/>"></div>
							    					</c:if>	
							    										
							    					
							        
									</c:if>	
									
													
							    </c:forEach>	--> <c:if test="${sublevel1 =='0'}">
																					<input type="hidden"
																						name="checkboxFlag<c:out value="${paramItem.id}"/>"
																						value="1">
																					<div class="checkbox">
																						<input type="checkbox" value="FA"
																							name="accessRight<c:out value="${paramItem.id}"/>"
																							id="accessRight<c:out value="${paramItem.id}"/>">
																					</div>
																				</c:if>



																			</td>
																			<td align="center">
																				<!--<c:forEach var="subModule" items="${ModuleList}">
									<c:if test="${subModule.parentId==paramItem.id}">									
									
									<c:set var="tmp1" value="${subModule.parentId}"/>								
									<c:forEach var="subModule2" items="${ModuleList}">
									
									<c:if test="${subModule2.parentId==subModule.id}">						
									<c:set var="tmp2" value="${subModule2.parentId}"/>
									<div>&nbsp;</div>
									 <div class="checkbox"><input type="checkbox" value="RO" name="accessRight<c:out value="${subModule2.id}"/>"  id="accessRight<c:out value="${subModule2.id}"/>"></div>								
									
									</c:if>
																		
							    </c:forEach>	
							    
							    <c:if test="${tmp1 !='0' && tmp2=='0'}">
							    					<div class="checkbox"><input type="checkbox" value="RO" name="accessRight<c:out value="${subModule.id}"/>" id="accessRight<c:out value="${subModule.id}"/>"></div>
							    					</c:if>		
							        
									
									 <c:if test="${tmp1 !='0' && tmp2!='0' &&  tmp2!=subModule.id}">
							    					<div class="checkbox"><input type="checkbox" value="RO" name="accessRight<c:out value="${subModule.id}"/>" id="accessRight<c:out value="${subModule.id}"/>"></div>
							    					</c:if>		
							        
									</c:if>	
									
													
							    </c:forEach>				
							    
							    					
							    					
							    					<c:if test="${tmp1 =='0'}">
							    					<div class="checkbox"><input type="checkbox" value="RO" name="accessRight<c:out value="${paramItem.id}"/>" id="accessRight<c:out value="${paramItem.id}"/>"></div>
							    					</c:if>
							    					
							    						-->

																			</td>


																			<td align="center">
																				<!--<c:forEach var="subModule" items="${ModuleList}">
									<c:if test="${subModule.parentId==paramItem.id}">									
									
									<c:set var="tmp1" value="${subModule.parentId}"/>								
									<c:forEach var="subModule2" items="${ModuleList}">
									
									<c:if test="${subModule2.parentId==subModule.id}">						
									<c:set var="tmp2" value="${subModule2.parentId}"/>
									<div>&nbsp;</div>
									 <div class="checkbox"><input type="checkbox" value="NA" name="accessRight<c:out value="${subModule2.id}"/>"  id="accessRight<c:out value="${subModule2.id}"/>"></div>								
									
									</c:if>
																		
							    </c:forEach>	
							    
							    <c:if test="${tmp1 !='0' && tmp2=='0'}">
							    					<div class="checkbox"><input type="checkbox" value="NA" name="accessRight<c:out value="${subModule.id}"/>" id="accessRight<c:out value="${subModule.id}"/>"></div>
							    					</c:if>									        
									
									 <c:if test="${tmp1 !='0' && tmp2!='0' &&  tmp2!=subModule.id}">
							    					<div class="checkbox"><input type="checkbox" value="NA" name="accessRight<c:out value="${subModule.id}"/>" id="accessRight<c:out value="${subModule.id}"/>"></div>
							    					</c:if>		
							        
									</c:if>	
									
													
							    </c:forEach>				
							    
							    					
							    					
							    					<c:if test="${tmp1 =='0'}">
							    					<div class="checkbox"><input type="checkbox" value="NA" name="accessRight<c:out value="${paramItem.id}"/>" id="accessRight<c:out value="${paramItem.id}"/>"></div>
							    					</c:if>
							    					
							    		-->

																			</td>

																		</tr>

																	</c:forEach>

																</tbody>
															</table>


														</div>
													</div>


													<div class="control-group">
														<div class="controls">
															<div id="messageModule"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i>
																	Please select module access</strong>
															</div>
														</div>
													</div>


													<div class="control-group">
														<div class="controls">&nbsp;</div>
													</div>


													<div class="control-group">
														<!-- Button -->
														<div class="controls">
															<button class="btn btn-warning"
																id="createGroupProfileBtn">Create User Group
																Profile</button>
															&nbsp;
															<button class="btn btn-danger" data-dismiss="modal"
																aria-label="Close">Cancel</button>
														</div>
													</div>
												</fieldset>
											</form>

										</div>
									</div>
								</div>

							</section>


						</div>
					</div>
					<!-- /.box-body -->



				</div>
				<div class="modal-footer">
					<!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</div>
	</div>


	<!-- end: modal Create-->

	<!-- ********************************************UPDATE GROUP****************************************** -->
	<!-- start: modal Update-->
	<div class="modal fade" id="updateUserGroup" tabindex="-1"
		role="dialog" aria-labelledby="updateUserGroupLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="reportDetailLabel">Update User
						Group Profile</h4>
				</div>
				<div class="modal-body">

					<div class="box-body">
						<div class="content-wrapper">

							<section class="content">
								<div class="container">
									<div class="row">
										<div class="col-md-6">

											<form class="form-horizontal"
												action="updateUserGroupProfileAction" method="POST"
												id="updateGroupProfileForm">
												<fieldset>

													<input type="hidden" name="totalModule" id="totalModule"
														value="${TotalModule}"> <input type="hidden"
														name="id" id="id" value="">

													<div class="control-group">
														<label class="control-label" for="username">Group
															Name</label>
														<div class="controls">
															<input type="text" id="name" name="name" placeholder=""
																class="form-control input-lg" required>
														</div>
													</div>
													<div class="control-group">
														<div class="controls">
															<div id="recordFoundUpdate"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i> Group
																	Already Exist</strong>
															</div>
														</div>
													</div>

													<div class="control-group">
														<div class="controls">
															<div id="messageNameUpdate"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i>
																	Please enter Group Name</strong>
															</div>
														</div>
													</div>
													<div class="control-group">

														<div class="controls">&nbsp;</div>
													</div>

													<div class="control-group">
														<label class="control-label" for="username">Status</label>
														<div class="controls">

															<select class="form-control" name="status" id="status"
																required>
																<option value="">-- Select Status--</option>


																<c:forEach var="statusItem" items="${StatusList}">
																	<option
																		value="<c:out value="${statusItem.paramCode}"/>">
																		<c:out value="${statusItem.paramName}" />
																	</option>
																</c:forEach>



															</select>
														</div>
													</div>

													<div class="control-group">
														<div class="controls">
															<div id="messageStatusUpdate"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i>
																	Please enter Status</strong>
															</div>
														</div>
													</div>

													<div class="control-group">

														<div class="controls">&nbsp;</div>
													</div>

													<div class="control-group">
														<label class="control-label" for="username">Module
															Access</label>
														<div class="controls">&nbsp;</div>
													</div>


													<div class="control-group">
														<div class="controls">

															<table
																class="table table-striped table-warning table-hover"
																id="admin-datatable-second_admin">
																<thead>
																	<tr>
																		<th align="center">Module</th>
																		<th align="center">Full Access</th>
																		<th align="center">Read Only</th>
																		<th align="center">No Access</th>
																	</tr>
																</thead>
																<tbody>

																	<c:forEach var="paramItem" items="${ModuleMainList}"
																		varStatus="loop">
																		<c:set var="tmp1" value="0" />
																		<c:set var="tmp2" value="0" />

																		<tr>
																			<td><div>
																					<b>${paramItem.name}</b>
																				</div> <c:forEach var="subModule" items="${ModuleList}">
																					<c:if test="${subModule.parentId==paramItem.id}">

																						<c:set var="tmp1" value="${subModule.parentId}" />
																						<div>
																							<ul style="list-style-type: square">
																								<li>${subModule.name}</li>
																							</ul>
																						</div>

																						<c:forEach var="subModule2" items="${ModuleList}">
																							<c:if test="${subModule2.parentId==subModule.id}">
																								<c:set var="tmp2" value="${subModule2.parentId}" />

																								<div>
																									<ul>
																										<li style="list-style-type: none"><ul
																												style="list-style-type: disc">
																												<li>${subModule2.name}</li>
																											</ul></li>
																									</ul>
																								</div>
																							</c:if>



																						</c:forEach>

																					</c:if>

																				</c:forEach></td>

																			<!-- start display check box  -->
																			<td align="center"><c:forEach var="subModule"
																					items="${ModuleList}">
																					<c:if test="${subModule.parentId==paramItem.id}">

																						<c:set var="tmp1" value="${subModule.parentId}" />
																						<c:forEach var="subModule2" items="${ModuleList}">

																							<c:if test="${subModule2.parentId==subModule.id}">
																								<c:set var="tmp2" value="${subModule2.parentId}" />
																								<div>&nbsp;</div>
																								<input type="hidden"
																									name="checkboxFlag<c:out value="${subModule2.id}"/>"
																									value="1">
																								<div class="checkbox">
																									<input type="checkbox" value="FA"
																										name="accessRight<c:out value="${subModule2.id}"/>"
																										id="accessRight<c:out value="${subModule2.id}"/>">
																								</div>

																							</c:if>

																						</c:forEach>

																						<c:if test="${tmp1 !='0' && tmp2=='0'}">
																							<input type="hidden"
																								name="checkboxFlag<c:out value="${subModule.id}"/>"
																								value="1">
																							<div class="checkbox">
																								<input type="checkbox" value="FA"
																									name="accessRight<c:out value="${subModule.id}"/>"
																									id="accessRight<c:out value="${subModule.id}"/>">
																							</div>
																						</c:if>


																						<c:if
																							test="${tmp1 !='0' && tmp2!='0' && tmp2!=subModule.id}">
																							<input type="hidden"
																								name="checkboxFlag<c:out value="${subModule.id}"/>"
																								value="1">
																							<div class="checkbox">
																								<input type="checkbox" value="FA"
																									name="accessRight<c:out value="${subModule.id}"/>"
																									id="accessRight<c:out value="${subModule.id}"/>">
																							</div>
																						</c:if>

																					</c:if>


																				</c:forEach> <c:if test="${tmp1 =='0'}">
																					<input type="hidden"
																						name="checkboxFlag<c:out value="${paramItem.id}"/>"
																						value="1">
																					<div class="checkbox">
																						<input type="checkbox" value="FA"
																							name="accessRight<c:out value="${paramItem.id}"/>"
																							id="accessRight<c:out value="${paramItem.id}"/>">
																					</div>
																				</c:if></td>
																			<td align="center"><c:forEach var="subModule"
																					items="${ModuleList}">
																					<c:if test="${subModule.parentId==paramItem.id}">

																						<c:set var="tmp1" value="${subModule.parentId}" />
																						<c:forEach var="subModule2" items="${ModuleList}">

																							<c:if test="${subModule2.parentId==subModule.id}">
																								<c:set var="tmp2" value="${subModule2.parentId}" />
																								<div>&nbsp;</div>
																								<div class="checkbox">
																									<input type="checkbox" value="RO"
																										name="accessRight<c:out value="${subModule2.id}"/>"
																										id="accessRight<c:out value="${subModule2.id}"/>">
																								</div>

																							</c:if>

																						</c:forEach>

																						<c:if test="${tmp1 !='0' && tmp2=='0'}">
																							<div class="checkbox">
																								<input type="checkbox" value="RO"
																									name="accessRight<c:out value="${subModule.id}"/>"
																									id="accessRight<c:out value="${subModule.id}"/>">
																							</div>
																						</c:if>

																						<c:if
																							test="${tmp1 !='0' && tmp2!='0' && tmp2!=subModule.id}">
																							<div class="checkbox">
																								<input type="checkbox" value="RO"
																									name="accessRight<c:out value="${subModule.id}"/>"
																									id="accessRight<c:out value="${subModule.id}"/>">
																							</div>
																						</c:if>

																					</c:if>


																				</c:forEach> <c:if test="${tmp1 =='0'}">
																					<div class="checkbox">
																						<input type="checkbox" value="RO"
																							name="accessRight<c:out value="${paramItem.id}"/>"
																							id="accessRight<c:out value="${paramItem.id}"/>">
																					</div>
																				</c:if></td>


																			<td align="center"><c:forEach var="subModule"
																					items="${ModuleList}">
																					<c:if test="${subModule.parentId==paramItem.id}">

																						<c:set var="tmp1" value="${subModule.parentId}" />
																						<c:forEach var="subModule2" items="${ModuleList}">

																							<c:if test="${subModule2.parentId==subModule.id}">
																								<c:set var="tmp2" value="${subModule2.parentId}" />
																								<div>&nbsp;</div>
																								<div class="checkbox">
																									<input type="checkbox" value="NA"
																										name="accessRight<c:out value="${subModule2.id}"/>"
																										id="accessRight<c:out value="${subModule2.id}"/>">
																								</div>

																							</c:if>

																						</c:forEach>

																						<c:if test="${tmp1 !='0' && tmp2=='0'}">
																							<div class="checkbox">
																								<input type="checkbox" value="NA"
																									name="accessRight<c:out value="${subModule.id}"/>"
																									id="accessRight<c:out value="${subModule.id}"/>">
																							</div>
																						</c:if>

																						<c:if
																							test="${tmp1 !='0' && tmp2!='0' && tmp2!=subModule.id}">
																							<div class="checkbox">
																								<input type="checkbox" value="NA"
																									name="accessRight<c:out value="${subModule.id}"/>"
																									id="accessRight<c:out value="${subModule.id}"/>">
																							</div>
																						</c:if>

																					</c:if>


																				</c:forEach> <c:if test="${tmp1 =='0'}">
																					<div class="checkbox">
																						<input type="checkbox" value="NA"
																							name="accessRight<c:out value="${paramItem.id}"/>"
																							id="accessRight<c:out value="${paramItem.id}"/>">
																					</div>
																				</c:if></td>

																		</tr>

																	</c:forEach>



																</tbody>
															</table>


														</div>
													</div>

													<div class="control-group">
														<div class="controls">
															<div id="messageModuleUpdate"
																class="alert alert-danger fade in alert-dismissable hidediv">
																<strong><i class="fa fa-exclamation"></i>
																	Please select module access</strong>
															</div>
														</div>
													</div>

													<div class="control-group">
														<div class="controls">&nbsp;</div>
													</div>


													<div class="control-group">
														<!-- Button -->
														<div class="controls">
															<button class="btn btn-warning"
																id="updateGroupProfileBtn">Update User Group
																Profile</button>
															&nbsp;
															<button class="btn btn-danger" data-dismiss="modal"
																aria-label="Close">Cancel</button>
														</div>
													</div>
												</fieldset>
											</form>

										</div>
									</div>
								</div>

							</section>


						</div>
					</div>
					<!-- /.box-body -->



				</div>
				<div class="modal-footer">
					<!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</div>
	</div>


	<!-- end: modal -->

	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->



	<script type="text/javascript">
         	$(document).ready(function() {
		        	 var t = $('#admin-datatable-second_admin').DataTable( {
	         			 
		         			dom: 'Bfrtip',
		         			  buttons: [	
		{
		    extend: 'pageLength',
		    className: 'btn btn-secondary btn-sm active'
		},
		{
            extend: 'excel',
            filename: 'Etiqa Group List',
            className: 'btn btn-warning btn-sm',
            text:'Export to XLS <i class="fa fa-download" aria-hidden="true"></i>',
            
           	 exportOptions: {
                    columns: [1,2,3,4,5,6]
                }
       }
		           
		         			  ],
		         		      "searching" :false, 
		         	          "columnDefs": [ {
		         	          "searchable": false,
		         	          "orderable": false,
		         	          "targets": [0,1,2,3,4,5,6],
		         	          "bSortable": false
		         	         
		         	        } ],
		         	        "order": [[ 0, 'asc' ]]
		         	    } );
		         	 
		         	    t.on( 'order.dt search.dt', function () {
		         	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		         	            cell.innerHTML = i+1;
		         	        } );
		         	    } ).draw();
		         	           	
		         	  
		         	  // t.buttons( 0, null ).containers().appendTo( '#btnhere' );
		         	   
		         	});
         	
         	
         	
         	$('#name').keyup(function() {
         	    if($(this).val() == ''){
         	       $('#messageName').show();
         	    }else{
         	       $('#messageName').hide();
         	       $('#recordFound').hide();
         	       
         	    }
         	});
         	
         	
         	
         	 $("#createGroupProfileBtn").on("click", function(event) {
        		  
	         		$('#recordFound').hide();	          		
	          		$('#messageName').hide();	
                    $('#messageModule').hide();						
	          		
						event.preventDefault();
						$.ajax({
							url : 'validateCreateGroup',
							type : "POST",
							data : $("#createGroupProfileForm").serialize(),
							dataType : 'json',
							success : function(response) {
								var obj = JSON.stringify(response);
						    	 console.log(response);
												
						    	 var result = $.parseJSON(obj);
											  		  
								 
								 $.each(result, function() {		  			 

									console.log(result.message);
									
									
									
									if(result.requiredName == true){
										
										
										$('#messageName').show();
									}
									
																
									
									if(result.recordFound == true && result.count !=-1){
									
										$('#recordFound').show();
									}
									
									if(result.requiredModule == true){
										
										$('#messageModule').show();
									}
									
									
                                     if(result.recordFound == false){
										
										if(result.requiredName == false){
											
										
											if(result.requiredModule ==false){
										
										$('#createGroupProfileForm').attr('action', 'createUserGroupProfileAction');
					                    $('#createGroupProfileForm').submit();
									}
										}
										
									}
									
					  	    
						  	    	
						  		  });
								 
						  			  
						  			 		
								
							},
							error : function(data, status, er) {
								//alert(data + "_" + status + "_" + er);
							}
						});
					});
					
					
					      	 $("#updateGroupProfileBtn").on("click", function(event) {
        		  
	         		$('#recordFoundUpdate').hide();	          		
	          		$('#messageNameUpdate').hide();	
                    $('#messageStatusUpdate').hide();					
	          		$('#messageModuleUpdate').hide();
					
						event.preventDefault();
						$.ajax({
							url : 'validateUpdateGroup',
							type : "POST",
							data : $("#updateGroupProfileForm").serialize(),
							dataType : 'json',
							success : function(response) {
								var obj = JSON.stringify(response);
						    	 console.log(response);
												
						    	 var result = $.parseJSON(obj);
											  		  
								 
								 $.each(result, function() {		  			 

									console.log(result.message);
									
									
									
									if(result.requiredName == true){
										
										
										$('#messageNameUpdate').show();
									}
									
																
									
									if(result.recordFound == true && result.count !=-1){
									
										$('#recordFoundUpdate').show();
									}
									
									
									if(result.requiredStatus == true){
										
										$('#messageStatusUpdate').show();
									}
									
									
									if(result.requiredModule == true){
										
										$('#messageModuleUpdate').show();
									}
									
									
                                     if(result.recordFound == false){
										
										if(result.requiredName == false){
											
											if(result.requiredStatus ==false){
												
												if(result.requiredModule ==false){
										
										$('#updateGroupProfileForm').attr('action', 'updateUserGroupProfileAction');
					                    $('#updateGroupProfileForm').submit();
											}
									}
										}
										
									}
									
					  	    
						  	    	
						  		  });
								 
						  			  
						  			 		
								
							},
							error : function(data, status, er) {
								//alert(data + "_" + status + "_" + er);
							}
						});
					});
		         
         	//to reset form after close modal
         $('.modal').on('hidden.bs.modal', function(){
         	    $(this).find('form')[0].reset();  
         	   $('input:checkbox').removeAttr('checked');
        		$('#messageName').hide();        		
        		$('#recordFound').hide();
        		$('#messageModule').hide();
        		$('#messageNameUpdate').hide();
        		$('#recordFoundUpdate').hide();
        		$('#messageStatusUpdate').hide();
        		$('#messageModuleUpdate').hide();
        		
         	}); 
        
         	
      
       	
       	 $('#updateUserGroup').on('show.bs.modal', function (event) {
       	 var modal = $(this);	
       	
       	 
       	var button = $(event.relatedTarget) // Button that triggered the modal
  	  var recordID = button.data('value') // Extract info from data-* attributes
  	  
     	
  	 $.ajax({  
		     type : "Get",   
		     url : "userGroupDetail", //see in UserController.java
		     data : "id=" + recordID, 
		   dataType: "json", //set to JSON   
		     success : function(response) {
		    	var obj = JSON.stringify(response);
		    	 console.log(response);
		
		
		    	 var result = $.parseJSON(obj);
		  		  $.each(result, function() {		  			 

		  			// modal.find('.modal-title').text("Transaction Record : "+this['mptransactionid'] );
		  	    	  //modal.find('.modal-body input').val(result.name);
		  	    	  modal.find('#name').val(result.name);
		  	    	  modal.find('.modal-body select').val(result.status);
		  	    	  modal.find('#id').val(result.id);	  
		  	    	  
		  	    
		  	    	for (var i=0;i<result.count;i++){
		  	    		//checkbox checked based on result
		  	    		$('input[type=checkbox][name="accessRight'+result.moduleID[i]+'"]').filter('[value='+result.accessRight[i]+']').prop('checked', true);		  	    		  	    		
		  	    		
		  	    	}
		  	    	
		  	    	
		  		  });
		     },  
		     error : function(e) {  
		      alert('Error: ' + e);   
		     }  
		    });  	
	  

  	  
  	  
  	  
  	  
       	 
       	})
       	 
       	// to allow only one checkbox checked
       		$('input[type="checkbox"]').on('change', function() {
       	    $('input[name="' + this.name + '"]').not(this).prop('checked', false);
       	});
       	
        </script>




</body>
</html>