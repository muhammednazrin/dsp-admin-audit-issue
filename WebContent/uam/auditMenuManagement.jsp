<%@ page import="java.util.List"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%-- <%@ page import="com.etiqa.model.report.TransactionalReport"%> --%>
<%@ page import="java.io.*,java.util.*"%>
<%@ page import="javax.servlet.*,java.text.*"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}
</style>


<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}
</style>
<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<!-- <link href="assets/css/bootstrap.min.css" rel="stylesheet"> -->
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->

<!-- <link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" /> -->


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->


<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

form#agentE, form#agentD {
	display: inline-block;
}

form#agentE input[type=submit], form#agentD input[type=submit] {
	padding: 5px;
}
</style>


</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1__.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Audit Trails - Menu
														Management</a></li>
											</ol>
											<!-- End breadcrumb -->


										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">


												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">

															<div class="content-inner">
																<div class="the-box">
																	<form name="rptForm"
																		action="${pageContext.request.contextPath}/uam/searchMenuAudit"
																		id="searchMenuAudit" method="post"
																		onsubmit="return validateForm()">
																		<%-- <input type="hidden" name="CSRFToken"  value="<%=session.getAttribute("CSRFToken") %>" />
	 --%>
																		<div class="col-sm-6">
																			<div class="form-horizontal">
																				<div class="form-group">
																					<label class="col-sm-4 control-label">From
																						Date<span class="text-danger">*</span>
																					</label>
																					<div class="col-sm-8">
																						<input type="text" readonly id="fromdate" required
																							value="${fromdate}" required name="fromdate"
																							class="form-control">
																					</div>
																				</div>
																			</div>
																		</div>

																		<div class="col-sm-6">
																			<div class="form-horizontal">
																				<div class="form-group">
																					<label class="col-sm-4 control-label">To
																						Date <span class="text-danger">*</span>
																					</label>
																					<div class="col-sm-8">
																						<input type="text" readonly name="todate" required
																							id="todate" value="${todate}"
																							class="form-control">
																					</div>
																				</div>
																			</div>
																		</div>


																		<div class="col-sm-12 text-right">
																			<button type="submit" class="btn btn-default">Search</button>
																		</div>
																	</form>
																</div>




															</div>


															<div class="col-sm-12">

																<!--No record found Invisible  -->
																<div id="noRecordFound"
																	class="alert alert-danger fade in alert-dismissable hidediv">
																	<!--   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X—</button> -->
																	<strong><i class="fa fa-exclamation"></i> No
																		record found</strong>
																</div>
																<!--No record found Invisible  -->
																<div class="content-inner">
																	<div class="the-box static">
																		<div class="title-second">
																			<div class="sub">
																				<h4>Menu Management Logs</h4>
																			</div>
																		</div>

																		<div>
																			<div class="form-horizontal" id="MyTable">
																				<div id="dvData">
																					<!-- class="table table-striped table-hover" -->
																					<!--  <button id="exportButton" class="btn btn-lg btn-danger clearfix"><span class="fa fa-file-excel-o"></span> Export to Excel</button>-->
																					<table id="admin-datatable-second_admin"
																						class="table table-striped table-hover">
																						<thead>
																							<tr>
																								<th>Menu Name</th>
																								<th>Status</th>
																								<th>Date</th>
																								<th>Last Modified</th>
																								<th>Action</th>
																							</tr>
																						</thead>
																						<tbody>
																							<c:forEach items="${auditMenuList}"
																								var="AuditMenu">
																								<tr>

																									<td align='left'><b>After : </b>${AuditMenu.menuName}<br>
																										<b>Before : </b>
																										${AuditMenu.oMenuName}&nbsp;&nbsp;</td>

																									<td align='center'><b>After : </b> <c:if
																											test="${AuditMenu.status eq 1}">
																											<span class="label label-success">ACTIVE</span>
																										</c:if> <c:if test="${AuditMenu.status eq 0}">
																											<span class="label label-danger">INACTIVE</span>
																										</c:if><br> <b>Before : </b> <c:if
																											test="${AuditMenu.oStatus eq 1}">
																											<span class="label label-success">ACTIVE</span>
																										</c:if> <c:if test="${AuditMenu.oStatus eq 0}">
																											<span class="label label-danger">INACTIVE</span>
																										</c:if>&nbsp;&nbsp;</td>

																									<td align='center'>${AuditMenu.dateWhen}</td>
																									<c:if
																										test="${AuditMenu.recordComment eq 'INSERT'}">

																										<td align='left'><b>After : </b>${AuditMenu.createBy}<br>
																											<b>Before : </b>
																											${AuditMenu.oCreateBy}&nbsp;&nbsp;</td>
																									</c:if>

																									<c:if
																										test="${AuditMenu.recordComment eq 'UPDATE'}">

																										<td align='left'><b>After : </b>${AuditMenu.updateBy}<br>
																											<b>Before : </b>
																											${AuditMenu.oUpdateBy}&nbsp;&nbsp;</td>
																									</c:if>
																									<td align='center'>${AuditMenu.recordComment}</td>


																								</tr>
																							</c:forEach>
																						</tbody>
																					</table>

																				</div>

																			</div>
																		</div>
																	</div>
																</div>
																<!--                                                    
                                                    -->
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>


											</div>
										</div>
									</div>
									<!--row -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->

	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%--    <jsp:include page="siteFooter.jsp" />   --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->


	<!-- JAVA Script Goes Here -->

	<script
		src="<c:url value="/resources/assets/js/jquery-2.2.4.min.js" />"></script>
	<!--  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
	<script src="<c:url value="/resources/assets/js/apps.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/retina/retina.min.js"/>" />
	">
	</script>
	<script
		src="<c:url value="/resources/assets/plugins/nicescroll/jquery.nicescroll.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/slimscroll/jquery.slimscroll.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/backstretch/jquery.backstretch.min.js"/>"></script>


	<!-- PLUGINS -->
	<script
		src="<c:url value="/resources/assets/plugins/owl-carousel/owl.carousel.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/prettify/prettify.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"/>"></script>

	<script
		src="<c:url value="/resources/assets/plugins/chosen/chosen.jquery.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/icheck/icheck.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/datepicker/bootstrap-datepicker.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/timepicker/bootstrap-timepicker.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/mask/jquery.mask.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/validator/bootstrapValidator.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/datatable/js/jquery.dataTables.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/datatable/js/bootstrap.datatable.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/datatable/js/jquery.highlight.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/datatable/js/dataTables.searchHighlight.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/summernote/summernote.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/markdown/markdown.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/markdown/to-markdown.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/markdown/bootstrap-markdown.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/slider/bootstrap-slider.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/toastr/toastr.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/newsticker/jquery.newsTicker.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/placeholder/jquery.placeholder.js"/>"></script>

	<script type="text/javascript"
		src="<c:url value="/resources/assets/js/shieldui-all.min.js"/>"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/assets/js/jszip.min.js"/>"></script>

	<script
		src="<c:url value="/resources/assets/plugins/jquery-knob/jquery.knob.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/jquery-knob/knob.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/js/jquery.dataTables.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/js/dataTables.bootstrap.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/js/dataTables.buttons.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/js/buttons.bootstrap.min.js"/>"></script>
	<script src="<c:url value="/resources/assets/js/jszip.min.js"/>"></script>
	<script src="<c:url value="/resources/assets/js/pdfmake.min.js"/>"></script>
	<script src="<c:url value="/resources/assets/js/vfs_fonts.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/js/buttons.html5.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/js/buttons.print.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/js/buttons.colVis.min.js"/>"></script>
	<script src="<c:url value="/resources/assets/js/toastr.min.js"/>"></script>


	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->

	<script>
		$(document).ready(function() {

			$('#admin-datatable-second_admin').DataTable({
				"order" : [],
				dom : 'Bfrtip',
				title : ' Agent List',

				buttons : [ 'copy', 'csv', 'print', {
					extend : 'excelHtml5',
					title : 'Audit Trail - Role Management'
				}, {
					extend : 'pdfHtml5',
					title : 'Audit Trail - Role Management'
				} ]
			});

			// $('#date1').datepicker();
			$("#fromdate").datepicker({
				format : 'dd/mm/yyyy',
				autoclose : true,
			}).on('changeDate', function(ev) {
				$(this).datepicker('hide');
			});
			
			
			$("#todate").datepicker({
				format : 'dd/mm/yyyy',
				autoclose : true,
			}).on('changeDate', function(ev) {
				$(this).datepicker('hide');
			});

			
		});
	</script>


	<script>
		function validateForm() {
			var x = document.forms["rptForm"]["fromdate"].value;
			var y = document.forms["rptForm"]["todate"].value;

			if (x == null || x == "") {

				alert("Please choose Date From");
				document.forms["rptForm"]["fromdate"].focus();

				return false;
			} else if (y == null || y == "") {

				alert("Please choose Date To");
				document.forms["rptForm"]["todate"].focus();
				return false;
			}
			
			else if (x==y) {

				alert("Please choose different Date From and Date To");
				document.forms["rptForm"]["fromdate"].focus();
				
				
				return false;
			}
			
			

			return true;

		}
	</script>






</body>
</html>