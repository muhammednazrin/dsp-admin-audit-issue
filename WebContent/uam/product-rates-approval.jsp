<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<% java.text.DateFormat df=new java.text.SimpleDateFormat("dd/MM/yyyy"); %>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link
	href="<c:url value="/resources/assets/plugins/font-awesome/css/font-awesome.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/css/owl.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/css/bootstrap.css" />"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/css/bootstrap-select.css" />"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/plugins/datatable/css/bootstrap.datatable.min.css"/>"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/plugins/datatable/css/dataTables.searchHighlight.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/css/menu.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/plugins/all.min.css"/>"
	rel="stylesheet" type="text/css" />
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>

</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1__.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">System Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Motor Approver</h4>

														</div>
													</div>


													<!-- Start Form -->
													<form name="userGroup"
														action="${pageContext.request.contextPath}/uam/userRole"
														id="userGroup" method="post">
														<section class="content bg-white">
															<div class="row">
																<div class="col-md-6 mb-3">
																	<h4 class="text-center">Original</h4>
																	<table
																		class="table table-striped table-warning table-hover table-outline">
																		<tbody>
																			<tr>
																				<td width="400" align="right">Sum Covered
																					Limit(Min.)</td>
																				<td width="5">:</td>
																				<td width="400">RM0</td>
																			</tr>
																			<tr>
																				<td align="right">Sum Covered Limit(Max.)</td>
																				<td width="5">:</td>
																				<td>RM500000</td>
																			</tr>
																			<tr>
																				<td align="right">Direct Discount (Online)</td>
																				<td width="5">:</td>
																				<td>10</td>
																			</tr>
																			<tr>
																				<td align="right">GST</td>
																				<td width="5">:</td>
																				<td>6</td>
																			</tr>

																			<tr>
																				<td align="right">5 seater incl. driver</td>
																				<td width="5">:</td>
																				<td>RM61.00</td>
																			</tr>
																			<tr>
																				<td align="right">7 seater incl. driver</td>
																				<td width="5">:</td>
																				<td>RM85.00</td>
																			</tr>
																			<tr>
																				<td align="right">Stamp Duty</td>
																				<td width="5">:</td>
																				<td>RM10</td>
																			</tr>
																		</tbody>
																	</table>
																</div>
																<div class="col-md-6 mb-3">
																	<h4 class="text-center">Changes</h4>
																	<table
																		class="table table-striped table-warning table-hover">
																		<tbody>
																			<tr>
																				<td align="right">Sum Covered Limit(Min.)</td>
																				<td width="5">:</td>
																				<td>RM0</td>
																			</tr>
																			<tr>
																				<td align="right">Sum Covered Limit(Max.)</td>
																				<td width="5">:</td>
																				<td>RM500000</td>
																			</tr>
																			<tr>
																				<td align="right">Direct Discount (Online)</td>
																				<td width="5">:</td>
																				<td>12</td>
																			</tr>
																			<tr>
																				<td align="right">GST</td>
																				<td width="5">:</td>
																				<td>6</td>
																			</tr>

																			<tr>
																				<td align="right">5 seater incl. driver</td>
																				<td width="5">:</td>
																				<td>RM68.00</td>
																			</tr>
																			<tr>
																				<td align="right">7 seater incl. driver</td>
																				<td width="5">:</td>
																				<td>RM85.00</td>
																			</tr>
																			<tr>
																				<td align="right">Stamp Duty</td>
																				<td width="5">:</td>
																				<td>RM10</td>
																			</tr>
																		</tbody>
																	</table>
																</div>
															</div>
															<div class="row">
																<div class="col-sm-12 text-center mb-5">
																	<div class="controls">
																		<button class="btn btn-success">Approve</button>
																		&nbsp;&nbsp;
																		<button class="btn btn-danger" data-dismiss="modal"
																			aria-label="Close">Reject</button>
																	</div>
																</div>
															</div>
														</section>
													</form>
													<!-- END FORM -->

													<!-- END FORM  End by pramaiyan 25022018 -->

												</div>
												<!-- End warning color table -->
											</div>
										</div>
									</div>
									<!--row -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->

	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->

	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->

	<!-- ********************************************CREATE GROUP****************************************** -->
	<!-- start: modal Create-->


	<div class="modal fade" id="updateUserGroup" tabindex="-1"
		role="dialog" aria-labelledby="updateUserGroupLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="reportDetailLabel">Motor Insurance</h4>
				</div>
				<div class="modal-body">
					<section class="content">
						<div class="row">
							<div class="col-md-6 mb-3">
								<h4 class="tex-center">Original</h4>
								<table class="table table-striped table-warning table-hover">
									<tbody>
										<tr>
											<td align="right">Sum Covered Limit(Min.)</td>
											<td width="5">:</td>
											<td>RM0</td>
										</tr>
										<tr>
											<td align="right">Sum Covered Limit(Max.)</td>
											<td width="5">:</td>
											<td>RM500000</td>
										</tr>
										<tr>
											<td align="right">Direct Discount (Online)</td>
											<td width="5">:</td>
											<td>10</td>
										</tr>
										<tr>
											<td align="right">GST</td>
											<td width="5">:</td>
											<td>6</td>
										</tr>
										<hr>
										<tr>
											<td align="right">5 seater incl. driver</td>
											<td width="5">:</td>
											<td>RM61.00</td>
										</tr>
										<tr>
											<td align="right">7 seater incl. driver</td>
											<td width="5">:</td>
											<td>RM85.00</td>
										</tr>
										<tr>
											<td align="right">Stamp Duty</td>
											<td width="5">:</td>
											<td>RM10</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="col-md-6 mb-3">
								<h4 class="tex-center">Changes</h4>
								<table class="table table-striped table-warning table-hover">
									<tbody>
										<tr>
											<td align="right">Sum Covered Limit(Min.)</td>
											<td width="5">:</td>
											<td>RM0</td>
										</tr>
										<tr>
											<td align="right">Sum Covered Limit(Max.)</td>
											<td width="5">:</td>
											<td>RM500000</td>
										</tr>
										<tr>
											<td align="right">Direct Discount (Online)</td>
											<td width="5">:</td>
											<td>12</td>
										</tr>
										<tr>
											<td align="right">GST</td>
											<td width="5">:</td>
											<td>6</td>
										</tr>
										<hr>
										<tr>
											<td align="right">5 seater incl. driver</td>
											<td width="5">:</td>
											<td>RM68.00</td>
										</tr>
										<tr>
											<td align="right">7 seater incl. driver</td>
											<td width="5">:</td>
											<td>RM85.00</td>
										</tr>
										<tr>
											<td align="right">Stamp Duty</td>
											<td width="5">:</td>
											<td>RM10</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="controls tex-center">
									<button class="btn btn-success">Approve</button>
									&nbsp;&nbsp;
									<button class="btn btn-danger" data-dismiss="modal"
										aria-label="Close">Reject</button>
								</div>
							</div>
						</div>
					</section>
				</div>
				<div class="modal-footer">
					<!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</div>
	</div>



	<!-- end: modal Create-->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->



	<script type="text/javascript">
         	$(document).ready(function() {
		        	 var t = $('#admin-datatable-second_admin').DataTable( {
	         			 
		         			dom: 'Bfrtip',
		         			  buttons: [	
		{
		    extend: 'pageLength',
		    className: 'btn btn-secondary btn-sm active'
		},
		{
            extend: 'excel',
            filename: 'Etiqa Group List',
            className: 'btn btn-warning btn-sm',
            text:'Export to XLS <i class="fa fa-download" aria-hidden="true"></i>',
            
           	 exportOptions: {
                    columns: [1,2,3,4,5,6]
                }
       }
		           
		         			  ],
		         		      "searching" :false, 
		         	          "columnDefs": [ {
		         	          "searchable": false,
		         	          "orderable": false,
		         	          "targets": [0,1,2,3,4,5,6],
		         	          "bSortable": false
		         	         
		         	        } ],
		         	        "order": [[ 0, 'asc' ]]
		         	    } );
		         	 
		         	    t.on( 'order.dt search.dt', function () {
		         	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		         	            cell.innerHTML = i+1;
		         	        } );
		         	    } ).draw();
		         	           	
		         	  
		         	  // t.buttons( 0, null ).containers().appendTo( '#btnhere' );
		         	   
		         	});
         	
         	
         	
         	$('#name').keyup(function() {
         	    if($(this).val() == ''){
         	       $('#messageName').show();
         	    }else{
         	       $('#messageName').hide();
         	       $('#recordFound').hide();
         	       
         	    }
         	});
         	 
         	
         	 $("#createRoleModuleBtn").on("click", function(event) { 
	         		$('#recordFound').hide();	          		
	          		$('#messageName').hide();	
                     $('#messageModule').hide();						
	          		
						event.preventDefault();
						$.ajax({
							url : 'validateCreateRoleModule',
							type : "POST",
							data : $("#createGroupProfileForm").serialize(),
							dataType : 'json',
							success : function(response) {
								var obj = JSON.stringify(response);
						    	 
						    	 var result = $.parseJSON(obj); 
								 
								 $.each(result, function() {		  			 
  
									if(result.recordFound == true && result.count !=-1){
									
										$('#recordFound').show();
									}
									
									if(result.requiredModule == true){
										
										$('#messageModule').show();
									}
									
									
									if(result.requiredRole == true){
										
										$('#"messageRole"').show();
									}
									 
                                     if(result.recordFound == false){  
										if(result.requiredModule == false){ 
											if(result.requiredRole ==false){ 
										$('#createGroupProfileForm').attr('action', 'createUserRole');
					                    $('#createGroupProfileForm').submit();
									}
										}
										
									} 
						  		  }); 
							},
							error : function(data, status, er) {
								//alert(data + "_" + status + "_" + er);
							}
						});
					});
					
					
 
		         
         	//to reset form after close modal
         $('.modal').on('hidden.bs.modal', function(){
         	    $(this).find('form')[0].reset();  
         	   //$('input:checkbox').removeAttr('checked');
        		//$('#messageName').hide();        		
        		  
         	}); 
         
         $('#createUserGroup').on('show.bs.modal', function (event) {        	
          	    $(this).find('form')[0].reset(); 
          	 	$('#recordFound').hide(); 
	          	$('#messageRole').hide();
	    		$('#messageModule').hide();
	    		$('#recordFound').hide();
         });
       	
       	 $('#updateUserGroup').on('show.bs.modal', function (event) {
       	 var modal = $(this);	 
       	var button = $(event.relatedTarget) // Button that triggered the modal
  	  var recordID = button.data('value') // Extract info from data-* attributes
  	  
     	
  	 $.ajax({  
		     type : "Get",   
		     url : "${pageContext.request.contextPath}/uam/userGroupDetail", //see in UserController.java
		     data : "id=" + recordID, 
		   dataType: "json", //set to JSON   
		     success : function(response) {
		    	var obj = JSON.stringify(response);
		    	 console.log(response);
		
		
		    	 var result = $.parseJSON(obj);
		  		  $.each(result, function() {		  			 

		  			// modal.find('.modal-title').text("Transaction Record : "+this['mptransactionid'] );
		  	    	  //modal.find('.modal-body input').val(result.name);
		  	    	  modal.find('#name').val(result.name);
		  	    	  modal.find('.modal-body select').val(result.status);
		  	    	  modal.find('#id').val(result.id);	  
		  	    	  
		  	    
		  	    	for (var i=0;i<result.count;i++){
		  	    		//checkbox checked based on result
		  	    		$('input[type=checkbox][name="accessRight'+result.moduleID[i]+'"]').filter('[value='+result.accessRight[i]+']').prop('checked', true);		  	    		  	    		
		  	    		
		  	    	}
		  	    	
		  	    	
		  		  });
		     },  
		     error : function(e) {  
		      alert('Error: ' + e);   
		     }  
		    });  	
	  

  	  
  	  
  	  
  	  
       	 
       	})
       	 
       	// to allow only one checkbox checked
       		$('input[type="checkbox"]').on('change', function() {
       	    $('input[name="' + this.name + '"]').not(this).prop('checked', false);
       	});
       	 
      	//Start by Pramaiyan 
    	/*  $("#moduleSelect").on("change", function(event) { 
					event.preventDefault();
					$.ajax({
						url : 'fetchPermissionDesc',
						type : "POST",
						data : $("#createGroupProfileForm").serialize(),
						dataType : 'json',
						success : function(response) {
							alert("ok");
							 var obj = JSON.stringify(response);
					    	 console.log(response);
					    	 var result = $.parseJSON(obj);	 
							 $.each(result, function() {		  			 
								console.log(result.message);
                              if(result.recordFound == false){
									if(result.requiredName == false){
										if(result.requiredModule ==false){
									$('#createGroupProfileForm').attr('action', 'createUserGroupProfileAction');
				                    $('#createGroupProfileForm').submit();
								}
									}
								}  
					  		  });
						},
						error : function(data, status, er) {
							 
						}
					});
				}); */
		 //End By Pramaiyan
		 
        </script>

</body>
</html>