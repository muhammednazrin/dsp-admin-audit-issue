<%@ page import="java.io.*,java.util.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>

</head>

<body>
	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->


	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->

		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->

					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back" style="width: 100%;">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">System Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<form
												action="${pageContext.request.contextPath}/uam/setPasswordDone"
												method="post" enctype="multipart/form-data"
												id="updatepassword">
												<!--  <input type="hidden" name="action" value="changepassword">-->
												<div class="row">
													<div class="col-sm-12">
														<div class="title">
															<div class="sub">
																<h4>System Administration</h4>
															</div>
														</div>
														<div class="content-inner">
															<div class="the-box">
																<div class="row">
																	<div class="col-sm-12">
																		<div class="title">
																			<div class="sub">
																				<h4>User Set Password Link Expired</h4>
																			</div>
																		</div>
																	</div>
																	<div class="gap gap-mini"></div>
																</div>

																<div class="row">
																	<div class="col-md-8">
																		<div class="form-horizontal info-meor">
																			<div class="col-xs-12">User Set Password Link
																				Expired</div>
																			<br> <a href="agentLogin"
																				class="btn btn-warning btn-sm"><i
																				class="fa fa-edit"></i> Back</a>
																		</div>
																	</div>
																</div>



															</div>


														</div>
														<!--content inner -->
													</div>
													<!--col-sm-12 -->


												</div>
												<!--row -->
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->
		</div>
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->

	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
            <div id="back-top">
                <a href="#top"><i class="fa fa-chevron-up"></i></a>
            </div>
            -->
	<!-- END BACK TO TOP -->


	<!--
            ===========================================================
            END PAGE
            ===========================================================
            -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->

	<script>
      
    </script>

	<script type="text/javascript">     
        
            function submitForm(){
                result=true;
                if(!$('#username').val().length) {
                    $('#msg_user').removeClass('hidden');
                    $('#msg_user').addClass('visible');
                    $('#username').focus();
                    result=false;
                }
                if(!$('#newpassword').val().length) {
                    $('#msg_newpw').removeClass('hidden');
                    $('#msg_newpw').addClass('visible');
                    $('#newpassword').focus();
                    result=false;
                }
                if(!$('#confirmpassword').val().length) {
                    $('#msg_cnfpw').removeClass('hidden');
                    $('#msg_cnfpw').addClass('visible');
                    $('#confirmpassword').focus();
                    result=false;
                }
             
            }

            $('#username').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_user').removeClass('visible');
                    $('#msg_user').addClass('hidden');
                }
            });
            $('#newpassword').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_newpw').removeClass('visible');
                    $('#msg_newpw').addClass('hidden');
                }
            });
            $('#confirmpassword').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_cnfpw').removeClass('visible');
                    $('#msg_cnfpw').addClass('hidden');
                }
            });
          
            


        </script>
</body>
</html>