<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page session="true"%>

<div class="col-sm-3 col-md-2 fluid-menu">
	<div class="sidebar-left sidebar-nicescroller admin">
		<!-- desktop menu -->
		<ul class="sidebar-menu admin">
			<!--   <li>
                                        <a href="#">
                                            <span class="icon-sidebar icon services"></span>
                                            <i class="fa fa-angle-right chevron-icon-sidebar"></i>
                                            System Administration
                                        </a>
                                        <ul class="submenu" >
                                            <li><a href="#"><i class="fa fa-angle-right chevron-icon-sidebar""></i>User Roles &amp; Permission</a>
                                            <ul class="submenu" > <li><a href="userProfile">User Profile</a></ul>
                                            <ul class="submenu" > <li><a href="userRole">User Role Profile</a></ul>
                                            <ul class="submenu" > <li><a href="createRole">Create Role</a></ul>
                                            </li>
                                           
                                            
                                            <li><a href="admin-system-audit-log.html">Audit Log Maintenance</a></li>
                                            <li class="active"><a href="auditLogList">Audit Trail/Log Report</a></li>
                                             <li><a href="admin-system-log-report.html">Integration Log Report</a></li>
                                         </ul>
                                    </li>   -->
			<!--   <li>
                                        <a href="#">
                                            <span class="icon-sidebar icon services"></span>
                                            <i class="fa fa-angle-right chevron-icon-sidebar"></i>
                                             AUDIT TRAIL
                                        </a>
                                        <ul class="submenu" >
                                            <li><a href="searchUserAudit">User Management</a></li>
                                             <li><a href="searchRoleAudit">Role Management</a></li>
                                              <li><a href="searchMenuAudit">Menu Management</a></li> 
                                              <li><a href="searchLoginAudit">Login Management</a></li>
                                         
                                         </ul>
                                    </li>   -->


			<% System.out.println(session.getAttribute("level_1")); %>
			<c:forEach var="paramItem1" items="${sessionScope.level_1}">
				<li><a
					href="<c:choose> 
                                             <c:when test = "${paramItem1.url!=null}">
										           ${pageContext.request.contextPath}${paramItem1.url}
										         </c:when>  
										         <c:otherwise>
										           #
										         </c:otherwise>
                                                 </c:choose>">
						<span class="icon-sidebar icon services"></span> <i
						class="fa fa-angle-right chevron-icon-sidebar"></i> <c:out
							value='${paramItem1.name}' /> <c:if
							test="${paramItem1.id==paramItem2.parentId}">
							<c:if test="${sessionScope.level_2.size()>0}">
								<i class="fa fa-angle-right chevron-icon-sidebar"></i>
							</c:if>
						</c:if>

				</a> <c:forEach var="paramItem2" items="${sessionScope.level_2}">
						<c:if test="${paramItem1.id==paramItem2.parentId}">
							<ul class="submenu">
								<li><a
									href="<c:choose>
                                            
                                             <c:when test = "${paramItem2.url!=null}">
										           ${pageContext.request.contextPath}${paramItem2.url}
										         </c:when> 
										         <c:otherwise>
										           #
										         </c:otherwise>
                                                 </c:choose>
                                             ">
										<c:if test="${sessionScope.level_3.size()>0}">

											<c:forEach var="paramItem3" items="${sessionScope.level_3}">
												<c:if test="${paramItem2.id==paramItem3.parentId}">
													<i class="fa fa-angle-right chevron-icon-sidebar"></i>
												</c:if>
											</c:forEach>
										</c:if> <c:out value='${paramItem2.name}' />
								</a> <c:forEach var="paramItem3" items="${sessionScope.level_3}">
										<c:if test="${paramItem2.id==paramItem3.parentId}">
											<ul class="submenu">
												<li><a
													href="${pageContext.request.contextPath}${paramItem3.url}">
														<c:if test="${sessionScope.level_4.size()>0}">
															<i class="fa fa-angle-right chevron-icon-sidebar"></i>
														</c:if> <c:if test="${paramItem2.id==paramItem3.parentId}">
															<c:out value='${paramItem3.name}' />
														</c:if>
												</a> <c:forEach var="paramItem4" items="${sessionScope.level_4}">
														<c:if test="${paramItem3.id==paramItem4.parentId}">
															<ul class="submenu">
																<li><a
																	href="${pageContext.request.contextPath}${paramItem4.url}">
																		<c:out value='${paramItem4.name}' /> <c:if
																			test="${paramItem3.id==paramItem4.parentId}">
																			<c:out value='${paramItem4.name}' />
																		</c:if> <c:out value='${paramItem4.name}' />
																</a></li>
															</ul>
														</c:if>
													</c:forEach></li>
											</ul>
										</c:if>
									</c:forEach></li>
							</ul>
						</c:if>
					</c:forEach></li>
			</c:forEach>
		</ul>
	</div>
</div>