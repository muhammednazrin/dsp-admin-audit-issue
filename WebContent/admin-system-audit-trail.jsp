<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.etiqa.model.report.TransactionalReport"%>
<% java.text.DateFormat df=new java.text.SimpleDateFormat("dd/MM/yyyy"); %>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>

</head>

<body>
	<%
        
       
   	 DecimalFormat formatter = new DecimalFormat("#,###.00");
        
 if (request.getMethod().equals("GET")){
			
			session.setAttribute("ProductType", "");		
			session.setAttribute("Status", "");
			session.setAttribute("PolicyCertificateNo", "");
			session.setAttribute("NRIC","");
			session.setAttribute("ReceiptNo", "");
			session.setAttribute("dateFrom","");
			session.setAttribute("dateTo","");
			session.setAttribute("DateFrom", "");
			session.setAttribute("DateTo","");
			session.setAttribute("PlateNo","");
			session.setAttribute("ProductEntity","");
			
 }
			
        int totalRecord = 0;
        int recordPerPage =10;
        int pageCount =0;
        int startRow=0;
        int endRow=0;
        //temporary 
        endRow=recordPerPage;
        
       int start=0;       
       int end=0;
       double lastPage=0;
       String textColor="";
       String status="";
       String totalAmount="0.00";
       String transactionStatus="";
       String lastStep="";
       String nextStep="";
       String reason="";
       int i=0;
       int rowLine=0;
       String txtColor="";
       double netAmt= 0.00 ;
       double nettAmount=10.00;
       
       
       
        
        %>



	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">System Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Audit Trail/Log Report</h4>

														</div>
													</div>


												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">

														<div class="table-responsive" id="MyTable">
															<table
																class="table table-striped table-warning table-hover"
																id="admin-datatable-second_admin">
																<thead>

																	<tr>
																		<th align="center">No</th>
																		<th align="center">Module</th>
																		<th align="center">Action</th>
																		<th align="center">Action/Changed By</th>
																		<th align="center"><center>Remarks</center></th>
																		<th align="center">Created Date</th>
																	</tr>
																</thead>
																<tbody>



																	<c:forEach items="${AuditLogReport}" var="element"
																		varStatus="loop">

																		<tr>
																			<td align="center">${loop.count}</td>
																			<td align="center"><c:if
																					test="${element.moduleId == '1'}">Transactional Report</c:if></td>
																			<td align="center"><c:if
																					test="${element.action == 'E'}">Edit</c:if></td>
																			<td align="center">${element.actionby}</td>
																			<td align="center">${element.remarks}</td>
																			<td align="center">${element.createdate}</td>

																		</tr>
																	</c:forEach>

																</tbody>
															</table>
														</div>
														<!-- /.table-responsive -->
													</div>
													<!-- /.the-box -->
												</div>
												<!-- End warning color table -->
											</div>
										</div>
									</div>
									<!--row -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->

	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->



	<script type="text/javascript">
         	$(document).ready(function() {
         		
         		 $("#date_pretty").datepicker({ 
         	    });
         	    var myDate = new Date();
         	    var month = myDate.getMonth() + 1;
         	    var prettyDate =  month + '/' + myDate.getDate()  + '/' + myDate.getFullYear();
         	    $("#date_pretty").val(prettyDate);
         		
         		        		
         		$('#eib-pr').hide();
                $('#eib-list').hide();
				$('#etb-list').hide();
				$('#all-list').hide();
			
				
				var productTypeSelected = $('#productTypeValSession').val();
				var productEntitySelected = $('#productEntityValSession').val();
				
				if(productTypeSelected =="MI" || productTypeSelected =="MT")
					{
					
					$('#mi-option').show();	
					
					}
				
				else{
					
					$('#mi-option').hide();	
					
				}
				
				if(productEntitySelected =="EIB")
				{
					$('#eib-pr').show();
					$('#eib-list').show();
    				$('#etb-list').hide();
    				$('#all-list').hide();
    				
    				
				
				}
				
				if(productEntitySelected =="ETB")
				{
					$('#eib-pr').show();
					$('#eib-list').hide();
    				$('#etb-list').show();
    				$('#all-list').hide();
    				
				
				}
			
				if(productEntitySelected =="")
				{
					
					$('#eib-list').hide();
    				$('#etb-list').hide();
    				$('#all-list').hide();
    				
				
				}
			
			
		
				
				 $("#productEntity").on('change', function() {	
	        			alert(this.value);
	        			if (this.value == 'ETB')
	        			{
	        				$('#eib-pr').show();
	        				$('#eib-list').hide();
	        				$('#etb-list').show();
	        				$('#all-list').hide();
	        				$('#mi-option').hide();	
	        				plateNo.value = null;
	        				productType.value=null;
	        				
	        				//$('#mi-option').css("display","none");
	        				
	        			} 
	        			
	        			else if (this.value == 'EIB')
	        				{
	        				$('#eib-pr').show();
	        				$('#eib-list').show();
	        				$('#etb-list').hide();
	        				$('#all-list').hide();
	        				$('#mi-option').hide();	
	        				plateNo.value = null;
	        				productType.value=null;
	        				//$('#mi-option').css("display","none");
	        				
	        				}
	        			
	        			else {
	        				$('#eib-pr').hide();
	        				$('#eib-list').hide();
	        				$('#etb-list').hide();
	        				$('#all-list').hide();
	        				$('#mi-option').hide();	
	        				plateNo.value = null;
	        				productType.value=null;
	        				
	        				//$('#mi-option').css("display","none");
	        				
	        			}
	        		});
				 
				 
				 $('#productTypeEIB').on('change', function() {	
		         		//alert("ETB = "+this.value);
		         		productType.value = this.value;
		         		
		         		if(this.value == "MI"){
		         			
		         			$('#mi-option').show();	
		         			
		         			
		         		}
		         		else{
		         			
		         			$('#mi-option').hide();
		         			plateNo.value = null;
		         			
		         		}
		         			
		         		});	
		         	
		         	$('#productTypeETB').on('change', function() {	
		         		//alert("EIB = "+this.value);
		         		productType.value = this.value;
                       
		         			
if(this.value == "MT"){
		         			
		         			$('#mi-option').show();	
		         			
		         			
		         		}
		         		else{
		         			
		         			$('#mi-option').hide();
		         			plateNo.value = null;
		         		}
		         			
		         		});	
		         	
		        	$('#productTypeALL').on('change', function() {	
		         		//alert("ALL ="+this.value);
		         		productType.value = this.value;
		         			
							if(this.value == "MI" ){
									         			
							$('#mi-option').show();	
									         			
							}
							else{
									
									$('#mi-option').hide();
									plateNo.value = null;
								}
									         			
		         		});	
		         	
         		
		        	 var t = $('#admin-datatable-second_admin').DataTable( {
	         			 
		         			dom: 'Bfrtip',
		         			  buttons: [	
		{
		    extend: 'pageLength',
		    className: 'btn btn-secondary btn-sm active'
		}
		//,
		          // {
		               // extend: 'print',
		               // className: 'btn btn-warning btn-sm',
		                //title : 'Etiqa Transaction Report',
		               // text: 'Print <i class="fa fa-print" aria-hidden="true"></i>'
		           // }
		//,
		           // {
		               //extend: 'excel',
		              // filename: 'Audit Log Report',
		               //className: 'btn btn-warning btn-sm',
		               //text:'Export to XLS <i class="fa fa-download" aria-hidden="true"></i>'
		          // }
		         			  ],
		         		      "searching" :false, 
		         	          "columnDefs": [ {
		         	          "searchable": false,
		         	          "orderable": false,
		         	          "targets": [0,1,2,3,4,5],
		         	          "bSortable": false
		         	         
		         	        } ],
		         	        "order": [[ 5, 'desc' ]]
		         	    } );
		         	 
		         	    t.on( 'order.dt search.dt', function () {
		         	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		         	            cell.innerHTML = i+1;
		         	        } );
		         	    } ).draw();
		         	           	
		         	  
		         	   t.buttons( 0, null ).containers().appendTo( '#btnhere' );
		         	   
		         	});
		         
         
         	
    
        function validateForm() {
        	   var x = document.forms["txnreport"]["dateFrom"].value;
        	   var y = document.forms["txnreport"]["dateTo"].value;
        	   var z=  document.forms["txnreport"]["status"].value;
        	   var w=  document.forms["txnreport"]["productType"].value;        	   
        	   
        	   if ( x == null || x == "" ) {
        		   if(y !=""){
        	      alert("Please choose Date From");
        	      document.forms["txnreport"]["dateFrom"].focus();
        	      
        	      return false;
        	   } 
        		  
        	   }
        	   
        	   if(z=="R"|| z =="O"){
        		   
        		   if(w =="")
        			   {
        			   
        			   alert ("Please choose Product Type");
        			   document.forms["txnreport"]["productType"].focus();
        			   return false;	   
        			   }      		   
        		   
        	   }
        	   return true;
        	  
        	}
        
        
        </script>




</body>
</html>