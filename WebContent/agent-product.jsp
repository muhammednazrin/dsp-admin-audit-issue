<%@ page import="java.io.*,java.util.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<link rel="stylesheet" href="assets/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/menu.css">
<script src="assets/js/jquery.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.js" type="text/javascript"></script>
</head>

<body>
	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->


	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Cyber Agent Management</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<form action="agent" method="post"
												enctype="multipart/form-data" id="registration">
												<input type="hidden" name="action" value="save">
												<div class="row">
													<div class="col-sm-12">
														<div class="title">
															<div class="sub">
																<h4>Register Agent</h4>
															</div>
														</div>
														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-12">
																	<div class="title">
																		<div class="sub">
																			<h4>Agent Information Details</h4>
																		</div>
																	</div>
																</div>
																<div class="gap gap-mini"></div>
																<div class="col-sm-6">
																	<div class="form-horizontal info-meor">
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Reference No</label>
																			</div>
																			<div class="col-sm-9">
																				<input type="text" class="form-control"
																					name="referenceNo" required id="referenceNo">
																				<span id="msg_refno" class="hidden"
																					style="color: red; text-align: left">Please
																					enter your Reference No</span>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Agent Code</label>
																			</div>
																			<div class="col-sm-9">
																				<input type="text" class="form-control"
																					name="agentCode" required id="agentCode"> <span
																					id="msg_agentcode" class="hidden"
																					style="color: red; text-align: left">Please
																					enter your Agent Code</span>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Registration
																					Date</label>
																			</div>
																			<div class="col-sm-9">
																				<input type="text" placeholder="Date"
																					class="form-control" required
																					name="registrationDate" id="datepicker1"> <span
																					id="msg_date" class="hidden"
																					style="color: red; text-align: left">Please
																					select registration Date</span>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Annual Sales
																					Target (RM)</label>
																			</div>
																			<div class="col-sm-9">
																				<input type="text" name="annualSalesTarget"
																					class="form-control" required
																					id="annualSalesTarget"> <span
																					id="msg_salesTarget" class="hidden"
																					style="color: red; text-align: left">Please
																					enter your Annual Sales Target</span>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Discount</label>
																			</div>
																			<b>%</b>
																			<div class="col-sm-3">
																				<input type="text" maxlength="2"
																					class="form-control" name="agentDiscount" required
																					id="agentDiscount">

																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Agent Type</label>
																			</div>

																			<div class="col-sm-3">
																				<input type="text" maxlength="3" min="3"
																					class="form-control" name="agentType" required
																					id="agentType">

																			</div>
																		</div>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-horizontal info-meor">


																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Agent Category</label>
																			</div>
																			<div class="col-sm-9">
																				<select class="form-control" name="agentCategory"
																					required id="agentCategory"
																					onblur="checkOnBlur(this,'Please Select Your Category');"
																					onchange="checkOnChange(this);">
																					<option value=" -Please Select-">-Please
																						Select-</option>

																					<option value="Individual">Individual</option>
																					<option value="Company/Organization">
																						Company/Organization</option>

																				</select>

																			</div>
																		</div>

																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Insurance Type</label>
																			</div>
																			<div class="col-sm-9">
																				<select class="form-control" required
																					name="insuranceType" id="insuranceType"
																					onblur="checkOnBlur(this,'Please Select Your Insurance Type');"
																					onchange="checkOnChange(this);">
																					<option value=" -Please Select-">-Please
																						Select-</option>
																					<option value="Insurance">Insurance</option>
																					<option value="Takaful">Takaful</option>
																				</select>

																			</div>
																		</div>


																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Status</label>
																			</div>
																			<div class="col-sm-9">











																				<select class="form-control" required
																					name="agentStatus" id="agentStatus"
																					onblur="checkOnBlur(this,'Please Select Your Status');"
																					onchange="checkOnChange(this);">
																					<option value=" -Please Select-">-Please
																						Select-</option>
																					<option value="Permohonan Baru">New
																						Application</option>
																					<option value="Borang Permohonan Tertangguh">Pending
																						Application Form</option>
																					<option value="Belum Ditemuduga">Pending
																						Interview</option>
																					<option value="Dokumen Diterima">Documents
																						Received</option>
																					<option value="Dokumen Tidak Lengkap">Incomplete
																						Documents</option>
																					<option value="Permohonan Dilulus">Application
																						Approved</option>
																					<option value="Permohonan Ditamat">Application
																						Rejected</option>
																					<option value="Permohonan Ditolak">Application
																						End</option>
																					<option value="Ejen Ditamat">Agent End</option>
																				</select>

																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Agent Alias
																					Name</label>
																			</div>
																			<div class="col-sm-8">
																				<input type="text" required
																					class="form-control required" name="agentAliasName"
																					id="agentAliasName"> <span
																					id="msg_agentaliasname" class="hidden"
																					style="color: red; text-align: left">Please
																					key in Agent Name</span>
																			</div>
																			<span class="right tooltips"> <a
																				id="tooltips-2"> <i
																					class="fa fa-question-circle fa-lg"></i>
																			</a>
																			</span>
																			<!-- Popover 2 hidden title -->
																			<div id="tooltips-2Title" style="display: none">
																				Agent referral ID is the link management solution to
																				shorten the product referral link<br> e.g.
																				http://www.etiqa.com.my/agentname<br>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Commission</label>
																			</div>
																			<b>%</b>
																			<div class="col-sm-3">
																				<input type="text" maxlength="2"
																					class="form-control" name="agentCommission"
																					required id="agentCommission">

																			</div>

																		</div>
																	</div>
																</div>




																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Agent Logo</label>
																			</div>
																			<div class="col-sm-9">
																				<div id="fine-uploader-gallery">
																					<input type="file" name="agentLogo">
																				</div>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-3 column">
																				<label class="control-label"></label>
																			</div>
																			<div class="col-sm-9">Please upload logo with
																				this format only .jpg, jpeg, .gif and .png. Do not
																				exceed 345px width X 100px height of image size</div>
																		</div>
																	</div>
																</div>


																<div class="col-sm-12">
																	<div class="form-inline info-meor">
																		<div class="form-group" name="registrationDocument">
																			<label class="control-label col-sm-12">Agent
																				Registration Document</label>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-12">
																				<div class="checkbox">

																					<label> <input type="checkbox"
																						id="registrationDocument"
																						name="registrationDocument"
																						value="Application Form"> Application Form
																						<!-- <span id="msg_agentregdoc" class="hidden" style="color:red;text-align:left">Please select to proceed</span> -->
																					</label> <label> <input type="checkbox"
																						id="registrationDocument"
																						name="registrationDocument"
																						value="Cyber Agent Agreement"> Cyber Agent
																						Agreement <!-- <span id="msg_agentregdoc" class="hidden" style="color:red;text-align:left">Please select to proceed</span> -->
																					</label>

																				</div>
																			</div>
																		</div>


																	</div>
																</div>
															</div>

															<div class="the-box">
																<div class="col-sm-12">
																	<div class="title">
																		<div class="sub">
																			<h4>Personal Details</h4>
																		</div>
																	</div>
																</div>
																<div class="gap gap-mini"></div>
																<div class="col-sm-6">
																	<div class="form-horizontal info-meor">
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Agent Name</label>
																			</div>
																			<div class="col-sm-9">
																				<input type="text" required class="form-control"
																					name="agentName" id="agentName" /> <span
																					id="msg_agentname" class="hidden"
																					style="color: red; text-align: left">Please
																					select to proceed</span>
																			</div>
																		</div>

																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">ID Type</label>
																			</div>
																			<div class="col-sm-9 column">
																				<div class="row">
																					<div class="col-sm-4 column">
																						<select class="form-control" name="idType"
																							required
																							onblur="checkOnBlur(this,'Please Select Your ID Type');"
																							onchange="checkOnChange(this);">
																							<option value="Non-Civilian">Non-Civilian</option>
																							<option value="MyKad">MyKad</option>
																						</select> <span id="msg_idtype" class="hidden"
																							style="color: red; text-align: left">Please
																							select ID Type</span>
																					</div>
																					<div class="col-sm-8 column">
																						<input type="text" required class="form-control"
																							name="idNo" id="idNo"> <span
																							id="msg_idno" class="hidden"
																							style="color: red; text-align: left">Please
																							key in your ID number</span>
																					</div>
																				</div>
																			</div>
																		</div>


																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Phone No</label>
																			</div>
																			<div class="col-sm-9">
																				<div class="row">
																					<div class="col-sm-4">
																						<select class="form-control" name="phoneType"
																							required
																							onblur="checkOnBlur(this,'Please Select Your Phone Type');"
																							onchange="checkOnChange(this);">
																							<option value="Mobile">Mobile</option>
																							<option value="Office">Office</option>
																						</select> <span id="msg_phonetype" class="hidden"
																							style="color: red; text-align: left">Please
																							select your Phone Type</span>
																					</div>
																					<div class="col-sm-8">
																						<input type="text" required class="form-control"
																							name="phoneNO" id="phoneNo"> <span
																							id="msg_phoneno" class="hidden"
																							style="color: red; text-align: left">Please
																							key in your phone number</span>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Email</label>
																			</div>
																			<div class="col-sm-9">
																				<input type="text" required placeholder="Email"
																					class="form-control" name="email" id="email">
																				<span id="msg_email" class="hidden"
																					style="color: red; text-align: left">Please
																					key in your Email ID</span> <span id="msg_validmail"
																					class="hidden" style="color: red; font-size: bold">Please
																					Enter valid email ID</span>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-horizontal info-meor">
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Address</label>
																			</div>
																			<div class="col-sm-9">
																				<div class="form-group">
																					<div class="col-sm-12">
																						<input type="text" required class="form-control"
																							name="address1" id="address1"> <span
																							id="msg_address" class="hidden"
																							style="color: red; text-align: left">Please
																							key in your Address</span>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="col-sm-12">
																						<input type="text" class="form-control"
																							name="address2">
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="col-sm-12">
																						<input type="text" class="form-control"
																							name="address3">
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="col-sm-5">
																						<input type="text" class="form-control"
																							placeholder="Postcode" required name="postcode"
																							id="postcode"> <span id="msg_postcode"
																							class="hidden"
																							style="color: red; text-align: left">Please
																							key in your Postcode</span>
																					</div>
																					<div class="col-sm-7">
																						<select class="form-control" name="city" id="city">
																							<option>Select</option>
																							<option>Petaling Jaya</option>
																							<option>Bangsar</option>

																						</select> <span id="msg_city" class="hidden"
																							style="color: red; text-align: left">Please
																							select your city</span>
																					</div>
																				</div>

																				<div class="form-group">
																					<div class="col-sm-12">
																						<select class="form-control" name="state"
																							id="state">
																							<option value="">Select</option>
																							<option value="Selangor">Selangor</option>
																							<option value="WP Labuan">WP Labuan</option>
																							<option value="Negeri Sembilan">Negeri
																								Sembilan</option>
																							<option value="Perlis">Perlis</option>
																							<option value="Kelantan">Kelantan</option>
																							<option value="Terengganu">Terengganu</option>
																							<option value="Pahang">Pahang</option>
																							<option value="Kedah">Kedah</option>
																							<option value="WP Kuala Lumpur">WP Kuala
																								Lumpur</option>
																							<option value="WP Putrajaya">WP
																								Putrajaya</option>
																							<option value="Sarawak">Sarawak</option>
																							<option value="Johor">Johor</option>
																							<option value="Perak">Perak</option>
																							<option value="Pulau Pinang">Pulau
																								Pinang</option>
																						</select> <span id="msg_state" class="hidden"
																							style="color: red; text-align: left">Please
																							select your state</span>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="col-sm-12">
																						<select class="form-control" name="country">
																							<option value="Malaysia" selected="">Malaysia</option>
																						</select>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>

														</div>
														<!--content inner -->
													</div>
													<!--col-sm-12 -->

													<div class="col-sm-12">
														<div class="text-right">
															<input class="btn btn-warning btn-sm"
																onClick="submitRegistration();" type="submit"
																value="Submit" />
														</div>
													</div>
													<!--col-sm-12 -->
												</div>
												<!--row -->
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->
		</div>
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->

	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
            <div id="back-top">
                <a href="#top"><i class="fa fa-chevron-up"></i></a>
            </div>
            -->
	<!-- END BACK TO TOP -->


	<!--
            ===========================================================
            END PAGE
            ===========================================================
            -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->

	<script>
        $('#fine-uploader-gallery').fineUploader({
            template: 'qq-template-gallery',
            request: {
                endpoint: '/server/uploads'
            },
            thumbnails: {
                placeholders: {
                    waitingPath: '/source/placeholders/waiting-generic.png',
                    notAvailablePath: '/source/placeholders/not_available-generic.png'
                }
            },
            validation: {
                allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
            }
        });
    </script>

	<script type="text/javascript">
            function submitRegistration(){
                result=true;
                if(!$('#referenceNo').val().length) {
                    $('#msg_refno').removeClass('hidden');
                    $('#msg_refno').addClass('visible');
                    $('#referenceNo').focus();
                    result=false;
                }
                if(!$('#agentCode').val().length) {
                    $('#msg_agentcode').removeClass('hidden');
                    $('#msg_agentcode').addClass('visible');
                    $('#agentCode').focus();
                    result=false;
                }
                if(!$('#datepicker1').val().length) {
                    $('#msg_date').removeClass('hidden');
                    $('#msg_date').addClass('visible');
                    $('#datepicker1').focus();
                    result=false;
                }
                if(!$('#annualSalesTarget').val().length) {
                    $('#msg_salesTarget').removeClass('hidden');
                    $('#msg_salesTarget').addClass('visible');
                    $('#annualSalesTarget').focus();
                    result=false;
                }
                if($('#agentCategory').index()== 0) {
                    $('#msg_agentcat').removeClass('hidden');
                    $('#msg_agentcat').addClass('visible');
                    $('#agentCategory').focus();
                    result=false;
                }
                if($('#insuranceType').index()== 0) {
                    $('#msg_instype').removeClass('hidden');
                    $('#msg_instype').addClass('visible');
                    $('#insuranceType').focus();
                    result=false;
                }
                if($('#agentStatus').index()== 0) {
                    $('#msg_agentstatus').removeClass('hidden');
                    $('#msg_agentstatus').addClass('visible');
                    $('#agentStatus').focus();
                    result=false;
                }
                if(!$('#agentAliasName').val().length) {
                    $('#msg_agentaliasname').removeClass('hidden');
                    $('#msg_agentaliasname').addClass('visible');
                    $('#agentAliasName').focus();
                    result=false;
                }
                if(!$('input[name=products]:checked').val()) {                     
                    $('#msg_productselect').removeClass('hidden');
                    $('#msg_productselect').addClass('visible');
                    $('#products').focus();
                    result=false;
                }
                if(!$('input[name=registrationDocument]:checked').val()) {                     
                    $('#msg_agentregdoc').removeClass('hidden');
                    $('#msg_agentregdoc').addClass('visible');
                    $('#registrationDocument').focus();
                    result=false;
                }
                if(!$('#agentName').val().length) {
                    $('#msg_agentname').removeClass('hidden');
                    $('#msg_agentname').addClass('visible');
                    $('#agentName').focus();
                    result=false;
                }
                if($('#idType').index()== 0) {
                    $('#msg_idtype').removeClass('hidden');
                    $('#msg_idtype').addClass('visible');
                    $('#idType').focus();
                    result=false;
                }
                if(!$('#idNo').val().length) {
                    $('#msg_idno').removeClass('hidden');
                    $('#msg_idno').addClass('visible');
                    $('#idNo').focus();
                    result=false;
                }
                if($('#phoneType').index()== 0) {
                    $('#msg_phonetype').removeClass('hidden');
                    $('#msg_phonetype').addClass('visible');
                    $('#phoneType').focus();
                    result=false;
                }
                if(!$('#phoneNo').val().length) {
                    $('#msg_phoneno').removeClass('hidden');
                    $('#msg_phoneno').addClass('visible');
                    $('#phoneNo').focus();
                    result=false;
                }
                if(!$('#email').val().length) {
                    $('#msg_email').removeClass('hidden');
                    $('#msg_email').addClass('visible');
                    $('#email').focus();
                    result=false;
                }
                if(!$('#address1').val().length) {
                    $('#msg_address').removeClass('hidden');
                    $('#msg_address').addClass('visible');
                    $('#address1').focus();
                    result=false;
                }
                if(!$('#postcode').val().length) {
                    $('#msg_postcode').removeClass('hidden');
                    $('#msg_postcode').addClass('visible');
                    $('#postcode').focus();
                    result=false;
                }
             //   if($('#city').index()== 0) {
              //      $('#msg_city').removeClass('hidden');
              ///      $('#msg_city').addClass('visible');
               //     $('#city').focus();
               //     result=false;
              //  }
             //   if($('#state').index()== 0) {
                //    $('#msg_state').removeClass('hidden');
                //    $('#msg_state').addClass('visible');
                 //   $('#state').focus();
                 //   result=false;
               // }
            }

            $('#referenceNo').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_refno').removeClass('visible');
                    $('#msg_refno').addClass('hidden');
                }
            });
            $('#agentCode').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_agentcode').removeClass('visible');
                    $('#msg_agentcode').addClass('hidden');
                }
            });
            $('#datepicker1').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_date').removeClass('visible');
                    $('#msg_date').addClass('hidden');
                }
            });
            $('#annualSalesTarget').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_salesTarget').removeClass('visible');
                    $('#msg_salesTarget').addClass('hidden');
                }
            });
            $('#agentCategory').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_agentcat').removeClass('visible');
                    $('#msg_agentcat').addClass('hidden');
                }
            });
            $('#insuranceType').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_instype').removeClass('visible');
                    $('#msg_instype').addClass('hidden');
                }
            });
            $('#agentStatus').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_agentstatus').removeClass('visible');
                    $('#msg_agentstatus').addClass('hidden');
                }
            });
            $('#agentAliasName').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_agentaliasname').removeClass('visible');
                    $('#msg_agentaliasname').addClass('hidden');
                }
            });
            $('#products').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_productselect').removeClass('visible');
                    $('#msg_productselect').addClass('hidden');
                }
            });
            $('#registrationDocument').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_agentregdoc').removeClass('visible');
                    $('#msg_agentregdoc').addClass('hidden');
                }
            });
            $('#agentName').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_agentname').removeClass('visible');
                    $('#msg_agentname').addClass('hidden');
                }
            });
            $('#idType').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_idtype').removeClass('visible');
                    $('#msg_idtype').addClass('hidden');
                }
            });
            $('#idNo').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_idno').removeClass('visible');
                    $('#msg_idno').addClass('hidden');
                }
            });
            $('#phoneType').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_phonetype').removeClass('visible');
                    $('#msg_phonetype').addClass('hidden');
                }
            });
            $('#phoneNo').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_phoneno').removeClass('visible');
                    $('#msg_phoneno').addClass('hidden');
                }
            });
            $('#email').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_email').removeClass('visible');
                    $('#msg_email').addClass('hidden');
                }
                if (validateEmail(this.value)) {
                    $('#msg_validmail').removeClass('visible');
                    $('#msg_validmail').addClass('hidden');
                } 
            });
            $('#address1').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_address').removeClass('visible');
                    $('#msg_address').addClass('hidden');
                }
            });
            $('#postcode').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_postcode').removeClass('visible');
                    $('#msg_postcode').addClass('hidden');
                }
            });
            $('#city').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_city').removeClass('visible');
                    $('#msg_city').addClass('hidden');
                }
            });
            $('#state').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_state').removeClass('visible');
                    $('#msg_state').addClass('hidden');
                }
            });

            

            $(document).ready(function() {
                $('#postcode').blur(function(e) {
                    if (validatePhone('postcode')) {
                       //alert('valid');
                        $('#msg_postcode').removeClass('visible');
                        $('#msg_postcode').addClass('hidden');
                    }
                    else {
                    //  alert('invalid');
                        $('#msg_postcode').removeClass('hidden');
                        $('#msg_postcode').addClass('visible');
                    }
                });
            });
            // Force Numeric Only 
            jQuery.fn.ForceNumericOnly = function() {
                return this.each(function() {
                    $(this).keydown(function(e) {
                        var key = e.charCode || e.keyCode || 0;
                        // allow backspace, tab, delete, arrows, letters, numbers and keypad numbers ONLY
                        return (
                            key == 8 || 
                            key == 9 ||
                            key == 46 ||
                            key == 32 ||
                            (key >= 48 && key <= 57));                
                    })
                })
            };
            // Function Numbers
            //for (var i=1;i<=counter;i++){
            /*$("#email").validateEmail();*/
            $("#referenceNo").ForceNumericOnly();
           // $("#agentCode").ForceNumericOnly();
            $("#annualSalesTarget").ForceNumericOnly();
            $("#idNo").ForceNumericOnly();
            $("#phoneNo").ForceNumericOnly();
            $("#postcode").ForceNumericOnly();
            //}

            /////////////// Fetching State from Postcode ////////////////  
            /*function fetch_state() { 
                //alert(document.getElementById("postcode").value);
                var obj;
                var arr = [];
                var formData = {postcode:document.getElementById("postcode").value}; //Array 
                if (document.getElementById("postcode").value.length != 0 && document.getElementById("postcode").value.length >= 4) {
                    $.ajax({
                        type: "post", 
                        url: "/getonline/sites/REST/controller/TravellerController/fetch_State",
                        data: formData,
                        success: function (data) {
                            if(data) {
                                document.getElementById("state").value=data;
                            }

                        },
                        error: function (request, status, error) {
                            alert(request.responseText);
                        }
                    }); 
                } 
            }*/
            function validateEmail(sEmail) {
                var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if (filter.test(sEmail)) {
                    return true;
                }
                else {
                    return false;
                }
            }

            function validatePhone(postcode) {
                var a = document.getElementById(postcode).value;
                var filter = /^[0-9-+]+$/;
                if (filter.test(a)) {
                    return true;
                }
                else {
                    return false;
                }
            }

            function checkOnBlur(me, message) {
                var  e_id = $(me).attr('id');
                if (!(me.value).length) {
                    var exist= document.getElementById(e_id+ '_e');
                    if (exist == null) {
                        var htmlString="";
                        htmlString+='<label id='+ e_id+ '_e' + '  ' + 'class="error">'+ message + '</label>';
                        $(htmlString).insertAfter("#"+ e_id);
                        $('#'+ e_id).focus();
                    }
                }
            }
                //////////////////////////////////////////////////////////////////////////////////////////////  
            /*function checkKeyUp(me) {
                var  e_id = $(me).attr('id');
                if($.trim(me.value).length) {
                    var exist= document.getElementById(e_id+ '_e');
                    if (exist != null) {
                        $('#'+ e_id + '_e').remove();
                    }
                }
            }*/
            //////////////////////////////////////////////////////////////////////////////////////////////
            function checkOnChange(me) {
                var  e_id = $(me).attr('id');
                if($.trim(me.value).length) {
                    var exist= document.getElementById(e_id+ '_e');
                    if (exist != null) {
                        $('#'+ e_id + '_e').remove();
                    }
                }
            }

            /*var js = jQuery.noConflict();
            js(document).ready(function() {
                js("#registration").validate({
                    rules: {
                        phoneType: {
                            required: true
                        }
                    },
                    messages: {
                        phoneType: {
                            required: "You must select a timezone"
                        }
                    }

                });
            });*/

        </script>
</body>
</html>