<%@ page import="java.util.List"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ page import="javax.sql.DataSource"%>
<%@ page import="java.util.*"%><%@ page import="javax.json.JsonArray"%>
<%@ page import="javax.json.JsonObject"%><%@ page
	import="javax.json.JsonReader"%>
<%@ page import="javax.json.*"%><%@ page import="java.io.BufferedReader"%>
<%@ page import="java.sql.CallableStatement"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.ResultSet"%>
<%@ page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="/siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style type="text/css">
.m-0 {
	margin: 0 !important
}

.mt-0, .my-0 {
	margin-top: 0 !important
}

.mr-0, .mx-0 {
	margin-right: 0 !important
}

.mb-0, .my-0 {
	margin-bottom: 0 !important
}

.ml-0, .mx-0 {
	margin-left: 0 !important
}

.m-1 {
	margin: .25rem !important
}

.mt-1, .my-1 {
	margin-top: .25rem !important
}

.mr-1, .mx-1 {
	margin-right: .25rem !important
}

.mb-1, .my-1 {
	margin-bottom: .25rem !important
}

.ml-1, .mx-1 {
	margin-left: .25rem !important
}

.m-2 {
	margin: .5rem !important
}

.mt-2, .my-2 {
	margin-top: .5rem !important
}

.mr-2, .mx-2 {
	margin-right: .5rem !important
}

.mb-2, .my-2 {
	margin-bottom: .5rem !important
}

.ml-2, .mx-2 {
	margin-left: .5rem !important
}

.m-3 {
	margin: 1rem !important
}

.mt-3, .my-3 {
	margin-top: 1rem !important
}

.mr-3, .mx-3 {
	margin-right: 1rem !important
}

.mb-3, .my-3 {
	margin-bottom: 1rem !important
}

.ml-3, .mx-3 {
	margin-left: 1rem !important
}

.m-4 {
	margin: 1.5rem !important
}

.mt-4, .my-4 {
	margin-top: 1.5rem !important
}

.mr-4, .mx-4 {
	margin-right: 1.5rem !important
}

.mb-4, .my-4 {
	margin-bottom: 1.5rem !important
}

.ml-4, .mx-4 {
	margin-left: 1.5rem !important
}

.m-5 {
	margin: 3rem !important
}

.mt-5, .my-5 {
	margin-top: 3rem !important
}

.mr-5, .mx-5 {
	margin-right: 3rem !important
}

.mb-5, .my-5 {
	margin-bottom: 3rem !important
}

.ml-5, .mx-5 {
	margin-left: 3rem !important
}

.p-0 {
	padding: 0 !important
}

.pt-0, .py-0 {
	padding-top: 0 !important
}

.pr-0, .px-0 {
	padding-right: 0 !important
}

.pb-0, .py-0 {
	padding-bottom: 0 !important
}

.pl-0, .px-0 {
	padding-left: 0 !important
}

.p-1 {
	padding: .25rem !important
}

.pt-1, .py-1 {
	padding-top: .25rem !important
}

.pr-1, .px-1 {
	padding-right: .25rem !important
}

.pb-1, .py-1 {
	padding-bottom: .25rem !important
}

.pl-1, .px-1 {
	padding-left: .25rem !important
}

.p-2 {
	padding: .5rem !important
}

.pt-2, .py-2 {
	padding-top: .5rem !important
}

.pr-2, .px-2 {
	padding-right: .5rem !important
}

.pb-2, .py-2 {
	padding-bottom: .5rem !important
}

.pl-2, .px-2 {
	padding-left: .5rem !important
}

.p-3 {
	padding: 1rem !important
}

.pt-3, .py-3 {
	padding-top: 1rem !important
}

.pr-3, .px-3 {
	padding-right: 1rem !important
}

.pb-3, .py-3 {
	padding-bottom: 1rem !important
}

.pl-3, .px-3 {
	padding-left: 1rem !important
}

.p-4 {
	padding: 1.5rem !important
}

.pt-4, .py-4 {
	padding-top: 1.5rem !important
}

.pr-4, .px-4 {
	padding-right: 1.5rem !important
}

.pb-4, .py-4 {
	padding-bottom: 1.5rem !important
}

.pl-4, .px-4 {
	padding-left: 1.5rem !important
}

.p-5 {
	padding: 3rem !important
}

.pt-5, .py-5 {
	padding-top: 3rem !important
}

.pr-5, .px-5 {
	padding-right: 3rem !important
}

.pb-5, .py-5 {
	padding-bottom: 3rem !important
}

.pl-5, .px-5 {
	padding-left: 3rem !important
}

input[type=file] {
	display: inline-block;
}

.btn-grey {
	color: #fff;
	background-color: #676c72;
	border-color: none;
	border-radius: 4px;
	padding: .62rem 2rem;
}

.btn-grey:hover, .btn-remove:hover {
	color: white;
}

.addons .panel-heading {
	padding: 0;
	border: none;
}

.addons .panel-title a {
	display: block;
	padding: 2rem 2rem 2rem;
	background-color: #eaeaea;
	font-size: 1.6rem;
}

.addons .panel-title a:hover {
	text-decoration: none;
}

.addons .panel-body {
	background-color: #dadada;
}

.addons .panel-body .input-group-addon, .addons .panel-body .form-control[disabled]
	{
	background: transparent;
}

.addons .btn-edit {
	cursor: pointer;
}

.more-less {
	float: right;
	color: #212121;
	right: 0;
}

.input-group-addon select.form-control {
	width: 100%;
	min-width: 250px;
}

.addons a.btn {
	width: 100%;
}

.addons>.input-group {
	border-bottom: 1px solid #ccc;
}

.addons .input-group-addon {
	border: none;
	background: #eaeaea;
	border-radius: 0;
}

.input-group-addon.dropdown {
	vertical-align: inherit;
	padding-right: .2rem;
}
</style>
</head>
<body>
	<div class="wrapper inner">
		<!-- header -->
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="/pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>

		<!-- end header -->

		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 bg-white">
						<div class="row">
							<form
								action="${pageContext.request.contextPath}/updateMotorAddOn"
								method="POST" id="updateMotorAddOn" name="updateMotorAddOn">
								<div>
									<!-- breadcrum -->
									<div class="breadcrum-grey">
										<div class="row">
											<div class="col-sm-12">
												<!-- Begin breadcrumb -->
												<ol class="breadcrumb">
													<li><a href="#fakelink"> Business Administration >
															Add On Management</a></li>
												</ol>
												<!-- End breadcrumb -->
											</div>
										</div>
									</div>
									<!-- breadcrum -->
								</div>
								<div class="col-sm-11 gap-mid p-5">
									<div class="row">
										<div class="col-sm-12">
											<div class="row">

												<div class="col-sm-4">
													<label class="control-label pl-4 pt-2">Choose
														Entity</label>
													<div class="form-group">

														<%--  <c:out value="${SequenceDropdown}"/>
                          <c:forEach var="m1" items="${Subcatagorylist}" >
                          <c:out value="${m1.CATEGORY_CODE}" />
                          
                          <c:out value="${m1.CATEGORY_DISPLAY}" />
                          <c:out value="${m1.STATUS}" />
                          <c:out value="${m1.CATEGORY_CODE}" />
                          <c:out value="${m1.CATEGORY_DISPLAY}" />   
                            <c:if test="${m1.additionalCoverCode == 'D003'}">
						    		  <c:out value="${m1.id}" />
			                         <c:out value="${m1.toolTip}" />
			                         <c:out value="${m1.additionalCoverCode}" />
						  </c:if>
							  <c:if test='${fn:contains(m1.additionalCoverCode, "D003")}'>
								       <c:out value="${m1.additionalCoverName}" />
								</c:if>
                       
                          </c:forEach>    --%>
														<select id="Entity" name="Entity" class="form-control">
															<option value="EIB">Insurance</option>
															<option value="ETB">Takaful</option>
														</select>
													</div>
												</div>
												<!--  <div class="col-sm-4">
                          <div class="form-group">                         
			                <div class="btn btn-grey" data-dismiss="modal" >Search</div>
			              </div>                          
                          </div> -->
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="col-sm-12" style="color: red">
										<h5>
											<i><b><c:out value="${errormessage} " /></b></i>
										</h5>
									</div>
									<div class="row">
										<div class="col-sm-12 addons">
											<c:forEach var="m1" items="${addtional}">
												<%-- <c:forEach items="${Sequence}"  varStatus="theCount_p"> --%>
												<%-- <c:set var = "theCount_p"  value = "${m1.CATEGORY_DISPLAY}"/> --%>
												<div class="input-group">
													<span class="input-group-addon"> <input
														type="checkbox" id="checkbox2" name="checkbox2"
														value="${m1.CATEGORY_CODE}"
														<c:if test="${m1.STATUS == 'ACTIVE'}">checked="checked"</c:if>
														aria-label="...">
													</span>
													<div class="panel-heading">
														<h4 class="panel-title">
															<input id="CATEGORY_CODE" name="CATEGORY_CODE"
																type="hidden" value="${m1.CATEGORY_CODE}" /> <a
																data-toggle="collapse"
																data-parent="#accordion${m1.SEQUENCE_COUNT}"
																href="#collapseOne${m1.SEQUENCE_COUNT}"> <!-- <a class="btn btn-addon collapsed" role="button" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> -->
																&nbsp; &nbsp;<c:out value="${m1.CATEGORY_NAME}" /> <i
																class="more-less glyphicon glyphicon-plus"></i></a>
														</h4>
													</div>
													<a href="javascript:void(0)" class="input-group-addon edit"
														id="edit"
														onclick="calleditModal('<c:out value='${m1.CATEGORY_CODE}'/>')"><span
														class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
													<span class="input-group-addon"> <select id="seq2"
														name="seq2" class="form-control">
															<option value="Select Sequence">Select Sequence</option>
															<c:forEach items="${SequenceDropdown}" var="val">
																<option value="${val}"
																	${m1.CATEGORY_DISPLAY == val ? 'selected' : ' '}><c:out
																		value="${val}"></c:out></option>
															</c:forEach>
													</select>
													</span>
												</div>
												<div id="collapseOne${m1.SEQUENCE_COUNT}"
													class="panel-collapse collapse">
													<%-- <c:if test="${ m1.ADDONS_OPTION  =='M'}"> --%>
													<div class="panel-body">
														<c:forEach var="m2" items="${Subcatagorylist}">
															<c:if test="${ m2.CATEGORY_CODE  == m1.CATEGORY_CODE}">
																<c:forEach var="m3" items="${m2.CATEGORY_CODE}">
																	<div class="row">
																		<div class="col-sm-9 col-sm-offset-3 mt-4">
																			<div class="input-group">
																				<span class="input-group-addon"> <input
																					id="ADDITIONAL_COVER_CODE"
																					name="ADDITIONAL_COVER_CODE" type="hidden"
																					value="${m2.ADDITIONAL_COVER_CODE}" /> <input
																					class="form-check-input" type="checkbox"
																					id="checkbox2_1" name="checkbox2_1"
																					value="${m2.ADDITIONAL_COVER_CODE}"
																					<c:if test="${m2.STATUS == 'ACTIVE'}">checked="checked"</c:if>></span>
																				<!-- disabled="true" -->
																				<label class="form-control"><c:out
																						value="${m2.ADDITIONAL_COVER_NAME}" /></label> <a
																					href="javascript:void(0)"
																					class="input-group-addon edit" id="SUBMODAL"
																					onclick="calleditSUBModal('<c:out value='${m2.ADDITIONAL_COVER_CODE}'/>')"><span
																					class="glyphicon glyphicon-pencil"
																					aria-hidden="true"></span></a> <span
																					class="input-group-addon dropdown"> <select
																					id="seq3" name="seq3" class="form-control">
																						<!-- disabled="" -->
																						<option value="Select Sequence">Select
																							Sequence</option>
																						<c:forEach items="${subsequence}" var="vale">
																							<option value="${vale}"
																								${m2.ADDONS_DISPLAY == vale ? 'selected' : ' '}><c:out
																									value="${vale}"></c:out></option>
																						</c:forEach>
																				</select>
																				</span>
																			</div>
																		</div>
																	</div>
																</c:forEach>
															</c:if>
														</c:forEach>
													</div>
													<%-- </c:if> --%>
												</div>
											</c:forEach>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12 gap-mid text-center">
											<button type="submit" class="btn btn-grey">Save</button>
											<div class="btn btn-grey" onclick="calleditModal('hello')">Back</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.page-content -->
	<!-- BEGIN FOOTER -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- END FOOTER -->
	</div>
	<!-- /.wrapper -->
	<div class="modal fade" id="edit-modal" tabindex="-1" role="dialog"
		data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<%-- <form class="form-horizontal" action="${pageContext.request.contextPath}/updateMotorAddOnpopup" method="POST" id="updateMotorAddOnpopup" name="updateMotorAddOnpopup" > --%>
				<div class="modal-header">
					<h3 class="modal-title">Update Information</h3>
				</div>
				<div class="modal-body text-center">
					<div class="row">
						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-6 pl-4 text-left">
									<div class="form-group"></div>
								</div>
								<div class="col-sm-6 pl-4 text-left">
									<div class="form-group"></div>
								</div>
								<input id="CATEGORY_CODE" name="CATEGORY_CODE" type="hidden" />
								<div class="col-sm-12 pl-4 text-left">
									<div class="form-group">
										<label for="comment">Name EN:</label>
										<textarea class="form-control" rows="1" name="nameEn"
											id="nameEn"></textarea>
									</div>
								</div>
								<div class="col-sm-12 pl-4 text-left">
									<div class="form-group">
										<label for="comment">Name BM:</label>
										<textarea class="form-control" rows="1" name="nameBm"
											id="nameBm"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="row">
						<div class="col-sm-12 gap-mid text-center mt-2 mb-2">
							<button type="submit" class="btn btn-grey" data-dismiss="modal"
								aria-label="Close" id="updateAdditionalBen">Save</button>
						</div>
					</div>
				</div>
				<%--  </form>  --%>
			</div>
		</div>
	</div>
	<!-- SUBCATAGORY MODAL -->
	<div class="modal fade" id="SUB-MODAL" tabindex="-1" role="dialog"
		data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<%-- <form class="form-horizontal" action="${pageContext.request.contextPath}/updateMotorAddOnpopup" method="POST" id="updateMotorAddOnpopup" name="updateMotorAddOnpopup" > --%>
				<div class="modal-header">
					<h3 class="modal-title">Update Information</h3>
				</div>
				<div class="modal-body text-center">
					<div class="row">
						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-6 pl-4 text-left">
									<div class="form-group">
										<!-- <label for="comment">Status</label>  
                        <select id="seq4" name="seq4" class="form-control">
	                          <option value="Select Sequence">Select Status</option>
	                              <option value="1">Active</option>
	                              <option value="0">Inactive</option>
	                            </select>   -->
									</div>
								</div>
								<div class="col-sm-6 pl-4 text-left">
									<div class="form-group">
										<!-- <label for="comment">Sequence</label>        
                        <select id="seq4" name="seq4" class="form-control">
	                          <option value="Select Sequence">Select Sequence</option>
	                              <option value="1">1</option>
	                              <option value="2">2</option>
	                              <option value="3">3</option>
	                              <option value="4">4</option>
	                            </select>  -->
									</div>
								</div>
								<!--  <div class="col-sm-12 pl-4 text-left">
                     <div class="form-group">
                      <label for="comment">VPMS Cover</label>        
                        <input type="text" name="vpmscode" id="vpmscode" class="form-control" aria-label="Amount (to the nearest dollar)" value="VPMS Code" disabled="" id="use-cover">           
                    </div> 
                  </div> -->
								<div class="col-sm-12 pl-4 text-left">
									<div class="form-group">
										<input id="SUB_productCode" name="SUB_productCode"
											type="hidden" /> <label for="comment">Name EN:</label>
										<textarea class="form-control" rows="1" name="nameEnSB"
											id="nameEnSB"></textarea>
									</div>
								</div>
								<div class="col-sm-12 pl-4 text-left">
									<div class="form-group">
										<label for="comment">Name BM:</label>
										<textarea class="form-control" rows="1" name="nameBmSB"
											id="nameBmSB"></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12 pl-4 text-left">
									<div class="form-group">
										<label for="comment">Tool Tip English:</label>
										<textarea class="form-control" rows="4" name="toolTipEnSB"
											id="toolTipEnSB"></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12 pl-4 text-left">
									<div class="form-group">
										<label for="comment">Tool Tip Bahasa Malay:</label>
										<textarea class="form-control" rows="4" name="toolTipBmSB"
											id="toolTipBmSB"></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12 pl-4 text-center">
									<div class="form-group">
										<label for="comment">Sum Covered</label><br> <label
											for="comment">Min Value</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="text" name="minValueSB" id="minValueSB"><br>
										<br> <label for="comment">Max Value</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="text" name="maxValueSB" id="maxValueSB"><br>
										<br> <label for="comment">Default Value</label>&nbsp;&nbsp;
										<input type="text" name="DefultValueSB" id="DefultValueSB">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="row">
						<div class="col-sm-12 gap-mid text-center mt-2 mb-2">
							<button type="submit" class="btn btn-grey" data-dismiss="modal"
								aria-label="Close" id="updateAdditionalBenSB">Save</button>
						</div>
					</div>
				</div>
				<%--  </form>  --%>
			</div>
		</div>
	</div>
	<!-- END PAGE CONTENT -->
	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->
	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->
	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<script
		src="<c:url value="/resources/assets/js/jquery-2.2.4.min.js" />"></script>
	<!--  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
	<script src="<c:url value="/resources/assets/js/apps.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/retina/retina.min.js" />"></script>
	<script
		src="<c:url value="/resources/assets/plugins/nicescroll/jquery.nicescroll.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/slimscroll/jquery.slimscroll.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/backstretch/jquery.backstretch.min.js"/>"></script>


	<!-- PLUGINS -->
	<script
		src="<c:url value="/resources/assets/plugins/owl-carousel/owl.carousel.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/prettify/prettify.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"/>"></script>

	<script
		src="<c:url value="/resources/assets/plugins/chosen/chosen.jquery.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/icheck/icheck.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/datepicker/bootstrap-datepicker.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/timepicker/bootstrap-timepicker.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/mask/jquery.mask.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/validator/bootstrapValidator.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/datatable/js/jquery.dataTables.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/datatable/js/bootstrap.datatable.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/datatable/js/jquery.highlight.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/datatable/js/dataTables.searchHighlight.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/summernote/summernote.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/markdown/markdown.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/markdown/to-markdown.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/markdown/bootstrap-markdown.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/slider/bootstrap-slider.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/toastr/toastr.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/newsticker/jquery.newsTicker.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/placeholder/jquery.placeholder.js"/>"></script>

	<script type="text/javascript"
		src="<c:url value="/resources/assets/js/shieldui-all.min.js"/>"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/assets/js/jszip.min.js"/>"></script>

	<script
		src="<c:url value="/resources/assets/plugins/jquery-knob/jquery.knob.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/jquery-knob/knob.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/js/jquery.dataTables.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/js/dataTables.bootstrap.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/js/dataTables.buttons.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/js/buttons.bootstrap.min.js"/>"></script>
	<script src="<c:url value="/resources/assets/js/jszip.min.js"/>"></script>
	<script src="<c:url value="/resources/assets/js/pdfmake.min.js"/>"></script>
	<script src="<c:url value="/resources/assets/js/vfs_fonts.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/js/buttons.html5.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/js/buttons.print.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/js/buttons.colVis.min.js"/>"></script>
	<script src="<c:url value="/resources/assets/js/toastr.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/jquery-knob/jquery.knob.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/jquery-knob/knob.js"/>"></script>
	<%-- <script src="<c:url value="/resources/assets/js/apps.js"/>"></script> --%>
	<script
		src="<c:url value="/resources/assets/plugins/jquery-knob/excanvas.js"/>"></script>
	<script src="<c:url value="/resources/assets/js/bootstrap.min.js"/>"></script>
	<%-- <script src="<c:url value="/resources/assets/js/apps.js"/>"></script> --%>
	<!-- <script src="js/apps.js"></script>  -->
	<!-- KNOB JS -->
	<!--[if IE]>
        <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->

	<script type="text/javascript">
         
         function calleditModal(dd){
        	// alert(dd); 
        	 $.ajax({
					url : 'fetchMotorAddOnpopupfr',
					type : "POST",
					/* data : $("#updateMotorAddOnpopup").serialize(), */
				/* 	data : $("#nameEn"), */
					data:{				
						"beniftName": dd							
						},
					//dataType : 'json',
					success : function(response) {
				    console.log(response);
				    var  obj = JSON.parse(response);
				    console.log(obj);
				    $('#nameEn').val(obj.CATEGORY_NAME_ENG);
				    $('#nameBm').val(obj.CATEGORY_NAME_BM);
				    $('#CATEGORY_CODE').val(obj.PRODUCT_CODE);
				    var PRODUCT_CODE=obj.PRODUCT_CODE;				    
				    if(PRODUCT_CODE=='WC')
				    	{
				    	$("#nameEn").prop('disabled', true);
				    	$("#nameBm").prop('disabled', true);
				    	//alert("producr");
				    	}
				    else{
				    	$("#nameEn").prop('disabled', false);
				    	$("#nameBm").prop('disabled', false);
				    }				    
					},
					error : function(data, status, er) {
						//alert(data + "_" + status + "_" + er);
					}
				}); 
        	 $('#edit-modal').modal('show');
         	//alert("helllll");
         }
         function calleditSUBModal(dd){
        	 $.ajax({
					url : 'fetchMotorAddOnpopupfr_sub',
					type : "POST",
					/* data : $("#updateMotorAddOnpopup").serialize(), */
				/* 	data : $("#nameEn"), */
					data:{				
						"beniftName": dd							
						},
					//dataType : 'json',
					success : function(response) {
				    console.log(response);
				    var  obj = JSON.parse(response);
				    console.log(obj);
				    $('#nameEnSB').val(obj.ADDITIONAL_COVER_NAME);
				    $('#nameBmSB').val(obj.ADDITIONAL_COVER_NAME1);
				    $('#toolTipEnSB').val(obj.TOOL_TIP);
				    $('#toolTipBmSB').val(obj.TOOL_TIP1);
				    $('#minValueSB').val(obj.MIN_AMOUNT);
				    $('#maxValueSB').val(obj.MAX_AMOUNT);
				    $('#DefultValueSB').val(obj.DEFAULT_AMOUNT);
				    $('#SUB_productCode').val(obj.productCode);
					},
					error : function(data, status, er) {
						//alert(data + "_" + status + "_" + er);
					}
				});
         	 $('#SUB-MODAL').modal('show');
          	//alert("helllll");
          }
       
         $("#updateAdditionalBen").on("click", function(event) {        	 
					$.ajax({
						url : 'updateMotorAddOnpopupfr',
						type : "POST",
						data:{				
							"nameEn": $('#nameEn').val(),
							"nameBm": $('#nameBm').val(),
							"productCode": $('#CATEGORY_CODE').val()							
							},
						success : function(response) {
					    console.log(response);								
						},
						error : function(data, status, er) {
							//alert(data + "_" + status + "_" + er);
						}
					}); 
				});
         $("#updateAdditionalBenSB").on("click", function(event) {        	 
				$.ajax({
					url : 'updateMotorAddOnpopupfr_SUB',
					type : "POST",
					data:{		
						"nameEnSB": $('#nameEnSB').val(),
						"nameBmSB": $('#nameBmSB').val(),
						"toolTipEnSB": $('#toolTipEnSB').val(),
						"toolTipBmSB": $('#toolTipBmSB').val(),
						"minValueSB": $('#minValueSB').val(),
						"maxValueSB": $('#maxValueSB').val(),
						"DefultValueSB": $('#DefultValueSB').val(),
						"productCode": $('#SUB_productCode').val()							
						},
					success : function(response) {
				    console.log(response);								
					},
					error : function(data, status, er) {
						//alert(data + "_" + status + "_" + er);
					}
				}); 
			});
    
         // $(function() {       
        $(".btn-addon").click(function() {
          if($(this).hasClass("collapsed") ) {
            $(this).find(".more-less").addClass("glyphicon-minus");
            $(this).find(".more-less").removeClass("glyphicon-plus");
          }
          else{
            $(this).find(".more-less").addClass("glyphicon-plus");
            $(this).find(".more-less").removeClass("glyphicon-minus");
          }
        });
      //});
        </script>
</body>
</html>