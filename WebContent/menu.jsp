<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ page import="java.io.IOException"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.text.ParseException"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.List"%>
<%@ page import="com.etiqa.DAO.RolesManagement_DAO"%>
<%@ page import="com.etiqa.admin.UserRole"%>



<%-- <%
	String strusername = "faheem";
	RolesManagement_DAO rdao = new RolesManagement_DAO();
	List<UserRole> releaseDataList = rdao.getUsersRolesMenu("faheem");
%> 
 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>ETIQA</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}
</style>
<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}
</style>
</head>
<body>
	<%
		List<UserRole> releaseDataList = null;
		if (session != null) {
			if (session.getAttribute("user") != null) {
				String name = (String) session.getAttribute("user");
				RolesManagement_DAO rdao = new RolesManagement_DAO();
				releaseDataList = rdao.getUsersRolesMenu(name);
			} else {
				response.sendRedirect("admin-login.jsp");
			}
		}
	%>

	<div class="col-sm-3 col-md-2 fluid-menu">
		<div class="sidebar-left sidebar-nicescroller admin">


			<!-- desktop menu -->
			<ul class="sidebar-menu admin">
				<% if(releaseDataList!=null){			
					for (int i = 0; i < releaseDataList.size(); i++) {
				%>

				<%=releaseDataList.get(0).getURL()%>

				<%
					}}
				%>

			</ul>
		</div>
		<!-- /.sidebar-left -->
	</div>







	<script src="assets/assets/js/jquery.min.js"></script>
	<script src="assets/assets/js/bootstrap.min.js"></script>
	<script src="assets/assets/js/bootstrap-select.min.js"></script>

	<script src="assets/assets/plugins/retina/retina.min.js"></script>
	<script src="assets/assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="assets/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script
		src="assets/assets/plugins/backstretch/jquery.backstretch.min.js"></script>


	<!-- PLUGINS -->
	<script src="assets/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="assets/assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="assets/assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script
		src="assets/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/assets/plugins/markdown/markdown.js"></script>
	<script src="assets/assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/assets/plugins/toastr/toastr.js"></script>
	<script src="assets/assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script src="assets/assets/plugins/footable-3/js/footable.js"></script>



</body>
</html>









<!--                     <div class="col-sm-3 col-md-2 fluid-menu">
                            <div class="sidebar-left sidebar-nicescroller admin">
                                desktop menu
                                <ul class="sidebar-menu admin">
                                    <li class="active">
                                        <a href="admin-dashboard.html">
                                            <span class="icon-sidebar icon dashboard"></span>
                                            Dashboard
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="icon-sidebar icon services"></span>
                                            <i class="fa fa-angle-right chevron-icon-sidebar"></i>
                                            System Administration
                                        </a>
                                        <ul class="submenu">
                                            <li><a href="admin-system-user-role.html">User Roles &amp; Permission</a></li>
                                            <li><a href="admin-system-audit-log.html">Audit Log Maintenance</a></li>
                                            <li><a href="admin-system-audit-trail.html">Audit Trail/Log Report</a></li>
                                            <li><a href="admin-system-log-report.html">Integration Log Report</a></li>
                                        </ul>
                                    </li>
                                    <li class="submenu">
                                        <a href="#">
                                            <span class="icon-sidebar icon services"></span>
                                            <i class="fa fa-angle-right chevron-icon-sidebar"></i>
                                            Business Administration
                                        </a>
                                        <ul class="submenu">
                                            <li class="">
                                                <a href="#">
                                                    <i class="fa fa-angle-right chevron-icon-sidebar2"></i>
                                                    Product Management
                                                </a>
                                                <ul class="submenu">
                                                    <li>
                                                        <a href="#">
                                                        <i class="fa fa-angle-right chevron-icon-sidebar3"></i>
                                                        Motor Insurance
                                                        </a>
                                                        <ul class="submenu">
                                                            <li>
                                                                <a href="admin-business-product-management-motor-product-infomation.html">Product Information</a></li>
                                                            <li><a href="admin-business-product-management-motor-product-rate.html">Product Rates</a></li>
                                                            <li><a href="admin-business-product-management-motor-excess.html">Excess</a></li>
                                                            <li><a href="admin-business-product-management-motor-loading.html">Loading</a></li>
                                                            <li><a href="admin-business-product-management-motor-nvic.html">NVIC Listing</a></li>
                                                            <li><a href="admin-business-product-management-motor-payment.html">Payment Method</a></li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                        <i class="fa fa-angle-right chevron-icon-sidebar3"></i>
                                                        Houseowner/Householder
                                                        </a>
                                                        <ul class="submenu visible">
                                                            <li><a href="admin-business-product-management-house-product-infomation.html">Product Information</a></li>
                                                            <li><a href="admin-business-product-management-house-product-rate.html">Product Rates</a></li>
                                                            <li class="active selected"><a href="admin-business-product-management-house-payment.html">Payment Method</a></li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                        <i class="fa fa-angle-right chevron-icon-sidebar3"></i>
                                                        World Traveller Care
                                                        </a>
                                                        <ul class="submenu">
                                                            <li><a href="admin-business-product-management-traveller-product-infomation.html">Product Information</a></li>
                                                            <li><a href="admin-business-product-management-traveller-product-rate.html">Product Rates</a></li>
                                                            <li><a href="admin-business-product-management-traveller-payment.html">Payment Method</a></li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                        <i class="fa fa-angle-right chevron-icon-sidebar3"></i>
                                                        Term Life
                                                        </a>
                                                        <ul class="submenu">
                                                            <li><a href="admin-business-product-management-term-product-infomation.html">Product Information</a></li>
                                                            <li><a href="admin-business-product-management-term-product-rate.html">Product Rates</a></li>
                                                            <li><a href="admin-business-product-management-term-payment.html">Payment Method</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a href="admin-business-list-member.html">List of Customer</a></li>
                                            <li><a href="admin-business-segmentation.html">Segmentation</a></li>
                                            <li><a href="admin-business-campaign-analysis.html">Campaign Analysis Setup</a></li>
                                            <li><a href="admin-business-straight-through.html">Rules Management</a></li>
                                            <li><a href="admin-business-discount.html">Discount Management</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="icon-sidebar icon services"></span>
                                            <i class="fa fa-angle-right chevron-icon-sidebar"></i>
                                            Agent Management
                                        </a>
                                        <ul class="submenu">
                                            <li><a href="agent-view-list-agent.html">List of Agent</a></li>
                                            <li><a href="agent-registration.html">Register Agent</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="icon-sidebar icon services"></span>
                                            <i class="fa fa-angle-right chevron-icon-sidebar"></i>
                                            Cyber Agent
                                        </a>
                                        <ul class="submenu">
                                            <li><a href="agent-view-profile.html">View Profile</a></li>
                                            <li><a href="agent-list-of-perator.html">List of Operator</a></li>
                                            <li><a href="agent-operatorregistration.html">Register Operator</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="icon-sidebar icon services"></span>
                                            <i class="fa fa-angle-right chevron-icon-sidebar"></i>
                                            Leads Management &amp; Campaign
                                        </a>
                                        <ul class="submenu">
                                            <li><a href="lead-sales-lead.html">Sales Leads Portal Registration</a></li>
                                            <li><a href="lead-sales-tools.html">Sales Tools</a></li>
                                            <li><a href="lead-manual-upload.html">Manual Upload</a></li>
                                            <li><a href="lead-abandonment-repository.html">Abandonment Repository</a></li>
                                            <li><a href="lead-campaign-banner.html">Campaign Banner</a></li>
                                            <li><a href="lead-data-download.html">Leads Data Download</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="icon-sidebar icon policy"></span>
                                            <i class="fa fa-angle-right chevron-icon-sidebar"></i>
                                            Report &amp; Analytics
                                        </a>
                                        <ul class="submenu">
                                            <li><a href="admin-report-operator.html">Operator Performance</a></li>
                                            <li><a href="admin-report-transaction.html">Transactional Report</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>/.sidebar-left
                        </div> -->