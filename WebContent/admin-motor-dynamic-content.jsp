<%@ page import="java.util.List"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ page import="javax.sql.DataSource"%>
<%@ page import="java.sql.CallableStatement"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.ResultSet"%>
<%@ page import="java.util.*"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="/siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>

<style type="text/css">
.m-0 {
	margin: 0 !important
}

.mt-0, .my-0 {
	margin-top: 0 !important
}

.mr-0, .mx-0 {
	margin-right: 0 !important
}

.mb-0, .my-0 {
	margin-bottom: 0 !important
}

.ml-0, .mx-0 {
	margin-left: 0 !important
}

.m-1 {
	margin: .25rem !important
}

.mt-1, .my-1 {
	margin-top: .25rem !important
}

.mr-1, .mx-1 {
	margin-right: .25rem !important
}

.mb-1, .my-1 {
	margin-bottom: .25rem !important
}

.ml-1, .mx-1 {
	margin-left: .25rem !important
}

.m-2 {
	margin: .5rem !important
}

.mt-2, .my-2 {
	margin-top: .5rem !important
}

.mr-2, .mx-2 {
	margin-right: .5rem !important
}

.mb-2, .my-2 {
	margin-bottom: .5rem !important
}

.ml-2, .mx-2 {
	margin-left: .5rem !important
}

.m-3 {
	margin: 1rem !important
}

.mt-3, .my-3 {
	margin-top: 1rem !important
}

.mr-3, .mx-3 {
	margin-right: 1rem !important
}

.mb-3, .my-3 {
	margin-bottom: 1rem !important
}

.ml-3, .mx-3 {
	margin-left: 1rem !important
}

.m-4 {
	margin: 1.5rem !important
}

.mt-4, .my-4 {
	margin-top: 1.5rem !important
}

.mr-4, .mx-4 {
	margin-right: 1.5rem !important
}

.mb-4, .my-4 {
	margin-bottom: 1.5rem !important
}

.ml-4, .mx-4 {
	margin-left: 1.5rem !important
}

.m-5 {
	margin: 3rem !important
}

.mt-5, .my-5 {
	margin-top: 3rem !important
}

.mr-5, .mx-5 {
	margin-right: 3rem !important
}

.mb-5, .my-5 {
	margin-bottom: 3rem !important
}

.ml-5, .mx-5 {
	margin-left: 3rem !important
}

.p-0 {
	padding: 0 !important
}

.pt-0, .py-0 {
	padding-top: 0 !important
}

.pr-0, .px-0 {
	padding-right: 0 !important
}

.pb-0, .py-0 {
	padding-bottom: 0 !important
}

.pl-0, .px-0 {
	padding-left: 0 !important
}

.p-1 {
	padding: .25rem !important
}

.pt-1, .py-1 {
	padding-top: .25rem !important
}

.pr-1, .px-1 {
	padding-right: .25rem !important
}

.pb-1, .py-1 {
	padding-bottom: .25rem !important
}

.pl-1, .px-1 {
	padding-left: .25rem !important
}

.p-2 {
	padding: .5rem !important
}

.pt-2, .py-2 {
	padding-top: .5rem !important
}

.pr-2, .px-2 {
	padding-right: .5rem !important
}

.pb-2, .py-2 {
	padding-bottom: .5rem !important
}

.pl-2, .px-2 {
	padding-left: .5rem !important
}

.p-3 {
	padding: 1rem !important
}

.pt-3, .py-3 {
	padding-top: 1rem !important
}

.pr-3, .px-3 {
	padding-right: 1rem !important
}

.pb-3, .py-3 {
	padding-bottom: 1rem !important
}

.pl-3, .px-3 {
	padding-left: 1rem !important
}

.p-4 {
	padding: 1.5rem !important
}

.pt-4, .py-4 {
	padding-top: 1.5rem !important
}

.pr-4, .px-4 {
	padding-right: 1.5rem !important
}

.pb-4, .py-4 {
	padding-bottom: 1.5rem !important
}

.pl-4, .px-4 {
	padding-left: 1.5rem !important
}

.p-5 {
	padding: 3rem !important
}

.pt-5, .py-5 {
	padding-top: 3rem !important
}

.pr-5, .px-5 {
	padding-right: 3rem !important
}

.pb-5, .py-5 {
	padding-bottom: 3rem !important
}

.pl-5, .px-5 {
	padding-left: 3rem !important
}

input[type=file] {
	display: inline-block;
}

.btn-grey {
	color: #fff;
	background-color: #676c72;
	border-color: none;
	min-width: 115px;
}

.btn-remove {
	background-color: #676c72;
	color: white;
	padding: .62rem;
}

.btn-grey:hover, .btn-remove:hover {
	color: white;
}

#insertDynamicContentManagement .form-group {
	margin: 0
}

#insertDynamicContentManagement .input-group {
	display: block
}
</style>
</head>
<body>

	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="/pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>

		<!-- end header -->

		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 bg-white">
						<div class="row">
							<form class="form-horizontal"
								action="${pageContext.request.contextPath}/insertDynamicContentManagement"
								method="POST" id="insertDynamicContentManagement"
								name="insertDynamicContentManagement"
								enctype="multipart/form-data">
								<div>
									<!-- breadcrum -->
									<div class="breadcrum-grey">
										<div class="row">
											<div class="col-sm-12">
												<!-- Begin breadcrumb -->
												<ol class="breadcrumb">
													<li><a href="#fakelink">Content Management</a></li>
												</ol>
												<!-- End breadcrumb -->
											</div>
										</div>
									</div>
									<!-- breadcrum -->
								</div>
								<div class="col-sm-11 gap-mid p-5">
									<div class="row">
										<div class="col-sm-12">
											<div class="row">
												<div class="col-sm-3">
													<div class="form-group">
														<label class="control-label pl-4 pt-2">Choose
															Entity</label> <select class="form-control" name="productEntity"
															id="productEntity">
															<option value="EIB">Insurance</option>
															<option value="ETB">Takaful</option>
														</select>
													</div>
												</div>
												<div class="col-sm-3" id="Sales-funnel-EIB">
													<div class="form-group">
														<label class="control-label pl-4 pt-2">Sales
															funnel</label> <select class="form-control" name="productTypeEIB"
															id="productTypeEIB">
															<option value="MI">Motor</option>
															<option value="WTC">World Traveller Care</option>
															<option value="HOHH">Houseowner/Householder</option>
															<option value="TL">EzyLife Secure</option>
														</select>
													</div>
												</div>
												<div class="col-sm-3" id="Sales-funnel-ETB">
													<div class="form-group">
														<label class="control-label pl-4 pt-2">Sales
															funnel</label> <select class="form-control" name="productTypeETB"
															id="productTypeETB">
															<option value="MT">Motor</option>
															<option value="TPT">World Traveller Care</option>
															<option value="HOHH-ETB">Houseowner/Householder</option>
														</select>
													</div>
												</div>
												<div class="col-sm-3">
													<div class="form-group">
														<label class="control-label pl-4 pt-2">pages</label> <select
															class="form-control" name="pages" id="pages">
															<option value="QQ1">QQ1</option>
															<option value="QQ2">QQ2</option>
															<option value="QQ3">QQ3</option>
															<option value="QQ4">QQ4</option>
														</select>
													</div>
												</div>
												<div class="col-sm-3">
													<div class="form-group">
														<label class="control-label pl-4 pt-2">Area</label> <select
															class="form-control" name="area" id="area">
															<option value="Header">Header</option>
															<option value="Footer">Footer</option>

														</select>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-10 gap-mid">
											<div class="form-group">
												<div class="col-sm-9 pl-0 mb-4">
													<div class="input-group">
														<label for="comment">English:</label>
														<textarea class="form-control" rows="4" name="nameEn"
															id="nameEn"></textarea>
													</div>
													<div id="err_nameEn" style="color: red;">Please keyin
														English Content</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-9 pl-0">
													<div class="input-group">
														<label for="comment">Bahasa Malay:</label>
														<textarea class="form-control" rows="4" name="nameBm"
															id="nameBm"></textarea>
													</div>
													<div id="err_nameBm" style="color: red;">Please keyin
														Bahasa Malay Content</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-8 gap-mid text-right">
										<!-- <button type="submit" class="btn btn-grey">Save</button> -->
										<input type="button" value="Save" id="next"
											class="btn btn-grey" onclick="submitForm()" /> <input
											type="button" value="Delete" id="deletecontentDescription"
											class="btn btn-grey" onclick="deleteDescription()" />
										<!--  <div type="button" class="btn btn-grey" id="deletecontentDescription">Delete</div> -->
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->
		</div>
		<!-- /.page-content -->
		<!-- BEGIN FOOTER -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
		<!-- END FOOTER -->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->
	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->
	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->
	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->


	<script
		src="<c:url value="/resources/assets/js/jquery-2.2.4.min.js" />"></script>
	<!--  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
	<script src="<c:url value="/resources/assets/js/apps.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/retina/retina.min.js" />"></script>
	<script
		src="<c:url value="/resources/assets/plugins/nicescroll/jquery.nicescroll.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/slimscroll/jquery.slimscroll.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/backstretch/jquery.backstretch.min.js"/>"></script>


	<!-- PLUGINS -->
	<script
		src="<c:url value="/resources/assets/plugins/owl-carousel/owl.carousel.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/prettify/prettify.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"/>"></script>

	<script
		src="<c:url value="/resources/assets/plugins/chosen/chosen.jquery.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/icheck/icheck.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/datepicker/bootstrap-datepicker.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/timepicker/bootstrap-timepicker.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/mask/jquery.mask.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/validator/bootstrapValidator.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/datatable/js/jquery.dataTables.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/datatable/js/bootstrap.datatable.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/datatable/js/jquery.highlight.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/datatable/js/dataTables.searchHighlight.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/summernote/summernote.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/markdown/markdown.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/markdown/to-markdown.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/markdown/bootstrap-markdown.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/slider/bootstrap-slider.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/toastr/toastr.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/newsticker/jquery.newsTicker.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/placeholder/jquery.placeholder.js"/>"></script>

	<script type="text/javascript"
		src="<c:url value="/resources/assets/js/shieldui-all.min.js"/>"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/assets/js/jszip.min.js"/>"></script>

	<script
		src="<c:url value="/resources/assets/plugins/jquery-knob/jquery.knob.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/jquery-knob/knob.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/js/jquery.dataTables.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/js/dataTables.bootstrap.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/js/dataTables.buttons.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/js/buttons.bootstrap.min.js"/>"></script>
	<script src="<c:url value="/resources/assets/js/jszip.min.js"/>"></script>
	<script src="<c:url value="/resources/assets/js/pdfmake.min.js"/>"></script>
	<script src="<c:url value="/resources/assets/js/vfs_fonts.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/js/buttons.html5.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/js/buttons.print.min.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/js/buttons.colVis.min.js"/>"></script>
	<script src="<c:url value="/resources/assets/js/toastr.min.js"/>"></script>

	<script
		src="<c:url value="/resources/assets/plugins/jquery-knob/jquery.knob.js"/>"></script>
	<script
		src="<c:url value="/resources/assets/plugins/jquery-knob/knob.js"/>"></script>
	<%--  <script src="<c:url value="/resources/assets/js/apps.js"/>"></script> --%>

	<!--  <script src="plugins/jquery-knob/jquery.knob.js"></script>
        <script src="plugins/jquery-knob/knob.js"></script>
        MAIN APPS JS-->
	<!--  <script src="js/apps.js"></script> -->
	<script type="text/javascript">
        
        $('#nameEn').keyup(function() {
            if($.trim(this.value).length) { // zero-length string AFTER a trim
          	 // $('#err_nameEn').removeClass('visible');
      			//$('#err_nameEn').addClass('hidden');
            	  $('#err_nameEn').hide();
            }
          });
        
        
        $('#nameBm').keyup(function() {
            if($.trim(this.value).length) { // zero-length string AFTER a trim
          	//  $('#err_nameBm').removeClass('visible');
      		//	$('#err_nameBm').addClass('hidden');
            	  $('#err_nameBm').hide();
            }
          });
          $(function() {
        // We can attach the `fileselect` event to all file inputs on the page
        $(document).on('change', ':file', function() {
          var input = $(this),
              numFiles = input.get(0).files ? input.get(0).files.length : 1,
              label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
          input.trigger('fileselect', [numFiles, label]);
        });

        // We can watch for our custom `fileselect` event like this
        $(document).ready( function() {
        	$('#err_nameEn').hide();
        	$('#err_nameBm').hide();
        	$('#Sales-funnel-ETB').hide();
            $(':file').on('fileselect', function(event, numFiles, label) {

                var input = $(this).parents('.input-group').find(':text'),
                    log = numFiles > 1 ? numFiles + ' files selected' : label;

                if( input.length ) {
                    input.val(log);
                } else {
                    if( log ) alert(log);
                }

            });
            getcontentDescription();
        });
      });
          $("#productEntity").change(function () {
           //   var end = this.value;
              var entity = $('#productEntity').val();
              if(entity=="EIB"){
            	  $('#Sales-funnel-ETB').hide();
            	  $('#Sales-funnel-EIB').show();
              }else{
            	  $('#Sales-funnel-ETB').show();
            	  $('#Sales-funnel-EIB').hide();
              }
              getcontentDescription();
          });
          
          
          function submitForm(){
        	  result = true;
        	  
        	  if(!$("#nameEn").val().length) {
                 $('#err_nameEn').show();
                 // $('#err_nameEn').addClass('visible');
          			//$('#err_nameEn').addClass('hidden');
                  $('#nameEn').focus();
                  result= false;
                }        	  
        	  if(!$("#nameBm").val().length) {
                  $('#err_nameBm').show();
                //  $('#err_nameBm').addClass('visible');
          			//$('#err_nameBm').addClass('hidden');
                  $('#nameBm').focus();
                  result= false;
                }
        	  
        	//  alert("hello");
        	  
        	  if (result == false){
                  return false;
                }
                else {
                  $('#insertDynamicContentManagement').submit();
                }
          }
          
			      $("#area").change(function () {
			    	  getcontentDescription();
			    	//alert("area");	  
			      });
				$("#pages").change(function () {
					getcontentDescription();
				// alert("pages");
				});
				$("#productTypeEIB").change(function () {
					getcontentDescription();
				// alert("productTypeEIB");
				});
				$("#productTypeETB").change(function () {
					getcontentDescription();
				 //alert("productTypeETB");
				});
        
				function getcontentDescription(){				
					 $.ajax({
							type: "POST",
							url: '${pageContext.request.contextPath}/fetchContentDescription',
							data:{				
							"productEntity": $('#productEntity').val(),
							"area": $('#area').val(),
							"pages": $('#pages').val(),
							"productTypeEIB": $('#productTypeEIB').val(),
							"productTypeETB": $('#productTypeETB').val()
							},
							async:false,
							success: function (data) {
								var  obj = JSON.parse(data);		
								//console.log('parse data '+data);
								/* console.log('parse data '+obj[0]);
								console.log('parse data '+obj[1]); */
         
								$('#nameEn').val(obj[0]);
								$('#nameBm').val(obj[1]);
								
							},
							error: function (request, status, error) {
							} 
							});  
				}
				//
				
				/* $("deletecontentDescription").click(function(){
  					alert("helloooooooooo");
          }); */
				function deleteDescription(){
					//alert("hellooooooooooeeeeeeeeeeeeeeee");
					 $.ajax({
							type: "POST",
							url: '${pageContext.request.contextPath}/deleteContentDescription',
							data:{				
							"productEntity": $('#productEntity').val(),
							"area": $('#area').val(),
							"pages": $('#pages').val(),
							"productTypeEIB": $('#productTypeEIB').val(),
							"productTypeETB": $('#productTypeETB').val()
							},
							async:false,
							success: function (data) {
								var  obj = JSON.parse(data);		
								//console.log('parse data '+data);
								/* console.log('parse data '+obj[0]);
								console.log('parse data '+obj[1]); */
								
								$('#nameEn').val(obj[0]);
								$('#nameBm').val(obj[1]);
								
							},
							error: function (request, status, error) {
							} 
							});  
					 getcontentDescription();
		          }
          
        </script>
</body>
</html>