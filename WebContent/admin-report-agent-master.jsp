<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page import="com.etiqa.model.report.AgentPerformanceReport"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}
</style>

</head>

<body>
	<%
        
       
   	 DecimalFormat formatter = new DecimalFormat("#,###.00");
        
 if (request.getMethod().equals("GET") && request.getParameter("startRow") == null){
			
			session.setAttribute("ProductType", "");
			session.setAttribute("agentCode", "");
			session.setAttribute("agentName", "");
			session.setAttribute("marketerCode","");
			session.setAttribute("marketerName", "");
			session.setAttribute("salesEntity", "");
			session.setAttribute("dateFrom","");
			session.setAttribute("dateTo","");
			session.setAttribute("DateFrom", "");
			session.setAttribute("DateTo","");
			
 }
			
        int totalRecord = 0;
        int recordPerPage =10;
        int pageCount =0;
        int startRow=0;
        int endRow=0;
        //temporary 
        endRow=recordPerPage;
        
       int start=0;       
       int end=0;
       double lastPage=0;
       String textColor="";
       String status="";
       String totalAmount="0.00";
       String transactionStatus="";
       String lastStep="";
       String nextStep="";
       String reason="";
       int i=0;
       int rowLine=0;
       String txtColor="";
       
       
       
       
        
        %>



	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Report &amp; Analytics</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Agent Performance Report</h4>
														</div>
													</div>
													<!-- Start Form -->
													<form name="txnreport" action="agentReport" method="post"
														onsubmit="return validateForm()">
														<input type="hidden" name="action" value="list" /> <input
															type="hidden" name="startRow" value="0" /> <input
															type="hidden" name="endRow" value="10" />

														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Search
																				by Date</label>
																			<div class="col-sm-4">
																				<input type="text" placeholder="Date From"
																					class="form-control" id="datepicker1"
																					name="dateFrom" value="${sessionScope.dateFrom}">
																			</div>
																			<div class="col-sm-5">
																				<input type="text" placeholder="Date To"
																					class="form-control" id="datepicker2" name="dateTo"
																					value="${sessionScope.dateTo}">
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Search
																				by Product Entity</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="productEntity"
																					id="productentity">
																					<option value="" selected>-View All-</option>
																					<option value="EIB">EIB</option>
																					<option value="ETB">ETB</option>
																				</select>
																			</div>
																		</div>



																		<!-- For ETB Only -->


																		<div id="etb-list" class="form-group"
																			style="display: none;">
																			<label class="col-sm-3 control-label">Product
																				Type</label>
																			<div class="col-sm-9">

																				<select class="form-control" name="etb-list">
																					<option value=""
																						<c:if test="${sessionScope.ProductType == ''}" >selected</c:if>>-View
																						All-</option>
																					<option value="WTCT"
																						<c:if test="${sessionScope.ProductType == 'WTCT'}" >selected</c:if>>
																						WORLD TRAVEL CARE TAKAFUL</option>


																					<option value="HHT"
																						<c:if test="${sessionScope.ProductType == 'HHT'}" >selected</c:if>>
																						HOUSE HOLDER TAKAFUL</option>
																					<option value="HOT"
																						<c:if test="${sessionScope.ProductType == 'HOT'}" >selected</c:if>>
																						HOUSE OWNER TAKAFUL</option>
																					<option value="HOHHT"
																						<c:if test="${sessionScope.ProductType == 'HOHHT'}" >selected</c:if>>
																						HOUSE OWNER HOUSE HOLDER TAKAFUL</option>


																				</select>


																			</div>
																		</div>


																		<!-- For Only EIB -->
																		<div id="eib-list" class="form-group"
																			style="display: none;">
																			<label class="col-sm-3 control-label">Product
																				Type</label>
																			<div class="col-sm-9">

																				<select class="form-control" name="eib-list">
																					<option value=""
																						<c:if test="${sessionScope.ProductType == ''}" >selected</c:if>>-View
																						All-</option>
																					<option value="WTC"
																						<c:if test="${sessionScope.ProductType == 'WTC'}" >selected</c:if>>
																						WORLD TRAVEL CARE</option>
																					<option value="MI"
																						<c:if test="${sessionScope.ProductType == 'MI'}" >selected</c:if>>
																						MOTOR INSURANCE</option>
																					<option value="TL"
																						<c:if test="${sessionScope.ProductType == 'TL'}" >selected</c:if>>
																						EZY LIFE</option>
																					<option value="HOHH"
																						<c:if test="${sessionScope.ProductType == 'HOHH'}" >selected</c:if>>
																						HOUSE OWNER HOUSE HOLDER</option>

																					<option value="HO"
																						<c:if test="${sessionScope.ProductType == 'HO'}" >selected</c:if>>
																						HOUSE OWNER</option>
																					<option value="HH"
																						<c:if test="${sessionScope.ProductType == 'HH'}" >selected</c:if>>
																						HOUSE HOLDER</option>

																				</select>


																			</div>
																		</div>

																		<!--  For All Products -->
																		<div id="total_list" class="form-group">
																			<label class="col-sm-3 control-label">Product
																				Type</label>
																			<div class="col-sm-9">

																				<select class="form-control" name="productType"
																					id="productType">
																					<option value=""
																						<c:if test="${sessionScope.ProductType == ''}" >selected</c:if>>-View
																						All-</option>
																					<option value="WTC"
																						<c:if test="${sessionScope.ProductType == 'WTC'}" >selected</c:if>>
																						WORLD TRAVEL CARE</option>
																					<option value="MI"
																						<c:if test="${sessionScope.ProductType == 'MI'}" >selected</c:if>>
																						MOTOR INSURANCE</option>
																					<option value="TL"
																						<c:if test="${sessionScope.ProductType == 'TL'}" >selected</c:if>>
																						EZY LIFE</option>
																					<option value="HOHH"
																						<c:if test="${sessionScope.ProductType == 'HOHH'}" >selected</c:if>>
																						HOUSE OWNER HOUSE HOLDER</option>
																					<option value="HOT"
																						<c:if test="${sessionScope.ProductType == 'HOT'}" >selected</c:if>>
																						HOUSE OWNER TAKAFUL</option>
																					<option value="HOHHT"
																						<c:if test="${sessionScope.ProductType == 'HOHHT'}" >selected</c:if>>
																						HOUSE OWNER HOUSE HOLDER TAKAFUL</option>
																					<option value="HO"
																						<c:if test="${sessionScope.ProductType == 'HO'}" >selected</c:if>>
																						HOUSE OWNER</option>
																					<option value="HH"
																						<c:if test="${sessionScope.ProductType == 'HH'}" >selected</c:if>>
																						HOUSE HOLDER</option>

																				</select>


																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Marketer
																				Code</label>
																			<div class="col-sm-9">
																				<input type="text" placeholder=""
																					class="form-control" name="marketerCode"
																					value="${sessionScope.marketerCode}">
																			</div>
																		</div>


																		<div id="mi-option" style="display: none">
																			<div class="form-group">
																				<label class="col-sm-3 control-label">Ecovernote
																					Status</label>
																				<div class="col-sm-9">
																					<select class="form-control">
																						<option value="-View All-" selected="">-View
																							All-</option>
																						<option value="a">All</option>
																						<option value="b">Digunakan</option>
																						<option value="c">Dibatalkan</option>
																					</select>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label">Plate
																						No.</label>
																					<div class="col-sm-9">
																						<input type="text" placeholder=""
																							class="form-control">
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>

																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Agent
																				Code</label>
																			<div class="col-sm-9">
																				<input type="text" placeholder=""
																					class="form-control" name="agentCode"
																					value="${sessionScope.agentCode}">
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Agent
																				Name</label>
																			<div class="col-sm-9">
																				<input type="text" placeholder=""
																					class="form-control" name="agentName"
																					value="${sessionScope.agentName}">
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Marketer
																				Name</label>

																			<div class="col-sm-9">
																				<input type="text" placeholder=""
																					class="form-control" name="marketerName"
																					value="${sessionScope.marketerName}">
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Sales
																				Entity</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="salesEntity">
																					<option value=""
																						<c:if test="${sessionScope.salesEntity == ''}" >selected</c:if>>-View
																						All-</option>
																					<option value="S"
																						<c:if test="${sessionScope.salesEntity == 'A'}" >selected</c:if>>Cyber
																						Agent</option>
																					<option value="F"
																						<c:if test="${sessionScope.salesEntity == 'M'}">selected</c:if>>Marketer</option>

																				</select>
																			</div>
																		</div>

																	</div>
																</div>


																<div class="col-sm-12 text-right">
																	<button type="submit" class="btn btn-default"
																		name="SEARCH">Search</button>
																</div>
															</div>
														</div>
													</form>
													<!-- END FORM -->
												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="row">
																<div class="col-xs-12 form-inline">
																	<div class="col-xs-6">
																		<!--   <div class="row">
                                                                        <label>
                                                                            <select class="form-control" disabled>
                                                                              <option value="5">5</option>
                                                                              <option value="10" selected>10</option>
                                                                              <option value="25">25</option>
                                                                              <option value="50">50</option>
                                                                              <option value="100">100</option>
                                                                            </select>
                                                                            records per page
                                                                        </label>
                                                                        </div>-->
																	</div>
																	<div class="col-xs-6">
																		<div class="row">
																			<div class="pull-right">
																				<button class="btn btn-warning btn-sm">
																					Print <i class="fa fa-print" aria-hidden="true"></i>
																				</button>
																				<button class="btn btn-warning btn-sm">
																					Export to XLS <i class="fa fa-download"
																						aria-hidden="true"></i>
																				</button>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="table-responsive" id="MyTable">
																<table
																	class="table table-striped table-warning table-hover"
																	id="admin-datatable-second_admin">
																	<thead>
																		<tr>
																			<th colspan="2">Gross Amount</th>
																			<th colspan="2">Nett Amount</th>

																			<th colspan="2">Total Transaction Amount</th>
																			<th colspan="2">Commission</th>
																		</tr>
																		<td colspan="2">
																			<%        
                                                                if (request.getAttribute("data") != null) {
														            List<AgentPerformanceReport> list = (List<AgentPerformanceReport>) request.getAttribute("data");
														           
														            
														            if(list.size() > 0 ){
													            	   	
														            	totalAmount = list.get(0).getTotalAmount();
														            	
														            }
														           
																	
														            if(session.getAttribute("ProductType").equals("TL") && session.getAttribute("Status").equals("")){
														            	
														            	
														            	totalAmount = request.getAttribute("totalAmount").toString();
														            	
														            	
														            	
														            }

                                                                } %> <b>RM</b>
																			<%=totalAmount %></td>
																		<td colspan="2"><b>RM</b> <%
                          %> <% if(session.getAttribute("ProductType").equals("TL")){
	                                                                    	%>
																			- <%  } else{
	                                                                    %>
																			<%=totalAmount %>
																			<% } %></td>
																		<td colspan="4"><b>RM</b> <%=totalAmount %></td>
																		<tr>
																			<th>No</th>
																			<th style="width: 40px;">Txn Date</th>
																			<th>Pol/Cert No</th>
																			<th>Customer</th>
																			<th>Product</th>
																			<th>Payment</th>
																			<th>Status</th>
																			<th>Amount(RM)</th>
																		</tr>


																	</thead>
																	<tbody>


																		<%
                                                                 
                                                                 //System.out.println(request.getAttribute("data"));
                                                                 if (request.getAttribute("data") != null) {
														            List<AgentPerformanceReport> list = (List<AgentPerformanceReport>) request.getAttribute("data");
														           
														           
														            if(list.size() > 0 ){
														            	   	
														            	totalRecord = list.get(0).getRecordCount();
														            }
														            
														            
														            System.out.println(totalRecord);
														            if(totalRecord >0) {
														            	startRow= list.get(0).getId();
														            	endRow=(startRow)+recordPerPage;
														            	
														            	if (endRow > totalRecord ){
														            		
														            		endRow=totalRecord;//at last page
														            		
														            	}
														            	
														            	
														                for(AgentPerformanceReport txn : list) {
														                	
						                		rowLine++;
														                		
														                
														                	
														                	if(txn.getPolicyNo()!=null && txn.getPolicyNo()!="" && !txn.getStatus().equals("S") && !txn.getStatus().equals("SUCCESS"))
														                	{
														                		
														                		txtColor="red";
														                		
														                	}
														                	
														                	
														        %>
																		<tr>
																			<td align="center"><%=txn.getDSPQQID()%></td>
																			<td>
																				<% if(txn.getTransactionDate()!=null && txn.getTransactionDate()!="" ) { %><%=txn.getTransactionDate()%>
																				<%} %>
																			</td>
																			<td>
																				<% if(txn.getPolicyNo()!=null && txn.getPolicyNo()!="" ) { %><a
																				color="<%=txtColor%>"
																				href="TransactionalReport?action=detail&id=<%=txn.getTransactionID()%>"><span
																					color="<%=txtColor%>"><%=txn.getPolicyNo()%></span></a>
																				<%} %> <% if(txn.getCAPSPolicyNo()!=null && txn.getCAPSPolicyNo()!="" ) { %><a
																				color="<%=txtColor%>"
																				href="TransactionalReport?action=detail&id=<%=txn.getTransactionID()%>"><span
																					color="<%=txtColor%>"><br /><%=txn.getCAPSPolicyNo()%></span></a>
																				<%} %> <br>
																			<span>
																					<% if(txn.getVehicleNo()!=null && txn.getVehicleNo()!="") { %>
																					<%=txn.getVehicleNo()%> <%} %>
																			</span>
																			<td><b>Name :</b>
																				<% if(txn.getTransactionID()!=null && txn.getTransactionID()!="" ) { %>
																				<a
																				href="TransactionalReport?action=detail&id=<%=txn.getTransactionID()%>"><%=txn.getCustomerName()%></a>
																				<%} else { %><%=txn.getCustomerName()%> <% }%><br />
																			<b>ID No : </b><%=txn.getCustomerNRIC()%><br>ETIQA
																				INSURANCE BERHAD - E CHANNEL (V0028758)<br>
																				Marketer Test (M100) <% if (txn.getProductCode().equals("TL"))
                                                                    
                                                                    {
                                                                    	 
                                                                    	 String coverage = txn.getCoverage();
                                                                    	 
                                                                    	 if(coverage != null ||  coverage !=""){
                                                                    	 
                                                                    		 double fmtCoverage = Double.parseDouble(coverage);
                                                                    	 coverage = formatter.format(fmtCoverage);

                                                                    	 System.out.println(coverage);
                                                                    	 }
                                                                    	 
                                                                    	%>
																				<br>
																			<b>Term:</b> <%=txn.getTerm()%> Years <br>
																			<b>Coverage:</b> RM <%=coverage%> <% 
                                                                    }
                                                                     %>


																			</td>
																			<!--  <td></td>-->
																			<td>
																				<% if(txn.getProductType()!=null || txn.getProductType()!="") { %><%=txn.getProductType()%>
																				<%} 	
                                                                    	%>
																			</td>
																			<td><b><%=txn.getPaymentChannel()%></b> <% if (txn.getPaymentChannel().equals("MPAY"))
                                                                    
                                                                    {
                                                                    	%>
																				<br>
																			<b>Ref No:</b><%=txn.getMPAY_RefNo()%> <br>
																			<b>Auth Code:</b> <%=txn.getMPAY_AuthCode()%> <br>
																				<b>Mode:</b> <%=txn.getPremiumMode()%> <% 
                                                                    }
                                                                    
                                                                    else if(txn.getPaymentChannel().equals("EBPG"))
                                                                   
                                                                    	
                                                                    {
                                                                    	%>
																				<br>
																			<b>Ref No:</b><%=txn.getEBPG_RefNo()%> <br>
																			<b>Auth Code:</b> <%=txn.getEBPG_AuthCode()%> <br>

																				<b>Tax Inv:</b> <%=txn.getInvoiceNo() %> <% 	
                                                                    	
                                                                    }
 																	else if(txn.getPaymentChannel().equals("M2U"))
                                                                   
                                                                    	
                                                                    {
                                                                    	%>
																				<br>
																			<b>Ref No:</b><%=txn.getM2U_RefNo()%> <br>
																			<b>App Code:</b> <%=txn.getM2U_AuthCode()%> <br>

																				<b>Tax Inv:</b> <%=txn.getInvoiceNo() %> <% 	
                                                                    	
                                                                    }
                                                                    
	                                                                else if(txn.getPaymentChannel().equals("FPX"))
                                                                   
                                                                    	
                                                                    {
                                                                    	%>
																				<br>
																			<b>Ref No:</b><%=txn.getFPX_RefNo()%> <br>
																			<b>FPX Status:</b> <%
                                                                    	System.out.println("txn.getFPX_AuthCode() "+txn.getFPX_AuthCode());
                                                                    	
                                                                    	if (txn.getFPX_AuthCode().equals("00"))
                                                            			{
                                                            				
                                                            				transactionStatus= txn.getFPX_AuthCode()+"-Successful";
                                                            					
                                                            			}
                                                            			else if (!txn.getFPX_AuthCode().equals("00") && !txn.getFPX_AuthCode().equals("") )
                                                            			{
                                                            				transactionStatus= txn.getFPX_AuthCode()+"-Unsuccessful";
                                                            				
                                                            			}
                                                            			else
                                                            			{
                                                            				
                                                            				transactionStatus="";
                                                            				
                                                            			}
                                                            			
                                                                    	
                                                                    	%>

																				<%=transactionStatus%> <br> <b>Tax Inv:</b> <%=txn.getInvoiceNo() %>
																				<% 	
                                                                    	
                                                                    }
                                                                    
                                                                    %></td>

																			<!-- start: temporary -->
																			<% if (txn.getStatus().equals("SUCCESS") || txn.getStatus().equals("S") ){
                                                                    	 
                                                                    	 status = "Successful";
                                                                    	 textColor="green";
                                                                    	 
                                                                     }
                                                                     
                                                                     if (txn.getStatus().equals("Incomplete") || txn.getStatus().equals("O"))
                                                                    	 
                                                                     {
                                                                    	
                                                                    		 
                                                                    		 status="Incomplete";
                                                                    		 textColor="";
                                                                    		 
                                                                    		
                                                                    		 System.out.println("txn.getDoneStep1() = " +txn.getDoneStep1());
                                                                    		 System.out.println("txn.getDoneStep2() =" +txn.getDoneStep2());
                                                                    		 
                                                                    		 
                                                                    		 if(txn.getDoneStep1() != "" || txn.getDoneStep1()  !=null)
                                                                    		 {
                                                                    			 
                                                                    			 
                                                                    				if(txn.getDoneStep2() == "" || txn.getDoneStep2()  ==null){
                                                                    				 
                                                                    			      lastStep = "Step 1: Get A Quote";
                                                                    			 //nextStep = "Detailed Information"; 
                                                                    			 
                                                                    			 }
                                                                    				 else if(txn.getDoneStep2() != "" || txn.getDoneStep2()  !=null){
                                                                    					 
                                                                    					 
                                                                    						 
                                                                    						 lastStep = "Step 2 : Detailed Information";	 
                                                                    						 
                                                                    					 }
                                                                    				
                                                                    			 
                                                                    		 }
                                                                    				 else{

                                                                    					 lastStep = "Step 1: Get A Quote";
                                                                    				 
                                                                    				 }
                                                                    			 
                                                                    		 
                                                                    	 
                                                                    	  
                                                                    	 
                                                                     }
                                                                    
                                                                     if (txn.getStatus().equals("Pending") || txn.getStatus().equals("AP"))
                                                                    	 
                                                                     {
                                                                    	
                                                                    		 
                                                                    		 status="Attempt Payment";
                                                                    		 textColor="";
                                                                    	 
                                                                    	  
                                                                    	 
                                                                     }
                                                                     
																	if (txn.getStatus().equals("Failed") || txn.getStatus().equals("F"))
                                                                    	 
                                                                     {
                                                                    	
                                                                    		 
                                                                    		 status="Failed";
                                                                    		 textColor="red";
                                                                    	 
                                                                    	  
                                                                    	 
                                                                     }
																	
																	if (txn.getStatus().equals("R"))
                                                                   	 
                                                                    {
                                                                   	   
																		if(txn.getReason() == null ||txn.getReason().trim() == "" ){
                                                                   		 
                                                                   		 reason="";
                                                                  
																		}
																		
																		else
																			
																		{
																		
																			reason=txn.getReason();
																			
																		}
																		
																		if(txn.getUWReason() != null &&  txn.getUWReason().trim() != ""){
																			
																			if(reason ==""){
	                                                                   		 
	                                                                   		 reason=txn.getUWReason();
	                                                                   		 
																			}
																			else
																			{
																				
																				System.out.println(reason);
																				System.out.println(txn.getUWReason());
																				if(!reason.trim().equals(txn.getUWReason().trim()) ){
																				 reason=reason+"("+txn.getUWReason()+")";	
																				}
																				
																				
																			}
	                                                                  
																			}
																			
																			
																		 status="Rejection : "+reason;
                                                                   		 textColor="red";
                                                                   		 
                                                                   	  
                                                                   	 
                                                                    }
                                                                    
																	
                                                                   if (txn.getStatus().equals("CANCELLED"))
                                                                    	 
                                                                     {
                                                                	   status="Cancelled";
                                                                    	 textColor="red";
                                                                    	 
                                                                    	 
                                                                     }
                                                                     
                                                                     
                                                                     %>
																			<!-- end: temporary -->
																			<td><font color=<%=textColor %>><%=status%>
																					<br><%=lastStep %> </font></td>
																			<td align="center"><%=txn.getPremiumAmount()%></td>
																		</tr>
																		<%
                                                                 
														                }
														                status="";
																           textColor="";
																           
														            }
																	else
																	{
																	%>
																		<tr>
																			<td colspan="8"><center>No Record
																					Registered</center></td>
																		</tr>
																		<% 	
																	
																	
														            }
                                                                 }
														            
														            else{
														            	
														            	%>
																		<tr>
																			<td colspan="8"><center>No Record
																					Registered</center></td>
																		</tr>
																		<% 	
														            }
														           
														        %>
																	</tbody>
																</table>
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>
											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->

	<script type="text/javascript">
         	$(document).ready(function() {
         		
         	
         		
         
         		
         		//var table = $('#admin-datatable-second_admin').DataTable();
         		//table.draw();
         		
         		
         		 var t = $('#admin-datatable-second_admin').DataTable( {
         			 
         		 "searching" :false,
         		 
         	        "columnDefs": [ {
         	            "searchable": false,
         	            "orderable": false,
         	           "targets": [1],
         	           "bSortable": false
         	         
         	        } ],
         	        "order": [[ 0, 'desc' ]]
         	    } );
         	 
         	    t.on( 'order.dt search.dt', function () {
         	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
         	            cell.innerHTML = i+1;
         	        } );
         	    } ).draw();
         	    
         	    
         	   $(document).ready( function() {
         		  $('#example').dataTable( {
         		    "aoColumnDefs": [
         		      { "bSortable": false, "aTargets": [ 0 ] }
         		    ] } );
         		} );
         		 
         		 
         	   
         	    
         		$('#productentity').on('change', function() {
         			if (this.value == 'ETB')
         			{
         				$('#eib-list').hide();
         				$('#etb-list').show();
         				//$('#mi-option').css("display","none");
         				
         			} 
         			
         			else if (this.value == 'EIB')
         				{
         				
         				$('#eib-list').show();
         				$('#etb-list').hide();
         				//$('#mi-option').css("display","none");
         				
         				}
         			
         			else {
         				$('#eib-list').hide();
         				$('#etb-list').hide();
         				//$('#mi-option').css("display","none");
         				
         			}
         		});
         		
         		
         	    
         		//$('#productentity').on('change', function() {
         			$("#productentity").change(function(){
         			//alert(this.value);
         			if (this.value == 'ETB')
         			{
         				$('#eib-list').hide();
         				$('#etb-list').show();
         				$('#total_list').hide();
         				//$('#mi-option').css("display","none");
         				
         			} 
         			
         			else if (this.value == 'EIB')
         				{
         				
         				$('#eib-list').show();
         				$('#etb-list').hide();
         				$('#total_list').hide();
         				//$('#mi-option').css("display","none");
         				
         				}
         			
         			else {
         				$('#eib-list').hide();
         				$('#etb-list').hide();
         				$('#total_list').show();
         				//$('#mi-option').css("display","none");
         				
         			}
         		});
         		
         		
         	//$('#productType').on('change', function() {		
         			//if (this.value.indexOf("WTC")=-1)
         			//{
         				//$('#eib-list').hide();
         				//$('#etb-list').show();
         				//$('#mi-option').show();
         				//$('#mi-option').css("display","block");
         				
         			//} 
         			
         		//});	
         	
 
         		
         	});
         
         	
         	
         
         </script>

	<script>
        function validateForm() {
        	   var x = document.forms["txnreport"]["dateFrom"].value;
        	   var y = document.forms["txnreport"]["dateTo"].value;
        	   var z=  document.forms["txnreport"]["status"].value;
        	   var w=  document.forms["txnreport"]["productType"].value;
        	   
        	   if ( x == null || x == "" ) {
        		   if(y !=""){
        	      alert("Please choose Date From");
        	      document.forms["txnreport"]["dateFrom"].focus();
        	      
        	      return false;
        	   } 
        		  
        	   }
        	   
        	   if(z=="R"|| z =="O"){
        		   
        		   if(w =="")
        			   {
        			   
        			   alert ("Please choose Product Type");
        			   document.forms["txnreport"]["productType"].focus();
        			   return false;	   
        			   }      		   
        		   
        	   }
        	   return true;
        	  
        	}
        
       
        </script>

	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script type="text/javascript" src="assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript" src="assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>

	<link href="assets/js/jquery.dataTables.min.css" rel="stylesheet"
		type="text/css">
	<link href="assets/js/buttons.dataTables.min.css" rel="stylesheet"
		type="text/css">
	<link href="assets/js/bootstrap.min.css" rel="stylesheet"
		type="text/css">
	<link href="assets/js/dataTables.bootstrap.min.css" rel="stylesheet"
		type="text/css">
	<link href="assets/js/buttons.bootstrap.min.css" rel="stylesheet"
		type="text/css">

	<script src="assets/js/jquery-1.12.3.js"></script>
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/buttons.print.min.js"></script>
	<script src="assets/js/buttons.colVis.min.js"></script>
</body>
</html>