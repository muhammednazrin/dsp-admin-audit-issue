<%@taglib prefix="botDetect" uri="https://captcha.com/java/jsp"%>
<%@ page import="java.io.*,java.util.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%
	java.text.DateFormat df = new java.text.SimpleDateFormat("dd/MM/yyyy");
%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<link rel="stylesheet" href="assets/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/menu.css">
<link rel="stylesheet" href="assets/css/toastr.min.css">

<link rel="stylesheet" href="assets/jAlert/jAlert.css">


<style type="text/css">
.inputError {
	border: 1px solid red;
}

.col-centered {
	float: none;
	text-align: center;
	margin: 0 auto;
}

.pad15 {
	padding: 0 15px !important;
}

.no-pad-right {
	padding-right: 0 !important;
}

.form-horizontal .form-group {
	margin-bottom: 10px;
}

.captcha-wrapper {
	width: 100%;
	text-align: center;
	max-width: 272px;
	clear: both;
}

.captcha-wrapper table {
	display: block;
	overflow: hidden;
	width: 100%;
}

.g-recaptcha {
	-webkit-transform: scale(0.80);
	-moz-transform: scale(0.80);
	-ms-transform: scale(0.80);
	-o-transform: scale(0.80);
	transform: scale(0.80);
	-webkit-transform-origin: 0 0;
	-moz-transform-origin: 0 0;
	-ms-transform-origin: 0 0;
	-o-transform-origin: 0 0;
	transform-origin: 0 0;
	width: 100% !important;
}
</style>

</head>

<body>
	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->


	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Cyber Agent Management </a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<!--  enctype="multipart/form-data" -->
											<form:form modelAttribute="agentProfile" name="regFrom"
												id="regFrom" action="saveAgentDone" method="POST"
												enctype="multipart/form-data">
												<!--   <input type="hidden" name="action" value="save"> -->
												<div class="row">
													<div class="col-sm-12">
														<div class="title">
															<div class="sub">
																<h4>Cyber Agent Management</h4>
															</div>
														</div>
														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-12">
																	<div class="title">
																		<div class="sub">
																			<h4>Agent Information Details</h4>
																		</div>
																	</div>
																</div>
																<div class="gap gap-mini"></div>
																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Reference No</label>
																			</div>
																			<div class="col-sm-9">
																				<spring:bind path="agentProfile.referenceNumber">
																					<input type="text" class="form-control"
																						name="${status.expression}" id="referenceNo"
																						value="${status.value}" required maxlength="30" />
																				</spring:bind>
																				<!--    <input type="text" class="form-control" name="referenceNo" id="referenceNo" maxlength="30"> -->
																				<span id="msg_refno" class="hidden"
																					style="color: red; text-align: left">Please
																					enter your Reference No</span>
																			</div>
																		</div>

																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Registration
																					Date</label>
																			</div>
																			<div class="col-sm-9">
																				<spring:bind path="agentProfile.registrationDate">
																					<%-- <input type="text" placeholder="Date" class="form-control" required name="${status.expression}" value="<c:out value="${s}" />" id="date1" /> --%>
																					<input type="text" placeholder="Date"
																						class="form-control" readonly
																						name="${status.expression}"
																						value="<c:out value="<%= df.format(new java.util.Date())%>" />"
																						id="date1" />

																				</spring:bind>
																				<!--  <input type="text" placeholder="Date" class="form-control"  name="registrationDate" id="date1"> -->
																				<span id="msg_date" class="hidden"
																					style="color: red; text-align: left">Please
																					select registration Date</span>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Annual Sales
																					Target (RM)</label>
																			</div>
																			<div class="col-sm-9">
																				<spring:bind path="agentProfile.annualSalesTarget">
																					<input type="text" name="${status.expression}"
																						value="${status.value}" class="form-control"
																						required id="annualSalesTarget" maxlength="10" />
																				</spring:bind>
																				<!--   <input type="text" name="annualSalesTarget" class="form-control"  id="annualSalesTarget" maxlength="10"> -->
																				<span id="msg_salesTarget" class="hidden"
																					style="color: red; text-align: left">Please
																					enter your Annual Sales Target</span>
																			</div>
																		</div>

																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Branch Code</label>
																			</div>
																			<div class="col-sm-9">
																				<spring:bind path="agentProfile.branchCode">
																					<input type="text" name="${status.expression}"
																						value="${status.value}" class="form-control"
																						id="branchCode" maxlength="50" />
																				</spring:bind>
																				<!--   <input type="text" name="annualSalesTarget" class="form-control"  id="annualSalesTarget" maxlength="10"> -->
																				<span id="msg_branchCode" class="hidden"
																					style="color: red; text-align: left">Please
																					enter Branch Code</span>
																			</div>
																		</div>

																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Maya Agent</label>
																			</div>
																			<div class="col-sm-9">
																				<spring:bind path="agentProfile.isMayaAgent"> 
																				YES &nbsp;<input type="radio"
																						name="${status.expression}"
																						<c:if test="${status.value eq 'y'}" >checked</c:if>
																						value="y" id="${status.expression}" />
																				NO &nbsp;<input type="radio"
																						name="${status.expression}" value="n"
																						<c:if test="${status.value ne 'y'}" >checked</c:if>
																						id="${status.expression}" />
																				</spring:bind>

																			</div>
																		</div>
																		<!-- Added by Chandra for Simplified Motor Cyber Agent on 20171030 -->
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Sell Simplified
																					Motor</label>
																			</div>
																			<div class="col-sm-9">
																				<spring:bind path="agentProfile.simplifiedMotor"> 
																				YES &nbsp;<input type="radio"
																						name="${status.expression}"
																						<c:if test="${status.value ne 'N'}" >checked</c:if>
																						value="Y" id="${status.expression}" />
																				NO &nbsp;<input type="radio"
																						name="${status.expression}" value="N"
																						<c:if test="${status.value eq 'N'}" >checked</c:if>
																						id="${status.expression}" />
																				</spring:bind>
																			</div>
																		</div>


																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-horizontal">


																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Agent Category</label>
																			</div>
																			<div class="col-sm-9">
																				<spring:bind path="agentProfile.agentCategory">
																					<select class="form-control" name="agentCategory"
																						required id="agentCategory"
																						onblur="checkOnBlur(this,'Please Select Your Category');"
																						onchange="checkOnChange(this);">
																						<option value=" -Please Select-">-Please
																							Select-</option>
																						<c:forEach items="${agentCategoryList}"
																							var="element">
																							<option
																								<c:if test="${status.value eq element.paramCode}" >selected</c:if>
																								value="<c:out value="${element.paramCode}"/>">
																								<c:out value="${element.paramName}" />
																							</option>
																						</c:forEach>
																					</select>
																				</spring:bind>
																				<%--  <select class="form-control" name="agentCategory" id="agentCategory"  onchange="checkOnChange(this);">
                                                                                        <option value=""> -Please Select-</option>
                                                                                        <c:forEach items="${agentCategoryList}" var="element"> 
						                                                                  <option						                                                                  
																									value="<c:out value="${element.paramCode}"/>">
																									<c:out value="${element.paramName}" />
																							</option>																																				
																						</c:forEach>                                                                                                                                                                               
                                                                                    </select> --%>
																				<span id="msg_agentcat" class="hidden"
																					style="color: red; text-align: left">Please
																					select agent category</span>
																			</div>
																		</div>

																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Agent Type</label>
																			</div>
																			<div class="col-sm-9">
																				<spring:bind path="agentProfile.agentType">
																					<select class="form-control" required
																						name="${status.expression}"
																						id="${status.expression}"
																						onblur="checkOnBlur(this,'Please Select Your Insurance Type');"
																						onchange="checkOnChange(this);">
																						<option value=" -Please Select-">-Please
																							Select-</option>
																						<c:forEach items="${agentTypeList}" var="element">
																							<option
																								<c:if test="${status.value eq element.paramCode}" >selected</c:if>
																								value="<c:out value="${element.paramCode}"/>">
																								<c:out value="${element.paramName}" />
																							</option>
																						</c:forEach>

																					</select>
																				</spring:bind>
																				<%--  <select class="form-control" name="agentType" id="agentType" onblur="checkOnBlur(this,'Please Select Your Insurance Type');"  onchange="checkOnChange(this);">
                                                                                        <option value=""> -Please Select-</option>
                                                                                       <c:forEach items="${agentTypeList}" var="element"> 
						                                                                  <option						                                                                  
																									value="<c:out value="${element.paramCode}"/>">
																									<c:out value="${element.paramName}" />
																							</option>																																				
																						</c:forEach>                                                                                  
                                                                                    </select> --%>
																				<span id="msg_agenttype" class="hidden"
																					style="color: red; text-align: left">Please
																					select agent type</span>
																			</div>
																		</div>


																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Status</label>
																			</div>
																			<div class="col-sm-9">
																				<spring:bind path="agentProfile.status">
																					<select class="form-control" required
																						name="${status.expression}"
																						id="${status.expression}"
																						onblur="checkOnBlur(this,'Please Select Your Status');"
																						onchange="checkOnChange(this);">
																						<option value=" -Please Select-">-Please
																							Select-</option>
																						<c:forEach items="${agentStatusList}"
																							var="element">
																							<option
																								<c:if test="${status.value eq element.paramCode}" >selected</c:if>
																								value="<c:out value="${element.paramCode}"/>">
																								<c:out value="${element.paramName}" />
																							</option>
																						</c:forEach>
																					</select>
																				</spring:bind>
																				<%-- <select class="form-control" name="agentStatus" id="agentStatus" onblur="checkOnBlur(this,'Please Select Your Status');"  onchange="checkOnChange(this);">
                                                                                        <option value=""> -Please Select-</option>
                                                                                        <c:forEach items="${agentStatusList}" var="element"> 
						                                                                  <option						                                                                  
																									value="<c:out value="${element.paramCode}"/>">
																									<c:out value="${element.paramName}" />
																							</option>																																				
																						</c:forEach>   
                                                                                    </select> --%>
																				<span id="msg_agentstatus" class="hidden"
																					style="color: red; text-align: left">Please
																					select agent status</span>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Agent Alias
																					Name</label>
																			</div>
																			<div class="col-sm-8">
																				<spring:bind path="agentProfile.agentAliasName">
																					<input type="text" required
																						name="${status.expression}" id="agentAliasName"
																						value="${status.value}"
																						class="form-control required" maxlength="20" />
																				</spring:bind>
																				<!--  <input type="text" class="form-control" name="agentAliasName" id="agentAliasName" maxlength="20"> -->
																				<span id="msg_agentaliasname" class="hidden"
																					style="color: red; text-align: left">Please
																					key in Agent Name</span>
																			</div>
																			<span class="right tooltips"> <a
																				id="tooltips-2"> <i
																					class="fa fa-question-circle fa-lg"></i>
																			</a>
																			</span>
																			<!-- Popover 2 hidden title -->
																			<div id="tooltips-2Title" style="display: none">
																				Agent referral ID is the link management solution to
																				shorten the product referral link<br> e.g.
																				http://www.etiqa.com.my/agentname<br>
																			</div>
																		</div>

																	</div>
																</div>




																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Agent Logo</label>
																			</div>
																			<div class="col-sm-9">
																				<div id="fine-uploader-gallery">
																					<input type="file" name="agentLogo">
																				</div>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-3 column">
																				<label class="control-label"></label>
																			</div>
																			<div class="col-sm-9">Please upload logo with
																				this format only .jpg, jpeg, .gif and .png. Do not
																				exceed 345px width X 100px height of image size</div>
																		</div>
																	</div>
																</div>


																<div class="col-sm-12">
																	<div class="form-inline info-meor">
																		<div class="form-group" name="registrationDocument">
																			<label class="control-label col-sm-12">Agent
																				Registration Document</label>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-12">
																				<div class="checkbox">

																					<label> <input type="checkbox"
																						id="registrationDocument"
																						name="registrationDocument"
																						value="Application Form"> Application Form
																						<!-- <span id="msg_agentregdoc" class="hidden" style="color:red;text-align:left">Please select to proceed</span> -->
																					</label> <label> <input type="checkbox"
																						id="registrationDocument"
																						name="registrationDocument"
																						value="Cyber Agent Agreement"> Cyber Agent
																						Agreement <!-- <span id="msg_agentregdoc" class="hidden" style="color:red;text-align:left">Please select to proceed</span> -->
																					</label>

																				</div>
																			</div>
																		</div>


																	</div>
																</div>
															</div>

															<div class="the-box">
																<div class="col-sm-12">
																	<div class="title">
																		<div class="sub">
																			<h4>Personal Details</h4>
																		</div>
																	</div>
																</div>
																<div class="gap gap-mini"></div>
																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Agent Name</label>
																			</div>
																			<div class="col-sm-9">
																				<spring:bind path="agentProfile.agentName">
																					<input type="text" required class="form-control"
																						maxlength="50" name="${status.expression}"
																						id="${status.expression}" value="${status.value}" />
																				</spring:bind>
																				<!-- <input type="text" class="form-control" name="agentName" id="agentName" maxlength="50"/> -->
																				<span id="msg_agentname" class="hidden"
																					style="color: red; text-align: left">Please
																					select to proceed</span>
																			</div>
																		</div>

																		<!-- <div class="form-group">
                                                                                <div class="col-sm-3 column text-right">
                                                                                <label class="control-label">User Id</label></div>
                                                                                <div class="col-sm-9">
                                                                                    <input type="text" required class="form-control" name="userId" id="userId" maxlength="20" />
                                                                                  
                                                                                </div>
                                                                            </div>
                                                                            
                                                                             <div class="form-group">
                                                                                <div class="col-sm-3 column text-right">
                                                                                <label class="control-label">Password</label></div>
                                                                                <div class="col-sm-9">
                                                                                    <input type="password"  class="form-control" name="password" id="password" maxlength="20" />
                                                                                    
                                                                                </div>
                                                                            </div> -->

																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Login ID</label>
																			</div>
																			<div class="col-sm-9 column pad15">
																				<div class="row">
																					<%-- <div class="col-sm-4 no-pad-right">
                                                                                            <select class="form-control" name="idType" onblur="checkOnBlur(this,'Please Select Your ID Type');"  onchange="checkOnChange(this);">                                                                                            
                                                                                                    <c:forEach items="${agentIdTypeList}" var="element"> 
						                                                                            <option						                                                                  
																									value="<c:out value="${element.paramCode}"/>">
																									<c:out value="${element.paramName}" />
																							</option>																																				
																						</c:forEach>  
                                  														 </select>
                                                                                            <span id="msg_idtype" class="hidden" style="color:red;text-align:left">Please select ID Type</span>
                                                                                        </div> --%>
																					<div class="col-sm-12 column">
																						<spring:bind path="agentProfile.idNo">
																							<input type="text" required class="form-control"
																								maxlength="12" name="${status.expression}"
																								id="${status.expression}"
																								value="${status.value}"
																								onblur="validateAgentId();" />
																						</spring:bind>
																						<!--  <input type="text" class="form-control" pattern="[0-9]+" name="idNo" id="idNo" maxlength="15" onblur="validateAgentId();"> -->
																						<span id="msg_idno" class="hidden"
																							style="color: red; text-align: left">Please
																							key in your ID number</span>
																					</div>
																				</div>
																			</div>
																		</div>


																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Phone No</label>
																			</div>
																			<div class="col-sm-9 pad15">
																				<div class="row">
																					<div class="col-sm-4 no-pad-right">
																						<spring:bind path="agentProfile.phoneType">
																							<select class="form-control" name="phoneType"
																								required
																								onblur="checkOnBlur(this,'Please Select Your Phone Type');"
																								onchange="checkOnChange(this);">
																								<c:forEach items="${agentPhoneTypeList}"
																									var="element">
																									<option
																										<c:if test="${status.value eq element.paramCode}" >selected</c:if>
																										value="<c:out value="${element.paramCode}"/>">
																										<c:out value="${element.paramName}" />
																									</option>
																								</c:forEach>
																							</select>
																						</spring:bind>
																						<%-- <select class="form-control" name="phoneType"  onchange="checkOnChange(this);">
                                                                                                 <c:forEach items="${agentPhoneTypeList}" var="element"> 
						                                                                            <option						                                                                  
																									value="<c:out value="${element.paramCode}"/>">
																									<c:out value="${element.paramName}" />
																							</option>																																				
																						    </c:forEach>    
                                                                                            </select> --%>
																						<span id="msg_phonetype" class="hidden"
																							style="color: red; text-align: left">Please
																							select your Phone Type</span>
																					</div>
																					<div class="col-sm-8">
																						<spring:bind path="agentProfile.phoneNumber">
																							<input type="text" required class="form-control"
																								maxlength="20" name="${status.expression}"
																								id="phoneNo" value="${status.value}" />
																						</spring:bind>
																						<!-- <input type="text" required class="form-control" name="phoneNO" id="phoneNo" maxlength="20"> -->
																						<span id="msg_phoneno" class="hidden"
																							style="color: red; text-align: left">Please
																							key in your phone number</span>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Email</label>
																			</div>
																			<div class="col-sm-9">
																				<spring:bind path="agentProfile.email">
																					<input type="text" required placeholder="Email"
																						class="form-control" name="${status.expression}"
																						id="${status.expression}" value="${status.value}"
																						maxlength="50" onblur="validateAgentEmail();" />
																				</spring:bind>
																				<!--   <input type="text" required placeholder="Email" class="form-control" name="email" id="email" maxlength="30" onblur="validateAgentEmail();"> -->
																				<span id="msg_email" class="hidden"
																					style="color: red; text-align: left">Please
																					key in your Email ID</span> <span id="msg_validmail"
																					class="hidden" style="color: red; font-size: bold">Please
																					Enter valid email ID</span> <span id="msg_validagentmail"
																					class="hidden" style="color: red; font-size: bold"></span>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<div class="col-sm-3 column text-right">
																				<label class="control-label">Address</label>
																			</div>
																			<div class="col-sm-9">
																				<div class="form-group">
																					<div class="col-sm-12">
																						<spring:bind path="agentProfile.address1">
																							<input type="text" required class="form-control"
																								name="${status.expression}"
																								id="${status.expression}"
																								value="${status.value}" maxlength="150" />
																						</spring:bind>
																						<!--  <input type="text" required class="form-control" name="address1" id="address1" maxlength="150"> -->
																						<span id="msg_address" class="hidden"
																							style="color: red; text-align: left">Please
																							key in your Address</span>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="col-sm-12">
																						<spring:bind path="agentProfile.address2">
																							<input type="text" class="form-control"
																								name="${status.expression}"
																								id="${status.expression}"
																								value="${status.value}" maxlength="150" />
																						</spring:bind>
																						<!--  <input type="text" class="form-control" name="address2" id="address2" maxlength="150"> -->
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="col-sm-12">
																						<spring:bind path="agentProfile.address3">
																							<input type="text" class="form-control"
																								name="${status.expression}"
																								id="${status.expression}"
																								value="${status.value}" maxlength="150" />
																						</spring:bind>
																						<!--   <input type="text" class="form-control" name="address3" id="address3" maxlength="150"> -->
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="col-sm-5">
																						<spring:bind path="agentProfile.postCode">
																							<input type="text" class="form-control"
																								placeholder="Postcode" required
																								name="${status.expression}" id="postcode"
																								value="${status.value}" maxlength="6" />
																						</spring:bind>
																						<!--   <input type="text" class="form-control" placeholder="Postcode" required name="postcode" id="postcode" maxlength="6"> -->
																						<span id="msg_postcode" class="hidden"
																							style="color: red; text-align: left">Please
																							key in your Postcode</span>
																					</div>
																					<!--  <div class="col-sm-7">
                                                                                            <select class="form-control" name="city"  id="city">
                                                                                            	<option >Select</option>
                                                                                                <option >Petaling Jaya</option>
                                                                                                <option >Bangsar</option>
                                                                                                
                                                                                            </select>
                                                                                            <span id="msg_city" class="hidden" style="color:red;text-align:left">Please select your city</span>
                                                                                        </div> -->
																				</div>

																				<div class="form-group">
																					<div class="col-sm-12">
																						<spring:bind path="agentProfile.state">
																							<select class="form-control" name="state"
																								id="state">
																								<option value="">Select</option>
																								<c:forEach items="${agentStateList}"
																									var="element">
																									<option
																										<c:if test="${status.value eq element.paramCode}" >selected</c:if>
																										value="<c:out value="${element.paramCode}"/>">
																										<c:out value="${element.paramName}" />
																									</option>
																								</c:forEach>

																							</select>
																						</spring:bind>
																						<%--  <select class="form-control" name="state" id="state">
                                                                                            	<option value="">Select</option>                                          
                                                                                                <c:forEach items="${agentStateList}" var="element"> 
						                                                                            <option						                                                                  
																									value="<c:out value="${element.paramCode}"/>">
																									<c:out value="${element.paramName}" />
																							</option>																																				
																						</c:forEach>              
                                                                                            </select> --%>
																						<span id="msg_state" class="hidden"
																							style="color: red; text-align: left">Please
																							select your state</span>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="col-sm-12">
																						<spring:bind path="agentProfile.country">
																							<select class="form-control" name="country">
																								<option value="Malaysia" selected="">Malaysia</option>
																							</select>
																						</spring:bind>
																						<!--  <select class="form-control" name="country">
                                                                                                <option value="Malaysia" selected="">Malaysia</option>
                                                                                            </select> -->
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>

														</div>
														<!--content inner -->
													</div>
													<!--col-sm-12 -->

													<!-- Captcha -->
													<div
														class="col-xs-5 col-sm-3 col-centered text-center captcha-wrapper space-top">
														<table border="0" width="100%">
															<tr>
																<td width="100%">
																	<!-- START CAPTCHA --> <botDetect:captcha
																		id="basicExample" userInputID="captchaCode"
																		codeLength="5" imageWidth="150" imageStyle="GRAFFITI2" />

																	<!-- END CAPTCHA -->
																</td>
																<!-- <td width="10%">&nbsp;</td> -->
															</tr>
															<tr>
																<td>
																	<div class="validationDiv">
																		<input name="captchaCode" type="text" id="captchaCode"
																			value="${basicExample.captchaCode}" />
																		<!--  <input type="submit" name="validateCaptchaButton" value="Validate" id="validateCaptchaButton" /> -->
																		<%--  <span class="correct">${basicExample.captchaCorrect}</span> --%>
																		<span class="incorrect" style="color: red">${basicExample.captchaIncorrect}</span>
																	</div>
																</td>
															</tr>
														</table>
													</div>




													<%-- <div class="col-xs-5 col-sm-3 col-centered text-center captcha-wrapper">
											                <c:if  test="${not empty error_c}" >
											               <div class="modal-body text-center col-sm-11 cap-fix">
											                   <table border="0" width="100%">
											                       <tr>
											                           <td width="100%">
											                                <span style="color:red" id="lan_en">Invalid Captcha, please re-try</span>
											                               <!--  <span style="color:red" id="lan_bm">CAPTCHA tidak Sah. sila cuba lagi</span> -->
											                           </td>
											                       </tr>
											                   </table>
											               </div>
											           </c:if>
											       </div>
											       <div class="col-xs-5 col-sm-3 col-centered text-center captcha-wrapper space-top">
											           <table border="0" width="100%">
											               <tr>
											                   <td width="100%">
											                       <!-- START CAPTCHA -->
											                       <div class="g-recaptcha" data-size="normal" data-sitekey="6Lf6uQwUAAAAAFhY4vRBKZ2mCT0edG9ZMu0k8hKh"></div>
											                       <!-- END CAPTCHA -->
											                   </td>
											                   <!-- <td width="10%">&nbsp;</td> -->
											               </tr>
											           </table>
											       </div> --%>
													<!-- Captcha -->



													<div class="col-sm-12">
														<div class="text-right">
															<a href="#" class="btn btn-warning btn-sm"
																onClick="submitRegistration();"><i
																class="fa fa-edit"></i> Submit</a>
															<!--   <input class="btn btn-warning btn-sm" onClick="submitRegistration();validateAgentEmail();" type="submit" value="Submit"/> -->
														</div>
													</div>
													<!--col-sm-12 -->
												</div>
												<!--row -->
											</form:form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->
		</div>
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->

	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
            <div id="back-top">
                <a href="#top"><i class="fa fa-chevron-up"></i></a>
            </div>
            -->
	<!-- END BACK TO TOP -->


	<!--
            ===========================================================
            END PAGE
            ===========================================================
            -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%--    <jsp:include page="siteFooter.jsp" /> --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->
	<%--    <script src="<c:url value="/resources/assets/js/jquery-2.2.4.min.js" />"></script> --%>
	<script src="assets/js/jquery.min.js" type="text/javascript"></script>
	<!--  <script src="assets/jAlert/jquery-1.11.3.min.js"></script> -->
	<script src="assets/jAlert/jAlert.min.js"></script>
	<script src="assets/jAlert/jAlert-functions.min.js"></script>
	<script src="assets/js/bootstrap.js" type="text/javascript"></script>
	<script src="assets/plugins/retina/retina.min.js"></script>
	<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>
	<script src="assets/js/apps.js"></script>

	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<!-- <script src="assets/plugins/chosen/chosen.jquery.min.js"></script> -->
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script type="text/javascript" src="assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript" src="assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>

	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/buttons.print.min.js"></script>
	<script src="assets/js/buttons.colVis.min.js"></script>

	<script src="assets/js/toastr.min.js"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>



	<script>
         $(document).ready(function() {			
 		/* 	 $('#date1').datepicker({
 				    format: 'dd/mm/yyyy',
 			}).on('changeDate', function(e){
 			    $(this).datepicker('hide');
 			}); */
 			 
 			 
 			$('#linkButton').click(function() {
 		       // show when the button is clicked
 		       toastr.success('Click Button');

 		    });
 			 
 			
 		});
         
    </script>

	<script type="text/javascript">
       
            function submitRegistration(){
                result=true;     
                var emailFound=validateAgentEmail() ;
                var idNoFound=validateAgentId() ;
                if(!$('#referenceNo').val().length) {
                    $('#msg_refno').removeClass('hidden');
                    $('#msg_refno').addClass('visible');
                    /* $('#referenceNo').css("border-color", "red"); */
                    $('#referenceNo').focus();
                    result=false;
                }
                if(!$('#date1').val().length) {
                    $('#msg_date').removeClass('hidden');
                    $('#msg_date').addClass('visible');
                    $('#date1').focus();
                    result=false;
                }
                if(!$('#annualSalesTarget').val().length) {
                    $('#msg_salesTarget').removeClass('hidden');
                    $('#msg_salesTarget').addClass('visible');
                    $('#annualSalesTarget').focus();
                    result=false;
                }
               
                
                if(!$('#agentCategory').val()) {
                    $('#msg_agentcat').removeClass('hidden');
                    $('#msg_agentcat').addClass('visible');
                    $('#agentCategory').focus();
                    result=false;
                }
                if(!$('#agentType').val()) {
                    $('#msg_agenttype').removeClass('hidden');
                    $('#msg_agenttype').addClass('visible');
                    $('#agentType').focus();
                    result=false;
                }
                if(!$('#status').val()) {
                    $('#msg_agentstatus').removeClass('hidden');
                    $('#msg_agentstatus').addClass('visible');
                    $('#agentStatus').focus();
                    result=false;
                }
                if(!$('#agentAliasName').val().length) {
                    $('#msg_agentaliasname').removeClass('hidden');
                    $('#msg_agentaliasname').addClass('visible');
                    $('#agentAliasName').focus();
                    result=false;
                }
               
                if(!$('#agentName').val().length) {
                    $('#msg_agentname').removeClass('hidden');
                    $('#msg_agentname').addClass('visible');
                    $('#agentName').focus();
                    result=false;
                }
                if($('#idType').index()== 0) {
                    $('#msg_idtype').removeClass('hidden');
                    $('#msg_idtype').addClass('visible');
                    $('#idType').focus();
                    result=false;
                }
                if(!$('#idNo').val().length) {
                    $('#msg_idno').removeClass('hidden');
                    $('#msg_idno').addClass('visible');
                    $('#idNo').focus();
                    result=false;
                }
                if($('#phoneType').index() == 0) {
                    $('#msg_phonetype').removeClass('hidden');
                    $('#msg_phonetype').addClass('visible');
                    $('#phoneType').focus();
                    result=false;
                }
                if(!$('#phoneNo').val().length) {
                    $('#msg_phoneno').removeClass('hidden');
                    $('#msg_phoneno').addClass('visible');
                    $('#phoneNo').focus();
                    result=false;
                }
                if(!$('#email').val().length) {
                    $('#msg_email').removeClass('hidden');
                    $('#msg_email').addClass('visible');
                    $('#email').focus();
                    result=false;
                }
                
                if(!validateEmail($('#email').val())) {
                	 $('#msg_email').removeClass('hidden');
                     $('#msg_email').addClass('visible');
                     $('#email').focus();
                	result=false;
                }
                
                if(!$('#address1').val().length) {
                    $('#msg_address').removeClass('hidden');
                    $('#msg_address').addClass('visible');
                    $('#address1').focus();
                    result=false;
                }
                if(!$('#postcode').val().length) {
                    $('#msg_postcode').removeClass('hidden');
                    $('#msg_postcode').addClass('visible');
                    $('#postcode').focus();
                    result=false;
                }
                
                 if(emailFound=="y") {
                   
                    result=false;
                }
                if(idNoFound=="y") {
                    
                    result=false;
                }
                
                
                if (result==true) {
                	$('#regFrom').submit();   
                }
                
            }

            $('#referenceNo').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_refno').removeClass('visible');
                    $('#msg_refno').addClass('hidden');
                    /* $('#referenceNo').css("border-color", "#ccc"); */
                }
            });
            /* $('#agentCode').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_agentcode').removeClass('visible');
                    $('#msg_agentcode').addClass('hidden');
                }
            }); */
            /* $('#datepicker1').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_date').removeClass('visible');
                    $('#msg_date').addClass('hidden');
                }
            }); */
            $('#annualSalesTarget').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_salesTarget').removeClass('visible');
                    $('#msg_salesTarget').addClass('hidden');
                }
            });
            $('#agentCategory').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_agentcat').removeClass('visible');
                    $('#msg_agentcat').addClass('hidden');
                }
            });
            $('#insuranceType').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_instype').removeClass('visible');
                    $('#msg_instype').addClass('hidden');
                }
            });
            $('#agentStatus').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_agentstatus').removeClass('visible');
                    $('#msg_agentstatus').addClass('hidden');
                }
            });
            $('#agentAliasName').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_agentaliasname').removeClass('visible');
                    $('#msg_agentaliasname').addClass('hidden');
                }
            });
            $('#products').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_productselect').removeClass('visible');
                    $('#msg_productselect').addClass('hidden');
                }
            });
            $('#registrationDocument').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_agentregdoc').removeClass('visible');
                    $('#msg_agentregdoc').addClass('hidden');
                }
            });
            $('#agentName').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_agentname').removeClass('visible');
                    $('#msg_agentname').addClass('hidden');
                }
            });
            $('#idType').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_idtype').removeClass('visible');
                    $('#msg_idtype').addClass('hidden');
                }
            });
            $('#idNo').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_idno').removeClass('visible');
                    $('#msg_idno').addClass('hidden');
                }
            });
            $('#phoneType').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_phonetype').removeClass('visible');
                    $('#msg_phonetype').addClass('hidden');
                }
            });
            $('#phoneNo').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_phoneno').removeClass('visible');
                    $('#msg_phoneno').addClass('hidden');
                }
            });
            $('#email').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_email').removeClass('visible');
                    $('#msg_email').addClass('hidden');
                    $('#msg_validagentmail').removeClass("visible");
	        		$('#msg_validagentmail').addClass("hidden");
	        		$(this).removeClass("inputError");
                }
                if (validateEmail(this.value)) {
                    $('#msg_validmail').removeClass('visible');
                    $('#msg_validmail').addClass('hidden');
                } 
            });
            $('#address1').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_address').removeClass('visible');
                    $('#msg_address').addClass('hidden');
                }
            });
            $('#postcode').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_postcode').removeClass('visible');
                    $('#msg_postcode').addClass('hidden');
                }
            });
            $('#city').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_city').removeClass('visible');
                    $('#msg_city').addClass('hidden');
                }
            });
            $('#state').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_state').removeClass('visible');
                    $('#msg_state').addClass('hidden');
                }
            });

            

            $(document).ready(function() {
                $('#postcode').blur(function(e) {
                    if (validatePhone('postcode')) {
                       //alert('valid');
                        $('#msg_postcode').removeClass('visible');
                        $('#msg_postcode').addClass('hidden');
                    }
                    else {
                    //  alert('invalid');
                        $('#msg_postcode').removeClass('hidden');
                        $('#msg_postcode').addClass('visible');
                    }
                });
                
                $('#basicExample_CaptchaImageDiv>a').remove(); 
            });
            
//------------------- Input Format Validation 

$("#idNo").keyup(function(e){     
    var str = $.trim( $(this).val() );
    if( str != "" ) {
      var regx = /^[A-Za-z0-9]+$/;
      if (!regx.test(str)) {
    	  e.preventDefault();
    	   return false;
      }
    }
});
            // Force Numeric Only 
            jQuery.fn.ForceNumericOnly = function(){
				return this.each(function(){
					$(this).keydown(function(e){
						var key = e.charCode || e.keyCode || 0;
						// allow backspace, tab, delete, arrows, letters, numbers and numpad numbers ONLY
						return ( key == 8 || key == 9 || key == 46 || key == 32 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));                
					})
				})
			};
            // Function Numbers
            //for (var i=1;i<=counter;i++){
            /*$("#email").validateEmail();*/
           
            //$("#agentCode").ForceNumericOnly();
            $("#annualSalesTarget").ForceNumericOnly();
            //$("#idNo").ForceNumericOnly();
            $("#phoneNo").ForceNumericOnly();
            $("#postcode").ForceNumericOnly();
            //}

            /////////////// Fetching State from Postcode ////////////////  
            /*function fetch_state() { 
                //alert(document.getElementById("postcode").value);
                var obj;
                var arr = [];
                var formData = {postcode:document.getElementById("postcode").value}; //Array 
                if (document.getElementById("postcode").value.length != 0 && document.getElementById("postcode").value.length >= 4) {
                    $.ajax({
                        type: "post", 
                        url: "/getonline/sites/REST/controller/TravellerController/fetch_State",
                        data: formData,
                        success: function (data) {
                            if(data) {
                                document.getElementById("state").value=data;
                            }

                        },
                        error: function (request, status, error) {
                            alert(request.responseText);
                        }
                    }); 
                } 
            }*/
            function validateEmail(sEmail) {
                var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if (filter.test(sEmail)) {
                    return true;
                }
                else {
                    return false;
                }
            }

            function validatePhone(postcode) {
                var a = document.getElementById(postcode).value;
                var filter = /^[0-9-+]+$/;
                if (filter.test(a)) {
                    return true;
                }
                else {
                    return false;
                }
            }

            function checkOnBlur(me, message) {
                var  e_id = $(me).attr('id');
                if (!(me.value).length) {
                    var exist= document.getElementById(e_id+ '_e');
                    if (exist == null) {
                        var htmlString="";
                        htmlString+='<label id='+ e_id+ '_e' + '  ' + 'class="error">'+ message + '</label>';
                        $(htmlString).insertAfter("#"+ e_id);
                        $('#'+ e_id).focus();
                    }
                }
            }
                //////////////////////////////////////////////////////////////////////////////////////////////  
            /*function checkKeyUp(me) {
                var  e_id = $(me).attr('id');
                if($.trim(me.value).length) {
                    var exist= document.getElementById(e_id+ '_e');
                    if (exist != null) {
                        $('#'+ e_id + '_e').remove();
                    }
                }
            }*/
            //////////////////////////////////////////////////////////////////////////////////////////////
            function checkOnChange(me) {
                var  e_id = $(me).attr('id');
                if($.trim(me.value).length) {
                    var exist= document.getElementById(e_id+ '_e');
                    if (exist != null) {
                        $('#'+ e_id + '_e').remove();
                    }
                }
            }
            ///////////////////////////////////////////////////////////////////////////////////////////////
			
			/////////////////////////////////////////////////////////////////////////////////////////////////
   				function validateAgentEmail() {	
  					emailExist="n";
  					var email= document.getElementById("email").value;
  			    if(email.length > 0){
    		    $.ajax({
    	        url: 'validateAgentEmail.html',
    	        type: 'GET',
    	        async: false,
    	        data: ({email : email}),
    	        success: function(data) {
    	        	var obj = jQuery.parseJSON( data );
    	        
    	        	if(obj.validateEmailFlag == 'y') {
    	        		$('#email').addClass("inputError");
    	        		//$('#msg_validagentmail').text("Email Already Registered, Please Choose Another Email!");
    	        		//$('#msg_validagentmail').removeClass("hidden");
    	        		//$('#msg_validagentmail').addClass("visible");
    	        		emailExist="y";
    	        		errorAlert("Error",email +" Already Registered, Please Choose Another Email!")
    	        		  //alert(email +" Already Registered, Please Choose Another Email!");
    	        	
    	        		//document.getElementById("email").value="";
    	        	}    	        	
					  	        	
    	        }
    	      });
  			     } 
  				return 	emailExist; 
     		    } 
  				
  				
   				function validateAgentId() {	
  					idNoExist="n";
  					
  					 if(( $('#idNo').val().length)){
  					var idNo= document.getElementById("idNo").value;
    		   
    		    $.ajax({
    	        url: 'validateAgentId.html',
    	        type: 'GET',
    	        async: false,
    	        data: ({idNo : idNo}),
    	        success: function(data) {
    	        	var obj = jQuery.parseJSON( data );
    	        
    	        	if(obj.validateidNoFlag == 'y') {
    	        		$('#idNo').addClass("inputError");
    	        		//$('#msg_validagentmail').text("Email Already Registered, Please Choose Another Email!");
    	        		//$('#msg_validagentmail').removeClass("hidden");
    	        		//$('#msg_validagentmail').addClass("visible");
    	        		idNoExist="y";
    	        		errorAlert("Error",idNo +" Already Registered, Please Choose Another ID No!")
    	        		  //alert(email +" Already Registered, Please Choose Another Email!");
    	        	
    	        		//document.getElementById("email").value="";
    	        	}    	        	
					  	        	
    	        }
    	      });
  			     } 
  				return 	idNoExist; 
     		    } 
  				
  			
            /*var js = jQuery.noConflict();
            js(document).ready(function() {
                js("#registration").validate({
                    rules: {
                        phoneType: {
                            required: true
                        }
                    },
                    messages: {
                        phoneType: {
                            required: "You must select a timezone"
                        }
                    }

                });
            });*/

        </script>
</body>
</html>