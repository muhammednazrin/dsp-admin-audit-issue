<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*,java.sql.*,java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<%@ page import="com.etiqa.model.report.TransactionalReport"%>
<%@ page import="com.etiqa.controller.ReportDetail"%>


<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>

</head>
<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>

					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Report &amp; Analytics</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<%
                                      

               			   SimpleDateFormat ft2 =  new SimpleDateFormat ("dd-MMM-yyyy");
               			 
                            DateFormat format = new SimpleDateFormat("ddMMyyyy");
                            String term="";
                            
                            DecimalFormat formatter = new DecimalFormat("#,###.00");
                            String homeCoverage="";
            		
                            
                            String transactionStatus="";
								 System.out.println(request.getAttribute("data"));
								 if (request.getAttribute("data") != null) {
									List<TransactionalReport> detail = (List<TransactionalReport>) request.getAttribute("data");
								   
									
										for(TransactionalReport txnDetail : detail) {
								%>
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="row">
																<div class="col-xs-12 form-inline">
																	<div class="col-xs-6">
																		<div class="row">
																			<label> Client Details </label>
																		</div>
																	</div>
																	<div class="col-xs-6">
																		<div class="row">
																			<div class="pull-right">

																				<!--  <button class="btn btn-warning btn-sm">Print <i class="fa fa-print" aria-hidden="true"></i></button>
                                                               <button class="btn btn-warning btn-sm">Export to XLS <i class="fa fa-download" aria-hidden="true"></i></button>-->


																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="table-responsive">
																<table
																	class="table table-striped table-warning table-hover"
																	id="showCustInfo">
																	<thead>
																		<tr>
																			<th>NRIC</th>
																			<th>Name</th>
																			<th>Email</th>
																			<th>Address</th>
																			<th>Postcode</th>
																			<th>State</th>
																			<th>Mobile Number</th>
																		</tr>
																	</thead>
																	<tbody>
																		<tr>



																			<td><%=txnDetail.getCustomerNRIC()%></td>
																			<td><%=txnDetail.getCustomerName()%></td>
																			<td><%=txnDetail.getCustomerEmail()%></td>
																			<td><%=txnDetail.getCustomerAddress1() %> ,<%=txnDetail.getCustomerAddress2() %>,<%=txnDetail.getCustomerAddress3() %>
																			</td>
																			<td><%=txnDetail.getCustomerPostcode() %></td>
																			<td><%=txnDetail.getCustomerState() %></td>
																			<td><%=txnDetail.getCustomerMobileNo() %></td>


																		</tr>

																	</tbody>
																</table>
															</div>

															<div style="display: block;">
																<button id="editCustInfo" class="btn btn-warning btn-sm">
																	Edit <i class="fa fa-edit" aria-hidden="true"></i>
																</button>
															</div>
															<div id="submitbtn" style="display: none;">
																<button type="submit"
																	class="btn btn-warning btn-sm text-right">
																	Save<i class="fa fa-edit" aria-hidden="true"></i>
																</button>
															</div>
															<!-- /.table-responsive -->

															<!-- Start Form -->
															<form name="editCustInfoForm"
																action="TransactionalReport" method="post"
																id="editCustInfoForm">
																<input type="hidden" name="action"
																	value="saveCustomerInfo" /> <input type="hidden"
																	name="customerID"
																	value="<%=txnDetail.getCustomerID()%>" /> <input
																	type="hidden" name="id" value="${param.id}" /> <input
																	type="hidden" name="dspQQID"
																	value="<%=txnDetail.getDSPQQID()%>" />
																<div class="content-inner">
																	<div class="the-box">
																		<div class="col-sm-6">
																			<div class="form-horizontal">

																				<div class="form-group">
																					<label class="col-sm-3 control-label">NRIC/ID
																						No.</label>
																					<div class="col-sm-9">
																						<input type="text" placeholder=""
																							class="form-control" name="NRIC"
																							value="<%=txnDetail.getCustomerNRIC() %>">
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-sm-3 control-label">Name</label>

																					<div class="col-sm-9">
																						<input type="text" placeholder=""
																							class="form-control" name="name"
																							value="<%=txnDetail.getCustomerName() %>">
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label">Email</label>

																					<div class="col-sm-9">
																						<input type="text" placeholder=""
																							class="form-control" name="email"
																							value="<%=txnDetail.getCustomerEmail()%>">
																					</div>
																				</div>


																				<div class="form-group">
																					<label class="col-sm-3 control-label">Address
																						1</label>

																					<div class="col-sm-9">
																						<input type="text" placeholder=""
																							class="form-control" name="address1"
																							value="<%=txnDetail.getCustomerAddress1() %>">
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label">Address
																						2</label>

																					<div class="col-sm-9">
																						<input type="text" placeholder=""
																							class="form-control" name="address2"
																							value="<%=txnDetail.getCustomerAddress2() %>">
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label">Address
																						3/City</label>

																					<div class="col-sm-9">
																						<input type="text" placeholder=""
																							class="form-control" name="address3"
																							value="<%=txnDetail.getCustomerAddress3() %>">
																					</div>
																				</div>


																				<div class="form-group">
																					<label class="col-sm-3 control-label">Postcode</label>

																					<div class="col-sm-9">
																						<input type="text" placeholder=""
																							class="form-control" name="postcode"
																							value="<%=txnDetail.getCustomerPostcode() %>">
																					</div>
																				</div>



																				<div class="form-group">
																					<label class="col-sm-3 control-label">State</label>
																					<div class="col-sm-9">
																						<input type="text" placeholder=""
																							class="form-control" name="state"
																							value="<%=txnDetail.getCustomerState() %>">
																					</div>
																				</div>



																				<div class="form-group">
																					<label class="col-sm-3 control-label">Mobile
																						No</label>
																					<div class="col-sm-9">
																						<input type="text" placeholder=""
																							class="form-control" name="mobileNo"
																							value="<%=txnDetail.getCustomerMobileNo() %>">
																					</div>
																				</div>


																				<div class="col-sm-12 text-right">
																					<button type="submit" class="btn btn-default"
																						name="saveCustInfo">Save</button>
																					<button type="button" class="btn btn-default"
																						name="cancelCustInfo" id="cancelCustInfo">Cancel</button>
																				</div>
																			</div>
																		</div>
															</form>
															<!-- END FORM -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>

												<div class="col-sm-12">

													<form name="editTxnInfoForm" action="TransactionalReport"
														method="post" id="editTxnInfoForm"
														onsubmit="return validateForm()">
														<input type="hidden" name="action" value="saveTxnInfo" />
														<input type="hidden" name="id" value="${param.id}" /> <input
															type="hidden" name="preData"
															value="<%=txnDetail.getStatus()%>" /> <input
															type="hidden" name="dspQQID"
															value="<%=txnDetail.getDSPQQID()%>" /> <input
															type="hidden" name="simpleNoteTran"
															value="<%=txnDetail.getSimplifiedResult()%>" />
														<div class="content-inner">
															<div class="the-box">
																<div class="title">
																	<div class="sub">
																		<h4>Transaction Details</h4>
																	</div>
																</div>
																<div class="col-sm-9">
																	<div class="form-horizontal">


																		<div class="form-group">
																			<label class="col-sm-3 control-label">Transaction
																				Date</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=txnDetail.getTransactionDate() %></label>
																			</div>
																		</div>


																		<div class="form-group">
																			<label class="col-sm-3 control-label">Policy/Certificate
																				No.</label>
																			<div class="col-sm-9">
																				<% if (txnDetail.getProductType().indexOf("L") !=-1 && txnDetail.getStatus().equals("S") || txnDetail.getProductType().indexOf("D") !=-1 && txnDetail.getStatus().equals("S") || (txnDetail.getProductType().indexOf("Ez") !=-1 && txnDetail.getStatus().equals("S")) || (txnDetail.getProductType().indexOf("i") !=-1 && txnDetail.getStatus().equals("S")) || (txnDetail.getProductType().indexOf("e-Cancer") !=-1 && txnDetail.getStatus().equals("S") )  || (txnDetail.getProductType().indexOf("Medical") !=-1 && txnDetail.getStatus().equals("S") )) { %>
																				<label class="control-label"><%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%></label>
																				<input type="hidden" name="policyNo"
																					value="<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>" />
																				<% }else if(txnDetail.getProductType().indexOf("L") ==-1 || txnDetail.getProductType().indexOf("D") ==-1 || txnDetail.getProductType().indexOf("i") ==-1  || txnDetail.getProductType().indexOf("e-Cancer") ==-1 || txnDetail.getProductType().indexOf("Medical") ==-1 ){
                                                            	%>
																				<label class="control-label"><%=txnDetail.getPolicyNo()%></label>
																				<input type="hidden" name="policyNo"
																					value="<%=txnDetail.getPolicyNo()%>" />
																				<% 
                                                              }
                                                              
                                                              if(!txnDetail.getCAPSPolicyNo().equals("")){
                                                              
                                                            	  %>
																				<br>
																				<label class="control-label"><%=txnDetail.getCAPSPolicyNo()%></label>
																				<% 
                                                              }
                                                              %>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Product</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=txnDetail.getProductType()%></label>
																			</div>
																		</div>

																		<% 
	 													if (txnDetail.getProductType().indexOf("Buddy") !=-1)
                                                            { %>


																		<div class="form-group">
																			<label class="col-sm-3 control-label">Buddy
																				Combo</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=txnDetail.getBuddyCombo()%></label>
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Plan
																				Name</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=txnDetail.getPlanName()%></label>
																			</div>
																		</div>
																		<%   }
 																if (txnDetail.getProductType().indexOf("Travel Ezy") !=-1)
                                                           { %>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Trip
																				Type</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=txnDetail.getTripType()%></label>
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Departure
																				Date</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=txnDetail.getDepartDate()%></label>
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Return
																				Date</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=txnDetail.getReturnDate()%></label>
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Number
																				Of Companion(s)</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=txnDetail.getNumberOfCompanion()%></label>
																			</div>
																		</div>
																		<%   }
	 													if (txnDetail.getProductType().indexOf("L") !=-1 || txnDetail.getProductType().indexOf("Ez") !=-1)
                                                            { %>



																		<div class="form-group">
																			<label class="col-sm-3 control-label">Coverage</label>
																			<div class="col-sm-9">
																				<label class="control-label">RM <%=txnDetail.getCoverage()%></label>
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Term</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=txnDetail.getTerm()%>
																					Years</label>
																			</div>
																		</div>


																		<div class="form-group">
																			<label class="col-sm-3 control-label">Mode</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=txnDetail.getPremiumMode()%></label>

																			</div>
																		</div>
																		
																		<%   }
																		if (txnDetail.getProductType().indexOf("e-Cancer") !=-1 )
                                                            { %>



																		<div class="form-group">
																			<label class="col-sm-3 control-label">Coverage</label>
																			<div class="col-sm-9">
																				<label class="control-label">RM <%=txnDetail.getCoverage()%></label>
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Term</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=txnDetail.getTerm()%>
																					Years</label>
																			</div>
																		</div>


																		<div class="form-group">
																			<label class="col-sm-3 control-label">Mode</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=txnDetail.getPremiumMode()%></label>

																			</div>
																		</div>
																		
																		<%   }
																		if (txnDetail.getProductType().indexOf("Medical") !=-1 )
                                                            { %>



																		<div class="form-group">
																			<label class="col-sm-3 control-label">Coverage</label>
																			<div class="col-sm-9">
																				<label class="control-label">RM <%=txnDetail.getCoverage()%></label>
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Term</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=txnDetail.getTerm()%>
																					Years</label>
																			</div>
																		</div>


																		<div class="form-group">
																			<label class="col-sm-3 control-label">Mode</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=txnDetail.getPremiumMode()%></label>

																			</div>
																		</div>
																		
										
																		<%   }if (txnDetail.getProductType().indexOf("TripCare") !=-1)
                                                           { %>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Adventurous
																				Activity</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=txnDetail.getAdventurousActivity()%></label>
																			</div>
																		</div>
																		<%   }
										                 %>



																		<c:set var="txnStatus"
																			value="<%=txnDetail.getStatus()%>" />

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Status</label>
																			<div class="col-sm-9">
																				<select class="form-control editField"
																					disabled="disabled" id="editTxnStatus"
																					name="status">


																					<option value="S"
																						<c:if test="${txnStatus == 'S'}" >selected</c:if>>Successful</option>
																					<option value="F"
																						<c:if test="${txnStatus == 'F'}" >selected</c:if>>Fail
																						Payment</option>
																					<option value="AP"
																						<c:if test="${txnStatus == 'AP'}" >selected</c:if>>Attempt
																						Payment</option>
																					<option value="O"
																						<c:if test="${txnStatus == 'O'}" >selected</c:if>>Incomplete</option>
																					<option value="C"
																						<c:if test="${txnStatus == 'C'}">selected</c:if>>Cancelled</option>
																					<option value="R"
																						<c:if test="${txnStatus == 'R'}">selected</c:if>>Rejection</option>
																				</select>
																			</div>

																		</div>

																		<%  if (txnDetail.getProductType().indexOf("L") ==-1 || txnDetail.getProductType().indexOf("Ez") !=-1)
			 									{  %>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Commission</label>
																			<div class="col-sm-9">
																				<label class="control-label"></label>
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Discount</label>
																			<div class="col-sm-9">
																				<label class="control-label"></label>
																			</div>
																		</div>


																		<div class="form-group">
																			<label class="col-sm-3 control-label">Incentive</label>
																			<div class="col-sm-9">
																				<label class="control-label"></label>
																			</div>
																		</div>
																		<%} %>
																		
																									<%  if (txnDetail.getProductType().indexOf("e-Cancer") ==-1) {  %>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Commission</label>
																			<div class="col-sm-9">
																				<label class="control-label"></label>
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Discount</label>
																			<div class="col-sm-9">
																				<label class="control-label"></label>
																			</div>
																		</div>


																		<div class="form-group">
																			<label class="col-sm-3 control-label">Incentive</label>
																			<div class="col-sm-9">
																				<label class="control-label"></label>
																			</div>
																		</div>
																		<%} %>
																		
																			<%  if (txnDetail.getProductType().indexOf("Medical") ==-1) {  %>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Commission</label>
																			<div class="col-sm-9">
																				<label class="control-label"></label>
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Discount</label>
																			<div class="col-sm-9">
																				<label class="control-label"></label>
																			</div>
																		</div>


																		<div class="form-group">
																			<label class="col-sm-3 control-label">Incentive</label>
																			<div class="col-sm-9">
																				<label class="control-label"></label>
																			</div>
																		</div>
																		<%} %>
																		
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Premium/Contribution</label>
																			<div class="col-sm-9">
																				<label class="control-label"> RM <%=txnDetail.getPremiumAmount() %></label>
																			</div>
																		</div>
																		<%  if (txnDetail.getProductType().indexOf("L") ==-1 || txnDetail.getProductType().indexOf("Ez") !=-1)
 									{  %>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Bank
																				Fee</label>
																			<div class="col-sm-9">
																				<label class="control-label"></label>
																			</div>
																		</div>
																		<%} %>
																		
																		<%  if (txnDetail.getProductType().indexOf("e-Cancer") ==-1 ) {  %>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Bank
																				Fee</label>
																			<div class="col-sm-9">
																				<label class="control-label"></label>
																			</div>
																		</div>
																		<%} %>
																		
																		<%  if (txnDetail.getProductType().indexOf("Medical") ==-1 ) {  %>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Bank
																				Fee</label>
																			<div class="col-sm-9">
																				<label class="control-label"></label>
																			</div>
																		</div>
																		<%} %>
																		
																		<% if (txnDetail.getPaymentChannel().equals("MPAY"))
                                                                    
                                                                    {
                                                                    	%>
																		<div class="form-group">
																			<label class="col-sm-3 control-label"><%=txnDetail.getPaymentChannel()%>
																				Reference Number</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=txnDetail.getMPAY_RefNo()%></label>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label"><%=txnDetail.getPaymentChannel()%>
																				Auth Code</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=txnDetail.getMPAY_AuthCode()%></label>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label"><%=txnDetail.getPaymentChannel()%>
																				Mode</label>
																			<div class="col-sm-9">
																				<label class="control-label"> <%=txnDetail.getPremiumMode()%>
																				</label>
																			</div>
																		</div>


																		<% 
                                                      } 
                                                    else if(txnDetail.getPaymentChannel().equals("EBPG"))
                                                    
                                                    {
                                                    	
                                                    	%>



																		<div class="form-group">
																			<label class="col-sm-3 control-label"><%=txnDetail.getPaymentChannel()%>
																				Reference Number</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=txnDetail.getEBPG_RefNo()%></label>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label"><%=txnDetail.getPaymentChannel()%>
																				Auth Code</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=txnDetail.getEBPG_AuthCode()%></label>
																			</div>
																		</div>



																		<% 	
                                                    	
                                                    	
                                                    }
                                                    
                                                    else if(txnDetail.getPaymentChannel().equals("M2U"))
                                                        
                                                    {
                                                    %>

																		<div class="form-group">
																			<label class="col-sm-3 control-label"><%=txnDetail.getPaymentChannel()%>
																				Reference Number</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=txnDetail.getM2U_RefNo()%></label>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label"><%=txnDetail.getPaymentChannel()%>
																				Approval Code</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=txnDetail.getM2U_AuthCode()%></label>
																			</div>
																		</div>

																		<% if(txnDetail.getPaymentChannel().equals("M2U") && txnDetail.getStatus().equals("AP")){ %>

																		<% 	}
                                                    }
                                                    
                                                    
                                                else if(txnDetail.getPaymentChannel().equals("FPX"))
                                                        
                                                    {
                                                    
                                                    
                                                    
                                                                    	if (txnDetail.getFPX_AuthCode().equals("00"))
                                                            			{
                                                            				
                                                            				transactionStatus= txnDetail.getFPX_AuthCode()+"-Successful";
                                                            					
                                                            			}
                                                                    	
                                                            			else if (!txnDetail.getFPX_AuthCode().equals("00") && !txnDetail.getFPX_AuthCode().equals("") )
                                                            			{
                                                            				transactionStatus= txnDetail.getFPX_AuthCode()+"-Unsuccessful";
                                                            				
                                                            			}
                                                            			else
                                                            			{
                                                            				
                                                            				transactionStatus="";
                                                            				
                                                            			}
                                                                    	%>




																		<div class="form-group">
																			<label class="col-sm-3 control-label"><%=txnDetail.getPaymentChannel()%>
																				Reference Number</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=txnDetail.getFPX_RefNo()%></label>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label"><%=txnDetail.getPaymentChannel()%>
																				Status</label>
																			<div class="col-sm-9">
																				<label class="control-label"><%=transactionStatus%></label>
																			</div>
																		</div>


																		<% 	
                                                    }

                                                        %>


																		<div class="form-group">
																			<label class="col-sm-3 control-label">*
																				Remarks</label>
																			<div class="col-sm-9">
																				<textarea class="editField" rows="4" cols="90"
																					disabled="disabled" id="editRemark" name="remark">
																					<% if (txnDetail.getAuditRemarks() !=null){%> <%=txnDetail.getAuditRemarks()%> <%} %>
																				</textarea>
																			</div>
																		</div>
																		<a class="btn btn-danger editBtn2" id="editTxnInfo">Edit</a>
																		<button type="submit" class="btn btn-default"
																			name="saveTxnInfo" id="saveTxnInfo">Save</button>
																		<button type="button" class="btn btn-default"
																			name="cancelTxnInfo" id="cancelTxnInfo">Cancel</button>
																	</div>
																</div>
															</div>
														</div>
													</form>
												</div>


												<%  if (txnDetail.getProductType().indexOf("H") !=-1)
 									{  %>


												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="title">
																<div class="sub">
																	<h4>Product Details</h4>
																</div>
															</div>
															<!-- /.table-responsive -->
															<div class="table-responsive">
																<table
																	class="table table-striped table-warning table-hover">
																	<tbody>
																		<tr>


																			<th>Home Coverage</th>
																			<td>:</td>

																			<%
                                                            if(txnDetail.getHomeCoverage().equals("building_content")){
                                                            homeCoverage="Home Building + Home Content";
                                                            	
                                                            }
                                                            else if (txnDetail.getHomeCoverage().equals("content")){
                                                            homeCoverage="Home Content";	
                                                            	
                                                            	
                                                            }
                                                            else if(txnDetail.getHomeCoverage().equals("building")){
                                                            homeCoverage="Home Buidling";	
                                                            	
                                                            }
                                                            
                                                            %>
																			<td><%=homeCoverage%></td>
																		</tr>
																		<tr>
																			<th>Home Type</th>
																			<td>:</td>
																			<% if(txnDetail.getHomeType().equals("non_landed")){
                                                        	   
                                                        	  %>
																			<td>Non Landed Property</td>

																			<% 
                                                           }
                                                           else{
                                                        	   
 %>
																			<td>Landed Property</td>

																			<%    
                                                        	   
                                                           }
                                                            %>
																		</tr>
																		<tr>
																			<th>Building Construction Type</th>
																			<td>:</td>

																			<td><%=txnDetail.getBuildConstructionType()%></td>
																		</tr>
																		<tr>
																			<th>Building</th>
																			<td>:</td>

																			<%  
                                                            
                                                            if(!txnDetail.getHomeSumInsured().isEmpty() && !txnDetail.getHomeSumInsured().equals("") && !txnDetail.getHomeSumInsured().equals(null)&&!txnDetail.getHomeSumInsured().equals("0")){
                                                            double fmtHomeSumInsured = Double.parseDouble(txnDetail.getHomeSumInsured());
                                                                    	 
                                                                    	 %>
																			<td>RM <%=formatter.format(fmtHomeSumInsured)%></td>
																			<% }
 									
                                                            else
                                                            { 
                                                            %>
																			<td>-</td>
																			<%
                                                            }
                                                            %>
																		</tr>
																		<tr>
																			<th>Content</th>
																			<td>:</td>
																			<%  
                                                            
                                                            if(!txnDetail.getContentSumInsured().isEmpty() && !txnDetail.getContentSumInsured().equals("") && !txnDetail.getContentSumInsured().equals(null)&& !txnDetail.getContentSumInsured().equals("0")){
                                                            double fmtContentSumInsured = Double.parseDouble(txnDetail.getContentSumInsured());
                                                                    	 
                                                                    	 %>
																			<td>RM <%=formatter.format(fmtContentSumInsured)%></td>
																			<% }
                                                            else
                                                            { 
                                                            %>
																			<td>-</td>
																			<%
                                                            }
                                                            %>
																		</tr>
																		<tr>
																			<th>Additional Benefit</th>
																			<td>:</td>
																			<%
                                                            if(txnDetail.getAddBenRiotStrike().equals("1") && !txnDetail.getAddBenRiotStrikeAmt().equals("0.00")){
                                                            	
                                                            	 if(txnDetail.getAddBenExtendedTheft().equals("1") && !txnDetail.getAddBenExtendedTheftAmt().equals("0.00")){
                                                            	
                                                            	
                                                            	%>
																			<td>Riot,Strike and Malicious Damage (RM <%=txnDetail.getAddBenRiotStrikeAmt()%>)
																				<br> Extended Theft Cover (RM <%=txnDetail.getAddBenExtendedTheftAmt()%>)
																			</td>

																			<% 
                                                            	 } else{
                                                            		%>
																			<td>Riot,Strike and Malicious Damage (RM <%=txnDetail.getAddBenRiotStrikeAmt()%>)

																				<% }
                                                            }
                                                            else{
                                                            	
                                                            	
                                                            	if(txnDetail.getAddBenExtendedTheft().equals("1") && !txnDetail.getAddBenExtendedTheftAmt().equals("0.00")){
                                                                	%>
																			<td>Extended Theft Cover (RM <%=txnDetail.getAddBenExtendedTheftAmt()%>)
																			</td>

																			<% }	
                                                            	else{
                                                            		%>
																			<td>-</td>
																			<% }
                                                            	
                                                            }
         %>

																		</tr>


																		<tr>
																			<th>Home Content Declaration</th>
																			<td colspan="2"></td>
																		</tr>
																		<tr>
																			<td colspan="2">
																				<table class="table table-bordered">
																					<thead>
																						<tr>
																							<th>Type</th>
																							<th>Description</th>
																							<th>Itemized Value</th>
																						</tr>
																					</thead>
																					<tbody>
																						<% 
                                                         
                                                         
                                                       ReportDetail p = new ReportDetail();
      List<TransactionalReport> benefitList= p.hohhBenefit(txnDetail.getDSPQQID());
 
      if(benefitList.size() >0){
      
      for(int i=0;i<benefitList.size();i++){
    	  %>
																						<tr>
																							<td><c:out
																									value="<%=benefitList.get(i).getAdditionalBenefitCode() %>" /></td>
																							<td><c:out
																									value="<%=benefitList.get(i).getAdditionalBenefitText() %>" /></td>
																							<td><c:out
																									value="<%=benefitList.get(i).getAdditionalBenefitValue() %>" /></td>
																						</tr>


																						<% 	 } 
    	  
      }
      else
    	{ %>
																						<tr>
																							<td colspan="3">No record registered</td>
																						</tr>

																						<% }  %>


																					</tbody>
																				</table>
																			</td>
																		</tr>

																	</tbody>
																</table>
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>

												<% }
 if (txnDetail.getProductType().indexOf("M") !=-1) { %>
												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="row">
																<div class="col-xs-12 form-inline">
																	<div class="col-xs-6">
																		<div class="row">
																			<label> e-Policy/e-Certificate Information </label>
																		</div>
																	</div>
																	<div class="col-xs-6">
																		<div class="row">
																			<div class="pull-right">


																				<%  if (txnDetail.getDriversPA().equals("on") && txnDetail.getCAPSPolicyNo().equals("") && !txnDetail.getPAPREMIUM().equals("0") && txnDetail.getTPCDPPA().equals("no") && txnDetail.getMIQUOTATIONSTATUS().equals("C")) { %>



																				<a
																					href="TransactionalReport?action=generateCAPS&id=${param.id}"><button
																						class="btn btn-success btn-sm">Generate
																						CAPS Policy</button></a>

																				<%} %>



																				<!--<button class="btn btn-warning btn-sm">Print <i class="fa fa-print" aria-hidden="true"></i></button>
                                                               <button class="btn btn-warning btn-sm">Export to XLS <i class="fa fa-download" aria-hidden="true"></i></button>-->
																				<!--   <a href="#" data-toggle="modal"  data-target="#policyDetail" data-value=""><button class="btn btn-success btn-sm">Print View <i class="fa fa-search" aria-hidden="true"></i></button></a>  -->
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="table-responsive">
																<table
																	class="table table-striped table-warning table-hover">
																	<thead>
																		<tr>
																			<th style="width: 30px;">Note</th>
																			<th>Status</th>
																			<th>Period of Insurance/Cover</th>
																			<th>Plate No</th>
																			<th>Vehicle Location</th>
																			<th>JPJ Status</th>
																		</tr>
																	</thead>
																	<tbody>
																		<tr>
																			<td><%=txnDetail.getPolicyNo()%></td>
																			<td>
																				<% if(txnDetail.getJPJStatus().equals("") && !txnDetail.getPolicyNo().equals("")){
                                                            	
                                                            	if(null!=request.getAttribute("JPJStatusMsg") && ""!=request.getAttribute("JPJStatusMsg") ){%>

																				<%=request.getAttribute("JPJStatusMsg") %> <% }
                                                            	else{%> <a
																				href="TransactionalReport?action=checkJPJISMStatus&id=${param.id}"><button
																						class="btn btn-warning btn-sm">
																						Check JPJ Status <i class="fa fa-refresh"
																							aria-hidden="true"></i>
																					</button></a> <% }
                                                            	}
                                                            else{
                                                            	%> <%=txnDetail.getJPJStatus()%>
																				<%if(!txnDetail.getJPJStatus().equalsIgnoreCase("Message:GLB000000I OK")) { %><br>
																				<a
																				href="TransactionalReport?action=checkJPJISMStatus&id=${param.id}"><button
																						class="btn btn-warning btn-sm">
																						Check JPJ Status <i class="fa fa-refresh"
																							aria-hidden="true"></i>
																					</button></a> <% }%> <%} %>
																		
																			</td>
																			<% String SDate="";
																			   String EDate="";
																			if(!(txnDetail.getCoverageStartDate().equals(""))&&
																					(!txnDetail.getCoverageEndDate().equals(""))){
																				%>
																				<td><b>Date : </b><%=ft2.format(format.parse(txnDetail.getCoverageStartDate()))%>
																				- <%=ft2.format(format.parse(txnDetail.getCoverageEndDate()))%>
																			</td>	<% 
																			}else{%>
																				<td></td>
																			<%}%>
	
																			
																			<td><%=txnDetail.getVehicleNo() %></td>
																			<td><%=txnDetail.getVehicleLoc() %></td>
																			<td><%=txnDetail.getJpjDocType() %>-<%=txnDetail.getJpjReasonCode() %></td>
																		</tr>
																	</tbody>
																</table>

															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>
												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="row">
																<div class="col-xs-12 form-inline">
																	<div class="col-xs-6">
																		<div class="row">
																			<label> Vehicle Details </label>
																		</div>
																	</div>

																</div>
															</div>
															<div class="table-responsive">
																<table
																	class="table table-striped table-warning table-hover">
																	<thead>
																		<tr>
																			<th>Make & Model</th>
																			<th>Engine No</th>
																			<th>Vehicle Year</th>
																			<th>Financed By</th>
																			<th>Previous</th>
																			<th>NVIC</th>
																			<th>Chassis No</th>
																			<th>Plate No</th>
																		</tr>
																	</thead>
																	<tbody>
																		<tr>
																			<td><%=txnDetail.getVehicleMake() %></td>
																			<td><%=txnDetail.getEngineNo() %></td>
																			<td><%=txnDetail.getYearMake() %></td>
																			<td><%=txnDetail.getFinancedBy() %></td>
																			<td><%=txnDetail.getInsCompany() %></td>
																			<%
                                                            
                                                            	if(!txnDetail.getNVIC().isEmpty()&& !txnDetail.getNVIC().equals(null) )	
                                                            	
                                                            { %>
																			<td><%=txnDetail.getNVIC() %></td>



																			<%} else {
                                                            	%>
																			<td>-</td>
																			<%  }
                                                            	 
                                                            %>
																			<td><%=txnDetail.getChassisNo() %></td>
																			<td><%=txnDetail.getVehicleNo() %></td>
																		</tr>
																	</tbody>
																</table>
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>

												<% } 
                                      
                                      
                                      if(txnDetail.getStatus().equals("S") || txnDetail.getStatus().equals("SUCCESS")){
                                      
                                      %>


												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="title">
																<div class="sub">
																	<h4>Task</h4>
																</div>
															</div>
															<!-- /.table-responsive -->
															<div class="table-responsive">
																<table
																	class="table table-striped table-warning table-hover">
																	<thead>
																		<tr>
																			<th>Email</th>
																			<th>Document Generation</th>
																			<th>Documents</th>
																		</tr>
																	</thead>
																	<tbody>
																		<tr>
																			<% 
                                      if(txnDetail.getProductType().indexOf("Travel Ezy") !=-1)
	                                           
	                                       {
                                                         
	                                       	 term="TravelEzy"; %>
																			<td><a
																				href="TransactionalReport?action=sendEmail&docId=<%=txnDetail.getDSPQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Resend
																					Email</a></td>
																			<td><a
																				href="TransactionalReport?action=generateDoc&docId=<%=txnDetail.getDSPQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Regenerate
																					Document without Email</a></td>
																			<%  }
                                      else if (txnDetail.getProductType().indexOf("L") !=-1 )
 									    { 
                                            
                                                            
 										    term="TL";
 										 %>
																			<td><a
																				href="TransactionalReport?action=sendEmail&docId=<%=txnDetail.getDSPQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Resend
																					Email</a></td>
																			<td><a
																				href="TransactionalReport?action=generateDoc&docId=<%=txnDetail.getDSPQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Regenerate
																					Document without Email</a></td>
																			<% }
												
																					
										else if (txnDetail.getProductType().indexOf("e-Cancer") !=-1 )
 									    { 
                                            String product=txnDetail.getProductType();	
                                            if("e-CancerCare".equalsIgnoreCase(product)){
												term = "PCCA01";
											}else if("e-CancerCare Takaful".equalsIgnoreCase(product)){
												term = "PTCA01";
											}              
 										   
 										 %>
																			<td><a
																				href="TransactionalReport?action=sendEmail&docId=<%=txnDetail.getDSPQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Resend
																					Email</a></td>
																			<td><a
																				href="TransactionalReport?action=generateDoc&docId=<%=txnDetail.getDSPQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Regenerate
																					Document without Email</a></td>
																					
																					<% }
												
																					
										else if (txnDetail.getProductType().indexOf("Medical") !=-1 )
 									    { 
                                            String product=txnDetail.getProductType();	
                                            if("Medical Pass".equalsIgnoreCase(product)){
												term = "MP";
											}else if("Medical Pass Takaful".equalsIgnoreCase(product)){
												term = "MPT";
											}              
 										   
 										 %>
																			<td><a
																				href="TransactionalReport?action=sendEmail&docId=<%=txnDetail.getDSPQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Resend
																					Email</a></td>
																			<td><a
																				href="TransactionalReport?action=generateDoc&docId=<%=txnDetail.getDSPQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Regenerate
																					Document without Email</a></td>
																			<% }
																			
	                                      else if(txnDetail.getProductType().indexOf("TripCare") !=-1)
                                               
                                           {
                                           
                                           	 term="TripCare"; %>
																			<td><a
																				href="TransactionalReport?action=sendEmail&docId=<%=txnDetail.getDSPQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Resend
																					Email</a></td>
																			<td><a
																				href="TransactionalReport?action=generateDoc&docId=<%=txnDetail.getDSPQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Regenerate
																					Document without Email</a></td>
																			<%  }
	                                                         
                                                        
						 								    else if(txnDetail.getProductType().indexOf("Ez") !=-1) /* Added by ananth */						                                        
						                                    {
						                                    
						                                    	 term="EZYTL"; %>

																			<td><a
																				href="TransactionalReport?action=sendEmail&docId=<%=txnDetail.getDSPQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Resend
																					Email</a></td>
																			<td><a
																				href="TransactionalReport?action=generateDoc&docId=<%=txnDetail.getDSPQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Regenerate
																					Document without Email</a></td>

																			<% }
                                                            else if(txnDetail.getProductType().indexOf("D") !=-1) /* Added by pramaiyan */						                                        
						                                    {
						                                    
						                                    	 term="IDS"; %>

																			<td><a
																				href="TransactionalReport?action=sendEmail&docId=<%=txnDetail.getDSPQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Resend
																					Email</a></td>
																			<td><a
																				href="TransactionalReport?action=generateDoc&docId=<%=txnDetail.getDSPQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Regenerate
																					Document without Email</a></td>
																			<%  }
                                                         
                                                            else if(txnDetail.getProductType().indexOf("M") !=-1)
                                                            	                                                            
                                                            {
                                                            
                                                            	 term="MI"; %>

																			<td><a
																				href="TransactionalReport?action=sendEmail&docId=<%=txnDetail.getQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Resend
																					Email</a></td>
																			<td><a
																				href="TransactionalReport?action=generateDoc&docId=<%=txnDetail.getQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Regenerate
																					Document without Email</a></td>
																			<%  }
                                                             else if(txnDetail.getProductType().indexOf("N") !=-1)	                                                            
                                                            {
                                                            
                                                            	 term="NCI"; %>

																			<td><a
																				href="TransactionalReport?action=sendEmail&docId=<%=txnDetail.getQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Resend
																					Email</a></td>
																			<td><a
																				href="TransactionalReport?action=generateDoc&docId=<%=txnDetail.getQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Regenerate
																					Document without Email</a></td>
																			<%  }
                                      							else if(txnDetail.getProductType().indexOf("i") !=-1) /* Added by ananth */						                                        
	                                    						{
	                                    	 						term="ISCTL"; %>

																			<td><a
																				href="TransactionalReport?action=sendEmail&docId=<%=txnDetail.getDSPQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Resend
																					Email</a></td>
																			<td><a
																				href="TransactionalReport?action=generateDoc&docId=<%=txnDetail.getDSPQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Regenerate
																					Document without Email</a></td>
																			<%  }
                                                          else if(txnDetail.getProductType().indexOf("W") !=-1)
                                                            	                                                            
                                                            {
                                                            
                                                            	 term="W"; %>
																			<td><a
																				href="TransactionalReport?action=sendEmail&docId=<%=txnDetail.getDSPQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Resend
																					Email</a></td>
																			<td><a
																				href="TransactionalReport?action=generateDoc&docId=<%=txnDetail.getDSPQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Regenerate
																					Document without Email</a></td>
																			<%  }
                                                         
 // For HOHH Resend Email
                                                         
                                                          else if(txnDetail.getProductType().indexOf("H") !=-1)
                                                            	                                                            
                                                            {
                                                            
                                                            	 term="H"; %>
																			<td><a
																				href="TransactionalReport?action=sendEmail&docId=<%=txnDetail.getDSPQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Resend
																					Email</a></td>
																			<td><a
																				href="TransactionalReport?action=generateDoc&docId=<%=txnDetail.getDSPQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Regenerate
																					Document without Email</a></td>
																			<%  }
                                                         
                                                          else if(txnDetail.getProductType().indexOf("Buddy") !=-1)
	                                                            
                                                          {
                                                          	 term="Buddy"; %>
																			<td><a
																				href="TransactionalReport?action=sendEmail&docId=<%=txnDetail.getDSPQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Resend
																					Email</a></td>
																			<td><a
																				href="TransactionalReport?action=generateDoc&docId=<%=txnDetail.getDSPQQID()%>&id=${param.id}&policyNo=<%=txnDetail.getPolicyNo()%>&term=<%=term%>&simpleNote=<%=txnDetail.getSimplifiedResult()%>">Regenerate
																					Document without Email</a></td>
																			<%  }
                                                         
                                                         
                                                          if(request.getAttribute("statusEm")!= null){
                                                        	  if(request.getAttribute("statusEm").equals("Fail")){
                                                        		 %>
																			<br>Email Sending Failed
																			<% }
                                                        	  else{
                                                        		  %><br>
																			Email was successful sent to Policy Owner
																			<% }
                                                         	  }
                                                            %>
																			<% 
                                                              if(request.getAttribute("statusDoc")!= null){
                                                        	  if(request.getAttribute("statusDoc").equals("Fail")){ 
                                                        		 %>
																			<br>Document Generation Failed
																			<% }
                                                        	  else{
                                                        	    %><br>
																			Document Generated Successfully
																			<% }
                                                         	  }
                                                            %>


																			<td>
																				<!--  start: download document--> <% 
 									System.out.println(txnDetail.getProductType()+" pramaiyan Product desc");
 									if (txnDetail.getProductType().indexOf("Travel Ezy") !=-1){
 										if (txnDetail.getProductType().indexOf("i") != -1) {
 	  	 								%> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-CertificateForm"
																				target="_blank" id="EPolicyInfoForm">Download
																					ECertificate Info Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-TaxInvoice"
																				target="_blank" id="taxinvoice">Download Tax
																					Invoice Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-PDSForm"
																				target="_blank" id="PDSForm">Download PDS Form</a> <br />
																				<a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-CertificateContract"
																				target="_blank" id="EPolicy">Download
																					Certificate Form</a> <br /> <%} else { %> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-PolicyForm"
																				target="_blank" id="EPolicyInfoForm">Download
																					EPolicy Info Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-TaxInvoice"
																				target="_blank" id="taxinvoice">Download Tax
																					Invoice Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-PDSForm"
																				target="_blank" id="PDSForm">Download PDS Form</a> <br />
																				<a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-PolicyContract"
																				target="_blank" id="EPolicy">Download Policy
																					Form</a> <br /> <%  }
 									}
 									else if (txnDetail.getProductType().indexOf("TripCare") !=-1){	 									 
 	  	 								if (txnDetail.getProductType().indexOf("Takaful") !=-1){	%>

																				<a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-CertificateForm"
																				target="_blank" id="EPolicyInfoForm">Download
																					ECertificate Info Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-TaxInvoice"
																				target="_blank" id="taxinvoice">Download Tax
																					Invoice Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-PDSForm"
																				target="_blank" id="PDSForm">Download PDS Form</a> <br />
																				<a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-CertificateContract"
																				target="_blank" id="EPolicy">Download
																					Certificate Form</a> <br /> <% } else { %> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-PolicyForm"
																				target="_blank" id="EPolicyInfoForm">Download
																					EPolicy Info Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-TaxInvoice"
																				target="_blank" id="taxinvoice">Download Tax
																					Invoice Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-PDSForm"
																				target="_blank" id="PDSForm">Download PDS Form</a> <br />
																				<a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-PolicyContract"
																				target="_blank" id="EPolicy">Download Policy
																					Form</a> <br /> <%  }
 									}
 									 else if (txnDetail.getProductType().indexOf("L") !=-1 ||txnDetail.getProductType().indexOf("Ez") !=-1)
                                                            { %> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-Policy"
																				target="_blank" id="ePolicyLink">Download
																					ePolicy</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-Receipt"
																				target="_blank" id="eReceiptLink">Download
																					eReceipt</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-ApplicationForm"
																				target="_blank" id="eApplicationFormLink">Download
																					eApplication Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-PolicyInformationPage"
																				target="_blank" id="ePolicyInformationPageLink">Download
																					ePolicy Information Page (PIP)</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-SalesIllustration"
																				target="_blank" id="eSalesIllustrationPageLink">Download
																					eSales Illustration (SI)</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-ProductDisclosureSheet"
																				target="_blank" id="eProductDisclosureSheetPageLink">Download
																					eProduct Disclosure Sheet (PDS)</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-NominationForm"
																				target="_blank" id="eNominationFormLink">Download
																					eNomination Form</a> <br /> <% }
 									 /* Added for IDS  by pramaiyan*/
 									 			//else  if (txnDetail.getProductType().indexOf("IDS") !=-1 || txnDetail.getProductType().indexOf("ISCTL") !=-1 || txnDetail.getProductType().indexOf("secure") != -1)
 									 			else  if (txnDetail.getProductType().indexOf("D") !=-1 || txnDetail.getProductType().indexOf("i") !=-1)
                                            { %> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-Certificate"
																				target="_blank" id="ePolicyLink">Download
																					eCertificate</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-Receipt"
																				target="_blank" id="eReceiptLink">Download
																					eReceipt</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-ApplicationForm"
																				target="_blank" id="eApplicationFormLink">Download
																					eApplication Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-TakafulSchedule"
																				target="_blank" id="ePolicyInformationPageLink">Download
																					eTakaful Schedule</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-MarketingIllustration"
																				target="_blank" id="eMarketingIllustrationPageLink">Download
																					eMarketing Illustration (MI)</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-ProductDisclosureSheet"
																				target="_blank" id="eProductDisclosureSheetPageLink">Download
																					eProduct Disclosure Sheet (PDS)</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-NominationForm"
																				target="_blank" id="eNominationFormLink">Download
																					eNomination Form</a> <br /> 
																					
																					
																					<%  }
 									
 									 else if (txnDetail.getProductType().indexOf("e-Cancer") !=-1 )
                                                            { %> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=contract"
																				target="_blank" id="ePolicyLink">Download
																					ePolicy</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=claims-guide"
																				target="_blank" id="eclaimsLink">Download
																					eClaimsGuide</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-Receipt"
																				target="_blank" id="eReceiptLink">Download
																					eReceipt</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-ApplicationForm"
																				target="_blank" id="eApplicationFormLink">Download
																					eApplication Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-PolicyInformationPage"
																				target="_blank" id="ePolicyInformationPageLink">Download
																					ePolicy Information Page (PIP)</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-SalesIllustration"
																				target="_blank" id="eSalesIllustrationPageLink">Download
																					eSales Illustration (SI)</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-ProductDisclosureSheet"
																				target="_blank" id="eProductDisclosureSheetPageLink">Download
																					eProduct Disclosure Sheet (PDS)</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=nomination"
																				target="_blank" id="eNominationFormLink">Download
																					eNomination Form</a> <br /> <% }
 								
																					 
															else if (txnDetail.getProductType().equalsIgnoreCase("Medical Pass"))
                                                            { %> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=Contract"
																				target="_blank" id="ePolicyLink">Download
																					ePolicy</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=Claims-Guide"
																				target="_blank" id="eclaimsLink">Download
																					eClaimsGuide</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-Receipt"
																				target="_blank" id="eReceiptLink">Download
																					eReceipt</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=EAppForm"
																				target="_blank" id="eApplicationFormLink">Download
																					eApplication Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-PolicyInformationPage"
																				target="_blank" id="ePolicyInformationPageLink">Download
																					ePolicy Information Page (PIP)</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-SalesIllustration"
																				target="_blank" id="eSalesIllustrationPageLink">Download
																					eSales Illustration (SI)</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-ProductDisclosureSheet"
																				target="_blank" id="eProductDisclosureSheetPageLink">Download
																					eProduct Disclosure Sheet (PDS)</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=Nomination"
																				target="_blank" id="eNominationFormLink">Download
																					eNomination Form</a> <br /> <% }
 								
															else if (txnDetail.getProductType().equalsIgnoreCase("Medical Pass Takaful") )
                                                            { %> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=Certificate"
																				target="_blank" id="ePolicyLink">Download
																					ePolicy</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=Claims-Guide"
																				target="_blank" id="eclaimsLink">Download
																					eClaimsGuide</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-Receipt"
																				target="_blank" id="eReceiptLink">Download
																					eReceipt</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=EAppForm"
																				target="_blank" id="eApplicationFormLink">Download
																					eApplication Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-TakafulSchedule"
																				target="_blank" id="ePolicyInformationPageLink">Download
																					ePolicy Information Page (PIP)</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-MarketingIllustration"
																				target="_blank" id="eSalesIllustrationPageLink">Download
																					eSales Illustration (SI)</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=e-ProductDisclosureSheet"
																				target="_blank" id="eProductDisclosureSheetPageLink">Download
																					eProduct Disclosure Sheet (PDS)</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%><%=txnDetail.getTLCheckDigit()%>&term=Nomination"
																				target="_blank" id="eNominationFormLink">Download
																					eNomination Form</a> <br /> <% }
 								
																					
																					
 									 			else if (txnDetail.getProductType().equalsIgnoreCase("Motor Insurance")){
 									 
 									 %> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=AutoAssistForm"
																				target="_blank" id="AutoAssistForm">Download
																					Auto Assist Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=EPolicyForm"
																				target="_blank" id="EPolicyForm">Download
																					e-Policy/e-Certificate Form</a> <br /> <%-- <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=PDPAForm"
																				target="_blank" id="PDPAForm">Download PDPA Form</a>
																				<br />  --%><a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=PDSMotorForm"
																				target="_blank" id="PDSMotorForm">Download PDS
																					Motor Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=TaxInvoiceForm"
																				target="_blank" id="TaxInvoiceForm">Download Tax
																					Invoice Form</a> <br />
																					<% if(!txnDetail.getEhailStatus().equals("0")){  %><a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=EHailingForm"
																				target="_blank" id="EHailingForm">Download EHailing
																					 Form</a><br /><%} %> <% if(txnDetail.getCAPSPolicyNo() !=null &&txnDetail.getCAPSPolicyNo() !="" ){
if(txnDetail.getPassengerPASelected() !=null && txnDetail.getPassengerPASelected() !="" ){
if(txnDetail.getPassengerPASelected().equals("on")){
 %> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=PDSMotor(CAPS)Form"
																				target="_blank" id="PDSMotor(CAPS)Form">Download
																					PDSMotor(CAPS) Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=EPolicy(CAPS)Form"
																				target="_blank" id="EPolicy(CAPS)Form">Download
																					e-Policy(CAPS)or e-Certificate(DPPA)</a> <br /> <% }
 		 else if(txnDetail.getPassengerPASelected().equals("O-1") || txnDetail.getPassengerPASelected().equals("O-2") || txnDetail.getPassengerPASelected().equals("O-3")){
 		 
 					 %> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=PDSMotor(OTO360)Form"
																				target="_blank" id="PDSMotor(OTO360)Form">Download
																					PDSMotor(OTO360) Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=EPolicy(OTO360)Form"
																				target="_blank" id="EPolicy(OTO360)Form">Download
																					EPolicy(OTO360)Form</a> <br /> <%		 } 
 					  }// this is modified braces
 									 }
}
 									 else if (txnDetail.getProductType().equalsIgnoreCase("Motor Takaful")){
 	 									 
 	 									 %> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=AutoAssistForm"
																				target="_blank" id="AutoAssistForm">Download
																					Auto Assist Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=ECertificateForm"
																				target="_blank" id="ECertificateForm">Download
																					e-Policy/e-Certificate Form</a> <br /> <%-- <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=PDPAForm"
																				target="_blank" id="PDPAForm">Download PDPA Form</a>
																				<br /> --%> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=PDSMotorForm"
																				target="_blank" id="PDSMotorForm">Download PDS
																					Motor Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=TaxInvoiceForm"
																				target="_blank" id="TaxInvoiceForm">Download Tax
																					Invoice Form</a> <br /> 
																					<% if(!txnDetail.getEhailStatus().equals("0")){  %> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=EHailingForm"
																				target="_blank" id="EHailingForm">Download EHailing
																					 Form</a> <br /><%} %><% if(txnDetail.getCAPSPolicyNo() !=null &&txnDetail.getCAPSPolicyNo() !="" ){
 	if(txnDetail.getPassengerPASelected() !=null &&txnDetail.getPassengerPASelected() !="" ){
if(txnDetail.getPassengerPASelected().equals("on")){
 	 %> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=PDSMotor(DPPA)Form"
																				target="_blank" id="PDSMotor(DPPA)Form">Download
																					PDSMotor(DPPA) Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=ECertificate(DPPA)Form"
																				target="_blank" id="ECertificate(DPPA)Form">Download
																					e-Certificate(DPPA)</a> <br /> <%  }else if(txnDetail.getPassengerPASelected().equals("O-1") || txnDetail.getPassengerPASelected().equals("O-2") || txnDetail.getPassengerPASelected().equals("O-3")){
 	 									 %> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=PDSMotor(OTO360)Form"
																				target="_blank" id="PDSMotor(OTO360)Form">Download
																					PDSMotor(OTO360) Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=ECertificate(OTO360)Form"
																				target="_blank" id="ECertificate(OTO360)Form">Download
																					ECertificate(OTO360) Form</a> <br /> <% 
 	 									 }
 	 									 }}
 									 }
                                     
 									else if (txnDetail.getProductType().equalsIgnoreCase("New Car Insurance")){
	 									 
	 									 %> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=AutoAssistForm"
																				target="_blank" id="AutoAssistForm">Download
																					Auto Assist Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=EPolicyForm"
																				target="_blank" id="EPolicyForm">Download
																					e-Policy/e-Certificate Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=PDPAForm"
																				target="_blank" id="PDPAForm">Download PDPA Form</a>
																				<br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=PDSMotorForm"
																				target="_blank" id="PDSMotorForm">Download PDS
																					Motor Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=TaxInvoiceForm"
																				target="_blank" id="TaxInvoiceForm">Download Tax
																					Invoice Form</a> <br /> <% if(txnDetail.getCAPSPolicyNo() !=null &&txnDetail.getCAPSPolicyNo() !="" ){ %>

																				<a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=PDSMotor(CAPS)Form"
																				target="_blank" id="PDSMotor(CAPS)Form">Download
																					PDSMotor(CAPS) Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=EPolicy(CAPS)Form"
																				target="_blank" id="EPolicy(CAPS)Form">Download
																					e-Policy(CAPS)or e-Certificate(DPPA)</a> <br /> <%  }
	}
	 									 else if (txnDetail.getProductType().equalsIgnoreCase("New Car Insurance Takaful")){
	 	 									 
	 	 									 %> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=AutoAssistForm"
																				target="_blank" id="AutoAssistForm">Download
																					Auto Assist Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=ECertificateForm"
																				target="_blank" id="ECertificateForm">Download
																					e-Policy/e-Certificate Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=PDPAForm"
																				target="_blank" id="PDPAForm">Download PDPA Form</a>
																				<br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=PDSMotorForm"
																				target="_blank" id="PDSMotorForm">Download PDS
																					Motor Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=TaxInvoiceForm"
																				target="_blank" id="TaxInvoiceForm">Download Tax
																					Invoice Form</a> <br /> <% if(txnDetail.getCAPSPolicyNo() !=null &&txnDetail.getCAPSPolicyNo() !="" ){ %>

																				<a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=PDSMotor(DPPA)Form"
																				target="_blank" id="PDSMotor(DPPA)Form">Download
																					PDSMotor(DPPA) Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=ECertificate(DPPA)Form"
																				target="_blank" id="ECertificate(DPPA)Form">Download
																					e-Certificate(DPPA)</a> <br /> <%  }
	 									 }




 									 else if (txnDetail.getProductType().indexOf("H") !=-1){
 	 									 %> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=EHOHH_PDPA"
																				target="_blank" id="PDPAForm">Download PDPA Form</a>

																				<br> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=EHOHH-PDS"
																				target="_blank" id="PDSForm">Download PDS Form</a> <br />

																				<a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=ETaxInvoice"
																				target="_blank" id="taxinvoice">Download Tax
																					Invoice Form</a> <br> <% if (txnDetail.getPolicyNo().indexOf("FH") !=-1){%>


																				<a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=EPolicy"
																				target="_blank" id="EPolicy">Download Policy
																					Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=EPolicy-Contract"
																				target="_blank" id="EPolicyContract">Download
																					Policy Contract Form</a> <br /> <% 
 	 									}
 	 									
 	 									else if (txnDetail.getPolicyNo().indexOf("RH") !=-1){%> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=ETakafulCertificate"
																				target="_blank" id="ETakafulCertificate">Download
																					Takaful Certificate</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=ETakaful-Certificate-Contract"
																				target="_blank" id="ETakafulCertificateContract">Download
																					Takaful Certificate Contract</a> <br /> <% 	}
 	 									}
                                   
 									 
 									 else if (txnDetail.getProductType().indexOf("W") !=-1){
 	 									 
  	 								%> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-PolicyForm"
																				target="_blank" id="EPolicyInfoForm">Download
																					EPolicy Info Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-Taxinvoice"
																				target="_blank" id="taxinvoice">Download Tax
																					Invoice Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-PDSForm"
																				target="_blank" id="PDSForm">Download PDS Form</a> <br />

																				<a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-PolicyContract"
																				target="_blank" id="EPolicy">Download Policy
																					Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-PDPAForm"
																				target="_blank" id="PDPAForm">Download PDPA Form</a>


																				<br /> <!-- 	<a href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-NominationForm"			
												target="_blank" id="NominationForm">Download Nomination Form</a>											
												
												
								<br />	 
 									 --> <%  }
 									 else if (txnDetail.getProductType().indexOf("Buddy") != -1){
 										 if (txnDetail.getProductType().indexOf("Takaful") != -1) {
 									 %> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-CertificateForm"
																				target="_blank" id="EPolicyInfoForm">Download
																					ECertificate Info Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-TaxInvoice"
																				target="_blank" id="taxinvoice">Download Tax
																					Invoice Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-PDSForm"
																				target="_blank" id="PDSForm">Download PDS Form</a> <br />
																				<a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-CertificateContract"
																				target="_blank" id="EPolicy">Download
																					Certificate Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-PDPAForm"
																				target="_blank" id="PDPAForm">Download PDPA Form</a>
																				<br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-NominationForm"
																				target="_blank" id="NominationForm">Download
																					Nomination Form</a> <br /> <%} else { %> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-PolicyForm"
																				target="_blank" id="EPolicyInfoForm">Download
																					EPolicy Info Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-TaxInvoice"
																				target="_blank" id="taxinvoice">Download Tax
																					Invoice Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-PDSForm"
																				target="_blank" id="PDSForm">Download PDS Form</a> <br />
																				<a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-PolicyContract"
																				target="_blank" id="EPolicy">Download Policy
																					Form</a> <br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-PDPAForm"
																				target="_blank" id="PDPAForm">Download PDPA Form</a>
																				<br /> <a
																				href="downloadFile?policyNo=<%=txnDetail.getPolicyNo()%>&term=e-NominationForm"
																				target="_blank" id="NominationForm">Download
																					Nomination Form</a> <br /> <%} 
 									}%>

																			</td>
																		</tr>
																	</tbody>
																</table>
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>


												<%} %>
											</div>
										</div>
									</div>
									<!--row -->

									<%
														                }
														                }
														            
														        %>




								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->

	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->

	<!-- start: modal -->
	<div class="modal fade" id="policyDetail" tabindex="-1" role="dialog"
		aria-labelledby="reportDetaillLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="reportDetailLabel">Transaction
						Record :</h4>
				</div>
				<div class="modal-body">

					<div id="result"></div>
					<div class="box-body">

						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th colspan="4">Coverage Information</th>
								</tr>
								<tr>
									<td colspan="2">Period Of Insurance</td>
									<td colspan="2" id="coveragePeriod"></td>
								</tr>
								<tr>
									<td colspan="2">Premium</td>
									<td colspan="2" id="premium"></td>
								</tr>
								<tr>
									<td>Estimated Value</td>
									<td id=""></td>
									<td id=""></td>
									<td id=""></td>
								</tr>
								<tr>
									<td>Cubic Capacity (CC)</td>
									<td id="capacity"></td>
								</tr>
								<tr>
									<td>Non-Claim-Discount (NCD)</td>
									<td id="transactionDateTime"></td>
								</tr>
								<tr>
									<td>Previous Hire Purchase</td>
									<td id="transactionStatus"></td>
								</tr>
								<tr>
									<td>Settlement Status</td>
									<td id="settlementStatus"></td>
								</tr>
								<tr>
									<td>Settlement Date</td>
									<td id="settlementDate"></td>
								</tr>
								<tr>
									<td>Transaction Type</td>
									<td id="transactionType"></td>
								</tr>
								<tr>
									<td>Payment Channel</td>
									<td id="paymentChannel"></td>
								</tr>
								<tr>
									<td>Payment Method</td>
									<td id="paymentMethod"></td>
								</tr>
								<tr>
									<td>Response Code</td>
									<td id="responseCode"></td>
								</tr>
								<tr>
									<td>Authorization Code</td>
									<td id="authorizationCode"></td>
								</tr>
								<tr>
									<td>Error Code</td>
									<td id="errorCode"></td>
								</tr>
								<tr>
									<td>Error Message</td>
									<td id="errorMessage"></td>
								</tr>
								<tr>
									<td>Response Message</td>
									<td id="responseMessage"></td>
								</tr>
								<tr>
									<th colspan="2">Customer Information</th>
								</tr>

								<tr>
									<td>Name</td>
									<td id="name"></td>
								</tr>
								<tr>
									<td>Phone</td>
									<td id="Phone"></td>
								</tr>
								<tr>
									<td>Email</td>
									<td id="Email"></td>
								</tr>
								<tr>
									<td>Beneficiary Account No</td>
									<td id="accountNo"></td>
								</tr>
								<tr>
									<td>Card No</td>
									<td id="cardNo"></td>
								</tr>
								<tr>
									<td>Card Type</td>
									<td id="cardType"></td>
								</tr>
								<tr>
									<td>Beneficiary Name</td>
									<td id="beneficiaryName"></td>
								</tr>
								<tr>
									<td>Beneficiary Bank</td>
									<td id="beneficiaryBank"></td>
								</tr>
								<tr>
									<td>Beneficiary Account No</td>
									<td id="accountNo"></td>
								</tr>
								<tr>
									<th colspan="2">Merchant Information</th>
								</tr>
								<tr>
									<td>Merchant</td>
									<td id="Merchant"></td>
								</tr>

								<tr>
									<td>Merchant ID (MID)</td>
									<td id="MID"></td>
								</tr>
								<tr>
									<td>Terminal ID (TID)</td>
									<td id="TID"></td>
								</tr>
								<tr>
							</thead>


						</table>
					</div>
					<!-- /.box-body -->



				</div>
				<div class="modal-footer">
					<!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</div>
	</div>


	<!-- end: modal -->








	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->

	<script type="text/javascript">
         
         
      	$(document).ready(function() {
      		
      		$('#editCustInfoForm').hide();
      		$('#cancelTxnInfo').hide();
      		$('#saveTxnInfo').hide();
      		
      		$('#editCustInfo').click(function() {
      			
      				$('#showCustInfo').hide();
      				$('#editCustInfo').hide();
      				$('#editCustInfoForm').show();
	
      		});
      		
      		$('#cancelCustInfo').click(function() {
      			
  				$('#showCustInfo').show();
  				$('#editCustInfo').show();
  				$('#editCustInfoForm').hide();

  		});

      		$('#editTxnInfo').click(function() {
      			
  				$('#editTxnStatus').removeAttr('disabled');
  				$('#editRemark').removeAttr('disabled');
  				$('#cancelTxnInfo').show();
  				$('#saveTxnInfo').show();
  				$('#editTxnInfo').hide();
	

  		});
  		
        $('#cancelTxnInfo').click(function() {
	
	    $('#editTxnStatus').attr('disabled', 'disabled');
	    $('#editRemark').attr('disabled', 'disabled');
        $('#cancelTxnInfo').hide();
        $('#saveTxnInfo').hide();
        $('#editTxnInfo').show();
  		});
  		
      		
      	
      	

      		
      	});
      
      	
      	
      
      </script>

	<script>
        function validateForm() {
        	   var x = document.forms["editTxnInfoForm"]["remark"].value;  
        	   
        	   alert(x);
        	  /* if ( x == null || x == "" ) {
        		   
        	      alert("Please insert Remarks");
        	      return false;
        	   
        		  
        	   }
        	   return true;*/
        	  
        	}
        
     
        $('#policyDetail').on('show.bs.modal', function (event) {
        	 var modal = $(this);
        	    
        	  var button = $(event.relatedTarget) // Button that triggered the modal
        	  var QQID = button.data('value') // Extract info from data-* attributes
        	  console.log(QQID);	
        	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        	   $.ajax({  
      		     type : "Get",   
      		     url : "TransactionalReport", //see in Report.java   
      		   data: {
                   action: 'policyDetail',
                   qqid: QQID
                 },
        	   
      		        //dataType: "json", //set to JSON   
      		         success : function(response) {    		    	 
      		    	 console.log(response);	
      		    	 var obj = JSON.stringify(response);
      		         var result = $.parseJSON(obj);
      		       console.log("result: "+result);
      		     //alert(result);
      		 
      		      
             		  $.each(result, function() {
             			  
             			  //alert(result.status);
             			  
             			  
             		 
             		  });        	  
             	  
      		 
      		 
      		 
      		 
      		 
      		 
      		 /* $.each(result, function() {

      			 modal.find('.modal-title').text("Transaction Record : "+this['policyNo'] );
      	    	  //modal.find('.modal-body input').val(reportID);
      	    	   /*$('#mpTransactionID').html(this['mptransactionid']);
      	    	 $('#orderDetail').html(this['orderdetail']);
      	    	 $('#transactionDateTime').html( new Date(this['transactiondatetime']));
      	    	 
      	    	$.datepicker.formatDate('MM d, yy', new Date(parseInt('/Date(1224043200000)/'.substr(6))));
      	    	
      	    	 $('#transactionStatus').html(this['transactionstatustext']); 
      	    	 $('#settlementStatus').html(this['settlementstatus']); 
      	    	$('#settlementDate').html(this['settlementdate']); 
      	    	if(this['cardno1'] == null && this['cardno2'] == null && this['cardno3'] == null ){
      	    		$('#cardNo').html("-"); 	
      	    	}
      	    	else{
      	    		$('#cardNo').html(this['cardno1']+this['cardno3']+this['cardno2']); 	
      	    	}
      	    	if(this['cardtype'] == null ){
      	    		$('#cardType').html("-"); 	
      	    	}
      	    	
      	    	
      	    	if(this['lastname'] == null){
      	    	$('#name').html(this['firstname']);
      	    	}else{
      	    		$('#name').html(this['firstname']+" "+this['lastname']);	
      	    	}
      	    	
      	    	
      	    	$('#Amount').html(this['currencytext']+" "+this['amount']);
      	    	$('#paymentMethod').html(this['paymentmethodtext']); 
      	    	
      	    	$('#beneficiaryBank').html(this['beneficiarybanktext']); 
      	    	$('#beneficiaryName').html(this['beneficiaryname']); 
      	    	$('#accountNo').html(this['account']); 
      	    	$('#Phone').html(this['phone']); 
      	    	$('#Email').html(this['email']); 
      	    	
      	    	$('#Merchant').html(this['name']);
      	    	 $('#MID').html(this['mid']);
      	    	 $('#TID').html(this['tid']);
      	    	
      		  });*/
    	    	
      		     },  
      		     error : function(e) {  
      		      alert('Error: ' + e);   
      		     }  
      		    });  	
        	  
      
        	 
        	
        	 
        	})
        	
        
        
        </script>


</body>
</html>