<%@ page import="java.io.*,java.util.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link href="assets/css/bootstrap.min.css" rel="stylesheet">

<!-- PLUGINS CSS -->
<link href="assets/plugins/owl-carousel/owl.carousel.min.css"
	rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.theme.min.css"
	rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.transitions.min.css"
	rel="stylesheet">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
<!--<link href="assets/css/test.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="assets/assets/libs/html5shiv.js"></script>
        <script src="assets/assets/libs/respond.min.js"></script>
        <![endif]-->
</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper dashboard">
		<!-- header second-->
		<div class="header-admin">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h2>Cyber Agent</h2>
					</div>
				</div>
			</div>
		</div>
		<!-- header second-->


		<!-- BEGIN PAGE CONTENT -->
		<div class="page-content">
			<div class="container">
				<!-- /.container-->
				<div class="row">
					<div class="col-sm-offset-1 col-sm-9">
						<div class="panel panel-default login">
							<div class="panel-heading">
								<h4>Etiqa Agent Login</h4>
							</div>
							<div class="panel-body">
								<div class="col-sm-6">
									<div class="text-center">
										<img width="115" src="assets/images/lock.png" alt="logo">
										<div class="strong">
											<h5>Use a valid username and password to gain access to
												the Agent Back-end.</h5>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<form:form modelAttribute="agentProfile"
										action="agentLoginDone" method="post">
										<div class="panel panel-default">
											<div class="panel-body">
												<div class="col-sm-12">
													<div class="logo form-group">
														<a href="index.html"><img width="80"
															src="assets/images/logo.png" alt="logo"></a>
													</div>
													<div class="col-xs-12">
														<c:if test="${(not empty  login_error_message)}">
															<div style="color: red">
																<c:out value="${login_error_message}" />
															</div>
														</c:if>
													</div>


												</div>

												<div class="col-xs-12">
													<div
														class="form-group has-feedback lg left-feedback no-label">
														<spring:bind path="agentProfile.agentUsername">
															<input type="text" class="form-control no-border rounded"
																name="${status.expression}" id="${status.expression}"
																value="${status.value}" placeholder="Username" autofocus>
														</spring:bind>
														<span class="fa fa-user form-control-feedback fa-lg"></span>
													</div>
												</div>
												<div class="col-xs-12">
													<div
														class="form-group has-feedback lg left-feedback no-label">
														<spring:bind path="agentProfile.password">
															<input type="password"
																class="form-control no-border rounded"
																autocomplete="off" name="${status.expression}"
																id="${status.expression}" value=""
																placeholder="Password">
														</spring:bind>
														<span class="fa fa-unlock-alt form-control-feedback fa-lg"></span>
													</div>
												</div>
												<div class="col-xs-12">
													<div class="text-right">
														<input type="submit" class="btn btn-warning" value="Login">
													</div>
													<div class="text-right">
														<a href="forgetPassword">Forget Password</a>
													</div>
												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</div>
						<!--
                            <div class="the-box">
                                <div>Etiqa Customer Web Portal! Administration Login</div>
                                <div class="col-sm-6">asdas</div>
                                <div class="col-sm-6">
                                    <div class="the-box">asdasd</div>
                                </div>
                            </div>
-->
					</div>
				</div>
				<!-- /.row -->
			</div>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->

	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->

	<script>
        var agentAddedMessage="<c:out value="${setpw_success}"/>";
	       // show when the button is clicked
	       if (agentAddedMessage.length) {
	    	  toastr.success(agentAddedMessage);
	       }
        </script>

</body>
</html>