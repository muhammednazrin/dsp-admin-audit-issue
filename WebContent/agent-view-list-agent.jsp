<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="com.etiqa.model.agent.Agent"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/js/jquery.dataTables.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/buttons.dataTables.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/dataTables.bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/buttons.bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link rel="stylesheet" href="assets/css/toastr.min.css">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}
</style>


<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}
</style>
<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet"
	href="assets/plugins/datepicker/datepicker.min.css">
<!-- PLUGINS CSS -->
<link href="assets/plugins/owl-carousel/owl.carousel.min.css"
	rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.theme.min.css"
	rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.transitions.min.css"
	rel="stylesheet">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />

<link rel="stylesheet"
	href="assets/jAlert/jAlert.css">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

form#agentE, form#agentD {
	display: inline-block;
}

form#agentE input[type=submit], form#agentD input[type=submit] {
	padding: 5px;
}

#agentD {
	display: inline-block;
}

#agentD input[type=submit] {
	padding: 5px;
}

.btn-sm {
	padding: 5px;
}
</style>
</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Agent Management</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>List of Agent</h4>
														</div>
													</div>


													<!-- Start Form -->
													<form:form name="agentList" action="searchAgent"
														id="agentList" method="post"
														onsubmit="return validateForm()">

														<!--   <input type="hidden" name="action" value="listAll" /> -->

														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Registration
																				Date</label>
																			<div class="col-sm-4">
																				<input type="text" placeholder="Date From"
																					class="form-control" name="dateFrom" id="date1"
																					value="<c:out value="${sessionScope.dateFrom}" />" />
																			</div>
																			<div class="col-sm-5">
																				<input type="text" placeholder="Date To"
																					class="form-control" name="dateTo" id="date2"
																					value="<c:out value="${sessionScope.dateTo}" />" />
																			</div>
																		</div>




																		<div class="form-group">
																			<label class="col-sm-3 control-label">Category</label>
																			<div class="col-sm-9">

																				<select class="form-control" name="agentCategory"
																					id="agentCategory">
																					<option value="" selected>-View All-</option>
																					<c:forEach items="${agentCategoryList}"
																						var="element">
																						<option
																							value="<c:out value="${element.paramCode}"/>">
																							<c:out value="${element.paramName}" />
																						</option>
																					</c:forEach>
																				</select>


																			</div>
																		</div>
																		<%-- <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Insurance Type</label>
                                                                    <div class="col-sm-9">
                                                                        <select class="form-control" name="insuranceType" id="insuranceType" >
                                                                        <option value="" selected>-View All-</option>
                                                                            
                                                                              <option
																						<c:if test="${sessionScope.insuranceType == 'Insurance'}" >selected</c:if>
																						value="Insurance">Insurance</option>
																					<option
																						<c:if test="${sessionScope.insuranceType == 'Takaful'}" >selected</c:if>
																						value="Takaful">Takaful</option>
                                                                        </select>
                                                                    </div>
                                                                  </div> --%>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Agent
																				Name</label>
																			<div class="col-sm-9">
																				<input type="text" name="agentName" maxlength="50"
																					placeholder="" class="form-control"
																					value="<c:out value="${sessionScope.agentName}" />" />
																			</div>
																		</div>




																		<%--  <div class="form-group">
                                                                    <label class="col-sm-3 control-label">Agent Code</label>
                                                                    <div class="col-sm-9">
                                                                        <input type="text" name="agentCode" maxlength="30" placeholder="" class="form-control" value="${sessionScope.agentCode}">
                                                                    </div>
                                                                  </div> --%>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Status</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="status">
																					<option value="" selected>-View All-</option>
																					<c:forEach items="${agentStatusList}" var="element">
																						<option
																							value="<c:out value="${element.paramCode}"/>">
																							<c:out value="${element.paramName}" />
																						</option>
																					</c:forEach>
																				</select>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col-sm-12 text-right">
																	<button type="submit" class="btn btn-default">Search</button>
																</div>
															</div>
														</div>
													</form:form>
													<!-- END FORM -->
												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="row">
																<div class="col-xs-12 form-inline">
																	<div class="col-xs-6">
																		<div class="row"></div>
																	</div>

																</div>
															</div>
															<!--  <button id="exportButton" class="btn btn-lg btn-danger clearfix"><span class="fa fa-file-excel-o"></span> Export to Excel</button>-->
															<div class="table-responsive" id="MyTable">
																<table
																	class="table table-striped table-warning table-hover"
																	id="admin-datatable-second_admin">
																	<thead>
																		<tr>
																			<th>No</th>
																			<th>Agent Name</th>
																			<th>Agent Phone</th>
																			<th>Agent Email</th>
																			<th>Agent Type</th>
																			<th>Agent Category</th>
																			<th>Branch Code</th>
																			<th>Maya Agent</th>
																			<th>Action</th>
																		</tr>
																	</thead>
																	<tbody>

																		<c:forEach items="${agentProfileList}" var="element"
																			varStatus="theCount">
																			<tr>
																				<td><c:out value="${theCount.count}" /></td>
																				<td><c:out value="${element.agentName}" /></td>
																				<td><c:out value="${element.phoneNumber}" /></td>
																				<td><c:out value="${element.email}" /></td>

																				<td><c:forEach items="${agentTypeList}"
																						var="item">
																						<c:if
																							test="${element.agentType eq item.paramCode}">
																							<c:out value="${item.paramName}" />
																						</c:if>
																					</c:forEach></td>

																				<td><c:forEach items="${agentCategoryList}"
																						var="item">
																						<c:if
																							test="${element.agentCategory eq item.paramCode}">
																							<c:out value="${item.paramName}" />
																						</c:if>
																					</c:forEach></td>
																				<td><c:out value="${element.branchCode}" /></td>
																				<td><c:if test="${element.isMayaAgent eq 'y'}">
																						<c:out value="Yes" />
																					</c:if> <c:if test="${element.isMayaAgent eq 'n'}">
																						<c:out value="No" />
																					</c:if></td>
																				<td><%
																				if(session!=null){
																				String jobRole=(String)session.getAttribute( "jobRolesession" );
																						if(jobRole.equals("1") || jobRole.equals("3")){
																							%>
																							<form:form name="agentE" id="agentE"
																							action="updateAgent" method="POST">
																							<!--  <input type="hidden" name="action" value="record" /> -->
																							<input type="hidden" name="id"
																								value="<c:out value="${element.id}"/>" />
																							<input class="btn btn-warning btn-sm"
																								type="submit" name="submit" value="Edit">
																						</form:form>
																							<% 
																						}
																						}%>
																					<!-- Pramain Start  --> <!-- Pramain end --> <!-- Pramain Start  -->
																				<%
																				if(session!=null){
																				String jobRole=(String)session.getAttribute( "jobRolesession" );
																						if(jobRole.equals("1") || jobRole.equals("4")){
																							%>
																						<input type="hidden" name="id"
																							value="<c:out value="${element.id}"/>" />
																						<input class="btn btn-warning btn-sm" id="agentD"
																							type="submit" name="submit"
																							onClick="deleteAgent('<c:out value="${element.id}"/>');"
																							value="Delete">
																							<% 
																						}
																						}%>	
																					<!-- Pramain end --> <%-- <a class="btn btn-warning btn-sm" 
														              href="agent?action=update&id=<%=agent.getId()%>">
														              <i class="fa fa-edit"></i>
														               Edit
														               </a> --%>
																				</td>
																				<!--  <div id="editAgentInfo"  style="display:block;"><button class="btn btn-warning btn-sm">Edit <i class="fa fa-edit" aria-hidden="true"></i></button></div>-->
																			</tr>
																		</c:forEach>


																	</tbody>
																</table>
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>


											</div>
										</div>
									</div>
									<!--row -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->

	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%--    <jsp:include page="siteFooter.jsp" />   --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->


	<!-- JAVA Script Goes Here -->

	<script
		src="<c:url value="resources/assets/js/jquery-2.2.4.min.js" />"></script>
	<!--  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
	<script src="assets/js/apps.js"></script>
	<script src="assets/plugins/retina/retina.min.js"></script>
	<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>


	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script type="text/javascript" src="assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript" src="assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/buttons.print.min.js"></script>
	<script src="assets/js/buttons.colVis.min.js"></script>
	<script src="assets/js/toastr.min.js"></script>

	<script
		src="assets/js/jAlertAudit.min.js"></script>
	<script
		src="assets/jAlert/jAlert-functions.min.js"></script>
	<script
		src=assets/jAlert/jquery.min.bk.js"
		type="text/javascript"></script>

	<script type="text/javascript">
	function deleteAgent(id) {				
		
		confirm(function(e,btn){ //event + button clicked
			e.preventDefault();
		    //successAlert('Confirmed!');			
			
		     $.ajax({
				type : "post",
				beforeSend : function() {
					$('.loader').addClass("is-active");
				},
				url : '${pageContext.request.contextPath}/deleteAgentDone',
				data : {
					id : id
				},
				success : function(data) {
					obj = JSON.parse(data);
					//alert("obj.result:"+obj.result);
					if (obj.result == '1') {
						//successAlert('Success!', 'Agent Deleted Successfully!');
						window.location.href = '${pageContext.request.contextPath}/listAgents';
					}
					else {
						errorAlert('Problem');			
					}
				},
				complete : function() {
					$('.loader').removeClass("is-active");
				},
				error : function(request, status, error) {
					alert(request.responseText);
				}
			});
		     
		}, function(e,btn){
		    e.preventDefault();
		   // errorAlert('Denied!');
		    //return false;						    
		});			
	}
	
	</script>
	<script>
		function validateForm() {
			var x = document.forms["agentList"]["dateFrom"].value;
			var y = document.forms["agentList"]["dateTo"].value;
			if (x == null || x == "") {
				if (y != "") {
					alert("Please choose Date From");
					return false;
				}

			}
			return true;

		}
	</script>
	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->

	<script>
		$(document)
				.ready(
						function() {

							$('#admin-datatable-second_admin').DataTable({
								"order" : [],
								dom : 'Bfrtip',
								title : ' Agent List',

								buttons : [ 'copy', 'csv', 'print', {
									extend : 'excelHtml5',
									title : ' Agent List'
								}, {
									extend : 'pdfHtml5',
									title : ' Agent List Report'
								} ]
							});

							// $('#date1').datepicker();
							$('#date1').datepicker({
								format : 'dd/mm/yyyy',
							}).on('changeDate', function(e) {
								$(this).datepicker('hide');

							});
							$('#date2').datepicker({
								format : 'dd/mm/yyyy',
							}).on('changeDate', function(e) {
								$(this).datepicker('hide');
							});

							var agentAddedMessage = "<c:out value="${agentAddedMessage}"/>";
							// show when the button is clicked
							if (agentAddedMessage.length) {
								toastr.success(agentAddedMessage);
							}

							var agentDeletedMessage = "<c:out value="${agentDeletedMessage}"/>";
							// show when the button is clicked
							if (agentDeletedMessage.length) {
								toastr.success(agentDeletedMessage);
							}

							var agentUpdatedMessage = "<c:out value="${agentUpdatedMessage}"/>";
							// show when the button is clicked
							if (agentUpdatedMessage.length) {
								toastr.success(agentUpdatedMessage);
							}
							var agentUpdatedMessage = "<c:out value="${agentUpdatedMessageE}"/>";
							// show when the button is clicked
							if (agentUpdatedMessage.length) {
								toastr.error(agentUpdatedMessage);
							}

						});
	</script>





</body>
</html>