<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.Date"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="../../siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="../../pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>

		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="../../menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Product Management | Houseowner/Householder</h4>
															&nbsp; (Admin are able to manage product including rates,
															discount and payment option)
														</div>
													</div>

												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="title">
																<div class="sub">
																	<h4>Product Infomation Takaful</h4>
																</div>
															</div>
															<div>
																<div class="form-horizontal info-meor">
																	<div class="col-sm-6">
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Product
																				Code</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">: MI</p>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Product
																				Name</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">:
																					Houseowner/Householder</p>
																			</div>
																		</div>
																	</div>
																	<div class="col-sm-6">
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Annual
																				Sales Target (RM)</label>
																			<div class="col-sm-6">
																				<input type="text" class="form-control"
																					value="50,000,000">
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Quotation
																				Validity (day)</label>
																			<div class="col-sm-6">
																				<input type="text" class="form-control" value="14">
																			</div>
																		</div>
																	</div>
																	<div class="gap gap-mini"></div>
																	<div>
																		<label class="control-label">E Document</label>
																	</div>
																	<div class="table-responsive">
																		<table
																			class="table table-striped table-warning table-hover">
																			<thead>
																				<tr>
																					<th style="width: 30px;">No</th>
																					<th>Date</th>
																					<th>File Name</th>
																					<th>File Size</th>
																					<th width="10%">Action</th>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<td>1</td>
																					<td>01-Jan-2020</td>
																					<td>Product Disclosure Sheet</td>
																					<td>226kb</td>
																					<td><a class="btn btn-warning btn-xs" href="#"><i
																							class="fa fa-trash"></i> Delete</a></td>
																				</tr>
																				<tr>
																					<td>2</td>
																					<td>01-Jan-2020</td>
																					<td>Product Disclosure Sheet</td>
																					<td>226kb</td>
																					<td><a class="btn btn-warning btn-xs" href="#"><i
																							class="fa fa-trash"></i> Delete</a></td>
																				</tr>
																				<tr>
																					<td>3</td>
																					<td>01-Jan-2020</td>
																					<td>Product Disclosure Sheet</td>
																					<td>226kb</td>
																					<td><a class="btn btn-warning btn-xs" href="#"><i
																							class="fa fa-trash"></i> Delete</a></td>
																				</tr>
																				<tr>
																					<td>4</td>
																					<td>01-Jan-2020</td>
																					<td>Product Disclosure Sheet</td>
																					<td>226kb</td>
																					<td><a class="btn btn-warning btn-xs" href="#"><i
																							class="fa fa-trash"></i> Delete</a></td>
																				</tr>
																				<tr>
																					<td>5</td>
																					<td>01-Jan-2020</td>
																					<td>Product Disclosure Sheet</td>
																					<td>226kb</td>
																					<td><a class="btn btn-warning btn-xs" href="#"><i
																							class="fa fa-trash"></i> Delete</a></td>
																				</tr>
																				<tr>
																					<td>6</td>
																					<td>01-Jan-2020</td>
																					<td>Product Disclosure Sheet</td>
																					<td>226kb</td>
																					<td><a class="btn btn-warning btn-xs" href="#"><i
																							class="fa fa-trash"></i> Delete</a></td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																	<div class="col-sm-12">
																		<div class="text-right">
																			<a class="btn btn-warning btn-xs" href="#"
																				data-toggle="modal" data-target="#mydelete"><i
																				class="fa fa-plus"></i> Add</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>

												<div class="col-sm-12">
													<div class="text-right">
														<a class="btn btn-default btn-sm" role="button"
															href="HOHHproductinfomation"><i
															class="fa fa-chevron-left"></i> Back</a> <a
															class="btn btn-warning btn-sm"
															href="HOHHproductinfomationUpdate"><i
															class="fa fa-edit"></i> Update</a>
													</div>
												</div>

											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.page-content -->

			<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
			<jsp:include page="../../pageFooter.jsp" />
			<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->

		</div>
		<!-- /.wrapper -->
		<!-- END PAGE CONTENT -->



		<!-- BEGIN BACK TO TOP BUTTON -->
		<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
		<!-- END BACK TO TOP -->


		<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

		<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
		<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="../../siteFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
		<!-- JAVA Script Goes Here -->

		<script type="text/javascript">
         
         
         
         </script>
</body>
</html>