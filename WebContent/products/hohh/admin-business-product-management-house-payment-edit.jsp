<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.Date"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="../../siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
</head>

<body>


	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="../../pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>

		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="../../menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<form:form action="HOHHpaymentUpdate" id="PmntMethodForm"
										name="PmntMethodForm" method="post">

										<div class="col-sm-12">
											<div class="content-inner">
												<div class="row">
													<div class="col-sm-12">
														<div class="title">
															<div class="sub">
																<h4>Product Management | Houseowner/Householder</h4>
																&nbsp; (Admin are able to manage product including
																rates, discount and payment option)
															</div>
														</div>

													</div>

													<div class="col-sm-12">
														<!-- Begin  table -->
														<div class="content-inner">
															<div class="the-box full no-border">
																<div class="title">
																	<div class="sub">
																		<h4>Payment Method</h4>
																	</div>
																</div>
																<div>
																	<div class="form-inline info-meor">
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="control-label">Payment Method</label>
																			</div>
																			<div class="form-group">
																				<div class="checkbox">
																					<label> <input type="checkbox" id="m2u"
																						name="m2u"
																						<c:forEach items="${m2ulist}" var="elementm2u"> 
																			        <c:if test="${elementm2u.status=='1'}">checked</c:if>
																			      </c:forEach>>
																						Maybank2u
																					</label>
																				</div>
																				<div class="checkbox">
																					<label> <input type="checkbox" id="fpx"
																						name="fpx"
																						<c:forEach items="${fpxlist}" var="elementfpx"> 
																			        <c:if test="${elementfpx.status=='1'}">checked</c:if>
																			      </c:forEach>>
																						FPX
																					</label>
																				</div>
																				<div class="checkbox">
																					<label> <input type="checkbox" id="ebpg"
																						name="ebpg"
																						<c:forEach items="${ebpglist}" var="elementebpg"> 
																			        <c:if test="${elementebpg.status=='1'}">checked</c:if>
																			      </c:forEach>>
																						EBPG (Visa/Mastercard)
																					</label>
																				</div>
																				<div class="checkbox">
																					<label> <input type="checkbox" id="amex"
																						name="amex"
																						<c:forEach items="${amexlist}" var="elementamex"> 
																			        <c:if test="${elementamex.status=='1'}">checked</c:if>
																			      </c:forEach>>
																						AMEX
																					</label>
																				</div>
																				<div>
																					<span id="msg_pmntmethod" class="hidden"
																						style="color: red; text-align: left">Please
																						select atleast one payment</span>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<!-- /.the-box -->
														</div>
														<!-- End warning color table -->
													</div>

													<div class="col-sm-12">
														<div class="text-right">
															<a class="btn btn-default btn-sm" role="button"
																href="HOHHpayment"><i class="fa fa-chevron-left"></i>
																Back</a> <input class="btn btn-warning btn-sm"
																id="updatepmntmethod"
																onClick="submitPaymentmethodForm();" type="button"
																value="Update" />
														</div>
													</div>

												</div>
											</div>
											<!--row -->
										</div>
									</form:form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.page-content -->

			<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
			<jsp:include page="../../pageFooter.jsp" />
			<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->

		</div>
		<!-- /.wrapper -->
		<!-- END PAGE CONTENT -->



		<!-- BEGIN BACK TO TOP BUTTON -->
		<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
		<!-- END BACK TO TOP -->


		<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

		<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
		<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="../../siteFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
		<!-- JAVA Script Goes Here -->

		<script type="text/javascript">
         
     
	
	
	
	
	 function submitPaymentmethodForm(){
         result=true;  

    var chkd = document.PmntMethodForm.m2u.checked || document.PmntMethodForm.fpx.checked || document.PmntMethodForm.ebpg.checked || document.PmntMethodForm.amex.checked;

    if (chkd == true) {}
    else {
        
		 $('#msg_pmntmethod').removeClass('hidden');
         $('#msg_pmntmethod').addClass('visible');
         $('#m2u').focus();
         result=false;
    }
        
       
		
         if (result==true) {
         	$('#PmntMethodForm').submit();   
         }
         
     }
	
       
         
     
         
         </script>
</body>
</html>