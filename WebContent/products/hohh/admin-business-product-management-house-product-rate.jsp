<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.Date"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="assets/jAlert/jAlert.css">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="../../siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
</head>

<body>


	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="../../pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>

		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="../../menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Product Management | Houseowner/Householder</h4>
															&nbsp; (Admin are able to manage product including rates,
															discount and payment option)
														</div>
													</div>

												</div>
												<!--  model.addAttribute("insGSTAmountList",insGSTAmountList);
		model.addAttribute("ins_AdditionalBenefit_ETCAmountList",ins_AdditionalBenefit_ETCAmountList);
		model.addAttribute("ins_AdditionalBenefit_RSMDAmountList",ins_AdditionalBenefit_RSMDAmountList);
		model.addAttribute("ins_StampDutyAmountList",ins_StampDutyAmountList);
		model.addAttribute("ins_HC_MinimumCoverageAmountAmountList",ins_HC_MinimumCoverageAmountAmountList);
		model.addAttribute("ins_HB_MaximumCoverageAmountAmountList",ins_HB_MaximumCoverageAmountAmountList);
		model.addAttribute("ins_HB_MinimumCoverageAmountAmountList",ins_HB_MinimumCoverageAmountAmountList);
		model.addAttribute("ins_DirectDiscountAmountList",ins_DirectDiscountAmountList);
		model.addAttribute("ins_HC_MaximumCoverageAmountAmountList",ins_HC_MaximumCoverageAmountAmountList); -->
												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="title">
																<div class="sub">
																	<h4>Product Rates Insurance</h4>
																</div>
															</div>
															<div>
																<div class="form-horizontal info-meor">
																	<div class="col-sm-6">
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Direct
																				Discount (Online)</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					:
																					<c:forEach items="${ins_DirectDiscountAmountList}"
																						var="elementDisc">
																						<c:out value="${elementDisc.paramValue}" />
																					</c:forEach>
																					%
																				</p>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">GST</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					:
																					<c:forEach items="${insGSTAmountList}"
																						var="elementGst">
																						<c:out value="${elementGst.paramValue}" />
																					</c:forEach>
																					%
																				</p>
																			</div>
																		</div>
																	</div>
																	<div class="col-sm-6">
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Stamp
																				Duty</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					: RM
																					<c:forEach items="${ins_StampDutyAmountList}"
																						var="elementStampDuty">
																						<c:out value="${elementStampDuty.paramValue}" />
																					</c:forEach>
																				</p>
																			</div>
																		</div>
																	</div>
																	<div class="gap gap-mini"></div>
																	<div class="col-sm-6">
																		<div class="form-group">
																			<label class="col-sm-6 control-label text-warning">Home
																				Building</label>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Minimum
																				Coverage Amount</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					: RM
																					<c:forEach
																						items="${ins_HB_MinimumCoverageAmountAmountList}"
																						var="elementMinimumCoverageAmount">
																						<c:out
																							value="${elementMinimumCoverageAmount.paramValue}" />
																					</c:forEach>
																				</p>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Maximum
																				Coverage Amount</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					: RM
																					<c:forEach
																						items="${ins_HB_MaximumCoverageAmountAmountList}"
																						var="elementMaximumCoverageAmount">
																						<c:out
																							value="${elementMaximumCoverageAmount.paramValue}" />
																					</c:forEach>
																				</p>
																			</div>
																		</div>
																	</div>
																	<div class="col-sm-6">
																		<div class="form-group">
																			<label class="col-sm-6 control-label text-warning">Home
																				Content</label>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Minimum
																				Coverage Amount</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					: RM
																					<c:forEach
																						items="${ins_HC_MinimumCoverageAmountAmountList}"
																						var="elementMinimumCoverageAmount">
																						<c:out
																							value="${elementMinimumCoverageAmount.paramValue}" />
																					</c:forEach>
																				</p>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Maximum
																				Coverage Amount</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					: RM
																					<c:forEach
																						items="${ins_HC_MaximumCoverageAmountAmountList}"
																						var="elementMaximumCoverageAmount">
																						<c:out
																							value="${elementMaximumCoverageAmount.paramValue}" />
																					</c:forEach>
																				</p>
																			</div>
																		</div>
																	</div>
																	<div class="gap gap-mini"></div>
																	<div class="col-sm-6">
																		<div class="form-group">
																			<label class="col-sm-6 control-label text-warning">Additional
																				Benefit</label>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Riot,
																				Strike and Malicious Damage (Home Building)</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					: RM
																					<c:forEach
																						items="${ins_AdditionalBenefit_RSMDAmountList}"
																						var="elementRSMD">
																						<c:out value="${elementRSMD.paramValue}" />
																					</c:forEach>
																					%
																				</p>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Extended
																				Theft Cover (Home Contents)</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					: RM
																					<c:forEach
																						items="${ins_AdditionalBenefit_ETCAmountList}"
																						var="elementETC">
																						<c:out value="${elementETC.paramValue}" />
																					</c:forEach>
																					%
																				</p>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>

												<div class="col-sm-12">
													<div class="text-right">
														<!--   <a class="btn btn-warning btn-sm" href="HOHHproductrateedit"><i class="fa fa-edit"></i> Edit</a> -->
														<c:set var="pervalue" value="${fn:split(param.pid,'$') }" />
														<c:if test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
															<a class="btn btn-warning btn-sm"
																href="HOHHproductrateedit"><i class="fa fa-edit"></i>
																Edit</a>
														</c:if>

													</div>
												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="title">
																<div class="sub">
																	<h4>Product Rates Takaful</h4>
																</div>
															</div>
															<div>
																<div class="form-horizontal info-meor">
																	<div class="col-sm-6">
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Direct
																				Discount (Online)</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					:
																					<c:forEach items="${inst_DirectDiscountAmountList}"
																						var="elementDisc">
																						<c:out value="${elementDisc.paramValue}" />

																					</c:forEach>
																					%
																				</p>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">GST</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					:
																					<c:forEach items="${instGSTAmountList}"
																						var="elementGst">
																						<c:out value="${elementGst.paramValue}" />
																					</c:forEach>
																					%
																				</p>
																			</div>
																		</div>
																	</div>
																	<div class="col-sm-6">
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Stamp
																				Duty</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					: RM
																					<c:forEach items="${inst_StampDutyAmountList}"
																						var="elementStampDuty">
																						<c:out value="${elementStampDuty.paramValue}" />
																					</c:forEach>
																				</p>
																			</div>
																		</div>
																	</div>
																	<div class="gap gap-mini"></div>
																	<div class="col-sm-6">
																		<div class="form-group">
																			<label class="col-sm-6 control-label text-warning">Home
																				Building</label>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Minimum
																				Coverage Amount</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					: RM
																					<c:forEach
																						items="${inst_HB_MinimumCoverageAmountAmountList}"
																						var="elementMinimumCoverageAmount">
																						<c:out
																							value="${elementMinimumCoverageAmount.paramValue}" />
																					</c:forEach>
																				</p>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Maximum
																				Coverage Amount</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					: RM
																					<c:forEach
																						items="${inst_HB_MaximumCoverageAmountAmountList}"
																						var="elementMaximumCoverageAmount">
																						<c:out
																							value="${elementMaximumCoverageAmount.paramValue}" />
																					</c:forEach>
																				</p>
																			</div>
																		</div>
																	</div>
																	<div class="col-sm-6">
																		<div class="form-group">
																			<label class="col-sm-6 control-label text-warning">Home
																				Content</label>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Minimum
																				Coverage Amount</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					: RM
																					<c:forEach
																						items="${inst_HC_MinimumCoverageAmountAmountList}"
																						var="elementMinimumCoverageAmount">
																						<c:out
																							value="${elementMinimumCoverageAmount.paramValue}" />
																					</c:forEach>
																				</p>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Maximum
																				Coverage Amount</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					: RM
																					<c:forEach
																						items="${inst_HC_MaximumCoverageAmountAmountList}"
																						var="elementMaximumCoverageAmount">
																						<c:out
																							value="${elementMaximumCoverageAmount.paramValue}" />
																					</c:forEach>
																				</p>
																			</div>
																		</div>
																	</div>
																	<div class="gap gap-mini"></div>
																	<div class="col-sm-6">
																		<div class="form-group">
																			<label class="col-sm-6 control-label text-warning">Additional
																				Benefit</label>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Riot,
																				Strike and Malicious Damage (Home Building)</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					: RM
																					<c:forEach
																						items="${inst_AdditionalBenefit_RSMDAmountList}"
																						var="elementRSMD">
																						<c:out value="${elementRSMD.paramValue}" />
																					</c:forEach>
																					%
																				</p>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Extended
																				Theft Cover (Home Contents)</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					: RM
																					<c:forEach
																						items="${inst_AdditionalBenefit_ETCAmountList}"
																						var="elementETC">
																						<c:out value="${elementETC.paramValue}" />
																					</c:forEach>
																					%
																				</p>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>

												<div class="col-sm-12">
													<div class="text-right">
														<!--  <a class="btn btn-warning btn-sm" href="HOHHproductrateeditTKFL"><i class="fa fa-edit"></i> Edit</a> -->

														<c:set var="pervalue" value="${fn:split(param.pid,'$') }" />

														<c:if test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
															<a class="btn btn-warning btn-sm"
																href="HOHHproductrateeditTKFL"><i class="fa fa-edit"></i>
																Edit</a>
														</c:if>

													</div>
												</div>

											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->

		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="../../pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->

	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="../../siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->
	<script src="assets/js/toastr.min.js"></script>
	<script src="assets/jAlert/jAlert.min.js"></script>
	<script src="assets/jAlert/jAlert-functions.min.js"></script>

	<script type="text/javascript">
         
         $(document).ready(function() {
         
	 		       
	 		  var UpdateMessage="<c:out value="${UpdateMessage}"/>";
	 		       // show when the button is clicked
	 		       if (UpdateMessage.length) {
	 		    	  successAlert('Success!',UpdateMessage);
	 		       }
	 		  
			 
			 
		
		    });
         
         </script>
</body>
</html>