<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.Date"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="assets/jAlert/jAlert.css">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="../../siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
</head>

<body>


	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="../../pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>

		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="../../menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Product Management | Houseowner/Householder</h4>
															&nbsp;(Admin are able to manage product including rates,
															discount and payment option)
														</div>

													</div>
												</div>

											</div>

											<div class="col-sm-12">
												<!-- Begin  table -->
												<div class="content-inner">
													<div class="the-box full no-border">
														<div class="title">
															<div class="sub">
																<h4>Payment Method</h4>
															</div>
														</div>
														<div>
															<div class="form-horizontal info-meor">
																<div class="col-sm-6">
																	<div class="form-group">
																		<label class="col-sm-3 control-label">Payment
																			Method</label>
																		<div class="col-sm-9">
																			<p class="form-control-static">
																				:
																				<c:forEach items="${m2ulist}" var="elementm2u">
																					<c:out value="${elementm2u.paramName}" />
																				</c:forEach>
																				<c:forEach items="${fpxlist}" var="elementfpx"> 
																			 / <c:out value="${elementfpx.paramName}" />
																				</c:forEach>
																				<c:forEach items="${ebpglist}" var="elementebpg"> 
																			 / <c:out value="${elementebpg.paramName}" />
																				</c:forEach>
																				<c:forEach items="${amexlist}" var="elementamex"> 
																			 / <c:out value="${elementamex.paramName}" />
																				</c:forEach>
																			</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- /.the-box -->
												</div>
												<!-- End warning color table -->
											</div>

											<div class="col-sm-12">
												<div class="text-right">
													<!-- Pramain Start  -->

													<c:set var="HohhPaypervalue"
														value="${fn:split(param.pid,'$')}" />
													<c:out value="${HohhPaypervalue[0]}"></c:out>
													<c:if
														test="${(HohhPaypervalue[0] eq 1) || (HohhPaypervalue[0] eq 3 )}">
														<a class="btn btn-warning btn-sm"
															href="HOHHhousepaymentedit"><i class="fa fa-edit"></i>
															Edit</a>
													</c:if>

													<!-- Pramain end -->

													<!--       <a class="btn btn-warning btn-sm" href="HOHHhousepaymentedit"><i class="fa fa-edit"></i> Edit</a> -->


												</div>
											</div>

											<div class="col-sm-12">
												<!-- Begin  table -->
												<div class="content-inner">
													<div class="the-box full no-border">
														<div class="title">
															<div class="sub">
																<h4>Payment Method Takaful</h4>
															</div>
														</div>
														<div>
															<div class="form-horizontal info-meor">
																<div class="col-sm-6">
																	<div class="form-group">
																		<label class="col-sm-3 control-label">Payment
																			Method</label>
																		<div class="col-sm-9">
																			<p class="form-control-static">
																				:
																				<c:forEach items="${m2ulistt}" var="elementm2u">
																					<c:out value="${elementm2u.paramName}" />
																				</c:forEach>
																				<c:forEach items="${fpxlistt}" var="elementfpx"> 
																			 / <c:out value="${elementfpx.paramName}" />
																				</c:forEach>
																				<c:forEach items="${ebpglistt}" var="elementebpg"> 
																			 / <c:out value="${elementebpg.paramName}" />
																				</c:forEach>
																				<c:forEach items="${amexlistt}" var="elementamex"> 
																			 / <c:out value="${elementamex.paramName}" />
																				</c:forEach>
																			</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- /.the-box -->
												</div>
												<!-- End warning color table -->
											</div>

											<div class="col-sm-12">
												<div class="text-right">

													<!-- Pramain Start  -->

													<c:set var="HohhPaypervalue"
														value="${fn:split(param.pid,'$')}" />
													<c:out value="${HohhPaypervalue[0]}"></c:out>
													<c:if
														test="${(HohhPaypervalue[0] eq 1) || (HohhPaypervalue[0] eq 3 )}">
														<a class="btn btn-warning btn-sm"
															href="HOHHhousepaymentedittakaful"><i
															class="fa fa-edit"></i> Edit</a>
													</c:if>

													<!-- Pramain end -->

													<!--   <a class="btn btn-warning btn-sm" href="HOHHhousepaymentedittakaful"><i class="fa fa-edit"></i> Edit</a> -->
												</div>
											</div>

										</div>
									</div>
									<!--row -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->

	</div>
	<!-- /.page-content -->

	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="../../pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->

	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="../../siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->
	<script src="assets/js/toastr.min.js"></script>
	<script src="assets/jAlert/jAlert.min.js"></script>
	<script src="assets/jAlert/jAlert-functions.min.js"></script>

	<script type="text/javascript">
         
         $(document).ready(function() {
         
	 		       
	 		  var UpdateMessage="<c:out value="${UpdateMessage}"/>";
	 		       // show when the button is clicked
	 		       if (UpdateMessage.length) {
	 		    	  successAlert('Success!',UpdateMessage);
	 		       }
	 		  
			 
			 
		
		    });
         
         </script>

</body>
</html>