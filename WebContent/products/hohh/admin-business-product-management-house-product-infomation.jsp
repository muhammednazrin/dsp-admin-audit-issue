<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.Date"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="assets/jAlert/jAlert.css">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="../../siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
</head>

<body>


	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="../../pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>

		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="../../menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Product Management | Houseowner/Householder</h4>
															&nbsp;(Admin are able to manage product including rates,
															discount and payment option)
														</div>
													</div>

												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="title">
																<div class="sub">
																	<h4>Product Infomation</h4>
																</div>
															</div>
															<div>
																<div class="form-horizontal info-meor">
																	<%--  <c:forEach var="product" items="${productList.rows}"> --%>
																	<div class="col-sm-6">
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Product
																				Code</label>
																			<div class="col-sm-6">
																				<%--  <p class="form-control-static">: <c:out value="${product.DSP_PRODUCT_CODE}" /></p> --%>
																				<p class="form-control-static">: HOHH</p>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Product
																				Name</label>
																			<div class="col-sm-6">
																				<%--   <p class="form-control-static">: <c:out value="${product.DSP_PRODUCT_CODE_NAME}" /></p> --%>
																				<p class="form-control-static">:
																					Houseowner/Householder</p>
																			</div>
																		</div>
																	</div>
																	<div class="col-sm-6">
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Annual
																				Sales Target</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					:
																					<c:forEach items="${annualSalesAmntList}"
																						var="elementAnnualSalesTarget">
																						<c:out
																							value="${elementAnnualSalesTarget.paramValue}" />
																					</c:forEach>

																				</p>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Quotation
																				Validity</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					:

																					<c:forEach items="${quotationValidityList}"
																						var="elementQuotationValidity">
																						<c:out
																							value="${elementQuotationValidity.paramValue}" />
																					</c:forEach>

																				</p>
																			</div>
																		</div>
																	</div>
																	<%--   </c:forEach> --%>
																	<div class="gap gap-mini"></div>
																	<div>
																		<label class="control-label">E Document</label>
																	</div>
																	<%--  <table class="table table-striped table-warning table-hover">
                                                                    <thead>
                                                                        <tr>
	                                                                        <th style="width: 30px;">No</th>
	                                                                        <th>Date</th>
	                                                                        <th>File Name</th>
	                                                                        <th>File Size</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
 
                                                                     <c:forEach var="productDoc" items="${productDocList.rows}" varStatus="loop">
                                                                        <tr>
	                                                                        <td><c:out value="${loop.count}" /></td>
	                                                                        <td><c:out value="${productDoc.CREATION_TIMESTAMP}" /></td>
	                                                                        <td><c:out value="${productDoc.FILENAME }" /></td>
	                                                                        <td>226kb</td>
                                                                        </tr>
                                                                     </c:forEach>   
                                                                    </tbody>
                                                                </table> --%>
																	<table
																		class="table table-striped table-warning table-hover"
																		id="tlPDSTable">
																		<thead>
																			<tr>
																				<th style="width: 30px;">No</th>
																				<th>Date</th>
																				<th>File Name</th>
																				<th>File Size</th>
																			</tr>
																		</thead>
																		<tbody>

																			<c:forEach items="${tblPdfInfoList}" var="element"
																				varStatus="theCount">
																				<tr>
																					<td><c:out value="${theCount.count}" /></td>
																					<c:set var="createDate"
																						value="${element.createdDate}" />
																					<td><fmt:formatDate type="both"
																							value="${createDate}"
																							pattern="dd-MMM-yyyy hh:mm:ss" /></td>
																					<td><form:form id="form1" name="form1"
																							action="DownloadPdfFile" method="post">
																							<input type="hidden" name="tlFilePathDownload"
																								value="<c:out value="${element.filePath}"/>" />
																							<a target="_blank" href="#"
																								onClick="submitTLPdfDownload();"><c:out
																									value="${element.fileName}" /></a>
																						</form:form></td>
																					<td><c:out value="${element.fileSize}" /> kb</td>
																				</tr>
																			</c:forEach>
																		</tbody>
																	</table>
																</div>
															</div>
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>

												<div class="col-sm-12">
													<div class="text-right">
														<!--   <a class="btn btn-warning btn-sm" href="HOHHproductinfomationEdit"><i class="fa fa-edit"></i> Edit</a> -->
														<c:set var="pervalue" value="${fn:split(param.pid,'$') }" />

														<c:if test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
															<a class="btn btn-warning btn-sm"
																href="HOHHproductinfomationEdit"><i
																class="fa fa-edit"></i> Edit</a>
														</c:if>
													</div>
												</div>

											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->

		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="../../pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->

	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="../../siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->
	<script src="assets/js/toastr.min.js"></script>
	<script src="assets/jAlert/jAlert.min.js"></script>
	<script src="assets/jAlert/jAlert-functions.min.js"></script>

	<script type="text/javascript">
         
         $(document).ready(function() {
         
	 		       
	 		  var updatemessage="<c:out value="${updatemessage}"/>";
	 		       // show when the button is clicked
	 		       if (updatemessage.length) {
	 		    	  successAlert('Success!',updatemessage);
	 		       }
	 		  
			 
			 
		
		    });
         
         </script>

	<script type="text/javascript">
         
         
         function submitTLPdfDownload(){
             
          	$('#form1').submit();   
          
      }
 	
        /*   $('#tlPDSTable').DataTable({
 			 "order": [],
 			dom : 'Bfrtip',
 			title : ' Agent List',
 		

 			buttons : [ 'csv', 'print', {
 				extend : 'excelHtml5',
 				title : 'Sales_Leads_'+today
 			}, {
 				extend : 'pdfHtml5',
 				title :  'Sales_Leads_'+today
 			} ]
 		});
 */          
         
         </script>

</body>
</html>