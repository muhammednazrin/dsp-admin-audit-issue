<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.Date"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="../../siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
</head>

<body>



	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="../../pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>

		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="../../menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Product Management | Houseowner/Householder</h4>
														</div>
													</div>

												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="title">
																<div class="sub">
																	<h4>Product Infomation</h4>
																	&nbsp; (Admin are able to manage product including
																	rates, discount and payment option)
																</div>
															</div>
															<div>
																<form:form action="HOHHProductInfoUpdateApproval"
																	id="updateDoneHOHHprodmngproductinfoForm"
																	name="updateDoneHOHHprodmngproductinfoForm"
																	method="post">
																	<%--  <form:form action="HOHHproductinfomationUpdate"  id="updateDoneHOHHprodmngproductinfoForm" name="updateDoneHOHHprodmngproductinfoForm" method="post"> --%>
																	<div class="form-horizontal info-meor">
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Product
																					Code</label>
																				<div class="col-sm-6">
																					<p class="form-control-static">: HOHH</p>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Product
																					Name</label>
																				<div class="col-sm-6">
																					<p class="form-control-static">:
																						Houseowner/Householder</p>
																				</div>
																			</div>
																		</div>
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Annual
																					Sales Target (RM)</label>
																				<div class="col-sm-6">
																					<c:forEach items="${annualSalesAmntList}"
																						var="elementAnnualSalesTarget">
																						<input type="text" name="AnnualSalesTarget"
																							id="AnnualSalesTarget" class="form-control"
																							value=<c:out value="${elementAnnualSalesTarget.paramValue}" />>
																					</c:forEach>

																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Quotation
																					Validity (day)</label>
																				<div class="col-sm-6">
																					<c:forEach items="${quotationValidityList}"
																						var="elementQuotationValidity">
																						<input type="text" name="QuotationValidity"
																							id="QuotationValidity" class="form-control"
																							value=<c:out value="${elementQuotationValidity.paramValue}" />>
																					</c:forEach>

																				</div>
																			</div>
																		</div>
																		<div class="gap gap-mini"></div>
																		<div>
																			<label class="control-label">E Document</label>
																		</div>
																		<!--  <div class="table-responsive">
                                                                    <table class="table table-striped table-warning table-hover">
                                                                        <thead>
                                                                            <tr><th style="width: 30px;">No</th><th>Date</th><th>File Name</th><th>File Size</th><th width="10%">Action</th></tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr><td>1</td><td>01-Jan-2020</td><td>Product Disclosure Sheet</td><td>226kb</td><td><a class="btn btn-warning btn-xs" href="#"><i class="fa fa-trash"></i> Delete</a></td></tr>
                                                                            <tr><td>2</td><td>01-Jan-2020</td><td>Product Disclosure Sheet</td><td>226kb</td><td><a class="btn btn-warning btn-xs" href="#"><i class="fa fa-trash"></i> Delete</a></td></tr>
                                                                            <tr><td>3</td><td>01-Jan-2020</td><td>Product Disclosure Sheet</td><td>226kb</td><td><a class="btn btn-warning btn-xs" href="#"><i class="fa fa-trash"></i> Delete</a></td></tr>
                                                                            <tr><td>4</td><td>01-Jan-2020</td><td>Product Disclosure Sheet</td><td>226kb</td><td><a class="btn btn-warning btn-xs" href="#"><i class="fa fa-trash"></i> Delete</a></td></tr>
                                                                            <tr><td>5</td><td>01-Jan-2020</td><td>Product Disclosure Sheet</td><td>226kb</td><td><a class="btn btn-warning btn-xs" href="#" ><i class="fa fa-trash"></i> Delete</a></td></tr>
                                                                            <tr><td>6</td><td>01-Jan-2020</td><td>Product Disclosure Sheet</td><td>226kb</td><td><a class="btn btn-warning btn-xs" href="#"><i class="fa fa-trash"></i> Delete</a></td></tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div> -->
																		<table
																			class="table table-striped table-warning table-hover">
																			<thead>
																				<tr>
																					<th style="width: 30px;">No</th>
																					<th>Date</th>
																					<th>File Name</th>
																					<th>File Size</th>
																					<th width="10%">Action</th>
																				</tr>
																			</thead>
																			<tbody>

																				<c:forEach items="${tblPdfInfoList}" var="element"
																					varStatus="theCount">
																					<tr>
																						<td><c:out value="${theCount.count}" /></td>
																						<c:set var="createDate"
																							value="${element.createdDate}" />
																						<td><fmt:formatDate type="both"
																								value="${createDate}"
																								pattern="dd-MMM-yyyy hh:mm:ss" /></td>
																						<td><c:out value="${element.fileName}" /></td>
																						<td><c:out value="${element.fileSize}" /> kb</td>

																						<td><form:form action="HOHHdeletePdsDone"
																								method="post" name="tlpds" id="tlpds">
																								<input type="hidden" name="id"
																									value="<c:out value="${element.id}"/>" />
																								<input type="hidden" name="tlFilePath"
																									value="<c:out value="${element.filePath}"/>" />
																								<input class="btn btn-warning btn-sm"
																									type="submit" name="submit" value="Delete">
																							</form:form> <!-- <a class="btn btn-warning btn-xs" href="#"><i class="fa fa-trash"></i> Delete</a> -->
																						</td>
																					</tr>
																				</c:forEach>
																			</tbody>
																		</table>

																	</div>
																</form:form>
																<div class="col-sm-12">
																	<div class="text-right">
																		<a class="btn btn-warning btn-xs" href="#mydelete"
																			data-toggle="modal" data-target=#mydelete><i
																			class="fa fa-plus"></i> Add</a>

																	</div>
																</div>
															</div>
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>

												<div class="col-sm-12">
													<div class="text-right">
														<a class="btn btn-default btn-sm" role="button"
															href="HOHHproductinfomation"><i
															class="fa fa-chevron-left"></i> Back</a>
														<!--                   <a class="btn btn-warning btn-sm" href="HOHHproductinfomationUpdate"><i class="fa fa-edit"></i> Update</a> -->
														<input class="btn btn-warning btn-sm"
															onClick="submitHOHHProductInfo();" type="submit"
															value="Update" />
													</div>
												</div>

											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Modal delete-->
				<!--  <form:form action="uploadTLPdfFile" method="post" enctype="multipart/form-data"  id="TLPdfFile"> -->
				<div class="modal fade" id="mydelete" tabindex="-1" role="dialog"
					aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<h4 class="modal-title" id="myModalLabel">Product
									Management | Ezy-Life Insurance | E-Documents</h4>
							</div>
							<div class="modal-body">
								<div class="form-harizontal">
									<div class="form-group">
										<label>File Name <span class="text-danger">*</span></label> <input
											type="text" name="tlFileName" class="form-control">
									</div>
								</div>
								<div class="form-group">
									<label>File <span class="text-danger">*</span></label> <input
										type="file" name="PDF" id="browsepdf" accept=".pdf">
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default"
									data-dismiss="modal">Cancel</button>
								<!--    <button type="button" class="btn btn-warning">Save</button> -->
								<label class="btn btn-default disabled" id="uploadTLPdf">
									Save <input type="submit" value="submit" style="display: none;">
								</label>
							</div>
						</div>
					</div>
				</div>
				<!--  </form:form> -->
				<!-- Modal -->


			</div>
			<!-- /.page-content -->

			<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
			<jsp:include page="../../pageFooter.jsp" />
			<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->

		</div>
		<!-- /.wrapper -->


		<!-- END PAGE CONTENT -->



		<!-- BEGIN BACK TO TOP BUTTON -->
		<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
		<!-- END BACK TO TOP -->


		<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

		<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
		<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="../../siteFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
		<!-- JAVA Script Goes Here -->




		<script type="text/javascript">
        function submitHOHHProductInfo(){    
           $('#updateDoneHOHHprodmngproductinfoForm').submit();
           return true;
        }
        
      
       
        </script>
</body>
</html>