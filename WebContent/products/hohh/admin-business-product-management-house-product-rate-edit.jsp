<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.Date"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="../../siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
</head>

<body>


	<!--
===========================================================
BEGIN PAGE
===========================================================
-->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="../../pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>

		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="../../menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Product Management | Houseowner/Householder</h4>
															&nbsp; (Admin are able to manage product including rates,
															discount and payment option)
														</div>
													</div>

												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="title">
																<div class="sub">
																	<h4>Product Rates</h4>
																</div>
															</div>
															<div>
																<%--   <form:form action="HOHHproductrateUpdate"  id="HOHHproductrateUpdate" name="HOHHproductrateUpdate" method="post"> --%>
																<form:form action="HOHHproductrateSave"
																	id="HOHHproductrateUpdate" name="HOHHproductrateUpdate"
																	method="post">
																	<div class="form-horizontal info-meor">
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Direct
																					Discount (Online) (%)</label>
																				<div class="col-sm-6">
																					<c:forEach items="${ins_DirectDiscountAmountList}"
																						var="elementDisc">
																						<input type="text" name="discountVal"
																							id="discountVal" class="form-control"
																							value=<c:out value="${elementDisc.paramValue}" />>
																					</c:forEach>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">GST
																					(%)</label>
																				<div class="col-sm-6">
																					<c:forEach items="${insGSTAmountList}"
																						var="elementGst">
																						<input type="text" name="GST" id="GST"
																							class="form-control"
																							value=<c:out value="${elementGst.paramValue}" />>
																					</c:forEach>
																				</div>
																			</div>
																		</div>
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Stamp
																					Duty (RM)</label>
																				<div class="col-sm-6">
																					<c:forEach items="${ins_StampDutyAmountList}"
																						var="elementStampDuty">
																						<input type="text" name="StampDuty" id="StampDuty"
																							class="form-control"
																							value=<c:out value="${elementStampDuty.paramValue}" />>
																					</c:forEach>
																				</div>
																			</div>
																		</div>
																		<div class="gap gap-mini"></div>
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-6 control-label text-warning">Home
																					Building</label>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Minimum
																					Coverage Amount (RM)</label>
																				<div class="col-sm-6">
																					<c:forEach
																						items="${ins_HB_MinimumCoverageAmountAmountList}"
																						var="elementMinimumCoverageAmount">
																						<input type="text" name="HBMinimumCoverageAmount"
																							id="HBMinimumCoverageAmount" class="form-control"
																							value=<c:out value="${elementMinimumCoverageAmount.paramValue}" />>
																					</c:forEach>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Maximum
																					Coverage Amount (RM)</label>
																				<div class="col-sm-6">
																					<c:forEach
																						items="${ins_HB_MaximumCoverageAmountAmountList}"
																						var="elementMaximumCoverageAmount">
																						<input type="text" name="HBMaximumCoverageAmount"
																							id="HBMaximumCoverageAmount" class="form-control"
																							value=<c:out value="${elementMaximumCoverageAmount.paramValue}" />>
																					</c:forEach>
																				</div>
																			</div>
																		</div>
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-6 control-label text-warning">Home
																					Content</label>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Minimum
																					Coverage Amount (RM)</label>
																				<div class="col-sm-6">
																					<c:forEach
																						items="${ins_HC_MinimumCoverageAmountAmountList}"
																						var="elementMinimumCoverageAmount">
																						<input type="text" name="HCMinimumCoverageAmount"
																							id="HCMinimumCoverageAmount" class="form-control"
																							value=<c:out value="${elementMinimumCoverageAmount.paramValue}" />>
																					</c:forEach>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Maximum
																					Coverage Amount (RM)</label>
																				<div class="col-sm-6">
																					<c:forEach
																						items="${ins_HC_MaximumCoverageAmountAmountList}"
																						var="elementMaximumCoverageAmount">
																						<input type="text" name="HCMaximumCoverageAmount"
																							id="HCMaximumCoverageAmount" class="form-control"
																							value=<c:out value="${elementMaximumCoverageAmount.paramValue}" />>
																					</c:forEach>
																				</div>
																			</div>
																		</div>
																		<div class="gap gap-mini"></div>
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-6 control-label text-warning">Additional
																					Benefit</label>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Riot,
																					Strike and Malicious Damage (Home Building) (RM)
																					(%)</label>
																				<div class="col-sm-6">
																					<c:forEach
																						items="${ins_AdditionalBenefit_RSMDAmountList}"
																						var="elementRSMD">
																						<input type="text" name="RSMD" id="RSMD"
																							class="form-control"
																							value=<c:out value="${elementRSMD.paramValue}" />>
																					</c:forEach>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Extended
																					Theft Cover (Home Contents) (RM) (%)</label>
																				<div class="col-sm-6">
																					<c:forEach
																						items="${ins_AdditionalBenefit_ETCAmountList}"
																						var="elementETC">
																						<input type="text" name="ETC" id="ETC"
																							class="form-control"
																							value=<c:out value="${elementETC.paramValue}" />>
																					</c:forEach>
																					<!--  <input type="text" class="form-control" value="5000000"> -->
																				</div>
																			</div>
																		</div>
																	</div>

																</form:form>
															</div>
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>

												<div class="col-sm-12">
													<div class="text-right">
														<a class="btn btn-default btn-sm" role="button"
															href="HOHHproductrate"><i class="fa fa-chevron-left"></i>
															Back</a> <input class="btn btn-warning btn-sm"
															onClick="submitTLProductRates();" type="submit"
															value="Update" />
														<!--  <a class="btn btn-warning btn-sm" href="HOHHproductrateUpdate"><i class="fa fa-edit"></i> Update</a> -->
													</div>
												</div>

											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.page-content -->

			<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
			<jsp:include page="../../pageFooter.jsp" />
			<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->

		</div>
		<!-- /.wrapper -->
		<!-- END PAGE CONTENT -->



		<!-- BEGIN BACK TO TOP BUTTON -->
		<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
		<!-- END BACK TO TOP -->


		<!--
===========================================================
END PAGE
===========================================================
-->

		<!--
===========================================================
Placed at the end of the document so the pages load faster
===========================================================
-->
		<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="../../siteFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
		<!-- JAVA Script Goes Here -->

		<script type="text/javascript">
function submitTLProductRates(){

    	$('#HOHHproductrateUpdate').submit();   
    
}

</script>
</body>
</html>