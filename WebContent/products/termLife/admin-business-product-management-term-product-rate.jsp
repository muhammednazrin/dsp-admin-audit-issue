<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!---------------------------------------------------- BEGIN Site Header----------------------------------------------------------->
<jsp:include page="../../siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>

<!--  <!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!--   PLUGINS CSS -->
<link href="plugins/owl-carousel/owl.carousel.min.css" rel="stylesheet">
<link href="plugins/owl-carousel/owl.theme.min.css" rel="stylesheet">
<link href="plugins/owl-carousel/owl.transitions.min.css"
	rel="stylesheet">

<!--  MAIN CSS (REQUIRED ALL PAGE) -->
<!-- <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="plugins/footable-3/css/footable.bootstrap.css" rel="stylesheet">
        <link href="css/owl.css" rel="stylesheet">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap-select.css" rel="stylesheet">
        <link href="css/menu.css" rel="stylesheet">
        <link href="plugins/datepicker/datepicker.min.css" rel="stylesheet"> -->
<!--<link href="css/style.css" rel="stylesheet">-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!-- header -->
		<link rel="stylesheet" href="assets/jAlert/jAlert.css">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="../../pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>


		<!-- end header -->
		<!-- header second-->

		<!-- header second-->
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="/menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>


					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Product Management | Life Secure</h4>
															&nbsp; (Admin are able to manage product including rates,
															discount and payment option)
														</div>
													</div>

												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="title">
																<div class="sub">
																	<h4>Product Rates</h4>
																</div>
															</div>
															<div>
																<div class="form-horizontal info-meor">
																	<div class="col-sm-6">
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Direct
																				Discount (Online)</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					:
																					<c:forEach items="${discountValueList}"
																						var="elementDisc">
																						<c:out value="${elementDisc.paramDesc}" />
																					</c:forEach>
																				</p>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">GST</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					:
																					<c:forEach items="${gstValueList}" var="elementGst">
																						<c:out value="${elementGst.paramDesc}" />
																					</c:forEach>
																				</p>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Stamp
																				Duty</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					:
																					<c:forEach items="${stampDutyList}"
																						var="elementStampDuty">
																						<c:out value="${elementStampDuty.paramDesc}" />
																					</c:forEach>
																				</p>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Minimum
																				Age</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					:
																					<c:forEach items="${dspTblRangeRulesList}"
																						var="elementminAge">
																						<c:out value="${elementminAge.ruleCodeMinValue}" />
																					</c:forEach>
																				</p>
																			</div>
																		</div>
																	</div>
																	<div class="col-sm-6">
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Minimum
																				Benefit Amount</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					:
																					<c:forEach items="${minBenAmountList}"
																						var="elementmin">
																						<c:out value="${elementmin.paramDesc}" />
																					</c:forEach>
																				</p>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Maximum
																				Benefit Amount</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					:
																					<c:forEach items="${maxBenAmountList}"
																						var="elementmax">
																						<c:out value="${elementmax.paramDesc}" />
																					</c:forEach>
																				</p>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Total
																				Sum Assured at Risk (TSAR)</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					:
																					<c:forEach items="${tsarValueList}"
																						var="elementTsar">
																						<c:out value="${elementTsar.paramDesc}" />
																					</c:forEach>
																				</p>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Maximum
																				Age</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					:
																					<c:forEach items="${dspTblRangeRulesList}"
																						var="elementmaxAge">
																						<c:out value="${elementmaxAge.ruleCodeMaxValue}" />
																					</c:forEach>
																				</p>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>

												<div class="col-sm-12">
													<div class="text-right">
														<!--    <a class="btn btn-warning btn-sm" href="updateTLProductRates"><i class="fa fa-edit"></i> Edit</a> -->

														<c:set var="pervalue" value="${fn:split(param.pid,'$') }" />

														<c:if test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
															<a class="btn btn-warning btn-sm"
																href="updateTLProductRates"><i class="fa fa-edit"></i>
																Edit</a>
														</c:if>


													</div>
												</div>

											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->
		<!-- BEGIN FOOTER -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<%--  <jsp:include page="../../pageFooter.jsp" /> --%>
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->

		<!-- END FOOTER -->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->

	<div id="back-top">
		<a href="#top"><i class="fa fa-chevron-up"></i></a>
	</div>

	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->

	<script
		src="<c:url value="/resources/assets/js/jquery-2.2.4.min.js" />"></script>
	<!--  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
	<script src="assets/js/apps.js"></script>
	<script src="assets/plugins/retina/retina.min.js"></script>
	<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>


	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script type="text/javascript" src="assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript" src="assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/buttons.print.min.js"></script>
	<script src="assets/js/buttons.colVis.min.js"></script>
	<script src="assets/js/toastr.min.js"></script>
	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="assets/jAlert/jAlert.min.js"></script>
	<script src="assets/jAlert/jAlert-functions.min.js"></script>
	<script>
		$(document).ready(function() {

			$('#tbl-nvic-list').DataTable({
				 "order": [],
				dom : 'Bfrtip',
				title : 'MI NVI List',
			

				buttons : [ 'copy', 'csv', 'print', {
					extend : 'excelHtml5',
					title : ' MI NVI List'
				}, {
					extend : 'pdfHtml5',
					title :  'MI NVI List Report'
				} ]
			});
			
			// $('#date1').datepicker();
			 $('#date1').datepicker({
				    format: 'dd/mm/yyyy',
			         }).on('changeDate', function(e){
	 			    $(this).datepicker('hide');
	 			
				});
			 $('#date2').datepicker({
				    format: 'dd/mm/yyyy',
			        }).on('changeDate', function(e){
	 			    $(this).datepicker('hide');
	 			    });
			
			 var SaveMessage="<c:out value="${SaveMessage}"/>";
	 		       // show when the button is clicked
	 		       if (SaveMessage.length) {
	 		    	   successAlert('Success!',SaveMessage);
	 		       }
	 		      
	 		  var deletemessage="<c:out value="${deletemessage}"/>";
	 		       // show when the button is clicked
	 		       if (deletemessage.length) {
	 		    	   successAlert('Success!',deletemessage);
	 		       }
	 		       
	 		  var updatemessage="<c:out value="${updatemessage}"/>";
	 		       // show when the button is clicked
	 		       if (updatemessage.length) {
	 		    	  successAlert('Success!',updatemessage);
	 		       }
	 		  
			 
			 
		
		    });
		    </script>

</body>
</html>