<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.Date"%>

<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="../../siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#deleteDoneprodinfo {
	background: none;
	border: none;
	text-decoration: underline;
	padding: 0;
	color: dodgerblue;
}

#deleteDoneprodinfo:hover {
	color: dodgerblue;
}
</style>

<link rel="stylesheet" href="assets/jAlert/jAlert.css">
</head>

<body>


	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="../../pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="/menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Product Management | Life</h4>
															&nbsp; (Admin are able to manage product including rates,
															discount and payment option)
														</div>
													</div>

												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="title">
																<div class="sub">
																	<h4>Product Infomation</h4>
																</div>
															</div>
															<div>
																<div class="form-horizontal info-meor">

																	<div class="col-sm-6">
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Product
																				Code</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">: TL</p>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Product
																				Name</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">: Ezy-Life Secure</p>
																			</div>
																		</div>
																	</div>
																	<div class="col-sm-6">
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Annual
																				Sales Target</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					:
																					<c:forEach items="${annualSalesAmntList}"
																						var="elementAnnSales">
																						<c:out value="${elementAnnSales.paramDesc}" />
																					</c:forEach>
																				</p>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-6 control-label">Quotation
																				Validity</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">
																					:
																					<c:forEach items="${quotationValidityList}"
																						var="elementQuotVal">
																						<c:out value="${elementQuotVal.paramDesc}" />
																					</c:forEach>
																				</p>
																			</div>
																		</div>
																	</div>

																</div>
																<div class="gap gap-mini"></div>
																<div>
																	<label class="control-label">E Document</label>
																</div>
																<table
																	class="table table-striped table-warning table-hover"
																	id="tlPDSTable">
																	<thead>
																		<tr>
																			<th style="width: 30px;">No</th>
																			<th>Date</th>
																			<th>File Name</th>
																			<th>File Size</th>
																		</tr>
																	</thead>
																	<tbody>

																		<c:forEach items="${tblPdfInfoList}" var="element"
																			varStatus="theCount">
																			<tr>
																				<td><c:out value="${theCount.count}" /></td>
																				<c:set var="createDate"
																					value="${element.createdDate}" />
																				<td><fmt:formatDate type="both"
																						value="${createDate}"
																						pattern="dd-MMM-yyyy hh:mm:ss" /></td>
																				<td><form:form id="form1" name="form1"
																						action="DownloadPdfFile" method="post">
																						<input type="hidden" name="tlFilePathDownload"
																							value="<c:out value="${element.filePath}"/>" />
																						<input type="hidden" name="tlFileName"
																							value="<c:out value="${element.fileName}"/>" />
																						<input type="hidden" name="id"
																							value="<c:out value="${element.id}"/>" />
																						<!--  <a target="_blank" href="#" onClick="return submitTLPdfDownload();" ></a> -->
																						<input class="btn btn-warning btn-sm"
																							id="deleteDoneprodinfo"
																							onClick="return submitTLPdfDownload();"
																							type="submit"
																							value="<c:out value="${element.fileName}"/>" />
																					</form:form></td>
																				<td><c:out value="${element.fileSize}" /> kb</td>
																			</tr>
																		</c:forEach>
																	</tbody>
																</table>
															</div>
														</div>
													</div>
													<!-- /.the-box -->
												</div>
												<!-- End warning color table -->
											</div>

											<div class="col-sm-12">
												<div class="text-right">
													<!--   <a class="btn btn-warning btn-sm" href="updateTlprodmngproductinfo"></i> Edit</a> -->
													<c:set var="pervalue" value="${fn:split(param.pid,'$') }" />
													<c:if test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
														<a class="btn btn-warning btn-sm"
															href="updateTlprodmngproductinfo"></i> Edit</a>
													</c:if>
												</div>
											</div>

										</div>
									</div>
									<!--row -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->

	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="../../pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%--        <jsp:include page="../../siteFooter.jsp" /> --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->

	<script
		src="<c:url value="/resources/assets/js/jquery-2.2.4.min.js" />"></script>
	<!--  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
	<script src="assets/js/apps.js"></script>
	<script src="assets/plugins/retina/retina.min.js"></script>
	<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>


	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script type="text/javascript" src="assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript" src="assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/buttons.print.min.js"></script>
	<script src="assets/js/buttons.colVis.min.js"></script>
	<script src="assets/js/toastr.min.js"></script>
	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="assets/jAlert/jAlert.min.js"></script>
	<script src="assets/jAlert/jAlert-functions.min.js"></script>

	<script type="text/javascript">
         
    
         
         $(document).ready(function() {
     		
			 var SaveMessage="<c:out value="${SaveMessage}"/>";
	 		       // show when the button is clicked
	 		       if (SaveMessage.length) {
	 		    	   successAlert('Success!',SaveMessage);
	 		       }
	 		      
	 		  var deletemessage="<c:out value="${deletemessage}"/>";
	 		       // show when the button is clicked
	 		       if (deletemessage.length) {
	 		    	   successAlert('Success!',deletemessage);
	 		       }
	 		       
	 		  var updatemessage="<c:out value="${updatemessage}"/>";
	 		       // show when the button is clicked
	 		       if (updatemessage.length) {
	 		    	  successAlert('Success!',updatemessage);
	 		       }
	 		       
	 		     });    
         
         
         </script>


	<script type="text/javascript">
         function submitTLPdfDownload(){
        	 $('#form1').attr('action', 'DownloadPdfFile'); 
             $('#form1').submit();
         	 return true;
         
     }
	
         $('#tlPDSTable').DataTable({
			 "order": [],
			dom : 'Bfrtip',
			title : ' Agent List',
		

			buttons : [ 'csv', 'print', {
				extend : 'excelHtml5',
				title : 'Sales_Leads_'+today
			}, {
				extend : 'pdfHtml5',
				title :  'Sales_Leads_'+today
			} ]
		});
         
  
         </script>
</body>
</html>