<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!---------------------------------------------------- BEGIN Site Header----------------------------------------------------------->
<jsp:include page="../../siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>

<!--  <!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link rel="stylesheet" href="assets/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/menu.css">
<link rel="stylesheet" href="assets/css/toastr.min.css">

<link rel="stylesheet" href="assets/jAlert/jAlert.css">


<style type="text/css">
.inputError {
	border: 1px solid red;
}

.pad15 {
	padding: 0 15px !important;
}

.no-pad-right {
	padding-right: 0 !important;
}

.form-horizontal .form-group {
	margin-bottom: 10px;
}
</style>

<!--   PLUGINS CSS -->
<!--     <link href="plugins/owl-carousel/owl.carousel.min.css" rel="stylesheet">
        <link href="plugins/owl-carousel/owl.theme.min.css" rel="stylesheet">
        <link href="plugins/owl-carousel/owl.transitions.min.css" rel="stylesheet"> -->

<!--  MAIN CSS (REQUIRED ALL PAGE) -->
<!-- <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="plugins/footable-3/css/footable.bootstrap.css" rel="stylesheet">
        <link href="css/owl.css" rel="stylesheet">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap-select.css" rel="stylesheet">
        <link href="css/menu.css" rel="stylesheet">
        <link href="plugins/datepicker/datepicker.min.css" rel="stylesheet"> -->
<!--<link href="css/style.css" rel="stylesheet">-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!-- header -->
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="../../pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>


		<!-- end header -->
		<!-- header second-->

		<!-- header second-->
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="/menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>


					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Product Management | Life Secure</h4>
															&nbsp; (Admin are able to manage product including rates,
															discount and payment option)
														</div>
													</div>

												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="title">
																<div class="sub">
																	<h4>Product Rates</h4>
																</div>
															</div>
															<div>
																<form:form modelAttribute="DspTblRangeRules"
																	action="updateDoneTLProductRatesApproval"
																	id="updateDoneTLProductRatesForm"
																	name="updateDoneTLProductRatesForm" method="post">
																	<%-- <form:form modelAttribute="DspTblRangeRules" action="updateDoneTLProductRates"  id="updateDoneTLProductRatesForm" name="updateDoneTLProductRatesForm" method="post"> --%>
																	<div class="form-horizontal info-meor">
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Direct
																					Discount (Online) (%)</label>
																				<div class="col-sm-6">
																					<c:forEach items="${discountValueList}"
																						var="elementDisc">
																						<input type="text" name="discountVal"
																							class="form-control" readonly
																							value=<c:out value="${elementDisc.paramCode}" />>
																					</c:forEach>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">GST
																					(%)</label>
																				<div class="col-sm-6">
																					<c:forEach items="${gstValueList}" var="elementGst">
																						<input type="text" name="gstVal"
																							class="form-control" readonly
																							value=<c:out value="${elementGst.paramCode}" />>
																					</c:forEach>

																				</div>
																			</div>

																			<div class="form-group">
																				<label class="col-sm-6 control-label">Stamp
																					Duty (RM)</label>
																				<div class="col-sm-6">
																					<c:forEach items="${stampDutyList}"
																						var="elementStampDuty">
																						<input type="text" name="stampDutyVal"
																							class="form-control" readonly
																							value=<c:out value="${elementStampDuty.paramCode}" />>
																					</c:forEach>

																				</div>
																			</div>

																			<div class="form-group">
																				<label class="col-sm-6 control-label">Minimum
																					Age</label>
																				<div class="col-sm-6">
																					<c:forEach items="${dspTblRangeRulesList}"
																						var="elementminAge">
																						<input type="text" name="minAgeVal"
																							class="form-control"
																							value=<c:out value="${elementminAge.ruleCodeMinValue}" />>
																					</c:forEach>
																				</div>
																			</div>
																		</div>
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Minimum
																					Benefit Amount (RM)</label>
																				<div class="col-sm-6">
																					<c:forEach items="${minBenAmountList}"
																						var="elementmin">
																						<input type="text" name="minAmntVal"
																							class="form-control"
																							value=<c:out value="${elementmin.paramCode}" />>
																					</c:forEach>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Maximum
																					Benefit Amount (RM)</label>
																				<div class="col-sm-6">
																					<c:forEach items="${maxBenAmountList}"
																						var="elementmax">
																						<input type="text" name="maxAmntVal"
																							class="form-control"
																							value=<c:out value="${elementmax.paramCode}" />>
																					</c:forEach>

																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Total
																					Sum Assured at Risk (TSAR) (RM)</label>
																				<div class="col-sm-6">
																					<c:forEach items="${tsarValueList}"
																						var="elementTsar">
																						<input type="text" name="tsarVal" readonly
																							class="form-control"
																							value=<c:out value="${elementTsar.paramCode}" />>
																					</c:forEach>

																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Maximum
																					Age</label>
																				<div class="col-sm-6">
																					<c:forEach items="${dspTblRangeRulesList}"
																						var="elementmaxAge">
																						<input type="text" name="maxAgeVal"
																							class="form-control"
																							value=<c:out value="${elementmaxAge.ruleCodeMaxValue}" />>

																					</c:forEach>

																				</div>
																			</div>
																		</div>
																	</div>
																</form:form>
															</div>
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>

												<div class="col-sm-12">
													<div class="text-right">
														<a class="btn btn-default btn-sm" role="button"
															href="tlprodmngproductrates"><i
															class="fa fa-chevron-left"></i> Back</a> <input
															class="btn btn-warning btn-sm"
															onClick="submitTLProductRates();" type="submit"
															value="Update" />
														<!-- <a class="btn btn-warning btn-sm"  id="updateTLProductRates" onClick="submitTLProductRates();"><i class="fa fa-edit"></i> Update</a> -->
													</div>
												</div>

											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.container -->

				</div>
				<!-- /.page-content -->
				<!-- BEGIN FOOTER -->
				<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
				<jsp:include page="../../pageFooter.jsp" />
				<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->

				<!-- END FOOTER -->
			</div>
			<!-- /.wrapper -->
			<!-- END PAGE CONTENT -->



			<!-- BEGIN BACK TO TOP BUTTON -->

			<div id="back-top">
				<a href="#top"><i class="fa fa-chevron-up"></i></a>
			</div>

			<!-- END BACK TO TOP -->


			<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

			<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->

			<script
				src="<c:url value="/resources/assets/js/jquery-2.2.4.min.js" />"></script>
			<!--  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
			<script src="assets/js/apps.js"></script>
			<script src="assets/plugins/retina/retina.min.js"></script>
			<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
			<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
			<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>


			<!-- PLUGINS -->
			<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
			<script src="assets/plugins/prettify/prettify.js"></script>
			<script
				src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

			<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
			<script src="assets/plugins/icheck/icheck.min.js"></script>
			<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
			<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
			<script src="assets/plugins/mask/jquery.mask.min.js"></script>
			<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
			<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
			<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
			<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
			<script
				src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
			<script src="assets/plugins/summernote/summernote.min.js"></script>
			<script src="assets/plugins/markdown/markdown.js"></script>
			<script src="assets/plugins/markdown/to-markdown.js"></script>
			<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
			<script src="assets/plugins/slider/bootstrap-slider.js"></script>
			<script src="assets/plugins/toastr/toastr.js"></script>
			<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
			<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
			<script
				src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
			<script
				src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>
			<script type="text/javascript" src="assets/js/shieldui-all.min.js"></script>
			<script type="text/javascript" src="assets/js/jszip.min.js"></script>
			<!--  Table Export -->
			<!-- KNOB JS -->
			<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
			<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
			<script src="assets/plugins/jquery-knob/knob.js"></script>
			<script src="assets/js/jquery.dataTables.min.js"></script>
			<script src="assets/js/dataTables.bootstrap.min.js"></script>
			<script src="assets/js/dataTables.buttons.min.js"></script>
			<script src="assets/js/buttons.bootstrap.min.js"></script>
			<script src="assets/js/jszip.min.js"></script>
			<script src="assets/js/pdfmake.min.js"></script>
			<script src="assets/js/vfs_fonts.js"></script>
			<script src="assets/js/buttons.html5.min.js"></script>
			<script src="assets/js/buttons.print.min.js"></script>
			<script src="assets/js/buttons.colVis.min.js"></script>
			<script src="assets/js/toastr.min.js"></script>
			<script type="text/javascript">
        function submitTLProductRates(){
        	
        
     
            	$('#updateDoneTLProductRatesForm').submit();   
            
        }
   	
        </script>
			<script
				src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</body>
</html>