<%@ page import="java.util.List"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="icon" href="resources/assets/img/maybankLogoICO.ico"
	type="image/x-icon" />

<link rel="stylesheet" type="text/css"
	href="assets/js/jquery.dataTables.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/buttons.dataTables.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/dataTables.bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/buttons.bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="assets/css/toastr.min.css" />
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="/siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}

#upload-file-selector {
	display: none;
}
</style>

<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}

.modal-dialog {
	width: 600px !important;
}
</style>

<!-- OI CSS (REQUIRED Travel Ezy)-->
<link rel="stylesheet" href="assets/css/oiCSS.css" />

<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet"
	href="assets/plugins/datepicker/datepicker.min.css" />
<!-- PLUGINS CSS -->
<link rel="stylesheet"
	href="assets/plugins/owl-carousel/owl.carousel.min.css" />
<link rel="stylesheet"
	href="assets/plugins/owl-carousel/owl.theme.min.css" />
<link rel="stylesheet"
	href="assets/plugins/owl-carousel/owl.transitions.min.css" />

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link rel="stylesheet"
	href="assets/plugins/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet" href="assets/css/owl.css" />
<link rel="stylesheet" href="assets/css/bootstrap.css" />
<link rel="stylesheet" href="assets/css/bootstrap-select.css" />
<link rel="stylesheet"
	href="assets/plugins/datatable/css/bootstrap.datatable.min.css" />
<link rel="stylesheet"
	href="assets/plugins/datatable/css/dataTables.searchHighlight.css" />
<link rel="stylesheet" href="assets/css/menu.css" />
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="libs/html5shiv.js"></script>
    <script src="libs/respond.min.js"></script>
<![endif]-->

<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

form#agentE, form#agentD {
	display: inline-block;
}

form#agentE input[type=submit], form#agentD input[type=submit] {
	padding: 5px;
}
</style>
</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
    -->

	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="/pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="/menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>

							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>TravelEzy | Product Information Management</h4>
															<span>(Admin are able to manage product including
																rates, discount and payment option)</span>
														</div>
													</div>

													<!-- Start Form -->
													<!--   <input type="hidden" name="action" value="listAll" /> -->

													<div class="col-sm-12">
														<!-- Begin  table -->
														<div class="content-inner">
															<div class="the-box full no-border">
																<form:form action="updateTravelEzyProduct" method="post"
																	name="addProduct" id="addProduct">
																	<div class="title">
																		<div class="sub">
																			<h4>Product Information</h4>
																			<div id="ErrmsgDiv"
																				class="errorMsg leftPadding errorMsgDisplayF">
																				<i class="fa fa-exclamation-triangle"></i> <span
																					id="ErrmsgAll">${Errmsg}</span>
																			</div>
																			<div id="SuccessMsgDiv"
																				class="successMsg leftPadding errorMsgDisplayF">
																				<i class="fa fa-exclamation-circle"></i> <span
																					id="SuccessMsgAll">${SuccessMsg}</span>
																			</div>
																		</div>
																	</div>
																	<div>
																		<div class="form-horizontal info-meor">
																			<div class="col-sm-12">
																				<div class="form-group">
																					<label class="col-sm-3 control-label">Company</label>
																					<div class="col-sm-8">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> <select
																								name="company" id="company"
																								onChange="submitCompany()">
																								<c:forEach items="${companyList}"
																									var="companyList">
																									<option value="${companyList.getName()}"
																										<c:if test="${company==companyList.getName().toString().toLowerCase()}">selected</c:if>>${companyList.getDescription()}
																									</option>
																								</c:forEach>
																							</select>
																						</p>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label">Discount
																						(%)</label>
																					<div class="col-sm-8">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> <input step=".01"
																								type="number" name="discount" id="discount"
																								value="<c:out value = "${discount}"/>" /> <span
																								class="error leftPadding"
																								id="errmsgStaffDiscount" style="display: none;">Discount
																								cannot be empty! </span>
																						</p>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label">Stamp
																						Duty (RM)</label>
																					<div class="col-sm-8">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> <input step=".01"
																								type="number" name="stampDuty" id="stampDuty"
																								value="<c:out value = "${stampDuty}"/>" /> <span
																								class="error leftPadding" id="errmsgStamp"
																								style="display: none;">Stamp duty cannot
																								be empty! </span>
																						</p>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label">POI</label>
																					<div class="col-sm-8">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> <input
																								type="number" name="poi" id="poi"
																								value="<c:out value = "${poi}"/>" /> <span
																								class="error leftPadding" id="errmsgPOI"
																								style="display: none;">POI cannot be
																								empty! </span>
																						</p>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label">GST
																						(%):</label>
																					<div class="col-sm-8">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> <input
																								type="number" name="gst" id="gst" step=".01"
																								value="<c:out value = "${gst}"/>" /> <span
																								class="error leftPadding" id="errmsgGst"
																								style="display: none;">GST cannot be
																								empty! </span>
																						</p>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label">GST
																						Effective Date :</label>
																					<div class="col-sm-8">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> <input
																								type="text" id="gstEffDate"
																								placeholder="DD/MM/YYYY" required
																								value="${gstEffDate}" readonly name="gstEffDate">
																						</p>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label">SST
																						(%):</label>
																					<div class="col-sm-8">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> <input
																								type="number" name="sst" id="sst" step=".01"
																								value="<c:out value = "${sst}"/>" /> <span
																								class="error leftPadding" id="errmsgSst"
																								style="display: none;">SST cannot be
																								empty! </span>
																						</p>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label">SST
																						Effective Date :</label>
																					<div class="col-sm-8">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> <input
																								type="text" id="sstEffDate"
																								placeholder="DD/MM/YYYY" required
																								value="${sstEffDate}" readonly name="sstEffDate">
																						</p>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label">Premium
																						Rate (RM) : <span class="dotSpace"></span>Direct
																					</label>
																					<div class="col-sm-8">
																						<p class="form-control-static minusPaddingTop">
																							<span class="dotSpace">:</span> <input
																								type="number" name="premiumRate"
																								id="premiumRate"
																								value="<c:out value = "${premiumRate}"/>" /> <span
																								class="error leftPadding" id="errmsgPremiumRate"
																								style="display: none;">Premium Rate
																								cannot be empty! </span>
																						</p>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label"></label>
																					<div class="col-sm-8">
																						<p class="form-control-static">
																							<span style="color: transparent;">:</span> <input
																								type="hidden" name="productCode"
																								id="productCode"
																								value="<c:out value = "${productCode}"/>" /> <input
																								type="hidden" name="version" id="version"
																								value="<c:out value = "${version}"/>" /> <input
																								type="hidden" name="exist" id="exist"
																								value="<c:out value = "${exist}"/>" /> <a
																								class="btn btn-warning btn-sm" id="btnAdd"
																								onClick="validateForm();"><i
																								class="fa fa-save"></i> Save</a>
																						</p>
																					</div>
																				</div>
																			</div>
																			<div class="gap gap-mini"></div>
																		</div>
																	</div>
																</form:form>
																<form:form id="loadCompany" name="loadCompany"
																	action="tezCompanyIndexChange">
																	<input type="hidden" id="selectedCompany"
																		name="selectedCompany">
																</form:form>
															</div>
															<!-- /.the-box -->
															<!-- End warning color table -->
														</div>
													</div>
													<div class="gap gap-mini"></div>
												</div>
											</div>
											<!--row -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.container -->

			</div>
			<!-- /.page-content -->
			<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
			<jsp:include page="/pageFooter.jsp" />
			<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
		</div>
		<!-- /.wrapper -->
		<!-- END PAGE CONTENT -->

		<!-- BEGIN BACK TO TOP BUTTON -->
		<!--
	        <div id="back-top">
	            <a href="#top"><i class="fa fa-chevron-up"></i></a>
	        </div>
		-->
		<!-- END BACK TO TOP -->
	</div>

	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%--    <jsp:include page="siteFooter.jsp" />   --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->

	<!-- JAVA Script Goes Here -->
	<script
		src="<c:url value="/resources/assets/js/jquery-2.2.4.min.js" />"></script>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="assets/js/apps.js"></script>
	<script src="assets/plugins/retina/retina.min.js"></script>
	<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>

	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script src="assets/js/shieldui-all.min.js" type="text/javascript"></script>
	<script src="assets/js/jszip.min.js" type="text/javascript"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
        <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
    <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/buttons.print.min.js"></script>
	<script src="assets/js/buttons.colVis.min.js"></script>
	<script src="assets/js/toastr.min.js"></script>
	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

	<script type="text/javascript">

	$(document).ready(function() {
		$("#gstEffDate").datepicker({
			format : 'dd/mm/yyyy',
			minDate: +1,
			autoclose : true,
		}).on('changeDate', function(ev) {
			$(this).datepicker('hide');
		});
		$("#sstEffDate").datepicker({
			format : 'dd/mm/yyyy',
			minDate: +1,
			autoclose : true,
		}).on('changeDate', function(ev) {
			$(this).datepicker('hide');
		});
	});
		
		
		function validateForm() {
			var Company = document.getElementById('company');
			var PremiumRate = document.getElementById('premiumRate');
			var POI = document.getElementById('poi');
			var stampDuty = document.getElementById('stampDuty');
			var Disc = document.getElementById('discount');
			var Gst = document.getElementById('gst');
			var Sst = document.getElementById('sst');
			var tGstDt = document.getElementById('gstEffDate');
			var tSstDt = document.getElementById('sstEffDate');

			if (Company.value != "" && PremiumRate.value != "" && POI.value != ""
					&& stampDuty.value != "" && Disc.value != "" && Gst.value != ""
					&& Sst.value != "" && tGstDt.value != "" && tSstDt.value != "") {
				if (tGstDt.value != tSstDt.value ){
				$('#addProduct').submit();
				return true;
				}
				else {
					alert("GST Effective Date cannot be the same as SST Effective Date!");
					return false;
				}
			} else {
				if(!$.trim($('#premiumRate').val()).length) {
					$('#errmsgPremiumRate').show();
				} else {
					$('#errmsgPremiumRate').hide();
				}
				if(!$.trim($('#POI').val()).length) {
					$('#errmsgPOI').show();
				} else {
					$('#errmsgPOI').hide();
				}
				if(!$.trim($('#discount').val()).length) {
					$('#errmsgStaffDiscount').show();
				} else {
					$('#errmsgStaffDiscount').hide();
				}
				if(!$.trim($('#stampDuty').val()).length) {
					$('#errmsgStamp').show();
				} else {
					$('#errmsgStamp').hide();
				}
				if(!$.trim($('#gst').val()).length) {
					$('#errmsgGst').show();
				} else {
					$('#errmsgGst').hide();
				}
				if(!$.trim($('#sst').val()).length) {
					$('#errmsgSst').show();
				} else {
					$('#errmsgSst').hide();
				}
				alert("Please fill in the mandatory fields!");
				return false;
			}
		}

		function submitCompany() {

			var tCompany = document.getElementById('company');
			var tSelectedCompany = document.getElementById('selectedCompany');
			tSelectedCompany.value = tCompany.value;
			$('#loadCompany').submit();
		}
	</script>

	<script type="text/javascript">
		$(document).ready(
				function() {
					// start checking for empty field
					$("#premiumRate").blur(function(){
						if(!$.trim($('#premiumRate').val()).length || !$.trim($('#premiumRate').val()).length) {
							$('#errmsgPremiumRate').show();
						} else {
							$('#errmsgPremiumRate').hide();
						}
					});
					
					$("#POI").blur(function(){
						if(!$.trim($('#poi').val()).length) {
							$('#errmsgPOI').show();
						} else {
							$('#errmsgPOI').hide();
						}
					});
					
					$("#discount").blur(function(){
						if(!$.trim($('#discount').val()).length) {
							$('#errmsgStaffDiscount').show();
						} else {
							$('#errmsgStaffDiscount').hide();
						}
					});
					
					$("#stampDuty").blur(function(){
						if(!$.trim($('#stampDuty').val()).length) {
							$('#errmsgStamp').show();
						} else {
							$('#errmsgStamp').hide();
						}
					});
					
					$("#gst").blur(function(){
						if(!$.trim($('#gst').val()).length) {
							$('#errmsgGst').show();
						} else {
							$('#errmsgGst').hide();
						}
					});
					
					$("#sst").blur(function(){
						if(!$.trim($('#sst').val()).length) {
							$('#errmsgSst').show();
						} else {
							$('#errmsgSst').hide();
						}
					});
					// end checking for empty field
					
					$("#ErrmsgAll").each(function() {
						if ($(this).text() == "") {
							$("#ErrmsgDiv").hide(); // this references the current element in the iteration
							$("#ErrmsgDiv").addClass("errorMsgDisplayF")
						} else
							$("#ErrmsgDiv").addClass("errorMsgDisplayT")

					});

					$("#ErrmsgDiv").click(function() {
						$("#ErrmsgDiv").hide(1000);
					});

					$("#SuccessMsgAll").each(function() {
						if ($(this).text() == "") {
							$("#SuccessMsgDiv").hide(); // this references the current element in the iteration
							$("#SuccessMsgDiv").addClass("errorMsgDisplayF")
						} else
							$("#SuccessMsgDiv").addClass("errorMsgDisplayT")
					});

					$("#SuccessMsgDiv").click(function() {
						$("#SuccessMsgDiv").hide(1000);
					});

					$(".addproduct").click(
							function() {
								// Holds the product ID of the clicked element
								var productId = $(this).attr('class').replace(
										'addproduct ', '');

								addToCart(productId);
							});
				});
	</script>

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
</body>
</html>