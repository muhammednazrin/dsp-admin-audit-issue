<%@ page import="java.util.List"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<%@ page import="com.spring.VO.CommonNVIC"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/js/jquery.dataTables.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/buttons.dataTables.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/dataTables.bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/buttons.bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link rel="stylesheet" href="assets/css/toastr.min.css">
<link rel="stylesheet" href="assets/jAlert/jAlert.css">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="/siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}

#upload-file-selector {
	display: none;
}
</style>


<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}
</style>
<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet"
	href="assets/plugins/datepicker/datepicker.min.css">
<!-- PLUGINS CSS -->
<link href="assets/plugins/owl-carousel/owl.carousel.min.css"
	rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.theme.min.css"
	rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.transitions.min.css"
	rel="stylesheet">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

form#agentE, form#agentD {
	display: inline-block;
}

form#agentE input[type=submit], form#agentD input[type=submit] {
	padding: 5px;
}
</style>
</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="/pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="/menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Product Management | Motor Insurance</h4>
															&nbsp; (Admin are able to manage product including rates,
															discount and payment option)
														</div>
													</div>


													<!-- Start Form -->
													<form:form name="nvicList" action="searchNvicList"
														id="nvicList" method="POST">

														<!--   <input type="hidden" name="action" value="listAll" /> -->

														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Make</label>
																			<div class="col-sm-9">
																				<select class="form-control" id="make" name="make"
																					onchange="MakeOnChange(this);">
																					<option selected="" value="">-Select Make-</option>
																					<c:forEach items="${makeList}" var="element">
																						<option
																							value="<c:out value="${element.makeCode}"/>"
																							${element.makeCode == modelid ? 'selected' : ''}>
																							<c:out value="${element.make}" />
																						</option>
																					</c:forEach>

																					<!--   <option value=" Select Make"> Select Make</option>
                                                                              <option value="AUDI">AUDI</option>
                                                                              <option value="MERCEDES-BENZ">MERCEDES-BENZ</option>
                                                                              <option value="PROTON">PROTON</option>
                                                                              <option value="SUZUKI">SUZUKI</option>
                                                                              <option value="HONDA">HONDA</option>-->
																				</select>
																			</div>
																		</div>

																		<div class="form-group">
																			<label class="col-sm-3 control-label">Model</label>
																			<div class="col-sm-9">
																				<select class="form-control" id="model" name="model">
																					<option selected="" value="">-Select
																						Model-</option>
																				</select>
																			</div>
																		</div>

																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-horizontal">
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Year</label>
																			<div class="col-sm-9">
																				<select class="form-control" name="year">
																					<!--  <option selected="" value="-View All-">-View All-</option>-->
																					<c:forEach items="${yearList}" var="element">
																						<option
																							<c:if test="${status.value eq element.year}" >selected</c:if>
																							value="<c:out value="${element.year}"/>">
																							<c:out value="${element.year}" />
																						</option>
																					</c:forEach>

																				</select>
																			</div>
																		</div>

																	</div>
																</div>
																<div class="col-sm-12 text-right">
																	<button type="submit" class="btn btn-default">Search</button>
																</div>
															</div>
														</div>
													</form:form>
													<!-- END FORM -->
												</div>
												<c:if test="${not empty norecords}">
													<div id="noRecordFound"
														class="alert alert-danger fade in alert-dismissable">
														<!--   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X—</button> -->
														<strong><i class="fa fa-exclamation"></i> No
															record found</strong>
													</div>
												</c:if>
												<c:if test="${not empty mnvicList}">
													<div class="col-sm-12">
														<!-- Begin  table -->
														<div class="content-inner">
															<div class="the-box full no-border">
																<div class="title">
																	<div class="sub">
																		<h4>NVIC Listing</h4>
																	</div>
																</div>
																<div>
																	<div class="form-horizontal form-group-sm">



																		<!--  <button id="exportButton" class="btn btn-lg btn-danger clearfix"><span class="fa fa-file-excel-o"></span> Export to Excel</button>-->
																		<div class="table-responsive" id="MyTable">

																			<table
																				class="table table-striped table-warning table-hover"
																				id="tbl-nvic-list">
																				<thead>
																					<tr>
																						<th>No</th>
																						<th>NVIC</th>
																						<th>NVIC Group</th>
																						<th>Make Code</th>
																						<th>Model Code</th>
																						<th>Make</th>
																						<th>Family</th>
																						<th>MM Code</th>
																						<th>Seat</th>
																						<th class="text-center">Actions</th>
																					</tr>
																				</thead>
																				<tbody>
																					<c:forEach items="${mnvicList}" var="element"
																						varStatus="theCount">

																						<tr>
																							<td><c:out value="${theCount.count}" /></td>
																							<td><c:out value="${element.nvicCode}" /></td>
																							<td><c:out value="${element.nvicGroup}" /></td>
																							<td><c:out value="${element.makeCode}" /></td>
																							<td><c:out value="${element.modelCode}" /></td>
																							<td><c:out value="${element.make}" /></td>
																							<td></td>
																							<td><c:out value="${element.mmCode}" /></td>
																							<td><c:out value="${element.seat}" /></td>
																							<td class="text-center"><form:form
																									action="updateNvicList" method="POST"
																									name="nvicE" id="nvicE">
																									<!--  <input type="hidden" name="action" value="record" /> -->
																									<input type="hidden" name="id"
																										value="<c:out value="${element.id}"/>" />
																									<input class="btn btn-warning btn-sm btn-space"
																										type="submit" name="submit" value="Edit">
																								</form:form> <form:form action="deleteNvicDone"
																									method="POST" name="nvicD" id="nvicD">
																									<input type="hidden" name="id"
																										value="<c:out value="${element.id}"/>" />
																									<input class="btn btn-warning btn-sm btn-space"
																										type="submit" name="submit" value="Delete">
																								</form:form></td>
																							<!--  <div id="editAgentInfo"  style="display:block;"><button class="btn btn-warning btn-sm">Edit <i class="fa fa-edit" aria-hidden="true"></i></button></div>-->
																						</tr>
																					</c:forEach>

																				</tbody>
																			</table>
																		</div>
																	</div>
																</div>
															</div>
															<!-- /.the-box -->
															<!-- End warning color table -->
														</div>
													</div>


												</c:if>
												<div class="gap gap-mini"></div>
												<div class="col-sm-14">
													<div class="text-right">

														<form:form action="uploadMICSV" method="post"
															enctype="multipart/form-data" id="registration">
															<a class="btn btn-warning btn-sm" href="AddNvic"><i
																class="fa fa-plus"></i> Add</a>
															<!-- <div class="col-xs-10 text-right">
                                                                         <div class="col-sm-6"><input type="file"  name ="CSV" id="upload-file-selector" accept=".csv">test</div>
                                                                         <i class="fa fa-edit"><input type="submit" value="Upload" name="submit" class="btn btn-warning btn-sm" for="upload-file-selector"></i>
                                                                               
                                                                          
                                                                        </div> -->
															<span id="fileselector"> <label
																class="btn btn-warning btn-sm"
																for="upload-file-selector"> <input
																	id="upload-file-selector" type="file" name="CSV">
																	<i class="fa fa-edit"></i> Choose file
															</label>
															</span>
															<input type="submit" value="Submit" name="submit"
																class="btn btn-warning btn-sm">
														</form:form>

													</div>
												</div>

											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="/pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%--    <jsp:include page="siteFooter.jsp" />   --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->


	<!-- JAVA Script Goes Here -->
	<script
		src="${pageContext.request.contextPath}/assets/js/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/js/jquery-2.2.4.min.js" />"></script>
	<!--  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
	<script src="assets/js/apps.js"></script>
	<script src="assets/plugins/retina/retina.min.js"></script>
	<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>

	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script type="text/javascript" src="assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript" src="assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/buttons.print.min.js"></script>
	<script src="assets/js/buttons.colVis.min.js"></script>
	<script src="assets/js/toastr.min.js"></script>
	<script src="assets/jAlert/jAlert.min.js"></script>
	<script src="assets/jAlert/jAlert-functions.min.js"></script>
	<script type="text/javascript">

         </script>

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->

	<script>
		$(document).ready(function() {

			$('#tbl-nvic-list').DataTable({
				 "order": [],
				dom : 'Bfrtip',
				title : 'MI NVI List',
			

				buttons : [ 'copy', 'csv', 'print', {
					extend : 'excelHtml5',
					title : ' MI NVI List'
				}, {
					extend : 'pdfHtml5',
					title :  'MI NVI List Report'
				} ]
			});
			
			// $('#date1').datepicker();
			 $('#date1').datepicker({
				    format: 'dd/mm/yyyy',
			         }).on('changeDate', function(e){
	 			    $(this).datepicker('hide');
	 			
				});
			 $('#date2').datepicker({
				    format: 'dd/mm/yyyy',
			        }).on('changeDate', function(e){
	 			    $(this).datepicker('hide');
	 			    });
			
			 var nvicAddedMessage="<c:out value="${nvicAddedMessage}"/>";
	 		       // show when the button is clicked
	 		       if (nvicAddedMessage.length) {
	 		    	  successAlert('Success!',nvicAddedMessage);
	 		       }
	 		      
	 		  var nvicDeletedMessage="<c:out value="${nvicDeletedMessage}"/>";
	 		       // show when the button is clicked
	 		       if (nvicDeletedMessage.length) {
	 		    	  successAlert('Success!',nvicDeletedMessage);
	 		       }
	 		       
	 		  var nvicUpdatedMessage="<c:out value="${nvicUpdatedMessage}"/>";
	 		       // show when the button is clicked
	 		       if (nvicUpdatedMessage.length) {
	 		    	 successAlert('Success!',nvicUpdatedMessage);
	 		       }
	 		    var nvicExistMessage="<c:out value="${nvicExistMessage}"/>";
	 		       // show when the button is clicked
	 		       if (nvicExistMessage.length) {
	 		    	  successAlert('Success!',nvicExistMessage);
	 		       }    
	 		       
	 		  
			 
			 
		
		    });
		
		function MakeOnChange(makecode){
		//alert(makecode.value);
		   
		   // window.location.href="${pageContext.request.contextPath}/NvicList?mid="+makecode.value;
		   // document.getElementById('make').selectedIndex=makecode.value;
    
	    var txt=makecode.value;
	    //alert("makeCode" +txt);
	    $.ajax({
        url: 'getModelList',
        type: 'GET',
        data: ({mid : txt}),
        success: function(data) {
       // alert(data);
        	var obj = jQuery.parseJSON( data );
        	var listItems= "";
        	for (var i = 0; i <obj.length; i++){
        	      listItems+= "<option value='" + obj[i].modelCode + "'>" + obj[i].model + "</option>";
        	    }
        	    //alert(listItems);
         $('#model').html(listItems);
          
          if( obj.length == 0 ){
	        	 $('#model').html("<option value=''>-Select Model-</option>");
	        	}
        }
	    
      });
	    }
	
	</script>





</body>
</html>