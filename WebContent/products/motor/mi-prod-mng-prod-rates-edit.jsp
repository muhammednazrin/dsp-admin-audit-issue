<%@ page import="java.util.List"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<%@ page import="com.spring.VO.CommonNVIC"%>
<%@ page import="com.spring.VO.DspTblRangeRules"%>
<%@ page import="com.spring.VO.dspMiTblParam"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="/siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<link rel="stylesheet" href="assets/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/menu.css">
<link rel="stylesheet" href="assets/jAlert/jAlert.css">

<style>
.col-sm-12.buttons {
	text-align: right;
}

.col-sm-12.buttons>div {
	display: inline-block;
}

.pad15 {
	padding-left: 15px !important;
	padding-right: 15px !important;
}
</style>
</head>
<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="/pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="/menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>

					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">

									<%-- <form:form action="updateDoneMIProductRates"  id="updateProdrateForm" name="updateProdrateForm" method="post"> --%>
									<form:form action="miProductRateSave" id="updateProdrateForm"
										name="updateProdrateForm" method="post">


										<div class="col-sm-12">
											<div class="content-inner">



												<div class="row">
													<div class="col-sm-12">
														<div class="title">
															<div class="sub">
																<h4>Product Management | Motor Insurance</h4>

																&nbsp;(Admin are able to manage product including rates,
																discount and payment option)
															</div>
														</div>
														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-12">
																	<div class="title">
																		<div class="sub">
																			<h4>Product Rates</h4>
																		</div>
																	</div>
																</div>
																<div class="gap gap-mini"></div>
																<div class="content-inner">
																	<div class="form-horizontal">
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Sum
																					Coverred Limit (Minimum) (RM)</label>
																				<div class="col-sm-6">
																					<c:forEach items="${suminsuredList}"
																						var="elementminsumins">
																						<input type="text" class="form-control"
																							value="<c:out value="${elementminsumins.ruleCodeMinValue}" />"
																							id="minSumcovered" name="minSumcovered">
																					</c:forEach>
																					<c:if test="${empty suminsuredList}">
																						<input type="text" class="form-control" value=""
																							id="minSumcovered" name="minSumcovered">
																					</c:if>
																					<span id="msg_minsumcover" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Sum Coverred Limit (Minimum)</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Sum
																					Coverred Limit (Maximum) (RM)</label>
																				<div class="col-sm-6">
																					<c:forEach items="${suminsuredList}"
																						var="elementmaxsumins">
																						<input type="text" class="form-control"
																							value="<c:out value="${elementmaxsumins.ruleCodeMaxValue}" />"
																							id="maxSumcovered" name="maxSumcovered">
																					</c:forEach>
																					<c:if test="${empty suminsuredList}">
																						<input type="text" class="form-control" value=""
																							id="maxSumcovered" name="maxSumcovered">

																					</c:if>
																					<span id="msg_maxsumcover" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Sum Coverred Limit (Maximum)</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Direct
																					Discount (Online) (%)</label>
																				<div class="col-sm-6">
																					<c:forEach items="${discountValueList}"
																						var="elementdiscount">
																						<input type="text" class="form-control"
																							value="<c:out value="${elementdiscount.description}" />"
																							id="discount" name="discount">
																					</c:forEach>
																					<c:if test="${empty discountValueList}">
																						<input type="text" class="form-control" value=""
																							id="discount" name="discount">
																					</c:if>
																					<span id="msg_discount" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Direct Discount</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">GST
																					(%)</label>
																				<div class="col-sm-6">
																					<c:forEach items="${gstValueList}" var="elementgst">
																						<input type="text" class="form-control"
																							value="<c:out value="${elementgst.description}" />"
																							id="gst" name="gst">
																					</c:forEach>
																					<c:if test="${empty gstValueList}">
																						<input type="text" class="form-control" value=""
																							id="gst" name="gst">
																					</c:if>
																					<span id="msg_gst" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your GST</span>
																				</div>
																			</div>

																			<div class="form-group">
																				<label class="col-sm-6 control-label">SST
																					(%)</label>
																				<div class="col-sm-6">
																					<c:forEach items="${gstValueList}" var="elementgst">
																						<input type="text" class="form-control"
																							value="<c:out value="${motorSstParam.taxPercentage}" />"
																							id="sst" name="sst">
																					</c:forEach>
																					<c:if test="${empty gstValueList}">
																						<input type="text" class="form-control" value=""
																							id="sst" name="sst">
																					</c:if>
																					<span id="msg_gst" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your SST</span>
																				</div>
																			</div>

																			<div class="form-group">
																				<label class="col-sm-6 control-label">SST
																					Effective Date</label>
																				<div class="col-sm-6">

																					<input type="text"
																						class="form-control effectiveDate"
																						value="<fmt:formatDate pattern = "dd/MM/yyyy" value = "${motorSstParam.effectiveDate}" />"
																						id="sst_effective_date" name="sst_effective_date">
																					<span id="msg_sst" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your SST Effective Date</span>
																				</div>
																			</div>
																		</div>
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Stamp
																					Duty (RM)</label>
																				<div class="col-sm-6">
																					<c:forEach items="${stampDutyList}"
																						var="elementsduty">
																						<input type="text" class="form-control"
																							value="<c:out value="${elementsduty.description}" />"
																							id="stampduty" name="stampduty">
																					</c:forEach>
																					<c:if test="${empty stampDutyList}">
																						<input type="text" class="form-control" value=""
																							id="stampduty" name="stampduty">
																					</c:if>
																					<span id="msg_stampduty" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Stamp Duty</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Maximum
																					No. of Drivers</label>
																				<div class="col-sm-6">
																					<c:forEach items="${driversList}"
																						var="elementsdrno">
																						<input type="text" class="form-control"
																							value="<c:out value="${elementsdrno.description}" />"
																							id="noofdrivers" name="noofdrivers">
																					</c:forEach>
																					<c:if test="${empty driversList}">
																						<input type="text" class="form-control" value=""
																							id="noofdrivers" name="noofdrivers">
																					</c:if>
																					<span id="msg_noofdrivers" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Maximum No. of Drivers</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Amount
																					for every 3rd driver or more (RM)</label>
																				<div class="col-sm-6">
																					<c:forEach items="${driverAmountList}"
																						var="elementsdramt">
																						<input type="text" class="form-control"
																							value="<c:out value="${elementsdramt.description}" />"
																							id="driveramt" name="driveramt">
																					</c:forEach>
																					<c:if test="${empty driverAmountList}">
																						<input type="text" class="form-control" value=""
																							id="driveramt" name="driveramt">
																					</c:if>
																					<span id="msg_driveramt" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Driver Amount</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Minimum
																					age</label>
																				<div class="col-sm-6">
																					<c:forEach items="${driverAgeList}"
																						var="elementminage">
																						<input type="text" class="form-control"
																							value="<c:out value="${elementminage.ruleCodeMinValue}" />"
																							id="mindriverage" name="mindriverage">
																					</c:forEach>
																					<c:if test="${empty driverAgeList}">
																						<input type="text" class="form-control" value=""
																							id="mindriverage" name="mindriverage">
																					</c:if>
																					<span id="msg_minage" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Driver Minimum age</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Maximum
																					age</label>
																				<div class="col-sm-6">
																					<c:forEach items="${driverAgeList}"
																						var="elementmaxage">
																						<input type="text" class="form-control"
																							value="<c:out value="${elementmaxage.ruleCodeMaxValue}" />"
																							id="maxdriverage" name="maxdriverage">
																					</c:forEach>
																					<c:if test="${empty driverAgeList}">
																						<input type="text" class="form-control" value=""
																							id="maxdriverage" name="maxdriverage">
																					</c:if>
																					<span id="msg_maxage" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Driver Maximum age</span>
																				</div>
																			</div>
																		</div>
																		<div class="gap gap-mini"></div>
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-6 control-label text-warning">Car
																					Accident Protection Special</label>
																				<div class="col-sm-6">
																					<p class="form-control-static">Plan (Inclusive
																						of GST)</p>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Direct
																					Discount (Online) (%)</label>
																				<div class="col-sm-6">
																					<c:forEach items="${capsdiscountValueList}"
																						var="elementscapsdis">
																						<input type="text" class="form-control"
																							value="<c:out value="${elementscapsdis.description}" />"
																							id="capsdiscount" name="capsdiscount">
																					</c:forEach>
																					<c:if test="${empty capsdiscountValueList}">
																						<input type="text" class="form-control" value=""
																							id="capsdiscount" name="capsdiscount">
																					</c:if>
																					<span id="msg_capsdiscount" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your CAPS Direct Discount</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">5
																					seater including driver (RM)</label>
																				<div class="col-sm-6">
																					<c:forEach items="${driver5seaterList}"
																						var="elementsseat">
																						<input type="text" class="form-control"
																							value="<c:out value="${elementsseat.description}" />"
																							id="5seater" name="5seater">
																					</c:forEach>
																					<c:if test="${empty driver5seaterList}">
																						<input type="text" class="form-control" value=""
																							id="5seater" name="5seater">
																					</c:if>
																					<span id="msg_5seater" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your 5 seater including driver</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">7
																					seater including driver (RM)</label>
																				<div class="col-sm-6">
																					<c:forEach items="${driver7seaterList}"
																						var="elementsseater">
																						<input type="text" class="form-control"
																							value="<c:out value="${elementsseater.description}" />"
																							id="7seater" name="7seater">
																					</c:forEach>
																					<c:if test="${empty driver7seaterList}">
																						<input type="text" class="form-control" value=""
																							id="7seater" name="7seater">
																					</c:if>
																					<span id="msg_7seater" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your 7 seater including driver</span>
																				</div>
																			</div>

																			<!-- DPPA Stamp Duty -->
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Stamp
																					Duty (RM)</label>
																				<div class="col-sm-6">

																					<input type="text" class="form-control"
																						value="<c:out value="${dppaStampDuty.code}" />"
																						id="dppa_stampduty" name="dppa_stampduty">
																					<span id="msg_stampduty" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Stamp Duty</span>
																				</div>
																			</div>
																			<!-- DPPA SST -->
																			<div class="form-group">
																				<label class="col-sm-6 control-label">SST
																					(%)</label>
																				<div class="col-sm-6">
																					<input type="text" class="form-control"
																						value="<c:out value="${dppaSstParam.taxPercentage}" />"
																						id="dppa_sst" name="dppa_sst"> <span
																						id="msg_gst" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your SST</span>
																				</div>
																			</div>

																			<div class="form-group">
																				<label class="col-sm-6 control-label">SST
																					Effective Date</label>
																				<div class="col-sm-6">

																					<input type="text"
																						class="form-control effectiveDate"
																						value="<fmt:formatDate pattern = "dd/MM/yyyy" value = "${dppaSstParam.effectiveDate}" />"
																						id="dppa_sst_effective_date"
																						name="dppa_sst_effective_date"> <span
																						id="msg_sst" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your SST Effective Date</span>
																				</div>
																			</div>
																		</div>

																		<!--  OTO360 Section -->
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-6 control-label text-warning">OTO
																					360</label>
																			</div>
																			<!-- DPPA Stamp Duty -->
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Stamp
																					Duty (RM)</label>
																				<div class="col-sm-6">
																					<c:forEach items="${stampDutyList}"
																						var="elementsduty">
																						<input type="text" class="form-control"
																							value="<c:out value="${otoStampDutyParam.stampDuty}" />"
																							id="oto_stampduty" name="oto_stampduty">
																					</c:forEach>
																					<c:if test="${empty stampDutyList}">
																						<input type="text" class="form-control" value=""
																							id="stampduty" name="stampduty">
																					</c:if>
																					<span id="msg_stampduty" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Stamp Duty</span>
																				</div>
																			</div>
																			<!-- OTO SST -->
																			<div class="form-group">
																				<label class="col-sm-6 control-label">SST
																					(%)</label>
																				<div class="col-sm-6">
																					<input type="text" class="form-control"
																						value="<c:out value="${otoSstParam.taxPercentage}" />"
																						id="oto_sst" name="oto_sst"> <span
																						id="msg_gst" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your SST</span>
																				</div>
																			</div>

																			<div class="form-group">
																				<label class="col-sm-6 control-label">SST
																					Effective Date</label>
																				<div class="col-sm-6">

																					<input type="text"
																						class="form-control effectiveDate"
																						value="<fmt:formatDate pattern = "dd/MM/yyyy" value = "${otoSstParam.effectiveDate}" />"
																						id="oto_sst_effective_date"
																						name="oto_sst_effective_date"> <span
																						id="msg_sst" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your SST Effective Date</span>
																				</div>
																			</div>
																		</div>
																		<!--End  OTO360 -->
																	</div>
																</div>
															</div>
														</div>
													</div>

												</div>
												<!--content inner -->
											</div>
											<!--col-sm-12 -->

											<div class="col-sm-12 buttons">
												<div>
													<input class="btn btn-warning btn-sm" id="updateprodrates"
														onClick="submitProdRatesForm();" type="submit"
														value="Update" />
												</div>

											</div>
											<!--col-sm-12 -->
										</div>
										<!--row -->
									</form:form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.container -->
	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="/pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->
	</div>


	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%-- <jsp:include page="siteFooter.jsp" /> --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->

	<script src="assets/js/jquery.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.js" type="text/javascript"></script>
	<script src="assets/jAlert/jAlert.min.js"></script>
	<script src="assets/jAlert/jAlert-functions.min.js"></script>
	<script src="assets/plugins/retina/retina.min.js"></script>
	<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>
	<script src="assets/js/apps.js"></script>

	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script type="text/javascript" src="assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript" src="assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/buttons.print.min.js"></script>
	<script src="assets/js/buttons.colVis.min.js"></script>
	<script type="text/javascript">
         
         
         
         </script>
	<script>
	
	$('#resendEmail').on("click", function(e){
	    e.preventDefault();
	    $('#updateProdrateForm').attr('action', "resendEmail").submit();
	});
	
	
	 function submitProdRatesForm(){
         result=true;           
       if(!$('#minsumcovered').val().length) {
             $('#msg_minsumcover').removeClass('hidden');
             $('#msg_minsumcover').addClass('visible');
           
             $('#minsumcovered').focus();
             result=false;
         }
        
         if(!$('#maxsumcovered').val().length) {
             $('#msg_maxsumcover').removeClass('hidden');
             $('#msg_maxsumcover').addClass('visible');
             $('#maxsumcovered').focus();
             result=false;
         }
        
         
         if(!$('#discount').val()) {
             $('#msg_discount').removeClass('hidden');
             $('#msg_discount').addClass('visible');
             $('#discount').focus();
             result=false;
         }
         if(!$('#gst').val()) {
             $('#msg_gst').removeClass('hidden');
             $('#msg_gst').addClass('visible');
             $('#gst').focus();
             result=false;
         }
         if(!$('#stampduty').val()) {
             $('#msg_stampduty').removeClass('hidden');
             $('#msg_stampduty').addClass('visible');
             $('#stampduty').focus();
             result=false;
         }
         if(!$('#noofdrivers').val().length) {
             $('#msg_noofdrivers').removeClass('hidden');
             $('#msg_noofdrivers').addClass('visible');
             $('#noofdrivers').focus();
             result=false;
         }
        
         if(!$('#driveramt').val().length) {
             $('#msg_driveramt').removeClass('hidden');
             $('#msg_driveramt').addClass('visible');
             $('#driveramt').focus();
             result=false;
         }
         if(!$('#mindriverage').val().length) {
             $('#msg_mindriverage').removeClass('hidden');
             $('#msg_mindriverage').addClass('visible');
             $('#mindriverage').focus();
             result=false;
         }
         if(isEmpty ($('#maxdriverage').val())) {
             $('#msg_maxdriverage').removeClass('hidden');
             $('#msg_maxdriverage').addClass('visible');
             $('#maxdriverage').focus();
             result=false;
         }
         
         if(!$('#5seater').val().length) {
             $('#msg_5seater').removeClass('hidden');
             $('#msg_5seater').addClass('visible');
             $('#5seater').focus();
             result=false;
         }
         if(isEmpty ($('#7seater').val())) {
             $('#msg_7seater').removeClass('hidden');
             $('#msg_7seater').addClass('visible');
             $('#7seater').focus();
             result=false;
         }
		
         if (result==true) {
         	$('#updateProdrateForm').submit();   
         }
         
     }
	
        function validateForm() {
        	   var x = document.forms["agentList"]["dateFrom"].value;
        	   var y = document.forms["agentList"]["dateTo"].value;
        	   if ( x == null || x == "" ) {
        		   if(y !=""){
        	      alert("Please choose Date From");
        	      return false;
        	   } 
        		  
        	   }
        	   return true;
        	  
        	}
        
           function isEmpty(value) {
        	  return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
        	}
        </script>
	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->

	<script>
		$(document).ready(function() {
			 $('#date1').datepicker({
				    format: 'dd/mm/yyyy',
			}).on('changeDate', function(e){
			    $(this).datepicker('hide');
			});
		});
	</script>


</body>
</html>