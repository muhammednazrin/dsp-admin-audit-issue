<%@ page import="java.util.List"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<%@ page import="com.spring.VO.CommonNVIC"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="/siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<link rel="stylesheet" href="assets/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/menu.css">
<link rel="stylesheet" href="assets/jAlert/jAlert.css">

<style>
.col-sm-12.buttons {
	text-align: right;
}

.col-sm-12.buttons>div {
	display: inline-block;
}

.pad15 {
	padding-left: 15px !important;
	padding-right: 15px !important;
}
</style>
</head>
<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="/pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="/menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>

					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">

									<form:form action="saveNvicDone" id="saveNvicForm"
										name="saveNvicForm" method="post">

										<div class="col-sm-12">
											<div class="content-inner">

												<div class="row">
													<div class="col-sm-12">
														<div class="title">
															<div class="sub">
																<h4>Product Management | Motor Insurance</h4>
																&nbsp;(Admin are able to manage product including rates,
																discount and payment option)
															</div>
														</div>
														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-12">
																	<div class="title">
																		<div class="sub">
																			<h4>NVIC Listing</h4>
																		</div>
																	</div>
																</div>
																<div class="gap gap-mini">${nvicSaveMessage}</div>
																<div class="content-inner">
																	<div class="form-horizontal">
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-4 control-label">NVIC</label>

																				<div class="col-sm-8">

																					<input type="text" class="form-control"
																						name="nvicCode" id="nvicCode" value="" required
																						maxlength="30" /> <span id="msg_nvicCode"
																						class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Nvic Code</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-4 control-label">NVic
																					Group</label>
																				<div class="col-sm-8">

																					<input type="text" class="form-control"
																						name="nvicGroup" id="nvicGroup" value="" required
																						maxlength="30" /> <span id="msg_nvicGroup"
																						class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Nvic Group</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-4 control-label">Make
																					Code</label>
																				<div class="col-sm-8">

																					<input type="text" class="form-control" value=""
																						id="makeCode" name="makeCode"> <span
																						id="msg_makeCode" class="hidden"
																						style="color: red; text-align: left">Please
																						select your make code</span>
																					<!-- <select class="form-control">
                                                                            <option value=" Select Make"> 29 -TOYOTA</option>
                                                                              <option value="AUDI">30 -TOYOTA</option>
                                                                              <option value="MERCEDES-BENZ">31 -TOYOTA</option>
                                                                            </select>-->
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-4 control-label">Make</label>
																				<div class="col-sm-8">

																					<input type="text" class="form-control" value=""
																						id="make" name="make"> <span id="msg_make"
																						class="hidden"
																						style="color: red; text-align: left">Please
																						select your make</span>
																					<!-- <select class="form-control">
                                                                            <option value=" Select Make"> 29 -TOYOTA</option>
                                                                              <option value="AUDI">30 -TOYOTA</option>
                                                                              <option value="MERCEDES-BENZ">31 -TOYOTA</option>
                                                                            </select>-->
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-4 control-label">Model
																					Code</label>
																				<div class="col-sm-8">

																					<input type="text" class="form-control"
																						name="modelCode" id="modelCode" value="" required
																						maxlength="30" /> <span id="msg_modelCode"
																						class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Model Code</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-4 control-label">Model</label>
																				<div class="col-sm-8">

																					<input type="text" class="form-control"
																						name="model" id="model" value="" required
																						maxlength="30" /> <span id="msg_model"
																						class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Model</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-4 control-label">Seat</label>
																				<div class="col-sm-8">

																					<input type="text" class="form-control" name="seat"
																						id="seat" value="" required maxlength="30" /> <span
																						id="msg_seat" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Seat</span>
																				</div>
																			</div>
																			<!--   <div class="form-group">
                                                                        <label class="col-sm-4 control-label">Total Sum</label>
                                                                        <div class="col-sm-8">
                                                                   														                    
																				<input type="text" class="form-control" name="totalsum" id="totalsum" value="" required maxlength="30" />																			
																		
																				<span id="msg_seat" class="hidden"
																					style="color: red; text-align: left">Please
																					enter your Total Sum</span>
                                                                        </div>
                                                                      </div>-->
																		</div>
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-4 control-label">MM
																					Code</label>
																				<div class="col-sm-8">

																					<input type="text" class="form-control"
																						name="mmCode" id="mmCode" value="" required
																						maxlength="30" /> <span id="msg_mmCode"
																						class="hidden"
																						style="color: red; text-align: left">Please
																						enter your MMcode</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-4 control-label">Variant</label>
																				<div class="col-sm-8">

																					<input type="text" class="form-control"
																						name="variant" id="variant" value="" required
																						maxlength="30" /> <span id="msg_variant"
																						class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Variant</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-4 control-label">Series</label>
																				<div class="col-sm-8">

																					<input type="text" class="form-control"
																						name="series" id="series" value="" required
																						maxlength="30" /> <span id="msg_series"
																						class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Series</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-4 control-label">Year
																					of Manufactured</label>
																				<div class="col-sm-8">

																					<input type="text" class="form-control" value=""
																						id="year" name="year">


																				</div>
																			</div>
																			<!--   <div class="form-group">
                                                                        <label class="col-sm-4 control-label">Engine Capacity</label>
                                                                        <div class="col-sm-8">
                                                                          													                    
																				<input type="text" class="form-control" name="engine" id="engine" value="" required maxlength="30" />																			
																			
				
                                                                        </div>
                                                                      </div>-->
																			<div class="form-group">
																				<label class="col-sm-4 control-label">Cubic
																					Capacity (CC)</label>
																				<div class="col-sm-8">

																					<input type="text" class="form-control" name="cc"
																						id="cc" value="" required maxlength="30" /> <span
																						id="msg_cc" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Cubic Capacity</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-4 control-label">Effective
																					date</label>
																				<div class="col-sm-8">

																					<input type="text" class="form-control"
																						name="effectiveDate" id="effectiveDate" value=""
																						required maxlength="30" /> <span id="msg_effdate"
																						class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Effective date</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-4 control-label">Style</label>
																				<div class="col-sm-8">
																					<input type="text" class="form-control"
																						name="style" id="style" value="" required
																						maxlength="30" />

																				</div>
																			</div>
																		</div>
																		<div class="col-sm-12">
																			<div class="form-group">
																				<label class="col-sm-2 control-label">Description</label>
																				<div class="col-sm-10">

																					<input type="text" class="form-control"
																						name="description" id="description" value=""
																						required maxlength="30" /> <span
																						id="msg_description" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Description</span>

																				</div>
																			</div>
																		</div>
																		<div class="col-sm-12 hidden-xs">
																			<div class="row">
																				<div class="col-sm-3"></div>
																				<div class="col-sm-3 text-center">
																					<label class="text-warning">Peninsular
																						Malaysia</label>
																				</div>
																				<div class="col-sm-3 text-center">
																					<label class="text-warning">Sabah &amp;
																						Sarawak</label>
																				</div>
																				<div class="col-sm-3 text-center">
																					<label class="text-warning">Langkawi &amp;
																						Labuan</label>
																				</div>
																			</div>
																		</div>
																		<div class="gap gap-mini"></div>
																		<div class="col-sm-12">
																			<div class="row">
																				<div class="col-sm-3">
																					<div class="form-group">
																						<label class="col-sm-8 control-label">Market
																							Value</label>
																					</div>
																				</div>
																				<div class="col-sm-3">
																					<div>
																						<label class="text-warning visible-xs">Peninsular
																							Malaysia</label> <input type="text" class="form-control"
																							name="marketValue1" id="marketValue1" value=""
																							required maxlength="30" /> <span
																							id="msg_marketValue1" class="hidden"
																							style="color: red; text-align: left">Please
																							enter your Market Value1</span>
																					</div>
																				</div>
																				<div class="col-sm-3">
																					<div>
																						<label class="text-warning visible-xs">Sabah
																							&amp; Sarawak</label> <input type="text"
																							class="form-control" name="marketValue2"
																							id="marketValue2" value="" required
																							maxlength="30" /> <span id="msg_marketValue2"
																							class="hidden"
																							style="color: red; text-align: left">Please
																							enter your Market Value2</span>
																					</div>
																				</div>
																				<div class="col-sm-3">
																					<div>
																						<label class="text-warning visible-xs">Langkawi
																							&amp; Labuan</label> <input type="text"
																							class="form-control" name="marketValue3"
																							id="marketValue3" value="" required
																							maxlength="30" /> <span id="msg_marketValue3"
																							class="hidden"
																							style="color: red; text-align: left">Please
																							enter your Market Value3</span>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="gap gap-mini"></div>
																		<div class="col-sm-12">
																			<div class="row">
																				<div class="col-sm-3">
																					<div class="form-group">
																						<label class="col-sm-8 control-label">Windscreen
																							Value</label>
																					</div>
																				</div>
																				<div class="col-sm-3">
																					<div>
																						<label class="text-warning visible-xs">Peninsular
																							Malaysia</label> <input type="text" class="form-control"
																							name="windscreenValue1" id="windscreenValue1"
																							value="" required maxlength="30" /> <span
																							id="msg_windscreenValue1" class="hidden"
																							style="color: red; text-align: left">Please
																							enter your Windscreen Value1</span>
																					</div>
																				</div>
																				<div class="col-sm-3">
																					<div>
																						<label class="text-warning visible-xs">Sabah
																							&amp; Sarawak</label> <input type="text"
																							class="form-control" name="windscreenValue2"
																							id="windscreenValue2" value="" required
																							maxlength="30" /> <span
																							id="msg_windscreenValue2" class="hidden"
																							style="color: red; text-align: left">Please
																							enter your Windscreen Value2</span>
																					</div>
																				</div>
																				<div class="col-sm-3">
																					<div>
																						<label class="text-warning visible-xs">Langkawi
																							&amp; Labuan</label> <input type="text"
																							class="form-control" name="windscreenValue3"
																							id="windscreenValue3" value="" required
																							maxlength="30" /> <span
																							id="msg_windscreenValue3" class="hidden"
																							style="color: red; text-align: left">Please
																							enter your Windscreen Value3</span>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="gap gap-mini"></div>
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-4 control-label">4wd</label>
																				<div class="col-sm-8">
																					<div class="checkbox">
																						<label> <input type="checkbox"
																							name="wd4Flg" id="wd4Flg" value="Y" />


																						</label>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-4 control-label">High
																					Performance</label>
																				<div class="col-sm-8">
																					<div class="checkbox">
																						<label> <input type="checkbox"
																							name="hpFlg" id="hpFlg" value="Y" />

																						</label>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-4 control-label">Mark
																					Up Percentage</label>
																				<div class="col-sm-8">

																					<input type="text" class="form-control"
																						name="markUpPer" id="markUpPer" value="" required
																						maxlength="30" /> <span id="msg_markUpPer"
																						class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Mark Up Percentage</span>
																				</div>
																			</div>
																		</div>
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-4 control-label">Mark
																					Down Percentage</label>
																				<div class="col-sm-8">

																					<input type="text" class="form-control"
																						name="markDownPer" id="markDownPer" value=""
																						required maxlength="30" /> <span
																						id="msg_markDownPer" class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Mark Down Percentage</span>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>

												</div>
												<!--content inner -->
											</div>
											<!--col-sm-12 -->

											<div class="col-sm-12 buttons">
												<div>
													<input class="btn btn-warning btn-sm" id="saveNvic"
														onClick="submitNvicForm();" type="button" value="Save" />
												</div>
												<div>

													<%-- <a href="resendEmail?email=<c:out value="${agentProfile.email}"/>" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i>Resend Email</a> --%>
												</div>
											</div>
											<!--col-sm-12 -->
										</div>
										<!--row -->
									</form:form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.container -->
	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="/pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->
	</div>


	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%-- <jsp:include page="siteFooter.jsp" /> --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->

	<script src="assets/js/jquery.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.js" type="text/javascript"></script>
	<script src="assets/jAlert/jAlert.min.js"></script>
	<script src="assets/jAlert/jAlert-functions.min.js"></script>
	<script src="assets/plugins/retina/retina.min.js"></script>
	<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>
	<script src="assets/js/apps.js"></script>

	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script type="text/javascript" src="assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript" src="assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/buttons.print.min.js"></script>
	<script src="assets/js/buttons.colVis.min.js"></script>
	<script type="text/javascript">
         
         
         
         </script>
	<script>
	
	$('#resendEmail').on("click", function(e){
	    e.preventDefault();
	    $('#saveNvicForm').attr('action', "resendEmail").submit();
	});
	
	
	 function submitNvicForm(){
         result=true;           
       if(!$('#nvicCode').val().length) {
             $('#msg_nvicCode').removeClass('hidden');
             $('#msg_nvicCode').addClass('visible');
           
             $('#nvicCode').focus();
             result=false;
         }
        
         if(!$('#nvicGroup').val().length) {
             $('#msg_nvicGroup').removeClass('hidden');
             $('#msg_nvicGroup').addClass('visible');
             $('#nvicGroup').focus();
             result=false;
         }
        
         
         if(!$('#makeCode').index() == 0) {
             $('#msg_makeCode').removeClass('hidden');
             $('#msg_makeCode').addClass('visible');
             $('#makeCode').focus();
             result=false;
         }
         if(!$('#modelCode').val()) {
             $('#msg_modelCode').removeClass('hidden');
             $('#msg_modelCode').addClass('visible');
             $('#modelCode').focus();
             result=false;
         }
         if(!$('#model').val()) {
             $('#msg_model').removeClass('hidden');
             $('#msg_model').addClass('visible');
             $('#model').focus();
             result=false;
         }
         if(!$('#seat').val().length) {
             $('#msg_seat').removeClass('hidden');
             $('#msg_seat').addClass('visible');
             $('#seat').focus();
             result=false;
         }
        
         if(!$('#description').val().length) {
             $('#msg_description').removeClass('hidden');
             $('#msg_description').addClass('visible');
             $('#description').focus();
             result=false;
         }
         if(!$('#mmCode').val().length) {
             $('#msg_mmCode').removeClass('hidden');
             $('#msg_mmCode').addClass('visible');
             $('#mmCode').focus();
             result=false;
         }
         if(isEmpty ($('#variant').val())) {
             $('#msg_variant').removeClass('hidden');
             $('#msg_variant').addClass('visible');
             $('#variant').focus();
             result=false;
         }
         
         if(!$('#series').val().length) {
             $('#msg_series').removeClass('hidden');
             $('#msg_series').addClass('visible');
             $('#series').focus();
             result=false;
         }
         if(isEmpty ($('#cc').val())) {
             $('#msg_cc').removeClass('hidden');
             $('#msg_cc').addClass('visible');
             $('#postcode').focus();
             result=false;
         }
		 if(isEmpty ($('#marketValue1').val())) {
             $('#msg_marketValue1').removeClass('hidden');
             $('#msg_marketValue1').addClass('visible');
             $('#marketValue1').focus();
             result=false;
         }
		 if(isEmpty ($('#marketValue2').val())) {
             $('#msg_marketValue2').removeClass('hidden');
             $('#msg_marketValue2').addClass('visible');
             $('#marketValue2').focus();
             result=false;
         }
		  if(isEmpty ($('#marketValue3').val())) {
             $('#msg_marketValue3').removeClass('hidden');
             $('#msg_marketValue3').addClass('visible');
             $('#marketValue3').focus();
             result=false;
         }
		 
		 if(isEmpty ($('#windscreenValue1').val())) {
             $('#msg_windscreenValue1').removeClass('hidden');
             $('#msg_windscreenValue1').addClass('visible');
             $('#windscreenValue1').focus();
             result=false;
         }
		  if(isEmpty ($('#windscreenValue2').val())) {
             $('#msg_windscreenValue2').removeClass('hidden');
             $('#msg_windscreenValue2').addClass('visible');
             $('#windscreenValue2').focus();
             result=false;
         }
		  if(isEmpty ($('#windscreenValue3').val())) {
             $('#msg_windscreenValue3').removeClass('hidden');
             $('#msg_windscreenValue3').addClass('visible');
             $('#windscreenValue3').focus();
             result=false;
         }
		/* if($('#wd4Flg').index() == 0) {
             $('#msg_wd4Flg').removeClass('hidden');
             $('#msg_wd4Flg').addClass('visible');
             $('#wd4Flg').focus();
             result=false;
         }*/
		  if(isEmpty ($('#markUpPer').val())) {
             $('#msg_markUpPer').removeClass('hidden');
             $('#msg_markUpPer').addClass('visible');
             $('#markUpPer').focus();
             result=false;
         }
		  if(isEmpty ($('#markDownPer').val())) {
             $('#msg_markDownPer').removeClass('hidden');
             $('#msg_markDownPer').addClass('visible');
             $('#markDownPer').focus();
             result=false;
         }
		/*  if($('#hpFlg').index() == 0) {
             $('#msg_hpFlg').removeClass('hidden');
             $('#msg_hpFlg').addClass('visible');
             $('#hpFlg').focus();
             result=false;
         }
		*/

		 
         if (result==true) {
         	$('#saveNvicForm').submit();   
         }
         
     }
	
       
        
           function isEmpty(value) {
        	  return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
        	}
        </script>
	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->

	<script>
		$(document).ready(function() {
			 $('#effectiveDate').datepicker({
				    format: 'dd/mm/yyyy',
			}).on('changeDate', function(e){
			    $(this).datepicker('hide');
			});
		});
	</script>


</body>
</html>