<%@ page import="java.util.List"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@ page import="com.spring.VO.MITblExcess"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/js/jquery.dataTables.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/buttons.dataTables.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/dataTables.bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/buttons.bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link rel="stylesheet" href="assets/css/toastr.min.css">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="/siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}

#upload-file-selector {
	display: none;
}
</style>


<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}
</style>
<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet"
	href="assets/plugins/datepicker/datepicker.min.css">
<!-- PLUGINS CSS -->
<link href="assets/plugins/owl-carousel/owl.carousel.min.css"
	rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.theme.min.css"
	rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.transitions.min.css"
	rel="stylesheet">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

form#agentE, form#agentD {
	display: inline-block;
}

form#agentE input[type=submit], form#agentD input[type=submit] {
	padding: 5px;
}
</style>
</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="/pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="/menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<form:form action="updateDoneMTExcess" id="updateExcessForm"
												name="updateExcessForm" method="post">

												<div class="row">
													<div class="col-sm-12">
														<div class="title">
															<div class="sub">
																<h4>Product Management | Motor Takaful</h4>
																&nbsp;(Admin are able to manage product including rates,
																discount and payment option)
															</div>
														</div>
													</div>

													<div class="col-sm-12">
														<!-- Begin  table -->
														<div class="content-inner">
															<div class="the-box full no-border">
																<div class="title">
																	<div class="sub">
																		<h4>Excess Table</h4>
																	</div>
																</div>
																<div>
																	<div class="form-horizontal form-group-sm">
																		<div class="table-responsive">
																			<table
																				class="table table-striped table-warning table-hover">
																				<thead>
																					<tr>
																						<th>Min CC</th>
																						<th>Max CC</th>
																						<th>Min Sum Insured(RM)</th>
																						<th>Max Sum Insured(RM)</th>
																						<th>Total Excess (%)</th>
																						<th>Total Max (RM)</th>
																					</tr>
																				</thead>
																				<tbody>
																					<c:forEach items="${excessList}" var="element"
																						varStatus="theCount">
																						<tr>
																							<td><input type="text" class="form-control"
																								name="mincc"
																								value="<c:out value="${element.mincc}" />"></td>
																							<td><input type="text" class="form-control"
																								name="maxcc"
																								value="<c:out value="${element.maxcc}" />"></td>
																							<td><input type="text" class="form-control"
																								name="minsum"
																								value="<c:out value="${element.minSuminsured}" />"></td>
																							<td><input type="text" class="form-control"
																								name="maxsum"
																								value="<c:out value="${element.maxSuminsured}" />"></td>
																							<td><input type="text" class="form-control"
																								name="totexcess"
																								value="<c:out value="${element.totExcess}" />"></td>
																							<td><input type="text" class="form-control"
																								name="totmax"
																								value="<c:out value="${element.totMax}" />">
																								<input type="hidden" class="form-control"
																								name="exid"
																								value="<c:out value="${element.id}" />">
																								<input type="hidden" class="form-control"
																								name="recCount"
																								value="<c:out value="${theCount.count}" />"></td>
																							<td><input class="btn btn-warning btn-sm"
																								id="deleteDoneMTExcess"
																								onClick="return deleteproduct();" type="submit"
																								value="Delete" /> <!--  <a class="btn btn-warning btn-sm" href="#" onclick="deleteproduct('<c:out value="${element.id}" />');"><i class="fa fa-plus"></i> Delete</a>  -->
																							</td>
																						</tr>
																					</c:forEach>
																				</tbody>
																			</table>
																		</div>
																	</div>
																</div>
															</div>
															<!-- /.the-box -->
															<!-- End warning color table -->
														</div>
													</div>


													<div class="gap gap-mini"></div>
													<div class="col-sm-12">
														<div class="text-right">
															<input class="btn btn-warning btn-sm"
																id="updateDoneExcess" onClick="submitExcessForm();"
																type="submit" value="Update" /> <a
																class="btn btn-warning btn-xs" href="AddMTExcess"><i
																class="fa fa-plus"></i> Add</a>



														</div>
													</div>

												</div>
											</form:form>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<jsp:include page="/pageFooter.jsp" />
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%--    <jsp:include page="siteFooter.jsp" />   --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->


	<!-- JAVA Script Goes Here -->
	<script
		src="${pageContext.request.contextPath}/assets/js/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/js/jquery-2.2.4.min.js" />"></script>
	<!--  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
	<script src="assets/js/apps.js"></script>
	<script src="assets/plugins/retina/retina.min.js"></script>
	<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>

	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script type="text/javascript" src="assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript" src="assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/buttons.print.min.js"></script>
	<script src="assets/js/buttons.colVis.min.js"></script>
	<script src="assets/js/toastr.min.js"></script>
	<script type="text/javascript">

         </script>

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->

	<script>
		$(document).ready(function() {

			$('#tbl-nvic-list').DataTable({
				 "order": [],
				dom : 'Bfrtip',
				title : 'MI NVI List',
			

				buttons : [ 'copy', 'csv', 'print', {
					extend : 'excelHtml5',
					title : ' MI NVI List'
				}, {
					extend : 'pdfHtml5',
					title :  'MI NVI List Report'
				} ]
			});
			
			// $('#date1').datepicker();
			 $('#date1').datepicker({
				    format: 'dd/mm/yyyy',
			         }).on('changeDate', function(e){
	 			    $(this).datepicker('hide');
	 			
				});
			 $('#date2').datepicker({
				    format: 'dd/mm/yyyy',
			        }).on('changeDate', function(e){
	 			    $(this).datepicker('hide');
	 			    });
			
			 var agentAddedMessage="<c:out value="${agentAddedMessage}"/>";
	 		       // show when the button is clicked
	 		       if (agentAddedMessage.length) {
	 		    	  toastr.success(agentAddedMessage);
	 		       }
	 		      
	 		  var agentDeletedMessage="<c:out value="${agentDeletedMessage}"/>";
	 		       // show when the button is clicked
	 		       if (agentDeletedMessage.length) {
	 		    	  toastr.success(agentDeletedMessage);
	 		       }
	 		       
	 		  var agentUpdatedMessage="<c:out value="${agentUpdatedMessage}"/>";
	 		       // show when the button is clicked
	 		       if (agentUpdatedMessage.length) {
	 		    	  toastr.success(agentUpdatedMessage);
	 		       }
	 		  
			 
			 
		
		    });
		
	    function deleteproduct(){
	           
	      var r = confirm("Are You Sure You Want to Delete this Product!");
        	 if (r == true) {
        	   
        		  $('#updateExcessForm').attr('action', 'deleteMTExcess'); 
	              $('#updateExcessForm').submit();
	              return true;
        	 } 
	      return false;
	    }
	</script>





</body>
</html>