<%@ page import="java.util.List"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<%@ page import="com.spring.VO.MITblExcess"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="/siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<link rel="stylesheet" href="assets/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/menu.css">
<link rel="stylesheet" href="assets/jAlert/jAlert.css">

<style>
.col-sm-12.buttons {
	text-align: right;
}

.col-sm-12.buttons>div {
	display: inline-block;
}

.pad15 {
	padding-left: 15px !important;
	padding-right: 15px !important;
}
</style>
</head>
<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="/pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="/menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>

					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">

									<form:form action="saveMTExcessDone" id="saveExcessForm"
										name="saveExcessForm" method="post">

										<div class="col-sm-12">
											<div class="content-inner">

												<div class="row">
													<div class="col-sm-12">
														<div class="title">
															<div class="sub">
																<h4>Product Management | Motor Takaful</h4>
																&nbsp;(Admin are able to manage product including rates,
																discount and payment option)
															</div>
														</div>
														<div class="content-inner">
															<div class="the-box">
																<div class="col-sm-12">
																	<div class="title">
																		<div class="sub">
																			<h4>Excess</h4>
																		</div>
																	</div>
																</div>
																<div class="gap gap-mini"></div>
																<div class="content-inner">
																	<div class="form-horizontal">
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-4 control-label">Min CC</label>

																				<div class="col-sm-8">

																					<input type="text" class="form-control"
																						name="mincc" id="mincc" value="" required
																						maxlength="30" /> <span id="msg_mincc"
																						class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Min CC</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-4 control-label">Max CC</label>
																				<div class="col-sm-8">

																					<input type="text" class="form-control"
																						name="maxcc" id="maxcc" value="" required
																						maxlength="30" /> <span id="msg_maxcc"
																						class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Max CC</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-4 control-label">Min
																					Sum Insured(RM)</label>
																				<div class="col-sm-8">

																					<input type="text" class="form-control" value=""
																						id="minsuminsured" name="minsuminsured"> <span
																						id="msg_minsum" class="hidden"
																						style="color: red; text-align: left">Please
																						select your Min Sum Insured(RM)</span>

																				</div>
																			</div>


																		</div>
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-4 control-label">Max
																					Sum Insured(RM)</label>
																				<div class="col-sm-8">

																					<input type="text" class="form-control"
																						name="maxsuminsured" id="maxsuminsured" value=""
																						required maxlength="30" /> <span id="msg_maxsum"
																						class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Max Sum Insured(RM)</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-4 control-label">Total
																					Excess (%)</label>
																				<div class="col-sm-8">

																					<input type="text" class="form-control"
																						name="totexcess" id="totexcess" value="" required
																						maxlength="30" /> <span id="msg_variant"
																						class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Total Excess (%)</span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-4 control-label">Total
																					Max (RM)</label>
																				<div class="col-sm-8">

																					<input type="text" class="form-control"
																						name="totmax" id="totmax" value="" required
																						maxlength="30" /> <span id="msg_series"
																						class="hidden"
																						style="color: red; text-align: left">Please
																						enter your Total Max (RM)</span>
																				</div>
																			</div>

																		</div>


																	</div>
																	<div class="gap gap-mini"></div>


																</div>
															</div>
														</div>
													</div>
												</div>

											</div>
											<!--content inner -->
										</div>
										<!--col-sm-12 -->

										<div class="col-sm-12 buttons">
											<div>
												<input class="btn btn-warning btn-sm" id="saveExcess"
													onClick="submitExcessForm();" type="submit" value="Save" />
											</div>
											<div>

												<%-- <a href="resendEmail?email=<c:out value="${agentProfile.email}"/>" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i>Resend Email</a> --%>
											</div>
										</div>
										<!--col-sm-12 -->
								</div>
								<!--row -->
								</form:form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	<!-- /.container -->
	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="/pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->
	</div>


	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%-- <jsp:include page="siteFooter.jsp" /> --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->

	<script src="assets/js/jquery.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.js" type="text/javascript"></script>
	<script src="assets/jAlert/jAlert.min.js"></script>
	<script src="assets/jAlert/jAlert-functions.min.js"></script>
	<script src="assets/plugins/retina/retina.min.js"></script>
	<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>
	<script src="assets/js/apps.js"></script>

	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script type="text/javascript" src="assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript" src="assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/buttons.print.min.js"></script>
	<script src="assets/js/buttons.colVis.min.js"></script>
	<script type="text/javascript">
         
         
         
         </script>
	<script>
	
	$('#resendEmail').on("click", function(e){
	    e.preventDefault();
	    $('#saveExcessForm').attr('action', "resendEmail").submit();
	});
	
	
	 function submitExcessForm(){
         result=true;           
       if(!$('#mincc').val().length) {
             $('#msg_mincc').removeClass('hidden');
             $('#msg_mincc').addClass('visible');
           
             $('#mincc').focus();
             result=false;
         }
        
         if(!$('#maxcc').val().length) {
             $('#msg_maxcc').removeClass('hidden');
             $('#msg_maxcc').addClass('visible');
             $('#maxcc').focus();
             result=false;
         }
        
         
         if(!$('#minsuminsured').index() == 0) {
             $('#msg_minsum').removeClass('hidden');
             $('#msg_minsum').addClass('visible');
             $('#minsuminsured').focus();
             result=false;
         }
         if(!$('#maxsuminsured').val()) {
             $('#msg_maxsum').removeClass('hidden');
             $('#msg_maxsum').addClass('visible');
             $('#maxsuminsured').focus();
             result=false;
         }
         if(!$('#totexcess').val()) {
             $('#msg_totexcess').removeClass('hidden');
             $('#msg_totexcess').addClass('visible');
             $('#totexcess').focus();
             result=false;
         }
         if(!$('#totmax').val().length) {
             $('#msg_totmax').removeClass('hidden');
             $('#msg_totmax').addClass('visible');
             $('#totmax').focus();
             result=false;
         }
        
         
		 
         if (result==true) {
         	$('#saveExcessForm').submit();   
         }
         
     }
	
       
        
           function isEmpty(value) {
        	  return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
        	}
        </script>
	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->

	<script>
		$(document).ready(function() {
			 $('#date1').datepicker({
				    format: 'dd/mm/yyyy',
			}).on('changeDate', function(e){
			    $(this).datepicker('hide');
			});
		});
	</script>


</body>
</html>