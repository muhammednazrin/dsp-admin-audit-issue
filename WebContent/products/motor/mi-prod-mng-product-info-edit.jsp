<%@page import="java.util.ArrayList"%>

<%@ page import="java.util.Date"%>

<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="/siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
</head>

<body>


	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="/pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="/menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Product Management | Motor Insurance</h4>
															&nbsp;(Admin are able to manage product including rates,
															discount and payment option)
														</div>
													</div>

												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="title">
																<div class="sub">
																	<h4>Product Infomation</h4>
																</div>
															</div>
															<div>

																<form:form action="updateDoneMIproductinfoApproval"
																	id="updateDoneMIproductinfoForm"
																	name="updateDoneMIproductinfoForm" method="post">
																	<%--  <form:form action="updateDoneMIproductinfo"  id="updateDoneMIproductinfoForm" name="updateDoneMIproductinfoForm" method="post"> --%>

																	<div class="form-horizontal info-meor">

																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Product
																					Code</label>
																				<div class="col-sm-6">
																					<p class="form-control-static">: MI</p>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Product
																					Name</label>
																				<div class="col-sm-6">
																					<p class="form-control-static">: Motor
																						Insurance</p>
																				</div>
																			</div>
																		</div>
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Annual
																					Sales Target</label>
																				<div class="col-sm-6">
																					<p class="form-control-static">
																						:
																						<c:forEach items="${annualSalesAmntList}"
																							var="elementAnnSales">
																							<input type="text" name="salesTargetVal"
																								class="form-control"
																								value=<c:out value="${elementAnnSales.code}" />>
																						</c:forEach>
																					</p>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Quotation
																					Validity</label>
																				<div class="col-sm-6">
																					<p class="form-control-static">
																						:
																						<c:forEach items="${quotationValidityList}"
																							var="elementQuotVal">
																							<input type="text" name="quotVal"
																								class="form-control"
																								value=<c:out value="${elementQuotVal.code}" />>
																						</c:forEach>
																					</p>
																				</div>
																			</div>
																		</div>

																	</div>
																	<div class="gap gap-mini"></div>
																	<div>
																		<label class="control-label">E Document</label>
																	</div>
																	<table
																		class="table table-striped table-warning table-hover">
																		<thead>
																			<tr>
																				<th style="width: 30px;">No</th>
																				<th>Date</th>
																				<th>File Name</th>
																				<th>File Size</th>
																				<th width="10%">Action</th>
																			</tr>
																		</thead>
																		<tbody>

																			<c:forEach items="${tblPdfInfoList}" var="element"
																				varStatus="theCount">
																				<tr>
																					<td><c:out value="${theCount.count}" /></td>
																					<c:set var="createDate"
																						value="${element.createdDate}" />
																					<td><fmt:formatDate type="both"
																							value="${createDate}"
																							pattern="dd-MMM-yyyy hh:mm:ss" /></td>
																					<td><c:out value="${element.fileName}" /></td>
																					<td><c:out value="${element.fileSize}" /> kb</td>

																					<td><input type="hidden" name="id"
																						value="<c:out value="${element.id}"/>" /> <input
																						type="hidden" name="miFilePath"
																						value="<c:out value="${element.filePath}"/>" /> <input
																						class="btn btn-warning btn-sm"
																						id="deleteDoneprodinfo"
																						onClick="return deleteproductmi();" type="submit"
																						value="Delete" /> <!-- <a class="btn btn-warning btn-xs" href="#"><i class="fa fa-trash"></i> Delete</a> -->
																					</td>
																				</tr>
																			</c:forEach>
																		</tbody>
																	</table>
															</div>
															<div class="col-sm-12">
																<div class="text-right">
																	<a class="btn btn-warning btn-xs" href="#"
																		data-toggle="modal" data-target="#mydelete"><i
																		class="fa fa-plus"></i> Add</a>
																</div>
															</div>
														</div>
													</div>
													<!-- /.the-box -->
												</div>
												<!-- End warning color table -->
											</div>

											<div class="col-sm-12">
												<div class="text-right">
													<a class="btn btn-default btn-sm" role="button"
														href="MIproductinfo"><i class="fa fa-chevron-left"></i>
														Back</a>
													<!-- <a class="btn btn-warning btn-sm" href="updateDoneMIproductinfo"><i class="fa fa-edit"></i> Update</a> -->
													<input class="btn btn-warning btn-sm"
														onClick="submitMIProductInfo();" type="submit"
														value="Update" />
												</div>
											</div>
											</form:form>
										</div>
									</div>
									<!--row -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->
		<!-- Modal delete-->
		<script type="text/javascript"
			src="//code.jquery.com/jquery-1.11.3.min.js"></script>
		<script
			src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<div class="modal fade" id="mydelete" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form:form action="uploadMIPdfFile" method="post"
						enctype="multipart/form-data" id="TLPdfFile">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Product Management
								| Motor Insurance | E-Documents</h4>
						</div>
						<div class="modal-body">
							<div class="form-harizontal">
								<div class="form-group">
									<label>File Name <span class="text-danger">*</span></label> <input
										type="text" name="miFileName" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label>File <span class="text-danger">*</span></label> <input
									type="file" name="PDF" id="browsepdf" accept=".pdf">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Cancel</button>
							<!--    <button type="button" class="btn btn-warning">Save</button> -->
							<label class="btn btn-default disabled" id="uploadMIPdf">
								Save <input type="submit" value="submit" style="display: none;">
							</label>
						</div>
					</form:form>
				</div>
			</div>
		</div>

		<!-- Modal -->
	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="/pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>

	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->

	<script
		src="<c:url value="/resources/assets/js/jquery-2.2.4.min.js" />"></script>
	<script src="assets/js/apps.js"></script>
	<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>
	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script type="text/javascript" src="assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript" src="assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/buttons.print.min.js"></script>
	<script src="assets/js/buttons.colVis.min.js"></script>
	<script src="assets/js/toastr.min.js"></script>
	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

	<script type="text/javascript">
        function submitMIProductInfo(){
   
            	 $('#updateDoneMIproductinfoForm').submit();
            	   return true;
            
        }
   	
   	     function deleteproductmi(){
	          
	      var r = confirm("Are You Sure You Want to Delete this Product!");
        	 if (r == true) {
        	   
        		  $('#updateDoneMIproductinfoForm').attr('action', 'deleteMIPdsDone'); 
	              $('#updateDoneMIproductinfoForm').submit();
	              return true;
        	 } 
	      return false;
	    }
        </script>
</body>
</html>