<%@ page import="java.util.List"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="icon" href="resources/assets/img/maybankLogoICO.ico"
	type="image/x-icon" />

<link rel="stylesheet" type="text/css"
	href="assets/js/jquery.dataTables.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/buttons.dataTables.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/dataTables.bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/buttons.bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="assets/css/toastr.min.css" />
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="/siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}

#upload-file-selector {
	display: none;
}
</style>

<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}

.modal-dialog {
	width: 600px !important;
}
</style>

<!-- OI CSS (REQUIRED Buddy PA)-->
<link rel="stylesheet" href="assets/css/oiCSS.css" />

<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet"
	href="assets/plugins/datepicker/datepicker.min.css" />
<!-- PLUGINS CSS -->
<link rel="stylesheet"
	href="assets/plugins/owl-carousel/owl.carousel.min.css" />
<link rel="stylesheet"
	href="assets/plugins/owl-carousel/owl.theme.min.css" />
<link rel="stylesheet"
	href="assets/plugins/owl-carousel/owl.transitions.min.css" />

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link rel="stylesheet"
	href="assets/plugins/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet" href="assets/css/owl.css" />
<link rel="stylesheet" href="assets/css/bootstrap.css" />
<link rel="stylesheet" href="assets/css/bootstrap-select.css" />
<link rel="stylesheet"
	href="assets/plugins/datatable/css/bootstrap.datatable.min.css" />
<link rel="stylesheet"
	href="assets/plugins/datatable/css/dataTables.searchHighlight.css" />
<link rel="stylesheet" href="assets/css/menu.css" />
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="libs/html5shiv.js"></script>
    <script src="libs/respond.min.js"></script>
<![endif]-->

<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

form#agentE, form#agentD {
	display: inline-block;
}

form#agentE input[type=submit], form#agentD input[type=submit] {
	padding: 5px;
}
</style>
</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
    -->

	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="/pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="/menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>

							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Buddy PA | Occupation Management</h4>
															<span>(Admin are able to manage product including
																rates, discount and payment option)</span>
														</div>
													</div>

													<!-- Start Form -->
													<!--   <input type="hidden" name="action" value="listAll" /> -->

													<div class="col-sm-12">
														<!-- Begin  table -->
														<div class="content-inner">
															<div class="the-box full no-border">
																<form:form action="addBuddyPAOccupation" method="post"
																	name="addOccupation" id="addOccupation">
																	<div class="title">
																		<div class="sub">
																			<h4>Occupation Add</h4>
																			<div id="ErrmsgDiv"
																				class="errorMsg leftPadding errorMsgDisplayF">
																				<i class="fa fa-exclamation-triangle"></i> <span
																					id="ErrmsgAll">${Errmsg}</span>
																			</div>
																			<div id="SuccessMsgDiv"
																				class="successMsg leftPadding errorMsgDisplayF">
																				<i class="fa fa-exclamation-circle"></i> <span
																					id="SuccessMsgAll">${SuccessMsg}</span>
																			</div>
																		</div>
																	</div>

																	<div>
																		<div class="form-horizontal info-meor">
																			<div class="col-sm-8">
																				<div class="form-group">
																					<label class="col-sm-3 control-label">Company</label>
																					<div class="col-sm-8">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> <select
																								onChange="submitCompany()" name="company"
																								id="company">
																								<c:forEach items="${companyList}"
																									var="companyList">
																									<option value="${companyList.getName()}"
																										<c:if test="${company==companyList.getName()}">selected</c:if>>${companyList.getDescription()}
																									</option>
																								</c:forEach>
																							</select> <span class="error leftPadding"
																								id="errmsgCompany" style="display: none;">Company
																								cannot be empty! </span>
																						</p>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label">Occupation
																						Code</label>
																					<div class="col-sm-8">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> <input
																								type="text" name="occupationCode"
																								id="occupationCode" MaxLength="10" /> <span
																								class="error leftPadding" id="errmsgCode"
																								style="display: none;">Occupation code
																								cannot be empty! </span>
																						</p>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label">Description</label>
																					<div class="col-sm-8">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> <input
																								type="text" name="description" id="description"
																								style="width: 300px;" MaxLength="50" /> <span
																								class="error leftPadding" id="errmsgDesc"
																								style="display: none;">Description cannot
																								be empty! </span>
																						</p>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label">Occupation
																						Class</label>
																					<div class="col-sm-8">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> <select
																								name="occupationClass" id="occupationClass">
																								<option value="">- Select Class -</option>
																								<option value="00">00</option>
																								<option value="01">01</option>
																								<option value="02">02</option>
																								<option value="03">03</option>
																							</select> <span class="error leftPadding" id="errmsgClass"
																								style="display: none;">Occupation class
																								cannot be empty! </span>
																						</p>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label">Declined
																						Product 1</label>
																					<div class="col-sm-8">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> <select
																								name="declinedProduct1" id="declinedProduct1">
																								<option value="">- Select Product -</option>
																								<option value="CPP">CPP</option>
																								<option value="TPP">TPP</option>
																							</select>
																						</p>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label">Declined
																						Product 2</label>
																					<div class="col-sm-8">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> <select
																								name="declinedProduct2" id="declinedProduct2">
																								<option value="">- Select Product -</option>
																								<option value="CPP">CPP</option>
																								<option value="TPP">TPP</option>
																							</select>
																						</p>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label">Enable</label>
																					<div class="col-sm-8">
																						<p class="form-control-static minusPaddingTop">
																							<span class="dotSpace">:</span> <input
																								type="checkbox" name="occupationEnable"
																								class="paddingTopCheckBox">
																						</p>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label"></label>
																					<div class="col-sm-8">
																						<p class="form-control-static">
																							<span style="color: transparent;">:</span> <a
																								class="btn btn-warning btn-sm" id="btnAdd"
																								onClick="validateForm('add');"><i
																								class="fa fa-plus"></i> Add</a>
																						</p>
																					</div>
																				</div>
																			</div>
																			<div class="gap gap-mini"></div>
																		</div>
																	</div>
																</form:form>
																<form:form id="loadCompany" name="loadCompany"
																	action="occCompany">
																	<input type="hidden" id="selectedCompany"
																		name="selectedCompany">
																</form:form>

																<div class="gap gap-mini"></div>
																<div class="table-responsive">
																	<table
																		class="table table-striped table-warning table-hover"
																		id="tlPDSTable">
																		<thead>
																			<tr>
																				<th class="firstChild"></th>
																				<th>Occupation Code</th>
																				<th>Description</th>
																				<th>Occupation Class</th>
																				<th>Declined Product 1</th>
																				<th>Declined Product 2</th>
																				<th>Enable</th>
																			</tr>
																		</thead>

																		<tbody>
																			<c:forEach items="${occupationList}" var="element"
																				varStatus="theCount">
																				<tr>
																					<td class="firstChild"><a
																						class="btn btn-warning btn-xs" href="#"
																						data-toggle="modal" data-target="#occupationEdit"
																						onClick="editOccupationInfo('${element.getCompanyId()}','${element.getOccupationCode()}','${element.getDescription()}','${element.getOccupationClass()}','${element.getDeclined1()}','${element.getDeclined2()}','${element.getEnable()}','${element.getVersion()}');"><i
																							class="fa fa-edit"></i> Edit</a></td>
																					<td align="center"><c:out
																							value="${element.getOccupationCode()}" /></td>
																					<td align="center"><c:out
																							value="${element.getDescription()}" /></td>
																					<td align="center"><c:out
																							value="${element.getOccupationClass()}" /></td>
																					<td align="center"><c:out
																							value="${element.getDeclined1()}" /></td>
																					<td align="center"><c:out
																							value="${element.getDeclined2()}" /></td>
																					<td align="center"><c:choose>
																							<c:when test="${element.getEnable()=='Y'}">Yes</c:when>
																							<c:otherwise>No</c:otherwise>
																						</c:choose></td>
																				</tr>
																			</c:forEach>

																		</tbody>
																	</table>
																</div>
															</div>
															<!-- /.the-box -->
															<!-- End warning color table -->
														</div>
													</div>

													<div class="gap gap-mini"></div>
												</div>
											</div>
											<!--row -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.container -->
				<!---------------------------------------------------- BEGIN Modal--------------------------------------------------------- -->
				<div class="col-sm-12">
					<!-- Begin  table -->
					<div class="modal fade" id="occupationEdit" tabindex="-1"
						role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
							<form:form action="updateBuddyPAOccupation" method="post"
								name="updOccupation" id="updOccupation">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<h4 class="modal-title modal-dialog-Title" id="myModalLabel">Buddy
											PA | Occupation Management | Edit Occupation</h4>
									</div>
									<div class="modal-body">
										<div class="form-harizontal">
											<div class="form-group">
												<label class="col-sm-3 control-label">Company</label>
												<div class="col-sm-8">
													<p class="form-control-static">
														<span class="dotSpace">:</span> <input type="text"
															name="updCompany" class="readOnlyText"
															readonly="readonly" id="updCompany" />
													</p>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">Occupation
													Code</label>
												<div class="col-sm-8">
													<p class="form-control-static">
														<span class="dotSpace">:</span> <input type="text"
															name="updOccupationCode" id="updOccupationCode"
															MaxLength="10" /><span class="error leftPadding"
															id="errmsgUCode" style="display: none;">Occupation
															code cannot be empty! </span>
													</p>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">Description</label>
												<div class="col-sm-8">
													<p class="form-control-static">
														<span class="dotSpace">:</span> <input type="text"
															name="updDescription" id="updDescription"
															style="width: 300px;" MaxLength="50" /> <span
															class="error leftPadding" id="errmsgUDesc"
															style="display: none;">Description cannot be
															empty! </span>
													</p>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-3 control-label">Occupation
													Class</label>
												<div class="col-sm-8">
													<p class="form-control-static">
														<span class="dotSpace">:</span> <select
															name="updOccupationClass" id="updOccupationClass">
															<option value="00">00</option>
															<option value="01">01</option>
															<option value="02">02</option>
															<option value="03">03</option>
														</select><span class="error leftPadding" id="errmsgUClass"
															style="display: none;">Occupation class cannot be
															empty! </span>
													</p>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-3 control-label">Declined
													Product 1</label>
												<div class="col-sm-8">
													<p class="form-control-static">
														<span class="dotSpace">:</span> <select
															name="updDeclinedProduct1" id="updDeclinedProduct1">
															<option value="">- Select Product -</option>
															<option value="CPP">CPP</option>
															<option value="TPP">TPP</option>
														</select>
													</p>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-3 control-label">Declined
													Product 2</label>
												<div class="col-sm-8">
													<p class="form-control-static">
														<span class="dotSpace">:</span> <select
															name="declinedProduct2" id="updDeclinedProduct2">
															<option value="">- Select Product -</option>
															<option value="CPP">CPP</option>
															<option value="TPP">TPP</option>
														</select>
													</p>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-3 control-label">Enable</label>
												<div class="col-sm-8">
													<p class="form-control-static minusPaddingTop">
														<span class="dotSpace">:</span> <input type="checkbox"
															name="updOccupationEnable" id="updOccupationEnable"
															class="paddingTopCheckBox" />
													</p>
												</div>
											</div>
										</div>
										<div class="gap gap-mini"></div>
									</div>
								</div>
								<div class="modal-footer">
									<input type="hidden" name="updOccupationVersion"
										id="updOccupationVersion" />
									<button type="button" class="btn btn-default"
										data-dismiss="modal">Cancel</button>
									<a class="btn btn-warning btn-sm" id="btnSave"
										onClick="validateForm('save');"> Save</a>
								</div>
							</form:form>
						</div>
					</div>
				</div>
				<!---------------------------------------------------- END Modal--------------------------------------------------------- -->

			</div>
			<!-- /.page-content -->
			<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
			<jsp:include page="/pageFooter.jsp" />
			<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
		</div>
		<!-- /.wrapper -->
		<!-- END PAGE CONTENT -->

		<!-- BEGIN BACK TO TOP BUTTON -->
		<!--
	        <div id="back-top">
	            <a href="#top"><i class="fa fa-chevron-up"></i></a>
	        </div>
		-->
		<!-- END BACK TO TOP -->
	</div>

	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%--    <jsp:include page="siteFooter.jsp" />   --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->

	<!-- JAVA Script Goes Here -->
	<script
		src="<c:url value="/resources/assets/js/jquery-2.2.4.min.js" />"></script>
	<!--  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
	<script src="assets/js/apps.js"></script>
	<script src="assets/plugins/retina/retina.min.js"></script>
	<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>

	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script src="assets/js/shieldui-all.min.js" type="text/javascript"></script>
	<script src="assets/js/jszip.min.js" type="text/javascript"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
        <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
    <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/buttons.print.min.js"></script>
	<script src="assets/js/buttons.colVis.min.js"></script>
	<script src="assets/js/toastr.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"
		type="text/javascript"></script>
	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript">
		function editOccupationInfo(vCompanyID, vOccupationCode, vDescription,
				vOccupationClass, vDeclinedProduct1, vDeclinedProduct2,
				vEnable, vOccupationVersion) {
			$('#updCompany').val($('#company option:selected').text());
			$('#updOccupationCode').val(vOccupationCode);
			$('#updDescription').val(vDescription);
			$('#updOccupationClass').val(vOccupationClass);
			$('#updDeclinedProduct1').val(vDeclinedProduct1);
			$('#updDeclinedProduct2').val(vDeclinedProduct2);
			if (vEnable == 'Yes' || vEnable == 'Y' || vEnable == 'y'
					|| vEnable == 'yes') {
				$('#updOccupationEnable')[0].checked = true;
			} else {
				$('#updOccupationEnable')[0].checked = false;
			};
			$('#updOccupationVersion').val(vOccupationVersion);
		}

		function validateForm(nm) {
			var tCode = document.getElementById('occupationCode');
			var tClass = document.getElementById('occupationClass');
			var tDesc = document.getElementById('description');

			var uCode = document.getElementById('updOccupationCode');
			var uClass = document.getElementById('updOccupationClass');
			var uDesc = document.getElementById('updDescription');

			if (nm == 'save') {
				if (uCode.value != "" && uClass.value != ""
						&& uDesc.value != "") {
					$('#updOccupation').submit();
					return true;
				} else {
					if(!$.trim($('#updOccupationCode').val()).length) {
						$('#errmsgUCode').show();
					}
					if(!$.trim($('#updDescription').val()).length) {
						$('#errmsgUDesc').show();
					}
					if(!$.trim($('#updOccupationClass').val()).length) {
						$('#errmsgUClass').show();
					}
					alert("Please fill in the mandatory fields before saving.");
					return false;
				}
			} else if (nm == 'add') {
				if (tCode.value != "" && tClass.value != ""
						&& tDesc.value != "") {
					$('#addOccupation').submit();
					return true;
				} else {
					if(!$.trim($('#occupationCode').val()).length) {
						$('#errmsgCode').show();
					}
					if(!$.trim($('#description').val()).length) {
						$('#errmsgDesc').show();
					}
					if(!$.trim($('#occupationClass').val()).length) {
						$('#errmsgClass').show();
					}				
					alert("Please fill in the mandatory fields before adding!");
					return false;
				}
			} else {
				return false;
			}
		}
		
		function submitCompany() {

			var tCompany = document.getElementById('company');
			var tSelectedCompany = document.getElementById('selectedCompany');
			tSelectedCompany.value = tCompany.value;
			$('#loadCompany').submit();
		}
	</script>
	<script type="text/javascript">
		$(document).ready(
				function() {
					// check for empty
					$('#occupationCode').blur(function() {
						if(!$.trim($('#occupationCode').val()).length) {
							$('#errmsgCode').show();
						} else {
							$('#errmsgCode').hide();
						}
					});
					
					$('#description').blur(function() {
						if(!$.trim($('#description').val()).length) {
							$('#errmsgDesc').show();
						} else {
							$('#errmsgDesc').hide();
						}
					});
					
					$('#occupationClass').blur(function() {
						if(!$.trim($('#occupationClass').val()).length) {
							$('#errmsgClass').show();
						} else {
							$('#errmsgClass').hide();
						}
					});
					
					$('#updOccupationCode').blur(function() {
						if(!$.trim($('#updOccupationCode').val()).length) {
							$('#errmsgUCode').show();
						} else {
							$('#errmsgUCode').hide();
						}
					});
					
					$('#updDescription').blur(function() {
						if(!$.trim($('#updDescription').val()).length) {
							$('#errmsgUDesc').show();
						} else {
							$('#errmsgUDesc').hide();
						}
					});
					
					$('#updOccupationClass').blur(function() {
						if(!$.trim($('#updOccupationClass').val()).length) {
							$('#errmsgUClass').show();
						} else {
							$('#errmsgUClass').hide();
						}
					});
					
					// end check for empty
					
					$("#ErrmsgAll").each(function() {
						if ($(this).text() == "") {
							$("#ErrmsgDiv").hide(); // this references the current element in the iteration
							$("#ErrmsgDiv").addClass("errorMsgDisplayF")
						} else
							$("#ErrmsgDiv").addClass("errorMsgDisplayT")

					});

					$("#ErrmsgDiv").click(function() {
						$("#ErrmsgDiv").hide(1000);
					});

					$("#SuccessMsgAll").each(function() {
						if ($(this).text() == "") {
							$("#SuccessMsgDiv").hide(); // this references the current element in the iteration
							$("#SuccessMsgDiv").addClass("errorMsgDisplayF")
						} else
							$("#SuccessMsgDiv").addClass("errorMsgDisplayT")
					});

					$("#SuccessMsgDiv").click(function() {
						$("#SuccessMsgDiv").hide(1000);
					});

					$(".addproduct").click(
							function() {
								// Holds the product ID of the clicked element
								var productId = $(this).attr('class').replace(
										'addproduct ', '');

								addToCart(productId);
							});
				});
	</script>

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
</body>
</html>