<%@ page import="java.util.List"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="icon" href="resources/assets/img/maybankLogoICO.ico"
	type="image/x-icon" />

<link rel="stylesheet" type="text/css"
	href="assets/js/jquery.dataTables.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/buttons.dataTables.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/dataTables.bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/buttons.bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="assets/css/toastr.min.css" />
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="/siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}

#upload-file-selector {
	display: none;
}
</style>

<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}

.modal-dialog {
	width: 700px !important;
}
</style>

<!-- OI CSS (REQUIRED Buddy PA)-->
<link rel="stylesheet" href="assets/css/oiCSS.css" />

<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet"
	href="assets/plugins/datepicker/datepicker.min.css" />
<!-- PLUGINS CSS -->
<link rel="stylesheet"
	href="assets/plugins/owl-carousel/owl.carousel.min.css" />
<link rel="stylesheet"
	href="assets/plugins/owl-carousel/owl.theme.min.css" />
<link rel="stylesheet"
	href="assets/plugins/owl-carousel/owl.transitions.min.css" />

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link rel="stylesheet"
	href="assets/plugins/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet" href="assets/css/owl.css" />
<link rel="stylesheet" href="assets/css/bootstrap.css" />
<link rel="stylesheet" href="assets/css/bootstrap-select.css" />
<link rel="stylesheet"
	href="assets/plugins/datatable/css/bootstrap.datatable.min.css" />
<link rel="stylesheet"
	href="assets/plugins/datatable/css/dataTables.searchHighlight.css" />
<link rel="stylesheet" href="assets/css/menu.css" />
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="libs/html5shiv.js"></script>
    <script src="libs/respond.min.js"></script>
<![endif]-->

<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

form#agentE, form#agentD {
	display: inline-block;
}

form#agentE input[type=submit], form#agentD input[type=submit] {
	padding: 5px;
}
</style>
</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
    -->

	<div class="wrapper inner">
		<!-- START  wrapper inner-->
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="/pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<!-- START page-content inner -->
			<div class="container-fluid black-back">
				<!-- START container -->
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="/menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>

							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Buddy PA | Benefit Management</h4>
															<span>(Admin are able to manage product including
																rates, discount and payment option)</span>
														</div>
													</div>

													<!-- Start Form -->
													<!--   <input type="hidden" name="action" value="listAll" /> -->

													<div class="col-sm-12">
														<!-- Begin  table -->
														<div class="content-inner">
															<div class="the-box full no-border">
																<form:form action="addBuddyPABenefit" method="post"
																	name="addBenefit" id="addBenefit">
																	<div class="title">
																		<div class="sub">
																			<h4>Benefit Add</h4>
																			<div id="ErrmsgDiv"
																				class="errorMsg leftPadding errorMsgDisplayF">
																				<i class="fa fa-exclamation-triangle"></i> <span
																					id="ErrmsgAll">${Errmsg}</span>
																			</div>
																			<div id="SuccessMsgDiv"
																				class="successMsg leftPadding errorMsgDisplayF">
																				<i class="fa fa-exclamation-circle"></i> <span
																					id="SuccessMsgAll">${SuccessMsg}</span>
																			</div>
																		</div>
																	</div>
																	<div>
																		<div class="form-horizontal info-meor">
																			<div class="col-sm-8">
																				<div class="form-group">
																					<label class="col-sm-3 control-label">Company</label>
																					<div class="col-sm-8">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> <select
																								onChange="submitCompany()" name="companyId"
																								id="companyId">
																								<c:forEach items="${companyList}"
																									var="companyList">
																									<option value="${companyList.getName()}"
																										<c:if test="${company==companyList.getName()}">selected</c:if>>${companyList.getDescription()}
																									</option>
																								</c:forEach>
																							</select> <span class="error leftPadding"
																								id="errmsgCompany" style="display: none;">Company
																								cannot be empty! </span>
																						</p>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label">Benefit
																						Name (En)</label>
																					<div class="col-sm-8">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> <input
																								type="text" name="benefitNameEn"
																								id="benefitNameEn" /> <span
																								class="error leftPadding" id="errmsgNameEn"
																								style="display: none;">Benefit Name
																								cannot be empty! </span>
																						</p>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label">Benefit
																						Name (Bm)</label>
																					<div class="col-sm-8">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> <input
																								type="text" name="benefitNameBm"
																								id="benefitNameBm" /> <span
																								class="error leftPadding" id="errmsgNameBm"
																								style="display: none;">Benefit Name
																								cannot be empty! </span>
																						</p>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label">Rate
																						& Eligibility</label>
																					<div class="col-sm-8" class="rateTableDiv">
																						<div class="form-control-static">
																							<table class="rateTable form-control-static"
																								border="1" cellpadding="0" cellspacing="0">
																								<tr height="20" style="background: #E5E7E9;">
																									<td align="center" width="750" colspan="5">Adult</td>
																									<td align="center" width="300" colspan="2">Child</td>
																								</tr>
																								<tr height="20">
																									<td align="center" width="150" rowspan="2">Rate</td>
																									<td align="center" width="300" colspan="2">Insured</td>
																									<td align="center" width="300" colspan="2">Spouse</td>
																									<td align="center" width="150" rowspan="2">Rate</td>
																									<td align="center" width="150" rowspan="2">Eligibility</td>
																								</tr>
																								<tr height="20">
																									<td align="center">Male</td>
																									<td align="center">Female</td>
																									<td align="center">Male</td>
																									<td align="center">Female</td>
																								</tr>
																								<tr height="20">
																									<td align="center"><input type="number"
																										name="adultRate" id="adultRate" min="0"
																										value="0" step=".001" /></td>
																									<td align="center"><input type="checkbox"
																										name="insuredMaleEnable"
																										id="insuredMaleEnable" /></td>
																									<td align="center"><input type="checkbox"
																										name="insuredFemaleEnable"
																										id="insuredFemaleEnable" /></td>
																									<td align="center"><input type="checkbox"
																										name="spouseMaleEnable" id="spouseMaleEnable" /></td>
																									<td align="center"><input type="checkbox"
																										name="spouseFemaleEnable"
																										id="spouseFemaleEnable" /></td>
																									<td align="center"><input type="number"
																										name="childRate" id="childRate" min="0"
																										value="0" step=".001" /></td>
																									<td align="center"><input type="checkbox"
																										name="eligibilityEnable"
																										id="eligibilityEnable" /></td>
																								</tr>
																							</table>
																						</div>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label">Enable</label>
																					<div class="col-sm-8">
																						<p class="form-control-static minusPaddingTop">
																							<span class="dotSpace">:</span> <input
																								type="checkbox" name="benefitEnable"
																								id="benefitEnable" class="paddingTopCheckBox" />
																						</p>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-sm-3 control-label"></label>
																					<div class="col-sm-8">
																						<p class="form-control-static">
																							<span style="color: transparent;">:</span> <a
																								class="btn btn-warning btn-sm" id="btnAdd"
																								onClick="validateForm('add');"><i
																								class="fa fa-plus"></i> Add</a>
																						</p>
																					</div>
																				</div>
																			</div>
																			<div class="gap gap-mini"></div>
																		</div>
																	</div>
																</form:form>
																<form:form id="loadCompany" name="loadCompany"
																	action="blCompany">
																	<input type="hidden" id="selectedCompany"
																		name="selectedCompany">
																</form:form>

																<div class="gap gap-mini"></div>

																<table id="listTable"
																	class="table table-striped table-warning table-hover">
																	<thead style="font-weight: bold;">
																		<tr>
																			<td align="center" rowspan="3" width="30px"></td>
																			<td align="center" rowspan="3">Benefit Name</td>
																			<td align="center" colspan="5">Adult</td>
																			<td align="center" colspan="2">Child</td>
																			<td align="center" rowspan="3">Enable</td>
																		</tr>
																		<tr>
																			<td align="center" rowspan="2">Rate</td>
																			<td align="center" colspan="2">Insured</td>
																			<td align="center" colspan="2">Spouse</td>
																			<td align="center" rowspan="2">Rate</td>
																			<td align="center" rowspan="2">Eligibility</td>
																		</tr>
																		<tr>
																			<td align="center">Male</td>
																			<td align="center">Female</td>
																			<td align="center">Male</td>
																			<td align="center">Female</td>
																		</tr>
																	</thead>
																	<tbody>
																		<c:forEach items="${benefitList}" var="element"
																			varStatus="theCount">
																			<tr>
																				<td rowspan="2" align="center"><a
																					class="btn btn-warning btn-xs" href="#"
																					data-toggle="modal" data-target="#benefitEdit"
																					onClick="editBenefitInfo('${element.getBenefitId()}','${element.getCompanyId()}','${element.getNameEn()}','${element.getNameBm()}'
																						,'${element.getAdultRate()}','${element.getInsuredMElig()}','${element.getInsuredFElig()}'
																						,'${element.getSpouseMElig()}','${element.getSpouseFElig()}','${element.getChildRate()}','${element.getChildElig()}'
																						,'${element.getEnable()}','${element.getVersion()}','${element.getBenefitId()}');"><i
																						class="fa fa-edit"></i> Edit</a></td>
																				<td>(En)<c:out value=" ${element.getNameEn()}" /></td>
																				<td rowspan="2" align="center"><c:out
																						value="${element.getAdultRate()}" /></td>
																				<td rowspan="2" align="center"><c:choose>
																						<c:when test="${element.getInsuredMElig()=='Y'}">Yes</c:when>
																						<c:otherwise>No</c:otherwise>
																					</c:choose></td>
																				<td rowspan="2" align="center"><c:choose>
																						<c:when test="${element.getInsuredFElig()=='Y'}">Yes</c:when>
																						<c:otherwise>No</c:otherwise>
																					</c:choose></td>
																				<td rowspan="2" align="center"><c:choose>
																						<c:when test="${element.getSpouseMElig()=='Y'}">Yes</c:when>
																						<c:otherwise>No</c:otherwise>
																					</c:choose></td>
																				<td rowspan="2" align="center"><c:choose>
																						<c:when test="${element.getSpouseFElig()=='Y'}">Yes</c:when>
																						<c:otherwise>No</c:otherwise>
																					</c:choose></td>
																				<td rowspan="2" align="center"><c:out
																						value="${element.getChildRate()}" /></td>
																				<td rowspan="2" align="center"><c:choose>
																						<c:when test="${element.getChildElig()=='Y'}">Yes</c:when>
																						<c:otherwise>No</c:otherwise>
																					</c:choose></td>
																				<td rowspan="2" align="center"><c:choose>
																						<c:when test="${element.getEnable()=='Y'}">Yes</c:when>
																						<c:otherwise>No</c:otherwise>
																					</c:choose></td>
																			</tr>
																			<tr>
																				<td>(Bm)<c:out value=" ${element.getNameBm()}" /></td>
																			</tr>
																		</c:forEach>
																	</tbody>
																</table>
															</div>
															<!-- /.the-box -->
															<!-- End warning color table -->
														</div>
													</div>
													<div class="gap gap-mini"></div>
												</div>
											</div>
											<!--row -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.container -->


				<!---------------------------------------------------- BEGIN Modal--------------------------------------------------------- -->
				<div class="col-sm-12">
					<!-- Begin  table -->
					<div class="modal fade" id="benefitEdit" tabindex="-1"
						role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
							<form:form action="updateBuddyPABenefit" method="post"
								name="updBenefit" id="updBenefit">
								<input type="hidden" name="updBenefitVersion"
									id="updBenefitVersion" />
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<h4 class="modal-title modal-dialog-Title" id="myModalLabel">Buddy
											PA | Benefit Management | Benefit Edit</h4>
									</div>
									<div class="modal-body">
										<div class="form-harizontal">
											<div class="form-group">
												<label class="col-sm-3 control-label">Company</label>
												<div class="col-sm-8">
													<p class="form-control-static">
														<span class="dotSpace">:</span> <input type="text"
															name="updCompany" id="updCompany" readonly="readonly"
															class="readOnlyText" />
													</p>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-3 control-label">Benefit Name
													(En)</label>
												<div class="col-sm-8">
													<p class="form-control-static">
														<span class="dotSpace">:</span> <input type="hidden"
															name="updBenefitID" id="updBenefitID" /> <input
															type="text" name="updBenefitNameEn" id="updBenefitNameEn" />
														<span class="error leftPadding" id="errmsgUpdNameEn"
															style="display: none;">Benefit Name cannot be
															empty!</span>
													</p>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-3 control-label">Benefit Name
													(Bm)</label>
												<div class="col-sm-8">
													<p class="form-control-static">
														<span class="dotSpace">:</span> <input type="text"
															name="updBenefitNameBm" id="updBenefitNameBm" /> <span
															class="error leftPadding" id="errmsgUpdNameBm"
															style="display: none;">Benefit Name cannot be
															empty!</span>
													</p>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-3 control-label">Rate &
													Eligibility</label>
												<div class="col-sm-8" class="modalRateTableDiv">
													<div class="form-control-static">
														<table class="rateTable form-control-static" border="1"
															cellpadding="0" cellspacing="0">
															<tr height="20" style="background: #E5E7E9;">
																<td align="center" width="750" colspan="5">Adult</td>
																<td align="center" width="300" colspan="2">Child</td>
															</tr>
															<tr height="20">
																<td align="center" width="150" rowspan="2">Rate</td>
																<td align="center" width="300" colspan="2">Insured</td>
																<td align="center" width="300" colspan="2">Spouse</td>
																<td align="center" width="150" rowspan="2">Rate</td>
																<td align="center" width="150" rowspan="2">Eligibility</td>
															</tr>
															<tr height="20">
																<td align="center">Male</td>
																<td align="center">Female</td>
																<td align="center">Male</td>
																<td align="center">Female</td>
															</tr>
															<tr height="20">
																<td align="center"><input type="number"
																	name="updAdultRate" id="updAdultRate" min="0" value="0"
																	step=".001" /></td>
																<td align="center"><input type="checkbox"
																	name="updInsuredMaleEnable" id="updInsuredMaleEnable" /></td>
																<td align="center"><input type="checkbox"
																	name="updInsuredFemaleEnable"
																	id="updInsuredFemaleEnable" /></td>
																<td align="center"><input type="checkbox"
																	name="updSpouseMaleEnable" id="updSpouseMaleEnable" /></td>
																<td align="center"><input type="checkbox"
																	name="updSpouseFemaleEnable" id="updSpouseFemaleEnable" /></td>
																<td align="center"><input type="number"
																	name="updChildRate" id="updChildRate" min="0" value="0"
																	step=".001" /></td>
																<td align="center"><input type="checkbox"
																	name="updEligibilityEnable" id="updEligibilityEnable" /></td>
															</tr>
														</table>
													</div>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-3 control-label">Enable</label>
												<div class="col-sm-8">
													<p class="form-control-static minusPaddingTop">
														<span class="dotSpace">:</span> <input type="checkbox"
															name="updBenefitEnable" id="updBenefitEnable"
															class="paddingTopCheckBox" />
													</p>
												</div>
											</div>
										</div>
										<div class="gap gap-mini"></div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default"
										data-dismiss="modal">Cancel</button>
									<a class="btn btn-warning btn-sm" id="btnSave"
										onClick="validateForm('save');"><i class="fa fa-save"></i>
										Save</a>
								</div>
							</form:form>
						</div>
					</div>
				</div>
				<!---------------------------------------------------- END Modal--------------------------------------------------------- -->

			</div>
			<!-- END container -->
			<!-- /.page-content -->
			<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
			<jsp:include page="/pageFooter.jsp" />
			<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
		</div>
		<!-- END page-content inner -->

		<!-- END PAGE CONTENT -->


	</div>
	<!--END  wrapper inner-->

	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%--    <jsp:include page="siteFooter.jsp" />   --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->


	<!-- JAVA Script Goes Here -->

	<script
		src="<c:url value="/resources/assets/js/jquery-2.2.4.min.js" />"></script>
	<!--  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
	<script src="assets/js/apps.js"></script>
	<script src="assets/plugins/retina/retina.min.js"></script>
	<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>


	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script src="assets/js/shieldui-all.min.js" type="text/javascript"></script>
	<script src="assets/js/jszip.min.js" type="text/javascript"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
    	<script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
    <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/buttons.print.min.js"></script>
	<script src="assets/js/buttons.colVis.min.js"></script>
	<script src="assets/js/toastr.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"
		type="text/javascript"></script>
	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<script type="text/javascript">
		function editBenefitInfo(vBenefitID, vCompanyID, vBenefitNameEn, vBenefitNameBm,
				vAdultRate, vInsuredMElig, vInsuredFElig, vSpouseMElig, 
				vSpouseFElig, vChildRate, vChildElig, vEnable, vVersion, vBenefitID) {
			//alert(value);  //for testing purposes only
			var txt1 = document.getElementById('updCompany');
			var txt2 = document.getElementById('updBenefitNameEn');
			var txt3 = document.getElementById('updBenefitNameBm');
			var txt5 = document.getElementById('updAdultRate');
			var txt6 = document.getElementById('updInsuredMaleEnable');
			var txt7 = document.getElementById('updInsuredFemaleEnable');
			var txt8 = document.getElementById('updSpouseMaleEnable');
			var txt9 = document.getElementById('updSpouseFemaleEnable');
			var txt10 = document.getElementById('updChildRate');
			var txt11 = document.getElementById('updEligibilityEnable');
			var txt12 = document.getElementById('updBenefitEnable');
			var txt13 = document.getElementById('updBenefitVersion');
			var txt14 = document.getElementById('updBenefitID');			
			
			//txt1.disabled = true;  //cannot retrieve data when disabled
			txt1.value = vCompanyID;
			txt2.value = vBenefitNameEn;
			txt3.value = vBenefitNameBm;
			txt5.value = vAdultRate;

			if (vInsuredMElig == 'Yes' || vInsuredMElig == 'Y'
					|| vInsuredMElig == 'y' || vInsuredMElig == 'yes') {
				txt6.checked = true;
			} else {
				txt6.checked = false;
			}
			if (vInsuredFElig == 'Yes' || vInsuredFElig == 'Y'
					|| vInsuredFElig == 'y' || vInsuredFElig == 'yes') {
				txt7.checked = true;
			} else {
				txt7.checked = false;
			}
			if (vSpouseMElig == 'Yes' || vSpouseMElig == 'Y'
					|| vSpouseMElig == 'y' || vSpouseMElig == 'yes') {
				txt8.checked = true;
			} else {
				txt8.checked = false;
			}
			if (vSpouseFElig == 'Yes' || vSpouseFElig == 'Y'
					|| vSpouseFElig == 'y' || vSpouseFElig == 'yes') {
				txt9.checked = true;
			} else {
				txt9.checked = false;
			}

			txt10.value = vChildRate;
			if (vChildElig == 'Yes' || vChildElig == 'Y' || vChildElig == 'y'
					|| vChildElig == 'yes') {
				txt11.checked = true;
			} else {
				txt11.checked = false;
			}

			if (vEnable == 'Yes' || vEnable == 'Y' || vEnable == 'y'
					|| vEnable == 'yes') {
				txt12.checked = true;
			} else {
				txt12.checked = false;
			}

			txt13.value = vVersion;
			txt14.value = vBenefitID;
		}

		function validateForm(fnc) {
			if (fnc == 'add') {
				if ($.trim($('#benefitNameEn').val()).value != "" && $.trim($('#benefitNameBm').val()).value != "") {
					$('#addBenefit').submit();
					return true;
				} else {
					if (!$.trim($('#benefitNameEn').val()).length) {
						$('#errmsgNameEn').show();
					} else {
						$('#errmsgNameEn').hide();
					}

					if (!$.trim($('#benefitNameBm').val()).length) {
						$('#errmsgNameBm').show();
					} else {
						$('#errmsgNameBm').hide();
					}
				}
			} else if (fnc == 'save') {
				if ($.trim($('#updBenefitNameEn').val()) != "" && $.trim($('#updBenefitNameBm').val()) != "") {
					$('#updBenefit').submit();
					return true;
				} else {
					if (!$.trim($('#updBenefitNameEn').val()).length) {
						$('#errmsgUpdNameEn').show();
					} else {
						$('#errmsgUpdNameEn').hide();
					}

					if (!$.trim($('#updBenefitNameBm').val()).length) {
						$('#errmsgUpdNameBm').show();
					} else {
						$('#errmsgUpdNameBm').hide();
					}
					alert("Please fill the mandatory fields!");
				}
			}
		}
		
		function submitCompany() {

			var tCompany = document.getElementById('companyId');
			var tSelectedCompany = document.getElementById('selectedCompany');
			tSelectedCompany.value = tCompany.value;
			$('#loadCompany').submit();
		}
	</script>

	<script type="text/javascript">
		$(document).ready(
				function() {
					// check for empty
					$('#benefitNameEn').blur(function() {
						if (!$.trim($('#benefitNameEn').val()).length) {
							$('#errmsgNameEn').show();
						} else {
							$('#errmsgNameEn').hide();
						}
					});

					$('#benefitNameBm').blur(function() {
						if (!$.trim($('#benefitNameBm').val()).length) {
							$('#errmsgNameBm').show();
						} else {
							$('#errmsgNameBm').hide();
						}
					});

					$('#updBenefitNameEn').blur(function() {
						if (!$.trim($('#updBenefitNameEn').val()).length) {
							$('#errmsgUpdNameEn').show();
						} else {
							$('#errmsgUpdNameEn').hide();
						}
					});

					$('#updBenefitNameBm').blur(function() {
						if (!$.trim($('#updBenefitNameBm').val()).length) {
							$('#errmsgUpdNameBm').show();
						} else {
							$('#errmsgUpdNameBm').hide();
						}
					});
					// end check for empty

					$("#ErrmsgAll").each(function() {
						if ($(this).text() == "") {
							$("#ErrmsgDiv").hide(); // this references the current element in the iteration
							$("#ErrmsgDiv").addClass("errorMsgDisplayF")
						} else
							$("#ErrmsgDiv").addClass("errorMsgDisplayT")

					});

					$("#ErrmsgDiv").click(function() {
						$("#ErrmsgDiv").hide(1000);
					});

					$("#SuccessMsgAll").each(function() {
						if ($(this).text() == "") {
							$("#SuccessMsgDiv").hide(); // this references the current element in the iteration
							$("#SuccessMsgDiv").addClass("errorMsgDisplayF")
						} else
							$("#SuccessMsgDiv").addClass("errorMsgDisplayT")
					});

					$("#SuccessMsgDiv").click(function() {
						$("#SuccessMsgDiv").hide(1000);
					});

					$(".addproduct").click(
							function() {
								// Holds the product ID of the clicked element
								var productId = $(this).attr('class').replace(
										'addproduct ', '');

								addToCart(productId);
							});
				});
	</script>

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
</body>
</html>