<%@ page import="java.util.List"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="icon" href="resources/assets/img/maybankLogoICO.ico"
	type="image/x-icon" />

<link rel="stylesheet" type="text/css"
	href="assets/js/jquery.dataTables.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/buttons.dataTables.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/dataTables.bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/buttons.bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="assets/css/toastr.min.css" />
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="/siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}

#upload-file-selector {
	display: none;
}
</style>

<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}

.modal-dialog {
	width: 700px !important;
}
</style>

<!-- OI CSS (REQUIRED Buddy PA)-->
<link rel="stylesheet" href="assets/css/oiCSS.css" />

<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet"
	href="assets/plugins/datepicker/datepicker.min.css" />
<!-- PLUGINS CSS -->
<link rel="stylesheet"
	href="assets/plugins/owl-carousel/owl.carousel.min.css" />
<link rel="stylesheet"
	href="assets/plugins/owl-carousel/owl.theme.min.css" />
<link rel="stylesheet"
	href="assets/plugins/owl-carousel/owl.transitions.min.css" />

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link rel="stylesheet"
	href="assets/plugins/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet" href="assets/css/owl.css" />
<link rel="stylesheet" href="assets/css/bootstrap.css" />
<link rel="stylesheet" href="assets/css/bootstrap-select.css" />
<link rel="stylesheet"
	href="assets/plugins/datatable/css/bootstrap.datatable.min.css" />
<link rel="stylesheet"
	href="assets/plugins/datatable/css/dataTables.searchHighlight.css" />
<link rel="stylesheet" href="assets/css/menu.css" />
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="libs/html5shiv.js"></script>
    <script src="libs/respond.min.js"></script>
<![endif]-->

<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

form#agentE, form#agentD {
	display: inline-block;
}

form#agentE input[type=submit], form#agentD input[type=submit] {
	padding: 5px;
}
</style>
</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
    -->

	<div class="wrapper inner">
		<!-- START  wrapper inner-->
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="/pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<!-- START page-content inner -->
			<div class="container-fluid black-back">
				<!-- START container -->
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="/menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>

							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Buddy PA | Benefit Management</h4>
															<span>(Admin are able to manage product including
																rates, discount and payment option)</span>
														</div>
													</div>

													<!-- Start Form -->
													<!-- <input type="hidden" name="action" value="listAll" /> -->
													<div class="col-sm-12">
														<!-- Begin  table -->
														<div class="content-inner">
															<div class="the-box full no-border">
																<form:form action="updBuddyPABenefitList" method="post"
																	name="addBenefitGrp" id="addBenefitGrp">
																	<div class="title">
																		<div class="sub">
																			<h4>Benefit Grouping Add</h4>
																			<div id="ErrmsgDiv"
																				class="errorMsg leftPadding errorMsgDisplayF">
																				<i class="fa fa-exclamation-triangle"></i> <span
																					id="ErrmsgAll">${Errmsg}</span>
																			</div>
																			<div id="SuccessMsgDiv"
																				class="successMsg leftPadding errorMsgDisplayF">
																				<i class="fa fa-exclamation-circle"></i> <span
																					id="SuccessMsgAll">${SuccessMsg}</span>
																			</div>
																		</div>
																	</div>
																	<div>
																		<div class="form-horizontal info-meor">
																			<div class="col-sm-8">
																				<div class="form-group">
																					<label class="col-sm-3 control-label">Company</label>
																					<div class="col-sm-8">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span><select
																								id="ddlCompanyList" name="ddlCompanyList"
																								onChange="submitCompany()">
																								<c:forEach items="${companyList}"
																									var="companylist">
																									<option value="${companylist.getName()}"
																										<c:if test="${company==companylist.getName()}">selected</c:if>>${companylist.getDescription()}
																									</option>
																								</c:forEach>
																							</select>
																						</p>
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-sm-3 control-label">Combo
																						Name</label>
																					<div class="col-sm-8">
																						<p class="form-control-static">

																							<span class="dotSpace">:</span><select
																								id="ddlComboList" name="ddlComboList"
																								onChange="submitCompany()">
																								<c:forEach items="${comboList}" var="comboID">
																									<option value="${comboID.getReservedId()}"
																										<c:if test="${combo==comboID.getReservedId()}">selected</c:if>>${comboID.getName()}
																									</option>
																								</c:forEach>
																							</select><input type="hidden" name="comboId" id="comboId"
																								value="${comboId}">
																						</p>
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-sm-3 control-label">Plan
																						Name</label>
																					<div class="col-sm-8">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span><select
																								id="ddlPlanList" name="ddlPlanList"
																								onChange="submitCompany()"><c:forEach
																									items="${planList}" var="planID">
																									<option value="${planID.getPlanId()}"
																										<c:if test="${plan==planID.getPlanId()}">selected</c:if>>${planID.getNameEn()}
																									</option>
																								</c:forEach></select>
																						</p>
																					</div>
																				</div>

																				<div class="form-group">
																					<div>
																						<label class="col-sm-3 control-label"></label>
																						<div class="col-sm-8"
																							style="border: solid 1px black; margin-left: 15px;">
																							<div class="col-sm-6">
																								<input type="hidden" id="bgCount" name="bgCount"
																									value="${benefitListCount}">
																								<c:forEach items="${benefitList}"
																									var="benefitList" varStatus="benefitCount">
																									<table>
																										<tr>
																											<td><c:if
																													test="${(benefitCount.index%2) == 0}">
																													<input type="checkbox"
																														name="bnf_${benefitCount.index}"
																														id="bnf_${benefitCount.index}"
																														<c:if test="${benefitList.getEnable() == 'Y' }">checked="true"</c:if>
																														value="${benefitList.getBenefitId()}">${benefitList.getNameEn()}
																												</c:if></td>
																										</tr>
																									</table>
																								</c:forEach>
																							</div>

																							<div class="col-sm-6">
																								<c:forEach items="${benefitList}"
																									var="benefitList" varStatus="benefitCount">
																									<table>
																										<tr>
																											<td><c:if
																													test="${(benefitCount.index%2) != 0}">
																													<input type="checkbox"
																														name="bnf_${benefitCount.index}"
																														id="bnf_${benefitCount.index}"
																														<c:if test="${benefitList.getEnable() == 'Y' }">checked="true"</c:if>
																														value="${benefitList.getBenefitId()}">${benefitList.getNameEn()}
																												</c:if></td>
																										</tr>
																									</table>
																								</c:forEach>
																							</div>
																						</div>
																					</div>
																				</div>

																				<div class="form-group">
																					<div class="form-group text-left">
																						<label class="col-sm-3 control-label"></label>
																						<div class="col-sm-8">
																							<p class="form-control-static">
																								<a class="btn btn-warning btn-sm" id="btnAdd"
																									onClick="validateForm();"><i
																									class="fa fa-plus"></i> Add</a>
																							</p>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="gap gap-mini"></div>
																		</div>
																	</div>
																</form:form>
																<form:form id="test" name="test">
																	<input type="hidden" id="bgCompanyId"
																		name="bgCompanyId">
																	<input type="hidden" id="bgComboId" name="bgComboId">
																	<input type="hidden" id="bgPlanId" name="bgPlanId">
																</form:form>
																<div class="gap gap-mini"></div>
																<div class="table-responsive text-center">
																	<table id="listTable"
																		class="table table-striped table-warning table-hover"
																		style="font-size: small;">
																		<thead style="font-weight: bold;">
																			<tr>
																				<td align="center">Plan</td>
																				<c:forEach items="${premiumTotalList}"
																					var="headerList" varStatus="theCount">
																					<td align="center" colspan="5"><c:out
																							value="${headerList.getPlanName()}"></c:out> (<c:out
																							value="${headerList.getPlanId()}"></c:out>)</td>
																					<td align="center"><a
																						class="btn btn-warning btn-xs" href="#"
																						onClick="editBenefitGrp('${headerList.getPlanId()}','${headerList.getPlanName()}');"><i
																							class="fa fa-edit"></i> Edit</a></td>

																				</c:forEach>
																			</tr>
																			<tr>
																				<td align="center" rowspan="2">Type of Benefits</td>
																				<c:forEach var="i" begin="1"
																					end="${benefitGrpListCount}">
																					<td align="center" colspan="2">Sum Insured/<br>Limit
																						(RM)
																					</td>
																					<td align="center" colspan="2">Loading (%)</td>
																					<td align="center" colspan="2">Premium per<br>
																						person (RM)
																					</td>
																				</c:forEach>
																			</tr>
																			<tr>
																				<c:forEach var="i" begin="1"
																					end="${benefitGrpListCount}">
																					<td align="center">Adult</td>
																					<td align="center">Child</td>
																					<td align="center">Adult</td>
																					<td align="center">Child</td>
																					<td align="center">Adult</td>
																					<td align="center">Child</td>
																				</c:forEach>
																			</tr>
																		</thead>
																		<tbody>
																			<c:forEach items="${benefitListDist}" var="element"
																				varStatus="theCount">
																				<tr>
																					<td align="center" style="white-space: nowrap;"><c:out
																							value="${element.getNameEn()}" /></td>
																					<c:forEach items="${premiumTotalList}"
																						var="headerList" varStatus="headerCount">
																						<c:forEach items="${benefitGrpList}" var="list"
																							varStatus="listCount">
																							<c:if
																								test="${list.getBenefitId() == element.getBenefitId()}">
																								<c:if
																									test="${headerList.getPlanId() == list.getPlanId()}">
																									<td align="center"><c:choose>
																											<c:when
																												test="${list.getAdultSumInsured() != null }">
																												<fmt:formatNumber type="number"
																													pattern="#,##0.00"
																													value="${list.getAdultSumInsured()}" />
																											</c:when>
																											<c:otherwise>-</c:otherwise>
																										</c:choose></td>
																									<td align="center"><c:choose>
																											<c:when
																												test="${list.getChildSumInsured() != null }">
																												<fmt:formatNumber type="number"
																													pattern="#,##0.00"
																													value="${list.getChildSumInsured()}" />
																											</c:when>
																											<c:otherwise>-</c:otherwise>
																										</c:choose></td>
																									<td align="center"><c:choose>
																											<c:when
																												test="${list.getAdultLoading() != null }">
																												<fmt:formatNumber type="percent"
																													pattern="0.00"
																													value="${list.getAdultLoading()}" />
																											</c:when>
																											<c:otherwise>-</c:otherwise>
																										</c:choose></td>
																									<td align="center"><c:choose>
																											<c:when
																												test="${list.getChildLoading() != null }">
																												<fmt:formatNumber type="percent"
																													pattern="0.00"
																													value="${list.getChildLoading()}" />
																											</c:when>
																											<c:otherwise>-</c:otherwise>
																										</c:choose></td>
																									<td align="center"><c:choose>
																											<c:when
																												test="${list.getAdultPremium() != null }">
																												<fmt:formatNumber type="number"
																													pattern="#,##0.00"
																													value="${list.getAdultPremium()}" />
																											</c:when>
																											<c:otherwise>-</c:otherwise>
																										</c:choose></td>
																									<td align="center"><c:choose>
																											<c:when
																												test="${list.getChildPremium() != null }">
																												<fmt:formatNumber type="number"
																													pattern="#,##0.00"
																													value="${list.getChildPremium()}" />
																											</c:when>
																											<c:otherwise>-</c:otherwise>
																										</c:choose></td>
																								</c:if>
																							</c:if>
																						</c:forEach>
																					</c:forEach>
																				</tr>
																			</c:forEach>
																			<tr>
																				<td style="background: white; border: none;">&nbsp;</td>
																			</tr>
																			<tr>
																				<td align="center"
																					style="background: white; border: none; white-space: nowrap;">Provision
																					Premium/Contribution(per person)</td>
																				<c:forEach items="${premiumTotalList}" var="total"
																					varStatus="totalCount">
																					<td align="center"
																						style="background: white; border: none;"
																						colspan="4"></td>
																					<td align="center"
																						style="background: white; border-left: none; border-right: none; border-bottom: 2px solid #ddd;"><c:choose>
																							<c:when
																								test="${total.getAdultPremium() != null }">
																								<fmt:formatNumber type="number"
																									pattern="#,##0.00"
																									value="${total.getAdultPremium()}" />
																							</c:when>
																							<c:otherwise>-</c:otherwise>
																						</c:choose></td>
																					<td align="center"
																						style="background: white; border-left: none; border-right: none; border-bottom: 2px solid #ddd;"><c:choose>
																							<c:when
																								test="${total.getChildPremium() != null }">
																								<fmt:formatNumber type="number"
																									pattern="#,##0.00"
																									value="${total.getChildPremium()}" />
																							</c:when>
																							<c:otherwise>-</c:otherwise>
																						</c:choose></td>
																				</c:forEach>
																			</tr>
																		</tbody>
																	</table>

																	<table>
																	</table>
																</div>
															</div>
															<!-- /.the-box -->
															<!-- End warning color table -->
														</div>
													</div>

													<div class="gap gap-mini"></div>


												</div>
											</div>
											<!--row -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!---------------------------------------------------- BEGIN Modal--------------------------------------------------------- -->
					<!-- 				<div class="col-sm-12"> -->
					<!-- 					Begin  table -->
					<!-- 					<div class="modal fade" id="comboMatrixEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"> -->
					<!-- 						<div class="modal-dialog" role="document"> -->
					<%-- 							<form:form action="updateBuddyPAComboMatrix" method="post" name="updComboMatrix" id="updComboMatrix"> --%>
					<!-- 								<input type="hidden" name="updComboMatrixVersion" id="updComboMatrixVersion" /> -->
					<!-- 								<div class="modal-content"> -->
					<!-- 									<div class="modal-header"> -->
					<!-- 										<button type="button" class="close" data-dismiss="modal" aria-label="Close"> -->
					<!-- 											<span aria-hidden="true">&times;</span> -->
					<!-- 										</button> -->
					<!-- 										<h4 class="modal-title" id="myModalLabel">Buddy PA | Combo Matrix Edit</h4> -->
					<!-- 									</div> -->
					<!-- 									<div class="modal-body"> -->
					<!-- 										<div class="form-harizontal">											 -->
					<!-- 											<div class="form-group"> -->
					<!-- 												<label class="col-sm-3 control-label">Company</label> -->
					<!-- 												<div class="col-sm-8"> -->
					<!-- 													<p class="form-control-static"> -->
					<!-- 														<span class="dotSpace">:</span> -->
					<!-- 														<input type="text" name="updCompany" id="updCompany"  -->
					<!-- 															readonly="readonly" class="readOnlyText" /> -->
					<!-- 													</p> -->
					<!-- 												</div> -->
					<!-- 											</div> -->

					<!-- 											<div class="form-group"> -->
					<!-- 												<label class="col-sm-3 control-label">Combo Name</label> -->
					<!-- 												<div class="col-sm-8"> -->
					<!-- 													<p class="form-control-static"> -->
					<!-- 														<span class="dotSpace">:</span> -->
					<!-- 														<input type="text" name="updComboID" id="updComboID" /> -->
					<!-- 													</p> -->
					<!-- 												</div> -->
					<!-- 											</div> -->

					<!-- 											<div class="form-group"> -->
					<!-- 												<label class="col-sm-3 control-label">Insured</label> -->
					<!-- 												<div class="col-sm-8"> -->
					<!-- 													<p class="form-control-static"> -->
					<!-- 														<span class="dotSpace">:</span> -->
					<!-- 														<span class="spanWord">Plan ID</span> -->
					<!-- 														<select name="updInsuredPlanId" id="updInsuredPlanId"> -->
					<%-- 															<c:forEach items="${premiumClassList}" var="PREMIUM_CLASS"> --%>
					<%-- 																<option value="${PREMIUM_CLASS.getName()}">${PREMIUM_CLASS.getPremiumClass()} --%>
					<!-- 																</option> -->
					<%-- 															</c:forEach> --%>
					<!-- 														</select> -->

					<!-- 														<span class="spanCount">Count</span> -->
					<!-- 														<input type="number" name="updInsuredCount" id="updInsuredCount" /> -->
					<!-- 													</p> -->
					<!-- 												</div> -->
					<!-- 											</div> -->

					<!-- 											<div class="form-group"> -->
					<!-- 												<label class="col-sm-3 control-label">Spouse</label> -->
					<!-- 												<div class="col-sm-8"> -->
					<!-- 													<p class="form-control-static"> -->
					<!-- 														<span class="dotSpace">:</span> -->
					<!-- 														<span class="spanWord">Plan ID</span> -->
					<!-- 														<select name="updSpousePlanId" id="updSpousePlanId"> -->
					<%-- 															<c:forEach items="${premiumClassList}" var="PREMIUM_CLASS"> --%>
					<%-- 																<option value="${PREMIUM_CLASS.getName()}">${PREMIUM_CLASS.getPremiumClass()} --%>
					<!-- 																</option> -->
					<%-- 															</c:forEach> --%>
					<!-- 														</select> -->

					<!-- 														<span class="spanCount">Count</span> -->
					<!-- 														<input type="number" name="updSpouseCount" id="updSpouseCount" /> -->
					<!-- 													</p> -->
					<!-- 												</div> -->
					<!-- 											</div> -->

					<!-- 											<div class="form-group"> -->
					<!-- 												<label class="col-sm-3 control-label">Child</label> -->
					<!-- 												<div class="col-sm-8"> -->
					<!-- 													<p class="form-control-static"> -->
					<!-- 														<span class="dotSpace">:</span> -->
					<!-- 														<span class="spanWord">Plan ID</span> -->
					<!-- 														<select name="updChildPlanId" id="updChildPlanId"> -->
					<%-- 															<c:forEach items="${premiumClassList}" var="PREMIUM_CLASS"> --%>
					<%-- 																<option value="${PREMIUM_CLASS.getName()}">${PREMIUM_CLASS.getPremiumClass()} --%>
					<!-- 																</option> -->
					<%-- 															</c:forEach> --%>
					<!-- 														</select> -->

					<!-- 														<span class="spanCount">Count</span> -->
					<!-- 														<input type="number" name="updChildCount" id="updChildCount" /> -->
					<!-- 													</p> -->
					<!-- 												</div> -->
					<!-- 											</div> -->

					<!-- 											<div class="form-group"> -->
					<!-- 												<label class="col-sm-3 control-label">Combo Matrix ID</label> -->
					<!-- 												<div class="col-sm-8"> -->
					<!-- 													<p class="form-control-static"> -->
					<!-- 														<span class="dotSpace">:</span> -->
					<!-- 														<input type="text" name="updComboMatrixID" id="updComboMatrixID"  -->
					<!-- 															readonly="readonly" class="readOnlyText" /> -->
					<!-- 													</p> -->
					<!-- 												</div> -->
					<!-- 											</div> -->

					<!-- 											<div class="form-group"> -->
					<!-- 												<label class="col-sm-3 control-label">Enable</label> -->
					<!-- 												<div class="col-sm-8"> -->
					<!-- 													<p class="form-control-static"> -->
					<!-- 														<span class="dotSpace">:</span> -->
					<!-- 														<input type="checkbox" name="updComboMatrixEnable" id="updComboMatrixEnable" /> -->
					<!-- 													</p> -->
					<!-- 												</div> -->
					<!-- 											</div> -->
					<!-- 										</div> -->
					<!-- 										<div class="gap gap-mini"></div> -->
					<!-- 									</div> -->
					<!-- 								</div> -->
					<!-- 								<div class="modal-footer"> -->
					<!-- 									<button type="button" class="btn btn-default" -->
					<!-- 										data-dismiss="modal">Cancel</button> -->

					<!-- 									<input class="btn btn-warning btn-sm" type="submit" name="Save" -->
					<!-- 										value="Save" /> -->

					<!-- 								</div> -->
					<%-- 							</form:form> --%>
					<!-- 						</div> -->
					<!-- 					</div> -->
					<!-- 				</div> -->
					<!---------------------------------------------------- END Modal--------------------------------------------------------- -->
				</div>
				<!-- /.container -->
			</div>
			<!-- /.page-content -->
			<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
			<jsp:include page="/pageFooter.jsp" />
			<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
		</div>
		<!-- /.wrapper -->
		<!-- END PAGE CONTENT -->



		<!-- BEGIN BACK TO TOP BUTTON -->
		<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
		<!-- END BACK TO TOP -->
	</div>

	<!--END  wrapper inner-->

	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%--    <jsp:include page="siteFooter.jsp" />   --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->


	<!-- JAVA Script Goes Here -->

	<script
		src="<c:url value="/resources/assets/js/jquery-2.2.4.min.js" />"></script>
	<!--  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
	<script src="assets/js/apps.js"></script>
	<script src="assets/plugins/retina/retina.min.js"></script>
	<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>


	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script src="assets/js/shieldui-all.min.js" type="text/javascript"></script>
	<script src="assets/js/jszip.min.js" type="text/javascript"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
    	<script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
    <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/buttons.print.min.js"></script>
	<script src="assets/js/buttons.colVis.min.js"></script>
	<script src="assets/js/toastr.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"
		type="text/javascript"></script>
	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

	<script type="text/javascript">
		function editBenefitInfo(vPlanID, vPlanName, lst) {
			var oCompanyId = document.getElementById('oriCompanyId');
			var oComboId = document.getElementById('oriComboId');
			var lCompanyId = document.getElementById('ddlCompanyList');
			var lComboId = document.getElementById('ddlComboList');
			var oPlanId = document.getElementById('oriPlanId');
			var oPlanName = document.getElementById('oriPlanName');
			oCompanyId.value = lCompanyId.value;
			oComboId.value = lComboId.value;
			oPlanId.value = vPlanID;
			oPlanName.innerHTML = vPlanName;
		}

		function validateForm() {
			$('#addBenefitGrp').submit();
		}

		function submitCompany() {

			var t1 = document.getElementById('ddlCompanyList');
			var t2 = document.getElementById('ddlComboList');
			var t4 = document.getElementById('bgCompanyId');
			var t5 = document.getElementById('bgComboId');
			var t6 = document.getElementById('ddlPlanList');
			var t7 = document.getElementById('bgPlanId');
			t4.value = t1.value;
			t5.value = t2.value;
			t7.value = t6.value;
			$('#test').attr("action", "bgCompany");
			$('#test').submit();
		}

		function editBenefitGrp(vPlanID, vPlanName) {

			var t1 = document.getElementById('ddlCompanyList');
			var t2 = document.getElementById('ddlComboList');
			var t4 = document.getElementById('bgCompanyId');
			var t5 = document.getElementById('bgComboId');
			var t7 = document.getElementById('bgPlanId');
			t4.value = t1.value;
			t5.value = t2.value;
			t7.value = vPlanID;
			$('#test').attr("action", "bgEdit");
			$('#test').submit();
		}
	</script>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript">
		$(document).ready(
				function() {
					$("#ErrmsgAll").each(function() {
						if ($(this).text() == "") {
							$("#ErrmsgDiv").hide(); // this references the current element in the iteration
							$("#ErrmsgDiv").addClass("errorMsgDisplayF")
						} else
							$("#ErrmsgDiv").addClass("errorMsgDisplayT")

					});

					$("#ErrmsgDiv").click(function() {
						$("#ErrmsgDiv").hide(1000);
					});

					$("#SuccessMsgAll").each(function() {
						if ($(this).text() == "") {
							$("#SuccessMsgDiv").hide(); // this references the current element in the iteration
							$("#SuccessMsgDiv").addClass("errorMsgDisplayF")
						} else
							$("#SuccessMsgDiv").addClass("errorMsgDisplayT")
					});

					$("#SuccessMsgDiv").click(function() {
						$("#SuccessMsgDiv").hide(1000);
					});

					$(".addproduct").click(
							function() {
								// Holds the product ID of the clicked element
								var productId = $(this).attr('class').replace(
										'addproduct ', '');

								addToCart(productId);
							});
				});
	</script>
	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
</body>
</html>