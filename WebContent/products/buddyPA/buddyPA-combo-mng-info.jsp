<%@ page import="java.util.List"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="icon" href="resources/assets/img/maybankLogoICO.ico"
	type="image/x-icon" />

<link rel="stylesheet" type="text/css"
	href="assets/js/jquery.dataTables.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/buttons.dataTables.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/dataTables.bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/buttons.bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="assets/css/toastr.min.css" />
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="/siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}

#upload-file-selector {
	display: none;
}
</style>


<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}

.modal-dialog {
	width: 500px !important;
}
</style>

<!-- OI CSS (REQUIRED Buddy PA)-->
<link rel="stylesheet" href="assets/css/oiCSS.css" />

<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet"
	href="assets/plugins/datepicker/datepicker.min.css" />
<!-- PLUGINS CSS -->
<link rel="stylesheet"
	href="assets/plugins/owl-carousel/owl.carousel.min.css" />
<link rel="stylesheet"
	href="assets/plugins/owl-carousel/owl.theme.min.css" />
<link rel="stylesheet"
	href="assets/plugins/owl-carousel/owl.transitions.min.css" />

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link rel="stylesheet"
	href="assets/plugins/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet" href="assets/css/owl.css" />
<link rel="stylesheet" href="assets/css/bootstrap.css" />
<link rel="stylesheet" href="assets/css/bootstrap-select.css" />
<link rel="stylesheet"
	href="assets/plugins/datatable/css/bootstrap.datatable.min.css" />
<link rel="stylesheet"
	href="assets/plugins/datatable/css/dataTables.searchHighlight.css" />
<link rel="stylesheet" href="assets/css/menu.css" />
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="libs/html5shiv.js"></script>
    <script src="libs/respond.min.js"></script>
<![endif]-->

<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

form#agentE, form#agentD {
	display: inline-block;
}

form#agentE input[type=submit], form#agentD input[type=submit] {
	padding: 5px;
}
</style>
</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
    -->

	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="/pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="/menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>

							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Buddy PA | Combo Management</h4>
															<span>(Admin are able to manage product including
																rates, discount and payment option)</span>
														</div>
													</div>
												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<form:form action="addBuddyPACombo" method="post"
																name="addCombo" id="addCombo">
																<div class="title">
																	<div class="sub">
																		<h4>Combo Infomation</h4>
																		<div id="ErrmsgDiv"
																			class="errorMsg leftPadding errorMsgDisplayF">
																			<i class="fa fa-exclamation-triangle"></i> <span
																				id="ErrmsgAll">${Errmsg}</span>
																		</div>
																		<div id="SuccessMsgDiv"
																			class="successMsg leftPadding errorMsgDisplayF">
																			<i class="fa fa-exclamation-circle"></i> <span
																				id="SuccessMsgAll">${SuccessMsg}</span>
																		</div>
																	</div>
																</div>
																<div>
																	<div class="form-horizontal info-meor">
																		<div class="col-sm-8">
																			<div class="form-group">
																				<label class="col-sm-3 control-label">Company</label>
																				<div class="col-sm-8">
																					<p class="form-control-static">
																						<span class="dotSpace">:</span> <select
																							name="company" id="company"
																							onChange="submitCompany()">
																							<c:forEach items="${companyList}"
																								var="companyList">
																								<option value="${companyList.getName()}"
																									<c:if test="${company==companyList.getName()}">selected</c:if>>${companyList.getDescription()}</option>
																							</c:forEach>
																						</select>
																					</p>
																				</div>
																			</div>

																			<div class="form-group">
																				<label class="col-sm-3 control-label">Combo
																					Name</label>
																				<div class="col-sm-8">
																					<p class="form-control-static">
																						<span class="dotSpace">:</span> <input type="text"
																							id="name" maxlength=50 name="name" /> <span
																							class="error leftPadding" id="errmsg2"
																							style="display: none;">Combo Name cannot
																							be empty! </span>
																					</p>
																				</div>
																			</div>

																			<div class="form-group">
																				<label class="col-sm-3 control-label">Reserved
																					ID</label>
																				<div class="col-sm-8">
																					<p class="form-control-static">
																						<span class="dotSpace">:</span> <input type="text"
																							id="reservedId" maxlength=1 name="reservedId" />
																						<span class="error leftPadding" id="errmsg"
																							style="display: none;">Reserved ID cannot
																							be empty! </span>
																					</p>
																				</div>
																			</div>

																			<div class="form-group">
																				<label class="col-sm-3 control-label">Enable</label>
																				<div class="col-sm-8">
																					<p class="form-control-static minusPaddingTop">
																						<span class="dotSpace">:</span> <input
																							type="checkbox" id="enable" name="enable"
																							class="paddingTopCheckBox" />
																					</p>
																				</div>
																			</div>

																			<div class="form-group">
																				<label class="col-sm-3 control-label"></label>
																				<div class="col-sm-8">
																					<p class="form-control-static">
																						<span style="color: transparent;">:</span> <a
																							class="btn btn-warning btn-sm" id="btnAdd"
																							onClick="validateForm();"><i
																							class="fa fa-plus"></i> Add</a>
																						<%-- <input class="btn btn-warning btn-sm" id="btnAdd"
																							onClick="validateForm();" type="button" value="Add" /> --%>
																					</p>
																				</div>
																			</div>
																		</div>
																		<div class="gap gap-mini"></div>
																	</div>
																</div>
															</form:form>
															<form:form id="loadCompany" name="loadCompany"
																action="companyIndexCombo">
																<input type="hidden" id="selectedCompany"
																	name="selectedCompany">
															</form:form>

															<div class="gap gap-mini"></div>

															<table id="tlPDSTable"
																class="table table-striped table-warning table-hover">
																<thead>
																	<tr>
																		<th class="firstChild"></th>
																		<th>Combo Name</th>
																		<th>Reserved ID</th>
																		<th>URL (EN)</th>
																		<th>URL (BM)</th>
																		<th>Enable</th>
																	</tr>
																</thead>

																<tbody>
																	<c:forEach items="${comboList}" var="element"
																		varStatus="theCount">
																		<tr>
																			<td class="firstChild"><a
																				class="btn btn-warning btn-xs" href="#"
																				data-toggle="modal" data-target="#comboEdit"
																				onClick="editComboInfo('${element.getCompanyId()}','${element.getName()}','${element.getReservedId()}','${element.getEnable()}');">
																					<i class="fa fa-edit"></i> Edit
																			</a></td>
																			<td align="center"><c:out
																					value="${element.getName()}" /></td>
																			<td align="center"><c:out
																					value="${element.getReservedId()}" /></td>
																			<td><a href=""><c:out
																						value="${element.getUrlEn()}" /></a></td>
																			<td><a href=""><c:out
																						value="${element.getUrlBm()}" /></a></td>
																			<td align="center"><c:choose>
																					<c:when test="${element.getEnable()=='Y'}">Yes</c:when>
																					<c:otherwise>No</c:otherwise>
																				</c:choose></td>
																		</tr>
																	</c:forEach>
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
											<!-- /.the-box -->
										</div>
										<!-- End warning color table -->
									</div>
								</div>
							</div>
							<!--row -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.container -->
	<!---------------------------------------------------- BEGIN Modal--------------------------------------------------------- -->
	<div class="col-sm-12">
		<!-- Begin  table -->
		<div class="modal fade" id="comboEdit" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<form:form action="updateBuddyPACombo" method="post" name="updCombo"
					id="updCombo">
					<input type="hidden" name="updComboUid" id="updComboUid" />
					<input type="hidden" name="updComboVersion" id="updComboVersion" />
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title modal-dialog-Title" id="myModalLabel">Buddy
								PA | Combo Management | Edit Combo</h4>
						</div>
						<div class="modal-body">
							<div class="form-harizontal">
								<div class="form-group">
									<label class="col-sm-3 control-label">Company</label>
									<div class="col-sm-8">
										<p class="form-control-static">
											<span class="dotSpace">:</span> <input type="text"
												name="updCompanyID" id="updCompanyID" readonly="readonly"
												class="readOnlyText" />
										</p>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Combo Name</label>
									<div class="col-sm-8">
										<p class="form-control-static">
											<span class="dotSpace">:</span> <input type="text"
												maxlength=50 name="updComboName" id="updComboName" />
										</p>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Reserved ID</label>
									<div class="col-sm-8">
										<p class="form-control-static">
											<span class="dotSpace">:</span> <input type="text"
												maxlength=1 name="updReservedID" id="updReservedID"
												readonly="readonly" />
										</p>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Enable</label>
									<div class="col-sm-8">
										<p class="form-control-static minusPaddingTop">
											<span class="dotSpace">:</span> <input type="checkbox"
												name="updComboEnable" id="updComboEnable"
												class="paddingTopCheckBox" />
										</p>
									</div>
								</div>
							</div>
							<div class="gap gap-mini"></div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

						<input class="btn btn-warning btn-sm" type="submit" name="Save"
							value="Save" />

					</div>
				</form:form>
			</div>
		</div>
	</div>
	<!---------------------------------------------------- END Modal--------------------------------------------------------- -->

	<!--</div>-->
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="/pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!--</div>-->
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->

	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
	-->
	<!-- END BACK TO TOP -->

	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>

	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->
	<script src="assets/js/jquery.min.js" type="text/javascript"></script>
	<!--  <script src="assets/jAlert/jquery-1.11.3.min.js"></script> -->
	<script src="assets/jAlert/jAlert.min.js"></script>
	<script src="assets/jAlert/jAlert-functions.min.js"></script>
	<script src="assets/js/bootstrap.js" type="text/javascript"></script>
	<script src="assets/plugins/retina/retina.min.js"></script>
	<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>
	<script src="assets/js/apps.js"></script>

	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<!-- <script src="assets/plugins/chosen/chosen.jquery.min.js"></script> -->
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script src="assets/js/shieldui-all.min.js" type="text/javascript"></script>
	<script src="assets/js/jszip.min.js" type="text/javascript"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>

	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/toastr.min.js"></script>
	<script src="assets/jAlert/jAlert.min.js"></script>
	<script src="assets/jAlert/jAlert-functions.min.js"></script>

	<script type="text/javascript">
		/*function submitWTCPdfDownload(){
		   
		$('#form1').submit(); 
		  return true; 
		
		}
		
		$('#tlPDSTable').DataTable({
		 "order": [],
		dom : 'Bfrtip',
		title : ' Agent List',
		
		buttons : [ 'csv', 'print', {
			extend : 'excelHtml5',
			title : 'Sales_Leads_'+today
		}, {
			extend : 'pdfHtml5',
			title :  'Sales_Leads_'+today
		} ]
		});

		$(document).ready(function() {

			var SaveMessage = "<c:out value="${SaveMessage}"/>";
			// show when the button is clicked
			if (SaveMessage.length) {
				successAlert('Success!', SaveMessage);
			}

			var deletemessage = "<c:out value="${deletemessage}"/>";
			// show when the button is clicked

			var updatemessage = "<c:out value="${updatemessage}"/>";
			// show when the button is clicked
			if (updatemessage.length) {
				successAlert('Success!', updatemessage);
			}

		});
		 */
		function submitCompany() {

			var tCompany = document.getElementById('company');
			var tSelectedCompany = document.getElementById('selectedCompany');
			tSelectedCompany.value = tCompany.value;
			$('#loadCompany').submit();
		}

		function editComboInfo(companyIdValue, comboNameValue, reseredIdValue,enableValue) {
			//alert(value);  //for testing purposes only
			var value1 = document.getElementById('updCompanyID');
			var value2 = document.getElementById('updComboName');
			var value3 = document.getElementById('updReservedID');
			var value4 = document.getElementById('updComboEnable');

			value1.value = companyIdValue;
			//txt1.disabled = true;  //cannot retrieve data when disabled
			value2.value = comboNameValue;
			value3.value = reseredIdValue;
			if (enableValue == 'Yes' || enableValue == 'enableValue'
					|| enableValue == 'y' || enableValue == 'Y'
					|| enableValue == 'yes') {
				value4.checked = true;
			} else {
				value4.checked = false;
			}
		}

		function validateForm() {
			//$("span").html("");
			var value1 = document.getElementById('company');
			var value2 = document.getElementById('reservedId');
			var value3 = document.getElementById('name');
			var err = document.getElementById('errmsg'); //Reserved ID error
			var err2 = document.getElementById('errmsg2'); //name error

			//alert(txt1.value);  //for testing purposes only
			if (value1.value != "" && value2.value != "" && value3.value != "") {
				$('#addCombo').submit();
				return true;
			} else {
				if (value3.value == "") {
					err2.style.display = "";
					document.getElementById("errmsg").style.display == "none";
				} else {
					err.style.display = "";
					document.getElementById("errmsg2").style.display == "none";
				}
				return false;
			}
		}
	</script>

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript">
		$(document).ready(
				function() {
					$("#ErrmsgAll").each(function() {
						if ($(this).text() == "") {
							$("#ErrmsgDiv").hide(); // this references the current element in the iteration
							$("#ErrmsgDiv").addClass("errorMsgDisplayF")
						} else
							$("#ErrmsgDiv").addClass("errorMsgDisplayT")

					});

					$("#ErrmsgDiv").click(function() {
						$("#ErrmsgDiv").hide(1000);
					});

					$("#SuccessMsgAll").each(function() {
						if ($(this).text() == "") {
							$("#SuccessMsgDiv").hide(); // this references the current element in the iteration
							$("#SuccessMsgDiv").addClass("errorMsgDisplayF")
						} else
							$("#SuccessMsgDiv").addClass("errorMsgDisplayT")
					});

					$("#SuccessMsgDiv").click(function() {
						$("#SuccessMsgDiv").hide(1000);
					});

					$(".addproduct").click(
							function() {
								// Holds the product ID of the clicked element
								var productId = $(this).attr('class').replace(
										'addproduct ', '');

								addToCart(productId);
							});
				});
	</script>
</body>
</html>