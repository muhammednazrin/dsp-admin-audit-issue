<%@ page import="java.util.List"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="icon" href="resources/assets/img/maybankLogoICO.ico"
	type="image/x-icon" />

<link rel="stylesheet" type="text/css"
	href="assets/js/jquery.dataTables.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/buttons.dataTables.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/dataTables.bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="assets/js/buttons.bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="assets/css/toastr.min.css" />
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="/siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}

#upload-file-selector {
	display: none;
}
</style>

<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}

.modal-dialog {
	width: 700px !important;
}
</style>

<!-- OI CSS (REQUIRED Buddy PA)-->
<link rel="stylesheet" href="assets/css/oiCSS.css" />

<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet"
	href="assets/plugins/datepicker/datepicker.min.css" />
<!-- PLUGINS CSS -->
<link rel="stylesheet"
	href="assets/plugins/owl-carousel/owl.carousel.min.css" />
<link rel="stylesheet"
	href="assets/plugins/owl-carousel/owl.theme.min.css" />
<link rel="stylesheet"
	href="assets/plugins/owl-carousel/owl.transitions.min.css" />

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link rel="stylesheet"
	href="assets/plugins/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet" href="assets/css/owl.css" />
<link rel="stylesheet" href="assets/css/bootstrap.css" />
<link rel="stylesheet" href="assets/css/bootstrap-select.css" />
<link rel="stylesheet"
	href="assets/plugins/datatable/css/bootstrap.datatable.min.css" />
<link rel="stylesheet"
	href="assets/plugins/datatable/css/dataTables.searchHighlight.css" />
<link rel="stylesheet" href="assets/css/menu.css" />
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="libs/html5shiv.js"></script>
    <script src="libs/respond.min.js"></script>
<![endif]-->

<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

form#agentE, form#agentD {
	display: inline-block;
}

form#agentE input[type=submit], form#agentD input[type=submit] {
	padding: 5px;
}
</style>
</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
    -->

	<div class="wrapper inner">
		<!-- START  wrapper inner-->
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="/pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<!-- START page-content inner -->
			<div class="container-fluid black-back">
				<!-- START container -->
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="/menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>

							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Buddy PA | Benefit Management</h4>
															<span>(Admin are able to manage product including
																rates, discount and payment option)</span>
														</div>
													</div>

													<!-- Start Form -->


													<!--   <input type="hidden" name="action" value="listAll" /> -->

													<div class="col-sm-12">
														<!-- Begin  table -->
														<div class="content-inner">
															<div class="the-box full no-border">
																<form:form action="addBuddyPABenefitGrp" method="post"
																	name="addBenefitGrp" id="addBenefitGrp">
																	<div class="title">
																		<div class="sub">
																			<h4>Benefit Grouping Edit</h4>
																			<div id="ErrmsgDiv"
																				class="errorMsg leftPadding errorMsgDisplayF">
																				<i class="fa fa-exclamation-triangle"></i> <span
																					id="ErrmsgAll">${Errmsg}</span>
																			</div>
																			<div id="SuccessMsgDiv"
																				class="successMsg leftPadding errorMsgDisplayF">
																				<i class="fa fa-exclamation-circle"></i> <span
																					id="SuccessMsgAll">${SuccessMsg}</span>
																			</div>
																		</div>
																	</div>
																	<div>
																		<div class="form-horizontal info-meor">
																			<div class="col-sm-6">
																				<div class="form-group">
																					<label class="col-sm-6 control-label">Company</label>
																					<div class="col-sm-6">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span><select
																								id="ddlCompanyList" name="ddlCompanyList"
																								onChange="editBenefitGrp()">
																								<c:forEach items="${companyList}"
																									var="companyList">
																									<option value="${companyList.getName()}"
																										<c:if test="${company==companyList.getName()}">selected</c:if>>${companyList.getDescription()}
																									</option>
																								</c:forEach>
																							</select>
																						</p>
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-sm-6 control-label">Combo
																						Name</label>
																					<div class="col-sm-6">
																						<p class="form-control-static">

																							<span class="dotSpace">:</span><select
																								id="ddlComboList" name="ddlComboList"
																								onChange="editBenefitGrp()">
																								<c:forEach items="${comboList}" var="comboID">
																									<option value="${comboID.getReservedId()}"
																										<c:if test="${combo==comboID.getReservedId()}">selected</c:if>>${comboID.getName()}
																									</option>
																								</c:forEach>
																							</select>
																						</p>
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-sm-6 control-label">Plan
																						Name</label>
																					<div class="col-sm-6">
																						<p class="form-control-static">

																							<span class="dotSpace">:</span><select
																								id="ddlPlanList" name="ddlPlanList"
																								onChange="editBenefitGrp()"><c:forEach
																									items="${planList}" var="planID">
																									<option value="${planID.getPlanId()}"
																										<c:if test="${plan==planID.getPlanId()}">selected</c:if>>${planID.getNameEn()}
																									</option>
																								</c:forEach></select>
																						</p>
																					</div>
																				</div>
																			</div>
																			<div class="gap gap-mini"></div>
																		</div>
																	</div>
																</form:form>
																<form:form id="test" name="test">
																	<input type="hidden" id="bgCompanyId"
																		name="bgCompanyId">
																	<input type="hidden" id="bgComboId" name="bgComboId">
																	<input type="hidden" id="bgPlanId" name="bgPlanId">
																</form:form>


																<div class="form-group">
																	<label class="col-sm-12 control-label"></label>
																	<div class="text-right">
																		<p class="form-control-static">
																			<span style="color: transparent;">:</span> <a
																				class="btn btn-warning btn-sm" id="btnAdd"
																				onClick="validateForm('${plan}');"><i
																				class="fa fa-save"></i> Save</a>
																		</p>
																	</div>
																</div>
																<form:form id="editForm" name="editForm"
																	action="updBuddyPABenefitGrpList" method="post">
																	<input type="hidden" id="bgCompanyId2"
																		name="bgCompanyId2">
																	<input type="hidden" id="bgComboId2" name="bgComboId2">
																	<input type="hidden" id="bgPlanId2" name="bgPlanId2">
																	<input type="hidden" id="bgComboId2Nm"
																		name="bgComboId2Nm" value="${comboNm}">
																	<input type="hidden" id="bgPlanId2Nm"
																		name="bgPlanId2Nm" value="${planNm}">
																	<input type="hidden" id="bgCount" name="bgCount"
																		value="${benefitListCount}">
																	<div class="gap gap-mini"></div>
																	<table id="listTable"
																		class="table table-striped table-warning table-hover"
																		style="font-size: small;">
																		<thead style="font-weight: bold;">
																			<tr>
																				<td align="center">Plan</td>
																				<td align="center" colspan="4"><c:out
																						value="${planNm}"></c:out> (<c:out value="${plan}"></c:out>)</td>
																			</tr>
																			<tr>
																				<td align="center" rowspan="2">Type of Benefits</td>
																				<td align="center" colspan="2">Sum Insured/<br>Limit
																					(RM)
																				</td>
																				<td align="center" colspan="2">Loading (%)</td>
																			</tr>
																			<tr>
																				<td align="center">Adult</td>
																				<td align="center">Child</td>
																				<td align="center">Adult</td>
																				<td align="center">Child</td>
																			</tr>
																		</thead>
																		<tbody>
																			<c:forEach items="${editBenefitGrpList}" var="list"
																				varStatus="listCount">
																				<tr>
																					<td align="center"><input type="hidden"
																						id="bId${listCount.index}"
																						name="bId${listCount.index}"
																						value="${list.getBenefitId()}"> <input
																						type="hidden" id="bNm${listCount.index}"
																						name="bNm${listCount.index}"
																						value="${list.getBenefitName()}"> <input
																						type="hidden" id="v${listCount.index}"
																						name="v${listCount.index}"
																						value="${list.getVersion()}"> <c:out
																							value="${list.getBenefitName()}" /></td>
																					<td align="center"><input type="number"
																						style="min-width: 100px;" min="0" step=".01"
																						id="aSum${listCount.index}"
																						name="aSum${listCount.index}"
																						value="${list.getAdultSumInsured()}" /></td>
																					<td align="center"><input type="number"
																						style="min-width: 100px;" min="0" step=".01"
																						id="cSum${listCount.index}"
																						name="cSum${listCount.index}"
																						value="${list.getChildSumInsured()}" /></td>
																					<td align="center"><input type="number"
																						style="min-width: 100px;" min="0" step=".01"
																						id="aLoad${listCount.index}"
																						name="aLoad${listCount.index}"
																						value="${list.getAdultLoading()}" /></td>
																					<td align="center"><input type="number"
																						style="min-width: 100px;" min="0" step=".01"
																						id="cLoad${listCount.index}"
																						name="cLoad${listCount.index}"
																						value="${list.getChildLoading()}" /></td>
																				</tr>
																			</c:forEach>
																		</tbody>
																	</table>
																</form:form>
															</div>
															<!-- /.the-box -->
															<!-- End warning color table -->
														</div>
													</div>

													<div class="gap gap-mini"></div>


												</div>
											</div>
											<!--row -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.container -->
			</div>
			<!-- /.page-content -->
			<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
			<jsp:include page="/pageFooter.jsp" />
			<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
		</div>
		<!-- /.wrapper -->
		<!-- END PAGE CONTENT -->



		<!-- BEGIN BACK TO TOP BUTTON -->
		<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
		<!-- END BACK TO TOP -->
	</div>

	<!--END  wrapper inner-->

	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%--    <jsp:include page="siteFooter.jsp" />   --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->


	<!-- JAVA Script Goes Here -->

	<script
		src="<c:url value="/resources/assets/js/jquery-2.2.4.min.js" />"></script>
	<!--  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
	<script src="assets/js/apps.js"></script>
	<script src="assets/plugins/retina/retina.min.js"></script>
	<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>


	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script src="assets/js/shieldui-all.min.js" type="text/javascript"></script>
	<script src="assets/js/jszip.min.js" type="text/javascript"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
    	<script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
    <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/buttons.print.min.js"></script>
	<script src="assets/js/buttons.colVis.min.js"></script>
	<script src="assets/js/toastr.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"
		type="text/javascript"></script>
	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

	<script type="text/javascript">
		function editBenefitInfo(vPlanID, vPlanName, lst) {
			var oCompanyId = document.getElementById('oriCompanyId');
			var oComboId = document.getElementById('oriComboId');
			var lCompanyId = document.getElementById('ddlCompanyList');
			var lComboId = document.getElementById('ddlComboList');
			var oPlanId = document.getElementById('oriPlanId');
			var oPlanName = document.getElementById('oriPlanName');
			oCompanyId.value = lCompanyId.value;
			oComboId.value = lComboId.value;
			oPlanId.value = vPlanID;
			oPlanName.innerHTML = vPlanName;
		}

		function validateForm(vPlanID) {
			var t1 = document.getElementById('ddlCompanyList');
			var t2 = document.getElementById('ddlComboList');
			var t4 = document.getElementById('bgCompanyId2');
			var t5 = document.getElementById('bgComboId2');
			var t7 = document.getElementById('bgPlanId2');
			t4.value = t1.value;
			t5.value = t2.value;
			t7.value = vPlanID;
			$('#editForm').submit();
		}

		function submitCompany() {

			var t1 = document.getElementById('ddlCompanyList');
			var t2 = document.getElementById('ddlComboList');
			var t4 = document.getElementById('bgCompanyId');
			var t5 = document.getElementById('bgComboId');
			t4.value = t1.value;
			t5.value = t2.value;
			$('#test').attr("action", "bgCompany");
			$('#test').submit();
		}

		function editBenefitGrp() {
			var t1 = document.getElementById('ddlCompanyList');
			var t2 = document.getElementById('ddlComboList');
			var t3 = document.getElementById('ddlPlanList');
			var t4 = document.getElementById('bgCompanyId');
			var t5 = document.getElementById('bgComboId');
			var t7 = document.getElementById('bgPlanId');
			t4.value = t1.value;
			t5.value = t2.value;
			t7.value = t3.value;
			$('#test').attr("action", "bgEdit");
			$('#test').submit();
		}
	</script>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript">
		$(document).ready(
				function() {

					$("#ErrmsgAll").each(function() {
						if ($(this).text() == "") {
							$("#ErrmsgDiv").hide(); // this references the current element in the iteration
							$("#ErrmsgDiv").addClass("errorMsgDisplayF")
						} else
							$("#ErrmsgDiv").addClass("errorMsgDisplayT")

					});

					$("#ErrmsgDiv").click(function() {
						$("#ErrmsgDiv").hide(1000);
					});

					$("#SuccessMsgAll").each(function() {
						if ($(this).text() == "") {
							$("#SuccessMsgDiv").hide(); // this references the current element in the iteration
							$("#SuccessMsgDiv").addClass("errorMsgDisplayF")
						} else
							$("#SuccessMsgDiv").addClass("errorMsgDisplayT")
					});

					$("#SuccessMsgDiv").click(function() {
						$("#SuccessMsgDiv").hide(1000);
					});

					$(".addproduct").click(
							function() {
								// Holds the product ID of the clicked element
								var productId = $(this).attr('class').replace(
										'addproduct ', '');

								addToCart(productId);
							});
				});
	</script>
	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
</body>
</html>