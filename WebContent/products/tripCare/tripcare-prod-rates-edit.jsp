<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page import="com.spring.VO.MITblPmntParam"%>
<%@ page import="com.spring.VO.DomesticPremium"%>
<%@ page import="com.spring.VO.InterPremium"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/js/jquery.dataTables.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/buttons.dataTables.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/dataTables.bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/buttons.bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link rel="stylesheet" href="assets/css/toastr.min.css">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="/siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}

#upload-file-selector {
	display: none;
}
</style>


<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}
</style>
<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet"
	href="assets/plugins/datepicker/datepicker.min.css">
<!-- PLUGINS CSS -->
<link href="assets/plugins/owl-carousel/owl.carousel.min.css"
	rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.theme.min.css"
	rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.transitions.min.css"
	rel="stylesheet">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

form#agentE, form#agentD {
	display: inline-block;
}

form#agentE input[type=submit], form#agentD input[type=submit] {
	padding: 5px;
}
</style>
</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="/pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="/menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Product Management | TripCare360</h4>
															(Admin are able to manage product including rates,
															discount and payment option)
														</div>
													</div>

													<!-- Start Form -->

													<div class="col-sm-12">
														<!-- Begin  table -->
														<div class="content-inner">
															<form:form action="UpdateTripCareProdRatesForApproval"
																method="post" name="nvicE" id="nvicE">
																<div class="the-box full no-border">
																	<div class="title">
																		<div class="sub">
																			<h4>Product Rates</h4>
																		</div>
																	</div>
																	<div>
																		<div class="form-horizontal info-meor">
																			<div class="col-sm-6">
																				<div class="form-group">
																					<label class="col-sm-6 control-label">Direct
																						Discount (Online)</label>
																					<div class="col-sm-6">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> <input
																								type="text" name="discount" id="discount"
																								value="<c:out value="${discountValue}" />">
																						</p>
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-sm-6 control-label">GST</label>
																					<div class="col-sm-6">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> <input
																								type="text" name="gst" id="gst"
																								value="<c:out value="${gstValue}" />">
																						</p>
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-sm-6 control-label">GST
																						Effective Date :</label>
																					<div class="col-sm-6">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> <input
																								type="text" id="gstEffDate"
																								placeholder="DD/MM/YYYY" readonly required
																								value="${gstEffDateValue}" name="gstEffDate">
																						</p>
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-sm-6 control-label">SST</label>
																					<div class="col-sm-6">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> <input
																								type="text" name="sst" id="sst"
																								value="<c:out value="${sstValue}" />">
																						</p>
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-sm-6 control-label">SST
																						Effective Date :</label>
																					<div class="col-sm-6">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> <input
																								type="text" id="sstEffDate"
																								placeholder="DD/MM/YYYY" readonly required
																								value="${sstEffDateValue}" name="sstEffDate">
																						</p>
																					</div>
																				</div>
																			</div>
																			<div class="col-sm-6">
																				<div class="form-group">
																					<label class="col-sm-6 control-label">Stamp
																						Duty</label>
																					<div class="col-sm-6">
																						<p class="form-control-static">
																							<span class="dotSpace">:</span> RM <input
																								type="text" name="stampduty" id="stampduty"
																								value="<c:out value="${stampDutyValue}" />">
																						</p>
																					</div>
																				</div>
																			</div>
																			<div class="gap gap-mini"></div>
																		</div>
																	</div>
																	<div class="col-sm-12">
																		<div class="text-right">
																			<a class="btn btn-warning btn-sm" id="btnAdd"
																				onClick="validateForm();"><i class="fa fa-save"></i>
																				Save</a>
																		</div>
																	</div>


																</div>
																<!-- /.the-box -->
															</form:form>
															<!-- End warning color table -->
														</div>
													</div>

													<div class="gap gap-mini"></div>


												</div>
											</div>
											<!--row -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.container -->


			</div>
			<!-- /.page-content -->
			<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
			<jsp:include page="/pageFooter.jsp" />
			<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
		</div>
		<!-- /.wrapper -->
		<!-- END PAGE CONTENT -->



		<!-- BEGIN BACK TO TOP BUTTON -->
		<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
		<!-- END BACK TO TOP -->


		<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

		<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<%--    <jsp:include page="siteFooter.jsp" />   --%>
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->


		<!-- JAVA Script Goes Here -->
		<script type="text/javascript"
			src="//code.jquery.com/jquery-1.11.3.min.js"></script>
		<script
			src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

		<script
			src="<c:url value="/resources/assets/js/jquery-2.2.4.min.js" />"></script>
		<!--  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
		<script src="assets/js/apps.js"></script>
		<script src="assets/plugins/retina/retina.min.js"></script>
		<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
		<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>


		<!-- PLUGINS -->
		<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
		<script src="assets/plugins/prettify/prettify.js"></script>
		<script
			src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

		<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
		<script src="assets/plugins/icheck/icheck.min.js"></script>
		<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
		<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
		<script src="assets/plugins/mask/jquery.mask.min.js"></script>
		<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
		<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
		<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
		<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
		<script
			src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
		<script src="assets/plugins/summernote/summernote.min.js"></script>
		<script src="assets/plugins/markdown/markdown.js"></script>
		<script src="assets/plugins/markdown/to-markdown.js"></script>
		<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
		<script src="assets/plugins/slider/bootstrap-slider.js"></script>
		<script src="assets/plugins/toastr/toastr.js"></script>
		<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
		<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
		<script
			src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>
		<script type="text/javascript" src="assets/js/shieldui-all.min.js"></script>
		<script type="text/javascript" src="assets/js/jszip.min.js"></script>
		<!--  Table Export -->
		<!-- KNOB JS -->
		<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
		<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
		<script src="assets/plugins/jquery-knob/knob.js"></script>
		<script src="assets/js/jquery.dataTables.min.js"></script>
		<script src="assets/js/dataTables.bootstrap.min.js"></script>
		<script src="assets/js/dataTables.buttons.min.js"></script>
		<script src="assets/js/buttons.bootstrap.min.js"></script>
		<script src="assets/js/jszip.min.js"></script>
		<script src="assets/js/pdfmake.min.js"></script>
		<script src="assets/js/vfs_fonts.js"></script>
		<script src="assets/js/buttons.html5.min.js"></script>
		<script src="assets/js/buttons.print.min.js"></script>
		<script src="assets/js/buttons.colVis.min.js"></script>
		<script src="assets/js/toastr.min.js"></script>

		<script type="text/javascript">

 		$(document).ready(function() {
         $("#gstEffDate").datepicker({
				format : 'dd/mm/yyyy',
				minDate: +1,
				autoclose : true,
			}).on('changeDate', function(ev) {
				$(this).datepicker('hide');
			});
			$("#sstEffDate").datepicker({
				format : 'dd/mm/yyyy',
				minDate: +1,
				autoclose : true,
			}).on('changeDate', function(ev) {
				$(this).datepicker('hide');
			});
			
 		});
			function premiumEdit(value) {
			   alert(value);
			   var txt=value;
				     
			   $('#pid').val(value);
			   $('#PremDomestic').modal('show');
				   
			}
	  		
			function premiumIntEdit(value) {
			   alert(value);
			   var txt=value;
				     
			   $('#pintid').val(value);
			   $('#PremIntnational').modal('show');
	  	   
			}
			   
			   
			function submitTCPremInfo(){
         		result=true;           
       			if(!$('#premiumval').val().length) {
	             	$('#msg_premiumval').removeClass('hidden');
	             	$('#msg_premiumval').addClass('visible');
	           
	         	    $('#premiumval').focus();
	            	result=false;
            	}
		         if (result==true) {
		         	$('#UpdateTCForm').submit();   
		        }
	         	}
	         function submitTCPremIntInfo(){
		         result=true;          
		         if(!$('#premiumintval').val().length) {
		             $('#msg_premiumintval').removeClass('hidden');
		             $('#msg_premiumintval').addClass('visible');
		           
		             $('#premiumintval').focus();
		             result=false;
		         }
		         if (result==true) {
		         	$('#UpdateTCIntForm').submit();   
		         }
		     }         

	         function validateForm() {
	 			var tStampDuty = document.getElementById('stampduty')
	 			var tDisc = document.getElementById('discount');
	 			var tGst = document.getElementById('gst');
	 			var tSst = document.getElementById('sst');
	 			var tGstDt = document.getElementById('gstEffDate');
	 			var tSstDt = document.getElementById('sstEffDate');

	 			if (tStampDuty.value != "" && tDisc.value != "" 
	 					&& tGst.value != "" && tSst.value != ""
	 					&& tGstDt.value != "" && tSstDt.value != "") {
	 				if (tGstDt.value != tSstDt.value ){
	 					$('#nvicE').submit();
	 					return true;
	 				}
	 				else {
						alert("GST Effective Date cannot be the same as SST Effective Date!");
	 					return false;
	 				}
	 			} else {
	 				alert("Please fill in the mandatory fields!");
	 				return false;
	 			}
	 		}

         </script>

		<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
</body>
</html>