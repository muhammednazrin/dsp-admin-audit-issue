<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="com.spring.VO.MITblPmntParam"%>
<%@ page import="com.spring.VO.DomesticPremium"%>
<%@ page import="com.spring.VO.InterPremium"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/js/jquery.dataTables.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/buttons.dataTables.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/css/bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/dataTables.bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/buttons.bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link rel="stylesheet" href="assets/css/toastr.min.css">
<link rel="stylesheet" href="assets/jAlert/jAlert.css">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="/siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}

#upload-file-selector {
	display: none;
}
</style>


<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}
</style>
<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet"
	href="assets/plugins/datepicker/datepicker.min.css">
<!-- PLUGINS CSS -->
<link href="assets/plugins/owl-carousel/owl.carousel.min.css"
	rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.theme.min.css"
	rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.transitions.min.css"
	rel="stylesheet">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

form#agentE, form#agentD {
	display: inline-block;
}

form#agentE input[type=submit], form#agentD input[type=submit] {
	padding: 5px;
}
</style>
</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="/pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="/menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Product Management | TripCare360</h4>
															(Admin are able to manage product including rates,
															discount and payment option)
														</div>
													</div>


													<!-- Start Form -->

													<div class="col-sm-12">
														<!-- Begin  table -->
														<div class="content-inner">
															<div class="the-box full no-border">
																<div class="title">
																	<div class="sub">
																		<h4>Product Rates</h4>
																	</div>
																</div>
																<div>
																	<div class="form-horizontal info-meor">
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Direct
																					Discount (Online)</label>
																				<div class="col-sm-6">
																					<p class="form-control-static">
																						:
																						<c:out value="${discountValue}" />
																					</p>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">Stamp
																					Duty</label>
																				<div class="col-sm-6">
																					<p class="form-control-static">
																						: RM
																						<c:out value="${stampDutyValue}" />
																					</p>
																				</div>
																			</div>
																		</div>
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label class="col-sm-6 control-label">GST</label>
																				<div class="col-sm-6">
																					<p class="form-control-static">
																						:
																						<c:out value="${gstValue}" />
																					</p>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">GST
																					Effective Date</label>
																				<div class="col-sm-6">
																					<p class="form-control-static">
																						:
																						<c:out value="${gstEffDateValue}" />
																					</p>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">SST</label>
																				<div class="col-sm-6">
																					<p class="form-control-static">
																						:
																						<c:out value="${sstValue}" />
																					</p>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-6 control-label">SST
																					Effective Date</label>
																				<div class="col-sm-6">
																					<p class="form-control-static">
																						:
																						<c:out value="${sstEffDateValue}" />
																					</p>
																				</div>
																			</div>
																		</div>
																		<div class="gap gap-mini"></div>
																	</div>
																</div>
																<div class="col-sm-12">
																	<div class="text-right">
																		<form:form action="UpdateTripCareProdRates"
																			method="post" name="nvicE" id="nvicE">
																			<c:set var="travelpervalue"
																				value="${fn:split(param.pid,'$') }" />

																			<c:if
																				test="${(travelpervalue[0] eq 1) || (travelpervalue[0] eq 3 )}">
																				<input class="btn btn-warning btn-sm" type="submit"
																					name="submit" value="Edit">
																			</c:if>

																			<!--  -->
																		</form:form>
																	</div>
																</div>
																<div class="gap gap-mini"></div>
																<div class="col-sm-12">
																	<div class="table-responsive">
																		<table
																			class="table table-striped table-bordered table-warning table-hover text-center">
																			<thead>
																				<tr>
																					<th colspan="11">INDIVIDUAL</th>
																				</tr>
																				<tr>
																					<th rowspan="2">Plan</th>
																					<th colspan="10">Adult (18 to 70 years)</th>
																				</tr>
																				<tr>
																					<th>Domestic</th>
																					<th colspan="3">Silver</th>
																					<th colspan="3">Gold</th>
																					<th colspan="3">Platinum</th>
																				</tr>
																				<tr>
																					<th>No of Days</th>
																					<th>Malaysia</th>
																					<th>*Asia countries</th>
																					<th>Worldwide excl. USA &amp; Canada</th>
																					<th>Worldwide incl. USA &amp; Canada</th>
																					<th>*Asia countries</th>
																					<th>Worldwide excl. USA &amp; Canada</th>
																					<th>Worldwide incl. USA &amp; Canada</th>
																					<th>*Asia countries</th>
																					<th>Worldwide excl. USA &amp; Canada</th>
																					<th>Worldwide incl. USA &amp; Canada</th>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<td>1-5</td>
																					<td><c:forEach items="${DomPremiumList18}"
																							var="elementsPremiumWk1">
																							<c:if
																								test="${elementsPremiumWk1.daysRangeKeycode == 'D1_5'}">
																								<a href="#"
																									onClick="premiumEdit(${elementsPremiumWk1.id},${elementsPremiumWk1.premiumValue});"><c:out
																										value="${elementsPremiumWk1.premiumValue}" /></a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntPremiumWk1">
																							<c:if
																								test="${elementsIntPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntPremiumWk1.destinationCodeArea == 'AC' && 
																			      elementsIntPremiumWk1.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPremiumWk1.id},${elementsIntPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWOUPremiumWk1">
																							<c:if
																								test="${elementsIntWOUPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWOUPremiumWk1.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPremiumWk1.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPremiumWk1.id},${elementsIntWOUPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWUPremiumWk1">
																							<c:if
																								test="${elementsIntWUPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWUPremiumWk1.destinationCodeArea == 'WU' && 
																			      elementsIntWUPremiumWk1.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPremiumWk1.id},${elementsIntWUPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWUPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntGPremiumWk1">
																							<c:if
																								test="${elementsIntGPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntGPremiumWk1.destinationCodeArea == 'AC' && 
																			      elementsIntGPremiumWk1.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGPremiumWk1.id},${elementsIntGPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntGPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWOUGPremiumWk1">
																							<c:if
																								test="${elementsIntWOUGPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWOUGPremiumWk1.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGPremiumWk1.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGPremiumWk1.id},${elementsIntWOUGPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWUGPremiumWk1">
																							<c:if
																								test="${elementsIntWUGPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWUGPremiumWk1.destinationCodeArea == 'WU' && 
																			      elementsIntWUGPremiumWk1.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGPremiumWk1.id},${elementsIntWUGPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWUGPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntPlPremiumWk1">
																							<c:if
																								test="${elementsIntPlPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntPlPremiumWk1.destinationCodeArea == 'AC' && 
																			      elementsIntPlPremiumWk1.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlPremiumWk1.id},${elementsIntPlPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntPlPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWOUPlPremiumWk1">
																							<c:if
																								test="${elementsIntWOUPlPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWOUPlPremiumWk1.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlPremiumWk1.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlPremiumWk1.id},${elementsIntWOUPlPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWUPlPremiumWk1">
																							<c:if
																								test="${elementsIntWUPlPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWUPlPremiumWk1.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlPremiumWk1.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlPremiumWk1.id},${elementsIntWUPlPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>6-10</td>
																					<td><c:forEach items="${DomPremiumList18}"
																							var="elementsPremiumWk2">
																							<c:if
																								test="${elementsPremiumWk2.daysRangeKeycode == 'D6_10'}">
																								<a href="#"
																									onClick="premiumEdit(${elementsPremiumWk2.id},${elementsPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntPremiumWk2">
																							<c:if
																								test="${elementsIntPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntPremiumWk2.destinationCodeArea == 'AC' && 
																			      elementsIntPremiumWk2.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPremiumWk2.id},${elementsIntPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWOUPremiumWk2">
																							<c:if
																								test="${elementsIntWOUPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWOUPremiumWk2.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPremiumWk2.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPremiumWk2.id},${elementsIntWOUPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWUPremiumWk2">
																							<c:if
																								test="${elementsIntWUPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWUPremiumWk2.destinationCodeArea == 'WU' && 
																			      elementsIntWUPremiumWk2.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPremiumWk2.id},${elementsIntWUPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWUPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntGPremiumWk2">
																							<c:if
																								test="${elementsIntGPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntGPremiumWk2.destinationCodeArea == 'AC' && 
																			      elementsIntGPremiumWk2.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGPremiumWk2.id},${elementsIntGPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntGPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWOUGPremiumWk2">
																							<c:if
																								test="${elementsIntWOUGPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWOUGPremiumWk2.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGPremiumWk2.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGPremiumWk2.id},${elementsIntWOUGPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWUGPremiumWk2">
																							<c:if
																								test="${elementsIntWUGPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWUGPremiumWk2.destinationCodeArea == 'WU' && 
																			      elementsIntWUGPremiumWk2.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGPremiumWk2.id},${elementsIntWUGPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWUGPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntPlPremiumWk2">
																							<c:if
																								test="${elementsIntPlPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntPlPremiumWk2.destinationCodeArea == 'AC' && 
																			      elementsIntPlPremiumWk2.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlPremiumWk2.id},${elementsIntPlPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntPlPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWOUPlPremiumWk2">
																							<c:if
																								test="${elementsIntWOUPlPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWOUPlPremiumWk2.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlPremiumWk2.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlPremiumWk2.id},${elementsIntWOUPlPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWUPlPremiumWk2">
																							<c:if
																								test="${elementsIntWUPlPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWUPlPremiumWk2.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlPremiumWk2.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlPremiumWk2.id},${elementsIntWUPlPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>11-18</td>
																					<td><c:forEach items="${DomPremiumList18}"
																							var="elementsPremiumWk3">
																							<c:if
																								test="${elementsPremiumWk3.daysRangeKeycode == 'D11_18'}">
																								<a href="#"
																									onClick="premiumEdit(${elementsPremiumWk3.id},${elementsPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntPremiumWk3">
																							<c:if
																								test="${elementsIntPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntPremiumWk3.destinationCodeArea == 'AC' && 
																			      elementsIntPremiumWk3.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPremiumWk3.id},${elementsIntPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWOUPremiumWk3">
																							<c:if
																								test="${elementsIntWOUPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWOUPremiumWk3.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPremiumWk3.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPremiumWk3.id},${elementsIntWOUPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWUPremiumWk3">
																							<c:if
																								test="${elementsIntWUPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWUPremiumWk3.destinationCodeArea == 'WU' && 
																			      elementsIntWUPremiumWk3.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPremiumWk3.id},${elementsIntWUPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWUPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntGPremiumWk3">
																							<c:if
																								test="${elementsIntGPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntGPremiumWk3.destinationCodeArea == 'AC' && 
																			      elementsIntGPremiumWk3.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGPremiumWk3.id},${elementsIntGPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntGPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWOUGPremiumWk3">
																							<c:if
																								test="${elementsIntWOUGPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWOUGPremiumWk3.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGPremiumWk3.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGPremiumWk3.id},${elementsIntWOUGPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWUGPremiumWk3">
																							<c:if
																								test="${elementsIntWUGPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWUGPremiumWk3.destinationCodeArea == 'WU' && 
																			      elementsIntWUGPremiumWk3.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGPremiumWk3.id},${elementsIntWUGPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWUGPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntPlPremiumWk3">
																							<c:if
																								test="${elementsIntPlPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntPlPremiumWk3.destinationCodeArea == 'AC' && 
																			      elementsIntPlPremiumWk3.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlPremiumWk3.id},${elementsIntPlPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntPlPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWOUPlPremiumWk3">
																							<c:if
																								test="${elementsIntWOUPlPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWOUPlPremiumWk3.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlPremiumWk3.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlPremiumWk3.id},${elementsIntWOUPlPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWUPlPremiumWk3">
																							<c:if
																								test="${elementsIntWUPlPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWUPlPremiumWk3.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlPremiumWk3.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlPremiumWk3.id},${elementsIntWUPlPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>19-30</td>
																					<td><c:forEach items="${DomPremiumList18}"
																							var="elementsPremiumWk4">
																							<c:if
																								test="${elementsPremiumWk4.daysRangeKeycode == 'D19_30'}">
																								<a href="#"
																									onClick="premiumEdit(${elementsPremiumWk4.id},${elementsPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntPremiumWk4">
																							<c:if
																								test="${elementsIntPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntPremiumWk4.destinationCodeArea == 'AC' && 
																			      elementsIntPremiumWk4.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPremiumWk4.id},${elementsIntPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWOUPremiumWk4">
																							<c:if
																								test="${elementsIntWOUPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWOUPremiumWk4.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPremiumWk4.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPremiumWk4.id},${elementsIntWOUPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWUPremiumWk4">
																							<c:if
																								test="${elementsIntWUPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWUPremiumWk4.destinationCodeArea == 'WU' && 
																			      elementsIntWUPremiumWk4.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPremiumWk4.id},${elementsIntWUPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWUPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntGPremiumWk4">
																							<c:if
																								test="${elementsIntGPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntGPremiumWk4.destinationCodeArea == 'AC' && 
																			      elementsIntGPremiumWk4.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGPremiumWk4.id},${elementsIntGPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntGPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWOUGPremiumWk4">
																							<c:if
																								test="${elementsIntWOUGPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWOUGPremiumWk4.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGPremiumWk4.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGPremiumWk4.id},${elementsIntWOUGPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWUGPremiumWk4">
																							<c:if
																								test="${elementsIntWUGPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWUGPremiumWk4.destinationCodeArea == 'WU' && 
																			      elementsIntWUGPremiumWk4.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGPremiumWk4.id},${elementsIntWUGPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWUGPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntPlPremiumWk4">
																							<c:if
																								test="${elementsIntPlPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntPlPremiumWk4.destinationCodeArea == 'AC' && 
																			      elementsIntPlPremiumWk4.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlPremiumWk4.id},${elementsIntPlPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntPlPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWOUPlPremiumWk4">
																							<c:if
																								test="${elementsIntWOUPlPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWOUPlPremiumWk4.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlPremiumWk4.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlPremiumWk4.id},${elementsIntWOUPlPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWUPlPremiumWk4">
																							<c:if
																								test="${elementsIntWUPlPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWUPlPremiumWk4.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlPremiumWk4.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlPremiumWk4.id},${elementsIntWUPlPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>Each additional week</td>
																					<td>Not covered</td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntPremiumWk5">
																							<c:if
																								test="${elementsIntPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntPremiumWk5.destinationCodeArea == 'AC' && 
																			      elementsIntPremiumWk5.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPremiumWk5.id},${elementsIntPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWOUPremiumWk5">
																							<c:if
																								test="${elementsIntWOUPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWOUPremiumWk5.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPremiumWk5.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPremiumWk5.id},${elementsIntWOUPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWUPremiumWk5">
																							<c:if
																								test="${elementsIntWUPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWUPremiumWk5.destinationCodeArea == 'WU' && 
																			      elementsIntWUPremiumWk5.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPremiumWk5.id},${elementsIntWUPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWUPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntGPremiumWk5">
																							<c:if
																								test="${elementsIntGPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntGPremiumWk5.destinationCodeArea == 'AC' && 
																			      elementsIntGPremiumWk5.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGPremiumWk5.id},${elementsIntGPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntGPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWOUGPremiumWk5">
																							<c:if
																								test="${elementsIntWOUGPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWOUGPremiumWk5.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGPremiumWk5.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGPremiumWk5.id},${elementsIntWOUGPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWUGPremiumWk5">
																							<c:if
																								test="${elementsIntWUGPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWUGPremiumWk5.destinationCodeArea == 'WU' && 
																			      elementsIntWUGPremiumWk5.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGPremiumWk5.id},${elementsIntWUGPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWUGPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<!--end changes-->
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntPlPremiumWk5">
																							<c:if
																								test="${elementsIntPlPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntPlPremiumWk5.destinationCodeArea == 'AC' && 
																			      elementsIntPlPremiumWk5.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlPremiumWk5.id},${elementsIntPlPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntPlPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWOUPlPremiumWk5">
																							<c:if
																								test="${elementsIntWOUPlPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWOUPlPremiumWk5.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlPremiumWk5.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlPremiumWk5.id},${elementsIntWOUPlPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWUPlPremiumWk5">
																							<c:if
																								test="${elementsIntWUPlPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWUPlPremiumWk5.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlPremiumWk5.packageCode == 'platinum'}">

																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlPremiumWk5.id},${elementsIntWUPlPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>Annual</td>
																					<td>Not covered</td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntPremiumWk6">
																							<c:if
																								test="${elementsIntPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntPremiumWk6.destinationCodeArea == 'AC' && 
																			      elementsIntPremiumWk6.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPremiumWk6.id},${elementsIntPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWOUPremiumWk6">
																							<c:if
																								test="${elementsIntWOUPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWOUPremiumWk6.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPremiumWk6.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPremiumWk6.id},${elementsIntWOUPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWUPremiumWk6">
																							<c:if
																								test="${elementsIntWUPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWUPremiumWk6.destinationCodeArea == 'WU' && 
																			      elementsIntWUPremiumWk6.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPremiumWk6.id},${elementsIntWUPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWUPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntGPremiumWk6">
																							<c:if
																								test="${elementsIntGPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntGPremiumWk6.destinationCodeArea == 'AC' && 
																			      elementsIntGPremiumWk6.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGPremiumWk6.id},${elementsIntGPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntGPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWOUGPremiumWk6">
																							<c:if
																								test="${elementsIntWOUGPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWOUGPremiumWk6.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGPremiumWk6.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGPremiumWk6.id},${elementsIntWOUGPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWUGPremiumWk6">
																							<c:if
																								test="${elementsIntWUGPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWUGPremiumWk6.destinationCodeArea == 'WU' && 
																			      elementsIntWUGPremiumWk6.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGPremiumWk6.id},${elementsIntWUGPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWUGPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntPlPremiumWk6">
																							<c:if
																								test="${elementsIntPlPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntPlPremiumWk6.destinationCodeArea == 'AC' && 
																			      elementsIntPlPremiumWk6.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlPremiumWk6.id},${elementsIntPlPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntPlPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWOUPlPremiumWk6">
																							<c:if
																								test="${elementsIntWOUPlPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWOUPlPremiumWk6.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlPremiumWk6.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlPremiumWk6.id},${elementsIntWOUPlPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList18}"
																							var="elementsIntWUPlPremiumWk6">
																							<c:if
																								test="${elementsIntWUPlPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWUPlPremiumWk6.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlPremiumWk6.packageCode == 'platinum'}">

																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlPremiumWk6.id},${elementsIntWUPlPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>Adventurous Activities</td>
																					<td>Not covered</td>
																					<td colspan="3"><c:forEach
																							items="${IntPremiumList18}"
																							var="elementsIntPremiumWk6">
																							<c:if
																								test="${elementsIntPremiumWk6.daysRangeKeycode == 'D_ADV_ACT' && 
																			      elementsIntPremiumWk6.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPremiumWk6.id},${elementsIntPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td colspan="3"><c:forEach
																							items="${IntPremiumList18}"
																							var="elementsIntGPremiumWk6">
																							<c:if
																								test="${elementsIntGPremiumWk6.daysRangeKeycode == 'D_ADV_ACT' && 
																			      elementsIntGPremiumWk6.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGPremiumWk6.id},${elementsIntGPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntGPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td colspan="3"><c:forEach
																							items="${IntPremiumList18}"
																							var="elementsIntPlPremiumWk6">
																							<c:if
																								test="${elementsIntPlPremiumWk6.daysRangeKeycode == 'D_ADV_ACT' && 
																			      elementsIntPlPremiumWk6.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlPremiumWk6.id},${elementsIntPlPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntPlPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																	<div class="table-responsive">
																		<table
																			class="table table-striped table-bordered table-warning table-hover text-center">
																			<thead>
																				<tr>
																					<th rowspan="2">Plan</th>
																					<th colspan="10">Senior Citizen (71 to 80
																						years)</th>
																				</tr>
																				<tr>
																					<th>Domestic</th>
																					<th colspan="3">Silver</th>
																					<th colspan="3">Gold</th>
																					<th colspan="3">Platinum</th>
																				</tr>
																				<tr>
																					<th>No of Days</th>
																					<th>Malaysia</th>
																					<th>*Asia countries</th>
																					<th>Worldwide excl. USA &amp; Canada</th>
																					<th>Worldwide incl. USA &amp; Canada</th>
																					<th>*Asia countries</th>
																					<th>Worldwide excl. USA &amp; Canada</th>
																					<th>Worldwide incl. USA &amp; Canada</th>
																					<th>*Asia countries</th>
																					<th>Worldwide excl. USA &amp; Canada</th>
																					<th>Worldwide incl. USA &amp; Canada</th>
																				</tr>
																			</thead>
																			<tbody>
																			<tbody>
																				<tr>
																					<td>1-5</td>
																					<td><c:forEach items="${DomPremiumList71}"
																							var="elements71PremiumWk1">
																							<c:if
																								test="${elements71PremiumWk1.daysRangeKeycode == 'D1_5'}">
																								<a href="#"
																									onClick="premiumEdit(${elements71PremiumWk1.id},${elements71PremiumWk1.premiumValue});">
																									<c:out
																										value="${elements71PremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsInt71PremiumWk1">
																							<c:if
																								test="${elementsInt71PremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsInt71PremiumWk1.destinationCodeArea == 'AC' && 
																			      elementsInt71PremiumWk1.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsInt71PremiumWk1.id},${elementsInt71PremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsInt71PremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWOU71PremiumWk1">
																							<c:if
																								test="${elementsIntWOU71PremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWOU71PremiumWk1.destinationCodeArea == 'WOU' && 
																			      elementsIntWOU71PremiumWk1.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOU71PremiumWk1.id},${elementsIntWOU71PremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWOU71PremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWU71PremiumWk1">
																							<c:if
																								test="${elementsIntWU71PremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWU71PremiumWk1.destinationCodeArea == 'WU' && 
																			      elementsIntWU71PremiumWk1.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWU71PremiumWk1.id},${elementsIntWU71PremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWU71PremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntG71PremiumWk1">
																							<c:if
																								test="${elementsIntG71PremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntG71PremiumWk1.destinationCodeArea == 'AC' && 
																			      elementsIntG71PremiumWk1.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntG71PremiumWk1.id},${elementsIntG71PremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntG71PremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWOUG71PremiumWk1">
																							<c:if
																								test="${elementsIntWOUG71PremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWOUG71PremiumWk1.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUG71PremiumWk1.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUG71PremiumWk1.id},${elementsIntWOUG71PremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWOUG71PremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWUG71PremiumWk1">
																							<c:if
																								test="${elementsIntWUG71PremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWUG71PremiumWk1.destinationCodeArea == 'WU' && 
																			      elementsIntWUG71PremiumWk1.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUG71PremiumWk1.id},${elementsIntWUG71PremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWUG71PremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntPl71PremiumWk1">
																							<c:if
																								test="${elementsIntPl71PremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntPl71PremiumWk1.destinationCodeArea == 'AC' && 
																			      elementsIntPl71PremiumWk1.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPl71PremiumWk1.id},${elementsIntPl71PremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntPl71PremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWOUPl71PremiumWk1">
																							<c:if
																								test="${elementsIntWOUPl71PremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWOUPl71PremiumWk1.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPl71PremiumWk1.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPl71PremiumWk1.id},${elementsIntWOUPl71PremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPl71PremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWUPl71PremiumWk1">
																							<c:if
																								test="${elementsIntWUPl71PremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWUPl71PremiumWk1.destinationCodeArea == 'WU' && 
																			      elementsIntWUPl71PremiumWk1.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPl71PremiumWk1.id},${elementsIntWUPl71PremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWUPl71PremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>6-10</td>
																					<td><c:forEach items="${DomPremiumList71}"
																							var="elementsPremiumWk2">
																							<c:if
																								test="${elementsPremiumWk2.daysRangeKeycode == 'D6_10'}">
																								<a href="#"
																									onClick="premiumEdit(${elementsPremiumWk2.id},${elementsPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntPremiumWk2">
																							<c:if
																								test="${elementsIntPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntPremiumWk2.destinationCodeArea == 'AC' && 
																			      elementsIntPremiumWk2.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPremiumWk2.id},${elementsIntPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWOUPremiumWk2">
																							<c:if
																								test="${elementsIntWOUPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWOUPremiumWk2.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPremiumWk2.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPremiumWk2.id},${elementsIntWOUPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWUPremiumWk2">
																							<c:if
																								test="${elementsIntWUPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWUPremiumWk2.destinationCodeArea == 'WU' && 
																			      elementsIntWUPremiumWk2.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPremiumWk2.id},${elementsIntWUPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWUPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntGPremiumWk2">
																							<c:if
																								test="${elementsIntGPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntGPremiumWk2.destinationCodeArea == 'AC' && 
																			      elementsIntGPremiumWk2.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGPremiumWk2.id},${elementsIntGPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntGPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWOUGPremiumWk2">
																							<c:if
																								test="${elementsIntWOUGPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWOUGPremiumWk2.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGPremiumWk2.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGPremiumWk2.id},${elementsIntWOUGPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWUGPremiumWk2">
																							<c:if
																								test="${elementsIntWUGPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWUGPremiumWk2.destinationCodeArea == 'WU' && 
																			      elementsIntWUGPremiumWk2.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGPremiumWk2.id},${elementsIntWUGPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWUGPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntPlPremiumWk2">
																							<c:if
																								test="${elementsIntPlPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntPlPremiumWk2.destinationCodeArea == 'AC' && 
																			      elementsIntPlPremiumWk2.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlPremiumWk2.id},${elementsIntPlPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntPlPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWOUPlPremiumWk2">
																							<c:if
																								test="${elementsIntWOUPlPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWOUPlPremiumWk2.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlPremiumWk2.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlPremiumWk2.id},${elementsIntWOUPlPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWUPlPremiumWk2">
																							<c:if
																								test="${elementsIntWUPlPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWUPlPremiumWk2.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlPremiumWk2.packageCode == 'platinum'}">

																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlPremiumWk2.id},${elementsIntWUPlPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>11-18</td>
																					<td><c:forEach items="${DomPremiumList71}"
																							var="elementsPremiumWk3">
																							<c:if
																								test="${elementsPremiumWk3.daysRangeKeycode == 'D11_18'}">
																								<a href="#"
																									onClick="premiumEdit(${elementsPremiumWk3.id},${elementsPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntPremiumWk3">
																							<c:if
																								test="${elementsIntPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntPremiumWk3.destinationCodeArea == 'AC' && 
																			      elementsIntPremiumWk3.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPremiumWk3.id},${elementsIntPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWOUPremiumWk3">
																							<c:if
																								test="${elementsIntWOUPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWOUPremiumWk3.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPremiumWk3.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPremiumWk3.id},${elementsIntWOUPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWUPremiumWk3">
																							<c:if
																								test="${elementsIntWUPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWUPremiumWk3.destinationCodeArea == 'WU' && 
																			      elementsIntWUPremiumWk3.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPremiumWk3.id},${elementsIntWUPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWUPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntGPremiumWk3">
																							<c:if
																								test="${elementsIntGPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntGPremiumWk3.destinationCodeArea == 'AC' && 
																			      elementsIntGPremiumWk3.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGPremiumWk3.id},${elementsIntGPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntGPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWOUGPremiumWk3">
																							<c:if
																								test="${elementsIntWOUGPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWOUGPremiumWk3.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGPremiumWk3.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGPremiumWk3.id},${elementsIntWOUGPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWUGPremiumWk3">
																							<c:if
																								test="${elementsIntWUGPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWUGPremiumWk3.destinationCodeArea == 'WU' && 
																			      elementsIntWUGPremiumWk3.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGPremiumWk3.id},${elementsIntWUGPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWUGPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntPlPremiumWk3">
																							<c:if
																								test="${elementsIntPlPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntPlPremiumWk3.destinationCodeArea == 'AC' && 
																			      elementsIntPlPremiumWk3.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlPremiumWk3.id},${elementsIntPlPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntPlPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWOUPlPremiumWk3">
																							<c:if
																								test="${elementsIntWOUPlPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWOUPlPremiumWk3.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlPremiumWk3.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlPremiumWk3.id},${elementsIntWOUPlPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWUPlPremiumWk3">
																							<c:if
																								test="${elementsIntWUPlPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWUPlPremiumWk3.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlPremiumWk3.packageCode == 'platinum'}">

																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlPremiumWk3.id},${elementsIntWUPlPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>19-30</td>
																					<td><c:forEach items="${DomPremiumList71}"
																							var="elementsPremiumWk4">
																							<c:if
																								test="${elementsPremiumWk4.daysRangeKeycode == 'D19_30'}">
																								<a href="#"
																									onClick="premiumEdit(${elementsPremiumWk4.id},${elementsPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntPremiumWk4">
																							<c:if
																								test="${elementsIntPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntPremiumWk4.destinationCodeArea == 'AC' && 
																			      elementsIntPremiumWk4.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPremiumWk4.id},${elementsIntPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWOUPremiumWk4">
																							<c:if
																								test="${elementsIntWOUPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWOUPremiumWk4.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPremiumWk4.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPremiumWk4.id},${elementsIntWOUPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWUPremiumWk4">
																							<c:if
																								test="${elementsIntWUPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWUPremiumWk4.destinationCodeArea == 'WU' && 
																			      elementsIntWUPremiumWk4.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPremiumWk4.id},${elementsIntWUPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWUPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntGPremiumWk4">
																							<c:if
																								test="${elementsIntGPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntGPremiumWk4.destinationCodeArea == 'AC' && 
																			      elementsIntGPremiumWk4.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGPremiumWk4.id},${elementsIntGPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntGPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWOUGPremiumWk4">
																							<c:if
																								test="${elementsIntWOUGPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWOUGPremiumWk4.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGPremiumWk4.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGPremiumWk4.id},${elementsIntWOUGPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWUGPremiumWk4">
																							<c:if
																								test="${elementsIntWUGPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWUGPremiumWk4.destinationCodeArea == 'WU' && 
																			      elementsIntWUGPremiumWk4.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGPremiumWk4.id},${elementsIntWUGPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWUGPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntPlPremiumWk4">
																							<c:if
																								test="${elementsIntPlPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntPlPremiumWk4.destinationCodeArea == 'AC' && 
																			      elementsIntPlPremiumWk4.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlPremiumWk4.id},${elementsIntPlPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntPlPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWOUPlPremiumWk4">
																							<c:if
																								test="${elementsIntWOUPlPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWOUPlPremiumWk4.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlPremiumWk4.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlPremiumWk4.id},${elementsIntWOUPlPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWUPlPremiumWk4">
																							<c:if
																								test="${elementsIntWUPlPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWUPlPremiumWk4.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlPremiumWk4.packageCode == 'platinum'}">

																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlPremiumWk4.id},${elementsIntWUPlPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>Each additional week</td>
																					<td>Not covered</td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntPremiumWk5">
																							<c:if
																								test="${elementsIntPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntPremiumWk5.destinationCodeArea == 'AC' && 
																			      elementsIntPremiumWk5.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPremiumWk5.id},${elementsIntPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWOUPremiumWk5">
																							<c:if
																								test="${elementsIntWOUPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWOUPremiumWk5.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPremiumWk5.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPremiumWk5.id},${elementsIntWOUPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWUPremiumWk5">
																							<c:if
																								test="${elementsIntWUPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWUPremiumWk5.destinationCodeArea == 'WU' && 
																			      elementsIntWUPremiumWk5.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPremiumWk5.id},${elementsIntWUPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWUPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntGPremiumWk5">
																							<c:if
																								test="${elementsIntGPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntGPremiumWk5.destinationCodeArea == 'AC' && 
																			      elementsIntGPremiumWk5.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGPremiumWk5.id},${elementsIntGPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntGPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWOUGPremiumWk5">
																							<c:if
																								test="${elementsIntWOUGPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWOUGPremiumWk5.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGPremiumWk5.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGPremiumWk5.id},${elementsIntWOUGPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWUGPremiumWk5">
																							<c:if
																								test="${elementsIntWUGPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWUGPremiumWk5.destinationCodeArea == 'WU' && 
																			      elementsIntWUGPremiumWk5.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGPremiumWk5.id},${elementsIntWUGPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWUGPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntPlPremiumWk5">
																							<c:if
																								test="${elementsIntPlPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntPlPremiumWk5.destinationCodeArea == 'AC' && 
																			      elementsIntPlPremiumWk5.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlPremiumWk5.id},${elementsIntPlPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntPlPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWOUPlPremiumWk5">
																							<c:if
																								test="${elementsIntWOUPlPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWOUPlPremiumWk5.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlPremiumWk5.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlPremiumWk5.id},${elementsIntWOUPlPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWUPlPremiumWk5">
																							<c:if
																								test="${elementsIntWUPlPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWUPlPremiumWk5.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlPremiumWk5.packageCode == 'platinum'}">

																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlPremiumWk5.id},${elementsIntWUPlPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>Annual</td>
																					<td>Not covered</td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntPremiumWk6">
																							<c:if
																								test="${elementsIntPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntPremiumWk6.destinationCodeArea == 'AC' && 
																			      elementsIntPremiumWk6.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPremiumWk6.id},${elementsIntPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWOUPremiumWk6">
																							<c:if
																								test="${elementsIntWOUPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWOUPremiumWk6.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPremiumWk6.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPremiumWk6.id},${elementsIntWOUPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWUPremiumWk6">
																							<c:if
																								test="${elementsIntWUPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWUPremiumWk6.destinationCodeArea == 'WU' && 
																			      elementsIntWUPremiumWk6.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPremiumWk6.id},${elementsIntWUPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWUPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntGPremiumWk6">
																							<c:if
																								test="${elementsIntGPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntGPremiumWk6.destinationCodeArea == 'AC' && 
																			      elementsIntGPremiumWk6.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGPremiumWk6.id},${elementsIntGPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntGPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWOUGPremiumWk6">
																							<c:if
																								test="${elementsIntWOUGPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWOUGPremiumWk6.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGPremiumWk6.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGPremiumWk6.id},${elementsIntWOUGPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWUGPremiumWk6">
																							<c:if
																								test="${elementsIntWUGPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWUGPremiumWk6.destinationCodeArea == 'WU' && 
																			      elementsIntWUGPremiumWk6.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGPremiumWk6.id},${elementsIntWUGPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWUGPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntPlPremiumWk6">
																							<c:if
																								test="${elementsIntPlPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntPlPremiumWk6.destinationCodeArea == 'AC' && 
																			      elementsIntPlPremiumWk6.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlPremiumWk6.id},${elementsIntPlPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntPlPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWOUPlPremiumWk6">
																							<c:if
																								test="${elementsIntWOUPlPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWOUPlPremiumWk6.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlPremiumWk6.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlPremiumWk6.id},${elementsIntWOUPlPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumList71}"
																							var="elementsIntWUPlPremiumWk6">
																							<c:if
																								test="${elementsIntWUPlPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWUPlPremiumWk6.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlPremiumWk6.packageCode == 'platinum'}">

																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlPremiumWk6.id},${elementsIntWUPlPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																	<div class="table-responsive">
																		<table
																			class="table table-striped table-bordered table-warning table-hover text-center">
																			<thead>
																				<tr>
																					<th colspan="11">INDIVIDUAL &amp; SPOUSE</th>
																				</tr>
																				<tr>
																					<th rowspan="2">Plan</th>
																					<th colspan="10">Adult (18 to 70 years)</th>
																				</tr>
																				<tr>
																					<th>Domestic</th>
																					<th colspan="3">Silver</th>
																					<th colspan="3">Gold</th>
																					<th colspan="3">Platinum</th>
																				</tr>
																				<tr>
																					<th>No of Days</th>
																					<th>Malaysia</th>
																					<th>*Asia countries</th>
																					<th>Worldwide excl. USA &amp; Canada</th>
																					<th>Worldwide incl. USA &amp; Canada</th>
																					<th>*Asia countries</th>
																					<th>Worldwide excl. USA &amp; Canada</th>
																					<th>Worldwide incl. USA &amp; Canada</th>
																					<th>*Asia countries</th>
																					<th>Worldwide excl. USA &amp; Canada</th>
																					<th>Worldwide incl. USA &amp; Canada</th>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<td>1-5</td>
																					<td><c:forEach items="${DomPremiumListIndv}"
																							var="elementsIndvPremiumWk1">
																							<c:if
																								test="${elementsIndvPremiumWk1.daysRangeKeycode == 'D1_5'}">
																								<a href="#"
																									onClick="premiumEdit(${elementsIndvPremiumWk1.id},${elementsIndvPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIndvPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntIndvPremiumWk1">
																							<c:if
																								test="${elementsIntIndvPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntIndvPremiumWk1.destinationCodeArea == 'AC' && 
																			      elementsIntIndvPremiumWk1.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntIndvPremiumWk1.id},${elementsIntIndvPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntIndvPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWOUIndvPremiumWk1">
																							<c:if
																								test="${elementsIntWOUIndvPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWOUIndvPremiumWk1.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUIndvPremiumWk1.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUIndvPremiumWk1.id},${elementsIntWOUIndvPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWOUIndvPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWUIndvPremiumWk1">
																							<c:if
																								test="${elementsIntWUIndvPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWUIndvPremiumWk1.destinationCodeArea == 'WU' && 
																			      elementsIntWUIndvPremiumWk1.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUIndvPremiumWk1.id},${elementsIntWUIndvPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWUIndvPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntGIndvPremiumWk1">
																							<c:if
																								test="${elementsIntGIndvPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntGIndvPremiumWk1.destinationCodeArea == 'AC' && 
																			      elementsIntGIndvPremiumWk1.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGIndvPremiumWk1.id},${elementsIntGIndvPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntGIndvPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWOUGIndvPremiumWk1">
																							<c:if
																								test="${elementsIntWOUGIndvPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWOUGIndvPremiumWk1.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGIndvPremiumWk1.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGIndvPremiumWk1.id},${elementsIntWOUGIndvPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGIndvPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWUGIndvPremiumWk1">
																							<c:if
																								test="${elementsIntWUGIndvPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWUGIndvPremiumWk1.destinationCodeArea == 'WU' && 
																			      elementsIntWUGIndvPremiumWk1.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGIndvPremiumWk1.id},${elementsIntWUGIndvPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWUGIndvPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntPlIndvPremiumWk1">
																							<c:if
																								test="${elementsIntPlIndvPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntPlIndvPremiumWk1.destinationCodeArea == 'AC' && 
																			      elementsIntPlIndvPremiumWk1.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlIndvPremiumWk1.id},${elementsIntPlIndvPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntPlIndvPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWOUPlIndvPremiumWk1">
																							<c:if
																								test="${elementsIntWOUPlIndvPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWOUPlIndvPremiumWk1.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlIndvPremiumWk1.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlIndvPremiumWk1.id},${elementsIntWOUPlIndvPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlIndvPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWUPlIndvPremiumWk1">
																							<c:if
																								test="${elementsIntWUPlIndvPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWUPlIndvPremiumWk1.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlIndvPremiumWk1.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlIndvPremiumWk1.id},${elementsIntWUPlIndvPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlIndvPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>6-10</td>
																					<td><c:forEach items="${DomPremiumListIndv}"
																							var="elementsPremiumWk2">
																							<c:if
																								test="${elementsPremiumWk2.daysRangeKeycode == 'D6_10'}">
																								<a href="#"
																									onClick="premiumEdit(${elementsPremiumWk2.id},${elementsPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntPremiumWk2">
																							<c:if
																								test="${elementsIntPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntPremiumWk2.destinationCodeArea == 'AC' && 
																			      elementsIntPremiumWk2.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPremiumWk2.id},${elementsIntPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWOUPremiumWk2">
																							<c:if
																								test="${elementsIntWOUPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWOUPremiumWk2.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPremiumWk2.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPremiumWk2.id},${elementsIntWOUPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWUPremiumWk2">
																							<c:if
																								test="${elementsIntWUPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWUPremiumWk2.destinationCodeArea == 'WU' && 
																			      elementsIntWUPremiumWk2.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPremiumWk2.id},${elementsIntWUPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWUPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntGPremiumWk2">
																							<c:if
																								test="${elementsIntGPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntGPremiumWk2.destinationCodeArea == 'AC' && 
																			      elementsIntGPremiumWk2.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGPremiumWk2.id},${elementsIntGPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntGPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWOUGPremiumWk2">
																							<c:if
																								test="${elementsIntWOUGPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWOUGPremiumWk2.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGPremiumWk2.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGPremiumWk2.id},${elementsIntWOUGPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWUGPremiumWk2">
																							<c:if
																								test="${elementsIntWUGPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWUGPremiumWk2.destinationCodeArea == 'WU' && 
																			      elementsIntWUGPremiumWk2.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGPremiumWk2.id},${elementsIntWUGPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWUGPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntPlPremiumWk2">
																							<c:if
																								test="${elementsIntPlPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntPlPremiumWk2.destinationCodeArea == 'AC' && 
																			      elementsIntPlPremiumWk2.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlPremiumWk2.id},${elementsIntPlPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntPlPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWOUPlPremiumWk2">
																							<c:if
																								test="${elementsIntWOUPlPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWOUPlPremiumWk2.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlPremiumWk2.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlPremiumWk2.id},${elementsIntWOUPlPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWUPlPremiumWk2">
																							<c:if
																								test="${elementsIntWUPlPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWUPlPremiumWk2.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlPremiumWk2.packageCode == 'platinum'}">

																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlPremiumWk2.id},${elementsIntWUPlPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>11-18</td>
																					<td><c:forEach items="${DomPremiumListIndv}"
																							var="elementsPremiumWk3">
																							<c:if
																								test="${elementsPremiumWk3.daysRangeKeycode == 'D11_18'}">
																								<a href="#"
																									onClick="premiumEdit(${elementsPremiumWk3.id},${elementsPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntPremiumWk3">
																							<c:if
																								test="${elementsIntPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntPremiumWk3.destinationCodeArea == 'AC' && 
																			      elementsIntPremiumWk3.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPremiumWk3.id},${elementsIntPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWOUPremiumWk3">
																							<c:if
																								test="${elementsIntWOUPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWOUPremiumWk3.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPremiumWk3.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPremiumWk3.id},${elementsIntWOUPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWUPremiumWk3">
																							<c:if
																								test="${elementsIntWUPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWUPremiumWk3.destinationCodeArea == 'WU' && 
																			      elementsIntWUPremiumWk3.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPremiumWk3.id},${elementsIntWUPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWUPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntGPremiumWk3">
																							<c:if
																								test="${elementsIntGPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntGPremiumWk3.destinationCodeArea == 'AC' && 
																			      elementsIntGPremiumWk3.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGPremiumWk3.id},${elementsIntGPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntGPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<!-- changes done-->
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWOUGPremiumWk3">
																							<c:if
																								test="${elementsIntWOUGPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWOUGPremiumWk3.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGPremiumWk3.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGPremiumWk3.id},${elementsIntWOUGPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWUGPremiumWk3">
																							<c:if
																								test="${elementsIntWUGPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWUGPremiumWk3.destinationCodeArea == 'WU' && 
																			      elementsIntWUGPremiumWk3.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGPremiumWk3.id},${elementsIntWUGPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWUGPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntPlPremiumWk3">
																							<c:if
																								test="${elementsIntPlPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntPlPremiumWk3.destinationCodeArea == 'AC' && 
																			      elementsIntPlPremiumWk3.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlPremiumWk3.id},${elementsIntPlPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntPlPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWOUPlPremiumWk3">
																							<c:if
																								test="${elementsIntWOUPlPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWOUPlPremiumWk3.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlPremiumWk3.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlPremiumWk3.id},${elementsIntWOUPlPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWUPlPremiumWk3">
																							<c:if
																								test="${elementsIntWUPlPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWUPlPremiumWk3.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlPremiumWk3.packageCode == 'platinum'}">

																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlPremiumWk3.id},${elementsIntWUPlPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>19-30</td>
																					<td><c:forEach items="${DomPremiumListIndv}"
																							var="elementsPremiumWk4">
																							<c:if
																								test="${elementsPremiumWk4.daysRangeKeycode == 'D19_30'}">
																								<a href="#"
																									onClick="premiumEdit(${elementsPremiumWk4.id},${elementsPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntPremiumWk4">
																							<c:if
																								test="${elementsIntPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntPremiumWk4.destinationCodeArea == 'AC' && 
																			      elementsIntPremiumWk4.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPremiumWk4.id},${elementsIntPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWOUPremiumWk4">
																							<c:if
																								test="${elementsIntWOUPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWOUPremiumWk4.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPremiumWk4.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPremiumWk4.id},${elementsIntWOUPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWUPremiumWk4">
																							<c:if
																								test="${elementsIntWUPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWUPremiumWk4.destinationCodeArea == 'WU' && 
																			      elementsIntWUPremiumWk4.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPremiumWk4.id},${elementsIntWUPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWUPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntGPremiumWk4">
																							<c:if
																								test="${elementsIntGPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntGPremiumWk4.destinationCodeArea == 'AC' && 
																			      elementsIntGPremiumWk4.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGPremiumWk4.id},${elementsIntGPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntGPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWOUGPremiumWk4">
																							<c:if
																								test="${elementsIntWOUGPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWOUGPremiumWk4.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGPremiumWk4.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGPremiumWk4.id},${elementsIntWOUGPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWUGPremiumWk4">
																							<c:if
																								test="${elementsIntWUGPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWUGPremiumWk4.destinationCodeArea == 'WU' && 
																			      elementsIntWUGPremiumWk4.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGPremiumWk4.id},${elementsIntWUGPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWUGPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntPlPremiumWk4">
																							<c:if
																								test="${elementsIntPlPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntPlPremiumWk4.destinationCodeArea == 'AC' && 
																			      elementsIntPlPremiumWk4.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlPremiumWk4.id},${elementsIntPlPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntPlPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWOUPlPremiumWk4">
																							<c:if
																								test="${elementsIntWOUPlPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWOUPlPremiumWk4.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlPremiumWk4.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlPremiumWk4.id},${elementsIntWOUPlPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWUPlPremiumWk4">
																							<c:if
																								test="${elementsIntWUPlPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWUPlPremiumWk4.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlPremiumWk4.packageCode == 'platinum'}">

																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlPremiumWk4.id},${elementsIntWUPlPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>Each additional week</td>
																					<td>Not covered</td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntPremiumWk5">
																							<c:if
																								test="${elementsIntPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntPremiumWk5.destinationCodeArea == 'AC' && 
																			      elementsIntPremiumWk5.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPremiumWk5.id},${elementsIntPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWOUPremiumWk5">
																							<c:if
																								test="${elementsIntWOUPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWOUPremiumWk5.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPremiumWk5.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPremiumWk5.id},${elementsIntWOUPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWUPremiumWk5">
																							<c:if
																								test="${elementsIntWUPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWUPremiumWk5.destinationCodeArea == 'WU' && 
																			      elementsIntWUPremiumWk5.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPremiumWk5.id},${elementsIntWUPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWUPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntGPremiumWk5">
																							<c:if
																								test="${elementsIntGPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntGPremiumWk5.destinationCodeArea == 'AC' && 
																			      elementsIntGPremiumWk5.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGPremiumWk5.id},${elementsIntGPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntGPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWOUGPremiumWk5">
																							<c:if
																								test="${elementsIntWOUGPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWOUGPremiumWk5.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGPremiumWk5.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGPremiumWk5.id},${elementsIntWOUGPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWUGPremiumWk5">
																							<c:if
																								test="${elementsIntWUGPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWUGPremiumWk5.destinationCodeArea == 'WU' && 
																			      elementsIntWUGPremiumWk5.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGPremiumWk5.id},${elementsIntWUGPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWUGPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntPlPremiumWk5">
																							<c:if
																								test="${elementsIntPlPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntPlPremiumWk5.destinationCodeArea == 'AC' && 
																			      elementsIntPlPremiumWk5.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlPremiumWk5.id},${elementsIntPlPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntPlPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWOUPlPremiumWk5">
																							<c:if
																								test="${elementsIntWOUPlPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWOUPlPremiumWk5.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlPremiumWk5.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlPremiumWk5.id},${elementsIntWOUPlPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWUPlPremiumWk5">
																							<c:if
																								test="${elementsIntWUPlPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWUPlPremiumWk5.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlPremiumWk5.packageCode == 'platinum'}">

																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlPremiumWk5.id},${elementsIntWUPlPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>Annual</td>
																					<td>Not covered</td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntPremiumWk6">
																							<c:if
																								test="${elementsIntPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntPremiumWk6.destinationCodeArea == 'AC' && 
																			      elementsIntPremiumWk6.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPremiumWk6.id},${elementsIntPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWOUPremiumWk6">
																							<c:if
																								test="${elementsIntWOUPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWOUPremiumWk6.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPremiumWk6.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPremiumWk6.id},${elementsIntWOUPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWUPremiumWk6">
																							<c:if
																								test="${elementsIntWUPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWUPremiumWk6.destinationCodeArea == 'WU' && 
																			      elementsIntWUPremiumWk6.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPremiumWk6.id},${elementsIntWUPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWUPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntGPremiumWk6">
																							<c:if
																								test="${elementsIntGPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntGPremiumWk6.destinationCodeArea == 'AC' && 
																			      elementsIntGPremiumWk6.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGPremiumWk6.id},${elementsIntGPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntGPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWOUGPremiumWk6">
																							<c:if
																								test="${elementsIntWOUGPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWOUGPremiumWk6.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGPremiumWk6.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGPremiumWk6.id},${elementsIntWOUGPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWUGPremiumWk6">
																							<c:if
																								test="${elementsIntWUGPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWUGPremiumWk6.destinationCodeArea == 'WU' && 
																			      elementsIntWUGPremiumWk6.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGPremiumWk6.id},${elementsIntWUGPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWUGPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntPlPremiumWk6">
																							<c:if
																								test="${elementsIntPlPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntPlPremiumWk6.destinationCodeArea == 'AC' && 
																			      elementsIntPlPremiumWk6.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlPremiumWk6.id},${elementsIntPlPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntPlPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWOUPlPremiumWk6">
																							<c:if
																								test="${elementsIntWOUPlPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWOUPlPremiumWk6.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlPremiumWk6.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlPremiumWk6.id},${elementsIntWOUPlPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListIndv}"
																							var="elementsIntWUPlPremiumWk6">
																							<c:if
																								test="${elementsIntWUPlPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWUPlPremiumWk6.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlPremiumWk6.packageCode == 'platinum'}">

																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlPremiumWk6.id},${elementsIntWUPlPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>Adventurous Activities</td>
																					<td>Not covered</td>
																					<td colspan="3"><c:forEach
																							items="${IntPremiumListIndv}"
																							var="elementsIntPremiumWk6">
																							<c:if
																								test="${elementsIntPremiumWk6.daysRangeKeycode == 'D_ADV_ACT' && 
																			      elementsIntPremiumWk6.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPremiumWk6.id},${elementsIntPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td colspan="3"><c:forEach
																							items="${IntPremiumListIndv}"
																							var="elementsIntGPremiumWk6">
																							<c:if
																								test="${elementsIntGPremiumWk6.daysRangeKeycode == 'D_ADV_ACT' && 
																			      elementsIntGPremiumWk6.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGPremiumWk6.id},${elementsIntGPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntGPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td colspan="3"><c:forEach
																							items="${IntPremiumListIndv}"
																							var="elementsIntPlPremiumWk6">
																							<c:if
																								test="${elementsIntPlPremiumWk6.daysRangeKeycode == 'D_ADV_ACT' && 
																			      elementsIntPlPremiumWk6.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlPremiumWk6.id},${elementsIntPlPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntPlPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																	<div class="table-responsive">
																		<table
																			class="table table-striped table-bordered table-warning table-hover text-center">
																			<thead>
																				<tr>
																					<th colspan="11">FAMILY</th>
																				</tr>
																				<tr>
																					<th rowspan="2">Plan</th>
																					<th colspan="10">Adult (18 to 70 years) and
																						Child(ren) who are at least 45 days of age and not
																						more than 18 years of age (or not more than 23
																						years of age if studying full-time in a recognized
																						tertiary institution) on the effective date of
																						insurance.</th>
																				</tr>
																				<tr>
																					<th>Domestic</th>
																					<th colspan="3">Silver</th>
																					<th colspan="3">Gold</th>
																					<th colspan="3">Platinum</th>
																				</tr>
																				<tr>
																					<th>No of Days</th>
																					<th>Malaysia</th>
																					<th>*Asia countries</th>
																					<th>Worldwide excl. USA &amp; Canada</th>
																					<th>Worldwide incl. USA &amp; Canada</th>
																					<th>*Asia countries</th>
																					<th>Worldwide excl. USA &amp; Canada</th>
																					<th>Worldwide incl. USA &amp; Canada</th>
																					<th>*Asia countries</th>
																					<th>Worldwide excl. USA &amp; Canada</th>
																					<th>Worldwide incl. USA &amp; Canada</th>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<td>1-5</td>
																					<td><c:forEach items="${DomPremiumListFamily}"
																							var="elementsFPremiumWk1">
																							<c:if
																								test="${elementsFPremiumWk1.daysRangeKeycode == 'D1_5'}">
																								<a href="#"
																									onClick="premiumEdit(${elementsFPremiumWk1.id},${elementsFPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsFPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntFPremiumWk1">
																							<c:if
																								test="${elementsIntFPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntFPremiumWk1.destinationCodeArea == 'AC' && 
																			      elementsIntFPremiumWk1.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntFPremiumWk1.id},${elementsIntFPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntFPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWOUFPremiumWk1">
																							<c:if
																								test="${elementsIntWOUFPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWOUFPremiumWk1.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUFPremiumWk1.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUFPremiumWk1.id},${elementsIntWOUFPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWOUFPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWUFPremiumWk1">
																							<c:if
																								test="${elementsIntWUFPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWUFPremiumWk1.destinationCodeArea == 'WU' && 
																			      elementsIntWUFPremiumWk1.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUFPremiumWk1.id},${elementsIntWUFPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWUFPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntGFPremiumWk1">
																							<c:if
																								test="${elementsIntGFPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntGFPremiumWk1.destinationCodeArea == 'AC' && 
																			      elementsIntGFPremiumWk1.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGFPremiumWk1.id},${elementsIntGFPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntGFPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWOUGFPremiumWk1">
																							<c:if
																								test="${elementsIntWOUGFPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWOUGFPremiumWk1.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGFPremiumWk1.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGFPremiumWk1.id},${elementsIntWOUGFPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGFPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWUGFPremiumWk1">
																							<c:if
																								test="${elementsIntWUGFPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWUGFPremiumWk1.destinationCodeArea == 'WU' && 
																			      elementsIntWUGFPremiumWk1.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGFPremiumWk1.id},{elementsIntWUGFPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWUGFPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntPlFPremiumWk1">
																							<c:if
																								test="${elementsIntPlFPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntPlFPremiumWk1.destinationCodeArea == 'AC' && 
																			      elementsIntPlFPremiumWk1.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlFPremiumWk1.id},${elementsIntPlFPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntPlFPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWOUPlFPremiumWk1">
																							<c:if
																								test="${elementsIntWOUPlFPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWOUPlFPremiumWk1.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlFPremiumWk1.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlFPremiumWk1.id},${elementsIntWOUPlFPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlFPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWUPlFPremiumWk1">
																							<c:if
																								test="${elementsIntWUPlFPremiumWk1.daysRangeKeycode == 'D1_5' && 
																			      elementsIntWUPlFPremiumWk1.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlFPremiumWk1.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlFPremiumWk1.id},${elementsIntWUPlFPremiumWk1.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlFPremiumWk1.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>6-10</td>
																					<td><c:forEach items="${DomPremiumListFamily}"
																							var="elementsFPremiumWk2">
																							<c:if
																								test="${elementsFPremiumWk2.daysRangeKeycode == 'D6_10'}">
																								<a href="#"
																									onClick="premiumEdit(${elementsFPremiumWk2.id},${elementsFPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsFPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntFPremiumWk2">
																							<c:if
																								test="${elementsIntFPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntFPremiumWk2.destinationCodeArea == 'AC' && 
																			      elementsIntFPremiumWk2.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntFPremiumWk2.id},${elementsIntFPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntFPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWOUFPremiumWk2">
																							<c:if
																								test="${elementsIntWOUFPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWOUFPremiumWk2.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUFPremiumWk2.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUFPremiumWk2.id},${elementsIntWOUFPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWOUFPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWUFPremiumWk2">
																							<c:if
																								test="${elementsIntWUFPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWUFPremiumWk2.destinationCodeArea == 'WU' && 
																			      elementsIntWUFPremiumWk2.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUFPremiumWk2.id},${elementsIntWUFPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWUFPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntGFPremiumWk2">
																							<c:if
																								test="${elementsIntGFPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntGFPremiumWk2.destinationCodeArea == 'AC' && 
																			      elementsIntGFPremiumWk2.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGFPremiumWk2.id},${elementsIntGFPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntGFPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWOUGFPremiumWk2">
																							<c:if
																								test="${elementsIntWOUGFPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWOUGFPremiumWk2.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGFPremiumWk2.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGFPremiumWk2.id},${elementsIntWOUGFPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGFPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWUGFPremiumWk2">
																							<c:if
																								test="${elementsIntWUGFPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWUGFPremiumWk2.destinationCodeArea == 'WU' && 
																			      elementsIntWUGFPremiumWk2.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGFPremiumWk2.id},${elementsIntWUGFPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWUGFPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntPlFPremiumWk2">
																							<c:if
																								test="${elementsIntPlFPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntPlFPremiumWk2.destinationCodeArea == 'AC' && 
																			      elementsIntPlFPremiumWk2.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlFPremiumWk2.id},${elementsIntPlFPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntPlFPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWOUPlFPremiumWk2">
																							<c:if
																								test="${elementsIntWOUPlFPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWOUPlFPremiumWk2.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlFPremiumWk2.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlFPremiumWk2.id},${elementsIntWOUPlFPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlFPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWUPlFPremiumWk2">
																							<c:if
																								test="${elementsIntWUPlFPremiumWk2.daysRangeKeycode == 'D6_10' && 
																			      elementsIntWUPlFPremiumWk2.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlFPremiumWk2.packageCode == 'platinum'}">

																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlFPremiumWk2.id},${elementsIntWUPlFPremiumWk2.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlFPremiumWk2.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>11-18</td>
																					<td><c:forEach items="${DomPremiumListFamily}"
																							var="elementsFPremiumWk3">
																							<c:if
																								test="${elementsFPremiumWk3.daysRangeKeycode == 'D11_18'}">
																								<a href="#"
																									onClick="premiumEdit(${elementsFPremiumWk3.id},${elementsFPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsFPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntFPremiumWk3">
																							<c:if
																								test="${elementsIntFPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntFPremiumWk3.destinationCodeArea == 'AC' && 
																			      elementsIntFPremiumWk3.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntFPremiumWk3.id},${elementsIntFPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntFPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWOUFPremiumWk3">
																							<c:if
																								test="${elementsIntWOUFPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWOUFPremiumWk3.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUFPremiumWk3.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUFPremiumWk3.id},${elementsIntWOUFPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWOUFPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWUFPremiumWk3">
																							<c:if
																								test="${elementsIntWUFPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWUFPremiumWk3.destinationCodeArea == 'WU' && 
																			      elementsIntWUFPremiumWk3.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUFPremiumWk3.id},${elementsIntWUFPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWUFPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntGFPremiumWk3">
																							<c:if
																								test="${elementsIntGFPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntGFPremiumWk3.destinationCodeArea == 'AC' && 
																			      elementsIntGFPremiumWk3.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGFPremiumWk3.id},${elementsIntGFPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntGFPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWOUGFPremiumWk3">
																							<c:if
																								test="${elementsIntWOUGFPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWOUGFPremiumWk3.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGFPremiumWk3.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGFPremiumWk3.id},${elementsIntWOUGFPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGFPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWUGFPremiumWk3">
																							<c:if
																								test="${elementsIntWUGFPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWUGFPremiumWk3.destinationCodeArea == 'WU' && 
																			      elementsIntWUGFPremiumWk3.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGFPremiumWk3.id},${elementsIntWUGFPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWUGFPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntPlFPremiumWk3">
																							<c:if
																								test="${elementsIntPlFPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntPlFPremiumWk3.destinationCodeArea == 'AC' && 
																			      elementsIntPlFPremiumWk3.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlFPremiumWk3.id},${elementsIntPlFPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntPlFPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWOUPlFPremiumWk3">
																							<c:if
																								test="${elementsIntWOUPlFPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWOUPlFPremiumWk3.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlFPremiumWk3.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlFPremiumWk3.id},${elementsIntWOUPlFPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlFPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWUPlFPremiumWk3">
																							<c:if
																								test="${elementsIntWUPlFPremiumWk3.daysRangeKeycode == 'D11_18' && 
																			      elementsIntWUPlFPremiumWk3.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlFPremiumWk3.packageCode == 'platinum'}">

																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlFPremiumWk3.id},${elementsIntWUPlFPremiumWk3.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlFPremiumWk3.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>19-30</td>
																					<td><c:forEach items="${DomPremiumListFamily}"
																							var="elementsFPremiumWk4">
																							<c:if
																								test="${elementsFPremiumWk4.daysRangeKeycode == 'D19_30'}">
																								<a href="#"
																									onClick="premiumEdit(${elementsFPremiumWk4.id},${elementsFPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsFPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntFPremiumWk4">
																							<c:if
																								test="${elementsIntFPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntFPremiumWk4.destinationCodeArea == 'AC' && 
																			      elementsIntFPremiumWk4.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntFPremiumWk4.id},${elementsIntFPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntFPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWOUFPremiumWk4">
																							<c:if
																								test="${elementsIntWOUFPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWOUFPremiumWk4.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUFPremiumWk4.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUFPremiumWk4.id},${elementsIntWOUFPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWOUFPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWUFPremiumWk4">
																							<c:if
																								test="${elementsIntWUFPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWUFPremiumWk4.destinationCodeArea == 'WU' && 
																			      elementsIntWUFPremiumWk4.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUFPremiumWk4.id},${elementsIntWUFPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWUFPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntGFPremiumWk4">
																							<c:if
																								test="${elementsIntGFPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntGFPremiumWk4.destinationCodeArea == 'AC' && 
																			      elementsIntGFPremiumWk4.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGFPremiumWk4.id},${elementsIntGFPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntGFPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWOUGFPremiumWk4">
																							<c:if
																								test="${elementsIntWOUGFPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWOUGFPremiumWk4.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGFPremiumWk4.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGFPremiumWk4.id},${elementsIntWOUGFPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGFPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWUGFPremiumWk4">
																							<c:if
																								test="${elementsIntWUGFPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWUGFPremiumWk4.destinationCodeArea == 'WU' && 
																			      elementsIntWUGFPremiumWk4.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGFPremiumWk4.id},${elementsIntWUGFPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWUGFPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntPlFPremiumWk4">
																							<c:if
																								test="${elementsIntPlFPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntPlFPremiumWk4.destinationCodeArea == 'AC' && 
																			      elementsIntPlFPremiumWk4.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlFPremiumWk4.id},${elementsIntPlFPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntPlFPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWOUPlFPremiumWk4">
																							<c:if
																								test="${elementsIntWOUPlFPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWOUPlFPremiumWk4.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlFPremiumWk4.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlFPremiumWk4.id},${elementsIntWOUPlFPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlFPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWUPlFPremiumWk4">
																							<c:if
																								test="${elementsIntWUPlFPremiumWk4.daysRangeKeycode == 'D19_30' && 
																			      elementsIntWUPlFPremiumWk4.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlFPremiumWk4.packageCode == 'platinum'}">

																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlFPremiumWk4.id},${elementsIntWUPlFPremiumWk4.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlFPremiumWk4.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>Each additional week</td>
																					<td>Not covered</td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntFPremiumWk5">
																							<c:if
																								test="${elementsIntFPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntFPremiumWk5.destinationCodeArea == 'AC' && 
																			      elementsIntFPremiumWk5.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntFPremiumWk5.id},${elementsIntFPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntFPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWOUFPremiumWk5">
																							<c:if
																								test="${elementsIntWOUFPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWOUFPremiumWk5.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUFPremiumWk5.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUFPremiumWk5.id},${elementsIntWOUFPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWOUFPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWUFPremiumWk5">
																							<c:if
																								test="${elementsIntWUFPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWUFPremiumWk5.destinationCodeArea == 'WU' && 
																			      elementsIntWUFPremiumWk5.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUFPremiumWk5.id},${elementsIntWUFPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWUFPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntGFPremiumWk5">
																							<c:if
																								test="${elementsIntGFPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntGFPremiumWk5.destinationCodeArea == 'AC' && 
																			      elementsIntGFPremiumWk5.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGFPremiumWk5.id},${elementsIntGFPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntGFPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWOUGFPremiumWk5">
																							<c:if
																								test="${elementsIntWOUGFPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWOUGFPremiumWk5.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGFPremiumWk5.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGFPremiumWk5.id},${elementsIntWOUGFPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGFPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWUGFPremiumWk5">
																							<c:if
																								test="${elementsIntWUGFPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWUGFPremiumWk5.destinationCodeArea == 'WU' && 
																			      elementsIntWUGFPremiumWk5.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGFPremiumWk5.id},${elementsIntWUGFPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWUGFPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntPlFPremiumWk5">
																							<c:if
																								test="${elementsIntPlFPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntPlFPremiumWk5.destinationCodeArea == 'AC' && 
																			      elementsIntPlFPremiumWk5.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlFPremiumWk5.id},${elementsIntPlFPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntPlFPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWOUPlFPremiumWk5">
																							<c:if
																								test="${elementsIntWOUPlFPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWOUPlFPremiumWk5.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlFPremiumWk5.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlFPremiumWk5.id},${elementsIntWOUPlFPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlFPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWUPlFPremiumWk5">
																							<c:if
																								test="${elementsIntWUPlFPremiumWk5.daysRangeKeycode == 'D_ADD_WEEK' && 
																			      elementsIntWUPlFPremiumWk5.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlFPremiumWk5.packageCode == 'platinum'}">

																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlFPremiumWk5.id},${elementsIntWUPlFPremiumWk5.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlFPremiumWk5.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>Annual</td>
																					<td>Not covered</td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntFPremiumWk6">
																							<c:if
																								test="${elementsIntFPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntFPremiumWk6.destinationCodeArea == 'AC' && 
																			      elementsIntFPremiumWk6.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntFPremiumWk6.id},${elementsIntFPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntFPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWOUFPremiumWk6">
																							<c:if
																								test="${elementsIntWOUFPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWOUFPremiumWk6.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUFPremiumWk6.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUFPremiumWk6.id},${elementsIntWOUFPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWOUFPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWUFPremiumWk6">
																							<c:if
																								test="${elementsIntWUFPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWUFPremiumWk6.destinationCodeArea == 'WU' && 
																			      elementsIntWUFPremiumWk6.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUFPremiumWk6.id},${elementsIntWUFPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWUFPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntGFPremiumWk6">
																							<c:if
																								test="${elementsIntGFPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntGFPremiumWk6.destinationCodeArea == 'AC' && 
																			      elementsIntGFPremiumWk6.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGFPremiumWk6.id},${elementsIntGFPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntGFPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWOUGFPremiumWk6">
																							<c:if
																								test="${elementsIntWOUGFPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWOUGFPremiumWk6.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUGFPremiumWk6.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUGFPremiumWk6.id},${elementsIntWOUGFPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWOUGFPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWUGFPremiumWk6">
																							<c:if
																								test="${elementsIntWUGFPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWUGFPremiumWk6.destinationCodeArea == 'WU' && 
																			      elementsIntWUGFPremiumWk6.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUGFPremiumWk6.id},${elementsIntWUGFPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWUGFPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntPlFPremiumWk6">
																							<c:if
																								test="${elementsIntPlFPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntPlFPremiumWk6.destinationCodeArea == 'AC' && 
																			      elementsIntPlFPremiumWk6.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlFPremiumWk6.id},${elementsIntPlFPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntPlFPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWOUPlFPremiumWk6">
																							<c:if
																								test="${elementsIntWOUPlFPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWOUPlFPremiumWk6.destinationCodeArea == 'WOU' && 
																			      elementsIntWOUPlFPremiumWk6.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWOUPlFPremiumWk6.id},${elementsIntWOUPlFPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWOUPlFPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td><c:forEach items="${IntPremiumListFamily}"
																							var="elementsIntWUPlFPremiumWk6">
																							<c:if
																								test="${elementsIntWUPlFPremiumWk6.daysRangeKeycode == 'D356' && 
																			      elementsIntWUPlFPremiumWk6.destinationCodeArea == 'WU' && 
																			      elementsIntWUPlFPremiumWk6.packageCode == 'platinum'}">

																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntWUPlFPremiumWk6.id},${elementsIntWUPlFPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntWUPlFPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																				<tr>
																					<td>Adventurous Activities</td>
																					<td>Not covered</td>
																					<td colspan="3"><c:forEach
																							items="${IntPremiumListFamily}"
																							var="elementsIntFPremiumWk6">
																							<c:if
																								test="${elementsIntFPremiumWk6.daysRangeKeycode == 'D_ADV_ACT' && 
																			      elementsIntFPremiumWk6.packageCode == 'silver'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntFPremiumWk6.id},${elementsIntFPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntFPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td colspan="3"><c:forEach
																							items="${IntPremiumListFamily}"
																							var="elementsIntGFPremiumWk6">
																							<c:if
																								test="${elementsIntGFPremiumWk6.daysRangeKeycode == 'D_ADV_ACT' && 
																			      elementsIntGFPremiumWk6.packageCode == 'gold'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntGFPremiumWk6.id},${elementsIntGFPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntGFPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																					<td colspan="3"><c:forEach
																							items="${IntPremiumListFamily}"
																							var="elementsIntPlFPremiumWk6">
																							<c:if
																								test="${elementsIntPlFPremiumWk6.daysRangeKeycode == 'D_ADV_ACT' && 
																			      elementsIntPlFPremiumWk6.packageCode == 'platinum'}">
																								<a href="#"
																									onClick="premiumIntEdit(${elementsIntPlFPremiumWk6.id},${elementsIntPlFPremiumWk6.premiumValue});">
																									<c:out
																										value="${elementsIntPlFPremiumWk6.premiumValue}" />
																								</a>
																							</c:if>
																						</c:forEach></td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</div>

															</div>
															<!-- /.the-box -->
															<!-- End warning color table -->
														</div>
													</div>

													<div class="gap gap-mini"></div>


												</div>
											</div>
											<!--row -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.container -->

				<c:set var="travelpervalue" value="${fn:split(param.pid,'$') }" />

				<c:if
					test="${(travelpervalue[0] eq 1) || (travelpervalue[0] eq 3 )}">


					<form:form action="UpdateTripCareDProdRatesForApproval"
						method="post" name="UpdateTCForm" id="TCPremium">
						<div class="modal fade" id="PremDomestic" tabindex="-1"
							role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<h4 class="modal-title" id="myModalLabel">Product
											Management | TripCare360 | Product Rates</h4>
									</div>
									<div class="modal-body">
										<div class="form-harizontal">
											<div class="form-group">
												<label>Premium Value<span class="text-danger">*</span></label>
												<input type="text" name="premiumval" id="premiumval"
													class="form-control"> <input type="hidden"
													name="pid" id="pid" value="0"> <span
													id="msg_premiumval" class="hidden"
													style="color: red; text-align: left">Please enter
													your Premium</span>
											</div>
										</div>

									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default"
											data-dismiss="modal">Cancel</button>
										<!--    <button type="button" class="btn btn-warning">Save</button> -->
										<label class="btn btn-default" id="uploadTCPdf"> <input
											type="submit" value="Save" class="btn btn-warning"
											onClick="submitTCPremInfo();">
										</label>
									</div>
								</div>
							</div>
						</div>
					</form:form>

					<!--  -->
					<form:form action="UpdateTripCareIntProdRatesForApproval"
						method="post" name="UpdateTCIntForm" id="TCIntPremium">
						<div class="modal fade" id="PremIntnational" tabindex="-1"
							role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<h4 class="modal-title" id="myModalLabel">Product
											Management | TripCare360 | Product Rates</h4>
									</div>
									<div class="modal-body">
										<div class="form-harizontal">
											<div class="form-group">
												<label>Premium Value<span class="text-danger">*</span></label>
												<input type="text" name="premiumintval" id="premiumintval"
													class="form-control"> <input type="hidden"
													name="pintid" id="pintid" value="0"> <span
													id="msg_premiumintval" class="hidden"
													style="color: red; text-align: left">Please enter
													your Premium</span>
											</div>
										</div>

									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default"
											data-dismiss="modal">Cancel</button>
										<!--    <button type="button" class="btn btn-warning">Save</button> -->
										<label class="btn btn-default" id="uploadTCPrem"> <input
											type="submit" value="Save" class="btn btn-warning"
											onClick="submitTCPremIntInfo();">
										</label>
									</div>
								</div>
							</div>
						</div>
					</form:form>
				</c:if>
			</div>
			<!-- /.page-content -->
			<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
			<jsp:include page="/pageFooter.jsp" />
			<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
		</div>
		<!-- /.wrapper -->
		<!-- END PAGE CONTENT -->



		<!-- BEGIN BACK TO TOP BUTTON -->
		<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
		<!-- END BACK TO TOP -->


		<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

		<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
		<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
		<%--    <jsp:include page="siteFooter.jsp" />   --%>
		<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->


		<!-- JAVA Script Goes Here -->

		<script
			src="<c:url value="/resources/assets/js/jquery-2.2.4.min.js" />"></script>
		<!--  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
		<script src="assets/js/apps.js"></script>
		<script src="assets/plugins/retina/retina.min.js"></script>
		<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
		<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>

		<script type="text/javascript"
			src="assets/js/jquery.nicescroll.min.js"></script>
		<!-- PLUGINS -->
		<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
		<script src="assets/plugins/prettify/prettify.js"></script>
		<script
			src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

		<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
		<script src="assets/plugins/icheck/icheck.min.js"></script>
		<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
		<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
		<script src="assets/plugins/mask/jquery.mask.min.js"></script>
		<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
		<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
		<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
		<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
		<script
			src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
		<script src="assets/plugins/summernote/summernote.min.js"></script>
		<script src="assets/plugins/markdown/markdown.js"></script>
		<script src="assets/plugins/markdown/to-markdown.js"></script>
		<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
		<script src="assets/plugins/slider/bootstrap-slider.js"></script>
		<script src="assets/plugins/toastr/toastr.js"></script>
		<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
		<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
		<script type="text/javascript" src="assets/js/shieldui-all.min.js"></script>
		<script type="text/javascript" src="assets/js/jszip.min.js"></script>
		<!--  Table Export -->
		<!-- KNOB JS -->
		<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
		<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
		<script src="assets/plugins/jquery-knob/knob.js"></script>
		<script src="assets/js/jquery.dataTables.min.js"></script>
		<script src="assets/js/dataTables.bootstrap.min.js"></script>
		<script src="assets/js/dataTables.buttons.min.js"></script>
		<script src="assets/js/buttons.bootstrap.min.js"></script>
		<script src="assets/js/jszip.min.js"></script>
		<script src="assets/js/pdfmake.min.js"></script>
		<script src="assets/js/vfs_fonts.js"></script>
		<script src="assets/js/buttons.html5.min.js"></script>
		<script src="assets/js/buttons.print.min.js"></script>
		<script src="assets/js/buttons.colVis.min.js"></script>
		<script src="assets/js/toastr.min.js"></script>
		<!-- <script type="text/javascript" src="//code.jquery.com/jquery-1.11.3.min.js"></script> -->
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/jAlert/jAlert.min.js"></script>
		<script src="assets/jAlert/jAlert-functions.min.js"></script>

		<script type="text/javascript">
                  function premiumEdit(value,pval) {
				    // alert("hiiiii");
				     var txt=value;
				     
				      $('#pid').val(value);
					  $('#premiumval').val(pval);
				     $('#PremDomestic').modal('show');
				   
			   }
                  
                  
			    function premiumIntEdit(value,pval) {
			    	//  alert("hellooo");
				     var txt=value;
				     
				      $('#pintid').val(value);
					  $('#premiumintval').val(pval);
				     $('#PremIntnational').modal('show');
				    
	  
				   
			   }
			   
			   
			   function submitTCPremInfo(){
         result=true;           
       if(!$('#premiumval').val().length) {
             $('#msg_premiumval').removeClass('hidden');
             $('#msg_premiumval').addClass('visible');
           
             $('#premiumval').focus();
             result=false;
         }
         if (result==true) {
         	$('#UpdateTCForm').submit();   
         }
         }
         function submitTCPremIntInfo(){
         result=true;           
       if(!$('#premiumintval').val().length) {
             $('#msg_premiumintval').removeClass('hidden');
             $('#msg_premiumintval').addClass('visible');
           
             $('#premiumintval').focus();
             result=false;
         }
         if (result==true) {
         	$('#UpdateTCIntForm').submit();   
         }
         }
         
         $(document).ready(function() {
          var tcUpdatedMessage="<c:out value="${tcUpdatedMessage}"/>";
	 		       // show when the button is clicked
	 		       if (tcUpdatedMessage.length) {
	 		    	   successAlert('Success!',tcUpdatedMessage);
	 		       }
	 		      
	      var tcUpdatedprMessage="<c:out value="${tcUpdatedprMessage}"/>";
	 		       // show when the button is clicked
	 		       if (tcUpdatedprMessage.length) {
	 		    	  successAlert('Success!',tcUpdatedprMessage);
	 		       }
		    });
         </script>

		<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
</body>
</html>