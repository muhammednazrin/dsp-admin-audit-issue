$(document).ready(function() {
    
    // bar for Product Campaign Analysis (MTD)
    var chart = c3.generate({
    bindto: '#productcampaign',
    data: {
      columns: [
        ['Expenses', 32000],
        ['Revenue', 37000]
      ],
      types: {
        Expenses: 'bar',
        Revenue: 'bar'
      },
        
    },
        
    axis: {
      x: {
        type: 'categorized',
      }
    },
        
    });
    
    
    //chart for Renewal Retention(MTD)
    var chart = c3.generate({
    bindto: '#renewalretention',
    data: {
        // iris data from R
        columns: [
            ['WTC', 30],
            ['HOHH', 120],
            ['Motor', 120],
            ['TermLife', 120],
        ],
        type : 'pie',
        colors: {
            WTC: '#337ab7',
            HOHH: '#d9534f',
            Motor: '#f0ad4e',
            TermLife: '#5cb85c',
        },
    },
        
    legend: {
        show: true
        },
        
    });
    
    // Actual vs Budget(MTD)
    var chart = c3.generate({
    bindto: '#actualbmtd',
        data: {
            x: 'x',
            columns: [
                ['x', 'WTC', 'HOHH', 'Motor', 'Term Life'],
                ['Actual', 90, 100, 140, 200],
                ['Budget', 40, 120, 140, 20],
                ],
            type: 'bar',
        },
        axis: {
            rotated: true,
          x: {
            type: 'categorized',
          }
        },
    });
    
    // Actual vs Budget(MTD)
    var chart = c3.generate({
    bindto: '#actualbytd',
        data: {
            x: 'x',
            columns: [
                ['x', 'WTC', 'HOHH', 'Motor', 'Term Life'],
                ['Actual', 90, 100, 140, 200],
                ['Budget', 40, 120, 140, 20],
                ],
            type: 'bar',
        },
        axis: {
            rotated: true,
          x: {
            type: 'categorized',
          }
        },
    });
    
    // Actual vs Budget(MTD)
    var chart = c3.generate({
    bindto: '#cyberap',
        data: {
            x: 'x',
            columns: [
                ['x', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                ['Performance', 90, 100, 180, 130, 220, 250],
                ],
        },
        axis: {
          x: {
            type: 'categorized',
          }
        },
    });
    
    // MOTOR : Attachment Ratio(MTD)
    var chart = c3.generate({
    bindto: '#motorar',
        data: {
            x: 'x',
            columns: [
                ['x', 'Windscreen', 'SP', 'PA'],
                ['Performance', 90, 100, 180],
                ],
            type: 'bar',
        },
        axis: {
          x: {
            type: 'categorized',
          }
        },
    });
    
    
    // Source of Leads (MTD)
    var chart = c3.generate({
    bindto: '#sol',
        data: {
            columns: [
                ['Direct', 500],
                ['CWP', 1000],
                ['Email', 300],
                ['SMS', 200],
                ['Exit Intent', 600],
                ['Sale Tool', 900],
                ['Leads', 500],
                ['Cyber Agent Website', 450],
                ],
            type: 'donut',
        },
        axis: {
          x: {
            type: 'categorized',
          }
        },
        donut: {
            title: "Point Of Sale"
        }
    });
    
    // Abandonment vs Follow up Rate(MTD)
    var colors = ['#e7e7e6', '#ffbf00'];
    var chart = c3.generate({
    bindto: '#afur',
        data: {
            x: 'x',
            columns: [
                ['x', 'Abandonment', 'Follow up'],
                ['MTD', 90, 30],
                ],
            type: 'bar',
            color: function (color, d) {
                return colors[d.index];
            },
            labels: true
        },
        axis: {
          x: {
            type: 'category',
            categories: ['cat1', 'cat2', 'cat3', 'cat4', 'cat5', 'cat6', 'cat7', 'cat8', 'cat9']
          }
        },
        legend: {
            show: false
        }
    });
    
    
    var chart = c3.generate({
          bindto: '#cmpt',
          data: {
            x: 'x',
            columns: [
                ['x', '2016-01-01', '2016-01-02', '2016-01-03', '2016-01-04', '2016-01-05', '2016-01-06', '2016-01-07', '2016-01-08'
                , '2016-01-09', '2016-01-10', '2016-01-11', '2016-01-12', '2016-01-13', '2016-01-14', '2016-01-15', '2016-01-16'
                , '2016-01-17', '2016-01-18', '2016-01-19', '2016-01-20', '2016-01-21', '2016-01-22', '2016-01-23'],
                ['WTC', 30, 200, 200, 400, 150, 250],
                ['HOHH', 10, 300, 300, 400, 150, 250],
                ['Motor', 40, 100, 300, 400, 150, 250],
                ['TermLife', 70, 500, 100, 400, 150, 250],
            ],
            //type: 'bar'
            colors: {
                WTC: '#337ab7',
                HOHH: '#d9534f',
                Motor: '#f0ad4e',
                TermLife: '#5cb85c',
            },
          },
          axis: {
            x: {
              type: 'timeseries',
              tick: {
                format: '%e %b %Y',
                //fit: false
              }
            }
          }
        });

});


//Performance by Entity
    var dataMonth = [
    ['x', 'WTC', 'HOHH', 'Motor', 'Term Life'],
    ['EIB', 90, 100, 140, 200],
    ['ETB', 40, 120, 140, 20],
    ];

    var dataYear = [
    ['x', 'WTC', 'HOHH', 'Motor', 'Term Life'],
    ['EIB', 20, 1100, 1140, 2100],
    ['ETB', 40, 120, 140, 20],
    ];

$(function () {

    // Initial chart
    var chart = c3.generate({
        bindto: '#entity',
        data: {
            x: 'x',
            columns: dataMonth,
            type: 'bar',
        },
        axis: {
            rotated: true,
          x: {
            type: 'categorized',
          }
        },
    });

    // Redraw chart depending on which option is selected
    $("#DataType").change(function (evt) {
        var timeSelection = eval($("#DataType").val());
        var chart = c3.generate({
            bindto: '#entity',
            data: {
                x: 'x',
                columns: timeSelection,
                type: 'bar',
            },
            axis: {
                rotated: true,
              x: {
                type: 'categorized',
              }
            },
        });
    });

});


//Performance by Entity
    var CrossSell = [
    ['x', 'WTC', 'HOHH', 'Motor', 'Term'],
    ['WTC', 77, 100, 140, 100],
    ['HOHH', 40, 77, 140, 20],
    ['Motor', 40, 120, 77, 20],
    ['Term Life', 40, 120, 140, 77],
    ];

    var CrossUp = [
    ['x', 'WTC', 'HOHH', 'Motor', 'Term'],
    ['WTC', 77, 50, 50, 50],
    ['HOHH', 40, 77, 50, 20],
    ['Motor', 40, 10, 77, 20],
    ['Term Life', 40, 20, 10, 77],
    ];

$(function () {

    // Initial chart
    var chart = c3.generate({
        bindto: '#CrossRatio',
        data: {
            x: 'x',
            rows: CrossSell,
            type: 'bar',
            colors: {
                WTC: '#337ab7',
                HOHH: '#d9534f',
                Motor: '#f0ad4e',
                Term: '#5cb85c',
            },
            names: {
                WTC: 'WTC',
                HOHH: 'HOHH',
                Motor: 'Motor',
                Term: 'Term Life'
            }
        },
        axis: {
          x: {
            type: 'categorized',
          }
        },
    });

    // Redraw chart depending on which option is selected
    $("#Datacross").change(function (evt) {
        var timeSelection = eval($("#Datacross").val());
        var chart = c3.generate({
            bindto: '#CrossRatio',
            data: {
                x: 'x',
                rows: timeSelection,
                type: 'bar',
                colors: {
                    WTC: '#337ab7',
                    HOHH: '#d9534f',
                    Motor: '#f0ad4e',
                    Term: '#5cb85c',
                },
                names: {
                    WTC: 'WTC',
                    HOHH: 'HOHH',
                    Motor: 'Motor',
                    Term: 'Term Life'
                }
            },
            axis: {
              x: {
                type: 'categorized',
              }
            },
        });
    });

});




// funnel Sale
var dataf = [
        ['Step 1', 15654],
        ['Step 2', 4064],
        ['Step 3', 1987],
    ];
var datata = [
        ['Step 1', 5654],
        ['Step 2', 4064],
        ['Step 3', 1987],
    ];

//$(function () {
//
//    $('#funnel2').highcharts({
//        chart: {
//            type: 'funnel',
//            marginRight: 100
//        },
//        title: false,
//        plotOptions: {
//            series: {
//                dataLabels: {
//                    enabled: true,
//                    format: '<b>{point.percentage:.1f}% </b> <b>{point.name}</b>',
//                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
//                    softConnector: true
//                },
//                neckWidth: '30%',
//                neckHeight: '25%',
//                width: '64%',
//                //-- Other available options
//                // height: pixels or percent
//                // width: pixels or percent
//
//            },
//            area: {
//                stacking: 'percent',
//                lineColor: '#ffffff',
//                lineWidth: 1,
//                marker: {
//                    lineWidth: 1,
//                    lineColor: '#ffffff'
//                }
//            }
//        },
//        legend: {
//            enabled: false
//        },
//        series: [{
//            name: 'Sales Funnel',
//            data: dataf
//        }]
//    });
//});

$(function () {

    // Initial chart
    var funel =$("#funel2").highcharts({
        chart: {
            type: 'funnel',
            marginRight: 100
        },
        title: false,
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.percentage:.1f}% </b> <b>{point.name}</b>',
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                    softConnector: true
                },
                neckWidth: '30%',
                neckHeight: '25%',
                width: '64%',
                //-- Other available options
                // height: pixels or percent
                // width: pixels or percent

            },
            area: {
                stacking: 'percent',
                lineColor: '#ffffff',
                lineWidth: 1,
                marker: {
                    lineWidth: 1,
                    lineColor: '#ffffff'
                }
            }
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Sales Funnel',
            data: dataf
        }]
    });

    // Redraw chart depending on which option is selected
    $("#FunnelType").change(function (evt) {
        var timeSelection2 = eval($("#FunnelType").val());
        var funel =$("#funel2").highcharts({
            chart: {
                type: 'funnel',
                marginRight: 100
            },
            title: false,
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.percentage:.1f}% </b> <b>{point.name}</b>',
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                        softConnector: true
                    },
                    neckWidth: '30%',
                    neckHeight: '25%',
                    width: '64%',
                    //-- Other available options
                    // height: pixels or percent
                    // width: pixels or percent

                },
                area: {
                    stacking: 'percent',
                    lineColor: '#ffffff',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#ffffff'
                    }
                }
            },
            legend: {
                enabled: false
            },
            series: [{
                name: 'Sales Funnel',
                data: timeSelection2
            }]
        });
    });

});






