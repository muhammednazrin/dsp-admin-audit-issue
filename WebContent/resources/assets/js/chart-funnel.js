// funnel Sale
var dataf = [
        ['Step 1', 15654],
        ['Step 2', 4064],
        ['Step 3', 1987],
    ];
var datata = [
        ['Step 1', 5654],
        ['Step 2', 4064],
        ['Step 3', 1987],
    ];

$(function () {

    // Initial chart
    var funel =$("#funel2").highcharts({
        chart: {
            type: 'funnel',
            marginRight: 100
        },
        title: false,
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.percentage:.1f}% </b> <b>{point.name}</b>',
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                    softConnector: true
                },
                neckWidth: '30%',
                neckHeight: '25%',
                width: '64%',
                //-- Other available options
                // height: pixels or percent
                // width: pixels or percent

            },
            area: {
                stacking: 'percent',
                lineColor: '#ffffff',
                lineWidth: 1,
                marker: {
                    lineWidth: 1,
                    lineColor: '#ffffff'
                }
            }
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Sales Funnel',
            data: dataf
        }]
    });

    // Redraw chart depending on which option is selected
    $("#FunnelType").change(function (evt) {
        var timeSelection2 = eval($("#FunnelType").val());
        var funel =$("#funel2").highcharts({
            chart: {
                type: 'funnel',
                marginRight: 100
            },
            title: false,
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.percentage:.1f}% </b> <b>{point.name}</b>',
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                        softConnector: true
                    },
                    neckWidth: '30%',
                    neckHeight: '25%',
                    width: '64%',
                    //-- Other available options
                    // height: pixels or percent
                    // width: pixels or percent

                },
                area: {
                    stacking: 'percent',
                    lineColor: '#ffffff',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#ffffff'
                    }
                }
            },
            legend: {
                enabled: false
            },
            series: [{
                name: 'Sales Funnel',
                data: timeSelection2
            }]
        });
    });

});






