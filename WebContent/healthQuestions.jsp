
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">


<title>Etiqa</title>
<link
	href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900'
	rel='stylesheet' type='text/css'>
<!-- Bootstrap core CSS -->
<link href="<c:url value="/resources/assets/css/bootstrap.min.css" />"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link href="<c:url value="/resources/assets/css/rangeslider.css" />"
	rel="stylesheet">
<link
	href="<c:url value="/resources/assets/css/font-awesome.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/assets/css/style.css" />"
	rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	<!-- Fixed navbar -->
	<nav id="header"
		class="navbar navbar-default navbar-fixed-top navbar-custom">
		<div class="container">
			<div class="navbar-header">
				<!--
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
-->
				<a class="navbar-brand" href="#"><img alt="Etiqa logo"
					class="logo"
					src="<c:url value="/resources/assets/img/Etiqa-logo.png" />"></a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">

				<div class="page-title">
					<h1>Term Life Insurance</h1>
				</div>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#"><span class="cwp-header-title">Need
								Help?</span><br>
						<span class="cwp-header-title header-summary">Talk to an
								agent.</span></a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>
	<div class="container-fluid content-header">
		<div class="container">
			<div class="header-hero-top">Lorem Ipsum</div>
			<div class="content-breadcrumbs">
				<ul class="progressbar">
					<li class="active">Get Quote</li>
					<li>Details</li>
					<li>Confirmation</li>
					<li>Receipt</li>
				</ul>
			</div>
		</div>
	</div>
	<br>

	<div class="container">
		<table>
			<tr>
				<c:forEach items="${myList}" var="element">

					<td>${element.agentCode}</td>
					<br>

				</c:forEach>
			</tr>
		</table>
		<section>
			<div class="col-xs-12 ">
				<form class="form-horizontal text-center">
					<div class="row">
						<div class="col-xs-12 text-center form-label-bold-18">
							<br />
							<br />
							<h2>Health Questions</h2>
						</div>
					</div>

					<!-- Slider -->
					<section class="range-slider">
						<div class="row">
							<div class="col-sm-2 hidden-xs"></div>
							<div class="col-sm-8 col-xs-12">
								<h3>
									What is Your Current Height ?
									<p>(CM)</p>
								</h3>
								<input type="range" value="155" min="130" max="200"> <span
									class="slider-label-min">130 CM</span> <span
									class="slider-label-max">200 CM</span>
							</div>
							<div class="col-sm-2 hidden-xs"></div>
						</div>
						<div class="blank-20">&nbsp;</div>
						<div class="row">
							<div class="col-sm-2 hidden-xs"></div>
							<div class="col-sm-8 col-xs-12">
								<h3>
									What is Your Current Weight ?
									<p>(KG)</p>
								</h3>
								<input type="range" value="50" min="30" max="140"> <span
									class="slider-label-min">30 KG</span> <span
									class="slider-label-max">140 KG</span>
							</div>
							<div class="col-sm-2 hidden-xs"></div>
						</div>
					</section>
					<!-- end slider-->
					<div class="blank-20">&nbsp;</div>
					<!-- Question 01 -->
					<div class="col-sm-12 form-hquestions">
						<p>Have you ever had or been told to have or been treated for
							cancer, stroke, heart/coronary artery disease, kidney failure,
							diabetes, liver disorder/hepatitis, chronic lung disease, SLE,
							HIV/AIDS or any physical/mental impairment or any other serious
							illness not mentioned in this question?</p>

						<div class="col-sm-12 form-radio">
							<input type="radio" id="radio01" name="radio" /> <label
								for="radio01"><span class="check"></span>Yes</label>
							&nbsp;&nbsp; <input type="radio" id="radio02" name="radio" /> <label
								for="radio02"><span class="check"></span>No</label>
						</div>
					</div>
					<div class="blank-20">&nbsp;</div>
					<!-- Question 02 -->
					<div class="col-sm-12 form-hquestions">
						<p>Have you ever had an application, renewal or reinstatement
							of a Life Policy or Family Takaful contract, declined, postponed,
							rated or subject to special terms?</p>

						<div class="col-sm-12 form-radio">
							<input type="radio" id="radio01" name="radio" /> <label
								for="radio01"><span class="check"></span>Yes</label>
							&nbsp;&nbsp; <input type="radio" id="radio02" name="radio" /> <label
								for="radio02"><span class="check"></span>No</label>
						</div>
					</div>
					<div class="blank-20">&nbsp;</div>

					<!-- Question 03 -->
					<div class="col-sm-12 form-hquestions">
						<p>Do you intend to surrender or terminate any of your
							existing life insurance policies or Family Takaful contracts with
							this new application, even you may not receive any returns under
							these policies or contracts, and the returns may be lesser than
							the premiums or contributions paid?</p>

						<div class="col-sm-12 form-radio">
							<input type="radio" id="radio01" name="radio" /> <label
								for="radio01"><span class="check"></span>Yes</label>
							&nbsp;&nbsp; <input type="radio" id="radio02" name="radio" /> <label
								for="radio02"><span class="check"></span>No</label>
						</div>
					</div>

					<div class="blank-20">&nbsp;</div>
					<div class="col-sm-12 text-center form-buttons">
						<a href="master.html" class="btn btn-yellow">Back</a> <a
							href="term-insurance-page-3.html" onclick="getPremiumFreq1();"
							class="btn btn-yellow">Next</a>
					</div>

				</form>
			</div>
		</section>
	</div>

	<section class="form-footer">
		<div class="container-fluid">
			<div class="container">
				<div class="col-xs-12 col-md-6 footer-text">
					<p>&copy; 2016 Etiqa. All Rights Reserved.</p>
					<ul class="list-inline list-unstyled">
						<li><a href="#">Feedback</a></li>
						<li><a href="#">F.A.Q</a></li>
						<li><a href="#">Privacy Policy</a></li>
						<li><a href="#">Sitemap</a></li>
					</ul>
				</div>
				<div class="col-xs-12 col-md-6 social-icons">
					<ul
						class="social-icons icon-circle icon-zoom list-unstyled list-inline">
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<!-- /container -->


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!--    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>-->
	<script
		src="<c:url value="/resources/assets/js/jquery-2.2.4.min.js" />"></script>
	<script src="<c:url value="/resources/assets/js/bootstrap.min.js" />"></script>

	<!-- Retina support for images, just add @2x -->
	<script src="<c:url value="/resources/assets/js/retina.min.js" />"></script>
	<script src="<c:url value="/resources/assets/js/rangeslider.min.js" />"></script>
	<script type="text/javascript">
    
   
	function getPremiumFreq1(){
//var prefFreqCov=document.getElementById("prInCov").value;
//var durbene=document.getElementById("durofbe").value; 
//var gender=$('input[name=female]:checked').val();
//var isSmoker=$('input[name=radiosmoker]:checked').val();
		$.ajax({
		  type: "GET",
		  url: "http://localhost:8080/dsp/testurl",
		  //data: myusername,
		  
		  success: function(data){
			 $("#resultarea").text(data);
			 alert("Hello");   
		  },
            error: function (xhr, ajaxOptions, thrownError) {
      alert("error");    
                  
        }
		});
    }
    
    
    
        var valueBubble = '<output class="rangeslider__value-bubble" />';

        function updateValueBubble(pos, value, context) {
            pos = pos || context.position;
            value = value || context.value;
            var $valueBubble = $('.rangeslider__value-bubble', context.$range);
            var tempPosition = pos + context.grabPos;
            var position = (tempPosition <= context.handleDimension) ? context.handleDimension : (tempPosition >= context.maxHandlePos) ? context.maxHandlePos : tempPosition;

            if ($valueBubble.length) {
                $valueBubble[0].style.left = Math.ceil(position) + 'px';
                $valueBubble[0].innerHTML = value;
            }
        }

        $('input[type="range"]').rangeslider({
            polyfill: false,
            onInit: function () {
                this.$range.append($(valueBubble));
                updateValueBubble(null, null, this);
            },
            onSlide: function (pos, value) {
                updateValueBubble(pos, value, this);
            }
        });
    </script>

</body>

</html>