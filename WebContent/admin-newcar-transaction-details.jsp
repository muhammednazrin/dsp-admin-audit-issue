<%@ page import="java.util.List"%>
<%@ page import="java.io.*,java.util.*,java.sql.*,java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">

<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/assets/css/bootstrap.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/assets/css/menu.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/assets/css/css-loader.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/assets/css/toastr.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/assets/jAlert/jAlert.css">
<style>
</style>
</head>

<body>
	<div class="loader loader-default" data-text="Waiting for JPJ Reply"></div>
	<!--
===========================================================
BEGIN PAGE
===========================================================
-->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Report &amp; Analytics</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<c:if test="${not empty tranDetail}">
										<div class="col-sm-12">
											<div class="content-inner">
												<div class="row">
													<div class="col-sm-12">
														<!-- Begin  table -->
														<div class="content-inner">
															<div class="the-box full no-border">
																<div class="row">
																	<div class="col-xs-12 form-inline">
																		<div class="col-xs-6">
																			<div class="row">
																				<label> Client Details </label>
																			</div>
																		</div>
																		<div class="col-xs-6">
																			<div class="row">
																				<div class="pull-right"></div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="table-responsive">
																	<table
																		class="table table-striped table-warning table-hover"
																		id="showCustInfo">
																		<thead>
																			<tr>
																				<th>NRIC</th>
																				<th>Name</th>
																				<th>Email</th>
																				<th>Address</th>
																				<th>Postcode</th>
																				<th>State</th>
																				<th>Mobile Number</th>
																			</tr>
																		</thead>
																		<tbody>
																			<tr>
																				<td><c:out value="${customer.customerNricId}" /></td>
																				<td><c:out value="${customer.customerName}" /></td>
																				<td><c:out value="${customer.customerEmail}" /></td>
																				<td><c:out value="${customer.customerAddress1}" />,
																					<c:out value="${customer.customerAddress2}" />, <c:out
																						value="${customer.customerAddress3}" /></td>
																				<td><c:out value="${customer.customerPostcode}" /></td>
																				<td><c:out value="${customer.customerState}" />
																				</td>
																				<td><c:out value="${customer.customerMobileNo}" /></td>
																			</tr>
																		</tbody>
																	</table>
																</div>
																<c:if
																	test="${not empty tranDetail.policyNo && tranDetail.policy_status eq 'SUCCESS'}">
																	<div id="editCustInfo" style="display: block;">
																		<button class="btn btn-warning btn-sm">
																			Edit <i class="fa fa-edit" aria-hidden="true"></i>
																		</button>
																	</div>
																</c:if>
																<div id="submitbtn" style="display: none;">
																	<button type="submit"
																		class="btn btn-warning btn-sm text-right">
																		Save <i class="fa fa-edit" aria-hidden="true"></i>
																	</button>
																</div>
																<!-- /.table-responsive -->
																<!-- Start Customer Form-->
																<form:form modelAttribute="customer"
																	action="${pageContext.request.contextPath}/newcarCustomerUpdate"
																	id="updateCustomerForm" name="updateCustomerForm"
																	method="POST">
																	<!-- <input type="hidden" name="action" value="saveCustomerInfo" /> -->
																	<spring:bind path="customer.customerId">
																		<input type="hidden" name="${status.expression}"
																			id="${status.expression}" value="${status.value}" />
																	</spring:bind>
																	<input type="hidden" name="paymentTrxID"
																		value="<c:out value="${paymentTrxID}"/>" />
																	<input type="hidden" name="dspqqid"
																		value="<c:out value="${tranDetail.dspqqid}"/>" />
																	<input type="hidden" name="policyNo"
																		value="<c:out value="${tranDetail.policyNo}"/>" />
																	<div class="content-inner">
																		<div class="the-box">
																			<div class="col-sm-6">
																				<div class="form-horizontal">
																					<div class="form-group">
																						<label class="col-sm-3 control-label">NRIC/ID
																							No.</label>
																						<div class="col-sm-9">
																							<spring:bind path="customer.customerNricId">
																								<input type="text" placeholder=""
																									class="form-control"
																									name="${status.expression}"
																									id="${status.expression}"
																									value="${status.value}">
																							</spring:bind>
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="col-sm-3 control-label">Name</label>
																						<div class="col-sm-9">
																							<spring:bind path="customer.customerName">
																								<input type="text" placeholder=""
																									class="form-control"
																									name="${status.expression}"
																									id="${status.expression}"
																									value="${status.value}" >
																							</spring:bind>
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="col-sm-3 control-label">Email</label>
																						<div class="col-sm-9">
																							<spring:bind path="customer.customerEmail">
																								<input type="text" placeholder=""
																									class="form-control"
																									name="${status.expression}"
																									id="${status.expression}"
																									value="${status.value}"  />
																							</spring:bind>
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="col-sm-3 control-label">Address
																							1</label>
																						<div class="col-sm-9">
																							<spring:bind path="customer.customerAddress1">
																								<input type="text" placeholder=""
																									class="form-control"
																									name="${status.expression}"
																									id="${status.expression}"
																									value="${status.value}"  />
																							</spring:bind>
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="col-sm-3 control-label">Address
																							2</label>
																						<div class="col-sm-9">
																							<spring:bind path="customer.customerAddress2">
																								<input type="text" placeholder=""
																									class="form-control"
																									name="${status.expression}"
																									id="${status.expression}"
																									value="${status.value}"  />
																							</spring:bind>
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="col-sm-3 control-label">Address
																							3/City</label>
																						<div class="col-sm-9">
																							<spring:bind path="customer.customerAddress3">
																								<input type="text" placeholder=""
																									class="form-control"
																									name="${status.expression}"
																									id="${status.expression}"
																									value="${status.value}"  />
																							</spring:bind>
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="col-sm-3 control-label">Postcode</label>
																						<div class="col-sm-9">
																							<spring:bind path="customer.customerPostcode">
																								<input type="text" placeholder=""
																									class="form-control"
																									name="${status.expression}"
																									id="${status.expression}"
																									value="${status.value}"  />
																							</spring:bind>
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="col-sm-3 control-label">State</label>
																						<div class="col-sm-9">
																							<spring:bind path="customer.customerState">
																								<input type="text" placeholder=""
																									class="form-control"
																									name="${status.expression}"
																									id="${status.expression}"
																									value="${status.value}"  />
																							</spring:bind>
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="col-sm-3 control-label">Mobile
																							No</label>
																						<div class="col-sm-9">
																							<spring:bind path="customer.customerMobileNo">
																								<input type="text" placeholder=""
																									class="form-control"
																									name="${status.expression}"
																									id="${status.expression}"
																									value="${status.value}"  />
																							</spring:bind>
																						</div>
																					</div>
																					<div class="col-sm-12 text-right">
																						<input type="button" class="btn btn-default"
																							onclick="saveCustInfo()" value="Save" />
																						<!-- <button type="button" class="btn btn-default" onclick="saveCustInfo()">Save</button> -->
																						<button type="button" class="btn btn-default"
																							name="cancelCustInfo" id="cancelCustInfo">Cancel</button>
																					</div>
																				</div>
																			</div>
																
																<!-- END FORM -->
															</div>
															<!-- /.the-box -->
														</div>
														<!-- End warning color table -->
													</form:form>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-12">

														<form:form name="updateTxnStatusForm"
															action="${pageContext.request.contextPath}/newcarTxnStatusUpdate"
															id="updateTxnStatusForm" method="POST">

															<input type="hidden" name="currentStatus"
																value="<c:out value="${tranDetail.paymentStatus} "/>" />
															<input type="hidden" name="paymentTrxID"
																value="<c:out value="${paymentTrxID}"/>" />
															<input type="hidden" name="dspqqid"
																value="<c:out value="${tranDetail.dspqqid}"/>" />
															<div class="content-inner">
																<div class="the-box">
																	<div class="title">
																		<div class="sub">
																			<h4>Transaction Details</h4>
																		</div>
																	</div>
																	<div class="col-sm-9">
																		<div class="form-horizontal">
																			<div class="form-group">
																				<label class="col-sm-3 control-label">Transaction
																					Date</label>
																				<div class="col-sm-9">
																					<label class="control-label"> <c:out
																							value="${tranDetail.transactionDate}" />
																					</label>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-3 control-label">Policy/Certificate
																					No.</label>
																				<div class="col-sm-9">
																					<label class="control-label"> <c:out
																							value="${tranDetail.policyNo}" />
																					</label>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-3 control-label">Product</label>
																				<div class="col-sm-9">
																					<label class="control-label"> <c:out
																							value="${tranDetail.productType}" />
																					</label>
																				</div>
																			</div>
																			<c:set var="txnStatus"
																				value="${tranDetail.paymentStatus}" />
																			<div class="form-group">
																				<label class="col-sm-3 control-label">Status</label>
																				<div class="col-sm-9">
																					<select class="form-control editField"
																						disabled="disabled" id="editTxnStatus"
																						name="status">
																						<option value="S"
																							<c:if test="${txnStatus == 'S'}">selected</c:if>>Successful</option>
																							<option value="F" <c:if test="${txnStatus == 'F'}">selected</c:if>>Fail Payment </option>
																		            <option value="AP"<c:if test="${txnStatus == 'AP'}">selected</c:if>>Attempt Payment </option>
																		            <option value="O" <c:if test="${txnStatus == 'O'}">selected</c:if>>Incomplete </option>
																		            <option value="C" <c:if test="${txnStatus == 'C'}">selected</c:if>>Cancelled </option>
																		            <option value="R" <c:if test="${txnStatus == 'R'}">selected</c:if>>Rejection </option>
																		            </select>
	           											 </div>
	            									</div>
									            <div class="form-group">
									                <label class="col-sm-3 control-label">Commission</label>
									                <div class="col-sm-9">
									                    <label class="control-label"></label>
									                </div>
									            </div>
									            <div class="form-group">
									                <label class="col-sm-3 control-label">Discount</label>
									                <div class="col-sm-9">
									                    <label class="control-label"></label>
									                </div>
									            </div>
									            <div class="form-group">
									                <label class="col-sm-3 control-label">Incentive</label>
									                <div class="col-sm-9">
									                    <label class="control-label"></label>
									                </div>
									            </div>
									              <div class="form-group">
									                    <label class="col-sm-3 control-label">Premium/Contribution</label>
									                    <div class="col-sm-9">
									                        <label class="control-label"> RM
									                            <c:out value="${tranDetail.premiumAmount}" />
									                        </label>
									                    </div>
									                </div>
									                <div class="form-group">
									                    <label class="col-sm-3 control-label">Bank Fee</label>
									                    <div class="col-sm-9">
									                        <label class="control-label"></label>
									                    </div>
									                </div>
									                   <c:if test="${tranDetail.paymentChannel eq 'MPAY'}">
									                        <div class="form-group">
									                            <label class="col-sm-3 control-label">
									                                <c:out value="${tranDetail.paymentChannel}" /> Reference Number
									                            </label>
									                            <div class="col-sm-9">
									                                <label class="control-label">
									                                    <c:out value="${tranDetail.MPAY_RefNo}" />
									                                </label>
									                            </div>
									                        </div>
									                        <div class="form-group">
									                            <label class="col-sm-3 control-label">
									                                <c:out value="${tranDetail.paymentChannel}" /> Auth Code
									                            </label>
									                            <div class="col-sm-9">
									                                <label class="control-label">
									                                    <c:out value="${tranDetail.MPAY_AuthCode}" />
									                                </label>
									                            </div>
									                        </div>                      
									                    </c:if>
									                    <c:if test="${tranDetail.paymentChannel eq 'EBPG'}">
									                        <div class="form-group">
									                            <label class="col-sm-3 control-label">
									                                <c:out value="${tranDetail.paymentChannel}" /> Reference Number
									                            </label>
									                            <div class="col-sm-9">
									                                <label class="control-label">
									                                    <c:out value="${tranDetail.EBPG_RefNo}" />
									                                </label>
									                            </div>
									                        </div>
									                        <div class="form-group">
									                            <label class="col-sm-3 control-label">
									                                <c:out value="${tranDetail.paymentChannel}" /> Auth Code
									                            </label>
									                            <div class="col-sm-9">
									                                <label class="control-label">
									                                    <c:out value="${tranDetail.EBPG_AuthCode}" />
									                                </label>
									                            </div>
									                        </div>
									                    </c:if>
									                    <c:if test="${tranDetail.paymentChannel eq 'M2U'}">
									                        <div class="form-group">
									                            <label class="col-sm-3 control-label">
									                                <c:out value="${tranDetail.paymentChannel}" /> Reference Number
									                            </label>
									                            <div class="col-sm-9">
									                                <label class="control-label">
									                                    <c:out value="${tranDetail.M2U_RefNo}" />
									                                </label>
									                            </div>
									                        </div>
									                        <div class="form-group">
									                            <label class="col-sm-3 control-label">
									                                <c:out value="${tranDetail.paymentChannel}" /> Approval Code
									                            </label>
									                            <div class="col-sm-9">
									                                <label class="control-label">
									                                    <c:out value="${tranDetail.MPAY_AuthCode}" />
									                                </label>
									                            </div>
									                        </div>
									                    </c:if>
									                    <c:if test="${tranDetail.paymentChannel eq 'FPX'}">
									                        <div class="form-group">
									                            <label class="col-sm-3 control-label">
									                                <c:out value="${tranDetail.paymentChannel}" /> Reference Number
									                            </label>
									                            <div class="col-sm-9">
									                                <label class="control-label">
									                                    <c:out value="${tranDetail.FPX_RefNo}" />
									                                </label>
									                            </div>
									                        </div>
									                        <div class="form-group">
									                            <label class="col-sm-3 control-label">
									                                <c:out value="${tranDetail.paymentChannel}" /> Status
									                            </label>
									                            <div class="col-sm-9">
									                                <label class="control-label">
									                                    <c:out value="${tranDetail.FPX_AuthCode}" />
									                                </label>
									                            </div>
									                        </div>
						                    </c:if>
						                    <div class="form-group">
						                        <label class="col-sm-3 control-label">* Remarks</label>
						                        <div class="col-sm-9">
						                            <textarea class="editField" rows="4" cols="90" disabled="disabled" id="editRemark" name="remark"><c:out value="${tranDetail.auditRemarks}" /></textarea>
						                        </div>
						                    </div>
						                    <a class="btn btn-danger editBtn2" id="editTxnInfo">Edit</a>
						                    <input type="button" class="btn btn-default" name="saveTxnInfo" id="saveTxnInfo" onclick="updateTxnStatus()" value="Save">
						                    <button type="button" class="btn btn-default" id="cancelTxnInfo">Cancel</button>						                    						                      
							                    </div>
						                    </div>
					                    </div>
				                    </div>
	                    		</form:form>
                    		</div>
                    	</div>
                    	<div class="row">
		                    <!-- policy  certificate information -->
		                    <div class="col-sm-12">
		                        <!-- Begin  table -->
		                        <div class="content-inner">
		                            <div class="the-box full no-border">
		                                <div class="row">
		                                    <div class="col-xs-12 form-inline">
		                                        <div class="col-xs-6">
		                                            <div class="row">
		                                                <label>
		                                                    e-Policy/e-Certificate Information
		                                                </label>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
		                                <div class="table-responsive">
		                                    <table class="table table-striped table-warning table-hover">
		                                        <thead>
		                                            <tr>
		                                                <th style="width: 30px;">Note</th>
		                                                <th>Status</th>
		                                                <th>Period of Insurance/Cover</th>
		                                                <th>Plate No</th>                                              
		                                            </tr>
		                                        </thead>
		                                        <tbody>
		                                            <tr>
		                                                <td>
		                                                	<c:choose>
		                                                   		<c:when test="${not empty tranDetail.poiPolicyNo }">
		                                                    		<c:out value="${tranDetail.poiPolicyNo}" /> 
		                                                    	</c:when>
		                                                    	<c:otherwise>
		                                                    		<c:out value="${tranDetail.policyNo}" />
		                                                    	</c:otherwise>
		                                                    </c:choose>
		                                                </td>
		                                                <td>
		                                                    </a>
		                                                </td>
		                                                <td>
		                                                    <b>Date : </b>
		                                                    <c:out value="${tranDetail.coverageStartDate}" />-
		                                                    <c:out value="${tranDetail.coverageEndDate}" />
		                                                </td>
		                                                <td>
		                                                   <c:if test="${not empty tranDetail.vehicleNumber}" >
		                                                    <c:out value="${tranDetail.vehicleNumber}" />
		                                                   </c:if>
		                                                    <c:if test="${empty tranDetail.vehicleNumber}" >
		                                                    NA
		                                                   </c:if> 
		                                                </td>
		                                                <td>
		                                                 </td>
		                                            </tr>
		                                        </tbody>
		                                    </table>
		                                </div>
		                                 <c:if test="${not empty tranDetail.policyNo && tranDetail.policy_status eq 'SUCCESS'}">
		                                <div id="editPolicyInfo" style="display:block;">
		                                    <button class="btn btn-warning btn-sm">Edit <i class="fa fa-edit" aria-hidden="true"></i>
		                                    </button>
		                                </div>
		                                </c:if>
		                                <!-- Start- Policy Form  ------------------------------------------------------->
		                                <form:form action="${pageContext.request.contextPath}/updatePOI"  id="policyInfoForm" method="POST">
		                                     <input type="hidden" name="paymentTrxID" value="<c:out value="${paymentTrxID}"/>" />
		                                     <input type="hidden" name="qqid" value="<c:out value="${tranDetail.qqid}"/>" />
		                                    <div class="content-inner">
		                                        <div class="the-box">
		                                            <div class="col-sm-6">
		                                                <div class="form-horizontal">

		                                                    <div class="form-group">
		                                                        <label class="col-sm-3 control-label">Note:</label>
		                                                        <div class="col-sm-9">
		                                                            <label class="control-label">
		                                                                <c:out value="${tranDetail.policyNo}" />
		                                                            </label>
		                                                        </div>
		                                                    </div>
		                                                    <div class="form-group">
		                                                        <label class="col-sm-3 control-label">Status:</label>

		                                                        <div class="col-sm-9">
		                                                            <label class="control-label">
		                                                                <c:out value="" />
		                                                            </label>
		                                                        </div>
		                                                    </div>
		                                                    <div class="form-group">
		                                                        <label class="col-sm-3 control-label">Period of Insured(Start Date):</label>
		                                                        <div class="col-sm-9">
		                                                            <input type="text" placeholder="" id="datepicker1" class="form-control" name="coverageStartDate" value="${tranDetail.coverageStartDate}">
		                                                        </div>
		                                                    </div>
		                                                    <div class="form-group">
		                                                        <label class="col-sm-3 control-label">Vechile Reg.No:</label>
		                                                        <div class="col-sm-9">
		                                                            <input type="text" placeholder="" class="form-control" id="vehicleNumber" name="vehicleNumber" value="${tranDetail.vehicleNumber}">
		                                                        </div>
		                                                    </div>
		                                                    <div class="col-sm-12 text-right">
		                                                        <input type="button" class="btn btn-default" id="updatePOI" name="" value="Update" />
		                                                        <button type="button" class="btn btn-default" name="" id="cancelPolicyInfo">Cancel</button>
		                                                    </div>

		                                                </div>
		                                            </div>
		                                </form:form>
		                                 <!-- End- Policy Form  ------------------------------------------------------->
		                                </div>
		                                <!-- /.the-box -->
		                        </div>
		                        <!-- End warning color table -->
		                    </div>
		                </div>
                            <!-- policy  certificate information -->
                        <div class="row">
                            <!-- Vechile Details -->
                            <div class="col-sm-12">
                                <!-- Begin  table -->
                                <div class="content-inner">
                                    <div class="the-box full no-border">
                                        <div class="row">
                                            <div class="col-xs-12 form-inline">
                                                <div class="col-xs-6">
                                                    <div class="row">
                                                        <label>
                                                            Vehicle Details
                                                        </label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-warning table-hover" id="showVechileInfo">
                                                <thead>
                                                    <tr>
                                                        <th>Make & Model</th>
                                                        <th>Engine No</th>
                                                        <th>Vehicle Year</th>                                                                                                              
                                                        <th>NVIC</th>
                                                        <th>Chassis No</th>
                                                        <th>Plate No</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <c:out value="${tranDetail.vehicleMake}" />
                                                        </td>
                                                        <td>
                                                            <c:out value="${tranDetail.engineNo}" /> </td>
                                                        <td>
                                                            <c:out value="${tranDetail.yearMake}" /> </td>
                                                        <td>
                                                            <c:out value="${tranDetail.nvic}" /> </td>
                                                        <td>
                                                            <c:out value="${tranDetail.chassisNo}" />
                                                        </td>
                                                        <td>
                                                             <c:if test="${not empty tranDetail.vehicleNumber}" >
				                                                    <c:out value="${tranDetail.vehicleNumber}" />
				                                                   </c:if>
				                                                    <c:if test="${empty tranDetail.vehicleNumber}" >
				                                                    NA
				                                                   </c:if> </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                         <c:if test="${not empty tranDetail.policyNo}">
                                         	<c:if test="${tranDetail.policyNo eq maxPcyNo}">
		                                        <div id="editVechileInfo" style="display:block;">
		                                            <button class="btn btn-warning btn-sm">Edit <i class="fa fa-edit" aria-hidden="true"></i>
		                                            </button>
		                                        </div>
	                                        </c:if>
                                        </c:if>
                                        <!-- /.table-responsive -->
                                        <!-- Start vechile info Form -->
 										<!-- Start- vechile Form  ------------------------------------------------------->
                                        <form:form  id="updateChassisEngineNo" method="post">
                                            <input type="hidden" name="paymentTrxID" value="<c:out value="${paymentTrxID}"/>" />
                                            <input type="hidden" name="dspqqid" value="<c:out value="${tranDetail.dspqqid}"/>" />
                                            <input type="hidden" name="policyNo" value="<c:out value="${tranDetail.policyNo}"/>" />
                                            
                                            <div class="content-inner">
                                                <div class="the-box">
                                                    <div class="col-sm-6">
                                                        <div class="form-horizontal">

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">Issue Date:</label>
                                                                <div class="col-sm-9">
                                                                    <label class="control-label">
                                                                        <c:out value="${tranDetail.transactionDate}" />
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">CoverNote No:</label>

                                                                <div class="col-sm-9">
                                                                    <label class="control-label">
                                                                        <c:out value="${tranDetail.policyNo}" />
                                                                    </label>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">Name:</label>


                                                                <div class="col-sm-9">
                                                                    <label class="control-label">
                                                                        <c:out value="${tranDetail.customerName}" />
                                                                    </label>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">Chassis No:</label>

                                                                <div class="col-sm-9">
                                                                    <input type="text" placeholder="" class="form-control" name="chassisNo" value="${tranDetail.chassisNo}">
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">Engine No:</label>

                                                                <div class="col-sm-9">
                                                                    <input type="text" placeholder="" class="form-control" name="engineNo" value="${tranDetail.engineNo}">
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">JPJ Status:</label>

                                                                <div class="col-sm-9">
                                                                    <label class="control-label">
                                                                        <c:out value="" />
                                                                    </label>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">Response Code:</label>

                                                                <div class="col-sm-9">
                                                                    <label class="control-label">
                                                                        <c:out value="" />
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 text-right">
                                                                <input type="button" id="updateChassisEngine" class="btn btn-default" value="Update" />
                                                                <button type="button" class="btn btn-default" name="" id="cancelVechileInfo">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                        </form:form>
									  <!-- End- vechile Form  ------------------------------------------------------->
                                        <!-- End vechile  information form -->

                                        </div>
                                </div>
                            </div>
                        </div>
                        <c:if test="${not empty tranDetail.policyNo && tranDetail.policy_status eq 'SUCCESS'}"> 
                        <div class="row">
                            <!--row -->
                            <div class="col-sm-12">
                                <!-- Begin  table -->
                                <div class="content-inner">
                                    <div class="the-box full no-border">
                                        <div class="row">
                                            <div class="col-xs-12 form-inline">
                                                <div class="col-xs-6">
                                                    <div class="row">
                                                        <label>
                                                            Task
                                                        </label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-warning table-hover" id="showVechileInfo">
                                                <thead>
                                                    <tr>
                                                        <th>JSP Status</th>
                                                        <th></th>
                                                       

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                        <form:form  id="resendJpjForm" method="post">
                                            			<input type="hidden" name="paymentTrxID" value="<c:out value="${paymentTrxID}"/>" />
                                            			<input type="hidden" name="dspqqid" value="<c:out value="${tranDetail.dspqqid}"/>" />
                                           				<input type="hidden" name="policyNo" value="<c:out value="${tranDetail.policyNo}"/>" />
                                           				</form:form>
                                           			     <input type="button" id="resendJPJ" value="Resend JPJ Status" class="btn btn-link"  />	 
                                           			    <!--  <button class="btn btn-link" role="link" type="submit" name="op" value="Link 1">Resend JPJ Status</button>
                                                        <a href="#">Resend JPJ Status</a>  -->
                                                        <!-- <br>
                                                        	<body link="blue">
                                                        		<p><a href="#" class="btn btn-link sendEmail">Resend Email</a></p>
                                                        	</body>  -->
                                                        </td>
                                                        <td>
                                                        <c:if test="${not empty tranDetail.policyNo && tranDetail.policy_status eq 'SUCCESS'}">                                                        
	                                                        <input type="button" id="cancelPolicy" value="Cancel Policy" class="btn btn-danger" />	                                                        
                                                        </c:if>
                                                        </td>                                                       
                                                    </tr>
                                                   
                                                </tbody>
                                            </table>
                                        </div>
                                        
                                          <!--StartCancel Policy Form -->
                                            <form:form  id="cancelPolicyForm" method="post">
                                            <input type="hidden" name="paymentTrxID" value="<c:out value="${paymentTrxID}"/>" />
                                            <input type="hidden" name="dspqqid" value="<c:out value="${tranDetail.dspqqid}"/>" />
                                            <input type="hidden" name="policyNo" value="<c:out value="${tranDetail.policyNo}"/>" />
                                            <div class="content-inner">
                                                <div class="the-box">
                                                    <div class="col-sm-6">
                                                        <div class="form-horizontal">

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">Issue Date:</label>
                                                                <div class="col-sm-9">
                                                                    <label class="control-label">
                                                                        <c:out value="${tranDetail.transactionDate}" />
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">CoverNote No:</label>

                                                                <div class="col-sm-9">
                                                                    <label class="control-label">
                                                                        <c:out value="${tranDetail.policyNo}" />
                                                                    </label>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">Name:</label>


                                                                <div class="col-sm-9">
                                                                    <label class="control-label">
                                                                        <c:out value="${tranDetail.customerName}" />
                                                                    </label>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">Chassis No:</label>

                                                                <div class="col-sm-9">
                                                                  <label class="control-label">
                                                                <c:out value="${tranDetail.chassisNo}" />  
                                                                </label>                                                                
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">Engine No:</label>

                                                                <div class="col-sm-9">
                                                                 <label class="control-label">
                                                                   <c:out value="${tranDetail.engineNo}" />
                                                                      </label> 
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">JPJ Status:</label>

                                                                <div class="col-sm-9">
                                                                    <label class="control-label">
                                                                        <c:out value="" />
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">Response Code:</label>

                                                                <div class="col-sm-9">
                                                                    <label class="control-label">
                                                                        <c:out value="" />
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">Reason to Cancel:</label>
                                                                <div class="col-sm-9">
                                                                    <label class="control-label">
                                                                        <textarea class="editField" rows="4" cols="90"  id="cancelRemark" name="cancelRemark">
											                                <c:out value="${tranDetail.auditRemarks}" />
											                            </textarea>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 text-right">
                                                                <input type="button" id="cancelPolicyFormSubmit" class="btn btn-default" value="Cancel Policy" />
                                                                <button type="button" class="btn btn-default" name="" id="cancelPolicyFormCancel">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                        </form:form>
                                        <!--End Cancel Policy Form -->
                                    </div>
                                </div>
                            </div>
                            </div>
                            </c:if>                  
                            
                            </c:if>                          
                            
                            <!-- New car E-mail Start -->
                                        <c:if test="${not empty tranDetail.policyNo && tranDetail.policy_status eq 'SUCCESS'}"> 
                                        <div class="col-sm-12">
                                          <!-- Begin  table -->
                                          <div class="content-inner">
                                             <div class="the-box full no-border">
                                              <div class="title">
													 <div class="sub">
														<h4>Task</h4>
													 </div>
												  </div>
                                                <!-- /.table-responsive -->
                                                <div class="table-responsive">
                                                   <table class="table table-striped table-warning table-hover">
                                                      <thead>
                                                         <tr>
                                                            <th>Email</th>
                                                            <th>Document Generation</th>
                                                            <th>Epolicy</th>
                                                         </tr>
                                                      </thead>
                                                      <tbody>
                                                         <tr>
                                                        <!--  <td> <a href="#" class="sendEmail"> Resend Email </a> </td> -->
														<td> <a href="${pageContext.request.contextPath}/NewCarReportController?action=sendEmail&docId=<c:out value="${tranDetail.qqid}"/>&policyNo=<c:out value="${tranDetail.policyNo}"/>&id=<c:out value="${paymentTrxID}"/>" > Resend Email </a> </td>
                                                        <td><a href="${pageContext.request.contextPath}/NewCarReportController?action=regenDoc&docId=<c:out value="${tranDetail.qqid}"/>&policyNo=<c:out value="${tranDetail.policyNo}"/>&id=<c:out value="${paymentTrxID}"/>" >Regenerate Document without Email</a></td>
                                                         <td>  
														 <a href="${pageContext.request.contextPath}/downloadNewCar?policyNo=<c:out value="${tranDetail.policyNo}&term=AutoAssistForm" />" target="_blank" >Download AutoAssistForm</a><br>
														 
														 <c:choose>
														 	<c:when test="${tranDetail.productType eq 'New Car Insurance'}">
														 		<a href="${pageContext.request.contextPath}/downloadNewCar?policyNo=<c:out value="${tranDetail.policyNo}&term=EPolicyForm" />" target="_blank" >Download EPolicyForm</a><br>
														 	</c:when>														 	
														 	<c:otherwise>
														 		<a href="${pageContext.request.contextPath}/downloadNewCar?policyNo=<c:out value="${tranDetail.policyNo}&term=ECertificateForm" />" target="_blank" >Download ECertificateForm</a><br>
														 	</c:otherwise>														 	 
														 </c:choose>
														 
														 <a href="${pageContext.request.contextPath}/downloadNewCar?policyNo=<c:out value="${tranDetail.policyNo}&term=PDPAForm" />" target="_blank" >Download PDPAForm</a><br>
														 <a href="${pageContext.request.contextPath}/downloadNewCar?policyNo=<c:out value="${tranDetail.policyNo}&term=PDSMotorForm" />" target="_blank" >Download PDSMotorForm</a><br>
														 <a href="${pageContext.request.contextPath}/downloadNewCar?policyNo=<c:out value="${tranDetail.policyNo}&term=TaxInvoiceForm" />" target="_blank" >Download TaxInvoiceForm</a>
														</td>                                     					
													</td>		
                                                         </tr>
                                                      
                                                         <% if(request.getAttribute("statusEm")!= null){
                                                        	  
                                                        	  if(request.getAttribute("statusEm").equals("Fail")){                                                        		  
                                                        		 %> <br>                                                         		 
                                                        		 <div class="col-sm-9">
                                                        		 	<script src="${pageContext.request.contextPath}/assets/js/jquery.min.bk.js" type="text/javascript"></script>      
																    <script src="${pageContext.request.contextPath}/assets/jAlert/jAlert.min.js"></script>
																	<script src="${pageContext.request.contextPath}/assets/jAlert/jAlert-functions.min.js"></script> 																                                                        		 
                                                                  <script>                                                  
                                                                  		var pcyNo = '<c:out value="${tranDetail.policyNo}" />';
                                                                  		errorAlert("Email Sending Failed "+ pcyNo ); 
                                                                  	</script>
                                                                </div>
                                                        	 <% }
                                                        	  else{                                                        		  
                                                        		  %>                                                        		  
                                                        		  <div class="col-sm-9">
                                                        		 	<script src="${pageContext.request.contextPath}/assets/js/jquery.min.bk.js" type="text/javascript"></script>      
																    <script src="${pageContext.request.contextPath}/assets/jAlert/jAlert.min.js"></script>
																	<script src="${pageContext.request.contextPath}/assets/jAlert/jAlert-functions.min.js"></script> 																                                                        		 
                                                                  <script> 
                                                                   		var pcyNo = '<c:out value="${tranDetail.policyNo}" />';
                                                                  		successAlert('Success!', 'The email is resent successfully for policy number '+pcyNo);                                                                  			
                                                                  </script>
                                                                </div>                                                    		   
                                                        	  <% }
                                                         	  }
                                                            %>                                                         
                                                      </tbody>
                                                   </table>
                                                </div>
                                                <!-- /.table-responsive -->
                                             </div>
                                             <!-- /.the-box -->
                                          </div>
                                          <!-- End warning color table -->
                                       </div>
                                       
                                        </c:if>
                                        <!--  New Car E-mail End -->                                   
                        </div>
                    </div>
                        </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    <!-- /.container -->
                    </div>
                    <!-- /.page-content -->
                    <!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>

                   <%--  <jsp:include page="pageFooter.jsp" /> --%>

                    <!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
        </div>
      <!--   <div class="loader style-2"><div class="loading-wheel"></div></div>  -->     
        <!-- /.wrapper -->
        <!-- END PAGE CONTENT -->
        <!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
        <%-- <jsp:include page="siteFooter.jsp" /> --%>
        <!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
        <!-- JAVA Script Goes Here -->
    <script src="${pageContext.request.contextPath}/assets/js/jquery.min.bk.js" type="text/javascript"></script>     
    <script src="${pageContext.request.contextPath}/assets/jAlert/jAlert.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/jAlert/jAlert-functions.min.js"></script> 
    <script src="${pageContext.request.contextPath}/assets/js/bootstrap.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/assets/plugins/retina/retina.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/backstretch/jquery.backstretch.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/apps.js"></script>  
	<!-- PLUGINS -->
	<script src="${pageContext.request.contextPath}/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/prettify/prettify.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
	<!-- <script src="assets/plugins/chosen/chosen.jquery.min.js"></script> -->
	 <script src="${pageContext.request.contextPath}/assets/plugins/icheck/icheck.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/summernote/summernote.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/markdown/markdown.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/markdown/to-markdown.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/toastr/toastr.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/jszip.min.js"></script>  
	<script src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/knob.js"></script>
	<script src="${pageContext.request.contextPath}/assets/js/toastr.min.js"></script>  
     
        <!-- JAVA Script Goes Here -->
     <script type="text/javascript">   
     
       	$('input#vehicleNumber').keypress(function( e ) {
       if(e.which === 32) 
         return false;
    });

       
     $('input#vehicleNumber').keyup(function() {
                if (this.value.match(/[^a-zA-Z0-9 ]/g)) {
                    this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');
                }
            });
         $('input#vehicleNumber').keyup(function(){
                 this.value = this.value.toUpperCase();
               });
        
      $(document).ready(function() {
          $('#updateCustomerForm').hide();
          $('#cancelTxnInfo').hide();
          $('#saveTxnInfo').hide();
          $('#updateChassisEngineNo').hide();
          $('#policyInfoForm').hide();
          $('#cancelPolicyForm').hide();
          
          $('#editCustInfo').click(function() {
              $('#showCustInfo').hide();
              $('#editCustInfo').hide();
              $('#updateCustomerForm').show();
             // errorAlert("Already Registered, Please Choose Another Email!") ;
          });
          $('#cancelCustInfo').click(function() {
              $('#showCustInfo').show();
              $('#editCustInfo').show();
              $('#updateCustomerForm').hide();
              alert("TEST");
          });
          $('#editTxnInfo').click(function() {
              $('#editTxnStatus').removeAttr('disabled');
              $('#editRemark').removeAttr('disabled');
              $('#cancelTxnInfo').show();
              $('#saveTxnInfo').show();
              $('#editTxnInfo').hide();
          });

          $('#cancelTxnInfo').click(function() {

              $('#editTxnStatus').attr('disabled', 'disabled');
              $('#editRemark').attr('disabled', 'disabled');
              $('#cancelTxnInfo').hide();
              $('#saveTxnInfo').hide();
              $('#editTxnInfo').show();
          });
          $('#editVechileInfo').click(function() {
              $('#editVechileInfo').hide();
              $('#updateChassisEngineNo').show();
          });
      });
      $('#cancelVechileInfo').click(function() {
          $('#editVechileInfo').show();
          $('#updateChassisEngineNo').hide();
      });    
      $('#cancelPolicyFormCancel').click(function() {
    	  $('#cancelPolicyForm').hide();
    	  $('#cancelPolicy').show();
      });
      $('#cancelPolicyInfo').click(function() {
          $('#editPolicyInfo').show();
          $('#policyInfoForm').hide();
      });       
      $('#cancelPolicy').click(function() {
    	  $('#cancelPolicyForm').show();
    	  $('#cancelPolicy').hide();
    	  
      });
  
      $('#editPolicyInfo').click(function() {
          $('#editPolicyInfo').hide();
          $('#policyInfoForm').show();
      });      
      
/* ------------------------------------------------------------------ */
/* Start- Update Customer Info  */
/*-------------------------------------------------------------------  */ 
function saveCustInfo() {
	var str = $("#updateCustomerForm").serialize();
   /*  var customerNricIdFrom= $('#customerNricId').val();
    var customerNricId="<c:out value="${customer.customerNricId}" />";
	if(customerNricIdFrom == customerNricId) { // zero-length string AFTER a trim
		warningAlert('NRIC is not modified!'); 
    } else { */
		$.ajax({
				type: "post",
				beforeSend: function(){
				 $('.loader').addClass("is-active");
				},
				url: '${pageContext.request.contextPath}/newcarCustomerUpdate',
				data: str,
				success: function(data) {					
					 obj = JSON.parse(data);					 
					 if(obj.jpjResponse == '01') {
						 console.log("Hello");
						 alert("Customer Info Updated Successfully");
						 window.location.href  = '${pageContext.request.contextPath}/newCarDetails/'+obj.paymentTrxID+'/'+obj.policyNo;
						// window.location('${pageContext.request.contextPath}/newCarDetails/<c:out value="${tranDetail.policyNo}" />');
					 }					 	
				},
				 complete: function(){
					 $('.loader').removeClass("is-active");
				},
				error: function(request, status, error) {
					alert(request.responseText);
				}
			});	
	//} // End-IF			
}

/* ------------------------------------------------------------------ */
/* End- Update Customer Info  */
/*-------------------------------------------------------------------  */

/* ------------------------------------------------------------------ */
/* Start- Cancel Policy  */
/*-------------------------------------------------------------------  */ 
$("#cancelPolicyFormSubmit").click(function() {   
	var str = $("#cancelPolicyForm").serialize();
		$.ajax({
				type: "post",
				beforeSend: function(){
				 $('.loader').addClass("is-active");
				},
				url: '${pageContext.request.contextPath}/newcarCancelPolicy',
				data: str,
				success: function(data) {					
					 obj = JSON.parse(data);					 
					 if(obj.jpjResponse == '01') {
						 alert("Policy Canceled and JPJ Updated Successfully");
						 window.location.href  = '${pageContext.request.contextPath}/newCarDetails/'+obj.paymentTrxID+'/'+obj.policyNo;
					 } else {
						 errorAlert("Fail to Cancel the Policy"); 
					 }					 	
				},
				 complete: function(){
					 $('.loader').removeClass("is-active");
				},
				error: function(request, status, error) {
					alert(request.responseText);
				}
			});	
		
});
/* ------------------------------------------------------------------ */
/* End- Cancel Policy  */
/*-------------------------------------------------------------------  */
 
/* ------------------------------------------------------------------ */
/* Start- Update Chassis Engine No  */
/*-------------------------------------------------------------------  */ 
$("#updateChassisEngine").click(function() {   
	var str = $("#updateChassisEngineNo").serialize();
		$.ajax({
				type: "post",
				beforeSend: function(){
				 $('.loader').addClass("is-active");
				},
				url: '${pageContext.request.contextPath}/newcarChassisEngineUpdate',
				data: str,
				success: function(data) {					
					 obj = JSON.parse(data);					 
					 if(obj.jpjResponse == '01') {
						 //alert("Chassis/Engine Numbers Updated Successfully");						 
						 successAlert('Success!', 'Chassis/Engine Numbers Updated Successfully');
						 window.location.href  = '${pageContext.request.contextPath}/newCarDetails/'+obj.paymentTrxID+'/'+obj.policyNo;
					 } else {
						 errorAlert("You Must Cancel the Policy First"); 
					 }					 	
				},
				 complete: function(){
					 $('.loader').removeClass("is-active");
				},
				error: function(request, status, error) {
					alert(request.responseText);
				}
			});			
});
/* ------------------------------------------------------------------ */
/* End- Update Chassis Engine No  */
/*-------------------------------------------------------------------  */
$("#updatePOI").click(function() {
   $('#policyInfoForm').submit();

 });


$("#resendJPJ").click(function() {   
	var str = $("#resendJpjForm").serialize();
		$.ajax({
				type: "post",
				beforeSend: function(){
				 $('.loader').addClass("is-active");
				},
				url: '${pageContext.request.contextPath}/newcarResendJPJ',
				data: str,
				success: function(data) {					
					 obj = JSON.parse(data);					 
					 if(obj.jpjResponse == '01') {
						 alert("JPJ Updated Successfully");
						 window.location.href  = '${pageContext.request.contextPath}/newCarDetails/'+obj.paymentTrxID+'/'+obj.policyNo;
					 } else {
						 errorAlert("Could not Get Successful JPJ Status"); 
					 }					 	
				},
				 complete: function(){
					 $('.loader').removeClass("is-active");
				},
				error: function(request, status, error) {
					alert(request.responseText);
				}
			});			
});
/* ------------------------------------------------------------------ */
/* Start- Update Transaction Status  */
/*-------------------------------------------------------------------  */ 
function updateTxnStatus() {
	var str = $("#updateTxnStatusForm").serialize();//newcarCustomerUpdate newcarTxnStatusUpdate 
	console.log("updateTxnStatusForm");
		$.ajax({
				type: "post",
				beforeSend: function(){
				/*$('.loader').addClass("is-active");*/
				},
				url: '${pageContext.request.contextPath}/newcarTranUpdate',
				data: str,                               
				success: function(data) {		
					console.log(data+"updateTxnStatusForm data");
					 obj = JSON.parse(data);
					 console.log(obj.resDesc);
					
					 
					 if(obj.resCode == '00'){
						 successAlert(obj.resDesc);
						 if(obj.policyNo ==null){
						 window.location.href  = '${pageContext.request.contextPath}/newCarDetails/${paymentTrxID}'; 
						 }
						 else{
							 
							 window.location.href  = '${pageContext.request.contextPath}/newCarDetails/${paymentTrxID}/'+obj.policyNo; 	 
						 }
					 }
					 
					 else{
						 
						 warningAlert(obj.resDesc);
						 
						 
					 }
					 
					 //if(obj.statusResponse == '01') {
						// console.log("Update txn status");
						 //successAlert("Payment status updated");
						 //window.location.href  = '${pageContext.request.contextPath}/newCarDetails/${paymentTrxID}';
						// window.location('${pageContext.request.contextPath}/newCarDetails/<c:out value="${tranDetail.policyNo}" />');
					// }	
					// else if(obj.statusResponse == '02') {
						 //console.log("Error update payment status");
						 //errorAlert("Error occured during updating payment status");
						// window.location.href  = '${pageContext.request.contextPath}/newCarDetails/${paymentTrxID}';
						
						 
					// }
					// else if(obj.statusResponse == '00') {
						// console.log("Same payment status");
						 //warningAlert("No payment status changes");
						 //window.location.href  = '${pageContext.request.contextPath}/newCarDetails/${paymentTrxID}';
						// 
						 
					 //}
				},
				 complete: function(){
					 /*$('.loader').removeClass("is-active");*/
				},
				error: function(request, status, error) {
					alert(request.responseText);
				}
			});	
	//} // End-IF			
}
/* ------------------------------------------------------------------ */
/* End- Update Transaction Status  */
/*-------------------------------------------------------------------  */
  </script>
</body>

</html>