<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="com.etiqa.model.report.TransactionalReport"%>
<%@ page import="com.spring.admin.DashBoardController"%>
<%@ page import="java.io.*,java.util.*"%>
<%@ page import="javax.servlet.*,java.text.*"%>
<%--  <%if (session.getAttribute("user") == null) {  response.sendRedirect(request.getContextPath() + "/admin-login.jsp"); }%> --%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<link
	href="${pageContext.request.contextPath}/plugins/c3-chart/c3.min.css"
	rel="stylesheet">

<!-- Oriq CSS -->
<link rel="stylesheet" href="assets/css/oiCSS.css" />

<!-- <style>
#business_summary, #business_summary {
	display: none;
}

 .dataTable > thead > tr > th[class*="sort"]::after{display: none}
</style> -->

<SCRIPT type="text/javascript">
	window.history.forward();
	function noBack() {
		window.history.forward();
	}
</SCRIPT>


</head>
<body onload="noBack();" onpageshow="if (event.persisted) noBack();"
	onunload="">

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">

		<jsp:include page="pageHeader.jsp" />
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<jsp:include page="menu1.jsp" />

					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Dashboard</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<table width="100%">
														<tr>
															<td>
																<form
																	action="${pageContext.request.contextPath}/reload_dashboard"
																	name="rptForm" id="rptForm" method="post"
																	onsubmit="return validateForm()">
																	<div class="col-sm-12">
																		<table class="title">
																			<tr>
																				<td><h4>Date:&nbsp;</h4></td>
																				<td><input type="text" readonly id="fromdate"
																					required value="${fromdate}" required
																					name="fromdate" class="form-control"></td>
																			</tr>
																			<tr>
																				<td>
																					<h4>Report &nbsp;</h4>
																				</td>
																				<td align="left"><select id="selectchart"
																					name="selectchart" class="form-control">
																						<option value="business_summary"
																							<c:if test="${selectvalue eq 'business_summary'}"> selected </c:if>>Daily
																							New Business Summary</option>
																						<option value="renewal_retension"
																							<c:if test="${selectvalue eq 'renewal_retension'}"> selected </c:if>>Renewal
																							Retention(MTD)</option>
																						<option value="entity"
																							<c:if test="${selectvalue eq 'entity'}"> selected </c:if>>Performance
																							by Entity</option>
																						<option value="cmpt"
																							<c:if test="${selectvalue eq'cmpt'}"> selected </c:if>>Current
																							Month Performance Trend</option>
																						<option value="actual_budget"
																							<c:if test="${selectvalue eq 'actual_budget'}"> selected </c:if>>Actual
																							vs Budget</option>
																						<option value="agent_performance"
																							<c:if test="${selectvalue eq 'agent_performance'}"> selected </c:if>>Cyber
																							Agent Performance</option>
																						<option value="pcsr"
																							<c:if test="${selectvalue eq 'pcsr'}"> selected </c:if>>Product
																							Cross-Sell Ratio</option>
																						<option value="motoraratio"
																							<c:if test="${selectvalue eq 'motoraratio'}"> selected </c:if>>MOTOR
																							: Attachment Ratio</option>
																						<option value="sourcelead"
																							<c:if test="${selectvalue eq 'sourcelead'}"> selected </c:if>>Source
																							of Leads</option>
																						<option value="abandonment"
																							<c:if test="${selectvalue eq 'abandonment'}"> selected </c:if>>Abandonment
																							vs Follow up Rate</option>
																				</select></td>
																				<td>&nbsp;<input value="Search"
																					class="btn btn-default" type="submit" /></td>
																			</tr>
																		</table>
																	</div>
																</form>
															</td>
														</tr>
													</table>
													<c:choose>
														<c:when
															test="${dashBoard.selectvalue eq'business_summary'}">
															<div class="col-sm-12">
																<div class="title">
																	<div class="sub">
																		<h4>Daily New Business Summary
																			(${dashBoard.fromdate})</h4>
																	</div>
																	<div class="content-inner">
																		<div class="row" id="business_summary">
																			<!-- row -->
																			<div class="col-sm-12">
																				<!-- group 1 -->
																				<div class="row">
																					<%-- <div class="col-sm-3">
																						<div class="row">
																							<div class="col-sm-12">
																								<!-- BEGIN TODAY VISITOR TILES -->
																								<div
																									class="panel panel-primary panel-square panel-no-border">
																									<div class="panel-body">
																										<p class="cat-head text-center">WTC</p>
																										<h1 class="bolded tiles-number text-center">
																											<c:choose>
																												<c:when
																													test="${dashBoard.f_WTC_TotalPoliciesSold>'0'}">
																													<c:out
																														value="${dashBoard.f_WTC_TotalPoliciesSold}" />
																												</c:when>
																												<c:otherwise>
																													<c:out value="0" />
																												</c:otherwise>
																											</c:choose>
																										</h1>
																										<p class="text-center text20">
																											<strong>RM <c:choose>
																													<c:when
																														test="${dashBoard.f_WTC_Sold_Amount>'0'}">
																														<c:out
																															value="${dashBoard.f_WTC_Sold_Amount}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</strong>
																										</p>
																										<div class="gap gap-mini"></div>
																										<p>
																											Total Policy(MTD) <span class="pull-right">
																												<c:choose>
																													<c:when
																														test="${dashBoard.f_WTC_TotalPoliciesSold_PM>'0'}">
																														<c:out
																															value="${dashBoard.f_WTC_TotalPoliciesSold_PM}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</span>
																										</p>
																										<p>
																											Total Policy(YTD) <span class="pull-right">

																												<c:choose>
																													<c:when
																														test="${dashBoard.f_WTC_TotalPoliciesSold_PY>'0'}">
																														<c:out
																															value="${dashBoard.f_WTC_TotalPoliciesSold_PY}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</span>
																										</p>
																										<div class="gap gap-mini"></div>
																										<p class="small">
																											Prem/Cont. (M) <span class="pull-right">RM

																												<c:choose>
																													<c:when
																														test="${dashBoard.f_WTC_Sold_Amount_M>'0'}">
																														<c:out
																															value="${dashBoard.f_WTC_Sold_Amount_M}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</span>
																										</p>
																										<p class="small">
																											Prem/Cont. (Y) <span class="pull-right">RM

																												<c:choose>
																													<c:when
																														test="${dashBoard.f_WTC_Sold_Amount_Y>'0'}">
																														<c:out
																															value="${dashBoard.f_WTC_Sold_Amount_Y}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</span>
																										</p>
																									</div>
																									<!-- /.panel-body -->
																								</div>
																								<!-- /.panel panel-success panel-block-color -->
																								<!-- END TODAY VISITOR TILES -->
																							</div>
																							<div class="col-sm-12">
																								<!-- BEGIN TODAY VISITOR TILES -->
																								<div
																									class="panel panel-primary panel-square panel-no-border">
																									<div class="panel-body">
																										<p class="cat-head text-center">EGIB</p>
																										<h1 class="bolded tiles-number text-center">

																											<c:choose>
																												<c:when
																													test="${dashBoard.f_WTC_TotalPoliciesSold_EIB>'0'}">
																													<c:out
																														value="${dashBoard.f_WTC_TotalPoliciesSold_EIB}" />
																												</c:when>
																												<c:otherwise>
																													<c:out value="0" />
																												</c:otherwise>
																											</c:choose>
																										</h1>
																										<p class="text-center text20">
																											<strong>RM <c:choose>
																													<c:when
																														test="${dashBoard.f_WTC_Sold_Amount_EIB>'0'}">
																														<c:out
																															value="${dashBoard.f_WTC_Sold_Amount_EIB}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</strong>
																										</p>
																									</div>
																									<!-- /.panel-body -->
																								</div>
																								<!-- /.panel panel-success panel-block-color -->
																								<!-- END TODAY VISITOR TILES -->
																							</div>
																							<div class="col-sm-12">
																								<!-- BEGIN TODAY VISITOR TILES -->
																								<div
																									class="panel panel-primary panel-square panel-no-border">
																									<div class="panel-body">
																										<p class="cat-head text-center">EGTB</p>
																										<h1 class="bolded tiles-number text-center">

																											<c:choose>
																												<c:when
																													test="${dashBoard.f_WTC_TotalPoliciesSold_ETB>'0'}">
																													<c:out
																														value="${dashBoard.f_WTC_TotalPoliciesSold_ETB}" />
																												</c:when>
																												<c:otherwise>
																													<c:out value="0" />
																												</c:otherwise>
																											</c:choose>
																										</h1>
																										<p class="text-center text20">
																											<strong>RM <c:choose>
																													<c:when
																														test="${dashBoard.f_WTC_Sold_Amount_ETB>'0'}">
																														<c:out
																															value="${dashBoard.f_WTC_Sold_Amount_ETB}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>

																											</strong>
																										</p>
																									</div>
																									<!-- /.panel-body -->
																								</div>
																								<!-- /.panel panel-success panel-block-color -->
																								<!-- END TODAY VISITOR TILES -->
																							</div>
																						</div>
																					</div> --%>
																					<!-- /.col-sm-3 -->
																					<div class="col-sm-3">
																						<div class="row">
																							<div class="col-sm-12">
																								<!-- BEGIN TODAY VISITOR TILES -->
																								<div
																									class="panel panel-danger panel-square panel-no-border">
																									<div class="panel-body">
																										<p class="cat-head text-center">HOHH</p>
																										<h1 class="bolded tiles-number text-center">

																											<c:choose>
																												<c:when
																													test="${dashBoard.f_HOHH_TotalPoliciesSold>=0}">
																													<c:out
																														value="${dashBoard.f_HOHH_TotalPoliciesSold}" />
																												</c:when>
																												<c:otherwise>
																													<c:out value="0" />
																												</c:otherwise>
																											</c:choose>
																										</h1>
																										<p class="text-center text20">
																											<strong>RM <c:choose>
																													<c:when
																														test="${dashBoard.f_HOHH_Sold_Amount>'0'}">
																														<c:out
																															value="${dashBoard.f_HOHH_Sold_Amount}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>

																											</strong>
																										</p>
																										<div class="gap gap-mini"></div>
																										<p>
																											Total Policy(MTD) <span class="pull-right">
																												<c:choose>
																													<c:when
																														test="${dashBoard.f_HOHH_TotalPoliciesSold_PM>'0'}">
																														<c:out
																															value="${dashBoard.f_HOHH_TotalPoliciesSold_PM}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>

																											</span>
																										</p>
																										<p>
																											Total Policy(YTD) <span class="pull-right">
																												<c:choose>
																													<c:when
																														test="${dashBoard.f_HOHH_TotalPoliciesSold_PY>'0'}">
																														<c:out
																															value="${dashBoard.f_HOHH_TotalPoliciesSold_PY}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</span>
																										</p>
																										<div class="gap gap-mini"></div>
																										<p class="small">
																											Prem./Cont. (M) <span class="pull-right">RM
																												<c:choose>
																													<c:when
																														test="${dashBoard.f_HOHH_Sold_Amount_M>'0'}">
																														<c:out
																															value="${dashBoard.f_HOHH_Sold_Amount_M}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</span>
																										</p>
																										<p class="small">
																											Prem./Cont. (Y) <span class="pull-right">RM
																												<c:choose>
																													<c:when
																														test="${dashBoard.f_HOHH_Sold_Amount_Y>'0'}">
																														<c:out
																															value="${dashBoard.f_HOHH_Sold_Amount_Y}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</span>
																										</p>
																									</div>
																									<!-- /.panel-body -->
																								</div>
																								<!-- /.panel panel-success panel-block-color -->
																								<!-- END TODAY VISITOR TILES -->
																							</div>
																							<div class="col-sm-12">
																								<!-- BEGIN TODAY VISITOR TILES -->
																								<div
																									class="panel panel-danger panel-square panel-no-border">
																									<div class="panel-body">
																										<p class="cat-head text-center">EGIB</p>
																										<h1 class="bolded tiles-number text-center">
																											<c:choose>
																												<c:when
																													test="${dashBoard.f_HOHH_TotalPoliciesSold_EIB>'0'}">
																													<c:out
																														value="${dashBoard.f_HOHH_TotalPoliciesSold_EIB}" />
																												</c:when>
																												<c:otherwise>
																													<c:out value="0" />
																												</c:otherwise>
																											</c:choose>

																										</h1>
																										<p class="text-center text20">
																											<strong>RM <c:choose>
																													<c:when
																														test="${dashBoard.f_HOHH_Sold_Amount_EIB>'0'}">
																														<c:out
																															value="${dashBoard.f_HOHH_Sold_Amount_EIB}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</strong>
																										</p>
																									</div>
																									<!-- /.panel-body -->
																								</div>
																								<!-- /.panel panel-success panel-block-color -->
																								<!-- END TODAY VISITOR TILES -->
																							</div>
																							<div class="col-sm-12">
																								<!-- BEGIN TODAY VISITOR TILES -->
																								<div
																									class="panel panel-danger panel-square panel-no-border">
																									<div class="panel-body">
																										<p class="cat-head text-center">EGTB</p>
																										<h1 class="bolded tiles-number text-center">

																											<c:choose>
																												<c:when
																													test="${dashBoard.f_HOHH_TotalPoliciesSold_ETB>'0'}">
																													<c:out
																														value="${dashBoard.f_HOHH_TotalPoliciesSold_ETB}" />
																												</c:when>
																												<c:otherwise>
																													<c:out value="0" />
																												</c:otherwise>
																											</c:choose>
																										</h1>
																										<p class="text-center text20">
																											<strong>RM <c:choose>
																													<c:when
																														test="${dashBoard.f_HOHH_Sold_Amount_ETB>'0'}">
																														<c:out
																															value="${dashBoard.f_HOHH_Sold_Amount_ETB}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>




																											</strong>
																										</p>
																									</div>
																									<!-- /.panel-body -->
																								</div>
																								<!-- /.panel panel-success panel-block-color -->
																								<!-- END TODAY VISITOR TILES -->
																							</div>
																						</div>

																					</div>
																					<!-- /.col-sm-3 -->
																					<div class="col-sm-3">
																					<div class="row">
																						<div class="col-sm-12">
																						<!-- BEGIN TODAY VISITOR TILES -->
																						<div
																							class="panel panel-warning panel-square panel-no-border">
																							<div class="panel-body">
																								<p class="cat-head text-center">Motor</p>
																								<h1 class="bolded tiles-number text-center">

																									<c:choose>
																										<c:when
																											test="${dashBoard.f_MI_TotalPoliciesSold>'0'}">
																											<c:out
																												value="${dashBoard.f_MI_TotalPoliciesSold}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>
																								</h1>
																								<p class="text-center text20">
																									<strong>RM <c:choose>
																											<c:when
																												test="${dashBoard.f_Motor_Sold_Amount>'0'}">
																												<c:out
																													value="${dashBoard.f_Motor_Sold_Amount}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>

																									</strong>
																								</p>
																								<div class="gap gap-mini"></div>
																								<p>
																									Total Policy(MTD) <span class="pull-right">
																										<c:choose>
																											<c:when
																												test="${dashBoard.f_MI_TotalPoliciesSold_PM>'0'}">
																												<c:out
																													value="${dashBoard.f_MI_TotalPoliciesSold_PM}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>

																									</span>
																								</p>
																								<p>
																									Total Policy(YTD) <span class="pull-right">
																										<c:choose>
																											<c:when
																												test="${dashBoard.f_MI_TotalPoliciesSold_PY>'0'}">
																												<c:out
																													value="${dashBoard.f_MI_TotalPoliciesSold_PY}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>

																									</span>


																								</p>
																								<div class="gap gap-mini"></div>
																								<p class="small">
																									Prem./Cont. (M) <span class="pull-right">RM
																										<c:choose>
																											<c:when
																												test="${dashBoard.f_Motor_Sold_Amount_M>'0'}">
																												<c:out
																													value="${dashBoard.f_Motor_Sold_Amount_M}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>

																									</span>
																								</p>
																								<p class="small">
																									Prem./Cont. (Y) <span class="pull-right">RM
																										<c:choose>
																											<c:when
																												test="${dashBoard.f_Motor_Sold_Amount_Y>'0'}">
																												<c:out
																													value="${dashBoard.f_Motor_Sold_Amount_Y}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>
																									</span>
																								</p>
																							</div>
																							<!-- /.panel-body -->
																						</div>
																						</div>
																						<div class="col-sm-12">
																						<div
																							class="panel panel-warning panel-square panel-no-border">
																							<div class="panel-body">
																								<p class="cat-head text-center">EGIB</p>
																								<h1 class="bolded tiles-number text-center">
																									<c:choose>
																										<c:when
																											test="${dashBoard.f_MI_TotalPoliciesSold_EIB>'0'}">
																											<c:out
																												value="${dashBoard.f_MI_TotalPoliciesSold_EIB}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>

																								</h1>
																								<p class="text-center text20">
																									<strong>RM <c:choose>
																											<c:when
																												test="${dashBoard.f_MI_Sold_Amount_EIB>'0'}">
																												<c:out
																													value="${dashBoard.f_MI_Sold_Amount_EIB}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>
																									</strong>
																								</p>
																							</div>
																							<!-- /.panel-body -->
																						</div>
																						</div>
																						<div class="col-sm-12">
																						<div
																							class="panel panel-warning panel-square panel-no-border">
																							<div class="panel-body">
																								<p class="cat-head text-center">EGTB</p>
																								<h1 class="bolded tiles-number text-center">
																									<c:choose>
																										<c:when
																											test="${dashBoard.f_MT_TotalPoliciesSold_ETB>'0'}">
																											<c:out
																												value="${dashBoard.f_MT_TotalPoliciesSold_ETB}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>

																								</h1>
																								<p class="text-center text20">
																									<strong>RM <c:choose>
																											<c:when
																												test="${dashBoard.f_MT_Sold_Amount_ETB>'0'}">
																												<c:out
																													value="${dashBoard.f_MT_Sold_Amount_ETB}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>
																									</strong>
																								</p>
																							</div>
																							<!-- /.panel-body -->
																						</div>
																					</div>
																					<!-- /.col-sm-6 -->
																				</div>
																				<!--end row -->
																			</div>
																			
																			
																				<div class="col-sm-3">
																						<div class="row">
																							<div class="col-sm-12">
																								<!-- BEGIN TODAY VISITOR TILES -->
																								<div
																									class="panel panel-primary panel-square panel-no-border">
																									<div class="panel-body">
																										<p class="cat-head text-center">e-Medical Pass</p>
																										<h1 class="bolded tiles-number text-center">
																											<c:choose>
																										<c:when
																											test="${dashBoard.f_MP_TotalPoliciesSold>'0'}">
																											<c:out
																												value="${dashBoard.f_MP_TotalPoliciesSold}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>
																								</h1>
																								<p class="text-center text20">
																									<strong>RM <c:choose>
																											<c:when
																												test="${dashBoard.f_Medical_Sold_Amount>'0'}">
																												<c:out
																													value="${dashBoard.f_Medical_Sold_Amount}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>

																									</strong>
																								</p>
																								<div class="gap gap-mini"></div>
																								<p>
																									Total Policy(MTD) <span class="pull-right">
																										<c:choose>
																											<c:when
																												test="${dashBoard.f_MP_TotalPoliciesSold_PM>'0'}">
																												<c:out
																													value="${dashBoard.f_MP_TotalPoliciesSold_PM}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>

																									</span>
																								</p>
																								<p>
																									Total Policy(YTD) <span class="pull-right">
																										<c:choose>
																											<c:when
																												test="${dashBoard.f_MP_TotalPoliciesSold_PY>'0'}">
																												<c:out
																													value="${dashBoard.f_MP_TotalPoliciesSold_PY}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>

																									</span>


																								</p>
																								<div class="gap gap-mini"></div>
																								<p class="small">
																									Prem./Cont. (M) <span class="pull-right">RM
																										<c:choose>
																											<c:when
																												test="${dashBoard.f_Medical_Sold_Amount_M>'0'}">
																												<c:out
																													value="${dashBoard.f_Medical_Sold_Amount_M}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>

																									</span>
																								</p>
																								<p class="small">
																									Prem./Cont. (Y) <span class="pull-right">RM
																										<c:choose>
																											<c:when
																												test="${dashBoard.f_Medical_Sold_Amount_Y>'0'}">
																												<c:out
																													value="${dashBoard.f_Medical_Sold_Amount_Y}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>
																									</span>
																								</p>
																							</div>
																							<!-- /.panel-body -->
																						</div>
																						</div>
																						<!-- /.panel panel-success panel-block-color -->
																						<!-- END TODAY VISITOR TILES -->
																						<div class="col-sm-12">
																						<div
																							class="panel panel-primary panel-square panel-no-border">
																							<div class="panel-body">
																								<p class="cat-head text-center">ELIB</p>
																								<h1 class="bolded tiles-number text-center">
																									<c:choose>
																										<c:when
																											test="${dashBoard.f_MP_TotalPoliciesSold_EIB>'0'}">
																											<c:out
																												value="${dashBoard.f_MP_TotalPoliciesSold_EIB}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>

																								</h1>
																								<p class="text-center text20">
																									<strong>RM <c:choose>
																											<c:when
																												test="${dashBoard.f_MP_Sold_Amount_EIB>'0'}">
																												<c:out
																													value="${dashBoard.f_MP_Sold_Amount_EIB}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>
																									</strong>
																								</p>
																							</div>
																							<!-- /.panel-body -->
																						</div>
																						</div>
																						<div class="col-sm-12">
																						<div
																							class="panel panel-primary panel-square panel-no-border">
																							<div class="panel-body">
																								<p class="cat-head text-center">EFTB</p>
																								<h1 class="bolded tiles-number text-center">
																									<c:choose>
																										<c:when
																											test="${dashBoard.f_MPT_TotalPoliciesSold_ETB>'0'}">
																											<c:out
																												value="${dashBoard.f_MPT_TotalPoliciesSold_ETB}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>

																								</h1>
																								<p class="text-center text20">
																									<strong>RM <c:choose>
																											<c:when
																												test="${dashBoard.f_MPT_Sold_Amount_ETB>'0'}">
																												<c:out
																													value="${dashBoard.f_MPT_Sold_Amount_ETB}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>

																											</strong>
																										</p>
																									</div>
																									<!-- /.panel-body -->
																								</div>
																								<!-- /.panel panel-success panel-block-color -->
																								<!-- END TODAY VISITOR TILES -->
																							</div>
																						</div>
																					</div>
																			
																			
																			
												</div>
												</div>
												<div class="col-sm-12">
																				<!-- group 1 -->
																				<div class="row">
																					<div class="col-sm-3">
																						<div class="row">
																							<div class="col-sm-12">
																								<!-- BEGIN TODAY VISITOR TILES -->
																								<div
																									class="panel panel-success panel-square panel-no-border">
																									<div class="panel-body">
																										<p class="cat-head text-center">TERM LIFE</p>
																										<h1 class="bolded tiles-number text-center">

																											<c:choose>
																												<c:when
																													test="${dashBoard.f_TL_TotalPoliciesSold>'0'}">
																													<c:out
																														value="${dashBoard.f_TL_TotalPoliciesSold}" />
																												</c:when>
																												<c:otherwise>
																													<c:out value="0" />
																												</c:otherwise>
																											</c:choose>

																										</h1>
																										<p class="text-center text20">
																											<strong>RM <c:choose>
																													<c:when
																														test="${dashBoard.f_TL_Sold_Amount>'0'}">
																														<c:out
																															value="${dashBoard.f_TL_Sold_Amount}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>

																											</strong>
																										</p>
																										<div class="gap gap-mini"></div>
																										<p>
																											Total Policy(MTD) <span class="pull-right">
																												<c:choose>
																													<c:when
																														test="${dashBoard.f_TL_TotalPoliciesSold_PM>'0'}">
																														<c:out
																															value="${dashBoard.f_TL_TotalPoliciesSold_PM}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</span>
																										</p>
																										<p>
																											Total Policy(YTD) <span class="pull-right">
																												<c:choose>
																													<c:when
																														test="${dashBoard.f_TL_TotalPoliciesSold_PY>'0'}">
																														<c:out
																															value="${dashBoard.f_TL_TotalPoliciesSold_PY}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</span>
																										</p>
																										<div class="gap gap-mini"></div>
																										<p class="small">
																											Prem./Cont. (M) <span class="pull-right">RM
																												<c:choose>
																													<c:when
																														test="${dashBoard.f_TL_Sold_Amount_M>'0'}">
																														<c:out
																															value="${dashBoard.f_TL_Sold_Amount_M}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>

																											</span>
																										</p>
																										<p class="small">
																											Prem./Cont. (Y) <span class="pull-right">RM
																												<c:choose>
																													<c:when
																														test="${dashBoard.f_TL_Sold_Amount_Y>'0'}">
																														<c:out
																															value="${dashBoard.f_TL_Sold_Amount_Y}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</span>
																										</p>
																									</div>
																									<!-- /.panel-body -->
																								</div>
																								<!-- /.panel panel-success panel-block-color -->
																								<!-- END TODAY VISITOR TILES -->

																							</div>
																							<div class="col-sm-12">
																								<!-- BEGIN TODAY VISITOR TILES -->
																								<div
																									class="panel panel-success panel-square panel-no-border">
																									<div class="panel-body">
																										<p class="cat-head text-center">Ezy Life</p>
																										<h1 class="bolded tiles-number text-center">

																											<c:choose>
																												<c:when
																													test="${dashBoard.f_EL_TotalPoliciesSold>'0'}">
																													<c:out
																														value="${dashBoard.f_EL_TotalPoliciesSold}" />
																												</c:when>
																												<c:otherwise>
																													<c:out value="0" />
																												</c:otherwise>
																											</c:choose>

																										</h1>
																										<p class="text-center text20">
																											<strong>RM <c:choose>
																													<c:when
																														test="${dashBoard.f_EL_Sold_Amount>'0'}">
																														<c:out
																															value="${dashBoard.f_EL_Sold_Amount}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>

																											</strong>
																										</p>

																										<!-- /.panel-body -->
																									</div>
																									<!-- /.panel panel-success panel-block-color -->
																									<!-- END TODAY VISITOR TILES -->
																								</div>
																							</div>
																							<div class="col-sm-12">
																								<!-- BEGIN TODAY VISITOR TILES -->
																								<div
																									class="panel panel-success panel-square panel-no-border">
																									<div class="panel-body">
																										<p class="cat-head text-center">IDS</p>
																										<h1 class="bolded tiles-number text-center">

																											<c:choose>
																												<c:when
																													test="${dashBoard.f_IDS_TotalPoliciesSold>'0'}">
																													<c:out
																														value="${dashBoard.f_IDS_TotalPoliciesSold}" />
																												</c:when>
																												<c:otherwise>
																													<c:out value="0" />
																												</c:otherwise>
																											</c:choose>

																										</h1>
																										<p class="text-center text20">
																											<strong>RM <c:choose>
																													<c:when
																														test="${dashBoard.f_IDS_Sold_Amount>'0'}">
																														<c:out
																															value="${dashBoard.f_IDS_Sold_Amount}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</strong>
																										</p>
																										<!-- /.panel-body -->
																									</div>
																									<!-- /.panel panel-success panel-block-color -->
																									<!-- END TODAY VISITOR TILES -->
																								</div>
																							</div>
																						</div>
																					</div>
																					
																					<div class="col-sm-3">
																						<div class="row">
																							<!-- Pure Term Life Start -->
																							<div class="col-sm-12">
																								<!-- BEGIN TODAY VISITOR TILES -->
																								<div
																									class="panel panel-info panel-square panel-no-border">
																									<div class="panel-body">
																										<p class="cat-head text-center">PURE TERM
																											LIFE</p>
																										<h1 class="bolded tiles-number text-center">

																											<c:choose>
																												<c:when
																													test="${dashBoard.f_PTL_TotalPoliciesSold>'0'}">
																													<c:out
																														value="${dashBoard.f_PTL_TotalPoliciesSold}" />
																												</c:when>
																												<c:otherwise>
																													<c:out value="0" />
																												</c:otherwise>
																											</c:choose>

																										</h1>
																										<p class="text-center text20">
																											<strong>RM <c:choose>
																													<c:when
																														test="${dashBoard.f_PTL_Sold_Amount>'0'}">
																														<c:out
																															value="${dashBoard.f_PTL_Sold_Amount}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>

																											</strong>
																										</p>
																										<div class="gap gap-mini"></div>
																										<p>
																											Total Policy(MTD) <span class="pull-right">
																												<c:choose>
																													<c:when
																														test="${dashBoard.f_PTL_TotalPoliciesSold_PM>'0'}">
																														<c:out
																															value="${dashBoard.f_PTL_TotalPoliciesSold_PM}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</span>
																										</p>
																										<p>
																											Total Policy(YTD) <span class="pull-right">
																												<c:choose>
																													<c:when
																														test="${dashBoard.f_PTL_TotalPoliciesSold_PY>'0'}">
																														<c:out
																															value="${dashBoard.f_PTL_TotalPoliciesSold_PY}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</span>
																										</p>
																										<div class="gap gap-mini"></div>
																										<p class="small">
																											Prem./Cont. (M) <span class="pull-right">RM
																												<c:choose>
																													<c:when
																														test="${dashBoard.f_PTL_Sold_Amount_M>'0'}">
																														<c:out
																															value="${dashBoard.f_PTL_Sold_Amount_M}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>

																											</span>
																										</p>
																										<p class="small">
																											Prem./Cont. (Y) <span class="pull-right">RM
																												<c:choose>
																													<c:when
																														test="${dashBoard.f_PTL_Sold_Amount_Y>'0'}">
																														<c:out
																															value="${dashBoard.f_PTL_Sold_Amount_Y}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</span>
																										</p>
																									</div>
																									<!-- /.panel-body -->
																								</div>
																								<!-- /.panel panel-success panel-block-color -->
																								<!-- END TODAY VISITOR TILES -->

																							</div>

																							<!-- Added 13122017 For Ezy Secure    -->
																							<!-- /.col-sm-6 -->
																							<div class="col-sm-12">
																								<!-- BEGIN TODAY VISITOR TILES -->
																								<div
																									class="panel panel-info panel-square panel-no-border">
																									<div class="panel-body">
																										<p class="cat-head text-center">Ezy Secure</p>
																										<h1 class="bolded tiles-number text-center">

																											<c:choose>
																												<c:when
																													test="${dashBoard.f_EZS_TotalPoliciesSold>'0'}">
																													<c:out
																														value="${dashBoard.f_EZS_TotalPoliciesSold}" />
																												</c:when>
																												<c:otherwise>
																													<c:out value="0" />
																												</c:otherwise>
																											</c:choose>

																										</h1>
																										<p class="text-center text20">
																											<strong>RM <c:choose>
																													<c:when
																														test="${dashBoard.f_EZS_Sold_Amount>'0'}">
																														<c:out
																															value="${dashBoard.f_EZS_Sold_Amount}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>

																											</strong>
																										</p>

																										<!-- /.panel-body -->
																									</div>
																									<!-- /.panel panel-success panel-block-color -->
																									<!-- END TODAY VISITOR TILES -->
																								</div>
																							</div>


																							<!-- Added 13122017 For I-SECURE    -->
																							<!-- /.col-sm-6 -->
																							<div class="col-sm-12">
																								<!-- BEGIN TODAY VISITOR TILES -->
																								<div
																									class="panel panel-info panel-square panel-no-border">
																									<div class="panel-body">
																										<p class="cat-head text-center">I-SECURE</p>
																										<h1 class="bolded tiles-number text-center">

																											<c:choose>
																												<c:when
																													test="${dashBoard.f_ISC_TotalPoliciesSold>'0'}">
																													<c:out
																														value="${dashBoard.f_ISC_TotalPoliciesSold}" />
																												</c:when>
																												<c:otherwise>
																													<c:out value="0" />
																												</c:otherwise>
																											</c:choose>

																										</h1>
																										<p class="text-center text20">
																											<strong>RM <c:choose>
																													<c:when
																														test="${dashBoard.f_ISC_Sold_Amount>'0'}">
																														<c:out
																															value="${dashBoard.f_ISC_Sold_Amount}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</strong>
																										</p>
																										<!-- /.panel-body -->
																									</div>
																									<!-- /.panel panel-success panel-block-color -->
																									<!-- END TODAY VISITOR TILES -->
																								</div>
																							</div>

																						</div>
																						<!-- Pure Term Life End -->
																					</div>
																					
																					<!--  CANCER CARE -->
																					<!-- /.col-sm-3 -->
																					<div class="col-sm-3">
																					<div class="row">
																					<div class="col-sm-12">
																					
																						<div
																							class="panel panel-danger panel-square panel-no-border">
																							<div class="panel-body">
																								<p class="cat-head text-center">e-Cancer Care</p>
																								<h1 class="bolded tiles-number text-center">

																									<c:choose>
																										<c:when
																											test="${dashBoard.f_CI_TotalPoliciesSold>'0'}">
																											<c:out
																												value="${dashBoard.f_CI_TotalPoliciesSold}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>
																								</h1>
																								<p class="text-center text20">
																									<strong>RM <c:choose>
																											<c:when
																												test="${dashBoard.f_Cancer_Sold_Amount>'0'}">
																												<c:out
																													value="${dashBoard.f_Cancer_Sold_Amount}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>

																									</strong>
																								</p>
																								<div class="gap gap-mini"></div>
																								<p>
																									Total Policy(MTD) <span class="pull-right">
																										<c:choose>
																											<c:when
																												test="${dashBoard.f_CI_TotalPoliciesSold_PM>'0'}">
																												<c:out
																													value="${dashBoard.f_CI_TotalPoliciesSold_PM}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>

																									</span>
																								</p>
																								<p>
																									Total Policy(YTD) <span class="pull-right">
																										<c:choose>
																											<c:when
																												test="${dashBoard.f_CI_TotalPoliciesSold_PY>'0'}">
																												<c:out
																													value="${dashBoard.f_CI_TotalPoliciesSold_PY}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>

																									</span>


																								</p>
																								<div class="gap gap-mini"></div>
																								<p class="small">
																									Prem./Cont. (M) <span class="pull-right">RM
																										<c:choose>
																											<c:when
																												test="${dashBoard.f_Cancer_Sold_Amount_M>'0'}">
																												<c:out
																													value="${dashBoard.f_Cancer_Sold_Amount_M}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>

																									</span>
																								</p>
																								<p class="small">
																									Prem./Cont. (Y) <span class="pull-right">RM
																										<c:choose>
																											<c:when
																												test="${dashBoard.f_Cancer_Sold_Amount_Y>'0'}">
																												<c:out
																													value="${dashBoard.f_Cancer_Sold_Amount_Y}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>
																									</span>
																								</p>
																							</div>
																							<!-- /.panel-body -->
																						</div>
																						</div>
																						<!-- /.panel panel-success panel-block-color -->
																						<!-- END TODAY VISITOR TILES -->
																						<div class="col-sm-12">
																						<div
																							class="panel panel-danger panel-square panel-no-border">
																							<div class="panel-body">
																								<p class="cat-head text-center">ELIB</p>
																								<h1 class="bolded tiles-number text-center">
																									<c:choose>
																										<c:when
																											test="${dashBoard.f_CI_TotalPoliciesSold_EIB>'0'}">
																											<c:out
																												value="${dashBoard.f_CI_TotalPoliciesSold_EIB}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>

																								</h1>
																								<p class="text-center text20">
																									<strong>RM <c:choose>
																											<c:when
																												test="${dashBoard.f_CI_Sold_Amount_EIB>'0'}">
																												<c:out
																													value="${dashBoard.f_CI_Sold_Amount_EIB}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>
																									</strong>
																								</p>
																							</div>
																							<!-- /.panel-body -->
																						</div>
																						</div>
																						<div class="col-sm-12">
																						<div
																							class="panel panel-danger panel-square panel-no-border">
																							<div class="panel-body">
																								<p class="cat-head text-center">EFTB</p>
																								<h1 class="bolded tiles-number text-center">
																									<c:choose>
																										<c:when
																											test="${dashBoard.f_CT_TotalPoliciesSold_ETB>'0'}">
																											<c:out
																												value="${dashBoard.f_CT_TotalPoliciesSold_ETB}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>

																								</h1>
																								<p class="text-center text20">
																									<strong>RM <c:choose>
																											<c:when
																												test="${dashBoard.f_CT_Sold_Amount_ETB>'0'}">
																												<c:out
																													value="${dashBoard.f_CT_Sold_Amount_ETB}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>
																									</strong>
																								</p>
																							</div>
																							<!-- /.panel-body -->
																						</div>
																					</div>
																					<!-- /.col-sm-6 -->
																				</div>
																				<!--end row -->
																			</div>
																			</div>
																			</div>
																			<div class="col-sm-12">
																				<!-- group 1 -->
																				<div class="row">

																			
					
																					<div class="col-sm-3">

																						<div class="row">
																							<div class="col-sm-12">
																					
																						<!-- BEGIN TODAY VISITOR TILES -->
																						<div
																							class="panel panel-purple panel-square panel-no-border">
																							<div class="panel-body">
																								<p class="cat-head text-center margin0">Buddy
																									PA</p>
																								<h1 class="bolded tiles-number text-center">

																									<c:choose>
																										<c:when
																											test="${dashBoard.f_Buddy_TotalPoliciesSold>'0'}">
																											<c:out
																												value="${dashBoard.f_Buddy_TotalPoliciesSold}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>
																								</h1>
																								<p class="text-center text20 margin0">
																									<strong>RM <c:choose>
																											<c:when
																												test="${dashBoard.f_Buddy_Sold_Amount>'0'}">
																												<c:out
																													value="${dashBoard.f_Buddy_Sold_Amount}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>

																									</strong>
																								</p>
																								<div class="gap gap-mini"></div>
																								<p class="margin0">
																									Total Policy(MTD) <span class="pull-right">
																										<c:choose>
																											<c:when
																												test="${dashBoard.f_Buddy_TotalPoliciesSold_PM>'0'}">
																												<c:out
																													value="${dashBoard.f_Buddy_TotalPoliciesSold_PM}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>

																									</span>
																								</p>
																								<p class="margin0">
																									Total Policy(YTD) <span class="pull-right">
																										<c:choose>
																											<c:when
																												test="${dashBoard.f_Buddy_TotalPoliciesSold_PY>'0'}">
																												<c:out
																													value="${dashBoard.f_Buddy_TotalPoliciesSold_PY}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>

																									</span>


																								</p>
																								<div class="gap gap-mini"></div>
																								<p class="small margin0">
																									Prem./Cont. (M) <span class="pull-right">RM
																										<c:choose>
																											<c:when
																												test="${dashBoard.f_Buddy_Sold_Amount_M>'0'}">
																												<c:out
																													value="${dashBoard.f_Buddy_Sold_Amount_M}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>

																									</span>
																								</p>
																								<p class="small margin0">
																									Prem./Cont. (Y) <span class="pull-right">RM
																										<c:choose>
																											<c:when
																												test="${dashBoard.f_Buddy_Sold_Amount_Y>'0'}">
																												<c:out
																													value="${dashBoard.f_Buddy_Sold_Amount_Y}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>
																									</span>
																								</p>
																								<div class="gap gap-mini"></div>
																								<p class="small margin0">
																									Latest Policy Number (Insurance) <span
																										class="pull-right">${dashBoard.PA_POL_NUM_I}
																									</span>
																								</p>
																								<p class="small margin0">
																									Latest Policy Number (Takaful) <span
																										class="pull-right">${dashBoard.PA_POL_NUM_T}
																									</span>
																								</p>
																							</div>
																							</div>
																							</div>
																							<!-- /.panel-body -->
																						
																						<!-- /.panel panel-success panel-block-color -->
																						<!-- END TODAY VISITOR TILES -->
																						<div class="col-sm-12">
																						<div
																							class="panel panel-purple panel-square panel-no-border">
																							<div class="panel-body">
																								<p class="cat-head text-center margin0">EGIB</p>
																								<h1 class="bolded tiles-number text-center">
																									<c:choose>
																										<c:when
																											test="${dashBoard.f_Buddy_TotalPoliciesSold_EIB>'0'}">
																											<c:out
																												value="${dashBoard.f_Buddy_TotalPoliciesSold_EIB}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>

																								</h1>
																								<p class="text-center text20 margin0">
																									<strong>RM <c:choose>
																											<c:when
																												test="${dashBoard.f_Buddy_Sold_Amount_EIB>'0'}">
																												<c:out
																													value="${dashBoard.f_Buddy_Sold_Amount_EIB}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>
																									</strong>
																								</p>
																							</div>
																							<!-- /.panel-body -->
																						</div>
																						</div>
																						<div class="col-sm-12">
																						<div
																							class="panel panel-purple panel-square panel-no-border">
																							<div class="panel-body">
																								<p class="cat-head text-center margin0">EGTB</p>
																								<h1 class="bolded tiles-number text-center">
																									<c:choose>
																										<c:when
																											test="${dashBoard.f_Buddy_TotalPoliciesSold_ETB>'0'}">
																											<c:out
																												value="${dashBoard.f_Buddy_TotalPoliciesSold_ETB}" />
																										</c:when>
																										<c:otherwise>
																											<c:out value="0" />
																										</c:otherwise>
																									</c:choose>

																								</h1>
																								<p class="text-center text20 margin0">
																									<strong>RM <c:choose>
																											<c:when
																												test="${dashBoard.f_Buddy_Sold_Amount_ETB>'0'}">
																												<c:out
																													value="${dashBoard.f_Buddy_Sold_Amount_ETB}" />
																											</c:when>
																											<c:otherwise>
																												<c:out value="0" />
																											</c:otherwise>
																										</c:choose>
																									</strong>
																								</p>
																							</div>
																							<!-- /.panel-body -->
																						</div>
																					</div>
																					<!-- /.col-sm-6 -->
																				</div>
																				</div>
																				<!--end row -->
																				
																				
																					<div class="col-sm-3">

																						<div class="row">
																		
																							<div class="col-sm-12">
																								<!-- BEGIN TODAY VISITOR TILES -->
																								<div
																									class="panel panel-blue panel-square panel-no-border">
																									<div class="panel-body">
																										<p class="cat-head text-center margin0">Travel
																											Ezy</p>
																										<h1 class="bolded tiles-number text-center">

																											<c:choose>
																												<c:when
																													test="${dashBoard.f_TrEzy_TotalPoliciesSold>'0'}">
																													<c:out
																														value="${dashBoard.f_TrEzy_TotalPoliciesSold}" />
																												</c:when>
																												<c:otherwise>
																													<c:out value="0" />
																												</c:otherwise>
																											</c:choose>

																										</h1>
																										<p class="text-center text20 margin0">
																											<strong>RM <c:choose>
																													<c:when
																														test="${dashBoard.f_TrEzy_Sold_Amount>'0'}">
																														<c:out
																															value="${dashBoard.f_TrEzy_Sold_Amount}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>

																											</strong>
																										</p>
																										<div class="gap gap-mini"></div>
																										<p class="margin0">
																											Total Policy(MTD) <span class="pull-right">
																												<c:choose>
																													<c:when
																														test="${dashBoard.f_TrEzy_TotalPoliciesSold_PM>'0'}">
																														<c:out
																															value="${dashBoard.f_TrEzy_TotalPoliciesSold_PM}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</span>
																										</p>
																										<p class="margin0">
																											Total Policy(YTD) <span class="pull-right">
																												<c:choose>
																													<c:when
																														test="${dashBoard.f_TrEzy_TotalPoliciesSold_PY>'0'}">
																														<c:out
																															value="${dashBoard.f_TrEzy_TotalPoliciesSold_PY}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</span>
																										</p>
																										<div class="gap gap-mini"></div>
																										<p class="small margin0">
																											Prem./Cont. (M) <span class="pull-right">RM
																												<c:choose>
																													<c:when
																														test="${dashBoard.f_TrEzy_Sold_Amount_M>'0'}">
																														<c:out
																															value="${dashBoard.f_TrEzy_Sold_Amount_M}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>

																											</span>
																										</p>
																										<p class="small margin0">
																											Prem./Cont. (Y) <span class="pull-right">RM
																												<c:choose>
																													<c:when
																														test="${dashBoard.f_TrEzy_Sold_Amount_Y>'0'}">
																														<c:out
																															value="${dashBoard.f_TrEzy_Sold_Amount_Y}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</span>
																										</p>
																										<div class="gap gap-mini"></div>
																										<p class="small margin0">
																											Latest Policy Number (Insurance) <span
																												class="pull-right">${dashBoard.TRAVEL_POL_NUM_I}
																											</span>
																										</p>
																										<p class="small margin0">
																											Latest Policy Number (Takaful) <span
																												class="pull-right">${dashBoard.TRAVEL_POL_NUM_T}
																											</span>
																										</p>
																									</div>
																									<!-- /.panel-body -->
																								</div>
																								<!-- /.panel panel-success panel-block-color -->
																								<!-- END TODAY VISITOR TILES -->

																							</div>

																							<!-- Added 14112017 For Travel Ezy    -->
																							<!-- /.col-sm-6 -->
																							<div class="col-sm-12">
																								<!-- BEGIN TODAY VISITOR TILES -->
																								<div
																									class="panel panel-blue panel-square panel-no-border">
																									<div class="panel-body">
																										<p class="cat-head text-center margin0">Travel
																											Ezy</p>
																										<h1 class="bolded tiles-number text-center">

																											<c:choose>
																												<c:when
																													test="${dashBoard.f_TrEzy_TotalPoliciesSold_EIB>'0'}">
																													<c:out
																														value="${dashBoard.f_TrEzy_TotalPoliciesSold_EIB}" />
																												</c:when>
																												<c:otherwise>
																													<c:out value="0" />
																												</c:otherwise>
																											</c:choose>

																										</h1>
																										<p class="text-center text20 margin0">
																											<strong>RM <c:choose>
																													<c:when
																														test="${dashBoard.f_TrEzy_Sold_Amount_EIB>'0'}">
																														<c:out
																															value="${dashBoard.f_TrEzy_Sold_Amount_EIB}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>

																											</strong>
																										</p>

																										<!-- /.panel-body -->
																									</div>
																									<!-- /.panel panel-success panel-block-color -->
																									<!-- END TODAY VISITOR TILES -->
																								</div>
																							</div>


																							<!-- Added 14112017 For IDS    -->
																							<!-- /.col-sm-6 -->
																							<div class="col-sm-12">
																								<!-- BEGIN TODAY VISITOR TILES -->
																								<div
																									class="panel panel-blue panel-square panel-no-border">
																									<div class="panel-body">
																										<p class="cat-head text-center margin0">i
																											Travel Ezy</p>
																										<h1 class="bolded tiles-number text-center">

																											<c:choose>
																												<c:when
																													test="${dashBoard.f_TrEzy_TotalPoliciesSold_ETB>'0'}">
																													<c:out
																														value="${dashBoard.f_TrEzy_TotalPoliciesSold_ETB}" />
																												</c:when>
																												<c:otherwise>
																													<c:out value="0" />
																												</c:otherwise>
																											</c:choose>

																										</h1>
																										<p class="text-center text20 margin0">
																											<strong>RM <c:choose>
																													<c:when
																														test="${dashBoard.f_TrEzy_Sold_Amount_ETB>'0'}">
																														<c:out
																															value="${dashBoard.f_TrEzy_Sold_Amount_ETB}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</strong>
																										</p>
																										<!-- /.panel-body -->
																									</div>
																									<!-- /.panel panel-success panel-block-color -->
																									<!-- END TODAY VISITOR TILES -->
																								</div>
																							</div>
																							</div>
																							</div>
																					
																					<div class="col-sm-3">

																						<div class="row">
																							<div class="col-sm-12">
																								<!-- BEGIN TODAY VISITOR TILES -->
																								<div
																									class="panel panel-pink panel-square panel-no-border">
																									<div class="panel-body">
																										<p class="cat-head text-center margin0">TripCare360</p>
																										<h1 class="bolded tiles-number text-center">

																											<c:choose>
																												<c:when
																													test="${dashBoard.f_TC_TotalPoliciesSold>'0'}">
																													<c:out
																														value="${dashBoard.f_TC_TotalPoliciesSold}" />
																												</c:when>
																												<c:otherwise>
																													<c:out value="0" />
																												</c:otherwise>
																											</c:choose>

																										</h1>
																										<p class="text-center text20 margin0">
																											<strong>RM <c:choose>
																													<c:when
																														test="${dashBoard.f_TC_Sold_Amount>'0'}">
																														<c:out
																															value="${dashBoard.f_TC_Sold_Amount}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>

																											</strong>
																										</p>
																										<div class="gap gap-mini"></div>
																										<p class="margin0">
																											Total Policy(MTD) <span class="pull-right">
																												<c:choose>
																													<c:when
																														test="${dashBoard.f_TC_TotalPoliciesSold_PM>'0'}">
																														<c:out
																															value="${dashBoard.f_TC_TotalPoliciesSold_PM}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</span>
																										</p>
																										<p class="margin0">
																											Total Policy(YTD) <span class="pull-right">
																												<c:choose>
																													<c:when
																														test="${dashBoard.f_TC_TotalPoliciesSold_PY>'0'}">
																														<c:out
																															value="${dashBoard.f_TC_TotalPoliciesSold_PY}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</span>
																										</p>
																										<div class="gap gap-mini"></div>
																										<p class="small margin0">
																											Prem./Cont. (M) <span class="pull-right">RM
																												<c:choose>
																													<c:when
																														test="${dashBoard.f_TC_Sold_Amount_M>'0'}">
																														<c:out
																															value="${dashBoard.f_TC_Sold_Amount_M}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>

																											</span>
																										</p>
																										<p class="small margin0">
																											Prem./Cont. (Y) <span class="pull-right">RM
																												<c:choose>
																													<c:when
																														test="${dashBoard.f_TC_Sold_Amount_Y>'0'}">
																														<c:out
																															value="${dashBoard.f_TC_Sold_Amount_Y}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</span>
																										</p>
																										<div class="gap gap-mini"></div>
																										<p class="small margin0">
																											Latest Policy Number (Insurance) <span
																												class="pull-right">${dashBoard.TRAVEL_POL_NUM_I}
																											</span>
																										</p>
																										<p class="small margin0">
																											Latest Policy Number (Takaful) <span
																												class="pull-right">${dashBoard.TRAVEL_POL_NUM_T}
																											</span>
																										</p>
																									</div>
																									<!-- /.panel-body -->
																								</div>
																								<!-- /.panel panel-success panel-block-color -->
																								<!-- END TODAY VISITOR TILES -->
																								</div>
																							

																							<!-- Added  For TripCare   -->
																							<!-- /.col-sm-6 -->
																							<div class="col-sm-12">
																								<!-- BEGIN TODAY VISITOR TILES -->
																								<div
																									class="panel panel-pink panel-square panel-no-border">
																									<div class="panel-body">
																										<p class="cat-head text-center margin0">EGIB</p>
																										<h1 class="bolded tiles-number text-center">

																											<c:choose>
																												<c:when
																													test="${dashBoard.f_TC_TotalPoliciesSold_EIB>'0'}">
																													<c:out
																														value="${dashBoard.f_TC_TotalPoliciesSold_EIB}" />
																												</c:when>
																												<c:otherwise>
																													<c:out value="0" />
																												</c:otherwise>
																											</c:choose>

																										</h1>
																										<p class="text-center text20 margin0">
																											<strong>RM <c:choose>
																													<c:when
																														test="${dashBoard.f_TC_Sold_Amount_EIB>'0'}">
																														<c:out
																															value="${dashBoard.f_TC_Sold_Amount_EIB}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>

																											</strong>
																										</p>

																										<!-- /.panel-body -->
																									</div>
																									<!-- /.panel panel-success panel-block-color -->
																									<!-- END TODAY VISITOR TILES -->
																								</div>
																							</div>


																							<!-- /.col-sm-6 -->
																							<div class="col-sm-12">
																								<!-- BEGIN TODAY VISITOR TILES -->
																								<div
																									class="panel panel-pink panel-square panel-no-border">
																									<div class="panel-body">
																										<p class="cat-head text-center margin0">EGTB</p>
																										<h1 class="bolded tiles-number text-center">

																											<c:choose>
																												<c:when
																													test="${dashBoard.f_TC_TotalPoliciesSold_ETB>'0'}">
																													<c:out
																														value="${dashBoard.f_TC_TotalPoliciesSold_ETB}" />
																												</c:when>
																												<c:otherwise>
																													<c:out value="0" />
																												</c:otherwise>
																											</c:choose>

																										</h1>
																										<p class="text-center text20 margin0">
																											<strong>RM <c:choose>
																													<c:when
																														test="${dashBoard.f_TC_Sold_Amount_ETB>'0'}">
																														<c:out
																															value="${dashBoard.f_TC_Sold_Amount_ETB}" />
																													</c:when>
																													<c:otherwise>
																														<c:out value="0" />
																													</c:otherwise>
																												</c:choose>
																											</strong>
																										</p>
																										<!-- /.panel-body -->
																									</div>
																									<!-- /.panel panel-success panel-block-color -->
																									<!-- END TODAY VISITOR TILES -->
																								</div>
																							</div>

																						</div>
																					</div>
																					
																					
																				</div>
																			</div>
																			</div>
																			</div>
														
												</div>
												</div>
				
											</c:when>
													</c:choose>
												<div class="row">
													<c:choose>
														<c:when test="${selectvalue=='entity'}">

															<div class="col-sm-12">
																<div class="row">
																	<div class="col-sm-12">
																		<div class="the-box">
																			<div class="row">
																				<div class="col-sm-6">
																					<p class="small-title cat-head-2">Performance
																						by Entity</p>
																				</div>
																				<div class="col-sm-6">
																					<select id="DataType" class="form-control">
																						<option value="dataMonth">Premium/Contribution
																							(MTD)</option>
																						<option value="dataYear">Premium/Contribution
																							(YTD)</option>
																					</select>
																				</div>
																			</div>
																			<div id="entity" style="height: 300px;"></div>
																		</div>
																	</div>
																</div>
															</div>

														</c:when>
													</c:choose>
													<!--end row -->
												</div>
												<c:choose>
													<c:when test="${selectvalue=='renewal_retension'}">
														<div class="row">
															<div class="col-sm-12">
																<div>
																	<div class="the-box">
																		<p class="small-title cat-head-2 text-center">Renewal
																			Retention(MTD)</p>
																		<div id="renewalretention"
																			style="height: 295px; width: 100%;"></div>
																	</div>
																</div>
															</div>
														</div>
													</c:when>
												</c:choose>

												<!-- /.col-sm-12 -->

												<!--end row -->
												<script type="text/javascript"
													src="${pageContext.request.contextPath}/assets/js/canvasjs.min.js"></script>
												<script type="text/javascript"
													src="${pageContext.request.contextPath}/assets/js/jquery.canvasjs.min.js"></script>

												<c:choose>
													<c:when test="${selectvalue=='cmpt'}">
														<div class="row">
															<div class="col-sm-12">
																<div class="the-box">
																	<p class="small-title cat-head-2">Current Month
																		Performance Trend</p>
																	<div id="chartContainer"
																		style="height: 330px; width: 100%;"></div>

																</div>
															</div>

														</div>
													</c:when>
												</c:choose>
												<!--end row -->
												<c:choose>
													<c:when test="${selectvalue=='salesfunnel'}">
														<div class="row">
															<div class="col-sm-12">
																<div class="the-box">
																	<div class="row">
																		<div class="col-sm-6">
																			<p class="small-title cat-head-2">Sales Funnel</p>
																		</div>
																		<div class="col-sm-6">
																			<select id="FunnelType" class="form-control"
																				onchange="salesfunnelInfo(this.value);">
																				<option value="Motor">Motor</option>
																				<option value="WTC">WTC</option>
																				<option value="HOHH">HOHH</option>
																				<option value="TermLife">Term Life</option>
																			</select>
																		</div>
																	</div>
																	<div id="funel2" style="height: 330px;"></div>
																	<div class="row">
																		<div class="col-sm-4">
																			<div
																				class="panel panel-success panel-square panel-no-border">
																				<div class="panel-body">
																					<table>
																						<thead></thead>
																						<tbody id="SFcountBody">
																							<%-- <p class="cat-head text-center">${dashBoard.f_Fun_MI_Success_Tran} Successful
																							Transactions</p> --%>
																						</tbody>
																					</table>
																				</div>
																				<!-- /.panel-body -->
																			</div>
																		</div>
																		<div class="col-sm-8">
																			<table class="table table-bordered" id="resulttable">
																				<thead>
																					<tr class="active">
																						<th>Stage</th>
																						<th>Prospect</th>
																						<th>Lost</th>
																					</tr>
																				</thead>
																				<tbody id="resultBody">

																				</tbody>
																			</table>
																		</div>
																	</div>
																</div>
															</div>
													</c:when>
												</c:choose>



												<c:choose>
													<c:when test="${selectvalue=='actual_budget'}">

														<!-- /.col-sm-8 -->
														<div class="col-sm-12">
															<div class="row">
																<div class="col-sm-12">
																	<div class="the-box">
																		<p class="small-title cat-head-2 text-center">Actual
																			vs Budget(MTD)</p>
																		<div id="actualbmtd" style="height: 200px;"></div>
																	</div>
																</div>
																<div class="col-sm-12">
																	<div class="the-box">
																		<p class="small-title cat-head-2 text-center">Actual
																			vs Budget(YTD)</p>
																		<div id="actualbytd" style="height: 200px;"></div>
																	</div>
																</div>
															</div>
														</div>
													</c:when>
												</c:choose>
												<!-- /.col-sm-4 -->
											</div>
											<!--end row -->
											<c:choose>
												<c:when test="${selectvalue=='agent_performance'}">
													<div class="row">
														<div class="col-sm-12">
															<div class="the-box">
																<p class="small-title cat-head-2">Cyber Agent
																	Performance (YTD)</p>
																<div id="cyberap" style="height: 300px;"></div>
															</div>
														</div>
														<!-- /.col-sm-12 -->
													</div>
												</c:when>
											</c:choose>
											<!--end row -->
											<c:choose>
												<c:when test="${selectvalue=='pcsr'}">
													<div class="row">
														<div class="col-sm-12">
															<div class="the-box">
																<div class="row">
																	<div class="col-sm-6">
																		<p class="small-title cat-head-2">Product
																			Cross-Sell Ratio(MTD)</p>
																	</div>
																	<div class="col-sm-6">
																		<select id="Datacross" class="form-control">
																			<option value="CrossSell">Cross-Sell Ratio</option>
																			<option value="CrossUp">Up-Sell Ratio</option>
																		</select>
																	</div>
																</div>
																<div id="CrossRatio" style="height: 300px;"></div>
															</div>
														</div>
														<!-- /.col-sm-12 -->
													</div>
												</c:when>
											</c:choose>
											<!--end row -->
											<c:choose>
												<c:when test="${Selectvalue=='motoraratio'}">
													<div class="row">
														<div class="col-sm-12">
															<div class="the-box">
																<p class="small-title cat-head-2">MOTOR : Attachment
																	Ratio(MTD)</p>
																<div id="motorar" style="height: 300px;"></div>
															</div>
														</div>
														<!-- /.col-sm-12 -->
													</div>
												</c:when>
											</c:choose>
											<!--end row -->
											<c:choose>
												<c:when test="${selectvalue=='sourcelead'}">
													<div class="row">
														<div class="col-sm-8">
															<div class="the-box">
																<p class="small-title cat-head-2">Source of Leads
																	(MTD)</p>
																<div class="row">
																	<div class="col-sm-4">
																		<table class="table table-bordered">
																			<thead>
																				<tr class="active">
																					<th>Point Of Sales</th>
																					<th>Total</th>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<td>Direct</td>
																					<td>${dashBoard.f_SOL_Direct}</td>
																				</tr>
																				<tr>
																					<td>CWP</td>
																					<td>${dashBoard.f_SOL_CWP}</td>
																				</tr>
																				<tr>
																					<td>Email</td>
																					<td>${dashBoard.f_SOL_MAIL}</td>
																				</tr>
																				<tr>
																					<td>Cyber Agent Website</td>
																					<td>${dashBoard.f_SOL_NonDirect}</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																	<div class="col-sm-8">
																		<div id="sol" style="height: 300px;"></div>
																	</div>
																</div>
															</div>
														</div>
												</c:when>
											</c:choose>
											<!-- /.col-sm-6 -->
											<c:choose>
												<c:when test="${selectvalue=='abandonment'}">
													<div class="row">
														<div class="col-sm-12">
															<div class="the-box">
																<p class="small-title cat-head-2">Abandonment vs
																	Follow up Rate(MTD)</p>
																<div id="afur" style="height: 300px; width: 100%;"></div>
															</div>
														</div>
													</div>

												</c:when>
											</c:choose>
										</div>
										<!--end row -->
									</div>



								</div>

							</div>
						</div>
						<!--row -->
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>
	<!-- /.container -->

	</div>
	<!-- /.page-content -->
	<!-- BEGIN FOOTER -->
	<jsp:include page="pageFooter.jsp" />

	<!-- END FOOTER -->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
	<script
		src="${pageContext.request.contextPath}/assets/js/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/bootstrap-select.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/js/bootstrap-tabcollapse.js"></script>

	<script
		src="${pageContext.request.contextPath}/assets/plugins/retina/retina.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/backstretch/jquery.backstretch.min.js"></script>


	<!-- PLUGINS -->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/prettify/prettify.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/icheck/icheck.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/mask/jquery.mask.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/summernote/summernote.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/to-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/slider/bootstrap-slider.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/toastr/toastr.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/footable-3/js/footable.js"></script>

	<!-- C3 JS -->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/c3-chart/d3.v3.min.js"
		charset="utf-8"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/c3-chart/c3.min.js"></script>

	<!-- highcharts JS -->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/highcharts/highcharts.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/highcharts/modules/funnel.js"></script>

	<!-- KNOB JS -->
	<!--[if IE]>
        <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
	<script
		src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script
		src="${pageContext.request.contextPath}/assets/plugins/jquery-knob/knob.js"></script>

	<!-- MAIN APPS JS -->
	<script src="${pageContext.request.contextPath}/assets/js/apps.js"></script>




	<script type="text/javascript">
$(document).ready(function() {
    
    // bar for Product Campaign Analysis (MTD)
    var chart = c3.generate({
    bindto: '#productcampaign',
    tooltip: {
        format: {
            value: function(value) {
                return d3.format(",.2f")(value)
            }
        }
    },
    data: {
    labels: true,
      columns: [
        ['Expenses', 32000],
        ['Revenue', 37000]
        
      ],
      types: {
        Expenses: 'bar',
        Revenue: 'bar'
      },
        
    },
        
    axis: {
      x: {
        type: 'categorized',
      }
      
    },
        
    });
    
    //chart for Renewal Retention(MTD)
    var chart = c3.generate({
    bindto: '#renewalretention',
    data: {
        // iris data from R
        columns: [
            ['WTC', 0],
            ['HOHH', 0],
            ['Motor', ${dashBoard.f_Renewal_Retention_Motor}],
            ['TermLife', 0],
        ],
        type : 'pie',
        colors: {
            WTC: '#337ab7',
            HOHH: '#d9534f',
            Motor: '#f0ad4e',
            TermLife: '#5cb85c',
        },
    },
        
    legend: {
        show: true
        },
        
    });
    
    
    // Actual vs Budget(MTD)
     var chart = c3.generate({
    bindto: '#actualbmtd',
        data: {
            x: 'x',
            columns: [
                ['x', 'WTC', 'HOHH', 'Motor', 'Term Life'],
                ['Actual', ${dashBoard.f_WTC_Sold_Amount_M}, ${dashBoard.f_HOHH_Sold_Amount_M}, ${dashBoard.f_Motor_Sold_Amount_M}, ${dashBoard.f_TL_Sold_Amount_M}],
                ['Budget', Math.round(${dashBoard.f_AVB_WTC_Budget}/12), Math.round(${dashBoard.f_AVB_HOHH_Budget}/12), Math.round(${dashBoard.f_AVB_Motor_Budget}/12), Math.round(${dashBoard.f_AVB_TL_Budget}/12)],
                
                 
              
                ],
            type: 'bar',
            labels: true
        },
        axis: {
            rotated: true,
          x: {
            type: 'categorized',
          }
        },
    });
    
    // Actual vs Budget(MTD)
    var chart = c3.generate({
    bindto: '#actualbytd',
        data: {
            x: 'x',
            columns: [
                ['x', 'WTC', 'HOHH', 'Motor', 'Term Life'],
                 ['Actual', ${dashBoard.f_WTC_Sold_Amount_Y}, ${dashBoard.f_HOHH_Sold_Amount_Y}, ${dashBoard.f_Motor_Sold_Amount_Y}, ${dashBoard.f_TL_Sold_Amount_Y}],
                ['Budget', ${dashBoard.f_AVB_WTC_Budget}, ${dashBoard.f_AVB_HOHH_Budget}, ${dashBoard.f_AVB_Motor_Budget}, ${dashBoard.f_AVB_TL_Budget}],
                ],
            type: 'bar',
            labels: true
        },
        axis: {
            rotated: true,
          x: {
            type: 'categorized',
          }
        },
    });
    
    // Actual vs Budget(MTD)
    var d = new Date();
    var n = d.getMonth();
    var points_output;
    n=n+1;
    //alert(n);
      if(n=8)
      { points_output = "${dashBoard.f_AgentPer_JAN},${dashBoard.f_AgentPer_FEB},${dashBoard.f_AgentPer_MAR},${dashBoard.f_AgentPer_APR},${dashBoard.f_AgentPer_MAY},${dashBoard.f_AgentPer_JUN},${dashBoard.f_AgentPer_JUL},${dashBoard.f_AgentPer_AUG},${dashBoard.f_AgentPer_SEP},${dashBoard.f_AgentPer_OCT},${dashBoard.f_AgentPer_NOV},${dashBoard.f_AgentPer_DEC}";
      //  alert(points_output)
      }else{
        alert("I am in else")
      }
    var chart = c3.generate({
    bindto: '#cyberap',
        data: {
            x: 'x',
            labels: true,
            columns: [
                ['x', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                ['Performance', ${dashBoard.f_AgentPer_JAN},${dashBoard.f_AgentPer_FEB},${dashBoard.f_AgentPer_MAR},${dashBoard.f_AgentPer_APR},${dashBoard.f_AgentPer_MAY},${dashBoard.f_AgentPer_JUN},${dashBoard.f_AgentPer_JUL},${dashBoard.f_AgentPer_AUG} ,${dashBoard.f_AgentPer_SEP}/*,${dashBoard.f_AgentPer_OCT},${dashBoard.f_AgentPer_NOV},${dashBoard.f_AgentPer_DEC} */],
                ],
        },
        axis: {
          x: {
            type: 'categorized',
          }
        },
    });
    
    // MOTOR : Attachment Ratio(MTD)
    var chart = c3.generate({
    bindto: '#motorar',
        data: {
            x: 'x',
            columns: [
                ['x', 
                'Passenger Legal Liability', 
                'Liability for Acts of Negligence', 
                'Extended Flood Cover',
                'Basic Flood Cover',
                'NCD Relief',
                'Windscreen Coverage',
                'Vehicle Accessories',
                'NGV Gas',
                'Compensation for Assessed Repair Time',
                'ADD NAMED DRIVER',
                'Strike, Riot & Civil Commotion'],
                ['Performance', ${dashBoard.f_Motor_A_Ratio_01},${dashBoard.f_Motor_A_Ratio_02},
                ${dashBoard.f_Motor_A_Ratio_03},${dashBoard.f_Motor_A_Ratio_04},
                ${dashBoard.f_Motor_A_Ratio_05},${dashBoard.f_Motor_A_Ratio_06},
                ${dashBoard.f_Motor_A_Ratio_07},${dashBoard.f_Motor_A_Ratio_08},
                ${dashBoard.f_Motor_A_Ratio_09},${dashBoard.f_Motor_A_Ratio_10},
                ${dashBoard.f_Motor_A_Ratio_11}],
                ],
            type: 'bar',
            labels: true,
        },
        axis: {
          x: {
            type: 'categorized',
          }
        },
    });
    
    
    // Source of Leads (MTD)
    var chart = c3.generate({
    bindto: '#sol',
        data: {
            columns: [
                ['Direct', ${dashBoard.f_SOL_Direct}],
                ['CWP', ${dashBoard.f_SOL_CWP}],
                ['Email', ${dashBoard.f_SOL_MAIL}],
                ['Cyber Agent Website', ${dashBoard.f_SOL_NonDirect}],
                ],
            type: 'donut',
            labels: true,
        },
        axis: {
          x: {
            type: 'categorized',
          }
        },
        donut: {
            title: "Point Of Sale"
        }
    });
    
    // Abandonment vs Follow up Rate(MTD)
    var colors = ['#e7e7e6', '#ffbf00'];
    var chart = c3.generate({
    bindto: '#afur',
        data: {
            x: 'x',
            columns: [
                ['x', 'Abandonment', 'Follow up'],
                ['MTD',${dashBoard.f_Aband_TotalCount} , ${dashBoard.f_Aband_TotalSuccess}],
                ],
            type: 'bar',
            color: function (color, d) {
                return colors[d.index];
            },
            labels: true
        },
        axis: {
          x: {
            type: 'category',
            categories: ['cat1', 'cat2', 'cat3', 'cat4', 'cat5', 'cat6', 'cat7', 'cat8', 'cat9']
          }
        },
        legend: {
            show: false
        }
    });
    
  
      var chart = c3.generate({
          bindto: '#cmpt',
          data: {
         type: 'bar',
          
            labels: true,
            x: 'x',
            columns: [
                ['x', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'],
                 
              
            ],
            colors: {
                WTC: '#337ab7',
                HOHH: '#d9534f',
                Motor: '#f0ad4e',
                TermLife: '#5cb85c',
            },
          },
          axis: {
            x: {
            type: 'categorized',
            
            }
          }
        });

});
 
//Performance by Entity

  var dataMonth = [
    ['x', 'WTC', 'HOHH', 'Motor', 'Term Life'],
    ['EIB', ${dashBoard.f_Entity_WTC_TPS_PM}, ${dashBoard.f_Entity_HOHH_TPS_PM}, ${dashBoard.f_Entity_MI_TPS_PM}, ${dashBoard.f_Entity_TL_TPS_PM}],
    ['ETB', ${dashBoard.f_Entity_WTCT_TPS_PM}, ${dashBoard.f_Entity_HOHHT_TPS_PM}, ${dashBoard.f_Entity_MT_TPS_PM}, ${dashBoard.f_Entity_TLT_TPS_PM}],
    ];

    var dataYear = [
    ['x', 'WTC', 'HOHH', 'Motor', 'Term Life'],
    ['EIB', ${dashBoard.f_Entity_WTC_TPS_PY}, ${dashBoard.f_Entity_HOHH_TPS_PY}, ${dashBoard.f_Entity_MI_TPS_PY}, ${dashBoard.f_Entity_TL_TPS_PY}],
    ['ETB', ${dashBoard.f_Entity_WTCT_TPS_PY}, ${dashBoard.f_Entity_HOHHT_TPS_PY}, ${dashBoard.f_Entity_MT_TPS_PY}, ${dashBoard.f_Entity_TLT_TPS_PY}],
    ];

 
$(function () {

    // Initial chart
    var chart = c3.generate({
        bindto: '#entity',
        data: {
            x: 'x',
            columns: dataMonth,
            type: 'bar',
            labels: true,
        },
        axis: {
            rotated: true,
          x: {
            type: 'categorized',
          }
        },
    }); 

    // Redraw chart depending on which option is selected
    $("#DataType").change(function (evt) {
        var timeSelection = eval($("#DataType").val());
        var chart = c3.generate({
            bindto: '#entity',
            data: {
                x: 'x',
                columns: timeSelection,
                type: 'bar',
                labels: true,
            },
            axis: {
                rotated: true,
              x: {
                type: 'categorized',
              }
            },
        });
    });

});


//Product Cross-Sell Ratio(MTD)
    var CrossSell = [
    ['x', 'WTC', 'HOHH', 'Motor', 'Term'],
    ['WTC', ${dashBoard.f_CrossSale_WTC_Count}, ${dashBoard.f_CrossSale_WTC_HOHH_Count}, ${dashBoard.f_CrossSale_WTC_MI_Count}, ${dashBoard.f_CrossSale_WTC_TL_Count}],
    ['HOHH', ${dashBoard.f_CrossSale_HOHH_WTC_Count}, ${dashBoard.f_CrossSale_HOHH_Count}, ${dashBoard.f_CrossSale_HOHH_MI_Count}, ${dashBoard.f_CrossSale_HOHH_TL_Count}],
    ['Motor', ${dashBoard.f_CrossSale_MI_WTC_Count}, ${dashBoard.f_CrossSale_MI_HOHH_Count}, ${dashBoard.f_CrossSale_MI_Count}, ${dashBoard.f_CrossSale_MI_TL_Count}],
    ['Term Life', ${dashBoard.f_CrossSale_TL_WTC_Count}, ${dashBoard.f_CrossSale_TL_HOHH_Count}, ${dashBoard.f_CrossSale_TL_MI_Count}, ${dashBoard.f_CrossSale_TL_Count}],
    ];

    var CrossUp = [
    ['x', 'DPPA','CAPS'],
    ['Total Premium/Contribution [RM]', ${dashBoard.f_Upsale_DPPA_Amount},${dashBoard.f_Upsale_CAPS_Amount}],
    ];

$(function () {

    // Initial chart
    var chart = c3.generate({
        bindto: '#CrossRatio',
        data: {
            x: 'x',
            rows: CrossSell,
            type: 'bar',
            labels: true,
            colors: {
                WTC: '#337ab7',
                HOHH: '#d9534f',
                Motor: '#f0ad4e',
                Term: '#5cb85c',
            },
            names: {
                WTC: 'WTC',
                HOHH: 'HOHH',
                Motor: 'Motor',
                Term: 'Term Life'
            }
        },
        axis: {
          x: {
            type: 'categorized',
          }
        },
    });

    // Redraw chart depending on which option is selected
    $("#Datacross").change(function (evt) {
        var timeSelection = eval($("#Datacross").val());
        var chart = c3.generate({
            bindto: '#CrossRatio',
            data: {
                x: 'x',
                rows: timeSelection,
                type: 'bar',
                labels: true,
                colors: {
                    WTC: '#337ab7',
                    HOHH: '#d9534f',
                    Motor: '#f0ad4e',
                    Term: '#5cb85c',
                },
                names: {
                    WTC: 'WTC',
                    HOHH: 'HOHH',
                    Motor: 'Motor',
                    Term: 'Term Life'
                }
            },
            axis: {
              x: {
                type: 'categorized',
              }
            },
        });
    });

});

// funnel Sale  // dataf (WTC) datata(HOHH)

var WTC = [
        ['Step 1', 40],
        ['Step 2', 20],
        ['Step 3', 60],
    ];

var HOHH = [
        ['Step 1', 87],
        ['Step 2', 12],
        ['Step 3', 17],
    ];

var Motor = [
        ['Step 1', ${dashBoard.f_Fun_MI_Lstep1_Percentage}],
        ['Step 2', ${dashBoard.f_Fun_MI_Lstep2_Percentage}],
        ['Step 3', ${dashBoard.f_Fun_MI_Lstep3_Percentage}],
    ];

var TermLife = [
        ['Step 1', 66],
        ['Step 2', 54],
        ['Step 3', 11],
    ];
    
// $(function () {
//    $('#funnel2').highcharts({
//        chart: {
//            type: 'funnel',
//            marginRight: 100
//        },
//        title: false,
//        plotOptions: {
//            series: {
//                dataLabels: {
//                    enabled: true,
//                    format: '<b>{point.percentage:.1f}% </b> <b>{point.name}</b>',
//                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
//                    softConnector: true
//                },
//                neckWidth: '30%',
//                neckHeight: '25%',
//                width: '64%',
//                //-- Other available options
//                // height: pixels or percent
//                // width: pixels or percent
//            },
//            area: {
//                stacking: 'percent',
//                lineColor: '#ffffff',
//                lineWidth: 1,
//                marker: {
//                    lineWidth: 1,
//                    lineColor: '#ffffff'
//                }
//            }
//        },
//        legend: {
//            enabled: false
//        },
//        series: [{
//            name: 'Sales Funnel',
//            data: dataf
//        }]
//    });
// });

$(function () {

    // Initial chart
    var funel =$("#funel2").highcharts({
        chart: {
            type: 'funnel',
            marginRight: 100
        },
        title: false,
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    labels: true,
                    format: '<b>{point.percentage:.1f}% </b> <b>{point.name}</b>',
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                    softConnector: true
                },
                neckWidth: '20%',
                neckHeight: '20%',
                width: '60%',
                //-- Other available options
                // height: pixels or percent
                // width: pixels or percent

            },
            area: {
                stacking: 'percent',
                lineColor: '#ffffff',
                lineWidth: 1,
                marker: {
                    lineWidth: 1,
                    lineColor: '#ffffff'
                }
            }
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Sales Funnel',
            data: Motor
        }]
    });

    // Redraw chart depending on which option is selected
    $("#FunnelType").change(function (evt) {
        var timeSelection2 = eval($("#FunnelType").val());
        var funel =$("#funel2").highcharts({
            chart: {
                type: 'funnel',
                marginRight: 100
            },
            title: false,
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        labels: true,
                        format: '<b>{point.percentage:.1f}% </b> <b>{point.name}</b>',
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                        softConnector: true
                    },
                    neckWidth: '20%',
                    neckHeight: '20%',
                    width: '60%',
                    //-- Other available options
                    // height: pixels or percent
                    // width: pixels or percent

                },
                area: {
                    stacking: 'percent',
                    lineColor: '#ffffff',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#ffffff'
                    }
                }
            },
            legend: {
                enabled: false
            },
            series: [{
                name: 'Sales Funnel',
                data: timeSelection2
            }]
        });
    });

});


</script>

	<script>
		$(document).ready(function() {
      
			$('#admin-datatable-second_admin').DataTable({
				"order" : [],
				dom : 'Bfrtip',
				title : ' Agent List',

				buttons : [ 'copy', 'csv', 'print', {
					extend : 'excelHtml5',
					title : 'Abandonment Dashboard'
				}, {
					extend : 'pdfHtml5',
					title : 'Abandonment Dashboard'
				} ]
			});

			$('#admin-datatable-second_admin1').DataTable({
				"order" : [],
				dom : 'Bfrtip',
				title : ' Agent List',

				buttons : [ 'copy', 'csv', 'print', {
					extend : 'excelHtml5',
					title : 'Abandonment Dashboard'
				}, {
					extend : 'pdfHtml5',
					title : 'Abandonment Dashboard'
				} ]
			});

			$('#admin-datatable-second_admin2').DataTable({
				"order" : [],
				dom : 'Bfrtip',
				title : ' Agent List',

				buttons : [ 'copy', 'csv', 'print', {
					extend : 'excelHtml5',
					title : 'Abandonment Dashboard'
				}, {
					extend : 'pdfHtml5',
					title : 'Abandonment Dashboard'
				} ]
			});

			// $('#date1').datepicker();
			$("#fromdate").datepicker({
				format : 'dd/mm/yyyy',
				autoclose : true,
			}).on('changeDate', function(ev) {
				$(this).datepicker('hide');
			});
			$("#todate").datepicker({
				format : 'dd/mm/yyyy',
				autoclose : true,
			}).on('changeDate', function(ev) {
				$(this).datepicker('hide');
			});
	
		});
	</script>
	<script type="text/javascript">
		window.onload = function() {
		 CanvasJS.addColorSet("greenShades",
                [//colorSet Array

                "#2F4F4F",
                "#008080",
                "#2E8B57",
                "#3CB371",
                "#90EE90"                
                ]);
			var chart = new CanvasJS.Chart("chartContainer",
					
					{
		    zoomEnabled: true,
            zoomType: "xy",
            animationEnabled: true,
            animationDuration: 1000,
            exportEnabled: true,
						theme : "theme1",
				     	
						title : {
							text : ""
						},
				 		 toolTip:{   
			content: "{x} of the Month: RM {y}"      
		},  
			/*  toolTip: {
			shared: true,
			contentFormatter: function (e) {
				var content = " ";
				for (var i = 0; i < e.entries.length; i++) {
					content += e.entries[i].dataSeries.name + " " + "<strong>" + e.entries[i].dataPoint.y + "</strong>";
					content += "<br/>";
				}
				return content;
			}
		},  */

						axisY : [ {
						title: "Premium Amount (RM)",
							
						},
						],
						axisX : [ {
						 title: "Dates of the Month",
							interval: 1,
						},
						],
						data : [
								{
									type : "line", //change type to bar, line, area, pie, etc
									showInLegend : true,
									 lineColor: "darkblue",
									  markerColor: "darkblue",
									legendText : "WTC",
									indexLabel: "{y}",
									name: "Follow up Count",
									   indexLabelFontSize: 10,
									indexLabelFontFamily: "tahoma",
                                    indexLabelPlacement: "outside",
									dataPoints : [
	                                 <%=(String) request.getAttribute("DataListWTCPerformance")%> 
	                           	]
								},
								
								{
									type : "line",
									axisYIndex : 2,
									name: "Premium/Contribution Follow Up (RM)",
									
									showInLegend : true,
									lineColor: "orange",
									  markerColor: "orange",
									   indexLabelFontSize: 10,
									legendText : "Motor",
									indexLabel: "{y}",
									dataPoints : [
	<%=(String) request.getAttribute("DataListMIPerformance")%> 
		]
								} ,
								{
									type : "line",
									axisYIndex : 2,
									name: "Premium/Contribution Follow Up (RM)",
									
									showInLegend : true,
									lineColor: "green",
									  markerColor: "green",
									   indexLabelFontSize: 10,
									legendText : "Term Life",
									indexLabel: "{y}",
									dataPoints : [
	<%=(String) request.getAttribute("DataListTLPerformance")%> 
		]
								} ,
									{
									type : "line",
									axisYIndex : 2,
									name: "Premium/Contribution Follow Up (RM)",
									
									showInLegend : true,
									lineColor: "Red-Violet",
									  markerColor: "Red-Violet",
									   indexLabelFontSize: 10,
									legendText : "Cancer Care",
									indexLabel: "{y}",
									dataPoints : [
	<%=(String) request.getAttribute("DataListCCPerformance")%> 
		]
								},
								{
									type : "line",
									axisYIndex : 2,
									
									name: "Total Revenue (RM)",
									showInLegend : true,
									lineColor: "red",
									markerColor: "red",
									indexLabelFontSize: 12,
									legendText : "HOHH",
									indexLabel: "{y}",
									dataPoints : [
	<%=(String) request.getAttribute("DataListHOHHPerformance")%> 
		]
								},
								
								],
								
						legend : {
							cursor : "pointer",
							itemclick : function(e) {
								if (typeof (e.dataSeries.visible) === "undefined"
										|| e.dataSeries.visible) {
									e.dataSeries.visible = false;
								} else {
									e.dataSeries.visible = true;
								}
								chart.render();
							}
						}
					});

			chart.render();
		salesfunnelInfo("Motor");
        salesfunneltotalcount(value);
		}

		
	</script>



	<script type="text/javascript">
     function salesfunneltotalcount(value) 
     {
	   var totalcount="<p class='cat-head text-center'> 0 Successful Transactions </p>";
	 if(value=='Motor'){
	 
	    totalcount="";
	 
		totalcount = "<p class='cat-head text-center'> " + ${dashBoard.f_Fun_MI_Success_Tran} + " Successful Transactions</p>";
	 
     }
	
	 $('#SFcountBody').html(totalcount);
    
	 }



    function salesfunnelInfo(value) {
	
	salesfunneltotalcount(value);
	
	 var resp = "";
	 var tvalue1 = "0";
	 var tvalue2 = "0";
	 var tvalue3 = "0";
     var lvalue1 = "0";
	 var lvalue2 = "0";
	 var lvalue3 = "0";
     
     if(value=='Motor'){
     
     tvalue1=${dashBoard.f_Fun_MI_step1_count};
     tvalue2=${dashBoard.f_Fun_MI_step2_count};
     tvalue3=${dashBoard.f_Fun_MI_step3_count};
     lvalue1=${dashBoard.f_Fun_MI_Lstep1_count};
     lvalue2=${dashBoard.f_Fun_MI_Lstep2_count};
     lvalue3=${dashBoard.f_Fun_MI_Lstep3_count};
     
     }
     else{
     
     tvalue1=0;
     tvalue2=0;
     tvalue3=0;
     lvalue1=0;
     lvalue2=0;
     lvalue3=0;
     
     }
    
     resp = resp + "<tr><td>STEP 1</td><td>"+tvalue1+"</td><td>"+lvalue1+"</td></tr><tr><td>STEP 2</td><td>"+tvalue2+"</td><td>"+lvalue2+"</td></tr><tr><td>STEP 3</td><td>"+tvalue3+"</td><td>"+lvalue3+"</td></tr>";
		
	 $('#resultBody').html(resp);
    
    }

	</script>


	<!-- <script type="text/javascript">
         	$(document).ready(function() {
         	
         		$('#eib-pr').hide();
            	var productTypeSelected = $('#productTypeValSession').val();
				var productEntitySelected = $('#productEntityValSession').val();
				
				if(productTypeSelected =="MI" || productTypeSelected =="MT")
					{
					
					$('#mi-option').show();	
					
					}
				
				else{
					
					$('#mi-option').hide();	
					
				}
				
				
		    	});
		         
         
         	
   
        
        </script> -->





</body>
</html>