<%@ page import="java.io.*,java.util.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<link rel="stylesheet" href="assets/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/menu.css">
<script src="assets/js/jquery.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.js" type="text/javascript"></script>
</head>

<body>
	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->


	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Cyber Agent Management</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="the-box">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Operator Product Management</h4>
														</div>
													</div>
												</div>
												<div class="gap gap-mini"></div>
												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="col-sm-6">

														<div class="form-group">
															<label class="col-sm-3 control-label">Product : </label>
															<div class="col-sm-7">
																<select class="form-control" id="sel1">
																	<option>Select Product</option>
																	<option>Imtiyaz</option>
																	<option>Hayan</option>
																	<option>Farooq</option>
																	<option>Vijay</option>
																	<option>Shailendra</option>
																</select>
															</div>
															</select>
														</div>
														<br>
														<div class="form-group">
															<label class="col-sm-3 control-label">Type : </label>
															<div class="col-sm-9">
																<select class="form-control" required name="agentType"
																	id="agentType"
																	onblur="checkOnBlur(this,'Please Select Your Insurance Type');"
																	onchange="checkOnChange(this);">

																	<option value="D">Direct</option>
																	<option value="ND">Non Direct</option>
																</select>

															</div>
														</div>

													</div>
													<div class="col-sm-6">


														<div class="form-group">
															<label class="col-sm-3 control-label">Entity : </label>
															<div class="col-sm-7">
																<select class="form-control" id="sel1">

																	<option>EIB</option>
																	<option>ETB</option>

																</select>
															</div>
															<div class="col-sm-2"></div>
														</div>
													</div>
												</div>
												<a href="agent-list-of-perator.html"
													class="btn btn-warning btn-sm pull-right"><i
													class="fa fa-edit"></i> Submit</a>

												<!-- End warning color table -->
											</div>
										</div>
										<div class="content-inner">
											<div class="the-box">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Select Product</h4>
														</div>
													</div>
												</div>
												<div class="gap gap-mini"></div>


												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="table-responsive text-center">
																<table
																	class="table table-striped table-warning table-hover">
																	<thead>
																		<tr>
																			<th style="width: 30px;"></th>
																			<th>Product</th>
																			<th>Type</th>
																			<th>Entity</th>
																			<th>Operator Code</th>
																			<th>Discount<br>(%)
																			</th>
																			<th>Commission<br>(%)
																			</th>
																		</tr>
																	</thead>
																	<tbody>
																		<tr>
																			<td><label><input type="checkbox"
																					value=""></label></td>
																			<td>Motor Insurance</td>
																			<td>Non Direct</td>
																			<td>EIB</td>
																			<td><input type="text" class="form-control"
																				id="usr"></td>
																			<td><input type="text" class="form-control"
																				id="usr"></td>
																			<td><input type="text" class="form-control"
																				id="usr"></td>
																		</tr>
																		<tr>
																			<td><label><input type="checkbox"
																					value=""></label></td>
																			<td>Car Accident Protection Special<br>(CAPS)
																			</td>
																			<td>Non Direct</td>
																			<td>EIB</td>
																			<td><input type="text" class="form-control"
																				id="usr"></td>
																			<td><input type="text" class="form-control"
																				id="usr"></td>
																			<td><input type="text" class="form-control"
																				id="usr"></td>
																		</tr>
																		<tr>
																			<td><label><input type="checkbox"
																					value=""></label></td>
																			<td>HouseOwner/HouseHolder</td>
																			<td>Non Direct</td>
																			<td>EIB</td>
																			<td><input type="text" class="form-control"
																				id="usr"></td>
																			<td><input type="text" class="form-control"
																				id="usr"></td>
																			<td><input type="text" class="form-control"
																				id="usr"></td>
																		</tr>
																		<tr>
																			<td><label><input type="checkbox"
																					value=""></label></td>
																			<td>World Traveller Care</td>
																			<td>Non Direct</td>
																			<td>EIB</td>
																			<td><input type="text" class="form-control"
																				id="usr"></td>
																			<td><input type="text" class="form-control"
																				id="usr"></td>
																			<td><input type="text" class="form-control"
																				id="usr"></td>
																		</tr>
																		<tr>
																			<td><label><input type="checkbox"
																					value=""></label></td>
																			<td>Ezy-Life Secure</td>
																			<td>Non Direct</td>
																			<td>EIB</td>
																			<td><input type="text" class="form-control"
																				id="usr"></td>
																			<td><input type="text" class="form-control"
																				id="usr"></td>
																			<td><input type="text" class="form-control"
																				id="usr"></td>
																		</tr>
																	</tbody>
																</table>

															</div>
															<!-- /.table-responsive -->
															<a href="agent-list-of-perator-edit.html"
																class="btn btn-warning btn-sm pull-right"><i
																class="fa fa-edit"></i> Submit</a>
														</div>
														<!-- /.the-box -->

													</div>
													<!-- End warning color table -->

												</div>

											</div>

											<div class="the-box">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>Product Referral Url</h4>
														</div>
													</div>
												</div>
												<div class="gap gap-mini"></div>
												<div class="col-sm-6">
													<div class="form-horizontal info-meor">
														<div class="form-group">
															<label class="col-sm-3 control-label">Motor
																Insurance</label>
															<div class="col-sm-7">
																<input type="text" class="form-control">
															</div>
															<div class="col-sm-2">
																<a href="agent-list-of-perator.html"
																	class="btn btn-warning btn-sm"><i
																	class="fa fa-edit"></i> Preview</a>
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">HOHH</label>
															<div class="col-sm-7">
																<input type="text" class="form-control">
															</div>
															<div class="col-sm-2">
																<a href="agent-list-of-perator.html"
																	class="btn btn-warning btn-sm"><i
																	class="fa fa-edit"></i> Preview</a>
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">WTC</label>
															<div class="col-sm-7">
																<input type="text" class="form-control">
															</div>
															<div class="col-sm-2">
																<a href="agent-list-of-perator.html"
																	class="btn btn-warning btn-sm"><i
																	class="fa fa-edit"></i> Preview</a>
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">Ezy-Life
																Secure</label>
															<div class="col-sm-9">
																<input type="text" class="form-control"> <a
																	href="agent-list-of-perator.html"
																	class="btn btn-warning btn-sm"><i
																	class="fa fa-edit"> </i> Preview</a>
															</div>
														</div>



													</div>
												</div>

											</div>

										</div>
										<!--content inner -->
									</div>
									<!--col-sm-12 -->
								</div>
								<div class="col-sm-12">
									<div class="text-right">
										<input class="btn btn-warning btn-sm"
											onClick="submitRegistration();" type="submit" value="Submit" />
									</div>
								</div>
								<!--col-sm-12 -->
							</div>
							<!--row -->
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>
	<!-- /.container -->
	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->

	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
            <div id="back-top">
                <a href="#top"><i class="fa fa-chevron-up"></i></a>
            </div>
            -->
	<!-- END BACK TO TOP -->


	<!--
            ===========================================================
            END PAGE
            ===========================================================
            -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->

	<script>
        $('#fine-uploader-gallery').fineUploader({
            template: 'qq-template-gallery',
            request: {
                endpoint: '/server/uploads'
            },
            thumbnails: {
                placeholders: {
                    waitingPath: '/source/placeholders/waiting-generic.png',
                    notAvailablePath: '/source/placeholders/not_available-generic.png'
                }
            },
            validation: {
                allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
            }
        });
    </script>

	<script type="text/javascript">
            function submitRegistration(){
                result=true;
                if(!$('#referenceNo').val().length) {
                    $('#msg_refno').removeClass('hidden');
                    $('#msg_refno').addClass('visible');
                    $('#referenceNo').focus();
                    result=false;
                }
                if(!$('#agentCode').val().length) {
                    $('#msg_agentcode').removeClass('hidden');
                    $('#msg_agentcode').addClass('visible');
                    $('#agentCode').focus();
                    result=false;
                }
                if(!$('#datepicker1').val().length) {
                    $('#msg_date').removeClass('hidden');
                    $('#msg_date').addClass('visible');
                    $('#datepicker1').focus();
                    result=false;
                }
                if(!$('#annualSalesTarget').val().length) {
                    $('#msg_salesTarget').removeClass('hidden');
                    $('#msg_salesTarget').addClass('visible');
                    $('#annualSalesTarget').focus();
                    result=false;
                }
                if($('#agentCategory').index()== 0) {
                    $('#msg_agentcat').removeClass('hidden');
                    $('#msg_agentcat').addClass('visible');
                    $('#agentCategory').focus();
                    result=false;
                }
                if($('#insuranceType').index()== 0) {
                    $('#msg_instype').removeClass('hidden');
                    $('#msg_instype').addClass('visible');
                    $('#insuranceType').focus();
                    result=false;
                }
                if($('#agentStatus').index()== 0) {
                    $('#msg_agentstatus').removeClass('hidden');
                    $('#msg_agentstatus').addClass('visible');
                    $('#agentStatus').focus();
                    result=false;
                }
                if(!$('#agentAliasName').val().length) {
                    $('#msg_agentaliasname').removeClass('hidden');
                    $('#msg_agentaliasname').addClass('visible');
                    $('#agentAliasName').focus();
                    result=false;
                }
                if(!$('input[name=products]:checked').val()) {                     
                    $('#msg_productselect').removeClass('hidden');
                    $('#msg_productselect').addClass('visible');
                    $('#products').focus();
                    result=false;
                }
                if(!$('input[name=registrationDocument]:checked').val()) {                     
                    $('#msg_agentregdoc').removeClass('hidden');
                    $('#msg_agentregdoc').addClass('visible');
                    $('#registrationDocument').focus();
                    result=false;
                }
                if(!$('#agentName').val().length) {
                    $('#msg_agentname').removeClass('hidden');
                    $('#msg_agentname').addClass('visible');
                    $('#agentName').focus();
                    result=false;
                }
                if($('#idType').index()== 0) {
                    $('#msg_idtype').removeClass('hidden');
                    $('#msg_idtype').addClass('visible');
                    $('#idType').focus();
                    result=false;
                }
                if(!$('#idNo').val().length) {
                    $('#msg_idno').removeClass('hidden');
                    $('#msg_idno').addClass('visible');
                    $('#idNo').focus();
                    result=false;
                }
                if($('#phoneType').index()== 0) {
                    $('#msg_phonetype').removeClass('hidden');
                    $('#msg_phonetype').addClass('visible');
                    $('#phoneType').focus();
                    result=false;
                }
                if(!$('#phoneNo').val().length) {
                    $('#msg_phoneno').removeClass('hidden');
                    $('#msg_phoneno').addClass('visible');
                    $('#phoneNo').focus();
                    result=false;
                }
                if(!$('#email').val().length) {
                    $('#msg_email').removeClass('hidden');
                    $('#msg_email').addClass('visible');
                    $('#email').focus();
                    result=false;
                }
                if(!$('#address1').val().length) {
                    $('#msg_address').removeClass('hidden');
                    $('#msg_address').addClass('visible');
                    $('#address1').focus();
                    result=false;
                }
                if(!$('#postcode').val().length) {
                    $('#msg_postcode').removeClass('hidden');
                    $('#msg_postcode').addClass('visible');
                    $('#postcode').focus();
                    result=false;
                }
             //   if($('#city').index()== 0) {
              //      $('#msg_city').removeClass('hidden');
              ///      $('#msg_city').addClass('visible');
               //     $('#city').focus();
               //     result=false;
              //  }
             //   if($('#state').index()== 0) {
                //    $('#msg_state').removeClass('hidden');
                //    $('#msg_state').addClass('visible');
                 //   $('#state').focus();
                 //   result=false;
               // }
            }

            $('#referenceNo').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_refno').removeClass('visible');
                    $('#msg_refno').addClass('hidden');
                }
            });
            $('#agentCode').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_agentcode').removeClass('visible');
                    $('#msg_agentcode').addClass('hidden');
                }
            });
            $('#datepicker1').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_date').removeClass('visible');
                    $('#msg_date').addClass('hidden');
                }
            });
            $('#annualSalesTarget').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_salesTarget').removeClass('visible');
                    $('#msg_salesTarget').addClass('hidden');
                }
            });
            $('#agentCategory').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_agentcat').removeClass('visible');
                    $('#msg_agentcat').addClass('hidden');
                }
            });
            $('#insuranceType').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_instype').removeClass('visible');
                    $('#msg_instype').addClass('hidden');
                }
            });
            $('#agentStatus').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_agentstatus').removeClass('visible');
                    $('#msg_agentstatus').addClass('hidden');
                }
            });
            $('#agentAliasName').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_agentaliasname').removeClass('visible');
                    $('#msg_agentaliasname').addClass('hidden');
                }
            });
            $('#products').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_productselect').removeClass('visible');
                    $('#msg_productselect').addClass('hidden');
                }
            });
            $('#registrationDocument').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_agentregdoc').removeClass('visible');
                    $('#msg_agentregdoc').addClass('hidden');
                }
            });
            $('#agentName').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_agentname').removeClass('visible');
                    $('#msg_agentname').addClass('hidden');
                }
            });
            $('#idType').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_idtype').removeClass('visible');
                    $('#msg_idtype').addClass('hidden');
                }
            });
            $('#idNo').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_idno').removeClass('visible');
                    $('#msg_idno').addClass('hidden');
                }
            });
            $('#phoneType').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_phonetype').removeClass('visible');
                    $('#msg_phonetype').addClass('hidden');
                }
            });
            $('#phoneNo').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_phoneno').removeClass('visible');
                    $('#msg_phoneno').addClass('hidden');
                }
            });
            $('#email').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_email').removeClass('visible');
                    $('#msg_email').addClass('hidden');
                }
                if (validateEmail(this.value)) {
                    $('#msg_validmail').removeClass('visible');
                    $('#msg_validmail').addClass('hidden');
                } 
            });
            $('#address1').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_address').removeClass('visible');
                    $('#msg_address').addClass('hidden');
                }
            });
            $('#postcode').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_postcode').removeClass('visible');
                    $('#msg_postcode').addClass('hidden');
                }
            });
            $('#city').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_city').removeClass('visible');
                    $('#msg_city').addClass('hidden');
                }
            });
            $('#state').keyup(function() {
                if($.trim(this.value).length) {
                    $('#msg_state').removeClass('visible');
                    $('#msg_state').addClass('hidden');
                }
            });

            

            $(document).ready(function() {
                $('#postcode').blur(function(e) {
                    if (validatePhone('postcode')) {
                       //alert('valid');
                        $('#msg_postcode').removeClass('visible');
                        $('#msg_postcode').addClass('hidden');
                    }
                    else {
                    //  alert('invalid');
                        $('#msg_postcode').removeClass('hidden');
                        $('#msg_postcode').addClass('visible');
                    }
                });
            });
            // Force Numeric Only 
            jQuery.fn.ForceNumericOnly = function() {
                return this.each(function() {
                    $(this).keydown(function(e) {
                        var key = e.charCode || e.keyCode || 0;
                        // allow backspace, tab, delete, arrows, letters, numbers and keypad numbers ONLY
                        return (
                            key == 8 || 
                            key == 9 ||
                            key == 46 ||
                            key == 32 ||
                            (key >= 48 && key <= 57));                
                    })
                })
            };
            // Function Numbers
            //for (var i=1;i<=counter;i++){
            /*$("#email").validateEmail();*/
           
           // $("#agentCode").ForceNumericOnly();
            $("#annualSalesTarget").ForceNumericOnly();
            $("#idNo").ForceNumericOnly();
            $("#phoneNo").ForceNumericOnly();
            $("#postcode").ForceNumericOnly();
            //}

            /////////////// Fetching State from Postcode ////////////////  
            /*function fetch_state() { 
                //alert(document.getElementById("postcode").value);
                var obj;
                var arr = [];
                var formData = {postcode:document.getElementById("postcode").value}; //Array 
                if (document.getElementById("postcode").value.length != 0 && document.getElementById("postcode").value.length >= 4) {
                    $.ajax({
                        type: "post", 
                        url: "/getonline/sites/REST/controller/TravellerController/fetch_State",
                        data: formData,
                        success: function (data) {
                            if(data) {
                                document.getElementById("state").value=data;
                            }

                        },
                        error: function (request, status, error) {
                            alert(request.responseText);
                        }
                    }); 
                } 
            }*/
            function validateEmail(sEmail) {
                var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if (filter.test(sEmail)) {
                    return true;
                }
                else {
                    return false;
                }
            }

            function validatePhone(postcode) {
                var a = document.getElementById(postcode).value;
                var filter = /^[0-9-+]+$/;
                if (filter.test(a)) {
                    return true;
                }
                else {
                    return false;
                }
            }

            function checkOnBlur(me, message) {
                var  e_id = $(me).attr('id');
                if (!(me.value).length) {
                    var exist= document.getElementById(e_id+ '_e');
                    if (exist == null) {
                        var htmlString="";
                        htmlString+='<label id='+ e_id+ '_e' + '  ' + 'class="error">'+ message + '</label>';
                        $(htmlString).insertAfter("#"+ e_id);
                        $('#'+ e_id).focus();
                    }
                }
            }
                //////////////////////////////////////////////////////////////////////////////////////////////  
            /*function checkKeyUp(me) {
                var  e_id = $(me).attr('id');
                if($.trim(me.value).length) {
                    var exist= document.getElementById(e_id+ '_e');
                    if (exist != null) {
                        $('#'+ e_id + '_e').remove();
                    }
                }
            }*/
            //////////////////////////////////////////////////////////////////////////////////////////////
            function checkOnChange(me) {
                var  e_id = $(me).attr('id');
                if($.trim(me.value).length) {
                    var exist= document.getElementById(e_id+ '_e');
                    if (exist != null) {
                        $('#'+ e_id + '_e').remove();
                    }
                }
            }

            /*var js = jQuery.noConflict();
            js(document).ready(function() {
                js("#registration").validate({
                    rules: {
                        phoneType: {
                            required: true
                        }
                    },
                    messages: {
                        phoneType: {
                            required: "You must select a timezone"
                        }
                    }

                });
            });*/

        </script>
</body>
</html>