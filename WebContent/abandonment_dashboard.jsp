<%@ page import="java.util.List"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="com.etiqa.model.report.TransactionalReport"%>
<%@ page import="java.io.*,java.util.*"%>
<%@ page import="javax.servlet.*,java.text.*"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/js/jquery.dataTables.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/buttons.dataTables.min.css" rel="stylesheet"
	type="text/css">
<!-- <link href="assets/js/bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/dataTables.bootstrap.min.css" rel="stylesheet"
	type="text/css"> -->
<link href="assets/js/buttons.bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link rel="stylesheet" href="assets/css/toastr.min.css">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}
</style>


<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}
</style>
<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<!-- <link href="assets/css/bootstrap.min.css" rel="stylesheet"> -->
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet"
	href="assets/plugins/datepicker/datepicker.min.css">
<!-- PLUGINS CSS -->
<link href="assets/plugins/owl-carousel/owl.carousel.min.css"
	rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.theme.min.css"
	rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.transitions.min.css"
	rel="stylesheet">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<!-- <link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" /> -->


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->


<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

form#agentE, form#agentD {
	display: inline-block;
}

form#agentE input[type=submit], form#agentD input[type=submit] {
	padding: 5px;
}
</style>


</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Abandonment DashBoard</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">


												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">

															<div class="content-inner">
																<div class="the-box">
																	<form name="rptForm" id="rptForm" method="post"
																		onsubmit="return validateForm()">
																		<%-- <input type="hidden" name="CSRFToken"  value="<%=session.getAttribute("CSRFToken") %>" />
	 --%>
																		<div class="col-sm-6">
																			<div class="form-horizontal">
																				<div class="form-group">
																					<label class="col-sm-4 control-label">From
																						Date<span class="text-danger">*</span>
																					</label>
																					<div class="col-sm-8">
																						<input type="text" readonly id="fromdate" required
																							value="${fromdate}" required name="fromdate"
																							class="form-control">
																					</div>
																				</div>
																			</div>
																		</div>

																		<div class="col-sm-6">
																			<div class="form-horizontal">
																				<div class="form-group">
																					<label class="col-sm-4 control-label">To
																						Date <span class="text-danger">*</span>
																					</label>
																					<div class="col-sm-8">
																						<input type="text" readonly name="todate" required
																							id="todate" value="${todate}"
																							class="form-control">
																					</div>
																				</div>
																			</div>
																		</div>

																		<div class="col-sm-6">
																			<div class="form-horizontal">
																				<div class="form-group">
																					<label class="col-sm-4 control-label">
																						Product <span class="text-danger">*</span>
																					</label>
																					<div class="col-sm-8">

																						<select class="form-control" name="product"
																							id="product">
																							<option value="ALL" selected>-View All-</option>
																							<option value="Motor Insurance">Motor
																								Insurance</option>
																							<option value="Motor Takaful">Motor
																								Takaful</option>
																							<option value="Easy Life">Easy Life</option>
																							<option value="World Travel Care">World
																								Travel Care</option>
																							<option value="World Travel Care Takaful">World
																								Travel Care Takaful</option>
																							<option value="House Owner House Hold">House
																								Owner House Hold</option>
																							<option value="House Owner House Hold Takaful">House
																								Owner House Hold Takaful</option>
																						</select>



																					</div>
																				</div>
																			</div>
																		</div>


																		<div class="col-sm-12 text-right">
																			<input value="Search" class="btn btn-default"
																				type="submit" />
																		</div>
																	</form>
																</div>




															</div>


															<div class="col-sm-12">

																<!--No record found Invisible  -->
																<div id="noRecordFound"
																	class="alert alert-danger fade in alert-dismissable hidediv">
																	<!--   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X—</button> -->
																	<strong><i class="fa fa-exclamation"></i> No
																		record found</strong>
																</div>
																<!--No record found Invisible  -->
																<div class="content-inner">
																	<div class="the-box static">
																		<div class="title-second">
																			<div class="sub">
																				<h4>
																					Daily Progress Details for
																					<c:forEach items="${myDataListHeader}" var="Header">
																									${Header.product_indicator}
																							</c:forEach>

																					<c:if test="${not empty fromdate}"> From ${fromdate} - ${todate} </c:if>
																				</h4>
																			</div>
																		</div>

																		<div>
																			<div class="form-horizontal" id="MyTable">
																				<div id="dvData">
																					<!-- class="table table-striped table-hover" -->
																					<!--  <button id="exportButton" class="btn btn-lg btn-danger clearfix"><span class="fa fa-file-excel-o"></span> Export to Excel</button>-->
																					<table id="admin-datatable-second_admin"
																						width="500px"
																						class="table table-striped table-hover">
																						<thead>
																							<tr>
																								<th>QuotationDate</th>
																								<th>Follow&nbsp;upDate</th>
																								<th>Product</th>
																								<th>Follow&nbsp;upCount</th>
																								<th>SuccessfulFollow&nbsp;up</th>
																								<th>Prem-/Contr-Follow&nbsp;up</th>
																								<th>Successful(%)</th>
																								<!-- <th>TotalPolicy&nbsp;Count</th>
																							 	<th>TotalPrem-/Contr&nbsp;(RM)</th> -->



																							</tr>
																						</thead>
																						<tbody>
																							<c:forEach items="${myDataList}" var="Daily">
																								<tr>
																									<td align='center'>${Daily.transactiondate}</td>
																									<td align='center'>${Daily.abandonementdate}</td>
																									<td align='center'>${Daily.product_indicator}</td>
																									<td align='center'>${Daily.total_count}</td>
																									<td align='center'>${Daily.total_count_success}</td>
																									<td align='center'>${Daily.total_month_rm}</td>
																									<td align='center'>${Daily.percentsuccess}</td>
																									<%--  <td align='center'>${Daily.policycount}</td>
																								 	<td align='center'>${Daily.totalamount}</td> --%>

																								</tr>
																							</c:forEach>
																						</tbody>
																					</table>


																					<div class="title-second">
																						<div class="sub">
																							<h4>
																								Weekly Progress Details For
																								<c:forEach items="${myDataListHeader}"
																									var="Header">
																									${Header.product_indicator}
																							</c:forEach>
																								<c:if test="${not empty fromdate}"> From ${fromdate} - ${todate} </c:if>
																							</h4>
																						</div>
																					</div>

																					<table id="admin-datatable-second_admin1"
																						class="table table-striped table-hover">
																						<thead>
																							<tr>
																								<th>Follow&nbsp;up&nbsp;Date</th>
																								<th>Follow&nbsp;up&nbsp;Count</th>
																								<th>SuccessfulFollow&nbsp;up</th>
																								<th>Prem-/Contr-&nbsp;Follow&nbsp;up</th>
																								<th>Successful(%)</th>
																								<!--  <th>TotalPolicy&nbsp;Count</th>
																								<th>TotalPrem-/Contr(RM)</th> -->

																							</tr>
																						</thead>
																						<tbody>
																							<c:forEach items="${myDataListW}" var="Weekly">
																								<tr>
																									<td>Week ${Weekly.abandonementdate}
																										(${Weekly.start_week} - ${Weekly.end_week})</td>
																									<td align='center'>${Weekly.total_count}</td>
																									<td align='center'>${Weekly.total_count_success}</td>
																									<td align='center'>${Weekly.total_month_rm}</td>
																									<td align='center'>${Weekly.percentsuccess}</td>
																									<%-- <td align='center'>${Weekly.policycount}</td>
																								<td align='center'>${Weekly.totalamount}</td> --%>
																								</tr>
																							</c:forEach>
																						</tbody>
																					</table>

																					<div class="title-second">
																						<div class="sub">
																							<h4>
																								Monthly Progress Details For
																								<c:forEach items="${myDataListHeader}"
																									var="Header">
																									${Header.product_indicator}
																							</c:forEach>
																								<c:if test="${not empty fromdate}"> From ${fromdate} - ${todate} </c:if>
																							</h4>
																						</div>
																					</div>
																					<!-- <div id="MchartContainer" style="height: 350px; width: 100%;"></div> -->

																					<table id="admin-datatable-second_admin2"
																						class="table table-striped table-hover">
																						<thead>
																							<tr>
																								<th>Follow&nbsp;up&nbsp;Date</th>
																								<th>Follow&nbsp;up&nbsp;Count</th>
																								<th>SuccessfulFollow&nbsp;up</th>
																								<th>Prem-/Contr-Follow&nbsp;up</th>
																								<th>Successful(%)</th>
																								<!--  <th>TotalPolicy&nbsp;Count</th>
																								<th>TotalPrem-/Contr(RM)</th> -->

																							</tr>
																						</thead>
																						<tbody>
																							<c:forEach items="${myDataListM}" var="Monthly">
																								<tr>
																									<td align='center'>${Monthly.abandonementdate}</td>
																									<td align='center'>${Monthly.total_count}</td>
																									<td align='center'>${Monthly.total_count_success}</td>
																									<td align='center'>${Monthly.total_month_rm}</td>
																									<td align='center'>${Monthly.percentsuccess}</td>
																									<%-- <td align='center'>${Monthly.policycount}</td>
																								<td align='center'>${Monthly.totalamount}</td> --%>
																								</tr>
																							</c:forEach>
																						</tbody>
																					</table>
																					<!-- <div id="curve_chart"
																						style="width: 900px; height: 500px"></div> 
 -->

																					<!--   <div id="columnchart_material" style="width: 800px; height: 500px;"></div> -->
																					<!-- <script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script> -->

																					<script type="text/javascript"
																						src="${pageContext.request.contextPath}/assets/js/canvasjs.min.js"></script>
																					<script type="text/javascript"
																						src="${pageContext.request.contextPath}/assets/js/jquery.canvasjs.min.js"></script>

																					<div id="chartContainer"
																						style="height: 330px; width: 100%;"></div>

																					<center>
																						<table id="admin-datatable-second_admin1"
																							border='1' width='100%'>
																							<thead>
																								<tr>
																									<th><font size='1'>&nbsp;Follow
																											up&nbsp;Date</font></th>
																									<th><font size='1'>&nbsp;Follow
																											up&nbsp;Count</font></th>
																									<th><font size='1'>&nbsp;Successful&nbsp;Follow
																											up</font></th>
																									<th><font size='1'>&nbsp;Prem-/Contr-&nbsp;Follow
																											up(RM)</font></th>
																									<th><font size='1'>&nbsp;Success(%)</th>
																									<th><font size='1'>&nbsp;Total&nbsp;Policy&nbsp;Count</font></th>
																									<th><font size='1'>&nbsp;Total&nbsp;Prem-/Contr(RM)</font></th>






																								</tr>
																							</thead>
																							<tbody>
																								<c:forEach items="${myDataListW}"
																									var="WeeklyGraph">
																									<tr>
																										<td>&nbsp;Week
																											${WeeklyGraph.abandonementdate}
																											(${WeeklyGraph.start_week} -
																											${WeeklyGraph.end_week})</td>
																										<td align='center'>${WeeklyGraph.total_count}</td>
																										<td align='center'>${WeeklyGraph.total_count_success}</td>
																										<td align='center'>${WeeklyGraph.total_month_rm}</td>

																										<td align='center'>${WeeklyGraph.percentsuccess}</td>
																										<td align='center'>${WeeklyGraph.policycount}</td>
																										<td align='center'>${WeeklyGraph.totalamount}</td>
																									</tr>
																								</c:forEach>
																							</tbody>
																						</table>
																					</center>

																				</div>

																			</div>
																		</div>
																	</div>
																</div>
																<!--                                                    
                                                    -->
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>


											</div>
										</div>
									</div>
									<!--row -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->

	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%--    <jsp:include page="siteFooter.jsp" />   --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->


	<!-- JAVA Script Goes Here -->

	<script
		src="<c:url value="/resources/assets/js/jquery-2.2.4.min.js" />"></script>
	<!--  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
	<script src="assets/js/apps.js"></script>
	<script src="assets/plugins/retina/retina.min.js"></script>
	<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>


	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<!-- 	<script
		src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script> -->
	<script type="text/javascript" src="assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript" src="assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/buttons.print.min.js"></script>
	<script src="assets/js/buttons.colVis.min.js"></script>
	<script src="assets/js/toastr.min.js"></script>

	<script src="plugins/jquery-knob/knob.js"></script>

	<script type="text/javascript">
		window.onload = function() {
		 CanvasJS.addColorSet("greenShades",
                [//colorSet Array

                "#2F4F4F",
                "#008080",
                "#2E8B57",
                "#3CB371",
                "#90EE90"                
                ]);
			var chart = new CanvasJS.Chart("chartContainer",
					
					{
		    zoomEnabled: true,
            zoomType: "xy",
            animationEnabled: true,
            animationDuration: 1000,
            exportEnabled: true,
						theme : "theme1",
				     	
						title : {
							text : "Successful Abandonment Email Follow up by Week"
						},
				 		 toolTip:{   
			content: "Week {x}: RM {y}"      
		},  
			/*  toolTip: {
			shared: true,
			contentFormatter: function (e) {
				var content = " ";
				for (var i = 0; i < e.entries.length; i++) {
					content += e.entries[i].dataSeries.name + " " + "<strong>" + e.entries[i].dataPoint.y + "</strong>";
					content += "<br/>";
				}
				return content;
			}
		},  */

						axisY : [ {
						title: "Premium Amount (RM)",
							
						},
						],
						axisX : [ {
						 title: "Week",
							interval: 1,
						},
						],
						data : [
								{
									type : "line", //change type to bar, line, area, pie, etc
									showInLegend : true,
									 lineColor: "blue",
									  markerColor: "blue",
									legendText : "FollowUp Count",
									indexLabel: "{y}",
									name: "Follow up Count",
									   indexLabelFontSize: 10,
									indexLabelFontFamily: "tahoma",
                                    indexLabelPlacement: "outside",
									dataPoints : [
	                                     <%=(String) request.getAttribute("myDataListGH")%>
	                           	]
								},
								
								{
									type : "line",
									axisYIndex : 2,
									name: "Premium/Contribution Follow Up (RM)",
									
									showInLegend : true,
									lineColor: "darkblue",
									  markerColor: "darkblue",
									   indexLabelFontSize: 10,
									legendText : "Premium/Contribution Follow Up (RM)",
									indexLabel: "{y}",
									dataPoints : [
	<%=(String) request.getAttribute("myDataListRM")%>
		]
								} ,
								{
									type : "line",
									axisYIndex : 2,
									
									name: "Total Revenue (RM)",
									showInLegend : true,
									lineColor: "Green",
									markerColor: "Green",
									indexLabelFontSize: 12,
									legendText : "Total Revenue (RM)",
									indexLabel: "{y}",
									dataPoints : [
	<%=(String) request.getAttribute("myDataListRMT")%>
		]
								},
								
								],
								
						legend : {
							cursor : "pointer",
							itemclick : function(e) {
								if (typeof (e.dataSeries.visible) === "undefined"
										|| e.dataSeries.visible) {
									e.dataSeries.visible = false;
								} else {
									e.dataSeries.visible = true;
								}
								chart.render();
							}
						}
					});

			chart.render();
		}
	</script>


	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->

	<script>
		$(document).ready(function() {
      
			$('#admin-datatable-second_admin').DataTable({
				"order" : [],
				dom : 'Bfrtip',
				title : ' Agent List',

				buttons : [ 'copy', 'csv', 'print', {
					extend : 'excelHtml5',
					title : 'Abandonment Dashboard'
				}, {
					extend : 'pdfHtml5',
					title : 'Abandonment Dashboard'
				} ]
			});

			$('#admin-datatable-second_admin1').DataTable({
				"order" : [],
				dom : 'Bfrtip',
				title : ' Agent List',

				buttons : [ 'copy', 'csv', 'print', {
					extend : 'excelHtml5',
					title : 'Abandonment Dashboard'
				}, {
					extend : 'pdfHtml5',
					title : 'Abandonment Dashboard'
				} ]
			});

			$('#admin-datatable-second_admin2').DataTable({
				"order" : [],
				dom : 'Bfrtip',
				title : ' Agent List',

				buttons : [ 'copy', 'csv', 'print', {
					extend : 'excelHtml5',
					title : 'Abandonment Dashboard'
				}, {
					extend : 'pdfHtml5',
					title : 'Abandonment Dashboard'
				} ]
			});

			// $('#date1').datepicker();
			$("#fromdate").datepicker({
				format : 'dd/mm/yyyy',
				autoclose : true,
			}).on('changeDate', function(ev) {
				$(this).datepicker('hide');
			});
			$("#todate").datepicker({
				format : 'dd/mm/yyyy',
				autoclose : true,
			}).on('changeDate', function(ev) {
				$(this).datepicker('hide');
			});

			/* var agentAddedMessage = "<c:out value="${agentAddedMessage}"/>";
			// show when the button is clicked
			if (agentAddedMessage.length) {
				toastr.success(agentAddedMessage);
			}

			var agentDeletedMessage = "<c:out value="${agentDeletedMessage}"/>";
			// show when the button is clicked
			if (agentDeletedMessage.length) {
				toastr.success(agentDeletedMessage);
			}

			var agentUpdatedMessage = "<c:out value="${agentUpdatedMessage}"/>";
			// show when the button is clicked
			if (agentUpdatedMessage.length) {
				toastr.success(agentUpdatedMessage);
			}
			var agentUpdatedMessage = "<c:out value="${agentUpdatedMessageE}"/>";
			// show when the button is clicked
			if (agentUpdatedMessage.length) {
				toastr.error(agentUpdatedMessage);
			}
			 */
		});
	</script>


	<script>
	
		 function validateForm() {
        	   var x = document.forms["rptForm"]["fromdate"].value;
        	   var y = document.forms["rptForm"]["todate"].value;
        	   
        	   if ( x == null || x == "" ) {
        		  
        	      alert("Please choose Date From");
        	      document.forms["rptForm"]["fromdate"].focus();
        	      
        	      return false;
        	   }
        	   else if ( y == null || y == "" ) {
        		  
        	      alert("Please choose Date To");
        	      document.forms["rptForm"]["todate"].focus();
        	      return false;
        	   }
        	   
        	   return true;
        	  
        	}
	</script>






</body>
</html>