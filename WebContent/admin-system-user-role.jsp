<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.etiqa.model.report.TransactionalReport"%>
<%@ page import="com.spring.VO.AdminUsersProfile"%>
<jsp:useBean id="obj" class="com.etiqa.utils.SecurityUtil" />

<% java.text.DateFormat df=new java.text.SimpleDateFormat("dd/MM/yyyy"); %>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}

.dataTable>thead>tr>th[class*="sort"]::after {
	display: none
}
</style>

</head>

<body>
	<%
        
       
   	 DecimalFormat formatter = new DecimalFormat("#,###.00");
        
 if (request.getMethod().equals("GET")){
			
			session.setAttribute("ProductType", "");		
			session.setAttribute("Status", "");
			session.setAttribute("PolicyCertificateNo", "");
			session.setAttribute("NRIC","");
			session.setAttribute("ReceiptNo", "");
			session.setAttribute("dateFrom","");
			session.setAttribute("dateTo","");
			session.setAttribute("DateFrom", "");
			session.setAttribute("DateTo","");
			session.setAttribute("PlateNo","");
			session.setAttribute("ProductEntity","");
			
 }
			
        int totalRecord = 0;
        int recordPerPage =10;
        int pageCount =0;
        int startRow=0;
        int endRow=0;
        //temporary 
        endRow=recordPerPage;
        
       int start=0;       
       int end=0;
       double lastPage=0;
       String textColor="";
       String status="";
       String totalAmount="0.00";
       String transactionStatus="";
       String lastStep="";
       String nextStep="";
       String reason="";
       int i=0;
       int rowLine=0;
       String txtColor="";
       double netAmt= 0.00 ;
       double nettAmount=10.00;
       
       
       
        
        %>



	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">System Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>User Roles &amp; Permission</h4>

														</div>
													</div>


												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">

														<div class="table-responsive" id="MyTable">
															<table
																class="table table-striped table-warning table-hover"
																id="admin-datatable-second_admin">
																<thead>

																	<tr>
																		<th align="center">No</th>
																		<th align="center">Staff ID</th>
																		<th align="center">Full Name</th>
																		<th align="center">Created By</th>
																		<th align="center">Created Date</th>
																		<th align="center">Active</th>

																	</tr>
																</thead>
																<tbody>



																	<%  
                                                                if (request.getAttribute("data") != null) {
														            List<AdminUsersProfile> list = (List<AdminUsersProfile>) request.getAttribute("data");
														            
														            if(!list.isEmpty()) {
														            	int countVal=1;
														                for(AdminUsersProfile adminUsersProfile : list) {
														        %>

																	<tr>
																		<td align="center"><%=countVal%></td>
																		<td align="center"><%=obj.stripXSS(adminUsersProfile.getUsername())%></td>
																		<td align="center"><%=obj.stripXSS(adminUsersProfile.getFullname())%></td>
																		<td align="center"><%=obj.stripXSS(adminUsersProfile.getCreatedby())%></td>
																		<td align="center"><%=adminUsersProfile.getCreateddate()%></td>
																		<td align="center"><%=obj.stripXSS(adminUsersProfile.getIsactive())%></td>

																	</tr>
																	<%
                                                                    i++;
														                }
														                }
														            }
														        %>

																</tbody>
															</table>
														</div>
														<!-- /.table-responsive -->
													</div>
													<!-- /.the-box -->
												</div>
												<!-- End warning color table -->
											</div>
										</div>
									</div>
									<!--row -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->

	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="siteFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	<!-- JAVA Script Goes Here -->







</body>
</html>