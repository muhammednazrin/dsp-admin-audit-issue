$(document).ready(function() {
		var chart = new CanvasJS.Chart("chartContainer", {

            animationEnabled: true,
			axisX:{
				interval: 1,
				gridThickness: 0,
				labelFontSize: 10,
				labelFontStyle: "normal",
				labelFontWeight: "normal",
				labelFontFamily: "Lucida Sans Unicode"

			},
            axisY2:{
//				interlacedColor: "rgba(1,77,101,.2)",
//				gridColor: "rgba(1,77,101,.1)"
                gridThickness: 0,

			},
            
            colorSet:  "colorSet2",

			data: [
			{     
				type: "bar",
                name: "companies",
				axisYType: "secondary",
//				color: "#77787b",
                indexLabel: "{y}",
                percentFormatString: "#0.##",
				dataPoints: [
				
				{y: 55, label: "No"  },
				{y: 56, label: "Yes"  }
				]
			}
			
			]
		});

chart.render();
});


