<%@ page language="java" contentType="text/html; charset=ISO-8859-1" 
 pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>ETIQA</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" >
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="no-cache">
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="no-store">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--  <script src="js/datepicker/jquery.min.js"></script>
        <script src="js/datepicker/jquery-ui.min.js"></script>-->
<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet" href="plugins/datepicker/datepicker.min.css">
<!-- PLUGINS CSS -->
<link href="plugins/owl-carousel/owl.carousel.min.css" rel="stylesheet">
<link href="plugins/owl-carousel/owl.theme.min.css" rel="stylesheet">
<link href="plugins/owl-carousel/owl.transitions.min.css"
	rel="stylesheet">
<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="css/owl.css" rel="stylesheet">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-select.css" rel="stylesheet">
<link href="plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="js/all.min.css" />
<!--<link href="css/style.css" rel="stylesheet">-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}
</style>
<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}
</style>

</head>
<body>
	  <%
		if (session != null) {
			if (session.getAttribute("user") != null) {
				String name = (String) session.getAttribute("user");
			} else {
				response.sendRedirect("admin-login.jsp");
			}
		}
	%>  
	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!-- header -->
		<jsp:include page="Header.jsp" />
		<!-- end header -->
		<!-- header second-->
		<div class="header-second">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 hidden-xs">
						<!-- Begin breadcrumb -->
						<ol class="breadcrumb">
							<li><a href="#"><i class="fa fa-home"></i></a></li>
							
							<li class="active">Bank Account Update</li>
						</ol>
						<!-- End breadcrumb -->
					</div>

				</div>
			</div>
		</div>
		<!-- header second-->
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">

					<jsp:include page="cwpmenu.jsp" />

					<div class="col-sm-9 col-md-10 white-back">
						 <div class="row">
                               <div>
                                    <!-- breadcrum -->
                                    <div class="breadcrum-grey">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <!-- Begin breadcrumb -->
                                                <ol class="breadcrumb">
                                                    <li><a href="#fakelink">Bank Account Update</a></li>
                                                </ol>
                                                <!-- End breadcrumb -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- breadcrum -->
                                </div>
                                <div class="col-sm-11">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="title">
                                               <div class="sub">
                                                   <h4>Bank Account Update</h4>
                                               </div>
                                           </div>
                                            <form action="Accinfo" method="post" id="form2" name="form2">
                                            <div class="content-inner">
                                                <div class="the-box">
                                                    <div class="col-sm-6">
                                                        <div class="form-horizontal">
                                                          
                                                          <div class="form-group">
                                                            <label class="col-sm-3 control-label">NRIC Number</label>
                                                            <div class="col-sm-9">
                                                              <c:if test="${! empty Accinfoinsert}">
                                                                <p class="form-control-static">${Accinfoinsert.ic_number}
                                                                <input type="hidden" id="icnumber" name="icnumber" value="${Accinfoinsert.ic_number}"></p>
                                                               </c:if>
                                                              <c:if test="${empty Accinfoinsert}" >
                                                                <input type="text" id="icnumber" name="icnumber" id="icnumber" class="form-control" placeholder="NRIC" value=""></p>
                                                                <span id="msg_icnumber" class="hidden"
																					style="color: red; text-align: left">Please
																					enter your ic number</span>
                                                               </c:if>
                                                            </div>
                                                          </div>
                                                           <div class="form-group">
                                                            <label class="col-sm-3 control-label">Policy Number</label>
                                                            <div class="col-sm-9">
                                                           <c:if test="${! empty Accinfoinsert}">
                                                               <p class="form-control-static">${Accinfoinsert.policyno}
                                                               <input type="hidden" id="policyno" name="policyno" value="${Accinfoinsert.policyno}">
                                                               </p> 
                                                             </c:if>
                                                             <c:if test="${empty Accinfoinsert}" >
                                                              
                                                                <select  class="form-control" name="policyno" id="policyno">
                                                                  <c:if test="${empty policy}">
                                                                  
                                                                     <option value="" selected="selected">Select</option>
                                                                  
                                                                  </c:if>
                                                                           <c:forEach items="${policylist}" var="elementyr"> 
                                                                            <option
                                                                              <c:if test="${policy eq elementyr.policyno}" >selected</c:if> 
																									value="<c:out value="${elementyr.policyno}"/>">
																				
																								<c:out value="${elementyr.policyno}" />
																						</option>
																				</c:forEach> 
                                                                            </select>
                                                                             
                                                              
                                                             </c:if>
                                                            </div>
                                                          </div>
                                                           <div class="form-group">
                                                            <label class="col-sm-3 control-label">Customer Name</label>
                                                            <div class="col-sm-9">
                                                                 <input type="text" class="form-control" placeholder="name" name="custname" id="custname" value="${Accinfoinsert.updateby}">
                                                             <span id="msg_custname" class="hidden"
																					style="color: red; text-align: left">Please
																					select your customer name</span>
                                                            </div>
                                                          </div>
                                                          <div class="form-group">
                                                            <label class="col-sm-3 control-label">Customer Email</label>
                                                            <div class="col-sm-9">
                                                                 <input type="text" class="form-control" placeholder="email" name="email" id="email" value="${Accinfoinsert.email}">
                                                             <span id="msg_email" class="hidden"
																					style="color: red; text-align: left">Please
																					select your email</span>
                                                            </div>
                                                          </div>
                                                            <div class="form-group">
                                                            <label class="col-sm-3 control-label">Phone Number</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control" placeholder="Number" name="phno" id="phno" value="${Accinfoinsert.contactno}">
                                                             <span id="msg_phno" class="hidden"
																					style="color: red; text-align: left">Please
																					select your phone number</span>
                                                            </div>
                                                          </div>
                                                        
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-horizontal">
                                                            <div class="form-group">
                                                            <label class="col-sm-3 control-label">Bank Name</label>
                                                            <div class="col-sm-9">
                                                                <select class="form-control" name="bankname">
                                                                    <option value="Maybank"  <c:if test="${Accinfoinsert.bank_name} eq 'Maybank'}" >selected</c:if> >Maybank</option>
                                                                    <option value="CIMB" <c:if test="${Accinfoinsert.bank_name} eq 'CIMB'}" >selected</c:if>>CIMB</option>
                                                                </select>
                                                            </div>
                                                          </div>
                                                          <div class="form-group">
                                                            <label class="col-sm-3 control-label">Bank Account</label>
                                                            <div class="col-sm-9">
                                                               <input type="text" class="form-control" placeholder="Number" name="bankacc" id="bankacc" value="${Accinfoinsert.acc_no}">
                                                             <span id="msg_bankacc" class="hidden"
																					style="color: red; text-align: left">Please
																					enter your Branch Account</span>
                                                            </div>
                                                          </div>
                                                        <!-- <div class="form-group">
                                                            <label class="col-sm-3 control-label">Branch Name</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control" placeholder="Branch Name" name="branchname" id="branchname" value="${Accinfoinsert.branch_name}">
                                                              <span id="msg_branchname" class="hidden"
																					style="color: red; text-align: left">Please
																					enter your Branch Name</span>
                                                            </div>
                                                          </div>
                                                           <div class="form-group">
                                                            <label class="col-sm-3 control-label">Branch Code</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control" placeholder="Number" name="branchcode" id="branchcode" value="${Accinfoinsert.branch_code}">
                                                           	<span id="msg_branchcode" class="hidden"
																					style="color: red; text-align: left">Please
																					enter your Branch Code</span>
                                                            </div>
                                                          </div>  --> 
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 text-right">
                                                       
                                                      
                                                        <button class="btn btn-default"  onClick="return validateForm();">Update</button>
                                                    </div>
                                                </div>
                                            </div>
                                            </form>
                                        </div>
                                        
                                    </div>
                               </div>
                            </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>
	<!-- /.container -->
	</div>
	<!-- /.page-content -->
	<jsp:include page="Footer.jsp" />
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->
	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
		<script src="js/jquery-2.2.4.min.js"></script>
		<script src="js/jquery-2.2.4.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-select.min.js"></script>
	<script src="js/bootstrap-tabcollapse.js"></script>

	<script src="plugins/retina/retina.min.js"></script>
	<script src="plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="plugins/backstretch/jquery.backstretch.min.js"></script>


	<!-- PLUGINS -->
	<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="plugins/prettify/prettify.js"></script>
	<script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="plugins/chosen/chosen.jquery.min.js"></script>
	<script src="plugins/icheck/icheck.min.js"></script>
	<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="plugins/mask/jquery.mask.min.js"></script>
	<script src="plugins/validator/bootstrapValidator.min.js"></script>
	<script src="plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="plugins/datatable/js/jquery.highlight.js"></script>
	<script src="plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="plugins/summernote/summernote.min.js"></script>
	<script src="plugins/markdown/markdown.js"></script>
	<script src="plugins/markdown/to-markdown.js"></script>
	<script src="plugins/markdown/bootstrap-markdown.js"></script>
	<script src="plugins/slider/bootstrap-slider.js"></script>
	<script src="plugins/toastr/toastr.js"></script>
	<script src="plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="plugins/placeholder/jquery.placeholder.js"></script>
	<script src="plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script type="text/javascript" src="js/shieldui-all.min.js"></script>
	<script type="text/javascript" src="js/jszip.min.js"></script>


	<!--  Table Export -->



	<!-- KNOB JS -->
	<!--[if IE]>
        <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
	<script src="plugins/jquery-knob/jquery.knob.js"></script>
	<script src="plugins/jquery-knob/knob.js"></script>

	<!-- MAIN APPS JS -->
	<script src="js/apps.js"></script>
	<script>
	 function validateForm(){
		 
		  result=true;
		    if(document.getElementById("icnumber").value=="") {
             $('#msg_icnumber').removeClass('hidden');
             $('#msg_icnumber').addClass('visible');
             $('#icnumber').focus();
                result=false;
           // return false;
         }
           if(document.getElementById("custname").value=="") {
             $('#msg_custname').removeClass('hidden');
             $('#msg_custname').addClass('visible');
             $('#custname').focus();
                result=false;
           // return false;
         }
		      if(document.getElementById("email").value=="") {
             $('#msg_email').removeClass('hidden');
             $('#msg_email').addClass('visible');
             $('#email').focus();
                result=false;
           // return false;
         }
          if(document.getElementById("phno").value=="") {
             $('#msg_phno').removeClass('hidden');
             $('#msg_phno').addClass('visible');
             $('#phno').focus();
                result=false;
            //return false;
         }
         if(document.getElementById("bankacc").value=="") {
             $('#msg_bankacc').removeClass('hidden');
             $('#msg_bankacc').addClass('visible');
             $('#bankacc').focus();
                result=false;
           //return false;
         }
          
         if (result==true) {
         	$('#form2').submit();   
         }
		     return result;
		 }
	</script>

</body>
</html>