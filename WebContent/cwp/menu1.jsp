<%@ page language="java" contentType="text/html; charset=ISO-8859-1" 
 pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ page import="java.io.IOException"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.text.ParseException"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.List"%>
<%@ page import="com.cwp.dao.RolesManagement_DAO"%>
<%@ page import="com.cwp.rpt.UserRole"%>

<%-- <%
	String strusername = "faheem";
	RolesManagement_DAO rdao = new RolesManagement_DAO();
	List<UserRole> releaseDataList = rdao.getUsersRolesMenu("faheem");
%> 
 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>ETIQA</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" >
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="no-cache">
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="no-store">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--  <script src="js/datepicker/jquery.min.js"></script>
        <script src="js/datepicker/jquery-ui.min.js"></script>-->
<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet" href="plugins/datepicker/datepicker.min.css">
<!-- PLUGINS CSS -->
<link href="plugins/owl-carousel/owl.carousel.min.css" rel="stylesheet">
<link href="plugins/owl-carousel/owl.theme.min.css" rel="stylesheet">
<link href="plugins/owl-carousel/owl.transitions.min.css"
	rel="stylesheet">
<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="css/owl.css" rel="stylesheet">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-select.css" rel="stylesheet">
<link href="plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="plugins/all.min.css" />
<!--<link href="css/style.css" rel="stylesheet">-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}
</style>
<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}
</style>
</head>
<body>
	<%
		List<UserRole> releaseDataList = null;
		if (session != null) {
			if (session.getAttribute("username") != null) {
				String name = (String) session.getAttribute("username");
				System.out.println("user" +name);
				RolesManagement_DAO rdao = new RolesManagement_DAO();
				releaseDataList = rdao.getUsersRolesMenu(name);
			} else {
				response.sendRedirect("admin-login.jsp");
			}
		}
	%>

	<div class="col-sm-3 col-md-2 fluid-menu">
		<div class="sidebar-left sidebar-nicescroller admin">


			<!-- desktop menu -->
			<ul class="sidebar-menu admin">
				<%  if(releaseDataList!=null){			
					for (int i = 0; i < releaseDataList.size(); i++) {
				%>

				<%=releaseDataList.get(i).getURL()%>

				<%
					}}
				%>


				<!-- <li><a href="login"> <span class="icon-sidebar icon update"></span>
						<i class="fa fa-angle-left chevron-icon-sidebar"></i> Update Phone
						Number



				</a></li>
				<li><a href="getReportData"> <span
						class="icon-sidebar icon notifi-manag"></span> Rejected Customers
				</a></li>
				<li><a href="getUserReportData"> <span
						class="icon-sidebar icon notifi-manag"></span> Registered
						Customers
				</a></li>

				<li><a href="getUserFailedAgentData"> <span
						class="icon-sidebar icon notifi-manag"></span>Invalid Customer ID

				</a></li>
				<li><a href="getFailedMobileData"> <span
						class="icon-sidebar icon notifi-manag"></span>Customer & Agent
						Mobile No
				</a></li>
				<li><a href="PollsReportData"> <span
						class="icon-sidebar icon notifi-manag"></span> Polls Report
				</a></li>
				 	<li><a href="#"> <span
										class="icon-sidebar icon notifi-manag"></span> Audit Trail for
										Admin
								</a></li>
								<li><a href="getLogSummaryData"> <span
										class="icon-sidebar icon notifi-manag"></span> Audit Trail for
										Customers
								</a></li>
				<li><a href="getUserACExpireData"> <span
						class="icon-sidebar icon notifi-manag"></span>AC Expired
				</a></li>

				<li><a href="getLastActivityData"> <span
						class="icon-sidebar icon notifi-manag"></span>Login to Logout
						Duration
				</a></li>
				<li><a href="EditQuestionsController?action=listuser"> <span
						class="icon-sidebar icon notifi-manag"></span>Add Security
						Question
				</a></li>
				<li><a href="NotificationController?action=listuser"> <span
						class="icon-sidebar icon notifi-manag"></span>Notifications
				</a></li>
				<li><a href="usermanagement"> <span
						class="icon-sidebar icon notifi-manag"></span>Add New User
				</a></li>

				<li><a href="UserController?action=listuser"> <span
						class="icon-sidebar icon notifi-manag"></span>User Management
				</a></li>
				<li><a href="./"> <span
						class="icon-sidebar icon notifi-manag"></span> Logout
				</a></li> -->
			</ul>
		</div>
		<!-- /.sidebar-left -->
	</div>

	<script src="plugins/retina/retina.min.js"></script>
	<script src="plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="plugins/backstretch/jquery.backstretch.min.js"></script>

	<!-- PLUGINS -->
	<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="plugins/prettify/prettify.js"></script>
	<script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="plugins/chosen/chosen.jquery.min.js"></script>
	<script src="plugins/icheck/icheck.min.js"></script>
	<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="plugins/mask/jquery.mask.min.js"></script>
	<script src="plugins/validator/bootstrapValidator.min.js"></script>
	<script src="plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="plugins/datatable/js/jquery.highlight.js"></script>
	<script src="plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="plugins/summernote/summernote.min.js"></script>
	<script src="plugins/markdown/markdown.js"></script>
	<script src="plugins/markdown/to-markdown.js"></script>
	<script src="plugins/markdown/bootstrap-markdown.js"></script>
	<script src="plugins/slider/bootstrap-slider.js"></script>
	<script src="plugins/toastr/toastr.js"></script>
	<script src="plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="plugins/placeholder/jquery.placeholder.js"></script>
	<script src="plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script type="text/javascript" src="js/shieldui-all.min.js"></script>
	<script type="text/javascript" src="js/jszip.min.js"></script>

</body>
</html>
