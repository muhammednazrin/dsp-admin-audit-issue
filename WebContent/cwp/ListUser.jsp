<%@ page language="java" contentType="text/html; charset=ISO-8859-1" 
 pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>ETIQA</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" >
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="no-cache">
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="no-store">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--  <script src="js/datepicker/jquery.min.js"></script>
        <script src="js/datepicker/jquery-ui.min.js"></script>-->
<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet" href="plugins/datepicker/datepicker.min.css">
<!-- PLUGINS CSS -->
<link href="plugins/owl-carousel/owl.carousel.min.css" rel="stylesheet">
<link href="plugins/owl-carousel/owl.theme.min.css" rel="stylesheet">
<link href="plugins/owl-carousel/owl.transitions.min.css"
	rel="stylesheet">
<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="css/owl.css" rel="stylesheet">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-select.css" rel="stylesheet">
<link href="plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="plugins/all.min.css" />
<!--<link href="css/style.css" rel="stylesheet">-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}
</style>
<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}
</style>

<script language="javascript">
	function editRecord(id) {
		window.location.href = "editrecord/" + id;
	}
	function deleteRecord(id) {
		window.location.href = "deleteUser/" + id;
	}
</script>

</head>
<body>
   <%-- <%
		if (session != null) {
			if (session.getAttribute("user") != null) {
				String name = (String) session.getAttribute("user");
				} else {
				response.sendRedirect("admin-login.jsp");
			}
			
			}
	%> --%> 
	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!-- header -->
		<jsp:include page="Header.jsp" />
		<!-- end header -->
		<!-- header second-->
		<div class="header-second">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 hidden-xs">
						<!-- Begin breadcrumb -->
						<ol class="breadcrumb">
							<li><a href="#"><i class="fa fa-home"></i></a></li>
							
							<li class="active">User List</li>
						</ol>
						<!-- End breadcrumb -->
					</div>
 
				</div>
			</div>
		</div>
		<!-- header second-->
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					
					<jsp:include page="cwpmenu.jsp" />
					
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#">User Management - Users List</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11">
								<div class="row">
									<div class="col-sm-12">
										<div class="title">
											<div class="sub">
												<h4>User Management - Users List</h4>
											</div>
										</div>
										<div class="content-inner">
											<div class="the-box">
										 	<table id="admin-datatable-second"
													class="table table-striped table-hover">
													<thead>
														<tr>
															<th data-type="html">User Name</th>
															<th data-type="html">Creation Date</th>
															<th data-type="html">Created By</th>
															<th data-type="html">Is Active</th>
															<th colspan=2 data-type="html">Action</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach items="${cwpusers}" var="mlistUsers">
															<tr>
																<td>${mlistUsers.username}</td>
																<td><c:out value="${mlistUsers.createddate}" /></td>
																<td><c:out value="${mlistUsers.createdby}" /></td>
																<td><c:out value="${mlistUsers.isactive}" /></td>
																<td><%-- <a
																	href="UserController?action=edit&id=<c:out value="${mlistUsers.ID}"/>">Update</a>
															 | --%> <a
																		href="UserRoleController?action=listuser&id=<c:out value="${mlistUsers.ID}"/>">Assign
																			Role</a> 
																</td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
												<!--	<p>
														<a href="UserController?action=insert">Add User</a>
													</p>-->
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<!--No record found Invisible  -->
										<div id="noRecordFound"
											class="alert alert-danger fade in alert-dismissable hidediv">
											<!--   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X—</button> -->
											<strong><i class="fa fa-exclamation"></i> No record
												found</strong>
										</div>
								
										<!--No record found Invisible  -->
										<div class="content-inner">
											<div class="the-box static">
												
												<div class="col-sm-12">
													<div class="form-horizontal" id="MyTable">
														<div id="dvData">
															<!--  <button id="exportButton" class="btn btn-lg btn-danger clearfix"><span class="fa fa-file-excel-o"></span> Export to Excel</button>-->

															<table id="exportTable"
																class="table table-striped table-hover"
																style="display: none;">
																<thead>
																	<tr>
																		<th data-type="html">User Name</th>
																		<th data-type="html">CREATION DATE</th>
																		<th data-type="html">CREATED BY</th>
																		<th data-type="html">IS ACTIVE</th>

																	</tr>
																</thead>
																<tbody>
																	<c:forEach items="${EditmyDataList}" var="UserRegRpt">
																		<tr>
																			<td>${UserRegRpt.username}</td>
																			<td>${UserRegRpt.creationdate}</td>
																			<td>${UserRegRpt.createdby}</td>
																			<td>${UserRegRpt.isactive}</td>
																			<td>${UserRegRpt.response}</td>
																		</tr>
																	</c:forEach>
																</tbody>
															</table>
														</div>
													</div>
													<div class="title-second">
													
													</div>

												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.page-content -->
	<jsp:include page="Footer.jsp" />
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->
	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	

	<script src="plugins/retina/retina.min.js"></script>
	<script src="plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="plugins/backstretch/jquery.backstretch.min.js"></script>


	<!-- PLUGINS -->
	<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="plugins/prettify/prettify.js"></script>
	<script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="plugins/chosen/chosen.jquery.min.js"></script>
	<script src="plugins/icheck/icheck.min.js"></script>
	<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="plugins/mask/jquery.mask.min.js"></script>
	<script src="plugins/validator/bootstrapValidator.min.js"></script>
	<script src="plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="plugins/datatable/js/jquery.highlight.js"></script>
	<script src="plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="plugins/summernote/summernote.min.js"></script>
	<script src="plugins/markdown/markdown.js"></script>
	<script src="plugins/markdown/to-markdown.js"></script>
	<script src="plugins/markdown/bootstrap-markdown.js"></script>
	<script src="plugins/slider/bootstrap-slider.js"></script>
	<script src="plugins/toastr/toastr.js"></script>
	<script src="plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="plugins/placeholder/jquery.placeholder.js"></script>
	<script src="plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script type="text/javascript"
		src="js/shieldui-all.min.js"></script>
	<script type="text/javascript"
		src="js/jszip.min.js"></script>


	<!--  Table Export -->



	<!-- KNOB JS -->
	<!--[if IE]>
        <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
	<script src="plugins/jquery-knob/jquery.knob.js"></script>
	<script src="plugins/jquery-knob/knob.js"></script>

	<!-- MAIN APPS JS -->
	<script src="js/apps.js"></script>
	
</body>
</html>