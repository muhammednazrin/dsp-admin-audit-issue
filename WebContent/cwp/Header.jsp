<%@ page language="java" contentType="text/html; charset=ISO-8859-1" 
 pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>ETIQA</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" >
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="no-cache">
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="no-store">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--  <script src="js/datepicker/jquery.min.js"></script>
        <script src="js/datepicker/jquery-ui.min.js"></script>-->
<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet" href="plugins/datepicker/datepicker.min.css">
<!-- PLUGINS CSS -->
<link href="plugins/owl-carousel/owl.carousel.min.css" rel="stylesheet">
<link href="plugins/owl-carousel/owl.theme.min.css" rel="stylesheet">
<link href="plugins/owl-carousel/owl.transitions.min.css"
	rel="stylesheet">
<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="css/owl.css" rel="stylesheet">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-select.css" rel="stylesheet">
<link href="plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="http://www.shieldui.com/shared/components/latest/css/light/all.min.css" />
<!--<link href="css/style.css" rel="stylesheet">-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}
</style>
<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}
</style>
</head>
<body>
	<%
		String fn = "";
		if (session != null) {
			if (session.getAttribute("user") != null) {
				String name = (String) session.getAttribute("user");
				fn = (String) session.getAttribute("FullName");
			} else {
				response.sendRedirect("admin-login.jsp");
			}
		}
		
		
	%>


	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!-- header -->
		<div class="header">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="visible-xs-block">
							<!-- Begin button sidebar left toggle -->
							<div class="btn-collapse-sidebar-left logo">
								<i class="fa fa-bars"></i>
							</div>
							<!-- /.btn-collapse-sidebar-left -->
							<!-- End button sidebar left toggle -->
						</div>
						<div class="logo">
							<img class="" src="images/logo.png" alt="logo">
						</div>
						<div class="title">MyAccount</div>
						<div class="clearfix"></div>
					</div>
					<div class="col-sm-6">
						<!-- BEGIN TOP NAV -->
						<p align="right" valign="top">
						
						
							Welcome! <b><%=fn%></b> <b>(</b><a href="${pageContext.request.contextPath}/doLogout">Logout</a><b>)</b>
							&nbsp;&nbsp;<a href="${pageContext.request.contextPath}/modulelist"><b>Back</b></a> 
						</p>
						<!-- END TOP NAV -->
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- /.row -->
			</div>
		</div>
		<!-- end header -->
</body>
</html>