<%@ page language="java" contentType="text/html; charset=ISO-8859-1" 
 pageEncoding="ISO-8859-1"%>

 <%
		if (session != null) {
			if (session.getAttribute("user") != null) {
				String name = (String) session.getAttribute("user");
				} else {
			//	response.sendRedirect("admin-login.jsp");
			}
		}
	%><!DOCTYPE html>
<html lang="en">
    <head>
        <title>Etiqa</title>
        <!-- meta info -->
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type" >
       <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="no-cache">
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="no-store">
        <meta name="keywords" content="Etiqa Customer Portal" />
        <meta name="description" content="Etiqa">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
 

        <!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        
        <!-- PLUGINS CSS -->
        <link href="plugins/owl-carousel/owl.carousel.min.css" rel="stylesheet">
        <link href="plugins/owl-carousel/owl.theme.min.css" rel="stylesheet">
        <link href="plugins/owl-carousel/owl.transitions.min.css" rel="stylesheet">
        
        <!-- MAIN CSS (REQUIRED ALL PAGE)-->
        <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/owl.css" rel="stylesheet">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap-select.css" rel="stylesheet">
        <!--<link href="css/test.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">-->
 
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
        <style>
        .error {
        color:red;
        }
        
        </style>
        
    </head>
 
    <body>
        
        <!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
          
           <%
          
           request.logout();
           session.removeAttribute("user");
           request.getSession().invalidate();
         
          %>
        <div class="wrapper dashboard">
            <!-- header second-->
            <div class="header-admin">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <h2>Administration</h2>
                        </div>
                    </div>
                </div>
            </div>
            <!-- header second-->
            
            
            <!-- BEGIN PAGE CONTENT -->
            <div class="page-content">
                <div class="container"><!-- /.container-->
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-9">
                            <div class="panel panel-default login">
                              <div class="panel-heading"><h4>Etiqa MyAccount! Administration</h4></div>
                              <div class="panel-body">
                                <%--  <form action="login" name="loginForm" id="loginForm" method="post">
                                   	<input type="hidden" name="CSRFToken"  value="<%=session.getAttribute("CSRFToken") %>" />
								 --%>
                                <div class="col-sm-6">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="col-sm-12">
                                                <div class="logo form-group">
                                                    <a href="index.html"><img width="80" src="images/logo.png" alt="logo"></a>
                                                </div>
                                            </div>
                                            
                                            <div class="col-sm-12">
                                                <div class="">
                                                    You have been logged out successfully
                                                </div>
                                                
                                                <div class="">
                                                   <p>
                                                   <a href="admin-login.jsp">Login</a>
                                                   </p>
                                                </div>
                                            </div>
                                     <!--        
                                        </form>
                                      -->   </div>
                                        
                                    </div>
                                </div>
                              </div>
                            </div>
<!--
                            <div class="the-box">
                                <div>Etiqa Customer Web Portal! Administration Login</div>
                                <div class="col-sm-6">asdas</div>
                                <div class="col-sm-6">
                                    <div class="the-box">asdasd</div>
                                </div>
                            </div>
-->
                        </div>
                    </div><!-- /.row -->  
                     
                </div><!-- /.container -->
            </div><!-- /.page-content -->
        </div><!-- /.wrapper -->
        <!-- END PAGE CONTENT -->
        <br><br><br><br><br><br><br><br>
        <jsp:include page="Footer.jsp" />
    
        <!-- BEGIN BACK TO TOP BUTTON -->
<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
        <!-- END BACK TO TOP -->
        
        
        <!--
        ===========================================================
        END PAGE
        ===========================================================
        -->
        
        <!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
        <!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
        	<script src="js/jquery-2.2.4.min.js"></script>
		<script src="js/jquery-2.2.4.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-select.min.js"></script>

        <script src="plugins/retina/retina.min.js"></script>
        <script src="plugins/nicescroll/jquery.nicescroll.js"></script>
        <script src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
        <script src="plugins/backstretch/jquery.backstretch.min.js"></script>

 
        <!-- PLUGINS -->
        <script src="plugins/owl-carousel/owl.carousel.min.js"></script>
        <script src="plugins/prettify/prettify.js"></script>
        <script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
        
        <script src="plugins/chosen/chosen.jquery.min.js"></script>
        <script src="plugins/icheck/icheck.min.js"></script>
        <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
        <script src="plugins/timepicker/bootstrap-timepicker.js"></script>
        <script src="plugins/mask/jquery.mask.min.js"></script>
        <script src="plugins/validator/bootstrapValidator.min.js"></script>
        <script src="plugins/datatable/js/jquery.dataTables.min.js"></script>
        <script src="plugins/datatable/js/bootstrap.datatable.js"></script>
        <script src="plugins/summernote/summernote.min.js"></script>
        <script src="plugins/markdown/markdown.js"></script>
        <script src="plugins/markdown/to-markdown.js"></script>
        <script src="plugins/markdown/bootstrap-markdown.js"></script>
       <script src="plugins/slider/bootstrap-slider.js"></script>
        <script src="plugins/toastr/toastr.js"></script>
        <script src="plugins/newsticker/jquery.newsTicker.min.js"></script>
        <script src="plugins/placeholder/jquery.placeholder.js"></script>
         <script src="plugins/jquery-validation/dist/jquery.validate.min.js"></script>
         <script src="plugins/jquery-validation/dist/jquery.validate.js"></script>
        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
        <script src="plugins/jquery-knob/jquery.knob.js"></script>
        <script src="plugins/jquery-knob/knob.js"></script>
        
        <!-- MAIN APPS JS -->
        <script src="js/apps.js"></script>
        
    </body>
</html>