<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="no-cache">
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="no-store">




<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet" href="plugins/datepicker/datepicker.min.css">
<!-- PLUGINS CSS -->
<link href="plugins/owl-carousel/owl.carousel.min.css" rel="stylesheet">
<link href="plugins/owl-carousel/owl.theme.min.css" rel="stylesheet">
<link href="plugins/owl-carousel/owl.transitions.min.css"
	rel="stylesheet">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="css/owl.css" rel="stylesheet">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-select.css" rel="stylesheet">
<link href="plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="css/menu.css" rel="stylesheet">
<!--<link href="css/style.css" rel="stylesheet">-->
<link rel="stylesheet" type="text/css" href="plugins/all.min.css" />
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}
</style>

<script type="text/javascript">
	function ShowICNumber() {
		var source, target;

		source = document.getElementById("newicnumber");
		target = document.getElementById("icnumber1");
		target.value = source.value;
		//source.disabled=false;
		target.readOnly = true;

		return true;
	}
</script>

</head>

<body>
<%--    <%
		if (session != null) {
			if (session.getAttribute("user") != null) {
				String name = (String) session.getAttribute("user");
				} else {
				response.sendRedirect("admin-login.jsp");
			}
		}
	%>
 --%>
	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->

	<div class="wrapper inner">
		<!-- header -->
		<jsp:include page="Header.jsp" />
		<!-- end header -->
		<!-- header second-->
		<div class="header-second">
			<div class="container">
				<div class="row">

					<div class="col-sm-6 hidden-xs">
						<!-- Begin breadcrumb -->
						<ol class="breadcrumb">
							<li><a href="#"><i class="fa fa-home"></i></a></li>
							
							<li class="active">Update Customer Phone Number</li>
						</ol>
						<!-- End breadcrumb -->
					</div>
					<br>
					<p align="right" valign="top">
						
					</p>

					<!--   <div class="col-sm-6 hidden-xs">
                            <div class="col-sm-8">
                                <div class="text-right last-login">
                                    Your last login was on 01 Feb 2016  10:01:01
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="language">
                                    <select class="selectpicker">
                                        <option value="United States">English</option>
                                        <option value="United Kingdom">Malay</option>
                                    </select>
                                </div>
                            </div>
                        </div> -->
					<!--   <div class="col-xs-12 visible-xs-block">
                            <div class="mobile-view">
                                <ul class="nav-user navbar-right">
                                    <li class="dropdown">
                                      <a href="#fakelink" class="dropdown-toggle text-right" data-toggle="dropdown">
                                        <i class="fa fa-user"></i>
                                        Welcome <strong>Siti Amirah Umar</strong>
                                        <i class="fa fa-angle-down fa-lg"></i>
                                      </a>
                                      <ul class="dropdown-menu square primary margin-list-rounded with-triangle pull-right">
                                        <li><a href="security-setting.html">Security setting</a></li>
                                        <li class="divider"></li>
                                        <li><a href="logout.html">Log out</a></li>
                                      </ul>
                                    </li>
                                </ul>
                            </div>
                        </div> -->
				</div>
			</div>
		</div>
		<!-- header second-->
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">

					<jsp:include page="cwpmenu.jsp" />


					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li>Update Customer Phone
														Number</li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11">
								<div class="row">
									<div class="col-sm-12">
										<div class="title">
											<div class="sub">
												<h4>Search Customer Information</h4>
											</div>
										</div>
										<div class="content-inner">
											<div class="the-box">
												<div class="col-sm-6">
													<div class="form-horizontal">
														<div class="form-group">
															<label class="col-sm-4 control-label">Search By</label>
															<div class="col-sm-8">
																<select class="form-control">
																	<option selected="" value="New NRIC">New IC</option>
																	<option value="Old IC">Old IC</option>
																	<option value="Police ID">Police ID</option>
																	<option value="Military ID">Military ID</option>
																	<option value="Pasport ">Passport</option>
																</select>
															</div>
														</div>

													</div>
												</div>
												<form name="searcICForm" id="searcICForm" method="post">
													  <%-- 	<input type="hidden" name="CSRFToken"  value="<%=session.getAttribute("CSRFToken") %>" />
						 --%>
													<div class="col-sm-6">
														<div class="form-horizontal">
															<div class="form-group">
																<label class="col-sm-4 control-label">IC Number
																	<span class="text-danger">*</span>
																</label>
																<div class="col-sm-8">
																	<input type="text" name="newicnumber" id="newicnumber"
																		class="form-control">
																</div>
															</div>
														</div>
													</div>

													<div class="col-sm-12 text-right">
														<input value="Search" id="Search" class="btn btn-default"
															onclick="return ShowICNumber();" type="submit" />
													</div>
												</form>
											</div>
										</div>
									</div>

									<div class="col-sm-12">

										<!--No record found Invisible  -->
										<div id="noRecordFound"
											class="alert alert-danger fade in alert-dismissable hidediv">
											<!--   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X—</button> -->
											<strong><i class="fa fa-exclamation"></i> No record
												found</strong>
										</div>
										<!--No record found Invisible  -->
										<!--  record found Invisible  -->
										<div id="recordFound"
											class="alert alert-danger fade in alert-dismissable hidediv">
											<button aria-hidden="true" data-dismiss="alert" class="close"
												type="button">X</button>
											<strong><i class="fa fa-exclamation"></i> Record
												Already Exist</strong>
										</div>
										<!--record found Invisible  -->
										<div
											class="alert alert-success fade in alert-dismissable hidediv">
											<button aria-hidden="true" data-dismiss="alert" class="close"
												type="button">X</button>
											<strong><i class="fa fa-check"></i> Successfully
												sent Activation Code (AC) to Mobile Number </strong>
										</div>
										<div id="successDiv"
											class="alert alert-success fade in alert-dismissable hidediv">
											<button aria-hidden="true" data-dismiss="alert" class="close"
												type="button">X</button>
											<strong><i class="fa fa-check"></i>User Successfully
												Inserted </strong>
										</div>
										
										  <div id="updateDiv"
											class="alert alert-success fade in alert-dismissable hidediv">
											<button aria-hidden="true" data-dismiss="alert" class="close"
												type="button">X</button>
											<strong><i class="fa fa-check"></i>Mobile number Successfully
												updated </strong>
										</div>
										<!-- <div class="content-inner">
                                                <div class="the-box static">
                                                    <div class="title-second">
                                                        <div class="sub">
                                                        <h4>Customer Information</h4>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-horizontal">
                                                          <div class="form-group">
                                                            <label class="col-xs-4 control-label">Name</label>
                                                              <div class="col-xs-8"><p class="form-control-static">Siti Amirah</p></div>
                                                          </div>
                                                          <div class="form-group">
                                                            <label class="col-xs-4 control-label">ID Type</label>
                                                              <div class="col-xs-8"><p class="form-control-static">New NRIC</p></div>
                                                          </div>
                                                          <div class="form-group">
                                                            <label class="col-xs-4 control-label">New NRIC Number</label>
                                                              <div class="col-xs-8"><p class="form-control-static">860918 10 6608</p></div>
                                                          </div> 
                                                          <div class="form-group">
                                                            <label class="col-xs-4 control-label">Date of Birth</label>
                                                              <div class="col-xs-8"><p class="form-control-static">18-Sept-1986</p></div>
                                                          </div> 
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-horizontal">
                                                          <div class="form-group">
                                                            <label class="col-xs-4 control-label">Gender</label>
                                                              <div class="col-xs-8"><p class="form-control-static">Female</p></div>
                                                          </div>
                                                          <div class="form-group">
                                                            <label class="col-xs-4 control-label">Nationality</label>
                                                              <div class="col-xs-8"><p class="form-control-static">Malaysian</p></div>
                                                          </div>
                                                          <div class="form-group">
                                                            <label class="col-xs-4 control-label">Marital Status</label>
                                                              <div class="col-xs-8"><p class="form-control-static">Married</p></div>
                                                          </div>
                                                          <div class="form-group">
                                                            <label class="col-xs-4 control-label">Race</label>
                                                              <div class="col-xs-8"><p class="form-control-static">Malay</p></div>
                                                          </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->

                                      <div id="regupdateForm" class="content-inner hidediv ">
											<form name="userForm" id="userForm" method="post">
											<div class="col-sm-6">
											<div class="form-horizontal">
															<div class="form-group">
																<label class="col-xs-4 control-label">Mobile</label>
																<div class="col-xs-8">
																<input type="text" id="phnumber" name="phnumber" required
																		class="form-control" value="" disabled>
																<input type="hidden" id="icno" name="icno" >
																<input type="hidden" name="update" id="update" value="1">
											                   <div id="errorphmsg" style="color: red;"></div>
																</div>
															</div>
														
												</div>
											</div>
											<div class="col-sm-6">
											<div class="form-horizontal">
											  <div class="form-group">
											         <button type="button" id="btnedit"  class="btn btn-default" onClick="return edit();"><i class="fa fa-pencil-square-o"></i> Edit</button>
											          <button type="submit" id="btnsubmit"  class="btn btn-default hidden" ><i class="fa fa-pencil-square-o"></i> Update</button>
												</div>
											  </div>
											</div>
											
											<div id="smsactivation" class="col-sm-12 hidediv">
														<div class="form-horizontal">
															<div class="form-group">
																<input type="button" id="otcupdsms" name="otcupdsms"
																	value="Sent Activation Code"
																	class="btn btn-warning btn-sm" onclick="sendupdatesms()" ; />


															</div>
															<div class="form-group"></div>
														</div>
													</div>
											</form>
											</div> 

										<div id="regForm" class="content-inner hidediv ">
											<form name="userRegForm" id="userRegForm" method="post">
												<div class="the-box static">
													<div class="title-second">
														<div class="sub">
															<h4>Customer Information</h4>
														</div>
													</div>

													<div class="col-sm-6">
														<div class="form-horizontal">
															<div class="form-group">
																<label class="col-xs-4 control-label">Name</label>
																<div class="col-xs-8">
																	<input type="text" name="name" required
																		class="form-control">
																</div>
															</div>
															<div class="form-group">
																<label class="col-xs-4 control-label">ID Type</label>
																<div class="col-xs-8">
																	<!-- <input type="text" name="idtype" required class="form-control">-->
																	<select class="form-control" name="idtype" required>
																		<option selected="" value="New NRIC">New IC</option>
																		<option value="Old IC">Old IC</option>
																		<option value="Police ID">Police ID</option>
																		<option value="Military ID">Military ID</option>
																		<option value="Pasport ">Passport</option>

																	</select>
																</div>
															</div>
															<div class="form-group">
																<label class="col-xs-4 control-label">IC Number</label>
																<div class="col-xs-8">
																	<input type="text" name="icnumber1" id="icnumber1"
																		required class="form-control">
																</div>
															</div>
															<div class="form-group">
																<label class="col-xs-4 control-label">Date of
																	Birth</label>
																<div class="col-xs-8">
																	<input id="datepicker" type="text" name="dob" required
																		class="form-control" readonly>
																</div>
															</div>
															<div class="form-group">
																<label class="col-xs-4 control-label">Address</label>
																<div class="col-xs-8">
																	<input type="text" name="address" required
																		class="form-control">
																</div>
															</div>
															<div class="form-group">
																<label class="col-xs-4 control-label">Mobile
																	Number</label>
																<div class="col-xs-8">
																	<input type="text" name="mobilenumber"
																		id="mobilenumber" required class="form-control">
																	<div id="errormsg" style="color: red;"></div>
																</div>
															</div>

															<div class="form-group">
																<label class="col-xs-4 control-label">Email</label>
																<div class="col-xs-8">
																	<input type="text" name="email" id="email" required
																		class="form-control">
																	<div id="emailmsg" style="color: red;"></div>
																</div>
															</div>
															<!-- <div class="form-group">
                                                            <label class="col-xs-4 control-label">Monthly Income</label>
                                                              <div class="col-xs-8"><input type="text" name="mincome" class="form-control"></div>
                                                          </div>
														    <div class="form-group">
                                                            <label class="col-xs-4 control-label">Prefered Language</label>
                                                              <div class="col-xs-8"><input type="text" name="planguage" class="form-control"></div>
                                                          </div> -->


														</div>
													</div>
													<div class="col-sm-6">
														<div class="form-horizontal">
															<div class="form-group">
																<label class="col-xs-4 control-label">Gender</label>
																<div class="col-xs-8">
																	<!--<input type="text" name="gender" required class="form-control">-->
																	<select class="form-control" name="gender" required>
																		<option selected="selected" value="Female">Female</option>
																		<option value="Male">Male</option>


																	</select>
																</div>
															</div>
															<div class="form-group">
																<label class="col-xs-4 control-label">Nationality</label>
																<div class="col-xs-8">
																	<input type="text" name="nationality" required
																		class="form-control">
																</div>
															</div>
															<div class="form-group">
																<label class="col-xs-4 control-label">Marital
																	Status</label>
																<div class="col-xs-8">
																	<input type="text" name="maritalstatus" required
																		class="form-control">
																</div>
															</div>
															<div class="form-group">
																<label class="col-xs-4 control-label">Race</label>
																<div class="col-xs-8">
																	<select class="form-control" name="race" required>
																		<option selected="selected" value="Malay">Malay</option>
																		<option value="Chinese">Chinese</option>
																		<option value="Indian">Indian</option>
																		<option value="Others">Others</option>
																	</select>
																</div>
															</div>

															<div class="form-group">
																<label class="col-xs-4 control-label">Home</label>
																<div class="col-xs-8">
																	<input type="text" name="homenumber" required
																		class="form-control">
																</div>
															</div>
															<div class="form-group">
																<label class="col-xs-4 control-label">City</label>
																<div class="col-xs-8">
																	<input type="text" name="city" required
																		class="form-control">
																</div>
															</div>

															<div class="form-group">
																<label class="col-xs-4 control-label">State</label>
																<div class="col-xs-8">
																	<input type="text" name="state" required
																		class="form-control">
																</div>
															</div>

															<div class="form-group">
																<label class="col-xs-4 control-label">Post Code</label>
																<div class="col-xs-8">
																	<input type="text" name="pcode" required
																		class="form-control">
																</div>
															</div>

															<!-- <div class="form-group">
                                                            <label class="col-xs-4 control-label">Religion</label>
                                                              <div class="col-xs-8"><input type="text" name="religion" class="form-control"></div>
                                                          </div>
														   <div class="form-group">
                                                            <label class="col-xs-4 control-label">Childrens</label>
                                                              <div class="col-xs-8"><input type="text" name="childresn" class="form-control"></div>
                                                          </div> -->
														</div>
													</div>
												</div>
												<!-- Buttons Save and Sent Activation code  -->
												<div class="the-box static">
													<div class="title-second">
														<div class="sub">
														<input type="hidden" name="insert" id="insert" value="1">
															<input class="btn btn-warning btn-sm" type="submit"
																value="Save" /> <input class="btn btn-warning btn-sm"
																type="reset" value="Reset" />
															<!-- data-toggle="modal" data-target="#alertValidation" -->
														</div>
													</div>

													<div id="activation" class="col-sm-12 hidediv">
														<div class="form-horizontal">
															<div class="form-group">
																<input type="button" id="octsms" name="otcsms"
																	value="Sent Activation Code"
																	class="btn btn-warning btn-sm" onclick="sendsms()" ; />


															</div>
															<div class="form-group"></div>
														</div>
													</div>
											</form>
										</div>
										<!-- Buttons Save and Sent Activation code  -->

									</div>


								</div>

							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.container -->



	</div>
	<!-- /.page-content -->
	<!-- BEGIN FOOTER -->

	<jsp:include page="Footer.jsp" />


	<!-- END FOOTER -->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
	<script src="js/jquery-2.2.4.min.js"></script>
		<script src="js/jquery-2.2.4.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-select.min.js"></script>
	<script src="js/bootstrap-tabcollapse.js"></script>

	<script src="plugins/retina/retina.min.js"></script>
	<script src="plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="plugins/backstretch/jquery.backstretch.min.js"></script>


	<!-- PLUGINS -->
	<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="plugins/prettify/prettify.js"></script>
	<script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="plugins/chosen/chosen.jquery.min.js"></script>
	<script src="plugins/icheck/icheck.min.js"></script>
	<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="plugins/mask/jquery.mask.min.js"></script>
	<script src="plugins/validator/bootstrapValidator.min.js"></script>
	<script src="plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="plugins/datatable/js/jquery.highlight.js"></script>
	<script src="plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="plugins/summernote/summernote.min.js"></script>
	<script src="plugins/markdown/markdown.js"></script>
	<script src="plugins/markdown/to-markdown.js"></script>
	<script src="plugins/markdown/bootstrap-markdown.js"></script>
	<script src="plugins/slider/bootstrap-slider.js"></script>
	<script src="plugins/toastr/toastr.js"></script>
	<script src="plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="plugins/placeholder/jquery.placeholder.js"></script>
	<script src="plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="plugins/jquery-validation/dist/jquery.validate.js"></script>


	<!-- KNOB JS -->
	<!--[if IE]>
        <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
	<script src="plugins/jquery-knob/jquery.knob.js"></script>
	<script src="plugins/jquery-knob/knob.js"></script>

	<!-- MAIN APPS JS -->
	<script src="js/apps.js"></script>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {

							// Start------------------------------Validation of Search Form 
							$("#searcICForm").validate({
								rules : {
									newicnumber : {
										required : true,
									},
								},
								messages : {
									newicnumber : {
										required : "IC Number is Required"
									},
								}

							});
							// End------------------------------Validation of Search Form  
							$("#datepicker").datepicker({
								autoclose : true,
								format : 'dd-mm-yyyy'
							});
							// Start------------------------------Submission of Search Form    
							$("#searcICForm")
									.on(
											"submit",
											function(event) {
												if ($(this).valid()) {
													// var data = new FormData(this);
													// data.append("action", "save");           
													event.preventDefault();
													var icnumber = document
															.getElementById("newicnumber").value;
													$
															.ajax({
																url : 'search',
																type : "post",
																data : {
																	"icnumber" : icnumber
																},
																//data : $("#myform").serialize(),
																dataType : 'json',
																success : function(
																		response) {

																	if (response.res_code == "fail") {
																		$(
																				'#noRecordFound')
																				.removeClass(
																						'hidediv');
																		$(
																				'#regForm')
																				.removeClass(
																						'hidediv');
																		$(
																				'#recordFound')
																				.addClass(
																						'hidediv');
																		$(
																				'#regupdateForm')
																				.addClass(
																						'hidediv');
																		$("#updateDiv").addClass('hidediv');
																		//  $('#userRegForm').trigger('reset');
																		// document.getElementById("newicnumber").value=''; 
																		// reset_form();
																	}
																	if (response.res_code == "success") {
																		$(
																				'#recordFound')
																				.removeClass(
																						'hidediv');
																		$(
																				'#noRecordFound')
																				.addClass(
																						'hidediv');
																		$(
																				'#regForm')
																				.addClass(
																						'hidediv');
																		$(
																				'#regupdateForm')
																				.removeClass(
																						'hidediv');
																		$(
																				'#smsactivation')
																				.addClass(
																						'hidediv');
																		
																		//alert(response.mobile);
																		$("#phnumber").val(response.mobile);
																		$("#icno").val(response.icno);

																	}
																},
																error : function(
																		data,
																		status,
																		er) {
																	alert(data
																			+ "_"
																			+ status
																			+ "_"
																			+ er);
																}
															});
												}
											});

							// End------------------------------Submission of Search Form  
							//------------------------------------------------------------------------------------------------------------------   
							// Start------------------------------Validation of User Registreation Form 
							$("#userRegForm").validate({
								rules : {
									name : {
										required : true,
									},
									icnumber : {
										required : true,
									},
									mobilenumber : {
										required : true,
									},
								},
								messages : {
									name : {
										required : "Name is Required"
									},
									icnumber : {
										required : "IC Number is Required"
									},
									mobilenumber : {
										required : "Mobile Number is Required"
									},
								}

							});

							$("#mobilenumber")
									.change(
											function() {
												var mobileno = document
														.getElementById("mobilenumber").value;
												$("#errormsg").text("");
												if (!mobileno.startsWith("6")) {
													//alert("Please enter Mobile Number Starts with 6.");
													$("#errormsg")
															.html(
																	"<b>Please enter Mobile Number Starts with 6.</b>");
													return false;
												}
											});

							$("#email")
									.change(
											function() {
												var email = document
														.getElementById("email").value;
												$("#emailmsg").text("");
												var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

												if (!filter.test(email)) {

													$("#emailmsg")
															.html(
																	"<b>Please provide a valid email address.</b>");
													return false;
												}
											}); // End------------------------------Validation of User Registreation Form  

							// Start------------------------------Submission of User Registreation Form  
							$("#userRegForm")
									.on(
											"submit",
											function(event) {
												var mobileno = document
														.getElementById("mobilenumber").value;
												$("#errormsg").text("");
												if (!mobileno.startsWith("6")) {
													//alert("Please enter Mobile Number Starts with 6.");
													$("#errormsg")
															.html(
																	"<b>Please enter Mobile Number Starts with 6.</b>");
													return false;
												}
												if ($(this).valid()) {
													// var data = new FormData(this);

													// data.append("action", "save");           
													event.preventDefault();
													$
															.ajax({
																url : 'reguser',
																type : "post",
																// data: {"name": "name"},                         
																data : $(
																		"#userRegForm")
																		.serialize(),
																dataType : 'json',
																success : function(
																		response) {
																	if (response.res_code == "success") {
																		$(
																				'#activation')
																				.removeClass(
																						'hidediv');
																		$(
																				'#successDiv')
																				.removeClass(
																						'hidediv');
																		$(
																				'#noRecordFound')
																				.addClass(
																						'hidediv');

																	}
																},
																error : function(
																		data,
																		status,
																		er) {
																	alert(data
																			+ "_"
																			+ status
																			+ "_"
																			+ er);
																}
															});
												}
											});

							// End------------------------------Submission of User Registreation Form 
							
							
							// Start------------------------------Validation of User UPDATE Form 
							$("#userForm").validate({
								rules : {
									phnumber : {
										required : true,
									},
									
								},
								messages : {
									phnumber : {
										required : "Name is Required"
									},
									
								}

							});

							$("#phnumber")
									.change(
											function() {
												var mobileno = document
														.getElementById("phnumber").value;
												$("#errorphmsg").text("");
												if (!mobileno.startsWith("6")) {
													//alert("Please enter Mobile Number Starts with 6.");
													$("#errorphmsg")
															.html(
																	"<b>Please enter Mobile Number Starts with 6.</b>");
													return false;
												}
											});
											
						// Start------------------------------Submission of update Form    
						
						$("#userForm")
									.on(
											"submit",
											function(event) {
												
												if ($(this).valid()) {
													// var data = new FormData(this);
                                                
													// data.append("action", "save");           
													event.preventDefault();
													$
															.ajax({
																url : 'reguser',
																type : "post",
																// data: {"name": "name"},                         
																data : $(
																		"#userForm")
																		.serialize(),
																dataType : 'json',
																success : function(
																		response) {
																if (response.res_code == "success") {
																		$(
																				'#recordFound')
																				.removeClass(
																						'hidediv');
																		$(
																				'#noRecordFound')
																				.addClass(
																						'hidediv');
																		$(
																				'#regForm')
																				.addClass(
																						'hidediv');
																		$(
																				'#regupdateForm')
																				.removeClass(
																						'hidediv');
																		
																		//alert(response.mobile);
																		
																		$("#phnumber").val(response.mobile);
																		$("#icno").val(response.icno);
																		$("#updateDiv").removeClass('hidediv');
																		$("#phnumber").prop("disabled",true);
		   $("#btnsubmit").addClass('hidden');
		   $("#btnedit").removeClass('hidden');
		   $(
																				'#smsactivation')
																				.removeClass(
																						'hidediv');
																		

																	}
																},
																error : function(
																		data,
																		status,
																		er) {
																	alert(data
																			+ "_"
																			+ status
																			+ "_"
																			+ er);
																}
															});
												}
											});
						
							// End------------------------------Submission of Search Form  
							
						});

		function sendsms() {

			var icnumber = document.getElementById("newicnumber").value;
			var mobile = document.getElementById("mobilenumber").value;
			
			$
					.ajax({
						url : 'sendsms',
						type : 'POST',
						data : {
							"icnumber" : icnumber,
							"mobile" : mobile
						},
						success : function(response) {
							if (response.res_code == "success") {
								alert("Activation Code Successfully Sent to the Customer");
							}

						}

					});
		}
		
		function edit(){
		//alert("welcome");
		   $("#phnumber").prop("disabled",false);
		   $("#btnsubmit").removeClass('hidden');
		   $("#btnedit").addClass('hidden');
		}
		
			function sendupdatesms() {

			var icnumber =document.getElementById("icno").value;
			var mobile = document.getElementById("phnumber").value;  
			
			  // alert(icnumber + mobile);
			
			$
					.ajax({
						url : 'sendsms',
						type : 'POST',
						data : {
							"icnumber" : icnumber,
							"mobile" : mobile
						},
						success : function(response) {
							if (response.res_code == "success") {
								alert("Activation Code Successfully Sent to the Customer");
							}

						}

					});
		}
	</script>
</body>
</html>