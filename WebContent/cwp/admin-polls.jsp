<%@ page language="java" contentType="text/html; charset=ISO-8859-1" 
 pageEncoding="ISO-8859-1"%>
<%@page import="com.cwp.rpt.Polls"%>
<%@page import="com.cwp.rpt.PollsListRpt"%>
<%@page import="com.cwp.dao.PollReportDAO"%>
<%@ page
	import="java.util.Date,
					   java.util.Calendar,
					   java.text.SimpleDateFormat,
					   java.util.HashSet"%>

<%
	PollsListRpt listRpt = null;
	if (request.getAttribute("pollslist") == null) {
		PollReportDAO pollReportDAO = new PollReportDAO();
		listRpt = pollReportDAO.getPollsList();
	} else {
		listRpt = (PollsListRpt) request.getAttribute("pollslist");
	}
	SimpleDateFormat dateFormatWithTime = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat dateParse = new SimpleDateFormat("MM/dd/yyyy");
%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" >
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="no-cache">
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="no-store">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet" href="plugins/datepicker/datepicker.min.css">
<!-- PLUGINS CSS -->
<link href="plugins/owl-carousel/owl.carousel.min.css" rel="stylesheet">
<link href="plugins/owl-carousel/owl.theme.min.css" rel="stylesheet">
<link href="plugins/owl-carousel/owl.transitions.min.css"
	rel="stylesheet">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="css/owl.css" rel="stylesheet">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-select.css" rel="stylesheet">
<link href="plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="css/menu.css" rel="stylesheet">
<!--<link href="css/style.css" rel="stylesheet">-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
        
        
        
        
</head>

<body>
<%
		/*if (session != null) {
			if (session.getAttribute("user") != null) {
				String name = (String) session.getAttribute("user");
				} else {
				response.sendRedirect("admin-login.jsp");
			}
		}*/
	%>

	<!--
			===========================================================
			BEGIN PAGE
			===========================================================
			-->
	<div class="wrapper inner">
		<!-- header -->
		<div class="header">
			<jsp:include page="Header.jsp" />

			<!-- end header -->
			<!-- header second-->
			<div class="header-second">
				<div class="container">
					<div class="row">

						<div class="col-xs-12 visible-xs-block">
							<div class="mobile-view"></div>
						</div>
					</div>
				</div>
			</div>
			<!-- header second-->
			<!-- BEGIN PAGE CONTENT -->
			<!-- Mobile menu -->
			<div class="page-content inner">
				<div class="container-fluid black-back">
					<!-- /.container-->
					<div class="row"><jsp:include page="cwpmenu.jsp" />
						<div class="col-sm-9 col-md-10 white-back">
							<div class="row">
								<div class="col-sm-11  gap-mid">
									<div class="row">
										<div class="col-sm-12" id="pollsReport">
											<!-- Begin  table -->
											<div class="title">
												<div class="sub">
													<h4>Polls</h4>
												</div>
											</div>
											<div class="content-inner">
												<div class="gap gap-mini">
													<!-- <form class="form-inline">
														<div class="form-group">
															<label for="exampleInputName2">Search Poll : </label> <input
																type="text" id="searchText" class="form-control"
																placeholder="Search">
														</div>
														<div class="poll-date pull-right">
															<div class="form-group">
																<label for="exampleInputName2">Create Date :
																	From</label> <input type="text" class="form-control"
																	id="datepicker1" placeholder="date">
															</div>
															<div class="form-group">
																<label for="exampleInputEmail2">To</label> <input
																	type="email" class="form-control" id="datepicker2"
																	placeholder="date">
															</div>
															<a onClick="return searchPolls()
														  "
																class="btn btn-warning">Search</a>
														</div>
													</form> -->
													<div class="clearfix"></div>
												</div>
												<div class="table-header">
													<h5>Polls List</h5>
													<div class="clearfix"></div>
												</div>
												<div class="the-box full no-border" id="pollList">
													<table id="admin-datatable-second"
														class="table table-striped table-hover">
														<thead>
															<tr>
																<td>ID</td>
																<td>Poll Name</td>
																<td>Create Date</td>
																<td>Expiry Date</td>
																<td width="6%">Action</td>
															</tr>
														</thead>
														<tbody id="pollslisttable">
															<%
																int i = 1;
																for (Polls polls : listRpt.getPolls()) {
															%>
															<tr id='<%=i%>'>
																<td><%=i%></td>
																<td><%=polls.getQuestion()%></td>
																<td><%=dateFormatWithTime.format(polls.getStartdate())%></td>
																<td><%=dateFormatWithTime.format(polls.getExpiryDate())%></td>
																<td><a onClick="showGraph('<%=polls.getId()%>')"
																	class="btn btn-default btn-sm">Report</a></td>
															</tr>
															<%
															    i++;
																}
															%>
														</tbody>
													</table>
												</div>
												<div class="clearfix"></div>
												
												<div class="gap-mini"></div>
											
											</div>
										</div>
										<div class="col-sm-12" id="pollsGraph" style="display: none">
											<!-- Begin  table -->
											<div class="title">
												<div class="sub">
													<h4>
														<div class="pull-right">
															<!--	<a onCLick="showReport()" href="javascript:void"
																class="btn btn-default btn-sm">Back to List item</a>-->
														</div>
													</h4>
												</div>
											</div>
											<div class="content-inner">
												<div class="col-sm-10">
													<div class="row" id="pollName"></div>
												</div>
												<div class="col-sm-2">
													<div class="row">
													
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="gap gap-mini"></div>
												<div class="col-sm-12">
													<div class="row">
														<div class="panel panel-square panel-default">
															<div class="panel-heading">
																<div class="col-sm-10" id="reportFor"></div>
																<div class="col-sm-2">
																	<select class="selectpicker"
																		onChange="changeGraph(this)">
																		<option value="bar">Show Graph in</option>
																		<option value="pie">Chart</option>
																	</select>
																</div>
																<div class="clearfix"></div>
															</div>
															<div class="panel-body">
																<div class="col-sm-12 text-right" id="totalVotes">

																</div>
																<div class="col-sm-12">
																	<div id="chartContainer"
																		style="height: 150px; width: 100%;"></div>
																</div>
															</div>
															<!-- /.panel-body -->
															<div class="panel-footer" id="reportFooter">*
																Number of times that appears each value. Percent in
																relation to the total of submissions. Date range
																01-01-2016 to 29-02-2016</div>
														</div>
													</div>
												</div>

											</div>
											<!-- End warning color table -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.container -->

			</div>
			<!-- /.page-content -->
			<!-- BEGIN FOOTER -->
			<div class="footer">
				<footer>
					<div class="fluid-yellow">
						<div class="container">
							<div class="row">
								<div class="col-sm-9">

									<div class="clearfix"></div>
								</div>
								<div class="col-sm-3">
									<div class="text-right gap">
										<img class="" src="images/member-my.png" alt="logo">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="fluid-grey">
						<div class="container">
							<div class="row">
								<div class="col-sm-6">
									<div class="gap">&copy; 2016 Etiqa. All Rights Reserved</div>
								</div>
								<div class="col-sm-6">
									<ul class="right">
										<li><a href="terms.html">Terms</a></li>
										<li><a href="privacy.html">Privacy</a></li>
										<li><a href="disclaimer.html">Disclaimer</a></li>
									</ul>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</footer>
			</div>
			<!-- END FOOTER -->
		</div>
		<!-- /.wrapper -->
		<!-- END PAGE CONTENT -->

		<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
		<script src="js/jquery-2.2.4.min.js"></script>
		<script src="js/jquery-2.2.4.js"></script>
		<script src="js/datepicker/jquery-ui.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/bootstrap-select.min.js"></script>
		<script src="js/bootstrap-tabcollapse.js"></script>

		<script src="plugins/retina/retina.min.js"></script>
		<script src="plugins/nicescroll/jquery.nicescroll.js"></script>
		<script src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="plugins/backstretch/jquery.backstretch.min.js"></script>


		<!-- PLUGINS -->
		<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
		<script src="plugins/prettify/prettify.js"></script>
		<script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

		<script src="plugins/chosen/chosen.jquery.min.js"></script>
		<script src="plugins/icheck/icheck.min.js"></script>
		<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
		<script src="plugins/timepicker/bootstrap-timepicker.js"></script>
		<script src="plugins/mask/jquery.mask.min.js"></script>
		<script src="plugins/validator/bootstrapValidator.min.js"></script>
		<script src="plugins/datatable/js/jquery.dataTables.min.js"></script>
		<script src="plugins/datatable/js/bootstrap.datatable.js"></script>
		<script src="plugins/datatable/js/jquery.highlight.js"></script>
		<script src="plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
		<script src="plugins/summernote/summernote.min.js"></script>
		<script src="plugins/markdown/markdown.js"></script>
		<script src="plugins/markdown/to-markdown.js"></script>
		<script src="plugins/markdown/bootstrap-markdown.js"></script>
		<script src="plugins/slider/bootstrap-slider.js"></script>
		<script src="plugins/toastr/toastr.js"></script>
		<script src="plugins/newsticker/jquery.newsTicker.min.js"></script>
		<script src="plugins/placeholder/jquery.placeholder.js"></script>
		<script src="plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<script src="plugins/jquery-validation/dist/jquery.validate.js"></script>


		<!-- KNOB JS -->

		<script src="plugins/jquery-knob/jquery.knob.js"></script>
		<script src="plugins/jquery-knob/knob.js"></script>

		<!-- MAIN APPS JS -->
		<script type="text/javascript"
			src="plugins/canvasjs/source/canvasjs.js"></script>
		<script src="js/apps.js"></script>
		<script>
			var graphResp = "";
			function showGraph(id) {
				$.ajax({
							type : "GET",
							url : "PollsReportData?operation=pollDetails&pollID="
									+ id,
							cache : false,
							dataType : "json",
							success : function(rep) {
								graphResp = rep;
								if (graphResp.isSucess) {
									$('#pollsReport').hide();
									$('#pollsGraph').show();
									$('#totalVotes').html(
											"<span>Total votes :"
													+ graphResp.total
													+ "</span>");
									$('#reportFor').html(
											"<h5>Report of values for : "
													+ graphResp.question
													+ "</h5>");
									$('#pollName').html(
											"<h5><strong>Poll Name</strong> : "
													+ graphResp.question
													+ "</h5>");
									$('#reportFooter')
											.html(
													"* Number of times that appears each value. Percent in relation to the total of submissions. Date range "
															+ graphResp.first_entry
															+ " to "
															+ graphResp.last_entry);
									var chart = new CanvasJS.Chart(
											"chartContainer",
											{
												animationEnabled : true,
												title : {
													text : "Polls Results"
												},
												data : [ {
													type : "bar", //change type to bar, line, area, pie, etc
													dataPoints : graphResp.graphData
												} ]
											});

									chart.render();
								} else {
									alert("Sorry! No Data found for requested report.");
								}
							},
							error : function(data) {
								$('#pollsGraph').html("Some Error Occured");

							}
						});
				return false;
			}
			function showReport() {
				$('#pollsReport').show();
				$('#pollsGraph').hide();
			}
			function changeGraph(graphType) {
				var chart = new CanvasJS.Chart("chartContainer", {
					animationEnabled : true,
					title : {
						text : "Polls Results"
					},
					data : [ {
						type : graphType.value, //change type to bar, line, area, pie, etc
						dataPoints : graphResp.graphData
					} ]
				});

				chart.render();
			}
			function searchPolls() {
				var resp = "";
				$
						.ajax({
							type : "GET",
							url : "PollsReportData?operation=searchPolls&searchString="
									+ $('#searchText').val()
									+ "&searchStartDate="
									+ $('#datepicker1').val()
									+ "&searchEndDate="
									+ $('#datepicker2').val(),
							cache : false,
							dataType : "json",
							success : function(data) {
								console.log(data);
								if (data.isSuccess == "true") {
									jQuery
											.each(
													data.searchData,
													function(i, val) {
														console.log(i + "***"
																+ val);
														console.log(i + "***"
																+ val.question);
														resp = resp
																+ "<tr><td>"
																+ val.row
																+ "</td><td>"
																+ val.question
																+ "</td><td>"
																+ val.startdate
																+ "</td><td>"
																+ val.expirydate
																+ "</td><td><a onClick=\"showGraph('"
																+ val.pollid
																+ "')\" class=\"btn btn-default btn-sm\" >Report</a></td></tr>";
													});
								}
								$('#pollslisttable').html(resp);

							},
							error : function(data) {
								// alert("error");
								$('#pollslisttable').html("Some Error Occured");
							}
						});
				return false;
			}
		</script>
</body>
</html>
