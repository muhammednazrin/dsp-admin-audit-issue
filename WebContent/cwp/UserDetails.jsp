<%@ page language="java" contentType="text/html; charset=ISO-8859-1" 
 pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>ETIQA</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" >
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="no-cache">
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="no-store">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--  <script src="js/datepicker/jquery.min.js"></script>
        <script src="js/datepicker/jquery-ui.min.js"></script>-->
<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet" href="plugins/datepicker/datepicker.min.css">
<!-- PLUGINS CSS -->
<link href="plugins/owl-carousel/owl.carousel.min.css" rel="stylesheet">
<link href="plugins/owl-carousel/owl.theme.min.css" rel="stylesheet">
<link href="plugins/owl-carousel/owl.transitions.min.css"
	rel="stylesheet">
<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="css/owl.css" rel="stylesheet">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-select.css" rel="stylesheet">
<link href="plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="http://www.shieldui.com/shared/components/latest/css/light/all.min.css" />
<!--<link href="css/style.css" rel="stylesheet">-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}
</style>
<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}
</style>

<script language="javascript">
function editRecord(id){
    window.location.href="editrecord/"+id;
}
function deleteRecord(id){
    window.location.href="deleteUser/"+id; 
}
</script>

</head>
<body>
 <%
		if (session != null) {
			if (session.getAttribute("user") != null) {
				String name = (String) session.getAttribute("user");
				} else {
				response.sendRedirect("admin-login.jsp");
			}
		}
	%>
	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!-- header -->
		<jsp:include page="Header.jsp" />
		<!-- end header -->
		<!-- header second-->
		<div class="header-second">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 hidden-xs">
						<!-- Begin breadcrumb -->
						<ol class="breadcrumb">
							<li><a href="#"><i class="fa fa-home"></i></a></li>
							
							<li class="active">User AC Expired Report</li>
						</ol>
						<!-- End breadcrumb -->
					</div>

				</div>
			</div>
		</div>
		<!-- header second-->
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
				
				<jsp:include page="cwpmenu.jsp" />
				
				
				<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#">User Management - Users List</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11">
								<div class="row">
									<div class="col-sm-12">
										<div class="title">
											<div class="sub">
												<h4>User Management - Users List</h4>
											</div>
										</div>
										<div class="content-inner">
											<div class="the-box">
												<form name="rptuserForm" id="userrptForm" method="post"
													action="">
													<%-- 	<input type="hidden" name="CSRFToken"  value="<%=session.getAttribute("CSRFToken") %>" />
	 --%>
													<input value="Search" class="btn btn-default" type="submit" />

													<table id="admin-datatable-second"
														class="table table-striped table-hover">
														<thead>
															<tr>
																<th data-type="html">User Name</th>
																<th data-type="html">CREATION DATE</th>
																<th data-type="html">CREATED BY</th>
																<th data-type="html">IS ACTIVE</th>

															</tr>
														</thead>
														<tbody>
															<c:forEach items="${EditmyDataList}" var="UserRegRpt">
																<tr>
																	<td>${UserRegRpt.username}</td>
																	<td>${UserRegRpt.creationdate}</td>
																	<td>${UserRegRpt.createdby}</td>
																	<td>${UserRegRpt.isactive}</td>
																	<td>${UserRegRpt.response}</td>
																	<td><input type="button" name="edit" value="Edit"
																		class="btn btn-warning"
																		onclick="editRecord(${UserRegRpt.username});">

																	</td>
																</tr>
															</c:forEach>
														</tbody>
													</table>
												</form>

											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<!--No record found Invisible  -->
										<div id="noRecordFound"
											class="alert alert-danger fade in alert-dismissable hidediv">
											<!--   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X—</button> -->
											<strong><i class="fa fa-exclamation"></i> No record
												found</strong>
										</div>
										<!--No record found Invisible  -->
										<div class="content-inner">
											<div class="the-box static">
												<div class="title-second">
													<div class="sub">
														<h4>
															Report Details
															<c:if test="${not empty fromdate}"> From ${fromdate} - ${todate} </c:if>
														</h4>
													</div>
												</div>
												<div class="col-sm-12">
													<div class="form-horizontal" id="MyTable">
														<div id="dvData">
															<!--  <button id="exportButton" class="btn btn-lg btn-danger clearfix"><span class="fa fa-file-excel-o"></span> Export to Excel</button>-->

															<table id="exportTable"
																class="table table-striped table-hover"
																style="display: none;">
																<thead>
																	<tr>
																		<th data-type="html">User Name</th>
																		<th data-type="html">CREATION DATE</th>
																		<th data-type="html">CREATED BY</th>
																		<th data-type="html">IS ACTIVE</th>

																	</tr>
																</thead>
																<tbody>
																	<c:forEach items="${EditmyDataList}" var="UserRegRpt">
																		<tr>
																			<td>${UserRegRpt.username}</td>
																			<td>${UserRegRpt.creationdate}</td>
																			<td>${UserRegRpt.createdby}</td>
																			<td>${UserRegRpt.isactive}</td>
																			<td>${UserRegRpt.response}</td>
																		</tr>
																	</c:forEach>
																</tbody>
															</table>
														</div>
													</div>
													<div class="title-second">
														<div>
															<h4>
																Export to:

																<button id="exportButton" class="btn btn-warning">
																	<span class="fa fa-file-excel-o"></span>Excel
																</button>
																<button id="exportPDF" class="btn btn-warning">
																	<span class="fa fa-file-pdf-o"></span> PDF
																</button>
																<!--  <a href="#" onClick ="$('#admin-datatable-second').tableExport({type:'json',escape:'false'});">JSON</a>-->
															</h4>
														</div>

													</div>

												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.page-content -->
	<jsp:include page="Footer.jsp" />
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->
	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
	<script src="js/jquery-2.2.4.min.js"></script>
		<script src="js/jquery-2.2.4.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-select.min.js"></script>
	<script src="js/bootstrap-tabcollapse.js"></script>

	<script src="plugins/retina/retina.min.js"></script>
	<script src="plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="plugins/backstretch/jquery.backstretch.min.js"></script>


	<!-- PLUGINS -->
	<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="plugins/prettify/prettify.js"></script>
	<script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="plugins/chosen/chosen.jquery.min.js"></script>
	<script src="plugins/icheck/icheck.min.js"></script>
	<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="plugins/mask/jquery.mask.min.js"></script>
	<script src="plugins/validator/bootstrapValidator.min.js"></script>
	<script src="plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="plugins/datatable/js/jquery.highlight.js"></script>
	<script src="plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="plugins/summernote/summernote.min.js"></script>
	<script src="plugins/markdown/markdown.js"></script>
	<script src="plugins/markdown/to-markdown.js"></script>
	<script src="plugins/markdown/bootstrap-markdown.js"></script>
	<script src="plugins/slider/bootstrap-slider.js"></script>
	<script src="plugins/toastr/toastr.js"></script>
	<script src="plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="plugins/placeholder/jquery.placeholder.js"></script>
	<script src="plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script type="text/javascript"
		src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
	<script type="text/javascript"
		src="http://www.shieldui.com/shared/components/latest/js/jszip.min.js"></script>


	<!--  Table Export -->



	<!-- KNOB JS -->
	<!--[if IE]>
        <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
	<script src="plugins/jquery-knob/jquery.knob.js"></script>
	<script src="plugins/jquery-knob/knob.js"></script>

	<!-- MAIN APPS JS -->
	<script src="js/apps.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			// Start------------------------------Validation of Search Form 
			$("#rptForm").validate({
				rules : {
					todate : {
						required : true,
					},
				},
				messages : {
					todate : {
						required : "Todate is Required"
					},
				}
			});
			// End------------------------------Validation of Search Form  
			$("#fromdate").datepicker({
				autoclose : true,
				format : 'dd-mm-yyyy'
			});
			$("#todate").datepicker({
				autoclose : true,
				format : 'dd-mm-yyyy'
			});
			// Start------------------------------Submission of Search Form    
		});
	</script>
	<!-- you need to include the shieldui css and js assets in order for the components to work -->
	<script type="text/javascript">
		jQuery(function($) {
			$("#exportButton").click(function() {
				// parse the HTML table element having an id=exportTable
				var dataSource = shield.DataSource.create({
					data : "#exportTable",
					schema : {
						type : "table",
						fields : {
							customername : {
								type : String
							},
							IC_Number : {
								type : String
							},
							policy_number : {
								type : String
							},
							last_update : {
								type : String
							},
							mobile : {
								type : String
							},
							email_address : {
								type : String
							}
						}
					}
				});
				// when parsing is done, export the data to Excel
				dataSource.read().then(function(data) {
					new shield.exp.OOXMLWorkbook({
						author : "PrepBootstrap",
						worksheets : [ {
							name : "PrepBootstrap Table",
							rows : [ {
								cells : [ {
									style : {
										bold : true
									},
									type : String,
									value : "CustomerName"
								}, {
									style : {
										bold : true
									},
									type : String,
									value : "IC_Number"
								}, {
									style : {
										bold : true
									},
									type : String,
									value : "Policy_Number"
								}, {
									style : {
										bold : true
									},
									type : String,
									value : "Last_Update"
								}, {
									style : {
										bold : true
									},
									type : String,
									value : "Mobile"
								}, {
									style : {
										bold : true
									},
									type : String,
									value : "Email_Address"
								} ]
							} ].concat($.map(data, function(item) {

								return {
									cells : [ {
										type : String,
										value : item.customername
									}, {
										type : String,
										value : item.IC_Number
									}, {
										type : String,
										value : item.policy_number
									}, {
										type : String,
										value : item.last_update
									}, {
										type : String,
										value : item.mobile
									}, {
										type : String,
										value : item.email_address
									} ]
								};
							}))
						} ]
					}).saveAs({
						fileName : "UserRegReportExcel"
					});
				});
			});
		});

		//Export to PDF
		jQuery(function($) {
			$("#exportPDF").click(function() {
				// parse the HTML table element having an id=exportTable
				var dataSource = shield.DataSource.create({
					data : "#exportTable",
					schema : {
						type : "table",
						fields : {
							CustomerName : {
								type : String
							},
							IC_Number : {
								type : String
							},
							Policy_Number : {
								type : String
							},
							Last_Update : {
								type : String
							},
							Mobile : {
								type : String
							},
							Email_Address : {
								type : String
							}
						}
					}
				});

				// when parsing is done, export the data to PDF
				dataSource.read().then(function(data) {
					var pdf = new shield.exp.PDFDocument({
						author : "PrepBootstrap",
						created : new Date()
					});

					pdf.addPage("a4", "portrait");

					pdf.table(50, 50, data, [ {
						field : "CustomerName",
						title : "CustomerName",
						width : 80
					}, {
						field : "IC_Number",
						title : "IC_Number",
						width : 85
					}, {
						field : "Policy_Number",
						title : "Policy_Number",
						width : 80
					}, {
						field : "Last_Update",
						title : "Last_Update",
						width : 80
					}, {
						field : "Mobile",
						title : "Mobile",
						width : 100
					}, {
						field : "Email_Address",
						title : "Email_Address",
						width : 100
					} ], {
						margins : {
							top : 50,
							left : 50
						}
					});

					pdf.saveAs({
						fileName : "UserRegReportPDF"
					});
				});
			});
		});
	</script>
	<style>
#exportButton {
	border-radius: 0;
}
</style>
</body>
</html>