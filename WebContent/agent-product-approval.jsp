<%@ page import="java.util.List"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.etiqa.model.report.TransactionalReport"%>
<%@ page import="java.io.*,java.util.*"%>
<%@ page import="javax.servlet.*,java.text.*"%>
<%@ page import="java.util.regex.Pattern" %>
<%@ page import="com.etiqa.utils.AddCodes" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<jsp:useBean id="obj" class="com.etiqa.utils.SecurityUtil" />

<%
    String apptpe ="";
   if(request.getParameter("appType")!=null){
       apptpe =obj.stripXSS(request.getParameter("appType").toString());
   }
 
 %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/js/jquery.dataTables.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/buttons.dataTables.min.css" rel="stylesheet"
	type="text/css">
<!-- <link href="assets/js/bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link href="assets/js/dataTables.bootstrap.min.css" rel="stylesheet"
	type="text/css"> -->
<link href="assets/js/buttons.bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link rel="stylesheet" href="assets/css/toastr.min.css">
<link rel="stylesheet" href="assets/jAlert/jAlert.css">
<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}
</style>


<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}
</style>
<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
<!-- <link href="assets/css/bootstrap.min.css" rel="stylesheet"> -->
<!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->
<link rel="stylesheet"
	href="assets/plugins/datepicker/datepicker.min.css">
<!-- PLUGINS CSS -->
<link href="assets/plugins/owl-carousel/owl.carousel.min.css"
	rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.theme.min.css"
	rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.transitions.min.css"
	rel="stylesheet">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<!-- <link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" /> -->


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->


<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}

form#agentE, form#agentD {
	display: inline-block;
}

form#agentE input[type=submit], form#agentD input[type=submit] {
	padding: 5px;
}
</style>


</head>

<body>
	<c:set var="OData" value='<%=AddCodes.OData%>'/>
	<c:set var="MCode" value='<%=AddCodes.MCode%>'/>
	<c:set var="QCode" value='<%=AddCodes.QCode%>'/>
	<c:set var="WTCCode" value='<%=AddCodes.WTCCode%>'/>
	<c:set var="WTCRateCode" value='<%=AddCodes.WTCRateCode%>'/>
	<c:set var="HOHHCode" value='<%=AddCodes.HOHHCode%>'/>
	<c:set var="HOHHTCode" value='<%=AddCodes.HOHHTCode%>'/>
	<c:set var="MIINFOCode" value='<%=AddCodes.MIINFOCode%>'/>
	<c:set var="MTINFOCode" value='<%=AddCodes.MTINFOCode%>'/>
	<c:set var="ELSINFOCode" value='<%=AddCodes.ELSINFOCode%>'/>
	<c:set var="HOHHINFOCode" value='<%=AddCodes.HOHHINFOCode%>'/>
	<c:set var="MIRules1" value='<%=AddCodes.MIRules1%>'/>
	<c:set var="MIRules2" value='<%=AddCodes.MIRules2%>'/>
	<c:set var="MIRules3" value='<%=AddCodes.MIRules3%>'/>
	<c:set var="MIRules4" value='<%=AddCodes.MIRules4%>'/>
	<c:set var="ELSOData" value='<%=AddCodes.ELSOData%>'/>
	<c:set var="MIRCode" value='<%=AddCodes.MIRCode%>'/>
	<c:set var="BOCode" value='<%=AddCodes.BOCode%>'/>
	<c:set var="BCCode" value='<%=AddCodes.BCCode%>'/>
	<c:set var="BBCode" value='<%=AddCodes.BBCode%>'/>
	<c:set var="BBGCode" value='<%=AddCodes.BBGCode%>'/>
	<c:set var="BPCode" value='<%=AddCodes.BPCode%>'/>
	<c:set var="TEZCode" value='<%=AddCodes.TEZCode%>'/>
	<c:set var="TCPCode" value='<%=AddCodes.TCPCode%>'/>
	<c:set var="TCPRCode" value='<%=AddCodes.TCPRCode%>'/>
	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!---------------------------------------------------- BEGIN Page Header--------------------------------------------------------- -->
		<jsp:include page="pageHeader.jsp" />
		<!----------------------------------------------------- END Page Header ------------------------------------------------------------>
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<!---------------------------------------------------- BEGIN MENU--------------------------------------------------------- -->
					<jsp:include page="menu1.jsp" />
					<!----------------------------------------------------- END MENU ------------------------------------------------------------>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
																		
												<li><a href="#fakelink">Agent Product Management</a></li>
											</ol>
											<!-- End breadcrumb -->

										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">


												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">

															<div class="content-inner">
																<div class="the-box">
																	<!-- Start Form -->
																	<!-- Start Form  search crietria by pramaiyan 25022018-->

																	<%--     <input type="hidden" name="CSRFToken"  value="<%=session.getAttribute("CSRFToken") %>" /> --%>

																	<form name="approval" id="approval" method="get"
																		action="">

																		<section class="content bg-white">
																			<div class="row">
																				<div class="col-md-6 mb-3">
																					<h4 class="text-center">Original</h4>

																					<c:set var="appId"
																						value='<%=apptpe%>'>
																					</c:set>
																					<c:out value="${appId}" />
																					<%
																					try{
																					String approveStatusVal = request.getParameter("appstatus").toString();
																					if(approveStatusVal !=null)
																					{
																					String decryptMsg =obj.decriptString(approveStatusVal); 
																					pageContext.setAttribute("approveStatus", decryptMsg);
																					System.out.print("decryptMsg >>>"+decryptMsg);
																					}
																					}catch(Exception e)
																					{
																						System.out.println("Problem In Decryption");
																					}
																					%>
																					<c:if test="${appId eq OData}">

																						<table
																							class="table table-striped table-warning table-hover table-outline">
																							<tbody align="center">
																								<tr>
																									<td align="left"><strong>Product</strong></td>
																									<td><strong>Type</strong></td>
																									<td><strong>Entity</strong></td>
																									<td><strong>Agent Code</strong></td>
																									<td><strong>Discount (%)</strong></td>
																									<td><strong>Commission (%)</strong></td>
																								</tr>
																								<c:if test="${not empty listOriginalData}">

																									<c:forEach items="${listOriginalData}"
																										var="element" varStatus="loop">
																										<tr>
																											<td align='left'>${element.productCode}
																											</td>

																											<td align='left'>${element.agentType}</td>

																											<td align='left'>${element.agentEntity}
																											</td>

																											<td align='left'>${element.agentCode}</td>

																											<td align='left'>${element.discount}</td>

																											<td align='left'>${element.commission}</td>
																										</tr>
																									</c:forEach>
																								</c:if>

																							</tbody>
																						</table>

																					</c:if>


																					<c:if test="${appId eq MCode}">

																						<section class="content bg-white">
																							<div class="row">
																								<div class="col-md-6 mb-3">
																									<table
																										class="table table-striped table-warning table-hover table-outline">
																										<tbody>
																											<c:if
																												test="${not empty originalMotorRatesValues}">
																												<c:forEach
																													items="${originalMotorRatesValues}"
																													var="element" varStatus="loop">
																													<tr>
																														<td width="400" align="right">Sum
																															Covered Limit(Min.)</td>
																														<td width="5">:</td>
																														<td width="400">${originalMotorRatesValues.sumCoveredMin}</td>
																													</tr>
																													<tr>
																														<td align="right">Sum Covered
																															Limit(Max.)</td>
																														<td width="5">:</td>
																														<td>${originalMotorRatesValues.sumCoveredMax}</td>
																													</tr>
																													<tr>
																														<td align="right">Direct Discount
																															(Online)</td>
																														<td width="5">:</td>
																														<td>${element.directDiscount}</td>
																													</tr>
																													<tr>
																														<td align="right">GST</td>
																														<td width="5">:</td>
																														<td>6</td>
																													</tr>

																													<tr>

																														<td align="right">Direct Discount
																															(Online)</td>
																														<td width="5">:</td>
																														<td>${element.capsDiscount}</td>
																													</tr>

																													<tr>
																														<td align="right">Fee(RM)</td>
																														<td width="5">:</td>
																														<td>${element.capsFee}</td>
																													</tr>

																													<tr>
																														<td align="right">5 seater incl.
																															driver</td>
																														<td width="5">:</td>
																														<td>${element.fiveSeater}</td>
																													</tr>
																													<tr>
																														<td align="right">7 seater incl.
																															driver</td>
																														<td width="5">:</td>
																														<td>${element.sevenSeater}</td>
																													</tr>

																													<tr>
																														<td align="right">GST</td>
																														<td width="5">:</td>
																														<td>${element.gst}</td>
																													</tr>
																													<tr>
																														<td align="right">Stamp Duty</td>
																														<td width="5">:</td>
																														<td>${element.stampDuty}</td>
																													</tr>

																													<tr>
																														<td align="right">Maximum No. of
																															Drivers</td>
																														<td width="5">:</td>
																														<td>${element.maxDrivers}</td>
																													</tr>
																													<tr>
																														<td align="right">Amount for 3rd
																															driver or more</td>
																														<td width="5">:</td>
																														<td>${element.thirdDriver}</td>
																													</tr>

																													<tr>
																														<td align="right">Minimum Age</td>
																														<td width="5">:</td>
																														<td>${element.minAge}</td>
																													</tr>
																													<tr>
																														<td align="right">Maximum Age</td>
																														<td width="5">:</td>
																														<td>${element.maxAge}</td>
																													</tr>
																												</c:forEach>
																											</c:if>
																										</tbody>
																									</table>
																								</div>

																							</div>
																						</section>

																					</c:if>


																					<c:if test="${appId eq QCode}">
																						<input type="hidden" name="uId" id="id"
																							value="${qarec.id}">
																						<table
																							class="table table-striped table-warning table-hover table-outline">
																							<tbody align="center">
																								<tr>
																									<td align="left"><strong>Security
																											Question</strong></td>
																									<td><strong>Security Question BM</strong></td>
																									<td><strong>Compulsory Answer</strong></td>
																								</tr>
																								<c:if test="${not empty listOriginalWTCRules}">

																									<c:forEach items="${listOriginalWTCRules}"
																										var="element" varStatus="loop">
																										<tr>
																											<td align='left'>
																												${element.questionDescription}</td>

																											<td align='left'>
																												${element.questionDescriptionBm }</td>

																											<td align='left'>
																												${element.correctAnswerCode }</td>

																										</tr>
																									</c:forEach>
																								</c:if>
																							</tbody>
																						</table>
																					</c:if>

																					<c:if test="${appId eq WTCCode}">
																						<input type="hidden" name="uId" id="id"
																							value="${qarec.id}">
																						<table
																							class="table table-striped table-warning table-hover table-outline">
																							<tbody align="center">
																								<tr>
																									<td align="left"><strong>Sales
																											target</strong></td>
																									<td><strong>Quota Validity</strong></td>
																								</tr>
																								<c:if
																									test="${not empty listOriginalWTCProductInfo}">

																									<c:forEach
																										items="${listOriginalWTCProductInfo}"
																										var="element" varStatus="loop">
																										<tr>
																											<td align='left'><c:out
																													value="${element.annualTarget}" /></td>

																											<td align='left'><c:out
																													value="${element.validity}" /></td>

																										</tr>
																									</c:forEach>
																								</c:if>
																							</tbody>
																						</table>
																					</c:if>


																					<c:if test="${appId eq WTCRateCode}">
																						<input type="hidden" name="uId" id="id"
																							value="${qarec.id}">
																						<c:choose>
																							<c:when test="${wtcprodtype=='0'}">
																								<table
																									class="table table-striped table-warning table-hover table-outline">
																									<tbody align="center">
																										<tr>
																											<td align="left"><strong>WTC
																													Discount </strong></td>
																											<td><strong>WTC Gst</strong></td>
																											<td><strong>WTC Stamp Duty</strong></td>
																										</tr>
																										<c:if
																											test="${not empty listOriginalWTCProductRate}">

																											<c:forEach
																												items="${listOriginalWTCProductRate}"
																												var="element" varStatus="loop">
																												<tr>
																													<td align='left'><c:out
																															value="${element.wtcDiscount}" /></td>

																													<td align='left'><c:out
																															value="${element.wtcGst}" /></td>

																													<td align='left'><c:out
																															value="${element.wtcStampDuty}" /></td>

																												</tr>
																											</c:forEach>
																										</c:if>
																									</tbody>
																								</table>
																							</c:when>
																							<c:otherwise>
																								<c:choose>
																									<c:when test="${wtcprodtype=='D'}">

																										<table
																											class="table table-striped table-warning table-hover table-outline">
																											<tbody align="center">
																												<tr>
																													<td align="left"><strong>WTC
																															Premium Type </strong></td>
																													<td><strong>WTC Plan</strong></td>
																													<td><strong>WTC Premium Value</strong></td>
																												</tr>
																												<c:if
																													test="${not empty listOriginalWTCProductRate}">

																													<c:forEach
																														items="${listOriginalWTCProductRate}"
																														var="element" varStatus="loop">
																														<tr>
																															<td align='left'><c:out
																																	value="WTC Domestic" /></td>

																															<td align='left'><c:out
																																	value="${element.daysRangeKeycode}" /></td>

																															<td align='left'><c:out
																																	value="${element.premiumValue}" /></td>

																														</tr>
																													</c:forEach>
																												</c:if>
																											</tbody>
																										</table>


																									</c:when>
																									<c:otherwise>


																										<table
																											class="table table-striped table-warning table-hover table-outline">
																											<tbody align="center">
																												<tr>
																													<td align="left"><strong>WTC
																															Premium Type </strong></td>
																													<td><strong>WTC Plan</strong></td>
																													<td><strong>WTC Premium Value</strong></td>
																												</tr>
																												<c:if
																													test="${not empty listOriginalWTCProductRate}">

																													<c:forEach
																														items="${listOriginalWTCProductRate}"
																														var="element" varStatus="loop">
																														<tr>
																															<td align='left'><c:out
																																	value="WTC International" /></td>

																															<td align='left'><c:out
																																	value="${element.daysRangeKeycode}" /></td>

																															<td align='left'><c:out
																																	value="${element.premiumValue}" /></td>

																														</tr>
																													</c:forEach>
																												</c:if>
																											</tbody>
																										</table>

																									</c:otherwise>
																								</c:choose>
																							</c:otherwise>
																						</c:choose>
																					</c:if>






																					<c:if test="${appId eq HOHHCode}">

																						<section class="content bg-white">
																							<div class="row">
																								<div class="">
																									<table
																										class="table table-striped table-warning table-hover table-outline">
																										<tbody>
																											<c:if
																												test="${not empty listHoHHInsOriginalData}">

																												<c:forEach
																													items="${listHoHHInsOriginalData}"
																													var="element" varStatus="loop">

																													<tr>
																														<td width="400" align="right">Direct
																															Discount (Online)</td>
																														<td width="5">:</td>
																														<td>${element.directDiscount}</td>
																													</tr>
																													<tr>
																														<td align="right">GST</td>
																														<td width="5">:</td>
																														<td>${element.gst}</td>
																													</tr>

																													<tr>

																														<td align="right">Minimum Coverage
																															Amount</td>
																														<td width="5">:</td>
																														<td>${element.minAmountBuild}</td>
																													</tr>
																													<tr>
																														<td align="right">Maximum Coverage
																															Amount</td>
																														<td width="5">:</td>
																														<td>${element.maxAmountBuild}</td>
																													</tr>

																													<tr>

																														<td align="right">Riot, Strike and
																															Malicious Damage (Home Building)</td>
																														<td width="5">:</td>
																														<td>${element.riot}</td>
																													</tr>

																													<tr>
																														<td align="right">Extended Theft
																															Cover (Home Contents)</td>
																														<td width="5">:</td>
																														<td>${element.theft}</td>
																													</tr>

																													<tr>
																														<td align="right">Stamp Duty</td>
																														<td width="5">:</td>
																														<td>${element.stampDuty}</td>
																													</tr>
																													<tr>
																														<td align="right">Minimum Coverage
																															Amount</td>
																														<td width="5">:</td>
																														<td>${element.minAmountContent}</td>
																													</tr>

																													<tr>
																														<td align="right">Maximum Coverage
																															Amount</td>
																														<td width="5">:</td>
																														<td>${element.maxAmountContent}</td>
																													</tr>


																												</c:forEach>
																											</c:if>
																										</tbody>
																									</table>
																								</div>

																							</div>
																						</section>

																					</c:if>

																					<c:if test="${appId eq HOHHTCode}">

																						<section class="content bg-white">
																							<div class="row">
																								<div class="">
																									<table
																										class="table text_center table-striped table-warning table-hover table-outline">
																										<tbody>
																											<c:if
																												test="${not empty listHoHHTakafulOriginalData}">

																												<c:forEach
																													items="${listHoHHTakafulOriginalData}"
																													var="element" varStatus="loop">

																													<tr>
																														<td width="400" align="right">Direct
																															Discount (Online)</td>
																														<td width="5">:</td>
																														<td>${element.directDiscount}</td>
																													</tr>
																													<tr>
																														<td align="right">GST</td>
																														<td width="5">:</td>
																														<td>${element.gst}</td>
																													</tr>

																													<tr>

																														<td align="right">Minimum Coverage
																															Amount</td>
																														<td width="5">:</td>
																														<td>${element.minAmountBuild}</td>
																													</tr>
																													<tr>
																														<td align="right">Maximum Coverage
																															Amount</td>
																														<td width="5">:</td>
																														<td>${element.maxAmountBuild}</td>
																													</tr>

																													<tr>

																														<td align="right">Riot, Strike and
																															Malicious Damage (Home Building)</td>
																														<td width="5">:</td>
																														<td>${element.riot}</td>
																													</tr>

																													<tr>
																														<td align="right">Extended Theft
																															Cover (Home Contents)</td>
																														<td width="5">:</td>
																														<td>${element.theft}</td>
																													</tr>

																													<tr>
																														<td align="right">Stamp Duty</td>
																														<td width="5">:</td>
																														<td>${element.stampDuty}</td>
																													</tr>
																													<tr>
																														<td align="right">Minimum Coverage
																															Amount</td>
																														<td width="5">:</td>
																														<td>${element.minAmountContent}</td>
																													</tr>

																													<tr>
																														<td align="right">Maximum Coverage
																															Amount</td>
																														<td width="5">:</td>
																														<td>${element.maxAmountContent}</td>
																													</tr>


																												</c:forEach>
																											</c:if>
																										</tbody>
																									</table>
																								</div>

																							</div>
																						</section>

																					</c:if>



																					<c:if test="${appId eq MIINFOCode}">

																						<section class="content bg-white">
																							<div class="row">
																								<div class="">
																									<table
																										class="table text-center table-striped table-warning table-hover table-outline">
																										<tbody>
																											<c:if
																												test="${not empty listOriginalMIProductInfo}">

																												<c:forEach
																													items="${listOriginalMIProductInfo}"
																													var="element" varStatus="loop">

																													<tr>
																														<td width="400" align="right">Product
																															Code</td>
																														<td width="5">:</td>
																														<td>MI</td>
																													</tr>
																													<tr>
																														<td align="right">Product Name</td>
																														<td width="5">:</td>
																														<td>Motor Insurance</td>
																													</tr>


																													<tr>


																														<td align="right">Annual Sales Target</td>
																														<div class="form-group"></div>
																														<td width="5">:</td>
																														<td>${element.annualTarget}</td>
																													</tr>
																													<tr>
																														<td align="right">Quotation Validity</td>
																														<td width="5">:</td>
																														<td>${element.validity}</td>
																													</tr>



																												</c:forEach>
																											</c:if>
																										</tbody>
																									</table>
																								</div>

																							</div>
																						</section>

																					</c:if>


																					<c:if test="${appId eq MTINFOCode}">

																						<section class="content bg-white">
																							<div class="row">
																								<div class="">
																									<table
																										class="table text-center table-striped table-warning table-hover table-outline">
																										<tbody>
																											<c:if
																												test="${not empty listOriginalMTProductInfo}">

																												<c:forEach
																													items="${listOriginalMTProductInfo}"
																													var="element" varStatus="loop">

																													<tr>
																														<td width="400" align="right">Product
																															Code</td>
																														<td width="5">:</td>
																														<td>MT</td>
																													</tr>
																													<tr>
																														<td align="right">Product Name</td>
																														<td width="5">:</td>
																														<td>Motor Takaful</td>
																													</tr>


																													<tr>


																														<td align="right">Annual Sales Target</td>
																														<div class="form-group"></div>
																														<td width="5">:</td>
																														<td>${element.annualTarget}</td>
																													</tr>
																													<tr>
																														<td align="right">Quotation Validity</td>
																														<td width="5">:</td>
																														<td>${element.validity}</td>
																													</tr>



																												</c:forEach>
																											</c:if>
																										</tbody>
																									</table>
																								</div>

																							</div>
																						</section>

																					</c:if>

																					<c:if test="${appId eq ELSINFOCode}">

																						<section class="content bg-white">
																							<div class="row">
																								<div class="">
																									<table
																										class="table text-center table-striped table-warning table-hover table-outline">
																										<tbody>
																											<c:if
																												test="${not empty listOriginalELSProductInfo}">

																												<c:forEach
																													items="${listOriginalELSProductInfo}"
																													var="element" varStatus="loop">

																													<tr>
																														<td width="400" align="right">Product
																															Code</td>
																														<td width="5">:</td>
																														<td>TL</td>
																													</tr>
																													<tr>
																														<td align="right">Product Name</td>
																														<td width="5">:</td>
																														<td>Ezy-Life Secure</td>
																													</tr>


																													<tr>


																														<td align="right">Annual Sales Target</td>
																														<div class="form-group"></div>
																														<td width="5">:</td>
																														<td>${element.annualTarget}</td>
																													</tr>
																													<tr>
																														<td align="right">Quotation Validity</td>
																														<td width="5">:</td>
																														<td>${element.validity}</td>
																													</tr>



																												</c:forEach>
																											</c:if>
																										</tbody>
																									</table>
																								</div>

																							</div>
																						</section>

																					</c:if>




																					<c:if test="${appId eq HOHHINFOCode}">

																						<section class="content bg-white">
																							<div class="row">
																								<div class="">
																									<table
																										class="table text-center table-striped table-warning table-hover table-outline">
																										<tbody>
																											<c:if
																												test="${not empty listOriginalHOHHProductInfo}">

																												<c:forEach
																													items="${listOriginalHOHHProductInfo}"
																													var="element" varStatus="loop">

																													<tr>
																														<td width="400" align="right">Product
																															Code</td>
																														<td width="5">:</td>
																														<td>HOHH</td>
																													</tr>
																													<tr>
																														<td align="right">Product Name</td>
																														<td width="5">:</td>
																														<td>Houseowner/Householder</td>
																													</tr>


																													<tr>


																														<td align="right">Annual Sales Target</td>
																														<div class="form-group"></div>
																														<td width="5">:</td>
																														<td>${element.annualTarget}</td>
																													</tr>
																													<tr>
																														<td align="right">Quotation Validity</td>
																														<td width="5">:</td>
																														<td>${element.validity}</td>
																													</tr>



																												</c:forEach>
																											</c:if>
																										</tbody>
																									</table>
																								</div>

																							</div>
																						</section>

																					</c:if>



																					<c:if
																						test="${(appId eq MIRules1) || (appId eq MIRules2) || (appId eq MIRules3) || (appId eq MIRules4) }">

																						<input type="hidden" name="uId" id="id"
																							value="${qarec.id}">
																						<table
																							class="table table-striped table-warning table-hover table-outline">
																							<tbody align="center">
																								<tr>
																									<td align="left"><strong>Security
																											Question</strong></td>
																									<td><strong>Security Question BM</strong></td>
																									<td><strong>Compulsory Answer</strong></td>
																								</tr>
																								</tr>
																								<c:if test="${not empty listOriginalMIRules}">

																									<c:forEach items="${listOriginalMIRules}"
																										var="element" varStatus="loop">
																										<tr>
																											<td align='left'>
																												${element.questionDescription}</td>

																											<td align='left'>
																												${element.questionDescriptionBm }</td>

																											<td align='left'>
																												${element.correctAnswerCode }</td>

																										</tr>
																									</c:forEach>
																								</c:if>
																							</tbody>
																						</table>
																					</c:if>





																					<!-- Rule Management Original value Listing start for MI Original  -->
																					<c:if test="${appId eq ELSOData}">

																						<section class="content bg-white">
																							<div class="row">
																								<div class="">
																									<table
																										class="table text_center table-striped table-warning table-hover table-outline">
																										<tbody>
																											<c:if test="${not empty listELSOriginalData}">

																												<c:forEach items="${listELSOriginalData}"
																													var="element" varStatus="loop">

																													<tr>
																														<td width="400" align="right">Direct
																															Discount (Online)</td>
																														<td width="5">:</td>
																														<td>${element.discVal}</td>
																													</tr>
																													<tr>
																														<td align="right">GST</td>
																														<td width="5">:</td>
																														<td>${element.gstVal}</td>
																													</tr>

																													<tr>
																														<td align="right">Stamp Duty</td>
																														<td width="5">:</td>
																														<td>${element.stampDuty}</td>
																													</tr>

																													<tr>
																														<td align="right">Minimum Age</td>
																														<td width="5">:</td>
																														<td>${element.minAge}</td>
																													</tr>

																													<tr>
																														<td align="right">Minimum Benefit
																															Amount</td>
																														<td width="5">:</td>
																														<td>${element.minAmt}</td>
																													</tr>

																													<tr>

																														<td align="right">Maximum Benefit
																															Amount</td>
																														<td width="5">:</td>
																														<td>${element.maxAmt}</td>
																													</tr>
																													<tr>
																														<td align="right">Total Sum Assured
																															at Risk(TSAR)</td>
																														<td width="5">:</td>
																														<td>${element.tsarVal}</td>
																													</tr>

																													<tr>

																														<td align="right">Maximum Age</td>
																														<td width="5">:</td>
																														<td>${element.maxAge}</td>
																													</tr>


																												</c:forEach>
																											</c:if>
																										</tbody>
																									</table>
																								</div>

																							</div>
																						</section>

																					</c:if>



																					<!-- MT Product Rate original  list 21032018 -->

																					<c:if test="${appId eq MIRCode}">

																						<section class="content bg-white">
																							<div class="row">
																								<div class="col-md-6 mb-3">
																									<table
																										class="table table-striped table-warning table-hover table-outline">
																										<tbody>
																											<c:if
																												test="${not empty originalMotorRatesValues}">
																												<tr>
																													<td width="400" align="right">Sum
																														Covered Limit(Min.)</td>
																													<td width="5">:</td>
																													<td width="400">${originalMotorRatesValues.sumCoveredMin}</td>
																												</tr>
																												<tr>
																													<td align="right">Sum Covered
																														Limit(Max.)</td>
																													<td width="5">:</td>
																													<td>${originalMotorRatesValues.sumCoveredMax}</td>
																												</tr>
																												<tr>
																													<td align="right">Direct Discount
																														(Online)</td>
																													<td width="5">:</td>
																													<td>${originalMotorRatesValues.directDiscount}</td>
																												</tr>
																												<tr>

																													<td align="right">Direct Discount
																														(Online)</td>
																													<td width="5">:</td>
																													<td>${originalMotorRatesValues.capsDiscount}</td>
																												</tr>
																												<tr>
																													<td align="right">Fee(RM)</td>
																													<td width="5">:</td>
																													<td>${originalMotorRatesValues.capsFee}</td>
																												</tr>
																												<tr>
																													<td align="right">5 seater incl.
																														driver</td>
																													<td width="5">:</td>
																													<td>${originalMotorRatesValues.fiveSeater}</td>
																												</tr>
																												<tr>
																													<td align="right">7 seater incl.
																														driver</td>
																													<td width="5">:</td>
																													<td>${originalMotorRatesValues.sevenSeater}</td>
																												</tr>

																												<%-- <tr>
																														<td align="right">GST</td>
																														<td width="5">:</td>
																														<td>${originalMotorRatesValues.gst}</td>
																													</tr>
																													<tr>
																														<td align="right">Stamp Duty</td>
																														<td width="5">:</td>
																														<td>${originalMotorRatesValues.stampDuty}</td>
																													</tr> --%>

																												<tr>
																													<td align="right">Maximum No. of
																														Drivers</td>
																													<td width="5">:</td>
																													<td>${originalMotorRatesValues.maxDrivers}</td>
																												</tr>
																												<tr>
																													<td align="right">Amount for 3rd
																														driver or more</td>
																													<td width="5">:</td>
																													<td>${originalMotorRatesValues.thirdDriver}</td>
																												</tr>
																												<tr>
																													<td align="right">Minimum Age</td>
																													<td width="5">:</td>
																													<td>${originalMotorRatesValues.minAge}</td>
																												</tr>
																												<tr>
																													<td align="right">Maximum Age</td>
																													<td width="5">:</td>
																													<td>${originalMotorRatesValues.maxAge}</td>
																												</tr>
																												<tr>
																													<td align="right">Motor SST</td>
																													<td width="5">:</td>
																													<td>${originalMotorRatesValues.motorTakafulSst}</td>
																												</tr>
																												<tr>
																													<td align="right">Motor SST Effective
																														Date</td>
																													<td width="5">:</td>
																													<td><fmt:formatDate
																															pattern="dd/MM/yyyy"
																															value="${originalMotorRatesValues.motorTakafulSstEffectiveDate}" />
																													</td>
																												</tr>

																												<tr>
																													<td align="right">DPPA SST</td>
																													<td width="5">:</td>
																													<td>${originalMotorRatesValues.dppa_sst}</td>
																												</tr>
																												<tr>
																													<td align="right">DPPA SST Effective
																														Date</td>
																													<td width="5">:</td>
																													<td><fmt:formatDate
																															pattern="dd/MM/yyyy"
																															value="${originalMotorRatesValues.dppa_sst_effective_date}" /></td>
																												</tr>
																												<tr>
																													<td align="right">DPPA Stamp Duty</td>
																													<td width="5">:</td>
																													<td>${originalMotorRatesValues.dppa_stampduty}</td>
																												</tr>
																												<tr>
																													<td align="right">OTO360 SST</td>
																													<td width="5">:</td>
																													<td>${originalMotorRatesValues.oto_sst}</td>
																												</tr>
																												<tr>
																													<td align="right">OTO360 SST Effective
																														Date</td>
																													<td width="5">:</td>
																													<td><fmt:formatDate
																															pattern="dd/MM/yyyy"
																															value="${originalMotorRatesValues.oto_sst_effective_date}" /></td>
																												</tr>
																												<tr>
																													<td align="right">OTO360 Stamp Duty</td>
																													<td width="5">:</td>
																													<td>${originalMotorRatesValues.oto_stampduty}</td>
																												</tr>
																											</c:if>
																										</tbody>
																									</table>
																								</div>

																							</div>
																						</section>

																					</c:if>

																					<!-- MT Product Rate original end   list 21032018 -->
																					<!-- BUDDY PA START -->

																					<c:if test="${appId eq BOCode}">
																						<input type="hidden" name="uId" id="id"
																							value="${qarec.id}">
																						<table
																							class="table table-striped table-warning table-hover table-outline">
																							<tbody align="center">
																								<c:if test="${not empty listOriginalBuddyPlan}">
																									<c:forEach items="${listOriginalBuddyPlan}"
																										var="element" varStatus="loop">
																										<tr>
																											<td width="400" align="right">Plan ID</td>
																											<td width="5">:</td>
																											<td>${element.planId}</td>
																										</tr>
																										<tr>
																											<td align="right">Plan Name (EN)</td>
																											<td width="5">:</td>
																											<td>${element.nameEn}</td>
																										</tr>
																										<tr>
																											<td align="right">Plan Name (BM)</td>
																											<td width="5">:</td>
																											<td>${element.nameBm}</td>
																										</tr>
																										<tr>
																											<td align="right">Enable</td>
																											<td width="5">:</td>
																											<td>${element.enable}</td>
																										</tr>
																									</c:forEach>
																								</c:if>
																							</tbody>
																						</table>
																					</c:if>

																					<c:if test="${appId eq BCCode}">
																						<input type="hidden" name="uId" id="id"
																							value="${qarec.id}">
																						<table
																							class="table table-striped table-warning table-hover table-outline">
																							<tbody align="center">
																								<c:if test="${not empty listOriginalBuddyCombo}">
																									<c:forEach items="${listOriginalBuddyCombo}"
																										var="element" varStatus="loop">
																										<tr>
																											<td width="400" align="right">Company</td>
																											<td width="5">:</td>
																											<td>${element.companyId}</td>
																										</tr>
																										<tr>
																											<td align="right">Combo Name</td>
																											<td width="5">:</td>
																											<td>${element.name}</td>
																										</tr>
																										<tr>
																											<td align="right">Reserved ID</td>
																											<td width="5">:</td>
																											<td>${element.reservedId}</td>
																										</tr>
																										<tr>
																											<td align="right">Enable</td>
																											<td width="5">:</td>
																											<td>${element.enable}</td>
																										</tr>
																									</c:forEach>
																								</c:if>
																							</tbody>
																						</table>
																					</c:if>

																					<c:if test="${appId eq BBCode}">
																						<input type="hidden" name="uId" id="id"
																							value="${qarec.id}">
																						<table
																							class="table table-striped table-warning table-hover table-outline">
																							<tbody align="center">
																								<c:if
																									test="${not empty listOriginalBuddyBenefit}">
																									<c:forEach items="${listOriginalBuddyBenefit}"
																										var="element" varStatus="loop">
																										<tr>
																											<td width="400" align="right">Company</td>
																											<td width="5">:</td>
																											<td>${element.companyId}</td>
																										</tr>
																										<tr>
																											<td align="right">Benefit Name (En)</td>
																											<td width="5">:</td>
																											<td>${element.nameEn}</td>
																										</tr>
																										<tr>
																											<td align="right">Benefit Name (Bm)</td>
																											<td width="5">:</td>
																											<td>${element.nameBm}</td>
																										</tr>
																										<tr>
																											<td align="right">Adult Rate</td>
																											<td width="5">:</td>
																											<td>${element.adultRate}</td>
																										</tr>
																										<tr>
																											<td align="right">Insured Male Eligible</td>
																											<td width="5">:</td>
																											<td>${element.insuredMElig}</td>
																										</tr>
																										<tr>
																											<td align="right">Insured Female
																												Eligible</td>
																											<td width="5">:</td>
																											<td>${element.insuredFElig}</td>
																										</tr>
																										<tr>
																											<td align="right">Spouse Male Eligible</td>
																											<td width="5">:</td>
																											<td>${element.spouseMElig}</td>
																										</tr>
																										<tr>
																											<td align="right">Spouse Female Eligible</td>
																											<td width="5">:</td>
																											<td>${element.spouseFElig}</td>
																										</tr>
																										<tr>
																											<td align="right">Child Rate</td>
																											<td width="5">:</td>
																											<td>${element.childRate}</td>
																										</tr>
																										<tr>
																											<td align="right">Child Eligible</td>
																											<td width="5">:</td>
																											<td>${element.childElig}</td>
																										</tr>
																										<tr>
																											<td align="right">Enable</td>
																											<td width="5">:</td>
																											<td>${element.enable}</td>
																										</tr>
																									</c:forEach>
																								</c:if>
																							</tbody>
																						</table>
																					</c:if>


																					<c:if test="${appId eq BBGCode}">
																						<input type="hidden" name="uId" id="id"
																							value="${qarec.id}">
																						<table
																							class="table table-striped table-warning table-hover table-outline">
																							<thead style="font-weight: bold;">
																								<c:if
																									test="${not empty listOriginalBuddyBenefitGrp}">
																									<tr>
																										<td align="center" colspan="5">Company :
																											<c:out
																												value="${listOriginalBuddyBenefitGrp[0].companyId}"></c:out>
																										</td>
																									</tr>
																									<tr>
																										<td align="center" colspan="5"><c:out
																												value="${listOriginalBuddyBenefitGrp[0].comboName}"></c:out>
																											(<c:out
																												value="${listOriginalBuddyBenefitGrp[0].comboId}"></c:out>)</td>
																									</tr>
																									<tr>
																										<td align="center">Plan</td>
																										<td align="center" colspan="4"><c:out
																												value="${listOriginalBuddyBenefitGrp[0].planName}"></c:out>
																											(<c:out
																												value="${listOriginalBuddyBenefitGrp[0].planId}"></c:out>)</td>
																									</tr>
																									<tr>
																										<td align="center" rowspan="2">Type of
																											Benefits</td>
																										<td align="center" colspan="2">Sum
																											Insured/<br>Limit (RM)
																										</td>
																										<td align="center" colspan="2">Loading
																											(%)</td>
																									</tr>
																									<tr>
																										<td align="center">Adult</td>
																										<td align="center">Child</td>
																										<td align="center">Adult</td>
																										<td align="center">Child</td>
																									</tr>
																								</c:if>
																							</thead>
																							<tbody align="center">
																								<c:if
																									test="${not empty listOriginalBuddyBenefitGrp}">
																									<c:forEach
																										items="${listOriginalBuddyBenefitGrp}"
																										var="element" varStatus="loop">
																										<tr>
																											<td>${element.benefitName}</td>
																											<td>${element.adultSumInsured}</td>
																											<td>${element.childSumInsured}</td>
																											<td>${element.adultLoading}</td>
																											<td>${element.childLoading}</td>
																										</tr>
																									</c:forEach>
																								</c:if>
																							</tbody>
																						</table>
																					</c:if>

																					<c:if test="${appId eq BPCode}">
																						<input type="hidden" name="uId" id="id"
																							value="${qarec.id}">
																						<table
																							class="table table-striped table-warning table-hover table-outline">
																							<tbody align="center">
																								<c:if
																									test="${not empty listOriginalBuddyProduct}">
																									<c:forEach items="${listOriginalBuddyProduct}"
																										var="element" varStatus="loop">
																										<tr>
																											<td width="400" align="right">Company ID</td>
																											<td width="5">:</td>
																											<td>${element.companyId}</td>
																										</tr>
																										<tr>
																											<td align="right">Product ID</td>
																											<td width="5">:</td>
																											<td>${element.productId}</td>
																										</tr>
																										<tr>
																											<td align="right">Adult Min. Age</td>
																											<td width="5">:</td>
																											<td>${element.adultMinAge}</td>
																										</tr>
																										<tr>
																											<td align="right">Adult Max. Age</td>
																											<td width="5">:</td>
																											<td>${element.adultMaxAge}</td>
																										</tr>
																										<tr>
																											<td align="right">Child Min. Age (Days)</td>
																											<td width="5">:</td>
																											<td>${element.childMinAgeDays}</td>
																										</tr>
																										<tr>
																											<td align="right">Child Max. Age</td>
																											<td width="5">:</td>
																											<td>${element.childMaxAge}</td>
																										</tr>
																										<tr>
																											<td align="right">Student Min. Age</td>
																											<td width="5">:</td>
																											<td>${element.studentMinAge}</td>
																										</tr>
																										<tr>
																											<td align="right">Student Max. Age</td>
																											<td width="5">:</td>
																											<td>${element.studentMaxAge}</td>
																										</tr>
																										<tr>
																											<td align="right">Max. Child Number</td>
																											<td width="5">:</td>
																											<td>${element.maxChildNo}</td>
																										</tr>
																										<tr>
																											<td align="right">Staff Discount
																												Percentage</td>
																											<td width="5">:</td>
																											<td>${element.staffDiscountPercentage}</td>
																										</tr>
																										<tr>
																											<td align="right">Stamp Duty</td>
																											<td width="5">:</td>
																											<td>${element.stampDuty}</td>
																										</tr>
																										<tr>
																											<td align="right">GST</td>
																											<td width="5">:</td>
																											<td>${element.gst}</td>
																										</tr>
																										<tr>
																											<td align="right">GST Effective date</td>
																											<td width="5">:</td>
																											<td>${element.gstEffDate}</td>
																										</tr>
																										<tr>
																											<td align="right">SST</td>
																											<td width="5">:</td>
																											<td>${element.sst}</td>
																										</tr>
																										<tr>
																											<td align="right">SST Effective Date</td>
																											<td width="5">:</td>
																											<td>${element.sstEffDate}</td>
																										</tr>
																									</c:forEach>
																								</c:if>
																							</tbody>
																						</table>
																					</c:if>

																					<!-- BUDDY PA END -->
																					<!-- TRAVEL EZY START -->
																					<c:if test="${appId eq TEZCode}">
																						<input type="hidden" name="uId" id="id"
																							value="${qarec.id}">
																						<table
																							class="table table-striped table-warning table-hover table-outline">
																							<tbody align="center">
																								<c:if
																									test="${not empty listOriginalTravelEzyProduct}">
																									<c:forEach
																										items="${listOriginalTravelEzyProduct}"
																										var="element" varStatus="loop">
																										<tr>
																											<td width="400" align="right">Company ID</td>
																											<td width="5">:</td>
																											<td>${element.companyId}</td>
																										</tr>
																										<tr>
																											<td align="right">Product Code</td>
																											<td width="5">:</td>
																											<td>${element.productCode}</td>
																										</tr>
																										<tr>
																											<td align="right">Discount</td>
																											<td width="5">:</td>
																											<td>${element.discount}</td>
																										</tr>
																										<tr>
																											<td align="right">Stamp Duty</td>
																											<td width="5">:</td>
																											<td>${element.stampDuty}</td>
																										</tr>
																										<tr>
																											<td align="right">POI</td>
																											<td width="5">:</td>
																											<td>${element.poi}</td>
																										</tr>
																										<tr>
																											<td align="right">GST</td>
																											<td width="5">:</td>
																											<td>${element.gst}</td>
																										</tr>
																										<tr>
																											<td align="right">GST Effective date</td>
																											<td width="5">:</td>
																											<td>${element.gstEffDate}</td>
																										</tr>
																										<tr>
																											<td align="right">SST</td>
																											<td width="5">:</td>
																											<td>${element.sst}</td>
																										</tr>
																										<tr>
																											<td align="right">SST Effective Date</td>
																											<td width="5">:</td>
																											<td>${element.sstEffDate}</td>
																										</tr>
																										<tr>
																											<td align="right">Premium Rate</td>
																											<td width="5">:</td>
																											<td>${element.premiumRate}</td>
																										</tr>
																									</c:forEach>
																								</c:if>
																							</tbody>
																						</table>
																					</c:if>
																					<!-- TRAVEL EZY END -->
																					<!-- TRIP CARE 360 START -->
																					<c:if test="${appId eq TCPCode}">
																						<input type="hidden" name="uId" id="id"
																							value="${qarec.id}">
																						<table
																							class="table table-striped table-warning table-hover table-outline">
																							<tbody align="center">
																								<tr>
																									<td align="left"><strong>Sales
																											target</strong></td>
																									<td><strong>Quotation Validity</strong></td>
																								</tr>
																								<c:if
																									test="${not empty listOriginalTCProductInfo}">

																									<c:forEach items="${listOriginalTCProductInfo}"
																										var="element" varStatus="loop">
																										<tr>
																											<td align='left'><c:out
																													value="${element.annualTarget}" /></td>

																											<td align='left'><c:out
																													value="${element.validity}" /></td>

																										</tr>
																									</c:forEach>
																								</c:if>
																							</tbody>
																						</table>
																					</c:if>

																					<c:if test="${appId eq TCPRCode}">
																						<input type="hidden" name="uId" id="id"
																							value="${qarec.id}">
																						<c:choose>
																							<c:when test="${tcprodtype=='0'}">
																								<table
																									class="table table-striped table-warning table-hover table-outline">
																									<tbody align="center">
																										<c:if
																											test="${not empty listOriginalTCProductRate}">

																											<c:forEach
																												items="${listOriginalTCProductRate}"
																												var="element" varStatus="loop">

																												<tr>
																													<td align="right"><strong>TripCare
																															Discount </strong></td>
																													<td width="5">:</td>
																													<td><c:out value="${element.discount}" /></td>
																												</tr>
																												<tr>
																													<td align="right">GST</td>
																													<td width="5">:</td>
																													<td>${element.gst}</td>
																												</tr>
																												<tr>
																													<td align="right">GST Effective date</td>
																													<td width="5">:</td>
																													<td>${element.gstEffDate}</td>
																												</tr>
																												<tr>
																													<td align="right">SST</td>
																													<td width="5">:</td>
																													<td>${element.sst}</td>
																												</tr>
																												<tr>
																													<td align="right">SST Effective Date</td>
																													<td width="5">:</td>
																													<td>${element.sstEffDate}</td>
																												</tr>
																												<tr>
																													<td align="right"><strong>TripCare
																															Stamp Duty</strong></td>
																													<td width="5">:</td>
																													<td><c:out
																															value="${element.stampDuty}" /></td>
																												</tr>
																											</c:forEach>
																										</c:if>
																									</tbody>
																								</table>
																							</c:when>
																							<c:otherwise>
																								<c:choose>
																									<c:when test="${tcprodtype=='D'}">

																										<table
																											class="table table-striped table-warning table-hover table-outline">
																											<tbody align="center">
																												<tr>
																													<td><strong>TripCare Premium
																															Type </strong></td>
																													<td><strong>TripCare Plan</strong></td>
																													<td><strong>TripCare Premium
																															Value</strong></td>
																												</tr>
																												<c:if
																													test="${not empty listOriginalTCProductRate}">

																													<c:forEach
																														items="${listOriginalTCProductRate}"
																														var="element" varStatus="loop">
																														<tr>
																															<td><c:out value="TripCare Domestic" /></td>

																															<td><c:out
																																	value="${element.daysRangeKeycode}" /></td>

																															<td><c:out
																																	value="${element.premiumValue}" /></td>

																														</tr>
																													</c:forEach>
																												</c:if>
																											</tbody>
																										</table>


																									</c:when>
																									<c:when test="${tcprodtype=='INT'}">


																										<table
																											class="table table-striped table-warning table-hover table-outline">
																											<tbody align="center">
																												<tr>
																													<td><strong>TripCare Premium
																															Type </strong></td>
																													<td><strong>TripCare Plan</strong></td>
																													<td><strong>TripCare Premium
																															Value</strong></td>
																												</tr>
																												<c:if
																													test="${not empty listOriginalTCProductRate}">

																													<c:forEach
																														items="${listOriginalTCProductRate}"
																														var="element" varStatus="loop">
																														<tr>
																															<td><c:out
																																	value="TripCare International" /></td>

																															<td><c:out
																																	value="${element.daysRangeKeycode}" /></td>

																															<td><c:out
																																	value="${element.premiumValue}" /></td>

																														</tr>
																													</c:forEach>
																												</c:if>
																											</tbody>
																										</table>

																									</c:when>
																								</c:choose>
																							</c:otherwise>
																						</c:choose>
																					</c:if>

																					<!-- TRIP CARE 360 END -->
																				</div>


																				<div class="col-md-6 mb-3">
																					<h4 class="text-center">Changes</h4>
																					<c:if test="${appId eq OData}">

																						<table
																							class="table table-striped table-warning table-hover table-outline">
																							<tbody align="center">
																								<tr>
																									<td align="left"><strong>Product</strong></td>
																									<td><strong>Type</strong></td>
																									<td><strong>Entity</strong></td>
																									<td><strong>Agent Code</strong></td>
																									<td><strong>Discount (%)</strong></td>
																									<td><strong>Commission (%)</strong></td>
																								</tr>
																								<c:if test="${not empty listOriginalData}">
																									<c:forEach items="${listChangeData}"
																										var="element" varStatus="loop">
																										<tr>
																											<td align='left'>${element.productCode}
																											</td>

																											<td align='left'>${element.agentType}</td>

																											<td align='left'>${element.agentEntity}
																											</td>

																											<td align='left'>${element.agentCode}</td>

																											<td align='left'>${element.discount}</td>

																											<td align='left'>${element.commission}</td>
																										</tr>
																									</c:forEach>
																								</c:if>

																							</tbody>
																						</table>
																					</c:if>


																					<c:if test="${appId eq MCode}">

																						<section class="content bg-white">
																							<div class="row">
																								<div class="col-md-6 mb-3">
																									<table
																										class="table table-striped table-warning table-hover table-outline">
																										<tbody>
																											<c:if
																												test="${not empty updatedMotorRatesValues}">
																												<tr>
																													<td width="400" align="right">Sum
																														Covered Limit(Min.)</td>
																													<td width="5">:</td>
																													<td width="400">${updatedMotorRatesValues.sumCoveredMin}</td>
																												</tr>
																												<tr>
																													<td align="right">Sum Covered
																														Limit(Max.)</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.sumCoveredMax}</td>
																												</tr>

																												<tr>
																													<td align="right">Direct Discount
																														(Online)</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.directDiscount}</td>
																												</tr>
																												<tr>

																													<td align="right">Direct Discount
																														(Online)</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.capsDiscount}</td>
																												</tr>

																												<tr>
																													<td align="right">Fee(RM)</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.capsFee}</td>
																												</tr>

																												<tr>
																													<td align="right">5 seater incl.
																														driver</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.fiveSeater}</td>
																												</tr>
																												<tr>
																													<td align="right">7 seater incl.
																														driver</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.sevenSeater}</td>
																												</tr>

																												<%-- <tr>
																														<td align="right">GST</td>
																														<td width="5">:</td>
																														<td>${updatedMotorRatesValues.gst}</td>
																													</tr>
																													<tr>
																														<td align="right">Stamp Duty</td>
																														<td width="5">:</td>
																														<td>${updatedMotorRatesValues.stampDuty}</td>
																													</tr> --%>

																												<tr>
																													<td align="right">Maximum No. of
																														Drivers</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.maxDrivers}</td>
																												</tr>
																												<tr>
																													<td align="right">Amount for 3rd
																														driver or more</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.thirdDriver}</td>
																												</tr>

																												<tr>
																													<td align="right">Minimum Age</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.minAge}</td>
																												</tr>
																												<tr>
																													<td align="right">Maximum Age</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.maxAge}</td>
																												</tr>
																												<tr>
																													<td align="right">Motor SST</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.motorTakafulSst}</td>
																												</tr>
																												<tr>
																													<td align="right">Motor SST Effective
																														Date</td>
																													<td width="5">:</td>
																													<td><fmt:formatDate
																															pattern="dd/MM/yyyy"
																															value="${updatedMotorRatesValues.motorTakafulSstEffectiveDate}" />
																													</td>
																												</tr>

																												<tr>
																													<td align="right">DPPA SST</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.dppa_sst}</td>
																												</tr>
																												<tr>
																													<td align="right">DPPA SST Effective
																														Date</td>
																													<td width="5">:</td>
																													<td><fmt:formatDate
																															pattern="dd/MM/yyyy"
																															value="${updatedMotorRatesValues.dppa_sst_effective_date}" /></td>
																												</tr>
																												<tr>
																													<td align="right">DPPA Stamp Duty</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.dppa_stampduty}</td>
																												</tr>
																												<tr>
																													<td align="right">OTO360 SST</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.oto_sst}</td>
																												</tr>
																												<tr>
																													<td align="right">OTO360 SST Effective
																														Date</td>
																													<td width="5">:</td>
																													<td><fmt:formatDate
																															pattern="dd/MM/yyyy"
																															value="${updatedMotorRatesValues.oto_sst_effective_date}" /></td>
																												</tr>
																												<tr>
																													<td align="right">OTO360 Stamp Duty</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.oto_stampduty}</td>
																												</tr>


																											</c:if>

																										</tbody>
																									</table>
																								</div>

																							</div>
																						</section>

																					</c:if>


																					<!-- Rule Management Change value Listing End for MI    -->

																					<c:if
																						test="${appId eq MIRules1 || appId eq MIRules2 || appId eq MIRules3 || appId eq MIRules4}">
																						<table
																							class="table table-striped table-warning table-hover table-outline">
																							<tbody align="center">
																								<tr>
																									<td align="left"><strong>Security
																											Question</strong></td>
																									<td><strong>Security Question BM</strong></td>
																									<td><strong>Compulsory Answer</strong></td>
																								</tr>
																								</tr>
																								<c:if
																									test="${not empty listChangeDataMIRules  }">

																									<c:forEach items="${listChangeDataMIRules}"
																										var="element" varStatus="loop">

																										<tr>
																											<td align='left'>
																												${element.questionDescription }</td>

																											<td align='left'>
																												${element.questionDescriptionBm }</td>

																											<td align='left'>
																												${element.correctAnswerCode }</td>

																										</tr>
																									</c:forEach>
																								</c:if>
																							</tbody>
																						</table>
																					</c:if>

																					<!-- Rule Management Change value Listing start for MI    -->



																					<c:if test="${appId eq HOHHCode}">

																						<section class="content bg-white">
																							<div class="row">
																								<div class="">
																									<table
																										class="table text-center table-striped table-warning table-hover table-outline">
																										<tbody>
																											<c:if
																												test="${not empty listChangeHOHHInsData}">

																												<c:forEach items="${listChangeHOHHInsData}"
																													var="element" varStatus="loop">

																													<tr>
																														<td width="400" align="right">Direct
																															Discount (Online)</td>
																														<td width="5">:</td>
																														<td>${element.directDiscount}</td>
																													</tr>
																													<tr>
																														<td align="right">GST</td>
																														<td width="5">:</td>
																														<td>${element.gst}</td>
																													</tr>

																													<tr>

																														<td align="right">Minimum Coverage
																															Amount</td>
																														<td width="5">:</td>
																														<td>${element.minAmountBuild}</td>
																													</tr>
																													<tr>
																														<td align="right">Maximum Coverage
																															Amount</td>
																														<td width="5">:</td>
																														<td>${element.maxAmountBuild}</td>
																													</tr>

																													<tr>

																														<td align="right">Riot, Strike and
																															Malicious Damage (Home Building)</td>
																														<td width="5">:</td>
																														<td>${element.riot}</td>
																													</tr>

																													<tr>
																														<td align="right">Extended Theft
																															Cover (Home Contents)</td>
																														<td width="5">:</td>
																														<td>${element.theft}</td>
																													</tr>

																													<tr>
																														<td align="right">Stamp Duty</td>
																														<td width="5">:</td>
																														<td>${element.stampDuty}</td>
																													</tr>
																													<tr>
																														<td align="right">Minimum Coverage
																															Amount</td>
																														<td width="5">:</td>
																														<td>${element.minAmountContent}</td>
																													</tr>

																													<tr>
																														<td align="right">Maximum Coverage
																															Amount</td>
																														<td width="5">:</td>
																														<td>${element.maxAmountContent}</td>
																													</tr>


																												</c:forEach>
																											</c:if>
																										</tbody>
																									</table>
																								</div>

																							</div>
																						</section>

																					</c:if>

																					<c:if test="${appId eq HOHHTCode}">

																						<section class="content bg-white">
																							<div class="row">
																								<div class="">
																									<table
																										class="table text-center table-striped table-warning table-hover table-outline"
																										style="margin-top: -1.2em">
																										<tbody>
																											<c:if
																												test="${not empty listChangeHOHHTakafulData}">

																												<c:forEach
																													items="${listChangeHOHHTakafulData}"
																													var="element" varStatus="loop">

																													<tr>
																														<td width="400" align="right">Direct
																															Discount (Online)</td>
																														<td width="5">:</td>
																														<td>${element.directDiscount}</td>
																													</tr>
																													<tr>
																														<td align="right">GST</td>
																														<td width="5">:</td>
																														<td>${element.gst}</td>
																													</tr>


																													<tr>


																														<td align="right">Minimum Coverage
																															Amount</td>
																														<div class="form-group"></div>
																														<td width="5">:</td>
																														<td>${element.minAmountBuild}</td>
																													</tr>
																													<tr>
																														<td align="right">Maximum Coverage
																															Amount</td>
																														<td width="5">:</td>
																														<td>${element.maxAmountBuild}</td>
																													</tr>

																													<tr>

																														<td align="right">Riot, Strike and
																															Malicious Damage (Home Building)</td>
																														<td width="5">:</td>
																														<td>${element.riot}</td>
																													</tr>

																													<tr>
																														<td align="right">Extended Theft
																															Cover (Home Contents)</td>
																														<td width="5">:</td>
																														<td>${element.theft}</td>
																													</tr>

																													<tr>
																														<td align="right">Stamp Duty</td>
																														<td width="5">:</td>
																														<td>${element.stampDuty}</td>
																													</tr>
																													<tr>
																														<td align="right">Minimum Coverage
																															Amount</td>
																														<td width="5">:</td>
																														<td>${element.minAmountContent}</td>
																													</tr>

																													<tr>
																														<td align="right">Maximum Coverage
																															Amount</td>
																														<td width="5">:</td>
																														<td>${element.maxAmountContent}</td>
																													</tr>


																												</c:forEach>
																											</c:if>
																										</tbody>
																									</table>
																								</div>

																							</div>
																						</section>

																					</c:if>



																					<c:if test="${appId eq MIINFOCode}">

																						<section class="content bg-white">
																							<div class="row">
																								<div class="">
																									<table
																										class="table text-center table-striped table-warning table-hover table-outline">
																										<tbody>
																											<c:if
																												test="${not empty listChangeMIProductInfo}">

																												<c:forEach
																													items="${listChangeMIProductInfo}"
																													var="element" varStatus="loop">

																													<tr>
																														<td width="400" align="right">Product
																															Code</td>
																														<td width="5">:</td>
																														<td>MI</td>
																													</tr>
																													<tr>
																														<td align="right">Product Name</td>
																														<td width="5">:</td>
																														<td>Motor Insurance</td>
																													</tr>


																													<tr>


																														<td align="right">Annual Sales Target</td>
																														<div class="form-group"></div>
																														<td width="5">:</td>
																														<td>${element.annualTarget}</td>
																													</tr>
																													<tr>
																														<td align="right">Quotation Validity</td>
																														<td width="5">:</td>
																														<td>${element.validity}</td>
																													</tr>



																												</c:forEach>
																											</c:if>
																										</tbody>
																									</table>
																								</div>

																							</div>
																						</section>

																					</c:if>


																					<c:if test="${appId eq MTINFOCode}">

																						<section class="content bg-white">
																							<div class="row">
																								<div class="">
																									<table
																										class="table text-center table-striped table-warning table-hover table-outline">
																										<tbody>
																											<c:if
																												test="${not empty listChangeMTProductInfo}">

																												<c:forEach
																													items="${listChangeMTProductInfo}"
																													var="element" varStatus="loop">

																													<tr>
																														<td width="400" align="right">Product
																															Code</td>
																														<td width="5">:</td>
																														<td>MT</td>
																													</tr>
																													<tr>
																														<td align="right">Product Name</td>
																														<td width="5">:</td>
																														<td>Motor Takaful</td>
																													</tr>


																													<tr>


																														<td align="right">Annual Sales Target</td>
																														<div class="form-group"></div>
																														<td width="5">:</td>
																														<td>${element.annualTarget}</td>
																													</tr>
																													<tr>
																														<td align="right">Quotation Validity</td>
																														<td width="5">:</td>
																														<td>${element.validity}</td>
																													</tr>




																												</c:forEach>
																											</c:if>
																										</tbody>
																									</table>
																								</div>

																							</div>
																						</section>

																					</c:if>



																					<c:if test="${appId eq QCode}">
																						<table
																							class="table table-striped table-warning table-hover table-outline">
																							<tbody align="center">
																								<tr>
																									<td align="left"><strong>Security
																											Question</strong></td>
																									<td><strong>Security Question BM</strong></td>
																									<td><strong>Compulsory Answer</strong></td>
																								</tr>
																								<c:if test="${not empty listChangeDataWTCRules}">

																									<c:forEach items="${listChangeDataWTCRules}"
																										var="element" varStatus="loop">

																										<tr>
																											<td align='left'>
																												${element.questionDescription }</td>

																											<td align='left'>
																												${element.questionDescriptionBm }</td>

																											<td align='left'>
																												${element.correctAnswerCode }</td>

																										</tr>
																									</c:forEach>
																								</c:if>
																							</tbody>
																						</table>
																					</c:if>


																					<c:if test="${appId eq WTCCode}">
																						<input type="hidden" name="uId" id="id"
																							value="${qarec.id}">
																						<table
																							class="table table-striped table-warning table-hover table-outline">
																							<tbody align="center">
																								<tr>
																									<td align="left"><strong>Sales
																											target</strong></td>
																									<td><strong>Quotation Validity</strong></td>
																								</tr>
																								<c:if
																									test="${not empty listChangeWTCProductInfo}">

																									<c:forEach items="${listChangeWTCProductInfo}"
																										var="element" varStatus="loop">
																										<tr>
																											<td align='left'><c:out
																													value="${element.annualTarget}" /></td>

																											<td align='left'><c:out
																													value="${element.validity}" /></td>

																										</tr>
																									</c:forEach>
																								</c:if>
																							</tbody>
																						</table>
																					</c:if>

																					<c:if test="${appId eq WTCRateCode}">
																						<input type="hidden" name="uId" id="id"
																							value="${qarec.id}">
																						<c:choose>
																							<c:when test="${wtcprodtype=='0'}">
																								<table
																									class="table table-striped table-warning table-hover table-outline">
																									<tbody align="center">
																										<tr>
																											<td align="left"><strong>WTC
																													Discount </strong></td>
																											<td><strong>WTC Gst</strong></td>
																											<td><strong>WTC Stamp Duty</strong></td>
																										</tr>
																										<c:if
																											test="${not empty listChangeWTCProductRate}">

																											<c:forEach
																												items="${listChangeWTCProductRate}"
																												var="element" varStatus="loop">
																												<tr>
																													<td align='left'><c:out
																															value="${element.wtcDiscount}" /></td>

																													<td align='left'><c:out
																															value="${element.wtcGst}" /></td>

																													<td align='left'><c:out
																															value="${element.wtcStampDuty}" /></td>

																												</tr>
																											</c:forEach>
																										</c:if>
																									</tbody>
																								</table>
																							</c:when>
																							<c:otherwise>
																								<c:choose>
																									<c:when test="${wtcprodtype=='D'}">

																										<table
																											class="table table-striped table-warning table-hover table-outline">
																											<tbody align="center">
																												<tr>
																													<td align="left"><strong>WTC
																															Premium Type </strong></td>
																													<td><strong>WTC Plan</strong></td>
																													<td><strong>WTC Premium Value</strong></td>
																												</tr>
																												<c:if
																													test="${not empty listChangeWTCProductRate}">

																													<c:forEach
																														items="${listChangeWTCProductRate}"
																														var="element" varStatus="loop">
																														<tr>
																															<td align='left'><c:out
																																	value="WTC Domestic" /></td>

																															<td align='left'><c:out
																																	value="${element.wtcdaysrangekeycodeDomestic}" /></td>

																															<td align='left'><c:out
																																	value="${element.wtcpremiumvalDomestic}" /></td>

																														</tr>
																													</c:forEach>
																												</c:if>
																											</tbody>
																										</table>


																									</c:when>
																									<c:otherwise>


																										<table
																											class="table table-striped table-warning table-hover table-outline">
																											<tbody align="center">
																												<tr>
																													<td align="left"><strong>WTC
																															Premium Type </strong></td>
																													<td><strong>WTC Plan</strong></td>
																													<td><strong>WTC Premium Value</strong></td>
																												</tr>
																												<c:if
																													test="${not empty listChangeWTCProductRate}">

																													<c:forEach
																														items="${listChangeWTCProductRate}"
																														var="element" varStatus="loop">
																														<tr>
																															<td align='left'><c:out
																																	value="WTC International" /></td>

																															<td align='left'><c:out
																																	value="${element.wtcdaysrangekeycodeInternational}" /></td>

																															<td align='left'><c:out
																																	value="${element.wtcpremiumvalInternational}" /></td>

																														</tr>
																													</c:forEach>
																												</c:if>
																											</tbody>
																										</table>

																									</c:otherwise>
																								</c:choose>
																							</c:otherwise>
																						</c:choose>
																					</c:if>


																					<c:if test="${appId eq ELSINFOCode}">

																						<section class="content bg-white">
																							<div class="row">
																								<div class="">
																									<table
																										class="table text-center table-striped table-warning table-hover table-outline">
																										<tbody>
																											<c:if
																												test="${not empty listChangeELSProductInfo}">

																												<c:forEach
																													items="${listChangeELSProductInfo}"
																													var="element" varStatus="loop">

																													<tr>
																														<td width="400" align="right">Product
																															Code</td>
																														<td width="5">:</td>
																														<td>TL</td>
																													</tr>
																													<tr>
																														<td align="right">Product Name</td>
																														<td width="5">:</td>
																														<td>Ezy-Life Secure</td>
																													</tr>


																													<tr>


																														<td align="right">Annual Sales Target</td>
																														<div class="form-group"></div>
																														<td width="5">:</td>
																														<td>${element.annualTarget}</td>
																													</tr>
																													<tr>
																														<td align="right">Quotation Validity</td>
																														<td width="5">:</td>
																														<td>${element.validity}</td>
																													</tr>



																												</c:forEach>
																											</c:if>
																										</tbody>
																									</table>
																								</div>

																							</div>
																						</section>

																					</c:if>





																					<c:if test="${appId eq HOHHINFOCode}">

																						<section class="content bg-white">
																							<div class="row">
																								<div class="">
																									<table
																										class="table text-center table-striped table-warning table-hover table-outline">
																										<tbody>
																											<c:if
																												test="${not empty listChangeHOHHProductInfo}">

																												<c:forEach
																													items="${listChangeHOHHProductInfo}"
																													var="element" varStatus="loop">

																													<tr>
																														<td width="400" align="right">Product
																															Code</td>
																														<td width="5">:</td>
																														<td>HOHH</td>
																													</tr>
																													<tr>
																														<td align="right">Product Name</td>
																														<td width="5">:</td>
																														<td>Houseowner/Householder</td>
																													</tr>


																													<tr>


																														<td align="right">Annual Sales Target</td>
																														<div class="form-group"></div>
																														<td width="5">:</td>
																														<td>${element.newAnnualTarget}</td>
																													</tr>
																													<tr>
																														<td align="right">Quotation Validity</td>
																														<td width="5">:</td>
																														<td>${element.newValidity}</td>
																													</tr>



																												</c:forEach>
																											</c:if>
																										</tbody>
																									</table>
																								</div>

																							</div>
																						</section>

																					</c:if>


																					<c:if test="${appId eq ELSOData}">

																						<section class="content bg-white">
																							<div class="row">
																								<div class="">
																									<table
																										class="table text_center table-striped table-warning table-hover table-outline">
																										<tbody>
																											<c:if test="${not empty listChangeELSData}">

																												<c:forEach items="${listChangeELSData}"
																													var="element" varStatus="loop">

																													<tr>
																														<td width="400" align="right">Direct
																															Discount (Online)</td>
																														<td width="5">:</td>
																														<td>${element.discVal}</td>
																													</tr>
																													<tr>
																														<td align="right">GST</td>
																														<td width="5">:</td>
																														<td>${element.gstVal}</td>
																													</tr>

																													<tr>
																														<td align="right">Stamp Duty</td>
																														<td width="5">:</td>
																														<td>${element.stampDuty}</td>
																													</tr>

																													<tr>
																														<td align="right">Minimum Age</td>
																														<td width="5">:</td>
																														<td>${element.minAge}</td>
																													</tr>

																													<tr>
																														<td align="right">Minimum Benefit
																															Amount</td>
																														<td width="5">:</td>
																														<td>${element.minAmt}</td>
																													</tr>

																													<tr>

																														<td align="right">Maximum Benefit
																															Amount</td>
																														<td width="5">:</td>
																														<td>${element.maxAmt}</td>
																													</tr>
																													<tr>
																														<td align="right">Total Sum Assured
																															at Risk(TSAR)</td>
																														<td width="5">:</td>
																														<td>${element.tsarVal}</td>
																													</tr>

																													<tr>

																														<td align="right">Maximum Age</td>
																														<td width="5">:</td>
																														<td>${element.maxAge}</td>
																													</tr>


																												</c:forEach>
																											</c:if>
																										</tbody>
																									</table>
																								</div>

																							</div>
																						</section>

																					</c:if>


																					<!-- Display Motor Takaful  Change Product  rate -->


																					<c:if test="${appId eq MIRCode}">
																						<section class="content bg-white">
																							<div class="row">
																								<div class="col-md-6 mb-3">
																									<table
																										class="table table-striped table-warning table-hover table-outline">
																										<tbody>
																											<c:if
																												test="${not empty updatedMotorRatesValues}">
																												<tr>
																													<td width="400" align="right">Sum
																														Covered Limit(Min.)</td>
																													<td width="5">:</td>
																													<td width="400">${updatedMotorRatesValues.sumCoveredMin}</td>
																												</tr>
																												<tr>
																													<td align="right">Sum Covered
																														Limit(Max.)</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.sumCoveredMax}</td>
																												</tr>

																												<tr>
																													<td align="right">Direct Discount
																														(Online)</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.directDiscount}</td>
																												</tr>
																												<tr>

																													<td align="right">Direct Discount
																														(Online)</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.capsDiscount}</td>
																												</tr>

																												<tr>
																													<td align="right">Fee(RM)</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.capsFee}</td>
																												</tr>

																												<tr>
																													<td align="right">5 seater incl.
																														driver</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.fiveSeater}</td>
																												</tr>
																												<tr>
																													<td align="right">7 seater incl.
																														driver</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.sevenSeater}</td>
																												</tr>

																												<tr>
																													<td align="right">GST</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.gst}</td>
																												</tr>
																												<tr>
																													<td align="right">Stamp Duty</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.stampDuty}</td>
																												</tr> --%>

																													<tr>
																													<td align="right">Maximum No. of
																														Drivers</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.maxDrivers}</td>
																												</tr>
																												<tr>
																													<td align="right">Amount for 3rd
																														driver or more</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.thirdDriver}</td>
																												</tr>

																												<tr>
																													<td align="right">Minimum Age</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.minAge}</td>
																												</tr>
																												<tr>
																													<td align="right">Maximum Age</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.maxAge}</td>
																												</tr>
																												<tr>
																													<td align="right">Motor SST</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.motorTakafulSst}</td>
																												</tr>
																												<tr>
																													<td align="right">Motor SST Effective
																														Date</td>
																													<td width="5">:</td>
																													<td><fmt:formatDate
																															pattern="dd/MM/yyyy"
																															value="${updatedMotorRatesValues.motorTakafulSstEffectiveDate}" />
																													</td>
																												</tr>

																												<tr>
																													<td align="right">DPPA SST</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.dppa_sst}</td>
																												</tr>
																												<tr>
																													<td align="right">DPPA SST Effective
																														Date</td>
																													<td width="5">:</td>
																													<td><fmt:formatDate
																															pattern="dd/MM/yyyy"
																															value="${updatedMotorRatesValues.dppa_sst_effective_date}" /></td>
																												</tr>
																												<tr>
																													<td align="right">DPPA Stamp Duty</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.dppa_stampduty}</td>
																												</tr>
																												<tr>
																													<td align="right">OTO360 SST</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.oto_sst}</td>
																												</tr>
																												<tr>
																													<td align="right">OTO360 SST Effective
																														Date</td>
																													<td width="5">:</td>
																													<td><fmt:formatDate
																															pattern="dd/MM/yyyy"
																															value="${updatedMotorRatesValues.oto_sst_effective_date}" /></td>
																												</tr>
																												<tr>
																													<td align="right">OTO360 Stamp Duty</td>
																													<td width="5">:</td>
																													<td>${updatedMotorRatesValues.oto_stampduty}</td>
																												</tr>


																											</c:if>

																										</tbody>
																									</table>
																								</div>

																							</div>
																						</section>

																					</c:if>

																					<!-- Display Motor Takaful  Change Product  rate  End -->

																					<!-- BUDDY PA START -->

																					<c:if test="${appId eq BOCode}">
																						<input type="hidden" name="uId" id="id"
																							value="${qarec.id}">
																						<table
																							class="table table-striped table-warning table-hover table-outline">
																							<tbody align="center">
																								<c:if test="${not empty listChangeBuddyPlan}">
																									<c:forEach items="${listChangeBuddyPlan}"
																										var="element" varStatus="loop">
																										<tr>
																											<td width="400" align="right">Plan ID</td>
																											<td width="5">:</td>
																											<td>${element.planId}</td>
																										</tr>
																										<tr>
																											<td align="right">Plan Name (EN)</td>
																											<td width="5">:</td>
																											<td>${element.nameEn}</td>
																										</tr>
																										<tr>
																											<td align="right">Plan Name (BM)</td>
																											<td width="5">:</td>
																											<td>${element.nameBm}</td>
																										</tr>
																										<tr>
																											<td align="right">Enable</td>
																											<td width="5">:</td>
																											<td>${element.enable}</td>
																										</tr>
																									</c:forEach>
																								</c:if>
																							</tbody>
																						</table>
																					</c:if>

																					<c:if test="${appId eq BCCode}">
																						<input type="hidden" name="uId" id="id"
																							value="${qarec.id}">
																						<table
																							class="table table-striped table-warning table-hover table-outline">
																							<tbody align="center">
																								<c:if test="${not empty listChangeBuddyCombo}">
																									<c:forEach items="${listChangeBuddyCombo}"
																										var="element" varStatus="loop">
																										<tr>
																											<td width="400" align="right">Company</td>
																											<td width="5">:</td>
																											<td>${element.companyId}</td>
																										</tr>
																										<tr>
																											<td align="right">Combo Name</td>
																											<td width="5">:</td>
																											<td>${element.name}</td>
																										</tr>
																										<tr>
																											<td align="right">Reserved ID</td>
																											<td width="5">:</td>
																											<td>${element.reservedId}</td>
																										</tr>
																										<tr>
																											<td align="right">Enable</td>
																											<td width="5">:</td>
																											<td>${element.enable}</td>
																										</tr>
																									</c:forEach>
																								</c:if>
																							</tbody>
																						</table>
																					</c:if>

																					<c:if test="${appId eq BBCode}">
																						<input type="hidden" name="uId" id="id"
																							value="${qarec.id}">
																						<table
																							class="table table-striped table-warning table-hover table-outline">
																							<tbody align="center">
																								<c:if test="${not empty listChangeBuddyBenefit}">
																									<c:forEach items="${listChangeBuddyBenefit}"
																										var="element" varStatus="loop">
																										<tr>
																											<td width="400" align="right">Company</td>
																											<td width="5">:</td>
																											<td>${element.companyId}</td>
																										</tr>
																										<tr>
																											<td align="right">Benefit Name (En)</td>
																											<td width="5">:</td>
																											<td>${element.nameEn}</td>
																										</tr>
																										<tr>
																											<td align="right">Benefit Name (Bm)</td>
																											<td width="5">:</td>
																											<td>${element.nameBm}</td>
																										</tr>
																										<tr>
																											<td align="right">Adult Rate</td>
																											<td width="5">:</td>
																											<td>${element.adultRate}</td>
																										</tr>
																										<tr>
																											<td align="right">Insured Male Eligible</td>
																											<td width="5">:</td>
																											<td>${element.insuredMElig}</td>
																										</tr>
																										<tr>
																											<td align="right">Insured Female
																												Eligible</td>
																											<td width="5">:</td>
																											<td>${element.insuredFElig}</td>
																										</tr>
																										<tr>
																											<td align="right">Spouse Male Eligible</td>
																											<td width="5">:</td>
																											<td>${element.spouseMElig}</td>
																										</tr>
																										<tr>
																											<td align="right">Spouse Female Eligible</td>
																											<td width="5">:</td>
																											<td>${element.spouseFElig}</td>
																										</tr>
																										<tr>
																											<td align="right">Child Rate</td>
																											<td width="5">:</td>
																											<td>${element.childRate}</td>
																										</tr>
																										<tr>
																											<td align="right">Child Eligible</td>
																											<td width="5">:</td>
																											<td>${element.childElig}</td>
																										</tr>
																										<tr>
																											<td align="right">Enable</td>
																											<td width="5">:</td>
																											<td>${element.enable}</td>
																										</tr>
																									</c:forEach>
																								</c:if>
																							</tbody>
																						</table>
																					</c:if>

																					<c:if test="${appId eq BBGCode}">
																						<input type="hidden" name="uId" id="id"
																							value="${qarec.id}">
																						<table
																							class="table table-striped table-warning table-hover table-outline">
																							<thead style="font-weight: bold;">
																								<c:if
																									test="${not empty listChangeBuddyBenefitGrp}">
																									<tr>
																										<td align="center" colspan="5">Company :
																											<c:out
																												value="${listChangeBuddyBenefitGrp[0].companyId}"></c:out>
																										</td>
																									</tr>
																									<tr>
																										<td align="center" colspan="5"><c:out
																												value="${listChangeBuddyBenefitGrp[0].comboName}"></c:out>
																											(<c:out
																												value="${listChangeBuddyBenefitGrp[0].comboId}"></c:out>)</td>
																									</tr>
																									<tr>
																										<td align="center">Plan</td>
																										<td align="center" colspan="4"><c:out
																												value="${listChangeBuddyBenefitGrp[0].planName}"></c:out>
																											(<c:out
																												value="${listChangeBuddyBenefitGrp[0].planId}"></c:out>)</td>
																									</tr>
																									<tr>
																										<td align="center" rowspan="2">Type of
																											Benefits</td>
																										<td align="center" colspan="2">Sum
																											Insured/<br>Limit (RM)
																										</td>
																										<td align="center" colspan="2">Loading
																											(%)</td>
																									</tr>
																									<tr>
																										<td align="center">Adult</td>
																										<td align="center">Child</td>
																										<td align="center">Adult</td>
																										<td align="center">Child</td>
																									</tr>
																								</c:if>
																							</thead>
																							<tbody align="center">
																								<c:if
																									test="${not empty listChangeBuddyBenefitGrp}">
																									<c:forEach items="${listChangeBuddyBenefitGrp}"
																										var="element" varStatus="loop">
																										<tr>
																											<td>${element.benefitName}</td>
																											<td>${element.adultSumInsured}</td>
																											<td>${element.childSumInsured}</td>
																											<td>${element.adultLoading}</td>
																											<td>${element.childLoading}</td>
																										</tr>
																									</c:forEach>
																								</c:if>
																							</tbody>
																						</table>
																					</c:if>

																					<c:if test="${appId eq BPCode}">
																						<input type="hidden" name="uId" id="id"
																							value="${qarec.id}">
																						<table
																							class="table table-striped table-warning table-hover table-outline">
																							<tbody align="center">
																								<c:if test="${not empty listChangeBuddyProduct}">
																									<c:forEach items="${listChangeBuddyProduct}"
																										var="element" varStatus="loop">
																										<tr>
																											<td width="400" align="right">Company ID</td>
																											<td width="5">:</td>
																											<td>${element.companyId}</td>
																										</tr>
																										<tr>
																											<td align="right">Product ID</td>
																											<td width="5">:</td>
																											<td>${element.productId}</td>
																										</tr>
																										<tr>
																											<td align="right">Adult Min. Age</td>
																											<td width="5">:</td>
																											<td>${element.adultMinAge}</td>
																										</tr>
																										<tr>
																											<td align="right">Adult Max. Age</td>
																											<td width="5">:</td>
																											<td>${element.adultMaxAge}</td>
																										</tr>
																										<tr>
																											<td align="right">Child Min. Age (Days)</td>
																											<td width="5">:</td>
																											<td>${element.childMinAgeDays}</td>
																										</tr>
																										<tr>
																											<td align="right">Child Max. Age</td>
																											<td width="5">:</td>
																											<td>${element.childMaxAge}</td>
																										</tr>
																										<tr>
																											<td align="right">Student Min. Age</td>
																											<td width="5">:</td>
																											<td>${element.studentMinAge}</td>
																										</tr>
																										<tr>
																											<td align="right">Student Max. Age</td>
																											<td width="5">:</td>
																											<td>${element.studentMaxAge}</td>
																										</tr>
																										<tr>
																											<td align="right">Max. Child Number</td>
																											<td width="5">:</td>
																											<td>${element.maxChildNo}</td>
																										</tr>
																										<tr>
																											<td align="right">Staff Discount
																												Percentage</td>
																											<td width="5">:</td>
																											<td>${element.staffDiscountPercentage}</td>
																										</tr>
																										<tr>
																											<td align="right">Stamp Duty</td>
																											<td width="5">:</td>
																											<td>${element.stampDuty}</td>
																										</tr>
																										<tr>
																											<td align="right">GST</td>
																											<td width="5">:</td>
																											<td>${element.gst}</td>
																										</tr>
																										<tr>
																											<td align="right">GST Effective date</td>
																											<td width="5">:</td>
																											<td>${element.gstEffDate}</td>
																										</tr>
																										<tr>
																											<td align="right">SST</td>
																											<td width="5">:</td>
																											<td>${element.sst}</td>
																										</tr>
																										<tr>
																											<td align="right">SST Effective Date</td>
																											<td width="5">:</td>
																											<td>${element.sstEffDate}</td>
																										</tr>
																									</c:forEach>
																								</c:if>
																							</tbody>
																						</table>
																					</c:if>
																					<!-- BUDDY PA END -->
																					<!-- TRAVEL EZY START -->
																					<c:if test="${appId eq TEZCode}">
																						<input type="hidden" name="uId" id="id"
																							value="${qarec.id}">
																						<table
																							class="table table-striped table-warning table-hover table-outline">
																							<tbody align="center">
																								<c:if
																									test="${not empty listChangeTravelEzyProduct}">
																									<c:forEach
																										items="${listChangeTravelEzyProduct}"
																										var="element" varStatus="loop">
																										<tr>
																											<td width="400" align="right">Company ID</td>
																											<td width="5">:</td>
																											<td>${element.companyId}</td>
																										</tr>
																										<tr>
																											<td align="right">Product Code</td>
																											<td width="5">:</td>
																											<td>${element.productCode}</td>
																										</tr>
																										<tr>
																											<td align="right">Discount</td>
																											<td width="5">:</td>
																											<td>${element.discount}</td>
																										</tr>
																										<tr>
																											<td align="right">Stamp Duty</td>
																											<td width="5">:</td>
																											<td>${element.stampDuty}</td>
																										</tr>
																										<tr>
																											<td align="right">POI</td>
																											<td width="5">:</td>
																											<td>${element.poi}</td>
																										</tr>
																										<tr>
																											<td align="right">GST</td>
																											<td width="5">:</td>
																											<td>${element.gst}</td>
																										</tr>
																										<tr>
																											<td align="right">GST Effective date</td>
																											<td width="5">:</td>
																											<td>${element.gstEffDate}</td>
																										</tr>
																										<tr>
																											<td align="right">SST</td>
																											<td width="5">:</td>
																											<td>${element.sst}</td>
																										</tr>
																										<tr>
																											<td align="right">SST Effective Date</td>
																											<td width="5">:</td>
																											<td>${element.sstEffDate}</td>
																										</tr>
																										<tr>
																											<td align="right">Premium Rate</td>
																											<td width="5">:</td>
																											<td>${element.premiumRate}</td>
																										</tr>
																									</c:forEach>
																								</c:if>
																							</tbody>
																						</table>
																					</c:if>
																					<!-- TRAVEL EZY END -->
																					<!-- TRIP CARE 360 START -->
																					<c:if test="${appId eq TCPCode}">
																						<input type="hidden" name="uId" id="id"
																							value="${qarec.id}">
																						<table
																							class="table table-striped table-warning table-hover table-outline">
																							<tbody align="center">
																								<tr>
																									<td align="left"><strong>Sales
																											target</strong></td>
																									<td><strong>Quotation Validity</strong></td>
																								</tr>
																								<c:if
																									test="${not empty listChangeTCProductInfo}">

																									<c:forEach items="${listChangeTCProductInfo}"
																										var="element" varStatus="loop">
																										<tr>
																											<td align='left'><c:out
																													value="${element.annualTarget}" /></td>

																											<td align='left'><c:out
																													value="${element.validity}" /></td>

																										</tr>
																									</c:forEach>
																								</c:if>
																							</tbody>
																						</table>
																					</c:if>

																					<c:if test="${appId eq TCPRCode}">
																						<input type="hidden" name="uId" id="id"
																							value="${qarec.id}">
																						<c:choose>
																							<c:when test="${tcprodtype=='0'}">
																								<table
																									class="table table-striped table-warning table-hover table-outline">
																									<tbody align="center">
																										<c:if
																											test="${not empty listChangeTCProductRate}">

																											<c:forEach items="${listChangeTCProductRate}"
																												var="element" varStatus="loop">
																												<tr>
																													<td align="right"><strong>TripCare
																															Discount </strong></td>
																													<td width="5">:</td>
																													<td><c:out value="${element.discount}" /></td>
																												</tr>
																												<tr>
																													<td align="right">GST</td>
																													<td width="5">:</td>
																													<td>${element.gst}</td>
																												</tr>
																												<tr>
																													<td align="right">GST Effective date</td>
																													<td width="5">:</td>
																													<td>${element.gstEffDate}</td>
																												</tr>
																												<tr>
																													<td align="right">SST</td>
																													<td width="5">:</td>
																													<td>${element.sst}</td>
																												</tr>
																												<tr>
																													<td align="right">SST Effective Date</td>
																													<td width="5">:</td>
																													<td>${element.sstEffDate}</td>
																												</tr>
																												<tr>
																													<td align="right"><strong>TripCare
																															Stamp Duty</strong></td>
																													<td width="5">:</td>
																													<td><c:out
																															value="${element.stampDuty}" /></td>
																												</tr>
																											</c:forEach>
																										</c:if>
																									</tbody>
																								</table>
																							</c:when>
																							<c:otherwise>
																								<c:choose>
																									<c:when test="${tcprodtype=='D'}">

																										<table
																											class="table table-striped table-warning table-hover table-outline">
																											<tbody align="center">
																												<tr>
																													<td><strong>TripCare Premium
																															Type </strong></td>
																													<td><strong>TripCare Plan</strong></td>
																													<td><strong>TripCare Premium
																															Value</strong></td>
																												</tr>
																												<c:if
																													test="${not empty listChangeTCProductRate}">

																													<c:forEach
																														items="${listChangeTCProductRate}"
																														var="element" varStatus="loop">
																														<tr>
																															<td><c:out value="TripCare Domestic" /></td>

																															<td><c:out
																																	value="${element.daysRangeKeycodeDomestic}" /></td>

																															<td><c:out
																																	value="${element.premiumValDomestic}" /></td>

																														</tr>
																													</c:forEach>
																												</c:if>
																											</tbody>
																										</table>


																									</c:when>
																									<c:when test="${tcprodtype=='INT'}">


																										<table
																											class="table table-striped table-warning table-hover table-outline">
																											<tbody align="center">
																												<tr>
																													<td><strong>TripCare Premium
																															Type </strong></td>
																													<td><strong>TripCare Plan</strong></td>
																													<td><strong>TripCare Premium
																															Value</strong></td>
																												</tr>
																												<c:if
																													test="${not empty listChangeTCProductRate}">

																													<c:forEach
																														items="${listChangeTCProductRate}"
																														var="element" varStatus="loop">
																														<tr>
																															<td><c:out
																																	value="TripCare International" /></td>

																															<td><c:out
																																	value="${element.daysRangeKeycodeInternational}" /></td>

																															<td><c:out
																																	value="${element.premiumValInternational}" /></td>

																														</tr>
																													</c:forEach>
																												</c:if>
																											</tbody>
																										</table>

																									</c:when>
																								</c:choose>
																							</c:otherwise>
																						</c:choose>
																					</c:if>

																					<!-- TRIP CARE 360 END -->
																				</div>
																			</div>
																			<div class="row">
																				<div class="col-sm-12 text-center mb-5">
																					<div class="controls">
																					<%  String alogsId ="";
   if(request.getParameter("id")!=null){
       alogsId =obj.stripXSS(request.getParameter("id").toString());
   }%>
																						<c:set var="alogId"
																							value='<%=alogsId%>'>
																						</c:set>
																						<c:if
																							test="${(appId eq OData && approveStatus eq MIINFOCode)}">
																							<%-- <input type="button" class="btn btn-success" onclick="approveAgentProduct(${alogId},${appId})" value="Approve"/>&nbsp;&nbsp; --%>
																							<c:forEach var="paramItem1"
																								items="${sessionScope.level_1}" begin="0"
																								end="0" varStatus="loop">
																								<%-- <c:out value="${paramItem1.permission_str}" /> --%>
																								<c:set var="pervalue"
																									value="${fn:split(paramItem1.permission_str,'$') }" />
																								<c:if
																									test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
																									<input type="button" class="btn btn-success"
																										onclick="approveAgentProduct(${alogId})"
																										value="Approve" />&nbsp;&nbsp; 		
																										<input type="button" class="btn btn-danger"
																										onclick="rejectAgentProduct(${alogId})"
																										value="Reject" />
																								</c:if>
																							</c:forEach>


																						</c:if>

																						<c:if
																							test="${appId eq MCode && approveStatus eq MIINFOCode}">
																							<c:forEach var="paramItem1"
																								items="${sessionScope.level_1}" begin="0"
																								end="0" varStatus="loop">
																								<%-- <c:out value="${paramItem1.permission_str}" /> --%>
																								<c:set var="pervalue"
																									value="${fn:split(paramItem1.permission_str,'$') }" />
																								<c:if
																									test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
																									<input type="button" class="btn btn-success"
																										onclick="approveMIProductRateChange(${alogId})"
																										value="Approve">	&nbsp;&nbsp; 	
																										<input type="button" class="btn btn-danger"
																										onclick="rejectMIProductRateChange(${alogId})"
																										value="Reject">
																								</c:if>
																							</c:forEach>


																						</c:if>



																						<c:if
																							test="${(appId eq HOHHCode && approveStatus eq MIINFOCode)}">
																							<c:forEach var="paramItem1"
																								items="${sessionScope.level_1}" begin="0"
																								end="0" varStatus="loop">
																								<%-- 	<c:out value="${paramItem1.permission_str}" /> --%>
																								<c:set var="pervalue"
																									value="${fn:split(paramItem1.permission_str,'$') }" />
																								<c:if
																									test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
																									<input type="button" class="btn btn-success"
																										onclick="approveHOHHProductRateChange(${alogId})"
																										value="Approve">	
																										&nbsp;&nbsp; <input type="button"
																										class="btn btn-danger"
																										onclick="rejectHOHHProductRateChange(${alogId})"
																										value="Reject">

																								</c:if>
																							</c:forEach>

																						</c:if>



																						<c:if
																							test="${appId eq HOHHTCode && approveStatus eq MIINFOCode}">
																							<c:forEach var="paramItem1"
																								items="${sessionScope.level_1}" begin="0"
																								end="0" varStatus="loop">
																								<%-- <c:out value="${paramItem1.permission_str}" /> --%>
																								<c:set var="pervalue"
																									value="${fn:split(paramItem1.permission_str,'$') }" />
																								<c:if
																									test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
																									<input type="button" class="btn btn-success"
																										onclick="approveHOHHTProductRateChange(${alogId})"
																										value="Approve">&nbsp;&nbsp; 
																										<input type="button" class="btn btn-danger"
																										onclick="rejectHOHHTProductRateChange(${alogId})"
																										value="Reject">

																								</c:if>
																							</c:forEach>

																						</c:if>

																						<c:if test="${(appId eq MIINFOCode && approveStatus eq MIINFOCode)}">
																							<c:forEach var="paramItem1"
																								items="${sessionScope.level_1}" begin="0"
																								end="0" varStatus="loop">
																								<%-- <c:out value="${paramItem1.permission_str}" /> --%>
																								<c:set var="pervalue"
																									value="${fn:split(paramItem1.permission_str,'$') }" />
																								<c:if
																									test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
																									<input type="button" class="btn btn-success"
																										onclick="approveMIProductInfoChange(${alogId})"
																										value="Approve">&nbsp;&nbsp; <input
																										type="button" class="btn btn-danger"
																										onclick="rejectMIProductInfoChange(${alogId})"
																										value="Reject">

																								</c:if>
																							</c:forEach>
																						</c:if>

																						<c:if test="${appId eq MTINFOCode && approveStatus eq MIINFOCode}">
																							<c:forEach var="paramItem1"
																								items="${sessionScope.level_1}" begin="0"
																								end="0" varStatus="loop">
																								<%-- 	<c:out value="${paramItem1.permission_str}" /> --%>
																								<c:set var="pervalue"
																									value="${fn:split(paramItem1.permission_str,'$') }" />
																								<c:if
																									test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
																									<input type="button" class="btn btn-success"
																										onclick="approveMTProductInfoChange(${alogId})"
																										value="Approve">&nbsp;&nbsp; <input
																										type="button" class="btn btn-danger"
																										onclick="rejectMTProductInfoChange(${alogId})"
																										value="Reject">
																								</c:if>
																							</c:forEach>

																						</c:if>

																						<c:if
																							test="${appId eq ELSINFOCode && approveStatus eq MIINFOCode}">
																							<c:forEach var="paramItem1"
																								items="${sessionScope.level_1}" begin="0"
																								end="0" varStatus="loop">
																								<%-- 	<c:out value="${paramItem1.permission_str}" /> --%>
																								<c:set var="pervalue"
																									value="${fn:split(paramItem1.permission_str,'$') }" />
																								<c:if
																									test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
																									<input type="button" class="btn btn-success"
																										onclick="approveELSProductInfoChange(${alogId})"
																										value="Approve">&nbsp;&nbsp; <input
																										type="button" class="btn btn-danger"
																										onclick="rejectELSProductInfoChange(${alogId})"
																										value="Reject">
																								</c:if>
																							</c:forEach>

																						</c:if>


																						<c:if
																							test="${appId eq ELSOData && approveStatus eq MIINFOCode}">
																							<c:forEach var="paramItem1"
																								items="${sessionScope.level_1}" begin="0"
																								end="0" varStatus="loop">
																								<%-- <c:out value="${paramItem1.permission_str}" /> --%>
																								<c:set var="pervalue"
																									value="${fn:split(paramItem1.permission_str,'$') }" />
																								<c:if
																									test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
																									<input type="button" class="btn btn-success"
																										onclick="approveELSProductRateChange(${alogId})"
																										value="Approve">&nbsp;&nbsp; <input
																										type="button" class="btn btn-danger"
																										onclick="rejectELSProductRateChange(${alogId})"
																										value="Reject">
																								</c:if>
																							</c:forEach>

																						</c:if>


																						<c:if
																							test="${(appId eq MIRules1 && approveStatus eq MIINFOCode) ||(appId eq MIRules2 && approveStatus eq MIINFOCode)|| (appId eq MIRules3 && approveStatus eq MIINFOCode)||(appId eq MIRules4 && approveStatus eq MIINFOCode)}">

																							<c:forEach var="paramItem1"
																								items="${sessionScope.level_1}" begin="0"
																								end="0" varStatus="loop">
																								<%-- <c:out value="${paramItem1.permission_str}" /> --%>
																								<c:set var="pervalue"
																									value="${fn:split(paramItem1.permission_str,'$') }" />
																								<c:if
																									test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
																									<input type="button" class="btn btn-success"
																										onclick="approveMIRuleManagementChange(${alogId})"
																										value="Approve">&nbsp;&nbsp; <input
																										type="button" class="btn btn-danger"
																										onclick="rejectMIRuleManagementChange(${alogId})"
																										value="Reject">
																								</c:if>
																							</c:forEach>

																						</c:if>
																						<c:if
																							test="${appId eq QCode && approveStatus eq MIINFOCode}">
																							<c:forEach var="paramItem1"
																								items="${sessionScope.level_1}" begin="0"
																								end="0" varStatus="loop">
																								<%-- <c:out value="${paramItem1.permission_str}" /> --%>
																								<c:set var="pervalue"
																									value="${fn:split(paramItem1.permission_str,'$') }" />
																								<c:if
																									test="${(pervalue[0] eq MIINFOCode) || (pervalue[0] eq 3 )}">
																									<input type="button" class="btn btn-success"
																										onclick="approveWTCRuleManagementChange(${alogId})"
																										value="Approve" />&nbsp;&nbsp; <input
																										type="button" class="btn btn-danger"
																										onclick="rejectWTCRuleManagementChange(${alogId})"
																										value="Reject" />
																								</c:if>
																							</c:forEach>

																						</c:if>


																						<c:if
																							test="${appId eq HOHHINFOCode && approveStatus eq MIINFOCode}">
																							<c:forEach var="paramItem1"
																								items="${sessionScope.level_1}" begin="0"
																								end="0" varStatus="loop">
																								<%-- <c:out value="${paramItem1.permission_str}" /> --%>
																								<c:set var="pervalue"
																									value="${fn:split(paramItem1.permission_str,'$') }" />
																								<c:if
																									test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
																									<input type="button" class="btn btn-success"
																										onclick="approveHOHHProductInfoChange(${alogId})"
																										value="Approve" />&nbsp;&nbsp;  <input
																										type="button" class="btn btn-danger"
																										onclick="rejectHOHHProductInfoChange(${alogId})"
																										value="Reject" />
																								</c:if>
																							</c:forEach>

																						</c:if>


																						<c:if test="${appId eq MIRCode && approveStatus eq MIINFOCode}">
																							<c:forEach var="paramItem1"
																								items="${sessionScope.level_1}" begin="0"
																								end="0" varStatus="loop">
																								<%-- <c:out value="${paramItem1.permission_str}" /> --%>
																								<c:set var="pervalue"
																									value="${fn:split(paramItem1.permission_str,'$') }" />
																								<c:if
																									test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
																									<input type="button" class="btn btn-success"
																										onclick="approveMTProductRateChange(${alogId})"
																										value="Approve">&nbsp;&nbsp;
                               																			 <input type="button"
																										class="btn btn-danger"
																										onclick="rejectMTProductRateChange(${alogId})"
																										value="Reject">
																								</c:if>
																							</c:forEach>

																						</c:if>


																						<c:if test="${appId eq WTCCode && approveStatus eq MIINFOCode}">
																							<c:forEach var="paramItem1"
																								items="${sessionScope.level_1}" begin="0"
																								end="0" varStatus="loop">
																								<%-- <c:out value="${paramItem1.permission_str}" /> --%>
																								<c:set var="pervalue"
																									value="${fn:split(paramItem1.permission_str,'$') }" />
																								<c:if
																									test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
																									<input type="button" class="btn btn-success"
																										onclick="approveWTCProdInfoChange(${alogId})"
																										value="Approve" />&nbsp;&nbsp; <input
																										type="button" class="btn btn-danger"
																										onclick="rejectWTCProdInfoChange(${alogId})"
																										value="Reject" />
																								</c:if>
																							</c:forEach>

																						</c:if>

																						<c:if test="${appId eq WTCRateCode && approveStatus eq MIINFOCode}">
																							<c:forEach var="paramItem1"
																								items="${sessionScope.level_1}" begin="0"
																								end="0" varStatus="loop">
																								<%-- 	<c:out value="${paramItem1.permission_str}" /> --%>
																								<c:set var="pervalue"
																									value="${fn:split(paramItem1.permission_str,'$') }" />
																								<c:if
																									test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
																									<input type="button" class="btn btn-success"
																										onclick="approveWTCProdRateChange(${alogId})"
																										value="Approve" />&nbsp;&nbsp; <input
																										type="button" class="btn btn-danger"
																										onclick="rejectWTCProdRateChange(${alogId})"
																										value="Reject" />
																								</c:if>
																							</c:forEach>

																						</c:if>


																						<!--  Buddy PA Start -->
																						<c:if
																							test="${appId eq BOCode && approveStatus eq MIINFOCode}">
																							<c:forEach var="paramItem1"
																								items="${sessionScope.level_1}" begin="0"
																								end="0" varStatus="loop">
																								<%-- <c:out value="${paramItem1.permission_str}" /> --%>
																								<c:set var="pervalue"
																									value="${fn:split(paramItem1.permission_str,'$') }" />
																								<c:if
																									test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
																									<input type="button" class="btn btn-success"
																										onclick="approveBuddyPAPlanChange(${alogId})"
																										value="Approve">	&nbsp;&nbsp; 	
																										<input type="button" class="btn btn-danger"
																										onclick="rejectBuddyPAChange(${alogId})"
																										value="Reject">
																								</c:if>
																							</c:forEach>
																						</c:if>

																						<c:if
																							test="${appId eq BCCode && approveStatus eq MIINFOCode}">
																							<c:forEach var="paramItem1"
																								items="${sessionScope.level_1}" begin="0"
																								end="0" varStatus="loop">
																								<c:set var="pervalue"
																									value="${fn:split(paramItem1.permission_str,'$') }" />
																								<c:if
																									test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
																									<input type="button" class="btn btn-success"
																										onclick="approveBuddyPAComboChange(${alogId})"
																										value="Approve">	&nbsp;&nbsp; 	
																										<input type="button" class="btn btn-danger"
																										onclick="rejectBuddyPAChange(${alogId})"
																										value="Reject">
																								</c:if>
																							</c:forEach>
																						</c:if>

																						<c:if
																							test="${appId eq BBCode && approveStatus eq MIINFOCode}">
																							<c:forEach var="paramItem1"
																								items="${sessionScope.level_1}" begin="0"
																								end="0" varStatus="loop">
																								<c:set var="pervalue"
																									value="${fn:split(paramItem1.permission_str,'$') }" />
																								<c:if
																									test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
																									<input type="button" class="btn btn-success"
																										onclick="approveBuddyPABenefitChange(${alogId})"
																										value="Approve">	&nbsp;&nbsp; 	
																										<input type="button" class="btn btn-danger"
																										onclick="rejectBuddyPAChange(${alogId})"
																										value="Reject">
																								</c:if>
																							</c:forEach>
																						</c:if>

																						<c:if
																							test="${appId eq BBGCode && approveStatus eq MIINFOCode}">
																							<c:forEach var="paramItem1"
																								items="${sessionScope.level_1}" begin="0"
																								end="0" varStatus="loop">
																								<c:set var="pervalue"
																									value="${fn:split(paramItem1.permission_str,'$') }" />
																								<c:if
																									test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
																									<input type="button" class="btn btn-success"
																										onclick="approveBuddyPABenefitGrpChange(${alogId})"
																										value="Approve">	&nbsp;&nbsp; 	
																										<input type="button" class="btn btn-danger"
																										onclick="rejectBuddyPAChange(${alogId})"
																										value="Reject">
																								</c:if>
																							</c:forEach>
																						</c:if>

																						<c:if
																							test="${appId eq BPCode && approveStatus eq MIINFOCode}">
																							<c:forEach var="paramItem1"
																								items="${sessionScope.level_1}" begin="0"
																								end="0" varStatus="loop">
																								<c:set var="pervalue"
																									value="${fn:split(paramItem1.permission_str,'$') }" />
																								<c:if
																									test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
																									<input type="button" class="btn btn-success"
																										onclick="approveBuddyPAProductChange(${alogId})"
																										value="Approve">	&nbsp;&nbsp; 	
																										<input type="button" class="btn btn-danger"
																										onclick="rejectBuddyPAChange(${alogId})"
																										value="Reject">
																								</c:if>
																							</c:forEach>
																						</c:if>
																						<!--  Buddy PA End -->
																						<!-- Travel Ezy Start -->
																						<c:if
																							test="${appId eq TEZCode && approveStatus eq MIINFOCode}">
																							<c:forEach var="paramItem1"
																								items="${sessionScope.level_1}" begin="0"
																								end="0" varStatus="loop">
																								<c:set var="pervalue"
																									value="${fn:split(paramItem1.permission_str,'$') }" />
																								<c:if
																									test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
																									<input type="button" class="btn btn-success"
																										onclick="approveTravelEzyProductChange(${alogId})"
																										value="Approve">	&nbsp;&nbsp; 	
																										<input type="button" class="btn btn-danger"
																										onclick="rejectTravelEzyProductChange(${alogId})"
																										value="Reject">
																								</c:if>
																							</c:forEach>
																						</c:if>
																						<!--  Travel Ezy End -->
																						<!-- Trip Care Start -->

																						<c:if test="${appId eq TCPCode && approveStatus eq MIINFOCode}">
																							<c:forEach var="paramItem1"
																								items="${sessionScope.level_1}" begin="0"
																								end="0" varStatus="loop">
																								<c:set var="pervalue"
																									value="${fn:split(paramItem1.permission_str,'$') }" />
																								<c:if
																									test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
																									<input type="button" class="btn btn-success"
																										onclick="approveTCProdInfoChange(${alogId})"
																										value="Approve" />&nbsp;&nbsp; <input
																										type="button" class="btn btn-danger"
																										onclick="rejectTCProdInfoChange(${alogId})"
																										value="Reject" />
																								</c:if>
																							</c:forEach>

																						</c:if>

																						<c:if
																							test="${appId eq TCPRCode && approveStatus eq MIINFOCode}">
																							<c:forEach var="paramItem1"
																								items="${sessionScope.level_1}" begin="0"
																								end="0" varStatus="loop">
																								<c:set var="pervalue"
																									value="${fn:split(paramItem1.permission_str,'$') }" />
																								<c:if
																									test="${(pervalue[0] eq 1) || (pervalue[0] eq 3 )}">
																									<input type="button" class="btn btn-success"
																										onclick="approveTCProdRateChange(${alogId})"
																										value="Approve" />&nbsp;&nbsp; <input
																										type="button" class="btn btn-danger"
																										onclick="rejectTCProdRateChange(${alogId})"
																										value="Reject" />
																								</c:if>
																							</c:forEach>

																						</c:if>

																						<!-- Trip Care End -->
																					</div>
																				</div>
																			</div>
																		</section>
																	</form>
																	<!-- END FORM -->

																</div>

															</div>

															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>


											</div>
										</div>
									</div>
									<!--row -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->

	</div>
	<!-- /.page-content -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<jsp:include page="pageFooter.jsp" />
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->



	<!-- BEGIN BACK TO TOP BUTTON -->
	<!--
        <div id="back-top">
            <a href="#top"><i class="fa fa-chevron-up"></i></a>
        </div>
-->
	<!-- END BACK TO TOP -->


	<!--
        ===========================================================
        END PAGE
        ===========================================================
        -->

	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->
	<!------------------------------------------------------------ BEGIN FOOTER ------------------------------------------------------------------>
	<%--    <jsp:include page="siteFooter.jsp" />   --%>
	<!-------------------------------------------------------------END FOOTER -------------------------------------------------------------------->


	<!-- JAVA Script Goes Here -->

	<script
		src="<c:url value="/resources/assets/js/jquery-2.2.4.min.js" />"></script>
	<!--  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
	<script src="assets/js/apps.js"></script>
	<script src="assets/plugins/retina/retina.min.js"></script>
	<script src="assets/plugins/nicescroll/jquery.nicescroll.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js"></script>


	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<!-- 	<script
		src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script> -->
	<script type="text/javascript" src="assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript" src="assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/buttons.print.min.js"></script>
	<script src="assets/js/buttons.colVis.min.js"></script>
	<script src="assets/js/toastr.min.js"></script>
	<script src="assets/jAlert/jAlert.min.js"></script>
	<script src="assets/jAlert/jAlert-functions.min.js"></script>
	<script src="plugins/jquery-knob/knob.js"></script>


	<!--
        ===========================================================
        Placed at the end of the document so the pages load faster
        ===========================================================
        -->

	<script>
		$(document).ready(function() {

			$('#admin-datatable-second_admin').DataTable({
				"order" : [],
				dom : 'Bfrtip',
				title : ' Agent List',

				buttons : [ 'copy', 'csv', 'print', {
					extend : 'excelHtml5',
					title : 'Audit Trail - Payments'
				}, {
					extend : 'pdfHtml5',
					title : 'Audit Trail - Payments'
				} ]
			});

			// $('#date1').datepicker();
			$("#fromdate").datepicker({
				format : 'dd/mm/yyyy',
				autoclose : true,
			}).on('changeDate', function(ev) {
				$(this).datepicker('hide');
			});
			$("#todate").datepicker({
				format : 'dd/mm/yyyy',
				autoclose : true,
			}).on('changeDate', function(ev) {
				$(this).datepicker('hide');
			});

			 
		});
	</script>


	<script>
		function validateForm() {
			var x = document.forms["rptForm"]["fromdate"].value;
			var y = document.forms["rptForm"]["todate"].value;

			if (x == null || x == "") {

				alert("Please choose Date From");
				document.forms["rptForm"]["fromdate"].focus();

				return false;
			} else if (y == null || y == "") {

				alert("Please choose Date To");
				document.forms["rptForm"]["todate"].focus();
				return false;
			}

			return true;

		}
	</script>

	<script type="text/javascript">
	
	
	
		function approveDataSubmit(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/approveChangeDataAction?approvalLogId="+id;
		}
	


		function rejectDataSubmit(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/rejectChangeDataAction?approvalLogId="+id;
		}
	
	//MI Product Rate
		function approveMIProductRateChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/approveMIProductRateChangeData?approvalLogId="+id;
		}
		
		function rejectMIProductRateChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/rejectMIProductRateChangeData?approvalLogId="+id;
		}
		
		function approveAgentProduct(alogid ) {
		 var id = alogid ; 	  
		 
		/* location.href = "${pageContext.request.contextPath}/approveAgentProductMapChangeData?approvalLogId="+id+"&appType="+appId; */
		 location.href = "${pageContext.request.contextPath}/approveAgentProductMapChangeData?approvalLogId="+id;
	}
	
	
	function rejectAgentProduct(alogid) {
		 var id = alogid ;
		location.href = "${pageContext.request.contextPath}/rejectAgentProductMapChangeData?approvalLogId="+id;
	}
	
	
	
		//MI Product Info
		function approveMIProductInfoChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/approveMIProductInfoChangeData?approvalLogId="+id;
		}
		
		function rejectMIProductInfoChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/rejectMIProductInfoChangeData?approvalLogId="+id;
		}
		
		//MT Product Info
		function approveMTProductInfoChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/approveMTProductInfoChangeData?approvalLogId="+id;
		}
		
		function rejectMTProductInfoChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/rejectMTProductInfoChangeData?approvalLogId="+id;
		}
		
		
		//ELS Product Info
		function approveELSProductInfoChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/approveELSProductInfoChangeData?approvalLogId="+id;
		}
		
		function rejectELSProductInfoChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/rejectELSProductInfoChangeData?approvalLogId="+id;
		}
		
		//ELS Product Rate
		function approveELSProductRateChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/approveELSProductRateChangeData?approvalLogId="+id;
		}
		
		function rejectELSProductRateChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/rejectELSProductRateChangeData?approvalLogId="+id;
		}
		
		
		
		//HOHHI Product Rate
		function approveHOHHProductRateChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/HOHHproductrateUpdateApproval?approvalLogId="+id;
		}
		
		function rejectHOHHProductRateChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/HOHHproductrateReject?approvalLogId="+id;
		}
		
		
		//HOHHT Product Rate
		function approveHOHHTProductRateChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/HOHHproductratetakafulUpdateApproval?approvalLogId="+id;
		}
		
		function rejectHOHHTProductRateChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/HOHHproductratetakafulReject?approvalLogId="+id;
		}
		
		//MI Rule management
		function approveMIRuleManagementChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/approveMIRuleManagementChange?approvalLogId="+id;
		}
		
		function rejectMIRuleManagementChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/rejectMIRuleManagementChange?approvalLogId="+id;
		}
		
		//HOHH Product Info
		function approveHOHHProductInfoChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/approveHOHHProductInfoChangeData?approvalLogId="+id;
		}


		function rejectHOHHProductInfoChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/rejectHOHHProductInfoChangeData?approvalLogId="+id;
		}
		
		//MT Product Rate
		function approveMTProductRateChange(alogid) {
		var id = alogid ;
		location.href = "${pageContext.request.contextPath}/approveMTProductRateChangeData?approvalLogId="+id;

		}
		
		function rejectMTProductRateChange(alogid) {
			var id = alogid ;
			location.href = "${pageContext.request.contextPath}/rejectMTProductRateChangeData?approvalLogId="+id;
			}
		
		
			//WTC Rule management
		function approveWTCRuleManagementChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/approveWTCRuleManagementChange?approvalLogId="+id;
		}
		
		function rejectWTCRuleManagementChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/rejectWTCRuleManagementChange?approvalLogId="+id;
		}
		
		
		//WTC Rate Change Approval and Reject Function
		function approveWTCProdRateChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/approvalWTCProductrateChange?approvalLogId="+id;
		}
		
		function rejectWTCProdRateChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/rejectWTCProductrateChange?approvalLogId="+id;
		}

		
		//WTC Product Information Approval and Reject Function
		function approveWTCProdInfoChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/approvalWTCProductInfoChange?approvalLogId="+id;
		}
		
		function rejectWTCProdInfoChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/rejectWTCProductInfoChange?approvalLogId="+id;
		}	
		//Buddy PA
		function approveBuddyPAPlanChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/approveBuddyPAPlanChange?approvalLogId="+id;
		}
		
		function approveBuddyPAComboChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/approveBuddyPAComboChange?approvalLogId="+id;
		}

		function approveBuddyPABenefitChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/approveBuddyPABenefitChange?approvalLogId="+id;
		}
		
		function approveBuddyPABenefitGrpChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/approveBuddyPABenefitGrpChange?approvalLogId="+id;
		}

		function approveBuddyPAPlanGrpChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/approveBuddyPAPlanGrpChange?approvalLogId="+id;
		}

		function approveBuddyPAProductChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/approveBuddyPAProductChange?approvalLogId="+id;
		}
		
		function rejectBuddyPAChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/rejectBuddyPAChange?approvalLogId="+id;
		}
		
		//Travel Ezy
		function approveTravelEzyProductChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/approveTravelEzyProductChange?approvalLogId="+id;
		}
		
		function rejectTravelEzyProductChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/rejectTravelEzyProductChange?approvalLogId="+id;
		}

		//Trip Care
		function approveTCProdRateChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/approvalTCProductRateChange?approvalLogId="+id;
		}
		
		function rejectTCProdRateChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/rejectTCProductRateChange?approvalLogId="+id;
		}
		
		function approveTCProdInfoChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/approvalTCProductInfoChange?approvalLogId="+id;
		}
		
		function rejectTCProdInfoChange(alogid) {
			 var id = alogid ;
			location.href = "${pageContext.request.contextPath}/rejectTCProductInfoChange?approvalLogId="+id;
		}	
	 
</script>
</body>