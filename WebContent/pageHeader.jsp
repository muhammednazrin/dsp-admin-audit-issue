

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List"%>
<%@ page import="com.spring.utils.Logouturl"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>ETIQA</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<style>
.error {
	color: red;
}

.hidediv {
	display: none;
}
</style>
<style>
a.export, a.export:visited {
	text-decoration: none;
	color: #000;
	background-color: #ddd;
	border: 1px solid #ccc;
	padding: 8px;
}
</style>
</head>
<body>
	<%
		 String fn = "";
		/* if (session != null) { */
			if (session.getAttribute("user") != null) {
				String name = (String) session.getAttribute("user");
				//System.out.println("from page header " + name);
				fn = (String) session.getAttribute("FullName");
			} else {
				response.sendRedirect("/admin-login.jsp");
				
			}
		/* }  */		
	  %>


	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->

	<!-- header -->
	<div class="header">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="visible-xs-block">
						<!-- Begin button sidebar left toggle -->
						<div class="btn-collapse-sidebar-left logo">
							<i class="fa fa-bars"></i>
						</div>
						<!-- /.btn-collapse-sidebar-left -->
						<!-- End button sidebar left toggle -->
					</div>
					<div class="logo">
						<img class=""
							src="${pageContext.request.contextPath}/assets/images/logo.png"
							alt="logo">
					</div>
					<div class="title">Administration</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-sm-6">
					<!-- BEGIN TOP NAV -->
					<p align="right" valign="top">

						<%-- Welcome! <b><%=fn%></b> <b>(</b><a href=<%=Logouturl.UAM_URL_LINK%>>Logout</a><b>)</b> --%>
						Welcome!<%=session.getAttribute("userloginName")%> <b>(</b><a
							href="${pageContext.request.contextPath}/doLogout">Logout</a><b>)</b>
							&nbsp;&nbsp;<a href="${pageContext.request.contextPath}/modulelist"><b>Back</b></a> 

					</p>
					<!-- END TOP NAV -->
					<div class="clearfix"></div>
				</div>
			</div>
			<!-- /.row -->
		</div>
	</div>
	<!-- end header -->
	<script type="text/javascript">
function idleTimer() {
   var t;
   //window.onload = resetTimer;
   window.onmousemove = resetTimer; // catches mouse movements
   window.onmousedown = resetTimer; // catches mouse movements
   window.onclick = resetTimer;     // catches mouse clicks
   window.onscroll = resetTimer;    // catches scrolling
   window.onkeypress = resetTimer;  //catches keyboard actions

   function logout() {
    
       window.location.href =<%=Logouturl.UAM_LOGIN_URL_LINK%>;  //Adapt to actual logout script
   }

  function reload() {
   
         window.location = self.location.href;  //Reloads the current page
  }

  function resetTimer() {
       clearTimeout(t);
       t = setTimeout(logout, 120000);  // time is in milliseconds (1000 is 1 second)
       t= setTimeout(reload,  120000);  // time is in milliseconds (1000 is 1 second)
   }
}
idleTimer();
</script>
</body>
</html>
