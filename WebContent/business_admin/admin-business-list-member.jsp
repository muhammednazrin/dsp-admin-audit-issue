<%@ page import="java.util.List"%>

<%@ page import="java.io.*,java.util.*, java.text.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page import="com.etiqa.model.report.TransactionalReport"%>
<% java.text.DateFormat df=new java.text.SimpleDateFormat("MM/dd/yyyy"); %>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Etiqa</title>
<!-- meta info -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="Etiqa Customer Portal" />
<meta name="description" content="Etiqa">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- MAIN CSS (REQUIRED ALL PAGE)-->
<link href="assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/owl.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-select.css" rel="stylesheet">
<link href="assets/plugins/datatable/css/bootstrap.datatable.min.css"
	rel="stylesheet">
<link href="assets/plugins/datatable/css/dataTables.searchHighlight.css"
	rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/plugins/all.min.css" />
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
        <script src="libs/html5shiv.js"></script>
        <script src="libs/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
table.dataTable tbody th, table.dataTable tbody td {
	padding: 6px 0 !important;
}
</style>

<!---------------------------------------------------- BEGIN Site Header--------------------------------------------------------- -->
<jsp:include page="/siteHeader.jsp" />
<!----------------------------------------------------- END Site Header ------------------------------------------------------------>
<style>
#eib-list, #etb-list {
	display: none;
}
</style>

</head>

<body>

	<!--
        ===========================================================
        BEGIN PAGE
        ===========================================================
        -->
	<div class="wrapper inner">
		<!-- header -->
		<div class="header">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="visible-xs-block">
							<!-- Begin button sidebar left toggle -->
							<div class="btn-collapse-sidebar-left logo">
								<i class="fa fa-bars"></i>
							</div>
							<!-- /.btn-collapse-sidebar-left -->
							<!-- End button sidebar left toggle -->
						</div>
						<div class="logo">
							<img class="" src="images/logo.png" alt="logo">
						</div>
						<div class="title">Administration</div>
						<div class="clearfix"></div>
					</div>
					<div class="col-sm-6">
						<!-- BEGIN TOP NAV -->
						<div class="top-navbar">
							<div class="top-navbar-inner">

								<div class="top-nav-content">

									<!-- Begin Collapse menu nav -->
									<div class="collapse navbar-collapse" id="main-fixed-nav">
										<!-- Begin user session nav -->
										<ul class="nav-user navbar-right">
											<li class="dropdown"><a href="#fakelink"
												class="dropdown-toggle" data-toggle="dropdown"> <i
													class="fa fa-user"></i> Welcome <strong>Siti
														Amirah Umar</strong> <i class="fa fa-angle-down fa-lg"></i>
											</a>
												<ul
													class="dropdown-menu square primary margin-list-rounded with-triangle">
													<li><a href="security-setting.html">Security
															setting</a></li>
													<li class="divider"></li>
													<li><a href="logout.html">Log out</a></li>
												</ul></li>
										</ul>
										<!-- End user session nav -->
										<ul class="nav navbar-nav alert navbar-right">
											<!-- Begin nav notification -->
											<li class="dropdown"><a href="#fakelink"
												class="dropdown-toggle" data-toggle="dropdown"> <span
													class="badge badge-warning icon-count">7</span> <i
													class="fa fa-bell"></i>
											</a>
												<ul
													class="dropdown-menu square margin-list-rounded with-triangle">
													<li>
														<div class="nav-dropdown-heading">Notifications</div> <!-- /.nav-dropdown-heading -->
														<div class="nav-dropdown-content scroll-nav-dropdown">
															<ul>
																<li class="unread"><a href="#fakelink">
																		<div class="absolute-left-content icon-task progress">I</div>
																		Creating documentation <span class="small-caps">Completed
																			: Yesterday</span>
																</a></li>
																<li><a href="#fakelink">
																		<div class="absolute-left-content icon-task progress">G</div>
																		Eating sands <span class="small-caps">Deadline
																			: Tomorrow</span>
																</a></li>
																<li><a href="#fakelink">
																		<div class="absolute-left-content icon-task progress">I</div>
																		Sending payment <span class="small-caps">Deadline
																			: Next week</span>
																</a></li>
																<li class="unread"><a href="#fakelink">
																		<div class="absolute-left-content icon-task progress">G</div>
																		Sleeping under bridge <span class="small-caps">Completed
																			: Dec 31, 2013</span>
																</a></li>
																<li class="unread"><a href="#fakelink">
																		<div class="absolute-left-content icon-task progress">G</div>
																		Buying some cigarettes <span class="small-caps">Completed
																			: 2 days ago</span>
																</a></li>
															</ul>
														</div> <!-- /.nav-dropdown-content scroll-nav-dropdown -->
														<form action="notification.html">
															<button class="btn btn-warning btn-square btn-block">See
																all task</button>
														</form>
													</li>
												</ul></li>
											<!-- End nav notification -->
										</ul>
										<!-- Begin nav search form -->
										<div class="navbar-form navbar-right" role="search">
											<div class="form-group">
												<div id="custom-search-input">
													<div class="input-group col-md-12">
														<input type="text" class="  search-query form-control"
															placeholder="Search" /> <span class="input-group-btn">
															<button class="btn btn-danger" type="button">
																<span class=" glyphicon glyphicon-search"></span>
															</button>
														</span>
													</div>
												</div>
											</div>
										</div>
										<!-- End nav search form -->

									</div>
									<!-- /.navbar-collapse -->
									<!-- End Collapse menu nav -->
								</div>
								<!-- /.top-nav-content -->
							</div>
							<!-- /.top-navbar-inner -->
						</div>
						<!-- /.top-navbar -->
						<!-- END TOP NAV -->
						<div class="clearfix"></div>
					</div>

					<!-- search mobile view-->
					<div class="col-xs-11 visible-xs">
						<div class="navbar-collapse searching"
							id="bs-example-navbar-collapse-1">
							<form class="navbar-form" role="search">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Search..">
									<span class="input-group-btn">
										<button type="reset" class="btn btn-default">
											<span class="glyphicon glyphicon-remove"> <span
												class="sr-only">Close</span>
											</span>
										</button>
										<button type="submit" class="btn btn-default">

											<span class="glyphicon glyphicon-search"> <span
												class="sr-only">Search</span>
											</span>
										</button>
									</span>
								</div>
							</form>
						</div>
						<!-- /.navbar-collapse -->
					</div>
					<!-- search mobile view-->

				</div>
				<!-- /.row -->
			</div>
		</div>
		<!-- end header -->
		<!-- header second-->
		<div class="header-second">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 hidden-xs">
						<!-- Begin breadcrumb -->
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i></li>
							<li><a href="admin-system-user-role.html">List of
									Customer</a></li>
						</ol>
						<!-- End breadcrumb -->
					</div>
					<div class="col-sm-6 hidden-xs">
						<div class="col-sm-8">
							<div class="text-right last-login">Your last login was on
								01 Feb 2016 10:01:01</div>
						</div>
						<div class="col-sm-4">
							<div class="language">
								<select class="selectpicker">
									<option value="United States">English</option>
									<option value="United Kingdom">Malay</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-xs-12 visible-xs-block">
						<div class="mobile-view">
							<ul class="nav-user navbar-right">
								<li class="dropdown"><a href="#fakelink"
									class="dropdown-toggle text-right" data-toggle="dropdown">
										<i class="fa fa-user"></i> Welcome <strong>Siti
											Amirah Umar</strong> <i class="fa fa-angle-down fa-lg"></i>
								</a>
									<ul
										class="dropdown-menu square primary margin-list-rounded with-triangle pull-right">
										<li><a href="security-setting.html">Security setting</a></li>
										<li class="divider"></li>
										<li><a href="logout.html">Log out</a></li>
									</ul></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- header second-->
		<!-- BEGIN PAGE CONTENT -->
		<!-- Mobile menu -->
		<div class="page-content inner">
			<div class="container-fluid black-back">
				<!-- /.container-->
				<div class="row">
					<div class="col-sm-3 col-md-2 fluid-menu">
						<div class="sidebar-left sidebar-nicescroller admin">
							<!-- desktop menu -->
							<ul class="sidebar-menu admin">
								<li><a href="admin-dashboard.html"> <span
										class="icon-sidebar icon dashboard"></span> Dashboard
								</a></li>
								<li><a href="#"> <span
										class="icon-sidebar icon services"></span> <i
										class="fa fa-angle-right chevron-icon-sidebar"></i> System
										Administration
								</a>
									<ul class="submenu">
										<li><a href="admin-system-user-role.html">User Roles
												&amp; Permission</a></li>
										<li><a href="admin-system-audit-log.html">Audit Log
												Maintenance</a></li>
										<li><a href="admin-system-audit-trail.html">Audit
												Trail/Log Report</a></li>
										<li><a href="admin-system-log-report.html">Integration
												Log Report</a></li>
									</ul></li>
								<li class="submenu selected active"><a href="#"> <span
										class="icon-sidebar icon services"></span> <i
										class="fa fa-angle-right chevron-icon-sidebar"></i> Business
										Administration
								</a>
									<ul class="submenu visible">
										<li class=""><a href="#"> <i
												class="fa fa-angle-right chevron-icon-sidebar2"></i> Product
												Management
										</a>
											<ul class="submenu">
												<li><a href="#"> <i
														class="fa fa-angle-right chevron-icon-sidebar3"></i> Motor
														Insurance
												</a>
													<ul class="submenu">
														<li><a
															href="admin-business-product-management-motor-product-infomation.html">Product
																Information</a></li>
														<li><a
															href="admin-business-product-management-motor-product-rate.html">Product
																Rates</a></li>
														<li><a
															href="admin-business-product-management-motor-excess.html">Excess</a></li>
														<li><a
															href="admin-business-product-management-motor-loading.html">Loading</a></li>
														<li><a
															href="admin-business-product-management-motor-nvic.html">NVIC
																Listing</a></li>
														<li><a
															href="admin-business-product-management-motor-payment.html">Payment
																Method</a></li>
													</ul></li>
												<li><a href="#"> <i
														class="fa fa-angle-right chevron-icon-sidebar3"></i>
														Houseowner/Householder
												</a>
													<ul class="submenu visible">
														<li><a
															href="admin-business-product-management-house-product-infomation.html">Product
																Information</a></li>
														<li><a
															href="admin-business-product-management-house-product-rate.html">Product
																Rates</a></li>
														<li class="active selected"><a
															href="admin-business-product-management-house-payment.html">Payment
																Method</a></li>
													</ul></li>
												<li><a href="#"> <i
														class="fa fa-angle-right chevron-icon-sidebar3"></i> World
														Traveller Care
												</a>
													<ul class="submenu">
														<li><a
															href="admin-business-product-management-traveller-product-infomation.html">Product
																Information</a></li>
														<li><a
															href="admin-business-product-management-traveller-product-rate.html">Product
																Rates</a></li>
														<li><a
															href="admin-business-product-management-traveller-payment.html">Payment
																Method</a></li>
													</ul></li>
												<li><a href="#"> <i
														class="fa fa-angle-right chevron-icon-sidebar3"></i> Term
														Life
												</a>
													<ul class="submenu">
														<li><a
															href="admin-business-product-management-term-product-infomation.html">Product
																Information</a></li>
														<li><a
															href="admin-business-product-management-term-product-rate.html">Product
																Rates</a></li>
														<li><a
															href="admin-business-product-management-term-payment.html">Payment
																Method</a></li>
													</ul></li>
											</ul></li>
										<li class="active"><a
											href="admin-business-list-member.jsp">List of Customer</a></li>
										<li><a href="admin-business-segmentation.html">Segmentation</a></li>
										<li><a href="admin-business-campaign-analysis.html">Campaign
												Analysis Setup</a></li>
										<li><a href="admin-business-straight-through.html">Rules
												Management</a></li>
										<li><a href="admin-business-discount.html">Discount
												Management</a></li>
									</ul></li>
								<li><a href="#"> <span
										class="icon-sidebar icon services"></span> <i
										class="fa fa-angle-right chevron-icon-sidebar"></i> Agent
										Management
								</a>
									<ul class="submenu">
										<li><a href="agent-view-list-agent.html">List of
												Agent</a></li>
										<li><a href="agent-registration.html">Register Agent</a></li>
									</ul></li>
								<li><a href="#"> <span
										class="icon-sidebar icon services"></span> <i
										class="fa fa-angle-right chevron-icon-sidebar"></i> Cyber
										Agent
								</a>
									<ul class="submenu">
										<li><a href="agent-view-profile.html">View Profile</a></li>
										<li><a href="agent-list-of-perator.html">List of
												Operator</a></li>
										<li><a href="agent-operatorregistration.html">Register
												Operator</a></li>
									</ul></li>
								<li><a href="#"> <span
										class="icon-sidebar icon services"></span> <i
										class="fa fa-angle-right chevron-icon-sidebar"></i> Leads
										Management &amp; Campaign
								</a>
									<ul class="submenu">
										<li><a href="lead-sales-lead.html">Sales Leads Portal
												Registration</a></li>
										<li><a href="lead-sales-tools.html">Sales Tools</a></li>
										<li><a href="lead-manual-upload.html">Manual Upload</a></li>
										<li><a href="lead-abandonment-repository.html">Abandonment
												Repository</a></li>
										<li><a href="lead-campaign-banner.html">Campaign
												Banner</a></li>
										<li><a href="lead-data-download.html">Leads Data
												Download</a></li>
									</ul></li>
								<li><a href="#"> <span class="icon-sidebar icon policy"></span>
										<i class="fa fa-angle-right chevron-icon-sidebar"></i> Report
										&amp; Analytics
								</a>
									<ul class="submenu">
										<li><a href="admin-report-operator.html">Operator
												Performance</a></li>
										<li><a href="admin-report-transaction.html">Transactional
												Report</a></li>
									</ul></li>
							</ul>
						</div>
						<!-- /.sidebar-left -->
					</div>
					<div class="col-sm-9 col-md-10 white-back">
						<div class="row">
							<div>
								<!-- breadcrum -->
								<div class="breadcrum-grey">
									<div class="row">
										<div class="col-sm-12">
											<!-- Begin breadcrumb -->
											<ol class="breadcrumb">
												<li><a href="#fakelink">Business Administration</a></li>
											</ol>
											<!-- End breadcrumb -->
										</div>
									</div>
								</div>
								<!-- breadcrum -->
							</div>
							<div class="col-sm-11 gap-mid">
								<div class="row">
									<div class="col-sm-12">
										<div class="content-inner">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">
														<div class="sub">
															<h4>List of Customer</h4>
														</div>
													</div>
													<div class="content-inner">
														<div class="the-box">
															<div class="gap gap-mini">
																<form class="form-inline">
																	<div class="form-group">
																		<label for="exampleInputName2">Search Member :
																		</label> <input type="text" name="email"
																			placeholder="Name/Email" class="form-control">
																	</div>
																	<div class="poll-date pull-right">
																		<div class="form-group">
																			<label for="exampleInputName2">Search by date
																				: From</label> <input type="text" name="dateFrom"
																				placeholder="date" id="datepicker1"
																				class="form-control">
																		</div>
																		<div class="form-group">
																			<label for="exampleInputEmail2">To</label> <input
																				type="email" name="dateTo" placeholder="date"
																				id="datepicker2" class="form-control">
																		</div>
																		<button class="btn btn-warning" type="submit">Search</button>
																	</div>
																</form>
																<div class="clearfix"></div>
															</div>
														</div>
													</div>
												</div>

												<div class="col-sm-12">
													<!-- Begin  table -->
													<div class="content-inner">
														<div class="the-box full no-border">
															<div class="row">
																<div class="col-xs-12 form-inline">
																	<div class="col-xs-6">
																		<div class="row">
																			<label> <select class="form-control">
																					<option>5</option>
																					<option>10</option>
																					<option>25</option>
																					<option>50</option>
																					<option>100</option>
																			</select> records per page
																			</label>
																		</div>
																	</div>
																	<div class="col-xs-6">
																		<div class="row">
																			<div class="pull-right">
																				<button class="btn btn-warning btn-sm">
																					Export to XLS <i class="fa fa-download"
																						aria-hidden="true"></i>
																				</button>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="table-responsive">
																<table
																	class="table table-striped table-warning table-hover">
																	<thead>
																		<tr>
																			<th style="width: 30px;">No</th>
																			<th>Email</th>
																			<th>Full Name</th>
																			<th>Contact No</th>
																			<th>State</th>
																			<th>Status</th>
																			<th>Date</th>

																		</tr>
																	</thead>
																	<tbody>
																		<tr>
																			<td>1</td>
																			<td>etiqa@gmail.com</td>
																			<td>Azhar Sulaiman</td>
																			<td>017892874</td>
																			<td>Selangor</td>
																			<td>Open</td>
																			<td>01-Jan-2020</td>

																		</tr>


																	</tbody>
																</table>
															</div>
															<!-- /.table-responsive -->
														</div>
														<!-- /.the-box -->
													</div>
													<!-- End warning color table -->
												</div>
												<div class="row">
													<div class="col-xs-12">
														<div class="col-sm-6">Showing 1 to 5 of 15 entries</div>
														<div class="col-sm-6">
															<ul class="pagination pull-right">
																<li class="disabled"><a href="#">&laquo;</a></li>
																<li class="active"><a href="#">1</a></li>
																<li><a href="#">2</a></li>
																<li><a href="#">3</a></li>
																<li><a href="#">4</a></li>
																<li><a href="#">5</a></li>
																<li><a href="#">&raquo;</a></li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!--row -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->

		</div>
		<!-- /.page-content -->
		<!-- BEGIN FOOTER -->
		<div class="footer">
			<footer>
				<div class="fluid-yellow">
					<div class="container">
						<div class="row">
							<div class="col-sm-9">
								<ul class="left gap">
									<li><a href="faq.html"><i class="fa fa-question"></i>FAQ</a></li>
									<li><a href="help.html"><i class="fa fa-medkit"></i>Help</a></li>
									<li><a href="contactus.html"><i class="fa fa-envelope"></i>Contact
											Us</a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-3">
								<div class="text-right gap">
									<img class="" src="images/member-my.png" alt="logo">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="fluid-grey">
					<div class="container">
						<div class="row">
							<div class="col-sm-6">
								<div class="gap">&copy; 2016 Etiqa. All Rights Reserved</div>
							</div>
							<div class="col-sm-6">
								<ul class="right">
									<li><a href="terms.html">Terms</a></li>
									<li><a href="privacy.html">Privacy</a></li>
									<li><a href="disclaimer.html">Disclaimer</a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>
		<!-- END FOOTER -->
	</div>
	<!-- /.wrapper -->
	<!-- END PAGE CONTENT -->

	<!-- PLUGINS -->
	<script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets/plugins/prettify/prettify.js"></script>
	<script
		src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

	<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="assets/plugins/icheck/icheck.min.js"></script>
	<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="assets/plugins/timepicker/bootstrap-timepicker.js"></script>
	<script src="assets/plugins/mask/jquery.mask.min.js"></script>
	<script src="assets/plugins/validator/bootstrapValidator.min.js"></script>
	<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/js/bootstrap.datatable.js"></script>
	<script src="assets/plugins/datatable/js/jquery.highlight.js"></script>
	<script
		src="assets/plugins/datatable/js/dataTables.searchHighlight.min.js"></script>
	<script src="assets/plugins/summernote/summernote.min.js"></script>
	<script src="assets/plugins/markdown/markdown.js"></script>
	<script src="assets/plugins/markdown/to-markdown.js"></script>
	<script src="assets/plugins/markdown/bootstrap-markdown.js"></script>
	<script src="assets/plugins/slider/bootstrap-slider.js"></script>
	<script src="assets/plugins/toastr/toastr.js"></script>
	<script src="assets/plugins/newsticker/jquery.newsTicker.min.js"></script>
	<script src="assets/plugins/placeholder/jquery.placeholder.js"></script>
	<script
		src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>
	<script type="text/javascript" src="assets/js/shieldui-all.min.js"></script>
	<script type="text/javascript" src="assets/js/jszip.min.js"></script>
	<!--  Table Export -->
	<!-- KNOB JS -->
	<!--[if IE]>
       <script type="text/javascript" src="plugins/jquery-knob/excanvas.js"></script>
       <![endif]-->
	<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
	<script src="assets/plugins/jquery-knob/knob.js"></script>

	<link href="assets/js/jquery.dataTables.min.css" rel="stylesheet"
		type="text/css">
	<link href="assets/js/buttons.dataTables.min.css" rel="stylesheet"
		type="text/css">
	<link href="assets/js/bootstrap.min.css" rel="stylesheet"
		type="text/css">
	<link href="assets/js/dataTables.bootstrap.min.css" rel="stylesheet"
		type="text/css">
	<link href="assets/js/buttons.bootstrap.min.css" rel="stylesheet"
		type="text/css">

	<script src="assets/js/jquery-1.12.3.js"></script>
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/dataTables.buttons.min.js"></script>
	<script src="assets/js/buttons.bootstrap.min.js"></script>
	<script src="assets/js/jszip.min.js"></script>
	<script src="assets/js/pdfmake.min.js"></script>
	<script src="assets/js/vfs_fonts.js"></script>
	<script src="assets/js/buttons.html5.min.js"></script>
	<script src="assets/js/buttons.print.min.js"></script>
	<script src="assets/js/buttons.colVis.min.js"></script>
</body>
</html>