package com.etiqa.common.DB;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.sql.DriverManager;

public class cwpdbutil {
	public Connection getConnection() throws Exception {
		Context ctx = null;
		Connection conn = null;
		try {

			
			ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup("jdbc/csuser");
			conn = ds.getConnection();
			return conn;
		} catch (SQLException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
	}
}
