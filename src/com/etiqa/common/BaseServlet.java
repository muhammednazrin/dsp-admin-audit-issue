package com.etiqa.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class BaseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public static final String DAO_FACTORY_CLASS = "daoFactoryClass";
	public static final String ACTION = "action";
	public static final String ACTION_EVENT_LIST = "list";
	public static final String ACTION_EVENT_LISTALL = "listAll";
	public static final String ACTION_EVENT_ADD = "add";
	public static final String ACTION_EVENT_SAVE = "save";
	public static final String ACTION_EVENT_EDIT = "edit";
	public static final String ACTION_EVENT_UPDATE = "update";
	public static final String ACTION_EVENT_DELETE = "delete";
	public static final String ACTION_EVENT_RECORD = "record";
	public static final String ACTION_EVENT_CHANGEPW = "changepassword";

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		handleRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		handleRequest(request, response);
	}

	protected void forward(HttpServletRequest request, HttpServletResponse response, String view) {
		try {
			request.getRequestDispatcher(view).forward(request, response);
		} catch (ServletException e) {
			throw new RuntimeException(e.getMessage());
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	public abstract void handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException;
}
