package com.etiqa.ldap;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.etiqa.dsp.dao.common.pojo.RegisterEmailVo;
import com.etiqa.email.ProductMailTemplateLoad;

public class EmailCPF {
	static Properties mailServerProperties;
	static Session getMailSession;
	static MimeMessage generateMailMessage;
	private static Properties activeDirMailProp = new Properties();
	ProductMailTemplateLoad productTemplate = null;

	public void DspEmailDispatchProcessor(ProductMailTemplateLoad productTemplate) {
		this.productTemplate = productTemplate;
	}

	public String generateAndSendEmail(RegisterEmailVo registerEmailVo) throws FileNotFoundException {
		loadEmailProps();
		String res = null;
		String Subject = registerEmailVo.getEmailSubject();
		String Content = registerEmailVo.getTemplateEmail();

		
		// Assuming you are sending email from localhost
		String host = activeDirMailProp.getProperty("emailhost");
		String fromemail = activeDirMailProp.getProperty("from");
		String port = activeDirMailProp.getProperty("emailport");
	
		

		// Get system properties
		// SMTP Prod Email---
		Properties prop = System.getProperties();
		 //prop.setProperty("mail.smtp.host", host);
		 prop.put("mail.smtp.host",host); 
		 prop.put("mail.smtp.port", port);
		 prop.put("mail.smtp.auth", "false");
		 prop.put("mail.smtp.starttls.enable", "false");
		 prop.put("mail.smtp.ssl.trust", host);
		Session session = Session.getDefaultInstance(prop);
		// END SMPTP PROD
		// smtp.gmail.com

	
		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromemail));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(registerEmailVo.getUserEmail()));
			message.setSubject(Subject);
			message.setContent(Content, "text/html");

			Transport.send(message);

			res = "Done";
			System.out.println("response" + res);
			/*
			 * //InternetAddress.parse(emailTo) Message message = new MimeMessage(session);
			 * message.setFrom(new InternetAddress(fromemail));
			 * message.setRecipients(Message.RecipientType.TO,
			 * InternetAddress.parse(template.getEmailTo()));
			 * message.setSubject(template.getTemplateSubject());
			 * message.setContent(template.getTemplateBody(), "text/html");
			 *
			 *
			 *
			 * BodyPart messageBodyPart = new MimeBodyPart();
			 * messageBodyPart.setText(template.getTemplateBody());
			 * messageBodyPart.setContent(template.getTemplateBody(), "text/html");
			 *
			 * Multipart multipart = new MimeMultipart();
			 * multipart.addBodyPart(messageBodyPart);
			 *
			 * String filePath =
			 * "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs";
			 * String fileName = PolicyNo+".zip";
			 *
			 * File file = new File(filePath+"/"+fileName); if(!file.exists()){
			 *
			 * String filePath_tmp =
			 * "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_backup/admin";
			 * File file_tmp = new File(filePath_tmp+"/"+fileName);
			 *
			 * System.out.println("zipfilename------"+filePath_tmp+"/"+fileName);
			 * addAttachment(multipart, filePath_tmp+"/"+fileName,fileName); // Send the
			 * complete message parts message.setContent(multipart);
			 * Transport.send(message); res ="Done"; } else{
			 *
			 *
			 * System.out.println("zipfilename------"+filePath+"/"+fileName);
			 * addAttachment(multipart, filePath+"/"+fileName,fileName); // Send the
			 * complete message parts message.setContent(multipart);
			 * Transport.send(message); res ="Done";
			 *
			 * }
			 *
			 *
			 *
			 *
			 *
			 * // String filePath = "/tmp/"+PolicyNo+"/";
			 *
			 *
			 * // List<GeneratingReportsResponseVo> FileNamelist=
			 * //for(GeneratingReportsResponseVo fileName : FileNamelist){ // PolicyNo=
			 * fileName.getPolicyNo(); // System.out.println("Fin Obj: "+PolicyNo); //
			 * addAttachment(multipart, filePath+PolicyNo+".zip",PolicyNo+".zip"); //}
			 *
			 * // Send the complete message parts // message.setContent(multipart);
			 * //Transport.send(message); //res ="Done";
			 */ } catch (MessagingException e) {
			e.printStackTrace();
			res = "Failed";
		} catch (Exception e) {
			e.printStackTrace();
			res = "Failed";
		}
		return res;
	}

	private void loadEmailProps() {
		InputStream infoad = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/dsp/sales/process/email/emailconfig.properties");
		try {
			System.out.println(infoad + "mail File");
			activeDirMailProp.load(infoad); // com.etiqa.dsp.sales.process.email
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
