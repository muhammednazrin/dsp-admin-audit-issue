package com.etiqa.email;

import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailService {
	public void sendHtmlEmail(String host, String port, final String userName, final String password, String toAddress,
			String subject, String message) throws AddressException, MessagingException { // sets SMTP server properties
		Properties properties = new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.auth", "false");
		properties.put("mail.smtp.starttls.enable", "false");
		// creates a new session with an authenticator
		Authenticator auth = new Authenticator() {
			@Override
			public PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(userName, password);
			}
		};
		Session session = Session.getInstance(properties, auth);
		session.setDebug(true);
		// creates a new e-mail message
		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(userName));
		InternetAddress[] toAddresses = { new InternetAddress(toAddress) };
		msg.setRecipients(Message.RecipientType.TO, toAddresses);
		msg.setSubject(subject);
		msg.setSentDate(new Date());
		// set plain text message
		msg.setContent(message, "text/html");
		// sends the e-mail
		Transport.send(msg);
	}

	public static void main(String[] args) {
		// SMTP server information
		String host = "smtp.gmail.com";
		String port = "587";
		String mailFrom = "raju.comp@gmail.com";
		String password = "rajeeblochanpanda";
		String test = "Rajeeb lochan panda";
		// outgoing message information
		String mailTo = "raju.comp@gmail.com";
		String subject = "Hello my friend";
		String color = "background-color:#FFC000";
		// message contains HTML markups
		String message = "<i>Greetings!</i><br>";
		message += "<b>Dear  MAZLIWATI MOHD,</b><br><br><br>";
		message += "Thank you for subscribing World Traveller Care plan.<br>";
		message += "Here are some information which maybe helpful for you:<br><br><br><br>";
		message += "In case of any emergency during your travels, you may call our 24-hour Travel and Medical Assistance Helpline at 03-2161 0270<br><br>";
		message += "Please keep all original receipts and related documents (i.e.: boarding pass, e-ticket/flight ticket, airline letter/report of luggage delayed/damaged, police report) in case you need to make a claim<br>";
		message += "Please find attached a copy of your e-Policy and Tax Invoice./ Receipt for your reference:<br><br><br>";
		message += "World Traveller Care<br><br>";

		message += "<b>___________________________________________________________________________________________________________</b><br><br><br>";

		message += "Transaction Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;  "
				+ test + "<br>";
		message += "Transaction No. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;  "
				+ test + "<br>";
		message += "Policy Holder Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ test + "<br>";
		message += "NRIC No. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>:</b>&nbsp;&nbsp;"
				+ test + "<br>";
		message += "Plan Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ test + "<br>";
		message += "Contribution Frequency &nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;" + test + "<br>";
		message += "Effective Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>:</b>&nbsp;&nbsp;"
				+ test + "<br>";
		message += "e-Policy No. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;;"
				+ test + "<br>";
		message += "Tax Invoice No &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ test + "<br>";
		message += "Payment Mode &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ test + "<br>";
		message += "Premium Amount (RM) &nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;" + test + "<br><br><br>";

		message += "Please visit www.etiqa.com.my if you would like to make a nomination for this policy.<br><br><br>";

		message += "<div style=" + color
				+ ">Travel with a peace of mind knowing that your house is protected while you�re away with our Houseonwer/Householder home protection plan.</br> "
				+ "Enjoy a 15% discount when you sign up online. Click here to find out more. </br></div><br>";
		// message += "Enjoy a 15% discount when you sign up online. Click here to find
		// out more.<br>";

		message += "<b><font>Note:  &nbsp; &nbsp;</font></b>";
		message += "This is an auto email notification. Please do not reply this email. You may contact our Etiqa Oneline at 1300 13 8888 or email us at info@etiqa.com.my.<br>";

		message += "Thank you.<br><br>";
		message += "Yours Sincerely,<br><br>";
		message += "Etiqa Insurance Berhad<br><br>";

		// message += "<b><font color=red>Duke</font></b><br>";

		EmailService mailer = new EmailService();
		try {
			mailer.sendHtmlEmail(host, port, mailFrom, password, mailTo, subject, message);
			System.out.println("Email sent.");
		} catch (Exception ex) {
			System.out.println("Failed to sent email.");
			ex.printStackTrace();
		}
	}
}
