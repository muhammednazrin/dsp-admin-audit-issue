package com.etiqa.email;

import java.io.FileNotFoundException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.dsp.dao.common.pojo.custQuotPmntPolicyVo;

import oracle.jdbc.OracleTypes;

public class EzyLifeEmailService {

	public String callingEzyLifeEmailService(String policyNo) {

		Connection connection = null;
		Statement statement = null;
		ResultSet rs = null;

		String langVal = "";
		connection = ConnectionFactory.getConnection();
		custQuotPmntPolicyVo cdvo = new custQuotPmntPolicyVo();
		CallableStatement cstmt;
		try {
			cstmt = connection.prepareCall("{CALL DSP_ADM_SP_TLINFO_SELECT(?,?,?)}");

			cstmt.setString(1, policyNo);
			cstmt.setString(2, langVal);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(3);
			while (rs.next()) {
				cdvo.setPOLICY_NUMBER(rs.getString(1));
				cdvo.setDSP_QQ_ID(rs.getInt(2));
				cdvo.setCUSTOMER_NAME(rs.getString(3));
				cdvo.setCUSTOMER_EMAIL(rs.getString(4));
				cdvo.setCUSTOMER_NRIC_ID(rs.getString(5));
				cdvo.setDOB(rs.getString(6));
				cdvo.setCREATED_DATE(rs.getString(7));
				cdvo.setTRANSACTION_ID(rs.getInt(8));
				cdvo.setFULL_NAME(rs.getString(9));
				cdvo.setPREMIUM_PAYMENT_FREQUENCY_TYPE(rs.getString(10));
				cdvo.setPREMIUM_PAYMENT_FREQUENCY(rs.getString(11));
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*
		 * C.POLICY_NUMBER||C.TL_CHECK_DIGIT, B.DSP_QQ_ID, A.CUSTOMER_NAME,
		 * A.CUSTOMER_EMAIL, A.CUSTOMER_NRIC_ID, D.DOB, B.CREATED_DATE,
		 * C.TRANSACTION_ID, D.FULL_NAME, D.PREMIUM_PAYMENT_FREQUENCY_TYPE,
		 * D.PREMIUM_PAYMENT_FREQUENCY , D.PREMIUM_PAYMENT_FREQUENCY
		 */

		DspEmailDispatchProcessor emailProcessor = new DspEmailDispatchProcessor(new TermLifeProductMailTemplateLoad());
		String status = null;
		try {
			status = emailProcessor.emailDispatchProcess(cdvo);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(status + "email Service status");
		return status;
	}
}
