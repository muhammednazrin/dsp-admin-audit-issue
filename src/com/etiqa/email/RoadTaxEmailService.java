package com.etiqa.email;

import java.io.FileNotFoundException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.etiqa.common.DB.ConnectionFactory;

//import com.etiqa.dsp.Admin.ConnectionFactory;

import com.etiqa.dsp.dao.common.pojo.MotorInsuranceCustDetails;
import com.etiqa.dsp.dao.common.pojo.custQuotPmntPolicyVo;

import oracle.jdbc.OracleTypes;

public class RoadTaxEmailService {
	public String callingRoadTaxEmailService(String policyNumber, String roadTaxMailStatus, String consignmentNo,
			String address, String expDeliveryDt) {

		Connection connection = null;
		Statement statement = null;
		ResultSet rs = null;

		String langVal = "";
		connection = ConnectionFactory.getConnection();
		MotorInsuranceCustDetails micdvo = new MotorInsuranceCustDetails();

		CallableStatement cstmt;
		try {

			cstmt = connection.prepareCall("{CALL DSP_ADM_SP_ROADTAX_SELECT(?,?)}");

			cstmt.setString(1, policyNumber);
			cstmt.registerOutParameter(2, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(2);

			while (rs.next()) {
				micdvo.setPOLICY_NUMBER(rs.getString(1));
				micdvo.setCUSTOMER_NAME(rs.getString(2));
				micdvo.setCUSTOMER_NRIC_ID(rs.getString(3));
				micdvo.setCUSTOMER_EMAIL(rs.getString(4));
				micdvo.setCUSTOMER_DOB(rs.getString(5));
				micdvo.setREGISTRATION_NUMBER(rs.getString(6));
				micdvo.setPOLICY_EXPIRY_TIMESTAMP(rs.getString(7));
				micdvo.setPRODUCT_CODE(rs.getString(8));

			}

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		DspEmailDispatchProcessor emailProcessor = new DspEmailDispatchProcessor(new TermLifeProductMailTemplateLoad());
		String status = null;
		custQuotPmntPolicyVo cdvo = new custQuotPmntPolicyVo();
		cdvo.setCUSTOMER_NAME(micdvo.getCUSTOMER_NAME());
		cdvo.setCUSTOMER_EMAIL(micdvo.getCUSTOMER_EMAIL());
		cdvo.setPOLICY_NUMBER(micdvo.getPOLICY_NUMBER());
		cdvo.setPRODUCT_CODE(micdvo.getPRODUCT_CODE());
		// Todo
		cdvo.setRoadTasMailStatus(roadTaxMailStatus);
		cdvo.setConsignmentNo(consignmentNo);
		cdvo.setDeliveryAddress(address);
		cdvo.setExpDeliveryDt(expDeliveryDt);

		try {
			status = emailProcessor.emailDispatchProcessForRoadTax(cdvo);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(status + "email Service status");
		return status;
	}

	public String callingRoadTaxThresholdEmailService(String eMailIds, String thresholdLimit, String currentBalance,
			String entity) {

		MotorInsuranceCustDetails micdvo = new MotorInsuranceCustDetails();

		DspEmailDispatchProcessor emailProcessor = new DspEmailDispatchProcessor(new TermLifeProductMailTemplateLoad());

		String status = null;
		custQuotPmntPolicyVo cdvo = new custQuotPmntPolicyVo();
		cdvo.setCUSTOMER_NAME(micdvo.getCUSTOMER_NAME());
		cdvo.setCUSTOMER_EMAIL(micdvo.getCUSTOMER_EMAIL());
		cdvo.setPOLICY_NUMBER(micdvo.getPOLICY_NUMBER());
		cdvo.setPRODUCT_CODE(micdvo.getPRODUCT_CODE());
		// Todo
		cdvo.setRoadTasMailStatus("thresholdAlert");
		cdvo.seteMailIds(eMailIds);
		cdvo.setThresholdLimit(thresholdLimit);
		cdvo.setCurrentBalance(currentBalance);
		cdvo.setEntity(entity);

		try {
			status = emailProcessor.emailDispatchProcessForRoadTax(cdvo);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("callingRoadTaxThresholdEmailService<status> =>" + status);
		return status;
	}

	public String gettingValueisNullOrNotInString(int indexVal, ResultSet rs) {
		String iVal = null;
		try {
			System.out.println(indexVal + "indexVal");
			iVal = rs.getString(indexVal);
			Double Value = Double.parseDouble(iVal);
			iVal = String.format("%.2f", Value);
			if (rs.wasNull()) {

				System.out.println(indexVal + "indexVal  value null or empty In Double");
				iVal = "0.00";
			}

			System.out.println(iVal);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return iVal;
	}
}
