package com.etiqa.email;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import com.etiqa.dsp.dao.common.pojo.MotorInsuranceCustDetails;
import com.etiqa.dsp.dao.common.pojo.WTCcustQuotPmntPolicyVo;
import com.etiqa.dsp.dao.common.pojo.custQuotPmntPolicyVo;
import com.etiqa.dsp.dao.email.DspEmailTemplateBean;
import com.etiqa.dsp.dao.email.WtcDspEmailTemplateBean;

public class TermLifeProductMailTemplateLoad implements ProductMailTemplateLoad {
	String encryptionKey = "NAkjfrwGsLpRrfOr";
	DecriptionEncription advancedEncryptionStandard = new DecriptionEncription(encryptionKey);

	@Override
	public DspEmailTemplateBean loadMailTemplate(custQuotPmntPolicyVo custVo) {
		System.out.println(custVo.getCUSTOMER_NAME() + "c name\n" + custVo.getCUSTOMER_EMAIL() + "email ID");
		DspEmailTemplateBean templateBean = new DspEmailTemplateBean();

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy  h:mm:ss a");
		String formattedDate = sdf.format(date);

		String subject = "Ezy-Life Secure Policy Detail (" + custVo.getPOLICY_NUMBER() + ")";

		/*
		 * String message = ""; message +=
		 * "<b>Dear   "+custVo.getCUSTOMER_NAME()+",</b><br><br><br>"; message +=
		 * "Congratulations! Your transaction is successful. We are pleased to extend our warmest welcome to you for being a part of our family.<br><br>"
		 * ; message +=
		 * "Please find the copy of your e-policy and receipt as attached for your reference.<br><br>"
		 * ;
		 *
		 * message += "<b>Ezy-Life Secure</b><br>"; message += "<hr>"; message +=
		 * "<br>"; message +=
		 * "Transaction Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: "
		 * +formattedDate+"<br><br>"; message +=
		 * "Transaction No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: "
		 * +custVo.getTRANSACTION_ID()+"<br><br>"; message +=
		 * "Participant Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: "
		 * +custVo.getCUSTOMER_NAME()+"<br><br>"; message +=
		 * "NRIC No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: "
		 * +custVo.getCUSTOMER_NRIC_ID()+"<br><br>"; message +=
		 * "Plan Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Ezy-Life Secure<br><br>"
		 * ; message += "Contribution Frequency&nbsp;&nbsp;&nbsp;: "+custVo.
		 * getPREMIUM_PAYMENT_FREQUENCY_TYPE()+"<br><br>"; message +=
		 * "E-Policy No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: "
		 * +custVo.getPOLICY_NUMBER()+"<br><br>"; message +=
		 * "Receipt No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: "
		 * +custVo.getPOLICY_NUMBER()+"<br><br>"; message +=
		 * "Payment Mode&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Credit Card (VISA/MASTER) <br><br>"
		 * ; message +=
		 * "Contribution Amount (RM)&nbsp;: "+custVo.getPREMIUM_PAYMENT_FREQUENCY()
		 * +" <br><br>"; message +=
		 * "Please visit www.etiqa.com.my if you wish to perform nomination in the future."
		 * ; message += "<br><br>"; message +=
		 * "Note: This is an auto email notification. Please do not reply this email. You may call our Etiqa"
		 * ; message +=
		 * "Oneline call centre at 1300 13 8888 or email us at info@etiqa.com.my.<br><br>"
		 * ;
		 *
		 * message += "Thank you.<br><br>"; message += "Yours Sincerely,<br>"; message
		 * += "Etiqa Insurance Berhad<br><br>";
		 */

		/*
		 * String templateBody = "Dear "+custVo.getCUSTOMER_NAME() +
		 * "\n Congratulations on successfully " +
		 * "purchasing the insurance Policy no :"+custVo.getPOLICY_NUMBER()+
		 * "\n\n\nRegards," + "Etiqa Insurance BHD";
		 */
		Double SumInsuredDoubleVal = null;
		System.out.println("custVo.getPREMIUM_PAYMENT_FREQUENCY() " + custVo.getPREMIUM_PAYMENT_FREQUENCY());
		if (custVo.getPREMIUM_PAYMENT_FREQUENCY() != null) {
			SumInsuredDoubleVal = Double.parseDouble(custVo.getPREMIUM_PAYMENT_FREQUENCY());
		}
		DecimalFormat format = new DecimalFormat("#,###.00");
		String formatted = format.format(SumInsuredDoubleVal);
		String frequencyType = "";
		if (custVo.getPREMIUM_PAYMENT_FREQUENCY_TYPE().equals("annualy")) {
			frequencyType = "Annually";
		} else {
			frequencyType = "Monthly";
		}
		String message = "";
		message += "<b>Dear   " + custVo.getCUSTOMER_NAME() + ",</b><br><br><br>";
		message += "Congratulations! Your transaction is successful. We are pleased to extend our warmest welcome to you for being a part of our family.<br><br>";
		message += "Please find the copy of your e-policy and receipt as attached for your reference.<br><br>";

		message += "<b>Ezy-Life Secure</b><br>";
		message += "<hr>";
		message += "<br>";
		message += "Transaction Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: "
				+ custVo.getCREATED_DATE() + "<br><br>";//
		message += "Transaction No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: "
				+ custVo.getTRANSACTION_ID() + "<br><br>";
		message += "Participant Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: "
				+ custVo.getCUSTOMER_NAME() + "<br><br>";
		message += "NRIC No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: "
				+ custVo.getCUSTOMER_NRIC_ID() + "<br><br>";
		message += "Plan Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Ezy-Life Secure<br><br>";
		message += "Contribution Frequency&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: " + frequencyType + "<br><br>";
		message += "E-Policy No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: "
				+ custVo.getPOLICY_NUMBER() + "<br><br>";
		message += "Receipt No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: "
				+ custVo.getPOLICY_NUMBER() + "<br><br>";
		message += "Payment Mode&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Credit Card (VISA/MASTER) <br><br>";
		message += "Contribution Amount (RM)&nbsp;: " + formatted + "<br><br>";
		message += "Please visit www.etiqa.com.my if you wish to perform nomination in the future.";
		message += "<br><br>";
		message += "Note: This is an auto email notification. Please do not reply this email. You may call our Etiqa";
		message += "Oneline call centre at 1300 13 8888 or email us at info@etiqa.com.my.<br><br>";

		message += "Thank you.<br><br>";
		message += "Yours Sincerely,<br>";
		message += "Etiqa Insurance Berhad<br><br>";
		String templateBody = message;
		templateBean.setTemplateSubject(subject);
		templateBean.setTemplateBody(templateBody);
		templateBean.setEmailTo(custVo.getCUSTOMER_EMAIL());
		return templateBean;
	}

	@Override
	public DspEmailTemplateBean loadMailTemplateForUpdatedNominationForm(custQuotPmntPolicyVo cdvo) {
		// Ezy-Life Secure Policy Detail (Updated Nomination)
		System.out.println(cdvo.getCUSTOMER_NAME() + "c name\n" + cdvo.getCUSTOMER_EMAIL() + "email ID");
		DspEmailTemplateBean templateBean = new DspEmailTemplateBean();

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy  h:mm:ss a");
		String formattedDate = sdf.format(date);

		String subject = "Ezy-Life Secure Policy Detail (Updated Nomination)";
		String message = "";
		message += "<b>Dear   " + cdvo.getCUSTOMER_NAME() + ",</b><br><br><br>";
		message += "Congratulations! Your nomination is successful.<br><br>";
		message += "Please find the copy of your updated nomination form as attached for your reference.<br><br>";

		message += "Note: This is an auto email notification. Please do not reply this email. You may call our Etiqa Oneline call centre at 1300 13 8888 or email us at info@etiqa.com.my.<br><br>";

		message += "Thank you.<br><br>";
		message += "Yours Sincerely,<br>";
		message += "Etiqa Insurance Berhad<br><br>";
		String templateBody = message;
		templateBean.setTemplateSubject(subject);
		templateBean.setTemplateBody(templateBody);
		templateBean.setEmailTo(cdvo.getCUSTOMER_EMAIL());
		return templateBean;
	}

	@Override
	public DspEmailTemplateBean loadMailTemplateBM(custQuotPmntPolicyVo custVo) {
		System.out.println(custVo.getCUSTOMER_NAME() + "c name\n" + custVo.getCUSTOMER_EMAIL() + "email ID");
		DspEmailTemplateBean templateBean = new DspEmailTemplateBean();

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy  h:mm:ss a");
		String formattedDate = sdf.format(date);

		String subject = "Maklumat Polisi Ezy-Life Secure  (" + custVo.getPOLICY_NUMBER() + ")";

		Double SumInsuredDoubleVal = null;
		if (custVo.getPREMIUM_PAYMENT_FREQUENCY() != null) {
			SumInsuredDoubleVal = Double.parseDouble(custVo.getPREMIUM_PAYMENT_FREQUENCY());
		}
		DecimalFormat format = new DecimalFormat("#,###.00");
		String formatted = format.format(SumInsuredDoubleVal);
		String frequencyType = "";
		if (custVo.getPREMIUM_PAYMENT_FREQUENCY_TYPE().equals("annualy")) {
			frequencyType = "Annually";
		} else {
			frequencyType = "Monthly";
		}

		String Mode = "";
		if (custVo.getLangValue().equals("lan_en")) {
			if (custVo.getPREMIUM_PAYMENT_FREQUENCY_TYPE().equals("annualy")) {
				Mode = "Yearly";
			} else {
				Mode = "Monthly";
			}
		} else {
			if (custVo.getPREMIUM_PAYMENT_FREQUENCY_TYPE().equals("monthly")) {
				Mode = "bulanan";
			} else {
				Mode = "tahunan";
			}
		}

		String message = "";
		message += "<b>Kepada    " + custVo.getCUSTOMER_NAME() + ",</b><br><br><br>";
		message += "Tahniah! Transaksi anda telah berjaya. Kami gembira untuk mengalu-alukan kedatangan anda menjadi sebahagian daripada keluarga kami.<br><br>";
		message += "Sila dapatkan salinan e-Polisi dan resit yang dilampirkan untuk rujukan anda.<br><br>";

		message += "<b>Ezy-Life Secure</b><br>";
		message += "<hr>";
		message += "<br>";
		message += "Tarikh Transaksi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: "
				+ formattedDate + "<br><br>";
		message += "No Transaksi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: "
				+ custVo.getTRANSACTION_ID() + "<br><br>";
		message += "Nama Pemegang Polisi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: " + custVo.getCUSTOMER_NAME() + "<br><br>";
		message += "No. Kad Pengenalan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: "
				+ custVo.getCUSTOMER_NRIC_ID() + "<br><br>";
		message += "Nama Pelan  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Ezy-Life Secure<br><br>";
		message += "Kekerapan Sumbangan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: " + Mode + "<br><br>";
		message += "No. e-Sijil&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: "
				+ custVo.getPOLICY_NUMBER() + "<br><br>";
		message += "No Resit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: "
				+ custVo.getPOLICY_NUMBER() + "<br><br>";
		message += "Cara Pembayaran&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Credit Card (VISA/MASTER) <br><br>";
		message += "Jumlah Sumbangan (RM)&nbsp;&nbsp;: " + formatted + "<br><br>";
		message += "Sila lawati www.etiqa.com.my jika anda ingin melakukan penamaan calon di masa akan datang.";
		message += "<br><br>";
		message += "Nota: Ini adalah pemberitahuan e-mel secara automatik. Sila jangan membalas emel ini. Anda boleh menghubungi pusat panggilan Etiqa Oneline kami di talian 1300 13 8888 atau email kepada kami di info@etiqa.com.my.";
		message += "<br><br>";

		message += "Terima kasih.<br><br>";
		message += "Yang ikhlas,<br>";
		message += "Etiqa Insurance Berhad<br><br>";
		String templateBody = message;
		templateBean.setTemplateSubject(subject);
		templateBean.setTemplateBody(templateBody);
		templateBean.setEmailTo(custVo.getCUSTOMER_EMAIL());
		return templateBean;
	}

	@Override
	public DspEmailTemplateBean loadMailTemplateForUpdatedNominationFormBM(custQuotPmntPolicyVo cdvo) {
		// Ezy-Life Secure Policy Detail (Updated Nomination)
		System.out.println(cdvo.getCUSTOMER_NAME() + "c name\n" + cdvo.getCUSTOMER_EMAIL() + "email ID");
		DspEmailTemplateBean templateBean = new DspEmailTemplateBean();

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy  h:mm:ss a");
		String formattedDate = sdf.format(date);

		String subject = "Maklumat Polisi Ezy-Life Secure (Kemaskini Penamaan)";
		String message = "";
		message += "<b>Kepada   " + cdvo.getCUSTOMER_NAME() + ",</b><br><br><br>";
		message += "Tahniah! Penamaan anda telah berjaya.<br><br>";
		message += "Sila dapatkan salinan borang penamaan yang telah dikemaskini seperti yang dilampirkan untuk rujukan anda.<br><br>";

		message += "Nota: Ini adalah pemberitahuan emel automatik. Sila jangan membalas emel ini. Anda boleh menghubungi pusat panggilan Etiqa Oneline kami di 1300 13 8888 atau emel kepada kami di info@etiqa.com.my.<br><br>";

		message += "Terima kasih.<br><br>";
		message += "Yang Ikhlas,<br>";
		message += "Etiqa Insurance Berhad<br><br>";
		String templateBody = message;
		templateBean.setTemplateSubject(subject);
		templateBean.setTemplateBody(templateBody);
		templateBean.setEmailTo(cdvo.getCUSTOMER_EMAIL());
		return templateBean;
	}

	@Override
	public DspEmailTemplateBean loadMailTemplateForMotor(custQuotPmntPolicyVo custVo,
			List<MotorInsuranceCustDetails> listOfAdditionalCoverage) {
		/*
		 * System.out.println(custVo.getCUSTOMER_NAME()+"c name\n"+custVo.
		 * getCUSTOMER_EMAIL()+"email ID"); DspEmailTemplateBean templateBean = new
		 * DspEmailTemplateBean(); String subject = "Etiqa Term Life Inusrance Policy";
		 * String templateBody = "Dear "+custVo.getCUSTOMER_NAME() +
		 * "\n Congratulations on successfully purchasing the insurance Policy no :"
		 * +custVo.getPOLICY_NUMBER()+ "\n\n\nRegards," + "Etiqa Insurance BHD";
		 *
		 * templateBean.setTemplateSubject(subject);
		 * templateBean.setTemplateBody(templateBody);
		 * templateBean.setEmailTo(custVo.getCUSTOMER_EMAIL());
		 *
		 * return templateBean;
		 */
		System.out.println(custVo.getCUSTOMER_NAME() + "c name\n" + custVo.getCUSTOMER_EMAIL() + "email ID");
		DspEmailTemplateBean templateBean = new DspEmailTemplateBean();

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy  h:mm:ss a");
		String formattedDate = sdf.format(date);

		String subject = "Motor Insurance Policy Detail (" + custVo.getPOLICY_NUMBER() + ")";
		String message = "";
		File log = null;
		InputStream stream;
		String filePath = "";
		try (InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/email/documents.properties")) {
			Properties prop = new Properties();
			prop.load(in);
			// String htmlFileTemplate = "com/etiqa/dsp/policy/templates/Test.txt";
			String htmlFileTemplate = "com/etiqa/email/Test.txt";
			InputStream input = this.getClass().getClassLoader().getResourceAsStream(htmlFileTemplate);
			log = StreamUtil.stream2file(input);
			System.out.println(log.getAbsolutePath() + "Absolute Path in Html load");

			String search = "CustomerNameValue";
			String replace = custVo.getCUSTOMER_NAME();
			String search1 = "VehicleNumberValue";
			String replace1 = custVo.getVehRegNo();
			String search2 = "ePolicyNumberValue";
			String replace2 = custVo.getPOLICY_NUMBER();
			String search3 = "ExpiryDateValue";
			String toDate = custVo.getPOLICY_EXPIRY_TIMESTAMP();

			toDate = toDate.substring(0, 2) + "-" + toDate.substring(2, toDate.length());
			toDate = toDate.substring(0, 5) + "-" + toDate.substring(5, toDate.length());

			String replace3 = toDate;

			String addCoverageSearch1 = "TickMarkValue1";
			String source1 = "Passenger Legal Liability Coverage";
			String addCoverageReplace1 = getAdditionalCoverageValue(source1, listOfAdditionalCoverage);

			String addCoverageSearch2 = "TickMarkValue2";
			String source2 = "Legal Liability of Pessenger for Acts of Negligence Coverage";
			String addCoverageReplace2 = getAdditionalCoverageValue(source2, listOfAdditionalCoverage);

			String addCoverageSearch3 = "TickMarkValue3";
			String source3 = "Extended Flood Cover";
			String addCoverageReplace3 = getAdditionalCoverageValue(source3, listOfAdditionalCoverage);

			String addCoverageSearch4 = "TickMarkValue4";
			String source4 = "Basic Flood Cover";
			String addCoverageReplace4 = getAdditionalCoverageValue(source4, listOfAdditionalCoverage);

			String addCoverageSearch5 = "TickMarkValue5";
			String source5 = "NCD Relief";
			String addCoverageReplace5 = getAdditionalCoverageValue(source5, listOfAdditionalCoverage);

			String addCoverageSearch6 = "TickMarkValue6";
			String source6 = "Windscreen Coverage";
			String addCoverageReplace6 = getAdditionalCoverageValue(source6, listOfAdditionalCoverage);

			String addCoverageSearch7 = "TickMarkValue7";
			String source7 = "Vehicle Accessories";
			String addCoverageReplace7 = getAdditionalCoverageValue(source7, listOfAdditionalCoverage);

			String addCoverageSearch8 = "TickMarkValue8";
			String source8 = "NGV Gas";
			String addCoverageReplace8 = getAdditionalCoverageValue(source8, listOfAdditionalCoverage);

			String addCoverageSearch9 = "TickMarkValue9";
			String source9 = "Compensation for Assessed Repair Time";
			String addCoverageReplace9 = getAdditionalCoverageValue(source9, listOfAdditionalCoverage);

			String addCoverageSearch10 = "ExtraTickMark1";
			String source10 = "ADD NAMED DRIVER";
			String addCoverageReplace10 = getAdditionalCoverageValue(source10, listOfAdditionalCoverage);

			String addCoverageSearch11 = "ExtraTickMark2";
			String source11 = "Strike, Riot & Civil Commotion";
			String addCoverageReplace11 = getAdditionalCoverageValue(source11, listOfAdditionalCoverage);

			/*
			 * Passenger Legal Liability Coverage Legal Liability of Pessenger for Acts of
			 * Negligence Coverage Extended Flood Cover Basic Flood Cover NCD Relief
			 * Windscreen Coverage Vehicle Accessories NGV Gas Compensation for Assessed
			 * Repair Time ADD NAMED DRIVER Strike, Riot & Civil Commotion
			 */

			try {
				FileReader fr = new FileReader(log);
				String s;
				String totalStr = "";
				try (BufferedReader br = new BufferedReader(fr)) {

					while ((s = br.readLine()) != null) {
						totalStr += s;
					}
					totalStr = totalStr.replaceAll(search, replace);
					totalStr = totalStr.replaceAll(search1, replace1);
					totalStr = totalStr.replaceAll(search2, replace2);
					totalStr = totalStr.replaceAll(search3, replace3);

					totalStr = totalStr.replaceAll(addCoverageSearch1, addCoverageReplace1);
					totalStr = totalStr.replaceAll(addCoverageSearch2, addCoverageReplace2);
					totalStr = totalStr.replaceAll(addCoverageSearch3, addCoverageReplace3);
					totalStr = totalStr.replaceAll(addCoverageSearch4, addCoverageReplace4);
					totalStr = totalStr.replaceAll(addCoverageSearch5, addCoverageReplace5);
					totalStr = totalStr.replaceAll(addCoverageSearch6, addCoverageReplace6);
					totalStr = totalStr.replaceAll(addCoverageSearch7, addCoverageReplace7);
					totalStr = totalStr.replaceAll(addCoverageSearch8, addCoverageReplace8);
					totalStr = totalStr.replaceAll(addCoverageSearch9, addCoverageReplace9);
					totalStr = totalStr.replaceAll(addCoverageSearch10, addCoverageReplace10);
					totalStr = totalStr.replaceAll(addCoverageSearch11, addCoverageReplace11);
					FileWriter fw = new FileWriter(log);
					fw.write(totalStr);
					fw.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			stream = new FileInputStream(log);
			byte[] b = new byte[stream.available()];
			stream.read(b);
			message = new String(b);
			System.out.println("Successfully Changed Text");
			/*
			 * String Temp=null;
			 *
			 * Temp=search; search=replace; replace=Temp;
			 *
			 * Temp=search1; search1=replace1; replace1=Temp;
			 *
			 * Temp=search2; search2=replace2; replace2=Temp;
			 *
			 * Temp=search3; search3=replace3; replace3=Temp;
			 *
			 * Temp=addCoverageSearch1; addCoverageSearch1=addCoverageReplace1;
			 * addCoverageReplace1=Temp;
			 *
			 * Temp=addCoverageSearch2; addCoverageSearch2=addCoverageReplace2;
			 * addCoverageReplace2=Temp;
			 *
			 * Temp=addCoverageSearch3; addCoverageSearch3=addCoverageReplace3;
			 * addCoverageReplace3=Temp;
			 *
			 * Temp=addCoverageSearch4; addCoverageSearch4=addCoverageReplace4;
			 * addCoverageReplace4=Temp;
			 *
			 * Temp=addCoverageSearch5; addCoverageSearch5=addCoverageReplace5;
			 * addCoverageReplace5=Temp;
			 *
			 * Temp=addCoverageSearch6; addCoverageSearch6=addCoverageReplace6;
			 * addCoverageReplace6=Temp;
			 *
			 * Temp=addCoverageSearch7; addCoverageSearch7=addCoverageReplace7;
			 * addCoverageReplace7=Temp;
			 *
			 * Temp=addCoverageSearch8; addCoverageSearch8=addCoverageReplace8;
			 * addCoverageReplace8=Temp;
			 *
			 * Temp=addCoverageSearch9; addCoverageSearch9=addCoverageReplace9;
			 * addCoverageReplace9=Temp;
			 *
			 * Temp=addCoverageSearch10; addCoverageSearch1=addCoverageReplace10;
			 * addCoverageReplace10=Temp;
			 *
			 * Temp=addCoverageSearch11; addCoverageSearch11=addCoverageReplace11;
			 * addCoverageReplace11=Temp;
			 *
			 *
			 *
			 * try{ FileReader fr = new FileReader(log); String s; String totalStr = ""; try
			 * (BufferedReader br = new BufferedReader(fr)) {
			 *
			 * while ((s = br.readLine()) != null) { totalStr += s; } totalStr =
			 * totalStr.replaceAll(search, replace); totalStr = totalStr.replaceAll(search1,
			 * replace1); totalStr = totalStr.replaceAll(search2, replace2); FileWriter fw =
			 * new FileWriter(log); fw.write(totalStr); fw.close(); } }catch(Exception e){
			 * e.printStackTrace(); }
			 */
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Successfully Undo Text");
		String templateBody = message;
		templateBean.setTemplateSubject(subject);
		templateBean.setTemplateBody(templateBody);
		templateBean.setEmailTo(custVo.getCUSTOMER_EMAIL());
		return templateBean;
	}

	@Override
	public WtcDspEmailTemplateBean loadMailTemplate(WTCcustQuotPmntPolicyVo custVo) {
		Date date = new Date();
		String formattedDate = "";
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy  h:mm:ss a");
		formattedDate = sdf.format(date);
		System.out.println(custVo.getCUSTOMER_NAME() + "c name\n" + custVo.getCUSTOMER_EMAIL() + "      email ID");
		String effectiveDate = custVo.getTRAVEL_END_DATE() == null ? "" : custVo.getTRAVEL_END_DATE();
		// String tripType=custVo.getTRIP_TYPE()==null?"Single":custVo.getTRIP_TYPE();
		String tripType = custVo.getTRIP_TYPE() == null ? "singl" : custVo.getTRIP_TYPE();
		if (custVo.getLangValue().equals("lan_en")) {
			if (tripType.equals("singl")) {
				tripType = "Single Trip";
			} else {
				tripType = "Annual Trip";
			}
		} else {
			if (tripType.equals("singl")) {
				tripType = "Setiap Perjalanan";
			} else {
				tripType = "Tahunan";
			}
		}
		String travelWith = "";
		if (custVo.getLangValue().equals("lan_en")) {
			if (custVo.getTRAVLLING_WITH().equals("W01")) {
				travelWith = "Myself (18 - 70 years old)";
			} else if (custVo.getTRAVLLING_WITH().equals("W03")) {
				travelWith = "Myself and Spouse";
			} else if (custVo.getTRAVLLING_WITH().equals("W05")) {
				travelWith = "My Family";
			} else {
				travelWith = "Myself/Senior Citizen (71 - 80 years old)";
			}
		} else {
			if (custVo.getTRAVLLING_WITH().equals("W01")) {
				travelWith = "Individu (18-70 tahun)";
			} else if (custVo.getTRAVLLING_WITH().equals("W03")) {
				travelWith = "Individu dan Pasangan";
			} else if (custVo.getTRAVLLING_WITH().equals("W05")) {
				travelWith = "Keluarga";
			} else {
				travelWith = "Warga Emas (71-80 tahun)";
			}
		}
		// String
		// planName=custVo.getOFFERED_PLAN_NAME().equals("1")?"Domestic":custVo.getOFFERED_PLAN_NAME();
		String planName = custVo.getOFFERED_PLAN_NAME().equals("1") ? "Domestic" : custVo.getOFFERED_PLAN_NAME();
		if (custVo.getLangValue().equals("lan_en")) {
			if (planName.equals("Domestic")) {
				planName = "Domestic";
			}
		} else {
			if (planName.equals("Domestic")) {
				planName = "Domestik";
			}
		}

		WtcDspEmailTemplateBean templateBean = new WtcDspEmailTemplateBean();
		String subject = "Etiqa World Traveller Care e-Policy " + custVo.getPOLICY_NUMBER();
		String templateBody = "Dear            " + custVo.getCUSTOMER_NAME()
				+ "\n Congratulations on successfully purchasing the insurance Policy no :     "
				+ custVo.getPOLICY_NUMBER() + "\n\n\nRegards," + "Etiqa Insurance BHD";
		String test = "test";
		String color = "background-color:#FFC000";
		String message = "<i> </i><br>";
		message += "<b>Dear &nbsp;&nbsp;" + custVo.getCUSTOMER_NAME() + ",</b><br><br><br>";
		message += "Thank you for subscribing World Traveller Care plan.<br>";
		message += "Here are some information which maybe helpful for you:<br><br>";
		message += "In case of any emergency during your travels, you may call our 24-hour Travel and Medical Assistance Helpline at 03-2161 0270<br><br>";
		message += "Please keep all original receipts and related documents (i.e.: boarding pass, e-ticket/flight ticket, airline letter/report of luggage delayed/damaged, police report) in case you need to make a claim<br>";
		message += "Please find attached a copy of your e-Policy and Tax Invoice./ Receipt for your reference:<br><br>";
		message += "World Traveller Care<br><br>";

		message += "<hr><br><br>";

		message += "Transaction Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;  "
				+ custVo.getTRANSACTION_DATETIME() + "<br>";//
		message += "Transaction No. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;  "
				+ custVo.getTRANSACTION_ID() + "<br>";
		message += "Policy Holder Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ custVo.getCUSTOMER_NAME() + "<br>";
		message += "NRIC No. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>:</b>&nbsp;&nbsp;"
				+ custVo.getCUSTOMER_NRIC_ID() + "<br>";
		message += "Plan Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ planName + "-" + travelWith + "<br>";
		message += "Contribution Frequency &nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;" + tripType + "<br>";
		message += "Effective Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>:</b>&nbsp;&nbsp;"
				+ effectiveDate + "<br>";
		message += "e-Policy No. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ custVo.getPOLICY_NUMBER() + "<br>";
		message += "Tax Invoice No &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ custVo.getINVOICE_NO() + "<br>";
		message += "Payment Mode &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ custVo.getPMNT_GATEWAY_CODE() + "<br>";
		message += "Premium Amount (RM) &nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;" + custVo.getAMOUNT()
				+ "<br><br><br>";

		message += "Please visit www.etiqa.com.my if you would like to make a nomination for this policy.<br><br><br>";

		message += "<div style=" + color
				+ ">Travel with a peace of mind knowing that your house is protected while you're away with our Houseonwer/Householder home protection plan.</br> "
				+ "Enjoy a 15% discount when you sign up online. <a href='https://www.etiqapartner.com.my/dsp/hohh/Customer/HOHH/GetPremiumValue.aspx' target='_blank'>Click here</a> to find out more. </br></div><br>";

		// message += "Travel with a peace of mind knowing that your house is protected
		// while you�re away with our Houseonwer/Householder home protection plan.<br>";
		// message += "Enjoy a 15% discount when you sign up online. Click here to find
		// out more.<br>";

		message += "<b><font>Note:  &nbsp; &nbsp;</font></b>";
		message += "This is an auto email notification. Please do not reply this email. You may contact our Etiqa Oneline at 1300 13 8888 or email us at info@etiqa.com.my.<br><br>";

		message += "Thank you.<br><br>";
		message += "Yours Sincerely,<br><br>";
		message += "Etiqa Insurance Berhad<br><br>";

		templateBean.setTemplateSubject(subject);
		templateBean.setTemplateBody(message);
		templateBean.setEmailTo(custVo.getCUSTOMER_EMAIL());

		return templateBean;

	}

	@Override
	public WtcDspEmailTemplateBean loadMailTemplateBM(WTCcustQuotPmntPolicyVo custVo) {

		Date date = new Date();
		String formattedDate = "";
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy  h:mm:ss a");
		formattedDate = sdf.format(date);
		System.out.println(custVo.getCUSTOMER_NAME() + "c name\n" + custVo.getCUSTOMER_EMAIL() + "      email ID");
		String effectiveDate = custVo.getTRAVEL_END_DATE() == null ? "" : custVo.getTRAVEL_END_DATE();
		String tripType = custVo.getTRIP_TYPE() == null ? "singl" : custVo.getTRIP_TYPE();
		if (custVo.getLangValue().equals("lan_en")) {
			if (tripType.equals("singl")) {
				tripType = "Single Trip";
			} else {
				tripType = "Annual Trip";
			}
		} else {
			if (tripType.equals("singl")) {
				tripType = "Setiap Perjalanan";
			} else {
				tripType = "Tahunan";
			}
		}
		String travelWith = "";
		if (custVo.getLangValue().equals("lan_en")) {
			if (custVo.getTRAVLLING_WITH().equals("W01")) {
				travelWith = "Myself (18 - 70 years old)";
			} else if (custVo.getTRAVLLING_WITH().equals("W03")) {
				travelWith = "Myself and Spouse";
			} else if (custVo.getTRAVLLING_WITH().equals("W05")) {
				travelWith = "My Family";
			} else {
				travelWith = "Myself/Senior Citizen (71 - 80 years old)";
			}
		} else {
			if (custVo.getTRAVLLING_WITH().equals("W01")) {
				travelWith = "Individu (18-70 tahun)";
			} else if (custVo.getTRAVLLING_WITH().equals("W03")) {
				travelWith = "Individu dan Pasangan";
			} else if (custVo.getTRAVLLING_WITH().equals("W05")) {
				travelWith = "Keluarga";
			} else {
				travelWith = "Warga Emas (71-80 tahun)";
			}
		}
		// String
		// planName=custVo.getOFFERED_PLAN_NAME().equals("1")?"Domestic":custVo.getOFFERED_PLAN_NAME();
		String planName = custVo.getOFFERED_PLAN_NAME().equals("1") ? "Domestic" : custVo.getOFFERED_PLAN_NAME();
		if (custVo.getLangValue().equals("lan_en")) {
			if (planName.equals("Domestic")) {
				planName = "Domestic";
			}
		} else {
			if (planName.equals("Domestic")) {
				planName = "Domestik";
			}
		}

		WtcDspEmailTemplateBean templateBean = new WtcDspEmailTemplateBean();
		String subject = "Etiqa World Traveller Care No. e-Polisi " + custVo.getPOLICY_NUMBER();
		// String templateBody = "Kepada "+custVo.getCUSTOMER_NAME()+",";
		// + "\n Congratulations on successfully purchasing the insurance Policy no :
		// "+custVo.getPOLICY_NUMBER()+ "\n\n\nRegards,"
		// + "Etiqa Insurance BHD";
		//
		String color = "background-color:#FFC000";
		String message = "<i> </i><br>";
		message += "<b>Hai &nbsp;&nbsp;" + custVo.getCUSTOMER_NAME() + ",</b><br><br><br>";
		message += "Terima kasih kerana melanggani pelan World Traveller Care.<br>";
		message += "Berikut adalah beberapa maklumat yang berguna untuk anda:<br><br>";
		message += "Sekiranya anda menghadapi sebarang kecemasan semasa perjalanan, sila hubungi Talian Bantuan Perjalanan dan Perubatan (24 Jam) kami di +603-2161 0270.<br><br>";
		message += "Sila simpan semua resit asal dan dokumen yang berkaitan (Contoh: Pas masuk, e-tiket / tiket penerbangan, surat syarikat penerbangan / laporan kelewatan bagasi / rosak, laporan polis) sekiranya anda ingin membuat tuntutan. Dilampirkan di sini salinan e-Resit dan Invois / Resit Cukai sebagai rujukan anda: <br>";
		// message += "Please find attached a copy of your e-Policy and Tax Invoice./
		// Receipt for your reference:<br><br>";
		message += "World Traveller Care<br><br>";

		message += "<hr><br><br>";

		message += "Tarikh Transaksi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp; "
				+ custVo.getTRANSACTION_DATETIME() + "<br>";// custVo.getTRANSACTION_DATETIME()
		message += "No Transaksi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp; "
				+ custVo.getTRANSACTION_ID() + "<br>";
		message += "Nama Pemegang Polisi;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;" + custVo.getCUSTOMER_NAME() + "<br>";
		message += "No KP Baru&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>:</b>&nbsp;&nbsp;"
				+ custVo.getCUSTOMER_NRIC_ID() + "<br>";
		message += "Nama Pelan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ planName + "-" + travelWith + "<br>";
		message += "Kekerapan Sumbangan&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;" + tripType + "<br>";
		message += "Tarikh Kuatkuasa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>:</b>&nbsp;&nbsp;"
				+ effectiveDate + "<br>";
		message += "No e-Polisi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ custVo.getPOLICY_NUMBER() + "<br>";
		message += "No Invois Cukai&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ custVo.getINVOICE_NO() + "<br>";
		message += "Cara Pembayaran&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> :</b>&nbsp;&nbsp;"
				+ custVo.getPMNT_GATEWAY_CODE() + "<br>";
		message += "Jumlah Premium (RM)&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;" + custVo.getAMOUNT()
				+ "<br><br><br>";

		message += "Sila layari www.etiqa.com.my jika anda ingin membuat penamaan untuk dasar ini.<br><br><br>";

		message += "<div style=" + color
				+ ">Melancong dengan tenang mengetahui bahawa rumah anda dilindungi semasa anda tiada di rumah dengan pelan perlindungan Empunya Rumah / Isi Rumah kami. Nikmati diskaun 15% apabila anda mendaftar dalam talian. Klik <a href='https://www.etiqapartner.com.my/dsp/hohh/Customer/HOHH/GetPremiumValue.aspx' target='_blank'>di sini</a> untuk mengetahui lebih lanjut.</br></div><br>";

		// message += "Travel with a peace of mind knowing that your house is protected
		// while you�re away with our Houseonwer/Householder home protection plan.<br>";
		// message += "Enjoy a 15% discount when you sign up online. Click here to find
		// out more.<br>";

		message += "<b>Nota:  &nbsp; &nbsp;</b>";
		message += "Ini adalah pemberitahuan e-mel secara automatik. Sila jangan membalas emel ini. Anda boleh menghubungi Etiqa Oneline ditalian 1300 13 8888 atau email kepada kami di info@etiqa.com.my.<br><br>";

		message += "Terima kasih.<br><br>";
		message += "Yang ikhlas,<br><br>";
		message += "Etiqa Insurance Berhad<br><br>";

		templateBean.setTemplateSubject(subject);
		templateBean.setTemplateBody(message);
		templateBean.setEmailTo(custVo.getCUSTOMER_EMAIL());

		return templateBean;

	}

	@Override
	public WtcDspEmailTemplateBean loadMailTemplateTakaful(WTCcustQuotPmntPolicyVo custVo) {
		Date date = new Date();
		String formattedDate = "";
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy  h:mm:ss a");
		formattedDate = sdf.format(date);
		System.out.println(custVo.getCUSTOMER_NAME() + "c name\n" + custVo.getCUSTOMER_EMAIL() + "      email ID");
		String effectiveDate = custVo.getTRAVEL_END_DATE() == null ? "" : custVo.getTRAVEL_END_DATE();
		String tripType = custVo.getTRIP_TYPE() == null ? "singl" : custVo.getTRIP_TYPE();
		if (custVo.getLangValue().equals("lan_en")) {
			if (tripType.equals("singl")) {
				tripType = "Single Trip";
			} else {
				tripType = "Annual Trip";
			}
		} else {
			if (tripType.equals("singl")) {
				tripType = "Setiap Perjalanan";
			} else {
				tripType = "Tahunan";
			}
		}
		String travelWith = "";
		if (custVo.getLangValue().equals("lan_en")) {
			if (custVo.getTRAVLLING_WITH().equals("W01")) {
				travelWith = "Myself (18 - 70 years old)";
			} else if (custVo.getTRAVLLING_WITH().equals("W03")) {
				travelWith = "Myself and Spouse";
			} else if (custVo.getTRAVLLING_WITH().equals("W05")) {
				travelWith = "My Family";
			} else {
				travelWith = "Myself/Senior Citizen (71 - 80 years old)";
			}
		} else {
			if (custVo.getTRAVLLING_WITH().equals("W01")) {
				travelWith = "Individu (18-70 tahun)";
			} else if (custVo.getTRAVLLING_WITH().equals("W03")) {
				travelWith = "Individu dan Pasangan";
			} else if (custVo.getTRAVLLING_WITH().equals("W05")) {
				travelWith = "Keluarga";
			} else {
				travelWith = "Warga Emas (71-80 tahun)";
			}
		}
		// String
		// planName=custVo.getOFFERED_PLAN_NAME().equals("1")?"Domestic":custVo.getOFFERED_PLAN_NAME();
		String planName = custVo.getOFFERED_PLAN_NAME().equals("1") ? "Domestic" : custVo.getOFFERED_PLAN_NAME();
		if (custVo.getLangValue().equals("lan_en")) {
			if (planName.equals("Domestic")) {
				planName = "Domestic";
			}
		} else {
			if (planName.equals("Domestic")) {
				planName = "Domestik";
			}
		}

		WtcDspEmailTemplateBean templateBean = new WtcDspEmailTemplateBean();
		String subject = "Etiqa World Traveller Care Takaful e-Certificate " + custVo.getPOLICY_NUMBER();
		String templateBody = "Dear            " + custVo.getCUSTOMER_NAME()
				+ "\n Congratulations on successfully purchasing the takaful e-Certificate No :     "
				+ custVo.getPOLICY_NUMBER() + "\n\n\nRegards," + "Etiqa Takaful BHD";
		String test = "test";
		String color = "background-color:#FFC000";

		// message += "<b><font color=red>Duke</font></b><br>";

		String message = "<i> </i><br>";
		message += "<b>Dear &nbsp;&nbsp;" + custVo.getCUSTOMER_NAME() + ",</b><br><br><br>";
		message += "Thank you for subscribing World Traveller Care Takaful plan.<br>";
		message += "Here are some information which maybe helpful for you:<br><br>";
		message += "In case of any emergency during your travels, you may call our 24-hour Travel and Medical Assistance Helpline at 03-2161 0270<br><br>";
		message += "Please keep all original receipts and related documents (i.e.: boarding pass, e-ticket/flight ticket, airline letter/report of luggage delayed/damaged, police report) in case you need to make a claim<br>";
		message += "Please find attached a copy of your e-Policy and Tax Invoice./ Receipt for your reference:<br><br>";
		message += "World Traveller Care Takaful<br><br>";

		message += "<hr><br><br>";

		message += "Transaction Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ custVo.getTRANSACTION_DATETIME() + "<br>";// custVo.getTRANSACTION_DATETIME()
		message += "Transaction No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ custVo.getTRANSACTION_ID() + "<br>";
		message += "Participant Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ custVo.getCUSTOMER_NAME() + "<br>";
		message += "NRIC No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>:</b>&nbsp;&nbsp;"
				+ custVo.getCUSTOMER_NRIC_ID() + "<br>";
		message += "Plan Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ planName + "-" + travelWith + "<br>";
		message += "Contribution Frequency&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;" + tripType + "<br>";
		message += "Effective Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>:</b>&nbsp;&nbsp;"
				+ effectiveDate + "<br>";
		message += "e-Certificate No.&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ custVo.getPOLICY_NUMBER() + "<br>";
		message += "Tax Invoice No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ custVo.getINVOICE_NO() + "<br>";
		message += "Payment Mode &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ custVo.getPMNT_GATEWAY_CODE() + "<br>";
		message += "Contribution Amount (RM)&nbsp;&nbsp;<b>:</b>&nbsp;&nbsp;" + custVo.getAMOUNT() + "<br><br><br>";

		message += "Please visit www.etiqa.com.my if you would like to make a nomination for this policy.<br><br><br>";

		message += "<div style=" + color
				+ ">Travel with a peace of mind knowing that your house is protected while you're away with our Houseonwer/Householder home protection plan.</br> "
				+ "Enjoy a 15% discount when you sign up online. <a href='https://www.etiqapartner.com.my/dsp/hohh/Customer/TakafulHOHH/GetPremiumValue.aspx' target='_blank'>Click here</a> to find out more. </br></div><br>";

		// message += "Travel with a peace of mind knowing that your house is protected
		// while you�re away with our Houseonwer/Householder home protection plan.<br>";
		// message += "Enjoy a 15% discount when you sign up online. Click here to find
		// out more.<br>";

		message += "<b><font>Note:  &nbsp; &nbsp;</font></b>";
		message += "This is an auto email notification. Please do not reply this email. You may contact our Etiqa Oneline at 1300 13 8888 or email us at info@etiqa.com.my.<br><br>";

		message += "Thank you.<br><br>";
		message += "Yours Sincerely,<br><br>";
		message += "Etiqa Takaful Berhad<br><br>";

		templateBean.setTemplateSubject(subject);
		templateBean.setTemplateBody(message);
		templateBean.setEmailTo(custVo.getCUSTOMER_EMAIL());

		return templateBean;
	}

	@Override
	public WtcDspEmailTemplateBean loadMailTemplateTakafulBM(WTCcustQuotPmntPolicyVo custVo) {
		Date date = new Date();
		String formattedDate = "";
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy  h:mm:ss a");
		formattedDate = sdf.format(date);
		System.out.println(custVo.getCUSTOMER_NAME() + "c name\n" + custVo.getCUSTOMER_EMAIL() + "      email ID");
		String effectiveDate = custVo.getTRAVEL_END_DATE() == null ? "" : custVo.getTRAVEL_END_DATE();
		String tripType = custVo.getTRIP_TYPE() == null ? "singl" : custVo.getTRIP_TYPE();
		if (custVo.getLangValue().equals("lan_en")) {
			if (tripType.equals("singl")) {
				tripType = "Single Trip";
			} else {
				tripType = "Annual Trip";
			}
		} else {
			if (tripType.equals("singl")) {
				tripType = "Setiap Perjalanan";
			} else {
				tripType = "Tahunan";
			}
		}
		String travelWith = "";
		if (custVo.getLangValue().equals("lan_en")) {
			if (custVo.getTRAVLLING_WITH().equals("W01")) {
				travelWith = "Myself (18 - 70 years old)";
			} else if (custVo.getTRAVLLING_WITH().equals("W03")) {
				travelWith = "Myself and Spouse";
			} else if (custVo.getTRAVLLING_WITH().equals("W05")) {
				travelWith = "My Family";
			} else {
				travelWith = "Myself/Senior Citizen (71 - 80 years old)";
			}
		} else {
			if (custVo.getTRAVLLING_WITH().equals("W01")) {
				travelWith = "Individu (18-70 tahun)";
			} else if (custVo.getTRAVLLING_WITH().equals("W03")) {
				travelWith = "Individu dan Pasangan";
			} else if (custVo.getTRAVLLING_WITH().equals("W05")) {
				travelWith = "Keluarga";
			} else {
				travelWith = "Warga Emas (71-80 tahun)";
			}
		}
		// String
		// planName=custVo.getOFFERED_PLAN_NAME().equals("1")?"Domestic":custVo.getOFFERED_PLAN_NAME();
		String planName = custVo.getOFFERED_PLAN_NAME().equals("1") ? "Domestic" : custVo.getOFFERED_PLAN_NAME();
		if (custVo.getLangValue().equals("lan_en")) {
			if (planName.equals("Domestic")) {
				planName = "Domestic";
			}
		} else {
			if (planName.equals("Domestic")) {
				planName = "Domestik";
			}
		}

		WtcDspEmailTemplateBean templateBean = new WtcDspEmailTemplateBean();
		String subject = "Etiqa World Traveller Care Takaful No. e-Sijil " + custVo.getPOLICY_NUMBER();
		String color = "background-color:#FFC000";
		String message = "<i> </i><br>";
		message += "<b>Hai  &nbsp;&nbsp;" + custVo.getCUSTOMER_NAME() + ",</b><br><br><br>";
		message += "Terima kasih kerana melanggani pelan World Traveller Care Takaful.<br>";
		message += "Berikut adalah beberapa maklumat yang berguna untuk anda:<br><br>";
		message += "Sekiranya anda menghadapi sebarang kecemasan semasa perjalanan, sila hubungi Talian Bantuan Perjalanan dan Perubatan (24 Jam) kami di +603-2161 0270.<br><br>";
		message += "Sila simpan semua resit asal dan dokumen yang berkaitan (Contoh: Pas masuk, e-tiket / tiket penerbangan, surat syarikat penerbangan / laporan kelewatan bagasi / rosak, laporan polis) sekiranya anda ingin membuat tuntutan. Dilampirkan di sini salinan e-Resit dan Invois / Resit Cukai sebagai rujukan anda:<br>";
		// message += "Please find attached a copy of your e-Policy and Tax Invoice./
		// Receipt for your reference:<br><br>";
		message += "World Traveller Care Takaful<br><br>";

		message += "<hr><br><br>";

		message += "Tarikh Transaksi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp; "
				+ custVo.getTRANSACTION_DATETIME() + "<br>";// custVo.getTRANSACTION_DATETIME()
		message += "No Transaksi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp; "
				+ custVo.getTRANSACTION_ID() + "<br>";
		message += "Nama Peserta&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ custVo.getCUSTOMER_NAME() + "<br>";
		message += "No KP Baru&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>:</b>&nbsp;&nbsp;"
				+ custVo.getCUSTOMER_NRIC_ID() + "<br>";
		message += "Nama Pelan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ planName + "-" + travelWith + "<br>";
		message += "Kekerapan Sumbangan&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;" + tripType + "<br>";
		message += "Tarikh Kuatkuasa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>:</b>&nbsp;&nbsp;"
				+ effectiveDate + "<br>";
		message += "No e-Sijil&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ custVo.getPOLICY_NUMBER() + "<br>";
		message += "No Invois Cukai&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>:</b>&nbsp;&nbsp;"
				+ custVo.getINVOICE_NO() + "<br>";
		message += "Cara Bayaran&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> :</b>&nbsp;&nbsp;"
				+ custVo.getPMNT_GATEWAY_CODE() + "<br>";
		message += "Jumlah Sumbangan (RM)&nbsp;<b>:</b>&nbsp;&nbsp;" + custVo.getAMOUNT() + "<br><br><br>";
		// message += "Sila layari www.etiqa.com.my jika anda ingin membuat penamaan
		// untuk dasar ini.<br><br><br>";

		message += "<div style=" + color
				+ ">Melancong dengan tenang mengetahui bahawa rumah anda dilindungi semasa anda tiada di rumah dengan pelan perlindungan Empunya Rumah / Isi Rumah Takaful kami. Nikmati diskaun 15% apabila anda mendaftar dalam talian. Klik <a href='https://www.etiqapartner.com.my/dsp/hohh/Customer/TakafulHOHH/GetPremiumValue.aspx' target='_blank'>di sini</a> untuk mengetahui lebih lanjut.</br></div><br>";

		// message += "Travel with a peace of mind knowing that your house is protected
		// while you�re away with our Houseonwer/Householder home protection plan.<br>";
		// message += "Enjoy a 15% discount when you sign up online. Click here to find
		// out more.<br>";

		message += "<b>Nota:  &nbsp; &nbsp;</b>";
		message += "Ini adalah pemberitahuan e-mel secara automatik. Sila jangan membalas emel ini. Anda boleh menghubungi Etiqa Oneline ditalian 1300 13 8888 atau email kepada kami di info@etiqa.com.my.<br><br>";

		message += "Terima kasih.<br><br>";
		message += "Yang ikhlas,<br><br>";
		message += "Etiqa Takaful Berhad<br><br>";

		templateBean.setTemplateSubject(subject);
		templateBean.setTemplateBody(message);
		templateBean.setEmailTo(custVo.getCUSTOMER_EMAIL());

		return templateBean;
	}

	public String getAdditionalCoverageValue(String source, List<MotorInsuranceCustDetails> listOfAdditionalCoverage) {
		String resultOfStatus = null;
		int count = 0;
		if (listOfAdditionalCoverage.size() != 0) {
			for (MotorInsuranceCustDetails AdditionalInfo : listOfAdditionalCoverage) {
				if (AdditionalInfo.getAdditionalCoverageText().trim().equals(source)) {
					count = 1;
				}

				if (count == 1) {
					resultOfStatus = "<img class=\"center\" style=\"outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block;border: 0;height: auto;line-height: 100%;margin: 0 auto;float: none;width: 24px;max-width: 24px\" align=\"center\" border=\"0\" src=\"https://www.etiqa.com.my/dspadmin/assets/images/email/green%20tick.png\" alt=\"Image\" title=\"Image\" width=\"24\">";
				} else {
					resultOfStatus = "N/A";
				}
			}
		} else {
			resultOfStatus = "N/A";
		}
		return resultOfStatus;
	}

	@Override
	public DspEmailTemplateBean loadMailTemplateForMotorTakaful(custQuotPmntPolicyVo custVo,
			List<MotorInsuranceCustDetails> listOfAdditionalCoverage) {
		System.out.println(custVo.getCUSTOMER_NAME() + "c name\n" + custVo.getCUSTOMER_EMAIL() + "email ID");
		DspEmailTemplateBean templateBean = new DspEmailTemplateBean();

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy  h:mm:ss a");
		String formattedDate = sdf.format(date);

		String subject = "Motor Takaful Policy Detail (" + custVo.getPOLICY_NUMBER() + ")";
		String message = "";
		File log = null;
		InputStream stream;
		String filePath = "";
		try (InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/dsp/sales/process/email/documents.properties")) {
			Properties prop = new Properties();
			prop.load(in);
			String htmlFileTemplate = "com/etiqa/dsp/policy/images/Test.txt";
			InputStream input = this.getClass().getClassLoader().getResourceAsStream(htmlFileTemplate);
			log = StreamUtil.stream2file(input);
			System.out.println(log.getAbsolutePath() + "Absolute Path in Html load");

			String search = "CustomerNameValue";
			String replace = custVo.getCUSTOMER_NAME();
			String search1 = "VehicleNumberValue";
			String replace1 = custVo.getVehRegNo();
			String search2 = "ePolicyNumberValue";
			String replace2 = custVo.getPOLICY_NUMBER();
			String search3 = "ExpiryDateValue";
			String toDate = custVo.getPOLICY_EXPIRY_TIMESTAMP();

			toDate = toDate.substring(0, 2) + "-" + toDate.substring(2, toDate.length());
			toDate = toDate.substring(0, 5) + "-" + toDate.substring(5, toDate.length());

			String replace3 = toDate;

			String addCoverageSearch1 = "TickMarkValue1";
			String source1 = "Passenger Legal Liability Coverage";
			String addCoverageReplace1 = getAdditionalCoverageValue(source1, listOfAdditionalCoverage);

			String addCoverageSearch2 = "TickMarkValue2";

			String source2 = "Legal Liability of Passenger for Acts of Negligence Coverage";
			String addCoverageReplace2 = getAdditionalCoverageValue(source2, listOfAdditionalCoverage);

			String addCoverageSearch3 = "TickMarkValue3";
			String source3 = "Extended Flood Cover";
			String addCoverageReplace3 = getAdditionalCoverageValue(source3, listOfAdditionalCoverage);

			String addCoverageSearch4 = "TickMarkValue4";
			String source4 = "Basic Flood Cover";
			String addCoverageReplace4 = getAdditionalCoverageValue(source4, listOfAdditionalCoverage);

			String addCoverageSearch5 = "TickMarkValue5";
			String source5 = "NCD Relief";
			String addCoverageReplace5 = getAdditionalCoverageValue(source5, listOfAdditionalCoverage);

			String addCoverageSearch6 = "TickMarkValue6";
			String source6 = "Windscreen Coverage";
			String addCoverageReplace6 = getAdditionalCoverageValue(source6, listOfAdditionalCoverage);

			String addCoverageSearch7 = "TickMarkValue7";
			String source7 = "Vehicle Accessories";
			String addCoverageReplace7 = getAdditionalCoverageValue(source7, listOfAdditionalCoverage);

			String addCoverageSearch8 = "TickMarkValue8";
			String source8 = "NGV Gas";
			String addCoverageReplace8 = getAdditionalCoverageValue(source8, listOfAdditionalCoverage);

			String addCoverageSearch9 = "TickMarkValue9";
			String source9 = "Compensation for Assessed Repair Time";
			String addCoverageReplace9 = getAdditionalCoverageValue(source9, listOfAdditionalCoverage);

			String addCoverageSearch10 = "ExtraTickMark1";
			String source10 = "ADD NAMED DRIVER";
			String addCoverageReplace10 = getAdditionalCoverageValue(source10, listOfAdditionalCoverage);

			String addCoverageSearch11 = "ExtraTickMark2";
			String source11 = "Strike, Riot & Civil Commotion";
			String addCoverageReplace11 = getAdditionalCoverageValue(source11, listOfAdditionalCoverage);

			/*
			 * Passenger Legal Liability Coverage Legal Liability of Pessenger for Acts of
			 * Negligence Coverage Extended Flood Cover Basic Flood Cover NCD Relief
			 * Windscreen Coverage Vehicle Accessories NGV Gas Compensation for Assessed
			 * Repair Time ADD NAMED DRIVER Strike, Riot & Civil Commotion
			 */

			try {
				FileReader fr = new FileReader(log);
				String s;
				String totalStr = "";
				try (BufferedReader br = new BufferedReader(fr)) {

					while ((s = br.readLine()) != null) {
						totalStr += s;
					}
					totalStr = totalStr.replaceAll(search, replace);
					totalStr = totalStr.replaceAll(search1, replace1);
					totalStr = totalStr.replaceAll(search2, replace2);
					totalStr = totalStr.replaceAll(search3, replace3);

					totalStr = totalStr.replaceAll(addCoverageSearch1, addCoverageReplace1);
					totalStr = totalStr.replaceAll(addCoverageSearch2, addCoverageReplace2);
					totalStr = totalStr.replaceAll(addCoverageSearch3, addCoverageReplace3);
					totalStr = totalStr.replaceAll(addCoverageSearch4, addCoverageReplace4);
					totalStr = totalStr.replaceAll(addCoverageSearch5, addCoverageReplace5);
					totalStr = totalStr.replaceAll(addCoverageSearch6, addCoverageReplace6);
					totalStr = totalStr.replaceAll(addCoverageSearch7, addCoverageReplace7);
					totalStr = totalStr.replaceAll(addCoverageSearch8, addCoverageReplace8);
					totalStr = totalStr.replaceAll(addCoverageSearch9, addCoverageReplace9);
					totalStr = totalStr.replaceAll(addCoverageSearch10, addCoverageReplace10);
					totalStr = totalStr.replaceAll(addCoverageSearch11, addCoverageReplace11);
					FileWriter fw = new FileWriter(log);
					fw.write(totalStr);
					fw.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			stream = new FileInputStream(log);
			byte[] b = new byte[stream.available()];
			stream.read(b);
			message = new String(b);
			System.out.println("Successfully Changed Text");
			/*
			 * String Temp=null;
			 *
			 * Temp=search; search=replace; replace=Temp;
			 *
			 * Temp=search1; search1=replace1; replace1=Temp;
			 *
			 * Temp=search2; search2=replace2; replace2=Temp;
			 *
			 * Temp=search3; search3=replace3; replace3=Temp;
			 *
			 * Temp=addCoverageSearch1; addCoverageSearch1=addCoverageReplace1;
			 * addCoverageReplace1=Temp;
			 *
			 * Temp=addCoverageSearch2; addCoverageSearch2=addCoverageReplace2;
			 * addCoverageReplace2=Temp;
			 *
			 * Temp=addCoverageSearch3; addCoverageSearch3=addCoverageReplace3;
			 * addCoverageReplace3=Temp;
			 *
			 * Temp=addCoverageSearch4; addCoverageSearch4=addCoverageReplace4;
			 * addCoverageReplace4=Temp;
			 *
			 * Temp=addCoverageSearch5; addCoverageSearch5=addCoverageReplace5;
			 * addCoverageReplace5=Temp;
			 *
			 * Temp=addCoverageSearch6; addCoverageSearch6=addCoverageReplace6;
			 * addCoverageReplace6=Temp;
			 *
			 * Temp=addCoverageSearch7; addCoverageSearch7=addCoverageReplace7;
			 * addCoverageReplace7=Temp;
			 *
			 * Temp=addCoverageSearch8; addCoverageSearch8=addCoverageReplace8;
			 * addCoverageReplace8=Temp;
			 *
			 * Temp=addCoverageSearch9; addCoverageSearch9=addCoverageReplace9;
			 * addCoverageReplace9=Temp;
			 *
			 * Temp=addCoverageSearch10; addCoverageSearch1=addCoverageReplace10;
			 * addCoverageReplace10=Temp;
			 *
			 * Temp=addCoverageSearch11; addCoverageSearch11=addCoverageReplace11;
			 * addCoverageReplace11=Temp;
			 *
			 *
			 *
			 * try{ FileReader fr = new FileReader(log); String s; String totalStr = ""; try
			 * (BufferedReader br = new BufferedReader(fr)) {
			 *
			 * while ((s = br.readLine()) != null) { totalStr += s; } totalStr =
			 * totalStr.replaceAll(search, replace); totalStr = totalStr.replaceAll(search1,
			 * replace1); totalStr = totalStr.replaceAll(search2, replace2); FileWriter fw =
			 * new FileWriter(log); fw.write(totalStr); fw.close(); } }catch(Exception e){
			 * e.printStackTrace(); }
			 */
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Successfully Undo Text");
		String templateBody = message;
		templateBean.setTemplateSubject(subject);
		templateBean.setTemplateBody(templateBody);
		templateBean.setEmailTo(custVo.getCUSTOMER_EMAIL());
		return templateBean;
	}

	/**
	 *
	 */
	@Override
	public DspEmailTemplateBean loadMailTemplateForRoadTax(custQuotPmntPolicyVo custVo) {

		String roadTaxMailStatus = custVo.getRoadTasMailStatus();
		System.out.println("loadMailTemplateForRoadTax<roadTaxMailStatus> => " + roadTaxMailStatus);

		if (roadTaxMailStatus.equals("uPrint")) {

			System.out.println(custVo.getCUSTOMER_NAME() + "c name\n" + custVo.getCUSTOMER_EMAIL() + "email ID");
			System.out.println(custVo.getPOLICY_NUMBER() + "policy Number is:");
			System.out.println(custVo.getPRODUCT_CODE() + "product code is:");
			DspEmailTemplateBean templateBean = new DspEmailTemplateBean();

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy  h:mm:ss a");
			String formattedDate = sdf.format(date);
			String policyNo = custVo.getPOLICY_NUMBER();
			System.out.println("loadMailTemplateForRoadTax<policyNo> => " + policyNo);
			String code = custVo.getPRODUCT_CODE();
			// Todo
			// String roadTaxMailStatus = custVo.getRoadTasMailStatus();
			// System.out.println("loadMailTemplateForRoadTax<roadTaxMailStatus> =>
			// "+roadTaxMailStatus);
			System.out.println("ConsignmentNo => " + custVo.getConsignmentNo());
			System.out.println("Expected Delivery Date => " + custVo.getExpectedDeliveryDate());
			System.out.println("Delivery Address => " + custVo.getDeliveryAddress());
			System.out.println("Expected Delivery Dates => " + custVo.getExpDeliveryDt());

			String policy = "";

			try {
				policy = advancedEncryptionStandard.encrypt(policyNo);
				System.out.println("Encrypt policy no is:" + policy);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String subject = "Looks like there was a problem with your Road Tax issuance, " + custVo.getCUSTOMER_NAME()
					+ "";
			String message = "";
			message += "<b>Dear   " + custVo.getCUSTOMER_NAME() + ",</b>";
			message += "<br><br>";
			message += "We are sorry to inform you that we are unable to process your Road Tax.<br>";
			message += "If you wish to clarify further, you may call Etiqa Oneline at 1 300 13 8888 or get in touch with our Live Chat team.";
			message += "<br><br>";
			message += "OR";
			message += "<br>";
			// message += "Click here for your refund:";
			if (code.equals("MT")) {
				// message += "<a
				// href='https://192.168.14.243:10441/getonline/mtRoadTaxRefund?policy="+policy+"'>Click
				// here</a> for your refund. </br></div><br>";
				message += "<a href='https://uat.etiqa.com.my:4441/getonline/mtRoadTaxRefund?policy=" + policy
						+ "'>Click here</a> for your refund. </br></div><br>";
			} else {
				// message += "<a
				// href='https://192.168.14.243:10442/getonline/miRoadTaxRefund?policy="+policy+"'>Click
				// here</a> for your refund. </br></div><br>";
				message += "<a href='https://uat.etiqa.com.my:4442/getonline/miRoadTaxRefund?policy=" + policy
						+ "'>Click here</a> for your refund. </br></div><br>";
			}
			String templateBody = message;
			templateBean.setTemplateSubject(subject);
			templateBean.setTemplateBody(templateBody);
			templateBean.setEmailTo(custVo.getCUSTOMER_EMAIL());
			return templateBean;
		} else if (roadTaxMailStatus.equals("print")) {

			System.out.println(custVo.getCUSTOMER_NAME() + "c name\n" + custVo.getCUSTOMER_EMAIL() + "email ID");
			System.out.println(custVo.getPOLICY_NUMBER() + "policy Number is:");
			System.out.println(custVo.getPRODUCT_CODE() + "product code is:");
			DspEmailTemplateBean templateBean = new DspEmailTemplateBean();

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy  h:mm:ss a");
			String formattedDate = sdf.format(date);
			String policyNo = custVo.getPOLICY_NUMBER();
			System.out.println("loadMailTemplateForRoadTax<policyNo> => " + policyNo);
			String code = custVo.getPRODUCT_CODE();
			// Todo
			// String roadTaxMailStatus = custVo.getRoadTasMailStatus();
			// System.out.println("loadMailTemplateForRoadTax<roadTaxMailStatus> =>
			// "+roadTaxMailStatus);
			System.out.println("ConsignmentNo => " + custVo.getConsignmentNo());
			System.out.println("Expected Delivery Date => " + custVo.getExpectedDeliveryDate());
			System.out.println("Delivery Address => " + custVo.getDeliveryAddress());
			System.out.println("Expected Delivery Dates => " + custVo.getExpDeliveryDt());

			String policy = "";

			try {
				policy = advancedEncryptionStandard.encrypt(policyNo);
				System.out.println("Encrypt policy no is:" + policy);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// String color="background-color:#FFC000";

			String subject = "Your Road Tax is on its way";
			// String message = "";
			StringBuffer sb = new StringBuffer();

			sb.append("Dear  " + custVo.getCUSTOMER_NAME() + "," + "<br>");
			sb.append("<p><font size=4>" + "Your road tax renewal is successful and is on its way!</font></p>");
			// sb.append("<p align=center><font size=4>"+"Your road tax renewal is
			// successful and is on its way!</font></p>");
			// sb.append("<h1>"+"Your road tax renewal is successful and is on its
			// way!</font></h1>"+"<br><br>");
			sb.append("Thank you for protecting your car with Etiqa. ");
			sb.append("We are pleased to inform you that your Road Tax has been processed and is on its way to you."
					+ "<br><br>");
			sb.append("You can expect to receive it by " + custVo.getExpDeliveryDt() + " and delivered to "
					+ custVo.getDeliveryAddress() + "<br>");
			sb.append("Policy Number:" + policyNo + "<br>");
			sb.append("Tracking Number:" + custVo.getConsignmentNo() + "<br>");
			sb.append("Track your Road Tax here: http://www.poslaju.com.my/track-trace-v2/ " + "<br>");
			sb.append("(Pos Laju requires 24 hours for the tracking number to be updated)" + "<br><br>");

			String templateBody = sb.toString();
			templateBean.setTemplateSubject(subject);
			templateBean.setTemplateBody(templateBody);
			templateBean.setEmailTo(custVo.getCUSTOMER_EMAIL());
			return templateBean;
		} else {

			DspEmailTemplateBean templateBean = new DspEmailTemplateBean();

			String subject = "";

			if (custVo.getEntity().equalsIgnoreCase("EIB")) {
				subject = "Float threshold � Motor Insurance (EIB)";
			} else {
				subject = "Float threshold � Motor Insurance (ETB)";
			}

			StringBuffer sb = new StringBuffer();

			NumberFormat formatter = new DecimalFormat("#0,000.00");

			String thresholdAmout = custVo.getThresholdLimit();
			String currentBalance = custVo.getCurrentBalance();

			sb.append("Road tax float has reach lower than threshold. Please reload!" + "<br><br>");
			sb.append("Threshold limit: RM " + formatter.format(Double.parseDouble(thresholdAmout)) + "<br>");
			sb.append("Current Balance: RM " + formatter.format(Double.parseDouble(currentBalance)) + "<br>");

			String mailTo = custVo.geteMailIds();

			String templateBody = sb.toString();
			templateBean.setTemplateSubject(subject);
			templateBean.setTemplateBody(templateBody);
			// templateBean.setEmailTo(custVo.getCUSTOMER_EMAIL());
			// templateBean.setEmailTo(custVo.geteMailIds());
			templateBean.setEmailTo(mailTo.replaceAll(";", ","));

			return templateBean;

		}
	}

}
