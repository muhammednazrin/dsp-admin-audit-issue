package com.etiqa.email;

import java.io.FileNotFoundException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.dsp.dao.common.pojo.MotorInsuranceCustDetails;
import com.etiqa.dsp.dao.common.pojo.custQuotPmntPolicyVo;

import oracle.jdbc.OracleTypes;

public class MotorInsuranceEmailService {
	public String callingMotorInsuranceEmailService(String policyNo) {

		Connection connection = null;
		Statement statement = null;
		ResultSet rs = null, rsOfAdditionalValue = null;

		String langVal = "";
		connection = ConnectionFactory.getConnection();
		MotorInsuranceCustDetails micdvo = new MotorInsuranceCustDetails();
		List<MotorInsuranceCustDetails> listOfAdditionalCoverage = new ArrayList<MotorInsuranceCustDetails>();
		CallableStatement cstmt;
		try {
			cstmt = connection.prepareCall("{CALL DSP_ADM_SP_MIINFO_SELECT(?,?,?)}");

			cstmt.setString(1, policyNo);
			cstmt.registerOutParameter(2, OracleTypes.CURSOR);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			/*
			 * C.POLICY_NUMBER, A.CUSTOMER_NAME, A.CUSTOMER_NRIC_ID, A.CUSTOMER_EMAIL,
			 * A.CUSTOMER_DOB, D.REGISTRATION_NUMBER, C.POLICY_EXPIRY_TIMESTAMP
			 */
			rs = (ResultSet) cstmt.getObject(2);
			rsOfAdditionalValue = (ResultSet) cstmt.getObject(3);
			while (rs.next()) {
				micdvo.setPOLICY_NUMBER(rs.getString(1));
				micdvo.setCUSTOMER_NAME(rs.getString(2));
				micdvo.setCUSTOMER_NRIC_ID(rs.getString(3));
				micdvo.setCUSTOMER_EMAIL(rs.getString(4));
				micdvo.setCUSTOMER_DOB(rs.getString(5));
				micdvo.setREGISTRATION_NUMBER(rs.getString(6));
				micdvo.setPOLICY_EXPIRY_TIMESTAMP(rs.getString(7));

			}

			while (rsOfAdditionalValue.next()) {
				MotorInsuranceCustDetails miAdditionalCoverage = new MotorInsuranceCustDetails();

				miAdditionalCoverage.setAdditionalCoverageCode(rsOfAdditionalValue.getString(1));
				if (rsOfAdditionalValue.getString(1).charAt(0) == 'P') {
					miAdditionalCoverage.setAdditionalCoverageText("Compensation for Assessed Repair Time");
				} else {
					miAdditionalCoverage.setAdditionalCoverageText(rsOfAdditionalValue.getString(2));
				}
				miAdditionalCoverage
						.setAdditionalCoverageValue(gettingValueisNullOrNotInString(3, rsOfAdditionalValue));
				miAdditionalCoverage
						.setAdditionalCoverageSumInsuredValue(gettingValueisNullOrNotInString(4, rsOfAdditionalValue));
				listOfAdditionalCoverage.add(miAdditionalCoverage);
			}

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*
		 * C.POLICY_NUMBER||C.TL_CHECK_DIGIT, B.DSP_QQ_ID, A.CUSTOMER_NAME,
		 * A.CUSTOMER_EMAIL, A.CUSTOMER_EMAIL, D.DOB, B.CREATED_DATE, C.TRANSACTION_ID,
		 * D.FULL_NAME, D.PREMIUM_PAYMENT_FREQUENCY_TYPE, D.PREMIUM_PAYMENT_FREQUENCY ,
		 * D.PREMIUM_PAYMENT_FREQUENCY
		 */

		DspEmailDispatchProcessor emailProcessor = new DspEmailDispatchProcessor(new TermLifeProductMailTemplateLoad());
		String status = null;
		custQuotPmntPolicyVo cdvo = new custQuotPmntPolicyVo();
		cdvo.setCUSTOMER_NAME(micdvo.getCUSTOMER_NAME());
		cdvo.setCUSTOMER_EMAIL(micdvo.getCUSTOMER_EMAIL());
		cdvo.setPOLICY_NUMBER(micdvo.getPOLICY_NUMBER());
		cdvo.setCUSTOMER_DOB(micdvo.getCUSTOMER_DOB());
		cdvo.setCUSTOMER_NRIC_ID(micdvo.getCUSTOMER_NRIC_ID());
		cdvo.setPOLICY_EXPIRY_TIMESTAMP(micdvo.getPOLICY_EXPIRY_TIMESTAMP());
		cdvo.setVehRegNo(micdvo.getREGISTRATION_NUMBER());

		try {
			status = emailProcessor.emailDispatchProcessForMotor(cdvo, listOfAdditionalCoverage);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(status + "email Service status");
		return status;
	}

	public String gettingValueisNullOrNotInString(int indexVal, ResultSet rs) {
		String iVal = null;
		try {
			System.out.println(indexVal + "indexVal");
			iVal = rs.getString(indexVal);
			Double Value = Double.parseDouble(iVal);
			iVal = String.format("%.2f", Value);
			if (rs.wasNull()) {

				System.out.println(indexVal + "indexVal  value null or empty In Double");
				iVal = "0.00";
			}

			System.out.println(iVal);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return iVal;
	}
}
