package com.etiqa.email;

import java.io.FileNotFoundException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.dsp.dao.common.pojo.WTCcustQuotPmntPolicyVo;

import oracle.jdbc.OracleTypes;

public class WtcEmailService {
	public String callingWtcEmailService(String policyNo) {

		Connection connection = null;
		Statement statement = null;
		ResultSet rs = null;

		String langVal = "";
		connection = ConnectionFactory.getConnection();
		WTCcustQuotPmntPolicyVo cdvo = new WTCcustQuotPmntPolicyVo();
		CallableStatement cstmt;
		try {
			cstmt = connection.prepareCall("{CALL DSP_ADM_SP_WTCINFO_SELECT(?,?)}");

			cstmt.setString(1, policyNo);
			cstmt.registerOutParameter(2, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(2);
			while (rs.next()) {
				cdvo.setCUSTOMER_ID(rs.getInt("CUSTOMER_ID"));
				cdvo.setCUSTOMER_ID_TYPE(rs.getString("CUSTOMER_ID_TYPE"));
				cdvo.setCUSTOMER_NRIC_ID(rs.getString("CUSTOMER_NRIC_ID"));
				cdvo.setCUSTOMER_NAME(rs.getString("CUSTOMER_NAME"));
				cdvo.setCUSTOMER_DOB(rs.getString("CUSTOMER_DOB")); // rs.getString(5)
				cdvo.setCUSTOMER_GENDER(rs.getString("CUSTOMER_GENDER")); // rs.getString(6)
				cdvo.setCUSTOMER_RACE(rs.getString("CUSTOMER_RACE"));
				cdvo.setCUSTOMER_RELIGION(rs.getString("CUSTOMER_RELIGION"));
				cdvo.setCUSTOMER_NATIONALITY(rs.getString("CUSTOMER_NATIONALITY"));
				cdvo.setCUSTOMER_TITLE(rs.getString("CUSTOMER_TITLE"));
				cdvo.setCUSTOMER_NATIONALITY_RACE(rs.getString("CUSTOMER_NATIONALITY_RACE"));
				cdvo.setCUSTOMER_EDU_LEVEL(rs.getString("CUSTOMER_EDU_LEVEL"));
				cdvo.setCUSTOMER_MARITALSTATUS(rs.getString("CUSTOMER_MARITALSTATUS")); // rs.getString(13)
				cdvo.setCUSTOMER_SALARY_RANGE(rs.getString("CUSTOMER_SALARY_RANGE"));
				cdvo.setCUSTOMER_OCCUPATION(rs.getString("CUSTOMER_OCCUPATION"));
				cdvo.setCUSTOMER_ADDRESS1(rs.getString("CUSTOMER_ADDRESS1"));
				cdvo.setCUSTOMER_ADDRESS2(rs.getString("CUSTOMER_ADDRESS2"));
				cdvo.setCUSTOMER_ADDRESS3(rs.getString("CUSTOMER_ADDRESS3"));
				cdvo.setCUSTOMER_POSTCODE(rs.getString("CUSTOMER_POSTCODE"));
				cdvo.setCUSTOMER_STATE(rs.getString("CUSTOMER_STATE"));
				cdvo.setCUSTOMER_COUNTRY(rs.getString("CUSTOMER_COUNTRY"));
				cdvo.setCUSTOMER_MAIL_ADDRESS1(rs.getString("CUSTOMER_MAIL_ADDRESS1")); // rs.getString(22)
				cdvo.setCUSTOMER_MAIL_ADDRESS2(rs.getString("CUSTOMER_MAIL_ADDRESS2"));// rs.getString(23)
				cdvo.setCUSTOMER_MAIL_ADDRESS3(rs.getString("CUSTOMER_MAIL_ADDRESS3"));// rs.getString(24)
				cdvo.setCUSTOMER_MAIL_POSTCODE(rs.getString("CUSTOMER_MAIL_POSTCODE"));// rs.getString(25)
				cdvo.setCUSTOMER_MAIL_STATE(rs.getString("CUSTOMER_MAIL_STATE"));// rs.getString(26)
				cdvo.setCUSTOMER_MAIL_COUNTRY(rs.getString("CUSTOMER_MAIL_COUNTRY"));// rs.getString(27)
				cdvo.setCUSTOMER_NO_CHILDREN(rs.getInt("CUSTOMER_NO_CHILDREN"));
				cdvo.setCUSTOMER_MOBILE_NO(rs.getString("CUSTOMER_MOBILE_NO"));
				cdvo.setCUSTOMER_EMAIL(rs.getString("CUSTOMER_EMAIL"));
				cdvo.setLEADS_FLAG(rs.getString("LEADS_FLAG"));
				// cdvo.setSALES_TOOL(rs.getString(""));
				cdvo.setEMAIL_SENT_COUNTER(rs.getString("EMAIL_SENT_COUNTER"));
				cdvo.setQQ_ID(rs.getInt("QQ_ID"));
				cdvo.setCREATE_DATE(rs.getString("CREATE_DATE"));
				cdvo.setUPDATED_DATE(rs.getString("UPDATED_DATE"));
				cdvo.setCUSTOMER_EMPLOYER(rs.getString("CUSTOMER_EMPLOYER"));
				cdvo.setCUSTOMER_CLIENTTYPE(rs.getString("CUSTOMER_CLIENTTYPE"));
				cdvo.setCUSTOMER_INDUSTRY(""); // rs.getString(39)
				cdvo.setDSP_QQ_ID(rs.getInt("DSP_QQ_ID"));
				cdvo.setPRODUCT_CODE(rs.getString("PRODUCT_CODE"));
				cdvo.setCREATED_DATE(rs.getString("CREATED_DATE"));
				cdvo.setID(rs.getInt("ID"));
				// cdvo.setPOLICY_NUMBER (rs.getString("PRODUCT_PLAN_CODE"));
				cdvo.setPRODUCT_PLAN_CODE(rs.getString("PRODUCT_PLAN_CODE"));
				cdvo.setPOLICY_EFFECTIVE_TIMESTAMP(rs.getString("POLICY_EFFECTIVE_TIMESTAMP"));
				cdvo.setSUM_INSURED(rs.getInt("SUM_INSURED"));
				cdvo.setPOLICY_STATUS(rs.getString("POLICY_STATUS"));
				cdvo.setPOLICY_TYPE_CODE(rs.getString("POLICY_TYPE_CODE"));
				cdvo.setPOLICY_EXPIRY_TIMESTAMP(rs.getString("POLICY_EXPIRY_TIMESTAMP"));
				cdvo.setTRANSACTION_ID(rs.getInt("TRANSACTION_ID"));
				cdvo.setCREATION_TIMESTAMP(rs.getString("CREATION_TIMESTAMP"));

				cdvo.setWTC_QQ_ID(rs.getString("WTC_QQ_ID"));
				// private String DSP_QQ_ID,
				cdvo.setLEADS_EMAIL_ID(rs.getString("LEADS_EMAIL_ID"));
				cdvo.setPRODUCT_ID(rs.getString("PRODUCT_ID"));
				cdvo.setSUB_PRODUCT_ID(rs.getString("SUB_PRODUCT_ID"));
				cdvo.setTRAVEL_AREA_TYPE(rs.getString("TRAVEL_AREA_TYPE"));
				cdvo.setCOUNTRY_1(rs.getString("COUNTRY_1"));
				cdvo.setCOUNTRY_2(rs.getString("COUNTRY_2"));
				cdvo.setCOUNTRY_3(rs.getString("COUNTRY_3"));
				cdvo.setCOUNTRY_4(rs.getString("COUNTRY_4"));
				cdvo.setCOUNTRY_5(rs.getString("COUNTRY_5"));
				cdvo.setTRAVLLING_WITH(rs.getString("TRAVLLING_WITH"));
				cdvo.setTRIP_TYPE(rs.getString("TRIP_TYPE"));
				cdvo.setTRAVEL_START_DATE(rs.getString("TRAVEL_START_DATE"));
				cdvo.setTRAVEL_END_DATE(rs.getString("TRAVEL_END_DATE"));
				cdvo.setTRAVEL_DURATION(rs.getString("TRAVEL_DURATION"));
				cdvo.setFULL_NAME(rs.getString("FULL_NAME"));
				cdvo.setOFFERED_PLAN_NAME(rs.getString("OFFERED_PLAN_NAME"));
				cdvo.setINS_REJECTED_DECLERATION(rs.getString("INS_REJECTED_DECLERATION"));
				cdvo.setPROMOTION_CODE(rs.getString("PROMOTION_CODE"));
				cdvo.setPROMOTION_AMOUNT(rs.getString("PROMOTION_AMOUNT"));
				cdvo.setDISCOUNT_ID(rs.getString("DISCOUNT_ID"));
				cdvo.setDISCOUNT_AMOUNT(rs.getString("DISCOUNT_AMOUNT"));
				cdvo.setWT_POLICY_SUM_INSURED(rs.getString("WT_POLICY_SUM_INSURED"));
				cdvo.setWT_POLICY_PREMIUM(rs.getString("WT_POLICY_PREMIUM"));
				cdvo.setWT_GROSS_PREMIUM(rs.getString("WT_GROSS_PREMIUM"));
				cdvo.setGROSS_PREMIUM_GST(rs.getString("GROSS_PREMIUM_GST"));
				cdvo.setGROSS_PREMIUM_STAMP_DUTY(rs.getString("GROSS_PREMIUM_STAMP_DUTY"));
				cdvo.setTOTAL_PREMIUM_PAID(rs.getString("TOTAL_PREMIUM_PAID"));
				cdvo.setETIQA_PDPA_ACCEPT(rs.getString("ETIQA_PDPA_ACCEPT"));
				cdvo.setIMP_NOTICE_DECLERATION_PDA(rs.getString("IMP_NOTICE_DECLERATION_PDA"));
				cdvo.setIS_PURCHASED(rs.getString("IS_PURCHASED"));
				// cdvo.setET_WC_DSP_CUSTOMER_CUSTOMER_ID(rs.getString("ET_WC_DSP_CUSTOMER_CUSTOMER_ID"));
				cdvo.setPLAN_CODE(rs.getString("PLAN_CODE"));
				cdvo.setPOLICY_NUMBER(policyNo);
				// cdvo.setAGENT_CODE(rs.getString(94));
				/*
				 * QQ_STATUS 92 CREATE_DATE 93 AGENT_CODE 94 OPERATOR_CODE 95 BANK_CODE, 96
				 * ACCOUNT_NUMBER, 97 PAGE_NO, 98 UPDATE_DATE, 99 LANGUAGE 100 commission_amount
				 * 101 discount_percent 102 commission_percent 103
				 */
				/*
				 * TRANSACTION_ID 104 DSP_QQ_ID 105 PMNT_GATEWAY_CODE 106 DISCOUNT_CODE 107
				 * PROMO_CODE 108 AMOUNT 109 PMNT_STATUS 110 INVOICE_NO 111 TRANSACTION_DATETIME
				 * 112 TRANSACTION_STATUS 113
				 */
				// For UAT
				/*
				 * cdvo.setPMNT_GATEWAY_CODE(rs.getString(106));
				 * cdvo.setDISCOUNT_CODE(rs.getString(107));
				 * cdvo.setPROMO_CODE(rs.getString(108)); cdvo.setAMOUNT(rs.getString(109));
				 * cdvo.setPMNT_STATUS(rs.getString(110));
				 * cdvo.setINVOICE_NO(rs.getString(111));
				 * cdvo.setTRANSACTION_DATETIME(rs.getString(112));
				 * System.out.println(rs.getString(108));
				 * cdvo.setTRANSACTION_STATUS(rs.getString(113));
				 */

				// For production uncomment below code
				cdvo.setPMNT_GATEWAY_CODE(rs.getString("PMNT_GATEWAY_CODE"));
				cdvo.setDISCOUNT_CODE(rs.getString("DISCOUNT_CODE"));
				cdvo.setPROMO_CODE(rs.getString("PROMO_CODE"));
				cdvo.setAMOUNT(rs.getString("AMOUNT"));
				cdvo.setPMNT_STATUS(rs.getString("PMNT_STATUS"));
				cdvo.setINVOICE_NO(rs.getString("INVOICE_NO"));
				cdvo.setTRANSACTION_DATETIME(rs.getString("TRANSACTION_DATETIME"));
				cdvo.setTRANSACTION_STATUS(rs.getString("TRANSACTION_STATUS"));

			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		DspEmailDispatchProcessor emailProcessor = new DspEmailDispatchProcessor(new TermLifeProductMailTemplateLoad());
		String status = null;
		try {
			cdvo.setLangValue("lan_en");
			status = emailProcessor.emailDispatchProcessOfWtc(cdvo, policyNo);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(status + "email Service status");
		return status;
	}
}