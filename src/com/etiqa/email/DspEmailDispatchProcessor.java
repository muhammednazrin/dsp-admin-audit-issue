package com.etiqa.email;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
//import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
//import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.etiqa.dsp.dao.common.pojo.GeneratingReportsResponseVo;
import com.etiqa.dsp.dao.common.pojo.MotorInsuranceCustDetails;
import com.etiqa.dsp.dao.common.pojo.WTCcustQuotPmntPolicyVo;
import com.etiqa.dsp.dao.common.pojo.custQuotPmntPolicyVo;
import com.etiqa.dsp.dao.email.DspEmailTemplateBean;
import com.etiqa.dsp.dao.email.WtcDspEmailTemplateBean;

//@WebService
public class DspEmailDispatchProcessor {

	static Properties mailServerProperties;
	static Session getMailSession;
	static MimeMessage generateMailMessage;
	private static Properties activeDirMailProp = new Properties();
	ProductMailTemplateLoad productTemplate = null;

	public DspEmailDispatchProcessor(ProductMailTemplateLoad productTemplate) {
		this.productTemplate = productTemplate;
	}
	/*
	 * @WebMethod
	 *
	 * @WebResult(name="EmailCPF") public EmailCPFBean
	 * tplDetails(@WebParam(name="secusername")String
	 * secusername,@WebParam(name="secpassword")String
	 * secpassword,@WebParam(name="emailTo")String
	 * emailTo,@WebParam(name="templateId")int
	 * templateId, @WebParam(name="templateName")String
	 * templateName,@WebParam(name="identificationType")String
	 * identificationType,@WebParam(name="idNumber")String
	 * idNumber, @WebParam(name="mobileNumber")String
	 * mobileNumber,@WebParam(name="email")String
	 * email, @WebParam(name="requestDate")String
	 * requestDate,@WebParam(name="policyNo")String
	 * policyNo,@WebParam(name="username")String
	 * username, @WebParam(name="failmsg")String
	 * failmsg, @WebParam(name="trx_id")String trx_id){
	 *
	 * EmailCPFBean tplb = new EmailCPFBean(); SecureHeader sech = new
	 * SecureHeader(); tplb = sech.GetSecureHeader(secusername,secpassword);
	 * if(tplb.getValHeader()==1) { UID uid = new UID(); if(trx_id == null ||
	 * "".equalsIgnoreCase(trx_id)) trx_id = uid.toString();
	 * tplb.setEmailTo(emailTo); tplb.setTemplateId(templateId);
	 * tplb.setTemplateName(templateName);
	 * tplb.setIdentificationType(identificationType); tplb.setIdNumber(idNumber);
	 * tplb.setMobileNumber(mobileNumber); tplb.setPolicyNo(policyNo);
	 * tplb.setRequestDate(requestDate); tplb.setEmail(email);
	 * tplb.setUsername(username); tplb.setFailmsg(failmsg); tplb.setTrx_id(trx_id);
	 * String errormsg =""; loadEmailProps(); if(templateId == 0){ errormsg =
	 * errormsg.concat("Template Id is invalid"); }if(templateName == ""){ errormsg
	 * = errormsg.concat("\n templateName is invalid"); }if(emailTo == ""){ errormsg
	 * = errormsg.concat("\n emailTo is invalid"); }else { tplb =
	 * queryDatabaseDashboard(tplb); } if(errormsg != ""){
	 * tplb.setError_code("0003"); tplb.setError_msg(errormsg); } } else {
	 * tplb.setError_code("D7777"); tplb.setError_msg("system error"); } return
	 * tplb; }
	 */

	/*
	 * public static EmailCPFBean queryDatabaseDashboard(EmailCPFBean tplInfo){
	 * Connection connection = null;
	 *
	 * ResultSet rs = null; try { //dbutil database= new dbutil();
	 *
	 * connection = ConnectionFactory.getConnection(); CallableStatement cstmt =
	 * connection.prepareCall("{call EMAIL_TEMPLATE_RETRIVE(?,?,?)}");
	 * cstmt.setInt(1, tplInfo.getTemplateId()); cstmt.setString(2,
	 * tplInfo.getTemplateName()); cstmt.registerOutParameter(3,
	 * OracleTypes.CURSOR);
	 *
	 * cstmt.execute(); rs =(ResultSet)cstmt.getObject (3);
	 *
	 * DateFormat dateFormat = new SimpleDateFormat("MMM dd,yyyy 'at' HH:mm:ss a");
	 * Date date = new Date(); String todate = dateFormat.format(date); // print the
	 * results List<TemplateListBean> tpllst = new ArrayList<TemplateListBean>();
	 * if(rs!=null){
	 *
	 * tplInfo.setError_code("0000"); tplInfo.setError_msg("Succeed");
	 *
	 * }else {
	 *
	 * tplInfo.setError_code("0001"); tplInfo.setError_msg("Failed"); } String
	 * emailContent =""; String emailStatus ="";
	 *
	 * while (rs.next()) { TemplateListBean tplls = new TemplateListBean();
	 * tplls.setSubject(rs.getString(3)); emailContent =rs.getString(2);
	 * emailContent = emailContent.replace("[[username]]", tplInfo.getUsername());
	 * emailContent = emailContent.replace("[[TodayDate]]", todate); emailContent =
	 * emailContent.replace("[[identificationType]]",
	 * tplInfo.getIdentificationType()); emailContent =
	 * emailContent.replace("[[idNumber]]", tplInfo.getIdNumber()); emailContent =
	 * emailContent.replace("[[mobileNumber]]", tplInfo.getMobileNumber());
	 * emailContent = emailContent.replace("[[email]]", tplInfo.getEmail());
	 * emailContent = emailContent.replace("[[policyNo]]", tplInfo.getPolicyNo());
	 * emailContent = emailContent.replace("[[requestDate]]",
	 * tplInfo.getRequestDate()); emailContent = emailContent.replace("[[Failmsg]]",
	 * tplInfo.getFailmsg()); //System.out.println(emailContent);
	 * tplls.setTemplateDesc(rs.getString(2)); tplls.setTemplateEmail(emailContent);
	 * emailStatus = generateAndSendEmail(tplls,tplInfo.getEmailTo());
	 *
	 * tplls.setEmailStatus(emailStatus); tpllst.add(tplls); } if(emailStatus
	 * =="Done"){ tplInfo.setError_code("0000"); tplInfo.setError_msg("Succeed");
	 *
	 * }else{
	 *
	 * tplInfo.setError_code("0001"); tplInfo.setError_msg("Failed to send email");
	 *
	 * } tplInfo.setTplDesc(tpllst);
	 *
	 *
	 *
	 *
	 *
	 * } catch (Exception e) { // a failure occurred log message;
	 * System.out.println("[ERROR]"+new Date()+"Exception " +e.getMessage()+" "+
	 * e.getCause()); e.printStackTrace(); }finally { //cstmt.close(); try {
	 * if(connection!=null)connection.close(); } catch (SQLException e1) { // TODO
	 * Auto-generated catch block e1.printStackTrace(); } connection = null; try {
	 * if(rs!=null)rs.close();
	 *
	 * } catch (SQLException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } } return tplInfo; }
	 */

	private String dispatchDspEmail(DspEmailTemplateBean template, String PolicyNo) throws FileNotFoundException {

		String res = null;
		String Subject = template.getTemplateSubject();
		String Content = template.getTemplateBody();

	
		// Assuming you are sending email from localhost
		String host = activeDirMailProp.getProperty("emailhost");
		String fromemail = activeDirMailProp.getProperty("from");
		String port = activeDirMailProp.getProperty("emailport");
	
	

		
		// SMTP Prod Email---
		Properties prop = System.getProperties();
		//prop.setProperty("mail.smtp.host", host);
		 prop.put("mail.smtp.host",host); 
		 prop.put("mail.smtp.port", port);
		 prop.put("mail.smtp.auth", "false");
		 prop.put("mail.smtp.starttls.enable", "false");
		 prop.put("mail.smtp.ssl.trust", host);
		 Session session = Session.getDefaultInstance(prop);
		// END SMPTP PROD
		// smtp.gmail.com

	
		try {
			// InternetAddress.parse(emailTo)
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromemail));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(template.getEmailTo()));
			message.setSubject(template.getTemplateSubject());
			message.setContent(template.getTemplateBody(), "text/html");

			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(template.getTemplateBody());
			messageBodyPart.setContent(template.getTemplateBody(), "text/html");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			String filePath = "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs";
			String fileName = PolicyNo + ".zip";

			File file = new File(filePath + "/" + fileName);
			if (!file.exists()) {

				String filePath_tmp = "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_backup";
				File file_tmp = new File(filePath_tmp + "/" + fileName);

				System.out.println("zipfilename------" + filePath_tmp + "/" + fileName);
				addAttachment(multipart, filePath_tmp + "/" + fileName, fileName);
				// Send the complete message parts
				message.setContent(multipart);
				Transport.send(message);
				res = "Done";
			} else {

				System.out.println("zipfilename------" + filePath + "/" + fileName);
				addAttachment(multipart, filePath + "/" + fileName, fileName);
				// Send the complete message parts
				message.setContent(multipart);
				Transport.send(message);
				res = "Done";

			}

			// String filePath = "/tmp/"+PolicyNo+"/";

			// List<GeneratingReportsResponseVo> FileNamelist=
			// for(GeneratingReportsResponseVo fileName : FileNamelist){
			// PolicyNo= fileName.getPolicyNo();
			// System.out.println("Fin Obj: "+PolicyNo);
			// addAttachment(multipart, filePath+PolicyNo+".zip",PolicyNo+".zip");
			// }

			// Send the complete message parts
			// message.setContent(multipart);
			// Transport.send(message);
			// res ="Done";
		} catch (MessagingException e) {
			e.printStackTrace();
			res = "Failed";
		} catch (Exception e) {
			e.printStackTrace();
			res = "Failed";
		}
		return res;
	}

	private static void addAttachment(Multipart multipart, String filename, String singleFileName)
			throws MessagingException {
		DataSource source = new FileDataSource(filename);
		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(singleFileName);
		multipart.addBodyPart(messageBodyPart);
	}



	public String processDspMailConfigurations() {

		return null;
	}

	// Load the email properties
	private void loadEmailProps() {
		InputStream infoad = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/email/emailconfig.properties");
		try {
			activeDirMailProp.load(infoad);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String emailDispatchProcess(custQuotPmntPolicyVo custVo) throws FileNotFoundException {

		DspEmailTemplateBean templateBean = null;

		// Load mail properties
		loadEmailProps();

		// Load of the Policy
		templateBean = productTemplate.loadMailTemplate(custVo);
		
		// send the mail
		String status = dispatchDspEmail(templateBean, custVo.getPOLICY_NUMBER());

		return status;
	}

	public String emailDispatchProcessForMotor(custQuotPmntPolicyVo custVo,
			List<MotorInsuranceCustDetails> listOfAdditionalCoverage) throws FileNotFoundException {

		DspEmailTemplateBean templateBean = null;

		// Load mail properties
		loadEmailProps();

		// Load of the Policy
		if (productTemplate != null) {
			templateBean = productTemplate.loadMailTemplateForMotor(custVo, listOfAdditionalCoverage);
		}

		// send the mail
		String status = dispatchDspEmailForMotor(templateBean, custVo.getPOLICY_NUMBER());

		return status;
	}

	public String emailDispatchProcessOfWtc(WTCcustQuotPmntPolicyVo custVo, String policyNo)
			throws FileNotFoundException {
		System.out.println("Email ------------------------sucessful");
		WtcDspEmailTemplateBean templateBean = null;

		// Load mail properties
		loadEmailProps();

		// Load of the Policy
		if (productTemplate != null) {
			if (custVo.getPRODUCT_CODE().equals("TPT")) {

				if (custVo.getLangValue().equals("lan_en")) {
					templateBean = productTemplate.loadMailTemplateTakaful(custVo);
				} else {
					templateBean = productTemplate.loadMailTemplateTakafulBM(custVo);
				}
			} else {
				if (custVo.getLangValue().equals("lan_en")) {
					templateBean = productTemplate.loadMailTemplate(custVo);
				} else {
					templateBean = productTemplate.loadMailTemplateBM(custVo);
				}

			}

		}
		// send the mail
		String status = dispatchDspEmailForWtc(templateBean, policyNo);
		return status;
	}

	private String dispatchDspEmailForWtc(WtcDspEmailTemplateBean templateBean, String policy_NUMBER) {

		String res = null;
		String Subject = templateBean.getTemplateSubject();
		String Content = templateBean.getTemplateBody();

		// Assuming you are sending email from localhost
		String host = activeDirMailProp.getProperty("emailhost");
		String fromemail = activeDirMailProp.getProperty("from");
		String port = activeDirMailProp.getProperty("emailport");
	
		
		Properties prop = System.getProperties();
		//prop.setProperty("mail.smtp.host", host);
		 prop.put("mail.smtp.host",host); 
		 prop.put("mail.smtp.port", port);
		 prop.put("mail.smtp.auth", "false");
		 prop.put("mail.smtp.starttls.enable", "false");
		 prop.put("mail.smtp.ssl.trust", host);
		 Session session = Session.getDefaultInstance(prop);
		try {
			// InternetAddress.parse(emailTo)
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromemail));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(templateBean.getEmailTo()));
			message.setSubject(templateBean.getTemplateSubject());
			message.setContent(templateBean.getTemplateBody(), "text/html");

			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(templateBean.getTemplateBody());
			messageBodyPart.setContent(templateBean.getTemplateBody(), "text/html");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			String filePath = "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs";
			String fileName = policy_NUMBER + ".zip";

			File file = new File(filePath + "/" + fileName);
			if (!file.exists()) {
				System.out.println("file not exist in eiwdocs");

				String filePath_backup = "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_backup";
				File file_backup = new File(filePath_backup + "/" + fileName);
				if (!file_backup.exists()) {
					System.out.println("file not exist in eiwdocs backup");
					String filePath_temp = "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs/admin";
					File file_temp = new File(filePath_temp + "/" + fileName);
					if (file_temp.exists()) {
						System.out.println("file  exist in eiwdocs admin ");
					} else {
						System.out.println("file not exist in eiwdocs admin");
					}
					System.out.println("zipfilename------" + filePath_temp + "/" + fileName);
					addAttachment(multipart, filePath_temp + "/" + fileName, fileName);
					// Send the complete message parts
					message.setContent(multipart);
					Transport.send(message);
					res = "Done";
				} else {
					System.out.println("zipfilename------" + filePath_backup + "/" + fileName);
					addAttachment(multipart, filePath_backup + "/" + fileName, fileName);
					// Send the complete message parts
					message.setContent(multipart);
					Transport.send(message);
					res = "Done";
				}

			} else {

				System.out.println("zipfilename------" + filePath + "/" + fileName);
				addAttachment(multipart, filePath + "/" + fileName, fileName);
				// Send the complete message parts
				message.setContent(multipart);
				Transport.send(message);
				res = "Done";

			}

			// addAttachment(multipart, filePath+policy_NUMBER+".zip",policy_NUMBER+".zip");

			/*
			 * // String filePath =
			 * "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs/";
			 * String filePath = "/tmp/"+cdvo.getPOLICY_NUMBER()+"/";
			 * for(GeneratingReportsResponseVo fileName : FileNamelist){ String filename=
			 * fileName.getFileName(); System.out.println("Fin Obj: "+filename);
			 * addAttachment(multipart, filePath+filename,filename); }
			 * addAttachment(multipart,
			 * filePath+cdvo.getPOLICY_NUMBER()+".zip",cdvo.getPOLICY_NUMBER()+".zip");
			 */
			// Send the complete message parts
			// message.setContent(multipart);
			// Transport.send(message);
			// res ="Done";
		} catch (MessagingException e) {
			// throw new RuntimeException(e);
			res = e.getMessage();
			// res=RuntimeException(e);
			// Messaging
		}
		return res;
	}

	private String dispatchDspEmailForMotor(DspEmailTemplateBean template, String PolicyNo)
			throws FileNotFoundException {

		String res = null;
		String Subject = template.getTemplateSubject();
		String Content = template.getTemplateBody();

		
		// Assuming you are sending email from localhost
		String host = activeDirMailProp.getProperty("emailhost");
		String fromemail = activeDirMailProp.getProperty("from");
		String port = activeDirMailProp.getProperty("emailport");
		// String host = "172.31.13.10";
		System.out.println(host);

		// Get system properties
		// SMTP Prod Email---
		 Properties prop = System.getProperties();
		// prop.setProperty("mail.smtp.host", host);
		 prop.put("mail.smtp.host",host); 
		 prop.put("mail.smtp.port", port);
		 prop.put("mail.smtp.auth", "false");
		 prop.put("mail.smtp.starttls.enable", "false");
		 prop.put("mail.smtp.ssl.trust", host);
		 Session session = Session.getDefaultInstance(prop);
		// END SMPTP PROD
		// smtp.gmail.com

		

		try {
			// InternetAddress.parse(emailTo)
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromemail));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(template.getEmailTo()));
			message.setSubject(template.getTemplateSubject());
			message.setContent(template.getTemplateBody(), "text/html");

			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(template.getTemplateBody());
			messageBodyPart.setContent(template.getTemplateBody(), "text/html");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			String filePath = "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs";
			String fileName = PolicyNo + ".zip";

			File file = new File(filePath + "/" + fileName);
			if (!file.exists()) {

				String filePath_tmp = "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_backup";
				File file_tmp = new File(filePath_tmp + "/" + fileName);

				System.out.println("zipfilename------" + filePath_tmp + "/" + fileName);
				addAttachment(multipart, filePath_tmp + "/" + fileName, fileName);
				// Send the complete message parts
				message.setContent(multipart);
				Transport.send(message);
				res = "Done";
			} else {

				System.out.println("zipfilename------" + filePath + "/" + fileName);
				addAttachment(multipart, filePath + "/" + fileName, fileName);
				// Send the complete message parts
				message.setContent(multipart);
				Transport.send(message);
				res = "Done";

			}

			// String filePath = "/tmp/"+custVo.getPOLICY_NUMBER()+"/";
			/*
			 * for(GeneratingReportsResponseVo fileName : FileNamelist){ String filename=
			 * fileName.getFileName(); System.out.println("Fin Obj: "+filename);
			 * addAttachment(multipart, filePath+filename,filename); }
			 */

			// String filePath = "/tmp/"+PolicyNo+"/";

			// List<GeneratingReportsResponseVo> FileNamelist=
			// for(GeneratingReportsResponseVo fileName : FileNamelist){
			// PolicyNo= fileName.getPolicyNo();
			// System.out.println("Fin Obj: "+PolicyNo);
			// addAttachment(multipart, filePath+PolicyNo+".zip",PolicyNo+".zip");
			// }

			// Send the complete message parts

			/*
			 * String filePath =
			 * "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs/";
			 *
			 * System.out.println(FileNamelist.size()); for(GeneratingReportsResponseVo
			 * fileName : FileNamelist){ String filename= fileName.getFileName();
			 * System.out.println("Fin Obj: "+filename); addAttachment(multipart,
			 * filePath+filename,filename); }
			 *
			 * // Send the complete message parts
			 */ // message.setContent(multipart);
				// Transport.send(message);
				// res ="Done";
		} catch (MessagingException e) {
			e.printStackTrace();
			res = "Failed";
		} catch (Exception e) {
			e.printStackTrace();
			res = "Failed";
		}
		return res;
	}

	public static void main(String args[]) {
		// EmailCPFBean tplb = new EmailCPFBean();
		// DspEmailDispatchProcessor ecp =new DspEmailDispatchProcessor();
		/*
		 * //String secCon =""; String secusername ="admin"; String
		 * secpassword="@#$FRdsff"; SecureHeader sech = new SecureHeader(); tplb =
		 * sech.GetSecureHeader(secusername,secpassword); if(tplb.getValHeader()==1) {
		 * tplb.setEmailTo("tulasi@absecmy.com"); tplb.setTemplateId(1);
		 * tplb.setTemplateName("FailedRegister"); tplb.setIdentificationType("Police");
		 * tplb.setIdNumber("870908767574"); tplb.setMobileNumber("0134233434");
		 * //tplb.setPolicyNo(""); tplb.setUsername("test");
		 * tplb.setRequestDate("2016-03-24"); tplb.setEmail("test@gmail.com");
		 * tplb.setFailmsg("failmsg"); tplb.setPolicyNo("PA43434");
		 *
		 *
		 *
		 * tplb = queryDatabaseDashboard(tplb); } else { tplb.setError_code("test"); }
		 * System.out.println(tplb.getValHeader());
		 */
		/*
		 * TemplateListBean tlbean=new TemplateListBean(); UID uid = new UID();
		 *
		 * tlbean.setSubject("test"); tlbean.setTemplateEmail("tesssd");
		 * tplb.setEmailTo("farooq@absecmy.com"); tplb.setTrx_id(uid.toString());
		 * DspEmailDispatchProcessor em=new DspEmailDispatchProcessor();
		 * em.loadEmailProps(); String emailStatus =
		 * generateAndSendEmail(tlbean,tplb.getEmailTo());
		 * if(emailStatus.equalsIgnoreCase("Done")){ tplb.setError_code("D0000");
		 * tplb.setError_msg("email successfully sent"); }else{
		 * tplb.setError_code("D9999"); tplb.setError_msg(emailStatus); }
		 * System.out.println(tplb.getError_msg());
		 */
	}

	public String emailDispatchProcessForUpdatedNominationForm(custQuotPmntPolicyVo cdvo,
			List<GeneratingReportsResponseVo> list) {
		DspEmailTemplateBean templateBean = null;

		// Load mail properties
		loadEmailProps();

		// Load of the Policy
		/*
		 * if(productTemplate != null){ templateBean =
		 * productTemplate.loadMailTemplateForUpdatedNominationForm(cdvo); }
		 */
		if (productTemplate != null) {
			if (cdvo.getLangValue().equals("lan_en")) {
				templateBean = productTemplate.loadMailTemplateForUpdatedNominationForm(cdvo);
			} else {
				templateBean = productTemplate.loadMailTemplateForUpdatedNominationFormBM(cdvo);
			}
		}

		// send the mail
		String status = null;

		status = dispatchDspEmailForUpdatedNominationForm(templateBean, list, cdvo.getPOLICY_NUMBER());

		return status;
	}

	private String dispatchDspEmailForUpdatedNominationForm(DspEmailTemplateBean template,
			List<GeneratingReportsResponseVo> FileNamelist, String policy_NUMBER) {
		String res = null;
		String Subject = template.getTemplateSubject();
		String Content = template.getTemplateBody();

	
		// Assuming you are sending email from localhost
		String host = activeDirMailProp.getProperty("emailhost");
		String fromemail = activeDirMailProp.getProperty("from");
		String port = activeDirMailProp.getProperty("emailport");
		
		
		// Get system properties
		// SMTP Prod Email---
		 Properties prop = System.getProperties();
		// prop.setProperty("mail.smtp.host", host);
		 prop.put("mail.smtp.host",host); 
		 prop.put("mail.smtp.port", port);
		 prop.put("mail.smtp.auth", "false");
		 prop.put("mail.smtp.starttls.enable", "false");
		 prop.put("mail.smtp.ssl.trust", host);
		 Session session = Session.getDefaultInstance(prop);
		// END SMPTP PROD
		// smtp.gmail.com

		try {
			// InternetAddress.parse(emailTo)
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromemail));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(template.getEmailTo()));
			message.setSubject(template.getTemplateSubject());
			message.setContent(template.getTemplateBody(), "text/html");

			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(template.getTemplateBody());
			messageBodyPart.setContent(template.getTemplateBody(), "text/html");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			// String filePath =
			// "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs/";
			String filePath = "/tmp/";
			System.out.println(FileNamelist.size());
			for (GeneratingReportsResponseVo fileName : FileNamelist) {
				String filename = fileName.getFileName();
				System.out.println("Fin Obj: " + filename);
				addAttachment(multipart, filePath + filename, filename);
			}

			// Send the complete message parts
			message.setContent(multipart);
			Transport.send(message);
			res = "Done";
		} catch (MessagingException e) {
			e.printStackTrace();
			res = "Failed";
		} catch (Exception e) {
			e.printStackTrace();
			res = "Failed";
		}
		return res;
	}

	public String emailDispatchProcessForMotorTakaful(custQuotPmntPolicyVo cdvo,
			List<MotorInsuranceCustDetails> listOfAdditionalCoverage) throws FileNotFoundException {

		DspEmailTemplateBean templateBean = null;

		// Load mail properties
		loadEmailProps();

		// Load of the Policy
		if (productTemplate != null) {
			templateBean = productTemplate.loadMailTemplateForMotorTakaful(cdvo, listOfAdditionalCoverage);
		}

		// send the mail
		String status = null;
		status = dispatchDspEmailForMotorTakaful(templateBean, cdvo.getPOLICY_NUMBER());

		return status;
	}

	private String dispatchDspEmailForMotorTakaful(DspEmailTemplateBean template, String PolicyNo) {
		String res = null;
		String Subject = template.getTemplateSubject();
		String Content = template.getTemplateBody();

	
		// Assuming you are sending email from localhost
		String host = activeDirMailProp.getProperty("emailhost");
		String fromemail = activeDirMailProp.getProperty("from");
		String port = activeDirMailProp.getProperty("emailport");
	
		

		// Get system properties
		// SMTP Prod Email---
		 Properties prop = System.getProperties();
		 //prop.setProperty("mail.smtp.host", host);
		 prop.put("mail.smtp.host",host); 
		 prop.put("mail.smtp.port", port);
		 prop.put("mail.smtp.auth", "false");
		 prop.put("mail.smtp.starttls.enable", "false");
		 prop.put("mail.smtp.ssl.trust", host);
		Session session = Session.getDefaultInstance(prop);
		// END SMPTP PROD
		// smtp.gmail.com



		try {
			// InternetAddress.parse(emailTo)
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromemail));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(template.getEmailTo()));
			message.setSubject(template.getTemplateSubject());
			message.setContent(template.getTemplateBody(), "text/html");

			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(template.getTemplateBody());
			messageBodyPart.setContent(template.getTemplateBody(), "text/html");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			String filePath = "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs";
			String fileName = PolicyNo + ".zip";

			File file = new File(filePath + "/" + fileName);
			if (!file.exists()) {

				String filePath_tmp = "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_backup";
				File file_tmp = new File(filePath_tmp + "/" + fileName);

				System.out.println("zipfilename------" + filePath_tmp + "/" + fileName);
				addAttachment(multipart, filePath_tmp + "/" + fileName, fileName);
				// Send the complete message parts
				message.setContent(multipart);
				Transport.send(message);
				res = "Done";
			} else {

				System.out.println("zipfilename------" + filePath + "/" + fileName);
				addAttachment(multipart, filePath + "/" + fileName, fileName);
				// Send the complete message parts
				message.setContent(multipart);
				Transport.send(message);
				res = "Done";

			}

			// String filePath = "/tmp/"+custVo.getPOLICY_NUMBER()+"/";
			/*
			 * for(GeneratingReportsResponseVo fileName : FileNamelist){ String filename=
			 * fileName.getFileName(); System.out.println("Fin Obj: "+filename);
			 * addAttachment(multipart, filePath+filename,filename); }
			 */

			// String filePath = "/tmp/"+PolicyNo+"/";

			// List<GeneratingReportsResponseVo> FileNamelist=
			// for(GeneratingReportsResponseVo fileName : FileNamelist){
			// PolicyNo= fileName.getPolicyNo();
			// System.out.println("Fin Obj: "+PolicyNo);
			// addAttachment(multipart, filePath+PolicyNo+".zip",PolicyNo+".zip");
			// }

			// Send the complete message parts

			/*
			 * String filePath =
			 * "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs/";
			 *
			 * System.out.println(FileNamelist.size()); for(GeneratingReportsResponseVo
			 * fileName : FileNamelist){ String filename= fileName.getFileName();
			 * System.out.println("Fin Obj: "+filename); addAttachment(multipart,
			 * filePath+filename,filename); }
			 *
			 * // Send the complete message parts
			 */ // message.setContent(multipart);
				// Transport.send(message);
				// res ="Done";
		} catch (MessagingException e) {
			e.printStackTrace();
			res = "Failed";
		} catch (Exception e) {
			e.printStackTrace();
			res = "Failed";
		}
		return res;
	}

	/**
	 * EMail
	 *
	 * @param custVo
	 * @return
	 * @throws FileNotFoundException
	 */
	public String emailDispatchProcessForRoadTax(custQuotPmntPolicyVo custVo) throws FileNotFoundException {

		DspEmailTemplateBean templateBean = null;

		// Load mail properties
		loadEmailProps();

		// Load of the Policy
		if (productTemplate != null) {
			templateBean = productTemplate.loadMailTemplateForRoadTax(custVo);
		}

		// send the mail
		String status = dispatchDspEmailForRoadTax(templateBean, custVo.getPOLICY_NUMBER());

		return status;
	}

	/**
	 * EMail
	 *
	 * @param template
	 * @param PolicyNo
	 * @return
	 * @throws FileNotFoundException
	 */
	private String dispatchDspEmailForRoadTax(DspEmailTemplateBean template, String PolicyNo)
			throws FileNotFoundException {

		String res = null;
		String Subject = template.getTemplateSubject();
		String Content = template.getTemplateBody();

	
		// Assuming you are sending email from localhost
		String host = activeDirMailProp.getProperty("emailhost");
		String fromemail = activeDirMailProp.getProperty("from");
		String port = activeDirMailProp.getProperty("emailport");
		// String host = "172.31.13.10";
		System.out.println(host);

		// Get system properties
		// SMTP Prod Email---
		 Properties prop = System.getProperties();
		// prop.setProperty("mail.smtp.host", host);
		 prop.put("mail.smtp.host",host); 
		 prop.put("mail.smtp.port", port);
		 prop.put("mail.smtp.auth", "false");
		 prop.put("mail.smtp.starttls.enable", "false");
		 prop.put("mail.smtp.ssl.trust", host);
		 Session session = Session.getDefaultInstance(prop);
		// END SMPTP PROD
		// smtp.gmail.com

		

		try {
			// InternetAddress.parse(emailTo)
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromemail));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(template.getEmailTo()));
			message.setSubject(template.getTemplateSubject());
			message.setContent(template.getTemplateBody(), "text/html");

			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(template.getTemplateBody());
			messageBodyPart.setContent(template.getTemplateBody(), "text/html");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			/*
			 * String filePath =
			 * "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs";
			 * String fileName = PolicyNo+".zip";
			 */

			/*
			 * File file = new File(filePath+"/"+fileName); if(!file.exists()){
			 */
			/*
			 * String filePath_tmp =
			 * "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_backup";
			 * File file_tmp = new File(filePath_tmp+"/"+fileName);
			 *
			 * System.out.println("zipfilename------"+filePath_tmp+"/"+fileName);
			 * addAttachment(multipart, filePath_tmp+"/"+fileName,fileName);
			 */
			// Send the complete message parts
			message.setContent(multipart);
			Transport.send(message);
			res = "Done";
			// }
			/*
			 * else{
			 *
			 *
			 * System.out.println("zipfilename------"+filePath+"/"+fileName);
			 * addAttachment(multipart, filePath+"/"+fileName,fileName); // Send the
			 * complete message parts message.setContent(multipart);
			 * Transport.send(message); res ="Done";
			 *
			 * }
			 */

			// String filePath = "/tmp/"+custVo.getPOLICY_NUMBER()+"/";
			/*
			 * for(GeneratingReportsResponseVo fileName : FileNamelist){ String filename=
			 * fileName.getFileName(); System.out.println("Fin Obj: "+filename);
			 * addAttachment(multipart, filePath+filename,filename); }
			 */

			// String filePath = "/tmp/"+PolicyNo+"/";

			// List<GeneratingReportsResponseVo> FileNamelist=
			// for(GeneratingReportsResponseVo fileName : FileNamelist){
			// PolicyNo= fileName.getPolicyNo();
			// System.out.println("Fin Obj: "+PolicyNo);
			// addAttachment(multipart, filePath+PolicyNo+".zip",PolicyNo+".zip");
			// }

			// Send the complete message parts

			/*
			 * String filePath =
			 * "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs/";
			 *
			 * System.out.println(FileNamelist.size()); for(GeneratingReportsResponseVo
			 * fileName : FileNamelist){ String filename= fileName.getFileName();
			 * System.out.println("Fin Obj: "+filename); addAttachment(multipart,
			 * filePath+filename,filename); }
			 *
			 * // Send the complete message parts
			 */ // message.setContent(multipart);
				// Transport.send(message);
				// res ="Done";
		} catch (MessagingException e) {
			e.printStackTrace();
			res = "Failed";
		} catch (Exception e) {
			e.printStackTrace();
			res = "Failed";
		}
		return res;
	}
}
