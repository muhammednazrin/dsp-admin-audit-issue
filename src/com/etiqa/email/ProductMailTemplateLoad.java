package com.etiqa.email;

import java.util.List;

import com.etiqa.dsp.dao.common.pojo.MotorInsuranceCustDetails;
import com.etiqa.dsp.dao.common.pojo.WTCcustQuotPmntPolicyVo;
import com.etiqa.dsp.dao.common.pojo.custQuotPmntPolicyVo;
import com.etiqa.dsp.dao.email.DspEmailTemplateBean;
import com.etiqa.dsp.dao.email.WtcDspEmailTemplateBean;

public interface ProductMailTemplateLoad {

	public DspEmailTemplateBean loadMailTemplate(custQuotPmntPolicyVo custVo);

	public DspEmailTemplateBean loadMailTemplateForRoadTax(custQuotPmntPolicyVo custVo);

	public DspEmailTemplateBean loadMailTemplateForUpdatedNominationForm(custQuotPmntPolicyVo cdvo);

	public DspEmailTemplateBean loadMailTemplateBM(custQuotPmntPolicyVo custVo);

	public DspEmailTemplateBean loadMailTemplateForUpdatedNominationFormBM(custQuotPmntPolicyVo cdvo);

	public DspEmailTemplateBean loadMailTemplateForMotor(custQuotPmntPolicyVo custVo,
			List<MotorInsuranceCustDetails> listOfAdditionalCoverage);

	public WtcDspEmailTemplateBean loadMailTemplate(WTCcustQuotPmntPolicyVo custVo);

	public WtcDspEmailTemplateBean loadMailTemplateTakaful(WTCcustQuotPmntPolicyVo custVo);

	public WtcDspEmailTemplateBean loadMailTemplateBM(WTCcustQuotPmntPolicyVo custVo);

	public WtcDspEmailTemplateBean loadMailTemplateTakafulBM(WTCcustQuotPmntPolicyVo custVo);

	public DspEmailTemplateBean loadMailTemplateForMotorTakaful(custQuotPmntPolicyVo cdvo,
			List<MotorInsuranceCustDetails> listOfAdditionalCoverage);

}
