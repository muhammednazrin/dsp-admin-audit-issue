package com.etiqa.email;

import java.io.FileNotFoundException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.common.DB.DBUtil;
import com.etiqa.dsp.dao.common.pojo.HOHHcustQuotPmntPolicyVo;
import com.etiqa.dsp.sales.process.email.HOHHDspEmailDispatchProcessor;
import com.etiqa.dsp.sales.process.email.HOHHProductMailTemplateLoadimpl;

import oracle.jdbc.OracleTypes;

public class HohhEmailService {

	public String getGenReportResponse(String policyNo) {
		Connection connection = null;
		Statement statement = null;
		ResultSet rs = null;
		ResultSet rs1 = null;

		List<String> addtional1 = new ArrayList<String>();
		List<String> addtional2 = new ArrayList<String>();
		List<String> addtional3 = new ArrayList<String>();
		List<Integer> addtional4 = new ArrayList<Integer>();

		try {
			connection = ConnectionFactory.getConnection();
			// CallableStatement cstmt = connection.prepareCall("{CALL
			// DSP_HOHH_SP_TOTALINFO(?,?,?)}");
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_HOHHINFO_SELECT(?,?,?)}");
			// DSP_HOHH_SP_TOTALINFO_test
			cstmt.setString(1, policyNo);
			cstmt.registerOutParameter(2, OracleTypes.CURSOR);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();

			List<String> list = new ArrayList<String>();
			HOHHcustQuotPmntPolicyVo cdvo = new HOHHcustQuotPmntPolicyVo();

			rs = (ResultSet) cstmt.getObject(2);
			rs1 = (ResultSet) cstmt.getObject(3);

			ResultSetMetaData rsmd = rs.getMetaData();
			int columnsNumber = rsmd.getColumnCount();

			System.out.println("numbers of records i n result set is    :" + columnsNumber);

			/*
			 * while(rs1.next()) {
			 *
			 * System.out.println("Content----------"+rs1.getString(1)+"--"+rs1.getString(2)
			 * +"--"+rs1.getString(3)+"--"+rs1.getString(4)+"--"+rs1.getString(5)); }
			 */
			int ItemNo = 0;
			if (rs1 != null) {
				while (rs1.next()) {
					ItemNo++;
					addtional1.add(rs1.getString("ADDITIONAL_BENEFIT_CODE"));
					addtional2.add(rs1.getString("ADDITIONAL_BENEFIT_TEXT"));
					addtional3.add(rs1.getString("ADDITIONAL_BENEFIT_VALUE"));
					addtional4.add(ItemNo);
					System.out.println(rs1.getString("ADDITIONAL_BENEFIT_CODE") + "====="
							+ rs1.getString("ADDITIONAL_BENEFIT_TEXT") + "====="
							+ rs1.getString("ADDITIONAL_BENEFIT_VALUE") + "=====" + ItemNo);

				}
			}

			cdvo.setADDITIONAL_BENEFIT_CODE(addtional1);
			cdvo.setADDITIONAL_BENEFIT_TEXT(addtional2);
			cdvo.setADDITIONAL_BENEFIT_VALUE(addtional3);
			cdvo.setADDITIONAL_BENEFIT_ITEMNUMBER(addtional4);

			while (rs.next()) {
				// cdvo.setCUSTOMER_ID(rs.getInt(1));
				// cdvo.setCUSTOMER_ID_TYPE(rs.getString(2));
				cdvo.setCUSTOMER_NRIC_ID(rs.getString(1));
				cdvo.setCUSTOMER_NAME(rs.getString(2));
				System.out.println("custermername----------" + rs.getString(2));
				// cdvo.setCUSTOMER_DOB(rs.getString(5)); //rs.getString(5)
				// cdvo.setCUSTOMER_GENDER(rs.getString(6)); //rs.getString(6)
				// cdvo.setCUSTOMER_RACE(rs.getString(7));
				// cdvo.setCUSTOMER_RELIGION(rs.getString(8));
				// cdvo.setCUSTOMER_NATIONALITY(rs.getString(9));
				// cdvo.setCUSTOMER_TITLE(rs.getString(10));
				// cdvo.setCUSTOMER_NATIONALITY_RACE(rs.getString(11));
				// cdvo.setCUSTOMER_EDU_LEVEL(rs.getString(12));
				// cdvo.setCUSTOMER_MARITALSTATUS(rs.getString(13)); //rs.getString(13)
				// cdvo.setCUSTOMER_SALARY_RANGE (rs.getString(14));
				// cdvo.setCUSTOMER_OCCUPATION(rs.getString(15));
				cdvo.setCUSTOMER_ADDRESS1(rs.getString(3));
				cdvo.setCUSTOMER_ADDRESS2(rs.getString(4));
				cdvo.setCUSTOMER_ADDRESS3(rs.getString(5));
				cdvo.setCUSTOMER_POSTCODE(rs.getString(6));
				cdvo.setCUSTOMER_STATE(rs.getString(7));
				cdvo.setCUSTOMER_COUNTRY(rs.getString(8));
				// cdvo.setCUSTOMER_MAIL_ADDRESS1(rs.getString(22)); //rs.getString(22)
				// cdvo.setCUSTOMER_MAIL_ADDRESS2(rs.getString(23));//rs.getString(23)
				// cdvo.setCUSTOMER_MAIL_ADDRESS3 (rs.getString(24));//rs.getString(24)
				// cdvo.setCUSTOMER_MAIL_POSTCODE(rs.getString(25));//rs.getString(25)
				// cdvo.setCUSTOMER_MAIL_STATE (rs.getString(26));//rs.getString(26)
				// cdvo.setCUSTOMER_MAIL_COUNTRY(rs.getString(27));//rs.getString(27)
				// cdvo.setCUSTOMER_NO_CHILDREN (rs.getInt(28));
				cdvo.setCUSTOMER_MOBILE_NO(rs.getString(9));
				cdvo.setCUSTOMER_EMAIL(rs.getString(10));
				System.out.println("mail id---------------" + rs.getString(10));
				// cdvo.setLEADS_FLAG (rs.getString(31));
				// cdvo.setSALES_TOOL(rs.getString(32));
				// cdvo.setEMAIL_SENT_COUNTER (rs.getString(33));
				// cdvo.setQQ_ID (rs.getInt(34));
				// cdvo.setCREATE_DATE (rs.getString(35));
				// cdvo.setUPDATED_DATE (rs.getString(36));
				// cdvo.setCUSTOMER_EMPLOYER (rs.getString(37));
				// cdvo.setCUSTOMER_CLIENTTYPE(rs.getString(38));
				// cdvo.setCUSTOMER_INDUSTRY(""); //rs.getString(39)
				cdvo.setDSP_QQ_ID(rs.getString(11));
				cdvo.setPRODUCT_CODE(rs.getString(12));
				cdvo.setCREATED_DATE(rs.getString(14));
				// cdvo.setID(rs.getInt(44));
				cdvo.setPOLICY_NUMBER(rs.getString(15));
				// cdvo.setPRODUCT_PLAN_CODE(rs.getString(47));
				cdvo.setPOLICY_EFFECTIVE_TIMESTAMP(rs.getString(16));
				// cdvo.setSUM_INSURED (rs.getInt(49));
				cdvo.setPOLICY_STATUS(rs.getString(17));
				// cdvo.setPOLICY_TYPE_CODE (rs.getString(51));
				cdvo.setPOLICY_EXPIRY_TIMESTAMP(rs.getString(18));
				//
				cdvo.setCREATION_TIMESTAMP(rs.getString(19));

				// cdvo.setHOHH_QQ_ID(rs.getString(58));
				// private String DSP_QQ_ID;
				// cdvo.setLEADS_EMAIL_ID(rs.getString(60));
				// private String PRODUCT_CODE;
				// cdvo.setSUB_PRODUCT_ID(rs.getString(62));
				cdvo.setHOME_COVERAGE(rs.getString(20));
				cdvo.setHOME_TYPE(rs.getString(21));
				cdvo.setBUILDING_CONSTRUCTION_TYPE(rs.getString(22));
				cdvo.setNUMBER_OF_STOREYS(rs.getString(23));
				cdvo.setHOME_SUM_INSURED(rs.getString(24));
				cdvo.setCONTENT_SUM_INSURED(rs.getString(25));
				cdvo.setPROVIDED_ETIQA_OFFER(rs.getString(26));
				// cdvo.setPROMOTION_CODE(rs.getString(70));
				// cdvo.setPROMOTION_AMOUNT(rs.getString(71));
				cdvo.setDISCOUNT_ID(rs.getString(27));
				cdvo.setDISCOUNT_AMOUNT(rs.getString(28));
				cdvo.setHOHH_POLICY_HOME_SUM_INS(rs.getString(29));
				// cdvo.setHOHH_SUM_INSURED_PREMIUM(rs.getString(75));
				// cdvo. setHOHH_ADD_BENIFITS_PREMIUM(rs.getString(76));
				// cdvo.setHOHH_POLICY_PREMIUM(rs.getString(77));
				cdvo.setGROSS_PREMIUM_GST(rs.getString(30));
				cdvo.setGROSS_PREMIUM_STAMP_DUTY(rs.getString(31));
				cdvo.setHOHH_TOTAL_PREMIUM_PAID(rs.getString(32));
				cdvo.setPROPERTY_INSURED_ADDRESS(rs.getString(33));
				cdvo.setPROPERTY_INSURED_POSTCODE(rs.getString(34));
				cdvo.setPROPERTY_INSURED_STATE(rs.getString(35));
				cdvo.setPROPERTY_INSURED_COUNTRY(rs.getString(36));
				// cdvo.setUNDER_WRITING_QUESTION_1(rs.getString(85));
				// cdvo.setETIQA_PDPA_ACCEPT(rs.getString(86));
				// cdvo.setIMP_NOTICE_DECLERATION_PDA(rs.getString(87));
				// cdvo.setPAYMENT_METHOD(rs.getString(88));
				// cdvo.setET_WC_DSP_CUSTOMER_CUSTOMER_ID(rs.getString(89));
				cdvo.setHOHH_GROSS_PREMIUM(rs.getString(37));
				cdvo.setCREATE_DATE_QQ(rs.getString(38));
				cdvo.setADD_BEN_EXTENDED_THEFT(rs.getString(39));
				cdvo.setADD_BEN_RIOT_STRIKE(rs.getString(40));
				cdvo.setADD_BEN_RIOT_STRIKE_AMT(rs.getString(41));
				cdvo.setADD_BEN_EXTENDED_THEFT_AMT(rs.getString(42));

				// cdvo.setIS_PURCHASED(rs.getString(91));
				// cdvo.setFULL_NAME(rs.getString(92));

				System.out.println("payment field call");

				cdvo.setPMNT_GATEWAY_CODE(rs.getString(43));

				// cdvo.setDISCOUNT_CODE(rs.getString(96));

				// cdvo.setPROMO_CODE(rs.getString(97));

				cdvo.setAMOUNT(rs.getString(44));

				cdvo.setPMNT_STATUS(rs.getString(45));

				cdvo.setINVOICE_NO(rs.getString(46));

				cdvo.setTRANSACTION_DATETIME(rs.getString(47));

				cdvo.setTRANSACTION_STATUS(rs.getString(48));
				cdvo.setTRANSACTION_ID(rs.getInt(49));

				cdvo.setHOHH_LANGUAGE(rs.getString(50));
				cdvo.setCUSTOMER_DOB(rs.getString(51));
				System.out.println("Custermor DOB--" + rs.getString(51));

				cdvo.setURQ1(rs.getString(52));
				cdvo.setURQ2(rs.getString(53));
				cdvo.setURQ3(rs.getString(54));
				cdvo.setURQ4(rs.getString(55));
				cdvo.setURQ5(rs.getString(56));
				cdvo.setURQ6(rs.getString(57));
				cdvo.setPROPERTY_INSURED_ADDRESS1(rs.getString(58));
				cdvo.setPROPERTY_INSURED_ADDRESS2(rs.getString(59));

			}

			HOHHDspEmailDispatchProcessor emailProcessor = new HOHHDspEmailDispatchProcessor(
					new HOHHProductMailTemplateLoadimpl());
			String status = null;
			try {
				status = emailProcessor.emailDispatchProcess(cdvo);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(status + "  :email status");

			return status;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);

		}
	}
}
