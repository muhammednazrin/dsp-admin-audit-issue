package com.etiqa.DAO;

import java.util.List;

import com.spring.VO.BuddyPABenefit;
import com.spring.VO.BuddyPABenefitGrpList;
import com.spring.VO.BuddyPAPlan;
import com.spring.VO.BuddyPAPlanGrpList;

public interface BuddyPADAO {

	public List<BuddyPAPlanGrpList> getPlanGrpList(String companyId);

	public List<BuddyPAPlan> getPlanList(String companyId, String comboId);

	public List<BuddyPABenefit> getPremiumList(String companyId);

	public List<BuddyPABenefit> getBenefitList(String companyId, String comboId, String planId);

	public List<BuddyPABenefit> getBenefitListDist(String companyId, String comboId);

	public List<BuddyPABenefitGrpList> getBenefitGrpList(String companyId, String comboId);

	public List<BuddyPABenefitGrpList> getEditBenefitGrpList(String companyId, String comboId, String planId);

	public List<BuddyPAPlan> getDistPlanList(String companyId, String comboId);

	public String updBenefitGrouping(BuddyPABenefitGrpList benefitGrpItem);

	public String updBenefitList(BuddyPABenefitGrpList benefitGrpItem);

	public List<BuddyPABenefitGrpList> getPremiumTotal(String companyId, String comboId);

}
