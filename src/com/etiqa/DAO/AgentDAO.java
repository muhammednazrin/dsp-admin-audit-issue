
package com.etiqa.DAO;

import java.util.List;

import com.etiqa.model.agent.Agent;

public interface AgentDAO {
	public List<Agent> getAgentDetails(int ID);

	public Agent findById(Long id);

	public List<Agent> findAll();

	public void save(Agent entity);

	public Agent update(Agent entity);

	public void delete(Agent entity);

	public List<Agent> searchByList(String agentName, String agentCode, String agentCategory, String insuranceType,
			String status, String dateFrom, String dateTo);
	

}
