package com.etiqa.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.common.DB.DBUtil;
import com.spring.VO.BuddyPABenefit;
import com.spring.VO.BuddyPABenefitGrpList;
import com.spring.VO.BuddyPAPlan;
import com.spring.VO.BuddyPAPlanGrpList;

import oracle.jdbc.OracleTypes;

public class BuddyPADAOImpl implements BuddyPADAO {

	Connection connection = ConnectionFactory.getConnection();
	Statement statement = null;
	ResultSet rs = null;

	@Override
	public List<BuddyPAPlanGrpList> getPlanGrpList(String companyId) {

		try {

			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_BUDDY_SP_TBL_PLAN_GROUPING(?,?)}");

			cstmt.setString(1, companyId);
			cstmt.registerOutParameter(2, OracleTypes.CURSOR);
			cstmt.execute();
			List<BuddyPAPlanGrpList> detail = new ArrayList<BuddyPAPlanGrpList>();

			rs = (ResultSet) cstmt.getObject(2);
			while (rs.next()) {

				BuddyPAPlanGrpList buddyPAPlanGrpList = new BuddyPAPlanGrpList();
				buddyPAPlanGrpList.setComboId(rs.getString("COMBO_ID"));
				buddyPAPlanGrpList.setComboName(rs.getString("COMBO_NAME"));
				buddyPAPlanGrpList.setCompanyName(rs.getString("COMPANY_NAME"));
				buddyPAPlanGrpList.setCompanyId(rs.getString("COMPANY_ID"));
				buddyPAPlanGrpList.setPlanId(rs.getString("PLAN_ID"));
				buddyPAPlanGrpList.setPlanName(rs.getString("PLAN_NAME"));
				buddyPAPlanGrpList.setEnable(rs.getString("PLAN_ENABLE"));

				detail.add(buddyPAPlanGrpList);

			}
			cstmt.close();
			return detail;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	@Override
	public List<BuddyPAPlan> getPlanList(String companyId, String comboId) {

		try {

			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_BUDDY_SP_TBL_PLAN_LIST(?,?,?)}");

			cstmt.setString(1, companyId);
			cstmt.setString(2, comboId);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			List<BuddyPAPlan> detail = new ArrayList<BuddyPAPlan>();

			rs = (ResultSet) cstmt.getObject(3);
			while (rs.next()) {

				BuddyPAPlan buddyPAPlan = new BuddyPAPlan();
				buddyPAPlan.setPlanId(rs.getString("Plan_Id"));
				buddyPAPlan.setNameEn(rs.getString("Plan_Name"));

				detail.add(buddyPAPlan);

			}

			cstmt.close();
			return detail;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	@Override
	public List<BuddyPABenefit> getPremiumList(String companyId) {

		try {

			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_BUDDY_SP_TBL_BENEFIT_PREMIUM_CLASS(?,?)}");

			cstmt.setString(1, companyId);
			cstmt.registerOutParameter(2, OracleTypes.CURSOR);
			cstmt.execute();
			List<BuddyPABenefit> detail = new ArrayList<BuddyPABenefit>();

			rs = (ResultSet) cstmt.getObject(2);
			while (rs.next()) {

				BuddyPABenefit buddyPABenefit = new BuddyPABenefit();
				buddyPABenefit.setPremiumClass(rs.getObject("Premium_Class"));
				buddyPABenefit.setCompanyId(rs.getObject("Company_Id"));

				detail.add(buddyPABenefit);

			}

			cstmt.close();
			return detail;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	@Override
	public List<BuddyPABenefit> getBenefitList(String companyId, String comboId, String planId) {

		try {

			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_BUDDY_SP_TBL_BENEFIT_LIST(?,?,?,?)}");

			cstmt.setString(1, companyId);
			cstmt.setString(2, comboId);
			cstmt.setString(3, planId);
			cstmt.registerOutParameter(4, OracleTypes.CURSOR);
			cstmt.execute();
			List<BuddyPABenefit> detail = new ArrayList<BuddyPABenefit>();

			rs = (ResultSet) cstmt.getObject(4);
			while (rs.next()) {

				BuddyPABenefit buddyPABenefit = new BuddyPABenefit();
				buddyPABenefit.setBenefitId(Integer.parseInt(rs.getObject("Benefit_Id").toString(), 10));
				buddyPABenefit.setNameEn(rs.getObject("Benefit_Name"));
				buddyPABenefit.setEnable(rs.getObject("Benefit_Enable"));

				detail.add(buddyPABenefit);

			}

			cstmt.close();
			return detail;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	@Override
	public List<BuddyPABenefitGrpList> getBenefitGrpList(String companyId, String comboId) {
		try {

			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_BUDDY_SP_TBL_BENEFIT_GRP(?,?,?)}");

			cstmt.setString(1, companyId);
			cstmt.setString(2, comboId);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			List<BuddyPABenefitGrpList> detail = new ArrayList<BuddyPABenefitGrpList>();

			rs = (ResultSet) cstmt.getObject(3);
			while (rs.next()) {

				BuddyPABenefitGrpList buddyPABenefit = new BuddyPABenefitGrpList();
				buddyPABenefit.setCompanyId(rs.getObject("Company_Id"));
				buddyPABenefit.setComboId(rs.getObject("Combo_Id"));
				buddyPABenefit.setComboName(rs.getObject("Combo_Name"));
				buddyPABenefit.setPlanId(rs.getObject("Plan_Id"));
				buddyPABenefit.setPlanName(rs.getObject("Plan_Name"));
				buddyPABenefit.setBenefitId(Integer.parseInt(rs.getObject("Benefit_Id").toString(), 10));
				buddyPABenefit.setBenefitName(rs.getObject("Benefit_Name"));
				buddyPABenefit.setAdultSumInsured(rs.getBigDecimal("Adult_Sum_Insured"));
				buddyPABenefit.setChildSumInsured(rs.getBigDecimal("Child_Sum_Insured"));
				buddyPABenefit.setAdultLoading(rs.getBigDecimal("Adult_Loading"));
				buddyPABenefit.setChildLoading(rs.getBigDecimal("Child_Loading"));
				buddyPABenefit.setAdultPremium(rs.getBigDecimal("Adult_Premium"));
				buddyPABenefit.setChildPremium(rs.getBigDecimal("Child_Premium"));
				buddyPABenefit.setEnable(rs.getObject("Benefit_Enable"));
				buddyPABenefit.setVersion((short) Integer.parseInt(rs.getObject("Version").toString()));

				detail.add(buddyPABenefit);

			}
			cstmt.close();
			return detail;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	@Override
	public List<BuddyPABenefitGrpList> getEditBenefitGrpList(String companyId, String comboId, String planId) {
		try {

			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_BUDDY_SP_TBL_BENEFIT_PLAN(?,?,?,?)}");

			cstmt.setString(1, companyId);
			cstmt.setString(2, comboId);
			cstmt.setString(3, planId);
			cstmt.registerOutParameter(4, OracleTypes.CURSOR);
			cstmt.execute();
			List<BuddyPABenefitGrpList> detail = new ArrayList<BuddyPABenefitGrpList>();

			rs = (ResultSet) cstmt.getObject(4);
			while (rs.next()) {

				BuddyPABenefitGrpList buddyPABenefit = new BuddyPABenefitGrpList();
				buddyPABenefit.setCompanyId(rs.getObject("Company_Id"));
				buddyPABenefit.setComboId(rs.getObject("Combo_Id"));
				buddyPABenefit.setComboName(rs.getObject("Combo_Name"));
				buddyPABenefit.setPlanId(rs.getObject("Plan_Id"));
				buddyPABenefit.setPlanName(rs.getObject("Plan_Name"));
				buddyPABenefit.setBenefitId(Integer.parseInt(rs.getObject("Benefit_Id").toString(), 10));
				buddyPABenefit.setBenefitName(rs.getObject("Benefit_Name"));
				buddyPABenefit.setAdultSumInsured(rs.getBigDecimal("Adult_Sum_Insured"));
				buddyPABenefit.setChildSumInsured(rs.getBigDecimal("Child_Sum_Insured"));
				buddyPABenefit.setAdultLoading(rs.getBigDecimal("Adult_Loading"));
				buddyPABenefit.setChildLoading(rs.getBigDecimal("Child_Loading"));
				buddyPABenefit.setAdultPremium(rs.getBigDecimal("Adult_Premium"));
				buddyPABenefit.setChildPremium(rs.getBigDecimal("Child_Premium"));
				buddyPABenefit.setEnable(rs.getObject("Benefit_Enable"));
				buddyPABenefit.setVersion((short) Integer.parseInt(rs.getObject("Version").toString()));
				detail.add(buddyPABenefit);

			}
			return detail;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	@Override
	public List<BuddyPAPlan> getDistPlanList(String companyId, String comboId) {

		try {

			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_BUDDY_SP_TBL_PLAN_DIST(?,?,?)}");

			cstmt.setString(1, companyId);
			cstmt.setString(2, comboId);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			List<BuddyPAPlan> detail = new ArrayList<BuddyPAPlan>();

			rs = (ResultSet) cstmt.getObject(3);
			while (rs.next()) {

				BuddyPAPlan buddyPAPlan = new BuddyPAPlan();
				buddyPAPlan.setPlanId(rs.getString("Plan_Id"));
				buddyPAPlan.setNameEn(rs.getString("Plan_Name"));

				detail.add(buddyPAPlan);

			}
			return detail;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	@Override
	public String updBenefitGrouping(BuddyPABenefitGrpList benefitGrpItem) {
		String strMsg = null;
		try {

			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_BUDDY_SP_TBL_BGRP_UPD(?,?,?,?,?,?,?,?,?,?)}");

			cstmt.setString(1, benefitGrpItem.getCompanyId().toString());
			cstmt.setString(2, benefitGrpItem.getComboId().toString());
			cstmt.setString(3, benefitGrpItem.getPlanId().toString());
			cstmt.setInt(4, Integer.parseInt(benefitGrpItem.getBenefitId().toString()));
			cstmt.setBigDecimal(5, benefitGrpItem.getAdultSumInsured());
			cstmt.setBigDecimal(6, benefitGrpItem.getChildSumInsured());
			cstmt.setBigDecimal(7, benefitGrpItem.getAdultLoading());
			cstmt.setBigDecimal(8, benefitGrpItem.getChildLoading());
			cstmt.setString(9, benefitGrpItem.getModifiedBy().toString());
			cstmt.setInt(10, benefitGrpItem.getVersion());
			//
			System.out.println(benefitGrpItem.getCompanyId().toString());
			System.out.println(benefitGrpItem.getComboId().toString());
			System.out.println(benefitGrpItem.getPlanId().toString());
			System.out.println("benefit id : " + Integer.parseInt(benefitGrpItem.getBenefitId().toString()));
			System.out.println(benefitGrpItem.getAdultSumInsured());
			System.out.println(benefitGrpItem.getChildSumInsured());
			System.out.println(benefitGrpItem.getAdultLoading());
			System.out.println(benefitGrpItem.getChildLoading());
			System.out.println(benefitGrpItem.getModifiedBy().toString());
			System.out.println(benefitGrpItem.getVersion());
			//
			cstmt.execute();

			return strMsg;
		} catch (SQLException e) {
			e.printStackTrace();
			return strMsg;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	@Override
	public String updBenefitList(BuddyPABenefitGrpList benefitGrpItem) {
		String strMsg = null;
		try {

			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_BUDDY_SP_TBL_BLIST_UPD(?,?,?,?,?,?)}");

			cstmt.setString(1, benefitGrpItem.getCompanyId().toString());
			cstmt.setString(2, benefitGrpItem.getComboId().toString());
			cstmt.setString(3, benefitGrpItem.getPlanId().toString());
			cstmt.setInt(4, Integer.parseInt(benefitGrpItem.getBenefitId().toString()));
			cstmt.setString(5, benefitGrpItem.getEnable().toString());
			cstmt.setString(6, benefitGrpItem.getModifiedBy().toString());
			cstmt.execute();

			return strMsg;
		} catch (SQLException e) {
			e.printStackTrace();
			return strMsg;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	@Override
	public List<BuddyPABenefitGrpList> getPremiumTotal(String companyId, String comboId) {
		try {

			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_BUDDY_SP_TBL_BGRP_TOTAL(?,?,?)}");

			cstmt.setString(1, companyId);
			cstmt.setString(2, comboId);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			List<BuddyPABenefitGrpList> detail = new ArrayList<BuddyPABenefitGrpList>();

			rs = (ResultSet) cstmt.getObject(3);
			while (rs.next()) {

				BuddyPABenefitGrpList buddyPABenefit = new BuddyPABenefitGrpList();
				buddyPABenefit.setCompanyId(rs.getObject("Company_Id"));
				buddyPABenefit.setComboId(rs.getObject("Combo_Id"));
				buddyPABenefit.setPlanId(rs.getObject("Plan_Id"));
				buddyPABenefit.setPlanName(rs.getObject("Plan_Name"));
				buddyPABenefit.setAdultPremium(rs.getBigDecimal("Adult_Premium"));
				buddyPABenefit.setChildPremium(rs.getBigDecimal("Child_Premium"));
				detail.add(buddyPABenefit);

			}
			cstmt.close();
			return detail;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	@Override
	public List<BuddyPABenefit> getBenefitListDist(String companyId, String comboId) {
		try {

			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_BUDDY_SP_TBL_BLIST_DIST(?,?,?)}");

			cstmt.setString(1, companyId);
			cstmt.setString(2, comboId);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			List<BuddyPABenefit> detail = new ArrayList<BuddyPABenefit>();

			rs = (ResultSet) cstmt.getObject(3);
			while (rs.next()) {

				BuddyPABenefit buddyPABenefit = new BuddyPABenefit();
				buddyPABenefit.setCompanyId(rs.getObject("Company_Id"));
				buddyPABenefit.setBenefitId(Integer.parseInt(String.valueOf(rs.getObject("Benefit_Id"))));
				buddyPABenefit.setNameEn(rs.getObject("Benefit_Name"));

				detail.add(buddyPABenefit);

			}
			cstmt.close();
			return detail;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

}