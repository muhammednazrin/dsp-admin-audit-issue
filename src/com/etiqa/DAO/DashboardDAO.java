package com.etiqa.DAO;

public interface DashboardDAO {

	
	public String getTLPerformance(String fromdt);

	public String getWTCPerformance(String fromdt);

	public String getHOHHPerformance(String fromdt);

	public String getMIPerformance(String fromdt);
	
	//public String getCCPerformance(String fromdt);

}
