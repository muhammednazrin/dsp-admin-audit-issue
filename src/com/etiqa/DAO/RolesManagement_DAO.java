package com.etiqa.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.etiqa.admin.Roles;
import com.etiqa.admin.UserRole;
import com.etiqa.common.DB.ConnectionFactory;

public class RolesManagement_DAO {

	// private static DBUtil db1;
	private static Connection connection;

	public List<UserRole> getAllUsersRoles(int Userid) {
		List<UserRole> users = new ArrayList<UserRole>();
		Connection connection = null;
		connection = ConnectionFactory.getConnection();
		ResultSet rs = null;
		Statement statement = null;
		try {
			// db1 = new DBUtil();
			// connection = db1.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
					"Select U.USERNAME,R.USERID,UR.URL,UR.ROLENAME,R.ASSIGNEDBY,R.ASSIGNDATE,R.ISACTIVE,R.ID AS ID from DSPADMIN_USER_ROLE R,DSPADMIN_USERS U,DSPADMIN_ROLES UR where U.ID=R.UserID and UR.ID=R.ROLEID and U.ID=?");
			preparedStatement.setInt(1, Userid);
			rs = preparedStatement.executeQuery();
			while (rs.next()) {
				UserRole muser = new UserRole();
				muser.setID(rs.getInt("ID"));
				muser.setUserName(rs.getString("USERNAME"));
				muser.setRoleName(rs.getString("ROLENAME"));
				muser.setAssignedby(rs.getString("ASSIGNEDBY"));
				muser.setAssignedDate(rs.getString("ASSIGNDATE"));
				muser.setURL(rs.getString("URL"));
				muser.setUserID(rs.getInt("USERID"));
				System.out.println(rs.getInt("USERID"));
				muser.setIsactive(rs.getString("ISACTIVE"));
				users.add(muser);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return users;
	}

	public List<UserRole> getUsersRolesMenu(String Username) {
		List<UserRole> users = new ArrayList<UserRole>();
		Connection connection = null;
		connection = ConnectionFactory.getConnection();
		ResultSet rs = null;
		Statement statement = null;
		try {
			// db1 = new DBUtil();
			// connection = db1.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
					"Select U.USERNAME,URL,UR.ROLENAME,R.ASSIGNEDBY,R.ASSIGNDATE,R.ISACTIVE,R.ID AS ID from DSPADMIN_USER_ROLE R,DSPADMIN_USERS U,DSPADMIN_ROLES UR where U.ID=R.UserID and UR.ID=R.ROLEID and U.USERNAME=?");
			preparedStatement.setString(1, Username);
			rs = preparedStatement.executeQuery();
			while (rs.next()) {
				UserRole muser = new UserRole();
				muser.setID(rs.getInt("ID"));
				muser.setUserName(rs.getString("USERNAME"));
				muser.setRoleName(rs.getString("ROLENAME"));
				muser.setAssignedby(rs.getString("ASSIGNEDBY"));
				muser.setAssignedDate(rs.getString("ASSIGNDATE"));
				muser.setURL(rs.getString("URL"));
				muser.setIsactive(rs.getString("ISACTIVE"));
				users.add(muser);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return users;
	}

	public void updateUser(UserRole muser) {
		Connection connection = null;
		connection = ConnectionFactory.getConnection();
		ResultSet rs = null;
		Statement statement = null;
		try {
			// db1 = new DBUtil();
			// connection = db1.getConnection();
			PreparedStatement preparedStatement = connection
					.prepareStatement("Update DSPADMIN_USER_ROLE set ISACTIVE=? where id=?");
			// Parameters start with 1
			preparedStatement.setString(1, muser.getIsactive());
			preparedStatement.setInt(2, muser.getID());

			System.out.println("ID = " + muser.getID() + " IsActive = " + muser.getIsactive());
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					System.out.println("update Error");
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void addUserRole(UserRole muser) {
		Connection connection = null;
		connection = ConnectionFactory.getConnection();
		ResultSet rs = null;
		Statement statement = null;
		try {
			// db1 = new DBUtil();
			// connection = db1.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
					"insert into DSPADMIN_USER_ROLE(USERID,ROLEID,ASSIGNEDBY,ASSIGNDATE,ISACTIVE) values (?,?,?,?,?)");
			preparedStatement.setInt(1, muser.getUserID());
			preparedStatement.setInt(2, muser.getRoleID());
			preparedStatement.setString(3, muser.getAssignedby());
			preparedStatement.setString(4, muser.getAssignedDate());
			preparedStatement.setString(5, muser.getIsactive());
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public UserRole getuserRoleById(int userid) {
		Connection connection = null;
		connection = ConnectionFactory.getConnection();
		ResultSet rs = null;
		Statement statement = null;
		UserRole muser = new UserRole();
		try {
			// db1 = new DBUtil();
			// connection = db1.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
					"Select U.USERNAME,UR.ROLENAME,R.ASSIGNEDBY,R.ASSIGNDATE,R.ISACTIVE,R.ID from DSPADMIN_USER_ROLE R,DSPADMIN_USERS U,DSPADMIN_ROLES UR where U.ID=R.UserID and UR.ID=R.ROLEID and R.Id=?");
			preparedStatement.setInt(1, userid);
			System.out.println(userid);
			rs = preparedStatement.executeQuery();

			if (rs.next()) {
				muser.setID(rs.getInt("ID"));
				muser.setUserName(rs.getString("USERNAME"));
				muser.setRoleName(rs.getString("ROLENAME"));
				muser.setAssignedby(rs.getString("ASSIGNEDBY"));
				muser.setAssignedDate(rs.getString("ASSIGNDATE"));
				muser.setIsactive(rs.getString("ISACTIVE"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return muser;
	}

	public Roles getAllRole() {
		Connection connection = null;
		connection = ConnectionFactory.getConnection();
		ResultSet rs = null;
		Statement statement = null;
		Roles muser = new Roles();
		try {
			// db1 = new DBUtil();
			// connection = db1.getConnection();
			PreparedStatement preparedStatement = connection
					.prepareStatement("Select ID,ROLENAME from DSPADMIN_ROLES Where isactive=1");
			rs = preparedStatement.executeQuery();

			if (rs.next()) {
				muser.setRoleID(rs.getInt("ID"));
				muser.setRoleName(rs.getString("ROLENAME"));

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return muser;
	}

	public static void main(String args[]) {
		RolesManagement_DAO rdao = new RolesManagement_DAO();
		List<UserRole> releaseDataList = rdao.getUsersRolesMenu("faheem");
		Iterator itr = releaseDataList.iterator();
		while (itr.hasNext()) {
			UserRole rd = (UserRole) itr.next();
			
			
		}
	}

}
