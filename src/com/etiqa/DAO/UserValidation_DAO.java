package com.etiqa.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.etiqa.admin.User;
import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.common.DB.DBUtil;
import com.etiqa.dsp.dao.common.pojo.RegisterEmailVo;
import com.etiqa.ldap.EmailCPF;
import com.spring.uam.ldap.LDAPAttributesBean;

import oracle.jdbc.OracleTypes;

public class UserValidation_DAO {

	// private static DBUtil db1;
	// private static Connection connection;

	public static User getvalidate(String username) {
		Connection connection = null;
		connection = ConnectionFactory.getConnection();

		ResultSet rs = null;
		Statement statement = null;
		User muser = new User();
		try {
			// db1=new DBUtil();
			// connection = db1.getConnection();
			PreparedStatement preparedStatement = connection
					.prepareStatement("select * from DSPADMIN_USERS where username=? and ISACTIVE=1");
			preparedStatement.setString(1, username);
			rs = preparedStatement.executeQuery();

			if (rs.next()) {
				muser.setIsVarified(1);
				muser.setFullName(rs.getString("FULLNAME"));
				muser.setUserRole(rs.getString("ROLE"));

			} else {
				muser.setIsVarified(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return muser;
	}

	public static RegisterEmailVo emailCallingForRegister(RegisterEmailVo registerEmailVo) {
		Connection connection = null;

		ResultSet rs = null;
		try {

			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{call EMAIL_TEMPLATE_RETRIVE(?,?)}");

			cstmt.setString(1, registerEmailVo.getTemplateName());
			cstmt.registerOutParameter(2, OracleTypes.CURSOR);

			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(2);

			DateFormat dateFormat = new SimpleDateFormat("MMM dd,yyyy 'at' HH:mm:ss a");
			Date date = new Date();
			String todate = dateFormat.format(date);
			// print the results

			if (rs != null) {

				registerEmailVo.setError_code("0000");
				registerEmailVo.setError_msg("Succeed");

			} else {

				registerEmailVo.setError_code("0001");
				registerEmailVo.setError_msg("Failed");
			}
			String emailContent = "";
			String emailStatus = "";

			while (rs.next()) {

				registerEmailVo.setEmailSubject(rs.getString(3));
				emailContent = rs.getString(2);
				emailContent = emailContent.replace("[[UserName]]", registerEmailVo.getUserName());
				emailContent = emailContent.replace("[[UserNric]]", registerEmailVo.getUserName());
				emailContent = emailContent.replace("[[link]]", registerEmailVo.getEmailLink());
				emailContent = emailContent.replace("[[UserPswd]]", registerEmailVo.getUserPswd());
				// registerEmailVo.setTemplateDesc(rs.getString(2));
				registerEmailVo.setTemplateEmail(emailContent);
				EmailCPF emailCPF = new EmailCPF();

				emailStatus = emailCPF.generateAndSendEmail(registerEmailVo);
				// emailStatus = emailCPF.generateAndSendEmail(registerEmailVo);

				registerEmailVo.setEmailStatus(emailStatus);

			}
			if (emailStatus == "Done") {
				registerEmailVo.setError_code("0000");
				registerEmailVo.setError_msg("Succeed");

			} else {

				registerEmailVo.setError_code("0001");
				registerEmailVo.setError_msg("Failed to send email");

			}

		} catch (Exception e) {
			// a failure occurred log message;
			System.out.println("[ERROR]" + new Date() + "Exception " + e.getMessage() + " " + e.getCause());
			e.printStackTrace();
		} finally {
			// cstmt.close();
			try {
				if (connection != null) {
					connection.close();
				}
				DBUtil.close(connection);
				DBUtil.close(rs);
				// DBUtil.close(statement);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			connection = null;
			try {
				if (rs != null) {
					rs.close();
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return registerEmailVo;
	}

	// Password Validation

	public static LDAPAttributesBean queryDatabasePwValidation(LDAPAttributesBean pwInfo) {
		Connection connection = null;

		ResultSet rs = null;
		System.out.println("user Password " + pwInfo.getPassword());
		try {

			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{call PASSWORD_POLICY_INFO(?)}");

			cstmt.registerOutParameter(1, OracleTypes.CURSOR);

			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(1);
			String DisplayError = "";

			// print the results
			int chk = printPasswordStats(pwInfo.getPassword(), pwInfo.getUserrole());

			while (rs.next()) {

				if (rs.getString(2).contains("PL")) {
					int pwlength = Integer.parseInt(rs.getString(6));
					if (pwInfo.getPassword().length() < pwlength) {

						pwInfo.setError_code("D0015");
						DisplayError = DisplayError.concat(
								"Your password length must be 8 To 12 characters. Please change the password\n");

					}
				}
				if (rs.getString(2).contains("PID") && rs.getString(6).contains("true")) {
					if (pwInfo.getUsername().equalsIgnoreCase(pwInfo.getPassword())) {
						pwInfo.setError_code("D0016");
						DisplayError = DisplayError.concat("The Password and ID Must Not be the same<br>");
					}
				}

				if (rs.getString(2).contains("PSAI") && rs.getString(6).contains("true")
						&& (pwInfo.getUserrole() == 1 || pwInfo.getUserrole() == 2)) {
					if (chk == 0) {
						pwInfo.setError_code("D0017");
						DisplayError = DisplayError.concat(
								"The password must be at least 1 number, 1 special character, 1 upper and 1 lower case alphabets\n");
					}
				}

				if (rs.getString(2).contains("PSE") && rs.getString(6).contains("true") && pwInfo.getUserrole() == 3) {
					if (chk == 0) {
						pwInfo.setError_code("D0017");
						DisplayError = DisplayError
								.concat("The password must be at least 1 number, 1 upper and 1 lower case alphabets\n");
					}
				}

			}
			if (DisplayError != "") {

				// pwInfo.setError_code("D0018");
				pwInfo.setError_msg(DisplayError);

			} else {
				pwInfo.setError_code("D0000");
				pwInfo.setError_msg("success");

			}

		} catch (Exception e) {
			// a failure occurred log message;
			System.out.println("[ERROR]" + new Date() + "Exception " + e.getMessage() + " " + e.getCause());
			pwInfo.setError_code("D9999");
			pwInfo.setError_msg(e.getMessage());
			e.printStackTrace();
		} finally {
			// cstmt.close();
			try {
				if (connection != null) {
					connection.close();
				}
				DBUtil.close(connection);
				DBUtil.close(rs);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			connection = null;
			try {
				if (rs != null) {
					rs.close();
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return pwInfo;
	}

	// Password checking with Alphanumeric characters
	public static int printPasswordStats(String password, int userrole) {
		int numOfSpecial = 0;
		int numOfLetters = 0;
		int numOfUpperLetters = 0;
		int numOfLowerLetters = 0;
		int numOfDigits = 0;
		int valcheck = 0;
		Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(password);
		boolean b = m.find();

		byte[] bytes = password.getBytes();
		for (byte tempByte : bytes) {

			if (tempByte >= 33 && tempByte <= 47) {
				numOfSpecial++;
			}

			char tempChar = (char) tempByte;

			if (Character.isDigit(tempChar)) {
				numOfDigits++;
			}

			if (Character.isLetter(tempChar)) {
				numOfLetters++;
			}

			if (Character.isUpperCase(tempChar)) {
				numOfUpperLetters++;
			}

			if (Character.isLowerCase(tempChar)) {
				numOfLowerLetters++;
			}
		}
		System.out.println(b);
		if (userrole == 1 || userrole == 2) {
			if (numOfLetters > 0 && numOfUpperLetters > 0 && numOfLowerLetters > 0 && numOfDigits > 0) {
				if (b) {
					valcheck = 1;
				}
			}
		} else {
			if (numOfLetters > 0 && numOfUpperLetters > 0 && numOfLowerLetters > 0 && numOfDigits > 0) {
				valcheck = 1;
			}
		}

		return valcheck;
	}

	public static void main(String args[]) {
		UserValidation_DAO vdao = new UserValidation_DAO();
		User muser = new User();
		muser = UserValidation_DAO.getvalidate("000777");

		System.out.println(muser.getFullName());

	}

	// Added By Pramaiyan
	public static User getvalidateByUserID(String userId) {
		Connection connection = null;
		connection = ConnectionFactory.getConnection();

		ResultSet rs = null;
		Statement statement = null;
		User muser = new User();
		try {
			// db1=new DBUtil();
			// connection = db1.getConnection();
			PreparedStatement preparedStatement = connection
					.prepareStatement("select * from dsp_adm_tbl_user where ID=? and status=1");
			preparedStatement.setString(1, userId);
			rs = preparedStatement.executeQuery();

			if (rs.next()) {
				muser.setIsVarified(1);
				muser.setFullName(rs.getString("NAME"));
				muser.setID(rs.getInt("ID"));
				muser.setUsername(rs.getString("PF_NUMBER"));
			} else {
				muser.setIsVarified(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return muser;
	}

}
