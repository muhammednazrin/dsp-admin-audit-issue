package com.etiqa.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.etiqa.common.DB.ConnectionFactory;
import com.spring.VO.AbandonmentBean;
import com.spring.VO.TotalSalesBean;

import oracle.jdbc.OracleTypes;

public class AbandonmentDAO {

	private static ConnectionFactory db1;
	private static Connection connection;

	public List<AbandonmentBean> getReportHeader(String fromdt, String todt, String product) {
		List<AbandonmentBean> myDataList = new ArrayList<AbandonmentBean>();

		System.out.println("fromdate DAO : " + fromdt);
		System.out.println("todate DAO : " + todt);
		System.out.println("product DAO : " + product);

		AbandonmentBean reportData = new AbandonmentBean();
		reportData.setProduct_indicator(product);
		myDataList.add(reportData);

		return myDataList;

	}

	public List<AbandonmentBean> getReportData(String fromdt, String todt, String product) {
		List<AbandonmentBean> myDataList = new ArrayList<AbandonmentBean>();

		System.out.println("fromdate DAO : " + fromdt);
		System.out.println("todate DAO : " + todt);
		System.out.println("product DAO : " + product);

		ResultSet resultSet = null;
		Statement statement = null;

		try {

			connection = ConnectionFactory.getConnection();

			String selectType = product;

			CallableStatement cstmt = null;

			if (selectType.equals("Motor Insurance")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_MI(?,?,?)}");
			} else if (selectType.equals("Motor Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_MT(?,?,?)}");
			} else if (selectType.equals("Easy Life")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_EZ(?,?,?)}");
			} else if (selectType.equals("World Travel Care")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_WTC(?,?,?)}");
			} else if (selectType.equals("World Travel Care Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_WTCT(?,?,?)}");
			} else if (selectType.equals("House Owner House Hold")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_HOHH(?,?,?)}");
			} else if (selectType.equals("House Owner House Hold Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_HOHHT(?,?,?)}");
			} else if (selectType.equals("ALL")) {
				cstmt = connection.prepareCall("{call DSP_ADM_AbandonmentDashBoard(?,?,?)}");
			} else {
				cstmt = connection.prepareCall("{call DSP_ADM_AbandonmentDashBoard(?,?,?)}");
			}

			cstmt.setString(1, fromdt);
			cstmt.setString(2, todt);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);

			cstmt.execute();

			resultSet = (ResultSet) cstmt.getObject(3);

			while (resultSet.next()) {

				AbandonmentBean reportData = new AbandonmentBean();

				reportData.setTransactiondate(resultSet.getString("QUOTATION_CREATION_DATETIME"));
				reportData.setAbandonementdate(resultSet.getString("AbandonmentDate"));
				reportData.setProduct_indicator(resultSet.getString("ProductName"));

				double totalcount = Double.parseDouble(resultSet.getString("TotalCount"));
				reportData.setTotal_count(String.format("%,.0f", totalcount));

				double totalcountsuccess = Double.parseDouble(resultSet.getString("TotalCountSuccess"));
				reportData.setTotal_count_success(String.format("%,.0f", totalcountsuccess));

				double money = Double.parseDouble(resultSet.getString("TotalAmountRM"));
				reportData.setTotal_month_rm(String.format("%,.2f", money));

				double p_value = Double.parseDouble(resultSet.getString("PercentSuccess"));
				reportData.setPercentsuccess(String.format("%,.2f", p_value));

				myDataList.add(reportData);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return myDataList;

	}

	public List<AbandonmentBean> getReportDataMonthly(String fromdt, String todt, String product) {
		List<AbandonmentBean> myDataList = new ArrayList<AbandonmentBean>();

		ResultSet resultSet = null;
		Statement statement = null;

		System.out.println("fromdate DAO Monthly : " + fromdt);
		System.out.println("todate DAO Monthly : " + todt);
		System.out.println("product DAO Monthly : " + product);

		try {

			connection = ConnectionFactory.getConnection();

			String selectType = product;

			CallableStatement cstmt = null;

			if (selectType.equals("Motor Insurance")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_MI_M(?,?,?)}");
			} else if (selectType.equals("Motor Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_MT_M(?,?,?)}");
			} else if (selectType.equals("Easy Life")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_EZ_M(?,?,?)}");
			} else if (selectType.equals("World Travel Care")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_WTC_M(?,?,?)}");
			} else if (selectType.equals("World Travel Care Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_WTCT_M(?,?,?)}");
			} else if (selectType.equals("House Owner House Hold")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_HOHH_M(?,?,?)}");
			} else if (selectType.equals("House Owner House Hold Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_HOHT_M(?,?,?)}");
			} else if (selectType.equals("ALL")) {
				cstmt = connection.prepareCall("{call DSP_ADM_AbandonmentDashBoard_M(?,?,?)}");
			} else {
				cstmt = connection.prepareCall("{call DSP_ADM_AbandonmentDashBoard_M(?,?,?)}");
			}

			cstmt.setString(1, fromdt);
			cstmt.setString(2, todt);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);

			cstmt.execute();

			resultSet = (ResultSet) cstmt.getObject(3);

			while (resultSet.next()) {

				AbandonmentBean reportData = new AbandonmentBean();

				reportData.setAbandonementdate(resultSet.getString("AbandonmentDate"));

				double totalcount = Double.parseDouble(resultSet.getString("TotalCount"));
				reportData.setTotal_count(String.format("%,.0f", totalcount));

				double totalcountsuccess = Double.parseDouble(resultSet.getString("TotalCountSuccess"));
				reportData.setTotal_count_success(String.format("%,.0f", totalcountsuccess));

				double money = Double.parseDouble(resultSet.getString("TotalAmountRM"));
				reportData.setTotal_month_rm(String.format("%,.2f", money));

				double p_value = Double.parseDouble(resultSet.getString("PercentSuccess"));
				reportData.setPercentsuccess(String.format("%,.2f", p_value));

				myDataList.add(reportData);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return myDataList;

	}

	public List<AbandonmentBean> getReportDataWeek(String fromdt, String todt, String product) {
		List<AbandonmentBean> myDataList = new ArrayList<AbandonmentBean>();
		List<TotalSalesBean> releaseDataList = gettotalamount(fromdt, todt, product);

		int weekno = 0;
		ResultSet resultSet = null;
		Statement statement = null;

		try {

			connection = ConnectionFactory.getConnection();

			String selectType = product;

			CallableStatement cstmt = null;

			if (selectType.equals("Motor Insurance")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_MI_W(?,?,?)}");
			} else if (selectType.equals("Motor Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_MT_W(?,?,?)}");
			} else if (selectType.equals("Easy Life")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_EZ_W(?,?,?)}");
			} else if (selectType.equals("World Travel Care")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_WTC_W(?,?,?)}");
			} else if (selectType.equals("World Travel Care Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_WTCT_W(?,?,?)}");
			} else if (selectType.equals("House Owner House Hold")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_HOHH_W(?,?,?)}");
			} else if (selectType.equals("House Owner House Hold Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_HOHT_W(?,?,?)}");
			} else if (selectType.equals("ALL")) {
				System.out.println("I am here in else if W");
				cstmt = connection.prepareCall("{call DSP_ADM_AbandonmentDashBoard_W(?,?,?)}");
			} else {
				System.out.println("I am here in else");
				cstmt = connection.prepareCall("{call DSP_ADM_AbandonmentDashBoard_W(?,?,?)}");
			}

			cstmt.setString(1, fromdt);
			cstmt.setString(2, todt);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);

			cstmt.execute();

			resultSet = (ResultSet) cstmt.getObject(3);

			while (resultSet.next()) {

				AbandonmentBean reportData = new AbandonmentBean();

				reportData.setAbandonementdate(resultSet.getString("AbandonmentDate"));

				double totalcount = Double.parseDouble(resultSet.getString("TotalCount"));
				reportData.setTotal_count(String.format("%,.0f", totalcount));

				double totalcountsuccess = Double.parseDouble(resultSet.getString("TotalCountSuccess"));
				reportData.setTotal_count_success(String.format("%,.0f", totalcountsuccess));

				double money = Double.parseDouble(resultSet.getString("TotalAmountRM"));
				reportData.setTotal_month_rm(String.format("%,.2f", money));

				double p_value = Double.parseDouble(resultSet.getString("PercentSuccess"));
				reportData.setPercentsuccess(String.format("%,.2f", p_value));

				weekno = Integer.parseInt(resultSet.getString("AbandonmentDate"));

				for (int i = 0; i < releaseDataList.size(); i++) {
					if (Integer.parseInt(releaseDataList.get(i).getWeek_no()) == weekno) {
						double value = Double.parseDouble(releaseDataList.get(i).getAmount());
						reportData.setTotalamount(String.format("%,.2f", value));
						double policycount = Double.parseDouble(releaseDataList.get(i).getPolicycount());
						reportData.setPolicycount(String.format("%,.0f", policycount));
					}

				}

				Calendar cal = Calendar.getInstance();
				cal.set(Calendar.WEEK_OF_YEAR, weekno);
				// "calculate" the start date of the week
				Calendar first = (Calendar) cal.clone();
				first.add(Calendar.DAY_OF_WEEK, first.getFirstDayOfWeek() - first.get(Calendar.DAY_OF_WEEK) + 1);
				// and add six days to the end date
				Calendar last = (Calendar) first.clone();
				last.add(Calendar.DAY_OF_YEAR, 6);
				// print the result
				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				// System.out.println(df.format(first.getTime()) + " -> " +
				// df.format(last.getTime()));
				reportData.setStart_week(df.format(first.getTime()).toString());
				reportData.setEnd_week(df.format(last.getTime()).toString());

				myDataList.add(reportData);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return myDataList;

	}

	public List<TotalSalesBean> gettotalamount(String fromdt, String todt, String product) {
		List<TotalSalesBean> myDataList = new ArrayList<TotalSalesBean>();

		ResultSet resultSet = null;
		Statement statement = null;

		try {

			connection = ConnectionFactory.getConnection();

			String selectType = product;

			CallableStatement cstmt = null;

			if (selectType.equals("Motor Insurance")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_MI_T(?,?,?)}");
			} else if (selectType.equals("Motor Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_MT_T(?,?,?)}");
			} else if (selectType.equals("Easy Life")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_EZ_T(?,?,?)}");
			} else if (selectType.equals("World Travel Care")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_WTC_T(?,?,?)}");
			} else if (selectType.equals("World Travel Care Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_WTCT_T(?,?,?)}");
			} else if (selectType.equals("House Owner House Hold")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_HOHH_T(?,?,?)}");
			} else if (selectType.equals("House Owner House Hold Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_HOHT_T(?,?,?)}");
			} else if (selectType.equals("ALL")) {
				System.out.println("I am here in else if W");
				cstmt = connection.prepareCall("{call DSP_ADM_AbandonmentDashBoard_T(?,?,?)}");
			} else {
				System.out.println("I am here in else");
				cstmt = connection.prepareCall("{call DSP_ADM_AbandonmentDashBoard_T(?,?,?)}");
			}

			cstmt.setString(1, fromdt);
			cstmt.setString(2, todt);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);

			cstmt.execute();

			resultSet = (ResultSet) cstmt.getObject(3);

			while (resultSet.next()) {

				TotalSalesBean b = new TotalSalesBean();

				String amount = resultSet.getString("TotalAmount");
				String week_no = resultSet.getString("Transaction_Date");
				b.setWeek_no(week_no);
				b.setAmount(amount);
				b.setPolicycount(resultSet.getString("policycount"));
				myDataList.add(b);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return myDataList;

	}

	public List<TotalSalesBean> gettotalamountdaily(String fromdt, String todt, String product) {
		List<TotalSalesBean> myDataList = new ArrayList<TotalSalesBean>();

		ResultSet resultSet = null;
		Statement statement = null;

		try {

			connection = ConnectionFactory.getConnection();

			String selectType = product;

			CallableStatement cstmt = null;

			/*
			 * if (selectType.equals("Motor Insurance")) { cstmt =
			 * connection.prepareCall("{call DSP_ADM_Aband_DashBoard_MI_T(?,?,?)}"); } else
			 * if (selectType.equals("Motor Takaful")) { cstmt =
			 * connection.prepareCall("{call DSP_ADM_Aband_DashBoard_MT_T(?,?,?)}"); } else
			 * if (selectType.equals("Easy Life")) { cstmt =
			 * connection.prepareCall("{call DSP_ADM_Aband_DashBoard_EZ_T(?,?,?)}"); } else
			 * if (selectType.equals("World Travel Care")) { cstmt =
			 * connection.prepareCall("{call DSP_ADM_Aband_DashBoard_WTC_T(?,?,?)}"); } else
			 * if (selectType.equals("World Travel Care Takaful")) { cstmt =
			 * connection.prepareCall("{call DSP_ADM_Aband_DashBoard_WTCT_T(?,?,?)}"); }
			 * else if (selectType.equals("House Owner House Hold")) { cstmt =
			 * connection.prepareCall("{call DSP_ADM_Aband_DashBoard_HOHH_T(?,?,?)}"); }
			 * else if (selectType.equals("House Owner House Hold Takaful")) { cstmt =
			 * connection.prepareCall("{call DSP_ADM_Aband_DashBoard_HOHT_T(?,?,?)}"); }
			 * else if (selectType.equals("ALL")) {
			 * System.out.println("I am here in else if W"); cstmt =
			 * connection.prepareCall("{call DSP_ADM_AbandonmentDB_Count_D(?,?,?)}"); } else
			 * { System.out.println("I am here in else");
			 */
			cstmt = connection.prepareCall("{call DSP_ADM_AbandonmentDB_Count_D(?,?,?)}");
			// }

			cstmt.setString(1, fromdt);
			cstmt.setString(2, todt);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);

			cstmt.execute();

			resultSet = (ResultSet) cstmt.getObject(3);

			while (resultSet.next()) {

				TotalSalesBean b = new TotalSalesBean();

				String amount = resultSet.getString("TotalAmount");
				String week_no = resultSet.getString("Transaction_Date");
				b.setWeek_no(week_no);
				b.setAmount(amount);
				b.setPolicycount(resultSet.getString("policycount"));
				myDataList.add(b);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return myDataList;

	}

	public List<TotalSalesBean> gettotalamountmonthly(String fromdt, String todt, String product) {
		List<TotalSalesBean> myDataList = new ArrayList<TotalSalesBean>();

		ResultSet resultSet = null;
		Statement statement = null;

		try {

			connection = ConnectionFactory.getConnection();

			String selectType = product;

			CallableStatement cstmt = null;

			/*
			 * if (selectType.equals("Motor Insurance")) { cstmt =
			 * connection.prepareCall("{call DSP_ADM_Aband_DashBoard_MI_T(?,?,?)}"); } else
			 * if (selectType.equals("Motor Takaful")) { cstmt =
			 * connection.prepareCall("{call DSP_ADM_Aband_DashBoard_MT_T(?,?,?)}"); } else
			 * if (selectType.equals("Easy Life")) { cstmt =
			 * connection.prepareCall("{call DSP_ADM_Aband_DashBoard_EZ_T(?,?,?)}"); } else
			 * if (selectType.equals("World Travel Care")) { cstmt =
			 * connection.prepareCall("{call DSP_ADM_Aband_DashBoard_WTC_T(?,?,?)}"); } else
			 * if (selectType.equals("World Travel Care Takaful")) { cstmt =
			 * connection.prepareCall("{call DSP_ADM_Aband_DashBoard_WTCT_T(?,?,?)}"); }
			 * else if (selectType.equals("House Owner House Hold")) { cstmt =
			 * connection.prepareCall("{call DSP_ADM_Aband_DashBoard_HOHH_T(?,?,?)}"); }
			 * else if (selectType.equals("House Owner House Hold Takaful")) { cstmt =
			 * connection.prepareCall("{call DSP_ADM_Aband_DashBoard_HOHT_T(?,?,?)}"); }
			 * else if (selectType.equals("ALL")) {
			 * System.out.println("I am here in else if W"); cstmt =
			 * connection.prepareCall("{call DSP_ADM_AbandonmentDB_Count_M(?,?,?)}"); } else
			 * { System.out.println("I am here in else");
			 */
			cstmt = connection.prepareCall("{call DSP_ADM_AbandonmentDB_Count_M(?,?,?)}");
			/* } */

			cstmt.setString(1, fromdt);
			cstmt.setString(2, todt);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);

			cstmt.execute();

			resultSet = (ResultSet) cstmt.getObject(3);

			while (resultSet.next()) {

				TotalSalesBean b = new TotalSalesBean();

				String amount = resultSet.getString("TotalAmount");
				String week_no = resultSet.getString("Transaction_Date");
				b.setWeek_no(week_no);
				b.setAmount(amount);
				b.setPolicycount(resultSet.getString("policycount"));
				myDataList.add(b);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return myDataList;

	}

	public String getReportDataGraph(String fromdt, String todt, String product) {
		List myDataList = new ArrayList<AbandonmentBean>();

		ResultSet resultSet = null;
		Statement statement = null;
		String resp = "";

		try {

			connection = ConnectionFactory.getConnection();

			String selectType = product;

			CallableStatement cstmt = null;

			if (selectType.equals("Motor Insurance")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_G_MI(?,?,?)}");
			} else if (selectType.equals("Motor Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_G_MT(?,?,?)}");
			} else if (selectType.equals("Easy Life")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_G_EZ(?,?,?)}");
			} else if (selectType.equals("World Travel Care")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_G_WTC(?,?,?)}");
			} else if (selectType.equals("World Travel Care Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_G_WTCT(?,?,?)}");
			} else if (selectType.equals("House Owner House Hold")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_G_HOHH(?,?,?)}");
			} else if (selectType.equals("House Owner House Hold Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_G_HOHT(?,?,?)}");
			} else if (selectType.equals("ALL")) {
				cstmt = connection.prepareCall("{call DSP_ADM_AbandonmentDashBoard_G(?,?,?)}");
			} else {
				cstmt = connection.prepareCall("{call DSP_ADM_AbandonmentDashBoard_G(?,?,?)}");
			}

			// CallableStatement cstmt = connection.prepareCall("{call
			// DSP_ADM_AbandonmentDashBoard_G(?,?,?)}");
			cstmt.setString(1, fromdt);
			cstmt.setString(2, todt);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);

			cstmt.execute();
			// resp = "{\"resp\":[";
			resultSet = (ResultSet) cstmt.getObject(3);

			while (resultSet.next()) {

				AbandonmentBean reportData = new AbandonmentBean();

				int strweek = Integer.parseInt(resultSet.getString("AbandonmentDate"));

				resp = resp + "{ x: " + strweek + ", y:" + resultSet.getInt("TotalCountSuccess") + "},";

			}

			resp = resp.substring(0, resp.length() - 1);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return resp;

	}

	public String getReportDataGraphHistorgam(String fromdt, String todt, String product) {
		List myDataList = new ArrayList<AbandonmentBean>();

		ResultSet resultSet = null;
		Statement statement = null;
		String resp = "";

		try {

			connection = ConnectionFactory.getConnection();

			String selectType = product;

			CallableStatement cstmt = null;

			if (selectType.equals("Motor Insurance")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_G_MI(?,?,?)}");
			} else if (selectType.equals("Motor Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_G_MT(?,?,?)}");
			} else if (selectType.equals("Easy Life")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_G_EZ(?,?,?)}");
			} else if (selectType.equals("World Travel Care")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_G_WTC(?,?,?)}");
			} else if (selectType.equals("World Travel Care Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_G_WTCT(?,?,?)}");
			} else if (selectType.equals("House Owner House Hold")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_G_HOHH(?,?,?)}");
			} else if (selectType.equals("House Owner House Hold Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_G_HOHT(?,?,?)}");
			} else if (selectType.equals("ALL")) {
				cstmt = connection.prepareCall("{call DSP_ADM_AbandonmentDashBoard_G(?,?,?)}");
			} else {
				cstmt = connection.prepareCall("{call DSP_ADM_AbandonmentDashBoard_G(?,?,?)}");
			}

			// CallableStatement cstmt = connection.prepareCall("{call
			// DSP_ADM_AbandonmentDashBoard_G(?,?,?)}");
			cstmt.setString(1, fromdt);
			cstmt.setString(2, todt);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);

			cstmt.execute();
			// resp = "{\"resp\":[";
			resultSet = (ResultSet) cstmt.getObject(3);

			while (resultSet.next()) {

				AbandonmentBean reportData = new AbandonmentBean();

				int strweek = Integer.parseInt(resultSet.getString("AbandonmentDate"));

				resp = resp + "{ x: " + strweek + ", y:" + resultSet.getInt("TotalCount") + "},";

			}

			resp = resp.substring(0, resp.length() - 1);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return resp;

	}

	public String getReportDataGraphRM(String fromdt, String todt, String product) {
		List myDataList = new ArrayList<AbandonmentBean>();

		ResultSet resultSet = null;
		Statement statement = null;
		String resp = "";

		try {

			connection = ConnectionFactory.getConnection();

			String selectType = product;

			CallableStatement cstmt = null;

			if (selectType.equals("Motor Insurance")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_G_MI(?,?,?)}");
			} else if (selectType.equals("Motor Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_G_MT(?,?,?)}");
			} else if (selectType.equals("Easy Life")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_G_EZ(?,?,?)}");
			} else if (selectType.equals("World Travel Care")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_G_WTC(?,?,?)}");
			} else if (selectType.equals("World Travel Care Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_G_WTCT(?,?,?)}");
			} else if (selectType.equals("House Owner House Hold")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_G_HOHH(?,?,?)}");
			} else if (selectType.equals("House Owner House Hold Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_G_HOHT(?,?,?)}");
			} else if (selectType.equals("ALL")) {
				cstmt = connection.prepareCall("{call DSP_ADM_AbandonmentDashBoard_G(?,?,?)}");
			} else {
				cstmt = connection.prepareCall("{call DSP_ADM_AbandonmentDashBoard_G(?,?,?)}");
			}

			// CallableStatement cstmt = connection.prepareCall("{call
			// DSP_ADM_AbandonmentDashBoard_G(?,?,?)}");
			cstmt.setString(1, fromdt);
			cstmt.setString(2, todt);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);

			cstmt.execute();
			// resp = "{\"resp\":[";
			resultSet = (ResultSet) cstmt.getObject(3);

			while (resultSet.next()) {

				AbandonmentBean reportData = new AbandonmentBean();
				int strweek = Integer.parseInt(resultSet.getString("AbandonmentDate"));

				resp = resp + "{ x:" + strweek + ", y:" + resultSet.getDouble("TotalAmountRM") + "},";

			}

			resp = resp.substring(0, resp.length() - 1);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return resp;

	}

	public String getReportDataGraphT(String fromdt, String todt, String product) {
		List myDataList = new ArrayList<AbandonmentBean>();

		ResultSet resultSet = null;
		Statement statement = null;
		String resp = "";

		try {

			connection = ConnectionFactory.getConnection();

			String selectType = product;

			CallableStatement cstmt = null;

			if (selectType.equals("Motor Insurance")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_T_MI(?,?,?)}");
			} else if (selectType.equals("Motor Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_T_MT(?,?,?)}");
			} else if (selectType.equals("Easy Life")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_T_EZ(?,?,?)}");
			} else if (selectType.equals("World Travel Care")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_T_WTC(?,?,?)}");
			} else if (selectType.equals("World Travel Care Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_T_WTCT(?,?,?)}");
			} else if (selectType.equals("House Owner House Hold")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_T_HOHH(?,?,?)}");
			} else if (selectType.equals("House Owner House Hold Takaful")) {
				cstmt = connection.prepareCall("{call DSP_ADM_Aband_DashBoard_T_HOHT(?,?,?)}");
			} else if (selectType.equals("ALL")) {
				cstmt = connection.prepareCall("{call DSP_ADM_AbandonmentDashBoard_T(?,?,?)}");
			} else {
				cstmt = connection.prepareCall("{call DSP_ADM_AbandonmentDashBoard_T(?,?,?)}");
			}

			// CallableStatement cstmt = connection.prepareCall("{call
			// DSP_ADM_AbandonmentDashBoard_T(?,?,?)}");
			cstmt.setString(1, fromdt);
			cstmt.setString(2, todt);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);

			cstmt.execute();
			resultSet = (ResultSet) cstmt.getObject(3);

			while (resultSet.next()) {

				AbandonmentBean reportData = new AbandonmentBean();

				int strweek = Integer.parseInt(resultSet.getString("Transaction_Date"));

				resp = resp + "{ x: " + strweek + ", y:" + resultSet.getDouble("TotalAmount") + "},";
			}

			resp = resp.substring(0, resp.length() - 1);

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error : " + e.getMessage());
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return resp;

	}

	public static void main(String args[]) {

		/*
		 * AbandonmentDAO rdao = new AbandonmentDAO();
		 *
		 * String releaseDataList = rdao.getReportDataGraphT("16/05/2017",
		 * "01/08/2019","");
		 *
		 * System.out.println(releaseDataList);
		 */

		/*
		 * Iterator itr = releaseDataList.iterator(); System.out.println(" Test");
		 *
		 * while(itr.hasNext()) { System.out.println(" Test1");
		 *
		 * AbandonmentBean rd = (AbandonmentBean) itr.next();
		 * System.out.println("getAbandonementdate: " + rd.getAbandonementdate());
		 * System.out.println("getTotal_count :  " + rd.getTotal_count());
		 * System.out.println("getStart_week :  " + rd.getStart_week());
		 * System.out.println("getEnd_week :  " + rd.getEnd_week()); }
		 */

		/*
		 * Calendar cal = Calendar.getInstance(); //cal.set(2017, 07 - 1, 26);
		 *
		 * cal.set(Calendar.WEEK_OF_YEAR, 30); // "calculate" the start date of the week
		 * Calendar first = (Calendar) cal.clone(); first.add(Calendar.DAY_OF_WEEK,
		 * first.getFirstDayOfWeek() - first.get(Calendar.DAY_OF_WEEK));
		 *
		 * // and add six days to the end date Calendar last = (Calendar) first.clone();
		 * last.add(Calendar.DAY_OF_YEAR, 6);
		 *
		 * // print the result SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 * System.out.println(df.format(first.getTime()) + " -> " +
		 * df.format(last.getTime()));
		 */

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.WEEK_OF_YEAR, 34);
		// "calculate" the start date of the week
		Calendar first = (Calendar) cal.clone();
		first.add(Calendar.DAY_OF_WEEK, first.getFirstDayOfWeek() - first.get(Calendar.DAY_OF_WEEK) + 1);
		// and add six days to the end date
		Calendar last = (Calendar) first.clone();
		last.add(Calendar.DAY_OF_YEAR, 6);
		// print the result
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		System.out.println(df.format(first.getTime()) + " -> " + df.format(last.getTime()));

	}

}
