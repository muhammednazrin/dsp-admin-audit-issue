package com.etiqa.DAO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.common.DB.DBUtil;
import com.etiqa.model.report.SalesLeads;
import com.etiqa.model.report.TransactionalReport;
import com.spring.admin.AgentController;

import oracle.jdbc.OracleTypes;

public class ReportDAOImpl implements ReportDAO {
	final static Logger logger = Logger.getLogger(ReportDAO.class);

	Connection connection = ConnectionFactory.getConnection();
	Statement statement = null;
	ResultSet rs = null;
	SimpleDateFormat ft = new SimpleDateFormat("dd/mm/yyyy");

	SimpleDateFormat ft2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	// 2017-01-20 14:30:01

	DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	// 20/01/2017 16:30:58
	// DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa");

	//////////////////////////// START: ADD ON 1 MARCH
	//////////////////////////// 2017/////////////////////////////////////////////

	@Override
	// query result for motor total info for export excel
	public List<TransactionalReport> getMotorRecord(String productEntity, String status, String policyCertificateNo,
			String dateFrom, String dateTo, String NRIC, String receiptNo, String plateNo, String agentCode) {
logger.info(productEntity+"productEntity");
logger.info(status+"status");
logger.info(policyCertificateNo+"policyCertificateNo");
logger.info(dateFrom+"dateFrom");
logger.info(dateTo+"dateTo");
logger.info(NRIC+"NRIC");
logger.info(receiptNo+"receiptNo");
logger.info(plateNo+"plateNo");
logger.info(agentCode+"agentCode");
		try {

			int i = 1;
			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_MI_ALL_TMP(?,?,?,?,?,?,?,?,?,?)}");
			cstmt.setString(1, productEntity.trim());
			cstmt.setString(2, status);
			cstmt.setString(3, policyCertificateNo);
			cstmt.setString(4, dateFrom);
			cstmt.setString(5, dateTo);
			cstmt.setString(6, NRIC);
			cstmt.setString(7, receiptNo);
			cstmt.setString(8, plateNo);
			cstmt.setString(9, agentCode);
			cstmt.registerOutParameter(10, OracleTypes.CURSOR);
			cstmt.execute();
			List<TransactionalReport> list = new ArrayList<TransactionalReport>();
			rs = (ResultSet) cstmt.getObject(10);
			while (rs.next()) {

				TransactionalReport TranRPT = new TransactionalReport();
				TranRPT.setId(i);
				if (rs.getString("TRANSACTIONID") == null) {

					TranRPT.setTransactionID("");
				} else {

					TranRPT.setTransactionID(rs.getString("TRANSACTIONID"));
				}

				if (rs.getString("PMNT_GATEWAY_CODE") == null) {

					TranRPT.setPaymentMethod("");
				} else

				{
					TranRPT.setPaymentMethod(rs.getString("PMNT_GATEWAY_CODE"));

				}

				if (rs.getString("AMOUNT") == null) {

					TranRPT.setPremiumAmount("");

				} else {
					TranRPT.setPremiumAmount(rs.getString("AMOUNT"));

				}

				if (rs.getString("PMNT_STATUS") == null) {

					TranRPT.setStatus("");

				} else

				{
					TranRPT.setStatus(rs.getString("PMNT_STATUS"));

				}

				if (rs.getString("TRANSACTION_DATETIME") == null) {

					TranRPT.setTransactionDate("");
				} else {
					String tmpDate = "";

					try {

						tmpDate = ft2.format(format1.parse(rs.getString("TRANSACTION_DATETIME")));

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					TranRPT.setTransactionDate(tmpDate);

				}

				if (rs.getString("INVOICE_NO") == null) {

					TranRPT.setInvoiceNo("");

				} else

				{
					TranRPT.setInvoiceNo(rs.getString("INVOICE_NO"));

				}

				/*
				 * if(TranRPT.getInvoiceNo().indexOf("EIB")!=-1){
				 *
				 * TranRPT.setProductEntity("EIB");
				 *
				 * }
				 *
				 * else if(TranRPT.getInvoiceNo().indexOf("ETB")!=-1)
				 *
				 * { TranRPT.setProductEntity("ETB");
				 *
				 * } else{
				 *
				 *
				 *
				 *
				 * TranRPT.setProductEntity("EIB");
				 *
				 * }
				 */

				if (rs.getString("QQ_ID") == null) {

					TranRPT.setQQID("");

				} else

				{
					TranRPT.setQQID(rs.getString("QQ_ID"));

				}

				if (rs.getString("DSP_QQ_ID") == null) {

					TranRPT.setDSPQQID("");

				} else

				{
					TranRPT.setDSPQQID(rs.getString("DSP_QQ_ID"));

				}

				if (rs.getString("PRODUCT_CODE") == null) {

					TranRPT.setProductCode("");

				} else

				{
					TranRPT.setProductCode(rs.getString("PRODUCT_CODE"));

				}

				if (TranRPT.getProductCode().equals("MI")) {

					TranRPT.setProductEntity("EIB");

				} else {

					TranRPT.setProductEntity("ETB");

				}

				if (rs.getString("PRODUCT_CODE_NAME") == null) {

					TranRPT.setProductType("");

				} else

				{
					TranRPT.setProductType(rs.getString("PRODUCT_CODE_NAME"));

				}

				if (rs.getString("REGISTRATION_NUMBER") == null) {

					TranRPT.setVehicleNo("");

				} else

				{
					TranRPT.setVehicleNo(rs.getString("REGISTRATION_NUMBER"));

				}

				if (rs.getString("VEHICLELOC") == null) {

					TranRPT.setVehicleLoc("");

				} else

				{
					TranRPT.setVehicleLoc(rs.getString("VEHICLELOC"));

				}

				if (rs.getString("PREVIOUS_INS_COMPANY") == null) {

					TranRPT.setPreviousInsurance("");

				} else

				{
					TranRPT.setPreviousInsurance(rs.getString("PREVIOUS_INS_COMPANY"));

				}

				if (rs.getString("YEAR_MAKE") == null) {

					TranRPT.setYearMake("");

				} else

				{
					TranRPT.setYearMake(rs.getString("YEAR_MAKE"));

				}

				if (rs.getString("VEHICLEMAKE") == null) {

					TranRPT.setVehicleMake("");

				} else

				{
					TranRPT.setVehicleMake(rs.getString("VEHICLEMAKE"));

				}

				if (rs.getString("VEHICLEMODEL") == null) {

					TranRPT.setVehicleModel("");

				} else

				{
					TranRPT.setVehicleModel(rs.getString("VEHICLEMODEL"));

				}

				if (rs.getString("SEAT") == null) {

					TranRPT.setVehicleSeat("");

				} else

				{
					TranRPT.setVehicleSeat(rs.getString("SEAT"));

				}

				if (rs.getString("CC") == null) {

					TranRPT.setVehicleCC("");

				} else

				{
					TranRPT.setVehicleCC(rs.getString("CC"));

				}

				if (rs.getString("MARKET_VALUE") == null) {

					TranRPT.setMarketValue("");

				} else

				{
					TranRPT.setMarketValue(rs.getString("MARKET_VALUE"));

				}

				if (rs.getString("ENGINE_NUMBER") == null) {

					TranRPT.setEngineNo("");

				} else

				{
					TranRPT.setEngineNo(rs.getString("ENGINE_NUMBER"));

				}

				if (rs.getString("CHASSIS_NUMBER") == null) {

					TranRPT.setChassisNo("");

				} else

				{
					TranRPT.setChassisNo(rs.getString("CHASSIS_NUMBER"));

				}

				if (rs.getString("FINANCED_BY") == null) {

					TranRPT.setFinancedBy("");

				} else

				{
					TranRPT.setFinancedBy(rs.getString("FINANCED_BY"));

				}

				if (rs.getString("NVIC_CODE") == null) {

					TranRPT.setNVIC("");

				} else

				{
					TranRPT.setNVIC(rs.getString("NVIC_CODE"));

				}

				if (rs.getString("POLICY_SUM_INSURED") == null) {

					TranRPT.setSumInsured("");

				} else

				{
					TranRPT.setSumInsured(rs.getString("POLICY_SUM_INSURED"));

				}

				if (rs.getString("PREMIUM_ON_SUM_INSURED") == null) {

					TranRPT.setPremiumSumInsured("");

				} else

				{
					TranRPT.setPremiumSumInsured(rs.getString("PREMIUM_ON_SUM_INSURED"));

				}

				if (rs.getString("GST_ON_POLICY_PREMIUM") == null) {

					TranRPT.setGSTPolicyPremium("");

				} else

				{
					TranRPT.setGSTPolicyPremium(rs.getString("GST_ON_POLICY_PREMIUM"));

				}

				if (rs.getString("STAMP_DUTY_POLICY_PREMIUM") == null) {

					TranRPT.setStampDutyPolicyPremium("");

				} else

				{
					TranRPT.setStampDutyPolicyPremium(rs.getString("STAMP_DUTY_POLICY_PREMIUM"));

				}

				if (rs.getString("PASSENGER_PA_PREMIUM_PAYABLE") == null) {

					TranRPT.setPAPremiumPayable("");

				} else

				{
					TranRPT.setPAPremiumPayable(rs.getString("PASSENGER_PA_PREMIUM_PAYABLE"));

				}

				if (rs.getString("TOTAL_PREMIUM_PAYABLE") == null) {

					TranRPT.setTotalPremiumPayable("");

				} else

				{
					TranRPT.setTotalPremiumPayable(rs.getString("TOTAL_PREMIUM_PAYABLE"));

				}

				if (rs.getString("PREMIUM_AFTER_DISCOUNT") == null) {

					TranRPT.setPremiumAfterDiscount("");

				} else

				{
					TranRPT.setPremiumAfterDiscount(rs.getString("PREMIUM_AFTER_DISCOUNT"));

				}

				if (rs.getString("PREMIUM_AFTER_GST") == null) {

					TranRPT.setPremiumAfterGST("");

				} else

				{
					TranRPT.setPremiumAfterGST(rs.getString("PREMIUM_AFTER_GST"));

				}

				if (rs.getString("GROSSPREMIUM_FINAL") == null) {

					TranRPT.setGrossPremiumFinal("");

				} else

				{
					TranRPT.setGrossPremiumFinal(rs.getString("GROSSPREMIUM_FINAL"));

				}

				if (rs.getString("DISCOUNT") == null) {

					TranRPT.setDiscount("");

				} else

				{
					TranRPT.setDiscount(rs.getString("DISCOUNT"));

				}

				if (rs.getString("GST") == null) {

					TranRPT.setGST("");

				} else

				{
					TranRPT.setGST(rs.getString("GST"));

				}

				if (rs.getString("EXCESS") == null) {

					TranRPT.setExcess("");

				} else

				{
					TranRPT.setExcess(rs.getString("EXCESS"));

				}

				if (rs.getString("NON_CLAIM_DISCOUNT") == null) {

					TranRPT.setNonClaimDiscount("");

				} else

				{
					TranRPT.setNonClaimDiscount(rs.getString("NON_CLAIM_DISCOUNT"));

				}

				if (rs.getString("PARKING_MAIL_ADDRESS_1") == null) {

					TranRPT.setParkingAddress1("");

				} else

				{
					TranRPT.setParkingAddress1(rs.getString("PARKING_MAIL_ADDRESS_1"));

				}

				if (rs.getString("PARKING_MAIL_ADDRESS_2") == null) {

					TranRPT.setParkingAddress2("");

				} else

				{
					TranRPT.setParkingAddress2(rs.getString("PARKING_MAIL_ADDRESS_2"));

				}

				if (rs.getString("PARKING_MAIL_POSTCODE") == null) {

					TranRPT.setParkingPostcode("");

				} else

				{
					TranRPT.setParkingPostcode(rs.getString("PARKING_MAIL_POSTCODE"));

				}

				if (rs.getString("PARKING_MAIL_STATE") == null) {

					TranRPT.setParkingState("");

				} else

				{
					TranRPT.setParkingState(rs.getString("PARKING_MAIL_STATE"));

				}

				if (rs.getString("PERIOD_OF_COVERAGE") == null) {

					TranRPT.setPeriodCoverage("");

				} else

				{
					TranRPT.setPeriodCoverage(rs.getString("PERIOD_OF_COVERAGE"));

				}

				if (rs.getString("AGENT_CODE") == null) {

					TranRPT.setAgentCode("");

				} else

				{
					TranRPT.setAgentCode(rs.getString("AGENT_CODE"));

				}

				if (rs.getString("AGENT_NAME") == null) {

					TranRPT.setAgentName("");

				} else

				{
					TranRPT.setAgentName(rs.getString("AGENT_NAME"));

				}

				if (rs.getString("OPERATOR_CODE") == null) {

					TranRPT.setOperatorCode("");

				} else

				{
					TranRPT.setOperatorCode(rs.getString("OPERATOR_CODE"));

				}

				if (rs.getString("QUOTATION_CREATION_DATETIME") == null) {

					TranRPT.setQuotationCreateDateTime("");

				} else

				{
					TranRPT.setQuotationCreateDateTime(rs.getString("QUOTATION_CREATION_DATETIME"));

				}

				if (rs.getString("QUOTATION_UPDATE_DATETIME") == null) {

					TranRPT.setQuotationUpdateDateTime("");

				} else

				{
					TranRPT.setQuotationUpdateDateTime(rs.getString("QUOTATION_UPDATE_DATETIME"));

				}

				if (rs.getString("NO_OF_DRIVERS") == null) {

					TranRPT.setDriverNo("");

				} else

				{
					TranRPT.setDriverNo(rs.getString("NO_OF_DRIVERS"));

				}

				if (rs.getString("LOADING") == null) {

					TranRPT.setLoading("");

				} else

				{
					TranRPT.setLoading(rs.getString("LOADING"));

				}

				if (rs.getString("LOADING_PERCENTAGE") == null) {

					TranRPT.setLoadingPercentage("");

				} else

				{
					TranRPT.setLoadingPercentage(rs.getString("LOADING_PERCENTAGE"));

				}

				if (rs.getString("ADDITINAL_CODES") == null) {

					TranRPT.setAdditinalCodes("");

				} else

				{
					TranRPT.setAdditinalCodes(rs.getString("ADDITINAL_CODES"));

				}

				if (rs.getString("NCD_AMOUNT") == null) {

					TranRPT.setNCDAmount("");

				} else

				{
					TranRPT.setNCDAmount(rs.getString("NCD_AMOUNT"));

				}

				if (rs.getString("AGENT_COMMISSION_AMT") == null) {

					TranRPT.setAgentCommissionAmount("");

				} else

				{
					TranRPT.setAgentCommissionAmount(rs.getString("AGENT_COMMISSION_AMT"));

				}

				if (rs.getString("CAPS_COMMISSION") == null) {

					TranRPT.setCAPSCommissionAmount("");

				} else

				{
					TranRPT.setCAPSCommissionAmount(rs.getString("CAPS_COMMISSION"));

				}

				if (rs.getString("DISCOUNT_CODE") == null) {

					TranRPT.setDiscountCode("");

				} else

				{
					TranRPT.setDiscountCode(rs.getString("DISCOUNT_CODE"));

				}

				if (rs.getString("IDTYPE") == null) {

					TranRPT.setIDType("");

				} else

				{
					TranRPT.setIDType(rs.getString("IDTYPE"));

				}

				if (rs.getString("CUSTOMER_NRIC_ID") == null) {

					TranRPT.setCustomerNRIC("");

				} else

				{
					TranRPT.setCustomerNRIC(rs.getString("CUSTOMER_NRIC_ID"));

				}

				if (rs.getString("CUSTOMER_NAME") == null) {

					TranRPT.setCustomerName("");

				} else

				{
					TranRPT.setCustomerName(removeScriptTags(rs.getString("CUSTOMER_NAME")));

				}

				if (rs.getString("CUSTOMER_DOB") == null) {

					TranRPT.setCustomerDOB("");
					TranRPT.setCustomerAge("");

				} else

				{
					TranRPT.setCustomerDOB(rs.getString("CUSTOMER_DOB"));

					String[] dob_split = TranRPT.getCustomerDOB().split("/");

					// logger.info("day ="+dob_split[0]);
					// logger.info("month ="+dob_split[1]);
					// logger.info("year ="+dob_split[2]);

					LocalDate start = LocalDate.of(Integer.parseInt(dob_split[2]), Integer.parseInt(dob_split[1]),
							Integer.parseInt(dob_split[0]));
					LocalDate end = LocalDate.now(); // use for age-calculation: LocalDate.now()
					long age = ChronoUnit.YEARS.between(start, end);
					// logger.info("age ="+age); // 17

					TranRPT.setCustomerAge(new Long(age).toString());

				}

				if (rs.getString("CUSTOMER_GENDER") == null) {

					TranRPT.setCustomerGender("");

				} else

				{
					TranRPT.setCustomerGender(rs.getString("CUSTOMER_GENDER"));

				}

				if (rs.getString("CUSTOMER_ADDRESS1") == null) {

					TranRPT.setCustomerAddress1("");

				} else

				{
					TranRPT.setCustomerAddress1(rs.getString("CUSTOMER_ADDRESS1"));

				}

				if (rs.getString("CUSTOMER_ADDRESS2") == null) {

					TranRPT.setCustomerAddress2("");

				} else

				{
					TranRPT.setCustomerAddress2(rs.getString("CUSTOMER_ADDRESS2"));

				}

				if (rs.getString("CUSTOMER_ADDRESS3") == null) {

					TranRPT.setCustomerAddress3("");

				} else

				{
					TranRPT.setCustomerAddress3(rs.getString("CUSTOMER_ADDRESS3"));

				}

				if (rs.getString("CUSTOMER_POSTCODE") == null) {

					TranRPT.setCustomerPostcode("");

				} else

				{
					TranRPT.setCustomerPostcode(rs.getString("CUSTOMER_POSTCODE"));

				}

				if (rs.getString("CUSTOMER_STATE") == null) {

					TranRPT.setCustomerState("");

				} else

				{
					TranRPT.setCustomerState(rs.getString("CUSTOMER_STATE"));

				}

				if (rs.getString("CUSTOMER_EMAIL") == null) {

					TranRPT.setCustomerEmail("");

				} else

				{
					TranRPT.setCustomerEmail(rs.getString("CUSTOMER_EMAIL"));

				}

				if (rs.getString("CUSTOMER_MOBILE_NO") == null) {

					TranRPT.setCustomerMobileNo("");

				} else

				{
					TranRPT.setCustomerMobileNo(rs.getString("CUSTOMER_MOBILE_NO"));

				}

				if (rs.getString("POLICY_NUMBER") == null) {

					TranRPT.setPolicyNo("");

				} else

				{
					TranRPT.setPolicyNo(rs.getString("POLICY_NUMBER"));

				}

				if (rs.getString("POLICY_NUMBER_CAPS") == null) {

					TranRPT.setCAPSPolicyNo("");

				} else

				{
					TranRPT.setCAPSPolicyNo(rs.getString("POLICY_NUMBER_CAPS"));

				}

				if (rs.getString("POLICY_STATUS") == null) {

					TranRPT.setPolicyStatus("");

				} else

				{
					TranRPT.setPolicyStatus(rs.getString("POLICY_STATUS"));

				}

				if (rs.getString("EBPG_REFNO") == null) {

					TranRPT.setEBPG_RefNo("");
				} else

				{
					TranRPT.setEBPG_RefNo(rs.getString("EBPG_REFNO"));

				}

				if (rs.getString("EBPG_AUTHCODE") == null) {

					TranRPT.setEBPG_AuthCode("");
				} else

				{
					TranRPT.setEBPG_AuthCode(rs.getString("EBPG_AUTHCODE"));

				}

				if (rs.getString("M2U_REFNO") == null) {

					TranRPT.setM2U_RefNo("");
				} else

				{
					TranRPT.setM2U_RefNo(rs.getString("M2U_REFNO"));

				}

				if (rs.getString("M2U_AUTHCODE") == null) {

					TranRPT.setM2U_AuthCode("");
				} else

				{
					TranRPT.setM2U_AuthCode(rs.getString("M2U_AUTHCODE"));

				}

				if (rs.getString("FPX_REFNO") == null) {

					TranRPT.setFPX_RefNo("");
				} else

				{
					TranRPT.setFPX_RefNo(rs.getString("FPX_REFNO"));

				}

				if (rs.getString("FPX_AUTHCODE") == null) {

					TranRPT.setFPX_AuthCode("");
				} else

				{
					TranRPT.setFPX_AuthCode(rs.getString("FPX_AUTHCODE"));

				}

				if (rs.getString("LLOP") == null) {

					TranRPT.setLLOP("");
				} else

				{
					TranRPT.setLLOP(rs.getString("LLOP"));

				}

				if (rs.getString("LL2P") == null) {

					TranRPT.setLL2P("");
				} else

				{
					TranRPT.setLL2P(rs.getString("LL2P"));

				}

				if (rs.getString("Extended_Flood_Cover") == null) {

					TranRPT.setFloodCover("");
				} else

				{
					TranRPT.setFloodCover(rs.getString("Extended_Flood_Cover"));

				}

				if (rs.getString("NCD_Relief") == null) {

					TranRPT.setNCDRelief("");
				} else

				{
					TranRPT.setNCDRelief(rs.getString("NCD_Relief"));

				}

				if (rs.getString("SRCC") == null) {

					TranRPT.setSRCC("");
				} else

				{
					TranRPT.setSRCC(rs.getString("SRCC"));

				}

				if (rs.getString("Windscreen_SC") == null) {

					TranRPT.setWindscreenSC("");
				} else

				{
					TranRPT.setWindscreenSC(rs.getString("Windscreen_SC"));

				}
				if (rs.getString("Windscreen_P") == null) {

					TranRPT.setWindscreenP("");
				} else

				{
					TranRPT.setWindscreenP(rs.getString("Windscreen_P"));

				}

				if (rs.getString("VehicleAcc_SC") == null) {

					TranRPT.setVehicleAccSC("");
				} else

				{
					TranRPT.setVehicleAccSC(rs.getString("VehicleAcc_SC"));

				}

				if (rs.getString("VehicleAcc_P") == null) {

					TranRPT.setVehicleAccP("");
				} else

				{
					TranRPT.setVehicleAccP(rs.getString("VehicleAcc_P"));

				}

				if (rs.getString("NGV_Gas_SC") == null) {

					TranRPT.setNGVGasSC("");
				} else

				{
					TranRPT.setNGVGasSC(rs.getString("NGV_Gas_SC"));

				}

				if (rs.getString("NGV_Gas_P") == null) {

					TranRPT.setNGVGasP("");
				} else

				{
					TranRPT.setNGVGasP(rs.getString("NGV_Gas_P"));

				}

				if (rs.getString("FIRSTDRIVER") == null) {

					TranRPT.setFirstDriver("");
				} else

				{
					TranRPT.setFirstDriver(rs.getString("FIRSTDRIVER"));

				}

				if (rs.getString("SECONDDRIVER") == null) {

					TranRPT.setSecondDriver("");
				} else

				{
					TranRPT.setSecondDriver(rs.getString("SECONDDRIVER"));

				}

				if (rs.getString("JPJSTATUS") == null) {

					TranRPT.setJPJStatus("");
				} else

				{
					TranRPT.setJPJStatus(rs.getString("JPJSTATUS"));

				}
				if (rs.getString("JPJDOCTYPE") == null) {

					// TranRPT.setJPJStatus("");
					TranRPT.setJpjDocType("");
				} else

				{
					TranRPT.setJpjDocType(rs.getString("JPJDOCTYPE"));

				}
				if (rs.getString("JPJREASONCODE") == null) {

					// TranRPT.setJPJStatus("");
					TranRPT.setJpjReasonCode("");
				} else

				{
					TranRPT.setJpjReasonCode(rs.getString("JPJREASONCODE"));

				}
				if (rs.getString("CART_PREMIUM") == null) {

					TranRPT.setCartPremium("");
				} else

				{
					TranRPT.setCartPremium(rs.getString("CART_PREMIUM"));

				}

				if (rs.getString("CART_PREMIUM_DESC") == null) {

					TranRPT.setCartPremiumDesc("");
				} else

				{
					String tmpCARTDesc = rs.getString("CART_PREMIUM_DESC").replaceAll(",", "|");
					tmpCARTDesc = tmpCARTDesc.replaceAll("Days", "");
					tmpCARTDesc = tmpCARTDesc.replaceAll("RM", "");
					tmpCARTDesc = tmpCARTDesc.replaceAll("/day", "");

					TranRPT.setCartPremiumDesc(tmpCARTDesc + "|");

				}

				if (rs.getString("MARK_UP_PER") == null) {

					TranRPT.setMarkUpPer("");
				} else

				{
					TranRPT.setMarkUpPer(rs.getString("MARK_UP_PER"));

				}

				if (TranRPT.getPaymentMethod().indexOf("EBPG") != -1) {

					TranRPT.setPaymentMethod("CREDIT CARD");
					TranRPT.setPaymentChannel("EBPG");
				}

				else if (TranRPT.getPaymentMethod().indexOf("AMEX") != -1) {

					TranRPT.setPaymentMethod("CREDIT CARD");
					TranRPT.setPaymentChannel("AMEX");
				}

				else if (TranRPT.getPaymentMethod().indexOf("FPX") != -1) {

					TranRPT.setPaymentMethod("ONLINE BANKING");
					TranRPT.setPaymentChannel("FPX");
				}

				else if (TranRPT.getPaymentMethod().indexOf("Maybank2U") != -1) {

					TranRPT.setPaymentMethod("DEBIT ACCOUNT");
					TranRPT.setPaymentChannel("M2U");
				} else {

					TranRPT.setPaymentMethod("");
					TranRPT.setPaymentChannel("");
				}

				// B.MARKET_VALUE-((NV.MARK_UP_PER*B.MARKET_VALUE)/100) AS SI_Min
				// B.MARKET_VALUE+((NV.MARK_UP_PER*B.MARKET_VALUE)/100) AS SI_MAX

				if (rs.getString("MARKET_VALUE") != null && rs.getString("MARKET_VALUE") != ""
						&& rs.getString("MARK_UP_PER") != null && rs.getString("MARK_UP_PER") != "") {
					DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(Locale.US);
					format.setParseBigDecimal(true);
					BigDecimal amt1 = new BigDecimal(0.00);
					BigDecimal amt2 = new BigDecimal(0.00);
					BigDecimal amt3 = new BigDecimal(100.00);
					try {
						amt1 = (BigDecimal) format.parse(rs.getString("MARKET_VALUE"));
						amt2 = (BigDecimal) format.parse(rs.getString("MARK_UP_PER"));

						BigDecimal tmpAmount = amt2.multiply(amt1).divide(amt3);
						BigDecimal SIMin = amt1.subtract(tmpAmount);
						BigDecimal SIMax = amt1.add(tmpAmount);

						TranRPT.setSIMin(String.valueOf(SIMin));
						TranRPT.setSIMax(String.valueOf(SIMax));

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				if (rs.getString("FIRSTDRIVERINFO") == null || rs.getString("FIRSTDRIVERINFO").equals("||||||")) {

					TranRPT.setFirstDriverInfo("");
				} else

				{

					TranRPT.setFirstDriverInfo(rs.getString("FIRSTDRIVERINFO"));

				}

				if (rs.getString("SECONDDRIVERINFO") == null || rs.getString("SECONDDRIVERINFO").equals("||||||")) {

					TranRPT.setSecondDriverInfo("");
				} else

				{

					TranRPT.setSecondDriverInfo(rs.getString("SECONDDRIVERINFO"));

				}

				if (rs.getString("THIRDDRIVERINFO") == null || rs.getString("THIRDDRIVERINFO").equals("||||||")) {

					TranRPT.setThirdDriverInfo("");
				} else

				{

					TranRPT.setThirdDriverInfo(rs.getString("THIRDDRIVERINFO"));

				}

				if (rs.getString("FORTHDRIVERINFO") == null || rs.getString("FORTHDRIVERINFO").equals("||||||")) {

					TranRPT.setForthDriverInfo("");
				} else

				{

					TranRPT.setForthDriverInfo(rs.getString("FORTHDRIVERINFO"));

				}

				if (rs.getString("FIFTHDRIVERINFO") == null || rs.getString("FIFTHDRIVERINFO").equals("||||||")) {

					TranRPT.setFifthDriverInfo("");
				} else

				{

					TranRPT.setFifthDriverInfo(rs.getString("FIFTHDRIVERINFO"));

				}

				if (rs.getString("SIXTHDRIVERINFO") == null || rs.getString("SIXTHDRIVERINFO").equals("||||||")) {

					TranRPT.setSixthDriverInfo("");
				} else

				{

					TranRPT.setSixthDriverInfo(rs.getString("SIXTHDRIVERINFO"));

				}

				if (rs.getString("SEVENTHDRIVERINFO") == null || rs.getString("SEVENTHDRIVERINFO").equals("||||||")) {

					TranRPT.setSeventhDriverInfo("");
				} else

				{

					TranRPT.setSeventhDriverInfo(rs.getString("SEVENTHDRIVERINFO"));

				}

				if (rs.getString("VEHICLE_SAFTEY_LIST") == null) {

					TranRPT.setCARSECM("");
				} else

				{
					TranRPT.setCARSECM(rs.getString("VEHICLE_SAFTEY_LIST"));

				}

				if (rs.getString("VEHICLE_ANTI_THEFT_LIST") == null) {

					TranRPT.setATITFTCD("");
				} else

				{
					TranRPT.setATITFTCD(rs.getString("VEHICLE_ANTI_THEFT_LIST"));

				}

				if (rs.getString("VEHICLE_PARK_OVERNEIGHT_LIST") == null) {

					TranRPT.setGARAGECD("");
				} else

				{
					TranRPT.setGARAGECD(rs.getString("VEHICLE_PARK_OVERNEIGHT_LIST"));

				}

				if (rs.getString("LAST_PAGE") == null) {

					TranRPT.setLastPage("");
				} else

				{
					TranRPT.setLastPage(rs.getString("LAST_PAGE"));

				}

				if (rs.getString("REJECTION_REASON") == null) {

					TranRPT.setReason("");
				} else

				{
					TranRPT.setReason(rs.getString("REJECTION_REASON"));

				}

				if (rs.getString("PASSENGER_PA_PREMIUM_PAYABLE") == null) {

					TranRPT.setCapsAmount("");

				} else

				{
					TranRPT.setCapsAmount(rs.getString("PASSENGER_PA_PREMIUM_PAYABLE"));

				}

				if (rs.getString("PREMIUM_AFTER_GST") == null) {

					TranRPT.setMotorAmount(null);

				} else

				{
					// TranRPT.setMotorAmount(rs.getString("PREMIUM_AFTER_GST"));

					DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(Locale.US);
					format.setParseBigDecimal(true);
					if (rs.getString("MOTOR_NET_PREMIUM") == null) {

						BigDecimal amt1 = new BigDecimal(0.00);
						BigDecimal amt2 = new BigDecimal(10.00);
						BigDecimal amt3 = new BigDecimal(0.01);
						try {
							amt1 = (BigDecimal) format.parse(rs.getString("PREMIUM_AFTER_GST"));
							BigDecimal overallAmount = amt1.add(amt2);
							BigDecimal finalAmount = overallAmount.setScale(2, RoundingMode.CEILING);

							TranRPT.setMotorAmount(finalAmount);
							// logger.info("finalAmount "+finalAmount);

							/*
							 * if(TranRPT.getPremiumAmount() != TranRPT.getMotorAmount().toString() &&
							 * TranRPT.getCapsAmount().equals("")){
							 *
							 * finalAmount = finalAmount.subtract(amt3);
							 * TranRPT.setMotorAmount(finalAmount);
							 * //logger.info("finalAmount ... "+finalAmount); }
							 */

						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					else {

						try {
							TranRPT.setMotorAmount(((BigDecimal) format.parse(rs.getString("MOTOR_NET_PREMIUM")))
									.setScale(2, RoundingMode.CEILING));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				}

				/*
				 * if(!TranRPT.getCapsAmount().equals("") &&
				 * !TranRPT.getCapsAmount().equals("0") &&
				 * !TranRPT.getCapsAmount().equals(null)){
				 *
				 * String string_temp = TranRPT.getMotorAmount().toString(); String string_form
				 * = string_temp.substring(0,string_temp.indexOf('.'));
				 *
				 *
				 * String string_temp1 = TranRPT.getPremiumAmount().toString(); String
				 * string_form1 = string_temp1.substring(0,string_temp1.indexOf('.'));
				 *
				 * if(string_form.equals(string_form1)){
				 *
				 * TranRPT.setCapsAmount("");
				 *
				 *
				 *
				 * }
				 *
				 * }
				 */

				TranRPT.setRecordCount(Integer.parseInt(rs.getString("RECORDCOUNT")));
				TranRPT.setTotalAmount(rs.getString("TOTALAMOUNT"));
				list.add(TranRPT);

			}

			return list;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}

	}

	/////////////////////////////////////// END: ADD ON 1 MARCH
	/////////////////////////////////////// 2017//////////////////////////////////////

	//////////////////////////// START: ADD ON 25 MAY
	//////////////////////////// 2017/////////////////////////////////////////////

	@Override
	// query result for motor
	public List<TransactionalReport> getMotorRecordAll(String productEntity, String status, String policyCertificateNo,
			String dateFrom, String dateTo, String NRIC, String receiptNo, String plateNo, String agentCode) {

		try {

			int i = 1;
			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_MI_REPORT(?,?,?,?,?,?,?,?,?,?)}");
			cstmt.setString(1, productEntity.trim());
			cstmt.setString(2, status);
			cstmt.setString(3, policyCertificateNo);
			cstmt.setString(4, dateFrom);
			cstmt.setString(5, dateTo);
			cstmt.setString(6, NRIC);
			cstmt.setString(7, receiptNo);
			cstmt.setString(8, plateNo);
			cstmt.setString(9, agentCode);
			cstmt.registerOutParameter(10, OracleTypes.CURSOR);
			cstmt.execute();
			List<TransactionalReport> list = new ArrayList<TransactionalReport>();
			rs = (ResultSet) cstmt.getObject(10);
			while (rs.next()) {

				TransactionalReport TranRPT = new TransactionalReport();
				TranRPT.setId(i);
				if (rs.getString("TRANSACTIONID") == null) {

					TranRPT.setTransactionID("");
				} else {

					TranRPT.setTransactionID(rs.getString("TRANSACTIONID"));
				}

				if (rs.getString("PMNT_GATEWAY_CODE") == null) {

					TranRPT.setPaymentMethod("");
				} else

				{
					TranRPT.setPaymentMethod(rs.getString("PMNT_GATEWAY_CODE"));

				}

				if (rs.getString("AMOUNT") == null) {

					TranRPT.setPremiumAmount("");

				} else {
					TranRPT.setPremiumAmount(rs.getString("AMOUNT"));

				}

				if (rs.getString("PMNT_STATUS") == null) {

					TranRPT.setStatus("");

				} else

				{
					TranRPT.setStatus(rs.getString("PMNT_STATUS"));

				}

				if (rs.getString("TRANSACTION_DATETIME") == null) {

					TranRPT.setTransactionDate("");
				} else {
					String tmpDate = "";

					try {

						tmpDate = ft2.format(format1.parse(rs.getString("TRANSACTION_DATETIME")));

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					TranRPT.setTransactionDate(tmpDate);

				}

				if (rs.getString("INVOICE_NO") == null) {

					TranRPT.setInvoiceNo("");

				} else

				{
					TranRPT.setInvoiceNo(rs.getString("INVOICE_NO"));

				}

				/*
				 * if(TranRPT.getInvoiceNo().indexOf("EIB")!=-1){
				 *
				 * TranRPT.setProductEntity("EIB");
				 *
				 * }
				 *
				 * else if(TranRPT.getInvoiceNo().indexOf("ETB")!=-1)
				 *
				 * { TranRPT.setProductEntity("ETB");
				 *
				 * } else{
				 *
				 *
				 *
				 *
				 * TranRPT.setProductEntity("EIB");
				 *
				 * }
				 */

				if (rs.getString("QQ_ID") == null) {

					TranRPT.setQQID("");

				} else

				{
					TranRPT.setQQID(rs.getString("QQ_ID"));

				}

				if (rs.getString("DSP_QQ_ID") == null) {

					TranRPT.setDSPQQID("");

				} else

				{
					TranRPT.setDSPQQID(rs.getString("DSP_QQ_ID"));

				}

				if (rs.getString("PRODUCT_CODE") == null) {

					TranRPT.setProductCode("");

				} else

				{
					TranRPT.setProductCode(rs.getString("PRODUCT_CODE"));

				}

				if (TranRPT.getProductCode().equals("MI")) {

					TranRPT.setProductEntity("EIB");

				} else {

					TranRPT.setProductEntity("ETB");

				}

				if (rs.getString("PRODUCT_CODE_NAME") == null) {

					TranRPT.setProductType("");

				} else

				{
					TranRPT.setProductType(rs.getString("PRODUCT_CODE_NAME"));

				}

				if (rs.getString("REGISTRATION_NUMBER") == null) {

					TranRPT.setVehicleNo("");

				} else

				{
					TranRPT.setVehicleNo(rs.getString("REGISTRATION_NUMBER"));

				}

				if (rs.getString("PASSENGER_PA_PREMIUM_PAYABLE") == null) {

					TranRPT.setPAPremiumPayable("");

				} else

				{
					TranRPT.setPAPremiumPayable(rs.getString("PASSENGER_PA_PREMIUM_PAYABLE"));

				}

				if (rs.getString("TOTAL_PREMIUM_PAYABLE") == null) {

					TranRPT.setTotalPremiumPayable("");

				} else

				{
					TranRPT.setTotalPremiumPayable(rs.getString("TOTAL_PREMIUM_PAYABLE"));

				}

				if (rs.getString("PREMIUM_AFTER_DISCOUNT") == null) {

					TranRPT.setPremiumAfterDiscount("");

				} else

				{
					TranRPT.setPremiumAfterDiscount(rs.getString("PREMIUM_AFTER_DISCOUNT"));

				}

				if (rs.getString("PREMIUM_AFTER_GST") == null) {

					TranRPT.setPremiumAfterGST("");

				} else

				{
					TranRPT.setPremiumAfterGST(rs.getString("PREMIUM_AFTER_GST"));

				}

				if (rs.getString("GROSSPREMIUM_FINAL") == null) {

					TranRPT.setGrossPremiumFinal("");

				} else

				{
					TranRPT.setGrossPremiumFinal(rs.getString("GROSSPREMIUM_FINAL"));

				}

				if (rs.getString("DISCOUNT") == null) {

					TranRPT.setDiscount("");

				} else

				{
					TranRPT.setDiscount(rs.getString("DISCOUNT"));

				}

				if (rs.getString("GST") == null) {

					TranRPT.setGST("");

				} else

				{
					TranRPT.setGST(rs.getString("GST"));

				}

				if (rs.getString("AGENT_CODE") == null) {

					TranRPT.setAgentCode("");

				} else

				{
					TranRPT.setAgentCode(rs.getString("AGENT_CODE"));

				}

				if (rs.getString("AGENT_NAME") == null) {

					TranRPT.setAgentName("");

				} else

				{
					TranRPT.setAgentName(rs.getString("AGENT_NAME"));

				}

				if (rs.getString("OPERATOR_CODE") == null) {

					TranRPT.setOperatorCode("");

				} else

				{
					TranRPT.setOperatorCode(rs.getString("OPERATOR_CODE"));

				}

				if (rs.getString("QUOTATION_UPDATE_DATETIME") == null) {

					TranRPT.setQuotationUpdateDateTime("");

				} else

				{
					TranRPT.setQuotationUpdateDateTime(rs.getString("QUOTATION_UPDATE_DATETIME"));

				}

				if (rs.getString("CUSTOMER_NRIC_ID") == null) {

					TranRPT.setCustomerNRIC("");

				} else

				{
					TranRPT.setCustomerNRIC(rs.getString("CUSTOMER_NRIC_ID"));

				}

				if (rs.getString("CUSTOMER_NAME") == null) {

					TranRPT.setCustomerName("");

				} else

				{
					TranRPT.setCustomerName(removeScriptTags(rs.getString("CUSTOMER_NAME")));

				}

				if (rs.getString("POLICY_NUMBER") == null) {

					TranRPT.setPolicyNo("");

				} else

				{
					TranRPT.setPolicyNo(rs.getString("POLICY_NUMBER"));

				}

				if (rs.getString("POLICY_NUMBER_CAPS") == null) {

					TranRPT.setCAPSPolicyNo("");

				} else

				{
					TranRPT.setCAPSPolicyNo(rs.getString("POLICY_NUMBER_CAPS"));

				}

				if (rs.getString("POLICY_STATUS") == null) {

					TranRPT.setPolicyStatus("");

				} else

				{
					TranRPT.setPolicyStatus(rs.getString("POLICY_STATUS"));

				}

				if (rs.getString("EBPG_REFNO") == null) {

					TranRPT.setEBPG_RefNo("");
				} else

				{
					TranRPT.setEBPG_RefNo(rs.getString("EBPG_REFNO"));

				}

				if (rs.getString("EBPG_AUTHCODE") == null) {

					TranRPT.setEBPG_AuthCode("");
				} else

				{
					TranRPT.setEBPG_AuthCode(rs.getString("EBPG_AUTHCODE"));

				}

				if (rs.getString("M2U_REFNO") == null) {

					TranRPT.setM2U_RefNo("");
				} else

				{
					TranRPT.setM2U_RefNo(rs.getString("M2U_REFNO"));

				}

				if (rs.getString("M2U_AUTHCODE") == null) {

					TranRPT.setM2U_AuthCode("");
				} else

				{
					TranRPT.setM2U_AuthCode(rs.getString("M2U_AUTHCODE"));

				}

				if (rs.getString("FPX_REFNO") == null) {

					TranRPT.setFPX_RefNo("");
				} else

				{
					TranRPT.setFPX_RefNo(rs.getString("FPX_REFNO"));

				}

				if (rs.getString("FPX_AUTHCODE") == null) {

					TranRPT.setFPX_AuthCode("");
				} else

				{
					TranRPT.setFPX_AuthCode(rs.getString("FPX_AUTHCODE"));

				}

				if (TranRPT.getPaymentMethod().indexOf("EBPG") != -1) {

					TranRPT.setPaymentMethod("CREDIT CARD");
					TranRPT.setPaymentChannel("EBPG");
				}

				else if (TranRPT.getPaymentMethod().indexOf("AMEX") != -1) {

					TranRPT.setPaymentMethod("CREDIT CARD");
					TranRPT.setPaymentChannel("AMEX");
				}

				else if (TranRPT.getPaymentMethod().indexOf("FPX") != -1) {

					TranRPT.setPaymentMethod("ONLINE BANKING");
					TranRPT.setPaymentChannel("FPX");
				}

				else if (TranRPT.getPaymentMethod().indexOf("Maybank2U") != -1) {

					TranRPT.setPaymentMethod("DEBIT ACCOUNT");
					TranRPT.setPaymentChannel("M2U");
				} else {

					TranRPT.setPaymentMethod("");
					TranRPT.setPaymentChannel("");
				}

				if (rs.getString("LAST_PAGE") == null) {

					TranRPT.setLastPage("");
				} else

				{
					TranRPT.setLastPage(rs.getString("LAST_PAGE"));

				}

				if (rs.getString("REJECTION_REASON") == null) {

					TranRPT.setReason("");
				} else

				{
					TranRPT.setReason(rs.getString("REJECTION_REASON"));

				}

				if (rs.getString("PASSENGER_PA_PREMIUM_PAYABLE") == null) {

					TranRPT.setCapsAmount("");

				} else

				{
					TranRPT.setCapsAmount(rs.getString("PASSENGER_PA_PREMIUM_PAYABLE"));

				}

				if (rs.getString("PREMIUM_AFTER_GST") == null) {

					TranRPT.setMotorAmount(null);

				} else

				{
					// TranRPT.setMotorAmount(rs.getString("PREMIUM_AFTER_GST"));

					DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(Locale.US);
					format.setParseBigDecimal(true);
					if (rs.getString("MOTOR_NET_PREMIUM") == null) {

						BigDecimal amt1 = new BigDecimal(0.00);
						BigDecimal amt2 = new BigDecimal(10.00);
						BigDecimal amt3 = new BigDecimal(0.01);
						try {
							amt1 = (BigDecimal) format.parse(rs.getString("PREMIUM_AFTER_GST"));
							BigDecimal overallAmount = amt1.add(amt2);
							BigDecimal finalAmount = overallAmount.setScale(2, RoundingMode.CEILING);

							TranRPT.setMotorAmount(finalAmount);
							// logger.info("finalAmount "+finalAmount);

							/*
							 * if(TranRPT.getPremiumAmount() != TranRPT.getMotorAmount().toString() &&
							 * TranRPT.getCapsAmount().equals("")){
							 *
							 * finalAmount = finalAmount.subtract(amt3);
							 * TranRPT.setMotorAmount(finalAmount);
							 * //logger.info("finalAmount ... "+finalAmount); }
							 */

						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					else {

						try {
							TranRPT.setMotorAmount(((BigDecimal) format.parse(rs.getString("MOTOR_NET_PREMIUM")))
									.setScale(2, RoundingMode.CEILING));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				}

				TranRPT.setRecordCount(Integer.parseInt(rs.getString("RECORDCOUNT")));
				TranRPT.setTotalAmount(rs.getString("TOTALAMOUNT"));
				list.add(TranRPT);

			}

			return list;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}

	}

	/////////////////////////////////////// END: ADD ON 25 MAY
	/////////////////////////////////////// 2017//////////////////////////////////////

	////////////////////////////////////// START HOHH : ADD ON 6 MARCH
	////////////////////////////////////// 2017///////////////////////////////

	// query result for hohh total info for export excel

	@Override
	public List<TransactionalReport> getHOHHRecord(String productType, String productEntity, String status,
			String policyCertificateNo, String dateFrom, String dateTo, String NRIC, String receiptNo,
			String agentCode) {

		logger.info(productType + "productType");
		logger.info(productEntity + "productEntity");
		logger.info(status + "status");
		logger.info(dateFrom + "dateFrom");
		logger.info(dateTo + "dateTo");
		logger.info(receiptNo + "receiptNo");

		ResultSet rs = null;
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");

		try {

			int i = 1;
			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_HOHH_ALL_TMP(?,?,?,?,?,?,?,?,?,?)}");
			cstmt.setString(1, productType.trim());
			cstmt.setString(2, productEntity.trim());
			cstmt.setString(3, status);
			cstmt.setString(4, policyCertificateNo);
			cstmt.setString(5, dateFrom);
			cstmt.setString(6, dateTo);
			cstmt.setString(7, NRIC);
			cstmt.setString(8, receiptNo);
			cstmt.setString(9, agentCode);
			cstmt.registerOutParameter(10, OracleTypes.CURSOR);
			cstmt.execute();
			List<TransactionalReport> list = new ArrayList<TransactionalReport>();
			rs = (ResultSet) cstmt.getObject(10);
			while (rs.next()) {

				TransactionalReport TranRPT = new TransactionalReport();
				TranRPT.setId(i);

				if (rs.getString("DSP_QQ_ID") == null) {

					TranRPT.setDSPQQID("");
				} else {

					TranRPT.setDSPQQID(rs.getString("DSP_QQ_ID"));
				}

				if (rs.getString("TRANSACTIONID") == null) {

					TranRPT.setTransactionID("");
				} else {

					TranRPT.setTransactionID(rs.getString("TRANSACTIONID"));
				}

				if (rs.getString("PMNT_GATEWAY_CODE") == null) {

					TranRPT.setPaymentMethod("");
				} else

				{
					TranRPT.setPaymentMethod(rs.getString("PMNT_GATEWAY_CODE"));

				}

				if (rs.getString("AMOUNT") == null) {

					TranRPT.setPremiumAmount("");
					TranRPT.setTotalPremiumPayable("");

				} else {
					TranRPT.setPremiumAmount(rs.getString("AMOUNT"));
					TranRPT.setTotalPremiumPayable(rs.getString("AMOUNT"));

				}

				if (rs.getString("PMNT_STATUS") == null) {

					TranRPT.setStatus("");

				} else

				{
					TranRPT.setStatus(rs.getString("PMNT_STATUS"));

				}

				if (rs.getString("TRANSACTION_DATETIME") == null) {

					TranRPT.setTransactionDate("");
				} else {
					String tmpDate = "";

					try {
						tmpDate = ft2.format(format1.parse(rs.getString("TRANSACTION_DATETIME")));

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					TranRPT.setTransactionDate(tmpDate);

				}

				if (rs.getString("INVOICE_NO") == null) {

					TranRPT.setInvoiceNo("");

				} else

				{
					TranRPT.setInvoiceNo(rs.getString("INVOICE_NO"));

				}

				/*
				 * if(TranRPT.getInvoiceNo().indexOf("EIB")!=-1){
				 *
				 * TranRPT.setProductEntity("EIB");
				 *
				 * }
				 *
				 * else if(TranRPT.getInvoiceNo().indexOf("ETB")!=-1)
				 *
				 * { TranRPT.setProductEntity("ETB");
				 *
				 * } else{
				 *
				 * TranRPT.setProductEntity("EIB");
				 *
				 * }
				 */

				if (rs.getString("DSP_QQ_ID") == null) {

					TranRPT.setDSPQQID("");

				} else

				{
					TranRPT.setDSPQQID(rs.getString("DSP_QQ_ID"));

				}

				if (rs.getString("PRODUCT_CODE") == null) {

					TranRPT.setProductCode("");

				} else

				{
					TranRPT.setProductCode(rs.getString("PRODUCT_CODE"));

				}

				if (TranRPT.getProductCode().equals("HOHH")) {

					TranRPT.setProductEntity("EIB");

				} else {

					TranRPT.setProductEntity("ETB");

				}

				if (rs.getString("HOHH_AGENT_CODE") == null) {

					TranRPT.setAgentCode("");

				} else

				{
					TranRPT.setAgentCode(rs.getString("HOHH_AGENT_CODE"));

				}

				if (rs.getString("AGENT_NAME") == null) {

					TranRPT.setAgentName("");

				} else

				{
					TranRPT.setAgentName(rs.getString("AGENT_NAME"));

				}

				if (rs.getString("HOHH_OPERATOR_CODE") == null) {

					TranRPT.setOperatorCode("");

				} else

				{
					TranRPT.setOperatorCode(rs.getString("HOHH_OPERATOR_CODE"));

				}

				if (rs.getString("IDTYPE") == null) {

					TranRPT.setIDType("");

				} else

				{
					TranRPT.setIDType(rs.getString("IDTYPE"));

				}

				if (rs.getString("CUSTOMER_NRIC_ID") == null) {

					TranRPT.setCustomerNRIC("");

				} else

				{
					TranRPT.setCustomerNRIC(rs.getString("CUSTOMER_NRIC_ID"));

				}

				if (rs.getString("CUSTOMER_NAME") == null) {

					TranRPT.setCustomerName(removeScriptTags(rs.getString("FULL_NAME")));

				} else

				{
					TranRPT.setCustomerName(removeScriptTags(rs.getString("CUSTOMER_NAME")));

				}

				if (rs.getString("CUSTOMER_GENDER") == null) {

					TranRPT.setCustomerGender("");

				} else

				{
					TranRPT.setCustomerGender(rs.getString("CUSTOMER_GENDER"));

				}

				if (rs.getString("CUSTOMER_ADDRESS1") == null) {

					TranRPT.setCustomerAddress1("");

				} else

				{
					TranRPT.setCustomerAddress1(rs.getString("CUSTOMER_ADDRESS1"));

				}

				if (rs.getString("CUSTOMER_ADDRESS2") == null) {

					TranRPT.setCustomerAddress2("");

				} else

				{
					TranRPT.setCustomerAddress2(rs.getString("CUSTOMER_ADDRESS2"));

				}

				if (rs.getString("CUSTOMER_ADDRESS3") == null) {

					TranRPT.setCustomerAddress3("");

				} else

				{
					TranRPT.setCustomerAddress3(rs.getString("CUSTOMER_ADDRESS3"));

				}

				if (rs.getString("CUSTOMER_POSTCODE") == null) {

					TranRPT.setCustomerPostcode("");

				} else

				{
					TranRPT.setCustomerPostcode(rs.getString("CUSTOMER_POSTCODE"));

				}

				if (rs.getString("CUSTOMER_STATE") == null) {

					TranRPT.setCustomerState("");

				} else

				{
					TranRPT.setCustomerState(rs.getString("CUSTOMER_STATE"));

				}

				if (rs.getString("CUSTOMER_EMAIL") == null) {

					TranRPT.setCustomerEmail(rs.getString("LEADS_EMAIL_ID"));

				} else

				{
					TranRPT.setCustomerEmail(rs.getString("CUSTOMER_EMAIL"));

				}

				if (rs.getString("CUSTOMER_MOBILE_NO") == null) {

					TranRPT.setCustomerMobileNo("");

				} else

				{
					TranRPT.setCustomerMobileNo(rs.getString("CUSTOMER_MOBILE_NO"));

				}

				if (rs.getString("POLICY_NUMBER") == null) {

					TranRPT.setPolicyNo("");

				} else

				{
					TranRPT.setPolicyNo(rs.getString("POLICY_NUMBER"));

				}

				if (rs.getString("POLICY_STATUS") == null) {

					TranRPT.setPolicyStatus("");

				} else

				{
					TranRPT.setPolicyStatus(rs.getString("POLICY_STATUS"));

				}

				if (rs.getString("EBPG_REFNO") == null) {

					TranRPT.setEBPG_RefNo("");
				} else

				{
					TranRPT.setEBPG_RefNo(rs.getString("EBPG_REFNO"));

				}

				if (rs.getString("EBPG_AUTHCODE") == null) {

					TranRPT.setEBPG_AuthCode("");
				} else

				{
					TranRPT.setEBPG_AuthCode(rs.getString("EBPG_AUTHCODE"));

				}

				if (rs.getString("M2U_REFNO") == null) {

					TranRPT.setM2U_RefNo("");
				} else

				{
					TranRPT.setM2U_RefNo(rs.getString("M2U_REFNO"));

				}

				if (rs.getString("M2U_AUTHCODE") == null) {

					TranRPT.setM2U_AuthCode("");
				} else

				{
					TranRPT.setM2U_AuthCode(rs.getString("M2U_AUTHCODE"));

				}

				if (rs.getString("FPX_REFNO") == null) {

					TranRPT.setFPX_RefNo("");
				} else

				{
					TranRPT.setFPX_RefNo(rs.getString("FPX_REFNO"));

				}

				if (rs.getString("FPX_AUTHCODE") == null) {

					TranRPT.setFPX_AuthCode("");
				} else

				{
					TranRPT.setFPX_AuthCode(rs.getString("FPX_AUTHCODE"));

				}

				if (TranRPT.getPaymentMethod().indexOf("EBPG") != -1) {

					TranRPT.setPaymentMethod("CREDIT CARD");
					TranRPT.setPaymentChannel("EBPG");
				}

				else if (TranRPT.getPaymentMethod().indexOf("AMEX") != -1) {

					TranRPT.setPaymentMethod("CREDIT CARD");
					TranRPT.setPaymentChannel("AMEX");
				}

				else if (TranRPT.getPaymentMethod().indexOf("FPX") != -1) {

					TranRPT.setPaymentMethod("ONLINE BANKING");
					TranRPT.setPaymentChannel("FPX");
				}

				else if (TranRPT.getPaymentMethod().indexOf("Maybank2U") != -1) {

					TranRPT.setPaymentMethod("DEBIT ACCOUNT");
					TranRPT.setPaymentChannel("M2U");
				} else {

					TranRPT.setPaymentMethod("");
					TranRPT.setPaymentChannel("");
				}

				if (rs.getString("COVERAGE_START_DATE") == null) {

					TranRPT.setCoverageStartDate("");
				} else

				{

					String valueFromDB = rs.getString("COVERAGE_START_DATE");
					Date d1 = null;
					try {
						d1 = sdf1.parse(valueFromDB);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
					String startDateWithoutTime = sdf.format(d1);
					logger.info("sdf.format(d1) " + startDateWithoutTime);

					TranRPT.setCoverageStartDate(startDateWithoutTime);

				}

				if (rs.getString("COVERAGE_END_DATE") == null) {

					TranRPT.setCoverageEndDate("");
				} else

				{

					String valueFromDB = rs.getString("COVERAGE_END_DATE");
					Date d2 = null;
					try {
						d2 = sdf1.parse(valueFromDB);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
					String endDateWithoutTime = sdf.format(d2);
					logger.info("sdf.format(d2) " + endDateWithoutTime);

					TranRPT.setCoverageEndDate(endDateWithoutTime);

				}

				if (rs.getString("BENEFIT_CODE_1") == null) {

					TranRPT.setBenefitCode1("");
				} else

				{
					TranRPT.setBenefitCode1(rs.getString("BENEFIT_CODE_1"));

				}

				if (rs.getString("BENEFIT_CODE_2") == null) {

					TranRPT.setBenefitCode2("");
				} else

				{

					TranRPT.setBenefitCode2(rs.getString("BENEFIT_CODE_2"));

				}

				if (rs.getString("BENEFIT_CODE_3") == null) {

					TranRPT.setBenefitCode3("");
				} else

				{

					TranRPT.setBenefitCode3(rs.getString("BENEFIT_CODE_3"));

				}

				if (rs.getString("BENEFIT_CODE_4") == null) {

					TranRPT.setBenefitCode4("");
				} else

				{
					TranRPT.setBenefitCode4(rs.getString("BENEFIT_CODE_4"));

				}

				if (rs.getString("BENEFIT_CODE_5") == null) {

					TranRPT.setBenefitCode5("");
				} else

				{

					TranRPT.setBenefitCode5(rs.getString("BENEFIT_CODE_5"));

				}

				if (rs.getString("BENEFIT_CODE_6") == null) {

					TranRPT.setBenefitCode6("");
				} else

				{
					TranRPT.setBenefitCode6(rs.getString("BENEFIT_CODE_6"));

				}

				if (rs.getString("BENEFIT_CODE_7") == null) {

					TranRPT.setBenefitCode7("");
				} else

				{
					TranRPT.setBenefitCode7(rs.getString("BENEFIT_CODE_7"));

				}

				if (rs.getString("BENEFIT_CODE_8") == null) {

					TranRPT.setBenefitCode8("");
				} else

				{
					TranRPT.setBenefitCode8(rs.getString("BENEFIT_CODE_8"));

				}

				if (rs.getString("BENEFIT_CODE_9") == null) {

					TranRPT.setBenefitCode9("");
				} else

				{
					TranRPT.setBenefitCode9(rs.getString("BENEFIT_CODE_9"));
				}
				if (rs.getString("BENEFIT_CODE_10") == null) {

					TranRPT.setBenefitCode10("");
				} else

				{
					TranRPT.setBenefitCode10(rs.getString("BENEFIT_CODE_10"));
				}

				if (rs.getString("BENEFIT_TEXT_1") == null) {

					TranRPT.setBenefitText1("");
				} else

				{
					TranRPT.setBenefitText1(removeScriptTags(rs.getString("BENEFIT_TEXT_1")));

				}
				if (rs.getString("BENEFIT_TEXT_2") == null) {

					TranRPT.setBenefitText2("");
				} else

				{
					TranRPT.setBenefitText2(removeScriptTags(rs.getString("BENEFIT_TEXT_2")));

				}
				if (rs.getString("BENEFIT_TEXT_3") == null) {

					TranRPT.setBenefitText3("");
				} else

				{
					TranRPT.setBenefitText3(removeScriptTags(rs.getString("BENEFIT_TEXT_3")));

				}

				if (rs.getString("BENEFIT_TEXT_4") == null) {

					TranRPT.setBenefitText4("");
				} else

				{
					TranRPT.setBenefitText4(removeScriptTags(rs.getString("BENEFIT_TEXT_4")));

				}
				if (rs.getString("BENEFIT_TEXT_5") == null) {

					TranRPT.setBenefitText5("");
				} else

				{
					TranRPT.setBenefitText5(removeScriptTags(rs.getString("BENEFIT_TEXT_5")));

				}
				if (rs.getString("BENEFIT_TEXT_6") == null) {

					TranRPT.setBenefitText6("");
				} else

				{
					TranRPT.setBenefitText6(removeScriptTags(rs.getString("BENEFIT_TEXT_6")));

				}
				if (rs.getString("BENEFIT_TEXT_7") == null) {

					TranRPT.setBenefitText7("");
				} else

				{
					TranRPT.setBenefitText7(removeScriptTags(rs.getString("BENEFIT_TEXT_7")));

				}
				if (rs.getString("BENEFIT_TEXT_8") == null) {

					TranRPT.setBenefitText8("");
				} else

				{
					TranRPT.setBenefitText8(removeScriptTags(rs.getString("BENEFIT_TEXT_8")));

				}

				if (rs.getString("BENEFIT_TEXT_9") == null) {

					TranRPT.setBenefitText9("");
				} else

				{
					TranRPT.setBenefitText9(removeScriptTags(rs.getString("BENEFIT_TEXT_9")));

				}
				if (rs.getString("BENEFIT_TEXT_10") == null) {

					TranRPT.setBenefitText10("");
				} else

				{
					TranRPT.setBenefitText2(removeScriptTags(rs.getString("BENEFIT_TEXT_10")));

				}

				if (rs.getString("HOME_TYPE") == null) {

					TranRPT.setHomeType("");
				} else

				{
					TranRPT.setHomeType(rs.getString("HOME_TYPE"));

				}

				if (rs.getString("BUILDING_CONSTRUCTION_TYPE") == null) {

					TranRPT.setBuildConstructionType("");
				} else

				{
					TranRPT.setBuildConstructionType(rs.getString("BUILDING_CONSTRUCTION_TYPE"));

				}

				if (rs.getString("PROPERTY_INSURED_ADDRESS1") == null) {

					TranRPT.setPropertyInsuredAddress1("");
				} else

				{
					TranRPT.setPropertyInsuredAddress1(rs.getString("PROPERTY_INSURED_ADDRESS1"));

				}

				if (rs.getString("PROPERTY_INSURED_ADDRESS2") == null) {

					TranRPT.setPropertyInsuredAddress2("");
				} else

				{
					TranRPT.setPropertyInsuredAddress2(rs.getString("PROPERTY_INSURED_ADDRESS2"));

				}

				if (rs.getString("PROPERTY_INSURED_POSTCODE") == null) {

					TranRPT.setPropertyInsuredPostcode("");
				} else

				{
					TranRPT.setPropertyInsuredPostcode(rs.getString("PROPERTY_INSURED_POSTCODE"));

				}

				if (rs.getString("PROPERTY_INSURED_STATE") == null) {

					TranRPT.setPropertyInsuredState("");
				} else

				{
					TranRPT.setPropertyInsuredState(rs.getString("PROPERTY_INSURED_STATE"));

				}

				/*
				 * if(rs.getString("HOHH_TOTAL_PREMIUM_PAID") == null){
				 *
				 * TranRPT.setTotalPremiumPayable(""); }else
				 *
				 * { TranRPT.setTotalPremiumPayable(rs.getString("HOHH_TOTAL_PREMIUM_PAID"));
				 *
				 * }
				 */

				if (rs.getString("HOHH_GROSS_PREMIUM") == null) {

					TranRPT.setGrossPremiumFinal("");
				} else

				{
					TranRPT.setGrossPremiumFinal(rs.getString("HOHH_GROSS_PREMIUM"));

				}

				if (rs.getString("DISCOUNT_ID") == null) {

					TranRPT.setDiscount("");

				} else {
					TranRPT.setDiscount(rs.getString("DISCOUNT_ID"));

				}

				if (rs.getString("GROSS_PREMIUM_GST") == null) {

					TranRPT.setGST("");
				}

				else {
					TranRPT.setGST(rs.getString("GROSS_PREMIUM_GST"));

				}

				if (rs.getString("URQ2") == null) {

					TranRPT.setUW1("");
				}

				else {
					// TranRPT.setUW1(rs.getString("URQ1"));

					if (rs.getString("URQ2").equals("y")) {

						TranRPT.setUW1("All outside doors and windows have locks and deadlocks");

					} else {

						TranRPT.setUW1("");
					}

				}

				if (rs.getString("URQ3") == null) {

					TranRPT.setUW2("");
				}

				else {
					if (rs.getString("URQ3").equals("y")) {

						TranRPT.setUW2("All outside windows up to three storeys have security grills");

					} else {

						TranRPT.setUW2("");
					}

				}

				if (rs.getString("URQ4") == null) {

					TranRPT.setUW3("");
				}

				else {

					if (rs.getString("URQ4").equals("y")) {

						TranRPT.setUW3("All outside access points are covered by CCTV");

					} else {

						TranRPT.setUW3("");
					}

				}

				if (rs.getString("URQ5") == null) {

					TranRPT.setUW4("");
				}

				else {

					if (rs.getString("URQ5").equals("y")) {

						TranRPT.setUW4("Alarm system and CCTV (if any) are connected to a 24-hour response service");

					} else {

						TranRPT.setUW4("");
					}

				}

				if (rs.getString("URQ6") == null) {

					TranRPT.setUW5("");
				}

				else {

					if (rs.getString("URQ6").equals("y")) {

						TranRPT.setUW5("Permanent security guard");

					} else {

						TranRPT.setUW5("");
					}

				}

				if (rs.getString("QQ4_UDQ") == null) {

					TranRPT.setPDPA("");
				}

				else {

					if (rs.getString("QQ4_UDQ").equals("y")) {

						TranRPT.setPDPA("Yes");

					} else {

						TranRPT.setPDPA("No");
					}

				}

				if (rs.getString("PRODUCT_TYPE") == null) {

					TranRPT.setProductType("");

				} else

				{
					TranRPT.setProductType(rs.getString("PRODUCT_TYPE"));

				}

				/*
				 * if(rs.getString("HOME_COVERAGE") == null){
				 *
				 * TranRPT.setProductType(""); }
				 *
				 * else {
				 *
				 * if(rs.getString("HOME_COVERAGE").equals("building")){
				 *
				 * TranRPT.setProductType("HO");
				 *
				 * } else if(rs.getString("HOME_COVERAGE").equals("content")){
				 *
				 * TranRPT.setProductType("HH"); }
				 *
				 * else if(rs.getString("HOME_COVERAGE").equals("building_content")){
				 *
				 * TranRPT.setProductType("HOHH"); }
				 *
				 * }
				 *
				 *
				 */

				if (rs.getString("HOHH_POLICY_HOME_SUM_INS") == null) {

					TranRPT.setHOSI("");
					TranRPT.setHHSI("");
					TranRPT.setHOHHSI("");
				}

				else {

					if (rs.getString("HOME_COVERAGE").equals("building")) {

						TranRPT.setHOSI(rs.getString("HOHH_POLICY_HOME_SUM_INS"));

					} else if (rs.getString("HOME_COVERAGE").equals("content")) {

						TranRPT.setHHSI(rs.getString("HOHH_POLICY_HOME_SUM_INS"));
					}

					else if (rs.getString("HOME_COVERAGE").equals("building_content")) {

						TranRPT.setHOHHSI(rs.getString("HOHH_POLICY_HOME_SUM_INS"));
					}

				}

				if (rs.getString("COMMISSION") == null) {

					TranRPT.setCommission("");
				}

				else {
					TranRPT.setCommission(rs.getString("COMMISSION"));

				}

				if (rs.getString("CUSTOMER_GENDER") == null) {

					TranRPT.setCustomerGender("");
				}

				else {
					TranRPT.setCustomerGender(rs.getString("CUSTOMER_GENDER"));

				}

				if (rs.getString("CREATED_DATE") == null) {

					TranRPT.setQuotationCreateDateTime("");
				}

				else {
					TranRPT.setQuotationCreateDateTime(rs.getString("CREATED_DATE"));

				}

				if (rs.getString("QUOTATION_UPDATE_DATETIME") == null) {

					TranRPT.setQuotationUpdateDateTime("");
				}

				else {
					TranRPT.setQuotationUpdateDateTime(rs.getString("QUOTATION_UPDATE_DATETIME"));

				}

				if (rs.getString("ADD_BEN_RIOT_STRIKE_AMT") == null) {

					TranRPT.setAddBenRiotStrikeAmt("");
				}

				else {
					TranRPT.setAddBenRiotStrikeAmt(rs.getString("ADD_BEN_RIOT_STRIKE_AMT"));

				}

				if (rs.getString("ADD_BEN_EXTENDED_THEFT_AMT") == null) {

					TranRPT.setAddBenExtendedTheftAmt("");
				}

				else {
					TranRPT.setAddBenExtendedTheftAmt(rs.getString("ADD_BEN_EXTENDED_THEFT_AMT"));

				}

				if (rs.getString("HOME_COVERAGE") == null) {

					TranRPT.setHomeCoverage("");
				}

				else {
					TranRPT.setHomeCoverage(rs.getString("HOME_COVERAGE"));

				}

				if (rs.getString("HOME_SUM_INSURED") == null) {

					TranRPT.setHomeSumInsured("");

				} else

				{
					TranRPT.setHomeSumInsured(rs.getString("HOME_SUM_INSURED"));
				}

				if (rs.getString("CONTENT_SUM_INSURED") == null) {

					TranRPT.setContentSumInsured("");

				} else

				{
					TranRPT.setContentSumInsured(rs.getString("CONTENT_SUM_INSURED"));
				}

				if (rs.getString("ADD_BEN_RIOT_STRIKE") == null) {

					TranRPT.setAddBenRiotStrike("");

				} else

				{
					TranRPT.setAddBenRiotStrike(rs.getString("ADD_BEN_RIOT_STRIKE"));
				}

				if (rs.getString("ADD_BEN_EXTENDED_THEFT") == null) {

					TranRPT.setAddBenExtendedTheft("");

				} else

				{
					TranRPT.setAddBenExtendedTheft(rs.getString("ADD_BEN_EXTENDED_THEFT"));
				}

				if (rs.getString("LAST_PAGE") == null) {

					TranRPT.setLastPage("");

				} else

				{
					TranRPT.setLastPage(rs.getString("LAST_PAGE"));
				}
				TranRPT.setRecordCount(Integer.parseInt(rs.getString("RECORDCOUNT")));
				TranRPT.setTotalAmount(rs.getString("TOTALAMOUNT"));
				list.add(TranRPT);

			}

			return list;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}

	}

	@Override
	public List<SalesLeads> findBySalesLeads(String agentName, String agentCode, String fromDate, String toDate,
			int startRow, int endRow) {
		try {
			int i = startRow;
			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_REPORT_SALES(?,?,?,?,?,?,?)}");
			cstmt.setString(1, agentName);
			cstmt.setString(2, agentCode);
			cstmt.setString(3, fromDate);
			cstmt.setString(4, toDate);
			cstmt.setInt(5, startRow);
			cstmt.setInt(6, endRow);
			cstmt.registerOutParameter(7, OracleTypes.CURSOR);
			cstmt.execute();

			List<SalesLeads> list = new ArrayList<SalesLeads>();
			rs = (ResultSet) cstmt.getObject(7);
			while (rs.next()) {

				SalesLeads SalesLeadsRPT = new SalesLeads();
				SalesLeadsRPT.setID(i);

				if (rs.getString("CREATE_DATE") == null) {
					SalesLeadsRPT.setCREATE_DATE("");
				} else {
					SalesLeadsRPT.setCREATE_DATE(rs.getString("CREATE_DATE"));
				}
				if (rs.getString("CUSTOMER_NAME") == null) {
					SalesLeadsRPT.setCUSTOMER_NAME("");
				} else {
					SalesLeadsRPT.setCUSTOMER_NAME(rs.getString("CUSTOMER_NAME"));
				}
				if (rs.getString("VEHREGNO") == null) {
					SalesLeadsRPT.setVEHREGNO("");
				} else {
					SalesLeadsRPT.setVEHREGNO(rs.getString("VEHREGNO"));
				}
				if (rs.getString("EMAIL") == null) {
					SalesLeadsRPT.setEMAIL("");
				} else {
					SalesLeadsRPT.setEMAIL(rs.getString("EMAIL"));
				}
				if (rs.getString("PHONENO") == null) {
					SalesLeadsRPT.setPHONENO("");
				} else {
					SalesLeadsRPT.setPHONENO(rs.getString("PHONENO"));
				}

				if (rs.getString("RDTAXEXPIRY") == null) {
					SalesLeadsRPT.setRDTAXEXPIRY("");
				} else {
					SalesLeadsRPT.setRDTAXEXPIRY(rs.getString("RDTAXEXPIRY"));
				}

				if (rs.getString("AGENT_CODE") == null) {
					SalesLeadsRPT.setAGENT_CODE("");
				} else {
					SalesLeadsRPT.setAGENT_CODE(rs.getString("AGENT_CODE"));
				}
				if (rs.getString("AGENT_NAME") == null) {
					SalesLeadsRPT.setAGENT_NAME("");
				} else {
					SalesLeadsRPT.setAGENT_NAME(rs.getString("AGENT_NAME"));
				}
				i++;
				list.add(SalesLeadsRPT);

			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}
	///////////////////////////////////////////// END: ADD ON 6 MARCH
	///////////////////////////////////////////// 2017////////////////////////////////////////

	@Override
	// query result for view all
	public List<TransactionalReport> findByProductType(String productType, String productEntity, String status,
			String policyCertificateNo, String dateFrom, String dateTo, String NRIC, String receiptNo, int startRow,
			int endRow, String plateNo, String agentCode) {

		double totalAmount = 0.00;

		logger.info("productType " + productType);
		logger.info("status " + status);

		logger.info("AgentCode " + agentCode);

		try {
			int i = startRow;
			connection = ConnectionFactory.getConnection();
			// CallableStatement cstmt = connection.prepareCall("{CALL
			// DSP_ADM_SP_RPT_TRANSACTIONAL_S(?,?,?,?,?,?,?,?,?,?)}");
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_REPORT_ALL(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			// CallableStatement cstmt = connection.prepareCall("{CALL
			// DSP_ADM_SP_TRANSACTION_RPT_S6(?,?,?,?,?,?,?,?,?,?,?)}");
			cstmt.setString(1, productType.trim());
			cstmt.setString(2, productEntity.trim());
			cstmt.setString(3, status);
			cstmt.setString(4, policyCertificateNo);
			cstmt.setString(5, dateFrom);
			cstmt.setString(6, dateTo);
			cstmt.setString(7, NRIC);
			cstmt.setString(8, receiptNo);
			cstmt.setInt(9, startRow);
			cstmt.setInt(10, endRow);
			cstmt.setString(11, plateNo);
			cstmt.setString(12, agentCode);
			cstmt.registerOutParameter(13, OracleTypes.CURSOR);
			// cstmt.registerOutParameter(11, OracleTypes.CURSOR);
			cstmt.execute();

			List<TransactionalReport> list = new ArrayList<TransactionalReport>();
			rs = (ResultSet) cstmt.getObject(13);
			while (rs.next()) {

				TransactionalReport TranRPT = new TransactionalReport();
				TranRPT.setId(i);

				if (rs.getString("DSPQQID") == null) {

					TranRPT.setDSPQQID("");
				} else {

					TranRPT.setDSPQQID(rs.getString("DSPQQID"));
				}

				if (rs.getString("DSPTXNID") == null) {

					TranRPT.setTransactionID("");
				} else

				{
					TranRPT.setTransactionID(rs.getString("DSPTXNID"));

				}

				if (rs.getString("AMOUNT") == null) {

					TranRPT.setPremiumAmount("");

				} else {
					TranRPT.setPremiumAmount(rs.getString("AMOUNT"));

				}

				if (rs.getString("PMNT_GATEWAY_CODE") == null) {

					TranRPT.setPaymentMethod("");

				} else

				{
					TranRPT.setPaymentMethod(rs.getString("PMNT_GATEWAY_CODE"));

				}

				if (rs.getString("PMNT_STATUS") == null) {

					TranRPT.setStatus("");

				} else

				{
					TranRPT.setStatus(rs.getString("PMNT_STATUS"));

				}

				if (rs.getString("TRANSACTION_DATETIME") == null) {

					TranRPT.setTransactionDate("");

				} else

				{
					TranRPT.setTransactionDate(rs.getString("TRANSACTION_DATETIME"));

				}

				if (rs.getString("CREATED_DATE") == null) {

					TranRPT.setQuotationCreateDateTime("");
					;

				} else

				{
					TranRPT.setQuotationCreateDateTime(rs.getString("CREATED_DATE"));

				}

				if (rs.getString("INVOICE_NO") == null) {

					TranRPT.setInvoiceNo("");

				} else

				{
					TranRPT.setInvoiceNo(rs.getString("INVOICE_NO"));

				}

				if (TranRPT.getInvoiceNo().indexOf("EIB") != -1) {

					TranRPT.setProductEntity("EIB");

				}

				else if (TranRPT.getInvoiceNo().indexOf("ETB") != -1)

				{
					TranRPT.setProductEntity("ETB");

				} else {

					TranRPT.setProductEntity("EIB");

				}

				if (rs.getString("PRODUCT_CODE") == null) {

					TranRPT.setProductCode("");

				} else

				{
					TranRPT.setProductCode(rs.getString("PRODUCT_CODE"));

				}

				if (rs.getString("PRODUCT_CODE_NAME") == null) {

					TranRPT.setProductType("");

				} else

				{
					TranRPT.setProductType(rs.getString("PRODUCT_CODE_NAME"));

				}

				if (rs.getString("HOMECOVERAGE") != null) { // hohh

					TranRPT.setProductType(rs.getString("PRODUCT_TYPE"));

				}

				if (rs.getString("POLICY_NUMBER") == null) {

					TranRPT.setPolicyNo("");

				} else

				{
					TranRPT.setPolicyNo(rs.getString("POLICY_NUMBER"));

				}

				if (rs.getString("CUSTOMER_NAME") == null) {

					TranRPT.setCustomerName("");

				} else

				{

					TranRPT.setCustomerName(removeScriptTags(rs.getString("CUSTOMER_NAME")));

				}

				if (rs.getString("CUSTOMER_ID") == null) {

					TranRPT.setCustomerID("");

				} else

				{
					TranRPT.setCustomerID(rs.getString("CUSTOMER_ID"));

				}

				if (rs.getString("CUSTOMER_NRIC_ID") == null) {

					TranRPT.setCustomerNRIC("");

				} else

				{
					TranRPT.setCustomerNRIC(rs.getString("CUSTOMER_NRIC_ID"));

				}

				if (rs.getString("TERM") == null) {

					TranRPT.setTerm("");

				} else

				{
					TranRPT.setTerm(rs.getString("TERM"));

				}

				if (rs.getString("COVERAGE") == null) {

					TranRPT.setCoverage("");

				} else

				{
					TranRPT.setCoverage(rs.getString("COVERAGE"));

				}

				if (rs.getString("PREMIUM_MODE") == null) {

					TranRPT.setPremiumMode("");

				} else

				{

					if (rs.getString("PREMIUM_MODE").trim() != "") {

						String tmp = rs.getString("PREMIUM_MODE").substring(0, 1).toUpperCase()
								+ rs.getString("PREMIUM_MODE").substring(1);
						TranRPT.setPremiumMode(tmp);

					} else {

						TranRPT.setPremiumMode(rs.getString("PREMIUM_MODE"));

					}

				}

				if (rs.getString("MPAY_REFNO") == null) {

					TranRPT.setMPAY_RefNo("");

				} else

				{
					TranRPT.setMPAY_RefNo(rs.getString("MPAY_REFNO"));

				}

				if (rs.getString("MPAY_AUTHCODE") == null) {

					TranRPT.setMPAY_AuthCode("");

				} else

				{
					TranRPT.setMPAY_AuthCode(rs.getString("MPAY_AUTHCODE"));

				}

				if (rs.getString("EBPG_REFNO") == null) {

					TranRPT.setEBPG_RefNo("");

				} else

				{
					TranRPT.setEBPG_RefNo(rs.getString("EBPG_REFNO"));

				}

				if (rs.getString("EBPG_AUTHCODE") == null) {

					TranRPT.setEBPG_AuthCode("");

				} else

				{
					TranRPT.setEBPG_AuthCode(rs.getString("EBPG_AUTHCODE"));

				}

				if (rs.getString("M2U_REFNO") == null) {

					TranRPT.setM2U_RefNo("");

				} else

				{
					TranRPT.setM2U_RefNo(rs.getString("M2U_REFNO"));

				}

				if (rs.getString("M2U_AUTHCODE") == null) {

					TranRPT.setM2U_AuthCode("");

				} else

				{
					TranRPT.setM2U_AuthCode(rs.getString("M2U_AUTHCODE"));

				}

				if (rs.getString("FPX_REFNO") == null) {

					TranRPT.setFPX_RefNo("");

				} else

				{
					TranRPT.setFPX_RefNo(rs.getString("FPX_REFNO"));

				}

				if (rs.getString("FPX_AUTHCODE") == null) {

					TranRPT.setFPX_AuthCode("");

				} else

				{
					TranRPT.setFPX_AuthCode(rs.getString("FPX_AUTHCODE"));

				}

				TranRPT.setPaymentChannel("");
				if (TranRPT.getPaymentMethod().indexOf("CREDIT CARD") != -1) {

					TranRPT.setPaymentChannel("MPAY");

				} else if (TranRPT.getPaymentMethod().indexOf("EBPG") != -1) {

					TranRPT.setPaymentMethod("CREDIT CARD");
					TranRPT.setPaymentChannel("EBPG");
				}

				else if (TranRPT.getPaymentMethod().indexOf("AMEX") != -1) {

					TranRPT.setPaymentMethod("CREDIT CARD");
					TranRPT.setPaymentChannel("AMEX");
				}

				else if (TranRPT.getPaymentMethod().indexOf("FPX") != -1) {

					TranRPT.setPaymentMethod("ONLINE BANKING");
					TranRPT.setPaymentChannel("FPX");
				}

				else if (TranRPT.getPaymentMethod().indexOf("Maybank2U") != -1) {

					TranRPT.setPaymentMethod("DEBIT ACCOUNT");
					TranRPT.setPaymentChannel("M2U");
				}

				TranRPT.setRecordCount(Integer.parseInt(rs.getString("RECORDCOUNT")));
				TranRPT.setTotalAmount(rs.getString("TOTALAMOUNT"));

				if (rs.getString("VEHICLENO") == null) {

					TranRPT.setVehicleNo("");

				} else

				{
					TranRPT.setVehicleNo(rs.getString("VEHICLENO"));
				}
				if (rs.getString("POLICY_NUMBER_CAPS") == null) {

					TranRPT.setCAPSPolicyNo("");

				} else

				{
					TranRPT.setCAPSPolicyNo(rs.getString("POLICY_NUMBER_CAPS"));
				}

				// For HOHH Coverage -- Content, Building
				if (rs.getString("HOMECOVERAGE") == null) {

					TranRPT.setHomeCoverage("");

				} else

				{
					TranRPT.setHomeCoverage(rs.getString("HOMECOVERAGE"));
				}

				// For HOHH Coverage -- Content, Building
				if (rs.getString("HOMETYPE") == null) {

					TranRPT.setHomeType("");

				} else

				{
					TranRPT.setHomeType(rs.getString("HOMETYPE"));
				}
				if (rs.getString("HOMESUMINSURED") == null) {

					TranRPT.setHomeSumInsured("");

				} else

				{
					TranRPT.setHomeSumInsured(rs.getString("HOMESUMINSURED"));
				}

				if (rs.getString("ADDBENRIOTSTRIKE") == null) {

					TranRPT.setAddBenRiotStrike("");

				} else

				{
					TranRPT.setAddBenRiotStrike(rs.getString("ADDBENRIOTSTRIKE"));
				}

				if (rs.getString("ADDBENEXTENDEDTHEFT") == null) {

					TranRPT.setAddBenExtendedTheft("");

				} else

				{
					TranRPT.setAddBenExtendedTheft(rs.getString("ADDBENEXTENDEDTHEFT"));
				}

				if (rs.getString("CONTENTSUMINSURED") == null) {

					TranRPT.setContentSumInsured("");

				} else

				{
					TranRPT.setContentSumInsured(rs.getString("CONTENTSUMINSURED"));
				}

				if (rs.getString("TRAVELSTARTDATE") == null) {

					TranRPT.setTravelStartDate("");

				} else

				{
					TranRPT.setTravelStartDate(rs.getString("TRAVELSTARTDATE"));
				}

				if (rs.getString("TRAVELENDDATE") == null) {

					TranRPT.setTravelEndDate("");

				} else

				{
					TranRPT.setTravelEndDate(rs.getString("TRAVELENDDATE"));
				}

				if (!TranRPT.getTravelStartDate().equals(null) && !TranRPT.getTravelStartDate().equals("")) {

					if (!TranRPT.getTravelEndDate().equals(null) && !TranRPT.getTravelEndDate().equals("")) {
						DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");

						Date d1 = null;
						Date d2 = null;

						try {
							d1 = format.parse(TranRPT.getTravelStartDate());
							d2 = format.parse(TranRPT.getTravelEndDate());

							// in milliseconds
							long diff = d2.getTime() - d1.getTime();

							long diffSeconds = diff / 1000 % 60;
							long diffMinutes = diff / (60 * 1000) % 60;
							long diffHours = diff / (60 * 60 * 1000) % 24;
							long diffDays = diff / (24 * 60 * 60 * 1000);

							// System.out.print(diffDays + " days, ");
							// System.out.print(diffHours + " hours, ");
							// System.out.print(diffMinutes + " minutes, ");
							// System.out.print(diffSeconds + " seconds.");

							TranRPT.setTravelDuration(diffDays + 1);

						} catch (Exception e) {
							e.printStackTrace();
						}

					}
				}

				if (rs.getString("OFFEREDPLANNAME") == null || rs.getString("OFFEREDPLANNAME") == "") {

					TranRPT.setOfferedPlanName("");

				} else

				{

					TranRPT.setOfferedPlanName(rs.getString("OFFEREDPLANNAME").substring(0, 1).toUpperCase()
							+ rs.getString("OFFEREDPLANNAME").substring(1).toLowerCase());
				}

				if (rs.getString("TRAVELAREATYPE") == null) {

					TranRPT.setTravelAreaType("");

				} else

				{

					if (rs.getString("TRAVELAREATYPE").equals("Mal")) {

						TranRPT.setTravelAreaType("Domestic");
					} else {
						TranRPT.setTravelAreaType("International");

					}
				}

				if (rs.getString("TRAVELLINGWITH") == null) {

					TranRPT.setTravellingWith("");

				} else

				{
					TranRPT.setTravellingWith(rs.getString("TRAVELLINGWITH"));
				}

				if (rs.getString("AGENTCODETL") != "" && rs.getString("AGENTCODETL") != null) {

					TranRPT.setAgentCode(rs.getString("AGENTCODETL"));

				} else if (rs.getString("AGENTCODEMI") != "" && rs.getString("AGENTCODEMI") != null)

				{
					TranRPT.setAgentCode(rs.getString("AGENTCODEMI"));
				} else if (rs.getString("AGENTCODEWTC") != "" && rs.getString("AGENTCODEWTC") != null)

				{
					TranRPT.setAgentCode(rs.getString("AGENTCODEWTC"));
				} else if (rs.getString("AGENTCODEHOHH") != "" && rs.getString("AGENTCODEHOHH") != null)

				{
					TranRPT.setAgentCode(rs.getString("AGENTCODEHOHH"));
				} else {

					TranRPT.setAgentCode("");
				}

				if (rs.getString("AGENTNAMETL") != "" && rs.getString("AGENTNAMETL") != null) {

					TranRPT.setAgentName(rs.getString("AGENTNAMETL"));

				} else if (rs.getString("AGENTNAMEMI") != "" && rs.getString("AGENTNAMEMI") != null)

				{
					TranRPT.setAgentName(rs.getString("AGENTNAMEMI"));
				} else if (rs.getString("AGENTNAMEWTC") != "" && rs.getString("AGENTNAMEWTC") != null)

				{
					TranRPT.setAgentName(rs.getString("AGENTNAMEWTC"));
				} else if (rs.getString("AGENTNAMEHOHH") != "" && rs.getString("AGENTNAMEHOHH") != null)

				{
					TranRPT.setAgentName(rs.getString("AGENTNAMEHOHH"));
				}

				else {

					TranRPT.setAgentName("");
				}

				if (TranRPT.getCoverageStartDate() == null || TranRPT.getCoverageStartDate() == "") {

					if (rs.getString("TRAVELSTARTDATE") == null) {

						TranRPT.setCoverageStartDate("");

					} else

					{
						DateFormat format = new SimpleDateFormat("yyyy-mm-dd");

						try {
							TranRPT.setCoverageStartDate(ft.format(format.parse(rs.getString("TRAVELSTARTDATE"))));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				}

				if (TranRPT.getCoverageEndDate() == null || TranRPT.getCoverageEndDate() == "") {

					if (rs.getString("TRAVELENDDATE") == null) {

						TranRPT.setCoverageEndDate("");

					} else

					{
						DateFormat format = new SimpleDateFormat("yyyy-mm-dd");

						try {
							TranRPT.setCoverageEndDate(ft.format(format.parse(rs.getString("TRAVELENDDATE"))));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				}

				if (TranRPT.getCoverageStartDate() == null || TranRPT.getCoverageStartDate() == "") {
					if (rs.getString("HOHHSTARTDATE") == null) {

						TranRPT.setCoverageStartDate("");

					} else

					{
						DateFormat format = new SimpleDateFormat("yyyy-mm-dd");

						try {
							TranRPT.setCoverageStartDate(ft.format(format.parse(rs.getString("HOHHSTARTDATE"))));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

				if (TranRPT.getCoverageEndDate() == null || TranRPT.getCoverageEndDate() == "") {

					if (rs.getString("HOHHENDDATE") == null) {

						TranRPT.setCoverageEndDate("");

					} else

					{

						DateFormat format = new SimpleDateFormat("yyyy-mm-dd");

						try {
							TranRPT.setCoverageEndDate(ft.format(format.parse(rs.getString("HOHHENDDATE"))));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				if (TranRPT.getCoverageStartDate() == null || TranRPT.getCoverageStartDate() == "") {
					if (rs.getString("MOTORSTARTDATE") == null) {

						TranRPT.setCoverageStartDate("");

					} else

					{
						DateFormat format = new SimpleDateFormat("ddmmyyyy");

						try {
							TranRPT.setCoverageStartDate(ft.format(format.parse(rs.getString("MOTORSTARTDATE"))));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

				if (TranRPT.getCoverageEndDate() == null || TranRPT.getCoverageEndDate() == "") {
					if (rs.getString("MOTORENDDATE") == null) {

						TranRPT.setCoverageEndDate("");

					} else

					{
						DateFormat format = new SimpleDateFormat("ddmmyyyy");

						try {
							TranRPT.setCoverageEndDate(ft.format(format.parse(rs.getString("MOTORENDDATE"))));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}
				if (rs.getString("TLDURATION") == null) {

					TranRPT.setTerm("");

				} else

				{
					TranRPT.setCoverageEndDate(rs.getString("TLDURATION"));
				}

				if (rs.getString("QQID") == null) {

					TranRPT.setQQID("");

				} else

				{
					TranRPT.setQQID(rs.getString("QQID"));
				}

				if (rs.getString("PASSENGER_PA_PREMIUM_PAYABLE") == null) {

					TranRPT.setCapsAmount("");

				} else

				{
					TranRPT.setCapsAmount(rs.getString("PASSENGER_PA_PREMIUM_PAYABLE"));
				}

				if (rs.getString("PREMIUM_AFTER_GST") == null) {

					TranRPT.setMotorAmount(null);

				} else

				{
					// TranRPT.setMotorAmount(rs.getString("PREMIUM_AFTER_GST"));

					DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(Locale.US);
					format.setParseBigDecimal(true);
					BigDecimal amt1 = new BigDecimal(0.00);
					BigDecimal amt2 = new BigDecimal(10.00);
					BigDecimal amt3 = new BigDecimal(0.01);
					try {
						amt1 = (BigDecimal) format.parse(rs.getString("PREMIUM_AFTER_GST"));
						BigDecimal overallAmount = amt1.add(amt2);
						BigDecimal finalAmount = overallAmount.setScale(2, RoundingMode.CEILING);

						TranRPT.setMotorAmount(finalAmount);
						// logger.info("finalAmount "+finalAmount);

						/*
						 * if(TranRPT.getPremiumAmount() != TranRPT.getMotorAmount().toString() &&
						 * TranRPT.getCapsAmount().equals("")){
						 *
						 * finalAmount = finalAmount.subtract(amt3);
						 * TranRPT.setMotorAmount(finalAmount);
						 * //logger.info("finalAmount ... "+finalAmount); }
						 */

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				if (rs.getString("TLCHECKDIGIT") == null) {

					TranRPT.setTLCheckDigit("");

				} else

				{
					TranRPT.setTLCheckDigit(rs.getString("TLCHECKDIGIT"));
				}

				if (rs.getString("GROSS_PREMIUM") == null) {

					TranRPT.setGrossPremiumFinal("");

				} else

				{
					TranRPT.setGrossPremiumFinal(rs.getString("GROSS_PREMIUM"));
				}

				if (rs.getString("DISCOUNT") == null) {

					TranRPT.setDiscount("");

				} else

				{
					TranRPT.setDiscount(rs.getString("DISCOUNT"));
				}

				if (rs.getString("GST") == null) {

					TranRPT.setGST("");

				} else

				{
					TranRPT.setGST(rs.getString("GST"));
				}

				// logger.info(" id="+i+" "+rs.getString(1)+" "+rs.getString(2)+"
				// "+rs.getString(3)+" "+rs.getString(4)+" "+rs.getString(5));
				i++;

				// totalAmount = totalAmount+Integer.parseInt(TranRPT.getPremiumAmount());

				// logger.info(TranRPT.getPremiumAmount());
				// TranRPT.setTotalAmount(totalAmount);

				list.add(TranRPT);

			}

			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	@Override

	// query result for admin-transaction-detail.jsp
	public List<TransactionalReport> getTransactionDetail(int ID) {
		String SimplifiedResult = "";
		logger.info("ID " + ID);
		try {

			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt1 = connection.prepareCall("{CALL DSP_ADM_SP_ALLPROD_SIMPL(?,?)}");
			// CallableStatement cstmt = connection.prepareCall("{CALL
			// DSP_ADM_SP_RPT_TRANSACTIONAL_D(?,?)}");
			cstmt1.setInt(1, ID);
			cstmt1.registerOutParameter(2, OracleTypes.VARCHAR);
			cstmt1.execute();

			SimplifiedResult = cstmt1.getString(2);
			logger.info(SimplifiedResult + "SimplifiedResult" + ID);
			
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_REPORT_DETAIL(?,?)}");
			// CallableStatement cstmt = connection.prepareCall("{CALL
			// DSP_ADM_SP_RPT_TRANSACTIONAL_D(?,?)}");
			cstmt.setInt(1, ID);
			cstmt.registerOutParameter(2, OracleTypes.CURSOR);
			cstmt.execute();
			List<TransactionalReport> detail = new ArrayList<TransactionalReport>();
			rs = (ResultSet) cstmt.getObject(2);
			
			while (rs.next()) {

				TransactionalReport TranDetail = new TransactionalReport();
				// TranRPT.setId(i);
				// TranDetail.setPolicyNo(rs.getString(1));
				// TranDetail.setProductType(rs.getString(2));
				logger.info(SimplifiedResult + "In while Loop SimplifiedResult" + ID);
				TranDetail.setSimplifiedResult(SimplifiedResult);

				if (rs.getString("TRANSACTION_ID") == null) {

					TranDetail.setTransactionID("");

				} else

				{
					TranDetail.setTransactionID(rs.getString("TRANSACTION_ID"));

				}

				if (rs.getString("TRANSACTION_DATETIME") == null) {

					TranDetail.setTransactionDate("");

				} else

				{
					TranDetail.setTransactionDate(rs.getString("TRANSACTION_DATETIME"));

				}

				if (rs.getString("PMNT_STATUS") == null) {

					TranDetail.setStatus("");

				} else

				{
					TranDetail.setStatus(rs.getString("PMNT_STATUS"));

				}

				if (rs.getString("AMOUNT") == null) {

					TranDetail.setPremiumAmount("");

				} else

				{
					TranDetail.setPremiumAmount(rs.getString("AMOUNT"));

				}

				if (rs.getString("CUSTOMER_ID") == null) {

					TranDetail.setCustomerID("");

				} else

				{
					TranDetail.setCustomerID(rs.getString("CUSTOMER_ID"));

				}

				if (rs.getString("CUSTOMER_NRIC_ID") == null) {

					TranDetail.setCustomerNRIC("");

				} else

				{
					TranDetail.setCustomerNRIC(rs.getString("CUSTOMER_NRIC_ID"));

				}

				if (rs.getString("CUSTOMER_NAME") == null) {

					TranDetail.setCustomerName("");

				} else

				{
					TranDetail.setCustomerName(removeScriptTags(rs.getString("CUSTOMER_NAME")));

				}

				if (rs.getString("CUSTOMER_ADDRESS1") == null) {

					TranDetail.setCustomerAddress1("");

				} else

				{
					TranDetail.setCustomerAddress1(rs.getString("CUSTOMER_ADDRESS1"));

				}

				if (rs.getString("CUSTOMER_ADDRESS2") == null) {

					TranDetail.setCustomerAddress2("");

				} else

				{
					TranDetail.setCustomerAddress2(rs.getString("CUSTOMER_ADDRESS2"));

				}

				if (rs.getString("CUSTOMER_ADDRESS3") == null) {

					TranDetail.setCustomerAddress3("");

				} else

				{
					TranDetail.setCustomerAddress3(rs.getString("CUSTOMER_ADDRESS3"));

				}

				if (rs.getString("CUSTOMER_POSTCODE") == null) {

					TranDetail.setCustomerPostcode("");

				} else

				{
					TranDetail.setCustomerPostcode(rs.getString("CUSTOMER_POSTCODE"));

				}

				TranDetail.setCustomerCity("");

				if (rs.getString("CUSTOMER_STATE") == null) {

					TranDetail.setCustomerState("");

				} else

				{
					TranDetail.setCustomerState(rs.getString("CUSTOMER_STATE"));

				}

				if (rs.getString("CUSTOMER_COUNTRY") == null) {

					TranDetail.setCustomerCountry("");

				} else

				{
					TranDetail.setCustomerCountry(rs.getString("CUSTOMER_COUNTRY"));

				}

				if (rs.getString("CUSTOMER_MOBILE_NO") == null) {

					TranDetail.setCustomerMobileNo("");

				} else

				{
					TranDetail.setCustomerMobileNo(rs.getString("CUSTOMER_MOBILE_NO"));

				}

				if (rs.getString("CUSTOMER_EMAIL") == null) {

					TranDetail.setCustomerEmail("");

				} else

				{
					TranDetail.setCustomerEmail(rs.getString("CUSTOMER_EMAIL"));

				}

				if (rs.getString("DSP_QQ_ID") == null) {

					TranDetail.setDSPQQID("");

				} else

				{
					TranDetail.setDSPQQID(rs.getString("DSP_QQ_ID"));

				}

				if (rs.getString("PMNT_GATEWAY_CODE") == null) {

					TranDetail.setPaymentMethod("");

				} else

				{
					TranDetail.setPaymentMethod(rs.getString("PMNT_GATEWAY_CODE"));

				}

				TranDetail.setPaymentChannel("");
				if (rs.getString("PMNT_GATEWAY_CODE").indexOf("CREDIT CARD") != -1) {

					TranDetail.setPaymentChannel("MPAY");

				} else if (rs.getString("PMNT_GATEWAY_CODE").indexOf("EBPG") != -1) {

					TranDetail.setPaymentMethod("CREDIT CARD");
					TranDetail.setPaymentChannel("EBPG");
				} else if (rs.getString("PMNT_GATEWAY_CODE").indexOf("AMEX") != -1) {

					TranDetail.setPaymentMethod("CREDIT CARD");
					TranDetail.setPaymentChannel("AMEX");
				}

				else if (rs.getString("PMNT_GATEWAY_CODE").indexOf("FPX") != -1) {

					TranDetail.setPaymentMethod("ONLINE BANKING");
					TranDetail.setPaymentChannel("FPX");
				}

				else if (rs.getString("PMNT_GATEWAY_CODE").indexOf("Maybank2U") != -1) {

					TranDetail.setPaymentMethod("DEBIT ACCOUNT");
					TranDetail.setPaymentChannel("M2U");
				}

				if (rs.getString("PRODUCT_CODE_NAME") == null) {

					TranDetail.setProductType("");
				} else

				{
					TranDetail.setProductType(rs.getString("PRODUCT_CODE_NAME"));

				}
				if (rs.getString("BUDDY_COMBO") == null) {

					TranDetail.setBuddyCombo("");
				} else

				{

					TranDetail.setBuddyCombo(rs.getString("BUDDY_COMBO"));

				}

				if (rs.getString("PLAN_NAME") == null) {

					TranDetail.setPlanName("");
				} else

				{

					TranDetail.setPlanName(rs.getString("PLAN_NAME"));

				}
				if (this.rs.getString("TRIP_TYPE") == null) {
					TranDetail.setTripType("");
				} else {
					TranDetail.setTripType(this.rs.getString("TRIP_TYPE"));
				}
				if (this.rs.getString("D_FLIGHT_DT") == null) {
					TranDetail.setDepartDate("");
				} else {
					TranDetail.setDepartDate(this.rs.getString("D_FLIGHT_DT"));
				}
				if (this.rs.getString("R_FLIGHT_DT") == null) {
					TranDetail.setReturnDate("-");
				} else {
					TranDetail.setReturnDate(this.rs.getString("R_FLIGHT_DT"));
				}
				if (this.rs.getString("NUMBER_OF_COMPANION") == null) {
					TranDetail.setNumberOfCompanion(Integer.valueOf(0));
				} else {
					TranDetail.setNumberOfCompanion(Integer.valueOf(this.rs.getInt("NUMBER_OF_COMPANION")));
				}
				if (this.rs.getString("ADVENTUROUS_ACTIVITY") == null) {
					TranDetail.setAdventurousActivity("");
				} else {
					TranDetail.setAdventurousActivity(this.rs.getString("ADVENTUROUS_ACTIVITY"));
				}

				if (rs.getString("TERM") == null) {

					TranDetail.setTerm("");
				} else

				{
					TranDetail.setTerm(rs.getString("TERM"));

				}

				if (rs.getString("COVERAGE") == null) {

					TranDetail.setCoverage("");
				} else

				{
					TranDetail.setCoverage(rs.getString("COVERAGE"));

				}

				if (rs.getString("PREMIUM_MODE") == null) {

					TranDetail.setPremiumMode("");

				} else

				{

					if (rs.getString("PREMIUM_MODE").trim() != "") {

						String tmp = rs.getString("PREMIUM_MODE").substring(0, 1).toUpperCase()
								+ rs.getString("PREMIUM_MODE").substring(1);
						TranDetail.setPremiumMode(tmp);

					} else {

						TranDetail.setPremiumMode(rs.getString("PREMIUM_MODE"));

					}

				}

				if (rs.getString("MPAY_REFNO") == null) {

					TranDetail.setMPAY_RefNo("");

				} else

				{
					TranDetail.setMPAY_RefNo(rs.getString("MPAY_REFNO"));

				}

				if (rs.getString("MPAY_AUTHCODE") == null) {

					TranDetail.setMPAY_AuthCode("");

				} else

				{
					TranDetail.setMPAY_AuthCode(rs.getString("MPAY_AUTHCODE"));

				}

				if (rs.getString("EBPG_REFNO") == null) {

					TranDetail.setEBPG_RefNo("");

				} else

				{
					TranDetail.setEBPG_RefNo(rs.getString("EBPG_REFNO"));

				}

				if (rs.getString("EBPG_AUTHCODE") == null) {

					TranDetail.setEBPG_AuthCode("");

				} else

				{
					TranDetail.setEBPG_AuthCode(rs.getString("EBPG_AUTHCODE"));

				}

				if (rs.getString("M2U_REFNO") == null) {

					TranDetail.setM2U_RefNo("");

				} else

				{
					TranDetail.setM2U_RefNo(rs.getString("M2U_REFNO"));

				}

				if (rs.getString("M2U_AUTHCODE") == null) {

					TranDetail.setM2U_AuthCode("");

				} else

				{
					TranDetail.setM2U_AuthCode(rs.getString("M2U_AUTHCODE"));

				}

				if (rs.getString("FPX_REFNO") == null) {

					TranDetail.setFPX_RefNo("");

				} else

				{
					TranDetail.setFPX_RefNo(rs.getString("FPX_REFNO"));

				}

				if (rs.getString("FPX_AUTHCODE") == null) {

					TranDetail.setFPX_AuthCode("");
				} else

				{
					TranDetail.setFPX_AuthCode(rs.getString("FPX_AUTHCODE"));

				}

				if (rs.getString("POLICY_NUMBER") == null) {

					TranDetail.setPolicyNo("");
				} else

				{

					TranDetail.setPolicyNo(rs.getString("POLICY_NUMBER"));

				}

				if (rs.getString("AUDITREMARKS") == null) {

					TranDetail.setAuditRemarks("");
				} else

				{

					TranDetail.setAuditRemarks(rs.getString("AUDITREMARKS"));

				}

				if (rs.getString("VEHICLENO") == null) {

					TranDetail.setVehicleNo("");
				} else

				{

					TranDetail.setVehicleNo(rs.getString("VEHICLENO"));

				}

				if (rs.getString("VEHICLELOC") == null) {

					TranDetail.setVehicleLoc("");
				} else

				{

					TranDetail.setVehicleLoc(rs.getString("VEHICLELOC"));

				}

				if (rs.getString("VEHICLEMAKE") == null) {

					TranDetail.setVehicleMake("");
				} else

				{

					TranDetail.setVehicleMake(rs.getString("VEHICLEMAKE"));

				}

				if (rs.getString("VEHICLEMODEL") == null) {

					TranDetail.setVehicleModel("");
				} else

				{

					TranDetail.setVehicleModel(rs.getString("VEHICLEMODEL"));

				}

				if (rs.getString("YEARMAKE") == null) {

					TranDetail.setYearMake("");
				} else

				{

					TranDetail.setYearMake(rs.getString("YEARMAKE"));

				}

				if (rs.getString("CHASSISNO") == null) {

					TranDetail.setChassisNo("");
				} else

				{

					TranDetail.setChassisNo(rs.getString("CHASSISNO"));

				}

				if (rs.getString("FINANCEDBY") == null) {

					TranDetail.setFinancedBy("");
				} else

				{

					TranDetail.setFinancedBy(rs.getString("FINANCEDBY"));

				}

				if (rs.getString("COVERAGESTARTDATE") == null) {

					TranDetail.setCoverageStartDate("");
				} else

				{

					TranDetail.setCoverageStartDate(rs.getString("COVERAGESTARTDATE"));

				}

				if (rs.getString("COVERAGEENDDATE") == null) {

					TranDetail.setCoverageEndDate("");
				} else

				{

					TranDetail.setCoverageEndDate(rs.getString("COVERAGEENDDATE"));

				}

				if (rs.getString("ENGINENO") == null) {

					TranDetail.setEngineNo("");
				} else

				{

					TranDetail.setEngineNo(rs.getString("ENGINENO"));

				}

				if (rs.getString("NVIC") == null) {

					TranDetail.setNVIC("");
				} else

				{

					TranDetail.setNVIC(rs.getString("NVIC"));

				}

				if (rs.getString("TLCHECKDIGIT") == null) {

					TranDetail.setTLCheckDigit("");
				} else

				{

					TranDetail.setTLCheckDigit(rs.getString("TLCHECKDIGIT"));

				}

				if (rs.getString("QQID") == null) {

					TranDetail.setQQID("");
				} else

				{

					TranDetail.setQQID(rs.getString("QQID"));

				}

				if (rs.getString("JPJSTATUS") == null) {

					TranDetail.setJPJStatus("");
				} else

				{

					TranDetail.setJPJStatus(rs.getString("JPJSTATUS"));

				}
				if (rs.getString("JPJDOCTYPE") == null) {

					// TranRPT.setJPJStatus("");
					TranDetail.setJpjDocType("");
				} else

				{
					TranDetail.setJpjDocType(rs.getString("JPJDOCTYPE"));

				}
				if (rs.getString("JPJREASONCODE") == null) {

					// TranRPT.setJPJStatus("");
					TranDetail.setJpjReasonCode("");
				} else

				{
					TranDetail.setJpjReasonCode(rs.getString("JPJREASONCODE"));

				}
				if (rs.getString("POLICY_NUMBER_CAPS") == null) {

					TranDetail.setCAPSPolicyNo("");
				} else

				{

					TranDetail.setCAPSPolicyNo(rs.getString("POLICY_NUMBER_CAPS"));

				}

				if (rs.getString("HOMETYPE") == null) {

					TranDetail.setHomeType("");
				} else

				{

					TranDetail.setHomeType(rs.getString("HOMETYPE"));

				}

				if (rs.getString("BUILDCONSTRUCTIONTYPE") == null) {

					TranDetail.setBuildConstructionType("");
				} else

				{

					if (rs.getString("BUILDCONSTRUCTIONTYPE").equals("80%brick")) {

						TranDetail.setBuildConstructionType("80% Brick 20% Non-Flammable");

					} else if (rs.getString("BUILDCONSTRUCTIONTYPE").equals("100%brick")) {

						TranDetail.setBuildConstructionType("Full Brick");

					}

				}
				if (rs.getString("HOMESUMINSURED") == null) {

					TranDetail.setHomeSumInsured("");
				} else

				{

					TranDetail.setHomeSumInsured(rs.getString("HOMESUMINSURED"));

				}

				if (rs.getString("ADDBENRIOTSTRIKE") == null) {

					TranDetail.setAddBenRiotStrike("");
				} else

				{

					TranDetail.setAddBenRiotStrike(rs.getString("ADDBENRIOTSTRIKE"));

				}
				if (rs.getString("ADDBENEXTENDEDTHEFT") == null) {

					TranDetail.setAddBenExtendedTheft("");
				} else

				{

					TranDetail.setAddBenExtendedTheft(rs.getString("ADDBENEXTENDEDTHEFT"));

				}

				if (rs.getString("ADDBENRIOTSTRIKEAMT") == null) {

					TranDetail.setAddBenRiotStrikeAmt("");
				} else

				{

					TranDetail.setAddBenRiotStrikeAmt(rs.getString("ADDBENRIOTSTRIKEAMT"));

				}

				if (rs.getString("ADDBENEXTENDEDTHEFTAMT") == null) {

					TranDetail.setAddBenExtendedTheftAmt("");
				} else

				{

					TranDetail.setAddBenExtendedTheftAmt(rs.getString("ADDBENEXTENDEDTHEFTAMT"));

				}

				if (rs.getString("CONTENTSUMINSURED") == null) {

					TranDetail.setContentSumInsured("");
				} else

				{

					TranDetail.setContentSumInsured(rs.getString("CONTENTSUMINSURED"));

				}
				if (rs.getString("HOMECOVERAGE") == null) {

					TranDetail.setHomeCoverage("");
				} else

				{

					TranDetail.setHomeCoverage(rs.getString("HOMECOVERAGE"));

				}

				if (rs.getString("PREVIOUSINSURANCE") == null) {

					TranDetail.setPreviousInsurance("");
				} else

				{

					TranDetail.setPreviousInsurance(rs.getString("PREVIOUSINSURANCE"));

				}
				// logger.info(" id="+i+" "+rs.getString(1)+" "+rs.getString(2)+"
				// "+rs.getString(3)+" "+rs.getString(4)+" "+rs.getString(5));

				if (rs.getString("INS_COMPANY") == null) {

					TranDetail.setInsCompany("");
				} else

				{

					TranDetail.setInsCompany(rs.getString("INS_COMPANY"));

				}

				if (rs.getString("DRIVERSPA") == null) {

					TranDetail.setDriversPA("");
				} else

				{

					TranDetail.setDriversPA(rs.getString("DRIVERSPA"));

				}

				if (rs.getString("TPCD_PPA_QUESTION_ANS") == null) {

					TranDetail.setTPCDPPA("");
				} else

				{

					TranDetail.setTPCDPPA(rs.getString("TPCD_PPA_QUESTION_ANS"));

				}

				if (rs.getString("PASSENGGER_PA_PREMIUM") == null) {

					TranDetail.setPAPREMIUM("");
				} else

				{

					TranDetail.setPAPREMIUM(rs.getString("PASSENGGER_PA_PREMIUM"));

				}

				if (rs.getString("MI_QUOTATION_STATUS") == null) {

					TranDetail.setMIQUOTATIONSTATUS("");
				} else

				{

					TranDetail.setMIQUOTATIONSTATUS(rs.getString("MI_QUOTATION_STATUS"));

				}
				if (rs.getString("PASSENGER_PA_SELECTED") == null) {
					TranDetail.setPassengerPASelected("");
				} else {
					TranDetail.setPassengerPASelected(rs.getString("PASSENGER_PA_SELECTED"));
				}
				if (rs.getString("EHAILSTATUS") == null) {
					TranDetail.setEhailStatus("0");
				} else {
					TranDetail.setEhailStatus(rs.getString("EHAILSTATUS"));
				}
				
				detail.add(TranDetail);
			}
			return detail;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}

	}

	
	// query result for HOHH additional benefit
	@Override
	public List<TransactionalReport> getHOHHBenefit(int ID) {

		logger.info("ID " + ID);
		try {

			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_REPORT_DETAIL_HOHH(?,?)}");
			// CallableStatement cstmt = connection.prepareCall("{CALL
			// DSP_ADM_SP_RPT_TRANSACTIONAL_D(?,?)}");
			cstmt.setInt(1, ID);
			cstmt.registerOutParameter(2, OracleTypes.CURSOR);
			cstmt.execute();
			List<TransactionalReport> hohhBenefit = new ArrayList<TransactionalReport>();
			rs = (ResultSet) cstmt.getObject(2);
			while (rs.next()) {

				TransactionalReport TranDetailHOHHBenefit = new TransactionalReport();
				// TranRPT.setId(i);
				// TranDetail.setPolicyNo(rs.getString(1));
				// TranDetail.setProductType(rs.getString(2));

				if (rs.getString("ADDITIONAL_BENEFIT_CODE") == null) {

					TranDetailHOHHBenefit.setAdditionalBenefitCode("");

				} else

				{
					if (rs.getString("ADDITIONAL_BENEFIT_CODE").equals("oth")
							|| rs.getString("ADDITIONAL_BENEFIT_CODE").equals("Others")
							|| rs.getString("ADDITIONAL_BENEFIT_CODE").equals("Lain-Lain")) {

						TranDetailHOHHBenefit.setAdditionalBenefitCode("Others");
					}

					else if (rs.getString("ADDITIONAL_BENEFIT_CODE").equals("gol")
							|| rs.getString("ADDITIONAL_BENEFIT_CODE").equals("Gold")
							|| rs.getString("ADDITIONAL_BENEFIT_CODE").equals("Emas")) {

						TranDetailHOHHBenefit.setAdditionalBenefitCode("Gold");

					}

					else if (rs.getString("ADDITIONAL_BENEFIT_CODE").equals("sil")
							|| rs.getString("ADDITIONAL_BENEFIT_CODE").equals("Silver")
							|| rs.getString("ADDITIONAL_BENEFIT_CODE").equals("Perak")) {
						TranDetailHOHHBenefit.setAdditionalBenefitCode("Silver");

					} else if (rs.getString("ADDITIONAL_BENEFIT_CODE").equals("pla")
							|| rs.getString("ADDITIONAL_BENEFIT_CODE").equals("Platinum")) {

						TranDetailHOHHBenefit.setAdditionalBenefitCode("Platinum");

					} else if (rs.getString("ADDITIONAL_BENEFIT_CODE").equals("jew")
							|| rs.getString("ADDITIONAL_BENEFIT_CODE").equals("Jewellery")
							|| rs.getString("ADDITIONAL_BENEFIT_CODE").equals("Barang-Kemas")) {

						TranDetailHOHHBenefit.setAdditionalBenefitCode("Jewellery");

					} else if (rs.getString("ADDITIONAL_BENEFIT_CODE").equals("fur")
							|| rs.getString("ADDITIONAL_BENEFIT_CODE").equals("Furs")
							|| rs.getString("ADDITIONAL_BENEFIT_CODE").equals("Bulu-Haiwan")) {
						TranDetailHOHHBenefit.setAdditionalBenefitCode("Furs");

					}

				}

				if (rs.getString("ADDITIONAL_BENEFIT_VALUE") == null) {

					TranDetailHOHHBenefit.setAdditionalBenefitValue("");

				} else

				{
					TranDetailHOHHBenefit.setAdditionalBenefitValue(rs.getString("ADDITIONAL_BENEFIT_VALUE"));

				}

				if (rs.getString("ADDITIONAL_BENEFIT_TEXT") == null) {

					TranDetailHOHHBenefit.setAdditionalBenefitText("");

				} else

				{
					TranDetailHOHHBenefit.setAdditionalBenefitText(rs.getString("ADDITIONAL_BENEFIT_TEXT"));

				}

				hohhBenefit.add(TranDetailHOHHBenefit);
			}
			return hohhBenefit;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}

	}

	@Override
	// update customer detail in admin-transaction-detail.jsp
	public String updateCustomerInfo(int ID, String NRIC, String name, String email, String address1, String address2,
			String address3, String postcode, String state, String mobileNo) {

		logger.info(name + "name before update reportDAO");
		try {
			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_CUSTOMER_UPDATE(?,?,?,?,?,?,?,?,?,?)}");
			cstmt.setInt(1, ID);
			cstmt.setString(2, NRIC);
			cstmt.setString(3, name);
			cstmt.setString(4, email);
			cstmt.setString(5, address1);
			cstmt.setString(6, address2);
			cstmt.setString(7, address3);
			cstmt.setString(8, postcode);
			cstmt.setString(9, state);
			cstmt.setString(10, mobileNo);
			cstmt.executeUpdate();
			return "UPDATED";

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}

	}

	@Override
	// update transaction status, remark in admin-transaction-detail.jsp
	public String updateTxnInfo(int ID, String status, String remark, int moduleID, String action, int createdBy,
			String preData, String postData, String actionBy) {
		try {
			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_TRANSACTION_UPDATE(?,?,?,?,?,?,?,?,?)}");
			cstmt.setInt(1, ID);
			cstmt.setString(2, status);
			cstmt.setString(3, remark);
			cstmt.setInt(4, moduleID);
			cstmt.setString(5, action);
			cstmt.setInt(6, createdBy);
			cstmt.setString(7, preData);
			cstmt.setString(8, postData);
			cstmt.setString(9, actionBy);
			cstmt.executeUpdate();
			return "UPDATED";

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}

	}
	
	@Override
	// generate policy for motor in admin-transaction-detail.jsp
	public String generatePolicy_MP(int transactionID) {

		String QQID = null;
		// String Lang=null;

		try {
			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{CALL DSP_MP_SP_POLICY_UPDATE(?,?)}");
			cstmt.setInt(1, transactionID);
			cstmt.registerOutParameter(2, java.sql.Types.VARCHAR);
			cstmt.execute();

			QQID = (String) cstmt.getObject(2);
			// Lang= (String)cstmt.getObject(5);
			return QQID;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}

	}

	@Override
	// generate policy for motor in admin-transaction-detail.jsp
	public String generatePolicyNo(int transactionID) {

		String QQID = null;
		// String Lang=null;

		try {
			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_MI_POLICY_INSERT(?,?,?,?)}");

			cstmt.setInt(1, transactionID);
			cstmt.registerOutParameter(2, java.sql.Types.VARCHAR);
			cstmt.registerOutParameter(3, java.sql.Types.VARCHAR);
			cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
			// cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
			cstmt.execute();

			QQID = (String) cstmt.getObject(4);
			// Lang= (String)cstmt.getObject(5);
			return QQID;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}

	}

	@Override
	// generate policy for motor in admin-transaction-detail.jsp
	public String generateCAPSPolicy(int transactionID) {

		String QQID = null;
		// String Lang=null;

		try {
			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_MI_CAPS_INSERT(?,?,?,?)}");

			cstmt.setInt(1, transactionID);
			cstmt.registerOutParameter(2, java.sql.Types.VARCHAR);
			cstmt.registerOutParameter(3, java.sql.Types.VARCHAR);
			cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
			// cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
			cstmt.execute();

			QQID = (String) cstmt.getObject(4);
			// Lang= (String)cstmt.getObject(5);
			return QQID;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}

	}

	// generate policy for HOHH in admin-transaction-detail.jsp
	@Override
	public String generatePolicyNoHOHH(String DSPQQID, int transactionID) {
		//
		String policyNo = null;
		// String Lang=null;

		try {
			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_HOHH_POLICY_INSERT(?,?,?)}");

			cstmt.setString(1, DSPQQID);
			cstmt.setInt(2, transactionID);
			cstmt.registerOutParameter(3, java.sql.Types.VARCHAR);
			cstmt.execute();

			policyNo = (String) cstmt.getObject(3);
			// Lang= (String)cstmt.getObject(5);
			return policyNo;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}

	}

	// generate policy for wtc in admin-transaction-detail.jsp
	@Override
	public String generatePolicyNoWTC(String DSPQQID, int transactionID) {
		//
		String policyNo = null;
		// String Lang=null;

		try {
			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_WTC_POLICY_INSERT(?,?,?)}");

			cstmt.setString(1, DSPQQID);
			cstmt.setInt(2, transactionID);
			cstmt.registerOutParameter(3, java.sql.Types.VARCHAR);
			cstmt.execute();

			policyNo = (String) cstmt.getObject(3);
			// Lang= (String)cstmt.getObject(5);
			return policyNo;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	// generate policy for buddy in admin-transaction-detail.jsp
	@Override
	public String generatePolicyNoBuddy(String DSPQQID, int transactionID) {
		//
		String policyNo = null;
		// String Lang=null;

		try {
			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{CALL DSP_BUDDY_SP_POLICY_INSERT(?,?,?)}");

			cstmt.setString(1, DSPQQID);
			cstmt.setInt(2, transactionID);
			cstmt.registerOutParameter(3, java.sql.Types.VARCHAR);
			cstmt.execute();

			policyNo = (String) cstmt.getObject(3);
			// Lang= (String)cstmt.getObject(5);
			return policyNo;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	// generate policy for Travel Ezy in admin-transaction-detail.jsp
	@Override
	public String generatePolicyNoTravEz(String DSPQQID, int transactionID) {
		//
		String policyNo = null;
		// String Lang=null;

		try {
			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{CALL DSP_TRAVEZY_SP_POLICY_INSERT(?,?,?)}");

			cstmt.setString(1, DSPQQID);
			cstmt.setInt(2, transactionID);
			cstmt.registerOutParameter(3, java.sql.Types.VARCHAR);
			cstmt.execute();

			policyNo = (String) cstmt.getObject(3);
			// Lang= (String)cstmt.getObject(5);
			return policyNo;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	// generate policy for TripCare in admin-transaction-detail.jsp
	@Override
	public String generatePolicyNoTripCare(String DSPQQID, int transactionID) {
		//
		String policyNo = null;
		// String Lang=null;

		try {
			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{CALL DSP_TC360_SP_POLICY_INSERT(?,?,?)}");

			cstmt.setString(1, DSPQQID);
			cstmt.setInt(2, transactionID);
			cstmt.registerOutParameter(3, java.sql.Types.VARCHAR);
			cstmt.execute();

			policyNo = (String) cstmt.getObject(3);
			// Lang= (String)cstmt.getObject(5);
			return policyNo;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	@Override

	public List<TransactionalReport> getPolicyDetail(String QQID) {

		// logger.info("QQID" +QQID);
		/*
		 * try {
		 *
		 * connection = ConnectionFactory.getConnection(); CallableStatement cstmt =
		 * connection.
		 * prepareCall("{CALL DSP_ADM_SP_MI_TOTALINFO(?,?,?,?,?,?,?,?,?,?,?)}");
		 * cstmt.setString(1,QQID); cstmt.registerOutParameter(2,OracleTypes.CURSOR);
		 * cstmt.execute(); List<TransactionalReport> policyDetail = new
		 * ArrayList<TransactionalReport>(); rs = (ResultSet)cstmt.getObject(2);
		 * while(rs.next()) {
		 *
		 * TransactionalReport PolicyDetailList = new TransactionalReport();
		 * policyDetail.add(PolicyDetailList); } return policyDetail; } catch
		 * (SQLException e) { e.printStackTrace(); return null; } finally {
		 * DBUtil.close(rs); DBUtil.close(statement); DBUtil.close(connection); }
		 */
		return null;
	}

	public int gettingValueisNullOrNot(int indexVal, ResultSet rs) {
		int iVal = 0;
		try {
			// logger.info(indexVal+"indexVal");
			iVal = rs.getInt(indexVal);
			if (rs.wasNull()) {
				// logger.info(indexVal+"indexVal value null or empty");
				iVal = 0;
			}
			logger.info(iVal);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return iVal;
	}

	public Double gettingValueisNullOrNotInDouble(int indexVal, ResultSet rs) {
		Double iVal = null;
		try {
			// logger.info(indexVal+"indexVal");
			iVal = (double) rs.getDouble(indexVal);
			DecimalFormat df = new DecimalFormat("0.00");
			// logger.info(df.format(iVal));
			String formatVal = df.format(iVal);
			iVal = Double.parseDouble(formatVal);
			if (rs.wasNull()) {
				// logger.info(indexVal+"indexVal value null or empty In Double");
				iVal = (double) 0;
			}
			logger.info(iVal);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return iVal;
	}

	public String gettingValueisNullOrNotInString(int indexVal, ResultSet rs) {
		String iVal = null;
		try {
			// logger.info(indexVal+"indexVal");
			iVal = rs.getString(indexVal);
			Double Value = Double.parseDouble(iVal);
			iVal = String.format("%.2f", Value);
			if (rs.wasNull()) {

				// logger.info(indexVal+"indexVal value null or empty In Double");
				iVal = "0.00";
			}

			logger.info(iVal);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return iVal;
	}

	// GET WTC RECORD
	@Override
	public List<TransactionalReport> getWTCRecord(String productEntity, String status, String policyCertificateNo,
			String dateFrom, String dateTo, String NRIC, String receiptNo, String agentCode) {
		ResultSet rs = null;
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		try {

			int i = 1;
			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_WTC_ALL_TMP(?,?,?,?,?,?,?,?,?)}");
			cstmt.setString(1, productEntity.trim());
			cstmt.setString(2, status);
			cstmt.setString(3, policyCertificateNo);
			cstmt.setString(4, dateFrom);
			cstmt.setString(5, dateTo);
			cstmt.setString(6, NRIC);
			cstmt.setString(7, receiptNo);
			cstmt.setString(8, agentCode);
			cstmt.registerOutParameter(9, OracleTypes.CURSOR);
			cstmt.execute();
			;

			List<TransactionalReport> list = new ArrayList<TransactionalReport>();
			rs = (ResultSet) cstmt.getObject(9);
			while (rs.next()) {
				TransactionalReport TranRPT = new TransactionalReport();
				TranRPT.setId(i);
				if (rs.getString("TRANSACTIONID") == null) {

					TranRPT.setTransactionID("");
				} else {

					TranRPT.setTransactionID(rs.getString("TRANSACTIONID"));
				}

				if (rs.getString("PMNT_GATEWAY_CODE") == null) {

					TranRPT.setPaymentMethod("");
				} else

				{
					TranRPT.setPaymentMethod(rs.getString("PMNT_GATEWAY_CODE"));

				}

				if (rs.getString("AMOUNT") == null) {

					TranRPT.setPremiumAmount("");

				} else {
					TranRPT.setPremiumAmount(rs.getString("AMOUNT"));

				}

				if (rs.getString("PMNT_STATUS") == null) {

					TranRPT.setStatus("");

				} else

				{
					TranRPT.setStatus(rs.getString("PMNT_STATUS"));

				}

				// logger.info("rs.getString('TRANSACTION_DATETIME')
				// "+rs.getString("TRANSACTION_DATETIME"));
				if (rs.getString("TRANSACTION_DATETIME") == null) {

					TranRPT.setTransactionDate("");

				} else

				{
					String tmpDate = "";

					try {
						tmpDate = ft2.format(format1.parse(rs.getString("TRANSACTION_DATETIME")));

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					TranRPT.setTransactionDate(tmpDate);
				}

				if (rs.getString("INVOICE_NO") == null) {

					TranRPT.setInvoiceNo("");

				} else

				{
					TranRPT.setInvoiceNo(rs.getString("INVOICE_NO"));

				}

				/*
				 * if(TranRPT.getInvoiceNo().indexOf("EIB")!=-1){
				 *
				 * TranRPT.setProductEntity("EIB");
				 *
				 * }
				 *
				 * else if(TranRPT.getInvoiceNo().indexOf("ETB")!=-1)
				 *
				 * { TranRPT.setProductEntity("ETB");
				 *
				 * } else{
				 *
				 * TranRPT.setProductEntity("EIB");
				 *
				 * }
				 */

				if (rs.getString("DSP_QQ_ID") == null) {

					TranRPT.setDSPQQID("");

				} else

				{
					TranRPT.setDSPQQID(rs.getString("DSP_QQ_ID"));

				}

				if (rs.getString("PRODUCT_ID") == null) {

					TranRPT.setProductCode("");

				} else

				{
					TranRPT.setProductCode(rs.getString("PRODUCT_ID"));

				}

				if (TranRPT.getProductCode().equals("WTC")) {

					TranRPT.setProductEntity("EIB");

				} else {

					TranRPT.setProductEntity("ETB");

				}

				// logger.info("PRODUCT_CODE_NAME "+rs.getString("PRODUCT_CODE_NAME"));
				if (rs.getString("PRODUCT_CODE_NAME") == null) {

					TranRPT.setProductType("");

				} else

				{
					TranRPT.setProductType(rs.getString("PRODUCT_CODE_NAME"));

				}

				if (rs.getString("AGENT_CODE") == null) {

					TranRPT.setAgentCode("");

				} else

				{
					TranRPT.setAgentCode(rs.getString("AGENT_CODE"));

				}

				if (rs.getString("AGENT_NAME") == null) {

					TranRPT.setAgentName("");

				} else

				{
					TranRPT.setAgentName(rs.getString("AGENT_NAME"));

				}

				if (rs.getString("OPERATOR_CODE") == null) {

					TranRPT.setOperatorCode("");

				} else

				{
					TranRPT.setOperatorCode(rs.getString("OPERATOR_CODE"));

				}

				if (rs.getString("IDTYPE") == null) {

					TranRPT.setIDType("");

				} else

				{
					TranRPT.setIDType(rs.getString("IDTYPE"));

				}

				if (rs.getString("CUSTOMER_NRIC_ID") == null) {

					TranRPT.setCustomerNRIC("");

				} else

				{
					TranRPT.setCustomerNRIC(rs.getString("CUSTOMER_NRIC_ID"));

				}

				if (rs.getString("CUSTOMER_NAME") == null) {

					TranRPT.setCustomerName("");

				} else

				{
					TranRPT.setCustomerName(removeScriptTags(rs.getString("CUSTOMER_NAME")));

				}

				if (rs.getString("CUSTOMER_GENDER") == null) {

					TranRPT.setCustomerGender("");

				} else

				{
					TranRPT.setCustomerGender(rs.getString("CUSTOMER_GENDER"));

				}

				if (rs.getString("CUSTOMER_ADDRESS1") == null) {

					TranRPT.setCustomerAddress1("");

				} else

				{
					TranRPT.setCustomerAddress1(rs.getString("CUSTOMER_ADDRESS1"));

				}

				if (rs.getString("CUSTOMER_ADDRESS2") == null) {

					TranRPT.setCustomerAddress2("");

				} else

				{
					TranRPT.setCustomerAddress2(rs.getString("CUSTOMER_ADDRESS2"));

				}

				if (rs.getString("CUSTOMER_ADDRESS3") == null) {

					TranRPT.setCustomerAddress3("");

				} else

				{
					TranRPT.setCustomerAddress3(rs.getString("CUSTOMER_ADDRESS3"));

				}

				if (rs.getString("CUSTOMER_POSTCODE") == null) {

					TranRPT.setCustomerPostcode("");

				} else

				{
					TranRPT.setCustomerPostcode(rs.getString("CUSTOMER_POSTCODE"));

				}

				if (rs.getString("CUSTOMER_STATE") == null) {

					TranRPT.setCustomerState("");

				} else

				{
					TranRPT.setCustomerState(rs.getString("CUSTOMER_STATE"));

				}

				if (rs.getString("CUSTOMER_EMAIL") == null) {

					TranRPT.setCustomerEmail("");

				} else

				{
					TranRPT.setCustomerEmail(rs.getString("CUSTOMER_EMAIL"));

				}

				if (rs.getString("CUSTOMER_MOBILE_NO") == null) {

					TranRPT.setCustomerMobileNo("");

				} else

				{
					TranRPT.setCustomerMobileNo(rs.getString("CUSTOMER_MOBILE_NO"));

				}

				if (rs.getString("POLICY_NUMBER") == null) {

					TranRPT.setPolicyNo("");

				} else

				{
					TranRPT.setPolicyNo(rs.getString("POLICY_NUMBER"));

				}

				if (rs.getString("POLICY_STATUS") == null) {

					TranRPT.setPolicyStatus("");

				} else

				{
					TranRPT.setPolicyStatus(rs.getString("POLICY_STATUS"));

				}

				if (rs.getString("EBPG_REFNO") == null) {

					TranRPT.setEBPG_RefNo("");
				} else

				{
					TranRPT.setEBPG_RefNo(rs.getString("EBPG_REFNO"));

				}

				if (rs.getString("EBPG_AUTHCODE") == null) {

					TranRPT.setEBPG_AuthCode("");
				} else

				{
					TranRPT.setEBPG_AuthCode(rs.getString("EBPG_AUTHCODE"));

				}

				if (rs.getString("M2U_REFNO") == null) {

					TranRPT.setM2U_RefNo("");
				} else

				{
					TranRPT.setM2U_RefNo(rs.getString("M2U_REFNO"));

				}

				if (rs.getString("M2U_AUTHCODE") == null) {

					TranRPT.setM2U_AuthCode("");
				} else

				{
					TranRPT.setM2U_AuthCode(rs.getString("M2U_AUTHCODE"));

				}

				if (rs.getString("FPX_REFNO") == null) {

					TranRPT.setFPX_RefNo("");
				} else

				{
					TranRPT.setFPX_RefNo(rs.getString("FPX_REFNO"));

				}

				if (rs.getString("FPX_AUTHCODE") == null) {

					TranRPT.setFPX_AuthCode("");
				} else

				{
					TranRPT.setFPX_AuthCode(rs.getString("FPX_AUTHCODE"));

				}

				if (TranRPT.getPaymentMethod().indexOf("EBPG") != -1) {

					TranRPT.setPaymentMethod("CREDIT CARD");
					TranRPT.setPaymentChannel("EBPG");
				}

				else if (TranRPT.getPaymentMethod().indexOf("AMEX") != -1) {

					TranRPT.setPaymentMethod("CREDIT CARD");
					TranRPT.setPaymentChannel("AMEX");
				}

				else if (TranRPT.getPaymentMethod().indexOf("FPX") != -1) {

					TranRPT.setPaymentMethod("ONLINE BANKING");
					TranRPT.setPaymentChannel("FPX");
				}

				else if (TranRPT.getPaymentMethod().indexOf("Maybank2U") != -1) {

					TranRPT.setPaymentMethod("DEBIT ACCOUNT");
					TranRPT.setPaymentChannel("M2U");
				} else {
					TranRPT.setPaymentMethod("");
					TranRPT.setPaymentChannel("");

				}

				if (rs.getString("TRAVEL_START_DATE") == null) {

					TranRPT.setCoverageStartDate("");
				} else

				{

					// TranRPT.setCoverageStartDate(rs.getString("TRAVEL_START_DATE"));

					String valueFromDB = rs.getString("TRAVEL_START_DATE");
					Date d1 = null;
					try {
						d1 = sdf1.parse(valueFromDB);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
					String startDateWithoutTime = sdf.format(d1);
					// logger.info("sdf.format(d1) " + startDateWithoutTime);

					TranRPT.setCoverageStartDate(startDateWithoutTime);

				}

				if (rs.getString("TRAVEL_END_DATE") == null) {

					TranRPT.setCoverageEndDate("");
				} else

				{

					// TranRPT.setCoverageEndDate(rs.getString("TRAVEL_END_DATE"));

					String valueFromDB = rs.getString("TRAVEL_END_DATE");
					Date d2 = null;
					try {
						d2 = sdf1.parse(valueFromDB);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
					String endDateWithoutTime = sdf.format(d2);
					// logger.info("sdf.format(d2) " + endDateWithoutTime);

					TranRPT.setCoverageEndDate(endDateWithoutTime);

				}

				if (rs.getString("TRAVEL_START_DATE") == null) {

					TranRPT.setTravelStartDate("");

				} else

				{
					TranRPT.setTravelStartDate(rs.getString("TRAVEL_START_DATE"));
				}

				if (rs.getString("TRAVEL_END_DATE") == null) {

					TranRPT.setTravelEndDate("");

				} else

				{
					TranRPT.setTravelEndDate(rs.getString("TRAVEL_END_DATE"));
				}

				if (!TranRPT.getTravelStartDate().equals(null) && !TranRPT.getTravelStartDate().equals("")) {

					if (!TranRPT.getTravelEndDate().equals(null) && !TranRPT.getTravelEndDate().equals("")) {
						DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");

						Date d1 = null;
						Date d2 = null;

						try {
							d1 = format.parse(TranRPT.getTravelStartDate());
							d2 = format.parse(TranRPT.getTravelEndDate());

							// in milliseconds
							long diff = d2.getTime() - d1.getTime();

							long diffSeconds = diff / 1000 % 60;
							long diffMinutes = diff / (60 * 1000) % 60;
							long diffHours = diff / (60 * 60 * 1000) % 24;
							long diffDays = diff / (24 * 60 * 60 * 1000);

							// System.out.print(diffDays + " days, ");
							// System.out.print(diffHours + " hours, ");
							// System.out.print(diffMinutes + " minutes, ");
							// System.out.print(diffSeconds + " seconds.");

							TranRPT.setTravelDuration(diffDays + 1);
							logger.info("travel duration=" + TranRPT.getTravelDuration());

						} catch (Exception e) {
							e.printStackTrace();
						}

					}
				}

				if (rs.getString("OFFERED_PLAN_NAME") == null || rs.getString("OFFERED_PLAN_NAME").equals("1")) {

					TranRPT.setOfferedPlanName("");
				} else

				{

					TranRPT.setOfferedPlanName(rs.getString("OFFERED_PLAN_NAME"));

				}

				if (rs.getString("TRAVEL_AREA_TYPE") == null) {

					TranRPT.setTravelAreaType("");
				} else

				{

					// ranRPT.setTravelAreaType(rs.getString("TRAVEL_AREA_TYPE"));

					if (rs.getString("TRAVEL_AREA_TYPE").equals("Mal")) {

						TranRPT.setTravelAreaType("Domestic");
					} else {
						TranRPT.setTravelAreaType("International");

					}

				}

				if (rs.getString("AREA_CODE") == null) {

					TranRPT.setAreaCode("");
				} else {

					TranRPT.setAreaCode(rs.getString("AREA_CODE"));
				}

				if (rs.getString("TRAVLLING_WITH") == null) {

					TranRPT.setTravellingWith("");
				} else

				{

					TranRPT.setTravellingWith(rs.getString("TRAVLLING_WITH"));

				}

				if (rs.getString("PROMOTION_CODE") == null) {

					TranRPT.setDiscountCode("");
				} else

				{

					TranRPT.setDiscountCode(rs.getString("PROMOTION_CODE"));

				}

				if (rs.getString("TOTAL_PREMIUM_PAID") == null) {

					TranRPT.setTotalPremiumPayable("");
				} else

				{
					TranRPT.setTotalPremiumPayable(rs.getString("TOTAL_PREMIUM_PAID"));

				}

				if (rs.getString("WT_GROSS_PREMIUM") == null) {

					TranRPT.setGrossPremiumFinal("");
				} else

				{
					TranRPT.setGrossPremiumFinal(rs.getString("WT_GROSS_PREMIUM"));

				}

				if (rs.getString("DISCOUNT_AMOUNT") == null) {

					TranRPT.setDiscount("");

				} else {
					TranRPT.setDiscount(rs.getString("DISCOUNT_AMOUNT"));

				}

				if (rs.getString("GROSS_PREMIUM_GST") == null) {

					TranRPT.setGST("");
				}

				else {
					TranRPT.setGST(rs.getString("GROSS_PREMIUM_GST"));

				}

				if (rs.getString("COMMISSION_AMOUNT") == null) {

					TranRPT.setCommission("");
				}

				else {
					TranRPT.setCommission(rs.getString("COMMISSION_AMOUNT"));

				}

				if (rs.getString("CUSTOMER_GENDER") == null) {

					TranRPT.setCustomerGender("");
				}

				else {
					TranRPT.setCustomerGender(rs.getString("CUSTOMER_GENDER"));

				}

				if (rs.getString("CREATED_DATE") == null) {

					TranRPT.setQuotationCreateDateTime("");
				}

				else {
					TranRPT.setQuotationCreateDateTime(rs.getString("CREATED_DATE"));

				}

				TranRPT.setQuotationUpdateDateTime("");
				/*
				 * if(rs.getString("TRANSACTION_DATETIME") == null){
				 *
				 * TranRPT.setQuotationUpdateDateTime(""); }
				 *
				 * else {
				 *
				 *
				 * String valueFromDB = rs.getString("TRANSACTION_DATETIME"); Date d1 = null;
				 * try { d1 = sdf2.parse(valueFromDB); } catch (ParseException e) { // TODO
				 * Auto-generated catch block e.printStackTrace(); } SimpleDateFormat sdf = new
				 * SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); String transactionDateTime =
				 * sdf.format(d1); //logger.info("txn date wtc " + transactionDateTime);
				 * TranRPT.setQuotationUpdateDateTime(transactionDateTime);
				 *
				 * }
				 */

				if (rs.getString("SPOUSE_NAME") == null) {

					TranRPT.setSpouseName("");
				}

				else {
					TranRPT.setSpouseName(rs.getString("SPOUSE_NAME"));

				}

				if (rs.getString("SPOUSE_EMAIL") == null) {

					TranRPT.setSpouseEmail("");
				}

				else {
					TranRPT.setSpouseEmail(rs.getString("SPOUSE_EMAIL"));

				}

				if (rs.getString("SPOUSE_ID_TYPE") == null) {

					TranRPT.setSpouseIDType("");
				}

				else {
					TranRPT.setSpouseIDType(rs.getString("SPOUSE_ID_TYPE"));

				}

				if (rs.getString("SPOUSE_ID_NUMBER") == null) {

					TranRPT.setSpouseIDNumber("");
				}

				else {
					TranRPT.setSpouseIDNumber(rs.getString("SPOUSE_ID_NUMBER"));

				}

				if (rs.getString("SPOUSE_DOB") == null) {

					TranRPT.setSpouseDOB("");
				}

				else {
					TranRPT.setSpouseDOB(rs.getString("SPOUSE_DOB"));

				}

				if (rs.getString("SPOUSE_GENDER") == null) {

					TranRPT.setSpouseGender("");
				}

				else {
					TranRPT.setSpouseGender(rs.getString("SPOUSE_GENDER"));

				}

				if (rs.getString("CHILD_NAME") == null) {

					TranRPT.setChildName("");
				}

				else {

					// later plan, set TranRPT.setChildName as array

					TranRPT.setChildName(rs.getString("CHILD_NAME"));

					if (TranRPT.getChildName().indexOf(",") != -1) {

						String[] childName = TranRPT.getChildName().split(",");

						int childCount = childName.length;

						for (int j = 0; j < childCount; j++) {

							if (j == 0) {
								TranRPT.setChildName_1(childName[j]);
							}
							if (j == 1) {
								TranRPT.setChildName_2(childName[j]);
							}
							if (j == 2) {
								TranRPT.setChildName_3(childName[j]);
							}
							if (j == 3) {
								TranRPT.setChildName_4(childName[j]);
							}
							if (j == 4) {
								TranRPT.setChildName_5(childName[j]);
							}

							if (j == 5) {
								TranRPT.setChildName_6(childName[j]);
							}

							if (j == 6) {
								TranRPT.setChildName_7(childName[j]);
							}

						}

					}

					else {
						TranRPT.setChildName_1(rs.getString("CHILD_NAME"));

					}

				}

				if (rs.getString("CHILD_ID_TYPE") == null) {

					TranRPT.setChildIDType("");
				}

				else {
					TranRPT.setChildIDType(rs.getString("CHILD_ID_TYPE"));

					if (TranRPT.getChildIDType().indexOf(",") != -1) {

						String[] childIDType = TranRPT.getChildIDType().split(",");

						int childCount = childIDType.length;

						for (int j = 0; j < childCount; j++) {

							if (j == 0) {
								TranRPT.setChildIDType_1(childIDType[j]);
							}
							if (j == 1) {
								TranRPT.setChildIDType_2(childIDType[j]);
							}
							if (j == 2) {
								TranRPT.setChildIDType_3(childIDType[j]);
							}
							if (j == 3) {
								TranRPT.setChildIDType_4(childIDType[j]);
							}
							if (j == 4) {
								TranRPT.setChildIDType_5(childIDType[j]);
							}

							if (j == 5) {
								TranRPT.setChildIDType_6(childIDType[j]);
							}

							if (j == 6) {
								TranRPT.setChildIDType_7(childIDType[j]);
							}

						}

					}

					else {
						TranRPT.setChildIDType_1(rs.getString("CHILD_ID_TYPE"));

					}

				}

				if (rs.getString("CHILD_ID_NUMBER") == null) {

					TranRPT.setChildIDNumber("");
				}

				else {
					TranRPT.setChildIDNumber(rs.getString("CHILD_ID_NUMBER"));

					if (TranRPT.getChildIDNumber().indexOf(",") != -1) {

						String[] childIDNumber = TranRPT.getChildIDNumber().split(",");

						int childCount = childIDNumber.length;

						for (int j = 0; j < childCount; j++) {

							if (j == 0) {
								TranRPT.setChildIDNumber_1(childIDNumber[j]);
							}
							if (j == 1) {
								TranRPT.setChildIDNumber_2(childIDNumber[j]);
							}
							if (j == 2) {
								TranRPT.setChildIDNumber_3(childIDNumber[j]);
							}
							if (j == 3) {
								TranRPT.setChildIDNumber_4(childIDNumber[j]);
							}
							if (j == 4) {
								TranRPT.setChildIDNumber_5(childIDNumber[j]);
							}

							if (j == 5) {
								TranRPT.setChildIDNumber_6(childIDNumber[j]);
							}

							if (j == 6) {
								TranRPT.setChildIDNumber_7(childIDNumber[j]);
							}

						}

					}

					else {
						TranRPT.setChildIDNumber_1(rs.getString("CHILD_ID_NUMBER"));

					}

				}

				if (rs.getString("CHILD_DOB") == null) {

					TranRPT.setChildDOB("");
				}

				else {
					TranRPT.setChildDOB(rs.getString("CHILD_DOB"));

					if (TranRPT.getChildDOB().indexOf(",") != -1) {

						String[] childDOB = TranRPT.getChildDOB().split(",");

						int childCount = childDOB.length;

						for (int j = 0; j < childCount; j++) {

							if (j == 0) {
								TranRPT.setChildDOB_1(childDOB[j]);
							}
							if (j == 1) {
								TranRPT.setChildDOB_2(childDOB[j]);
							}
							if (j == 2) {
								TranRPT.setChildDOB_3(childDOB[j]);
							}
							if (j == 3) {
								TranRPT.setChildDOB_4(childDOB[j]);
							}
							if (j == 4) {
								TranRPT.setChildDOB_5(childDOB[j]);
							}

							if (j == 5) {
								TranRPT.setChildDOB_6(childDOB[j]);
							}

							if (j == 6) {
								TranRPT.setChildDOB_7(childDOB[j]);
							}

						}

					}

					else {
						TranRPT.setChildDOB_1(rs.getString("CHILD_DOB"));

					}

				}

				if (rs.getString("CHILD_GENDER") == null) {

					TranRPT.setChildGender("");
				}

				else {
					TranRPT.setChildGender(rs.getString("CHILD_GENDER"));

					if (TranRPT.getChildGender().indexOf(",") != -1) {

						String[] childGender = TranRPT.getChildGender().split(",");

						int childCount = childGender.length;

						for (int j = 0; j < childCount; j++) {

							if (j == 0) {
								TranRPT.setChildGender_1(childGender[j]);
							}
							if (j == 1) {
								TranRPT.setChildGender_2(childGender[j]);
							}
							if (j == 2) {
								TranRPT.setChildGender_3(childGender[j]);
							}
							if (j == 3) {
								TranRPT.setChildGender_4(childGender[j]);
							}
							if (j == 4) {
								TranRPT.setChildGender_5(childGender[j]);
							}

							if (j == 5) {
								TranRPT.setChildGender_6(childGender[j]);
							}

							if (j == 6) {
								TranRPT.setChildGender_7(childGender[j]);
							}

						}

					}

					else {
						TranRPT.setChildGender_1(rs.getString("CHILD_GENDER"));

					}

				}

				TranRPT.setRecordCount(Integer.parseInt(rs.getString("RECORDCOUNT")));
				TranRPT.setTotalAmount(rs.getString("TOTALAMOUNT"));

				if (rs.getString("LAST_PAGE") == null) {

					TranRPT.setLastPage("");

				} else

				{
					TranRPT.setLastPage(rs.getString("LAST_PAGE"));
				}

				list.add(TranRPT);

			}

			return list;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}

	}

	// GET EZYLIFE RECORD
	@Override
	public List<TransactionalReport> getTLRecord(String productEntity, String status, String policyCertificateNo,
			String dateFrom, String dateTo, String NRIC, String receiptNo, String agentCode) {
		ResultSet rs = null;
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		/*
		 * logger.info("TL");
		 * logger.info("**********************************************");
		 * logger.info("productType "+productType);
		 * logger.info("productEntity "+productEntity);
		 * logger.info("status "+status);
		 * logger.info("policyCertificateNo "+policyCertificateNo);
		 * logger.info("dateFrom "+dateFrom);
		 * logger.info("dateTo "+dateTo); logger.info("NRIC "+NRIC);
		 * logger.info("receiptNo "+receiptNo);
		 * logger.info("plateNo "+plateNo);
		 * logger.info("agentCode "+agentCode);
		 * logger.info("**********************************************");
		 */

		try {

			int i = 1;
			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_TL_ALL_TMP(?,?,?,?,?,?,?,?,?)}");
			cstmt.setString(1, productEntity.trim());
			cstmt.setString(2, status);
			cstmt.setString(3, policyCertificateNo);
			cstmt.setString(4, dateFrom);
			cstmt.setString(5, dateTo);
			cstmt.setString(6, NRIC);
			cstmt.setString(7, receiptNo);
			cstmt.setString(8, agentCode);
			cstmt.registerOutParameter(9, OracleTypes.CURSOR);
			cstmt.execute();
			;

			List<TransactionalReport> list = new ArrayList<TransactionalReport>();
			rs = (ResultSet) cstmt.getObject(9);

			while (rs.next()) {

				TransactionalReport TranRPT = new TransactionalReport();
				TranRPT.setId(i);

				TranRPT.setProductEntity("EIB");

				if (rs.getString("DSP_QQ_ID") == null) {

					TranRPT.setDSPQQID("");
				} else {

					TranRPT.setDSPQQID(rs.getString("DSP_QQ_ID"));
				}

				if (rs.getString("TRANSACTIONID") == null) {

					TranRPT.setTransactionID("");
				} else {

					TranRPT.setTransactionID(rs.getString("TRANSACTIONID"));
				}

				if (rs.getString("PMNT_GATEWAY_CODE") == null) {

					TranRPT.setPaymentMethod("");
				} else

				{
					TranRPT.setPaymentMethod(rs.getString("PMNT_GATEWAY_CODE"));

				}

				if (rs.getString("AMOUNT") == null) {

					TranRPT.setPremiumAmount("");

				} else {
					TranRPT.setPremiumAmount(rs.getString("AMOUNT"));

				}

				if (rs.getString("PMNT_STATUS") == null) {

					TranRPT.setStatus("");

				} else

				{
					TranRPT.setStatus(rs.getString("PMNT_STATUS"));

				}

				if (rs.getString("TRANSACTION_DATETIME") == null) {

					TranRPT.setTransactionDate("");

				} else

				{

					String valueFromDB = rs.getString("TRANSACTION_DATETIME");
					Date d1 = null;
					try {
						d1 = format1.parse(valueFromDB);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					String transactionDateTime = ft2.format(d1);
					TranRPT.setTransactionDate(transactionDateTime);

					// logger.info("TL TranRPT.getTransactionDate()
					// "+TranRPT.getTransactionDate());

				}

				if (rs.getString("AGENT_CODE") == null) {

					TranRPT.setAgentCode("");

				} else

				{
					TranRPT.setAgentCode(rs.getString("AGENT_CODE"));

				}

				if (rs.getString("AGENT_NAME") == null) {

					TranRPT.setAgentName("");

				} else

				{
					TranRPT.setAgentName(rs.getString("AGENT_NAME"));

				}

				if (rs.getString("OPERATOR_CODE") == null) {

					TranRPT.setOperatorCode("");

				} else

				{
					TranRPT.setOperatorCode(rs.getString("OPERATOR_CODE"));

				}

				if (rs.getString("CUSTOMER_NRIC_ID") == null) {

					TranRPT.setCustomerNRIC("");

				} else

				{
					TranRPT.setCustomerNRIC(rs.getString("CUSTOMER_NRIC_ID"));

				}

				if (rs.getString("CUSTOMER_NAME") == null) {

					TranRPT.setCustomerName(rs.getString("FULL_NAME"));

				} else

				{
					TranRPT.setCustomerName(removeScriptTags(rs.getString("CUSTOMER_NAME")));

				}

				if (rs.getString("CUSTOMER_GENDER") == null) {

					TranRPT.setCustomerGender("");

				} else

				{
					TranRPT.setCustomerGender(rs.getString("CUSTOMER_GENDER"));

				}

				if (rs.getString("CUSTOMER_ADDRESS1") == null) {

					TranRPT.setCustomerAddress1("");

				} else

				{
					TranRPT.setCustomerAddress1(rs.getString("CUSTOMER_ADDRESS1"));

				}

				if (rs.getString("CUSTOMER_ADDRESS2") == null) {

					TranRPT.setCustomerAddress2("");

				} else

				{
					TranRPT.setCustomerAddress2(rs.getString("CUSTOMER_ADDRESS2"));

				}

				if (rs.getString("CUSTOMER_ADDRESS3") == null) {

					TranRPT.setCustomerAddress3("");

				} else

				{
					TranRPT.setCustomerAddress3(rs.getString("CUSTOMER_ADDRESS3"));

				}

				if (rs.getString("CUSTOMER_POSTCODE") == null) {

					TranRPT.setCustomerPostcode("");

				} else

				{
					TranRPT.setCustomerPostcode(rs.getString("CUSTOMER_POSTCODE"));

				}

				if (rs.getString("CUSTOMER_STATE") == null) {

					TranRPT.setCustomerState("");

				} else

				{
					TranRPT.setCustomerState(rs.getString("CUSTOMER_STATE"));

				}

				if (rs.getString("CUSTOMER_EMAIL") == null) {

					TranRPT.setCustomerEmail(rs.getString("LEADS_EMAIL_ID"));

				} else

				{
					TranRPT.setCustomerEmail(rs.getString("CUSTOMER_EMAIL"));

				}

				if (rs.getString("CUSTOMER_MOBILE_NO") == null) {

					TranRPT.setCustomerMobileNo("");

				} else

				{
					TranRPT.setCustomerMobileNo(rs.getString("CUSTOMER_MOBILE_NO"));

				}

				if (rs.getString("POLICY_NUMBER") == null) {

					TranRPT.setPolicyNo("");

				} else

				{
					TranRPT.setPolicyNo(rs.getString("POLICY_NUMBER"));

				}

				if (rs.getString("MPAY_REFNO") == null) {

					TranRPT.setMPAY_RefNo("");
				} else

				{
					TranRPT.setMPAY_RefNo(rs.getString("MPAY_REFNO"));

				}

				if (rs.getString("MPAY_AUTHCODE") == null) {

					TranRPT.setMPAY_AuthCode("");
				} else

				{
					TranRPT.setMPAY_AuthCode(rs.getString("MPAY_AUTHCODE"));

				}

				TranRPT.setPaymentChannel("MPAY");

				TranRPT.setPaymentMethod("CREDIT CARD");

				if (rs.getString("PROMO_CODE") == null) {

					TranRPT.setDiscountCode("");
				} else

				{

					TranRPT.setDiscountCode(rs.getString("PROMOTION_CODE"));

				}

				if (rs.getString("CUSTOMER_GENDER") == null) {

					TranRPT.setCustomerGender("");
				}

				else {
					TranRPT.setCustomerGender(rs.getString("CUSTOMER_GENDER"));

				}

				if (rs.getString("CREATED_DATE") == null) {

					TranRPT.setQuotationCreateDateTime("");
				}

				else {
					TranRPT.setQuotationCreateDateTime(rs.getString("CREATED_DATE"));

				}

				if (rs.getString("UPDATE_DATETIME") == null) {

					TranRPT.setQuotationUpdateDateTime("");
				}

				else {
					TranRPT.setQuotationUpdateDateTime(rs.getString("UPDATE_DATETIME"));

				}

				/*
				 * if(rs.getString("TRANSACTION_DATETIME") == null){
				 *
				 * TranRPT.setQuotationUpdateDateTime(""); }
				 *
				 * else {
				 *
				 *
				 * String valueFromDB = rs.getString("TRANSACTION_DATETIME"); Date d1 = null;
				 * try { d1 = sdf2.parse(valueFromDB); } catch (ParseException e) { // TODO
				 * Auto-generated catch block e.printStackTrace(); } SimpleDateFormat sdf = new
				 * SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); String transactionDateTime =
				 * sdf.format(d1); //logger.info("txn date wtc " + transactionDateTime);
				 * TranRPT.setQuotationUpdateDateTime(transactionDateTime);
				 *
				 * }
				 */

				if (rs.getString("TL_SUM_INSURED") == null) {

					TranRPT.setCoverage("");
				}

				else {
					TranRPT.setCoverage(rs.getString("TL_SUM_INSURED"));

				}

				if (rs.getString("DURATION_OF_BENIFIT") == null) {

					TranRPT.setTerm("");
				}

				else {
					TranRPT.setTerm(rs.getString("DURATION_OF_BENIFIT"));

				}

				if (rs.getString("REJECTED_REASON") == null) {

					TranRPT.setReason("");
				}

				else {
					TranRPT.setReason(rs.getString("REJECTED_REASON"));

				}

				if (rs.getString("PREMIUM_PAYMENT_FREQUENCY_TYPE") == null) {

					TranRPT.setPremiumMode("");

				}

				else {
					if (rs.getString("PREMIUM_PAYMENT_FREQUENCY_TYPE").trim() != "") {

						String tmp = rs.getString("PREMIUM_PAYMENT_FREQUENCY_TYPE").substring(0, 1).toUpperCase()
								+ rs.getString("PREMIUM_PAYMENT_FREQUENCY_TYPE").substring(1);
						TranRPT.setPremiumMode(tmp);

					} else {

						TranRPT.setPremiumMode(rs.getString("PREMIUM_PAYMENT_FREQUENCY_TYPE"));

					}

				}

				TranRPT.setMode(rs.getString("PREMIUM_PAYMENT_FREQUENCY_TYPE"));

				if (rs.getString("AMOUNT") == null) {

					TranRPT.setPremiumAmount("");
				}

				else {
					TranRPT.setPremiumAmount(rs.getString("AMOUNT"));

				}

				if (rs.getString("AGENT_CODE") == null) {

					TranRPT.setAgentCode("");
				}

				else {
					TranRPT.setAgentCode(rs.getString("AGENT_CODE"));

				}

				if (rs.getString("AGENT_NAME") == null) {

					TranRPT.setAgentName("");
				}

				else {
					TranRPT.setAgentName(rs.getString("AGENT_NAME"));

				}

				if (rs.getString("OPERATOR_CODE") == null) {

					TranRPT.setOperatorCode("");
				}

				else {
					TranRPT.setOperatorCode(rs.getString("OPERATOR_CODE"));

				}

				if (rs.getString("PRODUCT_CODE_NAME") == null) {

					TranRPT.setProductType("");
				}

				else {
					TranRPT.setProductType(rs.getString("PRODUCT_CODE_NAME"));

				}

				if (rs.getString("PRODUCT_CODE") == null) {

					TranRPT.setProductCode("");
				}

				else {
					TranRPT.setProductCode(rs.getString("PRODUCT_CODE"));

				}

				if (rs.getString("POLICY_NUMBER") == null) {

					TranRPT.setPolicyNo("");
				}

				else {
					TranRPT.setPolicyNo(rs.getString("POLICY_NUMBER"));

				}

				if (rs.getString("PREMIUM_PAYMENT_FREQUENCY") == null) {

					TranRPT.setAnnualPremium("");
				}

				else {

					if (TranRPT.getMode().equals("monthly")) {

						// multiply by 12 - do calculation here

						BigDecimal amt1 = new BigDecimal(rs.getString("PREMIUM_PAYMENT_FREQUENCY"));
						BigDecimal amt2 = new BigDecimal(12);
						BigDecimal tmpAmount = new BigDecimal(0.00);

						tmpAmount = amt2.multiply(amt1);

						TranRPT.setAnnualPremium(String.valueOf(tmpAmount));

					} else {
						TranRPT.setAnnualPremium(rs.getString("PREMIUM_PAYMENT_FREQUENCY"));

					}

				}

				TranRPT.setRecordCount(Integer.parseInt(rs.getString("RECORDCOUNT")));
				TranRPT.setTotalAmount(rs.getString("TOTALAMOUNT"));

				if (rs.getString("LAST_PAGE") == null) {

					TranRPT.setLastPage("");

				} else

				{
					TranRPT.setLastPage(rs.getString("LAST_PAGE"));
				}

				if (rs.getString("REJECTED_REASON") == null) {

					TranRPT.setReason("");

				} else {

					TranRPT.setReason(rs.getString("REJECTED_REASON"));
				}

				if (rs.getString("UWREASON") == null) {

					TranRPT.setUWReason("");

				} else {

					TranRPT.setUWReason(rs.getString("UWREASON"));
				}

				list.add(TranRPT);

			}

			return list;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}

	}

	@Override
	// check JPJ Status for motor in admin-transaction-detail.jsp
	public String checkJPJStatus(String policyNo, String vehicleNo, String effectDate, String expiryDate) {

		String JPJStatus = null;
		// String Lang=null;

		try {
			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_MI_JPJ_STATUS(?,?,?,?,?)}");

			cstmt.setString(1, policyNo);
			cstmt.setString(2, vehicleNo);
			cstmt.setString(3, effectDate);
			cstmt.setString(4, expiryDate);
			cstmt.registerOutParameter(5, OracleTypes.VARCHAR);
			cstmt.execute();

			JPJStatus = cstmt.getString(5);
			// Lang= (String)cstmt.getObject(5);
			return JPJStatus;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}

	}

	@Override
	// check ISM status for motor in admin-transaction-detail.jsp
	public int checkISMStatus(String QQID) {

		int ISMStatus;
		// String Lang=null;

		try {
			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_MI_ISM_STATUS(?,?)}");

			cstmt.setString(1, QQID);
			cstmt.registerOutParameter(2, OracleTypes.INTEGER);
			cstmt.execute();

			ISMStatus = cstmt.getInt(2);
			// Lang= (String)cstmt.getObject(5);
			return ISMStatus;

		} catch (SQLException e) {
			e.printStackTrace();
			return 1;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}

	}

	private static String removeScriptTags(String message) {
		String scriptRegex = "<(/)?[ ]*script[^>]*>";
		Pattern pattern2 = Pattern.compile(scriptRegex);

		if (message != null) {
			Matcher matcher2 = pattern2.matcher(message);
			StringBuffer str = new StringBuffer(message.length());
			while (matcher2.find()) {
				matcher2.appendReplacement(str, Matcher.quoteReplacement(" "));
			}
			matcher2.appendTail(str);
			message = str.toString();
		}
		return message;
	}

	@Override
	// query result for ezylife rejection record
	public List<TransactionalReport> rejectRecordTL(String productType,
			// String productEntity,
			String status, String policyCertificateNo, String dateFrom, String dateTo, String NRIC, String receiptNo,
			int startRow, int endRow, String agentCode) {

		double totalAmount = 0.00;

		// logger.info("productType " +productType);
		// logger.info("status " +status);
		// logger.info("NRIC " +NRIC);
		try {
			int i = startRow;
			connection = ConnectionFactory.getConnection();
			// CallableStatement cstmt = connection.prepareCall("{CALL
			// DSP_ADM_SP_RPT_TRANSACTIONAL_S(?,?,?,?,?,?,?,?,?,?)}");
			CallableStatement cstmt = connection
					.prepareCall("{CALL DSP_ADM_SP_REPORT_TL_REJECT(?,?,?,?,?,?,?,?,?,?,?)}");
			// CallableStatement cstmt = connection.prepareCall("{CALL
			// DSP_ADM_SP_TRANSACTION_RPT_S6(?,?,?,?,?,?,?,?,?,?,?)}");
			cstmt.setString(1, productType.trim());
			cstmt.setString(2, status);
			cstmt.setString(3, policyCertificateNo);
			cstmt.setString(4, dateFrom);
			cstmt.setString(5, dateTo);
			cstmt.setString(6, NRIC);
			cstmt.setString(7, receiptNo);
			cstmt.setInt(8, startRow);
			cstmt.setInt(9, endRow);
			cstmt.setString(10, agentCode);
			cstmt.registerOutParameter(11, OracleTypes.CURSOR);
			// cstmt.registerOutParameter(11, OracleTypes.CURSOR);
			cstmt.execute();

			List<TransactionalReport> list = new ArrayList<TransactionalReport>();
			rs = (ResultSet) cstmt.getObject(11);
			while (rs.next()) {

				TransactionalReport TranRPT = new TransactionalReport();
				TranRPT.setId(i);
				TranRPT.setDSPQQID(rs.getString("DSP_QQ_ID"));
				TranRPT.setTransactionID("");
				TranRPT.setProductEntity("EIB");

				if (rs.getString("AMOUNT") == null) {

					TranRPT.setPremiumAmount("");

				} else {

					TranRPT.setPremiumAmount(rs.getString("AMOUNT"));
				}
				TranRPT.setPaymentMethod("");

				if (rs.getString("TL_QQ_STATUS") == null) {

					TranRPT.setStatus("");

				} else {

					TranRPT.setStatus(rs.getString("TL_QQ_STATUS"));
				}

				if (rs.getString("CREATEDDATE2") == null) {

					TranRPT.setTransactionDate("");

				} else {

					TranRPT.setTransactionDate(rs.getString("CREATEDDATE2"));
				}

				TranRPT.setInvoiceNo("");

				if (rs.getString("PRODUCT_CODE") == null) {

					TranRPT.setProductCode("");

				} else {

					TranRPT.setProductCode(rs.getString("PRODUCT_CODE"));
				}
				if (rs.getString("PRODUCT_CODE_NAME") == null) {

					TranRPT.setProductType("");

				} else {

					TranRPT.setProductType(rs.getString("PRODUCT_CODE_NAME"));
				}

				TranRPT.setPolicyNo("");

				if (rs.getString("FULL_NAME") == null) {

					TranRPT.setCustomerName("");

				} else {

					TranRPT.setCustomerName(rs.getString("FULL_NAME"));
				}

				if (rs.getString("CUSTOMERID") == null) {

					TranRPT.setCustomerID("");

				} else {

					TranRPT.setCustomerID(rs.getString("CUSTOMERID"));
				}

				if (rs.getString("CUSTOMERNRIC") == null) {

					TranRPT.setCustomerNRIC("");

				} else {

					TranRPT.setCustomerNRIC(rs.getString("CUSTOMERNRIC"));
				}

				if (rs.getString("TERM") == null) {

					TranRPT.setTerm("");

				} else {

					TranRPT.setTerm(rs.getString("TERM"));
				}

				if (rs.getString("COVERAGE") == null) {

					TranRPT.setCoverage("");

				} else {

					TranRPT.setCoverage(rs.getString("COVERAGE"));
				}

				TranRPT.setPremiumMode("");

				TranRPT.setMPAY_RefNo("");
				TranRPT.setMPAY_AuthCode("");

				TranRPT.setEBPG_RefNo("");
				TranRPT.setEBPG_AuthCode("");

				TranRPT.setM2U_RefNo("");
				TranRPT.setM2U_AuthCode("");

				TranRPT.setFPX_RefNo("");
				TranRPT.setFPX_AuthCode("");

				TranRPT.setPaymentChannel("");
				if (TranRPT.getPaymentMethod().indexOf("CREDIT CARD") != -1) {

					TranRPT.setPaymentChannel("MPAY");

				} else if (TranRPT.getPaymentMethod().indexOf("EBPG") != -1) {

					TranRPT.setPaymentMethod("CREDIT CARD");
					TranRPT.setPaymentChannel("EBPG");
				}

				else if (TranRPT.getPaymentMethod().indexOf("AMEX") != -1) {

					TranRPT.setPaymentMethod("CREDIT CARD");
					TranRPT.setPaymentChannel("AMEX");
				}

				else if (TranRPT.getPaymentMethod().indexOf("FPX") != -1) {

					TranRPT.setPaymentMethod("ONLINE BANKING");
					TranRPT.setPaymentChannel("FPX");
				}

				else if (TranRPT.getPaymentMethod().indexOf("Maybank2U") != -1) {

					TranRPT.setPaymentMethod("DEBIT ACCOUNT");
					TranRPT.setPaymentChannel("M2U");
				}

				TranRPT.setRecordCount(Integer.parseInt(rs.getString("RECORDCOUNT")));
				TranRPT.setTotalAmount(rs.getString("TOTALAMOUNT"));

				if (rs.getString("REJECTED_REASON") == null) {

					TranRPT.setReason("");

				} else {

					TranRPT.setReason(rs.getString("REJECTED_REASON"));
				}

				if (rs.getString("UWREASON") == null) {

					TranRPT.setUWReason("");

				} else {

					TranRPT.setUWReason(rs.getString("UWREASON"));
				}

				TranRPT.setCapsAmount("");
				TranRPT.setMotorAmount(null);

				if (rs.getString("AGENT_CODE") == null) {

					TranRPT.setAgentCode("");

				} else {

					TranRPT.setAgentCode(rs.getString("AGENT_CODE"));
				}

				if (rs.getString("AGENTNAMETL") == null) {

					TranRPT.setAgentName("");

				} else {

					TranRPT.setAgentName(rs.getString("AGENTNAMETL"));
				}
				// logger.info(" id="+i+" "+rs.getString(1)+" "+rs.getString(2)+"
				// "+rs.getString(3)+" "+rs.getString(4)+" "+rs.getString(5));
				i++;

				// totalAmount = totalAmount+Integer.parseInt(TranRPT.getPremiumAmount());

				// logger.info(TranRPT.getPremiumAmount());
				// TranRPT.setTotalAmount(totalAmount);

				list.add(TranRPT);

			}

			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	@Override
	// query result for ezylife incomplete record
	public List<TransactionalReport> incompleteRecordTL(String productType,
			// String entityType,
			String status, String policyCertificateNo, String dateFrom, String dateTo, String NRIC, String receiptNo,
			int startRow, int endRow, String agentCode) {

		double totalAmount = 0.00;

		// logger.info("productType " +productType);
		// logger.info("status " +status);
		try {
			int i = startRow;
			connection = ConnectionFactory.getConnection();
			// CallableStatement cstmt = connection.prepareCall("{CALL
			// DSP_ADM_SP_RPT_TRANSACTIONAL_S(?,?,?,?,?,?,?,?,?,?)}");
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_REPORT_TL_INCO(?,?,?,?,?,?,?,?,?,?,?)}");
			// CallableStatement cstmt = connection.prepareCall("{CALL
			// DSP_ADM_SP_TRANSACTION_RPT_S6(?,?,?,?,?,?,?,?,?,?,?)}");
			cstmt.setString(1, productType.trim());
			cstmt.setString(2, status);
			cstmt.setString(3, policyCertificateNo);
			cstmt.setString(4, dateFrom);
			cstmt.setString(5, dateTo);
			cstmt.setString(6, NRIC);
			cstmt.setString(7, receiptNo);
			cstmt.setInt(8, startRow);
			cstmt.setInt(9, endRow);
			cstmt.setString(10, agentCode);
			cstmt.registerOutParameter(11, OracleTypes.CURSOR);
			// cstmt.registerOutParameter(11, OracleTypes.CURSOR);
			cstmt.execute();

			List<TransactionalReport> list = new ArrayList<TransactionalReport>();
			rs = (ResultSet) cstmt.getObject(11);
			while (rs.next()) {

				TransactionalReport TranRPT = new TransactionalReport();
				TranRPT.setId(i);
				TranRPT.setDSPQQID(rs.getString("DSP_QQ_ID"));
				TranRPT.setTransactionID("");

				if (rs.getString("AMOUNT") == null) {

					TranRPT.setPremiumAmount("");

				} else {

					TranRPT.setPremiumAmount(rs.getString("AMOUNT"));
				}
				TranRPT.setPaymentMethod("");
				TranRPT.setStatus(rs.getString("TL_QQ_STATUS"));
				TranRPT.setTransactionDate(rs.getString("CREATEDDATE2"));
				TranRPT.setInvoiceNo("");
				TranRPT.setPolicyNo("");
				TranRPT.setProductEntity("EIB");
				// TranRPT.setProductCode("");
				// TranRPT.setProductType("");

				if (rs.getString("PRODUCT_CODE") == null) {

					TranRPT.setProductCode("");

				} else {

					TranRPT.setProductCode(rs.getString("PRODUCT_CODE"));
				}

				if (rs.getString("PRODUCT_CODE_NAME") == null) {

					TranRPT.setProductType("");

				} else {

					TranRPT.setProductType(rs.getString("PRODUCT_CODE_NAME"));
				}

				if (rs.getString("FULL_NAME") == null) {

					TranRPT.setCustomerName("");

				} else {

					TranRPT.setCustomerName(rs.getString("FULL_NAME"));
				}

				if (rs.getString("CUSTOMERID") == null) {

					TranRPT.setCustomerID("");

				} else {

					TranRPT.setCustomerID(rs.getString("CUSTOMERID"));
				}

				if (rs.getString("CUSTOMERNRIC") == null) {

					TranRPT.setCustomerNRIC("");

				} else {

					TranRPT.setCustomerNRIC(rs.getString("CUSTOMERNRIC"));
				}

				if (rs.getString("TERM") == null) {

					TranRPT.setTerm("");

				} else {

					TranRPT.setTerm(rs.getString("TERM"));
				}

				if (rs.getString("COVERAGE") == null) {

					TranRPT.setCoverage("");

				} else {

					TranRPT.setCoverage(rs.getString("COVERAGE"));
				}

				TranRPT.setPremiumMode("");

				TranRPT.setMPAY_RefNo("");
				TranRPT.setMPAY_AuthCode("");

				TranRPT.setEBPG_RefNo("");
				TranRPT.setEBPG_AuthCode("");

				TranRPT.setM2U_RefNo("");
				TranRPT.setM2U_AuthCode("");

				TranRPT.setFPX_RefNo("");
				TranRPT.setFPX_AuthCode("");

				TranRPT.setPaymentChannel("");
				if (TranRPT.getPaymentMethod().indexOf("CREDIT CARD") != -1) {

					TranRPT.setPaymentChannel("MPAY");

				} else if (TranRPT.getPaymentMethod().indexOf("EBPG") != -1) {

					TranRPT.setPaymentMethod("CREDIT CARD");
					TranRPT.setPaymentChannel("EBPG");
				}

				else if (TranRPT.getPaymentMethod().indexOf("AMEX") != -1) {

					TranRPT.setPaymentMethod("CREDIT CARD");
					TranRPT.setPaymentChannel("AMEX");
				}

				else if (TranRPT.getPaymentMethod().indexOf("FPX") != -1) {

					TranRPT.setPaymentMethod("ONLINE BANKING");
					TranRPT.setPaymentChannel("FPX");
				}

				else if (TranRPT.getPaymentMethod().indexOf("Maybank2U") != -1) {

					TranRPT.setPaymentMethod("DEBIT ACCOUNT");
					TranRPT.setPaymentChannel("M2U");
				}

				TranRPT.setRecordCount(Integer.parseInt(rs.getString("RECORDCOUNT")));
				TranRPT.setTotalAmount(rs.getString("TOTALAMOUNT"));

				TranRPT.setDoneStep1(rs.getString("DONESTEP1"));
				TranRPT.setDoneStep2(rs.getString("DONESTEP2"));

				if (rs.getString("AGENT_CODE") == null) {

					TranRPT.setAgentCode("");

				} else {

					TranRPT.setAgentCode(rs.getString("AGENT_CODE"));
				}

				if (rs.getString("AGENTNAMETL") == null) {

					TranRPT.setAgentName("");

				} else {

					TranRPT.setAgentName(rs.getString("AGENTNAMETL"));
				}
				// logger.info(" id="+i+" "+rs.getString(1)+" "+rs.getString(2)+"
				// "+rs.getString(3)+" "+rs.getString(4)+" "+rs.getString(5));
				i++;

				// totalAmount = totalAmount+Integer.parseInt(TranRPT.getPremiumAmount());

				// logger.info(TranRPT.getPremiumAmount());
				// TranRPT.setTotalAmount(totalAmount);

				list.add(TranRPT);

			}

			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	// query result for TL total info for export excel

	@Override
	public List<TransactionalReport> getTLRecordDetail(String productType, String productEntity, String status,
			String policyCertificateNo, String dateFrom, String dateTo, String NRIC, String receiptNo, String plateNo,
			String agentCode) {
		ResultSet rs = null;
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		/*
		 * logger.info("TL");
		 * logger.info("**********************************************");
		 * logger.info("productType "+productType);
		 * logger.info("productEntity "+productEntity);
		 * logger.info("status "+status);
		 * logger.info("policyCertificateNo "+policyCertificateNo);
		 * logger.info("dateFrom "+dateFrom);
		 * logger.info("dateTo "+dateTo); logger.info("NRIC "+NRIC);
		 * logger.info("receiptNo "+receiptNo);
		 * logger.info("plateNo "+plateNo);
		 * logger.info("agentCode "+agentCode);
		 * logger.info("**********************************************");
		 */

		try {

			int i = 1;
			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection
					.prepareCall("{CALL DSP_ADM_SP_TL_TOTALINFO_TMP(?,?,?,?,?,?,?,?,?,?,?)}");
			cstmt.setString(1, productType.trim());
			cstmt.setString(2, productEntity.trim());
			cstmt.setString(3, status);
			cstmt.setString(4, policyCertificateNo);
			cstmt.setString(5, dateFrom);
			cstmt.setString(6, dateTo);
			cstmt.setString(7, NRIC);
			cstmt.setString(8, receiptNo);
			cstmt.setString(9, plateNo);
			cstmt.setString(10, agentCode);
			cstmt.registerOutParameter(11, OracleTypes.CURSOR);
			cstmt.execute();
			;

			List<TransactionalReport> list = new ArrayList<TransactionalReport>();
			rs = (ResultSet) cstmt.getObject(11);

			while (rs.next()) {

				TransactionalReport TranRPT = new TransactionalReport();
				TranRPT.setId(i);

				if (rs.getString("TRANSACTION_ID") == null) {

					TranRPT.setTransactionID("");
				} else {

					TranRPT.setTransactionID(rs.getString("TRANSACTION_ID"));
				}

				if (rs.getString("PMNT_GATEWAY_CODE") == null) {

					TranRPT.setPaymentMethod("");
				} else

				{
					TranRPT.setPaymentMethod(rs.getString("PMNT_GATEWAY_CODE"));

				}

				if (rs.getString("AMOUNT") == null) {

					TranRPT.setPremiumAmount("");

				} else {
					TranRPT.setPremiumAmount(rs.getString("AMOUNT"));

				}

				if (rs.getString("PMNT_STATUS") == null) {

					TranRPT.setStatus("");

				} else

				{
					TranRPT.setStatus(rs.getString("PMNT_STATUS"));

				}

				if (rs.getString("TRANSACTION_DATETIME") == null) {

					TranRPT.setTransactionDate("");

				} else

				{
					TranRPT.setTransactionDate(rs.getString("TRANSACTION_DATETIME"));

				}

				if (rs.getString("AGENT_CODE") == null) {

					TranRPT.setAgentCode("");

				} else

				{
					TranRPT.setAgentCode(rs.getString("AGENT_CODE"));

				}

				if (rs.getString("AGENT_NAME") == null) {

					TranRPT.setAgentName("");

				} else

				{
					TranRPT.setAgentName(rs.getString("AGENT_NAME"));

				}

				if (rs.getString("OPERATOR_CODE") == null) {

					TranRPT.setOperatorCode("");

				} else

				{
					TranRPT.setOperatorCode(rs.getString("OPERATOR_CODE"));

				}

				if (rs.getString("CUSTOMER_NRIC_ID") == null) {

					TranRPT.setCustomerNRIC("");

				} else

				{
					TranRPT.setCustomerNRIC(rs.getString("CUSTOMER_NRIC_ID"));

				}

				if (rs.getString("CUSTOMER_NAME") == null) {

					TranRPT.setCustomerName("");

				} else

				{
					TranRPT.setCustomerName(removeScriptTags(rs.getString("CUSTOMER_NAME")));

				}

				if (rs.getString("CUSTOMER_GENDER") == null) {

					TranRPT.setCustomerGender("");

				} else

				{
					TranRPT.setCustomerGender(rs.getString("CUSTOMER_GENDER"));

				}

				if (rs.getString("CUSTOMER_ADDRESS1") == null) {

					TranRPT.setCustomerAddress1("");

				} else

				{
					TranRPT.setCustomerAddress1(rs.getString("CUSTOMER_ADDRESS1"));

				}

				if (rs.getString("CUSTOMER_ADDRESS2") == null) {

					TranRPT.setCustomerAddress2("");

				} else

				{
					TranRPT.setCustomerAddress2(rs.getString("CUSTOMER_ADDRESS2"));

				}

				if (rs.getString("CUSTOMER_ADDRESS3") == null) {

					TranRPT.setCustomerAddress3("");

				} else

				{
					TranRPT.setCustomerAddress3(rs.getString("CUSTOMER_ADDRESS3"));

				}

				if (rs.getString("CUSTOMER_POSTCODE") == null) {

					TranRPT.setCustomerPostcode("");

				} else

				{
					TranRPT.setCustomerPostcode(rs.getString("CUSTOMER_POSTCODE"));

				}

				if (rs.getString("CUSTOMER_STATE") == null) {

					TranRPT.setCustomerState("");

				} else

				{
					TranRPT.setCustomerState(rs.getString("CUSTOMER_STATE"));

				}

				if (rs.getString("CUSTOMER_EMAIL") == null) {

					TranRPT.setCustomerEmail("");

				} else

				{
					TranRPT.setCustomerEmail(rs.getString("CUSTOMER_EMAIL"));

				}

				if (rs.getString("CUSTOMER_MOBILE_NO") == null) {

					TranRPT.setCustomerMobileNo("");

				} else

				{
					TranRPT.setCustomerMobileNo(rs.getString("CUSTOMER_MOBILE_NO"));

				}

				if (rs.getString("POLICY_NUMBER") == null) {

					TranRPT.setPolicyNo("");

				} else

				{
					TranRPT.setPolicyNo(rs.getString("POLICY_NUMBER"));

				}

				if (rs.getString("MPAY_REFNO") == null) {

					TranRPT.setMPAY_RefNo("");
				} else

				{
					TranRPT.setMPAY_RefNo(rs.getString("MPAY_REFNO"));

				}

				if (rs.getString("MPAY_AUTHCODE") == null) {

					TranRPT.setMPAY_AuthCode("");
				} else

				{
					TranRPT.setMPAY_AuthCode(rs.getString("MPAY_AUTHCODE"));

				}

				TranRPT.setPaymentChannel("MPAY");

				if (rs.getString("PROMO_CODE") == null) {

					TranRPT.setDiscountCode("");
				} else

				{

					TranRPT.setDiscountCode(rs.getString("PROMOTION_CODE"));

				}

				if (rs.getString("CUSTOMER_GENDER") == null) {

					TranRPT.setCustomerGender("");
				}

				else {
					TranRPT.setCustomerGender(rs.getString("CUSTOMER_GENDER"));

				}

				if (rs.getString("CREATED_DATE") == null) {

					TranRPT.setQuotationCreateDateTime("");
				}

				else {
					TranRPT.setQuotationCreateDateTime(rs.getString("CREATED_DATE"));

				}

				if (rs.getString("TRANSACTION_DATETIME") == null) {

					TranRPT.setQuotationUpdateDateTime("");
				}

				else {

					String valueFromDB = rs.getString("TRANSACTION_DATETIME");
					Date d1 = null;
					try {
						d1 = sdf2.parse(valueFromDB);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String transactionDateTime = sdf.format(d1);
					// logger.info("txn date wtc " + transactionDateTime);
					TranRPT.setQuotationUpdateDateTime(transactionDateTime);

				}

				if (rs.getString("TL_SUM_INSURED") == null) {

					TranRPT.setCoverage("");
				}

				else {
					TranRPT.setCoverage(rs.getString("TL_SUM_INSURED"));

				}

				if (rs.getString("DURATION_OF_BENIFIT") == null) {

					TranRPT.setTerm("");
				}

				else {
					TranRPT.setTerm(rs.getString("DURATION_OF_BENIFIT"));

				}

				if (rs.getString("REJECTED_REASON") == null) {

					TranRPT.setReason("");
				}

				else {
					TranRPT.setReason(rs.getString("REJECTED_REASON"));

				}

				if (rs.getString("PREMIUM_PAYMENT_FREQUENCY_TYPE") == null) {

					TranRPT.setMode("");
				}

				else {
					TranRPT.setMode(rs.getString("PREMIUM_PAYMENT_FREQUENCY_TYPE"));

				}

				if (rs.getString("AMOUNT") == null) {

					TranRPT.setPremiumAmount("");
				}

				else {
					TranRPT.setPremiumAmount(rs.getString("AMOUNT"));

				}

				if (rs.getString("AGENT_CODE") == null) {

					TranRPT.setAgentCode("");
				}

				else {
					TranRPT.setAgentCode(rs.getString("AGENT_CODE"));

				}

				if (rs.getString("AGENT_NAME") == null) {

					TranRPT.setAgentName("");
				}

				else {
					TranRPT.setAgentName(rs.getString("AGENT_NAME"));

				}

				if (rs.getString("OPERATOR_CODE") == null) {

					TranRPT.setOperatorCode("");
				}

				else {
					TranRPT.setOperatorCode(rs.getString("OPERATOR_CODE"));

				}

				if (rs.getString("PRODUCT_CODE_NAME") == null) {

					TranRPT.setProductType("");
				}

				else {
					TranRPT.setProductType(rs.getString("PRODUCT_CODE_NAME"));

				}

				if (rs.getString("POLICY_NUMBER") == null) {

					TranRPT.setPolicyNo("");
				}

				else {
					TranRPT.setPolicyNo(rs.getString("POLICY_NUMBER"));

				}

				if (rs.getString("PREMIUM_PAYMENT_FREQUENCY") == null) {

					TranRPT.setTotalAmount("");
				}

				else {

					if (TranRPT.getMode().equals("monthly")) {

						// multiply by 12 - do calculation here

						TranRPT.setTotalAmount(rs.getString("PREMIUM_PAYMENT_FREQUENCY"));

					} else {
						TranRPT.setTotalAmount(rs.getString("PREMIUM_PAYMENT_FREQUENCY"));

					}

				}
				list.add(TranRPT);

			}

			return list;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}

	}

	/**
	 * To insert the Customer audit details in DSP_COMMON_TBL_CUSTOMER_AUDIT table
	 */
	@Override
	public String insertCustomerAudit(int customerId, String userName, int userRole, int dspQqId) {

		String custAuditStatus;

		try {
			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{CALL DSP_AUDIT_SP_CUSTOMER_INSERT(?,?,?,?,?)}");

			cstmt.setInt(1, customerId);
			cstmt.setString(2, userName);
			// cstmt.setString(3,createDate);
			cstmt.setInt(3, userRole);
			cstmt.setInt(4, dspQqId);
			cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);

			cstmt.execute();

			custAuditStatus = cstmt.getString(5);
			return custAuditStatus;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				connection.commit();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	/**
	 * To insert the Transaction audit details in DSP_COMMON_TBL_PAYMENT_AUDIT table
	 */
	@Override
	public String insertPaymentforAudit(int paymentId, String userName, int userRole, int dspQqId, String policyNo) {

		String custAuditStatus;

		try {
			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{CALL DSP_AUDIT_SP_PAYMENT_INSERT(?,?,?,?,?,?)}");

			cstmt.setInt(1, paymentId);
			cstmt.setString(2, userName);
			// cstmt.setString(3,createDate);
			cstmt.setInt(3, userRole);
			cstmt.setInt(4, dspQqId);
			cstmt.setString(5, policyNo);
			cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);

			cstmt.execute();

			custAuditStatus = cstmt.getString(6);
			return custAuditStatus;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				connection.commit();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	/**
	 * To insert the Agent audit details in DSP_COMMON_TBL_AGENT_AUDIT table
	 */
	@Override
	public String insertAgentforAudit(String agentCode, String userName, int userRole) {

		String custAuditStatus;

		try {
			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{CALL DSP_AUDIT_SP_AGENT_INSERT(?,?,?,?)}");

			cstmt.setString(1, agentCode);
			cstmt.setString(2, userName);
			cstmt.setInt(3, userRole);
			cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);

			cstmt.execute();

			custAuditStatus = cstmt.getString(4);
			return custAuditStatus;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				connection.commit();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	// buddy pa
	@Override
	public List<TransactionalReport> getBuddyPARecord(String ProductEntity, String Status, String PolicyCertificateNo,
			String DateFrom, String DateTo, String NRIC, String ReceiptNo, String AgentCode) {
		ResultSet rs = null;
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		try {

			int i = 1;
			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_BUDDY_ALL_TMP(?,?,?,?,?,?,?,?,?)}");
			cstmt.setString(1, ProductEntity.trim());
			cstmt.setString(2, Status);
			cstmt.setString(3, PolicyCertificateNo);
			cstmt.setString(4, DateFrom);
			cstmt.setString(5, DateTo);
			cstmt.setString(6, NRIC);
			cstmt.setString(7, ReceiptNo);
			cstmt.setString(8, AgentCode);
			cstmt.registerOutParameter(9, OracleTypes.CURSOR);
			cstmt.execute();
			;

			List<TransactionalReport> list = new ArrayList<TransactionalReport>();
			rs = (ResultSet) cstmt.getObject(9);
			while (rs.next()) {
				TransactionalReport TranRPT = new TransactionalReport();
				TranRPT.setId(i);
				if (rs.getString("TRANSACTIONID") == null) {

					TranRPT.setTransactionID("");
				} else {

					TranRPT.setTransactionID(rs.getString("TRANSACTIONID"));
				}

				if (rs.getString("PMNT_GATEWAY_CODE") == null) {

					TranRPT.setPaymentMethod("");
				} else

				{
					TranRPT.setPaymentMethod(rs.getString("PMNT_GATEWAY_CODE"));

				}

				if (rs.getString("AMOUNT") == null) {

					TranRPT.setPremiumAmount("");

				} else {
					TranRPT.setPremiumAmount(rs.getString("AMOUNT"));

				}

				if (rs.getString("PMNT_STATUS") == null) {

					TranRPT.setStatus("");

				} else

				{
					TranRPT.setStatus(rs.getString("PMNT_STATUS"));

				}

				// logger.info("rs.getString('TRANSACTION_DATETIME')
				// "+rs.getString("TRANSACTION_DATETIME"));
				if (rs.getString("TRANSACTION_DATETIME") == null) {

					TranRPT.setTransactionDate("");

				} else

				{
					String tmpDate = "";

					try {
						tmpDate = ft2.format(format1.parse(rs.getString("TRANSACTION_DATETIME")));

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					TranRPT.setTransactionDate(tmpDate);
				}

				if (rs.getString("INVOICE_NO") == null) {

					TranRPT.setInvoiceNo("");

				} else

				{
					TranRPT.setInvoiceNo(rs.getString("INVOICE_NO"));

				}

				/*
				 * if(TranRPT.getInvoiceNo().indexOf("EIB")!=-1){
				 *
				 * TranRPT.setProductEntity("EIB");
				 *
				 * }
				 *
				 * else if(TranRPT.getInvoiceNo().indexOf("ETB")!=-1)
				 *
				 * { TranRPT.setProductEntity("ETB");
				 *
				 * } else{
				 *
				 * TranRPT.setProductEntity("EIB");
				 *
				 * }
				 */

				if (rs.getString("DSP_QQ_ID") == null) {

					TranRPT.setDSPQQID("");

				} else

				{
					TranRPT.setDSPQQID(rs.getString("DSP_QQ_ID"));

				}

				if (rs.getString("PRODUCT_ID") == null) {

					TranRPT.setProductCode("");

				} else

				{
					TranRPT.setProductCode(rs.getString("PRODUCT_ID"));

				}

				if (TranRPT.getProductCode().equals("CPP")) {

					TranRPT.setProductEntity("EIB");

				} else {

					TranRPT.setProductEntity("ETB");

				}

				// logger.info("PRODUCT_CODE_NAME "+rs.getString("PRODUCT_CODE_NAME"));
				if (rs.getString("PRODUCT_CODE_NAME") == null) {

					TranRPT.setProductType("");

				} else

				{
					TranRPT.setProductType(rs.getString("PRODUCT_CODE_NAME"));

				}

				if (rs.getString("AGENT_CODE") == null) {

					TranRPT.setAgentCode("");

				} else

				{
					TranRPT.setAgentCode(rs.getString("AGENT_CODE"));

				}

				if (rs.getString("AGENT_NAME") == null) {

					TranRPT.setAgentName("");

				} else

				{
					TranRPT.setAgentName(rs.getString("AGENT_NAME"));

				}

				if (rs.getString("OPERATOR_CODE") == null) {

					TranRPT.setOperatorCode("");

				} else

				{
					TranRPT.setOperatorCode(rs.getString("OPERATOR_CODE"));

				}

				if (rs.getString("IDTYPE") == null) {

					TranRPT.setIDType("");

				} else

				{
					TranRPT.setIDType(rs.getString("IDTYPE"));

				}

				if (rs.getString("CUSTOMER_NRIC_ID") == null) {

					TranRPT.setCustomerNRIC("");

				} else

				{
					TranRPT.setCustomerNRIC(rs.getString("CUSTOMER_NRIC_ID"));

				}

				if (rs.getString("CUSTOMER_NAME") == null) {

					TranRPT.setCustomerName("");

				} else

				{
					TranRPT.setCustomerName(removeScriptTags(rs.getString("CUSTOMER_NAME")));

				}

				if (rs.getString("CUSTOMER_GENDER") == null) {

					TranRPT.setCustomerGender("");

				} else

				{
					TranRPT.setCustomerGender(rs.getString("CUSTOMER_GENDER"));

				}

				if (rs.getString("CUSTOMER_ADDRESS1") == null) {

					TranRPT.setCustomerAddress1("");

				} else

				{
					TranRPT.setCustomerAddress1(rs.getString("CUSTOMER_ADDRESS1"));

				}

				if (rs.getString("CUSTOMER_ADDRESS2") == null) {

					TranRPT.setCustomerAddress2("");

				} else

				{
					TranRPT.setCustomerAddress2(rs.getString("CUSTOMER_ADDRESS2"));

				}

				if (rs.getString("CUSTOMER_ADDRESS3") == null) {

					TranRPT.setCustomerAddress3("");

				} else

				{
					TranRPT.setCustomerAddress3(rs.getString("CUSTOMER_ADDRESS3"));

				}

				if (rs.getString("CUSTOMER_POSTCODE") == null) {

					TranRPT.setCustomerPostcode("");

				} else

				{
					TranRPT.setCustomerPostcode(rs.getString("CUSTOMER_POSTCODE"));

				}

				if (rs.getString("CUSTOMER_STATE") == null) {

					TranRPT.setCustomerState("");

				} else

				{
					TranRPT.setCustomerState(rs.getString("CUSTOMER_STATE"));

				}

				if (rs.getString("CUSTOMER_EMAIL") == null) {

					TranRPT.setCustomerEmail("");

				} else

				{
					TranRPT.setCustomerEmail(rs.getString("CUSTOMER_EMAIL"));

				}

				if (rs.getString("CUSTOMER_MOBILE_NO") == null) {

					TranRPT.setCustomerMobileNo("");

				} else

				{
					TranRPT.setCustomerMobileNo(rs.getString("CUSTOMER_MOBILE_NO"));

				}

				if (rs.getString("POLICY_NUMBER") == null) {

					TranRPT.setPolicyNo("");

				} else

				{
					TranRPT.setPolicyNo(rs.getString("POLICY_NUMBER"));

				}

				if (rs.getString("POLICY_STATUS") == null) {

					TranRPT.setPolicyStatus("");

				} else

				{
					TranRPT.setPolicyStatus(rs.getString("POLICY_STATUS"));

				}

				if (rs.getString("EBPG_REFNO") == null) {

					TranRPT.setEBPG_RefNo("");
				} else

				{
					TranRPT.setEBPG_RefNo(rs.getString("EBPG_REFNO"));

				}

				if (rs.getString("EBPG_AUTHCODE") == null) {

					TranRPT.setEBPG_AuthCode("");
				} else

				{
					TranRPT.setEBPG_AuthCode(rs.getString("EBPG_AUTHCODE"));

				}

				if (rs.getString("M2U_REFNO") == null) {

					TranRPT.setM2U_RefNo("");
				} else

				{
					TranRPT.setM2U_RefNo(rs.getString("M2U_REFNO"));

				}

				if (rs.getString("M2U_AUTHCODE") == null) {

					TranRPT.setM2U_AuthCode("");
				} else

				{
					TranRPT.setM2U_AuthCode(rs.getString("M2U_AUTHCODE"));

				}

				if (rs.getString("FPX_REFNO") == null) {

					TranRPT.setFPX_RefNo("");
				} else

				{
					TranRPT.setFPX_RefNo(rs.getString("FPX_REFNO"));

				}

				if (rs.getString("FPX_AUTHCODE") == null) {

					TranRPT.setFPX_AuthCode("");
				} else

				{
					TranRPT.setFPX_AuthCode(rs.getString("FPX_AUTHCODE"));

				}

				if (TranRPT.getPaymentMethod().indexOf("EBPG") != -1) {

					TranRPT.setPaymentMethod("CREDIT CARD");
					TranRPT.setPaymentChannel("EBPG");
				}

				else if (TranRPT.getPaymentMethod().indexOf("AMEX") != -1) {

					TranRPT.setPaymentMethod("CREDIT CARD");
					TranRPT.setPaymentChannel("AMEX");
				}

				else if (TranRPT.getPaymentMethod().indexOf("FPX") != -1) {

					TranRPT.setPaymentMethod("ONLINE BANKING");
					TranRPT.setPaymentChannel("FPX");
				}

				else if (TranRPT.getPaymentMethod().indexOf("Maybank2U") != -1) {

					TranRPT.setPaymentMethod("DEBIT ACCOUNT");
					TranRPT.setPaymentChannel("M2U");
				} else {
					TranRPT.setPaymentMethod("");
					TranRPT.setPaymentChannel("");

				}
				if (rs.getString("POLICY_EFFECTIVE_TIMESTAMP") == null) {

					TranRPT.setPolicyEffectiveTimestamp("");

				} else

				{
					TranRPT.setPolicyEffectiveTimestamp(rs.getString("POLICY_EFFECTIVE_TIMESTAMP"));
				}

				if (rs.getString("POLICY_EXPIRY_TIMESTAMP") == null) {

					TranRPT.setPolicyExpiryTimestamp("");

				} else

				{
					TranRPT.setPolicyExpiryTimestamp(rs.getString("POLICY_EXPIRY_TIMESTAMP"));
				}

				if (rs.getString("BUDDY_COMBO") == null) {

					TranRPT.setBuddyCombo("");
				} else

				{

					TranRPT.setBuddyCombo(rs.getString("BUDDY_COMBO"));

				}

				if (rs.getString("PLAN_NAME") == null) {

					TranRPT.setPlanName("");
				} else

				{

					TranRPT.setPlanName(rs.getString("PLAN_NAME"));

				}

				if (rs.getString("TOTAL_PREMIUM_PAID") == null) {

					TranRPT.setTotalPremiumPayable("");
				} else

				{
					TranRPT.setTotalPremiumPayable(rs.getString("TOTAL_PREMIUM_PAID"));

				}

				if (rs.getString("DISCOUNT_AMOUNT") == null) {

					TranRPT.setDiscount("");

				} else {
					TranRPT.setDiscount(rs.getString("DISCOUNT_AMOUNT"));

				}

				if (rs.getString("GROSS_PREMIUM_GST") == null) {

					TranRPT.setGST("");
				}

				else {
					TranRPT.setGST(rs.getString("GROSS_PREMIUM_GST"));

				}

				if (rs.getString("CUSTOMER_GENDER") == null) {

					TranRPT.setCustomerGender("");
				}

				else {
					TranRPT.setCustomerGender(rs.getString("CUSTOMER_GENDER"));

				}

				if (rs.getString("CREATED_DATE") == null) {

					TranRPT.setQuotationCreateDateTime("");
				}

				else {
					TranRPT.setQuotationCreateDateTime(rs.getString("CREATED_DATE"));

				}

				TranRPT.setQuotationUpdateDateTime("");

				if (rs.getString("SPOUSE_NAME") == null) {

					TranRPT.setSpouseName("");
				}

				else {
					TranRPT.setSpouseName(rs.getString("SPOUSE_NAME"));

				}

				if (rs.getString("SPOUSE_EMAIL") == null) {

					TranRPT.setSpouseEmail("");
				}

				else {
					TranRPT.setSpouseEmail(rs.getString("SPOUSE_EMAIL"));

				}

				if (rs.getString("SPOUSE_ID_TYPE") == null) {

					TranRPT.setSpouseIDType("");
				}

				else {
					TranRPT.setSpouseIDType(rs.getString("SPOUSE_ID_TYPE"));

				}

				if (rs.getString("SPOUSE_ID_NUMBER") == null) {

					TranRPT.setSpouseIDNumber("");
				}

				else {
					TranRPT.setSpouseIDNumber(rs.getString("SPOUSE_ID_NUMBER"));

				}

				if (rs.getString("SPOUSE_DOB") == null) {

					TranRPT.setSpouseDOB("");
				}

				else {
					TranRPT.setSpouseDOB(rs.getString("SPOUSE_DOB"));

				}

				if (rs.getString("SPOUSE_GENDER") == null) {

					TranRPT.setSpouseGender("");
				}

				else {
					TranRPT.setSpouseGender(rs.getString("SPOUSE_GENDER"));

				}

				if (rs.getString("CHILD_NAME") == null) {

					TranRPT.setChildName("");
				}

				else {

					// later plan, set TranRPT.setChildName as array

					TranRPT.setChildName(rs.getString("CHILD_NAME"));

					if (TranRPT.getChildName().indexOf(",") != -1) {

						String[] childName = TranRPT.getChildName().split(",");

						int childCount = childName.length;

						for (int j = 0; j < childCount; j++) {

							if (j == 0) {
								TranRPT.setChildName_1(childName[j]);
							}
							if (j == 1) {
								TranRPT.setChildName_2(childName[j]);
							}
							if (j == 2) {
								TranRPT.setChildName_3(childName[j]);
							}
							if (j == 3) {
								TranRPT.setChildName_4(childName[j]);
							}
							if (j == 4) {
								TranRPT.setChildName_5(childName[j]);
							}

							if (j == 5) {
								TranRPT.setChildName_6(childName[j]);
							}

							if (j == 6) {
								TranRPT.setChildName_7(childName[j]);
							}

							if (j == 7) {
								TranRPT.setChildName_8(childName[j]);
							}

							if (j == 8) {
								TranRPT.setChildName_9(childName[j]);
							}

							if (j == 9) {
								TranRPT.setChildName_10(childName[j]);
							}

						}

					}

					else {
						TranRPT.setChildName_1(rs.getString("CHILD_NAME"));

					}

				}

				if (rs.getString("CHILD_ID_TYPE") == null) {

					TranRPT.setChildIDType("");
				}

				else {
					TranRPT.setChildIDType(rs.getString("CHILD_ID_TYPE"));

					if (TranRPT.getChildIDType().indexOf(",") != -1) {

						String[] childIDType = TranRPT.getChildIDType().split(",");

						int childCount = childIDType.length;

						for (int j = 0; j < childCount; j++) {

							if (j == 0) {
								TranRPT.setChildIDType_1(childIDType[j]);
							}
							if (j == 1) {
								TranRPT.setChildIDType_2(childIDType[j]);
							}
							if (j == 2) {
								TranRPT.setChildIDType_3(childIDType[j]);
							}
							if (j == 3) {
								TranRPT.setChildIDType_4(childIDType[j]);
							}
							if (j == 4) {
								TranRPT.setChildIDType_5(childIDType[j]);
							}

							if (j == 5) {
								TranRPT.setChildIDType_6(childIDType[j]);
							}

							if (j == 6) {
								TranRPT.setChildIDType_7(childIDType[j]);
							}

							if (j == 7) {
								TranRPT.setChildIDType_8(childIDType[j]);
							}

							if (j == 8) {
								TranRPT.setChildIDType_9(childIDType[j]);
							}

							if (j == 9) {
								TranRPT.setChildIDType_10(childIDType[j]);
							}

						}

					}

					else {
						TranRPT.setChildIDType_1(rs.getString("CHILD_ID_TYPE"));

					}

				}

				if (rs.getString("CHILD_ID_NUMBER") == null) {

					TranRPT.setChildIDNumber("");
				}

				else {
					TranRPT.setChildIDNumber(rs.getString("CHILD_ID_NUMBER"));

					if (TranRPT.getChildIDNumber().indexOf(",") != -1) {

						String[] childIDNumber = TranRPT.getChildIDNumber().split(",");

						int childCount = childIDNumber.length;

						for (int j = 0; j < childCount; j++) {

							if (j == 0) {
								TranRPT.setChildIDNumber_1(childIDNumber[j]);
							}
							if (j == 1) {
								TranRPT.setChildIDNumber_2(childIDNumber[j]);
							}
							if (j == 2) {
								TranRPT.setChildIDNumber_3(childIDNumber[j]);
							}
							if (j == 3) {
								TranRPT.setChildIDNumber_4(childIDNumber[j]);
							}
							if (j == 4) {
								TranRPT.setChildIDNumber_5(childIDNumber[j]);
							}

							if (j == 5) {
								TranRPT.setChildIDNumber_6(childIDNumber[j]);
							}

							if (j == 6) {
								TranRPT.setChildIDNumber_7(childIDNumber[j]);
							}

							if (j == 7) {
								TranRPT.setChildIDNumber_8(childIDNumber[j]);
							}

							if (j == 8) {
								TranRPT.setChildIDNumber_9(childIDNumber[j]);
							}

							if (j == 9) {
								TranRPT.setChildIDNumber_10(childIDNumber[j]);
							}

						}

					}

					else {
						TranRPT.setChildIDNumber_1(rs.getString("CHILD_ID_NUMBER"));

					}

				}

				if (rs.getString("CHILD_DOB") == null) {

					TranRPT.setChildDOB("");
				}

				else {
					TranRPT.setChildDOB(rs.getString("CHILD_DOB"));

					if (TranRPT.getChildDOB().indexOf(",") != -1) {

						String[] childDOB = TranRPT.getChildDOB().split(",");

						int childCount = childDOB.length;

						for (int j = 0; j < childCount; j++) {

							if (j == 0) {
								TranRPT.setChildDOB_1(childDOB[j]);
							}
							if (j == 1) {
								TranRPT.setChildDOB_2(childDOB[j]);
							}
							if (j == 2) {
								TranRPT.setChildDOB_3(childDOB[j]);
							}
							if (j == 3) {
								TranRPT.setChildDOB_4(childDOB[j]);
							}
							if (j == 4) {
								TranRPT.setChildDOB_5(childDOB[j]);
							}

							if (j == 5) {
								TranRPT.setChildDOB_6(childDOB[j]);
							}

							if (j == 6) {
								TranRPT.setChildDOB_7(childDOB[j]);
							}

							if (j == 7) {
								TranRPT.setChildDOB_8(childDOB[j]);
							}

							if (j == 8) {
								TranRPT.setChildDOB_9(childDOB[j]);
							}

							if (j == 9) {
								TranRPT.setChildDOB_10(childDOB[j]);
							}

						}

					}

					else {
						TranRPT.setChildDOB_1(rs.getString("CHILD_DOB"));

					}

				}

				if (rs.getString("CHILD_GENDER") == null) {

					TranRPT.setChildGender("");
				}

				else {
					TranRPT.setChildGender(rs.getString("CHILD_GENDER"));

					if (TranRPT.getChildGender().indexOf(",") != -1) {

						String[] childGender = TranRPT.getChildGender().split(",");

						int childCount = childGender.length;

						for (int j = 0; j < childCount; j++) {

							if (j == 0) {
								TranRPT.setChildGender_1(childGender[j]);
							}
							if (j == 1) {
								TranRPT.setChildGender_2(childGender[j]);
							}
							if (j == 2) {
								TranRPT.setChildGender_3(childGender[j]);
							}
							if (j == 3) {
								TranRPT.setChildGender_4(childGender[j]);
							}
							if (j == 4) {
								TranRPT.setChildGender_5(childGender[j]);
							}

							if (j == 5) {
								TranRPT.setChildGender_6(childGender[j]);
							}

							if (j == 6) {
								TranRPT.setChildGender_7(childGender[j]);
							}

							if (j == 7) {
								TranRPT.setChildGender_8(childGender[j]);
							}

							if (j == 8) {
								TranRPT.setChildGender_9(childGender[j]);
							}

							if (j == 9) {
								TranRPT.setChildGender_10(childGender[j]);
							}

						}

					}

					else {
						TranRPT.setChildGender_1(rs.getString("CHILD_GENDER"));

					}

				}

				TranRPT.setRecordCount(Integer.parseInt(rs.getString("RECORDCOUNT")));
				TranRPT.setTotalAmount(rs.getString("TOTALAMOUNT"));

				if (rs.getString("LAST_PAGE") == null) {

					TranRPT.setLastPage("");

				} else

				{
					TranRPT.setLastPage(rs.getString("LAST_PAGE"));
				}

				list.add(TranRPT);

			}

			return list;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}
	
	@Override
	public List<TransactionalReport> getPCCA01Record(String productEntity, String status, String policyCertificateNo,
			String dateFrom, String dateTo, String NRIC, String receiptNo, String agentCode) {
		ResultSet rs = null;
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		
		  logger.info("PCCA01");
		  logger.info("**********************************************");
		 // logger.info("productType "+productType);
		  logger.info("productEntity "+productEntity);
		  logger.info("status "+status);
		  logger.info("policyCertificateNo "+policyCertificateNo);
		  logger.info("dateFrom "+dateFrom);
		  logger.info("dateTo "+dateTo); logger.info("NRIC "+NRIC);
		  logger.info("receiptNo "+receiptNo);
		 // logger.info("plateNo "+plateNo);
		  logger.info("agentCode "+agentCode);
		  logger.info("**********************************************");
		 
		logger.info("productEntity "+productEntity);
		logger.info("status "+status);
		logger.info("dateFrom "+dateFrom);
		logger.info("dateTo "+dateTo); 
		try {

			int i = 1;
			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_PCCA01_ALL_TMP(?,?,?,?,?,?,?,?,?)}");
			cstmt.setString(1, productEntity.trim());
			cstmt.setString(2, status);
			cstmt.setString(3, policyCertificateNo);
			cstmt.setString(4, dateFrom);
			cstmt.setString(5, dateTo);
			cstmt.setString(6, NRIC);
			cstmt.setString(7, receiptNo);
			cstmt.setString(8, agentCode);
			cstmt.registerOutParameter(9, OracleTypes.CURSOR);
			cstmt.execute();
			;

			List<TransactionalReport> list = new ArrayList<TransactionalReport>();
			rs = (ResultSet) cstmt.getObject(9);

			while (rs.next()) {

				TransactionalReport TranRPT = new TransactionalReport();
				TranRPT.setId(i);

				TranRPT.setProductEntity("EIB");

				if (rs.getString("DSP_QQ_ID") == null) {

					TranRPT.setDSPQQID("");
				} else {

					TranRPT.setDSPQQID(rs.getString("DSP_QQ_ID"));
				}

				if (rs.getString("TRANSACTIONID") == null) {

					TranRPT.setTransactionID("");
				} else {

					TranRPT.setTransactionID(rs.getString("TRANSACTIONID"));
				}

				if (rs.getString("PMNT_GATEWAY_CODE") == null) {

					TranRPT.setPaymentMethod("");
				} else

				{
					TranRPT.setPaymentMethod(rs.getString("PMNT_GATEWAY_CODE"));

				}

				if (rs.getString("AMOUNT") == null) {

					TranRPT.setPremiumAmount("");

				} else {
					TranRPT.setPremiumAmount(rs.getString("AMOUNT"));

				}

				if (rs.getString("PMNT_STATUS") == null) {

					TranRPT.setStatus("");

				} else

				{
					TranRPT.setStatus(rs.getString("PMNT_STATUS"));

				}

				if (rs.getString("TRANSACTION_DATETIME") == null) {

					TranRPT.setTransactionDate("");

				} else

				{

					String valueFromDB = rs.getString("TRANSACTION_DATETIME");
					Date d1 = null;
					try {
						d1 = format1.parse(valueFromDB);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					String transactionDateTime = ft2.format(d1);
					TranRPT.setTransactionDate(transactionDateTime);

					// logger.info("TL TranRPT.getTransactionDate()
					// "+TranRPT.getTransactionDate());

				}

				if (rs.getString("AGENT_CODE") == null) {

					TranRPT.setAgentCode("");

				} else

				{
					TranRPT.setAgentCode(rs.getString("AGENT_CODE"));

				}

				if (rs.getString("AGENT_NAME") == null) {

					TranRPT.setAgentName("");

				} else

				{
					TranRPT.setAgentName(rs.getString("AGENT_NAME"));

				}

				if (rs.getString("OPERATOR_CODE") == null) {

					TranRPT.setOperatorCode("");

				} else

				{
					TranRPT.setOperatorCode(rs.getString("OPERATOR_CODE"));

				}

				if (rs.getString("CUSTOMER_NRIC_ID") == null) {

					TranRPT.setCustomerNRIC("");

				} else

				{
					TranRPT.setCustomerNRIC(rs.getString("CUSTOMER_NRIC_ID"));

				}

				if (rs.getString("CUSTOMER_NAME") == null) {

					TranRPT.setCustomerName(rs.getString("FULL_NAME"));

				} else

				{
					TranRPT.setCustomerName(removeScriptTags(rs.getString("CUSTOMER_NAME")));

				}

				if (rs.getString("CUSTOMER_GENDER") == null) {

					TranRPT.setCustomerGender("");

				} else

				{
					TranRPT.setCustomerGender(rs.getString("CUSTOMER_GENDER"));

				}

				if (rs.getString("CUSTOMER_ADDRESS1") == null) {

					TranRPT.setCustomerAddress1("");

				} else

				{
					TranRPT.setCustomerAddress1(rs.getString("CUSTOMER_ADDRESS1"));

				}

				if (rs.getString("CUSTOMER_ADDRESS2") == null) {

					TranRPT.setCustomerAddress2("");

				} else

				{
					TranRPT.setCustomerAddress2(rs.getString("CUSTOMER_ADDRESS2"));

				}

				if (rs.getString("CUSTOMER_ADDRESS3") == null) {

					TranRPT.setCustomerAddress3("");

				} else

				{
					TranRPT.setCustomerAddress3(rs.getString("CUSTOMER_ADDRESS3"));

				}

				if (rs.getString("CUSTOMER_POSTCODE") == null) {

					TranRPT.setCustomerPostcode("");

				} else

				{
					TranRPT.setCustomerPostcode(rs.getString("CUSTOMER_POSTCODE"));

				}

				if (rs.getString("CUSTOMER_STATE") == null) {

					TranRPT.setCustomerState("");

				} else

				{
					TranRPT.setCustomerState(rs.getString("CUSTOMER_STATE"));

				}

				if (rs.getString("CUSTOMER_EMAIL") == null) {

					TranRPT.setCustomerEmail(rs.getString("LEADS_EMAIL_ID"));

				} else

				{
					TranRPT.setCustomerEmail(rs.getString("CUSTOMER_EMAIL"));

				}

				if (rs.getString("CUSTOMER_MOBILE_NO") == null) {

					TranRPT.setCustomerMobileNo("");

				} else

				{
					TranRPT.setCustomerMobileNo(rs.getString("CUSTOMER_MOBILE_NO"));

				}

				if (rs.getString("POLICY_NUMBER") == null) {

					TranRPT.setPolicyNo("");

				} else

				{
					TranRPT.setPolicyNo(rs.getString("POLICY_NUMBER"));

				}

				if (rs.getString("MPAY_REFNO") == null) {

					TranRPT.setMPAY_RefNo("");
				} else

				{
					TranRPT.setMPAY_RefNo(rs.getString("MPAY_REFNO"));

				}

				if (rs.getString("MPAY_AUTHCODE") == null) {

					TranRPT.setMPAY_AuthCode("");
				} else

				{
					TranRPT.setMPAY_AuthCode(rs.getString("MPAY_AUTHCODE"));

				}

				TranRPT.setPaymentChannel("MPAY");

				TranRPT.setPaymentMethod("CREDIT CARD");

				if (rs.getString("PROMO_CODE") == null) {

					TranRPT.setDiscountCode("");
				} else

				{

					TranRPT.setDiscountCode(rs.getString("PROMOTION_CODE"));

				}

				if (rs.getString("CUSTOMER_GENDER") == null) {

					TranRPT.setCustomerGender("");
				}

				else {
					TranRPT.setCustomerGender(rs.getString("CUSTOMER_GENDER"));

				}

				if (rs.getString("CREATED_DATE") == null) {

					TranRPT.setQuotationCreateDateTime("");
				}

				else {
					TranRPT.setQuotationCreateDateTime(rs.getString("CREATED_DATE"));

				}

				if (rs.getString("UPDATE_DATETIME") == null) {

					TranRPT.setQuotationUpdateDateTime("");
				}

				else {
					TranRPT.setQuotationUpdateDateTime(rs.getString("UPDATE_DATETIME"));

				}

				/*
				 * if(rs.getString("TRANSACTION_DATETIME") == null){
				 *
				 * TranRPT.setQuotationUpdateDateTime(""); }
				 *
				 * else {
				 *
				 *
				 * String valueFromDB = rs.getString("TRANSACTION_DATETIME"); Date d1 = null;
				 * try { d1 = sdf2.parse(valueFromDB); } catch (ParseException e) { // TODO
				 * Auto-generated catch block e.printStackTrace(); } SimpleDateFormat sdf = new
				 * SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); String transactionDateTime =
				 * sdf.format(d1); //logger.info("txn date wtc " + transactionDateTime);
				 * TranRPT.setQuotationUpdateDateTime(transactionDateTime);
				 *
				 * }
				 */

				if (rs.getString("CI_SUM_INSURED") == null) {

					TranRPT.setCoverage("");
				}

				else {
					TranRPT.setCoverage(rs.getString("CI_SUM_INSURED"));

				}

				if (rs.getString("DURATION_OF_BENIFIT") == null) {

					TranRPT.setTerm("");
				}

				else {
					TranRPT.setTerm(rs.getString("DURATION_OF_BENIFIT"));

				}

				if (rs.getString("REJECTED_REASON") == null) {

					TranRPT.setReason("");
				}

				else {
					TranRPT.setReason(rs.getString("REJECTED_REASON"));

				}

				if (rs.getString("PREMIUM_PAYMENT_FREQUENCY_TYPE") == null) {

					TranRPT.setPremiumMode("");

				}

				else {
					if (rs.getString("PREMIUM_PAYMENT_FREQUENCY_TYPE").trim() != "") {

						String tmp = rs.getString("PREMIUM_PAYMENT_FREQUENCY_TYPE").substring(0, 1).toUpperCase()
								+ rs.getString("PREMIUM_PAYMENT_FREQUENCY_TYPE").substring(1);
						TranRPT.setPremiumMode(tmp);

					} else {

						TranRPT.setPremiumMode(rs.getString("PREMIUM_PAYMENT_FREQUENCY_TYPE"));

					}

				}

				TranRPT.setMode(rs.getString("PREMIUM_PAYMENT_FREQUENCY_TYPE"));

				if (rs.getString("AMOUNT") == null) {

					TranRPT.setPremiumAmount("");
				}

				else {
					TranRPT.setPremiumAmount(rs.getString("AMOUNT"));

				}

				if (rs.getString("AGENT_CODE") == null) {

					TranRPT.setAgentCode("");
				}

				else {
					TranRPT.setAgentCode(rs.getString("AGENT_CODE"));

				}

				if (rs.getString("AGENT_NAME") == null) {

					TranRPT.setAgentName("");
				}

				else {
					TranRPT.setAgentName(rs.getString("AGENT_NAME"));

				}

				if (rs.getString("OPERATOR_CODE") == null) {

					TranRPT.setOperatorCode("");
				}

				else {
					TranRPT.setOperatorCode(rs.getString("OPERATOR_CODE"));

				}

				if (rs.getString("PRODUCT_CODE_NAME") == null) {

					TranRPT.setProductType("");
				}

				else {
					TranRPT.setProductType(rs.getString("PRODUCT_CODE_NAME"));

				}

				if (rs.getString("PRODUCT_CODE") == null) {

					TranRPT.setProductCode("");
				}

				else {
					TranRPT.setProductCode(rs.getString("PRODUCT_CODE"));

				}

				if (rs.getString("POLICY_NUMBER") == null) {

					TranRPT.setPolicyNo("");
				}

				else {
					TranRPT.setPolicyNo(rs.getString("POLICY_NUMBER"));

				}

				if (rs.getString("PREMIUM_PAYMENT_FREQUENCY") == null) {

					TranRPT.setAnnualPremium("");
				}

				else {

					if (TranRPT.getMode().equals("monthly")) {

						// multiply by 12 - do calculation here

						BigDecimal amt1 = new BigDecimal(rs.getString("PREMIUM_PAYMENT_FREQUENCY"));
						BigDecimal amt2 = new BigDecimal(12);
						BigDecimal tmpAmount = new BigDecimal(0.00);

						tmpAmount = amt2.multiply(amt1);

						TranRPT.setAnnualPremium(String.valueOf(tmpAmount));

					} else {
						TranRPT.setAnnualPremium(rs.getString("PREMIUM_PAYMENT_FREQUENCY"));

					}

				}

				TranRPT.setRecordCount(Integer.parseInt(rs.getString("RECORDCOUNT")));
				TranRPT.setTotalAmount(rs.getString("TOTALAMOUNT"));

				if (rs.getString("LAST_PAGE") == null) {

					TranRPT.setLastPage("");

				} else

				{
					TranRPT.setLastPage(rs.getString("LAST_PAGE"));
				}

				if (rs.getString("REJECTED_REASON") == null) {

					TranRPT.setReason("");

				} else {

					TranRPT.setReason(rs.getString("REJECTED_REASON"));
				}

				if (rs.getString("UWREASON") == null) {

					TranRPT.setUWReason("");

				} else {

					TranRPT.setUWReason(rs.getString("UWREASON"));
				}

				list.add(TranRPT);

			}

			return list;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}

	}
	
	@Override
	public String getCustomerlanguage(String qq_id) {
		// TODO Auto-generated method stub
		
		ResultSet rs = null;
		String	language = null;
		try {

			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_MP_CUST_LAN(?,?)}");
			cstmt.setString(1, qq_id);
			cstmt.registerOutParameter(2, Types.VARCHAR);
			cstmt.execute();
			language = (String) cstmt.getObject(2);
		
		}catch(Exception e){
				logger.info(e);
			}
		return language;
	}
	

	
	@Override
	public List<TransactionalReport>getMPRecord(String productEntity, String status, String policyCertificateNo,
			String dateFrom, String dateTo, String NRIC, String receiptNo, String agentCode) {
		ResultSet rs = null;
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		/*
		 * logger.info("PCCA01");
		 * logger.info("**********************************************");
		 * logger.info("productType "+productType);
		 * logger.info("productEntity "+productEntity);
		 * logger.info("status "+status);
		 * logger.info("policyCertificateNo "+policyCertificateNo);
		 * logger.info("dateFrom "+dateFrom);
		 * logger.info("dateTo "+dateTo); logger.info("NRIC "+NRIC);
		 * logger.info("receiptNo "+receiptNo);
		 * logger.info("plateNo "+plateNo);
		 * logger.info("agentCode "+agentCode);
		 * logger.info("**********************************************");
		 */

		try {

			int i = 1;
			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_MP_ALL_TMP(?,?,?,?,?,?,?,?,?)}");
			cstmt.setString(1, productEntity.trim());
			cstmt.setString(2, status);
			cstmt.setString(3, policyCertificateNo);
			cstmt.setString(4, dateFrom);
			cstmt.setString(5, dateTo);
			cstmt.setString(6, NRIC);
			cstmt.setString(7, receiptNo);
			cstmt.setString(8, agentCode);
			cstmt.registerOutParameter(9, OracleTypes.CURSOR);
			cstmt.execute();
			;

			List<TransactionalReport> list = new ArrayList<TransactionalReport>();
			rs = (ResultSet) cstmt.getObject(9);

			while (rs.next()) {

				TransactionalReport TranRPT = new TransactionalReport();
				TranRPT.setId(i);

				TranRPT.setProductEntity("EIB");

				if (rs.getString("DSP_QQ_ID") == null) {

					TranRPT.setDSPQQID("");
				} else {

					TranRPT.setDSPQQID(rs.getString("DSP_QQ_ID"));
				}

				if (rs.getString("TRANSACTIONID") == null) {

					TranRPT.setTransactionID("");
				} else {

					TranRPT.setTransactionID(rs.getString("TRANSACTIONID"));
				}

				if (rs.getString("PMNT_GATEWAY_CODE") == null) {

					TranRPT.setPaymentMethod("");
				} else

				{
					TranRPT.setPaymentMethod(rs.getString("PMNT_GATEWAY_CODE"));

				}

				if (rs.getString("AMOUNT") == null) {

					TranRPT.setPremiumAmount("");

				} else {
					TranRPT.setPremiumAmount(rs.getString("AMOUNT"));

				}

				if (rs.getString("PMNT_STATUS") == null) {

					TranRPT.setStatus("");

				} else

				{
					TranRPT.setStatus(rs.getString("PMNT_STATUS"));

				}

				if (rs.getString("TRANSACTION_DATETIME") == null) {

					TranRPT.setTransactionDate("");

				} else

				{

					String valueFromDB = rs.getString("TRANSACTION_DATETIME");
					Date d1 = null;
					try {
						d1 = format1.parse(valueFromDB);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					String transactionDateTime = ft2.format(d1);
					TranRPT.setTransactionDate(transactionDateTime);

					// logger.info("TL TranRPT.getTransactionDate()
					// "+TranRPT.getTransactionDate());

				}

				if (rs.getString("AGENT_CODE") == null) {

					TranRPT.setAgentCode("");

				} else

				{
					TranRPT.setAgentCode(rs.getString("AGENT_CODE"));

				}

				if (rs.getString("AGENT_NAME") == null) {

					TranRPT.setAgentName("");

				} else

				{
					TranRPT.setAgentName(rs.getString("AGENT_NAME"));

				}

				if (rs.getString("OPERATOR_CODE") == null) {

					TranRPT.setOperatorCode("");

				} else

				{
					TranRPT.setOperatorCode(rs.getString("OPERATOR_CODE"));

				}

				if (rs.getString("CUSTOMER_NRIC_ID") == null) {

					TranRPT.setCustomerNRIC("");

				} else

				{
					TranRPT.setCustomerNRIC(rs.getString("CUSTOMER_NRIC_ID"));

				}

				if (rs.getString("CUSTOMER_NAME") == null) {

					TranRPT.setCustomerName(rs.getString("FULL_NAME"));

				} else

				{
					TranRPT.setCustomerName(removeScriptTags(rs.getString("CUSTOMER_NAME")));

				}

				if (rs.getString("CUSTOMER_GENDER") == null) {

					TranRPT.setCustomerGender("");

				} else

				{
					TranRPT.setCustomerGender(rs.getString("CUSTOMER_GENDER"));

				}

				if (rs.getString("CUSTOMER_ADDRESS1") == null) {

					TranRPT.setCustomerAddress1("");

				} else

				{
					TranRPT.setCustomerAddress1(rs.getString("CUSTOMER_ADDRESS1"));

				}

				if (rs.getString("CUSTOMER_ADDRESS2") == null) {

					TranRPT.setCustomerAddress2("");

				} else

				{
					TranRPT.setCustomerAddress2(rs.getString("CUSTOMER_ADDRESS2"));

				}

				if (rs.getString("CUSTOMER_ADDRESS3") == null) {

					TranRPT.setCustomerAddress3("");

				} else

				{
					TranRPT.setCustomerAddress3(rs.getString("CUSTOMER_ADDRESS3"));

				}

				if (rs.getString("CUSTOMER_POSTCODE") == null) {

					TranRPT.setCustomerPostcode("");

				} else

				{
					TranRPT.setCustomerPostcode(rs.getString("CUSTOMER_POSTCODE"));

				}

				if (rs.getString("CUSTOMER_STATE") == null) {

					TranRPT.setCustomerState("");

				} else

				{
					TranRPT.setCustomerState(rs.getString("CUSTOMER_STATE"));

				}

				if (rs.getString("CUSTOMER_EMAIL") == null) {

					TranRPT.setCustomerEmail(rs.getString("LEADS_EMAIL_ID"));

				} else

				{
					TranRPT.setCustomerEmail(rs.getString("CUSTOMER_EMAIL"));

				}

				if (rs.getString("CUSTOMER_MOBILE_NO") == null) {

					TranRPT.setCustomerMobileNo("");

				} else

				{
					TranRPT.setCustomerMobileNo(rs.getString("CUSTOMER_MOBILE_NO"));

				}

				if (rs.getString("POLICY_NUMBER") == null) {

					TranRPT.setPolicyNo("");

				} else

				{
					TranRPT.setPolicyNo(rs.getString("POLICY_NUMBER"));

				}

				if (rs.getString("MPAY_REFNO") == null) {

					TranRPT.setMPAY_RefNo("");
				} else

				{
					TranRPT.setMPAY_RefNo(rs.getString("MPAY_REFNO"));

				}

				if (rs.getString("MPAY_AUTHCODE") == null) {

					TranRPT.setMPAY_AuthCode("");
				} else

				{
					TranRPT.setMPAY_AuthCode(rs.getString("MPAY_AUTHCODE"));

				}

				TranRPT.setPaymentChannel("MPAY");

				TranRPT.setPaymentMethod("CREDIT CARD");

				if (rs.getString("PROMO_CODE") == null) {

					TranRPT.setDiscountCode("");
				} else

				{

					TranRPT.setDiscountCode(rs.getString("PROMOTION_CODE"));

				}

				if (rs.getString("CUSTOMER_GENDER") == null) {

					TranRPT.setCustomerGender("");
				}

				else {
					TranRPT.setCustomerGender(rs.getString("CUSTOMER_GENDER"));

				}

				if (rs.getString("CREATED_DATE") == null) {

					TranRPT.setQuotationCreateDateTime("");
				}

				else {
					TranRPT.setQuotationCreateDateTime(rs.getString("CREATED_DATE"));

				}

				if (rs.getString("UPDATE_DATETIME") == null) {

					TranRPT.setQuotationUpdateDateTime("");
				}

				else {
					TranRPT.setQuotationUpdateDateTime(rs.getString("UPDATE_DATETIME"));

				}

				/*
				 * if(rs.getString("TRANSACTION_DATETIME") == null){
				 *
				 * TranRPT.setQuotationUpdateDateTime(""); }
				 *
				 * else {
				 *
				 *
				 * String valueFromDB = rs.getString("TRANSACTION_DATETIME"); Date d1 = null;
				 * try { d1 = sdf2.parse(valueFromDB); } catch (ParseException e) { // TODO
				 * Auto-generated catch block e.printStackTrace(); } SimpleDateFormat sdf = new
				 * SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); String transactionDateTime =
				 * sdf.format(d1); //logger.info("txn date wtc " + transactionDateTime);
				 * TranRPT.setQuotationUpdateDateTime(transactionDateTime);
				 *
				 * }
				 */

				if (rs.getString("MP_SUM_INSURED") == null) {

					TranRPT.setCoverage("");
				}

				else {
					TranRPT.setCoverage(rs.getString("MP_SUM_INSURED"));

				}

				if (rs.getString("DURATION_OF_BENIFIT") == null) {

					TranRPT.setTerm("");
				}

				else {
					TranRPT.setTerm(rs.getString("DURATION_OF_BENIFIT"));

				}

				if (rs.getString("REJECTED_REASON") == null) {

					TranRPT.setReason("");
				}

				else {
					TranRPT.setReason(rs.getString("REJECTED_REASON"));

				}

				if (rs.getString("PREMIUM_PAYMENT_FREQUENCY_TYPE") == null) {

					TranRPT.setPremiumMode("");

				}

				else {
					if (rs.getString("PREMIUM_PAYMENT_FREQUENCY_TYPE").trim() != "") {

						String tmp = rs.getString("PREMIUM_PAYMENT_FREQUENCY_TYPE").substring(0, 1).toUpperCase()
								+ rs.getString("PREMIUM_PAYMENT_FREQUENCY_TYPE").substring(1);
						TranRPT.setPremiumMode(tmp);

					} else {

						TranRPT.setPremiumMode(rs.getString("PREMIUM_PAYMENT_FREQUENCY_TYPE"));

					}

				}

				TranRPT.setMode(rs.getString("PREMIUM_PAYMENT_FREQUENCY_TYPE"));

				if (rs.getString("AMOUNT") == null) {

					TranRPT.setPremiumAmount("");
				}

				else {
					TranRPT.setPremiumAmount(rs.getString("AMOUNT"));

				}

				if (rs.getString("AGENT_CODE") == null) {

					TranRPT.setAgentCode("");
				}

				else {
					TranRPT.setAgentCode(rs.getString("AGENT_CODE"));

				}

				if (rs.getString("AGENT_NAME") == null) {

					TranRPT.setAgentName("");
				}

				else {
					TranRPT.setAgentName(rs.getString("AGENT_NAME"));

				}

				if (rs.getString("OPERATOR_CODE") == null) {

					TranRPT.setOperatorCode("");
				}

				else {
					TranRPT.setOperatorCode(rs.getString("OPERATOR_CODE"));

				}

				if (rs.getString("PRODUCT_CODE_NAME") == null) {

					TranRPT.setProductType("");
				}

				else {
					TranRPT.setProductType(rs.getString("PRODUCT_CODE_NAME"));

				}

				if (rs.getString("PRODUCT_CODE") == null) {

					TranRPT.setProductCode("");
				}

				else {
					TranRPT.setProductCode(rs.getString("PRODUCT_CODE"));

				}

				if (rs.getString("POLICY_NUMBER") == null) {

					TranRPT.setPolicyNo("");
				}

				else {
					TranRPT.setPolicyNo(rs.getString("POLICY_NUMBER"));

				}

				if (rs.getString("PREMIUM_PAYMENT_FREQUENCY") == null) {

					TranRPT.setAnnualPremium("");
				}

				else {

					if (TranRPT.getMode().equals("monthly")) {

						// multiply by 12 - do calculation here

						BigDecimal amt1 = new BigDecimal(rs.getString("PREMIUM_PAYMENT_FREQUENCY"));
						BigDecimal amt2 = new BigDecimal(12);
						BigDecimal tmpAmount = new BigDecimal(0.00);

						tmpAmount = amt2.multiply(amt1);

						TranRPT.setAnnualPremium(String.valueOf(tmpAmount));

					} else {
						TranRPT.setAnnualPremium(rs.getString("PREMIUM_PAYMENT_FREQUENCY"));

					}

				}

				TranRPT.setRecordCount(Integer.parseInt(rs.getString("RECORDCOUNT")));
				TranRPT.setTotalAmount(rs.getString("TOTALAMOUNT"));

				if (rs.getString("LAST_PAGE") == null) {

					TranRPT.setLastPage("");

				} else

				{
					TranRPT.setLastPage(rs.getString("LAST_PAGE"));
				}

				if (rs.getString("REJECTED_REASON") == null) {

					TranRPT.setReason("");

				} else {

					TranRPT.setReason(rs.getString("REJECTED_REASON"));
				}

				if (rs.getString("UWREASON") == null) {

					TranRPT.setUWReason("");

				} else {

					TranRPT.setUWReason(rs.getString("UWREASON"));
				}

				list.add(TranRPT);

			}

			return list;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}

	}

	
	
}
