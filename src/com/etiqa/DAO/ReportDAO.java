package com.etiqa.DAO;

import java.util.List;

import com.etiqa.model.report.SalesLeads;
import com.etiqa.model.report.TransactionalReport;

public interface ReportDAO {

	//////////////////////////////// start: add on 1 march
	//////////////////////////////// 2017///////////////////////////////
	// EXTRACTION
	public List<TransactionalReport> getMotorRecord(String ProductEntity, String Status, String PolicyCertificateNo,
			String DateFrom, String DateTo, String NRIC, String ReceiptNo, String PlateNo, String AgentCode);

	////////////////////////////// end: add on 1 march
	////////////////////////////// 2017////////////////////////////////////

	//////////////////////////////// start: add on 25 MAY
	//////////////////////////////// 2017///////////////////////////////

	public List<TransactionalReport> getMotorRecordAll(String ProductEntity, String Status, String PolicyCertificateNo,
			String DateFrom, String DateTo, String NRIC, String ReceiptNo, String PlateNo, String AgentCode);

	////////////////////////////// end: add on 25 MAY
	////////////////////////////// 2017////////////////////////////////////

	//////////////////////////////// start: add on 6 march
	//////////////////////////////// 2017///////////////////////////////

	public List<TransactionalReport> getHOHHRecord(String ProductType, String ProductEntity, String Status,
			String PolicyCertificateNo, String DateFrom, String DateTo, String NRIC, String ReceiptNo,
			String AgentCode);

	////////////////////////////// end: add on 6 march
	////////////////////////////// 2017////////////////////////////////////

	//////////////////////////////// start: add on 8 march
	//////////////////////////////// 2017///////////////////////////////

	public List<TransactionalReport> getTLRecord(String ProductEntity, String Status, String PolicyCertificateNo,
			String DateFrom, String DateTo, String NRIC, String ReceiptNo, String AgentCode);
	
	public List<TransactionalReport> getPCCA01Record(String ProductEntity, String Status, String PolicyCertificateNo,
			String DateFrom, String DateTo, String NRIC, String ReceiptNo, String AgentCode);
	
	public List<TransactionalReport> getMPRecord(String ProductEntity, String Status, String PolicyCertificateNo,
			String DateFrom, String DateTo, String NRIC, String ReceiptNo, String AgentCode);
	
	public String getCustomerlanguage(String qq_id);

	public List<TransactionalReport> getWTCRecord(String ProductEntity, String Status, String PolicyCertificateNo,
			String DateFrom, String DateTo, String NRIC, String ReceiptNo, String AgentCode);
	////////////////////////////// end: add on 8 march
	////////////////////////////// 2017////////////////////////////////////

	// FOR VIEW ALL
	public List<TransactionalReport> findByProductType(String ProductType, String ProductEntity, String Status,
			String PolicyCertificateNo, String DateFrom, String DateTo, String NRIC, String ReceiptNo, int StartRow,
			int EndRow, String PlateNo, String AgentCode);

	// FOR TRANSACTION DETAIL
	public List<TransactionalReport> getTransactionDetail(int ID);

	// FOR TRANSACTION DETAIL HOHH BENEFIT
	public List<TransactionalReport> getHOHHBenefit(int ID);

	// FOR UPDATE CUSTOMER INFO
	public String updateCustomerInfo(int ID, String NRIC, String name, String email, String address1, String address2,
			String address3, String postcode, String state, String mobileNo);

	// FOR UPDATE TRANSACTION DETAIL - CHANGE STATUS
	public String updateTxnInfo(int id, String status, String remark, int moduleID, String action, int createdBy,
			String preData, String postData, String actionBy);

	// public String updateTxnInfo(int id,String status, String remark, int
	// moduleID, String action, int createdBy);

	// FOR GENERATE POLICY NO
	public String generatePolicyNo(int transactionID);
	public String generatePolicy_MP(int transactionID);

	public List<TransactionalReport> getPolicyDetail(String QQID);

	public String generatePolicyNoHOHH(String DSPQQID, int transactionID);

	public String generatePolicyNoWTC(String DSPQQID, int transactionID);

	public String generatePolicyNoBuddy(String DSPQQID, int transactionID);

	public String generatePolicyNoTravEz(String DSPQQID, int transactionID);

	public String generatePolicyNoTripCare(String DSPQQID, int transactionID);

	public String generateCAPSPolicy(int transactionID);

	public String checkJPJStatus(String policyNo, String vehicleNo, String effectDate, String expiryDate);

	public int checkISMStatus(String QQID);

	// ezylife incomplete & reject
	public List<TransactionalReport> rejectRecordTL(String productType,
			// String ProductEntity,
			String status, String policyCertificateNo, String dateFrom, String dateTo, String NRIC, String receiptNo,
			int startRow, int endRow, String agentCode);

	public List<TransactionalReport> incompleteRecordTL(String productType,
			// String ProductEntity,
			String status, String policyCertificateNo, String dateFrom, String dateTo, String NRIC, String receiptNo,
			int startRow, int endRow, String agentCode);

	public List<TransactionalReport> getTLRecordDetail(String ProductType, String ProductEntity, String Status,
			String PolicyCertificateNo, String DateFrom, String DateTo, String NRIC, String ReceiptNo, String PlateNo,
			String AgentCode);

	public List<SalesLeads> findBySalesLeads(String agentName, String agentCode, String fromDate, String toDate,
			int startRow, int endRow);

	// For Insert Customer Audit information in Audit table
	public String insertCustomerAudit(int customerId, String userName, int userRole, int dspQqId);

	// For Insert Payment Audit information in Audit table
	public String insertPaymentforAudit(int customerId, String userName, int userRole, int dspQqId, String policyNo);

	// For Insert Agent Audit information in Audit table
	public String insertAgentforAudit(String agentCode, String userName, int userRole);
	////////////////////////////////// START : BUDDY
	////////////////////////////////// PA////////////////////////////////////////

	public List<TransactionalReport> getBuddyPARecord(String ProductEntity, String Status, String PolicyCertificateNo,
			String DateFrom, String DateTo, String NRIC, String ReceiptNo, String AgentCode);

	/////////////////////////////////// END : BUDDY
	/////////////////////////////////// PA/////////////////////////////////////////
}
