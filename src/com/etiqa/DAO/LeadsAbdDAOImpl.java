package com.etiqa.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.etiqa.common.DB.ConnectionFactory;
import com.spring.VO.SalesLead;

import oracle.jdbc.OracleTypes;

public class LeadsAbdDAOImpl implements LeadsAbdDAO {
	Connection connection = ConnectionFactory.getConnection();
	Statement statement = null;
	ResultSet rs = null;

	@Override
	public List<SalesLead> getLeadsAbdSearchRecords(SalesLead salesLead) {
		ResultSet rsScenario1 = null;
		ResultSet rsScenario2 = null;
		ResultSet rsScenario3 = null;
		connection = ConnectionFactory.getConnection();
		List<SalesLead> listScenario1 = new ArrayList<SalesLead>();
		try {
			CallableStatement cstmt = null;
			// HOHH MI WTC TL
			if (salesLead.getLeadsAbdProduct().equals("HOHH")) {
				cstmt = connection.prepareCall("{CALL DSP_ADM_SP_HOHH_ABND_REPO(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			} else if (salesLead.getLeadsAbdProduct().equals("HOHH-ETB")) {
				cstmt = connection.prepareCall("{CALL DSP_ADM_SP_HOHH_ABND_REPO(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			} else if (salesLead.getLeadsAbdProduct().equals("MI")) {
				cstmt = connection.prepareCall("{CALL DSP_ADM_SP_MI_ABND_REPO(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			} else if (salesLead.getLeadsAbdProduct().equals("MT")) {
				cstmt = connection.prepareCall("{CALL DSP_ADM_SP_MI_ABND_REPO(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			} else if (salesLead.getLeadsAbdProduct().equals("WTC")) {
				cstmt = connection.prepareCall("{CALL DSP_ADM_SP_WTC_ABND_REPO(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			} else if (salesLead.getLeadsAbdProduct().equals("TPT")) {
				cstmt = connection.prepareCall("{CALL DSP_ADM_SP_WTC_ABND_REPO(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			} else {
				cstmt = connection.prepareCall("{CALL DSP_ADM_SP_TL_ABND_REPO(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			}

			// CallableStatement cstmt = connection.prepareCall("{CALL
			// DSP_ADM_SP_RPT_TRANSACTIONAL_D(?,?)}");
			System.out.println(salesLead.getLeadsAbdProduct() + " : " + salesLead.getLeadsAbdAgent() + " : "
					+ salesLead.getLeadsAbdAgentCode() + " : " + salesLead.getLeadsAbdStatus() + " : "
					+ salesLead.getLeadsAbdStep() + " : " + salesLead.getLeadsAbdFromDate() + " : "
					+ salesLead.getLeadsAbdToDate());

			cstmt.setString(1, salesLead.getLeadsAbdProduct() == null ? "" : salesLead.getLeadsAbdProduct());
			cstmt.setString(2, salesLead.getLeadsAbdAgent() == null ? "" : salesLead.getLeadsAbdAgent());
			cstmt.setString(3, salesLead.getLeadsAbdAgentCode() == null ? "" : salesLead.getLeadsAbdAgentCode());
			cstmt.setString(4, salesLead.getLeadsAbdStatus() == null ? "" : salesLead.getLeadsAbdStatus());
			cstmt.setString(5, salesLead.getLeadsAbdStep() == null ? "" : salesLead.getLeadsAbdStep());
			cstmt.setString(6, dateFormat(salesLead.getLeadsAbdFromDate()));
			cstmt.setString(7, dateFormat(salesLead.getLeadsAbdToDate()));
			cstmt.registerOutParameter(8, OracleTypes.CURSOR);
			cstmt.registerOutParameter(9, OracleTypes.CURSOR);
			cstmt.registerOutParameter(10, OracleTypes.CURSOR);
			cstmt.registerOutParameter(11, OracleTypes.INTEGER);
			cstmt.registerOutParameter(12, OracleTypes.INTEGER);
			cstmt.registerOutParameter(13, OracleTypes.INTEGER);
			cstmt.registerOutParameter(14, OracleTypes.INTEGER);
			cstmt.registerOutParameter(15, OracleTypes.INTEGER);
			cstmt.registerOutParameter(16, OracleTypes.INTEGER);
			cstmt.execute();

			if (salesLead.getLeadsAbdStep().equals("1")) {
				rsScenario1 = (ResultSet) cstmt.getObject(8);
			} else if (salesLead.getLeadsAbdStep().equals("2")) {
				rsScenario2 = (ResultSet) cstmt.getObject(9);
			} else if (salesLead.getLeadsAbdStep().equals("3")) {
				rsScenario3 = (ResultSet) cstmt.getObject(10);
			} else {
				rsScenario1 = (ResultSet) cstmt.getObject(8);
				rsScenario2 = (ResultSet) cstmt.getObject(9);
				rsScenario3 = (ResultSet) cstmt.getObject(10);
			}

			// DSP_QQ_ID,LEADS_EMAIL_ID,PRODUCT_CODE,QQ_STATUS,QQ_STEPS,FULL_NAME
			// ,CREATE_DATE, CUSTOMER_MOBILE_NO
			// DSP_QQ_ID,LEADS_EMAIL_ID,PRODUCT_CODE,QUOTATION_STATUS,FULL_NAME
			// ,QUOTATION_CREATION_DATETIME
			// AGENT_CODE,AGENT_NAME
			if (salesLead.getLeadsAbdStep().equals("1") || salesLead.getLeadsAbdStep().equals("")) {
				while (rsScenario1.next()) {
					SalesLead salesLeadVo = new SalesLead();
					salesLeadVo.setDSP_QQ_ID(rsScenario1.getString(1));
					salesLeadVo.setLEADS_EMAIL_ID(rsScenario1.getString(2));
					salesLeadVo.setPRODUCT_CODE(rsScenario1.getString(3));
					salesLeadVo.setQQ_STATUS(rsScenario1.getString(4));
					salesLeadVo.setQQ_STEPS(rsScenario1.getString(5));
					salesLeadVo.setFULL_NAME(rsScenario1.getString(6));
					salesLeadVo.setCREATE_DATE(rsScenario1.getString(7));
					listScenario1.add(salesLeadVo);
				}
			}
			if (salesLead.getLeadsAbdStep().equals("2") || salesLead.getLeadsAbdStep().equals("")) {
				while (rsScenario2.next()) {
					SalesLead salesLeadVo = new SalesLead();
					salesLeadVo.setDSP_QQ_ID(rsScenario2.getString(1));
					salesLeadVo.setLEADS_EMAIL_ID(rsScenario2.getString(2));
					salesLeadVo.setPRODUCT_CODE(rsScenario2.getString(3));
					salesLeadVo.setQQ_STATUS(rsScenario2.getString(4));
					salesLeadVo.setQQ_STEPS(rsScenario2.getString(5));
					salesLeadVo.setFULL_NAME(rsScenario2.getString(6));
					salesLeadVo.setCREATE_DATE(rsScenario2.getString(7));
					salesLeadVo.setCUSTOMER_MOBILE_NO(rsScenario2.getString(8));
					listScenario1.add(salesLeadVo);
				}
			}
			if (salesLead.getLeadsAbdStep().equals("3") || salesLead.getLeadsAbdStep().equals("")) {
				while (rsScenario3.next()) {
					SalesLead salesLeadVo = new SalesLead();
					salesLeadVo.setDSP_QQ_ID(rsScenario3.getString(1));
					salesLeadVo.setLEADS_EMAIL_ID(rsScenario3.getString(2));
					salesLeadVo.setPRODUCT_CODE(rsScenario3.getString(3));
					salesLeadVo.setQQ_STATUS(rsScenario3.getString(4));
					salesLeadVo.setQQ_STEPS(rsScenario3.getString(5));
					salesLeadVo.setFULL_NAME(rsScenario3.getString(6));
					salesLeadVo.setCREATE_DATE(rsScenario3.getString(7));
					salesLeadVo.setCUSTOMER_MOBILE_NO(rsScenario3.getString(8));
					listScenario1.add(salesLeadVo);
				}
			}
			SalesLead salesLeadVo = new SalesLead();
			salesLeadVo.setStep1_total(cstmt.getInt(11));
			salesLeadVo.setStep2_total(cstmt.getInt(12));
			salesLeadVo.setStep3_total(cstmt.getInt(13));

			// salesLeadVo.setStep1_lost(cstmt.getInt(14));
			// salesLeadVo.setStep2_lost(cstmt.getInt(15));
			// salesLeadVo.setStep3_lost(cstmt.getInt(16));

			int totalVal = cstmt.getInt(14) + cstmt.getInt(15) + cstmt.getInt(16);

			if (salesLeadVo.getStep1_total() >= cstmt.getInt(14)) {
				int getStep1_lost = salesLeadVo.getStep1_total() - cstmt.getInt(14);
				salesLeadVo.setStep1_lost(getStep1_lost);
			}
			if (salesLeadVo.getStep2_total() >= cstmt.getInt(15)) {
				int getStep2_lost = salesLeadVo.getStep2_total() - cstmt.getInt(15);
				salesLeadVo.setStep2_lost(getStep2_lost);

			}
			if (salesLeadVo.getStep3_total() >= cstmt.getInt(16)) {
				int getStep3_lost = salesLeadVo.getStep3_total() - cstmt.getInt(16);
				salesLeadVo.setStep3_lost(getStep3_lost);
			}

			salesLeadVo.setTotalSuccess(totalVal);
			listScenario1.add(salesLeadVo);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return listScenario1;
	}

	public String dateFormat(String dateVal) {

		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		DateFormat df1 = new SimpleDateFormat("yyyy/MM/dd");
		Date startDate = null;
		try {
			startDate = df.parse(dateVal);
			String newDateString = df1.format(startDate);
			System.out.println(newDateString);
			startDate = df1.parse(newDateString);
			dateVal = df1.format(startDate);
			System.out.println("Date format : " + startDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateVal;
	}
}
