package com.etiqa.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.dsp.dao.common.pojo.Audit_User_Bean;

import oracle.jdbc.OracleTypes;

public class Audit_User_DAO {

	private static ConnectionFactory db1;
	private static Connection connection;

	public List<Audit_User_Bean> getReportData(String fromdt, String todt) {
		List<Audit_User_Bean> myDataList = new ArrayList<Audit_User_Bean>();
		ResultSet resultSet = null;
		Statement statement = null;

		try {

			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{call GET_AUDIT_COMMON_TBL_USER(?,?,?)}");
			cstmt.setString(1, fromdt);
			cstmt.setString(2, todt);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);

			cstmt.execute();

			resultSet = (ResultSet) cstmt.getObject(3);

			while (resultSet.next()) {

				Audit_User_Bean reportData = new Audit_User_Bean();

				reportData.setOid(resultSet.getString("O_ID"));
				reportData.setOpfnumber(resultSet.getString("O_PF_NUMBER"));
				reportData.setOname(resultSet.getString("O_NAME"));
				reportData.setOgroupname(resultSet.getString("O_GROUP_ID"));
				reportData.setOstatus(resultSet.getString("O_STATUS"));
				reportData.setOcreatedate(resultSet.getString("O_CREATE_DATE"));
				reportData.setOupdatedate(resultSet.getString("O_UPDATE_DATE"));
				reportData.setOcreateby(resultSet.getString("O_CREATE_BY"));
				reportData.setOupdateby(resultSet.getString("O_UPDATE_BY"));

				reportData.setId(resultSet.getString("ID"));
				reportData.setPfnumber(resultSet.getString("PF_NUMBER"));
				reportData.setName(resultSet.getString("NAME"));
				reportData.setGroupname(resultSet.getString("GROUP_ID"));
				reportData.setStatus(resultSet.getString("STATUS"));
				reportData.setCreatedate(resultSet.getString("CREATE_DATE"));
				reportData.setUpdatedate(resultSet.getString("UPDATE_DATE"));
				reportData.setCreateby(resultSet.getString("CREATE_BY"));
				reportData.setUpdateby(resultSet.getString("UPDATE_BY"));

				myDataList.add(reportData);

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return myDataList;

	}

	public static void main(String args[]) {
		Audit_User_DAO rdao = new Audit_User_DAO();
		List<Audit_User_Bean> releaseDataList = rdao.getReportData("2011-08-01 00:00:00", "2019-08-02 00:00:00");
		Iterator itr = releaseDataList.iterator();
		while (itr.hasNext()) {
			Audit_User_Bean rd = (Audit_User_Bean) itr.next();
			System.out.println(" " + rd.getId() + rd.getName());
		}

	}

}
