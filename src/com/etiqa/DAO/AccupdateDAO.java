package com.etiqa.DAO;

import java.util.List;

import com.etiqa.admin.AccupdateBean;

public interface AccupdateDAO {

	public AccupdateBean queryUpdateAccno(AccupdateBean custInfo);

	public AccupdateBean queryDSPUpdateAccno(AccupdateBean custInfo);
	// Delete accinfo

	public AccupdateBean dspdeleteAccinfo(AccupdateBean custInfo);

	public AccupdateBean cwpdeleteAccinfo(AccupdateBean custInfo);
	// Fetch Account info

	public AccupdateBean queryfetchAccInfoByPolicy(AccupdateBean custInfo);
	// Fetch Account info

	public AccupdateBean queryfetchAccInfoByID(AccupdateBean custInfo);
	// Fetch Account info

	public List<AccupdateBean> queryfetchAccInfo(AccupdateBean custInfo);
	// Fetch policynos

	// Fetch Account info

	public List<AccupdateBean> getPolicylist(AccupdateBean custInfo);

}
