package com.etiqa.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.etiqa.common.DB.ConnectionFactory;

import oracle.jdbc.OracleTypes;

public class NciDAOImpl implements NciDAO {

	@Override
	public String NCI_policy_Insert(String dspqqId, String transactionId) throws SQLException {
		Connection connection = null;

		ResultSet rs = null;

		try {
			connection = ConnectionFactory.getConnection();
			CallableStatement proc = connection.prepareCall(
					"{ call DSP_NCI_ADM_SP_POLICY_INSERT(:IN_QQ_ID,:IN_TRANSACTION_ID,:OUT_POLICY_NUMBER) }");
			proc.setString("IN_QQ_ID", dspqqId);
			proc.setString("IN_TRANSACTION_ID", transactionId);
			proc.registerOutParameter("OUT_POLICY_NUMBER", OracleTypes.VARCHAR);

			proc.execute();
			String policyNo = (String) proc.getObject("OUT_POLICY_NUMBER");
			System.out.println(policyNo + " policy");
			return policyNo;
		} finally {
			connection.close();
		}
	}

}
