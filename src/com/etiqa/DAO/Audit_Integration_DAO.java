package com.etiqa.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.etiqa.common.DB.ConnectionFactory;
import com.spring.VO.Audit_Integration_Bean;

import oracle.jdbc.OracleTypes;

public class Audit_Integration_DAO {

	private static ConnectionFactory db1;
	private static Connection connection;

	public List<Audit_Integration_Bean> getReportData(String fromdt, String todt) {

		List<Audit_Integration_Bean> myDataList = new ArrayList<Audit_Integration_Bean>();

		ResultSet resultSet = null;
		Statement statement = null;

		try {

			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = null;

			/*
			 * if(product.equals("ALL")) { cstmt =
			 * connection.prepareCall("{call GET_AUDIT_Integ_Log_Rpt(?,?,?)}"); }else {
			 * cstmt = connection.prepareCall("{call GET_AUDIT_Integ_Log_Rpt(?,?,?)}"); }
			 */

			cstmt = connection.prepareCall("{call GET_AUDIT_INTEG_LOG_RPT(?,?,?)}");
			cstmt.setString(1, fromdt);
			cstmt.setString(2, todt);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);

			cstmt.execute();

			resultSet = (ResultSet) cstmt.getObject(3);

			while (resultSet.next()) {

				Audit_Integration_Bean reportData = new Audit_Integration_Bean();

				reportData.setRecon_id(resultSet.getString("RECON_ID"));

				reportData.setPolicynumber(resultSet.getString("POLICY_NUMBER"));
				reportData.setDateint(resultSet.getString("DATE_INT"));
				reportData.setRemarkint(resultSet.getString("REMARK_INT"));
				reportData.setStatuspolm(resultSet.getString("STATUS_POLM"));

				reportData.setDateint2(resultSet.getString("DATE_INT"));
				reportData.setRemarkint2(resultSet.getString("REMARK_INT"));
				reportData.setStatuspolm2(resultSet.getString("STATUS_POLM_2"));

				reportData.setUpdatetimestamp(resultSet.getString("UPDATE_TIMESTAMP"));

				myDataList.add(reportData);

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			if (connection != null) {

				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
		}

		return myDataList;

	}

	public static void main(String args[]) {

		Audit_Integration_DAO rdao = new Audit_Integration_DAO();
		List<Audit_Integration_Bean> releaseDataList = rdao.getReportData("01/01/2016", "01/01/2019");
		Iterator itr = releaseDataList.iterator();

		while (itr.hasNext()) {
			Audit_Integration_Bean rd = (Audit_Integration_Bean) itr.next();
			System.out.println(" " + rd.getUpdatetimestamp() + rd.getPolicynumber());
		}

	}

}
