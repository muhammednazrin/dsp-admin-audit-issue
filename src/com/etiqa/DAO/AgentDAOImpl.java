package com.etiqa.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.common.DB.DBUtil;
import com.etiqa.common.DB.NamedParameterStatement;
import com.etiqa.model.agent.Agent;
import com.etiqa.model.agent.AgentProduct;
import com.etiqa.model.agent.AgentRegistrationDocument;
import com.spring.admin.TranReportController;

import oracle.jdbc.OracleTypes;

public class AgentDAOImpl implements AgentDAO {
	final static Logger logger = Logger.getLogger(AgentDAOImpl.class);
	Connection connection = ConnectionFactory.getConnection();
	private static final SimpleDateFormat dfddMMYYYY = new SimpleDateFormat("MM/dd/yyyy");
	Statement statement = null;
	ResultSet rs = null;

	// For Agent Profile View
	@Override
	public List<Agent> getAgentDetails(int ID) {

		logger.info("ID " + ID);
		try {

			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_AGENT_PROFILE_D(?,?)}");
			cstmt.setInt(1, ID);
			cstmt.registerOutParameter(2, OracleTypes.CURSOR);
			cstmt.execute();
			List<Agent> detail = new ArrayList<Agent>();
			rs = (ResultSet) cstmt.getObject(2);
			while (rs.next()) {

				Agent agentDetail = new Agent();

				detail.add(agentDetail);
			}
			return detail;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}

	}

	@Override
	public List<Agent> searchByList(String agentName, String agentCode, String agentCategory, String insuranceType,
			String status, String dateFrom, String dateTo) {
		// int startRow,
		// int endRow) {
		logger.info("agentName " + agentName);
		logger.info("status " + status);
		logger.info("Agent code " + agentCode);
		logger.info("date from " + dateFrom);
		logger.info("date To " + dateTo);
		try {
			// int i = startRow;
			connection = ConnectionFactory.getConnection();
			//
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_AGENT_SEARCH(?,?,?,?,?,?,?,?)}");
			cstmt.setString(1, agentCode);
			cstmt.setString(2, agentName);
			cstmt.setString(3, agentCategory.trim());
			cstmt.setString(4, insuranceType);
			cstmt.setString(5, status);

			cstmt.setString(6, dateFrom);
			cstmt.setString(7, dateTo);

			cstmt.registerOutParameter(8, OracleTypes.CURSOR);
			cstmt.execute();
			List<Agent> list = new ArrayList<Agent>();
			rs = (ResultSet) cstmt.getObject(8);
			while (rs.next()) {
				// int i =0;
				Agent agt = new Agent();
				// agt.setId(i);
				// agt.setAgentCode(rs.getString(1));
				agt.setAgentName(rs.getString(1));
				agt.setAgentCode(rs.getString(2));
				agt.setAgentType(rs.getString(3));
				agt.setDiscount(rs.getString(4));
				agt.setCommission(rs.getString(5));
				agt.setAgentCategory(rs.getString(6));
				agt.setInsuranceType(rs.getString(7));
				agt.setStatus(rs.getString(8));
				agt.setDateFrom(rs.getString(9));
				agt.setDateTo(rs.getString(10));

				logger.info(rs.getString(1) + "  " + rs.getString(2) + "      " + rs.getString(3) + "      "
						+ rs.getString(4) + "        " + rs.getString(5) + "    " + rs.getString(6) + "    "
						+ rs.getString(7));
				// ds i++;

				list.add(agt);

			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	@Override
	public Agent findById(Long id) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		connection = ConnectionFactory.getConnection();
		String sql = "SELECT * FROM DSP_ADM_TBL_AGENT_REGISTRATION WHERE ID=?";
		String agentProducts = "SELECT PRODUCT_CODE FROM DSP_ADM_TBL_AGENT_PRODUCT  where AGP_ID = ?";
		String agentDocuments = "SELECT DOCUMENT_CODE FROM DSP_ADM_TBL_AGENT_DOCUMENT  where D_ID = ?";
		Agent agent = null;

		try {
			pstmt = connection.prepareStatement(sql);
			pstmt.setLong(1, id);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				agent = populateBean(rs);
			}
			rs.close();
			pstmt.close();

			pstmt = connection.prepareStatement(agentProducts);
			pstmt.setString(1, id.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				AgentProduct ap = new AgentProduct();
				ap.setAgentCode(id.toString());
				ap.setProductCode(rs.getString("PRODUCT_CODE"));
				agent.getAgentProdcuts().add(ap);
			}

			/*
			 * pstmt = connection.prepareStatement(agentDocuments); pstmt.setString(1,
			 * id.toString()); rs = pstmt.executeQuery(); while (rs.next()) {
			 * AgentRegistrationDocument agd = new AgentRegistrationDocument();
			 * agd.setAgentCode(id.toString());
			 * agd.setDocumentCode(rs.getString("DOCUMENT_CODE"));
			 * agent.getAgentDocuments().add(agd); }
			 */

		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		} finally {
			DBUtil.close(connection);
		}
		return agent;
	}

	@Override
	public List<Agent> findAll() {
		Connection connection = null;
		connection = ConnectionFactory.getConnection();

		String sql = "SELECT * FROM DSP_ADM_TBL_AGENT_REGISTRATION order by REGISTRATION_DATE DESC";

		List<Agent> list = new ArrayList<Agent>();

		try {
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery(sql);
			// logger.info("test1");
			while (rs.next()) {
				Agent agent = populateBean(rs);
				list.add(agent);
			}
			// logger.info("test2");
			rs.close();
			st.close();
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		} finally {
			DBUtil.close(connection);
		}
		return list;
	}

	@Override
	public void save(Agent entity) {

		Connection connection = null;
		connection = ConnectionFactory.getConnection();
		PreparedStatement pst = null;
		// String sql = "INSERT INTO DSP_ADM_TBL_AGENT_REGISTRATION VALUES
		// (ID,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		String sql = "INSERT INTO DSP_ADM_TBL_AGENT_REGISTRATION (REFERENCE_NUMBER,AGENT_CATEGORY,AGENT_USERNAME,PASSWORD,STATUS,ANNUAL_SALES_TARGET,AGENT_NAME,ID_TYPE_NO,ID_NO,PHONE_TYPE,PHONE_NUMBER,EMAIL,ADDRESS,CITY,STATE,COUNTRY,AGENT_ALIAS_NAME,POST_CODE,AGENT_LOGO,AGENT_TYPE,REGISTRATION_DATE) VALUES";
		sql += "(";

		sql += ":REFERENCE_NUMBER,";
		sql += ":AGENT_CATEGORY,";
		sql += ":AGENT_USERNAME,";
		sql += ":PASSWORD,";
		sql += ":STATUS,";
		sql += ":ANNUAL_SALES_TARGET,";
		sql += ":AGENT_NAME,";
		sql += ":ID_TYPE_NO,";
		sql += ":ID_NO,";
		sql += ":PHONE_TYPE,";
		sql += ":PHONE_NUMBER,";
		sql += ":EMAIL,";
		sql += ":ADDRESS,";
		sql += ":CITY,";
		sql += ":STATE,";
		sql += ":COUNTRY,";
		// sql +=":AGENT_CODE,";
		sql += ":AGENT_ALIAS_NAME,";
		sql += ":POST_CODE,";
		sql += ":AGENT_LOGO,";
		// sql +=":AGENT_DISCOUNT,";
		// sql +=":AGENT_COMMISSION,";
		sql += ":AGENT_TYPE,";
		sql += ":REGISTRATION_DATE)";

		// values
		// (3,'003',to_date('29-OCT-16','DD-MON-RR'),'Indivisual','HOHH','active',400000,'003','K.Sibba
		// Reddy','003','8891898989','bbb@g.com','CCanada','004');

		// String productSql = "INSERT INTO DSP_ADM_TBL_AGENT_PRODUCT
		// (AGENT_CODE,PRODUCT_CODE) VALUES (?,?)";
		String documentSql = "INSERT INTO DSP_ADM_TBL_AGENT_DOCUMENT (AGENT_EMAIL,DOCUMENT_CODE) VALUES (?,?)";

		try {
			NamedParameterStatement p = new NamedParameterStatement(connection, sql);

			p.setString("REFERENCE_NUMBER", entity.getReferenceNo());
			p.setString("AGENT_CATEGORY", entity.getAgentCategory());

			p.setString("AGENT_USERNAME", entity.getUserId());
			p.setString("PASSWORD", entity.getPassword());
			p.setString("STATUS", entity.getAgentStatus());
			p.setDouble("ANNUAL_SALES_TARGET", entity.getAnnualSalesTarget());

			p.setString("AGENT_NAME", entity.getAgentName());
			p.setString("ID_TYPE_NO", entity.getIdType());
			p.setString("ID_NO", entity.getIdNo());
			p.setString("PHONE_TYPE", entity.getPhoneType());
			p.setString("PHONE_NUMBER", entity.getPhoneNO());
			p.setString("EMAIL", entity.getEmail());
			p.setString("ADDRESS", entity.getAddress1());
			// p.setString("ADDRESS", entity.getAddress2());
			p.setString("CITY", entity.getCity());
			p.setString("STATE", entity.getState());
			p.setString("COUNTRY", entity.getCountry());

			// p.setString("AGENT_CODE", entity.getAgentCode());
			p.setString("AGENT_ALIAS_NAME", entity.getAgentAliasName());
			p.setString("POST_CODE", entity.getPostcode());
			p.setString("AGENT_LOGO", entity.getAgentLogo());
			// p.setString("AGENT_DISCOUNT",entity.getDiscount());
			// p.setString("AGENT_COMMISSION",entity.getCommission());
			p.setString("AGENT_TYPE", entity.getAgentType());
			p.setString("REGISTRATION_DATE", entity.getRegistrationDate());

			p.executeUpdate();

			if (entity.getAgentDocuments() != null && entity.getAgentDocuments().size() > 0) {
				pst = connection.prepareStatement(documentSql);
				for (AgentRegistrationDocument agentDocument : entity.getAgentDocuments()) {
					pst.setString(1, agentDocument.getAgentEmail());
					pst.setString(2, agentDocument.getDocumentCode());
					pst.executeUpdate();
				}
			}

			/*
			 * if( entity.getAgentProdcuts() != null && entity.getAgentProdcuts().size()>0){
			 * pst = connection.prepareStatement(productSql); for( AgentProduct agentProduct
			 * : entity.getAgentProdcuts() ){ pst.setString(1, agentProduct.getAgentCode());
			 * pst.setString(2, agentProduct.getProductCode()); pst.executeUpdate(); } }
			 *
			 */

			p.close();

		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		} finally {
			DBUtil.close(connection);
			DBUtil.close(pst);
		}
	}

	@Override
	public Agent update(Agent entity) {
		int update;
		Connection connection = null;
		connection = ConnectionFactory.getConnection();

		String sql = "UPDATE DSP_ADM_TBL_AGENT_REGISTRATION SET REFERENCE_NUMBER=?,AGENT_CATEGORY=?,INSURANCE_TYPE=?,STATUS=?,ANNUAL_SALES_TARGET=?,AGENT_NAME=?,ID_TYPE_NO=?,ID_NO=?,PHONE_TYPE=?,PHONE_NUMBER=?,EMAIL=?,ADDRESS=?,CITY=?,STATE=?,COUNTRY=?,AGENT_CODE=?,AGENT_ALIAS_NAME=?,POST_CODE=?,AGENT_LOGO=?,AGENT_DISCOUNT=?,AGENT_COMMISSION=?,AGENT_TYPE=?,REGISTRATION_DATE=? WHERE ID = ?";

		String productSql = "INSERT INTO DSP_ADM_TBL_AGENT_PRODUCT (AGENT_CODE,PRODUCT_CODE) VALUES (?,?)";
		String documentSql = "INSERT INTO DSP_ADM_TBL_AGENT_DOCUMENT (AGENT_CODE,DOCUMENT_CODE) VALUES (?,?)";

		try {
			PreparedStatement pstmt = connection.prepareStatement(sql);
			pstmt.setString(1, entity.getReferenceNo());
			pstmt.setString(2, entity.getAgentCategory());
			pstmt.setString(3, entity.getInsuranceType());
			pstmt.setString(4, entity.getStatus());
			pstmt.setDouble(5, entity.getAnnualSalesTarget());
			pstmt.setString(6, entity.getAgentName());
			pstmt.setString(7, entity.getIdType());
			pstmt.setString(8, entity.getIdNo());
			pstmt.setString(9, entity.getPhoneType());
			pstmt.setString(10, entity.getPhoneNO());
			pstmt.setString(11, entity.getEmail());
			pstmt.setString(12, entity.getAddress1());
			pstmt.setString(13, entity.getCity());
			pstmt.setString(14, entity.getState());
			pstmt.setString(15, entity.getCountry());
			pstmt.setString(16, entity.getAgentCode());
			pstmt.setString(17, entity.getAgentAliasName());
			pstmt.setString(18, entity.getPostcode());
			pstmt.setString(19, entity.getAgentLogo());
			pstmt.setString(20, entity.getDiscount());
			pstmt.setString(21, entity.getCommission());
			pstmt.setString(22, entity.getAgentType());
			// pstmt.setString(23, entity.getRegistrationDate());
			try {
				pstmt.setDate(23, new java.sql.Date(dfddMMYYYY.parse(entity.getRegistrationDate()).getTime()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			pstmt.setLong(24, entity.getId());
			update = pstmt.executeUpdate();

			if (entity.getAgentDocuments() != null && entity.getAgentDocuments().size() > 0) {
				pstmt = connection.prepareStatement(documentSql);
				for (AgentRegistrationDocument agentDocument : entity.getAgentDocuments()) {
					pstmt.setString(1, agentDocument.getAgentCode());
					pstmt.setString(2, agentDocument.getDocumentCode());
					pstmt.executeUpdate();
				}
			}

			if (entity.getAgentProdcuts() != null && entity.getAgentProdcuts().size() > 0) {
				pstmt = connection.prepareStatement(productSql);
				for (AgentProduct agentProduct : entity.getAgentProdcuts()) {
					pstmt.setString(1, agentProduct.getAgentCode());
					pstmt.setString(2, agentProduct.getProductCode());
					pstmt.executeUpdate();
				}
			}

			pstmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		} finally {
			DBUtil.close(connection);
		}

		return entity;
	}

	@Override
	public void delete(Agent entity) {
		Connection connection = null;
		connection = ConnectionFactory.getConnection();
		String sql = "DELETE FROM DSP_ADM_TBL_AGENT_REGISTRATION WHERE ID = ?";

		try {
			PreparedStatement pstmt = connection.prepareStatement(sql);
			pstmt.setLong(1, entity.getId());
			pstmt.executeUpdate();
			pstmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		} finally {
			DBUtil.close(connection);
		}
	}

	private Agent populateBean(ResultSet rs) throws SQLException {
		Agent agent = new Agent();
		agent.setId(rs.getInt("ID"));
		agent.setAnnualSalesTarget(rs.getDouble("ANNUAL_SALES_TARGET"));
		agent.setReferenceNo(rs.getString("REFERENCE_NUMBER"));
		agent.setAgentCategory(rs.getString("AGENT_CATEGORY"));
		agent.setAgentCode(rs.getString("AGENT_CODE"));
		agent.setAgentName(rs.getString("AGENT_NAME"));

		agent.setInsuranceType(rs.getString("INSURANCE_TYPE"));
		agent.setIdType(rs.getString("ID_TYPE_NO"));
		agent.setIdNo(rs.getString("ID_NO"));
		agent.setPhoneType(rs.getString("PHONE_TYPE"));
		agent.setPhoneNO(rs.getString("PHONE_NUMBER"));
		agent.setEmail(rs.getString("EMAIL"));
		agent.setAddress1(rs.getString("ADDRESS"));
		agent.setCity(rs.getString("CITY"));
		agent.setPostcode(rs.getString("POST_CODE"));
		agent.setState(rs.getString("STATE"));
		agent.setCountry(rs.getString("COUNTRY"));
		agent.setAgentAliasName(rs.getString("AGENT_ALIAS_NAME"));
		agent.setAgentStatus(rs.getString("STATUS"));
		agent.setDiscount(rs.getString("AGENT_DISCOUNT"));
		agent.setCommission(rs.getString("AGENT_COMMISSION"));
		agent.setAgentType(rs.getString("AGENT_TYPE"));

		if (rs.getDate("REGISTRATION_DATE") != null) {
			try {
				agent.setRegistrationDate(dfddMMYYYY.format(new Date(rs.getDate("REGISTRATION_DATE").getTime())));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return agent;
	}

	public static void main(String args[]) {
		// AgentDAOImpl impl = new AgentDAOImpl();
		// List<Agent> a = null;
		//
		// a =impl.searchByList("Naha","","","","","","");
		//
		// for(int b=0;b<a.size();b++)
		// {
		// logger.info(a.get(b).getAgentName());

		// long a= findById(1);

	}
	//
	//
	// }

}
