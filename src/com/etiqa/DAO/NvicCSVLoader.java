package com.etiqa.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.etiqa.common.DB.ConnectionFactory;
import com.spring.VO.MITblNvicUpload;

public class NvicCSVLoader {

	private static final String SQL_INSERT = "INSERT INTO ${table}(${keys}) VALUES(${values})";
	private static final String TABLE_REGEX = "\\$\\{table\\}";
	private static final String KEYS_REGEX = "\\$\\{keys\\}";
	private static final String VALUES_REGEX = "\\$\\{values\\}";

	private Connection connection;

	public void loadCSV(String tableName, boolean truncateBeforeLoad, List<MITblNvicUpload> csvList) throws Exception {

		connection = ConnectionFactory.getConnection();
		if (null == connection) {
			throw new Exception("Not a valid connection.");
		}

		String stkeys = "NVIC_CODE, NVIC_GROUP,MM_CODE, MAKE_CODE, MODEL_CODE, MAKE, MODEL, VARIANT,SERIES, YEAR, DESCRIPTION,";
		stkeys = stkeys
				+ "ENGINE, STYLE, TRANSMISSION,DRIVEN_WHEEL, SEAT, CC, FUEL_TYPE, CLASS_CODE, WD4_FLG, HP_FLG, MARK_UP_PER, MARK_DOWN_PER,TOTAL_SUM, MARKET_VALUE1,";
		stkeys = stkeys
				+ "MARKET_VALUE2,MARKET_VALUE3, WINDSCREEN_VALUE1, WINDSCREEN_VALUE2,WINDSCREEN_VALUE3, EFFECTIVE_DATE, PURVAL01,PURVAL02, PURVAL03";

		String questionmarks = StringUtils.repeat("?,", 34);
		questionmarks = (String) questionmarks.subSequence(0, questionmarks.length() - 1);

		String query = SQL_INSERT.replaceFirst(TABLE_REGEX, tableName);
		query = query.replaceFirst(KEYS_REGEX, stkeys);
		query = query.replaceFirst(VALUES_REGEX, questionmarks);

		System.out.println("Query: " + query);

		String[] nextLine;
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = this.connection;
			con.setAutoCommit(false);
			ps = con.prepareStatement(query);

			if (truncateBeforeLoad) {
				// delete data from table before loading csv
				con.createStatement().execute("truncate table " + tableName);
			}

			final int batchSize = 1000;
			int count = 0;
			Date date = null;
			while (count < csvList.size()) {
				MITblNvicUpload miTblNvicUpload = csvList.get(count);

				/*
				 * int index = 1; System.out.println("nextLine "+index +"- "+nextLine); for
				 * (String string : nextLine) { System.out.println("index "+index +"- "+string);
				 *
				 * ps.setString(index++, string);
				 *
				 *
				 * }
				 */
				ps.setString(1, miTblNvicUpload.getNvicCode());
				ps.setString(2, miTblNvicUpload.getNvicGroup());
				ps.setString(3, miTblNvicUpload.getMmCode());
				ps.setString(4, miTblNvicUpload.getMakeCode());
				ps.setString(5, miTblNvicUpload.getModelCode());
				ps.setString(6, miTblNvicUpload.getMake());
				ps.setString(7, miTblNvicUpload.getModel());
				ps.setString(8, miTblNvicUpload.getVariant());
				ps.setString(9, miTblNvicUpload.getSeries());
				ps.setString(10, miTblNvicUpload.getYear());
				ps.setString(11, miTblNvicUpload.getDescription());
				ps.setString(12, miTblNvicUpload.getEngine());
				ps.setString(13, miTblNvicUpload.getStyle());
				ps.setString(14, miTblNvicUpload.getTransmission());
				ps.setString(15, miTblNvicUpload.getDrivenWheel());
				ps.setString(16, miTblNvicUpload.getSeat());
				ps.setString(17, miTblNvicUpload.getCc());
				ps.setString(18, miTblNvicUpload.getFuelType());
				ps.setString(19, miTblNvicUpload.getClassCode());
				ps.setString(20, miTblNvicUpload.getWd4Flg());
				ps.setString(21, miTblNvicUpload.getHpFlg());
				ps.setBigDecimal(22, miTblNvicUpload.getMarkUpPer());
				ps.setBigDecimal(23, miTblNvicUpload.getMarkDownPer());
				ps.setBigDecimal(24, miTblNvicUpload.getTotalSum());
				ps.setBigDecimal(25, miTblNvicUpload.getMarketValue1());
				ps.setBigDecimal(26, miTblNvicUpload.getMarketValue2());
				ps.setBigDecimal(27, miTblNvicUpload.getMarketValue3());
				ps.setBigDecimal(28, miTblNvicUpload.getWindscreenValue1());
				ps.setBigDecimal(29, miTblNvicUpload.getWindscreenValue2());
				ps.setBigDecimal(30, miTblNvicUpload.getWindscreenValue3());
				ps.setString(31, miTblNvicUpload.getEffectiveDate());
				ps.setString(32, miTblNvicUpload.getPurval01());
				ps.setString(33, miTblNvicUpload.getPurval02());
				ps.setString(34, miTblNvicUpload.getPurval03());
				ps.addBatch();

				if (++count % batchSize == 0) {
					ps.executeBatch();
				}
			}
			ps.executeBatch(); // insert remaining records
			con.commit();
		} catch (Exception e) {
			con.rollback();
			e.printStackTrace();
			throw new Exception("Error occured while loading data from file to database." + e.getMessage());
		} finally {
			if (null != ps) {
				ps.close();
			}
			if (null != con) {
				con.close();
			}

		}
	}

}
