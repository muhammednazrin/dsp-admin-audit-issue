package com.etiqa.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.dsp.dao.common.pojo.Audit_Customer_Bean;

import oracle.jdbc.OracleTypes;

public class Audit_Customer_DAO {
	private static ConnectionFactory db1;
	private static Connection connection;

	public List<Audit_Customer_Bean> getReportData(String fromdt, String todt) {
		List<Audit_Customer_Bean> myDataList = new ArrayList<Audit_Customer_Bean>();
		ResultSet resultSet = null;
		Statement statement = null;

		try {

			connection = ConnectionFactory.getConnection();

			// GET_AUDIT_COMMON_TBL_CUSTOMER // ID,Name
			// GET_AUDIT_COMMON_TBL_PAYMENT // TRANSACTION_ID, AMOUNT
			// GET_AUDIT_OPT_REGISTRATION // ID,Name

			CallableStatement cstmt = connection.prepareCall("{call GET_AUDIT_COMMON_TBL_CUSTOMER(?,?,?)}");
			cstmt.setString(1, fromdt);
			cstmt.setString(2, todt);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);

			cstmt.execute();

			resultSet = (ResultSet) cstmt.getObject(3);

			while (resultSet.next()) {

				Audit_Customer_Bean reportData = new Audit_Customer_Bean();

				/*
				 * if (resultSet.getString("CUSTOMER_NRIC_ID").equals(resultSet.getString(
				 * "O_CUSTOMER_NRIC_ID"))) { reportData.setNricno("");
				 * reportData.setO_nricno(resultSet.getString("O_CUSTOMER_NRIC_ID")); } else {
				 * reportData.setNricno(resultSet.getString("CUSTOMER_NRIC_ID"));
				 * reportData.setO_nricno(resultSet.getString("O_CUSTOMER_NRIC_ID"));
				 *
				 * }
				 *
				 * if (resultSet.getString("CUSTOMER_NAME").equals(resultSet.getString(
				 * "O_CUSTOMER_NAME"))) { reportData.setName("");
				 * reportData.setO_name(resultSet.getString("O_CUSTOMER_NAME")); } else {
				 * reportData.setName(resultSet.getString("CUSTOMER_NAME"));
				 * reportData.setO_name(resultSet.getString("O_CUSTOMER_NAME"));
				 *
				 * }
				 *
				 * if (resultSet.getString("CUSTOMER_EMAIL").equals(resultSet.getString(
				 * "O_CUSTOMER_EMAIL"))) { reportData.setEmail("");
				 * reportData.setO_nricno(resultSet.getString("O_CUSTOMER_EMAIL")); } else {
				 * reportData.setEmail(resultSet.getString("CUSTOMER_EMAIL"));
				 * reportData.setO_nricno(resultSet.getString("O_CUSTOMER_EMAIL")); }
				 *
				 * if (resultSet.getString("CUSTOMER_ADDRESS1").equals(resultSet.getString(
				 * "O_CUSTOMER_ADDRESS1"))) { reportData.setAddress("");
				 * reportData.setO_address(resultSet.getString("O_CUSTOMER_ADDRESS1")); } else {
				 * reportData.setAddress(resultSet.getString("CUSTOMER_ADDRESS1"));
				 * reportData.setO_address(resultSet.getString("O_CUSTOMER_ADDRESS1")); }
				 *
				 * if ( null != resultSet.getString("O_CUSTOMER_ADDRESS2") ) {
				 * reportData.setAddress(resultSet.getString("CUSTOMER_ADDRESS2"));
				 * reportData.setO_address(resultSet.getString("O_CUSTOMER_ADDRESS2")); } else {
				 * reportData.setAddress(resultSet.getString("CUSTOMER_ADDRESS2"));
				 * reportData.setO_address(resultSet.getString("O_CUSTOMER_ADDRESS2")); }
				 *
				 * if ( null != resultSet.getString("O_CUSTOMER_ADDRESS3") ) {
				 * reportData.setAddress(resultSet.getString("CUSTOMER_ADDRESS3"));
				 * reportData.setO_address(resultSet.getString("O_CUSTOMER_ADDRESS3")); } else {
				 * reportData.setAddress(resultSet.getString("CUSTOMER_ADDRESS3"));
				 * reportData.setO_address(resultSet.getString("O_CUSTOMER_ADDRESS3")); }
				 *
				 * if (resultSet.getString("CUSTOMER_POSTCODE").equals(resultSet.getString(
				 * "O_CUSTOMER_POSTCODE"))) { reportData.setPostcode("");
				 * reportData.setO_postcode(resultSet.getString("O_CUSTOMER_POSTCODE")); } else
				 * { reportData.setPostcode(resultSet.getString("CUSTOMER_POSTCODE"));
				 * reportData.setO_postcode(resultSet.getString("O_CUSTOMER_POSTCODE")); }
				 *
				 * if (resultSet.getString("CUSTOMER_STATE").equals(resultSet.getString(
				 * "O_CUSTOMER_STATE"))) {
				 * reportData.setState(resultSet.getString("CUSTOMER_STATE"));
				 * reportData.setO_state(resultSet.getString("O_CUSTOMER_STATE")); } else {
				 * reportData.setState(resultSet.getString("CUSTOMER_STATE"));
				 * reportData.setO_state(resultSet.getString("O_CUSTOMER_STATE")); }
				 *
				 * if (resultSet.getString("CUSTOMER_MOBILE_NO").equals(resultSet.getString(
				 * "O_CUSTOMER_MOBILE_NO"))) {
				 * reportData.setMobileno(resultSet.getString("CUSTOMER_MOBILE_NO"));
				 * reportData.setO_mobileno(resultSet.getString("O_CUSTOMER_MOBILE_NO")); } else
				 * { reportData.setMobileno(resultSet.getString("CUSTOMER_MOBILE_NO"));
				 * reportData.setO_mobileno(resultSet.getString("O_CUSTOMER_MOBILE_NO")); }
				 */

				/*
				 * if ( null != resultSet.getString("O_CUSTOMER_MOBILE_NO") ) {
				 * reportData.setMobileno(resultSet.getString("CUSTOMER_MOBILE_NO"));
				 * reportData.setO_mobileno(resultSet.getString("O_CUSTOMER_MOBILE_NO")); } else
				 * { reportData.setMobileno(resultSet.getString("CUSTOMER_MOBILE_NO"));
				 * reportData.setO_mobileno(resultSet.getString("O_CUSTOMER_MOBILE_NO")); }
				 *
				 * if ( null != resultSet.getString("O_CUSTOMER_NRIC_ID") ) {
				 * reportData.setNricno(resultSet.getString("CUSTOMER_NRIC_ID"));
				 * reportData.setO_nricno(resultSet.getString("O_CUSTOMER_NRIC_ID")); } else {
				 * reportData.setNricno(resultSet.getString("CUSTOMER_NRIC_ID"));
				 * reportData.setO_nricno(resultSet.getString("O_CUSTOMER_NRIC_ID")); }
				 *
				 * if ( null != resultSet.getString("O_CUSTOMER_NAME") ) {
				 * reportData.setName(resultSet.getString("CUSTOMER_NAME"));
				 * reportData.setO_name(resultSet.getString("O_CUSTOMER_NAME")); } else {
				 * reportData.setName(resultSet.getString("CUSTOMER_NAME"));
				 * reportData.setO_name(resultSet.getString("O_CUSTOMER_NAME")); }
				 *
				 * if ( null != resultSet.getString("O_CUSTOMER_EMAIL") ) {
				 * reportData.setEmail(resultSet.getString("CUSTOMER_EMAIL"));
				 * reportData.setO_email(resultSet.getString("O_CUSTOMER_EMAIL")); } else {
				 * reportData.setEmail(resultSet.getString("CUSTOMER_EMAIL"));
				 * reportData.setO_email(resultSet.getString("O_CUSTOMER_EMAIL")); }
				 *
				 * if ( null != resultSet.getString("O_CUSTOMER_ADDRESS1") ) {
				 * reportData.setAddress(resultSet.getString("CUSTOMER_ADDRESS1"));
				 * reportData.setO_address(resultSet.getString("O_CUSTOMER_ADDRESS1")); } else {
				 * reportData.setAddress(resultSet.getString("CUSTOMER_ADDRESS1"));
				 * reportData.setO_address(resultSet.getString("O_CUSTOMER_ADDRESS1")); }
				 *
				 * if ( null != resultSet.getString("O_CUSTOMER_POSTCODE") ) {
				 * reportData.setPostcode(resultSet.getString("CUSTOMER_POSTCODE"));
				 * reportData.setO_postcode(resultSet.getString("O_CUSTOMER_POSTCODE")); } else
				 * { reportData.setPostcode(resultSet.getString("CUSTOMER_POSTCODE"));
				 * reportData.setO_postcode(resultSet.getString("O_CUSTOMER_POSTCODE")); }
				 *
				 * if ( null != resultSet.getString("O_CUSTOMER_STATE") ) {
				 * reportData.setState(resultSet.getString("CUSTOMER_STATE"));
				 * reportData.setO_state(resultSet.getString("O_CUSTOMER_STATE")); } else {
				 * reportData.setState(resultSet.getString("CUSTOMER_STATE"));
				 * reportData.setO_state(resultSet.getString("O_CUSTOMER_STATE")); }
				 */

				reportData.setMobileno(resultSet.getString("CUSTOMER_MOBILE_NO"));
				reportData.setO_mobileno(resultSet.getString("O_CUSTOMER_MOBILE_NO"));

				reportData.setNricno(resultSet.getString("CUSTOMER_NRIC_ID"));
				reportData.setO_nricno(resultSet.getString("O_CUSTOMER_NRIC_ID"));

				reportData.setName(resultSet.getString("CUSTOMER_NAME"));
				reportData.setO_name(resultSet.getString("O_CUSTOMER_NAME"));

				reportData.setEmail(resultSet.getString("CUSTOMER_EMAIL"));
				reportData.setO_email(resultSet.getString("O_CUSTOMER_EMAIL"));

				// Customer Address
				String o_address = "";

				if (null != resultSet.getString("O_CUSTOMER_ADDRESS1")) {
					o_address = resultSet.getString("O_CUSTOMER_ADDRESS1") + ",";
				}
				if (null != resultSet.getString("O_CUSTOMER_ADDRESS2")) {
					o_address = resultSet.getString("O_CUSTOMER_ADDRESS2") + ",";
				}
				if (null != resultSet.getString("O_CUSTOMER_ADDRESS3")) {
					o_address = resultSet.getString("O_CUSTOMER_ADDRESS3");
				}

				String address = "";
				if (null != resultSet.getString("CUSTOMER_ADDRESS1")) {
					address = resultSet.getString("CUSTOMER_ADDRESS1") + ",";
				}
				if (null != resultSet.getString("CUSTOMER_ADDRESS2")) {
					address = resultSet.getString("CUSTOMER_ADDRESS2") + ",";
				}
				if (null != resultSet.getString("CUSTOMER_ADDRESS3")) {
					address = resultSet.getString("CUSTOMER_ADDRESS3");
				}

				reportData.setAddress(address);
				reportData.setO_address(o_address);

				reportData.setPostcode(resultSet.getString("CUSTOMER_POSTCODE"));
				reportData.setO_postcode(resultSet.getString("O_CUSTOMER_POSTCODE"));

				reportData.setState(resultSet.getString("CUSTOMER_STATE"));
				reportData.setO_state(resultSet.getString("O_CUSTOMER_STATE"));

				reportData.setWho(resultSet.getString("WHO"));
				reportData.setDatewhen(resultSet.getString("DATE_WHEN"));
				reportData.setRecordcomment(resultSet.getString("RECORD_COMMENT"));

				myDataList.add(reportData);

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return myDataList;

	}

	public static void main(String args[]) {
		Audit_Customer_DAO rdao = new Audit_Customer_DAO();
		List<Audit_Customer_Bean> releaseDataList = rdao.getReportData("2011-08-01 00:00:00", "2019-08-02 00:00:00");
		Iterator itr = releaseDataList.iterator();
		while (itr.hasNext()) {
			Audit_Customer_Bean rd = (Audit_Customer_Bean) itr.next();
			System.out.println(" " + rd.getAddress() + rd.getName());
		}

	}
}
