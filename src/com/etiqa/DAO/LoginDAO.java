package com.etiqa.DAO;

import java.sql.SQLException;

import org.olap4j.metadata.Member;

import com.etiqa.model.others.Login;

public interface LoginDAO {
	Login getLogin(String userName, String password) throws SQLException;



}
