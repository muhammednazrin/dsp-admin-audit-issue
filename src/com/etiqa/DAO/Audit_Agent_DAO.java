package com.etiqa.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.dsp.dao.common.pojo.Audit_Agent_Bean;

import oracle.jdbc.OracleTypes;

public class Audit_Agent_DAO {

	private static ConnectionFactory db1;
	private static Connection connection;

	public List<Audit_Agent_Bean> getReportData(String fromdt, String todt) {
		List<Audit_Agent_Bean> myDataList = new ArrayList<Audit_Agent_Bean>();
		ResultSet resultSet = null;
		Statement statement = null;

		try {

			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{call GET_AUDIT_AGENT_REGISTRATION(?,?,?)}");
			cstmt.setString(1, fromdt);
			cstmt.setString(2, todt);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);

			cstmt.execute();

			resultSet = (ResultSet) cstmt.getObject(3);

			while (resultSet.next()) {

				Audit_Agent_Bean reportData = new Audit_Agent_Bean();

				/*
				 * try { if (resultSet.getString("REFERENCE_NUMBER").equals(resultSet.getString(
				 * "O_REFERENCE_NUMBER"))) { reportData.setReferenceno("");
				 * reportData.setOreferenceno(resultSet.getString("O_REFERENCE_NUMBER")); } else
				 * { reportData.setReferenceno(resultSet.getString("REFERENCE_NUMBER"));
				 * reportData.setOreferenceno(resultSet.getString("O_REFERENCE_NUMBER")); } }
				 * catch (Exception ex) { reportData.setReferenceno("");
				 * reportData.setOreferenceno(""); }
				 *
				 * try { if
				 * (resultSet.getString("AGENT_NAME").equals(resultSet.getString("O_AGENT_NAME")
				 * )) { reportData.setName("");
				 * reportData.setOname(resultSet.getString("O_AGENT_NAME")); } else {
				 * reportData.setName(resultSet.getString("AGENT_NAME"));
				 * reportData.setOname(resultSet.getString("O_AGENT_NAME")); } } catch
				 * (Exception ex) { reportData.setName(""); reportData.setOname(""); }
				 *
				 * try { if
				 * (resultSet.getString("ID_NO").equals(resultSet.getString("O_ID_NO"))) {
				 * reportData.setIdno(""); reportData.setOidno(resultSet.getString("O_ID_NO"));
				 * } else { reportData.setIdno(resultSet.getString("ID_NO"));
				 * reportData.setOidno(resultSet.getString("O_ID_NO")); } } catch (Exception ex)
				 * { reportData.setIdno(""); reportData.setOidno(""); }
				 *
				 * try { if (resultSet.getString("PHONE_NUMBER").equals(resultSet.getString(
				 * "O_PHONE_NUMBER"))) { reportData.setPhoneno("");
				 * reportData.setOphoneno(resultSet.getString("O_PHONE_NUMBER")); } else {
				 * reportData.setPhoneno(resultSet.getString("PHONE_NUMBER"));
				 * reportData.setOphoneno(resultSet.getString("O_PHONE_NUMBER")); } } catch
				 * (Exception ex) { reportData.setPhoneno(""); reportData.setOphoneno(""); }
				 *
				 * try { if
				 * (resultSet.getString("O_EMAIL").equals(resultSet.getString("O_EMAIL"))) {
				 * reportData.setEmail("");
				 * reportData.setOemail(resultSet.getString("O_EMAIL")); } else {
				 * reportData.setEmail(resultSet.getString("EMAIL"));
				 * reportData.setOemail(resultSet.getString("O_EMAIL")); } } catch (Exception
				 * ex) { reportData.setEmail(""); reportData.setOemail(""); }
				 *
				 * try { if
				 * (resultSet.getString("ADDRESS1").equals(resultSet.getString("O_ADDRESS1"))) {
				 * reportData.setAddress1("");
				 * reportData.setOaddress1(resultSet.getString("O_ADDRESS1")); } else {
				 * reportData.setAddress1(resultSet.getString("ADDRESS1"));
				 * reportData.setOaddress1(resultSet.getString("O_ADDRESS1")); } } catch
				 * (Exception ex) { reportData.setAddress1(""); reportData.setOaddress1(""); }
				 *
				 * try { if
				 * (resultSet.getString("AGENT_CODE").equals(resultSet.getString("O_AGENT_CODE")
				 * )) { reportData.setAgentcode(resultSet.getString("AGENT_CODE"));
				 * reportData.setOagentcode(resultSet.getString("O_AGENT_CODE")); } else {
				 * reportData.setAgentcode(resultSet.getString("AGENT_CODE"));
				 * reportData.setOagentcode(resultSet.getString("O_AGENT_CODE")); } } catch
				 * (Exception ex) { reportData.setAgentcode(resultSet.getString("AGENT_CODE"));
				 * reportData.setOagentcode(resultSet.getString("O_AGENT_CODE")); }
				 *
				 * try { if (resultSet.getString("AGENT_PRODUCT_SELECTION")
				 * .equals(resultSet.getString("O_AGENT_PRODUCT_SELECTION"))) {
				 * reportData.setAgentproductselection("");
				 * reportData.setOagentproductselection(resultSet.getString(
				 * "O_AGENT_PRODUCT_SELECTION")); } else {
				 * reportData.setAgentproductselection(resultSet.getString(
				 * "AGENT_PRODUCT_SELECTION"));
				 * reportData.setOagentproductselection(resultSet.getString(
				 * "O_AGENT_PRODUCT_SELECTION")); } } catch (Exception ex) {
				 * reportData.setAgentproductselection("");
				 * reportData.setOagentproductselection(""); }
				 *
				 * try { if (resultSet.getString("AGENT_DISCOUNT").equals(resultSet.getString(
				 * "O_AGENT_DISCOUNT"))) {
				 * reportData.setDiscount(resultSet.getString("AGENT_DISCOUNT"));
				 * reportData.setOdiscount(resultSet.getString("O_AGENT_DISCOUNT")); } else {
				 * reportData.setDiscount(resultSet.getString("AGENT_DISCOUNT"));
				 * reportData.setOdiscount(resultSet.getString("O_AGENT_DISCOUNT")); } } catch
				 * (Exception ex) { reportData.setDiscount(""); reportData.setOdiscount(""); }
				 *
				 * try { if (resultSet.getString("AGENT_COMMISSION").replace(" ", "")
				 * .equals(resultSet.getString("O_AGENT_COMMISSION").replace(" ", ""))) {
				 * reportData.setCommision(resultSet.getString("AGENT_COMMISSION"));
				 * reportData.setOcommision(resultSet.getString("O_AGENT_COMMISSION")); } else {
				 * reportData.setCommision(resultSet.getString("AGENT_COMMISSION"));
				 * reportData.setOcommision(resultSet.getString("O_AGENT_COMMISSION")); } }
				 * catch (Exception ex) {
				 * reportData.setCommision(resultSet.getString("AGENT_COMMISSION"));
				 * reportData.setOcommision(resultSet.getString("O_AGENT_COMMISSION")); }
				 *
				 * try { if (resultSet.getString("AGENT_COMMISSION").equals(resultSet.getString(
				 * "O_AGENT_COMMISSION"))) { reportData.setType("");
				 * reportData.setOtype(resultSet.getString("O_AGENT_TYPE")); } else {
				 * reportData.setType(resultSet.getString("AGENT_TYPE"));
				 * reportData.setOtype(resultSet.getString("O_AGENT_TYPE")); } } catch
				 * (Exception ex) { reportData.setType(""); reportData.setOtype(""); }
				 */

				// Chandra
				/*
				 * if ( null != resultSet.getString("O_REFERENCE_NUMBER") ) {
				 * reportData.setReferenceno(resultSet.getString("REFERENCE_NUMBER"));
				 * reportData.setOreferenceno(resultSet.getString("O_REFERENCE_NUMBER")); } else
				 * { reportData.setReferenceno(resultSet.getString("REFERENCE_NUMBER"));
				 * reportData.setOreferenceno(resultSet.getString("O_REFERENCE_NUMBER")); }
				 *
				 * if ( null != resultSet.getString("O_AGENT_PRODUCT_SELECTION") ) {
				 * reportData.setReferenceno(resultSet.getString("AGENT_PRODUCT_SELECTION"));
				 * reportData.setOreferenceno(resultSet.getString("O_AGENT_PRODUCT_SELECTION"));
				 * } else {
				 * reportData.setReferenceno(resultSet.getString("AGENT_PRODUCT_SELECTION"));
				 * reportData.setOreferenceno(resultSet.getString("O_AGENT_PRODUCT_SELECTION"));
				 * }
				 *
				 * if ( null != resultSet.getString("O_AGENT_DISCOUNT") ) {
				 * reportData.setReferenceno(resultSet.getString("AGENT_DISCOUNT"));
				 * reportData.setOreferenceno(resultSet.getString("O_AGENT_DISCOUNT")); } else {
				 * reportData.setReferenceno(resultSet.getString("AGENT_DISCOUNT"));
				 * reportData.setOreferenceno(resultSet.getString("O_AGENT_DISCOUNT")); }
				 *
				 * if ( null != resultSet.getString("O_AGENT_COMMISSION") ) {
				 * reportData.setReferenceno(resultSet.getString("AGENT_COMMISSION"));
				 * reportData.setOreferenceno(resultSet.getString("O_AGENT_COMMISSION")); } else
				 * { reportData.setReferenceno(resultSet.getString("AGENT_COMMISSION"));
				 * reportData.setOreferenceno(resultSet.getString("O_AGENT_COMMISSION")); }
				 *
				 * if ( null != resultSet.getString("O_AGENT_TYPE") ) {
				 * reportData.setReferenceno(resultSet.getString("AGENT_TYPE"));
				 * reportData.setOreferenceno(resultSet.getString("O_AGENT_TYPE")); } else {
				 * reportData.setReferenceno(resultSet.getString("AGENT_TYPE"));
				 * reportData.setOreferenceno(resultSet.getString("O_AGENT_TYPE")); }
				 */

				reportData.setReferenceno(resultSet.getString("REFERENCE_NUMBER"));
				reportData.setOreferenceno(resultSet.getString("O_REFERENCE_NUMBER"));

				/*
				 * reportData.setAgentproductselection(resultSet.getString(
				 * "AGENT_PRODUCT_SELECTION"));
				 * reportData.setOagentproductselection(resultSet.getString(
				 * "O_AGENT_PRODUCT_SELECTION"));
				 */

				reportData.setAgentCategory(resultSet.getString("AGENT_CATEGORY"));
				reportData.setoAgentCategory(resultSet.getString("O_AGENT_CATEGORY"));

				/*
				 * reportData.setDiscount(resultSet.getString("AGENT_DISCOUNT"));
				 * reportData.setOdiscount(resultSet.getString("O_AGENT_DISCOUNT"));
				 */

				reportData.setStatus(resultSet.getString("STATUS"));
				reportData.setoStatus(resultSet.getString("O_STATUS"));

				/*
				 * reportData.setCommision(resultSet.getString("AGENT_COMMISSION"));
				 * reportData.setOcommision(resultSet.getString("O_AGENT_COMMISSION"));
				 */

				reportData.setEmail(resultSet.getString("EMAIL"));
				reportData.setOemail(resultSet.getString("O_EMAIL"));

				reportData.setType(resultSet.getString("AGENT_TYPE"));
				reportData.setOtype(resultSet.getString("O_AGENT_TYPE"));

				reportData.setWho(resultSet.getString("WHO"));
				reportData.setDatewhen(resultSet.getString("DATE_WHEN"));
				reportData.setRecordcomment(resultSet.getString("RECORD_COMMENT"));

				myDataList.add(reportData);

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return myDataList;

	}

	public static void main(String args[]) {
		Audit_Agent_DAO rdao = new Audit_Agent_DAO();
		List<Audit_Agent_Bean> releaseDataList = rdao.getReportData("16/05/2017", "01/08/2019");
		Iterator itr = releaseDataList.iterator();
		while (itr.hasNext()) {
			Audit_Agent_Bean rd = (Audit_Agent_Bean) itr.next();
			System.out.println(" " + rd.getAgentcode() + rd.getName());
		}

	}
}
