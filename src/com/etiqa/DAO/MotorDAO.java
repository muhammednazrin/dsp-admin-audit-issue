package com.etiqa.DAO;

import java.util.List;

import com.spring.VO.CommonNVIC;

public interface MotorDAO {

	public List<CommonNVIC> getMakeList();

	public List<CommonNVIC> getModelList(String makecode);

	public List<CommonNVIC> getYearList();
}
