package com.etiqa.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.dsp.dao.common.pojo.Audit_Payment_Bean;

import oracle.jdbc.OracleTypes;

public class Audit_Payment_DAO {

	private static ConnectionFactory db1;
	private static Connection connection;

	public List<Audit_Payment_Bean> getReportData(String fromdt, String todt, String product) {
		List<Audit_Payment_Bean> myDataList = new ArrayList<Audit_Payment_Bean>();
		ResultSet resultSet = null;
		Statement statement = null;

		try {

			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = null;

			if (product.equals("ALL")) {
				cstmt = connection.prepareCall("{call GET_AUDIT_COMMON_TBL_PAYMENT(?,?,?,?)}");

			} else {
				cstmt = connection.prepareCall("{call GET_AUDIT_COMMON_TBL_PAY_FIL(?,?,?,?)}");
			}

			cstmt.setString(1, fromdt);
			cstmt.setString(2, todt);
			cstmt.setString(3, product);
			cstmt.registerOutParameter(4, OracleTypes.CURSOR);

			cstmt.execute();

			resultSet = (ResultSet) cstmt.getObject(4);

			while (resultSet.next()) {

				if (resultSet.getString("PMNT_STATUS").equals(resultSet.getString("O_PMNT_STATUS"))) {

				} else {

					Audit_Payment_Bean reportData = new Audit_Payment_Bean();

					try {

						String inv = resultSet.getString("INVOICE_NO").toString();
						String O_inv = resultSet.getString("INVOICE_NO").toString();
						if (inv.equals(O_inv)) {
							reportData.setInvoiceno("");
							reportData.setO_invoiceno(resultSet.getString("O_INVOICE_NO"));
						} else {

							reportData.setInvoiceno(resultSet.getString("INVOICE_NO"));
							reportData.setO_invoiceno(resultSet.getString("O_INVOICE_NO"));
						}
					} catch (Exception ex) {
						reportData.setInvoiceno("");
						reportData.setO_invoiceno("");

					}

					if (resultSet.getString("PMNT_GATEWAY_CODE").equals(resultSet.getString("O_PMNT_GATEWAY_CODE"))) {
						reportData.setPymt_gateway_code("");
						reportData.setO_pymt_gateway_code(resultSet.getString("O_PMNT_GATEWAY_CODE"));
					} else {
						reportData.setPymt_gateway_code(resultSet.getString("PMNT_GATEWAY_CODE"));
						reportData.setO_pymt_gateway_code(resultSet.getString("O_PMNT_GATEWAY_CODE"));
					}

					reportData.setDiscountcode(resultSet.getString("DISCOUNT_CODE"));

					if (resultSet.getString("TRANSACTION_DATETIME") == resultSet.getString("O_TRANSACTION_DATETIME")) {
						reportData.setTransactiondatettime("");
						reportData.setO_transactiondatettime(resultSet.getString("O_TRANSACTION_DATETIME"));
					} else {
						reportData.setTransactiondatettime(resultSet.getString("TRANSACTION_DATETIME"));
						reportData.setO_transactiondatettime(resultSet.getString("O_TRANSACTION_DATETIME"));
					}

					if (resultSet.getString("AMOUNT").equals(resultSet.getString("O_AMOUNT"))) {
						reportData.setAmount("");
						reportData.setO_amount(resultSet.getString("O_AMOUNT"));
					} else {
						reportData.setAmount(resultSet.getString("AMOUNT"));
						reportData.setO_amount(resultSet.getString("O_AMOUNT"));
					}

					if (resultSet.getString("PMNT_STATUS").equals(resultSet.getString("O_PMNT_STATUS"))) {
						reportData.setPaymentstatus("");
						reportData.setO_paymentstatus(resultSet.getString("O_PMNT_STATUS"));
					} else {
						reportData.setPaymentstatus(resultSet.getString("PMNT_STATUS"));
						reportData.setO_paymentstatus(resultSet.getString("O_PMNT_STATUS"));
					}

					try {

						if (resultSet.getString("TRANSACTION_STATUS")
								.equals(resultSet.getString("O_TRANSACTION_STATUS"))) {
							reportData.setTransactionstatus("");
							reportData.setO_transactionstatus(resultSet.getString("O_TRANSACTION_STATUS"));
						} else {
							reportData.setTransactionstatus(resultSet.getString("TRANSACTION_STATUS"));
							reportData.setO_transactionstatus(resultSet.getString("O_TRANSACTION_STATUS"));
						}

					} catch (Exception ex) {

					}

					try {

						if (resultSet.getString("Remarks").equals(resultSet.getString("Remarks"))) {
							reportData.setTransactionstatus("");
							reportData.setO_transactionstatus(resultSet.getString("O_TRANSACTION_STATUS"));
						} else {
							reportData.setTransactionstatus(resultSet.getString("Remarks"));
							reportData.setO_transactionstatus(resultSet.getString("O_TRANSACTION_STATUS"));
						}

					} catch (Exception ex) {

					}

					reportData.setRemarks(resultSet.getString("Remarks"));
					reportData.setWho(resultSet.getString("ActionBy"));
					reportData.setDatewhen(resultSet.getString("DATE_WHEN"));
					reportData.setRecordcomment(resultSet.getString("RECORD_COMMENT"));

					myDataList.add(reportData);

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return myDataList;

	}

	public static void main(String args[]) {

		Audit_Payment_DAO rdao = new Audit_Payment_DAO();
		List<Audit_Payment_Bean> releaseDataList = rdao.getReportData("2011-08-01 00:00:00", "2019-08-02 00:00:00",
				"MPAY");
		Iterator itr = releaseDataList.iterator();

		while (itr.hasNext()) {
			Audit_Payment_Bean rd = (Audit_Payment_Bean) itr.next();
			System.out.println(" " + rd.getO_paymentstatus() + rd.getAmount());
		}
	}
}
