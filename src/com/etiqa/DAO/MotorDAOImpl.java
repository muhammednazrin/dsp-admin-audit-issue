package com.etiqa.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.common.DB.DBUtil;
import com.spring.VO.CommonNVIC;

import oracle.jdbc.OracleTypes;

public class MotorDAOImpl implements MotorDAO {

	Connection connection = ConnectionFactory.getConnection();
	Statement statement = null;
	ResultSet rs = null;

	@Override
	public List<CommonNVIC> getMakeList() {

		try {

			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_MI_SP_MAKELIST(?)}");

			cstmt.registerOutParameter(1, OracleTypes.CURSOR);
			cstmt.execute();
			List<CommonNVIC> detail = new ArrayList<CommonNVIC>();
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {

				CommonNVIC commonnvic = new CommonNVIC();
				commonnvic.setMakeCode(rs.getString("MAKE_CODE"));
				commonnvic.setMake(rs.getString("MAKE"));

				detail.add(commonnvic);

			}
			return detail;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}

	}

	@Override
	public List<CommonNVIC> getModelList(String makeCode) {
		try {
			// int i = startRow;
			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{CALL DSP_MI_SP_MODELLIST(?,?,?)}");

			cstmt.setString(1, makeCode.trim());
			cstmt.registerOutParameter(2, OracleTypes.CURSOR);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);

			cstmt.execute();

			List<CommonNVIC> list = new ArrayList<CommonNVIC>();
			rs = (ResultSet) cstmt.getObject(3);
			while (rs.next()) {

				CommonNVIC commonnvic = new CommonNVIC();
				commonnvic.setModelCode(rs.getString("MODEL_CODE"));
				commonnvic.setModel(rs.getString("MODEL"));

				list.add(commonnvic);

			}

			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	@Override
	public List<CommonNVIC> getYearList() {
		try {
			// int i = startRow;
			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{CALL DSP_MI_SP_YEARLIST(?)}");

			cstmt.registerOutParameter(1, OracleTypes.CURSOR);

			cstmt.execute();

			List<CommonNVIC> list = new ArrayList<CommonNVIC>();
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {

				CommonNVIC commonnvic = new CommonNVIC();
				commonnvic.setYear(Short.valueOf(rs.getString("YEAR")));

				list.add(commonnvic);

			}

			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}
}
