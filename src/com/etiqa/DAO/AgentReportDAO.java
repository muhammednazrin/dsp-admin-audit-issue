package com.etiqa.DAO;

import java.util.List;

import com.etiqa.model.report.TransactionalReport;

public interface AgentReportDAO {

	/*
	 * public List<TransactionalReport> findByProductType(
	 *
	 * String ProductType, String ProductEntity, String Status, String
	 * PolicyCertificateNo, String DateFrom, String DateTo, String NRIC, String
	 * ReceiptNo, String agentCode, String agentName, String marketerCode, String
	 * marketerName, String salesEntity, int StartRow, int EndRow);
	 */

	public List<TransactionalReport> findByProductType(

			String ProductType, String ProductEntity, String Status, String PolicyCertificateNo, String DateFrom,
			String DateTo, String NRIC, String ReceiptNo,
			/*
			 * String agentCode, String agentName, String marketerCode, String marketerName,
			 * String salesEntity,
			 */
			int StartRow, int EndRow);

	public List<TransactionalReport> rejectRecordMI(String productType,
			// String ProductEntity,
			String status, String policyCertificateNo, String dateFrom, String dateTo, String NRIC, String receiptNo,
			int startRow, int endRow);

	public List<TransactionalReport> findByHohh(String productType, String ProductEntity, String status,
			String policyCertificateNo, String dateFrom, String dateTo, String NRIC, String receiptNo, int startRow,
			int endRow);

	public List<TransactionalReport> incompleteRecordMI(String productType,
			// String ProductEntity,
			String status, String policyCertificateNo, String dateFrom, String dateTo, String NRIC, String receiptNo,
			int startRow, int endRow);

	public List<TransactionalReport> rejectRecordTL(String productType,
			// String ProductEntity,
			String status, String policyCertificateNo, String dateFrom, String dateTo, String NRIC, String receiptNo,
			int startRow, int endRow);

	public List<TransactionalReport> incompleteRecordTL(String productType,
			// String ProductEntity,
			String status, String policyCertificateNo, String dateFrom, String dateTo, String NRIC, String receiptNo,
			int startRow, int endRow);

}
