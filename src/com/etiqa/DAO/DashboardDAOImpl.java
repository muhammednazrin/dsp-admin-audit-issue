package com.etiqa.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.stereotype.Repository;

import com.etiqa.common.DB.ConnectionFactory;

import oracle.jdbc.OracleTypes;

@Repository("dashboardDAO")
public class DashboardDAOImpl implements DashboardDAO {

	private static ConnectionFactory db1;
	private static Connection connection;

	@Override
	public String getTLPerformance(String fromdt) {
		ResultSet resultSet = null;
		Statement statement = null;
		String resp = "";
		String result_return = "";
		int count = 1;
		try {
			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{call DSP_ADM_DashBoard_TL_Perf(?,?)}");
			cstmt.setString(1, fromdt);
			cstmt.registerOutParameter(2, OracleTypes.CURSOR);
			cstmt.execute();
			resultSet = (ResultSet) cstmt.getObject(2);
			while (resultSet.next()) {
				// resp = resp + resultSet.getString("TotalSaleAmount") + ",";
				resp = resp + "{ x: " + resultSet.getString("Transaction_Date") + ", y:"
						+ resultSet.getString("TotalSaleAmount") + "},";

				count = count + 1;
			}
			resp = resp.substring(0, resp.length() - 1);
			// result_return = "['TermLife'," + resp + "],";
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return resp;
	}

	@Override
	public String getWTCPerformance(String fromdt) {
		ResultSet resultSet = null;
		Statement statement = null;
		String resp = "";
		String result_return = "";
		int count = 1;
	
		try {
			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{call DSP_ADM_DashBoard_WTC_T_Perf(?,?)}");
			cstmt.setString(1, fromdt);
			cstmt.registerOutParameter(2, OracleTypes.CURSOR);
			cstmt.execute();
			resultSet = (ResultSet) cstmt.getObject(2);

			while (resultSet.next()) {
				
				// resp = resp + resultSet.getString("TotalSaleAmount") + ",";
				resp = resp + "{ x: " + resultSet.getString("Transaction_Date") + ", y:"
						+ resultSet.getString("TotalSaleAmount") + "},";

				count = count + 1;
			}
			//System.out.println("-------------------------------------"+(resp.length() - 1));
			resp = resp.substring(0, resp.length() - 1);
			
			// result_return = "['WTC'," + resp + "],";
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return resp;
	}

	@Override
	public String getHOHHPerformance(String fromdt) {
		ResultSet resultSet = null;
		Statement statement = null;
		String resp = "";
		String result_return = "";
		int count = 1;
		try {
			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{call DSP_ADM_DashBoard_HOHH_T_Perf(?,?)}");
			cstmt.setString(1, fromdt);
			cstmt.registerOutParameter(2, OracleTypes.CURSOR);
			cstmt.execute();
			resultSet = (ResultSet) cstmt.getObject(2);
			while (resultSet.next()) {
				// resp = resp + resultSet.getString("TotalSaleAmount") + ",";
				resp = resp + "{ x: " + resultSet.getString("Transaction_Date") + ", y:"
						+ resultSet.getString("TotalSaleAmount") + "},";
				count = count + 1;
			}
			resp = resp.substring(0, resp.length() - 1);
			// result_return = "['HOHH'," + resp + "],";
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return resp;
	}

	@Override
	public String getMIPerformance(String fromdt) {
		ResultSet resultSet = null;
		Statement statement = null;
		String resp = "";
		String resp1 = "";
		String result_return = "";
		int count = 1;
		try {
			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{call DSP_ADM_DashBoard_MI_Perf(?,?)}");
			cstmt.setString(1, fromdt);
			cstmt.registerOutParameter(2, OracleTypes.CURSOR);
			cstmt.execute();
			resultSet = (ResultSet) cstmt.getObject(2);
			while (resultSet.next()) {

				// resp = resp + resultSet.getString("TotalSaleAmount") + ",";

				resp = resp + "{ x: " + resultSet.getString("Transaction_Date") + ", y:"
						+ resultSet.getString("TotalSaleAmount") + "},";
				count = count + 1;

			}

			resp = resp.substring(0, resp.length() - 1);
			result_return = "['Motor'," + resp + "],";

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return resp;

	}
	
	/*@Override
	public String getCCPerformance(String fromdt) {
		ResultSet resultSet = null;
		Statement statement = null;
		String resp = "";
		String resp1 = "";
		String result_return = "";
		int count = 1;
		try {
			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{call DSP_ADM_DashBoard_CC_Perf(?,?)}");
			cstmt.setString(1, fromdt);
			cstmt.registerOutParameter(2, OracleTypes.CURSOR);
			cstmt.execute();
			resultSet = (ResultSet) cstmt.getObject(2);
			while (resultSet.next()) {

				// resp = resp + resultSet.getString("TotalSaleAmount") + ",";

				resp = resp + "{ x: " + resultSet.getString("Transaction_Date") + ", y:"
						+ resultSet.getString("TotalSaleAmount") + "},";
				count = count + 1;

			}

			resp = resp.substring(0, resp.length() - 1);
			//result_return = "['Motor'," + resp + "],";

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return resp;
	}*/

}
