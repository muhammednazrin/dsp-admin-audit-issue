package com.etiqa.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.common.DB.DBUtil;
import com.etiqa.common.DB.NamedParameterStatement;
import com.etiqa.model.operator.Operator;

import oracle.jdbc.OracleTypes;

public class OperatorDAOImpl implements OperatorDAO {
	Connection connection = ConnectionFactory.getConnection();
	Statement statement = null;
	ResultSet rs = null;

	@Override
	public List<Operator> searchByList(String operatorName, String operatorCategory, String insuranceType,

			String dateFrom, String dateTo) {
		// int startRow,
		// int endRow) {

		System.out.println(
				"Operator Name " + operatorName + "category" + operatorCategory + "insurance Type" + insuranceType);

		// System.out.println("Operator code " +operatorCode);
		System.out.println("date from " + dateFrom);
		System.out.println("date To " + dateTo);
		try {
			// int i = startRow;
			connection = ConnectionFactory.getConnection();
			//
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_OPERATOR_SEARCH(?,?,?,?,?,?)}");

			cstmt.setString(1, operatorName);
			cstmt.setString(2, operatorCategory.trim());
			cstmt.setString(3, insuranceType);
			// cstmt.setString(5,status);
			cstmt.setString(4, dateFrom);
			cstmt.setString(5, dateTo);
			// cstmt.setDate(4, (Date)dateFrom);

			// cstmt.setInt(8,startRow);
			// cstmt.setInt(9,endRow);
			cstmt.registerOutParameter(6, OracleTypes.CURSOR);
			cstmt.execute();
			List<Operator> list = new ArrayList<Operator>();
			rs = (ResultSet) cstmt.getObject(6);
			System.out.println("Resultset " + rs.getFetchSize());
			System.out.println(
					"Operator Name " + operatorName + "category" + operatorCategory + "insurance Type" + insuranceType);

			while (rs.next()) {

				System.out.println("Resultset values " + rs.getLong("ID"));
				// int i =0;
				Operator operator = new Operator();
				operator.setId(rs.getLong("ID"));
				System.out.println(rs.getInt(1) + "ID");
				operator.setAnnualSalesTarget(rs.getDouble("ANNUAL_SALES_TARGET"));
				operator.setOperatorCode(rs.getString("OPERATOR_CODE"));
				operator.setOperatorCategory(rs.getString("OPERATOR_CATEGORY"));

				operator.setOperatorName(rs.getString("OPERATOR_NAME"));

				operator.setEntity(rs.getString("ENTITY"));
				operator.setIdType(rs.getString("ID_TYPE_NO"));
				operator.setIdNo(rs.getString("ID_NO"));
				operator.setPhoneType(rs.getString("PHONE_TYPE"));
				operator.setPhoneNO(rs.getString("PHONE_NUMBER"));
				operator.setEmail(rs.getString("EMAIL"));
				operator.setAddress1(rs.getString("ADDRESS"));
				operator.setCity(rs.getString("CITY"));
				operator.setPostcode(rs.getString("POST_CODE"));
				operator.setState(rs.getString("STATE"));
				operator.setCountry(rs.getString("COUNTRY"));
				operator.setOperatorType(rs.getString("OPERATOR_TYPE"));

				operator.setRegistrationDate(rs.getString("REGISTRATION_DATE"));
				System.out.println(operator.getOperatorType() + "  " + operator.getId());
				list.add(operator);

			}

			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	private static java.sql.Date getCurrentDate(Date date) {

		return new java.sql.Date(date.getTime());
	}

	@Override
	public Operator findById(Long id) {
		Connection connection = null;
		connection = ConnectionFactory.getConnection();
		String sql = "SELECT * FROM DSP_ADM_TBL_OPERATOR_REG WHERE ID=?";

		Operator operator = null;

		try {
			PreparedStatement pstmt = connection.prepareStatement(sql);
			pstmt.setLong(1, id);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				operator = populateBean(rs);
			}

			rs.close();
			pstmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		} finally {
			DBUtil.close(connection);
		}
		return operator;
	}

	@Override
	public List<Operator> findAll() {
		Connection connection = null;
		connection = ConnectionFactory.getConnection();

		String sql = "SELECT * FROM DSP_ADM_TBL_OPERATOR_REG order by ID DESC";

		List<Operator> list = new ArrayList<Operator>();

		try {
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				Operator operator = populateBean(rs);
				list.add(operator);
			}
			rs.close();
			st.close();
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		} finally {
			DBUtil.close(connection);
		}
		return list;
	}

	@Override
	public void save(Operator entity) {

		Connection connection = null;
		connection = ConnectionFactory.getConnection();
		PreparedStatement pst = null;

		String sql = "INSERT INTO DSP_ADM_TBL_OPERATOR_REG (OPERATOR_CATEGORY,USERNAME,PASSWORD,ENTITY,ANNUAL_SALES_TARGET,OPERATOR_NAME,ID_TYPE_NO,ID_NO,PHONE_TYPE,PHONE_NUMBER,EMAIL,ADDRESS,CITY,STATE,COUNTRY,POST_CODE,REGISTRATION_DATE, OPERATOR_TYPE) VALUES";
		sql += "(";

		// sql +=":OPERATOR_CODE,";
		sql += ":OPERATOR_CATEGORY,";
		sql += ":USERNAME,";
		sql += ":PASSWORD,";
		sql += ":ENTITY,";
		sql += ":ANNUAL_SALES_TARGET,";
		sql += ":OPERATOR_NAME,";
		sql += ":ID_TYPE_NO,";
		sql += ":ID_NO,";
		sql += ":PHONE_TYPE,";
		sql += ":PHONE_NUMBER,";
		sql += ":EMAIL,";
		sql += ":ADDRESS,";
		sql += ":CITY,";
		sql += ":STATE,";
		sql += ":COUNTRY,";

		sql += ":POST_CODE,";
		sql += ":REGISTRATION_DATE,";
		sql += ":OPERATOR_TYPE)";

		// values
		// (3,'003',to_date('29-OCT-16','DD-MON-RR'),'Indivisual','HOHH','active',400000,'003','K.Sibba
		// Reddy','003','8891898989','bbb@g.com','CCanada','004');
		// String productSql = "INSERT INTO DSP_ADM_TBL_OPERATOR_PRODUCT
		// (OPERATOR_CODE,PRODUCT_CODE) VALUES (?,?)";

		try {
			NamedParameterStatement p = new NamedParameterStatement(connection, sql);

			// p.setString("OPERATOR_CODE", entity.getOperatorCode());
			p.setString("OPERATOR_CATEGORY", entity.getOperatorCategory());
			p.setString("USERNAME", entity.getUserName());
			p.setString("PASSWORD", entity.getPassword());
			p.setString("ENTITY", entity.getEntity());

			p.setDouble("ANNUAL_SALES_TARGET", entity.getAnnualSalesTarget());

			p.setString("OPERATOR_NAME", entity.getOperatorName());
			p.setString("ID_TYPE_NO", entity.getIdType());
			p.setString("ID_NO", entity.getIdNo());
			p.setString("PHONE_TYPE", entity.getPhoneType());
			p.setString("PHONE_NUMBER", entity.getPhoneNO());
			p.setString("EMAIL", entity.getEmail());
			p.setString("ADDRESS", entity.getAddress1());

			p.setString("CITY", entity.getCity());
			p.setString("STATE", entity.getState());
			p.setString("COUNTRY", entity.getCountry());

			p.setString("POST_CODE", entity.getPostcode());

			p.setString("REGISTRATION_DATE", entity.getRegistrationDate());
			p.setString("OPERATOR_TYPE", entity.getOperatorType());

			p.executeUpdate();

			/*
			 * if( entity.getOperatorProducts() != null &&
			 * entity.getOperatorProducts().size()>0){ pst =
			 * connection.prepareStatement(productSql); for( OperatorProduct agentProduct :
			 * entity.getOperatorProducts() ){ //pst.setString(1,
			 * agentProduct.getOperatorCode()); //pst.setString(2,
			 * agentProduct.getProductCode()); //pst.executeUpdate(); } }
			 */
			p.close();

		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		} finally {
			DBUtil.close(connection);
		}
	}

	@Override
	public Operator update(Operator entity) {
		int update;
		Connection connection = null;
		connection = ConnectionFactory.getConnection();
		// FIXME: for testing purpose only 3 fields
		String sql = "UPDATE DSP_ADM_TBL_OPERATOR_REG SET OPERATOR_CODE=?,OPERATOR_CATEGORY=?,ENTITY=?,ANNUAL_SALES_TARGET=?,OPERATOR_NAME=?,ID_TYPE_NO=?,PHONE_NUMBER=?,EMAIL=?,ADDRESS=?,OPERATOR_TYPE=?,REGISTRATION_DATE=? WHERE ID = ?";

		try {
			PreparedStatement pstmt = connection.prepareStatement(sql);
			pstmt.setString(1, entity.getOperatorCode());
			pstmt.setString(2, entity.getOperatorCategory());
			pstmt.setString(3, entity.getEntity());
			pstmt.setDouble(4, entity.getAnnualSalesTarget());
			pstmt.setString(5, entity.getOperatorName());
			pstmt.setString(6, entity.getIdType());
			pstmt.setString(7, entity.getPhoneNO());
			pstmt.setString(8, entity.getEmail());
			pstmt.setString(9, entity.getAddress1());
			pstmt.setString(10, entity.getOperatorType());
			pstmt.setString(11, entity.getRegistrationDate());
			pstmt.setLong(12, entity.getId());

			update = pstmt.executeUpdate();
			pstmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		} finally {
			DBUtil.close(connection);
		}

		return entity;
	}

	@Override
	public void delete(Operator entity) {
		Connection connection = null;
		connection = ConnectionFactory.getConnection();
		String sql = "DELETE FROM DSP_ADM_TBL_OPERATOR_REG WHERE ID = ?";

		try {
			PreparedStatement pstmt = connection.prepareStatement(sql);
			pstmt.setLong(1, entity.getId());
			pstmt.executeUpdate();
			pstmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		} finally {
			DBUtil.close(connection);
		}
	}

	private Operator populateBean(ResultSet rs) throws SQLException {
		Operator operator = new Operator();
		operator.setId(rs.getLong("ID"));
		System.out.println(rs.getInt(1) + "ID");
		operator.setAnnualSalesTarget(rs.getDouble("ANNUAL_SALES_TARGET"));
		// operator.setOperatorCode(rs.getString("OPERATOR_CODE"));
		operator.setOperatorCategory(rs.getString("OPERATOR_CATEGORY"));

		operator.setOperatorName(rs.getString("OPERATOR_NAME"));
		operator.setUserName(rs.getString("USERNAME"));
		operator.setPassword(rs.getString("PASSWORD"));
		operator.setEntity(rs.getString("ENTITY"));
		operator.setIdType(rs.getString("ID_TYPE_NO"));
		operator.setIdNo(rs.getString("ID_NO"));
		operator.setPhoneType(rs.getString("PHONE_TYPE"));
		operator.setPhoneNO(rs.getString("PHONE_NUMBER"));
		operator.setEmail(rs.getString("EMAIL"));
		operator.setAddress1(rs.getString("ADDRESS"));
		operator.setCity(rs.getString("CITY"));
		operator.setPostcode(rs.getString("POST_CODE"));
		operator.setState(rs.getString("STATE"));
		operator.setCountry(rs.getString("COUNTRY"));

		operator.setRegistrationDate(rs.getString("REGISTRATION_DATE"));
		operator.setOperatorType(rs.getString("OPERATOR_TYPE"));

		return operator;
	}

	public static void main(String args[]) {
		OperatorDAO dao = new OperatorDAOImpl();// daoFactory.getAgentDAO();
		List<Operator> operator = dao.searchByList("Tulasi", "", "",
				// status,
				"2017-01-01", "2017-01-31");

		System.out.println(operator);

	}
}
