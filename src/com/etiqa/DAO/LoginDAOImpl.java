package com.etiqa.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.model.others.Login;

public class LoginDAOImpl implements LoginDAO {

	Connection connection = ConnectionFactory.getConnection();
	// Statement statement = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;

	@Override
	public Login getLogin(String userName, String password) throws SQLException {
		Login login = null;
		Connection connection = ConnectionFactory.getConnection();
		String sql = " SELECT USERNAME, PASSWORD FROM DSP_ADM_TBL_LOGIN WHERE USERNAME= ? and PASSWORD = ?";
		pstmt = connection.prepareStatement(sql);
		pstmt.setString(1, userName);
		pstmt.setString(2, password);

		rs = pstmt.executeQuery(sql);
		if (rs.next()) {
			login = new Login();
			login.setUserName(rs.getString("USERNAME"));
			login.setUserName(rs.getString("PASSWORD"));
		}
		return login;
	}

}
