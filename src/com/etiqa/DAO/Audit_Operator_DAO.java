package com.etiqa.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.dsp.dao.common.pojo.Audit_Operator_Bean;

import oracle.jdbc.OracleTypes;

public class Audit_Operator_DAO {

	private static ConnectionFactory db1;
	private static Connection connection;

	public List<Audit_Operator_Bean> getReportData(String fromdt, String todt) {
		List<Audit_Operator_Bean> myDataList = new ArrayList<Audit_Operator_Bean>();
		ResultSet resultSet = null;
		Statement statement = null;

		try {

			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{call GET_AUDIT_OPT_REGISTRATION(?,?,?)}");
			cstmt.setString(1, fromdt);
			cstmt.setString(2, todt);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);

			cstmt.execute();

			resultSet = (ResultSet) cstmt.getObject(3);

			while (resultSet.next()) {

				Audit_Operator_Bean reportData = new Audit_Operator_Bean();

				/*
				 * try {
				 *
				 * if (resultSet.getString("O_ID").equals(resultSet.getString("ID"))) {
				 * reportData.setO_id(resultSet.getString("O_ID")); reportData.setId("");
				 *
				 * } else { reportData.setO_id(resultSet.getString("O_ID"));
				 * reportData.setId(resultSet.getString("ID")); } } catch (Exception ex) { }
				 *
				 * try { if (resultSet.getString("OPERATOR_CODE").equals(resultSet.getString(
				 * "O_OPERATOR_CODE"))) {
				 * reportData.setO_operatorcode(resultSet.getString("O_OPERATOR_CODE"));
				 * reportData.setOperatorcode("");
				 *
				 * } else {
				 * reportData.setO_operatorcode(resultSet.getString("O_OPERATOR_CODE"));
				 * reportData.setOperatorcode(resultSet.getString("OPERATOR_CODE")); } } catch
				 * (Exception ex) { } try { if
				 * (resultSet.getString("OPERATOR_NAME").equals(resultSet.getString(
				 * "O_OPERATOR_NAME"))) {
				 * reportData.setO_operatorname(resultSet.getString("O_OPERATOR_NAME"));
				 * reportData.setOperatorname("");
				 *
				 * } else {
				 * reportData.setO_operatorname(resultSet.getString("O_OPERATOR_NAME"));
				 * reportData.setOperatorname(resultSet.getString("OPERATOR_NAME")); } } catch
				 * (Exception ex) { } try { if
				 * (resultSet.getString("O_REGISTRATION_DATE").equals(resultSet.getString(
				 * "REGISTRATION_DATE"))) {
				 * reportData.setO_registrationdate(resultSet.getString("O_REGISTRATION_DATE"));
				 * reportData.setRegistrationdate("");
				 *
				 * } else {
				 * reportData.setO_registrationdate(resultSet.getString("O_REGISTRATION_DATE"));
				 * reportData.setRegistrationdate(resultSet.getString("REGISTRATION_DATE")); } }
				 * catch (Exception ex) { } try { if
				 * (resultSet.getString("OPERATOR_CATEGORY").equals(resultSet.getString(
				 * "O_OPERATOR_CATEGORY"))) {
				 * reportData.setO_operatorcategory(resultSet.getString("O_OPERATOR_CATEGORY"));
				 * reportData.setOperatorcategory("");
				 *
				 * } else {
				 * reportData.setO_operatorcategory(resultSet.getString("O_OPERATOR_CATEGORY"));
				 * reportData.setOperatorcategory(resultSet.getString("OPERATOR_CATEGORY")); } }
				 * catch (Exception ex) { } try { if (resultSet.getString("ANNUAL_SALES_TARGET")
				 * .equals(resultSet.getString("O_ANNUAL_SALES_TARGET"))) {
				 * reportData.setO_annualsalestarget(resultSet.getString("O_ANNUAL_SALES_TARGET"
				 * )); reportData.setAnnualsalestarget("");
				 *
				 * } else {
				 * reportData.setO_annualsalestarget(resultSet.getString("O_ANNUAL_SALES_TARGET"
				 * ));
				 * reportData.setAnnualsalestarget(resultSet.getString("ANNUAL_SALES_TARGET"));
				 * } } catch (Exception ex) { } try { if
				 * (resultSet.getString("ADDRESS1").equals(resultSet.getString("O_ADDRESS1"))) {
				 * reportData.setO_address1(resultSet.getString("O_ADDRESS1"));
				 * reportData.setAddress1("");
				 *
				 * } else { reportData.setO_address1(resultSet.getString("O_ADDRESS1"));
				 * reportData.setAddress1(resultSet.getString("ADDRESS1")); } } catch (Exception
				 * ex) { } try { if
				 * (resultSet.getString("CITY").equals(resultSet.getString("O_CITY"))) {
				 * reportData.setO_city(resultSet.getString("O_CITY")); reportData.setCity("");
				 * } else { reportData.setO_city(resultSet.getString("O_CITY"));
				 * reportData.setCity(resultSet.getString("CITY")); } } catch (Exception ex) { }
				 * try { if
				 * (resultSet.getString("STATE").equals(resultSet.getString("O_STATE"))) {
				 * reportData.setO_state(resultSet.getString("O_STATE"));
				 * reportData.setState("");
				 *
				 * } else { reportData.setO_state(resultSet.getString("O_STATE"));
				 * reportData.setState(resultSet.getString("STATE")); } } catch (Exception ex) {
				 * } try { if
				 * (resultSet.getString("COUNTRY").equals(resultSet.getString("O_COUNTRY"))) {
				 * reportData.setO_country(resultSet.getString("O_COUNTRY"));
				 * reportData.setCountry("");
				 *
				 * } else { reportData.setO_country(resultSet.getString("O_COUNTRY"));
				 * reportData.setCountry(resultSet.getString("COUNTRY")); } } catch (Exception
				 * ex) { } try { if
				 * (resultSet.getString("POST_CODE").equals(resultSet.getString("O_POST_CODE")))
				 * { reportData.setO_postcode(resultSet.getString("O_POST_CODE"));
				 * reportData.setPostcode("");
				 *
				 * } else { reportData.setO_postcode(resultSet.getString("O_POST_CODE"));
				 * reportData.setPostcode(resultSet.getString("POST_CODE")); } } catch
				 * (Exception ex) { } try { if
				 * (resultSet.getString("AGENT_REFERAL_ID").equals(resultSet.getString(
				 * "O_AGENT_REFERAL_ID"))) {
				 * reportData.setO_annualreferalid(resultSet.getString("O_AGENT_REFERAL_ID"));
				 * reportData.setAnnualreferalid("");
				 *
				 * } else {
				 * reportData.setO_annualreferalid(resultSet.getString("O_AGENT_REFERAL_ID"));
				 * reportData.setAnnualreferalid(resultSet.getString("AGENT_REFERAL_ID")); } }
				 * catch (Exception ex) { } try { reportData.setWho(resultSet.getString("WHO"));
				 * reportData.setDatewhen(resultSet.getString("DATE_WHEN"));
				 * reportData.setRecordcomment(resultSet.getString("RECORD_COMMENT")); } catch
				 * (Exception ex) { }
				 */

				// Chandra
				/*
				 * if ( null != resultSet.getString("O_OPERATOR_CODE") ) {
				 * reportData.setOperatorcode(resultSet.getString("OPERATOR_CODE"));
				 * reportData.setO_operatorcode(resultSet.getString("O_OPERATOR_CODE")); } else
				 * { reportData.setOperatorcode(resultSet.getString("OPERATOR_CODE"));
				 * reportData.setO_operatorcode(resultSet.getString("O_OPERATOR_CODE")); }
				 *
				 * if ( null != resultSet.getString("O_OPERATOR_NAME") ) {
				 * reportData.setOperatorname(resultSet.getString("OPERATOR_NAME"));
				 * reportData.setO_operatorname(resultSet.getString("O_OPERATOR_NAME")); } else
				 * { reportData.setOperatorname(resultSet.getString("OPERATOR_NAME"));
				 * reportData.setO_operatorname(resultSet.getString("O_OPERATOR_NAME")); }
				 *
				 * if ( null != resultSet.getString("O_REGISTRATION_DATE") ) {
				 * reportData.setRegistrationdate(resultSet.getString("REGISTRATION_DATE"));
				 * reportData.setO_registrationdate(resultSet.getString("O_REGISTRATION_DATE"));
				 * } else {
				 * reportData.setRegistrationdate(resultSet.getString("REGISTRATION_DATE"));
				 * reportData.setO_registrationdate(resultSet.getString("O_REGISTRATION_DATE"));
				 * }
				 *
				 * if ( null != resultSet.getString("O_ANNUAL_SALES_TARGET") ) {
				 * reportData.setAnnualsalestarget(resultSet.getString("ANNUAL_SALES_TARGET"));
				 * reportData.setO_annualsalestarget(resultSet.getString("O_ANNUAL_SALES_TARGET"
				 * )); } else {
				 * reportData.setAnnualsalestarget(resultSet.getString("ANNUAL_SALES_TARGET"));
				 * reportData.setO_annualsalestarget(resultSet.getString("O_ANNUAL_SALES_TARGET"
				 * )); }
				 *
				 * if ( null != resultSet.getString("O_ADDRESS1") ) {
				 * reportData.setAddress1(resultSet.getString("ADDRESS1"));
				 * reportData.setO_address1(resultSet.getString("O_ADDRESS1")); } else {
				 * reportData.setAddress1(resultSet.getString("ADDRESS1"));
				 * reportData.setO_address1(resultSet.getString("O_ADDRESS1")); }
				 */

				reportData.setOperatorcode(resultSet.getString("OPERATOR_CODE"));
				reportData.setO_operatorcode(resultSet.getString("O_OPERATOR_CODE"));

				reportData.setOperatorname(resultSet.getString("OPERATOR_NAME"));
				reportData.setO_operatorname(resultSet.getString("O_OPERATOR_NAME"));

				// replacing the Registration Date
				String reg_date = "";
				String o_reg_date = "";
				if (null != resultSet.getString("REGISTRATION_DATE")) {
					reg_date = resultSet.getString("REGISTRATION_DATE");
					reportData.setRegistrationdate(reg_date.replaceAll("-", "/").substring(0, 10));
				}
				if (null != resultSet.getString("O_REGISTRATION_DATE")) {
					o_reg_date = resultSet.getString("O_REGISTRATION_DATE");
					reportData.setO_registrationdate(o_reg_date.replaceAll("-", "/").substring(0, 10));
				}

				reportData.setAnnualsalestarget(resultSet.getString("ANNUAL_SALES_TARGET"));
				reportData.setO_annualsalestarget(resultSet.getString("O_ANNUAL_SALES_TARGET"));

				reportData.setAddress1(resultSet.getString("ADDRESS1"));
				reportData.setO_address1(resultSet.getString("O_ADDRESS1"));

				reportData.setWho(resultSet.getString("WHO"));
				reportData.setDatewhen(resultSet.getString("DATE_WHEN"));
				reportData.setRecordcomment(resultSet.getString("RECORD_COMMENT"));

				myDataList.add(reportData);

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return myDataList;

	}

	public static void main(String args[]) {
		Audit_Operator_DAO rdao = new Audit_Operator_DAO();
		List<Audit_Operator_Bean> releaseDataList = rdao.getReportData("2011-08-01 00:00:00", "2019-08-02 00:00:00");
		Iterator itr = releaseDataList.iterator();
		while (itr.hasNext()) {
			Audit_Operator_Bean rd = (Audit_Operator_Bean) itr.next();
			System.out.println(" " + rd.getId() + rd.getOperatorname());
		}

	}
}
