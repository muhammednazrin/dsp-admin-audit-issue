
package com.etiqa.DAO;

import java.util.List;

import com.etiqa.model.operator.Operator;

public interface OperatorDAO {

	public Operator findById(Long id);

	public List<Operator> findAll();

	public void save(Operator entity);

	public Operator update(Operator entity);

	public void delete(Operator entity);

	public List<Operator> searchByList(String operatorName, String operatorCategory, String insuranceType,
			String dateFrom, String dateTo);

}
