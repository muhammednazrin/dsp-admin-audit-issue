package com.etiqa.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.common.DB.DBUtil;
import com.etiqa.model.report.TransactionalReport;
import com.spring.admin.TranReportController;

import oracle.jdbc.OracleTypes;

public class AgentReportDAOImpl implements AgentReportDAO {
	final static Logger logger = Logger.getLogger(AgentReportDAOImpl.class);
	Connection connection = ConnectionFactory.getConnection();
	Statement statement = null;
	ResultSet rs = null;

	@Override
	public List<TransactionalReport> findByProductType(String productType, String productEntity, String status,
			String policyCertificateNo, String dateFrom, String dateTo, String NRIC, String receiptNo,
			/*
			 * String agentCode, String agentName, String marketerCode, String marketerName,
			 * String salesEntity,
			 */
			int startRow, int endRow) {

		double totalAmount = 0.00;

		logger.info("productType " + productType);
		logger.info("status " + status);
		logger.info("AgentCode " + NRIC);
		try {
			int i = startRow;
			connection = ConnectionFactory.getConnection();
			// CallableStatement cstmt = connection.prepareCall("{CALL
			// DSP_ADM_SP_RPT_TRANSACTIONAL_S(?,?,?,?,?,?,?,?,?,?)}");
			CallableStatement cstmt = connection
					.prepareCall("{CALL DSP_ADM_SP_PERFORMANCE_RPT(?,?,?,?,?,?,?,?,?,?,?)}");
			// CallableStatement cstmt = connection.prepareCall("{CALL
			// DSP_ADM_SP_TRANSACTION_RPT_S6(?,?,?,?,?,?,?,?,?,?,?)}");
			cstmt.setString(1, productType.trim());
			cstmt.setString(2, productEntity.trim());
			cstmt.setString(3, status);
			cstmt.setString(4, policyCertificateNo);
			cstmt.setString(5, dateFrom);
			cstmt.setString(6, dateTo);
			cstmt.setString(7, NRIC);
			cstmt.setString(8, receiptNo);
			cstmt.setInt(9, startRow);
			cstmt.setInt(10, endRow);
			cstmt.registerOutParameter(11, OracleTypes.CURSOR);
			// cstmt.registerOutParameter(11, OracleTypes.CURSOR);
			cstmt.execute();

			/*
			 * @Override public List<TransactionalReport> findByProductType( String
			 * ProductType, String ProductEntity, String Status, String PolicyCertificateNo,
			 * String DateFrom, String DateTo, String NRIC, String ReceiptNo, String
			 * agentCode, String agentName, String marketerCode, String marketerName, String
			 * salesEntity, int StartRow, int EndRow) {
			 *
			 *
			 * double totalAmount= 0.00;
			 *
			 * logger.info("productType " +ProductType); logger.info("status "
			 * +Status); try { int i = StartRow; connection =
			 * ConnectionFactory.getConnection(); //CallableStatement cstmt = connection.
			 * prepareCall("{CALL DSP_ADM_SP_RPT_TRANSACTIONAL_S(?,?,?,?,?,?,?,?,?,?)}");
			 * CallableStatement cstmt = connection.
			 * prepareCall("{CALL DSP_ADM_SP_PERFORMANCE_RPT(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}"
			 * ); //CallableStatement cstmt = connection.
			 * prepareCall("{CALL DSP_ADM_SP_TRANSACTION_RPT_S6(?,?,?,?,?,?,?,?,?,?,?)}");
			 * cstmt.setString(1,ProductType.trim());
			 * cstmt.setString(2,ProductEntity.trim()); cstmt.setString(3,Status);
			 * cstmt.setString(4,PolicyCertificateNo); cstmt.setString(5,DateFrom);
			 * cstmt.setString(6,DateTo); cstmt.setString(7,NRIC);
			 * cstmt.setString(8,ReceiptNo);
			 *
			 * cstmt.setString(9,agentCode); cstmt.setString(10,agentName);
			 * cstmt.setString(11,marketerCode); cstmt.setString(12,marketerName);
			 * cstmt.setString(13,salesEntity);
			 *
			 * cstmt.setInt(14,StartRow); cstmt.setInt(15,EndRow);
			 * cstmt.registerOutParameter(11, OracleTypes.CURSOR);
			 * //cstmt.registerOutParameter(11, OracleTypes.CURSOR); cstmt.execute();
			 */

			List<TransactionalReport> list = new ArrayList<TransactionalReport>();
			rs = (ResultSet) cstmt.getObject(11);
			while (rs.next()) {

				TransactionalReport TranRPT = new TransactionalReport();
				TranRPT.setId(i);
				if (rs.getString(1) == null) {

					TranRPT.setDSPQQID("");
				} else {

					TranRPT.setDSPQQID(rs.getString(1));
				}

				if (rs.getString("DSPTXNID") == null) {

					TranRPT.setTransactionID("N/A");
				} else

				{
					TranRPT.setTransactionID(rs.getString("DSPTXNID"));

				}

				if (rs.getString("AMOUNT") == null) {

					TranRPT.setPremiumAmount("00.00");

				} else {
					TranRPT.setPremiumAmount(rs.getString("AMOUNT"));

				}

				if (rs.getString("PMNT_GATEWAY_CODE") == null) {

					TranRPT.setPaymentMethod("N/A");

				} else

				{
					TranRPT.setPaymentMethod(rs.getString("PMNT_GATEWAY_CODE"));

				}

				if (rs.getString("PMNT_STATUS") == null) {

					TranRPT.setStatus("N/A");

				} else

				{
					TranRPT.setStatus(rs.getString("PMNT_STATUS"));

				}

				if (rs.getString("TRANSACTION_DATETIME") == null) {

					TranRPT.setTransactionDate("00/00/0000");

				} else

				{
					TranRPT.setTransactionDate(rs.getString("TRANSACTION_DATETIME"));

				}

				if (rs.getString("INVOICE_NO") == null) {

					TranRPT.setInvoiceNo("N/A");

				} else

				{
					TranRPT.setInvoiceNo(rs.getString("INVOICE_NO"));

				}

				if (rs.getString("PRODUCT_CODE") == null) {

					TranRPT.setProductCode("N/A");

				} else

				{
					TranRPT.setProductCode(rs.getString("PRODUCT_CODE"));

				}

				if (rs.getString("PRODUCT_CODE_NAME") == null) {

					TranRPT.setProductType("N/A");

				} else

				{
					TranRPT.setProductType(rs.getString("PRODUCT_CODE_NAME"));

				}

				if (rs.getString("POLICY_NUMBER") == null) {

					TranRPT.setPolicyNo("N/A");

				} else

				{
					TranRPT.setPolicyNo(rs.getString("POLICY_NUMBER"));

				}

				if (rs.getString("CUSTOMER_NAME") == null) {

					TranRPT.setCustomerName("N/A");

				} else

				{
					TranRPT.setCustomerName(rs.getString("CUSTOMER_NAME"));

				}
				// Product Entity
				if (rs.getString("PRODUCT_CODE_NAME") != null && rs.getString("PRODUCT_CODE_NAME") != "") {

					if (rs.getString("PRODUCT_CODE_NAME").equalsIgnoreCase("BPT")
							|| rs.getString("PRODUCT_CODE_NAME").equalsIgnoreCase("MI")
							|| rs.getString("PRODUCT_CODE_NAME").equalsIgnoreCase("TL")
							|| rs.getString("PRODUCT_CODE_NAME").equalsIgnoreCase("HOHH")
							|| rs.getString("PRODUCT_CODE_NAME").equalsIgnoreCase("WTC")) {

						TranRPT.setProductEntity("EIB");

					} else

					{
						TranRPT.setProductEntity("ETB");

					}

				}

				if (rs.getString("CUSTOMER_ID") == null) {

					TranRPT.setCustomerID("N/A");

				} else

				{
					TranRPT.setCustomerID(rs.getString("CUSTOMER_ID"));

				}

				if (rs.getString("CUSTOMER_NRIC_ID") == null) {

					TranRPT.setCustomerNRIC("N/A");

				} else

				{
					TranRPT.setCustomerNRIC(rs.getString("CUSTOMER_NRIC_ID"));

				}

				if (rs.getString(14) == null) {

					TranRPT.setTerm("");

				} else

				{
					TranRPT.setTerm(rs.getString(14));

				}

				if (rs.getString(15) == null) {

					TranRPT.setCoverage("");

				} else

				{
					TranRPT.setCoverage(rs.getString(15));

				}

				if (rs.getString("PREMIUM_MODE") == null) {

					TranRPT.setPremiumMode("NO MODE");

				} else

				{

					if (rs.getString("PREMIUM_MODE").trim() != "") {

						String tmp = rs.getString("PREMIUM_MODE").substring(0, 1).toUpperCase()
								+ rs.getString("PREMIUM_MODE").substring(1);
						TranRPT.setPremiumMode(rs.getString("PREMIUM_MODE"));
						logger.info("Premium Mode" + rs.getString("PREMIUM_MODE") + "Testing Agent "
								+ "Premium Mode" + rs.getString("PREMIUM_MODE"));

					} else {

						TranRPT.setPremiumMode(rs.getString("PREMIUM_MODE"));

					}

				}

				if (rs.getString(17) == null) {

					TranRPT.setMPAY_RefNo("");

				} else

				{
					TranRPT.setMPAY_RefNo(rs.getString(17));

				}

				if (rs.getString(18) == null) {

					TranRPT.setMPAY_AuthCode("");

				} else

				{
					TranRPT.setMPAY_AuthCode(rs.getString(18));

				}

				if (rs.getString(19) == null) {

					TranRPT.setEBPG_RefNo("");

				} else

				{
					TranRPT.setEBPG_RefNo(rs.getString(19));

				}

				if (rs.getString(20) == null) {

					TranRPT.setEBPG_AuthCode("");

				} else

				{
					TranRPT.setEBPG_AuthCode(rs.getString(20));

				}

				if (rs.getString(21) == null) {

					TranRPT.setM2U_RefNo("");

				} else

				{
					TranRPT.setM2U_RefNo(rs.getString(21));

				}

				if (rs.getString(22) == null) {

					TranRPT.setM2U_AuthCode("");

				} else

				{
					TranRPT.setM2U_AuthCode(rs.getString(22));

				}

				if (rs.getString(23) == null) {

					TranRPT.setFPX_RefNo("");

				} else

				{
					TranRPT.setFPX_RefNo(rs.getString(23));

				}

				if (rs.getString(24) == null) {

					TranRPT.setFPX_AuthCode("");

				} else

				{
					TranRPT.setFPX_AuthCode(rs.getString(24));

				}

				TranRPT.setPaymentChannel("");
				if (TranRPT.getPaymentMethod().indexOf("CREDIT CARD") != -1) {

					TranRPT.setPaymentChannel("MPAY");

				} else if (TranRPT.getPaymentMethod().indexOf("EBPG") != -1) {

					TranRPT.setPaymentMethod("CREDIT CARD");
					TranRPT.setPaymentChannel("EBPG");
				}

				else if (TranRPT.getPaymentMethod().indexOf("FPX") != -1) {

					TranRPT.setPaymentMethod("ONLINE BANKING");
					TranRPT.setPaymentChannel("FPX");
				}

				else if (TranRPT.getPaymentMethod().indexOf("Maybank2U") != -1) {

					TranRPT.setPaymentMethod("DEBIT ACCOUNT");
					TranRPT.setPaymentChannel("M2U");
				}

				TranRPT.setRecordCount(Integer.parseInt(rs.getString(25)));
				TranRPT.setTotalAmount(rs.getString(26));

				if (rs.getString(27) == null) {

					TranRPT.setVehicleNo("");

				} else

				{
					TranRPT.setVehicleNo(rs.getString(27));
				}
				if (rs.getString(28) == null) {

					TranRPT.setCAPSPolicyNo("");

				} else

				{
					TranRPT.setCAPSPolicyNo(rs.getString(28));
				}

				// For HOHH Coverage -- Content, Building
				if (rs.getString(29) == null) {

					TranRPT.setHomeCoverage("");

				} else

				{
					TranRPT.setHomeCoverage(rs.getString(29));
				}
				if (rs.getString("AGENTNAMEWTC") != "" && rs.getString("AGENTNAMEWTC") != null) {

					TranRPT.setAgentName(rs.getString("AGENTNAMEWTC"));

				} else if (rs.getString("AGENTNAMETL") != "" && rs.getString("AGENTNAMETL") != null)

				{
					TranRPT.setAgentName(rs.getString("AGENTNAMETL"));
				} else if (rs.getString("AGENTNAMEMI") != "" && rs.getString("AGENTNAMEMI") != null)

				{
					TranRPT.setAgentName(rs.getString("AGENTNAMEMI"));
				} else if (rs.getString("AGENTNAMEHOHH") != "" && rs.getString("AGENTNAMEHOHH") != null)

				{
					TranRPT.setAgentName(rs.getString("AGENTNAMEHOHH"));
				} else {

					TranRPT.setAgentName("------");
				}
				if (rs.getString("AGENT_CODE") != "" && rs.getString("AGENT_CODE") != null) {

					TranRPT.setAgentCode(rs.getString("AGENT_CODE"));

				} else if (rs.getString("AGENTCODETL") != "" && rs.getString("AGENTCODETL") != null)

				{
					TranRPT.setAgentCode(rs.getString("AGENTCODETL"));
				} else if (rs.getString("AGENTCODEMI") != "" && rs.getString("AGENTCODEMI") != null)

				{
					TranRPT.setAgentCode(rs.getString("AGENTCODEMI"));
				} else if (rs.getString("AGENTCODEHOHH") != "" && rs.getString("AGENTCODEHOHH") != null)

				{
					TranRPT.setAgentCode(rs.getString("AGENTCODEHOHH"));
				} else {

					TranRPT.setAgentCode("------");
				}

				logger.info("  id=" + i + "    " + rs.getString(1) + "      " + rs.getString(2) + "      "
						+ rs.getString(3) + "        " + rs.getString(4) + "      " + rs.getString(5) + "     "
						+ rs.getString("AGENT_CODE") + "     " + rs.getString("AGENTCODETL") + "     "
						+ rs.getString("AGENTCODEMI") + "     " + rs.getString("AGENTCODEWTC") + "     "
						+ rs.getString("AGENTCODEHOHH"));
				i++;

				// totalAmount = totalAmount+Integer.parseInt(TranRPT.getPremiumAmount());

				// logger.info(TranRPT.getPremiumAmount());
				// TranRPT.setTotalAmount(totalAmount);

				list.add(TranRPT);

			}

			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	@Override
	public List<TransactionalReport> findByHohh(String productType, String productEntity, String status,
			String policyCertificateNo, String dateFrom, String dateTo, String NRIC, String receiptNo, int startRow,
			int endRow) {

		double totalAmount = 0.00;

		logger.info("productType " + productType);
		logger.info("status " + status);
		logger.info("agentCode1 " + NRIC);
		try {
			int i = startRow;
			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection
					.prepareCall("{CALL DSP_ADM_SP_TRANSACTION_RPT_S1(?,?,?,?,?,?,?,?,?,?,?)}");

			cstmt.setString(1, productType.trim());
			cstmt.setString(2, productEntity.trim());
			cstmt.setString(3, status);
			cstmt.setString(4, policyCertificateNo);
			cstmt.setString(5, dateFrom);
			cstmt.setString(6, dateTo);
			cstmt.setString(7, NRIC);
			cstmt.setString(8, receiptNo);
			cstmt.setInt(9, startRow);
			cstmt.setInt(10, endRow);
			cstmt.registerOutParameter(11, OracleTypes.CURSOR);

			cstmt.execute();

			List<TransactionalReport> list = new ArrayList<TransactionalReport>();
			rs = (ResultSet) cstmt.getObject(11);
			while (rs.next()) {

				TransactionalReport TranRPT = new TransactionalReport();
				TranRPT.setId(i);
				if (rs.getString(1) == null) {

					TranRPT.setDSPQQID("");
				} else {

					TranRPT.setDSPQQID(rs.getString(1));
				}

				if (rs.getString(2) == null) {

					TranRPT.setTransactionID("");
				} else

				{
					TranRPT.setTransactionID(rs.getString(2));

				}

				if (rs.getString(3) == null) {

					TranRPT.setPremiumAmount("");

				} else {
					TranRPT.setPremiumAmount(rs.getString(3));

				}

				if (rs.getString(4) == null) {

					TranRPT.setPaymentMethod("");

				} else

				{
					TranRPT.setPaymentMethod(rs.getString(4));

				}

				if (rs.getString(5) == null) {

					TranRPT.setStatus("");

				} else

				{
					TranRPT.setStatus(rs.getString(5));

				}

				if (rs.getString(6) == null) {

					TranRPT.setTransactionDate("");

				} else

				{
					TranRPT.setTransactionDate(rs.getString(6));

				}

				if (rs.getString(7) == null) {

					TranRPT.setInvoiceNo("");

				} else

				{
					TranRPT.setInvoiceNo(rs.getString(7));

				}

				if (rs.getString(8) == null) {

					TranRPT.setProductCode("");

				} else

				{
					TranRPT.setProductCode(rs.getString(8));

				}

				if (rs.getString(9) == null) {

					TranRPT.setProductType("");

				} else

				{
					TranRPT.setProductType(rs.getString(9));

				}

				if (rs.getString(10) == null) {

					TranRPT.setPolicyNo("");

				} else

				{
					TranRPT.setPolicyNo(rs.getString(10));

				}

				if (rs.getString(11) == null) {

					TranRPT.setCustomerName("");

				} else

				{
					TranRPT.setCustomerName(rs.getString(11));

				}

				if (rs.getString(12) == null) {

					TranRPT.setCustomerID("");

				} else

				{
					TranRPT.setCustomerID(rs.getString(12));

				}

				if (rs.getString(13) == null) {

					TranRPT.setCustomerNRIC("");

				} else

				{
					TranRPT.setCustomerNRIC(rs.getString(13));

				}

				if (rs.getString(14) == null) {

					TranRPT.setTerm("");

				} else

				{
					TranRPT.setTerm(rs.getString(14));

				}

				if (rs.getString(15) == null) {

					TranRPT.setCoverage("");

				} else

				{
					TranRPT.setCoverage(rs.getString(15));

				}

				if (rs.getString(16) == null) {

					TranRPT.setPremiumMode("");

				} else

				{

					if (rs.getString(17).trim() != "") {

						String tmp = rs.getString(16).substring(0, 1).toUpperCase() + rs.getString(16).substring(1);
						TranRPT.setPremiumMode(tmp);

					} else {

						TranRPT.setPremiumMode(rs.getString(17));

					}

				}

				if (rs.getString(17) == null) {

					TranRPT.setMPAY_RefNo("");

				} else

				{
					TranRPT.setMPAY_RefNo(rs.getString(17));

				}

				if (rs.getString(18) == null) {

					TranRPT.setMPAY_AuthCode("");

				} else

				{
					TranRPT.setMPAY_AuthCode(rs.getString(18));

				}

				if (rs.getString(19) == null) {

					TranRPT.setEBPG_RefNo("");

				} else

				{
					TranRPT.setEBPG_RefNo(rs.getString(19));

				}

				if (rs.getString(20) == null) {

					TranRPT.setEBPG_AuthCode("");

				} else

				{
					TranRPT.setEBPG_AuthCode(rs.getString(20));

				}

				if (rs.getString(21) == null) {

					TranRPT.setM2U_RefNo("");

				} else

				{
					TranRPT.setM2U_RefNo(rs.getString(21));

				}

				if (rs.getString(22) == null) {

					TranRPT.setM2U_AuthCode("");

				} else

				{
					TranRPT.setM2U_AuthCode(rs.getString(22));

				}

				if (rs.getString(23) == null) {

					TranRPT.setFPX_RefNo("");

				} else

				{
					TranRPT.setFPX_RefNo(rs.getString(23));

				}

				if (rs.getString(24) == null) {

					TranRPT.setFPX_AuthCode("");

				} else

				{
					TranRPT.setFPX_AuthCode(rs.getString(24));

				}

				TranRPT.setPaymentChannel("");
				if (TranRPT.getPaymentMethod().indexOf("CREDIT CARD") != -1) {

					TranRPT.setPaymentChannel("MPAY");

				} else if (TranRPT.getPaymentMethod().indexOf("EBPG") != -1) {

					TranRPT.setPaymentMethod("CREDIT CARD");
					TranRPT.setPaymentChannel("EBPG");
				}

				else if (TranRPT.getPaymentMethod().indexOf("FPX") != -1) {

					TranRPT.setPaymentMethod("ONLINE BANKING");
					TranRPT.setPaymentChannel("FPX");
				}

				else if (TranRPT.getPaymentMethod().indexOf("Maybank2U") != -1) {

					TranRPT.setPaymentMethod("DEBIT ACCOUNT");
					TranRPT.setPaymentChannel("M2U");
				}

				TranRPT.setRecordCount(Integer.parseInt(rs.getString(25)));
				TranRPT.setTotalAmount(rs.getString(26));

				if (rs.getString(27) == null) {

					TranRPT.setVehicleNo("");

				} else

				{
					TranRPT.setVehicleNo(rs.getString(27));
				}
				if (rs.getString(28) == null) {

					TranRPT.setCAPSPolicyNo("");

				} else

				{
					TranRPT.setCAPSPolicyNo(rs.getString(28));
				}

				// For HOHH Coverage -- Content, Building
				if (rs.getString(29) == null) {

					TranRPT.setHomeCoverage("");

				} else

				{
					TranRPT.setHomeCoverage(rs.getString(29));
				}

				if (rs.getString(40) == null) {

					TranRPT.setAgentCode("");

				} else

				{
					TranRPT.setAgentCode(rs.getString(40));
					logger.info("AGENT DAOIMPL-----before set---" + rs.getString(40));
				}

				logger.info("  id=" + i + "    " + rs.getString(1) + "      " + rs.getString(2) + "      "
						+ rs.getString(3) + "        " + rs.getString(4) + "      " + rs.getString(5));
				i++;

				// totalAmount = totalAmount+Integer.parseInt(TranRPT.getPremiumAmount());

				logger.info("AGENT DAOIMPL--------" + TranRPT.getAgentCode());
				// TranRPT.setTotalAmount(totalAmount);

				list.add(TranRPT);

			}

			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	@Override
	public List<TransactionalReport> rejectRecordMI(String productType,
			// String entityType,
			String status, String policyCertificateNo, String dateFrom, String dateTo, String NRIC, String receiptNo,
			int startRow, int endRow) {

		double totalAmount = 0.00;

		logger.info("productType " + productType);
		logger.info("status " + status);
		logger.info("agentCode2 " + NRIC);
		try {
			int i = startRow;
			connection = ConnectionFactory.getConnection();
			// CallableStatement cstmt = connection.prepareCall("{CALL
			// DSP_ADM_SP_RPT_TRANSACTIONAL_S(?,?,?,?,?,?,?,?,?,?)}");
			CallableStatement cstmt = connection
					.prepareCall("{CALL DSP_ADM_SP_TRANSACTION_RPT_R(?,?,?,?,?,?,?,?,?,?)}");
			// CallableStatement cstmt = connection.prepareCall("{CALL
			// DSP_ADM_SP_TRANSACTION_RPT_S6(?,?,?,?,?,?,?,?,?,?,?)}");
			cstmt.setString(1, productType.trim());
			cstmt.setString(2, status);
			cstmt.setString(3, policyCertificateNo);
			cstmt.setString(4, dateFrom);
			cstmt.setString(5, dateTo);
			cstmt.setString(6, NRIC);
			cstmt.setString(7, receiptNo);
			cstmt.setInt(8, startRow);
			cstmt.setInt(9, endRow);
			cstmt.registerOutParameter(10, OracleTypes.CURSOR);
			// cstmt.registerOutParameter(11, OracleTypes.CURSOR);
			cstmt.execute();

			List<TransactionalReport> list = new ArrayList<TransactionalReport>();
			rs = (ResultSet) cstmt.getObject(10);
			while (rs.next()) {

				TransactionalReport TranRPT = new TransactionalReport();
				TranRPT.setId(i);
				TranRPT.setDSPQQID("");
				TranRPT.setTransactionID("");

				if (rs.getString(7) == null) {

					TranRPT.setPremiumAmount("");

				} else {

					TranRPT.setPremiumAmount(rs.getString(7));
				}
				TranRPT.setPaymentMethod("");
				TranRPT.setStatus("R");

				if (rs.getString(4) == null) {

					TranRPT.setTransactionDate("");
				} else {
					TranRPT.setTransactionDate(rs.getString(4));

				}

				TranRPT.setInvoiceNo("");
				TranRPT.setProductCode("");
				TranRPT.setProductType("MOTOR INSURANCE");
				TranRPT.setPolicyNo("");

				if (rs.getString(5) == null) {

					TranRPT.setCustomerName("");
				} else {
					TranRPT.setCustomerName(rs.getString(5));

				}

				if (rs.getString(2) == null) {

					TranRPT.setCustomerID("");
				} else {
					TranRPT.setCustomerID(rs.getString(2));

				}

				if (rs.getString(6) == null) {

					TranRPT.setCustomerNRIC("");
				} else {
					TranRPT.setCustomerNRIC(rs.getString(6));

				}

				TranRPT.setTerm("");
				TranRPT.setCoverage("");
				TranRPT.setPremiumMode("");

				TranRPT.setMPAY_RefNo("");
				TranRPT.setMPAY_AuthCode("");

				TranRPT.setEBPG_RefNo("");
				TranRPT.setEBPG_AuthCode("");

				TranRPT.setM2U_RefNo("");
				TranRPT.setM2U_AuthCode("");

				TranRPT.setFPX_RefNo("");
				TranRPT.setFPX_AuthCode("");

				TranRPT.setPaymentChannel("");
				if (TranRPT.getPaymentMethod().indexOf("CREDIT CARD") != -1) {

					TranRPT.setPaymentChannel("MPAY");

				} else if (TranRPT.getPaymentMethod().indexOf("EBPG") != -1) {

					TranRPT.setPaymentMethod("CREDIT CARD");
					TranRPT.setPaymentChannel("EBPG");
				}

				else if (TranRPT.getPaymentMethod().indexOf("FPX") != -1) {

					TranRPT.setPaymentMethod("ONLINE BANKING");
					TranRPT.setPaymentChannel("FPX");
				}

				else if (TranRPT.getPaymentMethod().indexOf("Maybank2U") != -1) {

					TranRPT.setPaymentMethod("DEBIT ACCOUNT");
					TranRPT.setPaymentChannel("M2U");
				}

				TranRPT.setRecordCount(Integer.parseInt(rs.getString(8)));
				TranRPT.setTotalAmount(rs.getString(9));

				if (rs.getString(3) == null) {

					TranRPT.setReason("");
				} else {
					TranRPT.setReason(rs.getString(3));

				}

				// logger.info(" id="+i+" "+rs.getString(1)+" "+rs.getString(2)+"
				// "+rs.getString(3)+" "+rs.getString(4)+" "+rs.getString(5));
				i++;

				// totalAmount = totalAmount+Integer.parseInt(TranRPT.getPremiumAmount());

				// logger.info(TranRPT.getPremiumAmount());
				// TranRPT.setTotalAmount(totalAmount);

				list.add(TranRPT);

			}

			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	@Override
	public List<TransactionalReport> incompleteRecordMI(String productType,
			// String entityType,
			String status, String policyCertificateNo, String dateFrom, String dateTo, String NRIC, String receiptNo,
			int startRow, int endRow) {

		double totalAmount = 0.00;

		logger.info("productType " + productType);
		logger.info("status " + status);
		logger.info("agentCode3 " + NRIC);
		try {
			int i = startRow;
			connection = ConnectionFactory.getConnection();
			// CallableStatement cstmt = connection.prepareCall("{CALL
			// DSP_ADM_SP_RPT_TRANSACTIONAL_S(?,?,?,?,?,?,?,?,?,?)}");
			CallableStatement cstmt = connection
					.prepareCall("{CALL DSP_ADM_SP_TRANSACTION_RPT_O(?,?,?,?,?,?,?,?,?,?)}");
			// CallableStatement cstmt = connection.prepareCall("{CALL
			// DSP_ADM_SP_TRANSACTION_RPT_S6(?,?,?,?,?,?,?,?,?,?,?)}");
			cstmt.setString(1, productType.trim());
			cstmt.setString(2, status);
			cstmt.setString(3, policyCertificateNo);
			cstmt.setString(4, dateFrom);
			cstmt.setString(5, dateTo);
			cstmt.setString(6, NRIC);
			cstmt.setString(7, receiptNo);
			cstmt.setInt(8, startRow);
			cstmt.setInt(9, endRow);
			cstmt.registerOutParameter(10, OracleTypes.CURSOR);
			// cstmt.registerOutParameter(11, OracleTypes.CURSOR);
			cstmt.execute();

			List<TransactionalReport> list = new ArrayList<TransactionalReport>();
			rs = (ResultSet) cstmt.getObject(10);
			while (rs.next()) {

				TransactionalReport TranRPT = new TransactionalReport();
				TranRPT.setId(i);
				TranRPT.setDSPQQID(rs.getString(2));
				TranRPT.setTransactionID("");

				if (rs.getString(8) == null) {

					TranRPT.setPremiumAmount("");

				} else {

					TranRPT.setPremiumAmount(rs.getString(8));
				}
				TranRPT.setPaymentMethod("");

				if (rs.getString(9) == null) {

					TranRPT.setStatus("");
				} else {
					TranRPT.setStatus(rs.getString(9));

				}

				if (rs.getString(7) == null) {

					TranRPT.setTransactionDate("");
				} else {
					TranRPT.setTransactionDate(rs.getString(7));

				}

				TranRPT.setInvoiceNo("");
				TranRPT.setProductCode("");
				TranRPT.setProductType("MOTOR INSURANCE");
				TranRPT.setPolicyNo("");

				if (rs.getString(5) == null) {

					TranRPT.setCustomerName("");
				} else {
					TranRPT.setCustomerName(rs.getString(5));

				}

				if (rs.getString(2) == null) {

					TranRPT.setCustomerID("");
				} else {
					TranRPT.setCustomerID(rs.getString(2));

				}

				if (rs.getString(6) == null) {

					TranRPT.setCustomerNRIC("");
				} else {
					TranRPT.setCustomerNRIC(rs.getString(6));

				}

				TranRPT.setTerm("");
				TranRPT.setCoverage("");
				TranRPT.setPremiumMode("");

				TranRPT.setMPAY_RefNo("");
				TranRPT.setMPAY_AuthCode("");

				TranRPT.setEBPG_RefNo("");
				TranRPT.setEBPG_AuthCode("");

				TranRPT.setM2U_RefNo("");
				TranRPT.setM2U_AuthCode("");

				TranRPT.setFPX_RefNo("");
				TranRPT.setFPX_AuthCode("");

				TranRPT.setPaymentChannel("");
				if (TranRPT.getPaymentMethod().indexOf("CREDIT CARD") != -1) {

					TranRPT.setPaymentChannel("MPAY");

				} else if (TranRPT.getPaymentMethod().indexOf("EBPG") != -1) {

					TranRPT.setPaymentMethod("CREDIT CARD");
					TranRPT.setPaymentChannel("EBPG");
				}

				else if (TranRPT.getPaymentMethod().indexOf("FPX") != -1) {

					TranRPT.setPaymentMethod("ONLINE BANKING");
					TranRPT.setPaymentChannel("FPX");
				}

				else if (TranRPT.getPaymentMethod().indexOf("Maybank2U") != -1) {

					TranRPT.setPaymentMethod("DEBIT ACCOUNT");
					TranRPT.setPaymentChannel("M2U");
				}

				TranRPT.setRecordCount(Integer.parseInt(rs.getString(10)));
				TranRPT.setTotalAmount(rs.getString(11));

				if (rs.getString(3) == null) {

					TranRPT.setVehicleNo("");
				} else {
					TranRPT.setVehicleNo(rs.getString(3));

				}

				// logger.info(" id="+i+" "+rs.getString(1)+" "+rs.getString(2)+"
				// "+rs.getString(3)+" "+rs.getString(4)+" "+rs.getString(5));
				i++;

				// totalAmount = totalAmount+Integer.parseInt(TranRPT.getPremiumAmount());

				// logger.info(TranRPT.getPremiumAmount());
				// TranRPT.setTotalAmount(totalAmount);

				list.add(TranRPT);

			}

			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	@Override
	public List<TransactionalReport> rejectRecordTL(String productType,
			// String productEntity,
			String status, String policyCertificateNo, String dateFrom, String dateTo, String NRIC, String receiptNo,
			int startRow, int endRow) {

		double totalAmount = 0.00;

		logger.info("productType " + productType);
		logger.info("status " + status);
		logger.info("agentCode " + NRIC);
		try {
			int i = startRow;
			connection = ConnectionFactory.getConnection();
			// CallableStatement cstmt = connection.prepareCall("{CALL
			// DSP_ADM_SP_RPT_TRANSACTIONAL_S(?,?,?,?,?,?,?,?,?,?)}");
			CallableStatement cstmt = connection
					.prepareCall("{CALL DSP_ADM_SP_TRANSACTION_RPT_TR(?,?,?,?,?,?,?,?,?,?)}");
			// CallableStatement cstmt = connection.prepareCall("{CALL
			// DSP_ADM_SP_TRANSACTION_RPT_S6(?,?,?,?,?,?,?,?,?,?,?)}");
			cstmt.setString(1, productType.trim());
			cstmt.setString(2, status);
			cstmt.setString(3, policyCertificateNo);
			cstmt.setString(4, dateFrom);
			cstmt.setString(5, dateTo);
			cstmt.setString(6, NRIC);
			cstmt.setString(7, receiptNo);
			cstmt.setInt(8, startRow);
			cstmt.setInt(9, endRow);
			cstmt.registerOutParameter(10, OracleTypes.CURSOR);
			// cstmt.registerOutParameter(11, OracleTypes.CURSOR);
			cstmt.execute();

			List<TransactionalReport> list = new ArrayList<TransactionalReport>();
			rs = (ResultSet) cstmt.getObject(10);
			while (rs.next()) {

				TransactionalReport TranRPT = new TransactionalReport();
				TranRPT.setId(i);
				TranRPT.setDSPQQID(rs.getString(2));
				TranRPT.setTransactionID("");

				if (rs.getString(10) == null) {

					TranRPT.setPremiumAmount("");

				} else {

					TranRPT.setPremiumAmount(rs.getString(10));
				}
				TranRPT.setPaymentMethod("");

				if (rs.getString(3) == null) {

					TranRPT.setStatus("");

				} else {

					TranRPT.setStatus(rs.getString(3));
				}

				if (rs.getString(9) == null) {

					TranRPT.setTransactionDate("");

				} else {

					TranRPT.setTransactionDate(rs.getString(9));
				}

				TranRPT.setInvoiceNo("");
				TranRPT.setProductCode("");
				TranRPT.setProductType("EZY LIFE");
				TranRPT.setPolicyNo("");

				if (rs.getString(5) == null) {

					TranRPT.setCustomerName("");

				} else {

					TranRPT.setCustomerName(rs.getString(5));
				}

				if (rs.getString(6) == null) {

					TranRPT.setCustomerID("");

				} else {

					TranRPT.setCustomerID(rs.getString(6));
				}

				if (rs.getString(8) == null) {

					TranRPT.setCustomerNRIC("");

				} else {

					TranRPT.setCustomerNRIC(rs.getString(8));
				}

				TranRPT.setTerm("");
				TranRPT.setCoverage("");
				TranRPT.setPremiumMode("");

				TranRPT.setMPAY_RefNo("");
				TranRPT.setMPAY_AuthCode("");

				TranRPT.setEBPG_RefNo("");
				TranRPT.setEBPG_AuthCode("");

				TranRPT.setM2U_RefNo("");
				TranRPT.setM2U_AuthCode("");

				TranRPT.setFPX_RefNo("");
				TranRPT.setFPX_AuthCode("");

				TranRPT.setPaymentChannel("");
				if (TranRPT.getPaymentMethod().indexOf("CREDIT CARD") != -1) {

					TranRPT.setPaymentChannel("MPAY");

				} else if (TranRPT.getPaymentMethod().indexOf("EBPG") != -1) {

					TranRPT.setPaymentMethod("CREDIT CARD");
					TranRPT.setPaymentChannel("EBPG");
				}

				else if (TranRPT.getPaymentMethod().indexOf("FPX") != -1) {

					TranRPT.setPaymentMethod("ONLINE BANKING");
					TranRPT.setPaymentChannel("FPX");
				}

				else if (TranRPT.getPaymentMethod().indexOf("Maybank2U") != -1) {

					TranRPT.setPaymentMethod("DEBIT ACCOUNT");
					TranRPT.setPaymentChannel("M2U");
				}

				TranRPT.setRecordCount(Integer.parseInt(rs.getString(11)));
				TranRPT.setTotalAmount(rs.getString(12));

				if (rs.getString(4) == null) {

					TranRPT.setReason("");

				} else {

					TranRPT.setReason(rs.getString(4));
				}

				if (rs.getString(13) == null) {

					TranRPT.setUWReason("");

				} else {

					TranRPT.setUWReason(rs.getString(13));
				}
				// logger.info(" id="+i+" "+rs.getString(1)+" "+rs.getString(2)+"
				// "+rs.getString(3)+" "+rs.getString(4)+" "+rs.getString(5));
				i++;

				// totalAmount = totalAmount+Integer.parseInt(TranRPT.getPremiumAmount());

				// logger.info(TranRPT.getPremiumAmount());
				// TranRPT.setTotalAmount(totalAmount);

				list.add(TranRPT);

			}

			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

	@Override
	public List<TransactionalReport> incompleteRecordTL(String productType,
			// String entityType,
			String status, String policyCertificateNo, String dateFrom, String dateTo, String NRIC, String receiptNo,
			int startRow, int endRow) {

		double totalAmount = 0.00;

		logger.info("productType " + productType);
		logger.info("status " + status);
		logger.info("agentCode4 " + NRIC);
		try {
			int i = startRow;
			connection = ConnectionFactory.getConnection();
			// CallableStatement cstmt = connection.prepareCall("{CALL
			// DSP_ADM_SP_RPT_TRANSACTIONAL_S(?,?,?,?,?,?,?,?,?,?)}");
			CallableStatement cstmt = connection
					.prepareCall("{CALL DSP_ADM_SP_TRANSACTION_RPT_TO1(?,?,?,?,?,?,?,?,?,?)}");
			// CallableStatement cstmt = connection.prepareCall("{CALL
			// DSP_ADM_SP_TRANSACTION_RPT_S6(?,?,?,?,?,?,?,?,?,?,?)}");
			cstmt.setString(1, productType.trim());
			cstmt.setString(2, status);
			cstmt.setString(3, policyCertificateNo);
			cstmt.setString(4, dateFrom);
			cstmt.setString(5, dateTo);
			cstmt.setString(6, NRIC);
			cstmt.setString(7, receiptNo);
			cstmt.setInt(8, startRow);
			cstmt.setInt(9, endRow);
			cstmt.registerOutParameter(10, OracleTypes.CURSOR);
			// cstmt.registerOutParameter(11, OracleTypes.CURSOR);
			cstmt.execute();

			List<TransactionalReport> list = new ArrayList<TransactionalReport>();
			rs = (ResultSet) cstmt.getObject(10);
			while (rs.next()) {

				TransactionalReport TranRPT = new TransactionalReport();
				TranRPT.setId(i);
				TranRPT.setDSPQQID(rs.getString(2));
				TranRPT.setTransactionID("");

				if (rs.getString(10) == null) {

					TranRPT.setPremiumAmount("");

				} else {

					TranRPT.setPremiumAmount(rs.getString(10));
				}
				TranRPT.setPaymentMethod("");
				TranRPT.setStatus(rs.getString(3));
				TranRPT.setTransactionDate(rs.getString(9));
				TranRPT.setInvoiceNo("");
				TranRPT.setProductCode("");
				TranRPT.setProductType("EZY LIFE");
				TranRPT.setPolicyNo("");

				if (rs.getString(5) == null) {

					TranRPT.setCustomerName("");

				} else {

					TranRPT.setCustomerName(rs.getString(5));
				}

				if (rs.getString(6) == null) {

					TranRPT.setCustomerID("");

				} else {

					TranRPT.setCustomerID(rs.getString(6));
				}

				if (rs.getString(8) == null) {

					TranRPT.setCustomerNRIC("");

				} else {

					TranRPT.setCustomerNRIC(rs.getString(8));
				}

				TranRPT.setTerm("");
				TranRPT.setCoverage("");
				TranRPT.setPremiumMode("");

				TranRPT.setMPAY_RefNo("");
				TranRPT.setMPAY_AuthCode("");

				TranRPT.setEBPG_RefNo("");
				TranRPT.setEBPG_AuthCode("");

				TranRPT.setM2U_RefNo("");
				TranRPT.setM2U_AuthCode("");

				TranRPT.setFPX_RefNo("");
				TranRPT.setFPX_AuthCode("");

				TranRPT.setPaymentChannel("");
				if (TranRPT.getPaymentMethod().indexOf("CREDIT CARD") != -1) {

					TranRPT.setPaymentChannel("MPAY");

				} else if (TranRPT.getPaymentMethod().indexOf("EBPG") != -1) {

					TranRPT.setPaymentMethod("CREDIT CARD");
					TranRPT.setPaymentChannel("EBPG");
				}

				else if (TranRPT.getPaymentMethod().indexOf("FPX") != -1) {

					TranRPT.setPaymentMethod("ONLINE BANKING");
					TranRPT.setPaymentChannel("FPX");
				}

				else if (TranRPT.getPaymentMethod().indexOf("Maybank2U") != -1) {

					TranRPT.setPaymentMethod("DEBIT ACCOUNT");
					TranRPT.setPaymentChannel("M2U");
				}

				TranRPT.setRecordCount(Integer.parseInt(rs.getString(11)));
				TranRPT.setTotalAmount(rs.getString(12));

				TranRPT.setDoneStep1(rs.getString(13));
				TranRPT.setDoneStep2(rs.getString(14));

				// logger.info(" id="+i+" "+rs.getString(1)+" "+rs.getString(2)+"
				// "+rs.getString(3)+" "+rs.getString(4)+" "+rs.getString(5));
				i++;

				// totalAmount = totalAmount+Integer.parseInt(TranRPT.getPremiumAmount());

				// logger.info(TranRPT.getPremiumAmount());
				// TranRPT.setTotalAmount(totalAmount);

				list.add(TranRPT);

			}

			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
	}

}
