package com.etiqa.DAO;

import java.util.List;

import com.spring.VO.SalesLead;

public interface LeadsAbdDAO {

	public List<SalesLead> getLeadsAbdSearchRecords(SalesLead salesLead);

}
