package com.etiqa.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.etiqa.DAO.Audit_Integration_DAO;
import com.spring.VO.Audit_Integration_Bean;

@WebServlet("/getAuditIntegReport")
public class Audit_Integration_Controller extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// it will fire on a POST request (like submitting the form using POST
		// method)

		String fromdate = request.getParameter("fromdate");
		String todate = request.getParameter("todate");

		request.setAttribute("fromdate", fromdate);
		System.out.println("fromdt Controler : " + fromdate);

		request.setAttribute("todate", todate);
		System.out.println("todate Controler : " + todate);

		List<Audit_Integration_Bean> myDataList = new Audit_Integration_DAO().getReportData(fromdate, todate);
		request.setAttribute("myDataList", myDataList);
		request.setAttribute("fromdate", fromdate);
		request.setAttribute("todate", todate);
		request.getRequestDispatcher("AuditIntegrationReport.jsp").forward(request, response);
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("AuditIntegrationReport.jsp").forward(request, response);

	}

}
