package com.etiqa.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.log4j.Logger;

import com.etiqa.DAO.AgentDAO;
import com.etiqa.DAO.AgentDAOImpl;
import com.etiqa.common.BaseServlet;
import com.etiqa.model.agent.Agent;
import com.etiqa.model.agent.AgentProduct;
import com.etiqa.model.agent.AgentRegistrationDocument;
import com.spring.admin.DashBoardController;
import com.spring.uam.ldap.LDAPAttributesBean;
import com.spring.uam.ldap.RegCPF;

@MultipartConfig(maxFileSize = 16177215)
@WebServlet("/agent")
public class AgentController extends BaseServlet {
	final static Logger logger = Logger.getLogger(AgentController.class);

	private static final long serialVersionUID = 1L;
	private static SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
	private static final String LOGO_PATH = "C:\\agent_logo";

	@Override
	public void handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter(ACTION);
		logger.info(">>> action: " + "[" + action + "]");
		if (action.equals(ACTION_EVENT_LISTALL)) {
			listAll(request, response);
		} else if (action.equals(ACTION_EVENT_LIST)) {
			list(request, response);
		} else if (action.equals(ACTION_EVENT_SAVE)) {
			save(request, response);
		} else if (action.equals(ACTION_EVENT_UPDATE)) {
			update(request, response);
		} else if (action.equals(ACTION_EVENT_DELETE)) {
			delete(request, response);
		} else if (action.equals(ACTION_EVENT_RECORD)) {
			record(request, response);
		} else if (action.equals(ACTION_EVENT_CHANGEPW)) {
			changepassword(request, response);
		} else {
			throw new RuntimeException("Unknown Action");
		}
	}

	// for Agent Profile View
	private void detail(HttpServletRequest request, HttpServletResponse response) {

		int ID = Integer.parseInt(request.getParameter("id").trim());
		logger.info(ID);

		AgentDAO dao = new AgentDAOImpl();// daoFactory.getAgentDAO();
		List<Agent> agentDetail = dao.getAgentDetails(ID);
		request.setAttribute("data", agentDetail);
		forward(request, response, "agent-view-profile.jsp");
	}

	// For Updating Agent Password
	private void changepassword(HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		String agentUserid = "";
		String curpassword = "";
		String newpassword = "";
		if (session.getAttribute("agentUserid") != null) {
			agentUserid = (String) session.getAttribute("agentUserid");
		} else {
			agentUserid = "88888";
		}
		if (request.getMethod().equals("POST")) {
			curpassword = request.getParameter("curpassword").trim();
			newpassword = request.getParameter("newpassword").trim();
		}
		logger.info("calling Ad Creating User " + agentUserid);
		LDAPAttributesBean ldap = RegCPF.ADChangePassword(agentUserid, curpassword, newpassword);
		logger.info(ldap.getValidated() + "validateing USer ");
		logger.info(ldap.getError_code() + " Error code ");
		if (ldap.getValidated().equals(1)) {
			request.setAttribute("data", "Update Password Successfully");
		} else {
			request.setAttribute("data", ldap.getError_msg());
		}
		forward(request, response, "agent-changepassword.jsp");
	}

	private void listAll(HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		String agentName = null;
		String agentCode = null;
		String agentCategory = null;
		String insuranceType = null;
		String status = null;
		String dateFrom = null;
		String dateTo = null;

		if (request.getMethod().equals("POST"))

		{
			logger.info(agentName + "agentName");
			logger.info("agentName"+agentName);
			session.setAttribute("agentName", request.getParameter("agentName").trim());

			// session.setAttribute("agentCode", request.getParameter("agentCode").trim());
			session.setAttribute("agentCategory", request.getParameter("agentCategory").trim());
			session.setAttribute("insuranceType", request.getParameter("insuranceType").trim());
			session.setAttribute("status", request.getParameter("status").trim());
			session.setAttribute("dateFrom", request.getParameter("dateFrom").trim());
			session.setAttribute("dateTo", request.getParameter("dateTo").trim());
		}

		agentName = (String) session.getAttribute("agentName");
		// agentCode = (String) session.getAttribute("agentCode");
		agentCategory = (String) session.getAttribute("agentCategory");
		insuranceType = (String) session.getAttribute("insuranceType");
		status = (String) session.getAttribute("status");
		dateFrom = (String) session.getAttribute("dateFrom");
		dateTo = (String) session.getAttribute("dateTo");
logger.info("&&&&&&&&&&&&&&&agentName"+agentName);
		AgentDAO dao = new AgentDAOImpl();// daoFactory.getAgentDAO();
		List<Agent> agent = dao.searchByList(agentName, agentCode, agentCategory, insuranceType, status, dateFrom,
				dateTo);

		// StartRow,
		// EndRow);
		request.setAttribute("data", agent);
		forward(request, response, "agent-view-list-agent.jsp");
	}

	// List All Cyber Agents
	private void list(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		AgentDAO dao = new AgentDAOImpl();// daoFactory.getAgentDAO();
		List<Agent> agents = dao.findAll();
		request.setAttribute("data", agents);
		forward(request, response, "agent-view-list-agent.jsp");
	}

	private void record(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		AgentDAO dao = new AgentDAOImpl();// daoFactory.getAgentDAO();
		long ID = Long.parseLong(request.getParameter("id").trim());
		logger.info("Getting value : " + ID);
		Agent agents = dao.findById(ID);
		request.setAttribute("dataagent", agents);
		forward(request, response, "agent-view-list-agent-edit.jsp");
	}

	// Save New Cyber Agent
	private void save(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AgentDAO dao = new AgentDAOImpl();// daoFactory.getAgentDAO();

		Agent agent = new Agent();

		agent.setReferenceNo(request.getParameter("referenceNo"));
		agent.setAgentCode(request.getParameter("agentCode"));

		String d1 = request.getParameter("registrationDate");
		SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-yy");
		Date date = null;
		try {
			date = format1.parse(d1);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		logger.info(format2.format(date));
		logger.info("++++++++++++++agentname"+agent.getAgentName());
		logger.info("++++++++++++agentcode"+agent.getAgentCode());

		agent.setRegistrationDate(format2.format(date));
		logger.info(format2.format(date));
		// agent.setRegistrationDate(request.getParameter("registrationDate"));
		agent.setAnnualSalesTarget(Double.parseDouble(request.getParameter("annualSalesTarget")));
		agent.setAgentCategory(request.getParameter("agentCategory"));
		agent.setUserId(request.getParameter("userId"));
		agent.setAgentStatus(request.getParameter("agentStatus"));
		agent.setAgentAliasName(request.getParameter("agentAliasName"));
		agent.setPassword(request.getParameter("password"));
		agent.setAgentType(request.getParameter("agentType"));
		// agent.setCommission(request.getParameter("agentCommission"));
		// agent.setDiscount(request.getParameter("agentDiscount"));
		agent.setAgentName(request.getParameter("agentName"));
		agent.setIdType(request.getParameter("idType"));
		agent.setIdNo(request.getParameter("idNo"));
		agent.setPhoneType(request.getParameter("phoneType"));
		agent.setPhoneNO(request.getParameter("phoneNO"));
		agent.setEmail(request.getParameter("email"));
		agent.setAddress1(request.getParameter("address1"));
		// agent.setAddress2(request.getParameter("address2"));
		// agent.setAddress3(request.getParameter("address3"));
		agent.setPostcode(request.getParameter("postcode"));
		agent.setCity(request.getParameter("city"));
		agent.setState(request.getParameter("state"));
		agent.setCountry(request.getParameter("country"));
		// Creating New USER
		logger.info("calling Ad Creating User " + agent.getUserId() + " " + agent.getPassword());
		LDAPAttributesBean ldap = RegCPF.ADUserCreate(agent.getUserId(), agent.getPassword());
		logger.info(ldap.getValidated() + "validateing USer ");
		logger.info(ldap.getError_code() + " Error code ");
		String[] regDocs = request.getParameterValues("registrationDocument");
		if (regDocs != null) {
			List<AgentRegistrationDocument> agentDocuments = new ArrayList<AgentRegistrationDocument>(regDocs.length);
			for (String document : regDocs) {
				AgentRegistrationDocument agentDocument = new AgentRegistrationDocument();
				agentDocument.setAgentCode(agent.getAgentCode());
				agentDocument.setDocumentCode(document);
				agentDocuments.add(agentDocument);
			}

			agent.setAgentDocuments(agentDocuments);
		}

		// For Agent Logo TEST CODE1
		/*
		 * try{
		 *
		 * Part filePart = request.getPart("agentLogo");
		 *
		 * String s = agent.getAgentName(); agent.setAgentLogo(s);
		 *
		 * // String path="C:\\agent_logo"; String path =
		 * "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/configdsp/AgentLogos";
		 *
		 * File file=new File(path+"/"+s); if(!file.exists()){ file.mkdir(); }
		 *
		 *
		 * //file.createNewFile(); String fileName = getFileName(filePart);
		 *
		 * OutputStream out = null;
		 *
		 * InputStream filecontent = null;
		 *
		 * // PrintWriter writer = response.getWriter(); try { out = new
		 * FileOutputStream(new File(path + File.separator+s+File.separator +
		 * (s+"."+fileName.substring(fileName.lastIndexOf(".")+1))));
		 *
		 * filecontent = filePart.getInputStream();
		 *
		 *
		 * int read = 0; final byte[] bytes = new byte[1024];
		 *
		 * while ((read = filecontent.read(bytes)) != -1) { out.write(bytes, 0, read);
		 *
		 * s=path+"/"+fileName;
		 *
		 * } out.close(); filecontent.close(); }
		 *
		 * catch(Exception e) { e.printStackTrace(); }
		 *
		 * }catch(Exception e){ // e.pr }
		 */

		if (ldap.getValidated().equals(1)) {
			dao.save(agent);
			request.setAttribute("message", "New agent added!");
		} else {
			request.setAttribute("message", "New agent is not added!");
		}
		list(request, response);

	}

	private String getFileName(final Part part) {
		final String partHeader = part.getHeader("content-disposition");

		for (String content : part.getHeader("content-disposition").split(";")) {
			if (content.trim().startsWith("filename")) {
				return content.substring(content.indexOf('=') + 1).trim().replace("\"", "");
			}
		}
		return null;
	}

	private String extractFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] items = contentDisp.split(";");
		for (String s : items) {
			if (s.trim().startsWith("filename")) {
				return s.substring(s.indexOf("=") + 2, s.length() - 1);
			}
		}
		return "";
	}

	private void update(HttpServletRequest request, HttpServletResponse response) {
		AgentDAO dao = new AgentDAOImpl();// daoFactory.getAgentDAO();
		Agent agent = new Agent();
		String id = request.getParameter("id");
		String referenceNo = request.getParameter("referenceNo");

		String agentCode = request.getParameter("agentCode");

		// / Agent agent=dao.findById(Long.valueOf(id));
		String d1 = request.getParameter("registrationDate");
		agent.setRegistrationDate(d1);
		// logger.info(format2.format(date));

		// String d1=request.getParameter("registrationDate");

		// logger.info(format2.format(date));
		logger.info("I am calling inside function update");
		Double annualSalesTarget = Double.parseDouble(request.getParameter("annualSalesTarget"));

		String agentCategory = request.getParameter("agentCategory");
		String insuranceType = request.getParameter("insuranceType");
		String agentStatus = request.getParameter("agentStatus");
		String agentAliasName = request.getParameter("agentAliasName");
		String agentType = request.getParameter("agentType");
		String agentCommission = request.getParameter("agentCommission");
		String agentDiscount = request.getParameter("agentDiscount");
		String agentName = request.getParameter("agentName");
		String idType = request.getParameter("idType");
		String idNo = request.getParameter("idNo");
		String phoneType = request.getParameter("phoneType");
		String phoneNO = request.getParameter("phoneNO");
		String email = request.getParameter("email");
		String address1 = request.getParameter("address1");
		// String address2 = request.getParameter("address2");
		// String address3 = request.getParameter("address3");

		String postcode = request.getParameter("postcode");
		String city = request.getParameter("city");
		String state = request.getParameter("state");
		String country = request.getParameter("country");

		Agent ag = dao.findById(Long.valueOf(id));

		ag.setAgentCode(agentCode);
		ag.setReferenceNo(referenceNo);
		logger.info("I am calling after reference");
		ag.setAnnualSalesTarget(annualSalesTarget);
		ag.setAgentCategory(agentCategory);
		ag.setInsuranceType(insuranceType);
		ag.setAgentStatus(agentStatus);
		ag.setAgentAliasName(agentAliasName);

		ag.setAgentType(agentType);
		ag.setCommission(agentCommission);
		ag.setDiscount(agentDiscount);
		ag.setAgentName(agentName);
		ag.setIdType(idType);
		ag.setIdNo(idNo);
		ag.setPhoneNO(phoneNO);
		ag.setPhoneType(phoneType);
		ag.setEmail(email);
		ag.setAddress1(address1);
		// ag.setAddress2(address2);
		// ag.setAddress3(address3);
		ag.setPostcode(postcode);
		ag.setCity(city);
		ag.setState(state);
		ag.setCountry(country);
		ag.setIdNo(idNo);

		String[] results = request.getParameterValues("products");
		if (results != null) {
			List<AgentProduct> agentProducts = new ArrayList<AgentProduct>(results.length);
			for (String product : results) {
				AgentProduct agentProduct = new AgentProduct();
				agentProduct.setAgentCode(agent.getAgentCode());
				agentProduct.setProductCode(product);
				agentProducts.add(agentProduct);
			}

			agent.setAgentProdcuts(agentProducts);
			logger.info("++++++++++++++++++++++++ etiqa/agentcontroller/agentProducts"+agentProducts.get(0).getAgentCode());
			logger.info("++++++++++++++++++++++++ etiqa/agentcontroller/agentProducts"+agentProducts.get(0).getProductCode());
			
			
			
		}

		String[] regDocs = request.getParameterValues("registrationDocument");
		if (regDocs != null) {
			List<AgentRegistrationDocument> agentDocuments = new ArrayList<AgentRegistrationDocument>(regDocs.length);
			for (String document : regDocs) {
				AgentRegistrationDocument agentDocument = new AgentRegistrationDocument();
				agentDocument.setAgentCode(agent.getAgentCode());
				agentDocument.setDocumentCode(document);
				agentDocuments.add(agentDocument);
			}

			agent.setAgentDocuments(agentDocuments);
			logger.info("+++++++++++++++++++++++++++++++agentDocuments"+agentDocuments.get(0).getAgentCode());
			
		}

		// For Agent Logo TEST CODE1
		/*
		 * try{
		 *
		 * Part filePart = request.getPart("agentLogo");
		 *
		 * String s = agent.getAgentCode(); agent.setAgentLogo(s);
		 *
		 * // String path="C:\\agent_logo"; String path =
		 * "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/configdsp/AgentLogos";
		 *
		 * File file=new File(path+"/"+s); if(!file.exists()){ file.mkdir(); }
		 *
		 *
		 * //file.createNewFile(); String fileName = getFileName(filePart);
		 *
		 * OutputStream out = null;
		 *
		 * InputStream filecontent = null;
		 *
		 * // PrintWriter writer = response.getWriter(); try { out = new
		 * FileOutputStream(new File(path + File.separator+s+File.separator +
		 * (s+"."+fileName.substring(fileName.lastIndexOf(".")+1))));
		 *
		 * filecontent = filePart.getInputStream();
		 *
		 *
		 * int read = 0; final byte[] bytes = new byte[1024];
		 *
		 * while ((read = filecontent.read(bytes)) != -1) { out.write(bytes, 0, read);
		 *
		 * s=path+"/"+fileName;
		 *
		 * } out.close(); filecontent.close(); }
		 *
		 * catch(Exception e) { e.printStackTrace(); }
		 *
		 * }catch(Exception e){ // e.pr }
		 */

		dao.update(ag);
		// request.setAttribute("data", agent);
		request.setAttribute("message", "Update Success!");

		list(request, response);

		// forward(request, response, "agent-view-list-agent-edit.jsp");
	}

	// Delete-DeActivate Cyber Agent
	private void delete(HttpServletRequest request, HttpServletResponse response) {
		AgentDAO dao = new AgentDAOImpl();// daoFactory.getAgentDAO();

		String id = request.getParameter("id");

		Agent agent = dao.findById(Long.valueOf(id));
		dao.delete(agent);

		request.setAttribute("message", " records deleted!");

		list(request, response);
	}

	private static String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}

}
