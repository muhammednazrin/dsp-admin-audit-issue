package com.etiqa.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.spring.uam.ldap.LDAPAttributesBean;
import com.spring.uam.ldap.LoginCPF;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/login1")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		String userName = request.getParameter("username");
		
		String pwd = request.getParameter("password");

		LDAPAttributesBean ldap = LoginCPF.authenticateUser(userName, pwd);
	
		
		if (!ldap.getValidated().equals(0)) {

			/*
			 * HttpSession session = request.getSession(false); if(session!=null) {
			 * session.invalidate(); }
			 */
			// Success
			HttpSession session = request.getSession(true);
			session = request.getSession(true); // reuse existing
			// session if exist
			// or create one
			session.setAttribute("user", userName);
			session.setAttribute("FullName", ldap.getFULNAME());
			session.setAttribute("userRole", String.valueOf(ldap.getUserrole()));
		

		

			String userRole = (String) session.getAttribute("userRole");
		

			// Etiqa
			if ("1".equalsIgnoreCase(userRole)) {
				System.out.println("LoginController<doPost><userRole> <1>");
				response.sendRedirect("getDashboard");
				// response.sendRedirect("roadTaxLoginDone");
			}
			// Khairat
			/*
			 * else if("2".equalsIgnoreCase(userRole)) {
			 * System.out.println("LoginController<doPost><userRole> <2>" );
			 * response.sendRedirect("getKhairat"); }
			 */
			// POS KL
			/*
			 * else if("3".equalsIgnoreCase(userRole)) {
			 * System.out.println("LoginController<doPost><userRole> <3>" );
			 * session.setAttribute("stateCoverage", "Kuala Lumpur");
			 * response.sendRedirect("roadTaxLoginDone"); } //POS SHAH ALAM else
			 * if("4".equalsIgnoreCase(userRole)) {
			 * System.out.println("LoginController<doPost><userRole> <4>" );
			 * session.setAttribute("stateCoverage", "Selangor");
			 * response.sendRedirect("roadTaxLoginDone"); }
			 */
			/*
			 * // Etiqa else if("5".equalsIgnoreCase(userRole)) {
			 * System.out.println("LoginController<doPost><userRole> <5>" );
			 * session.setAttribute("stateCoverage", "Etiqa");
			 * response.sendRedirect("roadTaxLoginDone"); }
			 */
			// BankaPA
			/*
			 * else if("6".equalsIgnoreCase(userRole)) {
			 * System.out.println("LoginController<doPost><userRole> <6>" );
			 * response.sendRedirect("getBankaPA"); }
			 */
			else {
				// response.sendRedirect("getDashboard");
				System.out.println("ldap Else loop##########");
			}
		} else {
			// Fail
			// request.getRequestDispatcher("admin-login.jsp").forward(request, response);
			System.out.println("ldap login failed ##########");
			response.sendRedirect("admin-login.jsp?result=fail");
		}

	}

}
