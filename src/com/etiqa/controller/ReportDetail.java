package com.etiqa.controller;

import java.util.List;

import com.etiqa.DAO.ReportDAO;
import com.etiqa.DAO.ReportDAOImpl;
import com.etiqa.model.report.TransactionalReport;

public class ReportDetail {

	public List<TransactionalReport> hohhBenefit(String id) {
		int ID = Integer.parseInt(id.trim());

		ReportDAO dao = new ReportDAOImpl();// daoFactory.getAgentDAO();
		List<TransactionalReport> hohhBenefitList = dao.getHOHHBenefit(ID);
		// System.out.println(hohhBenefitList.size());
		return hohhBenefitList;
	}

}
