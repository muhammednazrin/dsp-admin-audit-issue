package com.etiqa.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.dsp.dao.common.pojo.Audit_Customer_Bean;

import oracle.jdbc.OracleTypes;

@WebServlet("/getExportCustomer")
public class AuditCustExportController extends HttpServlet {

	private static ConnectionFactory db1;
	private static Connection connection;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String fromdate = request.getParameter("fromdate");
		String todate = request.getParameter("todate");

		SimpleDateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date;
		Date toDateparse;

		try {

			date = originalFormat.parse(fromdate);
			toDateparse = originalFormat.parse(todate);

			request.setAttribute("fromdate", fromdate);
			request.setAttribute("todate", todate);

			fromdate = targetFormat.format(date);
			todate = targetFormat.format(toDateparse);

		} catch (ParseException ex) {

		}

		try {

			HSSFWorkbook hwb = new HSSFWorkbook();
			HSSFSheet sheet = hwb.createSheet("new sheet");

			HSSFRow rowhead = sheet.createRow(0);

			rowhead.createCell(0).setCellValue("O_CUSTOMER_ID");
			rowhead.createCell(1).setCellValue("CUSTOMER_ID");
			rowhead.createCell(2).setCellValue("O_CUSTOMER_NRIC_ID");
			rowhead.createCell(3).setCellValue("CUSTOMER_NRIC_ID");
			rowhead.createCell(4).setCellValue("O_CUSTOMER_NAME");
			rowhead.createCell(5).setCellValue("CUSTOMER_NAME");
			rowhead.createCell(6).setCellValue("O_CUSTOMER_DOB");
			rowhead.createCell(7).setCellValue("CUSTOMER_DOB");
			rowhead.createCell(8).setCellValue("O_CUSTOMER_GENDER");
			rowhead.createCell(9).setCellValue("CUSTOMER_GENDER");
			rowhead.createCell(10).setCellValue("O_CUSTOMER_RACE");
			rowhead.createCell(11).setCellValue("CUSTOMER_RACE");
			rowhead.createCell(12).setCellValue("O_CUSTOMER_RELIGION");
			rowhead.createCell(13).setCellValue("CUSTOMER_RELIGION");
			rowhead.createCell(14).setCellValue("O_CUSTOMER_NATIONALITY");
			rowhead.createCell(15).setCellValue("CUSTOMER_NATIONALITY");
			rowhead.createCell(16).setCellValue("O_CUSTOMER_TITLE");
			rowhead.createCell(17).setCellValue("CUSTOMER_TITLE");
			rowhead.createCell(18).setCellValue("O_CUSTOMER_NATIONALITY_RACE");
			rowhead.createCell(19).setCellValue("CUSTOMER_NATIONALITY_RACE");
			rowhead.createCell(20).setCellValue("O_CUSTOMER_EDU_LEVEL");
			rowhead.createCell(21).setCellValue("CUSTOMER_EDU_LEVEL");
			rowhead.createCell(22).setCellValue("O_CUSTOMER_MARITALSTATUS");
			rowhead.createCell(23).setCellValue("CUSTOMER_MARITALSTATUS");
			rowhead.createCell(24).setCellValue("O_CUSTOMER_SALARY_RANGE");
			rowhead.createCell(25).setCellValue("CUSTOMER_SALARY_RANGE");
			rowhead.createCell(26).setCellValue("O_CUSTOMER_OCCUPATION");
			rowhead.createCell(27).setCellValue("CUSTOMER_OCCUPATION");
			rowhead.createCell(28).setCellValue("O_CUSTOMER_ADDRESS1");
			rowhead.createCell(29).setCellValue("CUSTOMER_ADDRESS1");
			rowhead.createCell(30).setCellValue("O_CUSTOMER_ADDRESS2");
			rowhead.createCell(31).setCellValue("CUSTOMER_ADDRESS2");
			rowhead.createCell(32).setCellValue("O_CUSTOMER_ADDRESS3");
			rowhead.createCell(33).setCellValue("CUSTOMER_ADDRESS3");
			rowhead.createCell(34).setCellValue("O_CUSTOMER_POSTCODE");
			rowhead.createCell(35).setCellValue("CUSTOMER_POSTCODE");
			rowhead.createCell(36).setCellValue("O_CUSTOMER_STATE");
			rowhead.createCell(37).setCellValue("CUSTOMER_STATE");
			rowhead.createCell(38).setCellValue("O_CUSTOMER_COUNTRY");
			rowhead.createCell(39).setCellValue("CUSTOMER_COUNTRY");
			rowhead.createCell(40).setCellValue("O_CUSTOMER_MAIL_ADDRESS1");
			rowhead.createCell(41).setCellValue("CUSTOMER_MAIL_ADDRESS1");
			rowhead.createCell(42).setCellValue("O_CUSTOMER_MAIL_ADDRESS2");
			rowhead.createCell(43).setCellValue("CUSTOMER_MAIL_ADDRESS2");
			rowhead.createCell(44).setCellValue("O_CUSTOMER_MAIL_ADDRESS3");
			rowhead.createCell(45).setCellValue("CUSTOMER_MAIL_ADDRESS3");
			rowhead.createCell(46).setCellValue("O_CUSTOMER_MAIL_POSTCODE");
			rowhead.createCell(47).setCellValue("CUSTOMER_MAIL_POSTCODE");
			rowhead.createCell(48).setCellValue("O_CUSTOMER_MAIL_STATE");
			rowhead.createCell(49).setCellValue("CUSTOMER_MAIL_STATE");
			rowhead.createCell(50).setCellValue("O_CUSTOMER_MAIL_COUNTRY");
			rowhead.createCell(51).setCellValue("CUSTOMER_MAIL_COUNTRY");
			rowhead.createCell(52).setCellValue("O_CUSTOMER_NO_CHILDREN");
			rowhead.createCell(53).setCellValue("CUSTOMER_NO_CHILDREN");
			rowhead.createCell(54).setCellValue("O_CUSTOMER_MOBILE_NO");
			rowhead.createCell(55).setCellValue("CUSTOMER_MOBILE_NO");
			rowhead.createCell(56).setCellValue("O_CUSTOMER_EMAIL");
			rowhead.createCell(57).setCellValue("CUSTOMER_EMAIL");
			rowhead.createCell(58).setCellValue("O_LEADS_FLAG");
			rowhead.createCell(59).setCellValue("LEADS_FLAG");
			rowhead.createCell(60).setCellValue("O_HOME_MAIL_CHECK");
			rowhead.createCell(61).setCellValue("HOME_MAIL_CHECK");
			rowhead.createCell(62).setCellValue("O_EMAIL_SENT_COUNTER");
			rowhead.createCell(63).setCellValue("EMAIL_SENT_COUNTER");
			rowhead.createCell(64).setCellValue("O_QQ_ID");
			rowhead.createCell(65).setCellValue("QQ_ID");
			rowhead.createCell(66).setCellValue("O_CREATE_DATE");
			rowhead.createCell(67).setCellValue("CREATE_DATE");
			rowhead.createCell(68).setCellValue("O_UPDATED_DATE");
			rowhead.createCell(69).setCellValue("UPDATED_DATE");
			rowhead.createCell(70).setCellValue("O_CUSTOMER_EMPLOYER");
			rowhead.createCell(71).setCellValue("CUSTOMER_EMPLOYER");
			rowhead.createCell(72).setCellValue("O_CUSTOMER_CLIENTTYPE");
			rowhead.createCell(73).setCellValue("CUSTOMER_CLIENTTYPE");
			rowhead.createCell(74).setCellValue("O_CUSTOMER_INDUSTRY");
			rowhead.createCell(75).setCellValue("CUSTOMER_INDUSTRY");
			rowhead.createCell(76).setCellValue("WHO");
			rowhead.createCell(77).setCellValue("DATE_WHEN");
			rowhead.createCell(78).setCellValue("RECORD_COMMENT");

			List<Audit_Customer_Bean> myDataList = new ArrayList<Audit_Customer_Bean>();

			ResultSet resultSet = null;
			Statement statement = null;
			int i = 1;

			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{call GET_AUDIT_COMMON_TBL_CUSTOMER(?,?,?)}");
			cstmt.setString(1, fromdate);
			cstmt.setString(2, todate);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);

			cstmt.execute();

			resultSet = (ResultSet) cstmt.getObject(3);

			while (resultSet.next()) {

				HSSFRow row = sheet.createRow(i);

				try {

					if (resultSet.getString("O_CUSTOMER_ID").equals(resultSet.getString("CUSTOMER_ID"))) {
						row.createCell(0).setCellValue("");
						row.createCell(1).setCellValue(resultSet.getString("CUSTOMER_ID"));
					} else {
						row.createCell(0).setCellValue(resultSet.getString("O_CUSTOMER_ID"));
						row.createCell(1).setCellValue(resultSet.getString("CUSTOMER_ID"));
					}
				} catch (Exception ex) {
					row.createCell(0).setCellValue("");
					row.createCell(1).setCellValue("");
				}

				try {

					if (resultSet.getString("O_CUSTOMER_NRIC_ID").equals(resultSet.getString("CUSTOMER_NRIC_ID"))) {
						row.createCell(2).setCellValue("");
						row.createCell(3).setCellValue(resultSet.getString("CUSTOMER_NRIC_ID"));
					} else {
						row.createCell(2).setCellValue(resultSet.getString("O_CUSTOMER_NRIC_ID"));
						row.createCell(3).setCellValue(resultSet.getString("CUSTOMER_NRIC_ID"));
					}
				} catch (Exception ex) {
					row.createCell(2).setCellValue("");
					row.createCell(3).setCellValue("");
				}

				try {
					if (resultSet.getString("O_CUSTOMER_NAME").equals(resultSet.getString("CUSTOMER_NAME"))) {
						row.createCell(4).setCellValue("");
						row.createCell(5).setCellValue(resultSet.getString("CUSTOMER_NAME"));
					} else {
						row.createCell(4).setCellValue(resultSet.getString("O_CUSTOMER_NAME"));
						row.createCell(5).setCellValue(resultSet.getString("CUSTOMER_NAME"));
					}
				} catch (Exception ex) {
					row.createCell(4).setCellValue("");
					row.createCell(5).setCellValue("");
				}
				try {
					if (resultSet.getString("O_CUSTOMER_DOB").equals(resultSet.getString("CUSTOMER_DOB"))) {
						row.createCell(6).setCellValue("");
						row.createCell(7).setCellValue(resultSet.getString("CUSTOMER_DOB"));
					} else {
						row.createCell(6).setCellValue(resultSet.getString("O_CUSTOMER_DOB"));
						row.createCell(7).setCellValue(resultSet.getString("CUSTOMER_DOB"));
					}
				} catch (Exception ex) {
					row.createCell(6).setCellValue("");
					row.createCell(7).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_GENDER").equals(resultSet.getString("CUSTOMER_GENDER"))) {
						row.createCell(8).setCellValue("");
						row.createCell(9).setCellValue(resultSet.getString("CUSTOMER_GENDER"));
					} else {
						row.createCell(8).setCellValue(resultSet.getString("O_CUSTOMER_GENDER"));
						row.createCell(9).setCellValue(resultSet.getString("CUSTOMER_GENDER"));
					}
				} catch (Exception ex) {
					row.createCell(8).setCellValue("");
					row.createCell(9).setCellValue("");
				}

				try {

					if (resultSet.getString("O_CUSTOMER_RACE").equals(resultSet.getString("CUSTOMER_RACE"))) {
						row.createCell(10).setCellValue("");
						row.createCell(11).setCellValue(resultSet.getString("CUSTOMER_RACE"));
					} else {
						row.createCell(10).setCellValue(resultSet.getString("O_CUSTOMER_RACE"));
						row.createCell(11).setCellValue(resultSet.getString("CUSTOMER_RACE"));
					}
				} catch (Exception ex) {

					row.createCell(10).setCellValue("");
					row.createCell(11).setCellValue("");

				}

				try {
					if (resultSet.getString("O_CUSTOMER_RELIGION").equals(resultSet.getString("CUSTOMER_RELIGION"))) {
						row.createCell(12).setCellValue("");
						row.createCell(13).setCellValue(resultSet.getString("CUSTOMER_RELIGION"));
					} else {
						row.createCell(12).setCellValue(resultSet.getString("O_CUSTOMER_RELIGION"));
						row.createCell(13).setCellValue(resultSet.getString("CUSTOMER_RELIGION"));
					}
				} catch (Exception ex) {
					row.createCell(12).setCellValue("");
					row.createCell(13).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_NATIONALITY")
							.equals(resultSet.getString("CUSTOMER_NATIONALITY"))) {
						row.createCell(14).setCellValue("");
						row.createCell(15).setCellValue(resultSet.getString("CUSTOMER_NATIONALITY"));
					} else {
						row.createCell(14).setCellValue(resultSet.getString("O_CUSTOMER_NATIONALITY"));
						row.createCell(15).setCellValue(resultSet.getString("CUSTOMER_NATIONALITY"));
					}
				} catch (Exception ex) {
					row.createCell(14).setCellValue("");
					row.createCell(15).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_TITLE").equals(resultSet.getString("CUSTOMER_TITLE"))) {
						row.createCell(16).setCellValue("");
						row.createCell(17).setCellValue(resultSet.getString("CUSTOMER_TITLE"));
					} else {
						row.createCell(16).setCellValue(resultSet.getString("O_CUSTOMER_TITLE"));
						row.createCell(17).setCellValue(resultSet.getString("CUSTOMER_TITLE"));
					}
				} catch (Exception ex) {
					row.createCell(16).setCellValue("");
					row.createCell(17).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_NATIONALITY_RACE")
							.equals(resultSet.getString("CUSTOMER_NATIONALITY_RACE"))) {
						row.createCell(18).setCellValue("");
						row.createCell(19).setCellValue(resultSet.getString("CUSTOMER_NATIONALITY_RACE"));
					} else {
						row.createCell(18).setCellValue(resultSet.getString("O_CUSTOMER_NATIONALITY_RACE"));
						row.createCell(19).setCellValue(resultSet.getString("CUSTOMER_NATIONALITY_RACE"));
					}
				} catch (Exception ex) {
					row.createCell(18).setCellValue("");
					row.createCell(19).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_EDU_LEVEL").equals(resultSet.getString("CUSTOMER_EDU_LEVEL"))) {
						row.createCell(20).setCellValue("");
						row.createCell(21).setCellValue(resultSet.getString("CUSTOMER_EDU_LEVEL"));
					} else {
						row.createCell(20).setCellValue(resultSet.getString("O_CUSTOMER_EDU_LEVEL"));
						row.createCell(21).setCellValue(resultSet.getString("CUSTOMER_EDU_LEVEL"));
					}
				} catch (Exception ex) {
					row.createCell(20).setCellValue("");
					row.createCell(21).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_MARITALSTATUS")
							.equals(resultSet.getString("CUSTOMER_MARITALSTATUS"))) {
						row.createCell(22).setCellValue("");
						row.createCell(23).setCellValue(resultSet.getString("CUSTOMER_MARITALSTATUS"));
					} else {
						row.createCell(22).setCellValue(resultSet.getString("O_CUSTOMER_MARITALSTATUS"));
						row.createCell(23).setCellValue(resultSet.getString("CUSTOMER_MARITALSTATUS"));
					}
				} catch (Exception ex) {
					row.createCell(22).setCellValue("");
					row.createCell(23).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_SALARY_RANGE")
							.equals(resultSet.getString("CUSTOMER_SALARY_RANGE"))) {
						row.createCell(24).setCellValue("");
						row.createCell(25).setCellValue(resultSet.getString("CUSTOMER_SALARY_RANGE"));
					} else {
						row.createCell(24).setCellValue(resultSet.getString("O_CUSTOMER_SALARY_RANGE"));
						row.createCell(25).setCellValue(resultSet.getString("CUSTOMER_SALARY_RANGE"));
					}
				} catch (Exception ex) {
					row.createCell(24).setCellValue("");
					row.createCell(25).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_OCCUPATION")
							.equals(resultSet.getString("CUSTOMER_OCCUPATION"))) {
						row.createCell(26).setCellValue("");
						row.createCell(27).setCellValue(resultSet.getString("CUSTOMER_OCCUPATION"));
					} else {
						row.createCell(26).setCellValue(resultSet.getString("O_CUSTOMER_OCCUPATION"));
						row.createCell(27).setCellValue(resultSet.getString("CUSTOMER_OCCUPATION"));
					}
				} catch (Exception ex) {
					row.createCell(26).setCellValue("");
					row.createCell(27).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_ADDRESS1").equals(resultSet.getString("CUSTOMER_ADDRESS1"))) {
						row.createCell(28).setCellValue("");
						row.createCell(29).setCellValue(resultSet.getString("CUSTOMER_ADDRESS1"));

					} else {
						row.createCell(28).setCellValue(resultSet.getString("O_CUSTOMER_ADDRESS1"));
						row.createCell(29).setCellValue(resultSet.getString("CUSTOMER_ADDRESS1"));

					}
				} catch (Exception ex) {
					row.createCell(28).setCellValue("");
					row.createCell(29).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_ADDRESS2").equals(resultSet.getString("CUSTOMER_ADDRESS2"))) {
						row.createCell(30).setCellValue("");
						row.createCell(31).setCellValue(resultSet.getString("CUSTOMER_ADDRESS2"));
					} else {
						row.createCell(30).setCellValue(resultSet.getString("O_CUSTOMER_ADDRESS2"));
						row.createCell(31).setCellValue(resultSet.getString("CUSTOMER_ADDRESS2"));
					}
				} catch (Exception ex) {
					row.createCell(30).setCellValue("");
					row.createCell(31).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_ADDRESS3").equals(resultSet.getString("CUSTOMER_ADDRESS3"))) {
						row.createCell(32).setCellValue("");
						row.createCell(33).setCellValue(resultSet.getString("CUSTOMER_ADDRESS3"));
					} else {
						row.createCell(32).setCellValue(resultSet.getString("O_CUSTOMER_ADDRESS3"));
						row.createCell(33).setCellValue(resultSet.getString("CUSTOMER_ADDRESS3"));
					}
				} catch (Exception ex) {
					row.createCell(32).setCellValue("");
					row.createCell(33).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_POSTCODE").equals(resultSet.getString("CUSTOMER_POSTCODE"))) {
						row.createCell(34).setCellValue("");
						row.createCell(35).setCellValue(resultSet.getString("CUSTOMER_POSTCODE"));
					} else {
						row.createCell(34).setCellValue(resultSet.getString("O_CUSTOMER_POSTCODE"));
						row.createCell(35).setCellValue(resultSet.getString("CUSTOMER_POSTCODE"));
					}
				} catch (Exception ex) {
					row.createCell(34).setCellValue("");
					row.createCell(35).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_STATE").equals(resultSet.getString("CUSTOMER_STATE"))) {
						row.createCell(36).setCellValue("");
						row.createCell(37).setCellValue(resultSet.getString("CUSTOMER_STATE"));
					} else {
						row.createCell(36).setCellValue(resultSet.getString("O_CUSTOMER_STATE"));
						row.createCell(37).setCellValue(resultSet.getString("CUSTOMER_STATE"));
					}
				} catch (Exception ex) {
					row.createCell(36).setCellValue("");
					row.createCell(37).setCellValue("");
				}
				try {

					if (resultSet.getString("CUSTOMER_COUNTRY").equals(resultSet.getString("O_CUSTOMER_COUNTRY"))) {
						row.createCell(38).setCellValue("");
						row.createCell(39).setCellValue(resultSet.getString("CUSTOMER_COUNTRY"));
					} else {
						row.createCell(38).setCellValue(resultSet.getString("O_CUSTOMER_COUNTRY"));
						row.createCell(39).setCellValue(resultSet.getString("CUSTOMER_COUNTRY"));
					}
				} catch (Exception ex) {
					row.createCell(38).setCellValue("");
					row.createCell(39).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_MAIL_ADDRESS1")
							.equals(resultSet.getString("CUSTOMER_MAIL_ADDRESS1"))) {
						row.createCell(40).setCellValue("");
						row.createCell(41).setCellValue(resultSet.getString("CUSTOMER_MAIL_ADDRESS1"));
					} else {
						row.createCell(40).setCellValue(resultSet.getString("O_CUSTOMER_MAIL_ADDRESS1"));
						row.createCell(41).setCellValue(resultSet.getString("CUSTOMER_MAIL_ADDRESS1"));
					}
				} catch (Exception ex) {
					row.createCell(40).setCellValue("");
					row.createCell(41).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_MAIL_ADDRESS2")
							.equals(resultSet.getString("CUSTOMER_MAIL_ADDRESS2"))) {
						row.createCell(42).setCellValue("");
						row.createCell(43).setCellValue(resultSet.getString("CUSTOMER_MAIL_ADDRESS2"));
					} else {
						row.createCell(42).setCellValue(resultSet.getString("O_CUSTOMER_MAIL_ADDRESS2"));
						row.createCell(43).setCellValue(resultSet.getString("CUSTOMER_MAIL_ADDRESS2"));
					}
				} catch (Exception ex) {
					row.createCell(42).setCellValue("");
					row.createCell(43).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_MAIL_ADDRESS3")
							.equals(resultSet.getString("CUSTOMER_MAIL_ADDRESS3"))) {
						row.createCell(44).setCellValue("");
						row.createCell(45).setCellValue(resultSet.getString("CUSTOMER_MAIL_ADDRESS3"));
					} else {
						row.createCell(44).setCellValue(resultSet.getString("O_CUSTOMER_MAIL_ADDRESS3"));
						row.createCell(45).setCellValue(resultSet.getString("CUSTOMER_MAIL_ADDRESS3"));
					}
				} catch (Exception ex) {
					row.createCell(44).setCellValue("");
					row.createCell(45).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_MAIL_POSTCODE")
							.equals(resultSet.getString("CUSTOMER_MAIL_POSTCODE"))) {
						row.createCell(46).setCellValue(resultSet.getString("O_CUSTOMER_MAIL_POSTCODE"));
						row.createCell(47).setCellValue(resultSet.getString("CUSTOMER_MAIL_POSTCODE"));
					} else {
						row.createCell(46).setCellValue(resultSet.getString("O_CUSTOMER_MAIL_POSTCODE"));
						row.createCell(47).setCellValue(resultSet.getString("CUSTOMER_MAIL_POSTCODE"));
					}
				} catch (Exception ex) {
					row.createCell(46).setCellValue("");
					row.createCell(47).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_MAIL_STATE")
							.equals(resultSet.getString("CUSTOMER_MAIL_STATE"))) {
						row.createCell(48).setCellValue("");
						row.createCell(49).setCellValue(resultSet.getString("CUSTOMER_MAIL_STATE"));
					} else {
						row.createCell(48).setCellValue(resultSet.getString("O_CUSTOMER_MAIL_STATE"));
						row.createCell(49).setCellValue(resultSet.getString("CUSTOMER_MAIL_STATE"));
					}
				} catch (Exception ex) {
					row.createCell(48).setCellValue("");
					row.createCell(49).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_MAIL_COUNTRY")
							.equals(resultSet.getString("CUSTOMER_MAIL_COUNTRY"))) {
						row.createCell(50).setCellValue(resultSet.getString("O_CUSTOMER_MAIL_COUNTRY"));
						row.createCell(51).setCellValue(resultSet.getString("CUSTOMER_MAIL_COUNTRY"));
					} else {
						row.createCell(50).setCellValue(resultSet.getString("O_CUSTOMER_MAIL_COUNTRY"));
						row.createCell(51).setCellValue(resultSet.getString("CUSTOMER_MAIL_COUNTRY"));
					}
				} catch (Exception ex) {
					row.createCell(50).setCellValue("");
					row.createCell(51).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_NO_CHILDREN")
							.equals(resultSet.getString("CUSTOMER_NO_CHILDREN"))) {
						row.createCell(52).setCellValue(resultSet.getString("O_CUSTOMER_NO_CHILDREN"));
						row.createCell(53).setCellValue(resultSet.getString("CUSTOMER_NO_CHILDREN"));
					} else {
						row.createCell(52).setCellValue(resultSet.getString("O_CUSTOMER_NO_CHILDREN"));
						row.createCell(53).setCellValue(resultSet.getString("CUSTOMER_NO_CHILDREN"));
					}
				} catch (Exception ex) {
					row.createCell(52).setCellValue("");
					row.createCell(53).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_MOBILE_NO").equals(resultSet.getString("CUSTOMER_MOBILE_NO"))) {
						row.createCell(54).setCellValue("");
						row.createCell(55).setCellValue(resultSet.getString("CUSTOMER_MOBILE_NO"));
					} else {
						row.createCell(54).setCellValue(resultSet.getString("O_CUSTOMER_MOBILE_NO"));
						row.createCell(55).setCellValue(resultSet.getString("CUSTOMER_MOBILE_NO"));
					}
				} catch (Exception ex) {
					row.createCell(54).setCellValue("");
					row.createCell(55).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_EMAIL").equals(resultSet.getString("CUSTOMER_EMAIL"))) {
						row.createCell(56).setCellValue("");
						row.createCell(57).setCellValue(resultSet.getString("CUSTOMER_EMAIL"));
					} else {
						row.createCell(56).setCellValue(resultSet.getString("O_CUSTOMER_EMAIL"));
						row.createCell(57).setCellValue(resultSet.getString("CUSTOMER_EMAIL"));
					}
				} catch (Exception ex) {
					row.createCell(56).setCellValue("");
					row.createCell(57).setCellValue("");
				}
				try {

					if (resultSet.getString("O_LEADS_FLAG").equals(resultSet.getString("LEADS_FLAG"))) {
						row.createCell(58).setCellValue("");
						row.createCell(59).setCellValue(resultSet.getString("LEADS_FLAG"));
					} else {
						row.createCell(58).setCellValue(resultSet.getString("O_LEADS_FLAG"));
						row.createCell(59).setCellValue(resultSet.getString("LEADS_FLAG"));
					}
				} catch (Exception ex) {
					row.createCell(58).setCellValue("");
					row.createCell(59).setCellValue("");
				}
				try {

					if (resultSet.getString("O_HOME_MAIL_CHECK").equals(resultSet.getString("HOME_MAIL_CHECK"))) {
						row.createCell(60).setCellValue("");
						row.createCell(61).setCellValue(resultSet.getString("HOME_MAIL_CHECK"));
					} else {
						row.createCell(60).setCellValue(resultSet.getString("O_HOME_MAIL_CHECK"));
						row.createCell(61).setCellValue(resultSet.getString("HOME_MAIL_CHECK"));
					}
				} catch (Exception ex) {
					row.createCell(60).setCellValue("");
					row.createCell(61).setCellValue("");
				}
				try {

					if (resultSet.getString("O_EMAIL_SENT_COUNTER").equals(resultSet.getString("EMAIL_SENT_COUNTER"))) {
						row.createCell(62).setCellValue("");
						row.createCell(63).setCellValue(resultSet.getString("EMAIL_SENT_COUNTER"));
					} else {
						row.createCell(62).setCellValue(resultSet.getString("O_EMAIL_SENT_COUNTER"));
						row.createCell(63).setCellValue(resultSet.getString("EMAIL_SENT_COUNTER"));
					}
				} catch (Exception ex) {
					row.createCell(62).setCellValue("");
					row.createCell(63).setCellValue("");
				}
				try {

					if (resultSet.getString("O_QQ_ID").equals(resultSet.getString("QQ_ID"))) {
						row.createCell(64).setCellValue("");
						row.createCell(65).setCellValue(resultSet.getString("QQ_ID"));
					} else {
						row.createCell(64).setCellValue(resultSet.getString("O_QQ_ID"));
						row.createCell(65).setCellValue(resultSet.getString("QQ_ID"));
					}
				} catch (Exception ex) {
					row.createCell(64).setCellValue("");
					row.createCell(65).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CREATE_DATE").equals(resultSet.getString("CREATE_DATE"))) {
						row.createCell(66).setCellValue("");
						row.createCell(67).setCellValue(resultSet.getString("CREATE_DATE"));

					} else {
						row.createCell(66).setCellValue(resultSet.getString("O_CREATE_DATE"));
						row.createCell(67).setCellValue(resultSet.getString("CREATE_DATE"));
					}
				} catch (Exception ex) {
					row.createCell(66).setCellValue("");
					row.createCell(67).setCellValue("");
				}
				try {

					if (resultSet.getString("O_UPDATED_DATE").equals(resultSet.getString("UPDATED_DATE"))) {
						row.createCell(68).setCellValue("");
						row.createCell(69).setCellValue(resultSet.getString("UPDATED_DATE"));

					} else {
						row.createCell(68).setCellValue(resultSet.getString("O_UPDATED_DATE"));
						row.createCell(69).setCellValue(resultSet.getString("UPDATED_DATE"));
					}
				} catch (Exception ex) {
					row.createCell(68).setCellValue("");
					row.createCell(69).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_EMPLOYER").equals(resultSet.getString("CUSTOMER_EMPLOYER"))) {
						row.createCell(70).setCellValue("");
						row.createCell(71).setCellValue(resultSet.getString("CUSTOMER_EMPLOYER"));
					} else {
						row.createCell(70).setCellValue(resultSet.getString("O_CUSTOMER_EMPLOYER"));
						row.createCell(71).setCellValue(resultSet.getString("CUSTOMER_EMPLOYER"));
					}
				} catch (Exception ex) {
					row.createCell(70).setCellValue("");
					row.createCell(71).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_CLIENTTYPE")
							.equals(resultSet.getString("CUSTOMER_CLIENTTYPE"))) {
						row.createCell(72).setCellValue(resultSet.getString("O_CUSTOMER_CLIENTTYPE"));
						row.createCell(73).setCellValue(resultSet.getString("CUSTOMER_CLIENTTYPE"));
					} else {
						row.createCell(72).setCellValue(resultSet.getString("O_CUSTOMER_CLIENTTYPE"));
						row.createCell(73).setCellValue(resultSet.getString("CUSTOMER_CLIENTTYPE"));
					}
				} catch (Exception ex) {
					row.createCell(72).setCellValue("");
					row.createCell(73).setCellValue("");
				}
				try {

					if (resultSet.getString("O_CUSTOMER_INDUSTRY").equals(resultSet.getString("CUSTOMER_INDUSTRY"))) {
						row.createCell(74).setCellValue("");
						row.createCell(75).setCellValue(resultSet.getString("CUSTOMER_INDUSTRY"));
					} else {
						row.createCell(74).setCellValue(resultSet.getString("O_CUSTOMER_INDUSTRY"));
						row.createCell(75).setCellValue(resultSet.getString("CUSTOMER_INDUSTRY"));
					}
				} catch (Exception ex) {
					row.createCell(74).setCellValue("");
					row.createCell(75).setCellValue("");
				}

				row.createCell(76).setCellValue(resultSet.getString("WHO"));
				row.createCell(77).setCellValue(resultSet.getString("DATE_WHEN"));
				row.createCell(78).setCellValue(resultSet.getString("RECORD_COMMENT"));

				i++;

			}

			ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
			hwb.write(outByteStream);
			byte[] outArray = outByteStream.toByteArray();
			response.setContentType("application/ms-excel");
			response.setContentLength(outArray.length);
			response.setHeader("Expires:", "0");
			response.setHeader("Content-Disposition", "attachment; filename=Customer_Audit_Log.xls");
			OutputStream outStream = response.getOutputStream();
			outStream.write(outArray);
			outStream.flush();

			request.setAttribute("fromdate", fromdate);
			request.setAttribute("todate", todate);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("AuditCustomerReport.jsp").forward(request, response);

	}

}
