package com.etiqa.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.etiqa.DAO.AccupdateDAO;
import com.etiqa.DAO.AccupdateDAOImpl;
import com.etiqa.admin.AccupdateBean;
import com.etiqa.common.BaseServlet;

@WebServlet("/Accinfo")
public class AccountupdateController extends BaseServlet {

	private static final long serialVersionUID = 1L;

	@Override
	public void handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter(ACTION);
		System.out.println(">>> action: " + "[" + action + "]");
		if (action == null || action.isEmpty()) {
			action = "list";
		}
		if (action.equals(ACTION_EVENT_LIST)) {
			list(request, response);
		}
		if (action.equals(ACTION_EVENT_UPDATE)) {
			add(request, response);
		} else if (action.equals(ACTION_EVENT_SAVE)) {
			save(request, response);
		} else if (action.equals(ACTION_EVENT_RECORD)) {
			record(request, response);
		} else {
			list(request, response);
		}
	}

	private void record(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		AccupdateDAO dao = new AccupdateDAOImpl();// daoFactory.getAgentDAO();
		long ID = Long.parseLong(request.getParameter("accId").trim());

		System.out.println("Getting value : " + ID);
		AccupdateBean accupdate = new AccupdateBean();
		accupdate.setId(ID);

		accupdate = dao.queryfetchAccInfoByID(accupdate);

		List<AccupdateBean> policyList = new ArrayList<AccupdateBean>();
		policyList = dao.getPolicylist(accupdate);
		request.setAttribute("policylist", policyList);
		request.setAttribute("Accinfoinsert", accupdate);
		forward(request, response, "viewaccinfo-add-edit.jsp");
	}

	private void add(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		AccupdateDAO dao = new AccupdateDAOImpl();// daoFactory.getAgentDAO();

		AccupdateBean accupdate = new AccupdateBean();
		List<AccupdateBean> policyList = new ArrayList<AccupdateBean>();
		policyList = dao.getPolicylist(accupdate);
		request.setAttribute("policylist", policyList);
		forward(request, response, "viewaccinfo-add-edit.jsp");
	}

	private void list(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		AccupdateDAO dao = new AccupdateDAOImpl();// daoFactory.getAgentDAO();
		String policy = "";
		if (request.getParameter("policyno") != null) {
			policy = request.getParameter("policyno").trim();
		}
		String icno = "";
		if (request.getParameter("icnumber") != null) {
			icno = request.getParameter("icnumber").trim();
		}
		String updateby = "";
		if (request.getParameter("custname") != null) {
			updateby = request.getParameter("custname").trim();
		}
		AccupdateBean accupdate = new AccupdateBean();
		accupdate.setPolicyno(policy);
		accupdate.setIc_number(icno);
		accupdate.setUpdateby(updateby);
		List<AccupdateBean> accinfo = dao.queryfetchAccInfo(accupdate);
		List<AccupdateBean> policyList = new ArrayList<AccupdateBean>();

		policyList = dao.getPolicylist(accupdate);
		request.setAttribute("policylist", policyList);
		request.setAttribute("accountinfo", accinfo);
		forward(request, response, "viewaccinfo.jsp");
	}

	private void listall(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		AccupdateDAO dao = new AccupdateDAOImpl();// daoFactory.getAgentDAO();
		String policy = "";
		String icno = "";
		String updateby = "";

		AccupdateBean accupdate = new AccupdateBean();
		accupdate.setPolicyno(policy);
		accupdate.setIc_number(icno);
		accupdate.setUpdateby(updateby);
		List<AccupdateBean> accinfo = dao.queryfetchAccInfo(accupdate);
		List<AccupdateBean> policyList = new ArrayList<AccupdateBean>();

		policyList = dao.getPolicylist(accupdate);
		request.setAttribute("policylist", policyList);
		request.setAttribute("accountinfo", accinfo);
		forward(request, response, "viewaccinfo.jsp");
	}

	// Save New Cyber Agent
	private void save(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AccupdateDAO dao = new AccupdateDAOImpl();// daoFactory.getAgentDAO();
		// long ID= Long.parseLong(request.getParameter("id").trim());
		String policy = request.getParameter("policyno").trim();
		String icno = request.getParameter("icnumber").trim();
		String bankname = request.getParameter("bankname").trim();
		String bankacc = request.getParameter("bankacc").trim();
		// String branchname =request.getParameter("branchname").trim();
		// String branchcode =request.getParameter("branchcode").trim();
		String branchname = "b";
		String branchcode = "123";
		String updateby = request.getParameter("custname").trim();
		String phno = request.getParameter("phno").trim();
		String email = request.getParameter("email").trim();

		AccupdateBean accupdate = new AccupdateBean();
		accupdate.setPolicyno(policy);
		accupdate.setIc_number(icno);
		accupdate.setBank_name(bankname);
		accupdate.setBranch_name(branchname);
		accupdate.setAcc_no(bankacc);
		accupdate.setBranch_code(branchcode);
		accupdate.setContactno(phno);
		accupdate.setEmail(email);
		accupdate.setUpdateby(updateby);

		accupdate = dao.queryUpdateAccno(accupdate);
		accupdate = dao.queryDSPUpdateAccno(accupdate);

		request.setAttribute("agentAddedMessage", policy + " data saved successfully!");

		listall(request, response);

	}

}
