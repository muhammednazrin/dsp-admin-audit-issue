package com.etiqa.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.etiqa.DAO.AbandonmentDAO;
import com.spring.VO.AbandonmentBean;

@WebServlet("/getAbandonmentDashboard")
public class AbandonmentController extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// it will fire on a POST request (like submitting the form using POST
		// method)

		String fromdate = request.getParameter("fromdate");
		String todate = request.getParameter("todate");
		String product = request.getParameter("product");

		/*
		 * String fromdate = "01/07/2016"; String todate = "28/07/2016"; String product
		 * = "ALL";
		 */

		request.setAttribute("fromdate", fromdate);
		request.setAttribute("todate", todate);
		request.setAttribute("product", product);

		System.out.println("fromdate : " + fromdate);
		System.out.println("todate : " + todate);
		System.out.println("product : " + product);

		List<AbandonmentBean> myDataListHeader = new AbandonmentDAO().getReportHeader(fromdate, todate, product);
		request.setAttribute("myDataListHeader", myDataListHeader);

		List<AbandonmentBean> myDataList = new AbandonmentDAO().getReportData(fromdate, todate, product);
		request.setAttribute("myDataList", myDataList);

		List<AbandonmentBean> myDataListW = new AbandonmentDAO().getReportDataWeek(fromdate, todate, product);
		request.setAttribute("myDataListW", myDataListW);

		List<AbandonmentBean> myDataListM = new AbandonmentDAO().getReportDataMonthly(fromdate, todate, product);
		request.setAttribute("myDataListM", myDataListM);

		// product added

		String myDataListG = new AbandonmentDAO().getReportDataGraph(fromdate, todate, product);
		request.setAttribute("myDataListG", myDataListG);

		String myDataListGH = new AbandonmentDAO().getReportDataGraphHistorgam(fromdate, todate, product);
		request.setAttribute("myDataListGH", myDataListGH);

		String myDataListRM = new AbandonmentDAO().getReportDataGraphRM(fromdate, todate, product);
		request.setAttribute("myDataListRM", myDataListRM);

		String myDataListRMT = new AbandonmentDAO().getReportDataGraphT(fromdate, todate, product);
		request.setAttribute("myDataListRMT", myDataListRMT);

		request.setAttribute("fromdate", fromdate);
		request.setAttribute("todate", todate);

		request.getRequestDispatcher("abandonment_dashboard.jsp").forward(request, response);

	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy");

		Date date = new Date();

		request.getRequestDispatcher("abandonment_dashboard.jsp").forward(request, response);

	}
}
