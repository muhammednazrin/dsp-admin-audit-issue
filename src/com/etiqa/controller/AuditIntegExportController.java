package com.etiqa.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.etiqa.common.DB.ConnectionFactory;

import oracle.jdbc.OracleTypes;

@WebServlet("/getExportIntegration")
public class AuditIntegExportController extends HttpServlet {

	private static ConnectionFactory db1;
	private static Connection connection;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String fromdate = request.getParameter("fromdate");
		String todate = request.getParameter("todate");

		SimpleDateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date;
		Date toDateparse;

		try {

			date = originalFormat.parse(fromdate);
			toDateparse = originalFormat.parse(todate);

			request.setAttribute("fromdate", fromdate);
			request.setAttribute("todate", todate);

			fromdate = targetFormat.format(date);
			todate = targetFormat.format(toDateparse);

		} catch (ParseException ex) {

		}

		try {

			HSSFWorkbook hwb = new HSSFWorkbook();
			HSSFSheet sheet = hwb.createSheet("new sheet");

			HSSFRow rowhead = sheet.createRow(0);

			rowhead.createCell(0).setCellValue("RECON_ID");
			rowhead.createCell(1).setCellValue("POLICY_NUMBER");
			rowhead.createCell(2).setCellValue("DATE_INT");
			rowhead.createCell(3).setCellValue("REMARK_INT");
			rowhead.createCell(4).setCellValue("STATUS_POLM");
			rowhead.createCell(5).setCellValue("DATE_INT_2");
			rowhead.createCell(6).setCellValue("REMARK_INT_2");
			rowhead.createCell(7).setCellValue("STATUS_POLM_2");
			rowhead.createCell(8).setCellValue("UPDATE_TIMESTAMP");

			ResultSet resultSet = null;
			int i = 1;

			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{call GET_AUDIT_INTEG_LOG_RPT(?,?,?)}");
			cstmt.setString(1, fromdate);
			cstmt.setString(2, todate);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);

			cstmt.execute();

			resultSet = (ResultSet) cstmt.getObject(3);

			while (resultSet.next()) {

				HSSFRow row = sheet.createRow(i);

				row.createCell(0).setCellValue(resultSet.getString("RECON_ID"));
				row.createCell(1).setCellValue(resultSet.getString("POLICY_NUMBER"));
				row.createCell(2).setCellValue(resultSet.getString("DATE_INT"));
				row.createCell(3).setCellValue(resultSet.getString("REMARK_INT"));
				row.createCell(4).setCellValue(resultSet.getString("STATUS_POLM"));
				row.createCell(5).setCellValue(resultSet.getString("DATE_INT_2"));
				row.createCell(6).setCellValue(resultSet.getString("REMARK_INT_2"));
				row.createCell(7).setCellValue(resultSet.getString("STATUS_POLM_2"));
				row.createCell(8).setCellValue(resultSet.getString("UPDATE_TIMESTAMP"));

				i++;

			}

			ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
			hwb.write(outByteStream);
			byte[] outArray = outByteStream.toByteArray();
			response.setContentType("application/ms-excel");
			response.setContentLength(outArray.length);
			response.setHeader("Expires:", "0");
			response.setHeader("Content-Disposition", "attachment; filename=Integration_Audit_Log.xls");
			OutputStream outStream = response.getOutputStream();
			outStream.write(outArray);
			outStream.flush();

			request.setAttribute("fromdate", fromdate);
			request.setAttribute("todate", todate);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("AuditCustomerReport.jsp").forward(request, response);

	}

}
