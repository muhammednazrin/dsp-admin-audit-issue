package com.etiqa.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.etiqa.common.DB.ConnectionFactory;

import oracle.jdbc.OracleTypes;

@WebServlet("/getExportPayment")
public class AuditPaymentExportController extends HttpServlet {

	private static Connection connection;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String fromdate = request.getParameter("fromdate");
		String todate = request.getParameter("todate");
		String product = request.getParameter("product");

		SimpleDateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date;
		Date toDateparse;

		try {

			date = originalFormat.parse(fromdate);
			toDateparse = originalFormat.parse(todate);

			request.setAttribute("fromdate", fromdate);
			request.setAttribute("todate", todate);

			fromdate = targetFormat.format(date);
			todate = targetFormat.format(toDateparse);

		} catch (ParseException ex) {

		}

		try {

			HSSFWorkbook hwb = new HSSFWorkbook();
			HSSFSheet sheet = hwb.createSheet("new sheet");

			HSSFRow rowhead = sheet.createRow(0);

			rowhead.createCell(0).setCellValue("O_TRANSACTION_ID");
			rowhead.createCell(1).setCellValue("TRANSACTION_ID");
			rowhead.createCell(2).setCellValue("O_DSP_QQ_ID");
			rowhead.createCell(3).setCellValue("DSP_QQ_ID");
			rowhead.createCell(4).setCellValue("O_PMNT_GATEWAY_CODE");
			rowhead.createCell(5).setCellValue("PMNT_GATEWAY_CODE");
			rowhead.createCell(6).setCellValue("O_DISCOUNT_CODE");
			rowhead.createCell(7).setCellValue("DISCOUNT_CODE");
			rowhead.createCell(8).setCellValue("O_PROMO_CODE");
			rowhead.createCell(9).setCellValue("PROMO_CODE");
			rowhead.createCell(10).setCellValue("O_AMOUNT");
			rowhead.createCell(11).setCellValue("AMOUNT");
			rowhead.createCell(12).setCellValue("O_PMNT_STATUS");
			rowhead.createCell(13).setCellValue("PMNT_STATUS");
			rowhead.createCell(14).setCellValue("O_INVOICE_NO");
			rowhead.createCell(15).setCellValue("INVOICE_NO");
			rowhead.createCell(16).setCellValue("O_TRANSACTION_DATETIME");
			rowhead.createCell(17).setCellValue("TRANSACTION_DATETIME");
			rowhead.createCell(18).setCellValue("O_TRANSACTION_STATUS");
			rowhead.createCell(19).setCellValue("TRANSACTION_STATUS");
			rowhead.createCell(20).setCellValue("Remarks");
			rowhead.createCell(21).setCellValue("WHO");
			rowhead.createCell(22).setCellValue("DATE_WHEN");
			rowhead.createCell(23).setCellValue("RECORD_COMMENT");

			ResultSet resultSet = null;

			int i = 1;

			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = null;

			if (product.equals("ALL")) {
				cstmt = connection.prepareCall("{call GET_AUDIT_Export_TBL_PAYMENT(?,?,?,?)}");

			} else {
				cstmt = connection.prepareCall("{call GET_AUDIT_COMMON_TBL_PAY_FIL(?,?,?,?)}");
			}

			cstmt.setString(1, fromdate);
			cstmt.setString(2, todate);
			cstmt.setString(3, product);
			cstmt.registerOutParameter(4, OracleTypes.CURSOR);

			cstmt.execute();

			resultSet = (ResultSet) cstmt.getObject(4);

			while (resultSet.next()) {

				if (resultSet.getString("PMNT_STATUS").equals(resultSet.getString("O_PMNT_STATUS"))) {

				} else {

					HSSFRow row = sheet.createRow(i);

					try {

						if (resultSet.getString("O_TRANSACTION_ID").equals(resultSet.getString("TRANSACTION_ID"))) {
							row.createCell(0).setCellValue(resultSet.getString("O_TRANSACTION_ID"));
							row.createCell(1).setCellValue("");

						} else {
							row.createCell(0).setCellValue(resultSet.getString("O_TRANSACTION_ID"));
							row.createCell(1).setCellValue(resultSet.getString("TRANSACTION_ID"));
						}
					} catch (Exception ex) {

						row.createCell(0).setCellValue("");
						row.createCell(1).setCellValue("");
					}

					try {

						if (resultSet.getString("O_DSP_QQ_ID").equals(resultSet.getString("DSP_QQ_ID"))) {
							row.createCell(2).setCellValue(resultSet.getString("O_DSP_QQ_ID"));
							row.createCell(3).setCellValue("");

						} else {
							row.createCell(2).setCellValue(resultSet.getString("O_DSP_QQ_ID"));
							row.createCell(3).setCellValue(resultSet.getString("DSP_QQ_ID"));
						}
					} catch (Exception ex) {

						row.createCell(2).setCellValue("");
						row.createCell(3).setCellValue("");
					}

					try {

						if (resultSet.getString("O_PMNT_GATEWAY_CODE")
								.equals(resultSet.getString("PMNT_GATEWAY_CODE"))) {
							row.createCell(4).setCellValue(resultSet.getString("O_PMNT_GATEWAY_CODE"));
							row.createCell(5).setCellValue("");

						} else {
							row.createCell(4).setCellValue(resultSet.getString("O_PMNT_GATEWAY_CODE"));
							row.createCell(5).setCellValue(resultSet.getString("PMNT_GATEWAY_CODE"));
						}
					} catch (Exception ex) {

						row.createCell(4).setCellValue("");
						row.createCell(5).setCellValue("");
					}

					try {

						if (resultSet.getString("O_DISCOUNT_CODE").equals(resultSet.getString("DISCOUNT_CODE"))) {
							row.createCell(6).setCellValue(resultSet.getString("O_DISCOUNT_CODE"));
							row.createCell(7).setCellValue("");

						} else {
							row.createCell(6).setCellValue(resultSet.getString("O_DISCOUNT_CODE"));
							row.createCell(7).setCellValue(resultSet.getString("DISCOUNT_CODE"));
						}
					} catch (Exception ex) {

						row.createCell(6).setCellValue("");
						row.createCell(7).setCellValue("");
					}

					try {

						if (resultSet.getString("O_PROMO_CODE").equals(resultSet.getString("PROMO_CODE"))) {
							row.createCell(8).setCellValue(resultSet.getString("O_PROMO_CODE"));
							row.createCell(9).setCellValue("");

						} else {
							row.createCell(8).setCellValue(resultSet.getString("O_PROMO_CODE"));
							row.createCell(9).setCellValue(resultSet.getString("PROMO_CODE"));
						}
					} catch (Exception ex) {
						row.createCell(8).setCellValue("");
						row.createCell(9).setCellValue("");
					}

					try {

						if (resultSet.getString("O_AMOUNT").equals(resultSet.getString("AMOUNT"))) {
							row.createCell(10).setCellValue(resultSet.getString("O_AMOUNT"));
							row.createCell(11).setCellValue("");

						} else {
							row.createCell(10).setCellValue(resultSet.getString("O_AMOUNT"));
							row.createCell(11).setCellValue(resultSet.getString("AMOUNT"));
						}
					} catch (Exception ex) {
						row.createCell(10).setCellValue("");
						row.createCell(11).setCellValue("");
					}

					try {

						if (resultSet.getString("O_PMNT_STATUS").equals(resultSet.getString("PMNT_STATUS"))) {
							row.createCell(12).setCellValue(resultSet.getString("O_PMNT_STATUS"));
							row.createCell(13).setCellValue("");

						} else {
							row.createCell(12).setCellValue(resultSet.getString("O_PMNT_STATUS"));
							row.createCell(13).setCellValue(resultSet.getString("PMNT_STATUS"));
						}
					} catch (Exception ex) {
						row.createCell(12).setCellValue("");
						row.createCell(13).setCellValue("");
					}

					try {

						if (resultSet.getString("O_INVOICE_NO").equals(resultSet.getString("INVOICE_NO"))) {
							row.createCell(14).setCellValue(resultSet.getString("O_INVOICE_NO"));
							row.createCell(15).setCellValue("");

						} else {
							row.createCell(14).setCellValue(resultSet.getString("O_INVOICE_NO"));
							row.createCell(15).setCellValue(resultSet.getString("INVOICE_NO"));
						}
					} catch (Exception ex) {
						row.createCell(14).setCellValue("");
						row.createCell(15).setCellValue("");
					}

					try {

						if (resultSet.getString("O_TRANSACTION_DATETIME")
								.equals(resultSet.getString("TRANSACTION_DATETIME"))) {
							row.createCell(16).setCellValue(resultSet.getString("O_TRANSACTION_DATETIME"));
							row.createCell(17).setCellValue("");

						} else {
							row.createCell(16).setCellValue(resultSet.getString("O_TRANSACTION_DATETIME"));
							row.createCell(17).setCellValue(resultSet.getString("TRANSACTION_DATETIME"));
						}
					} catch (Exception ex) {
						row.createCell(16).setCellValue("");
						row.createCell(17).setCellValue("");
					}

					try {

						if (resultSet.getString("O_TRANSACTION_DATETIME")
								.equals(resultSet.getString("TRANSACTION_DATETIME"))) {
							row.createCell(18).setCellValue(resultSet.getString("O_TRANSACTION_STATUS"));
							row.createCell(19).setCellValue("");

						} else {
							row.createCell(18).setCellValue(resultSet.getString("O_TRANSACTION_STATUS"));
							row.createCell(19).setCellValue(resultSet.getString("TRANSACTION_STATUS"));
						}
					} catch (Exception ex) {
						row.createCell(18).setCellValue("");
						row.createCell(19).setCellValue("");
					}

					row.createCell(20).setCellValue(resultSet.getString("Remarks"));
					row.createCell(21).setCellValue(resultSet.getString("ActionBy"));
					row.createCell(22).setCellValue(resultSet.getString("DATE_WHEN"));
					row.createCell(23).setCellValue(resultSet.getString("RECORD_COMMENT"));

					i++;

				}

			}

			ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
			hwb.write(outByteStream);
			byte[] outArray = outByteStream.toByteArray();
			response.setContentType("application/ms-excel");
			response.setContentLength(outArray.length);
			response.setHeader("Expires:", "0");
			response.setHeader("Content-Disposition", "attachment; filename=Payment_Audit_Log.xls");
			OutputStream outStream = response.getOutputStream();
			outStream.write(outArray);
			outStream.flush();

			request.setAttribute("fromdate", fromdate);
			request.setAttribute("todate", todate);
			request.setAttribute("product", product);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("AuditPaymentReport.jsp").forward(request, response);

	}

}
