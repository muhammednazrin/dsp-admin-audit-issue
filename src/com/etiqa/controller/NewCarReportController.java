package com.etiqa.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;

import com.etiqa.DAO.ReportDAO;
import com.etiqa.DAO.ReportDAOImpl;
import com.etiqa.common.BaseServlet;
import com.etiqa.model.report.TransactionalReport;

@WebServlet("/NewCarReportController")
public class NewCarReportController extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private static String redirect = "";

	@Override
	public void handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		if (null == session.getAttribute("user") || "".equalsIgnoreCase((String) session.getAttribute("user"))) {

			response.sendRedirect("admin-login.jsp");
			// actionBy="1";
		}

		String action = request.getParameter("action");
		// System.out.println(">>> action: " + "[" + action + "]");

		if (action.equals("sendEmail")) {

			sendEmail(request, response);

		}
		else if (action.equals("regenDoc")) {

			regenDoc(request, response);

		}
		
		else {
			throw new RuntimeException("Unknown Action");
		}
	}

	private void sendEmail(HttpServletRequest request, HttpServletResponse response) {

		System.out.println("NciTranReportController<sendEmail>");

		String redirect = "";

		// redirect = "admin-newcar-transaction-details.jsp";

		String policyNo = request.getParameter("policyNo").trim();
		String qqID = request.getParameter("docId").trim();

		int id = Integer.parseInt(request.getParameter("id").trim());
		// String term= request.getParameter("term").trim();

		System.out.println("	<policyNo> => " + policyNo);
		System.out.println("	<qqID> => " + qqID); // 1915
		System.out.println("	<id> => " + id);

		// redirect =
		// "${pageContext.request.contextPath}/newCarDetails/'"+id+'/'+policyNo;
		redirect = "newCarDetails/" + id + "/" + policyNo;

		// System.out.println("Send email for POLICY NO = "+policyNo+" and
		// transaction_id = "+id);

		String emailStatus = "";

		String documenturl = "";

		if (policyNo.indexOf("V") != -1) {
			documenturl = "http://cpf:7011/CPF-DSP-NCI-DOCS";
		}

		else {
			documenturl = "http://cpf:7011/CPF-DSP-NCT-DOCS";

		}
		try {
			// http://192.168.0.247:7005/CPF20092016/MotorInsuranceDocGenWSImplService?WSDL
			// URL url = new URL(documenturl+"/MotorQQDataImplService?WSDL");
			QName servicename = new QName("http://motor.wcservices.dsp.etiqa.com/",
					"MotorInsuranceDocGenWSImplService");
			QName portname = new QName("http://motor.wcservices.dsp.etiqa.com/", "MotorInsuranceDocGenWSImplPort");
			Service service = Service.create(null, servicename);
			service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
					documenturl + "/MotorInsuranceDocGenWSImplService");
			Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class, Service.Mode.MESSAGE);
			MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
			SOAPMessage request1 = mf.createMessage();
			SOAPPart part = request1.getSOAPPart();
			SOAPEnvelope env = part.getEnvelope();
			SOAPBody body = env.getBody();
			// resp = body;
			SOAPElement operation = body.addChildElement("getStatus", "mot", "http://motor.wcservices.dsp.etiqa.com/");
			SOAPElement element_param = operation.addChildElement("QQID");
			element_param.addTextNode(qqID);
			SOAPElement element_param_lang = operation.addChildElement("langValue");
			element_param_lang.addTextNode("lan_en");

			request1.saveChanges();
			String result = null;

			if (request1 != null) {
				ByteArrayOutputStream baos = null;
				try {
					baos = new ByteArrayOutputStream();
					request1.writeTo(baos);
					result = baos.toString();
				} catch (Exception e) {
				} finally {
					if (baos != null) {
						try {
							baos.close();
						} catch (IOException ioe) {
						}
					}
				}
			}
			// System.out.println(result+"document insert");
			// resCode=body ;

			SOAPMessage response1 = dispatch.invoke(request1);
			SOAPBody responsebody = response1.getSOAPBody();
			System.out.println("regenerate doc Motor" + responsebody);
			/*
			 * java.util.Iterator otcupdates = responsebody.getChildElements(); while
			 * (otcupdates.hasNext()) { SOAPElement otcupdates1 =
			 * (SOAPElement)otcupdates.next();
			 *
			 * java.util.Iterator i = otcupdates1.getChildElements(); while( i.hasNext() ){
			 *
			 * SOAPElement e = (SOAPElement)i.next(); java.util.Iterator m =
			 * e.getChildElements(); while( m.hasNext() ){ SOAPElement e2 =
			 * (SOAPElement)m.next();
			 *
			 * } } }
			 */

		} catch (Exception e) {

			emailStatus = "Fail";
			// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
			e.printStackTrace();
		}

		// System.out.println(sendEmailStatus);
		ReportDAO dao = new ReportDAOImpl();
		List<TransactionalReport> TranDetail = dao.getTransactionDetail(id);
		request.setAttribute("data", TranDetail);
		request.setAttribute("statusEm", emailStatus);

		forward(request, response, redirect);

		// ${pageContext.request.contextPath}/newCarDetails/'+obj.paymentTrxID+'/'+obj.policyNo
	}
	private void regenDoc(HttpServletRequest request, HttpServletResponse response) {

		System.out.println("NciTranReportController<sendEmail>");

		String redirect = "";

		// redirect = "admin-newcar-transaction-details.jsp";

		String policyNo = request.getParameter("policyNo").trim();
		String qqID = request.getParameter("docId").trim();

		int id = Integer.parseInt(request.getParameter("id").trim());
		// String term= request.getParameter("term").trim();

		System.out.println("	<policyNo> => " + policyNo);
		System.out.println("	<qqID> => " + qqID); // 1915
		System.out.println("	<id> => " + id);

		// redirect =
		// "${pageContext.request.contextPath}/newCarDetails/'"+id+'/'+policyNo;
		redirect = "newCarDetails/" + id + "/" + policyNo;

		// System.out.println("Send email for POLICY NO = "+policyNo+" and
		// transaction_id = "+id);

		String emailStatus = "";

		String documenturl = "";
		String methodName = "";
		if (policyNo.indexOf("V") != -1) {
			documenturl = "http://cpf:7011/CPF-DSP-NCI-DOCS";
			methodName="getStatusOfDocGenOnlyForNCI";
		}

		else {
			documenturl = "http://cpf:7011/CPF-DSP-NCT-DOCS";
			methodName="getStatusOfDocGenOnlyForNCT";

		}
		try {
			// http://192.168.0.247:7005/CPF20092016/MotorInsuranceDocGenWSImplService?WSDL
			// URL url = new URL(documenturl+"/MotorQQDataImplService?WSDL");
			QName servicename = new QName("http://motor.wcservices.dsp.etiqa.com/",
					"MotorInsuranceDocGenWSImplService");
			QName portname = new QName("http://motor.wcservices.dsp.etiqa.com/", "MotorInsuranceDocGenWSImplPort");
			Service service = Service.create(null, servicename);
			service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
					documenturl + "/MotorInsuranceDocGenWSImplService");
			Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class, Service.Mode.MESSAGE);
			MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
			SOAPMessage request1 = mf.createMessage();
			SOAPPart part = request1.getSOAPPart();
			SOAPEnvelope env = part.getEnvelope();
			SOAPBody body = env.getBody();
			// resp = body;
			SOAPElement operation = body.addChildElement(methodName, "mot", "http://motor.wcservices.dsp.etiqa.com/");
			SOAPElement element_param = operation.addChildElement("QQID");
			element_param.addTextNode(qqID);
			SOAPElement element_param_lang = operation.addChildElement("langValue");
			element_param_lang.addTextNode("lan_en");

			request1.saveChanges();
			String result = null;

			if (request1 != null) {
				ByteArrayOutputStream baos = null;
				try {
					baos = new ByteArrayOutputStream();
					request1.writeTo(baos);
					result = baos.toString();
				} catch (Exception e) {
				} finally {
					if (baos != null) {
						try {
							baos.close();
						} catch (IOException ioe) {
						}
					}
				}
			}
			// System.out.println(result+"document insert");
			// resCode=body ;

			SOAPMessage response1 = dispatch.invoke(request1);
			SOAPBody responsebody = response1.getSOAPBody();
			System.out.println("regenerate doc Motor" + responsebody);
			/*
			 * java.util.Iterator otcupdates = responsebody.getChildElements(); while
			 * (otcupdates.hasNext()) { SOAPElement otcupdates1 =
			 * (SOAPElement)otcupdates.next();
			 *
			 * java.util.Iterator i = otcupdates1.getChildElements(); while( i.hasNext() ){
			 *
			 * SOAPElement e = (SOAPElement)i.next(); java.util.Iterator m =
			 * e.getChildElements(); while( m.hasNext() ){ SOAPElement e2 =
			 * (SOAPElement)m.next();
			 *
			 * } } }
			 */

		} catch (Exception e) {

			emailStatus = "Fail";
			// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
			e.printStackTrace();
		}

		// System.out.println(sendEmailStatus);
		ReportDAO dao = new ReportDAOImpl();
		List<TransactionalReport> TranDetail = dao.getTransactionDetail(id);
		request.setAttribute("data", TranDetail);
		request.setAttribute("statusEm", emailStatus);

		forward(request, response, redirect);

		// ${pageContext.request.contextPath}/newCarDetails/'+obj.paymentTrxID+'/'+obj.policyNo
	}
}
