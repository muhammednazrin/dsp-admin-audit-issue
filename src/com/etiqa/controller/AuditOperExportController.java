package com.etiqa.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.etiqa.common.DB.ConnectionFactory;

import oracle.jdbc.OracleTypes;

@WebServlet("/getOperatExport")
public class AuditOperExportController extends HttpServlet {

	private static ConnectionFactory db1;
	private static Connection connection;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String fromdate = request.getParameter("fromdate");
		String todate = request.getParameter("todate");

		SimpleDateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date;
		Date toDateparse;

		try {

			date = originalFormat.parse(fromdate);
			toDateparse = originalFormat.parse(todate);

			request.setAttribute("fromdate", fromdate);
			request.setAttribute("todate", todate);

			fromdate = targetFormat.format(date);
			todate = targetFormat.format(toDateparse);

		} catch (ParseException ex) {

		}

		try {

			HSSFWorkbook hwb = new HSSFWorkbook();
			HSSFSheet sheet = hwb.createSheet("new sheet");

			HSSFRow rowhead = sheet.createRow(0);

			rowhead.createCell(0).setCellValue("O_ID");
			rowhead.createCell(1).setCellValue("ID");
			rowhead.createCell(2).setCellValue("O_OPERATOR_CODE");
			rowhead.createCell(3).setCellValue("OPERATOR_CODE");
			rowhead.createCell(4).setCellValue("O_REGISTRATION_DATE");
			rowhead.createCell(5).setCellValue("REGISTRATION_DATE");
			rowhead.createCell(6).setCellValue("O_OPERATOR_CATEGORY");
			rowhead.createCell(7).setCellValue("OPERATOR_CATEGORY");
			rowhead.createCell(8).setCellValue("O_ENTITY");
			rowhead.createCell(9).setCellValue("ENTITY");
			rowhead.createCell(10).setCellValue("O_ANNUAL_SALES_TARGET");
			rowhead.createCell(11).setCellValue("ANNUAL_SALES_TARGET");
			rowhead.createCell(12).setCellValue("O_AGENT_ID");
			rowhead.createCell(13).setCellValue("AGENT_ID");
			rowhead.createCell(14).setCellValue("O_OPERATOR_NAME");
			rowhead.createCell(15).setCellValue("OPERATOR_NAME");
			rowhead.createCell(16).setCellValue("O_ID_TYPE_NO");
			rowhead.createCell(17).setCellValue("ID_TYPE_NO");
			rowhead.createCell(18).setCellValue("O_PHONE_NUMBER");
			rowhead.createCell(19).setCellValue("PHONE_NUMBER");
			rowhead.createCell(20).setCellValue("O_EMAIL");
			rowhead.createCell(21).setCellValue("EMAIL");
			rowhead.createCell(22).setCellValue("O_ID_NO");
			rowhead.createCell(23).setCellValue("ID_NO");
			rowhead.createCell(24).setCellValue("O_PHONE_TYPE");
			rowhead.createCell(25).setCellValue("PHONE_TYPE");
			rowhead.createCell(26).setCellValue("O_OPERATOR_TYPE");
			rowhead.createCell(27).setCellValue("OPERATOR_TYPE");
			rowhead.createCell(28).setCellValue("O_USERNAME");
			rowhead.createCell(29).setCellValue("USERNAME");
			rowhead.createCell(30).setCellValue("O_ADDRESS1");
			rowhead.createCell(31).setCellValue("ADDRESS1");
			rowhead.createCell(32).setCellValue("O_ADDRESS2");
			rowhead.createCell(33).setCellValue("ADDRESS2");
			rowhead.createCell(34).setCellValue("O_ADDRESS3");
			rowhead.createCell(35).setCellValue("ADDRESS3");
			rowhead.createCell(36).setCellValue("O_CITY");
			rowhead.createCell(37).setCellValue("CITY");
			rowhead.createCell(38).setCellValue("O_STATE");
			rowhead.createCell(39).setCellValue("STATE");
			rowhead.createCell(40).setCellValue("O_COUNTRY");
			rowhead.createCell(41).setCellValue("COUNTRY");
			rowhead.createCell(42).setCellValue("O_POST_CODE");
			rowhead.createCell(43).setCellValue("POST_CODE");
			rowhead.createCell(44).setCellValue("O_AGENT_REFERAL_ID");
			rowhead.createCell(45).setCellValue("AGENT_REFERAL_ID");
			rowhead.createCell(46).setCellValue("O_OPERATOR_USERNAME");
			rowhead.createCell(47).setCellValue("OPERATOR_USERNAME");
			rowhead.createCell(48).setCellValue("O_EMAIL_PASS_CLICKED");
			rowhead.createCell(49).setCellValue("EMAIL_PASS_CLICKED");
			rowhead.createCell(50).setCellValue("O_EMAIL_PASS_EXPIRY");
			rowhead.createCell(51).setCellValue("EMAIL_PASS_EXPIRY");
			rowhead.createCell(52).setCellValue("WHO");
			rowhead.createCell(53).setCellValue("DATE_WHEN");
			rowhead.createCell(54).setCellValue("RECORD_COMMENT");

			ResultSet resultSet = null;
			int i = 1;

			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{call GET_AUDIT_OPT_REGISTRATION(?,?,?)}");
			cstmt.setString(1, fromdate);
			cstmt.setString(2, todate);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);

			cstmt.execute();

			resultSet = (ResultSet) cstmt.getObject(3);

			while (resultSet.next()) {

				HSSFRow row = sheet.createRow(i);

				try {

					if (resultSet.getString("O_ID").equals(resultSet.getString("ID"))) {
						row.createCell(0).setCellValue(resultSet.getString("O_ID"));
						row.createCell(1).setCellValue("");

					} else {
						row.createCell(0).setCellValue(resultSet.getString("O_ID"));
						row.createCell(1).setCellValue(resultSet.getString("ID"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("OPERATOR_CODE").equals(resultSet.getString("O_OPERATOR_CODE"))) {
						row.createCell(2).setCellValue(resultSet.getString("O_OPERATOR_CODE"));
						row.createCell(3).setCellValue("");

					} else {
						row.createCell(2).setCellValue(resultSet.getString("O_OPERATOR_CODE"));
						row.createCell(3).setCellValue(resultSet.getString("OPERATOR_CODE"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("REGISTRATION_DATE").equals(resultSet.getString("O_REGISTRATION_DATE"))) {
						row.createCell(4).setCellValue(resultSet.getString("O_REGISTRATION_DATE"));
						row.createCell(5).setCellValue("");

					} else {
						row.createCell(4).setCellValue(resultSet.getString("O_REGISTRATION_DATE"));
						row.createCell(5).setCellValue(resultSet.getString("REGISTRATION_DATE"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("OPERATOR_CATEGORY").equals(resultSet.getString("O_OPERATOR_CATEGORY"))) {
						row.createCell(6).setCellValue(resultSet.getString("O_OPERATOR_CATEGORY"));
						row.createCell(7).setCellValue("");

					} else {
						row.createCell(6).setCellValue(resultSet.getString("O_OPERATOR_CATEGORY"));
						row.createCell(7).setCellValue(resultSet.getString("OPERATOR_CATEGORY"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("O_ENTITY").equals(resultSet.getString("ENTITY"))) {
						row.createCell(8).setCellValue(resultSet.getString("O_ENTITY"));
						row.createCell(9).setCellValue("");
					} else {
						row.createCell(8).setCellValue(resultSet.getString("O_ENTITY"));
						row.createCell(9).setCellValue(resultSet.getString("ENTITY"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("O_ANNUAL_SALES_TARGET")
							.equals(resultSet.getString("ANNUAL_SALES_TARGET"))) {
						row.createCell(10).setCellValue(resultSet.getString("O_ANNUAL_SALES_TARGET"));
						row.createCell(11).setCellValue("");
					} else {
						row.createCell(10).setCellValue(resultSet.getString("O_ANNUAL_SALES_TARGET"));
						row.createCell(11).setCellValue(resultSet.getString("ANNUAL_SALES_TARGET"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("AGENT_ID").equals(resultSet.getString("O_AGENT_ID"))) {
						row.createCell(12).setCellValue(resultSet.getString("O_AGENT_ID"));
						row.createCell(13).setCellValue("");

					} else {
						row.createCell(12).setCellValue(resultSet.getString("O_AGENT_ID"));
						row.createCell(13).setCellValue(resultSet.getString("AGENT_ID"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("OPERATOR_NAME").equals(resultSet.getString("O_OPERATOR_NAME"))) {
						row.createCell(14).setCellValue(resultSet.getString("O_OPERATOR_NAME"));
						row.createCell(15).setCellValue("");

					} else {
						row.createCell(14).setCellValue(resultSet.getString("O_OPERATOR_NAME"));
						row.createCell(15).setCellValue(resultSet.getString("OPERATOR_NAME"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("ID_TYPE_NO").equals(resultSet.getString("O_ID_TYPE_NO"))) {
						row.createCell(16).setCellValue(resultSet.getString("O_ID_TYPE_NO"));
						row.createCell(17).setCellValue("");

					} else {
						row.createCell(16).setCellValue(resultSet.getString("O_ID_TYPE_NO"));
						row.createCell(17).setCellValue(resultSet.getString("ID_TYPE_NO"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("PHONE_NUMBER").equals(resultSet.getString("O_PHONE_NUMBER"))) {
						row.createCell(18).setCellValue(resultSet.getString("O_PHONE_NUMBER"));
						row.createCell(19).setCellValue("");

					} else {
						row.createCell(18).setCellValue(resultSet.getString("O_PHONE_NUMBER"));
						row.createCell(19).setCellValue(resultSet.getString("PHONE_NUMBER"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("EMAIL").equals(resultSet.getString("O_EMAIL"))) {
						row.createCell(20).setCellValue(resultSet.getString("O_EMAIL"));
						row.createCell(21).setCellValue("");

					} else {
						row.createCell(20).setCellValue(resultSet.getString("O_EMAIL"));
						row.createCell(21).setCellValue(resultSet.getString("EMAIL"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("ID_NO").equals(resultSet.getString("O_ID_NO"))) {
						row.createCell(22).setCellValue(resultSet.getString("O_ID_NO"));
						row.createCell(23).setCellValue("");

					} else {
						row.createCell(22).setCellValue(resultSet.getString("O_ID_NO"));
						row.createCell(23).setCellValue(resultSet.getString("ID_NO"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("PHONE_TYPE").equals(resultSet.getString("O_PHONE_TYPE"))) {
						row.createCell(24).setCellValue(resultSet.getString("O_PHONE_TYPE"));
						row.createCell(25).setCellValue("");

					} else {
						row.createCell(24).setCellValue(resultSet.getString("O_PHONE_TYPE"));
						row.createCell(25).setCellValue(resultSet.getString("PHONE_TYPE"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("OPERATOR_TYPE").equals(resultSet.getString("O_OPERATOR_TYPE"))) {
						row.createCell(26).setCellValue(resultSet.getString("O_OPERATOR_TYPE"));
						row.createCell(27).setCellValue("");

					} else {
						row.createCell(26).setCellValue(resultSet.getString("O_OPERATOR_TYPE"));
						row.createCell(27).setCellValue(resultSet.getString("OPERATOR_TYPE"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("USERNAME").equals(resultSet.getString("O_USERNAME"))) {
						row.createCell(28).setCellValue(resultSet.getString("O_USERNAME"));
						row.createCell(29).setCellValue("");

					} else {
						row.createCell(28).setCellValue(resultSet.getString("O_USERNAME"));
						row.createCell(29).setCellValue(resultSet.getString("USERNAME"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("ADDRESS1").equals(resultSet.getString("O_ADDRESS1"))) {
						row.createCell(30).setCellValue(resultSet.getString("O_ADDRESS1"));
						row.createCell(31).setCellValue("");

					} else {
						row.createCell(30).setCellValue(resultSet.getString("O_ADDRESS1"));
						row.createCell(31).setCellValue(resultSet.getString("ADDRESS1"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("ADDRESS2").equals(resultSet.getString("O_ADDRESS2"))) {
						row.createCell(32).setCellValue(resultSet.getString("O_ADDRESS2"));
						row.createCell(33).setCellValue("");

					} else {
						row.createCell(32).setCellValue(resultSet.getString("O_ADDRESS2"));
						row.createCell(33).setCellValue(resultSet.getString("ADDRESS2"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("ADDRESS3").equals(resultSet.getString("O_ADDRESS3"))) {
						row.createCell(34).setCellValue(resultSet.getString("O_ADDRESS3"));
						row.createCell(35).setCellValue("");

					} else {
						row.createCell(34).setCellValue(resultSet.getString("O_ADDRESS3"));
						row.createCell(35).setCellValue(resultSet.getString("ADDRESS3"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("CITY").equals(resultSet.getString("O_CITY"))) {
						row.createCell(36).setCellValue(resultSet.getString("O_CITY"));
						row.createCell(37).setCellValue("");

					} else {
						row.createCell(36).setCellValue(resultSet.getString("O_CITY"));
						row.createCell(37).setCellValue(resultSet.getString("CITY"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("STATE").equals(resultSet.getString("O_STATE"))) {
						row.createCell(38).setCellValue(resultSet.getString("O_STATE"));
						row.createCell(39).setCellValue("");

					} else {
						row.createCell(38).setCellValue(resultSet.getString("O_STATE"));
						row.createCell(39).setCellValue(resultSet.getString("STATE"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("COUNTRY").equals(resultSet.getString("O_COUNTRY"))) {
						row.createCell(40).setCellValue(resultSet.getString("O_COUNTRY"));
						row.createCell(41).setCellValue("");

					} else {
						row.createCell(40).setCellValue(resultSet.getString("O_COUNTRY"));
						row.createCell(41).setCellValue(resultSet.getString("COUNTRY"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("POST_CODE").equals(resultSet.getString("O_POST_CODE"))) {
						row.createCell(42).setCellValue(resultSet.getString("O_POST_CODE"));
						row.createCell(43).setCellValue("");

					} else {
						row.createCell(42).setCellValue(resultSet.getString("O_POST_CODE"));
						row.createCell(43).setCellValue(resultSet.getString("POST_CODE"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("AGENT_REFERAL_ID").equals(resultSet.getString("O_AGENT_REFERAL_ID"))) {
						row.createCell(44).setCellValue(resultSet.getString("O_AGENT_REFERAL_ID"));
						row.createCell(45).setCellValue("");

					} else {
						row.createCell(44).setCellValue(resultSet.getString("O_AGENT_REFERAL_ID"));
						row.createCell(45).setCellValue(resultSet.getString("AGENT_REFERAL_ID"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("OPERATOR_USERNAME").equals(resultSet.getString("O_OPERATOR_USERNAME"))) {
						row.createCell(46).setCellValue(resultSet.getString("O_OPERATOR_USERNAME"));
						row.createCell(47).setCellValue("");

					} else {
						row.createCell(46).setCellValue(resultSet.getString("O_OPERATOR_USERNAME"));
						row.createCell(47).setCellValue(resultSet.getString("OPERATOR_USERNAME"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("EMAIL_PASS_CLICKED").equals(resultSet.getString("O_EMAIL_PASS_CLICKED"))) {
						row.createCell(48).setCellValue(resultSet.getString("O_EMAIL_PASS_CLICKED"));
						row.createCell(49).setCellValue("");
					} else {
						row.createCell(48).setCellValue(resultSet.getString("O_EMAIL_PASS_CLICKED"));
						row.createCell(49).setCellValue(resultSet.getString("EMAIL_PASS_CLICKED"));
					}
				} catch (Exception ex) {
				}

				try {

					if (resultSet.getString("EMAIL_PASS_EXPIRY").equals(resultSet.getString("O_EMAIL_PASS_EXPIRY"))) {
						row.createCell(50).setCellValue(resultSet.getString("O_EMAIL_PASS_EXPIRY"));
						row.createCell(51).setCellValue("");
					} else {
						row.createCell(50).setCellValue(resultSet.getString("O_EMAIL_PASS_EXPIRY"));
						row.createCell(51).setCellValue(resultSet.getString("EMAIL_PASS_EXPIRY"));
					}
				} catch (Exception ex) {
				}

				row.createCell(52).setCellValue(resultSet.getString("WHO"));
				row.createCell(53).setCellValue(resultSet.getString("DATE_WHEN"));
				row.createCell(54).setCellValue(resultSet.getString("RECORD_COMMENT"));

				i++;

			}

			ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
			hwb.write(outByteStream);
			byte[] outArray = outByteStream.toByteArray();
			response.setContentType("application/ms-excel");
			response.setContentLength(outArray.length);
			response.setHeader("Expires:", "0");
			response.setHeader("Content-Disposition", "attachment; filename=Operator_Audit_Log.xls");
			OutputStream outStream = response.getOutputStream();
			outStream.write(outArray);
			outStream.flush();

			request.setAttribute("fromdate", fromdate);
			request.setAttribute("todate", todate);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("AuditOperatorReport.jsp").forward(request, response);

	}

}
