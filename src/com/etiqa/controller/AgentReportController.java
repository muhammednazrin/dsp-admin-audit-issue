package com.etiqa.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.etiqa.DAO.AgentReportDAO;
import com.etiqa.DAO.AgentReportDAOImpl;
import com.etiqa.common.BaseServlet;
import com.etiqa.model.report.TransactionalReport;

@WebServlet("/agentReport")
public class AgentReportController extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	public void handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action = request.getParameter("action");
		System.out.println(">>> action: " + "[" + action + "]");
		if (action.equals(ACTION_EVENT_LIST)) {
			list(request, response);

		}

	}

	private void list(HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		String ProductType = null;
		String ProductEntity = null;
		String Status = null;
		String PolicyCertificateNo = null;
		String DateFrom = null;
		String DateTo = null;
		String NRIC = null;
		String ReceiptNo = null;
		String PlateNo = null;
		String eCoverStatus = null;
		/*
		 * String agentCode = null; String agentName= null; String marketerCode= null;
		 * String marketerName= null;
		 */
		String salesEntity = null;

		BigDecimal number1 = new BigDecimal(0.00);
		BigDecimal number2 = new BigDecimal(0.00);
		BigDecimal number3 = new BigDecimal(0.00);

		try {
			if (request.getMethod().equals("POST"))

			{
				session.setAttribute("ProductType", request.getParameter("productType").trim());
				session.setAttribute("ProductEntity", request.getParameter("productEntity").trim());
				session.setAttribute("Status", request.getParameter("status").trim());
				session.setAttribute("PolicyCertificateNo", request.getParameter("policyNo").trim());
				session.setAttribute("NRIC", request.getParameter("NRIC").trim());
				session.setAttribute("ReceiptNo", request.getParameter("receiptNo").trim());
				/*
				 * session.setAttribute("agentCode", request.getParameter("agentCode").trim());
				 * session.setAttribute("agentName", request.getParameter("agentName").trim());
				 * session.setAttribute("marketerCode",
				 * request.getParameter("marketerCode").trim());
				 * session.setAttribute("marketerName",
				 * request.getParameter("marketerName").trim());
				 */// session.setAttribute("salesEntity",
					// request.getParameter("salesEntity").trim());
				session.setAttribute("dateFrom", request.getParameter("dateFrom").trim());
				session.setAttribute("dateTo", request.getParameter("dateTo").trim());
				session.setAttribute("DateFrom", "");
				session.setAttribute("DateTo", "");

				if (request.getParameter("plateNo") != null) {
					session.setAttribute("PlateNo", request.getParameter("plateNo").trim());

				}

				SimpleDateFormat ft2 = new SimpleDateFormat("dd/MM/yyyy");

				if (request.getParameter("dateFrom").trim().length() > 0) {

					DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
					Date date = null;
					try {
						date = format.parse(request.getParameter("dateFrom").trim());

						System.out.println("From Date: " + ft2.format(date));

						session.setAttribute("DateFrom", ft2.format(date));

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println(date);

				}

				if (request.getParameter("dateTo").trim().length() > 0) {

					DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
					Date dateTo = null;
					try {
						dateTo = format.parse(request.getParameter("dateTo").trim());

						System.out.println("To Date: " + ft2.format(dateTo));

						session.setAttribute("DateTo", ft2.format(dateTo));

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println(dateTo);

				}

				if (request.getParameter("dateFrom").trim().length() > 0
						&& request.getParameter("dateTo").trim().length() == 0) {

					Date dNow = new Date();
					SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");

					System.out.println("Current Date: " + ft.format(dNow));

					session.setAttribute("DateTo", ft.format(dNow));

				}

			}

			ProductType = (String) session.getAttribute("ProductType");
			ProductEntity = (String) session.getAttribute("ProductEntity");
			Status = (String) session.getAttribute("Status");
			PolicyCertificateNo = (String) session.getAttribute("PolicyCertificateNo");
			NRIC = (String) session.getAttribute("NRIC");
			ReceiptNo = (String) session.getAttribute("ReceiptNo");

			// agentCode = (String) session.getAttribute("agentCode");
			// agentName = (String) session.getAttribute("agentName");
			// marketerCode = (String) session.getAttribute("marketerCode");
			// marketerName = (String) session.getAttribute("marketerName");
			// salesEntity = (String) session.getAttribute("salesEntity");

			DateFrom = (String) session.getAttribute("DateFrom");
			DateTo = (String) session.getAttribute("DateTo");
			PlateNo = (String) session.getAttribute("PlateNo");
			// ProductEntity=(String) session.getAttribute("ProductEntity");

			int StartRow = Integer.parseInt(request.getParameter("startRow"));
			int EndRow = Integer.parseInt(request.getParameter("endRow"));

			if (ProductType.equals("content") || ProductType.equals("building")
					|| ProductType.equals("building_content")) {

				AgentReportDAO dao = new AgentReportDAOImpl();// daoFactory.getAgentDAO();
				List<TransactionalReport> Travlrpt = dao.findByHohh(ProductType, ProductEntity, Status,
						PolicyCertificateNo, DateFrom, DateTo, NRIC, ReceiptNo, StartRow, EndRow);
				request.setAttribute("data", Travlrpt);
				forward(request, response, "admin-report-agent.jsp");

			}

			if (Status.trim().equals("R") && ProductType.equals("MI")) {

				AgentReportDAO dao = new AgentReportDAOImpl();// daoFactory.getAgentDAO();
				List<TransactionalReport> Travlrpt = dao.rejectRecordMI(ProductType,
						// ProductEntity,
						Status, PolicyCertificateNo, DateFrom, DateTo, NRIC, ReceiptNo, StartRow, EndRow);
				request.setAttribute("data", Travlrpt);
				forward(request, response, "admin-report-agent.jsp");

			}

			else if (Status.trim().equals("O") && ProductType.equals("MI")) {

				AgentReportDAO dao = new AgentReportDAOImpl();// daoFactory.getAgentDAO();
				List<TransactionalReport> Travlrpt = dao.incompleteRecordMI(ProductType,
						// ProductEntity,
						Status, PolicyCertificateNo, DateFrom, DateTo, NRIC, ReceiptNo, StartRow, EndRow);
				request.setAttribute("data", Travlrpt);
				forward(request, response, "admin-report-agent.jsp");

			}

			else if (Status.trim().equals("R") && ProductType.equals("TL")) {

				AgentReportDAO dao = new AgentReportDAOImpl();// daoFactory.getAgentDAO();
				List<TransactionalReport> Travlrpt = dao.rejectRecordTL(ProductType,
						// ProductEntity,
						Status, PolicyCertificateNo, DateFrom, DateTo, NRIC, ReceiptNo, StartRow, EndRow);
				request.setAttribute("data", Travlrpt);
				forward(request, response, "admin-report-agent.jsp");

			}

			else if (Status.trim().equals("O") && ProductType.equals("TL")) {

				AgentReportDAO dao = new AgentReportDAOImpl();// daoFactory.getAgentDAO();
				List<TransactionalReport> Travlrpt = dao.incompleteRecordTL(ProductType,
						// ProductEntity,
						Status, PolicyCertificateNo, DateFrom, DateTo, NRIC, ReceiptNo, StartRow, EndRow);
				request.setAttribute("data", Travlrpt);
				forward(request, response, "admin-report-agent.jsp");

			}

			else if (Status.trim().equals("") && ProductType.equals("TL")) {
				AgentReportDAO dao = new AgentReportDAOImpl();// daoFactory.getAgentDAO();
				List<TransactionalReport> Travlrpt = dao.findByProductType(ProductType, ProductEntity, Status,
						PolicyCertificateNo, DateFrom, DateTo, NRIC, ReceiptNo, StartRow, EndRow);

				List<TransactionalReport> Travlrpt2 = dao.rejectRecordTL(ProductType,
						// ProductEntity,
						Status, PolicyCertificateNo, DateFrom, DateTo, NRIC, ReceiptNo, StartRow, EndRow);

				List<TransactionalReport> Travlrpt3 = dao.incompleteRecordTL(ProductType,
						// ProductEntity,
						Status, PolicyCertificateNo, DateFrom, DateTo, NRIC, ReceiptNo, StartRow, EndRow);

				// request.setAttribute("data", Travlrpt);
				// request.setAttribute("data2", Travlrpt2);

				List newList = new ArrayList<>(Travlrpt.size() + Travlrpt2.size() + Travlrpt3.size());
				newList.addAll(Travlrpt);
				newList.addAll(Travlrpt2);
				newList.addAll(Travlrpt3);

				request.setAttribute("data", newList);

				DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(Locale.US);
				format.setParseBigDecimal(true);

				if (Travlrpt.size() > 0) {

					number1 = (BigDecimal) format.parse(Travlrpt.get(0).getTotalAmount());
				}

				if (Travlrpt2.size() > 0) {

					number2 = (BigDecimal) format.parse(Travlrpt2.get(0).getTotalAmount());
				}

				if (Travlrpt3.size() > 0) {

					number3 = (BigDecimal) format.parse(Travlrpt3.get(0).getTotalAmount());
				}

				/*
				 * BigDecimal overallAmount = number1.add(number2); overallAmount=
				 * overallAmount.add(number3);
				 * request.setAttribute("totalAmount",overallAmount);
				 */
				double commisionAmt = 0.1;
				BigDecimal overallAmount = number1.add(number2);
				overallAmount = overallAmount.add(number3);
				request.setAttribute("totalAmount", overallAmount);

				BigDecimal overallCommAmount = new BigDecimal(Double.toString(commisionAmt));
				overallCommAmount = overallAmount.multiply(overallCommAmount);
				request.setAttribute("commAmount", overallCommAmount);

				// System.out.println("totalAmount 1 = "+Travlrpt.get(0).getTotalAmount());
				// System.out.println("totalAmount 2 = "+Travlrpt2.get(0).getTotalAmount());
				// System.out.println("overall totalAmount =
				// "+request.getAttribute("totalAmount"));

				forward(request, response, "admin-report-agent.jsp");

			}

			else {
				System.out.println("agentCode::::::::" + NRIC);
				AgentReportDAO dao = new AgentReportDAOImpl();// daoFactory.getAgentDAO();
				List<TransactionalReport> Travlrpt = dao.findByProductType(ProductType, ProductEntity, Status,
						PolicyCertificateNo, DateFrom, DateTo, NRIC, ReceiptNo,
						/*
						 * agentCode, agentName, marketerCode, marketerName, salesEntity,
						 */
						StartRow, EndRow);
				request.setAttribute("data", Travlrpt);
				forward(request, response, "admin-report-agent.jsp");
				/*
				 * ReportDAO dao = new ReportDAOImpl();// daoFactory.getAgentDAO();
				 * List<TransactionalReport> Travlrpt = dao.findByProductType( ProductType,
				 * ProductEntity, Status, PolicyCertificateNo, DateFrom, DateTo, NRIC,
				 * ReceiptNo, StartRow, EndRow); request.setAttribute("data", Travlrpt);
				 * forward(request, response, "admin-report-agent.jsp");
				 */
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
