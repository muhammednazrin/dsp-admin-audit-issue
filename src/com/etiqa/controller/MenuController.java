package com.etiqa.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.etiqa.DAO.RolesManagement_DAO;
import com.etiqa.admin.UserRole;

public class MenuController extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String strusername = "xyz";
		List<UserRole> myDataList = new RolesManagement_DAO().getUsersRolesMenu(strusername);
		request.setAttribute("myDataListMenu", myDataList);
		request.getRequestDispatcher("menu.jsp").forward(request, response);
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("menu.jsp").forward(request, response);
	}

}
