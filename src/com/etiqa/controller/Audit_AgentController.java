package com.etiqa.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.etiqa.DAO.Audit_Agent_DAO;
import com.etiqa.dsp.dao.common.pojo.Audit_Agent_Bean;

@WebServlet("/getAuditAgentReport")
public class Audit_AgentController extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String fromdate = request.getParameter("fromdate");
		String todate = request.getParameter("todate");

		System.out.println("B fromdate : " + fromdate);
		System.out.println("B todate : " + todate);

		request.setAttribute("fromdate", fromdate);
		request.setAttribute("todate", todate);

		System.out.println("fromdate : " + fromdate);
		System.out.println("todate : " + todate);

		List<Audit_Agent_Bean> myDataList = new Audit_Agent_DAO().getReportData(fromdate, todate);

		request.setAttribute("myDataList", myDataList);
		request.setAttribute("fromdate", fromdate);
		request.setAttribute("todate", todate);
		request.getRequestDispatcher("AuditAgentReport.jsp").forward(request, response);

	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/*
		 * DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd"); DateFormat
		 * dateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
		 *
		 * Date date = new Date();
		 */
		request.getRequestDispatcher("AuditAgentReport.jsp").forward(request, response);

	}
}
