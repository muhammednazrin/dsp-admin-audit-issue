package com.etiqa.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.etiqa.DAO.Audit_Operator_DAO;
import com.etiqa.dsp.dao.common.pojo.Audit_Operator_Bean;

@WebServlet("/getAuditOperatorReport")
public class Audit_OperatorController extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// it will fire on a POST request (like submitting the form using POST
		// method)
		String fromdate = request.getParameter("fromdate");

		String todate = request.getParameter("todate");
		SimpleDateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date;
		Date toDateparse;
		try {
			date = originalFormat.parse(fromdate);
			toDateparse = originalFormat.parse(todate);

			request.setAttribute("fromdate", fromdate);
			request.setAttribute("todate", todate);
			fromdate = targetFormat.format(date);
			todate = targetFormat.format(toDateparse);

		} catch (ParseException ex) {
			// Handle Exception.
		}

		List<Audit_Operator_Bean> myDataList = new Audit_Operator_DAO().getReportData(fromdate, todate);
		request.setAttribute("myDataList", myDataList);
		request.setAttribute("fromdate", fromdate);
		request.setAttribute("todate", todate);

		request.getRequestDispatcher("AuditOperatorReport.jsp").forward(request, response);
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy");

		Date date = new Date();

		request.getRequestDispatcher("AuditOperatorReport.jsp").forward(request, response);

	}

}
