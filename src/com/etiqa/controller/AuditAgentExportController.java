package com.etiqa.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.dsp.dao.common.pojo.Audit_Customer_Bean;

import oracle.jdbc.OracleTypes;

@WebServlet("/getExportAgent")
public class AuditAgentExportController extends HttpServlet {

	private static ConnectionFactory db1;
	private static Connection connection;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String fromdate = request.getParameter("fromdate");
		String todate = request.getParameter("todate");

		SimpleDateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date;
		Date toDateparse;

		try {

			date = originalFormat.parse(fromdate);
			toDateparse = originalFormat.parse(todate);

			request.setAttribute("fromdate", fromdate);
			request.setAttribute("todate", todate);

			fromdate = targetFormat.format(date);
			todate = targetFormat.format(toDateparse);

		} catch (ParseException ex) {

		}

		try {

			HSSFWorkbook hwb = new HSSFWorkbook();
			HSSFSheet sheet = hwb.createSheet("new sheet");

			HSSFRow rowhead = sheet.createRow(0);

			rowhead.createCell(0).setCellValue("O_ID");
			rowhead.createCell(1).setCellValue("ID");
			rowhead.createCell(2).setCellValue("O_REFERENCE_NUMBER");
			rowhead.createCell(3).setCellValue("REFERENCE_NUMBER");
			rowhead.createCell(4).setCellValue("O_REGISTRATION_DATE");
			rowhead.createCell(5).setCellValue("REGISTRATION_DATE");
			rowhead.createCell(6).setCellValue("O_AGENT_CATEGORY");
			rowhead.createCell(7).setCellValue("AGENT_CATEGORY");
			rowhead.createCell(8).setCellValue("O_INSURANCE_TYPE");
			rowhead.createCell(9).setCellValue("INSURANCE_TYPE");
			rowhead.createCell(10).setCellValue("O_STATUS");
			rowhead.createCell(11).setCellValue("STATUS");
			rowhead.createCell(12).setCellValue("O_ANNUAL_SALES_TARGET");
			rowhead.createCell(13).setCellValue("ANNUAL_SALES_TARGET");
			rowhead.createCell(14).setCellValue("O_AGENT_REFERAL_ID");
			rowhead.createCell(15).setCellValue("AGENT_REFERAL_ID");
			rowhead.createCell(16).setCellValue("O_AGENT_NAME");
			rowhead.createCell(17).setCellValue("AGENT_NAME");
			rowhead.createCell(18).setCellValue("O_ID_TYPE_NO");
			rowhead.createCell(19).setCellValue("ID_TYPE_NO");
			rowhead.createCell(20).setCellValue("O_PHONE_NUMBER");
			rowhead.createCell(21).setCellValue("PHONE_NUMBER");
			rowhead.createCell(22).setCellValue("O_EMAIL");
			rowhead.createCell(23).setCellValue("EMAIL");
			rowhead.createCell(24).setCellValue("O_ADDRESS1");
			rowhead.createCell(25).setCellValue("ADDRESS1");
			rowhead.createCell(26).setCellValue("O_AGENT_CODE");
			rowhead.createCell(27).setCellValue("AGENT_CODE");
			rowhead.createCell(28).setCellValue("O_ID_NO");
			rowhead.createCell(29).setCellValue("ID_NO");
			rowhead.createCell(30).setCellValue("O_PHONE_TYPE");
			rowhead.createCell(31).setCellValue("PHONE_TYPE");
			rowhead.createCell(32).setCellValue("O_CITY");
			rowhead.createCell(33).setCellValue("CITY");
			rowhead.createCell(34).setCellValue("O_STATE");
			rowhead.createCell(35).setCellValue("STATE");
			rowhead.createCell(36).setCellValue("O_COUNTRY");
			rowhead.createCell(37).setCellValue("COUNTRY");
			rowhead.createCell(38).setCellValue("O_AGENT_ALIAS_NAME");
			rowhead.createCell(39).setCellValue("AGENT_ALIAS_NAME");
			rowhead.createCell(40).setCellValue("O_AGENT_PRODUCT_SELECTION");
			rowhead.createCell(41).setCellValue("AGENT_PRODUCT_SELECTION");
			rowhead.createCell(42).setCellValue("O_AGENT_REGISTRATION_DOC");
			rowhead.createCell(43).setCellValue("AGENT_REGISTRATION_DOC");
			rowhead.createCell(44).setCellValue("O_POST_CODE");
			rowhead.createCell(45).setCellValue("POST_CODE");
			rowhead.createCell(46).setCellValue("O_AGENT_LOGO");
			rowhead.createCell(47).setCellValue("AGENT_LOGO");
			rowhead.createCell(48).setCellValue("O_AGENT_DISCOUNT");
			rowhead.createCell(49).setCellValue("AGENT_DISCOUNT");
			rowhead.createCell(50).setCellValue("O_AGENT_COMMISSION");
			rowhead.createCell(51).setCellValue("AGENT_COMMISSION");
			rowhead.createCell(52).setCellValue("O_AGENT_TYPE");
			rowhead.createCell(53).setCellValue("AGENT_TYPE");
			rowhead.createCell(54).setCellValue("O_AGENT_USERNAME");
			rowhead.createCell(55).setCellValue("AGENT_USERNAME");
			/*
			 * rowhead.createCell(56).setCellValue("O_PASSWORD");
			 * rowhead.createCell(57).setCellValue("PASSWORD");
			 */
			rowhead.createCell(56).setCellValue("O_ADDRESS2");
			rowhead.createCell(57).setCellValue("ADDRESS2");
			rowhead.createCell(58).setCellValue("O_ADDRESS3");
			rowhead.createCell(59).setCellValue("ADDRESS3");
			rowhead.createCell(60).setCellValue("O_EMAIL_PASS_CLICKED");
			rowhead.createCell(61).setCellValue("EMAIL_PASS_CLICKED");
			rowhead.createCell(62).setCellValue("O_EMAIL_PASS_EXPIRY");
			rowhead.createCell(63).setCellValue("EMAIL_PASS_EXPIRY");
			rowhead.createCell(64).setCellValue("WHO");
			rowhead.createCell(65).setCellValue("DATE_WHEN");
			rowhead.createCell(66).setCellValue("RECORD_COMMENT");

			List<Audit_Customer_Bean> myDataList = new ArrayList<Audit_Customer_Bean>();

			ResultSet resultSet = null;
			Statement statement = null;
			int i = 1;

			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{call GET_AUDIT_AGENT_REGISTRATION(?,?,?)}");
			cstmt.setString(1, fromdate);
			cstmt.setString(2, todate);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);

			cstmt.execute();

			resultSet = (ResultSet) cstmt.getObject(3);

			while (resultSet.next()) {

				HSSFRow row = sheet.createRow(i);

				try {
					// if (resultSet.getString("O_ID").equals(resultSet.getString("ID"))) {
					row.createCell(0).setCellValue(resultSet.getString("O_ID"));
					row.createCell(1).setCellValue("");
					// } else {
					// row.createCell(0).setCellValue(resultSet.getString("O_ID"));
					// row.createCell(1).setCellValue(resultSet.getString("ID"));
					// }
				} catch (Exception ex) {
					row.createCell(0).setCellValue("");
					row.createCell(1).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_REFERENCE_NUMBER").equals(resultSet.getString("REFERENCE_NUMBER")))
					// {
					row.createCell(2).setCellValue(resultSet.getString("O_REFERENCE_NUMBER"));
					// row.createCell(3).setCellValue("");
					row.createCell(3).setCellValue(resultSet.getString("REFERENCE_NUMBER"));
					// } else {
					// row.createCell(2).setCellValue(resultSet.getString("O_REFERENCE_NUMBER"));
					row.createCell(3).setCellValue(resultSet.getString("REFERENCE_NUMBER"));
					// }
				} catch (Exception ex) {
					row.createCell(2).setCellValue("");
					row.createCell(3).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_REGISTRATION_DATE").equals(resultSet.getString("REGISTRATION_DATE")))
					// {
					row.createCell(4).setCellValue(resultSet.getString("O_REGISTRATION_DATE"));
					// row.createCell(5).setCellValue("");
					row.createCell(5).setCellValue(resultSet.getString("REGISTRATION_DATE"));
					// } else {
					// row.createCell(4).setCellValue(resultSet.getString("O_REGISTRATION_DATE"));
					row.createCell(5).setCellValue(resultSet.getString("REGISTRATION_DATE"));
					// }
				} catch (Exception ex) {
					row.createCell(4).setCellValue("");
					row.createCell(5).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_AGENT_CATEGORY").equals(resultSet.getString("AGENT_CATEGORY")))
					// {
					row.createCell(6).setCellValue(resultSet.getString("O_AGENT_CATEGORY"));
					// row.createCell(7).setCellValue("");
					row.createCell(7).setCellValue(resultSet.getString("AGENT_CATEGORY"));
					// } else {
					// row.createCell(6).setCellValue(resultSet.getString("O_AGENT_CATEGORY"));
					// row.createCell(7).setCellValue(resultSet.getString("AGENT_CATEGORY"));
					// }
				} catch (Exception ex) {
					row.createCell(6).setCellValue("");
					row.createCell(7).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_INSURANCE_TYPE").equals(resultSet.getString("INSURANCE_TYPE")))
					// {
					row.createCell(8).setCellValue(resultSet.getString("O_INSURANCE_TYPE"));
					// row.createCell(9).setCellValue("");
					row.createCell(9).setCellValue(resultSet.getString("INSURANCE_TYPE"));
					// } else {
					// row.createCell(8).setCellValue(resultSet.getString("O_INSURANCE_TYPE"));
					// row.createCell(9).setCellValue(resultSet.getString("INSURANCE_TYPE"));
					// }
				} catch (Exception ex) {
					row.createCell(8).setCellValue("");
					row.createCell(9).setCellValue("");
				}

				try {
					// if (resultSet.getString("O_STATUS").equals(resultSet.getString("STATUS"))) {
					row.createCell(10).setCellValue(resultSet.getString("O_STATUS"));
					// row.createCell(11).setCellValue("");
					row.createCell(11).setCellValue(resultSet.getString("STATUS"));
					// } else {
					// row.createCell(10).setCellValue(resultSet.getString("O_STATUS"));
					// row.createCell(11).setCellValue(resultSet.getString("STATUS"));
					// }
				} catch (Exception ex) {
					row.createCell(10).setCellValue("");
					row.createCell(11).setCellValue("");
				}

				try {
					// if (resultSet.getString("O_ANNUAL_SALES_TARGET")
					// .equals(resultSet.getString("ANNUAL_SALES_TARGET"))) {
					row.createCell(12).setCellValue(resultSet.getString("O_ANNUAL_SALES_TARGET"));
					// row.createCell(13).setCellValue("");
					row.createCell(13).setCellValue(resultSet.getString("ANNUAL_SALES_TARGET"));
					// } else {
					// row.createCell(12).setCellValue(resultSet.getString("O_ANNUAL_SALES_TARGET"));
					// row.createCell(13).setCellValue(resultSet.getString("ANNUAL_SALES_TARGET"));
					// }
				} catch (Exception ex) {
					row.createCell(12).setCellValue("");
					row.createCell(13).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_AGENT_REFERAL_ID").equals(resultSet.getString("AGENT_REFERAL_ID")))
					// {
					row.createCell(14).setCellValue(resultSet.getString("O_AGENT_REFERAL_ID"));
					// row.createCell(15).setCellValue("");
					row.createCell(15).setCellValue(resultSet.getString("AGENT_REFERAL_ID"));
					// } else {
					// row.createCell(14).setCellValue(resultSet.getString("O_AGENT_REFERAL_ID"));
					// row.createCell(15).setCellValue(resultSet.getString("AGENT_REFERAL_ID"));
					// }
				} catch (Exception ex) {
					row.createCell(14).setCellValue("");
					row.createCell(15).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_AGENT_NAME").equals(resultSet.getString("AGENT_NAME")))
					// {
					row.createCell(16).setCellValue(resultSet.getString("O_AGENT_NAME"));
					// row.createCell(17).setCellValue("");
					row.createCell(17).setCellValue(resultSet.getString("AGENT_NAME"));
					// } else {
					// row.createCell(16).setCellValue(resultSet.getString("O_AGENT_NAME"));
					// row.createCell(17).setCellValue(resultSet.getString("AGENT_NAME"));
					// }
				} catch (Exception ex) {
					row.createCell(16).setCellValue("");
					row.createCell(17).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_ID_TYPE_NO").equals(resultSet.getString("ID_TYPE_NO")))
					// {
					row.createCell(18).setCellValue(resultSet.getString("O_ID_TYPE_NO"));
					// row.createCell(19).setCellValue("");
					row.createCell(19).setCellValue(resultSet.getString("ID_TYPE_NO"));
					// } else {
					// row.createCell(18).setCellValue(resultSet.getString("O_ID_TYPE_NO"));
					// row.createCell(19).setCellValue(resultSet.getString("ID_TYPE_NO"));
					// }
				} catch (Exception ex) {
					row.createCell(18).setCellValue("");
					row.createCell(19).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_PHONE_NUMBER").equals(resultSet.getString("PHONE_NUMBER")))
					// {
					row.createCell(20).setCellValue(resultSet.getString("O_PHONE_NUMBER"));
					// row.createCell(21).setCellValue("");
					row.createCell(21).setCellValue(resultSet.getString("PHONE_NUMBER"));
					// } else {
					// row.createCell(20).setCellValue(resultSet.getString("O_PHONE_NUMBER"));
					// row.createCell(21).setCellValue(resultSet.getString("PHONE_NUMBER"));
					// }
				} catch (Exception ex) {
					row.createCell(20).setCellValue("");
					row.createCell(21).setCellValue("");
				}

				try {
					// if (resultSet.getString("O_EMAIL").equals(resultSet.getString("EMAIL"))) {
					row.createCell(22).setCellValue(resultSet.getString("O_EMAIL"));
					// row.createCell(23).setCellValue("");
					row.createCell(23).setCellValue(resultSet.getString("EMAIL"));
					// } else {
					// row.createCell(22).setCellValue(resultSet.getString("O_EMAIL"));
					// row.createCell(23).setCellValue(resultSet.getString("EMAIL"));
					// }
				} catch (Exception ex) {
					row.createCell(22).setCellValue("");
					row.createCell(23).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_ADDRESS1").equals(resultSet.getString("ADDRESS1"))) {
					row.createCell(24).setCellValue(resultSet.getString("O_ADDRESS1"));
					// row.createCell(25).setCellValue("");
					row.createCell(25).setCellValue(resultSet.getString("ADDRESS1"));
					// } else {
					// row.createCell(24).setCellValue(resultSet.getString("O_ADDRESS1"));
					// row.createCell(25).setCellValue(resultSet.getString("ADDRESS1"));
					// }
				} catch (Exception ex) {
					row.createCell(24).setCellValue("");
					row.createCell(25).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_AGENT_CODE").equals(resultSet.getString("AGENT_CODE")))
					// {
					row.createCell(26).setCellValue(resultSet.getString("O_AGENT_CODE"));
					// row.createCell(27).setCellValue("");
					row.createCell(27).setCellValue(resultSet.getString("AGENT_CODE"));
					// } else {
					// row.createCell(26).setCellValue(resultSet.getString("O_AGENT_CODE"));
					// row.createCell(27).setCellValue(resultSet.getString("AGENT_CODE"));
					// }
				} catch (Exception ex) {
					row.createCell(26).setCellValue("");
					row.createCell(27).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_AGENT_CODE").equals(resultSet.getString("AGENT_CODE")))
					// {
					row.createCell(28).setCellValue(resultSet.getString("O_ID_NO"));
					// row.createCell(29).setCellValue("");
					row.createCell(29).setCellValue(resultSet.getString("ID_NO"));
					// } else {
					// row.createCell(28).setCellValue(resultSet.getString("O_ID_NO"));
					// row.createCell(29).setCellValue(resultSet.getString("ID_NO"));
					// }
				} catch (Exception ex) {
					row.createCell(28).setCellValue("");
					row.createCell(29).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_AGENT_CODE").equals(resultSet.getString("AGENT_CODE")))
					// {
					row.createCell(30).setCellValue(resultSet.getString("O_PHONE_TYPE"));
					// row.createCell(31).setCellValue("");
					row.createCell(31).setCellValue(resultSet.getString("PHONE_TYPE"));
					// } else {
					// row.createCell(30).setCellValue(resultSet.getString("O_PHONE_TYPE"));
					// row.createCell(31).setCellValue(resultSet.getString("PHONE_TYPE"));
					// }
				} catch (Exception ex) {
					row.createCell(30).setCellValue("");
					row.createCell(31).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_AGENT_CODE").equals(resultSet.getString("AGENT_CODE")))
					// {
					row.createCell(32).setCellValue(resultSet.getString("O_CITY"));
					// row.createCell(33).setCellValue("");
					row.createCell(33).setCellValue(resultSet.getString("CITY"));
					// } else {
					// row.createCell(32).setCellValue(resultSet.getString("O_CITY"));
					// row.createCell(33).setCellValue(resultSet.getString("CITY"));
					// }
				} catch (Exception ex) {
					row.createCell(32).setCellValue("");
					row.createCell(33).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_AGENT_CODE").equals(resultSet.getString("AGENT_CODE")))
					// {
					row.createCell(34).setCellValue(resultSet.getString("O_STATE"));
					// row.createCell(35).setCellValue("");
					row.createCell(35).setCellValue(resultSet.getString("STATE"));
					// } else {
					// row.createCell(34).setCellValue(resultSet.getString("O_STATE"));
					// row.createCell(35).setCellValue(resultSet.getString("STATE"));
					// }
				} catch (Exception ex) {
					row.createCell(34).setCellValue("");
					row.createCell(35).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_AGENT_CODE").equals(resultSet.getString("AGENT_CODE")))
					// {
					row.createCell(36).setCellValue(resultSet.getString("O_COUNTRY"));
					// row.createCell(37).setCellValue("");
					row.createCell(37).setCellValue(resultSet.getString("COUNTRY"));
					// } else {
					// row.createCell(36).setCellValue(resultSet.getString("O_COUNTRY"));
					// row.createCell(37).setCellValue(resultSet.getString("COUNTRY"));
					// }
				} catch (Exception ex) {
					row.createCell(36).setCellValue("");
					row.createCell(37).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_AGENT_CODE").equals(resultSet.getString("AGENT_CODE")))
					// {
					row.createCell(38).setCellValue(resultSet.getString("O_AGENT_ALIAS_NAME"));
					// row.createCell(39).setCellValue("");
					row.createCell(39).setCellValue(resultSet.getString("AGENT_ALIAS_NAME"));
					// } else {
					// row.createCell(38).setCellValue(resultSet.getString("O_AGENT_ALIAS_NAME"));
					// row.createCell(39).setCellValue(resultSet.getString("AGENT_ALIAS_NAME"));
					// }
				} catch (Exception ex) {
					row.createCell(38).setCellValue("");
					row.createCell(39).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_AGENT_CODE").equals(resultSet.getString("AGENT_CODE")))
					// {
					row.createCell(40).setCellValue(resultSet.getString("O_AGENT_PRODUCT_SELECTION"));
					// row.createCell(41).setCellValue("");
					row.createCell(41).setCellValue(resultSet.getString("AGENT_PRODUCT_SELECTION"));
					// } else {
					// row.createCell(40).setCellValue(resultSet.getString("O_AGENT_PRODUCT_SELECTION"));
					// row.createCell(41).setCellValue(resultSet.getString("AGENT_PRODUCT_SELECTION"));
					// }
				} catch (Exception ex) {
					row.createCell(40).setCellValue("");
					row.createCell(41).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_AGENT_CODE").equals(resultSet.getString("AGENT_CODE")))
					// {
					row.createCell(42).setCellValue(resultSet.getString("O_AGENT_REGISTRATION_DOC"));
					// row.createCell(43).setCellValue("");
					row.createCell(43).setCellValue(resultSet.getString("AGENT_REGISTRATION_DOC"));
					// } else {
					// row.createCell(42).setCellValue(resultSet.getString("O_AGENT_REGISTRATION_DOC"));
					// row.createCell(43).setCellValue(resultSet.getString("AGENT_REGISTRATION_DOC"));
					// }
				} catch (Exception ex) {
					row.createCell(42).setCellValue("");
					row.createCell(43).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_POST_CODE").equals(resultSet.getString("POST_CODE")))
					// {
					row.createCell(44).setCellValue(resultSet.getString("O_POST_CODE"));
					// row.createCell(45).setCellValue("");
					row.createCell(45).setCellValue(resultSet.getString("POST_CODE"));
					// } else {
					// row.createCell(44).setCellValue(resultSet.getString("O_POST_CODE"));
					// row.createCell(45).setCellValue(resultSet.getString("POST_CODE"));
					// }
				} catch (Exception ex) {
					row.createCell(44).setCellValue("");
					row.createCell(45).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_AGENT_LOGO").equals(resultSet.getString("AGENT_LOGO")))
					// {
					row.createCell(46).setCellValue(resultSet.getString("O_AGENT_LOGO"));
					// row.createCell(47).setCellValue("");
					row.createCell(47).setCellValue(resultSet.getString("AGENT_LOGO"));
					// } else {
					// row.createCell(46).setCellValue(resultSet.getString("O_AGENT_LOGO"));
					// row.createCell(47).setCellValue(resultSet.getString("AGENT_LOGO"));
					// }
				} catch (Exception ex) {
					row.createCell(46).setCellValue("");
					row.createCell(47).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_AGENT_DISCOUNT").equals(resultSet.getString("AGENT_DISCOUNT")))
					// {
					row.createCell(48).setCellValue(resultSet.getString("O_AGENT_DISCOUNT"));
					// row.createCell(49).setCellValue("");
					row.createCell(49).setCellValue(resultSet.getString("AGENT_DISCOUNT"));
					// } else {
					// row.createCell(48).setCellValue(resultSet.getString("O_AGENT_DISCOUNT"));
					// row.createCell(49).setCellValue(resultSet.getString("AGENT_DISCOUNT"));
					// }
				} catch (Exception ex) {
					row.createCell(48).setCellValue("");
					row.createCell(49).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_AGENT_COMMISSION").equals(resultSet.getString("AGENT_COMMISSION")))
					// {
					row.createCell(50).setCellValue(resultSet.getString("O_AGENT_COMMISSION"));
					// row.createCell(51).setCellValue("");
					row.createCell(51).setCellValue(resultSet.getString("AGENT_COMMISSION"));
					// } else {
					// row.createCell(50).setCellValue(resultSet.getString("O_AGENT_COMMISSION"));
					// row.createCell(51).setCellValue(resultSet.getString("AGENT_COMMISSION"));
					// }
				} catch (Exception ex) {
					row.createCell(50).setCellValue("");
					row.createCell(51).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_AGENT_TYPE").equals(resultSet.getString("AGENT_TYPE")))
					// {
					row.createCell(52).setCellValue(resultSet.getString("O_AGENT_TYPE"));
					// row.createCell(53).setCellValue("");
					row.createCell(53).setCellValue(resultSet.getString("AGENT_TYPE"));
					// } else {
					// row.createCell(52).setCellValue(resultSet.getString("O_AGENT_TYPE"));
					// row.createCell(53).setCellValue(resultSet.getString("AGENT_TYPE"));
					// }
				} catch (Exception ex) {
					row.createCell(52).setCellValue("");
					row.createCell(53).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_AGENT_USERNAME").equals(resultSet.getString("AGENT_USERNAME")))
					// {
					row.createCell(54).setCellValue(resultSet.getString("O_AGENT_USERNAME"));
					// row.createCell(55).setCellValue("");
					row.createCell(55).setCellValue(resultSet.getString("AGENT_USERNAME"));
					// } else {
					// row.createCell(54).setCellValue(resultSet.getString("O_AGENT_USERNAME"));
					// row.createCell(55).setCellValue(resultSet.getString("AGENT_USERNAME"));
					// }
				} catch (Exception ex) {
					row.createCell(54).setCellValue("");
					row.createCell(55).setCellValue("");
				}

				/*
				 * row.createCell(56).setCellValue(resultSet.getString( "O_PASSWORD"));
				 * row.createCell(57).setCellValue(resultSet.getString( "PASSWORD"));
				 */

				try {
					// if
					// (resultSet.getString("O_ADDRESS2").equals(resultSet.getString("ADDRESS2"))) {
					row.createCell(56).setCellValue(resultSet.getString("O_ADDRESS2"));
					// row.createCell(57).setCellValue("");
					row.createCell(57).setCellValue(resultSet.getString("ADDRESS2"));
					// } else {
					// row.createCell(56).setCellValue(resultSet.getString("O_ADDRESS2"));
					// row.createCell(57).setCellValue(resultSet.getString("ADDRESS2"));
					// }
				} catch (Exception ex) {
					row.createCell(56).setCellValue("");
					row.createCell(57).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_ADDRESS3").equals(resultSet.getString("ADDRESS3"))) {
					row.createCell(58).setCellValue(resultSet.getString("O_ADDRESS3"));
					// row.createCell(59).setCellValue("");
					row.createCell(59).setCellValue(resultSet.getString("ADDRESS3"));
					// } else {
					// row.createCell(58).setCellValue(resultSet.getString("O_ADDRESS3"));
					// row.createCell(59).setCellValue(resultSet.getString("ADDRESS3"));
					// }
				} catch (Exception ex) {
					row.createCell(58).setCellValue("");
					row.createCell(59).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_EMAIL_PASS_CLICKED").equals(resultSet.getString("EMAIL_PASS_CLICKED")))
					// {
					row.createCell(60).setCellValue(resultSet.getString("O_EMAIL_PASS_CLICKED"));
					// row.createCell(61).setCellValue("");
					row.createCell(61).setCellValue(resultSet.getString("EMAIL_PASS_CLICKED"));
					// } else {
					// row.createCell(60).setCellValue(resultSet.getString("O_EMAIL_PASS_CLICKED"));
					// row.createCell(61).setCellValue(resultSet.getString("EMAIL_PASS_CLICKED"));
					// }
				} catch (Exception ex) {
					row.createCell(60).setCellValue("");
					row.createCell(61).setCellValue("");
				}

				try {
					// if
					// (resultSet.getString("O_EMAIL_PASS_EXPIRY").equals(resultSet.getString("EMAIL_PASS_EXPIRY")))
					// {
					row.createCell(62).setCellValue(resultSet.getString("O_EMAIL_PASS_EXPIRY"));
					// row.createCell(63).setCellValue("");
					row.createCell(63).setCellValue(resultSet.getString("EMAIL_PASS_EXPIRY"));
					// } else {
					// row.createCell(62).setCellValue(resultSet.getString("O_EMAIL_PASS_EXPIRY"));
					// row.createCell(63).setCellValue(resultSet.getString("EMAIL_PASS_EXPIRY"));
					// }
				} catch (Exception ex) {
					row.createCell(62).setCellValue("");
					row.createCell(63).setCellValue("");

				}

				row.createCell(64).setCellValue(resultSet.getString("WHO"));
				row.createCell(65).setCellValue(resultSet.getString("DATE_WHEN"));
				row.createCell(66).setCellValue(resultSet.getString("RECORD_COMMENT"));

				i++;

			}

			ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
			hwb.write(outByteStream);
			byte[] outArray = outByteStream.toByteArray();
			response.setContentType("application/ms-excel");
			response.setContentLength(outArray.length);
			response.setHeader("Expires:", "0");
			response.setHeader("Content-Disposition", "attachment; filename=Agent_Audit_Log.xls");
			OutputStream outStream = response.getOutputStream();
			outStream.write(outArray);
			outStream.flush();

			request.setAttribute("fromdate", fromdate);
			request.setAttribute("todate", todate);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("AuditAgentReport.jsp").forward(request, response);

	}

}
