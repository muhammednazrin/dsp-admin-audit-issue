package com.etiqa.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;

import org.apache.log4j.Logger;
//import com.etiqa.dsp.dao.common.WTCReportsGenerateDao;
//import com.etiqa.dsp.dao.common.WTCReportsGenerateDaoimpl;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.etiqa.DAO.ReportDAO;
import com.etiqa.DAO.ReportDAOImpl;
import com.etiqa.common.BaseServlet;
import com.etiqa.dsp.dao.common.motor.MotorInsuranceISMService;
import com.etiqa.dsp.dao.common.motor.MotorInsuranceJPJService;
import com.etiqa.dsp.dao.common.motor.MotorInsuranceReportsGenDao;
import com.etiqa.dsp.dao.common.motor.MotorInsuranceReportsGenDaoImpl;
import com.etiqa.dsp.dao.common.pojo.GeneratingReportsResponseVo;
import com.etiqa.model.report.TransactionalReport;
import com.spring.admin.AgentController;
import com.spring.constant.QQConstant;

@WebServlet("/TransactionalReport")
public class TransactionalReportController extends BaseServlet {
	final static Logger logger = Logger.getLogger(TransactionalReportController.class);
	private static final long serialVersionUID = 1L;
	private static String redirect = "";

	private static final String URL_BUDDY_DOCGEN = "http://webservices.buddy.etiqa.com/";
	private static final String URL_DOCGEN = "http://webservices.docgen.etiqa.com/";

	private static final String DOC_GEN_TC_WSDL_URL = "http://cpf:7011/CPF-DSP-TripCare360-Doc/TripCare360DocGenWebServiceImplService?WSDL";
	private static final String DOC_GEN_TC_URL = "http://cpf:7011/CPF-DSP-TripCare360-Doc/TripCare360DocGenWebServiceImplService";

	@Override
	public void handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		if (null == session.getAttribute("user") || "".equalsIgnoreCase((String) session.getAttribute("user"))) {

			response.sendRedirect("admin-login.jsp");
			// actionBy="1";
		}

		String action = request.getParameter("action");
		logger.info("TransactionalReport<action> => " + action);

		if (action.equals(ACTION_EVENT_LIST)) {
			list(request, response);

		}

		else if (action.equals("detail")) {

			detail(request, response);

		} else if (action.equals("saveCustomerInfo")) {

			saveCustomerInfo(request, response);

		}

		else if (action.equals("saveTxnInfo")) {

			saveTxnInfo(request, response);

		}

		else if (action.equals("sendEmail")) {

			sendEmail(request, response);

		} else if (action.equals("generateDoc")) {

			generateDoc(request, response);

		} else if (action.equals("policyDetail")) {

			policyDetail(request, response);

		}

		else if (action.equals("generateCAPS")) {

			generateCAPS(request, response);

		}

		else if (action.equals("checkJPJISMStatus")) {

			checkJPJISMStatus(request, response);

		}

		else {
			throw new RuntimeException("Unknown Action");
		}
	}

	private void list(HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		String ProductType = null;
		String ProductEntity = null;
		String Status = null;
		String PolicyCertificateNo = null;
		String DateFrom = null;
		String DateTo = null;
		String NRIC = null;
		String ReceiptNo = null;
		String PlateNo = null;
		String eCoverStatus = null;
		String AgentCode = null;

		BigDecimal number1 = new BigDecimal(0.00);
		BigDecimal number2 = new BigDecimal(0.00);
		BigDecimal number3 = new BigDecimal(0.00);

		try {
			if (request.getMethod().equals("POST"))

			{
				session.setAttribute("ProductType", request.getParameter("productType").trim());
				session.setAttribute("ProductEntity", request.getParameter("productEntity").trim());
				session.setAttribute("Status", request.getParameter("status").trim());
				session.setAttribute("PolicyCertificateNo", request.getParameter("policyNo").trim());
				session.setAttribute("NRIC", request.getParameter("NRIC").trim());
				session.setAttribute("ReceiptNo", request.getParameter("receiptNo").trim());
				session.setAttribute("dateFrom", request.getParameter("dateFrom").trim());
				session.setAttribute("dateTo", request.getParameter("dateTo").trim());
				session.setAttribute("DateFrom", "");
				session.setAttribute("DateTo", "");
				session.setAttribute("DataResult", "");
				session.setAttribute("AgentCode", "");

				redirect = "admin-report-transaction.jsp";

				if (request.getParameter("plateNo") != null
						&& request.getParameter("productType").trim().indexOf("M") != -1) {
					session.setAttribute("PlateNo", request.getParameter("plateNo").trim());
				} else {
					session.setAttribute("PlateNo", "");
				}

				// SimpleDateFormat ft2 = new SimpleDateFormat ("dd/MM/yyyy");

				if (request.getParameter("dateFrom").trim().length() > 0) {

					session.setAttribute("DateFrom", request.getParameter("dateFrom").trim());

				}

				if (request.getParameter("dateTo").trim().length() > 0) {

					session.setAttribute("DateTo", request.getParameter("dateTo").trim());

				}

				if (request.getParameter("dateFrom").trim().length() > 0
						&& request.getParameter("dateTo").trim().length() == 0) {

					Date dNow = new Date();
					SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");

					// logger.info("Current Date: " + ft.format(dNow));

					session.setAttribute("DateTo", ft.format(dNow));

				}

			}

			logger.info(ProductType + "Product Type Checking");
			ProductType = (String) session.getAttribute("ProductType");
			ProductEntity = (String) session.getAttribute("ProductEntity");
			Status = (String) session.getAttribute("Status");
			PolicyCertificateNo = (String) session.getAttribute("PolicyCertificateNo");
			NRIC = (String) session.getAttribute("NRIC");
			ReceiptNo = (String) session.getAttribute("ReceiptNo");
			DateFrom = (String) session.getAttribute("DateFrom");
			DateTo = (String) session.getAttribute("DateTo");
			PlateNo = (String) session.getAttribute("PlateNo");
			ProductEntity = (String) session.getAttribute("ProductEntity");

			if (!PlateNo.equals("") && !PlateNo.equals(null)) {

				PlateNo = PlateNo.toUpperCase();
			}

			int StartRow = Integer.parseInt(request.getParameter("startRow"));
			int EndRow = Integer.parseInt(request.getParameter("endRow"));

			// for motor

			if (ProductType.indexOf("MI") != -1 || ProductType.indexOf("MT") !=-1 || ProductType.indexOf("Mo") != -1) {

				ReportDAO dao = new ReportDAOImpl();

				List<TransactionalReport> MotorReport = dao.getMotorRecord(ProductEntity, Status, PolicyCertificateNo,
						DateFrom, DateTo, NRIC, ReceiptNo, PlateNo, AgentCode);
				request.setAttribute("data", MotorReport);
				session.setAttribute("DataResult", request.getAttribute("data"));
				forward(request, response, redirect);
			}

			// For HOHH Incomplete

			else if (ProductType.equals("building") || ProductType.equals("building_content")
					|| ProductType.equals("content")) {

				ReportDAO dao = new ReportDAOImpl();// daoFactory.getAgentDAO();
				List<TransactionalReport> Travlrpt = dao.getHOHHRecord(ProductType, ProductEntity, Status,
						PolicyCertificateNo, DateFrom, DateTo, NRIC, ReceiptNo, AgentCode);
				request.setAttribute("data", Travlrpt);
				session.setAttribute("DataResult", request.getAttribute("data"));
				forward(request, response, redirect);

			}

			else if (ProductType.equals("TL")) {
				ReportDAO dao = new ReportDAOImpl();// daoFactory.getAgentDAO();
				List<TransactionalReport> Travlrpt = dao.getTLRecord(ProductEntity, Status, PolicyCertificateNo,
						DateFrom, DateTo, NRIC, ReceiptNo, AgentCode);
				request.setAttribute("data", Travlrpt);
				session.setAttribute("DataResult", request.getAttribute("data"));
				forward(request, response, redirect);

			}
			else if (ProductType.equals(QQConstant.CANCERCARE_INSURANCE) || ProductType.equals(QQConstant.CANCERCARE_TAKAFUL)) {
				logger.info("cancercare contoller");
				ReportDAO dao = new ReportDAOImpl();
				List<TransactionalReport> Travlrpt = dao.getPCCA01Record(ProductEntity, Status, PolicyCertificateNo,
						DateFrom, DateTo, NRIC, ReceiptNo, AgentCode);
				request.setAttribute("data", Travlrpt);
				session.setAttribute("DataResult", request.getAttribute("data"));
				logger.info("*****************Dhana checking--request.getAttribute(\"data\")"+request.getAttribute("data"));
				forward(request, response, redirect);

			}
			
			else if (ProductType.equals(QQConstant.MEDICALPASS_INSURANCE) || ProductType.equals(QQConstant.MEDICALPASS_TAKAFUL)) {
				logger.info("MEDICAL contoller");
				ReportDAO dao = new ReportDAOImpl();
				List<TransactionalReport> Travlrpt = dao.getMPRecord(ProductEntity, Status, PolicyCertificateNo,
						DateFrom, DateTo, NRIC, ReceiptNo, AgentCode);
				request.setAttribute("data", Travlrpt);
				session.setAttribute("DataResult", request.getAttribute("data"));
				forward(request, response, redirect);

			}


			else if (ProductType.equals("WTC") || ProductType.equals("TPT")) {
				ReportDAO dao = new ReportDAOImpl();// daoFactory.getAgentDAO();
				List<TransactionalReport> Travlrpt = dao.getWTCRecord(ProductEntity, Status, PolicyCertificateNo,
						DateFrom, DateTo, NRIC, ReceiptNo, AgentCode);
				request.setAttribute("data", Travlrpt);
				session.setAttribute("DataResult", request.getAttribute("data"));
				forward(request, response, redirect);

			}

			else {

				ReportDAO dao = new ReportDAOImpl();// daoFactory.getAgentDAO();
				List<TransactionalReport> Travlrpt = dao.findByProductType(ProductType, ProductEntity, Status,
						PolicyCertificateNo, DateFrom, DateTo, NRIC, ReceiptNo, StartRow, EndRow, PlateNo, AgentCode);
				request.setAttribute("data", Travlrpt);
				// session.setAttribute("DataResult",request.getAttribute("data"));
				forward(request, response, redirect);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void policyDetail(HttpServletRequest request, HttpServletResponse response) throws IOException {

		String QQID = request.getParameter("qqid").trim();
		ReportDAO dao = new ReportDAOImpl();// daoFactory.getAgentDAO();
		List<TransactionalReport> policyDetail = dao.getPolicyDetail(QQID);
		request.setAttribute("data", policyDetail);

		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();

		// create Json Object
		JSONObject json = new JSONObject();

		// put some value pairs into the JSON object .
		try {
			json.put("data", request.getAttribute("data"));

			/*
			 * json.put("corpName", request.getAttribute("corpName")); json.put("payeeCode",
			 * request.getAttribute("payeeCode")); json.put("referenceNo",
			 * request.getAttribute("referenceNo")); json.put("accountNo",
			 * request.getAttribute("accountNo")); json.put("transAmount",
			 * request.getAttribute("transAmount")); json.put("approvalCode",
			 * request.getAttribute("approvalCode")); json.put("bankRefNo",
			 * request.getAttribute("bankRefNo")); json.put("transDate",
			 * request.getAttribute("transDate")); json.put("TransStatus",
			 * request.getAttribute("TransStatus")); json.put("verifyTransactionStatus",
			 * request.getAttribute("verifyTransactionStatus"));
			 */

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// finally output the json string
		out.print(json.toString());
		// logger.info(json.toString());

		// forward(request, response, "admin-transaction-details.jsp");
	}

	private void detail(HttpServletRequest request, HttpServletResponse response) {

		redirect = "admin-transaction-details.jsp";

		int ID = Integer.parseInt(request.getParameter("id").trim());
		//logger.info(ID);

		ReportDAO dao = new ReportDAOImpl();// daoFactory.getAgentDAO();
		List<TransactionalReport> TranDetail = dao.getTransactionDetail(ID);
		request.setAttribute("data", TranDetail);
		forward(request, response, redirect);
	}

	public String ReplaceSplCharacters(String Name) {

		String changedName = "";
		if (Name.indexOf("&amp;") != -1) {
			changedName = Name.replace("&amp;", "&");
		} else if (Name.indexOf("95") != -1) {
			changedName = Name.replace("95", "_");
		} else if (Name.indexOf("64") != -1) {
			changedName = Name.replace("64", "@");
		} else if (Name.indexOf("39") != -1) {
			changedName = Name.replace("39", "\'");
		} else {
			changedName = Name;
		}
		return changedName;
	}

	private void saveCustomerInfo(HttpServletRequest request, HttpServletResponse response) {

		redirect = "admin-transaction-details.jsp";

		HttpSession session = request.getSession();

		int customerID = Integer.parseInt(request.getParameter("customerID").trim());
		String NRIC = request.getParameter("NRIC").trim();
		String name = request.getParameter("name").trim();
		String email = request.getParameter("email").trim();
		String address1 = request.getParameter("address1").trim();
		String address2 = request.getParameter("address2").trim();
		String address3 = request.getParameter("address3").trim();
		// String city=request.getParameter("city").trim();
		String postcode = request.getParameter("postcode").trim();
		String state = request.getParameter("state").trim();
		String mobileNo = request.getParameter("mobileNo").trim();
		name = ReplaceSplCharacters(name);
		logger.info(name + "name before update controller");
		int id = Integer.parseInt(request.getParameter("id").trim());

		// logger.info(customerID);

		// To insert the Customer audit details in DSP_AUDIT_SP_CUSTOMER_INSERT table
		String loginUser = (String) session.getAttribute("actionby");
		String userRole = (String) session.getAttribute("userRole");
		String dspQQID = request.getParameter("dspQQID").trim();

		logger.info("TxnReportController<saveCustomerInfo><customerID> => " + customerID);
		logger.info("TxnReportController<saveCustomerInfo><loginUser> => " + loginUser);
		logger.info("TxnReportController<saveCustomerInfo><userRole> => " + userRole);
		logger.info("TxnReportController<saveCustomerInfo><dspQQID> => " + dspQQID);

		if (customerID > 0) {
			ReportDAO custAudit = new ReportDAOImpl();
			String cAudit = custAudit.insertCustomerAudit(customerID, loginUser, Integer.parseInt(userRole),
					Integer.parseInt(dspQQID));

			logger.info("TxnReportController<saveCustomerInfo><cAudit> => " + cAudit);
		}
		// End

		ReportDAO dao = new ReportDAOImpl();// daoFactory.getAgentDAO();

		String test = dao.updateCustomerInfo(customerID, NRIC, name, email, address1, address2, address3, postcode,
				state, mobileNo);

		// logger.info(test);
		List<TransactionalReport> TranDetail = dao.getTransactionDetail(id);
		request.setAttribute("data", TranDetail);
		forward(request, response, redirect);
	}

	/*
	 * private void sendEmail(HttpServletRequest request, HttpServletResponse
	 * response) {
	 *
	 *
	 * redirect="admin-transaction-details.jsp";
	 *
	 *
	 * String policyNo =request.getParameter("policyNo").trim(); int id=
	 * Integer.parseInt(request.getParameter("id").trim()); String term=
	 * request.getParameter("term").trim();
	 *
	 * logger.info("Send email for POLICY NO = "
	 * +policyNo+" and transaction_id = "+id);
	 *
	 *
	 * String emailStatus= ""; if(term.indexOf("MI")!=-1){
	 * if(policyNo.indexOf("V")!=-1){ MotorInsuranceEmailService sendEmailServiceMI
	 * = new MotorInsuranceEmailService();// daoFactory.getAgentDAO();
	 * emailStatus=sendEmailServiceMI.callingMotorInsuranceEmailService(policyNo);
	 * }else{
	 *
	 * MotorTakafulEmailService sendEmailServiceMT = new
	 * MotorTakafulEmailService();// daoFactory.getAgentDAO();
	 * emailStatus=sendEmailServiceMT.callingMotorInsuranceEmailService(policyNo); }
	 *
	 * } else if (term.indexOf("TL")!=-1){ EzyLifeEmailService sendEmailServiceTL =
	 * new EzyLifeEmailService(); emailStatus=
	 * sendEmailServiceTL.callingEzyLifeEmailService(policyNo);
	 *
	 * }
	 *
	 * else if (term.indexOf("H")!=-1){ HohhEmailService sendEmailServiceH = new
	 * HohhEmailService(); emailStatus=
	 * sendEmailServiceH.getGenReportResponse(policyNo);
	 *
	 *
	 * } else if (term.indexOf("W")!=-1){ WtcEmailService sendEmailServiceWTC = new
	 * WtcEmailService(); emailStatus=
	 * sendEmailServiceWTC.callingWtcEmailService(policyNo);
	 *
	 * } //logger.info(sendEmailStatus); ReportDAO dao = new ReportDAOImpl();
	 * List<TransactionalReport> TranDetail = dao.getTransactionDetail(id);
	 * request.setAttribute("data", TranDetail); request.setAttribute("statusEm",
	 * emailStatus); forward(request, response, redirect);
	 *
	 * }
	 */

	private void sendEmail(HttpServletRequest request, HttpServletResponse response) {

		redirect = "admin-transaction-details.jsp";

		String policyNo = request.getParameter("policyNo").trim();
		String docId = request.getParameter("docId").trim();
		int id = Integer.parseInt(request.getParameter("id").trim());
		String term = request.getParameter("term").trim();
		String simpleNote = request.getParameter("simpleNote").trim();
		// logger.info("Send email for POLICY NO = "+policyNo+" and
		// transaction_id = "+id);
		if (term.equals("EZYTL")) {
			term = "YT";
		} else if (term.equals("ISCTL")) {
			term = "CT";
		}
		logger.info("term =" + term);

		String emailStatus = "";
		if (term.indexOf("MI") != -1 || term.indexOf("MT") != -1 || term.indexOf("Mo") != -1) {

			String documenturl = "";

			if (policyNo.indexOf("V") != -1) {
				if (simpleNote.equals("SS")) {
					documenturl = "http://cpf:7011/CPF-DSP_SMI_Doc"; // CPF-DSP_MIDT_Doc
				} else {
					documenturl = "http://cpf:7011/CPF-DSP_MIDT_Doc"; // CPF-DSP_MT_Doc
				}
			} else {
				if (simpleNote.equals("SS")) {
					documenturl = "http://cpf:7011/CPF-DSP_SMT_Doc"; // CPF-DSP_MIDT_Doc
				} else {
					documenturl = "http://cpf:7011/CPF-DSP_MT_Doc"; // CPF-DSP_MT_Doc
				}
			}
			try {
				// http://192.168.0.247:7005/CPF20092016/MotorInsuranceDocGenWSImplService?WSDL
				// URL url = new URL(documenturl+"/MotorQQDataImplService?WSDL");
				QName servicename = new QName("http://motor.wcservices.dsp.etiqa.com/",
						"MotorInsuranceDocGenWSImplService");
				QName portname = new QName("http://motor.wcservices.dsp.etiqa.com/", "MotorInsuranceDocGenWSImplPort");
				Service service = Service.create(null, servicename);
				service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
						documenturl + "/MotorInsuranceDocGenWSImplService");
				Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
						Service.Mode.MESSAGE);
				MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
				SOAPMessage request1 = mf.createMessage();
				SOAPPart part = request1.getSOAPPart();
				SOAPEnvelope env = part.getEnvelope();
				SOAPBody body = env.getBody();
				// resp = body;
				SOAPElement operation = body.addChildElement("getStatus", "mot",
						"http://motor.wcservices.dsp.etiqa.com/");

				SOAPElement element_param = operation.addChildElement("QQID");
				element_param.addTextNode(docId);
				SOAPElement element_param_lang = operation.addChildElement("langValue");
				element_param_lang.addTextNode("lan_en");
				SOAPElement element_param_email = operation.addChildElement("emailVal");
				element_param_email.addTextNode("Y");

				request1.saveChanges();
				String result = null;

				if (request1 != null) {
					ByteArrayOutputStream baos = null;
					try {
						baos = new ByteArrayOutputStream();
						request1.writeTo(baos);
						result = baos.toString();
					} catch (Exception e) {
					} finally {
						if (baos != null) {
							try {
								baos.close();
							} catch (IOException ioe) {
							}
						}
					}
				}
				// logger.info(result+"document insert");
				// resCode=body ;

				SOAPMessage response1 = dispatch.invoke(request1);
				SOAPBody responsebody = response1.getSOAPBody();
				logger.info("regenerate doc Motor" + responsebody);
				/*
				 * java.util.Iterator otcupdates = responsebody.getChildElements(); while
				 * (otcupdates.hasNext()) { SOAPElement otcupdates1 =
				 * (SOAPElement)otcupdates.next();
				 *
				 * java.util.Iterator i = otcupdates1.getChildElements(); while( i.hasNext() ){
				 *
				 * SOAPElement e = (SOAPElement)i.next(); java.util.Iterator m =
				 * e.getChildElements(); while( m.hasNext() ){ SOAPElement e2 =
				 * (SOAPElement)m.next();
				 *
				 * } } }
				 */

			} catch (Exception e) {

				emailStatus = "Fail";
				// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
				e.printStackTrace();
			}

		} else if (term.indexOf("TL") != -1) {

			boolean recFound = false;
			// String param="Location";
			try {
				// String cpfRootUrlDoc = "http://192.168.0.248:7011/CPF-DSP_TL_BM";

				String cpfRootUrlDoc = "http://cpf:7011/CPF-DSP_TL_BM";

				QName servicename = new QName("http://termlife.wcservices.dsp.etiqa.com/",
						"TermLifeDocGenWebServiceImplService");
				QName portname = new QName("http://termlife.wcservices.dsp.etiqa.com/",
						"TermLifeDocGenWebServiceImplPort");
				Service service = Service.create(null, servicename);
				service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
						cpfRootUrlDoc + "/TermLifeDocGenWebServiceImplService?WSDL");
				Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
						Service.Mode.MESSAGE);
				MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

				SOAPMessage request1 = mf.createMessage();
				SOAPPart part = request1.getSOAPPart();

				SOAPEnvelope env = part.getEnvelope();
				SOAPBody body = env.getBody();
				// resp = body;
				SOAPElement operation = body.addChildElement("getStatus", "ter",
						"http://termlife.wcservices.dsp.etiqa.com/");

				SOAPElement element_param = operation.addChildElement("forAllDocQQID");
				element_param.addTextNode(docId);
				SOAPElement element_param1 = operation.addChildElement("langValue");
				element_param1.addTextNode("E");
				/*
				 * SOAPElement element_param2 = operation.addChildElement("emailVal");
				 * element_param2.addTextNode("Y");
				 */
				request1.saveChanges();
				String result = null;

				if (request1 != null) {
					ByteArrayOutputStream baos = null;
					try {
						baos = new ByteArrayOutputStream();
						request1.writeTo(baos);
						result = baos.toString();
					} catch (Exception e) {
					} finally {
						if (baos != null) {
							try {
								baos.close();
							} catch (IOException ioe) {
							}
						}
					}
				}
				// logger.info("Ezy Life DOC GEN"+result);
				// resCode=body ;
				SOAPMessage response1 = dispatch.invoke(request1);
				SOAPBody responsebody = response1.getSOAPBody();
				logger.info("regenerate doc TL " + responsebody);

				/*
				 * java.util.Iterator otcupdates = responsebody.getChildElements(); while
				 * (otcupdates.hasNext()) { SOAPElement otcupdates1 =
				 * (SOAPElement)otcupdates.next();
				 *
				 * java.util.Iterator i = otcupdates1.getChildElements(); while( i.hasNext() ){
				 *
				 * SOAPElement e = (SOAPElement)i.next(); java.util.Iterator m =
				 * e.getChildElements(); while( m.hasNext() ){ SOAPElement e2 =
				 * (SOAPElement)m.next();
				 *
				 * } } }
				 */
				// logger.info( "ezy life document generated result document insert");
			} catch (Exception e) {
				emailStatus = "Fail";
				// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
				e.printStackTrace();
			}

	}else if (term.indexOf("D") != -1) {

			boolean recFound = false;
			// String param="Location";
			try {
				// String cpfRootUrlDoc = "http://192.168.0.248:7011/CPF-DSP_TL_BM";

				String cpfRootUrlDoc = "http://cpf:7011/CPF-DSP_IDS";

				QName servicename = new QName("http://termlife.wcservices.dsp.etiqa.com/",
						"TermLifeDocGenWebServiceImplService");
				QName portname = new QName("http://termlife.wcservices.dsp.etiqa.com/",
						"TermLifeDocGenWebServiceImplPort");
				Service service = Service.create(null, servicename);
				service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
						cpfRootUrlDoc + "/TermLifeDocGenWebServiceImplService?WSDL");
				Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
						Service.Mode.MESSAGE);
				MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

				SOAPMessage request1 = mf.createMessage();
				SOAPPart part = request1.getSOAPPart();

				SOAPEnvelope env = part.getEnvelope();
				SOAPBody body = env.getBody();
				// resp = body;
				SOAPElement operation = body.addChildElement("getStatus", "ter",
						"http://termlife.wcservices.dsp.etiqa.com/");

				SOAPElement element_param = operation.addChildElement("forAllDocQQID");
				element_param.addTextNode(docId);
				SOAPElement element_param1 = operation.addChildElement("langValue");
				element_param1.addTextNode("E");
				/*
				 * SOAPElement element_param2 = operation.addChildElement("emailVal");
				 * element_param2.addTextNode("Y");
				 */
				request1.saveChanges();
				String result = null;

				if (request1 != null) {
					ByteArrayOutputStream baos = null;
					try {
						baos = new ByteArrayOutputStream();
						request1.writeTo(baos);
						result = baos.toString();
					} catch (Exception e) {
					} finally {
						if (baos != null) {
							try {
								baos.close();
							} catch (IOException ioe) {
							}
						}
					}
				}
				// logger.info("Ezy Life DOC GEN"+result);
				// resCode=body ;
				SOAPMessage response1 = dispatch.invoke(request1);
				SOAPBody responsebody = response1.getSOAPBody();
				logger.info("regenerate doc TL " + responsebody);

				/*
				 * java.util.Iterator otcupdates = responsebody.getChildElements(); while
				 * (otcupdates.hasNext()) { SOAPElement otcupdates1 =
				 * (SOAPElement)otcupdates.next();
				 *
				 * java.util.Iterator i = otcupdates1.getChildElements(); while( i.hasNext() ){
				 *
				 * SOAPElement e = (SOAPElement)i.next(); java.util.Iterator m =
				 * e.getChildElements(); while( m.hasNext() ){ SOAPElement e2 =
				 * (SOAPElement)m.next();
				 *
				 * } } }
				 */
				// logger.info( "ezy life document generated result document insert");
			} catch (Exception e) {
				emailStatus = "Fail";
				// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
				e.printStackTrace();
			}

		} else if (term.indexOf("YT") != -1) {// EZYTL

			boolean recFound = false;
			// String param="Location";
			try {
				// String cpfRootUrlDoc = "http://192.168.0.248:7011/CPF-DSP_TL_BM";

				String cpfRootUrlDoc = "http://cpf:7011/CPF-DSP-EZYSECURE";

				QName servicename = new QName("http://termlife.wcservices.dsp.etiqa.com/",
						"TermLifeDocGenWebServiceImplService");
				QName portname = new QName("http://termlife.wcservices.dsp.etiqa.com/",
						"TermLifeDocGenWebServiceImplPort");
				Service service = Service.create(null, servicename);
				service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
						cpfRootUrlDoc + "/TermLifeDocGenWebServiceImplService?WSDL");
				Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
						Service.Mode.MESSAGE);
				MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

				SOAPMessage request1 = mf.createMessage();
				SOAPPart part = request1.getSOAPPart();

				SOAPEnvelope env = part.getEnvelope();
				SOAPBody body = env.getBody();
				// resp = body;
				SOAPElement operation = body.addChildElement("getStatus", "ter",
						"http://termlife.wcservices.dsp.etiqa.com/");

				SOAPElement element_param = operation.addChildElement("forAllDocQQID");
				element_param.addTextNode(docId);
				SOAPElement element_param1 = operation.addChildElement("langValue");
				element_param1.addTextNode("E");
				/*
				 * SOAPElement element_param2 = operation.addChildElement("emailVal");
				 * element_param2.addTextNode("N");
				 */
				request1.saveChanges();
				String result = null;

				if (request1 != null) {
					ByteArrayOutputStream baos = null;
					try {
						baos = new ByteArrayOutputStream();
						request1.writeTo(baos);
						result = baos.toString();
					} catch (Exception e) {
					} finally {
						if (baos != null) {
							try {
								baos.close();
							} catch (IOException ioe) {
							}
						}
					}
				}
				// logger.info("Ezy Life DOC GEN"+result);
				// resCode=body ;
				SOAPMessage response1 = dispatch.invoke(request1);
				SOAPBody responsebody = response1.getSOAPBody();
				logger.info("regenerate doc IDS " + responsebody);

				/*
				 * java.util.Iterator otcupdates = responsebody.getChildElements(); while
				 * (otcupdates.hasNext()) { SOAPElement otcupdates1 =
				 * (SOAPElement)otcupdates.next();
				 *
				 * java.util.Iterator i = otcupdates1.getChildElements(); while( i.hasNext() ){
				 *
				 * SOAPElement e = (SOAPElement)i.next(); java.util.Iterator m =
				 * e.getChildElements(); while( m.hasNext() ){ SOAPElement e2 =
				 * (SOAPElement)m.next();
				 *
				 * } } }
				 */
				// logger.info( "ezy life document generated result document insert");
			} catch (Exception e) {
				emailStatus = "Fail";
				// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
				e.printStackTrace();
			}

		}

		else if (term.indexOf("CT") != -1) { // ISCTL

			boolean recFound = false;
			// String param="Location";
			try {
				// String cpfRootUrlDoc = "http://192.168.0.248:7011/CPF-DSP_TL_BM";

				String cpfRootUrlDoc = "http://cpf:7011/CPF-DSP-ISECURE3";

				QName servicename = new QName("http://termlife.wcservices.dsp.etiqa.com/",
						"TermLifeDocGenWebServiceImplService");
				QName portname = new QName("http://termlife.wcservices.dsp.etiqa.com/",
						"TermLifeDocGenWebServiceImplPort");
				Service service = Service.create(null, servicename);
				service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
						cpfRootUrlDoc + "/TermLifeDocGenWebServiceImplService?WSDL");
				Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
						Service.Mode.MESSAGE);
				MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

				SOAPMessage request1 = mf.createMessage();
				SOAPPart part = request1.getSOAPPart();

				SOAPEnvelope env = part.getEnvelope();
				SOAPBody body = env.getBody();
				// resp = body;
				SOAPElement operation = body.addChildElement("getStatus", "ter",
						"http://termlife.wcservices.dsp.etiqa.com/");

				SOAPElement element_param = operation.addChildElement("forAllDocQQID");
				element_param.addTextNode(docId);
				SOAPElement element_param1 = operation.addChildElement("langValue");
				element_param1.addTextNode("E");
				/*
				 * SOAPElement element_param2 = operation.addChildElement("emailVal");
				 * element_param2.addTextNode("N");
				 */
				request1.saveChanges();
				String result = null;

				if (request1 != null) {
					ByteArrayOutputStream baos = null;
					try {
						baos = new ByteArrayOutputStream();
						request1.writeTo(baos);
						result = baos.toString();
					} catch (Exception e) {
					} finally {
						if (baos != null) {
							try {
								baos.close();
							} catch (IOException ioe) {
							}
						}
					}
				}
				// logger.info("Ezy Life DOC GEN"+result);
				// resCode=body ;
				SOAPMessage response1 = dispatch.invoke(request1);
				SOAPBody responsebody = response1.getSOAPBody();
				logger.info("regenerate doc IDS " + responsebody);

				/*
				 * java.util.Iterator otcupdates = responsebody.getChildElements(); while
				 * (otcupdates.hasNext()) { SOAPElement otcupdates1 =
				 * (SOAPElement)otcupdates.next();
				 *
				 * java.util.Iterator i = otcupdates1.getChildElements(); while( i.hasNext() ){
				 *
				 * SOAPElement e = (SOAPElement)i.next(); java.util.Iterator m =
				 * e.getChildElements(); while( m.hasNext() ){ SOAPElement e2 =
				 * (SOAPElement)m.next();
				 *
				 * } } }
				 */
				// logger.info( "ezy life document generated result document insert");
			} catch (Exception e) {
				emailStatus = "Fail";
				// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
				e.printStackTrace();
			}

		}

		else if (term.indexOf("H") != -1) {

			logger.info("test--------------------->");
			String dspCpfHOHHEdocsINSURL = null;

			// if(TranDetail.get(0).getProductCode().indexOf("T")!=-1){
			if (policyNo.indexOf("F") != -1) {
				if (simpleNote.equals("SS")) {
					dspCpfHOHHEdocsINSURL = "http://cpf:7011/CPF-DSP-SHOHH-INSURANCE-DOC"; // CPF-DSP-SHOHH-INSURANCE-DOC
																							// CPF-DSP-HOHH-INS-DOC
				} else {
					dspCpfHOHHEdocsINSURL = "http://cpf:7011/CPF-DSP-HOHH-INS-DOC"; // CPF-DSP-SHOHH-INSURANCE-DOC
																					// CPF-DSP-HOHH-INS-DOC
				}
				logger.info("test1--------------------->");
			} else {
				if (simpleNote.equals("SS")) {
					dspCpfHOHHEdocsINSURL = "http://cpf:7011/CPF-DSP-SHOHH-TAKAFUL-DOC"; // CPF-DSP-SHOHH-TAKAFUL-DOC
																							// CPF-DSP-HOHH-TKF-DOC"
				} else {
					dspCpfHOHHEdocsINSURL = "http://cpf:7011/CPF-DSP-HOHH-TKF-DOC"; // CPF-DSP-SHOHH-TAKAFUL-DOC
																					// CPF-DSP-HOHH-TKF-DOC"
				}
				logger.info("test2--------------------->");
			}
			try {

				logger.info("test3--------------------->");

				QName servicename = new QName("http://hohh.wcservices.dsp.etiqa.com/",
						"HOHHDocGenWebServiceImplService");
				QName portname = new QName("http://hohh.wcservices.dsp.etiqa.com/", "HOHHDocGenWebServiceImplPort");
				Service service = Service.create(null, servicename);
				service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
						dspCpfHOHHEdocsINSURL + "/HOHHDocGenWebServiceImplService");
				Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
						Service.Mode.MESSAGE);
				MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
				SOAPMessage request1 = mf.createMessage();
				SOAPPart part = request1.getSOAPPart();
				SOAPEnvelope env = part.getEnvelope();
				SOAPBody body = env.getBody();
				// resp = body;
				SOAPElement operation = body.addChildElement("getHOHHStatus", "hohh",
						"http://hohh.wcservices.dsp.etiqa.com/");

				SOAPElement element_param = operation.addChildElement("forAllWTCDocQQID");
				element_param.addTextNode(docId);
				SOAPElement element_param1 = operation.addChildElement("emailVal");
				element_param1.addTextNode("Y");

				request1.saveChanges();

				// logger.info("Request from cpf " + body);
				String result = null;

				if (request != null) {
					ByteArrayOutputStream baos = null;
					try {
						baos = new ByteArrayOutputStream();
						request1.writeTo(baos);
						result = baos.toString();
					} catch (Exception e) {
					} finally {
						if (baos != null) {
							try {
								baos.close();
							} catch (IOException ioe) {
							}
						}
					}
				}
				logger.info(result + "document insert");
				// resCode=body ;
				logger.info("test4--------------------->");
				SOAPMessage response1 = dispatch.invoke(request1);
				SOAPBody responsebody = response1.getSOAPBody();
				logger.info("regenerate doc HOHH " + responsebody);
				/*
				 * java.util.Iterator otcupdates = responsebody.getChildElements(); while
				 * (otcupdates.hasNext()) { SOAPElement otcupdates1 =
				 * (SOAPElement)otcupdates.next();
				 *
				 * java.util.Iterator i = otcupdates1.getChildElements(); while( i.hasNext() ){
				 *
				 * SOAPElement e = (SOAPElement)i.next(); java.util.Iterator m =
				 * e.getChildElements(); while( m.hasNext() ){ SOAPElement e2 =
				 * (SOAPElement)m.next();
				 *
				 * } } }
				 */

			} catch (Exception e) {
				emailStatus = "Fail";
				// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
				e.printStackTrace();
				logger.info(e);
			}

		} else if (term.indexOf("W") != -1) {

			boolean recFound = false;
			// String param="Location";
			try {
				// String cpfRootUrlDoc = "http://192.168.0.248:7011/CPF-DSP_wtc_r1";

				// URL url = new
				// URL("http://192.168.0.248:7011/CPF-DSP_wtc_r1/WTCDocGenWebServiceImplService?WSDL");

				String cpfRootUrlDoc = "";
				// URL url = new
				// URL("http://192.168.0.248:7011/CPF-DSP_wtc_r1/WTCDocGenWebServiceImplService?WSDL");
				if (simpleNote.equals("SS")) {
					cpfRootUrlDoc = "http://cpf:7011/CPF-DSP_SWTC_Doc";
				} else {
					cpfRootUrlDoc = "http://cpf:7011/CPF-DSP_WTC_Doc";
				}
				// URL url = new
				// URL("http://10.252.21.60:7011:7011/CPF-DSP_wtc_r1/WTCDocGenWebServiceImplService?WSDL");

				QName servicename = new QName("http://wtc.wcservices.dsp.etiqa.com/", "WTCDocGenWebServiceImplService");
				QName portname = new QName("http://wtc.wcservices.dsp.etiqa.com/",
						"WTCDocGenWebServiceImplServicePort");
				Service service = Service.create(null, servicename);
				service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
						cpfRootUrlDoc + "/WTCDocGenWebServiceImplService?WSDL");
				Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
						Service.Mode.MESSAGE);
				MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

				SOAPMessage request1 = mf.createMessage();
				SOAPPart part = request1.getSOAPPart();

				SOAPEnvelope env = part.getEnvelope();
				SOAPBody body = env.getBody();
				// resp = body;
				SOAPElement operation = body.addChildElement("getWTCStatus", "getWTCStatusResponse",
						"http://wtc.wcservices.dsp.etiqa.com/");

				SOAPElement element_param = operation.addChildElement("forAllWTCDocQQID");
				element_param.addTextNode(docId);
				SOAPElement element_param1 = operation.addChildElement("langValue");
				element_param1.addTextNode("lan_en");
				SOAPElement element_param2 = operation.addChildElement("emailVal");
				element_param2.addTextNode("Y");

				request1.saveChanges();
				String result = null;

				if (request1 != null) {
					ByteArrayOutputStream baos = null;
					try {
						baos = new ByteArrayOutputStream();
						request1.writeTo(baos);
						result = baos.toString();
					} catch (Exception e) {
					} finally {
						if (baos != null) {
							try {
								baos.close();
							} catch (IOException ioe) {
							}
						}
					}
				}
				// logger.info("WTC DOC GEN"+result);
				// resCode=body ;
				SOAPMessage response1 = dispatch.invoke(request1);
				SOAPBody responsebody = response1.getSOAPBody();
				logger.info("regenerate doc WTC " + responsebody);
				/*
				 * java.util.Iterator otcupdates = responsebody.getChildElements(); while
				 * (otcupdates.hasNext()) { SOAPElement otcupdates1 =
				 * (SOAPElement)otcupdates.next();
				 *
				 * java.util.Iterator i = otcupdates1.getChildElements(); while( i.hasNext() ){
				 *
				 * SOAPElement e = (SOAPElement)i.next(); java.util.Iterator m =
				 * e.getChildElements(); while( m.hasNext() ){ SOAPElement e2 =
				 * (SOAPElement)m.next();
				 *
				 * } } }
				 */
				// logger.info( "WTC document generated result document insert");
			} catch (Exception e) {
				emailStatus = "Fail";
				// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
				e.printStackTrace();
			}

		}
		// buddy start
		else if (term.indexOf("Buddy") != -1) {

			try {

				String lang = "lan_en";

				URL url = new URL("http://cpf:7011/buddydoc/BuddyDocGenWebServiceImplService?WSDL");
				QName servicename = new QName("http://webservices.buddy.etiqa.com/",
						"BuddyDocGenWebServiceImplService");
				QName portname = new QName("http://webservices.buddy.etiqa.com/",
						"BuddyDocGenWebServiceImplServicePort");

				Service service = Service.create(null, servicename);
				service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
						"http://cpf:7011/buddydoc/BuddyDocGenWebServiceImplService");

				Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
						Service.Mode.MESSAGE);
				MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

				SOAPMessage req = mf.createMessage();
				SOAPPart part = req.getSOAPPart();
				SOAPEnvelope env = part.getEnvelope();
				SOAPBody body = env.getBody();

				SOAPElement operation = body.addChildElement("getStatus", "getStatusResponse", URL_BUDDY_DOCGEN);
				SOAPElement elementParam = operation.addChildElement("forAllDocQQID");
				elementParam.addTextNode(docId);

				SOAPElement elementParam1 = operation.addChildElement("langValue");
				elementParam1.addTextNode(lang);

				SOAPElement elementParam2 = operation.addChildElement("company");
				elementParam2.addTextNode("T");

				SOAPElement elementParam3 = operation.addChildElement("transactiontype");
				elementParam3.addTextNode("normalcustomer");

				SOAPElement elementParam4 = operation.addChildElement("emailVal");
				elementParam4.addTextNode("Y");

				SOAPElement elementParam5 = operation.addChildElement("isAdmin");
				elementParam5.addTextNode("false");

				req.saveChanges();

				String result = null;
				if (req != null) {
					ByteArrayOutputStream baos = null;

					try {
						baos = new ByteArrayOutputStream();
						req.writeTo(baos);
						result = baos.toString();

					} catch (Exception e) {

					} finally {

						if (baos != null) {

							try {
								baos.close();

							} catch (IOException ioe) {
							}
						}
					}
				}

				SOAPMessage res = dispatch.invoke(req);
				SOAPBody responsebody = res.getSOAPBody();

			} catch (Exception e) {
				emailStatus = "Fail";

				e.printStackTrace();
				logger.info(e);
			}

		}
		// buddy end
		// Travel Ezy start
		else if (term.indexOf("TravelEzy") != -1) {

			try {

				String lang = "lan_en";

				URL url = new URL("http://cpf:7011/CPF-DSP-TravelEzy-Doc/TravelEzyDocGenWebServiceImplService?WSDL");
				QName servicename = new QName(URL_DOCGEN, "TravelEzyDocGenWebServiceImplService");
				QName portname = new QName(URL_DOCGEN, "TravelEzyDocGenWebServiceImplServicePort");

				Service service = Service.create(null, servicename);
				service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
						"http://cpf:7011/CPF-DSP-TravelEzy-Doc/TravelEzyDocGenWebServiceImplService");

				Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
						Service.Mode.MESSAGE);
				MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

				SOAPMessage req = mf.createMessage();
				SOAPPart part = req.getSOAPPart();
				SOAPEnvelope env = part.getEnvelope();
				SOAPBody body = env.getBody();

				SOAPElement operation = body.addChildElement("getQQ4DocStatus", "getQQ4DocStatusResponse", URL_DOCGEN);
				SOAPElement elementParam = operation.addChildElement("forAllDocQQID");
				elementParam.addTextNode(docId);

				SOAPElement elementParam1 = operation.addChildElement("langValue");
				elementParam1.addTextNode(lang);

				SOAPElement elementParam4 = operation.addChildElement("emailVal");
				elementParam4.addTextNode("Y");

				SOAPElement elementParam2 = operation.addChildElement("isAdmin");
				elementParam2.addTextNode("false");

				req.saveChanges();

				String result = null;
				if (req != null) {
					ByteArrayOutputStream baos = null;

					try {
						baos = new ByteArrayOutputStream();
						req.writeTo(baos);
						result = baos.toString();

					} catch (Exception e) {

					} finally {

						if (baos != null) {

							try {
								baos.close();

							} catch (IOException ioe) {
							}
						}
					}
				}

				SOAPMessage res = dispatch.invoke(req);
				SOAPBody responsebody = res.getSOAPBody();

			} catch (Exception e) {
				emailStatus = "Fail";

				e.printStackTrace();
				logger.info(e);
			}

		}
		// Travel Ezy end
		// Trip Care start
		else if (term.indexOf("TripCare") != -1) {

			try {

				String lang = "lan_en";

				URL url = new URL(DOC_GEN_TC_WSDL_URL);
				QName servicename = new QName(URL_DOCGEN, "TripCare360DocGenWebServiceImplService");
				QName portname = new QName(URL_DOCGEN, "TripCare360DocGenWebServiceImplServicePort");

				Service service = Service.create(null, servicename);
				service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
						"http://cpf:7011/CPF-DSP-TripCare360-Doc/TripCare360DocGenWebServiceImplService");

				Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
						Service.Mode.MESSAGE);
				MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

				SOAPMessage req = mf.createMessage();
				SOAPPart part = req.getSOAPPart();
				SOAPEnvelope env = part.getEnvelope();
				SOAPBody body = env.getBody();

				SOAPElement operation = body.addChildElement("getQQ4DocStatus", "getQQ4DocStatusResponse", URL_DOCGEN);
				SOAPElement elementParam = operation.addChildElement("forAllDocQQID");
				elementParam.addTextNode(docId);

				SOAPElement elementParam1 = operation.addChildElement("langValue");
				elementParam1.addTextNode(lang);

				SOAPElement elementParam4 = operation.addChildElement("emailVal");
				elementParam4.addTextNode("Y");

				SOAPElement elementParam5 = operation.addChildElement("isAdmin");
				elementParam5.addTextNode("false");

				req.saveChanges();

				String result = null;
				if (req != null) {
					ByteArrayOutputStream baos = null;

					try {
						baos = new ByteArrayOutputStream();
						req.writeTo(baos);
						result = baos.toString();

					} catch (Exception e) {

					} finally {

						if (baos != null) {

							try {
								baos.close();

							} catch (IOException ioe) {
							}
						}
					}
				}

				SOAPMessage res = dispatch.invoke(req);
				SOAPBody responsebody = res.getSOAPBody();

			} catch (Exception e) {
				emailStatus = "Fail";
				e.printStackTrace();
				logger.info(e);
			}

		}
		else if (term.indexOf(QQConstant.CANCERCARE_INSURANCE) != -1 || term.indexOf(QQConstant.CANCERCARE_TAKAFUL) != -1 ) {

		logger.info("entered the pcca01 block");
		//String productcode = null;
		/*String product=TranDetail.get(0).getProductType();
		if("e-CancerCare".equalsIgnoreCase(product)){
			productcode = "PCCA01";
		}else if("e-CancerCare Takaful".equalsIgnoreCase(product)){
			productcode = "PTCA01";
		}*/

		/*logger.info("CancerCare dspqqid No =" + DSPQQID);
		logger.info("QQIdsession  CI life calling document Gen=" + DSPQQID);
		logger.info("product code"+product);
		logger.info("product code"+productcode);
*/
		String hrm_resPonseData;

		try {

			//String QQ_ID = TranDetail.get(0).getDSPQQID();
			Map<String,Object> mapResponse = new HashMap<String, Object>();
	
			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			map.add("DSP_QQ_ID", docId);
			map.add("LANGUAGE", "E");
			map.add("EMAILVALUE", "Y");
			map.add("PRODUCTCODE", term);
			
			RestTemplate restTemplate = new RestTemplate();
			hrm_resPonseData = restTemplate.postForObject("http://cpf:7011/CPF-DSP_CC_Doc/documents/documentGenarator",map ,  String.class);			
			logger.info("  RESPONSE CO::::   "+hrm_resPonseData);
			
		
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	//logger.info("Ezy Life DOC GEN" + hrm_resPonseData);
	//END CANCER INSURANCE

	}else if (term.indexOf(QQConstant.MEDICALPASS_INSURANCE) != -1 || term.indexOf(QQConstant.MEDICALPASS_TAKAFUL) != -1 ) {

	logger.info("entered the MEDICAL block");
	String language = null;
	String hrm_resPonseData;
	String Company = null;
	
	ReportDAO dao = new ReportDAOImpl();
	language =  dao.getCustomerlanguage(docId);
	if(term.equals(QQConstant.MEDICALPASS_INSURANCE)){
		Company = "I";
	}else{
		Company = "T";
	}
	
	try {

		//String QQ_ID = TranDetail.get(0).getDSPQQID();
		Map<String,Object> mapResponse = new HashMap<String, Object>();

		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		map.add("DSP_QQ_ID", docId);
		map.add("LANGUAGE", language);
		map.add("EMAILVALUE", "Y");
		map.add("COMPANY", Company);
		
		RestTemplate restTemplate = new RestTemplate();
		hrm_resPonseData = restTemplate.postForObject("http://cpf:7011/CPF-DSP_CC_Doc/medical/documentGenarator",map ,  String.class);			
		logger.info("  RESPONSE CO::::   "+hrm_resPonseData);
		
	
		
	} catch (Exception e) {
		e.printStackTrace();
	}
	}
	
		// Trip Care end
		// logger.info(sendEmailStatus);
		ReportDAO dao = new ReportDAOImpl();
		List<TransactionalReport> TranDetail = dao.getTransactionDetail(id);
		request.setAttribute("data", TranDetail);
		request.setAttribute("statusEm", emailStatus);
		forward(request, response, redirect);

	}

	private void saveTxnInfo(HttpServletRequest request, HttpServletResponse response) {

		redirect = "admin-transaction-details.jsp";

		HttpSession session = request.getSession();

		String status = request.getParameter("status").trim();
		String remark = request.getParameter("remark").trim();
		int id = Integer.parseInt(request.getParameter("id").trim());
		int moduleID = 1; // temporary
		String action = "E"; // edit - refer to table DSP_TBL_ADM_PARAM
		int createdBy = 1; // temporary

		String langValue = "lan_en";
		String preData = request.getParameter("preData").trim();
		String postData = status;
		String actionBy = "";

		if (null != session.getAttribute("actionby") && !"".equalsIgnoreCase((String) session.getAttribute("actionby"))) {
			actionBy = (String) session.getAttribute("actionby");
			logger.info(actionBy);
		}

		String policyNo = "";
		String vehicleNo = "";
		String engineNo = "";
		String chassisNo = "";
		String idNo = "";
		String startDate = "";
		String preIns = "";
		// String insCom= TranDetail.get(0).getInsCompany();
		String productType = "";

		String day_s = "";
		String mon_s = "";
		String yy_s = "";
		String start_date = "";

		ReportDAO dao = new ReportDAOImpl();// daoFactory.getAgentDAO();

		logger.info("preData" + preData);
		logger.info("postData" + postData);
		if (!preData.equals(postData)) {

			// Todo - Chandra - Testing
			// To insert the Transaction audit details in DSP_COMMON_TBL_PAYMENT_AUDIT table
			String loginUser = (String) session.getAttribute("actionby");
			String userRole = (String) session.getAttribute("userRole");
			String dspQQID = request.getParameter("dspQQID").trim();
			policyNo = request.getParameter("policyNo").trim();
			logger.info("TxnReportController<saveTxnInfo><txnID> => " + id);
			logger.info("TxnReportController<saveTxnInfo><loginUser> => " + loginUser);
			logger.info("TxnReportController<saveTxnInfo><userRole> => " + userRole);
			logger.info("TxnReportController<saveTxnInfo><dspQQID> => " + dspQQID);

			logger.info("TxnReportController<saveTxnInfo><policyNo> => " + policyNo);

			if (id > 0) {
				ReportDAO custAudit = new ReportDAOImpl();
				String pAudit = custAudit.insertPaymentforAudit(id, loginUser, Integer.parseInt(userRole),
						Integer.parseInt(dspQQID), policyNo);

				logger.info("TxnReportController<saveTxnInfo><pAudit> => " + pAudit);
			}
			// End

			String saveInfoStatus = dao.updateTxnInfo(id, status, remark, moduleID, action, createdBy, preData,
					postData, actionBy);
			// String saveInfoStatus = dao.updateTxnInfo(id,status,remark,moduleID, action,
			// createdBy);

			logger.info(saveInfoStatus);		
			List<TransactionalReport> TranDetail = dao.getTransactionDetail(id);
			
			if (TranDetail.get(0).getProductType().indexOf("Mo") != -1 || TranDetail.get(0).getProductType().indexOf("MI") != -1 ||
					TranDetail.get(0).getProductType().indexOf("MT") != -1) { // for motor
				policyNo = TranDetail.get(0).getPolicyNo();
				vehicleNo = TranDetail.get(0).getVehicleNo();
				engineNo = TranDetail.get(0).getEngineNo();
				chassisNo = TranDetail.get(0).getChassisNo();
				idNo = TranDetail.get(0).getCustomerNRIC();
				startDate = TranDetail.get(0).getCoverageStartDate();
				preIns = TranDetail.get(0).getPreviousInsurance();
				// String insCom= TranDetail.get(0).getInsCompany();
				productType = TranDetail.get(0).getProductType();

				// logger.info(productType);
				// logger.info(startDate);

				day_s = startDate.substring(0, 2);
				mon_s = startDate.substring(2, 4);
				yy_s = startDate.substring(4, 8);

				start_date = day_s + "/" + mon_s + "/" + yy_s;
				// logger.info(start_date);
			}

			if (status.equals("S")) { // change from not success to success transaction

				String DSPQQID = TranDetail.get(0).getDSPQQID();

				if (TranDetail.get(0).getProductType().indexOf("Mo") != -1 || TranDetail.get(0).getProductType().indexOf("MI") != -1 ||
						TranDetail.get(0).getProductType().indexOf("MT") != -1) {
					////////////////////////////// save status for
					////////////////////////////// motor////////////////////////////////////////////

					String QQID = dao.generatePolicyNo(id);

					logger.info("QQID =" + QQID);

					MotorInsuranceReportsGenDao genEmailDoc = new MotorInsuranceReportsGenDaoImpl();
					// List<String>
					// genEmailDocResult=genEmailDoc.getMIGenReportResponse(QQID,langValue);
					String documenturl = "";

					if (TranDetail.get(0).getProductType().equalsIgnoreCase("Motor Insurance")) {
						// documenturl="http://cpf:7011/CPF-DSP_MIDT_Doc";
						documenturl = "http://cpf:7011/CPF-DSP_SMI_Doc";
					}

					else if (TranDetail.get(0).getProductType().equalsIgnoreCase("Motor Takaful")) {
						// documenturl="http://cpf:7011/CPF-DSP_MT_Doc";
						documenturl = "http://cpf:7011/CPF-DSP_SMT_Doc";

					}
					try {
						// http://192.168.0.247:7005/CPF20092016/MotorInsuranceDocGenWSImplService?WSDL
						// URL url = new URL(documenturl+"/MotorQQDataImplService?WSDL");
						QName servicename = new QName("http://motor.wcservices.dsp.etiqa.com/",
								"MotorInsuranceDocGenWSImplService");
						QName portname = new QName("http://motor.wcservices.dsp.etiqa.com/",
								"MotorInsuranceDocGenWSImplPort");
						Service service = Service.create(null, servicename);
						service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
								documenturl + "/MotorInsuranceDocGenWSImplService");
						Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
								Service.Mode.MESSAGE);
						MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
						SOAPMessage request1 = mf.createMessage();
						SOAPPart part = request1.getSOAPPart();
						SOAPEnvelope env = part.getEnvelope();
						SOAPBody body = env.getBody();
						// resp = body;
						SOAPElement operation = body.addChildElement("getStatus", "mot",
								"http://motor.wcservices.dsp.etiqa.com/");

						SOAPElement element_param = operation.addChildElement("QQID");
						element_param.addTextNode(QQID);
						SOAPElement element_param_lang = operation.addChildElement("langValue");
						element_param_lang.addTextNode("lan_en");
						SOAPElement element_param_email = operation.addChildElement("emailVal");
						element_param_email.addTextNode("Y");

						request1.saveChanges();
						String result = null;

						if (request1 != null) {
							ByteArrayOutputStream baos = null;
							try {
								baos = new ByteArrayOutputStream();
								request1.writeTo(baos);
								result = baos.toString();
							} catch (Exception e) {
							} finally {
								if (baos != null) {
									try {
										baos.close();
									} catch (IOException ioe) {
									}
								}
							}
						}
						logger.info(result + "document insert");
						// resCode=body ;

						SOAPMessage response1 = dispatch.invoke(request1);
						SOAPBody responsebody = response1.getSOAPBody();
						logger.info("Document generated");
						java.util.Iterator otcupdates = responsebody.getChildElements();
						while (otcupdates.hasNext()) {
							SOAPElement otcupdates1 = (SOAPElement) otcupdates.next();

							java.util.Iterator i = otcupdates1.getChildElements();
							while (i.hasNext()) {

								SOAPElement e = (SOAPElement) i.next();
								java.util.Iterator m = e.getChildElements();
								while (m.hasNext()) {
									SOAPElement e2 = (SOAPElement) m.next();

								}
							}
						}

					} catch (Exception e) {
						// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
						e.printStackTrace();
					}

					// JPJ
					MotorInsuranceJPJService callJPJService = new MotorInsuranceJPJService();
					try {
						callJPJService.JPJInsert(policyNo, vehicleNo, engineNo, chassisNo, idNo, start_date);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					// NCD
					MotorInsuranceISMService callISMService = new MotorInsuranceISMService();
					try {
						callISMService.NCDConfirmService(vehicleNo, idNo, preIns, chassisNo, QQID, policyNo);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
				////////////////////////////// save status for
				////////////////////////////// wtc////////////////////////////////////////////
				else if (TranDetail.get(0).getProductType().indexOf("W") != -1
						|| TranDetail.get(0).getProductType().indexOf("TPT") != -1) {

					policyNo = TranDetail.get(0).getPolicyNo();

					if (policyNo.equals("") || policyNo.equals(null)) {
						policyNo = dao.generatePolicyNoWTC(DSPQQID, id);
					}
					logger.info("WTC Policy No =" + policyNo);
					logger.info("WTC dspqqid No =" + DSPQQID);
					// creating edoc
					// WTCReportGenerateDaoimpl wtc = new WTCReportGenerateDaoimpl();
					/*
					 * try{ WTCReportsGenerateDaoimpl wtc=new WTCReportsGenerateDaoimpl(); List
					 * <String> list=wtc.getGenReportResponse(DSPQQID, "lan_en"); }catch(Exception
					 * e){ e.printStackTrace(); }
					 */

					logger.info("QQIdsession  WTC calling document Gen=" + DSPQQID);

					boolean recFound = false;
					// String param="Location";
					try {
						// String cpfRootUrlDoc = "http://192.168.0.248:7011/CPF-DSP_wtc_r1";

						// URL url = new
						// URL("http://192.168.0.248:7011/CPF-DSP_wtc_r1/WTCDocGenWebServiceImplService?WSDL");

						String cpfRootUrlDoc = "http://cpf:7011/CPF-DSP_SWTC_Doc";// CPF-DSP_WTC_Doc
						// URL url = new
						// URL("http://10.252.21.60:7011:7011/CPF-DSP_wtc_r1/WTCDocGenWebServiceImplService?WSDL");

						QName servicename = new QName("http://wtc.wcservices.dsp.etiqa.com/",
								"WTCDocGenWebServiceImplService");
						QName portname = new QName("http://wtc.wcservices.dsp.etiqa.com/",
								"WTCDocGenWebServiceImplServicePort");
						Service service = Service.create(null, servicename);
						service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
								cpfRootUrlDoc + "/WTCDocGenWebServiceImplService?WSDL");
						Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
								Service.Mode.MESSAGE);
						MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

						SOAPMessage request1 = mf.createMessage();
						SOAPPart part = request1.getSOAPPart();

						SOAPEnvelope env = part.getEnvelope();
						SOAPBody body = env.getBody();
						// resp = body;
						SOAPElement operation = body.addChildElement("getWTCStatus", "getWTCStatusResponse",
								"http://wtc.wcservices.dsp.etiqa.com/");

						SOAPElement element_param = operation.addChildElement("forAllWTCDocQQID");
						element_param.addTextNode(DSPQQID);
						SOAPElement element_param1 = operation.addChildElement("langValue");
						element_param1.addTextNode("lan_en");
						SOAPElement element_param2 = operation.addChildElement("emailVal");
						element_param2.addTextNode("Y");
						request1.saveChanges();
						String result = null;

						if (request1 != null) {
							ByteArrayOutputStream baos = null;
							try {
								baos = new ByteArrayOutputStream();
								request1.writeTo(baos);
								result = baos.toString();
							} catch (Exception e) {
							} finally {
								if (baos != null) {
									try {
										baos.close();
									} catch (IOException ioe) {
									}
								}
							}
						}
						logger.info("WTC DOC GEN" + result);
						// resCode=body ;
						SOAPMessage response1 = dispatch.invoke(request1);
						SOAPBody responsebody = response1.getSOAPBody();
						java.util.Iterator otcupdates = responsebody.getChildElements();
						while (otcupdates.hasNext()) {
							SOAPElement otcupdates1 = (SOAPElement) otcupdates.next();

							java.util.Iterator i = otcupdates1.getChildElements();
							while (i.hasNext()) {

								SOAPElement e = (SOAPElement) i.next();
								java.util.Iterator m = e.getChildElements();
								while (m.hasNext()) {
									SOAPElement e2 = (SOAPElement) m.next();

								}
							}
						}
						logger.info("WTC document generated result document insert");
					} catch (Exception e) {
						// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
						e.printStackTrace();
					}

					// *********** end document gen Service Call

				}

				////////////////////// save status for hohh//////////////////

				else if (TranDetail.get(0).getProductType().indexOf("H") != -1) {

					policyNo = dao.generatePolicyNoHOHH(DSPQQID, id);

					String dspCpfHOHHEdocsINSURL = null;

					logger.info("testing------------>");
					// if(TranDetail.get(0).getProductCode().indexOf("T")!=-1){
					if (policyNo.indexOf("F") != -1) {
						dspCpfHOHHEdocsINSURL = "http://cpf:7011/CPF-DSP-SHOHH-INSURANCE-DOC";
						// dspCpfHOHHEdocsINSURL="http://cpf:7011/CPF-DSP-HOHH-INS-DOC";
					} else {
						dspCpfHOHHEdocsINSURL = "http://cpf:7011/CPF-DSP-SHOHH-TAKAFUL-DOC";
						// dspCpfHOHHEdocsINSURL="http://cpf:7011/CPF-DSP-HOHH-TKF-DOC";
					}
					try {

						QName servicename = new QName("http://hohh.wcservices.dsp.etiqa.com/",
								"HOHHDocGenWebServiceImplService");
						QName portname = new QName("http://hohh.wcservices.dsp.etiqa.com/",
								"HOHHDocGenWebServiceImplPort");
						Service service = Service.create(null, servicename);
						service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
								dspCpfHOHHEdocsINSURL + "/HOHHDocGenWebServiceImplService");
						Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
								Service.Mode.MESSAGE);
						MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
						SOAPMessage request1 = mf.createMessage();
						SOAPPart part = request1.getSOAPPart();
						SOAPEnvelope env = part.getEnvelope();
						SOAPBody body = env.getBody();
						// resp = body;
						SOAPElement operation = body.addChildElement("getHOHHStatus", "hohh",
								"http://hohh.wcservices.dsp.etiqa.com/");

						SOAPElement element_param = operation.addChildElement("forAllWTCDocQQID");
						element_param.addTextNode(DSPQQID);
						SOAPElement element_param1 = operation.addChildElement("emailVal");
						element_param1.addTextNode("Y");

						request1.saveChanges();

						// logger.info("Request from cpf " + body);
						String result = null;

						if (request != null) {
							ByteArrayOutputStream baos = null;
							try {
								baos = new ByteArrayOutputStream();
								request1.writeTo(baos);
								result = baos.toString();
							} catch (Exception e) {
							} finally {
								if (baos != null) {
									try {
										baos.close();
									} catch (IOException ioe) {
									}
								}
							}
						}
						logger.info(result + "document insert");
						// resCode=body ;

						SOAPMessage response1 = dispatch.invoke(request1);
						SOAPBody responsebody = response1.getSOAPBody();
						java.util.Iterator otcupdates = responsebody.getChildElements();
						while (otcupdates.hasNext()) {
							SOAPElement otcupdates1 = (SOAPElement) otcupdates.next();

							java.util.Iterator i = otcupdates1.getChildElements();
							while (i.hasNext()) {

								SOAPElement e = (SOAPElement) i.next();
								java.util.Iterator m = e.getChildElements();
								while (m.hasNext()) {
									SOAPElement e2 = (SOAPElement) m.next();

								}
							}
						}

					} catch (Exception e) {
						// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
						e.printStackTrace();
						logger.info(e);
					}
				}

				// Travel Ezy start
				else if (TranDetail.get(0).getProductType().indexOf("Travel Ezy") != -1) {
					policyNo = TranDetail.get(0).getPolicyNo();

					if (policyNo.equals("") || policyNo.equals(null)) {
						policyNo = dao.generatePolicyNoTravEz(DSPQQID, id);
					}
					logger.info("====================TranDetail Travel Ezy Policy No =" + policyNo);
					logger.info("====================TranDetail Travel Ezy dspqqid No =" + DSPQQID);
					try {
						String lang = "lan_en";

						URL url = new URL(
								"http://cpf:7011/CPF-DSP-TravelEzy-Doc/TravelEzyDocGenWebServiceImplService?WSDL");
						QName servicename = new QName(URL_DOCGEN, "TravelEzyDocGenWebServiceImplService");
						QName portname = new QName(URL_DOCGEN, "TravelEzyDocGenWebServiceImplServicePort");
						Service service = Service.create(null, servicename);
						service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
								"http://cpf:7011/CPF-DSP-TravelEzy-Doc/TravelEzyDocGenWebServiceImplService");
						Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
								Service.Mode.MESSAGE);
						MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
						SOAPMessage req = mf.createMessage();
						SOAPPart part = req.getSOAPPart();
						SOAPEnvelope env = part.getEnvelope();
						SOAPBody body = env.getBody();

						SOAPElement operation = body.addChildElement("getQQ4DocStatus", "getQQ4DocStatusResponse",
								URL_DOCGEN);
						SOAPElement element_param = operation.addChildElement("forAllDocQQID");
						element_param.addTextNode(DSPQQID);

						SOAPElement elementParam1 = operation.addChildElement("langValue");
						elementParam1.addTextNode(lang);

						SOAPElement elementParam4 = operation.addChildElement("emailVal");
						elementParam4.addTextNode("Y");

						SOAPElement elementParam5 = operation.addChildElement("isAdmin");
						elementParam5.addTextNode("false");

						req.saveChanges();

						String result = null;
						if (req != null) {
							ByteArrayOutputStream baos = null;

							try {
								baos = new ByteArrayOutputStream();
								req.writeTo(baos);
								result = baos.toString();
							} catch (Exception e) {

							} finally {
								if (baos != null) {
									try {
										baos.close();
									} catch (IOException ioe) {
									}
								}
							}
						}

						SOAPMessage res = dispatch.invoke(req);
						SOAPBody responsebody = res.getSOAPBody();

					} catch (Exception e) {
						e.printStackTrace();
						logger.info(e);
					}

				}
				// Travel Ezy end

				////////////////////////////// save status for
				////////////////////////////// ezylife////////////////////////////////////////////
				else if (TranDetail.get(0).getProductType().indexOf("E") != -1) {

					logger.info("Ezylife dspqqid No =" + DSPQQID);

					// start generate doc for ezylife based on DSPQQID

					logger.info("QQIdsession  Ezy life calling document Gen=" + DSPQQID);

					boolean recFound = false;
					// String param="Location";
					try {
						// String cpfRootUrlDoc = "http://192.168.0.248:7011/CPF-DSP_TL_BM";

						String cpfRootUrlDoc = "http://cpf:7011/CPF-DSP_TL_BM";

						QName servicename = new QName("http://termlife.wcservices.dsp.etiqa.com/",
								"TermLifeDocGenWebServiceImplService");
						QName portname = new QName("http://termlife.wcservices.dsp.etiqa.com/",
								"TermLifeDocGenWebServiceImplPort");
						Service service = Service.create(null, servicename);
						service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
								cpfRootUrlDoc + "/TermLifeDocGenWebServiceImplService?WSDL");
						Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
								Service.Mode.MESSAGE);
						MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

						SOAPMessage request1 = mf.createMessage();
						SOAPPart part = request1.getSOAPPart();

						SOAPEnvelope env = part.getEnvelope();
						SOAPBody body = env.getBody();
						// resp = body;
						SOAPElement operation = body.addChildElement("getStatus", "ter",
								"http://termlife.wcservices.dsp.etiqa.com/");

						SOAPElement element_param = operation.addChildElement("forAllDocQQID");
						element_param.addTextNode(DSPQQID);
						SOAPElement element_param1 = operation.addChildElement("langValue");
						element_param1.addTextNode("E");
						SOAPElement element_param2 = operation.addChildElement("emailVal");
						element_param2.addTextNode("Y");
						request1.saveChanges();
						String result = null;

						if (request1 != null) {
							ByteArrayOutputStream baos = null;
							try {
								baos = new ByteArrayOutputStream();
								request1.writeTo(baos);
								result = baos.toString();
							} catch (Exception e) {
							} finally {
								if (baos != null) {
									try {
										baos.close();
									} catch (IOException ioe) {
									}
								}
							}
						}
						logger.info("Ezy Life DOC GEN" + result);
						// resCode=body ;
						SOAPMessage response1 = dispatch.invoke(request1);
						SOAPBody responsebody = response1.getSOAPBody();
						java.util.Iterator otcupdates = responsebody.getChildElements();
						/*
						 * while (otcupdates.hasNext()) { SOAPElement otcupdates1 =
						 * (SOAPElement)otcupdates.next();
						 *
						 * java.util.Iterator i = otcupdates1.getChildElements(); while( i.hasNext() ){
						 *
						 * SOAPElement e = (SOAPElement)i.next(); java.util.Iterator m =
						 * e.getChildElements(); while( m.hasNext() ){ SOAPElement e2 =
						 * (SOAPElement)m.next();
						 *
						 * } } }
						 */
						logger.info("ezy life document generated result document insert");
					} catch (Exception e) {
						// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
						e.printStackTrace();
					}
				}

					// *********** end document gen Service Call
				
//////////////////////////////CANCER INSURANCE////////////////////////////////////////////
				else if (TranDetail.get(0).getProductType().indexOf("e-Cancer") != -1) {
					
					logger.info("entered the pcca01 block");
					String productcode = null;
					String product=TranDetail.get(0).getProductType();
					if("e-CancerCare".equalsIgnoreCase(product)){
						productcode = "PCCA01";
					}else if("e-CancerCare Takaful".equalsIgnoreCase(product)){
						productcode = "PTCA01";
					}


					String hrm_resPonseData;
			
					try {

						String QQ_ID = TranDetail.get(0).getDSPQQID();
						Map<String,Object> mapResponse = new HashMap<String, Object>();
				
						MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
						map.add("DSP_QQ_ID", QQ_ID);
						map.add("LANGUAGE", "E");
						map.add("EMAILVALUE", "Y");
						map.add("PRODUCTCODE", productcode);
						
						RestTemplate restTemplate = new RestTemplate();
						hrm_resPonseData = restTemplate.postForObject("http://cpf:7011/CPF-DSP_CC_Doc/documents/documentGenarator",map ,  String.class);			
						logger.info("  RESPONSE CO::::   "+hrm_resPonseData);
						
					
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				
				
				}


				else if (TranDetail.get(0).getProductType().indexOf("Medical") != -1) {
					

					String language = null;
					String hrm_resPonseData;
					String Company = null;
					

					
					String docId = dao.generatePolicy_MP(id);
					
					String product=TranDetail.get(0).getProductType();
					if("Medical Pass".equalsIgnoreCase(product)){
						Company = "I";
					}else if("Medical Pass Takaful".equalsIgnoreCase(product)){
						Company = "T";
					}
					
					language =  dao.getCustomerlanguage(docId);
					
					try {
						
						Map<String,Object> mapResponse = new HashMap<String, Object>();
				
						MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
						map.add("DSP_QQ_ID", docId);
						map.add("LANGUAGE", language);
						map.add("EMAILVALUE", "Y");
						map.add("COMPANY", Company);
						
						RestTemplate restTemplate = new RestTemplate();
						hrm_resPonseData = restTemplate.postForObject("http://cpf:7011/CPF-DSP_MP_Doc/medical/documentGenarator",map ,  String.class);			
						logger.info("  RESPONSE CO::::   "+hrm_resPonseData);
						
					
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				
			
				}


				 else if (TranDetail.get(0).getProductType().indexOf("D") != -1) {

					boolean recFound = false;
					// String param="Location";
					try {
						// String cpfRootUrlDoc = "http://192.168.0.248:7011/CPF-DSP_TL_BM";
						String cpfRootUrlDoc = "http://cpf:7011/CPF-DSP_IDS";

						QName servicename = new QName("http://termlife.wcservices.dsp.etiqa.com/",
								"TermLifeDocGenWebServiceImplService");
						QName portname = new QName("http://termlife.wcservices.dsp.etiqa.com/",
								"TermLifeDocGenWebServiceImplPort");
						Service service = Service.create(null, servicename);
						service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
								cpfRootUrlDoc + "/TermLifeDocGenWebServiceImplService?WSDL");
						Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
								Service.Mode.MESSAGE);
						MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

						SOAPMessage request1 = mf.createMessage();
						SOAPPart part = request1.getSOAPPart();

						SOAPEnvelope env = part.getEnvelope();
						SOAPBody body = env.getBody();
						// resp = body;
						SOAPElement operation = body.addChildElement("getStatus", "ter",
								"http://termlife.wcservices.dsp.etiqa.com/");

						SOAPElement element_param = operation.addChildElement("forAllDocQQID");
						element_param.addTextNode(DSPQQID);
						SOAPElement element_param1 = operation.addChildElement("langValue");
						element_param1.addTextNode("E");
						SOAPElement element_param2 = operation.addChildElement("emailVal");
						element_param2.addTextNode("Y");
						request1.saveChanges();
						String result = null;

						if (request1 != null) {
							ByteArrayOutputStream baos = null;
							try {
								baos = new ByteArrayOutputStream();
								request1.writeTo(baos);
								result = baos.toString();
							} catch (Exception e) {
							} finally {
								if (baos != null) {
									try {
										baos.close();
									} catch (IOException ioe) {
									}
								}
							}
						}
						// logger.info("Ezy Life DOC GEN"+result);
						// resCode=body ;
						SOAPMessage response1 = dispatch.invoke(request1);
						SOAPBody responsebody = response1.getSOAPBody();
						logger.info("regenerate doc IDS " + responsebody);

						/*
						 * java.util.Iterator otcupdates = responsebody.getChildElements(); while
						 * (otcupdates.hasNext()) { SOAPElement otcupdates1 =
						 * (SOAPElement)otcupdates.next();
						 *
						 * java.util.Iterator i = otcupdates1.getChildElements(); while( i.hasNext() ){
						 *
						 * SOAPElement e = (SOAPElement)i.next(); java.util.Iterator m =
						 * e.getChildElements(); while( m.hasNext() ){ SOAPElement e2 =
						 * (SOAPElement)m.next();
						 *
						 * } } }
						 */
						// logger.info( "ezy life document generated result document insert");
					} catch (Exception e) {
						// docStatus="Fail";
						// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
						e.printStackTrace();
					}

				}

				// buddy start
				else if (TranDetail.get(0).getProductType().indexOf("Buddy") != -1) {
					policyNo = TranDetail.get(0).getPolicyNo();

					if (policyNo.equals("") || policyNo.equals(null)) {
						policyNo = dao.generatePolicyNoBuddy(DSPQQID, id);
					}
					logger.info("====================TranDetail Buddy Policy No =" + policyNo);
					logger.info("====================TranDetail Buddy dspqqid No =" + DSPQQID);
					try {

						String lang = "lan_en";

						URL url = new URL("http://cpf:7011/buddydoc/BuddyDocGenWebServiceImplService?WSDL");
						QName servicename = new QName(URL_BUDDY_DOCGEN, "BuddyDocGenWebServiceImplService");
						QName portname = new QName(URL_BUDDY_DOCGEN, "BuddyDocGenWebServiceImplServicePort");

						Service service = Service.create(null, servicename);
						service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
								"http://cpf:7011/buddydoc/BuddyDocGenWebServiceImplService");

						Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
								Service.Mode.MESSAGE);
						MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

						SOAPMessage req = mf.createMessage();
						SOAPPart part = req.getSOAPPart();
						SOAPEnvelope env = part.getEnvelope();
						SOAPBody body = env.getBody();

						SOAPElement operation = body.addChildElement("getStatus", "getStatusResponse",
								URL_BUDDY_DOCGEN);
						SOAPElement elementParam = operation.addChildElement("forAllDocQQID");
						elementParam.addTextNode(DSPQQID);

						SOAPElement elementParam1 = operation.addChildElement("langValue");
						elementParam1.addTextNode(lang);

						SOAPElement elementParam2 = operation.addChildElement("company");
						elementParam2.addTextNode("T");

						SOAPElement elementParam3 = operation.addChildElement("transactiontype");
						elementParam3.addTextNode("normalcustomer");

						SOAPElement elementParam4 = operation.addChildElement("emailVal");
						elementParam4.addTextNode("Y");

						SOAPElement elementParam5 = operation.addChildElement("isAdmin");
						elementParam5.addTextNode("false");

						req.saveChanges();

						String result = null;
						if (req != null) {
							ByteArrayOutputStream baos = null;

							try {
								baos = new ByteArrayOutputStream();
								req.writeTo(baos);
								result = baos.toString();

							} catch (Exception e) {

							} finally {
								if (baos != null) {
									try {
										baos.close();
									} catch (IOException ioe) {

									}
								}
							}
						}

						SOAPMessage res = dispatch.invoke(req);
						SOAPBody responsebody = res.getSOAPBody();

					} catch (Exception e) {

						e.printStackTrace();
						logger.info(e);
					}
				}
				// buddy end
				// Trip Care start
				else if (TranDetail.get(0).getProductType().indexOf("TripCare") != -1) {
					policyNo = TranDetail.get(0).getPolicyNo();

					if (policyNo.equals("") || policyNo.equals(null)) {
						policyNo = dao.generatePolicyNoTripCare(DSPQQID, id);
					}
					logger.info("====================TranDetail TripCare Policy No =" + policyNo);
					logger.info("====================TranDetail TripCare dspqqid No =" + DSPQQID);
					try {
						String lang = "lan_en";

						URL url = new URL(
								"http://cpf:7011/CPF-DSP-TripCare360-Doc/TripCare360DocGenWebServiceImplService?WSDL");
						QName servicename = new QName(URL_DOCGEN, "TripCare360DocGenWebServiceImplService");
						QName portname = new QName(URL_DOCGEN, "TripCare360DocGenWebServiceImplServicePort");

						Service service = Service.create(null, servicename);
						service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
								"http://cpf:7011/CPF-DSP-TripCare360-Doc/TripCare360DocGenWebServiceImplService");

						Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
								Service.Mode.MESSAGE);
						MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

						SOAPMessage req = mf.createMessage();
						SOAPPart part = req.getSOAPPart();
						SOAPEnvelope env = part.getEnvelope();
						SOAPBody body = env.getBody();

						SOAPElement operation = body.addChildElement("getQQ4DocStatus", "getQQ4DocStatusResponse",
								URL_DOCGEN);
						SOAPElement elementParam = operation.addChildElement("forAllDocQQID");
						elementParam.addTextNode(DSPQQID);

						SOAPElement elementParam1 = operation.addChildElement("langValue");
						elementParam1.addTextNode(lang);

						SOAPElement elementParam4 = operation.addChildElement("emailVal");
						elementParam4.addTextNode("Y");

						SOAPElement elementParam5 = operation.addChildElement("isAdmin");
						elementParam5.addTextNode("false");

						req.saveChanges();

						String result = null;
						if (req != null) {
							ByteArrayOutputStream baos = null;

							try {
								baos = new ByteArrayOutputStream();
								req.writeTo(baos);
								result = baos.toString();

							} catch (Exception e) {

							} finally {
								if (baos != null) {
									try {
										baos.close();
									} catch (IOException ioe) {
									}
								}
							}
						}

						SOAPMessage res = dispatch.invoke(req);
						SOAPBody responsebody = res.getSOAPBody();

					} catch (Exception e) {
						e.printStackTrace();
						logger.info(e);
					}

				}
				// Trip Care end
			}

			// else if(TranDetail.get(0).getStatus().equals("S") && status.equals("C")){
			// //change from success to cancel transaction){

			else if (preData == "S" && TranDetail.get(0).getStatus().equals("C") && status.equals("C")) {
				if (TranDetail.get(0).getProductType().indexOf("MI") != -1 || TranDetail.get(0).getProductType().indexOf("MT") != -1 || 
						TranDetail.get(0).getProductType().indexOf("Mo") != -1) { // applicable for motor only

					// JPJ
					MotorInsuranceJPJService callJPJService = new MotorInsuranceJPJService();
					try {
						callJPJService.JPJCancel(policyNo, vehicleNo, engineNo, chassisNo, idNo, start_date);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			}
			/*
			 * // Todo - Chandra // To insert the Transaction audit details in
			 * DSP_COMMON_TBL_PAYMENT_AUDIT table String loginUser = (String)
			 * session.getAttribute("user"); String userRole = (String)
			 * session.getAttribute("userRole"); String
			 * dspQQID=request.getParameter("dspQQID").trim();
			 *
			 * logger.info("TxnReportController<saveTxnInfo><txnID> => "+id);
			 * logger.info("TxnReportController<saveTxnInfo><loginUser> => "
			 * +loginUser);
			 * logger.info("TxnReportController<saveTxnInfo><userRole> => "+userRole)
			 * ;
			 * logger.info("TxnReportController<saveTxnInfo><dspQQID> => "+dspQQID);
			 *
			 * logger.info("TxnReportController<saveTxnInfo><policyNo> => "+policyNo)
			 * ;
			 *
			 *
			 * if(id > 0 ) { ReportDAO custAudit = new ReportDAOImpl(); String pAudit =
			 * custAudit.insertPaymentforAudit(id, loginUser, Integer.parseInt(userRole),
			 * Integer.parseInt(dspQQID), policyNo );
			 *
			 * logger.info("TxnReportController<saveTxnInfo><pAudit> => "+pAudit); }
			 * //End
			 */ // try {
				// Thread.sleep(6000); //6 seconds to wait JPJ update
				// } catch(InterruptedException ex) {
				// Thread.currentThread().interrupt();
				// }
			List<TransactionalReport> TranDetailNew = dao.getTransactionDetail(id);
			logger.info("policyNo after change status" + TranDetailNew.get(0).getPolicyNo());
			request.setAttribute("data", TranDetailNew);
			forward(request, response, redirect);

		} else {

			List<TransactionalReport> TranDetailNew = dao.getTransactionDetail(id);
			request.setAttribute("data", TranDetailNew);
			forward(request, response, redirect);
			logger.info("No changes allowed");

		}
	}

	private void generateCAPS(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub

		redirect = "admin-transaction-details.jsp";

		int id = Integer.parseInt(request.getParameter("id").trim());
		String langValue = "lan_en";

		ReportDAO dao = new ReportDAOImpl();// daoFactory.getAgentDAO();

		String QQID = dao.generateCAPSPolicy(id);

		logger.info(QQID);

		MotorInsuranceReportsGenDao genEmailDoc = new MotorInsuranceReportsGenDaoImpl();
		// List<String>
		// genEmailDocResult=genEmailDoc.getMIGenReportResponse(QQID,langValue);

		String documenturl = "http://cpf:7011/CPF-DSP_SMI_Doc"; // http://cpf:7011/CPF-DSP_SMT_Doc
		try {
			// http://192.168.0.247:7005/CPF20092016/MotorInsuranceDocGenWSImplService?WSDL
			// URL url = new URL(documenturl+"/MotorQQDataImplService?WSDL");
			QName servicename = new QName("http://motor.wcservices.dsp.etiqa.com/",
					"MotorInsuranceDocGenWSImplService");
			QName portname = new QName("http://motor.wcservices.dsp.etiqa.com/", "MotorInsuranceDocGenWSImplPort");
			Service service = Service.create(null, servicename);
			service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
					documenturl + "/MotorInsuranceDocGenWSImplService");
			Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class, Service.Mode.MESSAGE);
			MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
			SOAPMessage request1 = mf.createMessage();
			SOAPPart part = request1.getSOAPPart();
			SOAPEnvelope env = part.getEnvelope();
			SOAPBody body = env.getBody();
			// resp = body;
			SOAPElement operation = body.addChildElement("getStatus", "mot", "http://motor.wcservices.dsp.etiqa.com/");

			SOAPElement element_param = operation.addChildElement("QQID");
			element_param.addTextNode(QQID);
			SOAPElement element_param_lang = operation.addChildElement("langValue");
			element_param_lang.addTextNode("lan_en");
			SOAPElement element_param_email = operation.addChildElement("emailVal");
			element_param_email.addTextNode("Y");
			request1.saveChanges();
			String result = null;

			if (request1 != null) {
				ByteArrayOutputStream baos = null;
				try {
					baos = new ByteArrayOutputStream();
					request1.writeTo(baos);
					result = baos.toString();
				} catch (Exception e) {
				} finally {
					if (baos != null) {
						try {
							baos.close();
						} catch (IOException ioe) {
						}
					}
				}
			}
			logger.info(result + "document insert");
			// resCode=body ;

			SOAPMessage response1 = dispatch.invoke(request1);
			SOAPBody responsebody = response1.getSOAPBody();
			java.util.Iterator otcupdates = responsebody.getChildElements();
			while (otcupdates.hasNext()) {
				SOAPElement otcupdates1 = (SOAPElement) otcupdates.next();

				java.util.Iterator i = otcupdates1.getChildElements();
				while (i.hasNext()) {

					SOAPElement e = (SOAPElement) i.next();
					java.util.Iterator m = e.getChildElements();
					while (m.hasNext()) {
						SOAPElement e2 = (SOAPElement) m.next();

					}
				}
			}

		} catch (Exception e) {
			// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
			e.printStackTrace();
		}

		List<TransactionalReport> TranDetail = dao.getTransactionDetail(id);
		request.setAttribute("data", TranDetail);
		forward(request, response, redirect);

	}

	private void checkJPJISMStatus(HttpServletRequest request, HttpServletResponse response) {

		redirect = "admin-transaction-details.jsp";

		int id = Integer.parseInt(request.getParameter("id").trim());
		String langValue = "lan_en";
		String policyNo = "";
		String vehicleNo = "";
		String engineNo = "";
		String chassisNo = "";
		String idNo = "";
		String startDate = "";
		String endDate = "";
		String preIns = "";
		// String insCom= TranDetail.get(0).getInsCompany();
		String productType = "";
		String day_s = "";
		String mon_s = "";
		String yy_s = "";
		String start_date = "";

		String day_s_jpj = "";
		String mon_s_jpj = "";
		String yy_s_jpj = "";
		String start_date_jpj = "";

		String day_e_jpj = "";
		String mon_e_jpj = "";
		String yy_e_jpj = "";
		String end_date_jpj = "";

		String QQID = "";

		ReportDAO dao = new ReportDAOImpl();// daoFactory.getAgentDAO();

		List<TransactionalReport> TranDetail = dao.getTransactionDetail(id);

		policyNo = TranDetail.get(0).getPolicyNo();
		vehicleNo = TranDetail.get(0).getVehicleNo();
		engineNo = TranDetail.get(0).getEngineNo();
		chassisNo = TranDetail.get(0).getChassisNo();
		idNo = TranDetail.get(0).getCustomerNRIC();
		startDate = TranDetail.get(0).getCoverageStartDate();
		endDate = TranDetail.get(0).getCoverageEndDate();
		preIns = TranDetail.get(0).getPreviousInsurance();
		// String insCom= TranDetail.get(0).getInsCompany();
		productType = TranDetail.get(0).getProductType();
		QQID = TranDetail.get(0).getQQID();

		// logger.info(productType);
		// logger.info(startDate);

		day_s = startDate.substring(0, 2);
		mon_s = startDate.substring(2, 4);
		yy_s = startDate.substring(4, 8);

		start_date = day_s + "/" + mon_s + "/" + yy_s;

		// to check JPJ status

		day_s_jpj = startDate.substring(0, 2);
		mon_s_jpj = startDate.substring(2, 4);
		yy_s_jpj = startDate.substring(4, 8);

		start_date_jpj = yy_s_jpj + mon_s_jpj + day_s_jpj;

		day_e_jpj = endDate.substring(0, 2);
		mon_e_jpj = endDate.substring(2, 4);
		yy_e_jpj = endDate.substring(4, 8);

		end_date_jpj = yy_e_jpj + mon_e_jpj + day_e_jpj;

		// logger.info(start_date);

		logger.info("policyNo:" + policyNo);
		logger.info("vehicleNo:" + vehicleNo);
		logger.info("start_date:" + start_date_jpj);
		logger.info("end_date:" + end_date_jpj);

		String JPJStatus = dao.checkJPJStatus(policyNo, vehicleNo, start_date_jpj, end_date_jpj);

		logger.info("JPJStatus:" + JPJStatus);

		String JPJMsg = "";

		// if(JPJStatus =="02" || JPJStatus == null || JPJStatus == ""){

		if (JPJStatus == null || JPJStatus == "" || JPJStatus == "02") {

			// JPJMsg="New";

			// 02= not success - can insert again
			logger.info("Insert JPJ here for " + policyNo);

			// insert into edoc table
			// JPJ
			MotorInsuranceJPJService callJPJService = new MotorInsuranceJPJService();
			try {
				callJPJService.JPJInsert(policyNo, vehicleNo, engineNo, chassisNo, idNo, start_date);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			JPJMsg = "Waiting for JPJ status update..";

		}

		logger.info("JPJMsg:" + JPJMsg);
		logger.info("QQID:" + QQID);
		int ISMStatus = dao.checkISMStatus(QQID);
		logger.info("ISM Record Count:" + ISMStatus);

		if (ISMStatus == 0) { // count - no record exist

			logger.info("Insert ISM here for " + policyNo + " and Vehicle No " + vehicleNo);

			// insert into ISM NCD table
			// NCD
			MotorInsuranceISMService callISMService = new MotorInsuranceISMService();
			try {
				callISMService.NCDConfirmService(vehicleNo, idNo, preIns, chassisNo, QQID, policyNo);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		List<TransactionalReport> TranDetail_new = dao.getTransactionDetail(id);
		request.setAttribute("data", TranDetail_new);
		request.setAttribute("JPJStatusMsg", JPJMsg);
		forward(request, response, redirect);

	}
	/* Document Generation Code Starts Here */

	private void generateDoc(HttpServletRequest request, HttpServletResponse response) {

		redirect = "admin-transaction-details.jsp";

		String policyNo = request.getParameter("policyNo").trim();
		String docId = request.getParameter("docId").trim();
		int id = Integer.parseInt(request.getParameter("id").trim());
		String term = request.getParameter("term").trim();
		String simpleNote = request.getParameter("simpleNote").trim();

		// logger.info("Send email for POLICY NO = "+term);
		ReportDAO dao = new ReportDAOImpl();

		String QQID = docId;// dao.generatePolicyNo(id);

		logger.info("QQID =" + QQID);
		logger.info("simpleNote =" + simpleNote);
		String docStatus = "";

		if (term.equals("EZYTL")) {
			term = "YT";
		} else if (term.equals("ISCTL")) {
			term = "CT";
		}
		logger.info("term =" + term);
		String emailStatus = "";

		if (term.indexOf("MI") != -1  || term.indexOf("Mo") != -1 || term.indexOf("MT") != -1) {

			String documenturl = "", methodName = "";

			if (policyNo.indexOf("V") != -1) {
				if (simpleNote.equals("SS")) {
					documenturl = "http://cpf:7011/CPF-DSP_SMI_Doc"; // CPF-DSP_MIDT_Doc
					methodName = "getStatusOfDocGenOnlyForSMI";
				} else {
					documenturl = "http://cpf:7011/CPF-DSP_MIDT_Doc"; // CPF-DSP_MT_Doc
					methodName = "getStatusOfDocGenOnlyForMI";
				}
			} else {
				if (simpleNote.equals("SS")) {
					documenturl = "http://cpf:7011/CPF-DSP_SMT_Doc"; // CPF-DSP_MIDT_Doc
					methodName = "getStatusOfDocGenOnlyForSMT";
				} else {
					documenturl = "http://cpf:7011/CPF-DSP_MT_Doc"; // CPF-DSP_MT_Doc
					methodName = "getStatusOfDocGenOnlyForMT";
				}
			}

			try {
				// http://192.168.0.247:7005/CPF20092016/MotorInsuranceDocGenWSImplService?WSDL
				// URL url = new URL(documenturl+"/MotorQQDataImplService?WSDL");
				QName servicename = new QName("http://motor.wcservices.dsp.etiqa.com/",
						"MotorInsuranceDocGenWSImplService");
				QName portname = new QName("http://motor.wcservices.dsp.etiqa.com/", "MotorInsuranceDocGenWSImplPort");
				Service service = Service.create(null, servicename);
				service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
						documenturl + "/MotorInsuranceDocGenWSImplService");
				Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
						Service.Mode.MESSAGE);
				MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
				SOAPMessage request1 = mf.createMessage();
				SOAPPart part = request1.getSOAPPart();
				SOAPEnvelope env = part.getEnvelope();
				SOAPBody body = env.getBody();
				// resp = body;
				SOAPElement operation = body.addChildElement(methodName, "mot",
						"http://motor.wcservices.dsp.etiqa.com/");
				SOAPElement element_param = operation.addChildElement("QQID");
				element_param.addTextNode(QQID);
				/*
				 * SOAPElement element_param = operation.addChildElement("QQID");
				 * element_param.addTextNode(docId);
				 */
				SOAPElement element_param_lang = operation.addChildElement("langValue");
				element_param_lang.addTextNode("lan_en");
				SOAPElement element_param_email = operation.addChildElement("emailVal");
				element_param_email.addTextNode("N");

				request1.saveChanges();
				String result = null;

				if (request1 != null) {
					ByteArrayOutputStream baos = null;
					try {
						baos = new ByteArrayOutputStream();
						request1.writeTo(baos);
						result = baos.toString();
					} catch (Exception e) {
					} finally {
						if (baos != null) {
							try {
								baos.close();
							} catch (IOException ioe) {
							}
						}
					}
				}
				// logger.info(result+"document insert");
				// resCode=body ;

				SOAPMessage response1 = dispatch.invoke(request1);
				SOAPBody responsebody = response1.getSOAPBody();
				logger.info("regenerate doc Motor" + responsebody);
				/*
				 * java.util.Iterator otcupdates = responsebody.getChildElements(); while
				 * (otcupdates.hasNext()) { SOAPElement otcupdates1 =
				 * (SOAPElement)otcupdates.next();
				 *
				 * java.util.Iterator i = otcupdates1.getChildElements(); while( i.hasNext() ){
				 *
				 * SOAPElement e = (SOAPElement)i.next(); java.util.Iterator m =
				 * e.getChildElements(); while( m.hasNext() ){ SOAPElement e2 =
				 * (SOAPElement)m.next();
				 *
				 * } } }
				 */

			} catch (Exception e) {

				docStatus = "Fail";
				// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
				e.printStackTrace();
			}

		} else if (term.indexOf("TL") != -1) {

			boolean recFound = false;
			// String param="Location";
			try {
				// String cpfRootUrlDoc = "http://192.168.0.248:7011/CPF-DSP_TL_BM";

				String cpfRootUrlDoc = "http://cpf:7011/CPF-DSP_TL_BM";

				QName servicename = new QName("http://termlife.wcservices.dsp.etiqa.com/",
						"TermLifeDocGenWebServiceImplService");
				QName portname = new QName("http://termlife.wcservices.dsp.etiqa.com/",
						"TermLifeDocGenWebServiceImplPort");
				Service service = Service.create(null, servicename);
				service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
						cpfRootUrlDoc + "/TermLifeDocGenWebServiceImplService?WSDL");
				Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
						Service.Mode.MESSAGE);
				MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

				SOAPMessage request1 = mf.createMessage();
				SOAPPart part = request1.getSOAPPart();

				SOAPEnvelope env = part.getEnvelope();
				SOAPBody body = env.getBody();
				// resp = body;
				SOAPElement operation = body.addChildElement("getStatusOfDocGenOnlyForTL", "ter",
						"http://termlife.wcservices.dsp.etiqa.com/");

				SOAPElement element_param = operation.addChildElement("forAllDocQQID");
				element_param.addTextNode(docId);
				SOAPElement element_param1 = operation.addChildElement("langValue");
				element_param1.addTextNode("E");
				/*
				 * SOAPElement element_param2 = operation.addChildElement("emailVal");
				 * element_param2.addTextNode("N");
				 */
				request1.saveChanges();
				String result = null;

				if (request1 != null) {
					ByteArrayOutputStream baos = null;
					try {
						baos = new ByteArrayOutputStream();
						request1.writeTo(baos);
						result = baos.toString();
					} catch (Exception e) {
					} finally {
						if (baos != null) {
							try {
								baos.close();
							} catch (IOException ioe) {
							}
						}
					}
				}
				// logger.info("Ezy Life DOC GEN"+result);
				// resCode=body ;
				SOAPMessage response1 = dispatch.invoke(request1);
				SOAPBody responsebody = response1.getSOAPBody();
				logger.info("regenerate doc TL " + responsebody);

				/*
				 * java.util.Iterator otcupdates = responsebody.getChildElements(); while
				 * (otcupdates.hasNext()) { SOAPElement otcupdates1 =
				 * (SOAPElement)otcupdates.next();
				 *
				 * java.util.Iterator i = otcupdates1.getChildElements(); while( i.hasNext() ){
				 *
				 * SOAPElement e = (SOAPElement)i.next(); java.util.Iterator m =
				 * e.getChildElements(); while( m.hasNext() ){ SOAPElement e2 =
				 * (SOAPElement)m.next();
				 *
				 * } } }
				 */
				// logger.info( "ezy life document generated result document insert");
			} catch (Exception e) {
				docStatus = "Fail";
				// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
				e.printStackTrace();
			}
		}else if (term.indexOf("PCCA01") != -1 || term.indexOf("PTCA01") != -1 ) {

			logger.info("entered the pcca01 block");
			//String productcode = null;
			/*String product=TranDetail.get(0).getProductType();
			if("e-CancerCare".equalsIgnoreCase(product)){
				productcode = "PCCA01";
			}else if("e-CancerCare Takaful".equalsIgnoreCase(product)){
				productcode = "PTCA01";
			}*/

			/*logger.info("CancerCare dspqqid No =" + DSPQQID);
			logger.info("QQIdsession  CI life calling document Gen=" + DSPQQID);
			logger.info("product code"+product);
			logger.info("product code"+productcode);
	*/
			String hrm_resPonseData;
	
			try {

				//String QQ_ID = TranDetail.get(0).getDSPQQID();
				Map<String,Object> mapResponse = new HashMap<String, Object>();
		
				MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
				map.add("DSP_QQ_ID", docId);
				map.add("LANGUAGE", "E");
				map.add("EMAILVALUE", "N");
				map.add("PRODUCTCODE", term);
				
				RestTemplate restTemplate = new RestTemplate();
				hrm_resPonseData = restTemplate.postForObject("http://cpf:7011/CPF-DSP_CC_Doc/documents/documentGenarator",map ,  String.class);			
				logger.info("  RESPONSE CO::::   "+hrm_resPonseData);
				
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}else if (term.indexOf(QQConstant.MEDICALPASS_INSURANCE) != -1 || term.indexOf(QQConstant.MEDICALPASS_TAKAFUL) != -1 ) {

			logger.info("entered the medical block");
			String language = null;
			String hrm_resPonseData;
			String Company = null;
			
			if(QQConstant.MEDICALPASS_INSURANCE.equalsIgnoreCase(term)){
				Company = "I";
			}else if(QQConstant.MEDICALPASS_TAKAFUL.equalsIgnoreCase(term)){
				Company = "T";
			}
			language =  dao.getCustomerlanguage(docId);
			
			logger.info("entered the medical block" + docId);
			logger.info("entered the medical block" +language);
			logger.info("entered the medical block"+Company);
			try {
				
				Map<String,Object> mapResponse = new HashMap<String, Object>();
		
				MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
				map.add("DSP_QQ_ID", docId);
				map.add("LANGUAGE", language);
				map.add("EMAILVALUE", "N");
				map.add("COMPANY", Company);
				
				RestTemplate restTemplate = new RestTemplate();
				hrm_resPonseData = restTemplate.postForObject("http://cpf:7011/CPF-DSP_MP_Doc/medical/documentGenarator",map ,  String.class);			
				logger.info("  RESPONSE CO::::   "+hrm_resPonseData);
				
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		} else if (term.indexOf("D") != -1) {

			boolean recFound = false;
			// String param="Location";
			try {
				// String cpfRootUrlDoc = "http://192.168.0.248:7011/CPF-DSP_TL_BM";

				String cpfRootUrlDoc = "http://cpf:7011/CPF-DSP_IDS";

				QName servicename = new QName("http://termlife.wcservices.dsp.etiqa.com/",
						"TermLifeDocGenWebServiceImplService");
				QName portname = new QName("http://termlife.wcservices.dsp.etiqa.com/",
						"TermLifeDocGenWebServiceImplPort");
				Service service = Service.create(null, servicename);
				service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
						cpfRootUrlDoc + "/TermLifeDocGenWebServiceImplService?WSDL");
				Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
						Service.Mode.MESSAGE);
				MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

				SOAPMessage request1 = mf.createMessage();
				SOAPPart part = request1.getSOAPPart();

				SOAPEnvelope env = part.getEnvelope();
				SOAPBody body = env.getBody();
				// resp = body;
				SOAPElement operation = body.addChildElement("getStatusOfDocGenOnlyForIDS", "ter",
						"http://termlife.wcservices.dsp.etiqa.com/");

				SOAPElement element_param = operation.addChildElement("forAllDocQQID");
				element_param.addTextNode(docId);
				SOAPElement element_param1 = operation.addChildElement("langValue");
				element_param1.addTextNode("E");
				/*
				 * SOAPElement element_param2 = operation.addChildElement("emailVal");
				 * element_param2.addTextNode("N");
				 */
				request1.saveChanges();
				String result = null;

				if (request1 != null) {
					ByteArrayOutputStream baos = null;
					try {
						baos = new ByteArrayOutputStream();
						request1.writeTo(baos);
						result = baos.toString();
					} catch (Exception e) {
					} finally {
						if (baos != null) {
							try {
								baos.close();
							} catch (IOException ioe) {
							}
						}
					}
				}
				// logger.info("Ezy Life DOC GEN"+result);
				// resCode=body ;
				SOAPMessage response1 = dispatch.invoke(request1);
				SOAPBody responsebody = response1.getSOAPBody();
				logger.info("regenerate doc IDS " + responsebody);

				/*
				 * java.util.Iterator otcupdates = responsebody.getChildElements(); while
				 * (otcupdates.hasNext()) { SOAPElement otcupdates1 =
				 * (SOAPElement)otcupdates.next();
				 *
				 * java.util.Iterator i = otcupdates1.getChildElements(); while( i.hasNext() ){
				 *
				 * SOAPElement e = (SOAPElement)i.next(); java.util.Iterator m =
				 * e.getChildElements(); while( m.hasNext() ){ SOAPElement e2 =
				 * (SOAPElement)m.next();
				 *
				 * } } }
				 */
				// logger.info( "ezy life document generated result document insert");
			} catch (Exception e) {
				docStatus = "Fail";
				// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
				e.printStackTrace();
			}

		}

		else if (term.indexOf("YT") != -1) { // EZYTL

			boolean recFound = false;
			// String param="Location";
			try {
				// String cpfRootUrlDoc = "http://192.168.0.248:7011/CPF-DSP_TL_BM";

				String cpfRootUrlDoc = "http://cpf:7011/CPF-DSP-EZYSECURE";

				QName servicename = new QName("http://termlife.wcservices.dsp.etiqa.com/",
						"TermLifeDocGenWebServiceImplService");
				QName portname = new QName("http://termlife.wcservices.dsp.etiqa.com/",
						"TermLifeDocGenWebServiceImplPort");
				Service service = Service.create(null, servicename);
				service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
						cpfRootUrlDoc + "/TermLifeDocGenWebServiceImplService?WSDL");
				Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
						Service.Mode.MESSAGE);
				MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

				SOAPMessage request1 = mf.createMessage();
				SOAPPart part = request1.getSOAPPart();

				SOAPEnvelope env = part.getEnvelope();
				SOAPBody body = env.getBody();
				// resp = body;
				SOAPElement operation = body.addChildElement("getStatusOfDocGenOnlyForEzySecure", "ter",
						"http://termlife.wcservices.dsp.etiqa.com/");

				SOAPElement element_param = operation.addChildElement("forAllDocQQID");
				element_param.addTextNode(docId);
				SOAPElement element_param1 = operation.addChildElement("langValue");
				element_param1.addTextNode("E");
				/*
				 * SOAPElement element_param2 = operation.addChildElement("emailVal");
				 * element_param2.addTextNode("N");
				 */
				request1.saveChanges();
				String result = null;

				if (request1 != null) {
					ByteArrayOutputStream baos = null;
					try {
						baos = new ByteArrayOutputStream();
						request1.writeTo(baos);
						result = baos.toString();
					} catch (Exception e) {
					} finally {
						if (baos != null) {
							try {
								baos.close();
							} catch (IOException ioe) {
							}
						}
					}
				}
				// logger.info("Ezy Life DOC GEN"+result);
				// resCode=body ;
				SOAPMessage response1 = dispatch.invoke(request1);
				SOAPBody responsebody = response1.getSOAPBody();
				logger.info("regenerate doc EzySecure " + responsebody);

				/*
				 * java.util.Iterator otcupdates = responsebody.getChildElements(); while
				 * (otcupdates.hasNext()) { SOAPElement otcupdates1 =
				 * (SOAPElement)otcupdates.next();
				 *
				 * java.util.Iterator i = otcupdates1.getChildElements(); while( i.hasNext() ){
				 *
				 * SOAPElement e = (SOAPElement)i.next(); java.util.Iterator m =
				 * e.getChildElements(); while( m.hasNext() ){ SOAPElement e2 =
				 * (SOAPElement)m.next();
				 *
				 * } } }
				 */
				// logger.info( "ezy life document generated result document insert");
			} catch (Exception e) {
				docStatus = "Fail";
				// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
				e.printStackTrace();
			}

		}

		else if (term.indexOf("CT") != -1) { // CT

			boolean recFound = false;
			// String param="Location";
			try {
				// String cpfRootUrlDoc = "http://192.168.0.248:7011/CPF-DSP_TL_BM";

				String cpfRootUrlDoc = "http://10.252.21.60:7011/CPF-DSP-ISECURE3";

				QName servicename = new QName("http://termlife.wcservices.dsp.etiqa.com/",
						"TermLifeDocGenWebServiceImplService");
				QName portname = new QName("http://termlife.wcservices.dsp.etiqa.com/",
						"TermLifeDocGenWebServiceImplPort");
				Service service = Service.create(null, servicename);
				service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
						cpfRootUrlDoc + "/TermLifeDocGenWebServiceImplService?WSDL");
				Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
						Service.Mode.MESSAGE);
				MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

				SOAPMessage request1 = mf.createMessage();
				SOAPPart part = request1.getSOAPPart();

				SOAPEnvelope env = part.getEnvelope();
				SOAPBody body = env.getBody();
				// resp = body;
				SOAPElement operation = body.addChildElement("getStatusOfDocGenOnlyForISecure", "ter",
						"http://termlife.wcservices.dsp.etiqa.com/");

				SOAPElement element_param = operation.addChildElement("forAllDocQQID");
				element_param.addTextNode(docId);
				SOAPElement element_param1 = operation.addChildElement("langValue");
				element_param1.addTextNode("E");
				/*
				 * SOAPElement element_param2 = operation.addChildElement("emailVal");
				 * element_param2.addTextNode("N");
				 */
				request1.saveChanges();
				String result = null;

				if (request1 != null) {
					ByteArrayOutputStream baos = null;
					try {
						baos = new ByteArrayOutputStream();
						request1.writeTo(baos);
						result = baos.toString();
					} catch (Exception e) {
					} finally {
						if (baos != null) {
							try {
								baos.close();
							} catch (IOException ioe) {
							}
						}
					}
				}
				// logger.info("Ezy Life DOC GEN"+result);
				// resCode=body ;
				SOAPMessage response1 = dispatch.invoke(request1);
				SOAPBody responsebody = response1.getSOAPBody();
				logger.info("regenerate doc ISecure " + responsebody);

				/*
				 * java.util.Iterator otcupdates = responsebody.getChildElements(); while
				 * (otcupdates.hasNext()) { SOAPElement otcupdates1 =
				 * (SOAPElement)otcupdates.next();
				 *
				 * java.util.Iterator i = otcupdates1.getChildElements(); while( i.hasNext() ){
				 *
				 * SOAPElement e = (SOAPElement)i.next(); java.util.Iterator m =
				 * e.getChildElements(); while( m.hasNext() ){ SOAPElement e2 =
				 * (SOAPElement)m.next();
				 *
				 * } } }
				 */
				// logger.info( "ezy life document generated result document insert");
			} catch (Exception e) {
				docStatus = "Fail";
				// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
				e.printStackTrace();
			}

		}

		else if (term.indexOf("H") != -1) {

			String dspCpfHOHHEdocsINSURL = null, methodName = "";

			// if(TranDetail.get(0).getProductCode().indexOf("T")!=-1){
			if (policyNo.indexOf("F") != -1) {
				if (simpleNote.equals("SS")) {
					dspCpfHOHHEdocsINSURL = "http://cpf:7011/CPF-DSP-SHOHH-INSURANCE-DOC"; // CPF-DSP-SHOHH-INSURANCE-DOC
					methodName = "getStatusOfDocGenOnlyForSHohh"; // CPF-DSP-HOHH-INS-DOC
				} else {
					dspCpfHOHHEdocsINSURL = "http://cpf:7011/CPF-DSP-HOHH-INS-DOC"; // CPF-DSP-SHOHH-INSURANCE-DOC
					methodName = "getStatusOfDocGenOnlyForHohh"; // CPF-DSP-HOHH-INS-DOC
				}
				logger.info("test1--------------------->");
			} else {
				if (simpleNote.equals("SS")) {
					dspCpfHOHHEdocsINSURL = "http://cpf:7011/CPF-DSP-SHOHH-TAKAFUL-DOC"; // CPF-DSP-SHOHH-TAKAFUL-DOC
					methodName = "getStatusOfDocGenOnlyForSHohhT"; // CPF-DSP-HOHH-TKF-DOC"
				} else {
					dspCpfHOHHEdocsINSURL = "http://cpf:7011/CPF-DSP-HOHH-TKF-DOC"; // CPF-DSP-SHOHH-TAKAFUL-DOC
					methodName = "getStatusOfDocGenOnlyForHohhT"; // CPF-DSP-HOHH-TKF-DOC"
				}
				logger.info("test2--------------------->");
			}
			try {
				logger.info("test2--------------------->"+dspCpfHOHHEdocsINSURL+methodName);
				QName servicename = new QName("http://hohh.wcservices.dsp.etiqa.com/",
						"HOHHDocGenWebServiceImplService");
				QName portname = new QName("http://hohh.wcservices.dsp.etiqa.com/", "HOHHDocGenWebServiceImplPort");
				Service service = Service.create(null, servicename);
				service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
						dspCpfHOHHEdocsINSURL + "/HOHHDocGenWebServiceImplService");
				Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
						Service.Mode.MESSAGE);
				MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
				SOAPMessage request1 = mf.createMessage();
				SOAPPart part = request1.getSOAPPart();
				SOAPEnvelope env = part.getEnvelope();
				SOAPBody body = env.getBody();
				// resp = body;
				SOAPElement operation = body.addChildElement(methodName, "hohh",
						"http://hohh.wcservices.dsp.etiqa.com/");

				SOAPElement element_param = operation.addChildElement("forAllWTCDocQQID");
				element_param.addTextNode(docId);
				SOAPElement element_param2 = operation.addChildElement("emailVal");
				element_param2.addTextNode("N");

				request1.saveChanges();

				// logger.info("Request from cpf " + body); emailVal
				String result = null;

				if (request != null) {
					ByteArrayOutputStream baos = null;
					try {
						baos = new ByteArrayOutputStream();
						request1.writeTo(baos);
						result = baos.toString();
					} catch (Exception e) {
					} finally {
						if (baos != null) {
							try {
								baos.close();
							} catch (IOException ioe) {
							}
						}
					}
				}
				logger.info(result + "document insert");
				// resCode=body ;

				SOAPMessage response1 = dispatch.invoke(request1);
				SOAPBody responsebody = response1.getSOAPBody();
				logger.info("regenerate doc HOHH " + responsebody);
				/*
				 * java.util.Iterator otcupdates = responsebody.getChildElements(); while
				 * (otcupdates.hasNext()) { SOAPElement otcupdates1 =
				 * (SOAPElement)otcupdates.next();
				 *
				 * java.util.Iterator i = otcupdates1.getChildElements(); while( i.hasNext() ){
				 *
				 * SOAPElement e = (SOAPElement)i.next(); java.util.Iterator m =
				 * e.getChildElements(); while( m.hasNext() ){ SOAPElement e2 =
				 * (SOAPElement)m.next();
				 *
				 * } } }
				 */

			} catch (Exception e) {
				docStatus = "Fail";
				// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
				e.printStackTrace();
				logger.info(e);
			}

		} else if (term.indexOf("Buddy") != -1) {

			try {

				String lang = "lan_en";

				URL url = new URL("http://cpf:7011/buddydoc/BuddyDocGenWebServiceImplService?WSDL");
				QName servicename = new QName(URL_BUDDY_DOCGEN, "BuddyDocGenWebServiceImplService");
				QName portname = new QName(URL_BUDDY_DOCGEN, "BuddyDocGenWebServiceImplServicePort");

				Service service = Service.create(null, servicename);
				service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
						"http://cpf:7011/buddydoc/BuddyDocGenWebServiceImplService");

				Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
						Service.Mode.MESSAGE);
				MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

				SOAPMessage req = mf.createMessage();
				SOAPPart part = req.getSOAPPart();
				SOAPEnvelope env = part.getEnvelope();
				SOAPBody body = env.getBody();

				SOAPElement operation = body.addChildElement("getStatus", "getStatusResponse", URL_BUDDY_DOCGEN);
				SOAPElement elementParam = operation.addChildElement("forAllDocQQID");
				elementParam.addTextNode(docId);

				SOAPElement elementParam1 = operation.addChildElement("langValue");
				elementParam1.addTextNode(lang);

				SOAPElement elementParam2 = operation.addChildElement("company");
				elementParam2.addTextNode("T");

				SOAPElement elementParam3 = operation.addChildElement("transactiontype");
				elementParam3.addTextNode("normalcustomer");

				SOAPElement elementParam4 = operation.addChildElement("emailVal");
				elementParam4.addTextNode("N");

				SOAPElement elementParam5 = operation.addChildElement("isAdmin");
				elementParam5.addTextNode("true");

				req.saveChanges();

				String result = null;
				if (req != null) {
					ByteArrayOutputStream baos = null;

					try {
						baos = new ByteArrayOutputStream();
						req.writeTo(baos);
						result = baos.toString();

					} catch (Exception e) {
						logger.info("Docgen exception=" + e.toString());

					} finally {

						if (baos != null) {

							try {
								baos.close();

							} catch (IOException ioe) {
							}
						}
					}
				}

				SOAPMessage res = dispatch.invoke(req);
				SOAPBody responsebody = res.getSOAPBody();

			} catch (Exception e) {
				emailStatus = "Fail";
				e.printStackTrace();
				logger.info(e);
			}
		} else if (term.indexOf("TravelEzy") != -1) {

			try {

				String lang = "lan_en";

				URL url = new URL("http://cpf:7011/CPF-DSP-TravelEzy-Doc/TravelEzyDocGenWebServiceImplService?WSDL");
				QName servicename = new QName(URL_DOCGEN, "TravelEzyDocGenWebServiceImplService");
				QName portname = new QName(URL_DOCGEN, "TravelEzyDocGenWebServiceImplServicePort");

				Service service = Service.create(null, servicename);
				service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
						"http://cpf:7011/CPF-DSP-TravelEzy-Doc/TravelEzyDocGenWebServiceImplService");

				Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
						Service.Mode.MESSAGE);
				MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

				SOAPMessage req = mf.createMessage();
				SOAPPart part = req.getSOAPPart();
				SOAPEnvelope env = part.getEnvelope();
				SOAPBody body = env.getBody();

				SOAPElement operation = body.addChildElement("getQQ4DocStatus", "getQQ4DocStatusResponse", URL_DOCGEN);
				SOAPElement elementParam = operation.addChildElement("forAllDocQQID");
				elementParam.addTextNode(docId);

				SOAPElement elementParam1 = operation.addChildElement("langValue");
				elementParam1.addTextNode(lang);

				SOAPElement elementParam4 = operation.addChildElement("emailVal");
				elementParam4.addTextNode("N");

				SOAPElement elementParam5 = operation.addChildElement("isAdmin");
				elementParam5.addTextNode("true");

				req.saveChanges();

				String result = null;
				if (req != null) {
					ByteArrayOutputStream baos = null;

					try {
						baos = new ByteArrayOutputStream();
						req.writeTo(baos);
						result = baos.toString();

					} catch (Exception e) {
						logger.info("Docgen exception=" + e.toString());

					} finally {

						if (baos != null) {

							try {
								baos.close();

							} catch (IOException ioe) {
							}
						}
					}
				}

				SOAPMessage res = dispatch.invoke(req);
				SOAPBody responsebody = res.getSOAPBody();

			} catch (Exception e) {
				emailStatus = "Fail";
				e.printStackTrace();
				logger.info(e);
			}
		}
		// TripCare
		else if (term.indexOf("TripCare") != -1) {

			try {

				String lang = "lan_en";

				URL url = new URL(
						"http://cpf:7011/CPF-DSP-TripCare360-Doc/TripCare360DocGenWebServiceImplService?WSDL");
				QName servicename = new QName(URL_DOCGEN, "TripCare360DocGenWebServiceImplService");
				QName portname = new QName(URL_DOCGEN, "TripCare360DocGenWebServiceImplServicePort");

				Service service = Service.create(null, servicename);
				service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
						"http://cpf:7011/CPF-DSP-TripCare360-Doc/TripCare360DocGenWebServiceImplService");

				Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
						Service.Mode.MESSAGE);
				MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

				SOAPMessage req = mf.createMessage();
				SOAPPart part = req.getSOAPPart();
				SOAPEnvelope env = part.getEnvelope();
				SOAPBody body = env.getBody();

				SOAPElement operation = body.addChildElement("getQQ4DocStatus", "getQQ4DocStatusResponse", URL_DOCGEN);
				SOAPElement elementParam = operation.addChildElement("forAllDocQQID");
				elementParam.addTextNode(docId);

				SOAPElement elementParam1 = operation.addChildElement("langValue");
				elementParam1.addTextNode(lang);

				SOAPElement elementParam4 = operation.addChildElement("emailVal");
				elementParam4.addTextNode("N");

				SOAPElement elementParam5 = operation.addChildElement("isAdmin");
				elementParam5.addTextNode("true");

				req.saveChanges();

				String result = null;
				if (req != null) {
					ByteArrayOutputStream baos = null;

					try {
						baos = new ByteArrayOutputStream();
						req.writeTo(baos);
						result = baos.toString();

					} catch (Exception e) {
						logger.info("Docgen exception=" + e.toString());

					} finally {

						if (baos != null) {

							try {
								baos.close();

							} catch (IOException ioe) {
							}
						}
					}
				}

				SOAPMessage res = dispatch.invoke(req);
				SOAPBody responsebody = res.getSOAPBody();

			} catch (Exception e) {
				emailStatus = "Fail";
				e.printStackTrace();
				logger.info(e);
			}
		} else if (term.indexOf("W") != -1) {

			boolean recFound = false;
			// String param="Location";
			try {
				// String cpfRootUrlDoc = "http://192.168.0.248:7011/CPF-DSP_wtc_r1";
				String cpfRootUrlDoc = "";
				// URL url = new
				// URL("http://192.168.0.248:7011/CPF-DSP_wtc_r1/WTCDocGenWebServiceImplService?WSDL");
				if (simpleNote.equals("SS")) {
					cpfRootUrlDoc = "http://cpf:7011/CPF-DSP_SWTC_Doc";
				} else {
					cpfRootUrlDoc = "http://cpf:7011/CPF-DSP_WTC_Doc";
				}

				// URL url = new
				// URL("http://10.252.21.60:7011:7011/CPF-DSP_wtc_r1/WTCDocGenWebServiceImplService?WSDL");

				QName servicename = new QName("http://wtc.wcservices.dsp.etiqa.com/", "WTCDocGenWebServiceImplService");
				QName portname = new QName("http://wtc.wcservices.dsp.etiqa.com/",
						"WTCDocGenWebServiceImplServicePort");
				Service service = Service.create(null, servicename);
				service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
						cpfRootUrlDoc + "/WTCDocGenWebServiceImplService?WSDL");
				Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
						Service.Mode.MESSAGE);
				MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

				SOAPMessage request1 = mf.createMessage();
				SOAPPart part = request1.getSOAPPart();

				SOAPEnvelope env = part.getEnvelope();
				SOAPBody body = env.getBody();
				// resp = body;
				SOAPElement operation = body.addChildElement("getWTCStatus", "getWTCStatusResponse",
						"http://wtc.wcservices.dsp.etiqa.com/");

				SOAPElement element_param = operation.addChildElement("forAllWTCDocQQID");
				element_param.addTextNode(docId);
				SOAPElement element_param1 = operation.addChildElement("langValue");
				element_param1.addTextNode("lan_en");
				SOAPElement element_param2 = operation.addChildElement("emailVal");
				element_param2.addTextNode("N");

				request1.saveChanges();
				String result = null;

				if (request1 != null) {
					ByteArrayOutputStream baos = null;
					try {
						baos = new ByteArrayOutputStream();
						request1.writeTo(baos);
						result = baos.toString();
					} catch (Exception e) {
					} finally {
						if (baos != null) {
							try {
								baos.close();
							} catch (IOException ioe) {
							}
						}
					}
				}
				// logger.info("WTC DOC GEN"+result);
				// resCode=body ;
				SOAPMessage response1 = dispatch.invoke(request1);
				SOAPBody responsebody = response1.getSOAPBody();
				logger.info("regenerate doc WTC " + responsebody);
				/*
				 * java.util.Iterator otcupdates = responsebody.getChildElements(); while
				 * (otcupdates.hasNext()) { SOAPElement otcupdates1 =
				 * (SOAPElement)otcupdates.next();
				 *
				 * java.util.Iterator i = otcupdates1.getChildElements(); while( i.hasNext() ){
				 *
				 * SOAPElement e = (SOAPElement)i.next(); java.util.Iterator m =
				 * e.getChildElements(); while( m.hasNext() ){ SOAPElement e2 =
				 * (SOAPElement)m.next();
				 *
				 * } } }
				 */
				// logger.info( "WTC document generated result document insert");
			} catch (Exception e) {
				docStatus = "Fail";
				// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
				e.printStackTrace();
			}

		}
		// logger.info(sendEmailStatus);
		ReportDAO dao1 = new ReportDAOImpl();
		List<TransactionalReport> TranDetail = dao1.getTransactionDetail(id);
		request.setAttribute("data", TranDetail);
		request.setAttribute("statusDoc", docStatus);
		forward(request, response, redirect);

	}

	/* Document Generation Code Ends Here */

}
