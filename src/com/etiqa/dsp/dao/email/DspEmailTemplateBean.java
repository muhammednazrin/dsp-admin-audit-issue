package com.etiqa.dsp.dao.email;

public class DspEmailTemplateBean {

	private String emailTo;
	private String templateName;
	private int templateCode;
	private String templateSubject;

	private String templateBody;
	private String error_code;
	private String error_msg;

	private String failmsg;
	private String trx_id;
	private int valHeader;

	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public int getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(int templateCode) {
		this.templateCode = templateCode;
	}

	public String getError_code() {
		return error_code;
	}

	public void setError_code(String error_code) {
		this.error_code = error_code;
	}

	public String getError_msg() {
		return error_msg;
	}

	public void setError_msg(String error_msg) {
		this.error_msg = error_msg;
	}

	public String getFailmsg() {
		return failmsg;
	}

	public void setFailmsg(String failmsg) {
		this.failmsg = failmsg;
	}

	public String getTrx_id() {
		return trx_id;
	}

	public void setTrx_id(String trx_id) {
		this.trx_id = trx_id;
	}

	public int getValHeader() {
		return valHeader;
	}

	public void setValHeader(int valHeader) {
		this.valHeader = valHeader;
	}

	public String getTemplateSubject() {
		return templateSubject;
	}

	public void setTemplateSubject(String templateSubject) {
		this.templateSubject = templateSubject;
	}

	public String getTemplateBody() {
		return templateBody;
	}

	public void setTemplateBody(String templateBody) {
		this.templateBody = templateBody;
	}

}
