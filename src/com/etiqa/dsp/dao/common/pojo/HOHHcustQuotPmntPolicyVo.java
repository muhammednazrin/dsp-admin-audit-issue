package com.etiqa.dsp.dao.common.pojo;

import java.util.List;

public class HOHHcustQuotPmntPolicyVo {

	private int CUSTOMER_ID;
	private String CUSTOMER_ID_TYPE;
	private String CUSTOMER_NRIC_ID;
	private String CUSTOMER_NAME;
	private String CUSTOMER_DOB;
	private String CUSTOMER_GENDER;
	private String CUSTOMER_RACE;
	private String CUSTOMER_RELIGION;
	private String CUSTOMER_NATIONALITY;
	private String CUSTOMER_TITLE;
	private String CUSTOMER_NATIONALITY_RACE;
	private String CUSTOMER_EDU_LEVEL;
	private String CUSTOMER_MARITALSTATUS;
	private String CUSTOMER_SALARY_RANGE;
	private String CUSTOMER_OCCUPATION;
	private String CUSTOMER_ADDRESS1;
	private String CUSTOMER_ADDRESS2;
	private String CUSTOMER_ADDRESS3;
	private String CUSTOMER_POSTCODE;
	private String CUSTOMER_STATE;
	private String CUSTOMER_COUNTRY;
	private String CUSTOMER_MAIL_ADDRESS1;
	private String CUSTOMER_MAIL_ADDRESS2;
	private String CUSTOMER_MAIL_ADDRESS3;
	private String CUSTOMER_MAIL_POSTCODE;
	private String CUSTOMER_MAIL_STATE;
	private String CUSTOMER_MAIL_COUNTRY;
	private int CUSTOMER_NO_CHILDREN;
	private String CUSTOMER_MOBILE_NO;
	private String CUSTOMER_EMAIL;
	private String LEADS_FLAG;
	private String SALES_TOOL;
	private String EMAIL_SENT_COUNTER;
	private int QQ_ID;
	private String CREATE_DATE;
	private String UPDATED_DATE;
	private String CUSTOMER_EMPLOYER;
	private String CUSTOMER_CLIENTTYPE;
	private String CUSTOMER_INDUSTRY;
	private String DSP_QQ_ID;
	private String PRODUCT_CODE;
	private String CREATED_DATE;
	private int ID;
	private String POLICY_NUMBER;
	private String PRODUCT_PLAN_CODE;
	private String POLICY_EFFECTIVE_TIMESTAMP;
	private int SUM_INSURED;
	private String POLICY_STATUS;
	private String POLICY_TYPE_CODE;
	private String POLICY_EXPIRY_TIMESTAMP;
	private int TRANSACTION_ID;
	private String CREATION_TIMESTAMP;

	// ---------------------payment columns--------------------

	// private String TRANSACTION_ID;
	// private String DSP_QQ_ID;

	private String PMNT_GATEWAY_CODE;
	private String DISCOUNT_CODE;
	private String PROMO_CODE;
	private String AMOUNT;
	private String PMNT_STATUS;
	private String INVOICE_NO;
	private String TRANSACTION_DATETIME;
	private String TRANSACTION_STATUS;

	// ---------------hohh QQ columns------------
	private String HOHH_QQ_ID;
	// private String DSP_QQ_ID;
	private String LEADS_EMAIL_ID;
	// private String PRODUCT_CODE;
	private String SUB_PRODUCT_ID;
	private String HOME_COVERAGE;
	private String HOME_TYPE;
	private String BUILDING_CONSTRUCTION_TYPE;
	private String NUMBER_OF_STOREYS;
	private String HOME_SUM_INSURED;
	private String CONTENT_SUM_INSURED;
	private String PROVIDED_ETIQA_OFFER;
	private String PROMOTION_CODE;
	private String PROMOTION_AMOUNT;
	private String DISCOUNT_ID;
	private String DISCOUNT_AMOUNT;
	private String HOHH_POLICY_HOME_SUM_INS;
	private String HOHH_SUM_INSURED_PREMIUM;
	private String HOHH_ADD_BENIFITS_PREMIUM;
	private String HOHH_POLICY_PREMIUM;
	private String GROSS_PREMIUM_GST;
	private String GROSS_PREMIUM_STAMP_DUTY;
	private String HOHH_TOTAL_PREMIUM_PAID;
	private String PROPERTY_INSURED_ADDRESS;
	private String PROPERTY_INSURED_POSTCODE;
	private String PROPERTY_INSURED_STATE;
	private String PROPERTY_INSURED_COUNTRY;
	private String UNDER_WRITING_QUESTION_1;
	private String ETIQA_PDPA_ACCEPT;
	private String IMP_NOTICE_DECLERATION_PDA;
	private String PAYMENT_METHOD;
	private String ET_WC_DSP_CUSTOMER_CUSTOMER_ID;
	private String HOHH_GROSS_PREMIUM;
	private String IS_PURCHASED;
	private String FULL_NAME;
	private String HOHH_LANGUAGE;

	private String URQ1;
	private String URQ2;
	private String URQ3;
	private String URQ4;
	private String URQ5;
	private String URQ6;

	private String CREATE_DATE_QQ;
	private String ADD_BEN_EXTENDED_THEFT;
	private String ADD_BEN_RIOT_STRIKE;
	private String ADD_BEN_RIOT_STRIKE_AMT;
	private String ADD_BEN_EXTENDED_THEFT_AMT;

	public String getURQ1() {
		return URQ1;
	}

	public void setURQ1(String uRQ1) {
		URQ1 = uRQ1;
	}

	public String getURQ2() {
		return URQ2;
	}

	public void setURQ2(String uRQ2) {
		URQ2 = uRQ2;
	}

	public String getURQ3() {
		return URQ3;
	}

	public void setURQ3(String uRQ3) {
		URQ3 = uRQ3;
	}

	public String getURQ4() {
		return URQ4;
	}

	public void setURQ4(String uRQ4) {
		URQ4 = uRQ4;
	}

	public String getURQ5() {
		return URQ5;
	}

	public void setURQ5(String uRQ5) {
		URQ5 = uRQ5;
	}

	public String getURQ6() {
		return URQ6;
	}

	public void setURQ6(String uRQ6) {
		URQ6 = uRQ6;
	}

	public String getCREATE_DATE_QQ() {
		return CREATE_DATE_QQ;
	}

	public void setCREATE_DATE_QQ(String cREATE_DATE_QQ) {
		CREATE_DATE_QQ = cREATE_DATE_QQ;
	}

	public String getADD_BEN_EXTENDED_THEFT() {
		return ADD_BEN_EXTENDED_THEFT;
	}

	public void setADD_BEN_EXTENDED_THEFT(String aDD_BEN_EXTENDED_THEFT) {
		ADD_BEN_EXTENDED_THEFT = aDD_BEN_EXTENDED_THEFT;
	}

	public String getADD_BEN_RIOT_STRIKE() {
		return ADD_BEN_RIOT_STRIKE;
	}

	public void setADD_BEN_RIOT_STRIKE(String aDD_BEN_RIOT_STRIKE) {
		ADD_BEN_RIOT_STRIKE = aDD_BEN_RIOT_STRIKE;
	}

	public String getADD_BEN_RIOT_STRIKE_AMT() {
		return ADD_BEN_RIOT_STRIKE_AMT;
	}

	public void setADD_BEN_RIOT_STRIKE_AMT(String aDD_BEN_RIOT_STRIKE_AMT) {
		ADD_BEN_RIOT_STRIKE_AMT = aDD_BEN_RIOT_STRIKE_AMT;
	}

	public String getADD_BEN_EXTENDED_THEFT_AMT() {
		return ADD_BEN_EXTENDED_THEFT_AMT;
	}

	public void setADD_BEN_EXTENDED_THEFT_AMT(String aDD_BEN_EXTENDED_THEFT_AMT) {
		ADD_BEN_EXTENDED_THEFT_AMT = aDD_BEN_EXTENDED_THEFT_AMT;
	}

	public int getCUSTOMER_ID() {
		return CUSTOMER_ID;
	}

	public void setCUSTOMER_ID(int cUSTOMER_ID) {
		CUSTOMER_ID = cUSTOMER_ID;
	}

	public String getCUSTOMER_ID_TYPE() {
		return CUSTOMER_ID_TYPE;
	}

	public void setCUSTOMER_ID_TYPE(String cUSTOMER_ID_TYPE) {
		CUSTOMER_ID_TYPE = cUSTOMER_ID_TYPE;
	}

	public String getCUSTOMER_NRIC_ID() {
		return CUSTOMER_NRIC_ID;
	}

	public void setCUSTOMER_NRIC_ID(String cUSTOMER_NRIC_ID) {
		CUSTOMER_NRIC_ID = cUSTOMER_NRIC_ID;
	}

	public String getCUSTOMER_NAME() {
		return CUSTOMER_NAME;
	}

	public void setCUSTOMER_NAME(String cUSTOMER_NAME) {
		CUSTOMER_NAME = cUSTOMER_NAME;
	}

	public String getCUSTOMER_DOB() {
		return CUSTOMER_DOB;
	}

	public void setCUSTOMER_DOB(String cUSTOMER_DOB) {
		CUSTOMER_DOB = cUSTOMER_DOB;
	}

	public String getCUSTOMER_GENDER() {
		return CUSTOMER_GENDER;
	}

	public void setCUSTOMER_GENDER(String cUSTOMER_GENDER) {
		CUSTOMER_GENDER = cUSTOMER_GENDER;
	}

	public String getCUSTOMER_RACE() {
		return CUSTOMER_RACE;
	}

	public void setCUSTOMER_RACE(String cUSTOMER_RACE) {
		CUSTOMER_RACE = cUSTOMER_RACE;
	}

	public String getCUSTOMER_RELIGION() {
		return CUSTOMER_RELIGION;
	}

	public void setCUSTOMER_RELIGION(String cUSTOMER_RELIGION) {
		CUSTOMER_RELIGION = cUSTOMER_RELIGION;
	}

	public String getCUSTOMER_NATIONALITY() {
		return CUSTOMER_NATIONALITY;
	}

	public void setCUSTOMER_NATIONALITY(String cUSTOMER_NATIONALITY) {
		CUSTOMER_NATIONALITY = cUSTOMER_NATIONALITY;
	}

	public String getCUSTOMER_TITLE() {
		return CUSTOMER_TITLE;
	}

	public void setCUSTOMER_TITLE(String cUSTOMER_TITLE) {
		CUSTOMER_TITLE = cUSTOMER_TITLE;
	}

	public String getCUSTOMER_NATIONALITY_RACE() {
		return CUSTOMER_NATIONALITY_RACE;
	}

	public void setCUSTOMER_NATIONALITY_RACE(String cUSTOMER_NATIONALITY_RACE) {
		CUSTOMER_NATIONALITY_RACE = cUSTOMER_NATIONALITY_RACE;
	}

	public String getCUSTOMER_EDU_LEVEL() {
		return CUSTOMER_EDU_LEVEL;
	}

	public void setCUSTOMER_EDU_LEVEL(String cUSTOMER_EDU_LEVEL) {
		CUSTOMER_EDU_LEVEL = cUSTOMER_EDU_LEVEL;
	}

	public String getCUSTOMER_MARITALSTATUS() {
		return CUSTOMER_MARITALSTATUS;
	}

	public void setCUSTOMER_MARITALSTATUS(String cUSTOMER_MARITALSTATUS) {
		CUSTOMER_MARITALSTATUS = cUSTOMER_MARITALSTATUS;
	}

	public String getCUSTOMER_SALARY_RANGE() {
		return CUSTOMER_SALARY_RANGE;
	}

	public void setCUSTOMER_SALARY_RANGE(String cUSTOMER_SALARY_RANGE) {
		CUSTOMER_SALARY_RANGE = cUSTOMER_SALARY_RANGE;
	}

	public String getCUSTOMER_OCCUPATION() {
		return CUSTOMER_OCCUPATION;
	}

	public void setCUSTOMER_OCCUPATION(String cUSTOMER_OCCUPATION) {
		CUSTOMER_OCCUPATION = cUSTOMER_OCCUPATION;
	}

	public String getCUSTOMER_ADDRESS1() {
		return CUSTOMER_ADDRESS1;
	}

	public void setCUSTOMER_ADDRESS1(String cUSTOMER_ADDRESS1) {
		CUSTOMER_ADDRESS1 = cUSTOMER_ADDRESS1;
	}

	public String getCUSTOMER_ADDRESS2() {
		return CUSTOMER_ADDRESS2;
	}

	public void setCUSTOMER_ADDRESS2(String cUSTOMER_ADDRESS2) {
		CUSTOMER_ADDRESS2 = cUSTOMER_ADDRESS2;
	}

	public String getCUSTOMER_ADDRESS3() {
		return CUSTOMER_ADDRESS3;
	}

	public void setCUSTOMER_ADDRESS3(String cUSTOMER_ADDRESS3) {
		CUSTOMER_ADDRESS3 = cUSTOMER_ADDRESS3;
	}

	public String getCUSTOMER_POSTCODE() {
		return CUSTOMER_POSTCODE;
	}

	public void setCUSTOMER_POSTCODE(String cUSTOMER_POSTCODE) {
		CUSTOMER_POSTCODE = cUSTOMER_POSTCODE;
	}

	public String getCUSTOMER_STATE() {
		return CUSTOMER_STATE;
	}

	public void setCUSTOMER_STATE(String cUSTOMER_STATE) {
		CUSTOMER_STATE = cUSTOMER_STATE;
	}

	public String getCUSTOMER_COUNTRY() {
		return CUSTOMER_COUNTRY;
	}

	public void setCUSTOMER_COUNTRY(String cUSTOMER_COUNTRY) {
		CUSTOMER_COUNTRY = cUSTOMER_COUNTRY;
	}

	public String getCUSTOMER_MAIL_ADDRESS1() {
		return CUSTOMER_MAIL_ADDRESS1;
	}

	public void setCUSTOMER_MAIL_ADDRESS1(String cUSTOMER_MAIL_ADDRESS1) {
		CUSTOMER_MAIL_ADDRESS1 = cUSTOMER_MAIL_ADDRESS1;
	}

	public String getCUSTOMER_MAIL_ADDRESS2() {
		return CUSTOMER_MAIL_ADDRESS2;
	}

	public void setCUSTOMER_MAIL_ADDRESS2(String cUSTOMER_MAIL_ADDRESS2) {
		CUSTOMER_MAIL_ADDRESS2 = cUSTOMER_MAIL_ADDRESS2;
	}

	public String getCUSTOMER_MAIL_ADDRESS3() {
		return CUSTOMER_MAIL_ADDRESS3;
	}

	public void setCUSTOMER_MAIL_ADDRESS3(String cUSTOMER_MAIL_ADDRESS3) {
		CUSTOMER_MAIL_ADDRESS3 = cUSTOMER_MAIL_ADDRESS3;
	}

	public String getCUSTOMER_MAIL_POSTCODE() {
		return CUSTOMER_MAIL_POSTCODE;
	}

	public void setCUSTOMER_MAIL_POSTCODE(String cUSTOMER_MAIL_POSTCODE) {
		CUSTOMER_MAIL_POSTCODE = cUSTOMER_MAIL_POSTCODE;
	}

	public String getCUSTOMER_MAIL_STATE() {
		return CUSTOMER_MAIL_STATE;
	}

	public void setCUSTOMER_MAIL_STATE(String cUSTOMER_MAIL_STATE) {
		CUSTOMER_MAIL_STATE = cUSTOMER_MAIL_STATE;
	}

	public String getCUSTOMER_MAIL_COUNTRY() {
		return CUSTOMER_MAIL_COUNTRY;
	}

	public void setCUSTOMER_MAIL_COUNTRY(String cUSTOMER_MAIL_COUNTRY) {
		CUSTOMER_MAIL_COUNTRY = cUSTOMER_MAIL_COUNTRY;
	}

	public int getCUSTOMER_NO_CHILDREN() {
		return CUSTOMER_NO_CHILDREN;
	}

	public void setCUSTOMER_NO_CHILDREN(int cUSTOMER_NO_CHILDREN) {
		CUSTOMER_NO_CHILDREN = cUSTOMER_NO_CHILDREN;
	}

	public String getCUSTOMER_MOBILE_NO() {
		return CUSTOMER_MOBILE_NO;
	}

	public void setCUSTOMER_MOBILE_NO(String cUSTOMER_MOBILE_NO) {
		CUSTOMER_MOBILE_NO = cUSTOMER_MOBILE_NO;
	}

	public String getCUSTOMER_EMAIL() {
		return CUSTOMER_EMAIL;
	}

	public void setCUSTOMER_EMAIL(String cUSTOMER_EMAIL) {
		CUSTOMER_EMAIL = cUSTOMER_EMAIL;
	}

	public String getLEADS_FLAG() {
		return LEADS_FLAG;
	}

	public void setLEADS_FLAG(String lEADS_FLAG) {
		LEADS_FLAG = lEADS_FLAG;
	}

	public String getSALES_TOOL() {
		return SALES_TOOL;
	}

	public void setSALES_TOOL(String sALES_TOOL) {
		SALES_TOOL = sALES_TOOL;
	}

	public String getEMAIL_SENT_COUNTER() {
		return EMAIL_SENT_COUNTER;
	}

	public void setEMAIL_SENT_COUNTER(String eMAIL_SENT_COUNTER) {
		EMAIL_SENT_COUNTER = eMAIL_SENT_COUNTER;
	}

	public int getQQ_ID() {
		return QQ_ID;
	}

	public void setQQ_ID(int qQ_ID) {
		QQ_ID = qQ_ID;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getUPDATED_DATE() {
		return UPDATED_DATE;
	}

	public void setUPDATED_DATE(String uPDATED_DATE) {
		UPDATED_DATE = uPDATED_DATE;
	}

	public String getCUSTOMER_EMPLOYER() {
		return CUSTOMER_EMPLOYER;
	}

	public void setCUSTOMER_EMPLOYER(String cUSTOMER_EMPLOYER) {
		CUSTOMER_EMPLOYER = cUSTOMER_EMPLOYER;
	}

	public String getCUSTOMER_CLIENTTYPE() {
		return CUSTOMER_CLIENTTYPE;
	}

	public void setCUSTOMER_CLIENTTYPE(String cUSTOMER_CLIENTTYPE) {
		CUSTOMER_CLIENTTYPE = cUSTOMER_CLIENTTYPE;
	}

	public String getCUSTOMER_INDUSTRY() {
		return CUSTOMER_INDUSTRY;
	}

	public void setCUSTOMER_INDUSTRY(String cUSTOMER_INDUSTRY) {
		CUSTOMER_INDUSTRY = cUSTOMER_INDUSTRY;
	}

	public String getDSP_QQ_ID() {
		return DSP_QQ_ID;
	}

	public void setDSP_QQ_ID(String dSP_QQ_ID) {
		DSP_QQ_ID = dSP_QQ_ID;
	}

	public String getPRODUCT_CODE() {
		return PRODUCT_CODE;
	}

	public void setPRODUCT_CODE(String pRODUCT_CODE) {
		PRODUCT_CODE = pRODUCT_CODE;
	}

	public String getCREATED_DATE() {
		return CREATED_DATE;
	}

	public void setCREATED_DATE(String cREATED_DATE) {
		CREATED_DATE = cREATED_DATE;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getPOLICY_NUMBER() {
		return POLICY_NUMBER;
	}

	public void setPOLICY_NUMBER(String pOLICY_NUMBER) {
		POLICY_NUMBER = pOLICY_NUMBER;
	}

	public String getPRODUCT_PLAN_CODE() {
		return PRODUCT_PLAN_CODE;
	}

	public void setPRODUCT_PLAN_CODE(String pRODUCT_PLAN_CODE) {
		PRODUCT_PLAN_CODE = pRODUCT_PLAN_CODE;
	}

	public String getPOLICY_EFFECTIVE_TIMESTAMP() {
		return POLICY_EFFECTIVE_TIMESTAMP;
	}

	public void setPOLICY_EFFECTIVE_TIMESTAMP(String pOLICY_EFFECTIVE_TIMESTAMP) {
		POLICY_EFFECTIVE_TIMESTAMP = pOLICY_EFFECTIVE_TIMESTAMP;
	}

	public int getSUM_INSURED() {
		return SUM_INSURED;
	}

	public void setSUM_INSURED(int sUM_INSURED) {
		SUM_INSURED = sUM_INSURED;
	}

	public String getPOLICY_STATUS() {
		return POLICY_STATUS;
	}

	public void setPOLICY_STATUS(String pOLICY_STATUS) {
		POLICY_STATUS = pOLICY_STATUS;
	}

	public String getPOLICY_TYPE_CODE() {
		return POLICY_TYPE_CODE;
	}

	public void setPOLICY_TYPE_CODE(String pOLICY_TYPE_CODE) {
		POLICY_TYPE_CODE = pOLICY_TYPE_CODE;
	}

	public String getPOLICY_EXPIRY_TIMESTAMP() {
		return POLICY_EXPIRY_TIMESTAMP;
	}

	public void setPOLICY_EXPIRY_TIMESTAMP(String pOLICY_EXPIRY_TIMESTAMP) {
		POLICY_EXPIRY_TIMESTAMP = pOLICY_EXPIRY_TIMESTAMP;
	}

	public int getTRANSACTION_ID() {
		return TRANSACTION_ID;
	}

	public void setTRANSACTION_ID(int tRANSACTION_ID) {
		TRANSACTION_ID = tRANSACTION_ID;
	}

	public String getCREATION_TIMESTAMP() {
		return CREATION_TIMESTAMP;
	}

	public void setCREATION_TIMESTAMP(String cREATION_TIMESTAMP) {
		CREATION_TIMESTAMP = cREATION_TIMESTAMP;
	}

	public String getPMNT_GATEWAY_CODE() {
		return PMNT_GATEWAY_CODE;
	}

	public void setPMNT_GATEWAY_CODE(String pMNT_GATEWAY_CODE) {
		PMNT_GATEWAY_CODE = pMNT_GATEWAY_CODE;
	}

	public String getDISCOUNT_CODE() {
		return DISCOUNT_CODE;
	}

	public void setDISCOUNT_CODE(String dISCOUNT_CODE) {
		DISCOUNT_CODE = dISCOUNT_CODE;
	}

	public String getPROMO_CODE() {
		return PROMO_CODE;
	}

	public void setPROMO_CODE(String pROMO_CODE) {
		PROMO_CODE = pROMO_CODE;
	}

	public String getAMOUNT() {
		return AMOUNT;
	}

	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}

	public String getPMNT_STATUS() {
		return PMNT_STATUS;
	}

	public void setPMNT_STATUS(String pMNT_STATUS) {
		PMNT_STATUS = pMNT_STATUS;
	}

	public String getINVOICE_NO() {
		return INVOICE_NO;
	}

	public void setINVOICE_NO(String iNVOICE_NO) {
		INVOICE_NO = iNVOICE_NO;
	}

	public String getTRANSACTION_DATETIME() {
		return TRANSACTION_DATETIME;
	}

	public void setTRANSACTION_DATETIME(String tRANSACTION_DATETIME) {
		TRANSACTION_DATETIME = tRANSACTION_DATETIME;
	}

	public String getTRANSACTION_STATUS() {
		return TRANSACTION_STATUS;
	}

	public void setTRANSACTION_STATUS(String tRANSACTION_STATUS) {
		TRANSACTION_STATUS = tRANSACTION_STATUS;
	}

	public String getHOHH_QQ_ID() {
		return HOHH_QQ_ID;
	}

	public void setHOHH_QQ_ID(String hOHH_QQ_ID) {
		HOHH_QQ_ID = hOHH_QQ_ID;
	}

	public String getLEADS_EMAIL_ID() {
		return LEADS_EMAIL_ID;
	}

	public void setLEADS_EMAIL_ID(String lEADS_EMAIL_ID) {
		LEADS_EMAIL_ID = lEADS_EMAIL_ID;
	}

	public String getSUB_PRODUCT_ID() {
		return SUB_PRODUCT_ID;
	}

	public void setSUB_PRODUCT_ID(String sUB_PRODUCT_ID) {
		SUB_PRODUCT_ID = sUB_PRODUCT_ID;
	}

	public String getHOME_COVERAGE() {
		return HOME_COVERAGE;
	}

	public void setHOME_COVERAGE(String hOME_COVERAGE) {
		HOME_COVERAGE = hOME_COVERAGE;
	}

	public String getHOME_TYPE() {
		return HOME_TYPE;
	}

	public void setHOME_TYPE(String hOME_TYPE) {
		HOME_TYPE = hOME_TYPE;
	}

	public String getBUILDING_CONSTRUCTION_TYPE() {
		return BUILDING_CONSTRUCTION_TYPE;
	}

	public void setBUILDING_CONSTRUCTION_TYPE(String bUILDING_CONSTRUCTION_TYPE) {
		BUILDING_CONSTRUCTION_TYPE = bUILDING_CONSTRUCTION_TYPE;
	}

	public String getNUMBER_OF_STOREYS() {
		return NUMBER_OF_STOREYS;
	}

	public void setNUMBER_OF_STOREYS(String nUMBER_OF_STOREYS) {
		NUMBER_OF_STOREYS = nUMBER_OF_STOREYS;
	}

	public String getHOME_SUM_INSURED() {
		return HOME_SUM_INSURED;
	}

	public void setHOME_SUM_INSURED(String hOME_SUM_INSURED) {
		HOME_SUM_INSURED = hOME_SUM_INSURED;
	}

	public String getCONTENT_SUM_INSURED() {
		return CONTENT_SUM_INSURED;
	}

	public void setCONTENT_SUM_INSURED(String cONTENT_SUM_INSURED) {
		CONTENT_SUM_INSURED = cONTENT_SUM_INSURED;
	}

	public String getPROVIDED_ETIQA_OFFER() {
		return PROVIDED_ETIQA_OFFER;
	}

	public void setPROVIDED_ETIQA_OFFER(String pROVIDED_ETIQA_OFFER) {
		PROVIDED_ETIQA_OFFER = pROVIDED_ETIQA_OFFER;
	}

	public String getPROMOTION_CODE() {
		return PROMOTION_CODE;
	}

	public void setPROMOTION_CODE(String pROMOTION_CODE) {
		PROMOTION_CODE = pROMOTION_CODE;
	}

	public String getPROMOTION_AMOUNT() {
		return PROMOTION_AMOUNT;
	}

	public void setPROMOTION_AMOUNT(String pROMOTION_AMOUNT) {
		PROMOTION_AMOUNT = pROMOTION_AMOUNT;
	}

	public String getDISCOUNT_ID() {
		return DISCOUNT_ID;
	}

	public void setDISCOUNT_ID(String dISCOUNT_ID) {
		DISCOUNT_ID = dISCOUNT_ID;
	}

	public String getDISCOUNT_AMOUNT() {
		return DISCOUNT_AMOUNT;
	}

	public void setDISCOUNT_AMOUNT(String dISCOUNT_AMOUNT) {
		DISCOUNT_AMOUNT = dISCOUNT_AMOUNT;
	}

	public String getHOHH_POLICY_HOME_SUM_INS() {
		return HOHH_POLICY_HOME_SUM_INS;
	}

	public void setHOHH_POLICY_HOME_SUM_INS(String hOHH_POLICY_HOME_SUM_INS) {
		HOHH_POLICY_HOME_SUM_INS = hOHH_POLICY_HOME_SUM_INS;
	}

	public String getHOHH_SUM_INSURED_PREMIUM() {
		return HOHH_SUM_INSURED_PREMIUM;
	}

	public void setHOHH_SUM_INSURED_PREMIUM(String hOHH_SUM_INSURED_PREMIUM) {
		HOHH_SUM_INSURED_PREMIUM = hOHH_SUM_INSURED_PREMIUM;
	}

	public String getHOHH_ADD_BENIFITS_PREMIUM() {
		return HOHH_ADD_BENIFITS_PREMIUM;
	}

	public void setHOHH_ADD_BENIFITS_PREMIUM(String hOHH_ADD_BENIFITS_PREMIUM) {
		HOHH_ADD_BENIFITS_PREMIUM = hOHH_ADD_BENIFITS_PREMIUM;
	}

	public String getHOHH_POLICY_PREMIUM() {
		return HOHH_POLICY_PREMIUM;
	}

	public void setHOHH_POLICY_PREMIUM(String hOHH_POLICY_PREMIUM) {
		HOHH_POLICY_PREMIUM = hOHH_POLICY_PREMIUM;
	}

	public String getGROSS_PREMIUM_GST() {
		return GROSS_PREMIUM_GST;
	}

	public void setGROSS_PREMIUM_GST(String gROSS_PREMIUM_GST) {
		GROSS_PREMIUM_GST = gROSS_PREMIUM_GST;
	}

	public String getGROSS_PREMIUM_STAMP_DUTY() {
		return GROSS_PREMIUM_STAMP_DUTY;
	}

	public void setGROSS_PREMIUM_STAMP_DUTY(String gROSS_PREMIUM_STAMP_DUTY) {
		GROSS_PREMIUM_STAMP_DUTY = gROSS_PREMIUM_STAMP_DUTY;
	}

	public String getHOHH_TOTAL_PREMIUM_PAID() {
		return HOHH_TOTAL_PREMIUM_PAID;
	}

	public void setHOHH_TOTAL_PREMIUM_PAID(String hOHH_TOTAL_PREMIUM_PAID) {
		HOHH_TOTAL_PREMIUM_PAID = hOHH_TOTAL_PREMIUM_PAID;
	}

	public String getPROPERTY_INSURED_ADDRESS() {
		return PROPERTY_INSURED_ADDRESS;
	}

	public void setPROPERTY_INSURED_ADDRESS(String pROPERTY_INSURED_ADDRESS) {
		PROPERTY_INSURED_ADDRESS = pROPERTY_INSURED_ADDRESS;
	}

	public String getPROPERTY_INSURED_POSTCODE() {
		return PROPERTY_INSURED_POSTCODE;
	}

	public void setPROPERTY_INSURED_POSTCODE(String pROPERTY_INSURED_POSTCODE) {
		PROPERTY_INSURED_POSTCODE = pROPERTY_INSURED_POSTCODE;
	}

	public String getPROPERTY_INSURED_STATE() {
		return PROPERTY_INSURED_STATE;
	}

	public void setPROPERTY_INSURED_STATE(String pROPERTY_INSURED_STATE) {
		PROPERTY_INSURED_STATE = pROPERTY_INSURED_STATE;
	}

	public String getPROPERTY_INSURED_COUNTRY() {
		return PROPERTY_INSURED_COUNTRY;
	}

	public void setPROPERTY_INSURED_COUNTRY(String pROPERTY_INSURED_COUNTRY) {
		PROPERTY_INSURED_COUNTRY = pROPERTY_INSURED_COUNTRY;
	}

	public String getUNDER_WRITING_QUESTION_1() {
		return UNDER_WRITING_QUESTION_1;
	}

	public void setUNDER_WRITING_QUESTION_1(String uNDER_WRITING_QUESTION_1) {
		UNDER_WRITING_QUESTION_1 = uNDER_WRITING_QUESTION_1;
	}

	public String getETIQA_PDPA_ACCEPT() {
		return ETIQA_PDPA_ACCEPT;
	}

	public void setETIQA_PDPA_ACCEPT(String eTIQA_PDPA_ACCEPT) {
		ETIQA_PDPA_ACCEPT = eTIQA_PDPA_ACCEPT;
	}

	public String getIMP_NOTICE_DECLERATION_PDA() {
		return IMP_NOTICE_DECLERATION_PDA;
	}

	public void setIMP_NOTICE_DECLERATION_PDA(String iMP_NOTICE_DECLERATION_PDA) {
		IMP_NOTICE_DECLERATION_PDA = iMP_NOTICE_DECLERATION_PDA;
	}

	public String getPAYMENT_METHOD() {
		return PAYMENT_METHOD;
	}

	public void setPAYMENT_METHOD(String pAYMENT_METHOD) {
		PAYMENT_METHOD = pAYMENT_METHOD;
	}

	public String getET_WC_DSP_CUSTOMER_CUSTOMER_ID() {
		return ET_WC_DSP_CUSTOMER_CUSTOMER_ID;
	}

	public void setET_WC_DSP_CUSTOMER_CUSTOMER_ID(String eT_WC_DSP_CUSTOMER_CUSTOMER_ID) {
		ET_WC_DSP_CUSTOMER_CUSTOMER_ID = eT_WC_DSP_CUSTOMER_CUSTOMER_ID;
	}

	public String getHOHH_GROSS_PREMIUM() {
		return HOHH_GROSS_PREMIUM;
	}

	public void setHOHH_GROSS_PREMIUM(String hOHH_GROSS_PREMIUM) {
		HOHH_GROSS_PREMIUM = hOHH_GROSS_PREMIUM;
	}

	public String getIS_PURCHASED() {
		return IS_PURCHASED;
	}

	public void setIS_PURCHASED(String iS_PURCHASED) {
		IS_PURCHASED = iS_PURCHASED;
	}

	public String getFULL_NAME() {
		return FULL_NAME;
	}

	public void setFULL_NAME(String fULL_NAME) {
		FULL_NAME = fULL_NAME;
	}

	public String getHOHH_LANGUAGE() {
		return HOHH_LANGUAGE;
	}

	public void setHOHH_LANGUAGE(String hOHH_LANGUAGE) {
		HOHH_LANGUAGE = hOHH_LANGUAGE;
	}

	List<String> ADDITIONAL_BENEFIT_CODE;
	List<String> ADDITIONAL_BENEFIT_VALUE;
	List<String> ADDITIONAL_BENEFIT_TEXT;
	List<Integer> ADDITIONAL_BENEFIT_ITEMNUMBER;

	public List<String> getADDITIONAL_BENEFIT_CODE() {
		return ADDITIONAL_BENEFIT_CODE;
	}

	public void setADDITIONAL_BENEFIT_CODE(List<String> aDDITIONAL_BENEFIT_CODE) {
		ADDITIONAL_BENEFIT_CODE = aDDITIONAL_BENEFIT_CODE;
	}

	public List<String> getADDITIONAL_BENEFIT_VALUE() {
		return ADDITIONAL_BENEFIT_VALUE;
	}

	public void setADDITIONAL_BENEFIT_VALUE(List<String> aDDITIONAL_BENEFIT_VALUE) {
		ADDITIONAL_BENEFIT_VALUE = aDDITIONAL_BENEFIT_VALUE;
	}

	public List<String> getADDITIONAL_BENEFIT_TEXT() {
		return ADDITIONAL_BENEFIT_TEXT;
	}

	public void setADDITIONAL_BENEFIT_TEXT(List<String> aDDITIONAL_BENEFIT_TEXT) {
		ADDITIONAL_BENEFIT_TEXT = aDDITIONAL_BENEFIT_TEXT;
	}

	public List<Integer> getADDITIONAL_BENEFIT_ITEMNUMBER() {
		return ADDITIONAL_BENEFIT_ITEMNUMBER;
	}

	public void setADDITIONAL_BENEFIT_ITEMNUMBER(List<Integer> aDDITIONAL_BENEFIT_ITEMNUMBER) {
		ADDITIONAL_BENEFIT_ITEMNUMBER = aDDITIONAL_BENEFIT_ITEMNUMBER;
	}

	String PROPERTY_INSURED_ADDRESS1;
	String PROPERTY_INSURED_ADDRESS2;

	public String getPROPERTY_INSURED_ADDRESS1() {
		return PROPERTY_INSURED_ADDRESS1;
	}

	public void setPROPERTY_INSURED_ADDRESS1(String pROPERTY_INSURED_ADDRESS1) {
		PROPERTY_INSURED_ADDRESS1 = pROPERTY_INSURED_ADDRESS1;
	}

	public String getPROPERTY_INSURED_ADDRESS2() {
		return PROPERTY_INSURED_ADDRESS2;
	}

	public void setPROPERTY_INSURED_ADDRESS2(String pROPERTY_INSURED_ADDRESS2) {
		PROPERTY_INSURED_ADDRESS2 = pROPERTY_INSURED_ADDRESS2;
	}

}
