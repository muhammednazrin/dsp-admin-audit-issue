package com.etiqa.dsp.dao.common.pojo;

public class Audit_User_Bean {

	private String id;
	private String pfnumber;
	private String name;
	private String groupid;
	private String groupname;
	private String status;
	private String createdate;
	private String updatedate;
	private String createby;
	private String updateby;

	private String oid;
	private String opfnumber;
	private String oname;
	private String ogroupid;
	private String ogroupname;
	private String ostatus;
	private String ocreatedate;
	private String oupdatedate;
	private String ocreateby;
	private String oupdateby;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPfnumber() {
		return pfnumber;
	}

	public void setPfnumber(String pfnumber) {
		this.pfnumber = pfnumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroupid() {
		return groupid;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreatedate() {
		return createdate;
	}

	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}

	public String getUpdatedate() {
		return updatedate;
	}

	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}

	public String getCreateby() {
		return createby;
	}

	public void setCreateby(String createby) {
		this.createby = createby;
	}

	public String getUpdateby() {
		return updateby;
	}

	public void setUpdateby(String updateby) {
		this.updateby = updateby;
	}

	public String getGroupname() {
		return groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public String getOpfnumber() {
		return opfnumber;
	}

	public void setOpfnumber(String opfnumber) {
		this.opfnumber = opfnumber;
	}

	public String getOname() {
		return oname;
	}

	public void setOname(String oname) {
		this.oname = oname;
	}

	public String getOgroupid() {
		return ogroupid;
	}

	public void setOgroupid(String ogroupid) {
		this.ogroupid = ogroupid;
	}

	public String getOgroupname() {
		return ogroupname;
	}

	public void setOgroupname(String ogroupname) {
		this.ogroupname = ogroupname;
	}

	public String getOstatus() {
		return ostatus;
	}

	public void setOstatus(String ostatus) {
		this.ostatus = ostatus;
	}

	public String getOcreatedate() {
		return ocreatedate;
	}

	public void setOcreatedate(String ocreatedate) {
		this.ocreatedate = ocreatedate;
	}

	public String getOupdatedate() {
		return oupdatedate;
	}

	public void setOupdatedate(String oupdatedate) {
		this.oupdatedate = oupdatedate;
	}

	public String getOcreateby() {
		return ocreateby;
	}

	public void setOcreateby(String ocreateby) {
		this.ocreateby = ocreateby;
	}

	public String getOupdateby() {
		return oupdateby;
	}

	public void setOupdateby(String oupdateby) {
		this.oupdateby = oupdateby;
	}

}
