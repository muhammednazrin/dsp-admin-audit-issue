package com.etiqa.dsp.dao.common.pojo;

import java.util.ArrayList;
import java.util.List;

public class FetchSalesIllustrationVpmsDataVo {
	List<SalesIllustrationVo> listOfSIVpmsData = new ArrayList<SalesIllustrationVo>();

	public List<SalesIllustrationVo> getListOfSIVpmsData() {
		return listOfSIVpmsData;
	}

	public void setListOfSIVpmsData(List<SalesIllustrationVo> listOfSIVpmsData) {
		this.listOfSIVpmsData = listOfSIVpmsData;
	}

}
