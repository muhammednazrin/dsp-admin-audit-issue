package com.etiqa.dsp.dao.common.pojo;

public class custQuotPmntPolicyVo {
	private int CUSTOMER_ID;
	private String CUSTOMER_ID_TYPE;
	private String CUSTOMER_NRIC_ID;
	private String CUSTOMER_NAME;
	private String CUSTOMER_DOB;
	private String CUSTOMER_GENDER;
	private String CUSTOMER_RACE;
	private String CUSTOMER_RELIGION;
	private String CUSTOMER_NATIONALITY;
	private String CUSTOMER_TITLE;
	private String CUSTOMER_NATIONALITY_RACE;
	private String CUSTOMER_EDU_LEVEL;
	private String CUSTOMER_MARITALSTATUS;
	private String CUSTOMER_SALARY_RANGE;
	private String CUSTOMER_OCCUPATION;
	private String CUSTOMER_ADDRESS1;
	private String CUSTOMER_ADDRESS2;
	private String CUSTOMER_ADDRESS3;
	private String CUSTOMER_POSTCODE;
	private String CUSTOMER_STATE;
	private String CUSTOMER_COUNTRY;
	private String CUSTOMER_MAIL_ADDRESS1;
	private String CUSTOMER_MAIL_ADDRESS2;
	private String CUSTOMER_MAIL_ADDRESS3;
	private String CUSTOMER_MAIL_POSTCODE;
	private String CUSTOMER_MAIL_STATE;
	private String CUSTOMER_MAIL_COUNTRY;
	private int CUSTOMER_NO_CHILDREN;
	private String CUSTOMER_MOBILE_NO;
	private String CUSTOMER_EMAIL;
	private String LEADS_FLAG;
	private String SALES_TOOL;
	private String EMAIL_SENT_COUNTER;
	private int QQ_ID;
	private String CREATE_DATE;
	private String UPDATED_DATE;
	private String CUSTOMER_EMPLOYER;
	private String CUSTOMER_CLIENTTYPE;
	private String CUSTOMER_INDUSTRY;
	private int DSP_QQ_ID;
	private String PRODUCT_CODE;
	private String CREATED_DATE;
	private int ID;
	private String POLICY_NUMBER;
	private String PRODUCT_PLAN_CODE;
	private String POLICY_EFFECTIVE_TIMESTAMP;
	private int SUM_INSURED;
	private String POLICY_STATUS;
	private String POLICY_TYPE_CODE;
	private String POLICY_EXPIRY_TIMESTAMP;
	private int TRANSACTION_ID;
	private String CREATION_TIMESTAMP;
	private String TL_CHECK_DIGIT;
	private String OCCUPATIONCLASS;

	private int TERMLIFE_QQ_ID;
	private String LEADS_EMAIL_ID;
	private String SUB_PRODUCT_ID;
	private String DOB;
	private String FULL_NAME;
	private String IS_SMOKER;
	private String GENDER;
	private String MOBILE_NUMBER;
	private String PROMO_CODE;
	private String PROMO_AMOUNT;
	private String DISCOUNT_ID;
	private String DISCOUNT_AMOUNT;
	private String TL_POLICY_SUM_INSURED;
	private String DURATION_OF_BENIFIT;
	private String TL_SUM_INSURED;
	private String TL_GROSS_PREMUM;
	private String GST_ON_GROSS_PREMIUM;
	private String STAMP_DUTY_PREMIUM;
	private String TL_TOTAL_PREMIUM_PAYABLE;
	private String PREMIUM_PAYMENT_FREQUENCY_TYPE;
	private String PREMIUM_PAYMENT_FREQUENCY;
	private String NEW_IC_COPY;
	private String UNDER_WRITING_QUESTION_1;
	private String UNDER_WRITING_QUESTION_2;
	private String UNDER_WRITING_QUESTION_3;
	private String HEIGHT;
	private String WEIGHT;
	private String BMI;
	private String USA_TAX_PAYER_ID;
	private String CONFIRM_AGREEMENT_DECL_QQ3;
	private String CONFIRM_ETIQA_DECLERATION_QQ3;
	private String CONFIRM_SALES_ILLUSTRATION_QQ4;
	private String CONFIRM_PRODUCT_DISCLOSURE_QQ4;
	private String CONFIRM_E_APPLICATION_DECL_QQ4;
	private String CONFIRM_DEDUCT_ANNUAL_QQ4;
	private String PLAN_NAME;
	private String COVERAGE_START_DATE;
	private String RECCURING_PREMIUM_FLAG;
	private String PAYMENT_METHOD;
	private String AGENT_CODE;
	private String TL_QQ_STATUS;
	private String REJECTED;
	private String REJECTED_REASON;
	private String IS_PURCHASED;
	private String DSP_CUSTOMER_ID;
	private String QUOTATION_CREATION_DATETIME;
	private String PRODUCT_VERSION;

	private String langValue;
	private String vehRegNo;

	private String roadTasMailStatus;
	private String consignmentNo;
	private String expectedDeliveryDate;
	private String deliveryAddress;
	private String expDeliveryDt;
	private String eMailIds;
	private String thresholdLimit;
	private String currentBalance;
	private String entity;

	public String getVehRegNo() {
		return vehRegNo;
	}

	public void setVehRegNo(String vehRegNo) {
		this.vehRegNo = vehRegNo;
	}

	public String getLangValue() {
		return langValue;
	}

	public void setLangValue(String langValue) {
		this.langValue = langValue;
	}

	public String getOCCUPATIONCLASS() {
		return OCCUPATIONCLASS;
	}

	public void setOCCUPATIONCLASS(String oCCUPATIONCLASS) {
		OCCUPATIONCLASS = oCCUPATIONCLASS;
	}

	public String getTL_CHECK_DIGIT() {
		return TL_CHECK_DIGIT;
	}

	public void setTL_CHECK_DIGIT(String tL_CHECK_DIGIT) {
		TL_CHECK_DIGIT = tL_CHECK_DIGIT;
	}

	public int getCUSTOMER_ID() {
		return CUSTOMER_ID;
	}

	public void setCUSTOMER_ID(int cUSTOMER_ID) {
		CUSTOMER_ID = cUSTOMER_ID;
	}

	public String getCUSTOMER_ID_TYPE() {
		return CUSTOMER_ID_TYPE;
	}

	public void setCUSTOMER_ID_TYPE(String cUSTOMER_ID_TYPE) {
		CUSTOMER_ID_TYPE = cUSTOMER_ID_TYPE;
	}

	public String getCUSTOMER_NRIC_ID() {
		return CUSTOMER_NRIC_ID;
	}

	public void setCUSTOMER_NRIC_ID(String cUSTOMER_NRIC_ID) {
		CUSTOMER_NRIC_ID = cUSTOMER_NRIC_ID;
	}

	public String getCUSTOMER_NAME() {
		return CUSTOMER_NAME;
	}

	public void setCUSTOMER_NAME(String cUSTOMER_NAME) {
		CUSTOMER_NAME = cUSTOMER_NAME;
	}

	public String getCUSTOMER_DOB() {
		return CUSTOMER_DOB;
	}

	public void setCUSTOMER_DOB(String cUSTOMER_DOB) {
		CUSTOMER_DOB = cUSTOMER_DOB;
	}

	public String getCUSTOMER_GENDER() {
		return CUSTOMER_GENDER;
	}

	public void setCUSTOMER_GENDER(String cUSTOMER_GENDER) {
		CUSTOMER_GENDER = cUSTOMER_GENDER;
	}

	public String getCUSTOMER_RACE() {
		return CUSTOMER_RACE;
	}

	public void setCUSTOMER_RACE(String cUSTOMER_RACE) {
		CUSTOMER_RACE = cUSTOMER_RACE;
	}

	public String getCUSTOMER_RELIGION() {
		return CUSTOMER_RELIGION;
	}

	public void setCUSTOMER_RELIGION(String cUSTOMER_RELIGION) {
		CUSTOMER_RELIGION = cUSTOMER_RELIGION;
	}

	public String getCUSTOMER_NATIONALITY() {
		return CUSTOMER_NATIONALITY;
	}

	public void setCUSTOMER_NATIONALITY(String cUSTOMER_NATIONALITY) {
		CUSTOMER_NATIONALITY = cUSTOMER_NATIONALITY;
	}

	public String getCUSTOMER_TITLE() {
		return CUSTOMER_TITLE;
	}

	public void setCUSTOMER_TITLE(String cUSTOMER_TITLE) {
		CUSTOMER_TITLE = cUSTOMER_TITLE;
	}

	public String getCUSTOMER_NATIONALITY_RACE() {
		return CUSTOMER_NATIONALITY_RACE;
	}

	public void setCUSTOMER_NATIONALITY_RACE(String cUSTOMER_NATIONALITY_RACE) {
		CUSTOMER_NATIONALITY_RACE = cUSTOMER_NATIONALITY_RACE;
	}

	public String getCUSTOMER_EDU_LEVEL() {
		return CUSTOMER_EDU_LEVEL;
	}

	public void setCUSTOMER_EDU_LEVEL(String cUSTOMER_EDU_LEVEL) {
		CUSTOMER_EDU_LEVEL = cUSTOMER_EDU_LEVEL;
	}

	public String getCUSTOMER_MARITALSTATUS() {
		return CUSTOMER_MARITALSTATUS;
	}

	public void setCUSTOMER_MARITALSTATUS(String cUSTOMER_MARITALSTATUS) {
		CUSTOMER_MARITALSTATUS = cUSTOMER_MARITALSTATUS;
	}

	public String getCUSTOMER_SALARY_RANGE() {
		return CUSTOMER_SALARY_RANGE;
	}

	public void setCUSTOMER_SALARY_RANGE(String cUSTOMER_SALARY_RANGE) {
		CUSTOMER_SALARY_RANGE = cUSTOMER_SALARY_RANGE;
	}

	public String getCUSTOMER_OCCUPATION() {
		return CUSTOMER_OCCUPATION;
	}

	public void setCUSTOMER_OCCUPATION(String cUSTOMER_OCCUPATION) {
		CUSTOMER_OCCUPATION = cUSTOMER_OCCUPATION;
	}

	public String getCUSTOMER_ADDRESS1() {
		return CUSTOMER_ADDRESS1;
	}

	public void setCUSTOMER_ADDRESS1(String cUSTOMER_ADDRESS1) {
		CUSTOMER_ADDRESS1 = cUSTOMER_ADDRESS1;
	}

	public String getCUSTOMER_ADDRESS2() {
		return CUSTOMER_ADDRESS2;
	}

	public void setCUSTOMER_ADDRESS2(String cUSTOMER_ADDRESS2) {
		CUSTOMER_ADDRESS2 = cUSTOMER_ADDRESS2;
	}

	public String getCUSTOMER_ADDRESS3() {
		return CUSTOMER_ADDRESS3;
	}

	public void setCUSTOMER_ADDRESS3(String cUSTOMER_ADDRESS3) {
		CUSTOMER_ADDRESS3 = cUSTOMER_ADDRESS3;
	}

	public String getCUSTOMER_POSTCODE() {
		return CUSTOMER_POSTCODE;
	}

	public void setCUSTOMER_POSTCODE(String cUSTOMER_POSTCODE) {
		CUSTOMER_POSTCODE = cUSTOMER_POSTCODE;
	}

	public String getCUSTOMER_STATE() {
		return CUSTOMER_STATE;
	}

	public void setCUSTOMER_STATE(String cUSTOMER_STATE) {
		CUSTOMER_STATE = cUSTOMER_STATE;
	}

	public String getCUSTOMER_COUNTRY() {
		return CUSTOMER_COUNTRY;
	}

	public void setCUSTOMER_COUNTRY(String cUSTOMER_COUNTRY) {
		CUSTOMER_COUNTRY = cUSTOMER_COUNTRY;
	}

	public String getCUSTOMER_MAIL_ADDRESS1() {
		return CUSTOMER_MAIL_ADDRESS1;
	}

	public void setCUSTOMER_MAIL_ADDRESS1(String cUSTOMER_MAIL_ADDRESS1) {
		CUSTOMER_MAIL_ADDRESS1 = cUSTOMER_MAIL_ADDRESS1;
	}

	public String getCUSTOMER_MAIL_ADDRESS2() {
		return CUSTOMER_MAIL_ADDRESS2;
	}

	public void setCUSTOMER_MAIL_ADDRESS2(String cUSTOMER_MAIL_ADDRESS2) {
		CUSTOMER_MAIL_ADDRESS2 = cUSTOMER_MAIL_ADDRESS2;
	}

	public String getCUSTOMER_MAIL_ADDRESS3() {
		return CUSTOMER_MAIL_ADDRESS3;
	}

	public void setCUSTOMER_MAIL_ADDRESS3(String cUSTOMER_MAIL_ADDRESS3) {
		CUSTOMER_MAIL_ADDRESS3 = cUSTOMER_MAIL_ADDRESS3;
	}

	public String getCUSTOMER_MAIL_POSTCODE() {
		return CUSTOMER_MAIL_POSTCODE;
	}

	public void setCUSTOMER_MAIL_POSTCODE(String cUSTOMER_MAIL_POSTCODE) {
		CUSTOMER_MAIL_POSTCODE = cUSTOMER_MAIL_POSTCODE;
	}

	public String getCUSTOMER_MAIL_STATE() {
		return CUSTOMER_MAIL_STATE;
	}

	public void setCUSTOMER_MAIL_STATE(String cUSTOMER_MAIL_STATE) {
		CUSTOMER_MAIL_STATE = cUSTOMER_MAIL_STATE;
	}

	public String getCUSTOMER_MAIL_COUNTRY() {
		return CUSTOMER_MAIL_COUNTRY;
	}

	public void setCUSTOMER_MAIL_COUNTRY(String cUSTOMER_MAIL_COUNTRY) {
		CUSTOMER_MAIL_COUNTRY = cUSTOMER_MAIL_COUNTRY;
	}

	public int getCUSTOMER_NO_CHILDREN() {
		return CUSTOMER_NO_CHILDREN;
	}

	public void setCUSTOMER_NO_CHILDREN(int cUSTOMER_NO_CHILDREN) {
		CUSTOMER_NO_CHILDREN = cUSTOMER_NO_CHILDREN;
	}

	public String getCUSTOMER_MOBILE_NO() {
		return CUSTOMER_MOBILE_NO;
	}

	public void setCUSTOMER_MOBILE_NO(String cUSTOMER_MOBILE_NO) {
		CUSTOMER_MOBILE_NO = cUSTOMER_MOBILE_NO;
	}

	public String getCUSTOMER_EMAIL() {
		return CUSTOMER_EMAIL;
	}

	public void setCUSTOMER_EMAIL(String cUSTOMER_EMAIL) {
		CUSTOMER_EMAIL = cUSTOMER_EMAIL;
	}

	public String getLEADS_FLAG() {
		return LEADS_FLAG;
	}

	public void setLEADS_FLAG(String lEADS_FLAG) {
		LEADS_FLAG = lEADS_FLAG;
	}

	public String getSALES_TOOL() {
		return SALES_TOOL;
	}

	public void setSALES_TOOL(String sALES_TOOL) {
		SALES_TOOL = sALES_TOOL;
	}

	public String getEMAIL_SENT_COUNTER() {
		return EMAIL_SENT_COUNTER;
	}

	public void setEMAIL_SENT_COUNTER(String eMAIL_SENT_COUNTER) {
		EMAIL_SENT_COUNTER = eMAIL_SENT_COUNTER;
	}

	public int getQQ_ID() {
		return QQ_ID;
	}

	public void setQQ_ID(int qQ_ID) {
		QQ_ID = qQ_ID;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getUPDATED_DATE() {
		return UPDATED_DATE;
	}

	public void setUPDATED_DATE(String uPDATED_DATE) {
		UPDATED_DATE = uPDATED_DATE;
	}

	public String getCUSTOMER_EMPLOYER() {
		return CUSTOMER_EMPLOYER;
	}

	public void setCUSTOMER_EMPLOYER(String cUSTOMER_EMPLOYER) {
		CUSTOMER_EMPLOYER = cUSTOMER_EMPLOYER;
	}

	public String getCUSTOMER_CLIENTTYPE() {
		return CUSTOMER_CLIENTTYPE;
	}

	public void setCUSTOMER_CLIENTTYPE(String cUSTOMER_CLIENTTYPE) {
		CUSTOMER_CLIENTTYPE = cUSTOMER_CLIENTTYPE;
	}

	public String getCUSTOMER_INDUSTRY() {
		return CUSTOMER_INDUSTRY;
	}

	public void setCUSTOMER_INDUSTRY(String cUSTOMER_INDUSTRY) {
		CUSTOMER_INDUSTRY = cUSTOMER_INDUSTRY;
	}

	public int getDSP_QQ_ID() {
		return DSP_QQ_ID;
	}

	public void setDSP_QQ_ID(int dSP_QQ_ID) {
		DSP_QQ_ID = dSP_QQ_ID;
	}

	public String getPRODUCT_CODE() {
		return PRODUCT_CODE;
	}

	public void setPRODUCT_CODE(String pRODUCT_CODE) {
		PRODUCT_CODE = pRODUCT_CODE;
	}

	public String getCREATED_DATE() {
		return CREATED_DATE;
	}

	public void setCREATED_DATE(String cREATED_DATE) {
		CREATED_DATE = cREATED_DATE;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getPOLICY_NUMBER() {
		return POLICY_NUMBER;
	}

	public void setPOLICY_NUMBER(String pOLICY_NUMBER) {
		POLICY_NUMBER = pOLICY_NUMBER;
	}

	public String getPRODUCT_PLAN_CODE() {
		return PRODUCT_PLAN_CODE;
	}

	public void setPRODUCT_PLAN_CODE(String pRODUCT_PLAN_CODE) {
		PRODUCT_PLAN_CODE = pRODUCT_PLAN_CODE;
	}

	public String getPOLICY_EFFECTIVE_TIMESTAMP() {
		return POLICY_EFFECTIVE_TIMESTAMP;
	}

	public void setPOLICY_EFFECTIVE_TIMESTAMP(String pOLICY_EFFECTIVE_TIMESTAMP) {
		POLICY_EFFECTIVE_TIMESTAMP = pOLICY_EFFECTIVE_TIMESTAMP;
	}

	public int getSUM_INSURED() {
		return SUM_INSURED;
	}

	public void setSUM_INSURED(int sUM_INSURED) {
		SUM_INSURED = sUM_INSURED;
	}

	public String getPOLICY_STATUS() {
		return POLICY_STATUS;
	}

	public void setPOLICY_STATUS(String pOLICY_STATUS) {
		POLICY_STATUS = pOLICY_STATUS;
	}

	public String getPOLICY_TYPE_CODE() {
		return POLICY_TYPE_CODE;
	}

	public void setPOLICY_TYPE_CODE(String pOLICY_TYPE_CODE) {
		POLICY_TYPE_CODE = pOLICY_TYPE_CODE;
	}

	public String getPOLICY_EXPIRY_TIMESTAMP() {
		return POLICY_EXPIRY_TIMESTAMP;
	}

	public void setPOLICY_EXPIRY_TIMESTAMP(String pOLICY_EXPIRY_TIMESTAMP) {
		POLICY_EXPIRY_TIMESTAMP = pOLICY_EXPIRY_TIMESTAMP;
	}

	public int getTRANSACTION_ID() {
		return TRANSACTION_ID;
	}

	public void setTRANSACTION_ID(int tRANSACTION_ID) {
		TRANSACTION_ID = tRANSACTION_ID;
	}

	public String getCREATION_TIMESTAMP() {
		return CREATION_TIMESTAMP;
	}

	public void setCREATION_TIMESTAMP(String cREATION_TIMESTAMP) {
		CREATION_TIMESTAMP = cREATION_TIMESTAMP;
	}

	public int getTERMLIFE_QQ_ID() {
		return TERMLIFE_QQ_ID;
	}

	public void setTERMLIFE_QQ_ID(int tERMLIFE_QQ_ID) {
		TERMLIFE_QQ_ID = tERMLIFE_QQ_ID;
	}

	public String getLEADS_EMAIL_ID() {
		return LEADS_EMAIL_ID;
	}

	public void setLEADS_EMAIL_ID(String lEADS_EMAIL_ID) {
		LEADS_EMAIL_ID = lEADS_EMAIL_ID;
	}

	public String getSUB_PRODUCT_ID() {
		return SUB_PRODUCT_ID;
	}

	public void setSUB_PRODUCT_ID(String sUB_PRODUCT_ID) {
		SUB_PRODUCT_ID = sUB_PRODUCT_ID;
	}

	public String getDOB() {
		return DOB;
	}

	public void setDOB(String dOB) {
		DOB = dOB;
	}

	public String getFULL_NAME() {
		return FULL_NAME;
	}

	public void setFULL_NAME(String fULL_NAME) {
		FULL_NAME = fULL_NAME;
	}

	public String getIS_SMOKER() {
		return IS_SMOKER;
	}

	public void setIS_SMOKER(String iS_SMOKER) {
		IS_SMOKER = iS_SMOKER;
	}

	public String getGENDER() {
		return GENDER;
	}

	public void setGENDER(String gENDER) {
		GENDER = gENDER;
	}

	public String getMOBILE_NUMBER() {
		return MOBILE_NUMBER;
	}

	public void setMOBILE_NUMBER(String mOBILE_NUMBER) {
		MOBILE_NUMBER = mOBILE_NUMBER;
	}

	public String getPROMO_CODE() {
		return PROMO_CODE;
	}

	public void setPROMO_CODE(String pROMO_CODE) {
		PROMO_CODE = pROMO_CODE;
	}

	public String getPROMO_AMOUNT() {
		return PROMO_AMOUNT;
	}

	public void setPROMO_AMOUNT(String pROMO_AMOUNT) {
		PROMO_AMOUNT = pROMO_AMOUNT;
	}

	public String getDISCOUNT_ID() {
		return DISCOUNT_ID;
	}

	public void setDISCOUNT_ID(String dISCOUNT_ID) {
		DISCOUNT_ID = dISCOUNT_ID;
	}

	public String getDISCOUNT_AMOUNT() {
		return DISCOUNT_AMOUNT;
	}

	public void setDISCOUNT_AMOUNT(String dISCOUNT_AMOUNT) {
		DISCOUNT_AMOUNT = dISCOUNT_AMOUNT;
	}

	public String getTL_POLICY_SUM_INSURED() {
		return TL_POLICY_SUM_INSURED;
	}

	public void setTL_POLICY_SUM_INSURED(String tL_POLICY_SUM_INSURED) {
		TL_POLICY_SUM_INSURED = tL_POLICY_SUM_INSURED;
	}

	public String getDURATION_OF_BENIFIT() {
		return DURATION_OF_BENIFIT;
	}

	public void setDURATION_OF_BENIFIT(String dURATION_OF_BENIFIT) {
		DURATION_OF_BENIFIT = dURATION_OF_BENIFIT;
	}

	public String getTL_SUM_INSURED() {
		return TL_SUM_INSURED;
	}

	public void setTL_SUM_INSURED(String tL_SUM_INSURED) {
		TL_SUM_INSURED = tL_SUM_INSURED;
	}

	public String getTL_GROSS_PREMUM() {
		return TL_GROSS_PREMUM;
	}

	public void setTL_GROSS_PREMUM(String tL_GROSS_PREMUM) {
		TL_GROSS_PREMUM = tL_GROSS_PREMUM;
	}

	public String getGST_ON_GROSS_PREMIUM() {
		return GST_ON_GROSS_PREMIUM;
	}

	public void setGST_ON_GROSS_PREMIUM(String gST_ON_GROSS_PREMIUM) {
		GST_ON_GROSS_PREMIUM = gST_ON_GROSS_PREMIUM;
	}

	public String getSTAMP_DUTY_PREMIUM() {
		return STAMP_DUTY_PREMIUM;
	}

	public void setSTAMP_DUTY_PREMIUM(String sTAMP_DUTY_PREMIUM) {
		STAMP_DUTY_PREMIUM = sTAMP_DUTY_PREMIUM;
	}

	public String getTL_TOTAL_PREMIUM_PAYABLE() {
		return TL_TOTAL_PREMIUM_PAYABLE;
	}

	public void setTL_TOTAL_PREMIUM_PAYABLE(String tL_TOTAL_PREMIUM_PAYABLE) {
		TL_TOTAL_PREMIUM_PAYABLE = tL_TOTAL_PREMIUM_PAYABLE;
	}

	public String getPREMIUM_PAYMENT_FREQUENCY_TYPE() {
		return PREMIUM_PAYMENT_FREQUENCY_TYPE;
	}

	public void setPREMIUM_PAYMENT_FREQUENCY_TYPE(String pREMIUM_PAYMENT_FREQUENCY_TYPE) {
		PREMIUM_PAYMENT_FREQUENCY_TYPE = pREMIUM_PAYMENT_FREQUENCY_TYPE;
	}

	public String getPREMIUM_PAYMENT_FREQUENCY() {
		return PREMIUM_PAYMENT_FREQUENCY;
	}

	public void setPREMIUM_PAYMENT_FREQUENCY(String pREMIUM_PAYMENT_FREQUENCY) {
		PREMIUM_PAYMENT_FREQUENCY = pREMIUM_PAYMENT_FREQUENCY;
	}

	public String getNEW_IC_COPY() {
		return NEW_IC_COPY;
	}

	public void setNEW_IC_COPY(String nEW_IC_COPY) {
		NEW_IC_COPY = nEW_IC_COPY;
	}

	public String getUNDER_WRITING_QUESTION_1() {
		return UNDER_WRITING_QUESTION_1;
	}

	public void setUNDER_WRITING_QUESTION_1(String uNDER_WRITING_QUESTION_1) {
		UNDER_WRITING_QUESTION_1 = uNDER_WRITING_QUESTION_1;
	}

	public String getUNDER_WRITING_QUESTION_2() {
		return UNDER_WRITING_QUESTION_2;
	}

	public void setUNDER_WRITING_QUESTION_2(String uNDER_WRITING_QUESTION_2) {
		UNDER_WRITING_QUESTION_2 = uNDER_WRITING_QUESTION_2;
	}

	public String getUNDER_WRITING_QUESTION_3() {
		return UNDER_WRITING_QUESTION_3;
	}

	public void setUNDER_WRITING_QUESTION_3(String uNDER_WRITING_QUESTION_3) {
		UNDER_WRITING_QUESTION_3 = uNDER_WRITING_QUESTION_3;
	}

	public String getHEIGHT() {
		return HEIGHT;
	}

	public void setHEIGHT(String hEIGHT) {
		HEIGHT = hEIGHT;
	}

	public String getWEIGHT() {
		return WEIGHT;
	}

	public void setWEIGHT(String wEIGHT) {
		WEIGHT = wEIGHT;
	}

	public String getBMI() {
		return BMI;
	}

	public void setBMI(String bMI) {
		BMI = bMI;
	}

	public String getUSA_TAX_PAYER_ID() {
		return USA_TAX_PAYER_ID;
	}

	public void setUSA_TAX_PAYER_ID(String uSA_TAX_PAYER_ID) {
		USA_TAX_PAYER_ID = uSA_TAX_PAYER_ID;
	}

	public String getCONFIRM_AGREEMENT_DECL_QQ3() {
		return CONFIRM_AGREEMENT_DECL_QQ3;
	}

	public void setCONFIRM_AGREEMENT_DECL_QQ3(String cONFIRM_AGREEMENT_DECL_QQ3) {
		CONFIRM_AGREEMENT_DECL_QQ3 = cONFIRM_AGREEMENT_DECL_QQ3;
	}

	public String getCONFIRM_ETIQA_DECLERATION_QQ3() {
		return CONFIRM_ETIQA_DECLERATION_QQ3;
	}

	public void setCONFIRM_ETIQA_DECLERATION_QQ3(String cONFIRM_ETIQA_DECLERATION_QQ3) {
		CONFIRM_ETIQA_DECLERATION_QQ3 = cONFIRM_ETIQA_DECLERATION_QQ3;
	}

	public String getCONFIRM_SALES_ILLUSTRATION_QQ4() {
		return CONFIRM_SALES_ILLUSTRATION_QQ4;
	}

	public void setCONFIRM_SALES_ILLUSTRATION_QQ4(String cONFIRM_SALES_ILLUSTRATION_QQ4) {
		CONFIRM_SALES_ILLUSTRATION_QQ4 = cONFIRM_SALES_ILLUSTRATION_QQ4;
	}

	public String getCONFIRM_PRODUCT_DISCLOSURE_QQ4() {
		return CONFIRM_PRODUCT_DISCLOSURE_QQ4;
	}

	public void setCONFIRM_PRODUCT_DISCLOSURE_QQ4(String cONFIRM_PRODUCT_DISCLOSURE_QQ4) {
		CONFIRM_PRODUCT_DISCLOSURE_QQ4 = cONFIRM_PRODUCT_DISCLOSURE_QQ4;
	}

	public String getCONFIRM_E_APPLICATION_DECL_QQ4() {
		return CONFIRM_E_APPLICATION_DECL_QQ4;
	}

	public void setCONFIRM_E_APPLICATION_DECL_QQ4(String cONFIRM_E_APPLICATION_DECL_QQ4) {
		CONFIRM_E_APPLICATION_DECL_QQ4 = cONFIRM_E_APPLICATION_DECL_QQ4;
	}

	public String getCONFIRM_DEDUCT_ANNUAL_QQ4() {
		return CONFIRM_DEDUCT_ANNUAL_QQ4;
	}

	public void setCONFIRM_DEDUCT_ANNUAL_QQ4(String cONFIRM_DEDUCT_ANNUAL_QQ4) {
		CONFIRM_DEDUCT_ANNUAL_QQ4 = cONFIRM_DEDUCT_ANNUAL_QQ4;
	}

	public String getPLAN_NAME() {
		return PLAN_NAME;
	}

	public void setPLAN_NAME(String pLAN_NAME) {
		PLAN_NAME = pLAN_NAME;
	}

	public String getCOVERAGE_START_DATE() {
		return COVERAGE_START_DATE;
	}

	public void setCOVERAGE_START_DATE(String cOVERAGE_START_DATE) {
		COVERAGE_START_DATE = cOVERAGE_START_DATE;
	}

	public String getRECCURING_PREMIUM_FLAG() {
		return RECCURING_PREMIUM_FLAG;
	}

	public void setRECCURING_PREMIUM_FLAG(String rECCURING_PREMIUM_FLAG) {
		RECCURING_PREMIUM_FLAG = rECCURING_PREMIUM_FLAG;
	}

	public String getPAYMENT_METHOD() {
		return PAYMENT_METHOD;
	}

	public void setPAYMENT_METHOD(String pAYMENT_METHOD) {
		PAYMENT_METHOD = pAYMENT_METHOD;
	}

	public String getAGENT_CODE() {
		return AGENT_CODE;
	}

	public void setAGENT_CODE(String aGENT_CODE) {
		AGENT_CODE = aGENT_CODE;
	}

	public String getTL_QQ_STATUS() {
		return TL_QQ_STATUS;
	}

	public void setTL_QQ_STATUS(String tL_QQ_STATUS) {
		TL_QQ_STATUS = tL_QQ_STATUS;
	}

	public String getREJECTED() {
		return REJECTED;
	}

	public void setREJECTED(String rEJECTED) {
		REJECTED = rEJECTED;
	}

	public String getREJECTED_REASON() {
		return REJECTED_REASON;
	}

	public void setREJECTED_REASON(String rEJECTED_REASON) {
		REJECTED_REASON = rEJECTED_REASON;
	}

	public String getIS_PURCHASED() {
		return IS_PURCHASED;
	}

	public void setIS_PURCHASED(String iS_PURCHASED) {
		IS_PURCHASED = iS_PURCHASED;
	}

	public String getDSP_CUSTOMER_ID() {
		return DSP_CUSTOMER_ID;
	}

	public void setDSP_CUSTOMER_ID(String dSP_CUSTOMER_ID) {
		DSP_CUSTOMER_ID = dSP_CUSTOMER_ID;
	}

	public String getQUOTATION_CREATION_DATETIME() {
		return QUOTATION_CREATION_DATETIME;
	}

	public void setQUOTATION_CREATION_DATETIME(String qUOTATION_CREATION_DATETIME) {
		QUOTATION_CREATION_DATETIME = qUOTATION_CREATION_DATETIME;
	}

	public String getPRODUCT_VERSION() {
		return PRODUCT_VERSION;
	}

	public void setPRODUCT_VERSION(String pRODUCT_VERSION) {
		PRODUCT_VERSION = pRODUCT_VERSION;
	}

	public String getRoadTasMailStatus() {
		return roadTasMailStatus;
	}

	public void setRoadTasMailStatus(String roadTasMailStatus) {
		this.roadTasMailStatus = roadTasMailStatus;
	}

	public String getConsignmentNo() {
		return consignmentNo;
	}

	public void setConsignmentNo(String consignmentNo) {
		this.consignmentNo = consignmentNo;
	}

	public String getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}

	public void setExpectedDeliveryDate(String expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getExpDeliveryDt() {
		return expDeliveryDt;
	}

	public void setExpDeliveryDt(String expDeliveryDt) {
		this.expDeliveryDt = expDeliveryDt;
	}

	public String geteMailIds() {
		return eMailIds;
	}

	public void seteMailIds(String eMailIds) {
		this.eMailIds = eMailIds;
	}

	public String getThresholdLimit() {
		return thresholdLimit;
	}

	public void setThresholdLimit(String thresholdLimit) {
		this.thresholdLimit = thresholdLimit;
	}

	public String getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(String currentBalance) {
		this.currentBalance = currentBalance;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

}
