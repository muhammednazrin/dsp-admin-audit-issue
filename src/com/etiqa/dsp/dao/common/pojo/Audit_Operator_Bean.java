package com.etiqa.dsp.dao.common.pojo;

public class Audit_Operator_Bean {

	private String id;
	private String operatorcode;
	private String operatorname;
	private String registrationdate;
	private String operatorcategory;
	private String annualsalestarget;
	private String address1;
	private String city;

	private String state;
	private String country;
	private String postcode;
	private String annualreferalid;

	private String o_id;
	private String o_operatorcode;
	private String o_operatorname;
	private String o_registrationdate;
	private String o_operatorcategory;
	private String o_annualsalestarget;
	private String o_address1;
	private String o_city;

	private String o_state;
	private String o_country;
	private String o_postcode;
	private String o_annualreferalid;

	private String who;
	private String datewhen;
	private String recordcomment;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOperatorcode() {
		return operatorcode;
	}

	public void setOperatorcode(String operatorcode) {
		this.operatorcode = operatorcode;
	}

	public String getOperatorname() {
		return operatorname;
	}

	public void setOperatorname(String operatorname) {
		this.operatorname = operatorname;
	}

	public String getRegistrationdate() {
		return registrationdate;
	}

	public void setRegistrationdate(String registrationdate) {
		this.registrationdate = registrationdate;
	}

	public String getOperatorcategory() {
		return operatorcategory;
	}

	public void setOperatorcategory(String operatorcategory) {
		this.operatorcategory = operatorcategory;
	}

	public String getAnnualsalestarget() {
		return annualsalestarget;
	}

	public void setAnnualsalestarget(String annualsalestarget) {
		this.annualsalestarget = annualsalestarget;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getAnnualreferalid() {
		return annualreferalid;
	}

	public void setAnnualreferalid(String annualreferalid) {
		this.annualreferalid = annualreferalid;
	}

	public String getWho() {
		return who;
	}

	public void setWho(String who) {
		this.who = who;
	}

	public String getDatewhen() {
		return datewhen;
	}

	public void setDatewhen(String datewhen) {
		this.datewhen = datewhen;
	}

	public String getRecordcomment() {
		return recordcomment;
	}

	public void setRecordcomment(String recordcomment) {
		this.recordcomment = recordcomment;
	}

	public String getO_id() {
		return o_id;
	}

	public void setO_id(String o_id) {
		this.o_id = o_id;
	}

	public String getO_operatorcode() {
		return o_operatorcode;
	}

	public void setO_operatorcode(String o_operatorcode) {
		this.o_operatorcode = o_operatorcode;
	}

	public String getO_operatorname() {
		return o_operatorname;
	}

	public void setO_operatorname(String o_operatorname) {
		this.o_operatorname = o_operatorname;
	}

	public String getO_registrationdate() {
		return o_registrationdate;
	}

	public void setO_registrationdate(String o_registrationdate) {
		this.o_registrationdate = o_registrationdate;
	}

	public String getO_operatorcategory() {
		return o_operatorcategory;
	}

	public void setO_operatorcategory(String o_operatorcategory) {
		this.o_operatorcategory = o_operatorcategory;
	}

	public String getO_annualsalestarget() {
		return o_annualsalestarget;
	}

	public void setO_annualsalestarget(String o_annualsalestarget) {
		this.o_annualsalestarget = o_annualsalestarget;
	}

	public String getO_address1() {
		return o_address1;
	}

	public void setO_address1(String o_address1) {
		this.o_address1 = o_address1;
	}

	public String getO_city() {
		return o_city;
	}

	public void setO_city(String o_city) {
		this.o_city = o_city;
	}

	public String getO_state() {
		return o_state;
	}

	public void setO_state(String o_state) {
		this.o_state = o_state;
	}

	public String getO_country() {
		return o_country;
	}

	public void setO_country(String o_country) {
		this.o_country = o_country;
	}

	public String getO_postcode() {
		return o_postcode;
	}

	public void setO_postcode(String o_postcode) {
		this.o_postcode = o_postcode;
	}

	public String getO_annualreferalid() {
		return o_annualreferalid;
	}

	public void setO_annualreferalid(String o_annualreferalid) {
		this.o_annualreferalid = o_annualreferalid;
	}

}
