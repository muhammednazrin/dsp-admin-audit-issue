package com.etiqa.dsp.dao.common.pojo;

public class Audit_Payment_Bean {

	private String invoiceno;
	private String o_invoiceno;
	private String discountcode;

	private String pymt_gateway_code;
	private String o_pymt_gateway_code;

	private String transactiondatettime;
	private String o_transactiondatettime;

	private String amount;
	private String o_amount;

	private String paymentstatus;
	private String o_paymentstatus;

	private String transactionstatus;
	private String o_transactionstatus;

	private String remarks;
	private String who;
	private String datewhen;
	private String recordcomment;

	public String getPymt_gateway_code() {
		return pymt_gateway_code;
	}

	public void setPymt_gateway_code(String pymt_gateway_code) {
		this.pymt_gateway_code = pymt_gateway_code;
	}

	public String getO_pymt_gateway_code() {
		return o_pymt_gateway_code;
	}

	public void setO_pymt_gateway_code(String o_pymt_gateway_code) {
		this.o_pymt_gateway_code = o_pymt_gateway_code;
	}

	public String getTransactiondatettime() {
		return transactiondatettime;
	}

	public void setTransactiondatettime(String transactiondatettime) {
		this.transactiondatettime = transactiondatettime;
	}

	public String getO_transactiondatettime() {
		return o_transactiondatettime;
	}

	public void setO_transactiondatettime(String o_transactiondatettime) {
		this.o_transactiondatettime = o_transactiondatettime;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getO_amount() {
		return o_amount;
	}

	public void setO_amount(String o_amount) {
		this.o_amount = o_amount;
	}

	public String getPaymentstatus() {
		return paymentstatus;
	}

	public void setPaymentstatus(String paymentstatus) {
		this.paymentstatus = paymentstatus;
	}

	public String getO_paymentstatus() {
		return o_paymentstatus;
	}

	public void setO_paymentstatus(String o_paymentstatus) {
		this.o_paymentstatus = o_paymentstatus;
	}

	public String getTransactionstatus() {
		return transactionstatus;
	}

	public void setTransactionstatus(String transactionstatus) {
		this.transactionstatus = transactionstatus;
	}

	public String getO_transactionstatus() {
		return o_transactionstatus;
	}

	public void setO_transactionstatus(String o_transactionstatus) {
		this.o_transactionstatus = o_transactionstatus;
	}

	public String getWho() {
		return who;
	}

	public void setWho(String who) {
		this.who = who;
	}

	public String getDatewhen() {
		return datewhen;
	}

	public void setDatewhen(String datewhen) {
		this.datewhen = datewhen;
	}

	public String getRecordcomment() {
		return recordcomment;
	}

	public void setRecordcomment(String recordcomment) {
		this.recordcomment = recordcomment;
	}

	public String getInvoiceno() {
		return invoiceno;
	}

	public void setInvoiceno(String invoiceno) {
		this.invoiceno = invoiceno;
	}

	public String getDiscountcode() {
		return discountcode;
	}

	public void setDiscountcode(String discountcode) {
		this.discountcode = discountcode;
	}

	public String getO_invoiceno() {
		return o_invoiceno;
	}

	public void setO_invoiceno(String o_invoiceno) {
		this.o_invoiceno = o_invoiceno;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
