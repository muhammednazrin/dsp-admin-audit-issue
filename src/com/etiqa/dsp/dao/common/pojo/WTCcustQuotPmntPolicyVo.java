package com.etiqa.dsp.dao.common.pojo;

public class WTCcustQuotPmntPolicyVo {

	private int CUSTOMER_ID;
	private String CUSTOMER_ID_TYPE;
	private String CUSTOMER_NRIC_ID;
	private String CUSTOMER_NAME;
	private String CUSTOMER_DOB;
	private String CUSTOMER_GENDER;
	private String CUSTOMER_RACE;
	private String CUSTOMER_RELIGION;
	private String CUSTOMER_NATIONALITY;
	private String CUSTOMER_TITLE;
	private String CUSTOMER_NATIONALITY_RACE;
	private String CUSTOMER_EDU_LEVEL;
	private String CUSTOMER_MARITALSTATUS;
	private String CUSTOMER_SALARY_RANGE;
	private String CUSTOMER_OCCUPATION;
	private String CUSTOMER_ADDRESS1;
	private String CUSTOMER_ADDRESS2;
	private String CUSTOMER_ADDRESS3;
	private String CUSTOMER_POSTCODE;
	private String CUSTOMER_STATE;
	private String CUSTOMER_COUNTRY;
	private String CUSTOMER_MAIL_ADDRESS1;
	private String CUSTOMER_MAIL_ADDRESS2;
	private String CUSTOMER_MAIL_ADDRESS3;
	private String CUSTOMER_MAIL_POSTCODE;
	private String CUSTOMER_MAIL_STATE;
	private String CUSTOMER_MAIL_COUNTRY;
	private int CUSTOMER_NO_CHILDREN;
	private String CUSTOMER_MOBILE_NO;
	private String CUSTOMER_EMAIL;
	private String LEADS_FLAG;
	private String SALES_TOOL;
	private String EMAIL_SENT_COUNTER;
	private int QQ_ID;
	private String CREATE_DATE;
	private String UPDATED_DATE;
	private String CUSTOMER_EMPLOYER;
	private String CUSTOMER_CLIENTTYPE;
	private String CUSTOMER_INDUSTRY;
	private int DSP_QQ_ID;
	private String PRODUCT_CODE;
	private String CREATED_DATE;
	private int ID;
	private String POLICY_NUMBER;
	private String PRODUCT_PLAN_CODE;
	private String POLICY_EFFECTIVE_TIMESTAMP;
	private int SUM_INSURED;
	private String POLICY_STATUS;
	private String POLICY_TYPE_CODE;
	private String POLICY_EXPIRY_TIMESTAMP;
	private int TRANSACTION_ID;
	private String CREATION_TIMESTAMP;

	// ---------------------payment columns--------------------

	// private String TRANSACTION_ID;
	// private String DSP_QQ_ID;

	private String PMNT_GATEWAY_CODE;
	private String DISCOUNT_CODE;
	private String PROMO_CODE;
	private String AMOUNT;
	private String PMNT_STATUS;
	private String INVOICE_NO;
	private String TRANSACTION_DATETIME;
	private String TRANSACTION_STATUS;

	// ---------------------payment columns ends --------------------

	private String WTC_QQ_ID;
	// private String DSP_QQ_ID,
	private String LEADS_EMAIL_ID;
	private String PRODUCT_ID;
	private String SUB_PRODUCT_ID;
	private String TRAVEL_AREA_TYPE;
	private String COUNTRY_1;
	private String COUNTRY_2;
	private String COUNTRY_3;
	private String COUNTRY_4;
	private String COUNTRY_5;
	private String TRAVLLING_WITH;
	private String TRIP_TYPE;
	private String TRAVEL_START_DATE;
	private String TRAVEL_END_DATE;
	private String TRAVEL_DURATION;
	private String FULL_NAME;
	private String OFFERED_PLAN_NAME;
	private String INS_REJECTED_DECLERATION;
	private String PROMOTION_CODE;
	private String PROMOTION_AMOUNT;
	private String DISCOUNT_ID;
	private String DISCOUNT_AMOUNT;
	private String WT_POLICY_SUM_INSURED;
	private String WT_POLICY_PREMIUM;
	private String WT_GROSS_PREMIUM;
	private String GROSS_PREMIUM_GST;
	private String GROSS_PREMIUM_STAMP_DUTY;
	private String TOTAL_PREMIUM_PAID;
	private String ETIQA_PDPA_ACCEPT;
	private String IMP_NOTICE_DECLERATION_PDA;
	private String IS_PURCHASED;
	private String ET_WC_DSP_CUSTOMER_CUSTOMER_ID;
	private String PLAN_CODE;

	private String langValue;

	public String getLangValue() {
		return langValue;
	}

	public void setLangValue(String langValue) {
		this.langValue = langValue;
	}

	public int getCUSTOMER_ID() {
		return CUSTOMER_ID;
	}

	public void setCUSTOMER_ID(int cUSTOMER_ID) {
		CUSTOMER_ID = cUSTOMER_ID;
	}

	public String getCUSTOMER_ID_TYPE() {
		return CUSTOMER_ID_TYPE;
	}

	public void setCUSTOMER_ID_TYPE(String cUSTOMER_ID_TYPE) {
		CUSTOMER_ID_TYPE = cUSTOMER_ID_TYPE;
	}

	public String getCUSTOMER_NRIC_ID() {
		return CUSTOMER_NRIC_ID;
	}

	public void setCUSTOMER_NRIC_ID(String cUSTOMER_NRIC_ID) {
		CUSTOMER_NRIC_ID = cUSTOMER_NRIC_ID;
	}

	public String getCUSTOMER_NAME() {
		return CUSTOMER_NAME;
	}

	public void setCUSTOMER_NAME(String cUSTOMER_NAME) {
		CUSTOMER_NAME = cUSTOMER_NAME;
	}

	public String getCUSTOMER_DOB() {
		return CUSTOMER_DOB;
	}

	public void setCUSTOMER_DOB(String cUSTOMER_DOB) {
		CUSTOMER_DOB = cUSTOMER_DOB;
	}

	public String getCUSTOMER_GENDER() {
		return CUSTOMER_GENDER;
	}

	public void setCUSTOMER_GENDER(String cUSTOMER_GENDER) {
		CUSTOMER_GENDER = cUSTOMER_GENDER;
	}

	public String getCUSTOMER_RACE() {
		return CUSTOMER_RACE;
	}

	public void setCUSTOMER_RACE(String cUSTOMER_RACE) {
		CUSTOMER_RACE = cUSTOMER_RACE;
	}

	public String getCUSTOMER_RELIGION() {
		return CUSTOMER_RELIGION;
	}

	public void setCUSTOMER_RELIGION(String cUSTOMER_RELIGION) {
		CUSTOMER_RELIGION = cUSTOMER_RELIGION;
	}

	public String getCUSTOMER_NATIONALITY() {
		return CUSTOMER_NATIONALITY;
	}

	public void setCUSTOMER_NATIONALITY(String cUSTOMER_NATIONALITY) {
		CUSTOMER_NATIONALITY = cUSTOMER_NATIONALITY;
	}

	public String getCUSTOMER_TITLE() {
		return CUSTOMER_TITLE;
	}

	public void setCUSTOMER_TITLE(String cUSTOMER_TITLE) {
		CUSTOMER_TITLE = cUSTOMER_TITLE;
	}

	public String getCUSTOMER_NATIONALITY_RACE() {
		return CUSTOMER_NATIONALITY_RACE;
	}

	public void setCUSTOMER_NATIONALITY_RACE(String cUSTOMER_NATIONALITY_RACE) {
		CUSTOMER_NATIONALITY_RACE = cUSTOMER_NATIONALITY_RACE;
	}

	public String getCUSTOMER_EDU_LEVEL() {
		return CUSTOMER_EDU_LEVEL;
	}

	public void setCUSTOMER_EDU_LEVEL(String cUSTOMER_EDU_LEVEL) {
		CUSTOMER_EDU_LEVEL = cUSTOMER_EDU_LEVEL;
	}

	public String getCUSTOMER_MARITALSTATUS() {
		return CUSTOMER_MARITALSTATUS;
	}

	public void setCUSTOMER_MARITALSTATUS(String cUSTOMER_MARITALSTATUS) {
		CUSTOMER_MARITALSTATUS = cUSTOMER_MARITALSTATUS;
	}

	public String getCUSTOMER_SALARY_RANGE() {
		return CUSTOMER_SALARY_RANGE;
	}

	public void setCUSTOMER_SALARY_RANGE(String cUSTOMER_SALARY_RANGE) {
		CUSTOMER_SALARY_RANGE = cUSTOMER_SALARY_RANGE;
	}

	public String getCUSTOMER_OCCUPATION() {
		return CUSTOMER_OCCUPATION;
	}

	public void setCUSTOMER_OCCUPATION(String cUSTOMER_OCCUPATION) {
		CUSTOMER_OCCUPATION = cUSTOMER_OCCUPATION;
	}

	public String getCUSTOMER_ADDRESS1() {
		return CUSTOMER_ADDRESS1;
	}

	public void setCUSTOMER_ADDRESS1(String cUSTOMER_ADDRESS1) {
		CUSTOMER_ADDRESS1 = cUSTOMER_ADDRESS1;
	}

	public String getCUSTOMER_ADDRESS2() {
		return CUSTOMER_ADDRESS2;
	}

	public void setCUSTOMER_ADDRESS2(String cUSTOMER_ADDRESS2) {
		CUSTOMER_ADDRESS2 = cUSTOMER_ADDRESS2;
	}

	public String getCUSTOMER_ADDRESS3() {
		return CUSTOMER_ADDRESS3;
	}

	public void setCUSTOMER_ADDRESS3(String cUSTOMER_ADDRESS3) {
		CUSTOMER_ADDRESS3 = cUSTOMER_ADDRESS3;
	}

	public String getCUSTOMER_POSTCODE() {
		return CUSTOMER_POSTCODE;
	}

	public void setCUSTOMER_POSTCODE(String cUSTOMER_POSTCODE) {
		CUSTOMER_POSTCODE = cUSTOMER_POSTCODE;
	}

	public String getCUSTOMER_STATE() {
		return CUSTOMER_STATE;
	}

	public void setCUSTOMER_STATE(String cUSTOMER_STATE) {
		CUSTOMER_STATE = cUSTOMER_STATE;
	}

	public String getCUSTOMER_COUNTRY() {
		return CUSTOMER_COUNTRY;
	}

	public void setCUSTOMER_COUNTRY(String cUSTOMER_COUNTRY) {
		CUSTOMER_COUNTRY = cUSTOMER_COUNTRY;
	}

	public String getCUSTOMER_MAIL_ADDRESS1() {
		return CUSTOMER_MAIL_ADDRESS1;
	}

	public void setCUSTOMER_MAIL_ADDRESS1(String cUSTOMER_MAIL_ADDRESS1) {
		CUSTOMER_MAIL_ADDRESS1 = cUSTOMER_MAIL_ADDRESS1;
	}

	public String getCUSTOMER_MAIL_ADDRESS2() {
		return CUSTOMER_MAIL_ADDRESS2;
	}

	public void setCUSTOMER_MAIL_ADDRESS2(String cUSTOMER_MAIL_ADDRESS2) {
		CUSTOMER_MAIL_ADDRESS2 = cUSTOMER_MAIL_ADDRESS2;
	}

	public String getCUSTOMER_MAIL_ADDRESS3() {
		return CUSTOMER_MAIL_ADDRESS3;
	}

	public void setCUSTOMER_MAIL_ADDRESS3(String cUSTOMER_MAIL_ADDRESS3) {
		CUSTOMER_MAIL_ADDRESS3 = cUSTOMER_MAIL_ADDRESS3;
	}

	public String getCUSTOMER_MAIL_POSTCODE() {
		return CUSTOMER_MAIL_POSTCODE;
	}

	public void setCUSTOMER_MAIL_POSTCODE(String cUSTOMER_MAIL_POSTCODE) {
		CUSTOMER_MAIL_POSTCODE = cUSTOMER_MAIL_POSTCODE;
	}

	public String getCUSTOMER_MAIL_STATE() {
		return CUSTOMER_MAIL_STATE;
	}

	public void setCUSTOMER_MAIL_STATE(String cUSTOMER_MAIL_STATE) {
		CUSTOMER_MAIL_STATE = cUSTOMER_MAIL_STATE;
	}

	public String getCUSTOMER_MAIL_COUNTRY() {
		return CUSTOMER_MAIL_COUNTRY;
	}

	public void setCUSTOMER_MAIL_COUNTRY(String cUSTOMER_MAIL_COUNTRY) {
		CUSTOMER_MAIL_COUNTRY = cUSTOMER_MAIL_COUNTRY;
	}

	public int getCUSTOMER_NO_CHILDREN() {
		return CUSTOMER_NO_CHILDREN;
	}

	public void setCUSTOMER_NO_CHILDREN(int cUSTOMER_NO_CHILDREN) {
		CUSTOMER_NO_CHILDREN = cUSTOMER_NO_CHILDREN;
	}

	public String getCUSTOMER_MOBILE_NO() {
		return CUSTOMER_MOBILE_NO;
	}

	public void setCUSTOMER_MOBILE_NO(String cUSTOMER_MOBILE_NO) {
		CUSTOMER_MOBILE_NO = cUSTOMER_MOBILE_NO;
	}

	public String getCUSTOMER_EMAIL() {
		return CUSTOMER_EMAIL;
	}

	public void setCUSTOMER_EMAIL(String cUSTOMER_EMAIL) {
		CUSTOMER_EMAIL = cUSTOMER_EMAIL;
	}

	public String getLEADS_FLAG() {
		return LEADS_FLAG;
	}

	public void setLEADS_FLAG(String lEADS_FLAG) {
		LEADS_FLAG = lEADS_FLAG;
	}

	public String getSALES_TOOL() {
		return SALES_TOOL;
	}

	public void setSALES_TOOL(String sALES_TOOL) {
		SALES_TOOL = sALES_TOOL;
	}

	public String getEMAIL_SENT_COUNTER() {
		return EMAIL_SENT_COUNTER;
	}

	public void setEMAIL_SENT_COUNTER(String eMAIL_SENT_COUNTER) {
		EMAIL_SENT_COUNTER = eMAIL_SENT_COUNTER;
	}

	public int getQQ_ID() {
		return QQ_ID;
	}

	public void setQQ_ID(int qQ_ID) {
		QQ_ID = qQ_ID;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getUPDATED_DATE() {
		return UPDATED_DATE;
	}

	public void setUPDATED_DATE(String uPDATED_DATE) {
		UPDATED_DATE = uPDATED_DATE;
	}

	public String getCUSTOMER_EMPLOYER() {
		return CUSTOMER_EMPLOYER;
	}

	public void setCUSTOMER_EMPLOYER(String cUSTOMER_EMPLOYER) {
		CUSTOMER_EMPLOYER = cUSTOMER_EMPLOYER;
	}

	public String getCUSTOMER_CLIENTTYPE() {
		return CUSTOMER_CLIENTTYPE;
	}

	public void setCUSTOMER_CLIENTTYPE(String cUSTOMER_CLIENTTYPE) {
		CUSTOMER_CLIENTTYPE = cUSTOMER_CLIENTTYPE;
	}

	public String getCUSTOMER_INDUSTRY() {
		return CUSTOMER_INDUSTRY;
	}

	public void setCUSTOMER_INDUSTRY(String cUSTOMER_INDUSTRY) {
		CUSTOMER_INDUSTRY = cUSTOMER_INDUSTRY;
	}

	public int getDSP_QQ_ID() {
		return DSP_QQ_ID;
	}

	public void setDSP_QQ_ID(int dSP_QQ_ID) {
		DSP_QQ_ID = dSP_QQ_ID;
	}

	public String getPRODUCT_CODE() {
		return PRODUCT_CODE;
	}

	public void setPRODUCT_CODE(String pRODUCT_CODE) {
		PRODUCT_CODE = pRODUCT_CODE;
	}

	public String getCREATED_DATE() {
		return CREATED_DATE;
	}

	public void setCREATED_DATE(String cREATED_DATE) {
		CREATED_DATE = cREATED_DATE;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getPOLICY_NUMBER() {
		return POLICY_NUMBER;
	}

	public void setPOLICY_NUMBER(String pOLICY_NUMBER) {
		POLICY_NUMBER = pOLICY_NUMBER;
	}

	public String getPRODUCT_PLAN_CODE() {
		return PRODUCT_PLAN_CODE;
	}

	public void setPRODUCT_PLAN_CODE(String pRODUCT_PLAN_CODE) {
		PRODUCT_PLAN_CODE = pRODUCT_PLAN_CODE;
	}

	public String getPOLICY_EFFECTIVE_TIMESTAMP() {
		return POLICY_EFFECTIVE_TIMESTAMP;
	}

	public void setPOLICY_EFFECTIVE_TIMESTAMP(String pOLICY_EFFECTIVE_TIMESTAMP) {
		POLICY_EFFECTIVE_TIMESTAMP = pOLICY_EFFECTIVE_TIMESTAMP;
	}

	public int getSUM_INSURED() {
		return SUM_INSURED;
	}

	public void setSUM_INSURED(int sUM_INSURED) {
		SUM_INSURED = sUM_INSURED;
	}

	public String getPOLICY_STATUS() {
		return POLICY_STATUS;
	}

	public void setPOLICY_STATUS(String pOLICY_STATUS) {
		POLICY_STATUS = pOLICY_STATUS;
	}

	public String getPOLICY_TYPE_CODE() {
		return POLICY_TYPE_CODE;
	}

	public void setPOLICY_TYPE_CODE(String pOLICY_TYPE_CODE) {
		POLICY_TYPE_CODE = pOLICY_TYPE_CODE;
	}

	public String getPOLICY_EXPIRY_TIMESTAMP() {
		return POLICY_EXPIRY_TIMESTAMP;
	}

	public void setPOLICY_EXPIRY_TIMESTAMP(String pOLICY_EXPIRY_TIMESTAMP) {
		POLICY_EXPIRY_TIMESTAMP = pOLICY_EXPIRY_TIMESTAMP;
	}

	public int getTRANSACTION_ID() {
		return TRANSACTION_ID;
	}

	public void setTRANSACTION_ID(int tRANSACTION_ID) {
		TRANSACTION_ID = tRANSACTION_ID;
	}

	public String getCREATION_TIMESTAMP() {
		return CREATION_TIMESTAMP;
	}

	public void setCREATION_TIMESTAMP(String cREATION_TIMESTAMP) {
		CREATION_TIMESTAMP = cREATION_TIMESTAMP;
	}

	public String getWTC_QQ_ID() {
		return WTC_QQ_ID;
	}

	public void setWTC_QQ_ID(String wTC_QQ_ID) {
		WTC_QQ_ID = wTC_QQ_ID;
	}

	public String getLEADS_EMAIL_ID() {
		return LEADS_EMAIL_ID;
	}

	public void setLEADS_EMAIL_ID(String lEADS_EMAIL_ID) {
		LEADS_EMAIL_ID = lEADS_EMAIL_ID;
	}

	public String getPRODUCT_ID() {
		return PRODUCT_ID;
	}

	public void setPRODUCT_ID(String pRODUCT_ID) {
		PRODUCT_ID = pRODUCT_ID;
	}

	public String getSUB_PRODUCT_ID() {
		return SUB_PRODUCT_ID;
	}

	public void setSUB_PRODUCT_ID(String sUB_PRODUCT_ID) {
		SUB_PRODUCT_ID = sUB_PRODUCT_ID;
	}

	public String getTRAVEL_AREA_TYPE() {
		return TRAVEL_AREA_TYPE;
	}

	public void setTRAVEL_AREA_TYPE(String tRAVEL_AREA_TYPE) {
		TRAVEL_AREA_TYPE = tRAVEL_AREA_TYPE;
	}

	public String getCOUNTRY_1() {
		return COUNTRY_1;
	}

	public void setCOUNTRY_1(String cOUNTRY_1) {
		COUNTRY_1 = cOUNTRY_1;
	}

	public String getCOUNTRY_2() {
		return COUNTRY_2;
	}

	public void setCOUNTRY_2(String cOUNTRY_2) {
		COUNTRY_2 = cOUNTRY_2;
	}

	public String getCOUNTRY_3() {
		return COUNTRY_3;
	}

	public void setCOUNTRY_3(String cOUNTRY_3) {
		COUNTRY_3 = cOUNTRY_3;
	}

	public String getCOUNTRY_4() {
		return COUNTRY_4;
	}

	public void setCOUNTRY_4(String cOUNTRY_4) {
		COUNTRY_4 = cOUNTRY_4;
	}

	public String getCOUNTRY_5() {
		return COUNTRY_5;
	}

	public void setCOUNTRY_5(String cOUNTRY_5) {
		COUNTRY_5 = cOUNTRY_5;
	}

	public String getTRAVLLING_WITH() {
		return TRAVLLING_WITH;
	}

	public void setTRAVLLING_WITH(String tRAVLLING_WITH) {
		TRAVLLING_WITH = tRAVLLING_WITH;
	}

	public String getTRIP_TYPE() {
		return TRIP_TYPE;
	}

	public void setTRIP_TYPE(String tRIP_TYPE) {
		TRIP_TYPE = tRIP_TYPE;
	}

	public String getTRAVEL_START_DATE() {
		return TRAVEL_START_DATE;
	}

	public void setTRAVEL_START_DATE(String tRAVEL_START_DATE) {
		TRAVEL_START_DATE = tRAVEL_START_DATE;
	}

	public String getTRAVEL_END_DATE() {
		return TRAVEL_END_DATE;
	}

	public void setTRAVEL_END_DATE(String tRAVEL_END_DATE) {
		TRAVEL_END_DATE = tRAVEL_END_DATE;
	}

	public String getTRAVEL_DURATION() {
		return TRAVEL_DURATION;
	}

	public void setTRAVEL_DURATION(String tRAVEL_DURATION) {
		TRAVEL_DURATION = tRAVEL_DURATION;
	}

	public String getFULL_NAME() {
		return FULL_NAME;
	}

	public void setFULL_NAME(String fULL_NAME) {
		FULL_NAME = fULL_NAME;
	}

	public String getOFFERED_PLAN_NAME() {
		return OFFERED_PLAN_NAME;
	}

	public void setOFFERED_PLAN_NAME(String oFFERED_PLAN_NAME) {
		OFFERED_PLAN_NAME = oFFERED_PLAN_NAME;
	}

	public String getINS_REJECTED_DECLERATION() {
		return INS_REJECTED_DECLERATION;
	}

	public void setINS_REJECTED_DECLERATION(String iNS_REJECTED_DECLERATION) {
		INS_REJECTED_DECLERATION = iNS_REJECTED_DECLERATION;
	}

	public String getPROMOTION_CODE() {
		return PROMOTION_CODE;
	}

	public void setPROMOTION_CODE(String pROMOTION_CODE) {
		PROMOTION_CODE = pROMOTION_CODE;
	}

	public String getPROMOTION_AMOUNT() {
		return PROMOTION_AMOUNT;
	}

	public void setPROMOTION_AMOUNT(String pROMOTION_AMOUNT) {
		PROMOTION_AMOUNT = pROMOTION_AMOUNT;
	}

	public String getDISCOUNT_ID() {
		return DISCOUNT_ID;
	}

	public void setDISCOUNT_ID(String dISCOUNT_ID) {
		DISCOUNT_ID = dISCOUNT_ID;
	}

	public String getDISCOUNT_AMOUNT() {
		return DISCOUNT_AMOUNT;
	}

	public void setDISCOUNT_AMOUNT(String dISCOUNT_AMOUNT) {
		DISCOUNT_AMOUNT = dISCOUNT_AMOUNT;
	}

	public String getWT_POLICY_SUM_INSURED() {
		return WT_POLICY_SUM_INSURED;
	}

	public void setWT_POLICY_SUM_INSURED(String wT_POLICY_SUM_INSURED) {
		WT_POLICY_SUM_INSURED = wT_POLICY_SUM_INSURED;
	}

	public String getWT_POLICY_PREMIUM() {
		return WT_POLICY_PREMIUM;
	}

	public void setWT_POLICY_PREMIUM(String wT_POLICY_PREMIUM) {
		WT_POLICY_PREMIUM = wT_POLICY_PREMIUM;
	}

	public String getWT_GROSS_PREMIUM() {
		return WT_GROSS_PREMIUM;
	}

	public void setWT_GROSS_PREMIUM(String wT_GROSS_PREMIUM) {
		WT_GROSS_PREMIUM = wT_GROSS_PREMIUM;
	}

	public String getGROSS_PREMIUM_GST() {
		return GROSS_PREMIUM_GST;
	}

	public void setGROSS_PREMIUM_GST(String gROSS_PREMIUM_GST) {
		GROSS_PREMIUM_GST = gROSS_PREMIUM_GST;
	}

	public String getGROSS_PREMIUM_STAMP_DUTY() {
		return GROSS_PREMIUM_STAMP_DUTY;
	}

	public void setGROSS_PREMIUM_STAMP_DUTY(String gROSS_PREMIUM_STAMP_DUTY) {
		GROSS_PREMIUM_STAMP_DUTY = gROSS_PREMIUM_STAMP_DUTY;
	}

	public String getTOTAL_PREMIUM_PAID() {
		return TOTAL_PREMIUM_PAID;
	}

	public void setTOTAL_PREMIUM_PAID(String tOTAL_PREMIUM_PAID) {
		TOTAL_PREMIUM_PAID = tOTAL_PREMIUM_PAID;
	}

	public String getETIQA_PDPA_ACCEPT() {
		return ETIQA_PDPA_ACCEPT;
	}

	public void setETIQA_PDPA_ACCEPT(String eTIQA_PDPA_ACCEPT) {
		ETIQA_PDPA_ACCEPT = eTIQA_PDPA_ACCEPT;
	}

	public String getIMP_NOTICE_DECLERATION_PDA() {
		return IMP_NOTICE_DECLERATION_PDA;
	}

	public void setIMP_NOTICE_DECLERATION_PDA(String iMP_NOTICE_DECLERATION_PDA) {
		IMP_NOTICE_DECLERATION_PDA = iMP_NOTICE_DECLERATION_PDA;
	}

	public String getIS_PURCHASED() {
		return IS_PURCHASED;
	}

	public void setIS_PURCHASED(String iS_PURCHASED) {
		IS_PURCHASED = iS_PURCHASED;
	}

	public String getET_WC_DSP_CUSTOMER_CUSTOMER_ID() {
		return ET_WC_DSP_CUSTOMER_CUSTOMER_ID;
	}

	public void setET_WC_DSP_CUSTOMER_CUSTOMER_ID(String eT_WC_DSP_CUSTOMER_CUSTOMER_ID) {
		ET_WC_DSP_CUSTOMER_CUSTOMER_ID = eT_WC_DSP_CUSTOMER_CUSTOMER_ID;
	}

	public String getPLAN_CODE() {
		return PLAN_CODE;
	}

	public void setPLAN_CODE(String pLAN_CODE) {
		PLAN_CODE = pLAN_CODE;
	}

	public String getPMNT_GATEWAY_CODE() {
		return PMNT_GATEWAY_CODE;
	}

	public void setPMNT_GATEWAY_CODE(String pMNT_GATEWAY_CODE) {
		PMNT_GATEWAY_CODE = pMNT_GATEWAY_CODE;
	}

	public String getDISCOUNT_CODE() {
		return DISCOUNT_CODE;
	}

	public void setDISCOUNT_CODE(String dISCOUNT_CODE) {
		DISCOUNT_CODE = dISCOUNT_CODE;
	}

	public String getPROMO_CODE() {
		return PROMO_CODE;
	}

	public void setPROMO_CODE(String pROMO_CODE) {
		PROMO_CODE = pROMO_CODE;
	}

	public String getAMOUNT() {
		return AMOUNT;
	}

	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}

	public String getPMNT_STATUS() {
		return PMNT_STATUS;
	}

	public void setPMNT_STATUS(String pMNT_STATUS) {
		PMNT_STATUS = pMNT_STATUS;
	}

	public String getINVOICE_NO() {
		return INVOICE_NO;
	}

	public void setINVOICE_NO(String iNVOICE_NO) {
		INVOICE_NO = iNVOICE_NO;
	}

	public String getTRANSACTION_DATETIME() {
		return TRANSACTION_DATETIME;
	}

	public void setTRANSACTION_DATETIME(String tRANSACTION_DATETIME) {
		TRANSACTION_DATETIME = tRANSACTION_DATETIME;
	}

	public String getTRANSACTION_STATUS() {
		return TRANSACTION_STATUS;
	}

	public void setTRANSACTION_STATUS(String tRANSACTION_STATUS) {
		TRANSACTION_STATUS = tRANSACTION_STATUS;
	}

	public void setsName(String string) {
		// TODO Auto-generated method stub

	}

	public void setsDob(String string) {
		// TODO Auto-generated method stub

	}

	public void setsIdno(String string) {
		// TODO Auto-generated method stub

	}

	public void setsGender(String string) {
		// TODO Auto-generated method stub

	}

	public void setsRelationShip(String string) {
		// TODO Auto-generated method stub

	}

	public void setcName(String string) {
		// TODO Auto-generated method stub

	}

	public void setcDob(String string) {
		// TODO Auto-generated method stub

	}

	public void setcIdno(String string) {
		// TODO Auto-generated method stub

	}

	public void setcGender(String string) {
		// TODO Auto-generated method stub

	}

	public void setcRelationShip(String string) {
		// TODO Auto-generated method stub

	}

}
