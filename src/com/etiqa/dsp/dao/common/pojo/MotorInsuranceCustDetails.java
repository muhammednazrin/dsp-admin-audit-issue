package com.etiqa.dsp.dao.common.pojo;

public class MotorInsuranceCustDetails {
	private String CUSTOMER_ID;
	private String CUSTOMER_ID_TYPE;
	private String CUSTOMER_NRIC_ID;
	private String CUSTOMER_NAME;
	private String CUSTOMER_DOB;
	private String CUSTOMER_GENDER;
	private String CUSTOMER_RACE;
	private String CUSTOMER_RELIGION;
	private String CUSTOMER_NATIONALITY;
	private String CUSTOMER_TITLE;
	private String CUSTOMER_NATIONALITY_RACE;
	private String CUSTOMER_EDU_LEVEL;
	private String CUSTOMER_MARITALSTATUS;
	private String CUSTOMER_SALARY_RANGE;
	private String CUSTOMER_OCCUPATION;
	private String CUSTOMER_ADDRESS1;
	private String CUSTOMER_ADDRESS2;
	private String CUSTOMER_ADDRESS3;
	private String CUSTOMER_POSTCODE;
	private String CUSTOMER_STATE;
	private String CUSTOMER_COUNTRY;
	private String CUSTOMER_MAIL_ADDRESS1;
	private String CUSTOMER_MAIL_ADDRESS2;
	private String CUSTOMER_MAIL_ADDRESS3;
	private String CUSTOMER_MAIL_POSTCODE;
	private String CUSTOMER_MAIL_STATE;
	private String CUSTOMER_MAIL_COUNTRY;
	private String CUSTOMER_NO_CHILDREN;
	private String CUSTOMER_MOBILE_NO;
	private String CUSTOMER_EMAIL;
	private String LEADS_FLAG;
	private String SALES_TOOL;
	private String EMAIL_SENT_COUNTER;
	private String QQ_ID;
	private String CREATE_DATE;
	private String UPDATED_DATE;
	private String CUSTOMER_EMPLOYER;
	private String CUSTOMER_CLIENTTYPE;
	private String CUSTOMER_INDUSTRY;
	private String ID;
	private String POLICY_NUMBER;
	private String PRODUCT_CODE;
	private String PRODUCT_PLAN_CODE;
	private String POLICY_EFFECTIVE_TIMESTAMP;
	private String SUM_INSURED;
	private String POLICY_STATUS;
	private String POLICY_TYPE_CODE;
	private String POLICY_EXPIRY_TIMESTAMP;
	private String TRANSACTION_ID;
	private String DSP_QQ_ID;
	private String CREATION_TIMESTAMP;
	// private String QQ_ID;
	// private String DSP_QQ_ID;,
	private String LEADS_EMAIL_ID;
	// private String PRODUCT_CODE;
	private String REGISTRATION_NUMBER;
	private String VEHICLE_LOCATION;
	private String PREVIOUS_INSURANCE_COMPANY;
	private String BMS_QUESTION_ANS;
	private String VEHICLE_MAKE;
	private String VEHICLE_MODEL;
	private String YEAR_MAKE;
	private String VEHICLE_VARIANT;
	private String FUEL_TYPE;
	private String MARKET_VALUE;
	private String ENGINE_NUMBER;
	private String CHASSIS_NUMBER;
	private String FINANCED_BY;
	private String NVIC_CODE;
	private String PASSENGER_PA_SELECTED;
	private String TPCD_PPA_QUESTION_ANS;
	private String POLICY_SUM_INSURED;
	private String PREMIUM_ON_SUM_INSURED;
	private String DISCOUNT_CODE;
	private String GST_ON_POLICY_PREMIUM;
	private String STAMP_DUTY_POLICY_PREMIUM;
	private String PASSANGER_PA_PREMIUM;
	private String GST_PASSANGER_PA_PREMIUM;
	private String STAMP_DUTY_PASSANGER_PA_DIRECT;
	private String PASSANGER_PA_PREMIUM_PAYABLE;
	private String TOTAL_PREMIUM_PAYABLE;
	private String PARKING_MAIL_ADDRESS_1;
	private String PARKING_MAIL_ADDRESS_2;
	private String PARKING_MAIL_POSTCODE;
	private String PARKING_MAIL_STATE;
	private String VEHICLE_PARK_OVERNEIGHT;
	private String VEHICLE_ANTI_THEFT;
	private String VEHICLE_SAFTEY;
	private String PDS_QUESTION_ANS;
	private String IND_QUESTION_ANS;
	private String PDPA_QUESTION_ANS;
	private String NON_CLAIM_DISCOUNT;
	private String EXCESS;
	private String COVERAGE_START_DATE;
	private String COVERAGE_END_DATE;
	private String AGENT_CODE;
	private String OPERATOR_CODE;
	private String IS_PURCHASED;
	private String QUOTATION_STATUS;
	private String LEAD_STATUS;
	private String QUOTATION_TYPE;
	private String QUOTATION_CREATION_DATETIME;
	private String CUSTOMER_IC;
	private String NO_OF_DRIVERS;
	private String QUOTATION_UPDATE_DATETIME;
	private String LOADING;
	private String DISCOUNT_AMOUNT;
	private String PA_ADDRESS_CHECK;
	private String FULL_NAME;
	// private String CUSTOMER_ID;
	private String POLICY_ELAPSE;
	private String ADDITINAL_COVERAGE_PREMIUM;
	// QUOTATION_NO
	private String ADDITINAL_CODES;
	private String ADDITINAL_SUMCOVERED;
	private String PREMIUM_AFTER_DISCOUNT;
	private String NCD_AMOUNT;
	private String PREMIUM_AFTER_GST;
	private String GROSSPREMIUM_FINAL;
	private String PASSENGER_PREMIUM_AFTDIS;
	private String BASIC_PREMIUM;
	private String LOADING_PERCENTAGE;
	// private String ID;
	// private String NVIC_CODE;
	private String NVIC_GROUP;
	private String MM_CODE;
	private String MAKE_CODE;
	private String MODEL_CODE;
	private String MAKE;
	private String MODEL;
	private String VARIANT;
	private String SERIES;
	private String YEAR;
	private String DESCRIPTION;
	private String ENGINE;
	private String STYLE;
	private String TRANSMISSION;
	private String DRIVEN_WHEEL;
	private String SEAT;
	private String CC;
	// private String FUEL_TYPE ;
	private String CLASS_CODE;
	private String WD4_FLG;
	private String HP_FLG;
	private String MARK_UP_PER;
	private String MARK_DOWN_PER;
	private String TOTAL_SUM;
	private String MARKET_VALUE1;
	private String MARKET_VALUE2;
	private String MARKET_VALUE3;
	private String WINDSCREEN_VALUE1;
	private String WINDSCREEN_VALUE2;
	private String WINDSCREEN_VALUE3;
	private String EFFECTIVE_DATE;

	private String INVOICE_NO;
	private String Vehicle_Type;

	// ******************* For Driver Info *******************************
	private String DFullName;
	private String DNewIc;
	private String DDOB;
	private String DGender;
	private String DMartialStatus;
	private String DOccupation;
	private String DRelationtoInsured;
	private String DrivingExp;
	private String DDrivingLicense;
	private String DNoofFaultClaim;
	private String DTotalAmntOfClaim;
	private String DMajorTraffic;
	private String DppaPolicyNo;
	// ******************* Additonal Coverage Parameters
	// *******************************
	private String AdditionalCoverageCode;
	private String AdditionalCoverageText;
	private String AdditionalCoverageValue;
	private String AdditionalCoverageSumInsuredValue;
	private String langVal;

	public String getAdditionalCoverageSumInsuredValue() {
		return AdditionalCoverageSumInsuredValue;
	}

	public void setAdditionalCoverageSumInsuredValue(String additionalCoverageSumInsuredValue) {
		AdditionalCoverageSumInsuredValue = additionalCoverageSumInsuredValue;
	}

	public String getLangVal() {
		return langVal;
	}

	public void setLangVal(String langVal) {
		this.langVal = langVal;
	}

	public String getVehicle_Type() {
		return Vehicle_Type;
	}

	public void setVehicle_Type(String vehicle_Type) {
		Vehicle_Type = vehicle_Type;
	}

	public String getDppaPolicyNo() {
		return DppaPolicyNo;
	}

	public void setDppaPolicyNo(String dppaPolicyNo) {
		DppaPolicyNo = dppaPolicyNo;
	}

	public String getAdditionalCoverageCode() {
		return AdditionalCoverageCode;
	}

	public void setAdditionalCoverageCode(String additionalCoverageCode) {
		AdditionalCoverageCode = additionalCoverageCode;
	}

	public String getAdditionalCoverageText() {
		return AdditionalCoverageText;
	}

	public void setAdditionalCoverageText(String additionalCoverageText) {
		AdditionalCoverageText = additionalCoverageText;
	}

	public String getAdditionalCoverageValue() {
		return AdditionalCoverageValue;
	}

	public void setAdditionalCoverageValue(String additionalCoverageValue) {
		AdditionalCoverageValue = additionalCoverageValue;
	}

	public String getINVOICE_NO() {
		return INVOICE_NO;
	}

	public void setINVOICE_NO(String iNVOICE_NO) {
		INVOICE_NO = iNVOICE_NO;
	}

	public String getGROSSPREMIUM_FINAL() {
		return GROSSPREMIUM_FINAL;
	}

	public void setGROSSPREMIUM_FINAL(String gROSSPREMIUM_FINAL) {
		GROSSPREMIUM_FINAL = gROSSPREMIUM_FINAL;
	}

	public String getPASSENGER_PREMIUM_AFTDIS() {
		return PASSENGER_PREMIUM_AFTDIS;
	}

	public void setPASSENGER_PREMIUM_AFTDIS(String pASSENGER_PREMIUM_AFTDIS) {
		PASSENGER_PREMIUM_AFTDIS = pASSENGER_PREMIUM_AFTDIS;
	}

	public String getBASIC_PREMIUM() {
		return BASIC_PREMIUM;
	}

	public void setBASIC_PREMIUM(String bASIC_PREMIUM) {
		BASIC_PREMIUM = bASIC_PREMIUM;
	}

	public String getLOADING_PERCENTAGE() {
		return LOADING_PERCENTAGE;
	}

	public void setLOADING_PERCENTAGE(String lOADING_PERCENTAGE) {
		LOADING_PERCENTAGE = lOADING_PERCENTAGE;
	}

	public String getADDITINAL_CODES() {
		return ADDITINAL_CODES;
	}

	public void setADDITINAL_CODES(String aDDITINAL_CODES) {
		ADDITINAL_CODES = aDDITINAL_CODES;
	}

	public String getADDITINAL_SUMCOVERED() {
		return ADDITINAL_SUMCOVERED;
	}

	public void setADDITINAL_SUMCOVERED(String aDDITINAL_SUMCOVERED) {
		ADDITINAL_SUMCOVERED = aDDITINAL_SUMCOVERED;
	}

	public String getPREMIUM_AFTER_DISCOUNT() {
		return PREMIUM_AFTER_DISCOUNT;
	}

	public void setPREMIUM_AFTER_DISCOUNT(String pREMIUM_AFTER_DISCOUNT) {
		PREMIUM_AFTER_DISCOUNT = pREMIUM_AFTER_DISCOUNT;
	}

	public String getNCD_AMOUNT() {
		return NCD_AMOUNT;
	}

	public void setNCD_AMOUNT(String nCD_AMOUNT) {
		NCD_AMOUNT = nCD_AMOUNT;
	}

	public String getPREMIUM_AFTER_GST() {
		return PREMIUM_AFTER_GST;
	}

	public void setPREMIUM_AFTER_GST(String pREMIUM_AFTER_GST) {
		PREMIUM_AFTER_GST = pREMIUM_AFTER_GST;
	}

	public String getDFullName() {
		return DFullName;
	}

	public void setDFullName(String dFullName) {
		DFullName = dFullName;
	}

	public String getDNewIc() {
		return DNewIc;
	}

	public void setDNewIc(String dNewIc) {
		DNewIc = dNewIc;
	}

	public String getDDOB() {
		return DDOB;
	}

	public void setDDOB(String dDOB) {
		DDOB = dDOB;
	}

	public String getDGender() {
		return DGender;
	}

	public void setDGender(String dGender) {
		DGender = dGender;
	}

	public String getDMartialStatus() {
		return DMartialStatus;
	}

	public void setDMartialStatus(String dMartialStatus) {
		DMartialStatus = dMartialStatus;
	}

	public String getDOccupation() {
		return DOccupation;
	}

	public void setDOccupation(String dOccupation) {
		DOccupation = dOccupation;
	}

	public String getDRelationtoInsured() {
		return DRelationtoInsured;
	}

	public void setDRelationtoInsured(String dRelationtoInsured) {
		DRelationtoInsured = dRelationtoInsured;
	}

	public String getDrivingExp() {
		return DrivingExp;
	}

	public void setDrivingExp(String drivingExp) {
		DrivingExp = drivingExp;
	}

	public String getDDrivingLicense() {
		return DDrivingLicense;
	}

	public void setDDrivingLicense(String dDrivingLicense) {
		DDrivingLicense = dDrivingLicense;
	}

	public String getDNoofFaultClaim() {
		return DNoofFaultClaim;
	}

	public void setDNoofFaultClaim(String dNoofFaultClaim) {
		DNoofFaultClaim = dNoofFaultClaim;
	}

	public String getDTotalAmntOfClaim() {
		return DTotalAmntOfClaim;
	}

	public void setDTotalAmntOfClaim(String dTotalAmntOfClaim) {
		DTotalAmntOfClaim = dTotalAmntOfClaim;
	}

	public String getDMajorTraffic() {
		return DMajorTraffic;
	}

	public void setDMajorTraffic(String dMajorTraffic) {
		DMajorTraffic = dMajorTraffic;
	}

	public String getNVIC_GROUP() {
		return NVIC_GROUP;
	}

	public void setNVIC_GROUP(String nVIC_GROUP) {
		NVIC_GROUP = nVIC_GROUP;
	}

	public String getMM_CODE() {
		return MM_CODE;
	}

	public void setMM_CODE(String mM_CODE) {
		MM_CODE = mM_CODE;
	}

	public String getMAKE_CODE() {
		return MAKE_CODE;
	}

	public void setMAKE_CODE(String mAKE_CODE) {
		MAKE_CODE = mAKE_CODE;
	}

	public String getMODEL_CODE() {
		return MODEL_CODE;
	}

	public void setMODEL_CODE(String mODEL_CODE) {
		MODEL_CODE = mODEL_CODE;
	}

	public String getMAKE() {
		return MAKE;
	}

	public void setMAKE(String mAKE) {
		MAKE = mAKE;
	}

	public String getMODEL() {
		return MODEL;
	}

	public void setMODEL(String mODEL) {
		MODEL = mODEL;
	}

	public String getVARIANT() {
		return VARIANT;
	}

	public void setVARIANT(String vARIANT) {
		VARIANT = vARIANT;
	}

	public String getSERIES() {
		return SERIES;
	}

	public void setSERIES(String sERIES) {
		SERIES = sERIES;
	}

	public String getYEAR() {
		return YEAR;
	}

	public void setYEAR(String yEAR) {
		YEAR = yEAR;
	}

	public String getDESCRIPTION() {
		return DESCRIPTION;
	}

	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}

	public String getENGINE() {
		return ENGINE;
	}

	public void setENGINE(String eNGINE) {
		ENGINE = eNGINE;
	}

	public String getSTYLE() {
		return STYLE;
	}

	public void setSTYLE(String sTYLE) {
		STYLE = sTYLE;
	}

	public String getTRANSMISSION() {
		return TRANSMISSION;
	}

	public void setTRANSMISSION(String tRANSMISSION) {
		TRANSMISSION = tRANSMISSION;
	}

	public String getDRIVEN_WHEEL() {
		return DRIVEN_WHEEL;
	}

	public void setDRIVEN_WHEEL(String dRIVEN_WHEEL) {
		DRIVEN_WHEEL = dRIVEN_WHEEL;
	}

	public String getSEAT() {
		return SEAT;
	}

	public void setSEAT(String sEAT) {
		SEAT = sEAT;
	}

	public String getCC() {
		return CC;
	}

	public void setCC(String cC) {
		CC = cC;
	}

	public String getCLASS_CODE() {
		return CLASS_CODE;
	}

	public void setCLASS_CODE(String cLASS_CODE) {
		CLASS_CODE = cLASS_CODE;
	}

	public String getWD4_FLG() {
		return WD4_FLG;
	}

	public void setWD4_FLG(String wD4_FLG) {
		WD4_FLG = wD4_FLG;
	}

	public String getHP_FLG() {
		return HP_FLG;
	}

	public void setHP_FLG(String hP_FLG) {
		HP_FLG = hP_FLG;
	}

	public String getMARK_UP_PER() {
		return MARK_UP_PER;
	}

	public void setMARK_UP_PER(String mARK_UP_PER) {
		MARK_UP_PER = mARK_UP_PER;
	}

	public String getMARK_DOWN_PER() {
		return MARK_DOWN_PER;
	}

	public void setMARK_DOWN_PER(String mARK_DOWN_PER) {
		MARK_DOWN_PER = mARK_DOWN_PER;
	}

	public String getTOTAL_SUM() {
		return TOTAL_SUM;
	}

	public void setTOTAL_SUM(String tOTAL_SUM) {
		TOTAL_SUM = tOTAL_SUM;
	}

	public String getMARKET_VALUE1() {
		return MARKET_VALUE1;
	}

	public void setMARKET_VALUE1(String mARKET_VALUE1) {
		MARKET_VALUE1 = mARKET_VALUE1;
	}

	public String getMARKET_VALUE2() {
		return MARKET_VALUE2;
	}

	public void setMARKET_VALUE2(String mARKET_VALUE2) {
		MARKET_VALUE2 = mARKET_VALUE2;
	}

	public String getMARKET_VALUE3() {
		return MARKET_VALUE3;
	}

	public void setMARKET_VALUE3(String mARKET_VALUE3) {
		MARKET_VALUE3 = mARKET_VALUE3;
	}

	public String getWINDSCREEN_VALUE1() {
		return WINDSCREEN_VALUE1;
	}

	public void setWINDSCREEN_VALUE1(String wINDSCREEN_VALUE1) {
		WINDSCREEN_VALUE1 = wINDSCREEN_VALUE1;
	}

	public String getWINDSCREEN_VALUE2() {
		return WINDSCREEN_VALUE2;
	}

	public void setWINDSCREEN_VALUE2(String wINDSCREEN_VALUE2) {
		WINDSCREEN_VALUE2 = wINDSCREEN_VALUE2;
	}

	public String getWINDSCREEN_VALUE3() {
		return WINDSCREEN_VALUE3;
	}

	public void setWINDSCREEN_VALUE3(String wINDSCREEN_VALUE3) {
		WINDSCREEN_VALUE3 = wINDSCREEN_VALUE3;
	}

	public String getEFFECTIVE_DATE() {
		return EFFECTIVE_DATE;
	}

	public void setEFFECTIVE_DATE(String eFFECTIVE_DATE) {
		EFFECTIVE_DATE = eFFECTIVE_DATE;
	}

	public String getCUSTOMER_ID() {
		return CUSTOMER_ID;
	}

	public void setCUSTOMER_ID(String cUSTOMER_ID) {
		CUSTOMER_ID = cUSTOMER_ID;
	}

	public String getCUSTOMER_ID_TYPE() {
		return CUSTOMER_ID_TYPE;
	}

	public void setCUSTOMER_ID_TYPE(String cUSTOMER_ID_TYPE) {
		CUSTOMER_ID_TYPE = cUSTOMER_ID_TYPE;
	}

	public String getCUSTOMER_NRIC_ID() {
		return CUSTOMER_NRIC_ID;
	}

	public void setCUSTOMER_NRIC_ID(String cUSTOMER_NRIC_ID) {
		CUSTOMER_NRIC_ID = cUSTOMER_NRIC_ID;
	}

	public String getCUSTOMER_NAME() {
		return CUSTOMER_NAME;
	}

	public void setCUSTOMER_NAME(String cUSTOMER_NAME) {
		CUSTOMER_NAME = cUSTOMER_NAME;
	}

	public String getCUSTOMER_DOB() {
		return CUSTOMER_DOB;
	}

	public void setCUSTOMER_DOB(String cUSTOMER_DOB) {
		CUSTOMER_DOB = cUSTOMER_DOB;
	}

	public String getCUSTOMER_GENDER() {
		return CUSTOMER_GENDER;
	}

	public void setCUSTOMER_GENDER(String cUSTOMER_GENDER) {
		CUSTOMER_GENDER = cUSTOMER_GENDER;
	}

	public String getCUSTOMER_RACE() {
		return CUSTOMER_RACE;
	}

	public void setCUSTOMER_RACE(String cUSTOMER_RACE) {
		CUSTOMER_RACE = cUSTOMER_RACE;
	}

	public String getCUSTOMER_RELIGION() {
		return CUSTOMER_RELIGION;
	}

	public void setCUSTOMER_RELIGION(String cUSTOMER_RELIGION) {
		CUSTOMER_RELIGION = cUSTOMER_RELIGION;
	}

	public String getCUSTOMER_NATIONALITY() {
		return CUSTOMER_NATIONALITY;
	}

	public void setCUSTOMER_NATIONALITY(String cUSTOMER_NATIONALITY) {
		CUSTOMER_NATIONALITY = cUSTOMER_NATIONALITY;
	}

	public String getCUSTOMER_TITLE() {
		return CUSTOMER_TITLE;
	}

	public void setCUSTOMER_TITLE(String cUSTOMER_TITLE) {
		CUSTOMER_TITLE = cUSTOMER_TITLE;
	}

	public String getCUSTOMER_NATIONALITY_RACE() {
		return CUSTOMER_NATIONALITY_RACE;
	}

	public void setCUSTOMER_NATIONALITY_RACE(String cUSTOMER_NATIONALITY_RACE) {
		CUSTOMER_NATIONALITY_RACE = cUSTOMER_NATIONALITY_RACE;
	}

	public String getCUSTOMER_EDU_LEVEL() {
		return CUSTOMER_EDU_LEVEL;
	}

	public void setCUSTOMER_EDU_LEVEL(String cUSTOMER_EDU_LEVEL) {
		CUSTOMER_EDU_LEVEL = cUSTOMER_EDU_LEVEL;
	}

	public String getCUSTOMER_MARITALSTATUS() {
		return CUSTOMER_MARITALSTATUS;
	}

	public void setCUSTOMER_MARITALSTATUS(String cUSTOMER_MARITALSTATUS) {
		CUSTOMER_MARITALSTATUS = cUSTOMER_MARITALSTATUS;
	}

	public String getCUSTOMER_SALARY_RANGE() {
		return CUSTOMER_SALARY_RANGE;
	}

	public void setCUSTOMER_SALARY_RANGE(String cUSTOMER_SALARY_RANGE) {
		CUSTOMER_SALARY_RANGE = cUSTOMER_SALARY_RANGE;
	}

	public String getCUSTOMER_OCCUPATION() {
		return CUSTOMER_OCCUPATION;
	}

	public void setCUSTOMER_OCCUPATION(String cUSTOMER_OCCUPATION) {
		CUSTOMER_OCCUPATION = cUSTOMER_OCCUPATION;
	}

	public String getCUSTOMER_ADDRESS1() {
		return CUSTOMER_ADDRESS1;
	}

	public void setCUSTOMER_ADDRESS1(String cUSTOMER_ADDRESS1) {
		CUSTOMER_ADDRESS1 = cUSTOMER_ADDRESS1;
	}

	public String getCUSTOMER_ADDRESS2() {
		return CUSTOMER_ADDRESS2;
	}

	public void setCUSTOMER_ADDRESS2(String cUSTOMER_ADDRESS2) {
		CUSTOMER_ADDRESS2 = cUSTOMER_ADDRESS2;
	}

	public String getCUSTOMER_ADDRESS3() {
		return CUSTOMER_ADDRESS3;
	}

	public void setCUSTOMER_ADDRESS3(String cUSTOMER_ADDRESS3) {
		CUSTOMER_ADDRESS3 = cUSTOMER_ADDRESS3;
	}

	public String getCUSTOMER_POSTCODE() {
		return CUSTOMER_POSTCODE;
	}

	public void setCUSTOMER_POSTCODE(String cUSTOMER_POSTCODE) {
		CUSTOMER_POSTCODE = cUSTOMER_POSTCODE;
	}

	public String getCUSTOMER_STATE() {
		return CUSTOMER_STATE;
	}

	public void setCUSTOMER_STATE(String cUSTOMER_STATE) {
		CUSTOMER_STATE = cUSTOMER_STATE;
	}

	public String getCUSTOMER_COUNTRY() {
		return CUSTOMER_COUNTRY;
	}

	public void setCUSTOMER_COUNTRY(String cUSTOMER_COUNTRY) {
		CUSTOMER_COUNTRY = cUSTOMER_COUNTRY;
	}

	public String getCUSTOMER_MAIL_ADDRESS1() {
		return CUSTOMER_MAIL_ADDRESS1;
	}

	public void setCUSTOMER_MAIL_ADDRESS1(String cUSTOMER_MAIL_ADDRESS1) {
		CUSTOMER_MAIL_ADDRESS1 = cUSTOMER_MAIL_ADDRESS1;
	}

	public String getCUSTOMER_MAIL_ADDRESS2() {
		return CUSTOMER_MAIL_ADDRESS2;
	}

	public void setCUSTOMER_MAIL_ADDRESS2(String cUSTOMER_MAIL_ADDRESS2) {
		CUSTOMER_MAIL_ADDRESS2 = cUSTOMER_MAIL_ADDRESS2;
	}

	public String getCUSTOMER_MAIL_ADDRESS3() {
		return CUSTOMER_MAIL_ADDRESS3;
	}

	public void setCUSTOMER_MAIL_ADDRESS3(String cUSTOMER_MAIL_ADDRESS3) {
		CUSTOMER_MAIL_ADDRESS3 = cUSTOMER_MAIL_ADDRESS3;
	}

	public String getCUSTOMER_MAIL_POSTCODE() {
		return CUSTOMER_MAIL_POSTCODE;
	}

	public void setCUSTOMER_MAIL_POSTCODE(String cUSTOMER_MAIL_POSTCODE) {
		CUSTOMER_MAIL_POSTCODE = cUSTOMER_MAIL_POSTCODE;
	}

	public String getCUSTOMER_MAIL_STATE() {
		return CUSTOMER_MAIL_STATE;
	}

	public void setCUSTOMER_MAIL_STATE(String cUSTOMER_MAIL_STATE) {
		CUSTOMER_MAIL_STATE = cUSTOMER_MAIL_STATE;
	}

	public String getCUSTOMER_MAIL_COUNTRY() {
		return CUSTOMER_MAIL_COUNTRY;
	}

	public void setCUSTOMER_MAIL_COUNTRY(String cUSTOMER_MAIL_COUNTRY) {
		CUSTOMER_MAIL_COUNTRY = cUSTOMER_MAIL_COUNTRY;
	}

	public String getCUSTOMER_NO_CHILDREN() {
		return CUSTOMER_NO_CHILDREN;
	}

	public void setCUSTOMER_NO_CHILDREN(String cUSTOMER_NO_CHILDREN) {
		CUSTOMER_NO_CHILDREN = cUSTOMER_NO_CHILDREN;
	}

	public String getCUSTOMER_MOBILE_NO() {
		return CUSTOMER_MOBILE_NO;
	}

	public void setCUSTOMER_MOBILE_NO(String cUSTOMER_MOBILE_NO) {
		CUSTOMER_MOBILE_NO = cUSTOMER_MOBILE_NO;
	}

	public String getCUSTOMER_EMAIL() {
		return CUSTOMER_EMAIL;
	}

	public void setCUSTOMER_EMAIL(String cUSTOMER_EMAIL) {
		CUSTOMER_EMAIL = cUSTOMER_EMAIL;
	}

	public String getLEADS_FLAG() {
		return LEADS_FLAG;
	}

	public void setLEADS_FLAG(String lEADS_FLAG) {
		LEADS_FLAG = lEADS_FLAG;
	}

	public String getSALES_TOOL() {
		return SALES_TOOL;
	}

	public void setSALES_TOOL(String sALES_TOOL) {
		SALES_TOOL = sALES_TOOL;
	}

	public String getEMAIL_SENT_COUNTER() {
		return EMAIL_SENT_COUNTER;
	}

	public void setEMAIL_SENT_COUNTER(String eMAIL_SENT_COUNTER) {
		EMAIL_SENT_COUNTER = eMAIL_SENT_COUNTER;
	}

	public String getQQ_ID() {
		return QQ_ID;
	}

	public void setQQ_ID(String qQ_ID) {
		QQ_ID = qQ_ID;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getUPDATED_DATE() {
		return UPDATED_DATE;
	}

	public void setUPDATED_DATE(String uPDATED_DATE) {
		UPDATED_DATE = uPDATED_DATE;
	}

	public String getCUSTOMER_EMPLOYER() {
		return CUSTOMER_EMPLOYER;
	}

	public void setCUSTOMER_EMPLOYER(String cUSTOMER_EMPLOYER) {
		CUSTOMER_EMPLOYER = cUSTOMER_EMPLOYER;
	}

	public String getCUSTOMER_CLIENTTYPE() {
		return CUSTOMER_CLIENTTYPE;
	}

	public void setCUSTOMER_CLIENTTYPE(String cUSTOMER_CLIENTTYPE) {
		CUSTOMER_CLIENTTYPE = cUSTOMER_CLIENTTYPE;
	}

	public String getCUSTOMER_INDUSTRY() {
		return CUSTOMER_INDUSTRY;
	}

	public void setCUSTOMER_INDUSTRY(String cUSTOMER_INDUSTRY) {
		CUSTOMER_INDUSTRY = cUSTOMER_INDUSTRY;
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getPOLICY_NUMBER() {
		return POLICY_NUMBER;
	}

	public void setPOLICY_NUMBER(String pOLICY_NUMBER) {
		POLICY_NUMBER = pOLICY_NUMBER;
	}

	public String getPRODUCT_CODE() {
		return PRODUCT_CODE;
	}

	public void setPRODUCT_CODE(String pRODUCT_CODE) {
		PRODUCT_CODE = pRODUCT_CODE;
	}

	public String getPRODUCT_PLAN_CODE() {
		return PRODUCT_PLAN_CODE;
	}

	public void setPRODUCT_PLAN_CODE(String pRODUCT_PLAN_CODE) {
		PRODUCT_PLAN_CODE = pRODUCT_PLAN_CODE;
	}

	public String getPOLICY_EFFECTIVE_TIMESTAMP() {
		return POLICY_EFFECTIVE_TIMESTAMP;
	}

	public void setPOLICY_EFFECTIVE_TIMESTAMP(String pOLICY_EFFECTIVE_TIMESTAMP) {
		POLICY_EFFECTIVE_TIMESTAMP = pOLICY_EFFECTIVE_TIMESTAMP;
	}

	public String getSUM_INSURED() {
		return SUM_INSURED;
	}

	public void setSUM_INSURED(String sUM_INSURED) {
		SUM_INSURED = sUM_INSURED;
	}

	public String getPOLICY_STATUS() {
		return POLICY_STATUS;
	}

	public void setPOLICY_STATUS(String pOLICY_STATUS) {
		POLICY_STATUS = pOLICY_STATUS;
	}

	public String getPOLICY_TYPE_CODE() {
		return POLICY_TYPE_CODE;
	}

	public void setPOLICY_TYPE_CODE(String pOLICY_TYPE_CODE) {
		POLICY_TYPE_CODE = pOLICY_TYPE_CODE;
	}

	public String getPOLICY_EXPIRY_TIMESTAMP() {
		return POLICY_EXPIRY_TIMESTAMP;
	}

	public void setPOLICY_EXPIRY_TIMESTAMP(String pOLICY_EXPIRY_TIMESTAMP) {
		POLICY_EXPIRY_TIMESTAMP = pOLICY_EXPIRY_TIMESTAMP;
	}

	public String getTRANSACTION_ID() {
		return TRANSACTION_ID;
	}

	public void setTRANSACTION_ID(String tRANSACTION_ID) {
		TRANSACTION_ID = tRANSACTION_ID;
	}

	public String getDSP_QQ_ID() {
		return DSP_QQ_ID;
	}

	public void setDSP_QQ_ID(String dSP_QQ_ID) {
		DSP_QQ_ID = dSP_QQ_ID;
	}

	public String getCREATION_TIMESTAMP() {
		return CREATION_TIMESTAMP;
	}

	public void setCREATION_TIMESTAMP(String cREATION_TIMESTAMP) {
		CREATION_TIMESTAMP = cREATION_TIMESTAMP;
	}

	public String getLEADS_EMAIL_ID() {
		return LEADS_EMAIL_ID;
	}

	public void setLEADS_EMAIL_ID(String lEADS_EMAIL_ID) {
		LEADS_EMAIL_ID = lEADS_EMAIL_ID;
	}

	public String getREGISTRATION_NUMBER() {
		return REGISTRATION_NUMBER;
	}

	public void setREGISTRATION_NUMBER(String rEGISTRATION_NUMBER) {
		REGISTRATION_NUMBER = rEGISTRATION_NUMBER;
	}

	public String getVEHICLE_LOCATION() {
		return VEHICLE_LOCATION;
	}

	public void setVEHICLE_LOCATION(String vEHICLE_LOCATION) {
		VEHICLE_LOCATION = vEHICLE_LOCATION;
	}

	public String getPREVIOUS_INSURANCE_COMPANY() {
		return PREVIOUS_INSURANCE_COMPANY;
	}

	public void setPREVIOUS_INSURANCE_COMPANY(String pREVIOUS_INSURANCE_COMPANY) {
		PREVIOUS_INSURANCE_COMPANY = pREVIOUS_INSURANCE_COMPANY;
	}

	public String getBMS_QUESTION_ANS() {
		return BMS_QUESTION_ANS;
	}

	public void setBMS_QUESTION_ANS(String bMS_QUESTION_ANS) {
		BMS_QUESTION_ANS = bMS_QUESTION_ANS;
	}

	public String getVEHICLE_MAKE() {
		return VEHICLE_MAKE;
	}

	public void setVEHICLE_MAKE(String vEHICLE_MAKE) {
		VEHICLE_MAKE = vEHICLE_MAKE;
	}

	public String getVEHICLE_MODEL() {
		return VEHICLE_MODEL;
	}

	public void setVEHICLE_MODEL(String vEHICLE_MODEL) {
		VEHICLE_MODEL = vEHICLE_MODEL;
	}

	public String getYEAR_MAKE() {
		return YEAR_MAKE;
	}

	public void setYEAR_MAKE(String yEAR_MAKE) {
		YEAR_MAKE = yEAR_MAKE;
	}

	public String getVEHICLE_VARIANT() {
		return VEHICLE_VARIANT;
	}

	public void setVEHICLE_VARIANT(String vEHICLE_VARIANT) {
		VEHICLE_VARIANT = vEHICLE_VARIANT;
	}

	public String getFUEL_TYPE() {
		return FUEL_TYPE;
	}

	public void setFUEL_TYPE(String fUEL_TYPE) {
		FUEL_TYPE = fUEL_TYPE;
	}

	public String getMARKET_VALUE() {
		return MARKET_VALUE;
	}

	public void setMARKET_VALUE(String mARKET_VALUE) {
		MARKET_VALUE = mARKET_VALUE;
	}

	public String getENGINE_NUMBER() {
		return ENGINE_NUMBER;
	}

	public void setENGINE_NUMBER(String eNGINE_NUMBER) {
		ENGINE_NUMBER = eNGINE_NUMBER;
	}

	public String getCHASSIS_NUMBER() {
		return CHASSIS_NUMBER;
	}

	public void setCHASSIS_NUMBER(String cHASSIS_NUMBER) {
		CHASSIS_NUMBER = cHASSIS_NUMBER;
	}

	public String getFINANCED_BY() {
		return FINANCED_BY;
	}

	public void setFINANCED_BY(String fINANCED_BY) {
		FINANCED_BY = fINANCED_BY;
	}

	public String getNVIC_CODE() {
		return NVIC_CODE;
	}

	public void setNVIC_CODE(String nVIC_CODE) {
		NVIC_CODE = nVIC_CODE;
	}

	public String getPASSENGER_PA_SELECTED() {
		return PASSENGER_PA_SELECTED;
	}

	public void setPASSENGER_PA_SELECTED(String pASSENGER_PA_SELECTED) {
		PASSENGER_PA_SELECTED = pASSENGER_PA_SELECTED;
	}

	public String getTPCD_PPA_QUESTION_ANS() {
		return TPCD_PPA_QUESTION_ANS;
	}

	public void setTPCD_PPA_QUESTION_ANS(String tPCD_PPA_QUESTION_ANS) {
		TPCD_PPA_QUESTION_ANS = tPCD_PPA_QUESTION_ANS;
	}

	public String getPOLICY_SUM_INSURED() {
		return POLICY_SUM_INSURED;
	}

	public void setPOLICY_SUM_INSURED(String pOLICY_SUM_INSURED) {
		POLICY_SUM_INSURED = pOLICY_SUM_INSURED;
	}

	public String getPREMIUM_ON_SUM_INSURED() {
		return PREMIUM_ON_SUM_INSURED;
	}

	public void setPREMIUM_ON_SUM_INSURED(String pREMIUM_ON_SUM_INSURED) {
		PREMIUM_ON_SUM_INSURED = pREMIUM_ON_SUM_INSURED;
	}

	public String getDISCOUNT_CODE() {
		return DISCOUNT_CODE;
	}

	public void setDISCOUNT_CODE(String dISCOUNT_CODE) {
		DISCOUNT_CODE = dISCOUNT_CODE;
	}

	public String getGST_ON_POLICY_PREMIUM() {
		return GST_ON_POLICY_PREMIUM;
	}

	public void setGST_ON_POLICY_PREMIUM(String gST_ON_POLICY_PREMIUM) {
		GST_ON_POLICY_PREMIUM = gST_ON_POLICY_PREMIUM;
	}

	public String getSTAMP_DUTY_POLICY_PREMIUM() {
		return STAMP_DUTY_POLICY_PREMIUM;
	}

	public void setSTAMP_DUTY_POLICY_PREMIUM(String sTAMP_DUTY_POLICY_PREMIUM) {
		STAMP_DUTY_POLICY_PREMIUM = sTAMP_DUTY_POLICY_PREMIUM;
	}

	public String getPASSANGER_PA_PREMIUM() {
		return PASSANGER_PA_PREMIUM;
	}

	public void setPASSANGER_PA_PREMIUM(String pASSANGER_PA_PREMIUM) {
		PASSANGER_PA_PREMIUM = pASSANGER_PA_PREMIUM;
	}

	public String getGST_PASSANGER_PA_PREMIUM() {
		return GST_PASSANGER_PA_PREMIUM;
	}

	public void setGST_PASSANGER_PA_PREMIUM(String gST_PASSANGER_PA_PREMIUM) {
		GST_PASSANGER_PA_PREMIUM = gST_PASSANGER_PA_PREMIUM;
	}

	public String getSTAMP_DUTY_PASSANGER_PA_DIRECT() {
		return STAMP_DUTY_PASSANGER_PA_DIRECT;
	}

	public void setSTAMP_DUTY_PASSANGER_PA_DIRECT(String sTAMP_DUTY_PASSANGER_PA_DIRECT) {
		STAMP_DUTY_PASSANGER_PA_DIRECT = sTAMP_DUTY_PASSANGER_PA_DIRECT;
	}

	public String getPASSANGER_PA_PREMIUM_PAYABLE() {
		return PASSANGER_PA_PREMIUM_PAYABLE;
	}

	public void setPASSANGER_PA_PREMIUM_PAYABLE(String pASSANGER_PA_PREMIUM_PAYABLE) {
		PASSANGER_PA_PREMIUM_PAYABLE = pASSANGER_PA_PREMIUM_PAYABLE;
	}

	public String getTOTAL_PREMIUM_PAYABLE() {
		return TOTAL_PREMIUM_PAYABLE;
	}

	public void setTOTAL_PREMIUM_PAYABLE(String tOTAL_PREMIUM_PAYABLE) {
		TOTAL_PREMIUM_PAYABLE = tOTAL_PREMIUM_PAYABLE;
	}

	public String getPARKING_MAIL_ADDRESS_1() {
		return PARKING_MAIL_ADDRESS_1;
	}

	public void setPARKING_MAIL_ADDRESS_1(String pARKING_MAIL_ADDRESS_1) {
		PARKING_MAIL_ADDRESS_1 = pARKING_MAIL_ADDRESS_1;
	}

	public String getPARKING_MAIL_ADDRESS_2() {
		return PARKING_MAIL_ADDRESS_2;
	}

	public void setPARKING_MAIL_ADDRESS_2(String pARKING_MAIL_ADDRESS_2) {
		PARKING_MAIL_ADDRESS_2 = pARKING_MAIL_ADDRESS_2;
	}

	public String getPARKING_MAIL_POSTCODE() {
		return PARKING_MAIL_POSTCODE;
	}

	public void setPARKING_MAIL_POSTCODE(String pARKING_MAIL_POSTCODE) {
		PARKING_MAIL_POSTCODE = pARKING_MAIL_POSTCODE;
	}

	public String getPARKING_MAIL_STATE() {
		return PARKING_MAIL_STATE;
	}

	public void setPARKING_MAIL_STATE(String pARKING_MAIL_STATE) {
		PARKING_MAIL_STATE = pARKING_MAIL_STATE;
	}

	public String getVEHICLE_PARK_OVERNEIGHT() {
		return VEHICLE_PARK_OVERNEIGHT;
	}

	public void setVEHICLE_PARK_OVERNEIGHT(String vEHICLE_PARK_OVERNEIGHT) {
		VEHICLE_PARK_OVERNEIGHT = vEHICLE_PARK_OVERNEIGHT;
	}

	public String getVEHICLE_ANTI_THEFT() {
		return VEHICLE_ANTI_THEFT;
	}

	public void setVEHICLE_ANTI_THEFT(String vEHICLE_ANTI_THEFT) {
		VEHICLE_ANTI_THEFT = vEHICLE_ANTI_THEFT;
	}

	public String getVEHICLE_SAFTEY() {
		return VEHICLE_SAFTEY;
	}

	public void setVEHICLE_SAFTEY(String vEHICLE_SAFTEY) {
		VEHICLE_SAFTEY = vEHICLE_SAFTEY;
	}

	public String getPDS_QUESTION_ANS() {
		return PDS_QUESTION_ANS;
	}

	public void setPDS_QUESTION_ANS(String pDS_QUESTION_ANS) {
		PDS_QUESTION_ANS = pDS_QUESTION_ANS;
	}

	public String getIND_QUESTION_ANS() {
		return IND_QUESTION_ANS;
	}

	public void setIND_QUESTION_ANS(String iND_QUESTION_ANS) {
		IND_QUESTION_ANS = iND_QUESTION_ANS;
	}

	public String getPDPA_QUESTION_ANS() {
		return PDPA_QUESTION_ANS;
	}

	public void setPDPA_QUESTION_ANS(String pDPA_QUESTION_ANS) {
		PDPA_QUESTION_ANS = pDPA_QUESTION_ANS;
	}

	public String getNON_CLAIM_DISCOUNT() {
		return NON_CLAIM_DISCOUNT;
	}

	public void setNON_CLAIM_DISCOUNT(String nON_CLAIM_DISCOUNT) {
		NON_CLAIM_DISCOUNT = nON_CLAIM_DISCOUNT;
	}

	public String getEXCESS() {
		return EXCESS;
	}

	public void setEXCESS(String eXCESS) {
		EXCESS = eXCESS;
	}

	public String getCOVERAGE_START_DATE() {
		return COVERAGE_START_DATE;
	}

	public void setCOVERAGE_START_DATE(String cOVERAGE_START_DATE) {
		COVERAGE_START_DATE = cOVERAGE_START_DATE;
	}

	public String getCOVERAGE_END_DATE() {
		return COVERAGE_END_DATE;
	}

	public void setCOVERAGE_END_DATE(String cOVERAGE_END_DATE) {
		COVERAGE_END_DATE = cOVERAGE_END_DATE;
	}

	public String getAGENT_CODE() {
		return AGENT_CODE;
	}

	public void setAGENT_CODE(String aGENT_CODE) {
		AGENT_CODE = aGENT_CODE;
	}

	public String getOPERATOR_CODE() {
		return OPERATOR_CODE;
	}

	public void setOPERATOR_CODE(String oPERATOR_CODE) {
		OPERATOR_CODE = oPERATOR_CODE;
	}

	public String getIS_PURCHASED() {
		return IS_PURCHASED;
	}

	public void setIS_PURCHASED(String iS_PURCHASED) {
		IS_PURCHASED = iS_PURCHASED;
	}

	public String getQUOTATION_STATUS() {
		return QUOTATION_STATUS;
	}

	public void setQUOTATION_STATUS(String qUOTATION_STATUS) {
		QUOTATION_STATUS = qUOTATION_STATUS;
	}

	public String getLEAD_STATUS() {
		return LEAD_STATUS;
	}

	public void setLEAD_STATUS(String lEAD_STATUS) {
		LEAD_STATUS = lEAD_STATUS;
	}

	public String getQUOTATION_TYPE() {
		return QUOTATION_TYPE;
	}

	public void setQUOTATION_TYPE(String qUOTATION_TYPE) {
		QUOTATION_TYPE = qUOTATION_TYPE;
	}

	public String getQUOTATION_CREATION_DATETIME() {
		return QUOTATION_CREATION_DATETIME;
	}

	public void setQUOTATION_CREATION_DATETIME(String qUOTATION_CREATION_DATETIME) {
		QUOTATION_CREATION_DATETIME = qUOTATION_CREATION_DATETIME;
	}

	public String getCUSTOMER_IC() {
		return CUSTOMER_IC;
	}

	public void setCUSTOMER_IC(String cUSTOMER_IC) {
		CUSTOMER_IC = cUSTOMER_IC;
	}

	public String getNO_OF_DRIVERS() {
		return NO_OF_DRIVERS;
	}

	public void setNO_OF_DRIVERS(String nO_OF_DRIVERS) {
		NO_OF_DRIVERS = nO_OF_DRIVERS;
	}

	public String getQUOTATION_UPDATE_DATETIME() {
		return QUOTATION_UPDATE_DATETIME;
	}

	public void setQUOTATION_UPDATE_DATETIME(String qUOTATION_UPDATE_DATETIME) {
		QUOTATION_UPDATE_DATETIME = qUOTATION_UPDATE_DATETIME;
	}

	public String getLOADING() {
		return LOADING;
	}

	public void setLOADING(String lOADING) {
		LOADING = lOADING;
	}

	public String getDISCOUNT_AMOUNT() {
		return DISCOUNT_AMOUNT;
	}

	public void setDISCOUNT_AMOUNT(String dISCOUNT_AMOUNT) {
		DISCOUNT_AMOUNT = dISCOUNT_AMOUNT;
	}

	public String getPA_ADDRESS_CHECK() {
		return PA_ADDRESS_CHECK;
	}

	public void setPA_ADDRESS_CHECK(String pA_ADDRESS_CHECK) {
		PA_ADDRESS_CHECK = pA_ADDRESS_CHECK;
	}

	public String getFULL_NAME() {
		return FULL_NAME;
	}

	public void setFULL_NAME(String fULL_NAME) {
		FULL_NAME = fULL_NAME;
	}

	public String getPOLICY_ELAPSE() {
		return POLICY_ELAPSE;
	}

	public void setPOLICY_ELAPSE(String pOLICY_ELAPSE) {
		POLICY_ELAPSE = pOLICY_ELAPSE;
	}

	public String getADDITINAL_COVERAGE_PREMIUM() {
		return ADDITINAL_COVERAGE_PREMIUM;
	}

	public void setADDITINAL_COVERAGE_PREMIUM(String aDDITINAL_COVERAGE_PREMIUM) {
		ADDITINAL_COVERAGE_PREMIUM = aDDITINAL_COVERAGE_PREMIUM;
	}

}
