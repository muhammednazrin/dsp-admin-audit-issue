package com.etiqa.dsp.dao.common.pojo;

public class NomineeAndTrusteeDetails {
	private String f_Nominee;
	private String f_NomineeName;
	private String f_NomineeICNo;
	private String f_NomineeRelation;
	private String f_NomineeShare;

	public String getF_Nominee() {
		return f_Nominee;
	}

	public void setF_Nominee(String f_Nominee) {
		this.f_Nominee = f_Nominee;
	}

	public String getF_NomineeName() {
		return f_NomineeName;
	}

	public void setF_NomineeName(String f_NomineeName) {
		this.f_NomineeName = f_NomineeName;
	}

	public String getF_NomineeICNo() {
		return f_NomineeICNo;
	}

	public void setF_NomineeICNo(String f_NomineeICNo) {
		this.f_NomineeICNo = f_NomineeICNo;
	}

	public String getF_NomineeRelation() {
		return f_NomineeRelation;
	}

	public void setF_NomineeRelation(String f_NomineeRelation) {
		this.f_NomineeRelation = f_NomineeRelation;
	}

	public String getF_NomineeShare() {
		return f_NomineeShare;
	}

	public void setF_NomineeShare(String f_NomineeShare) {
		this.f_NomineeShare = f_NomineeShare;
	}

}
