package com.etiqa.dsp.dao.common.pojo;

public class CustUpdatedNomTrusDetailsVo {
	private int CUSTOMER_ID;
	private String CUSTOMER_ID_TYPE;
	private String CUSTOMER_NRIC_ID;
	private String CUSTOMER_NAME;
	private String CUSTOMER_DOB;
	private String CUSTOMER_GENDER;
	private String CUSTOMER_RACE;
	private String CUSTOMER_RELIGION;
	private String CUSTOMER_NATIONALITY;
	private String CUSTOMER_TITLE;
	private String CUSTOMER_NATIONALITY_RACE;
	private String CUSTOMER_EDU_LEVEL;
	private String CUSTOMER_MARITALSTATUS;
	private String CUSTOMER_SALARY_RANGE;
	private String CUSTOMER_OCCUPATION;
	private String CUSTOMER_ADDRESS1;
	private String CUSTOMER_ADDRESS2;
	private String CUSTOMER_ADDRESS3;
	private String CUSTOMER_POSTCODE;
	private String CUSTOMER_STATE;
	private String CUSTOMER_COUNTRY;
	private String CUSTOMER_MAIL_ADDRESS1;
	private String CUSTOMER_MAIL_ADDRESS2;
	private String CUSTOMER_MAIL_ADDRESS3;
	private String CUSTOMER_MAIL_POSTCODE;
	private String CUSTOMER_MAIL_STATE;
	private String CUSTOMER_MAIL_COUNTRY;
	private String CUSTOMER_NO_CHILDREN;
	private String CUSTOMER_MOBILE_NO;
	private String CUSTOMER_EMAIL;
	private String LEADS_FLAG;
	private String SALES_TOOL;
	private String EMAIL_SENT_COUNTER;
	private int QQ_ID;
	private String CREATE_DATE;
	private String UPDATED_DATE;
	private String CUSTOMER_EMPLOYER;
	private String CUSTOMER_CLIENTTYPE;
	private String CUSTOMER_INDUSTRY;
	private int DSP_QQ_ID;
	private String PRODUCT_CODE;
	private String CREATED_DATE;
	private int ID;
	private String POLICY_NUMBER;
	private String PRODUCT_PLAN_CODE;
	private String POLICY_EFFECTIVE_TIMESTAMP;
	private String SUM_INSURED;
	private String POLICY_STATUS;
	private String POLICY_TYPE_CODE;
	private String POLICY_EXPIRY_TIMESTAMP;
	private String TRANSACTION_ID;
	private String CREATION_TIMESTAMP;
	private String TL_CHECK_DIGIT;

	// Nomination Details

	private int NOMINEE_ID;

	private String NOMINEE_TYPE;
	private String NOMINEE_NRIC;
	private String NOMINEE_NAME;
	private String NOMINEE_DOB;
	private String NOMINEE_GENDER;
	private String NOMINEE_NATIONALITY;
	private String NOMINEE_OCCUPATION;
	private String NOMINEE_NAME_OF_EMPLOYER;
	private String NOMINEE_NATURE_OF_BUSSINESS;
	private String NOMINEE_REL_TO_POLICY_OWNER;
	private String NOMINEE_MARITALSTATUS;
	private String NOMINEE_ACC_NO;
	private String NOMINEE_BANK_NAME;
	private String NOMINEE_SHARE_PERC;
	private String NOMINEE_ADDRESS1;
	private String NOMINEE_ADDRESS2;
	private String NOMINEE_ADDRESS3;
	private String NOMINEE_STATE;
	private String NOMINEE_COUNTRY;

	private int NOMINEE_NO_CHILDREN;

	private String NOMINEE_CONTACT_NO;
	private String NOMINEE_PURPOSE;
	private String NOMINEE_CUSTOMER_ID;
	private String NOMINEE_POLICY_NUMBER;
	private String NOMINEE_CREATE_DATE;
	private String NOMINEE_CLIENTTYPE;
	private String NOMINEE_INDUSTRY;
	private String NOMINEE_POSTCODE;
	private String NOMINEE_TITLE;
	private String NOMINEE_EMAIL;
	private String NOMINEE_MOBILE_NO;

	private int NOMINEE_DSP_QQ_ID;
	private String NOMINEE_FLAG;

	private String langValue;

	private String F_Nominee;
	private String F_NomineeName;
	private String F_NomineeICNo;
	private String F_NomineeRelation;
	private String F_NomineeShare;

	public String getF_Nominee() {
		return F_Nominee;
	}

	public void setF_Nominee(String f_Nominee) {
		F_Nominee = f_Nominee;
	}

	public String getF_NomineeName() {
		return F_NomineeName;
	}

	public void setF_NomineeName(String f_NomineeName) {
		F_NomineeName = f_NomineeName;
	}

	public String getF_NomineeICNo() {
		return F_NomineeICNo;
	}

	public void setF_NomineeICNo(String f_NomineeICNo) {
		F_NomineeICNo = f_NomineeICNo;
	}

	public String getF_NomineeRelation() {
		return F_NomineeRelation;
	}

	public void setF_NomineeRelation(String f_NomineeRelation) {
		F_NomineeRelation = f_NomineeRelation;
	}

	public String getF_NomineeShare() {
		return F_NomineeShare;
	}

	public void setF_NomineeShare(String f_NomineeShare) {
		F_NomineeShare = f_NomineeShare;
	}

	public String getLangValue() {
		return langValue;
	}

	public void setLangValue(String langValue) {
		this.langValue = langValue;
	}

	public String getNOMINEE_FLAG() {
		return NOMINEE_FLAG;
	}

	public void setNOMINEE_FLAG(String nOMINEE_FLAG) {
		NOMINEE_FLAG = nOMINEE_FLAG;
	}

	public int getCUSTOMER_ID() {
		return CUSTOMER_ID;
	}

	public String getTL_CHECK_DIGIT() {
		return TL_CHECK_DIGIT;
	}

	public void setTL_CHECK_DIGIT(String tL_CHECK_DIGIT) {
		TL_CHECK_DIGIT = tL_CHECK_DIGIT;
	}

	public void setCUSTOMER_ID(int cUSTOMER_ID) {
		CUSTOMER_ID = cUSTOMER_ID;
	}

	public String getCUSTOMER_ID_TYPE() {
		return CUSTOMER_ID_TYPE;
	}

	public void setCUSTOMER_ID_TYPE(String cUSTOMER_ID_TYPE) {
		CUSTOMER_ID_TYPE = cUSTOMER_ID_TYPE;
	}

	public String getCUSTOMER_NRIC_ID() {
		return CUSTOMER_NRIC_ID;
	}

	public void setCUSTOMER_NRIC_ID(String cUSTOMER_NRIC_ID) {
		CUSTOMER_NRIC_ID = cUSTOMER_NRIC_ID;
	}

	public String getCUSTOMER_NAME() {
		return CUSTOMER_NAME;
	}

	public void setCUSTOMER_NAME(String cUSTOMER_NAME) {
		CUSTOMER_NAME = cUSTOMER_NAME;
	}

	public String getCUSTOMER_DOB() {
		return CUSTOMER_DOB;
	}

	public void setCUSTOMER_DOB(String cUSTOMER_DOB) {
		CUSTOMER_DOB = cUSTOMER_DOB;
	}

	public String getCUSTOMER_GENDER() {
		return CUSTOMER_GENDER;
	}

	public void setCUSTOMER_GENDER(String cUSTOMER_GENDER) {
		CUSTOMER_GENDER = cUSTOMER_GENDER;
	}

	public String getCUSTOMER_RACE() {
		return CUSTOMER_RACE;
	}

	public void setCUSTOMER_RACE(String cUSTOMER_RACE) {
		CUSTOMER_RACE = cUSTOMER_RACE;
	}

	public String getCUSTOMER_RELIGION() {
		return CUSTOMER_RELIGION;
	}

	public void setCUSTOMER_RELIGION(String cUSTOMER_RELIGION) {
		CUSTOMER_RELIGION = cUSTOMER_RELIGION;
	}

	public String getCUSTOMER_NATIONALITY() {
		return CUSTOMER_NATIONALITY;
	}

	public void setCUSTOMER_NATIONALITY(String cUSTOMER_NATIONALITY) {
		CUSTOMER_NATIONALITY = cUSTOMER_NATIONALITY;
	}

	public String getCUSTOMER_TITLE() {
		return CUSTOMER_TITLE;
	}

	public void setCUSTOMER_TITLE(String cUSTOMER_TITLE) {
		CUSTOMER_TITLE = cUSTOMER_TITLE;
	}

	public String getCUSTOMER_NATIONALITY_RACE() {
		return CUSTOMER_NATIONALITY_RACE;
	}

	public void setCUSTOMER_NATIONALITY_RACE(String cUSTOMER_NATIONALITY_RACE) {
		CUSTOMER_NATIONALITY_RACE = cUSTOMER_NATIONALITY_RACE;
	}

	public String getCUSTOMER_EDU_LEVEL() {
		return CUSTOMER_EDU_LEVEL;
	}

	public void setCUSTOMER_EDU_LEVEL(String cUSTOMER_EDU_LEVEL) {
		CUSTOMER_EDU_LEVEL = cUSTOMER_EDU_LEVEL;
	}

	public String getCUSTOMER_MARITALSTATUS() {
		return CUSTOMER_MARITALSTATUS;
	}

	public void setCUSTOMER_MARITALSTATUS(String cUSTOMER_MARITALSTATUS) {
		CUSTOMER_MARITALSTATUS = cUSTOMER_MARITALSTATUS;
	}

	public String getCUSTOMER_SALARY_RANGE() {
		return CUSTOMER_SALARY_RANGE;
	}

	public void setCUSTOMER_SALARY_RANGE(String cUSTOMER_SALARY_RANGE) {
		CUSTOMER_SALARY_RANGE = cUSTOMER_SALARY_RANGE;
	}

	public String getCUSTOMER_OCCUPATION() {
		return CUSTOMER_OCCUPATION;
	}

	public void setCUSTOMER_OCCUPATION(String cUSTOMER_OCCUPATION) {
		CUSTOMER_OCCUPATION = cUSTOMER_OCCUPATION;
	}

	public String getCUSTOMER_ADDRESS1() {
		return CUSTOMER_ADDRESS1;
	}

	public void setCUSTOMER_ADDRESS1(String cUSTOMER_ADDRESS1) {
		CUSTOMER_ADDRESS1 = cUSTOMER_ADDRESS1;
	}

	public String getCUSTOMER_ADDRESS2() {
		return CUSTOMER_ADDRESS2;
	}

	public void setCUSTOMER_ADDRESS2(String cUSTOMER_ADDRESS2) {
		CUSTOMER_ADDRESS2 = cUSTOMER_ADDRESS2;
	}

	public String getCUSTOMER_ADDRESS3() {
		return CUSTOMER_ADDRESS3;
	}

	public void setCUSTOMER_ADDRESS3(String cUSTOMER_ADDRESS3) {
		CUSTOMER_ADDRESS3 = cUSTOMER_ADDRESS3;
	}

	public String getCUSTOMER_POSTCODE() {
		return CUSTOMER_POSTCODE;
	}

	public void setCUSTOMER_POSTCODE(String cUSTOMER_POSTCODE) {
		CUSTOMER_POSTCODE = cUSTOMER_POSTCODE;
	}

	public String getCUSTOMER_STATE() {
		return CUSTOMER_STATE;
	}

	public void setCUSTOMER_STATE(String cUSTOMER_STATE) {
		CUSTOMER_STATE = cUSTOMER_STATE;
	}

	public String getCUSTOMER_COUNTRY() {
		return CUSTOMER_COUNTRY;
	}

	public void setCUSTOMER_COUNTRY(String cUSTOMER_COUNTRY) {
		CUSTOMER_COUNTRY = cUSTOMER_COUNTRY;
	}

	public String getCUSTOMER_MAIL_ADDRESS1() {
		return CUSTOMER_MAIL_ADDRESS1;
	}

	public void setCUSTOMER_MAIL_ADDRESS1(String cUSTOMER_MAIL_ADDRESS1) {
		CUSTOMER_MAIL_ADDRESS1 = cUSTOMER_MAIL_ADDRESS1;
	}

	public String getCUSTOMER_MAIL_ADDRESS2() {
		return CUSTOMER_MAIL_ADDRESS2;
	}

	public void setCUSTOMER_MAIL_ADDRESS2(String cUSTOMER_MAIL_ADDRESS2) {
		CUSTOMER_MAIL_ADDRESS2 = cUSTOMER_MAIL_ADDRESS2;
	}

	public String getCUSTOMER_MAIL_ADDRESS3() {
		return CUSTOMER_MAIL_ADDRESS3;
	}

	public void setCUSTOMER_MAIL_ADDRESS3(String cUSTOMER_MAIL_ADDRESS3) {
		CUSTOMER_MAIL_ADDRESS3 = cUSTOMER_MAIL_ADDRESS3;
	}

	public String getCUSTOMER_MAIL_POSTCODE() {
		return CUSTOMER_MAIL_POSTCODE;
	}

	public void setCUSTOMER_MAIL_POSTCODE(String cUSTOMER_MAIL_POSTCODE) {
		CUSTOMER_MAIL_POSTCODE = cUSTOMER_MAIL_POSTCODE;
	}

	public String getCUSTOMER_MAIL_STATE() {
		return CUSTOMER_MAIL_STATE;
	}

	public void setCUSTOMER_MAIL_STATE(String cUSTOMER_MAIL_STATE) {
		CUSTOMER_MAIL_STATE = cUSTOMER_MAIL_STATE;
	}

	public String getCUSTOMER_MAIL_COUNTRY() {
		return CUSTOMER_MAIL_COUNTRY;
	}

	public void setCUSTOMER_MAIL_COUNTRY(String cUSTOMER_MAIL_COUNTRY) {
		CUSTOMER_MAIL_COUNTRY = cUSTOMER_MAIL_COUNTRY;
	}

	public String getCUSTOMER_MOBILE_NO() {
		return CUSTOMER_MOBILE_NO;
	}

	public void setCUSTOMER_MOBILE_NO(String cUSTOMER_MOBILE_NO) {
		CUSTOMER_MOBILE_NO = cUSTOMER_MOBILE_NO;
	}

	public String getCUSTOMER_EMAIL() {
		return CUSTOMER_EMAIL;
	}

	public void setCUSTOMER_EMAIL(String cUSTOMER_EMAIL) {
		CUSTOMER_EMAIL = cUSTOMER_EMAIL;
	}

	public String getLEADS_FLAG() {
		return LEADS_FLAG;
	}

	public void setLEADS_FLAG(String lEADS_FLAG) {
		LEADS_FLAG = lEADS_FLAG;
	}

	public String getSALES_TOOL() {
		return SALES_TOOL;
	}

	public void setSALES_TOOL(String sALES_TOOL) {
		SALES_TOOL = sALES_TOOL;
	}

	public String getEMAIL_SENT_COUNTER() {
		return EMAIL_SENT_COUNTER;
	}

	public void setEMAIL_SENT_COUNTER(String eMAIL_SENT_COUNTER) {
		EMAIL_SENT_COUNTER = eMAIL_SENT_COUNTER;
	}

	public int getQQ_ID() {
		return QQ_ID;
	}

	public void setQQ_ID(int qQ_ID) {
		QQ_ID = qQ_ID;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getUPDATED_DATE() {
		return UPDATED_DATE;
	}

	public void setUPDATED_DATE(String uPDATED_DATE) {
		UPDATED_DATE = uPDATED_DATE;
	}

	public String getCUSTOMER_EMPLOYER() {
		return CUSTOMER_EMPLOYER;
	}

	public void setCUSTOMER_EMPLOYER(String cUSTOMER_EMPLOYER) {
		CUSTOMER_EMPLOYER = cUSTOMER_EMPLOYER;
	}

	public String getCUSTOMER_CLIENTTYPE() {
		return CUSTOMER_CLIENTTYPE;
	}

	public void setCUSTOMER_CLIENTTYPE(String cUSTOMER_CLIENTTYPE) {
		CUSTOMER_CLIENTTYPE = cUSTOMER_CLIENTTYPE;
	}

	public String getCUSTOMER_INDUSTRY() {
		return CUSTOMER_INDUSTRY;
	}

	public void setCUSTOMER_INDUSTRY(String cUSTOMER_INDUSTRY) {
		CUSTOMER_INDUSTRY = cUSTOMER_INDUSTRY;
	}

	public int getDSP_QQ_ID() {
		return DSP_QQ_ID;
	}

	public void setDSP_QQ_ID(int dSP_QQ_ID) {
		DSP_QQ_ID = dSP_QQ_ID;
	}

	public String getPRODUCT_CODE() {
		return PRODUCT_CODE;
	}

	public void setPRODUCT_CODE(String pRODUCT_CODE) {
		PRODUCT_CODE = pRODUCT_CODE;
	}

	public String getCREATED_DATE() {
		return CREATED_DATE;
	}

	public void setCREATED_DATE(String cREATED_DATE) {
		CREATED_DATE = cREATED_DATE;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getPOLICY_NUMBER() {
		return POLICY_NUMBER;
	}

	public void setPOLICY_NUMBER(String pOLICY_NUMBER) {
		POLICY_NUMBER = pOLICY_NUMBER;
	}

	public String getPRODUCT_PLAN_CODE() {
		return PRODUCT_PLAN_CODE;
	}

	public void setPRODUCT_PLAN_CODE(String pRODUCT_PLAN_CODE) {
		PRODUCT_PLAN_CODE = pRODUCT_PLAN_CODE;
	}

	public String getPOLICY_EFFECTIVE_TIMESTAMP() {
		return POLICY_EFFECTIVE_TIMESTAMP;
	}

	public void setPOLICY_EFFECTIVE_TIMESTAMP(String pOLICY_EFFECTIVE_TIMESTAMP) {
		POLICY_EFFECTIVE_TIMESTAMP = pOLICY_EFFECTIVE_TIMESTAMP;
	}

	public String getPOLICY_STATUS() {
		return POLICY_STATUS;
	}

	public void setPOLICY_STATUS(String pOLICY_STATUS) {
		POLICY_STATUS = pOLICY_STATUS;
	}

	public String getPOLICY_TYPE_CODE() {
		return POLICY_TYPE_CODE;
	}

	public void setPOLICY_TYPE_CODE(String pOLICY_TYPE_CODE) {
		POLICY_TYPE_CODE = pOLICY_TYPE_CODE;
	}

	public String getPOLICY_EXPIRY_TIMESTAMP() {
		return POLICY_EXPIRY_TIMESTAMP;
	}

	public void setPOLICY_EXPIRY_TIMESTAMP(String pOLICY_EXPIRY_TIMESTAMP) {
		POLICY_EXPIRY_TIMESTAMP = pOLICY_EXPIRY_TIMESTAMP;
	}

	public String getCUSTOMER_NO_CHILDREN() {
		return CUSTOMER_NO_CHILDREN;
	}

	public void setCUSTOMER_NO_CHILDREN(String cUSTOMER_NO_CHILDREN) {
		CUSTOMER_NO_CHILDREN = cUSTOMER_NO_CHILDREN;
	}

	public String getSUM_INSURED() {
		return SUM_INSURED;
	}

	public void setSUM_INSURED(String sUM_INSURED) {
		SUM_INSURED = sUM_INSURED;
	}

	public String getTRANSACTION_ID() {
		return TRANSACTION_ID;
	}

	public void setTRANSACTION_ID(String tRANSACTION_ID) {
		TRANSACTION_ID = tRANSACTION_ID;
	}

	public String getCREATION_TIMESTAMP() {
		return CREATION_TIMESTAMP;
	}

	public void setCREATION_TIMESTAMP(String cREATION_TIMESTAMP) {
		CREATION_TIMESTAMP = cREATION_TIMESTAMP;
	}

	public int getNOMINEE_ID() {
		return NOMINEE_ID;
	}

	public void setNOMINEE_ID(int nOMINEE_ID) {
		NOMINEE_ID = nOMINEE_ID;
	}

	public String getNOMINEE_TYPE() {
		return NOMINEE_TYPE;
	}

	public void setNOMINEE_TYPE(String nOMINEE_TYPE) {
		NOMINEE_TYPE = nOMINEE_TYPE;
	}

	public String getNOMINEE_NRIC() {
		return NOMINEE_NRIC;
	}

	public void setNOMINEE_NRIC(String nOMINEE_NRIC) {
		NOMINEE_NRIC = nOMINEE_NRIC;
	}

	public String getNOMINEE_NAME() {
		return NOMINEE_NAME;
	}

	public void setNOMINEE_NAME(String nOMINEE_NAME) {
		NOMINEE_NAME = nOMINEE_NAME;
	}

	public String getNOMINEE_DOB() {
		return NOMINEE_DOB;
	}

	public void setNOMINEE_DOB(String nOMINEE_DOB) {
		NOMINEE_DOB = nOMINEE_DOB;
	}

	public String getNOMINEE_GENDER() {
		return NOMINEE_GENDER;
	}

	public void setNOMINEE_GENDER(String nOMINEE_GENDER) {
		NOMINEE_GENDER = nOMINEE_GENDER;
	}

	public String getNOMINEE_NATIONALITY() {
		return NOMINEE_NATIONALITY;
	}

	public void setNOMINEE_NATIONALITY(String nOMINEE_NATIONALITY) {
		NOMINEE_NATIONALITY = nOMINEE_NATIONALITY;
	}

	public String getNOMINEE_OCCUPATION() {
		return NOMINEE_OCCUPATION;
	}

	public void setNOMINEE_OCCUPATION(String nOMINEE_OCCUPATION) {
		NOMINEE_OCCUPATION = nOMINEE_OCCUPATION;
	}

	public String getNOMINEE_NAME_OF_EMPLOYER() {
		return NOMINEE_NAME_OF_EMPLOYER;
	}

	public void setNOMINEE_NAME_OF_EMPLOYER(String nOMINEE_NAME_OF_EMPLOYER) {
		NOMINEE_NAME_OF_EMPLOYER = nOMINEE_NAME_OF_EMPLOYER;
	}

	public String getNOMINEE_NATURE_OF_BUSSINESS() {
		return NOMINEE_NATURE_OF_BUSSINESS;
	}

	public void setNOMINEE_NATURE_OF_BUSSINESS(String nOMINEE_NATURE_OF_BUSSINESS) {
		NOMINEE_NATURE_OF_BUSSINESS = nOMINEE_NATURE_OF_BUSSINESS;
	}

	public String getNOMINEE_REL_TO_POLICY_OWNER() {
		return NOMINEE_REL_TO_POLICY_OWNER;
	}

	public void setNOMINEE_REL_TO_POLICY_OWNER(String nOMINEE_REL_TO_POLICY_OWNER) {
		NOMINEE_REL_TO_POLICY_OWNER = nOMINEE_REL_TO_POLICY_OWNER;
	}

	public String getNOMINEE_MARITALSTATUS() {
		return NOMINEE_MARITALSTATUS;
	}

	public void setNOMINEE_MARITALSTATUS(String nOMINEE_MARITALSTATUS) {
		NOMINEE_MARITALSTATUS = nOMINEE_MARITALSTATUS;
	}

	public String getNOMINEE_ACC_NO() {
		return NOMINEE_ACC_NO;
	}

	public void setNOMINEE_ACC_NO(String nOMINEE_ACC_NO) {
		NOMINEE_ACC_NO = nOMINEE_ACC_NO;
	}

	public String getNOMINEE_BANK_NAME() {
		return NOMINEE_BANK_NAME;
	}

	public void setNOMINEE_BANK_NAME(String nOMINEE_BANK_NAME) {
		NOMINEE_BANK_NAME = nOMINEE_BANK_NAME;
	}

	public String getNOMINEE_SHARE_PERC() {
		return NOMINEE_SHARE_PERC;
	}

	public void setNOMINEE_SHARE_PERC(String nOMINEE_SHARE_PERC) {
		NOMINEE_SHARE_PERC = nOMINEE_SHARE_PERC;
	}

	public String getNOMINEE_ADDRESS1() {
		return NOMINEE_ADDRESS1;
	}

	public void setNOMINEE_ADDRESS1(String nOMINEE_ADDRESS1) {
		NOMINEE_ADDRESS1 = nOMINEE_ADDRESS1;
	}

	public String getNOMINEE_ADDRESS2() {
		return NOMINEE_ADDRESS2;
	}

	public void setNOMINEE_ADDRESS2(String nOMINEE_ADDRESS2) {
		NOMINEE_ADDRESS2 = nOMINEE_ADDRESS2;
	}

	public String getNOMINEE_ADDRESS3() {
		return NOMINEE_ADDRESS3;
	}

	public void setNOMINEE_ADDRESS3(String nOMINEE_ADDRESS3) {
		NOMINEE_ADDRESS3 = nOMINEE_ADDRESS3;
	}

	public String getNOMINEE_STATE() {
		return NOMINEE_STATE;
	}

	public void setNOMINEE_STATE(String nOMINEE_STATE) {
		NOMINEE_STATE = nOMINEE_STATE;
	}

	public String getNOMINEE_COUNTRY() {
		return NOMINEE_COUNTRY;
	}

	public void setNOMINEE_COUNTRY(String nOMINEE_COUNTRY) {
		NOMINEE_COUNTRY = nOMINEE_COUNTRY;
	}

	public int getNOMINEE_NO_CHILDREN() {
		return NOMINEE_NO_CHILDREN;
	}

	public void setNOMINEE_NO_CHILDREN(int nOMINEE_NO_CHILDREN) {
		NOMINEE_NO_CHILDREN = nOMINEE_NO_CHILDREN;
	}

	public String getNOMINEE_CONTACT_NO() {
		return NOMINEE_CONTACT_NO;
	}

	public void setNOMINEE_CONTACT_NO(String nOMINEE_CONTACT_NO) {
		NOMINEE_CONTACT_NO = nOMINEE_CONTACT_NO;
	}

	public String getNOMINEE_PURPOSE() {
		return NOMINEE_PURPOSE;
	}

	public void setNOMINEE_PURPOSE(String nOMINEE_PURPOSE) {
		NOMINEE_PURPOSE = nOMINEE_PURPOSE;
	}

	public String getNOMINEE_CUSTOMER_ID() {
		return NOMINEE_CUSTOMER_ID;
	}

	public void setNOMINEE_CUSTOMER_ID(String nOMINEE_CUSTOMER_ID) {
		NOMINEE_CUSTOMER_ID = nOMINEE_CUSTOMER_ID;
	}

	public String getNOMINEE_POLICY_NUMBER() {
		return NOMINEE_POLICY_NUMBER;
	}

	public void setNOMINEE_POLICY_NUMBER(String nOMINEE_POLICY_NUMBER) {
		NOMINEE_POLICY_NUMBER = nOMINEE_POLICY_NUMBER;
	}

	public String getNOMINEE_CREATE_DATE() {
		return NOMINEE_CREATE_DATE;
	}

	public void setNOMINEE_CREATE_DATE(String nOMINEE_CREATE_DATE) {
		NOMINEE_CREATE_DATE = nOMINEE_CREATE_DATE;
	}

	public String getNOMINEE_CLIENTTYPE() {
		return NOMINEE_CLIENTTYPE;
	}

	public void setNOMINEE_CLIENTTYPE(String nOMINEE_CLIENTTYPE) {
		NOMINEE_CLIENTTYPE = nOMINEE_CLIENTTYPE;
	}

	public String getNOMINEE_INDUSTRY() {
		return NOMINEE_INDUSTRY;
	}

	public void setNOMINEE_INDUSTRY(String nOMINEE_INDUSTRY) {
		NOMINEE_INDUSTRY = nOMINEE_INDUSTRY;
	}

	public String getNOMINEE_POSTCODE() {
		return NOMINEE_POSTCODE;
	}

	public void setNOMINEE_POSTCODE(String nOMINEE_POSTCODE) {
		NOMINEE_POSTCODE = nOMINEE_POSTCODE;
	}

	public String getNOMINEE_TITLE() {
		return NOMINEE_TITLE;
	}

	public void setNOMINEE_TITLE(String nOMINEE_TITLE) {
		NOMINEE_TITLE = nOMINEE_TITLE;
	}

	public String getNOMINEE_EMAIL() {
		return NOMINEE_EMAIL;
	}

	public void setNOMINEE_EMAIL(String nOMINEE_EMAIL) {
		NOMINEE_EMAIL = nOMINEE_EMAIL;
	}

	public String getNOMINEE_MOBILE_NO() {
		return NOMINEE_MOBILE_NO;
	}

	public void setNOMINEE_MOBILE_NO(String nOMINEE_MOBILE_NO) {
		NOMINEE_MOBILE_NO = nOMINEE_MOBILE_NO;
	}

	public int getNOMINEE_DSP_QQ_ID() {
		return NOMINEE_DSP_QQ_ID;
	}

	public void setNOMINEE_DSP_QQ_ID(int nOMINEE_DSP_QQ_ID) {
		NOMINEE_DSP_QQ_ID = nOMINEE_DSP_QQ_ID;
	}

}
