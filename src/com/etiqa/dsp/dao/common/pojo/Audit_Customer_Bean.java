package com.etiqa.dsp.dao.common.pojo;

public class Audit_Customer_Bean {

	private String nricno;
	private String name;
	private String email;
	private String address;
	private String postcode;
	private String state;
	private String mobileno;
	private String o_nricno;
	private String o_name;
	private String o_email;
	private String o_address;
	private String o_postcode;
	private String o_state;
	private String o_mobileno;
	private String who;
	private String datewhen;
	private String recordcomment;

	public String getNricno() {
		return nricno;
	}

	public void setNricno(String nricno) {
		this.nricno = nricno;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getMobileno() {
		return mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

	public String getO_nricno() {
		return o_nricno;
	}

	public void setO_nricno(String o_nricno) {
		this.o_nricno = o_nricno;
	}

	public String getO_name() {
		return o_name;
	}

	public void setO_name(String o_name) {
		this.o_name = o_name;
	}

	public String getO_email() {
		return o_email;
	}

	public void setO_email(String o_email) {
		this.o_email = o_email;
	}

	public String getO_address() {
		return o_address;
	}

	public void setO_address(String o_address) {
		this.o_address = o_address;
	}

	public String getO_postcode() {
		return o_postcode;
	}

	public void setO_postcode(String o_postcode) {
		this.o_postcode = o_postcode;
	}

	public String getO_state() {
		return o_state;
	}

	public void setO_state(String o_state) {
		this.o_state = o_state;
	}

	public String getO_mobileno() {
		return o_mobileno;
	}

	public void setO_mobileno(String o_mobileno) {
		this.o_mobileno = o_mobileno;
	}

	public String getWho() {
		return who;
	}

	public void setWho(String who) {
		this.who = who;
	}

	public String getDatewhen() {
		return datewhen;
	}

	public void setDatewhen(String datewhen) {
		this.datewhen = datewhen;
	}

	public String getRecordcomment() {
		return recordcomment;
	}

	public void setRecordcomment(String recordcomment) {
		this.recordcomment = recordcomment;
	}

}
