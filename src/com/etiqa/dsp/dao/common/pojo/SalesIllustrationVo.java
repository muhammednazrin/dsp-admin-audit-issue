package com.etiqa.dsp.dao.common.pojo;

public class SalesIllustrationVo {
	private String f_EndOfPolicyYear;
	private String f_AnnualPremiumVal;
	private String f_BasicCashPolicyArray;
	private String f_BasicPolicyDeathBenefitArray;
	private String f_BasicPolicyNonDeathBenefitArray;
	private String f_BasicInsuredAgeEndOfYearArray;

	public String getF_EndOfPolicyYear() {
		return f_EndOfPolicyYear;
	}

	public void setF_EndOfPolicyYear(String f_EndOfPolicyYear) {
		this.f_EndOfPolicyYear = f_EndOfPolicyYear;
	}

	public String getF_AnnualPremiumVal() {
		return f_AnnualPremiumVal;
	}

	public void setF_AnnualPremiumVal(String f_AnnualPremiumVal) {
		this.f_AnnualPremiumVal = f_AnnualPremiumVal;
	}

	public String getF_BasicCashPolicyArray() {
		return f_BasicCashPolicyArray;
	}

	public void setF_BasicCashPolicyArray(String f_BasicCashPolicyArray) {
		this.f_BasicCashPolicyArray = f_BasicCashPolicyArray;
	}

	public String getF_BasicPolicyDeathBenefitArray() {
		return f_BasicPolicyDeathBenefitArray;
	}

	public void setF_BasicPolicyDeathBenefitArray(String f_BasicPolicyDeathBenefitArray) {
		this.f_BasicPolicyDeathBenefitArray = f_BasicPolicyDeathBenefitArray;
	}

	public String getF_BasicPolicyNonDeathBenefitArray() {
		return f_BasicPolicyNonDeathBenefitArray;
	}

	public void setF_BasicPolicyNonDeathBenefitArray(String f_BasicPolicyNonDeathBenefitArray) {
		this.f_BasicPolicyNonDeathBenefitArray = f_BasicPolicyNonDeathBenefitArray;
	}

	public String getF_BasicInsuredAgeEndOfYearArray() {
		return f_BasicInsuredAgeEndOfYearArray;
	}

	public void setF_BasicInsuredAgeEndOfYearArray(String f_BasicInsuredAgeEndOfYearArray) {
		this.f_BasicInsuredAgeEndOfYearArray = f_BasicInsuredAgeEndOfYearArray;
	}

}
