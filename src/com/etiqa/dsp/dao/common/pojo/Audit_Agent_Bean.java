package com.etiqa.dsp.dao.common.pojo;

public class Audit_Agent_Bean {

	private String referenceno;
	private String name;
	private String idno;
	private String phoneno;
	private String email;
	private String address1;
	private String agentcode;
	private String agentproductselection;
	private String discount;
	private String commision;
	private String type;

	private String oreferenceno;
	private String oname;
	private String oidno;
	private String ophoneno;
	private String oemail;
	private String oaddress1;
	private String oagentcode;
	private String oagentproductselection;
	private String odiscount;
	private String ocommision;
	private String otype;

	private String who;
	private String datewhen;
	private String recordcomment;

	// newly added
	private String oAgentCategory;
	private String agentCategory;

	private String oStatus;
	private String status;

	public String getReferenceno() {
		return referenceno;
	}

	public void setReferenceno(String referenceno) {
		this.referenceno = referenceno;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdno() {
		return idno;
	}

	public void setIdno(String idno) {
		this.idno = idno;
	}

	public String getPhoneno() {
		return phoneno;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAgentcode() {
		return agentcode;
	}

	public void setAgentcode(String agentcode) {
		this.agentcode = agentcode;
	}

	public String getAgentproductselection() {
		return agentproductselection;
	}

	public void setAgentproductselection(String agentproductselection) {
		this.agentproductselection = agentproductselection;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getCommision() {
		return commision;
	}

	public void setCommision(String commision) {
		this.commision = commision;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getWho() {
		return who;
	}

	public void setWho(String who) {
		this.who = who;
	}

	public String getDatewhen() {
		return datewhen;
	}

	public void setDatewhen(String datewhen) {
		this.datewhen = datewhen;
	}

	public String getRecordcomment() {
		return recordcomment;
	}

	public void setRecordcomment(String recordcomment) {
		this.recordcomment = recordcomment;
	}

	public String getOreferenceno() {
		return oreferenceno;
	}

	public void setOreferenceno(String oreferenceno) {
		this.oreferenceno = oreferenceno;
	}

	public String getOname() {
		return oname;
	}

	public void setOname(String oname) {
		this.oname = oname;
	}

	public String getOidno() {
		return oidno;
	}

	public void setOidno(String oidno) {
		this.oidno = oidno;
	}

	public String getOphoneno() {
		return ophoneno;
	}

	public void setOphoneno(String ophoneno) {
		this.ophoneno = ophoneno;
	}

	public String getOemail() {
		return oemail;
	}

	public void setOemail(String oemail) {
		this.oemail = oemail;
	}

	public String getOaddress1() {
		return oaddress1;
	}

	public void setOaddress1(String oaddress1) {
		this.oaddress1 = oaddress1;
	}

	public String getOagentcode() {
		return oagentcode;
	}

	public void setOagentcode(String oagentcode) {
		this.oagentcode = oagentcode;
	}

	public String getOagentproductselection() {
		return oagentproductselection;
	}

	public void setOagentproductselection(String oagentproductselection) {
		this.oagentproductselection = oagentproductselection;
	}

	public String getOdiscount() {
		return odiscount;
	}

	public void setOdiscount(String odiscount) {
		this.odiscount = odiscount;
	}

	public String getOcommision() {
		return ocommision;
	}

	public void setOcommision(String ocommision) {
		this.ocommision = ocommision;
	}

	public String getOtype() {
		return otype;
	}

	public void setOtype(String otype) {
		this.otype = otype;
	}

	public String getoAgentCategory() {
		return oAgentCategory;
	}

	public void setoAgentCategory(String oAgentCategory) {
		this.oAgentCategory = oAgentCategory;
	}

	public String getAgentCategory() {
		return agentCategory;
	}

	public void setAgentCategory(String agentCategory) {
		this.agentCategory = agentCategory;
	}

	public String getoStatus() {
		return oStatus;
	}

	public void setoStatus(String oStatus) {
		this.oStatus = oStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
