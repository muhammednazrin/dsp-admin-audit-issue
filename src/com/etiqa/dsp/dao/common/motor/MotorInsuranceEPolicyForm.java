package com.etiqa.dsp.dao.common.motor;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.dsp.dao.common.pojo.MotorInsuranceCustDetails;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.ExporterInput;
import net.sf.jasperreports.export.OutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import oracle.jdbc.OracleTypes;

public class MotorInsuranceEPolicyForm {
	public String EPolicyFormMethod(MotorInsuranceCustDetails miVo,
			List<MotorInsuranceCustDetails> listOfAdditionalCoverage)
			throws ClassNotFoundException, JRException, IOException, SQLException {

		// MotorInsuranceTaxInvoiceForm TaxInvoiceFormMethod
		String fileName = miVo.getPOLICY_NUMBER() + "-EPolicyForm.pdf";
		String policyDocCode = "EPOLFORM";
		String sysDate = "";
		if (miVo.getLangVal().equals("lan_en")) {
			Format formatter = new SimpleDateFormat("dd-MMM-yyyy");
			sysDate = formatter.format(new Date());
			System.out.println("Today Date:" + sysDate);
		} else {
			Format formatter = new SimpleDateFormat("dd-MMM-yyyy", new Locale("ms", "MY", "MY"));
			sysDate = formatter.format(new Date());
			System.out.println("Today Date:" + sysDate);
		}
		Date date = new Date();
		String formattedDate = "";
		if (miVo.getLangVal().equals("lan_en")) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy  h:mm:ss a");
			formattedDate = sdf.format(date);
		} else {
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy  h:mm:ss a", new Locale("ms", "MY", "MY"));
			formattedDate = sdf.format(date);
		}
		Map<String, Object> parameters = new HashMap<String, Object>();

		String AddressVal = "", add1 = "", add2 = "", add3 = "";
		if (miVo.getCUSTOMER_ADDRESS1() != null) {
			add1 = miVo.getCUSTOMER_ADDRESS1() + "\n";
		}
		if (miVo.getCUSTOMER_ADDRESS2() != null) {
			add2 = miVo.getCUSTOMER_ADDRESS2() + "\n";
		}
		if (miVo.getCUSTOMER_ADDRESS3() != null) {
			add3 = miVo.getCUSTOMER_ADDRESS3() + "\n";
		}

		if (miVo.getCUSTOMER_STATE() != null) {
			AddressVal = miVo.getCUSTOMER_STATE() + ",";
		}
		String CountryPincode = miVo.getCUSTOMER_POSTCODE();
		parameters.put("P_Address1", add1 + add2 + add3 + CountryPincode + " " + AddressVal + "Malaysia");

		parameters.put("P_Date", formattedDate);
		parameters.put("P_Address", add1 + add2 + add3 + CountryPincode + " " + AddressVal + "Malaysia");
		parameters.put("P_Name", miVo.getCUSTOMER_NAME());
		parameters.put("P_AgentCode", miVo.getAGENT_CODE() == null ? "" : miVo.getAGENT_CODE());
		parameters.put("P_PolicyNo", miVo.getPOLICY_NUMBER());
		parameters.put("P_PlanName", "Private Car");

		String fromDate = miVo.getPOLICY_EFFECTIVE_TIMESTAMP();
		String toDate = miVo.getPOLICY_EXPIRY_TIMESTAMP();
		fromDate = fromDate.substring(0, 2) + "/" + fromDate.substring(2, fromDate.length());
		fromDate = fromDate.substring(0, 5) + "/" + fromDate.substring(5, fromDate.length());
		toDate = toDate.substring(0, 2) + "/" + toDate.substring(2, toDate.length());
		toDate = toDate.substring(0, 5) + "/" + toDate.substring(5, toDate.length());

		parameters.put("P_FromDate", fromDate);
		parameters.put("P_ToDate", toDate);
		parameters.put("P_BusinessProf", miVo.getCUSTOMER_OCCUPATION());
		parameters.put("P_Cover", "COMPREHENSIVE");
		parameters.put("P_Business", "");
		parameters.put("P_VehRegNo", miVo.getREGISTRATION_NUMBER());
		String Make = miVo.getMAKE() + " ";
		String Model = miVo.getMODEL();
		parameters.put("P_MakeModel", Make + Model);
		parameters.put("P_EngineNo", miVo.getENGINE_NUMBER() == null ? "-" : miVo.getENGINE_NUMBER());
		parameters.put("P_LogBookNo", "-");

		if (miVo.getLangVal().equals("lan_en")) {
			parameters.put("P_UseFor", "Private Car - Private Use");
		} else {
			parameters.put("P_UseFor", "Persendirian");
		}
		if (miVo.getLangVal().equals("lan_en")) {
			parameters.put("P_RiskCls", "Private Car");
		} else {
			parameters.put("P_RiskCls", "Kereta Persendirian");
		}
		// Persendirian
		parameters.put("P_Risk", "1");
		parameters.put("P_Risk2", "1");
		parameters.put("P_CubicCapacity", miVo.getCC());
		parameters.put("P_YearOfManufacture", miVo.getYEAR_MAKE());
		parameters.put("P_ChassingNo", miVo.getCHASSIS_NUMBER());
		parameters.put("P_SeatingCapacity", miVo.getSEAT());
		parameters.put("P_EstimatedValue", miVo.getPOLICY_SUM_INSURED());
		parameters.put("P_AnnualContribution", miVo.getBASIC_PREMIUM());
		parameters.put("P_LoadingPercentage", miVo.getLOADING_PERCENTAGE() + "%");
		parameters.put("P_Loading", miVo.getLOADING());

		// miVo.getGROSSPREMIUM_FINAL()-miVo.getPREMIUM_AFTER_DISCOUNT()

		parameters.put("P_DiscountRebate", miVo.getDISCOUNT_AMOUNT());
		parameters.put("P_AnnualNCDPercentage", miVo.getNON_CLAIM_DISCOUNT() + "%");
		parameters.put("P_AnnualNCD", miVo.getNCD_AMOUNT());
		int sizeOfAdditionalCoverage = listOfAdditionalCoverage.size();
		if (listOfAdditionalCoverage.size() == 0) {
			if (miVo.getLangVal().equals("lan_en")) {
				parameters.put("P_AdditionalCoverageText1", "Goods and Services Tax (GST)");
				parameters.put("P_AdditionalCoverageText2", "Stamp Duty");
				parameters.put("P_AdditionalCoverageText3", "Total Due");
			} else {
				parameters.put("P_AdditionalCoverageText1", "Cukai Barangan dan Perkidmatan (GST)");
				parameters.put("P_AdditionalCoverageText2", "Duti Setem");
				parameters.put("P_AdditionalCoverageText3", "Jumlah Perlu Dibayar");
			}

			parameters.put("P_AdditionalCoverageValue1", miVo.getGST_ON_POLICY_PREMIUM());
			parameters.put("P_AdditionalCoverageValue2", miVo.getSTAMP_DUTY_POLICY_PREMIUM());
			Double premiumAfterGst = new Double(miVo.getPREMIUM_AFTER_GST());
			Double StampDuty = new Double(miVo.getSTAMP_DUTY_POLICY_PREMIUM());
			Double TotalDue = premiumAfterGst + StampDuty;
			parameters.put("P_AdditionalCoverageValue3", String.format("%.2f", TotalDue));

			// P_AdditionalCoverageSumInsValue
			// miVo.getTOTAL_PREMIUM_PAYABLE()
		} else {
			int count = 1;
			for (MotorInsuranceCustDetails AdditionalInfo : listOfAdditionalCoverage) {
				parameters.put("P_AdditionalCoverageText" + count, AdditionalInfo.getAdditionalCoverageText().trim());
				parameters.put("P_AdditionalCoverageValue" + count, AdditionalInfo.getAdditionalCoverageValue().trim());
				System.out.println(AdditionalInfo.getAdditionalCoverageText().trim() + ":"
						+ AdditionalInfo.getAdditionalCoverageValue().trim());
				String addCovSumInsValue = "";
				if (AdditionalInfo.getAdditionalCoverageSumInsuredValue().trim().equals("0.00")
						|| AdditionalInfo.getAdditionalCoverageSumInsuredValue().trim().equals("0")) {
					addCovSumInsValue = " ";
				} else {
					addCovSumInsValue = AdditionalInfo.getAdditionalCoverageSumInsuredValue().trim();
				}
				parameters.put("P_AdditionalCoverageSumInsValue" + count, addCovSumInsValue);
				count++;
			}
			System.out.println("Count Value Here" + count);
			if (miVo.getLangVal().equals("lan_en")) {
				parameters.put("P_AdditionalCoverageText" + (sizeOfAdditionalCoverage + 1),
						"Goods and Services Tax (GST)");
				parameters.put("P_AdditionalCoverageText" + (sizeOfAdditionalCoverage + 2), "Stamp Duty");
				parameters.put("P_AdditionalCoverageText" + (sizeOfAdditionalCoverage + 3), "Total Due");
			} else {
				parameters.put("P_AdditionalCoverageText" + (sizeOfAdditionalCoverage + 1),
						"Cukai Barangan dan Perkidmatan (GST)");
				parameters.put("P_AdditionalCoverageText" + (sizeOfAdditionalCoverage + 2), "Duti Setem");
				parameters.put("P_AdditionalCoverageText" + (sizeOfAdditionalCoverage + 3), "Jumlah Perlu Dibayar");
			}

			parameters.put("P_AdditionalCoverageValue" + (sizeOfAdditionalCoverage + 1),
					miVo.getGST_ON_POLICY_PREMIUM());
			parameters.put("P_AdditionalCoverageValue" + (sizeOfAdditionalCoverage + 2),
					miVo.getSTAMP_DUTY_POLICY_PREMIUM());
			Double premiumAfterGst = new Double(miVo.getPREMIUM_AFTER_GST());
			Double StampDuty = new Double(miVo.getSTAMP_DUTY_POLICY_PREMIUM());
			Double TotalDue = premiumAfterGst + StampDuty;
			parameters.put("P_AdditionalCoverageValue" + (sizeOfAdditionalCoverage + 3),
					String.format("%.2f", TotalDue));
		}

		if (listOfAdditionalCoverage.size() != 0) {
			int count = 1;

			for (MotorInsuranceCustDetails AdditionalInfo : listOfAdditionalCoverage) {
				parameters.put("P_AdditionalCoveragePageText" + count,
						AdditionalInfo.getAdditionalCoverageText().trim().toUpperCase());
				String addCoverageReplace = "";
				if (miVo.getLangVal().equals("lan_en")) {
					addCoverageReplace = getAdditionalCoverageValue(AdditionalInfo.getAdditionalCoverageText().trim());
				} else {
					addCoverageReplace = getAdditionalCoverageValueBM(
							AdditionalInfo.getAdditionalCoverageText().trim());
				}
				parameters.put("P_AdditionalCoveragePageValue" + count, addCoverageReplace);

				count++;
			}

		}

		// miVo.getTOTAL_PREMIUM_PAYABLE()
		/*
		 * parameters.put("P_GST", miVo.getGST_ON_POLICY_PREMIUM());
		 * parameters.put("P_StamDuty", "10.00"); parameters.put("P_TotalDue",
		 * miVo.getTOTAL_PREMIUM_PAYABLE());
		 */
		parameters.put("P_AuthorisedDriver", miVo.getCUSTOMER_NAME());
		parameters.put("P_HirePurchase", miVo.getFINANCED_BY() == null ? "" : miVo.getFINANCED_BY());
		parameters.put("P_IssuedDate", sysDate);
		parameters.put("P_LocationOfVehicleParked", miVo.getVEHICLE_PARK_OVERNEIGHT());
		parameters.put("P_AddressVehicle", add1 + add2 + add3 + AddressVal + CountryPincode);
		parameters.put("P_AntiTheftDevices", miVo.getVEHICLE_ANTI_THEFT());
		parameters.put("P_SaftyFeatures", miVo.getVEHICLE_SAFTEY());
		parameters.put("P_TypeOfVehicle", miVo.getVehicle_Type() == null ? "" : miVo.getVehicle_Type());
		parameters.put("P_StartingDate", fromDate);
		parameters.put("P_ExpDate", toDate);
		parameters.put("P_ExcessAllClaims", "0.00");

		Connection connection = null;
		Statement statement = null;
		ResultSet rs = null;

		connection = ConnectionFactory.getConnection();
		CallableStatement cstmtDInfo = connection.prepareCall("{CALL DSP_MI_SP_DRIVERINFO_FETCH(?,?)}");
		cstmtDInfo.setString(1, miVo.getQQ_ID());
		System.out.println(miVo.getQQ_ID());
		cstmtDInfo.registerOutParameter(2, OracleTypes.CURSOR);
		cstmtDInfo.execute();

		List<MotorInsuranceCustDetails> listOfDriverInfoDetails = new ArrayList<MotorInsuranceCustDetails>();

		rs = (ResultSet) cstmtDInfo.getObject(2);
		while (rs.next()) {
			MotorInsuranceCustDetails driverInfovo = new MotorInsuranceCustDetails();
			driverInfovo.setDFullName(rs.getString(1));
			// driverInfovo.setDNewIc(rs.getString(1));
			driverInfovo.setDDOB(rs.getString(2));
			driverInfovo.setDGender(rs.getString(3));
			driverInfovo.setDMartialStatus(rs.getString(4));
			driverInfovo.setDOccupation(rs.getString(5));
			// driverInfovo.setDRelationtoInsured(rs.getString(1));
			driverInfovo.setDrivingExp(rs.getString(6));
			driverInfovo.setDDrivingLicense(rs.getString(7));
			driverInfovo.setDNoofFaultClaim(rs.getString(8));
			driverInfovo.setDTotalAmntOfClaim(rs.getString(9));
			driverInfovo.setDMajorTraffic(rs.getString(10));
			listOfDriverInfoDetails.add(driverInfovo);
		}

		/*
		 * parameters.put("P_DFullName", miVo.getCUSTOMER_NAME());
		 * parameters.put("P_DNewIc", miVo.getCUSTOMER_NRIC_ID());
		 * parameters.put("P_DDOB", miVo.getCUSTOMER_DOB()); parameters.put("P_DGender",
		 * miVo.getCUSTOMER_GENDER()); parameters.put("P_DMartialStatus",
		 * miVo.getCUSTOMER_MARITALSTATUS()); parameters.put("P_DOccupation",
		 * miVo.getCUSTOMER_OCCUPATION()); parameters.put("P_DRelationtoInsured",
		 * "Policy Holder");
		 */

		if (miVo.getLangVal().equals("lan_en")) {
			parameters.put("P_DRelationtoInsured1", "Policy Holder");
		} else {
			parameters.put("P_DRelationtoInsured1", "Pemegang Polisi");
		}
		System.out.println(listOfDriverInfoDetails.size() + " drivers list size");
		int driversListSize = listOfDriverInfoDetails.size();
		int count = 1;
		for (MotorInsuranceCustDetails driverList : listOfDriverInfoDetails) {
			if (count <= driversListSize) {
				parameters.put("P_DFullName" + count, driverList.getDFullName());
				parameters.put("P_DNewIc" + count, "");
				parameters.put("P_DDOB" + count, driverList.getDDOB());
				parameters.put("P_DGender" + count, driverList.getDGender());
				parameters.put("P_DMartialStatus" + count, driverList.getDMartialStatus());
				parameters.put("P_DOccupation" + count, driverList.getDOccupation());

				parameters.put("P_DrivingExp" + count, driverList.getDrivingExp());
				parameters.put("P_DDrivingLicense" + count, driverList.getDDrivingLicense());
				if (driverList.getDTotalAmntOfClaim() != null) {
					Double Value = Double.parseDouble(driverList.getDTotalAmntOfClaim());
					driverList.setDTotalAmntOfClaim(String.format("%.2f", Value));
				} else {
					driverList.setDTotalAmntOfClaim("0.00");
				}
				parameters.put("P_DNo.ofFaultClaim" + count, driverList.getDNoofFaultClaim());
				parameters.put("P_DTotalAmntOfClaim" + count, driverList.getDTotalAmntOfClaim());
				String mtcValue = "";
				if (driverList.getDMajorTraffic().equalsIgnoreCase("N")) {
					if (miVo.getLangVal().equals("lan_en")) {
						mtcValue = "No";
					} else {
						mtcValue = "Tidak";
					}

				} else {
					if (miVo.getLangVal().equals("lan_en")) {
						mtcValue = "Yes";
					} else {
						mtcValue = "Ya";
					}

				}
				parameters.put("P_DMajorTraffic" + count, mtcValue);

				count++;
			}
		}

		parameters.put("P_DNewIc1", miVo.getCUSTOMER_NRIC_ID());
		// Pemandu

		String driverText = "";

		if (miVo.getLangVal().equals("lan_en")) {
			driverText = "Driver";
		} else {
			driverText = "Pemandu";
		}

		if (driversListSize == 2) {
			parameters.put("P_Driver2", driverText + " 2");
			parameters.put("P_Driver2N", " ");
			parameters.put("P_Driver3N", "TestWillPrint");
		}
		if (driversListSize == 3) {
			parameters.put("P_Driver2", driverText + " 2");
			parameters.put("P_Driver2N", " ");
			parameters.put("P_Driver3", driverText + " 3");
			parameters.put("P_Driver3N", " ");
			parameters.put("P_Driver4N", "TestWillPrint");
		}
		if (driversListSize == 4) {
			parameters.put("P_Driver2", driverText + " 2");
			parameters.put("P_Driver2N", " ");
			parameters.put("P_Driver3", driverText + " 3");
			parameters.put("P_Driver3N", " ");
			parameters.put("P_Driver4", driverText + " 4");
			parameters.put("P_Driver4N", " ");
			parameters.put("P_Driver5N", "TestWillPrint");
		}
		if (driversListSize == 5) {
			parameters.put("P_Driver2", driverText + " 2");
			parameters.put("P_Driver2N", " ");
			parameters.put("P_Driver3", driverText + " 3");
			parameters.put("P_Driver3N", " ");
			parameters.put("P_Driver4", driverText + " 4");
			parameters.put("P_Driver4N", " ");
			parameters.put("P_Driver5", driverText + " 5");
			parameters.put("P_Driver5N", " ");
			parameters.put("P_Driver6N", "TestWillPrint");
		}
		if (driversListSize == 6) {
			parameters.put("P_Driver2", driverText + " 2");
			parameters.put("P_Driver2N", " ");
			parameters.put("P_Driver3", driverText + " 3");
			parameters.put("P_Driver3N", " ");
			parameters.put("P_Driver4", driverText + " 4");
			parameters.put("P_Driver4N", " ");
			parameters.put("P_Driver5", driverText + " 5");
			parameters.put("P_Driver5N", " ");
			parameters.put("P_Driver6", driverText + " 6");
			parameters.put("P_Driver6N", " ");
			parameters.put("P_Driver7N", "TestWillPrint");
		}

		if (driversListSize == 7) {
			parameters.put("P_Driver2", driverText + " 2");
			parameters.put("P_Driver2N", " ");
			parameters.put("P_Driver3", driverText + " 3");
			parameters.put("P_Driver3N", " ");
			parameters.put("P_Driver4", driverText + " 4");
			parameters.put("P_Driver4N", " ");
			parameters.put("P_Driver5", driverText + " 5");
			parameters.put("P_Driver5N", " ");
			parameters.put("P_Driver6", driverText + " 6");
			parameters.put("P_Driver6N", " ");
			parameters.put("P_Driver7", driverText + " 7");
			parameters.put("P_Driver7N", " ");
			parameters.put("P_Driver8N", "TestWillPrint");
		}

		try (InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/dsp/sales/process/email/documents.properties")) {
			Properties prop = new Properties();
			prop.load(in);

			Connection con = ConnectionFactory.getConnection();
			String jrxmlFile = "";
			if (driversListSize == 1) {
				if (miVo.getLangVal().equals("lan_en")) {
					jrxmlFile = "com/etiqa/dsp/policy/templates/MI_EPolicyForm_EV.jrxml";
				} else {
					jrxmlFile = "com/etiqa/dsp/policy/templates/MI_EPolicyForm_BV.jrxml";
				}

			} else if (driversListSize < 5) {
				if (miVo.getLangVal().equals("lan_en")) {
					jrxmlFile = "com/etiqa/dsp/policy/templates/MI_EPolicyForm_EV5.jrxml";
				} else {
					jrxmlFile = "com/etiqa/dsp/policy/templates/MI_EPolicyForm_BV5.jrxml";
				}

			} else {
				if (miVo.getLangVal().equals("lan_en")) {
					jrxmlFile = "com/etiqa/dsp/policy/templates/MI_EPolicyForm_EV7.jrxml";
				} else {
					jrxmlFile = "com/etiqa/dsp/policy/templates/MI_EPolicyForm_BV7.jrxml";
				}

			}

			InputStream input = this.getClass().getClassLoader().getResourceAsStream(jrxmlFile);
			JasperReport jasperReport = JasperCompileManager.compileReport(input);
			JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
			// JasperExportManager.exportReportToPdfFile(print,
			// prop.getProperty("destinationPath")+fileName);
			JRPdfExporter exporter = new JRPdfExporter();
			ExporterInput exporterInput = new SimpleExporterInput(print);
			exporter.setExporterInput(exporterInput);
			OutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(
					prop.getProperty("destinationPath") + fileName);
			exporter.setExporterOutput(exporterOutput);
			SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
			exporter.setConfiguration(configuration);
			exporter.exportReport();
			System.out.print("PDPA Pdf Gen Done!");
			// System.out.println(prop.getProperty("destinationPath")+fileName+"Destination
			// Path");
			// System.out.println(prop.getProperty("sourcePath")+fileName+"Source Path");

			File fileDb = new File(prop.getProperty("sourcePath") + fileName);
			FileInputStream fr = new FileInputStream(fileDb);
			CallableStatement cstmt = con.prepareCall("{CALL DSP_POLICY_DOC_INSERT(?,?,?,?,?)}");

			cstmt.setString(1, miVo.getPOLICY_NUMBER());
			cstmt.setString(2, policyDocCode);
			cstmt.setString(3, fileName);
			cstmt.setString(4, miVo.getCUSTOMER_NAME());
			cstmt.registerOutParameter(5, Types.VARCHAR);
			cstmt.execute();
			String StatusOfInsertion = cstmt.getString(5);

			System.out.println(StatusOfInsertion + " ID inserterd");

			return fileName;

		} catch (Exception e) {
			e.printStackTrace();
			return "failtoStore";
		}

	}

	public String EPolicyDppaFormMethod(MotorInsuranceCustDetails miVo)
			throws ClassNotFoundException, JRException, IOException, SQLException {

		// MotorInsuranceTaxInvoiceForm TaxInvoiceFormMethod
		String fileName = miVo.getPOLICY_NUMBER() + "-EPolicy(CAPS)Form.pdf";
		String policyDocCode = "EPOLDPPA";

		String sysDate = "";
		if (miVo.getLangVal().equals("lan_en")) {
			Format formatter = new SimpleDateFormat("dd-MMM-yyyy");
			sysDate = formatter.format(new Date());
			System.out.println("Today Date:" + sysDate);
		} else {
			Format formatter = new SimpleDateFormat("dd-MMM-yyyy", new Locale("ms", "MY", "MY"));
			sysDate = formatter.format(new Date());
			System.out.println("Today Date:" + sysDate);
		}
		Date date = new Date();
		String formattedDate = "";
		if (miVo.getLangVal().equals("lan_en")) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy  h:mm:ss a");
			formattedDate = sdf.format(date);
		} else {
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy  h:mm:ss a", new Locale("ms", "MY", "MY"));
			formattedDate = sdf.format(date);
		}
		Map<String, Object> parameters = new HashMap<String, Object>();

		String AddressVal = "", add1 = "", add2 = "", add3 = "";
		if (miVo.getCUSTOMER_ADDRESS1() != null) {
			add1 = miVo.getCUSTOMER_ADDRESS1() + "\n";
		}
		if (miVo.getCUSTOMER_ADDRESS2() != null) {
			add2 = miVo.getCUSTOMER_ADDRESS2() + "\n";
		}
		if (miVo.getCUSTOMER_ADDRESS3() != null) {
			add3 = miVo.getCUSTOMER_ADDRESS3() + "\n";
		}

		if (miVo.getCUSTOMER_STATE() != null) {
			AddressVal = miVo.getCUSTOMER_POSTCODE() + ",";
		}
		String CountryPincode = miVo.getCUSTOMER_STATE() + "," + "Malaysia";
		String fromDate = miVo.getPOLICY_EFFECTIVE_TIMESTAMP();
		String toDate = miVo.getPOLICY_EXPIRY_TIMESTAMP();
		fromDate = fromDate.substring(0, 2) + "/" + fromDate.substring(2, fromDate.length());
		fromDate = fromDate.substring(0, 5) + "/" + fromDate.substring(5, fromDate.length());
		toDate = toDate.substring(0, 2) + "/" + toDate.substring(2, toDate.length());
		toDate = toDate.substring(0, 5) + "/" + toDate.substring(5, toDate.length());

		parameters.put("P_Date", formattedDate);
		parameters.put("P_Address", add1 + add2 + add3 + AddressVal + CountryPincode);
		parameters.put("P_Name", miVo.getCUSTOMER_NAME());
		parameters.put("P_AgentCode", "N0028758"); // miVo.getAGENT_CODE()==null?"":miVo.getAGENT_CODE()
		parameters.put("P_PolicyNo", miVo.getDppaPolicyNo());

		// Khas Perlindungan Kemalangan Kereta
		if (miVo.getLangVal().equals("lan_en")) {
			parameters.put("P_PlanName", "Car Accident Protection Special (CAPS)");
		} else {
			parameters.put("P_PlanName", "Khas Perlindungan Kemalangan Kereta");
		}
		parameters.put("P_FromDate", fromDate);
		parameters.put("P_ToDate", toDate);

		parameters.put("P_TotalSumInsured", "RM 15,000");
		parameters.put("P_AnnualPremium", "RM " + miVo.getPASSANGER_PA_PREMIUM());
		parameters.put("P_PremiumDue", miVo.getPASSANGER_PA_PREMIUM());
		parameters.put("P_TotalExcludingGST", miVo.getPASSANGER_PA_PREMIUM());
		Double premium = new Double(miVo.getPASSANGER_PA_PREMIUM());
		Double premiumAfterDisc = new Double(miVo.getPASSENGER_PREMIUM_AFTDIS());
		Double TotalDue = premium - premiumAfterDisc;
		parameters.put("P_Discount", String.format("%.2f", TotalDue));
		parameters.put("P_StampDuty", miVo.getSTAMP_DUTY_PASSANGER_PA_DIRECT());
		parameters.put("P_GST", miVo.getGST_PASSANGER_PA_PREMIUM());
		parameters.put("P_TotalAmountDue", miVo.getPASSANGER_PA_PREMIUM_PAYABLE());
		parameters.put("P_Risk", "1");
		parameters.put("P_NRICNo", miVo.getCUSTOMER_NRIC_ID());
		parameters.put("P_DOB", miVo.getCUSTOMER_DOB());
		parameters.put("P_Occupation", miVo.getCUSTOMER_OCCUPATION());
		parameters.put("P_RegNo", miVo.getREGISTRATION_NUMBER());
		String Make = miVo.getMAKE() + " ";
		String Model = miVo.getMODEL();
		parameters.put("P_MakeModel", Make + Model);
		parameters.put("P_NoofSeats", miVo.getSEAT());

		// Pelan 1 Bagi setiap orang
		if (miVo.getLangVal().equals("lan_en")) {
			parameters.put("P_Plan", "Plan 1");
		} else {
			parameters.put("P_Plan", "Pelan 1");
		}
		if (miVo.getLangVal().equals("lan_en")) {
			parameters.put("P_Basis", "Per Person");
		} else {
			parameters.put("P_Basis", "Bagi setiap orang");
		}

		parameters.put("P_AccidentalDeath", "RM 20,000.00");
		parameters.put("P_PermanentDisablement", "RM 20,000.00");
		parameters.put("P_MedicalExpances", "RM 500.00");
		parameters.put("P_FuneralExpances", "1,000");
		parameters.put("P_IssuedDate", sysDate);

		try (InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/dsp/sales/process/email/documents.properties")) {
			Properties prop = new Properties();
			prop.load(in);

			Connection con = ConnectionFactory.getConnection();
			// String jrxmlFile =
			// "com/etiqa/dsp/policy/templates/MI_EPolicyFormDppa_EV.jrxml";
			String jrxmlFile = "";

			if (miVo.getLangVal().equals("lan_en")) {
				jrxmlFile = "com/etiqa/dsp/policy/templates/MI_EPolicyFormDppa_EV.jrxml";
			} else {
				jrxmlFile = "com/etiqa/dsp/policy/templates/MI_EPolicyFormDppa_BV.jrxml";
			}
			InputStream input = this.getClass().getClassLoader().getResourceAsStream(jrxmlFile);
			JasperReport jasperReport = JasperCompileManager.compileReport(input);
			JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
			// JasperExportManager.exportReportToPdfFile(print,
			// prop.getProperty("destinationPath")+fileName);
			JRPdfExporter exporter = new JRPdfExporter();
			ExporterInput exporterInput = new SimpleExporterInput(print);
			exporter.setExporterInput(exporterInput);
			OutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(
					prop.getProperty("destinationPath") + fileName);
			exporter.setExporterOutput(exporterOutput);
			SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
			exporter.setConfiguration(configuration);
			exporter.exportReport();
			System.out.print("Epolicy Dppa Pdf Gen Done!");
			// System.out.println(prop.getProperty("destinationPath")+fileName+"Destination
			// Path");
			// System.out.println(prop.getProperty("sourcePath")+fileName+"Source Path");

			File fileDb = new File(prop.getProperty("sourcePath") + fileName);
			FileInputStream fr = new FileInputStream(fileDb);
			CallableStatement cstmt = con.prepareCall("{CALL DSP_POLICY_DOC_INSERT(?,?,?,?,?)}");

			cstmt.setString(1, miVo.getPOLICY_NUMBER());
			cstmt.setString(2, policyDocCode);
			cstmt.setString(3, fileName);
			cstmt.setString(4, miVo.getCUSTOMER_NAME());
			cstmt.registerOutParameter(5, Types.VARCHAR);
			cstmt.execute();
			String StatusOfInsertion = cstmt.getString(5);

			System.out.println(StatusOfInsertion + " ID inserterd");

			return fileName;

		} catch (Exception e) {
			e.printStackTrace();
			return "failtoStore";
		}

	}

	public String getAdditionalCoverageValue(String source) {

		if (source != null) {
			String source1 = "Passenger Legal Liability Coverage";
			if (source.equals(source1)) {
				String addCoverageSearch1 = "B100	100		";
				return addCoverageSearch1;
			}
			String source2 = "Legal Liability of Pessenger for Acts of Negligence Coverage";
			if (source.equals(source2)) {
				String addCoverageSearch2 = "B072	72		";
				return addCoverageSearch2;
			}

			String source3 = "Extended Flood Cover";
			if (source.equals(source3)) {
				String addCoverageSearch3 = "B057	57		";
				return addCoverageSearch3;
			}

			String source4 = "Basic Flood Cover";
			if (source.equals(source4)) {
				String addCoverageSearch4 = "C57A	  		";
				return addCoverageSearch4;
			}

			String source5 = "NCD Relief";
			if (source.equals(source5)) {
				String addCoverageSearch5 = "   	111		";
				return addCoverageSearch5;
			}

			String source6 = "Windscreen Coverage";
			if (source.equals(source6)) {
				String addCoverageSearch6 = "B089	89		";
				return addCoverageSearch6;
			}

			String source7 = "Vehicle Accessories";
			if (source.equals(source7)) {
				String addCoverageSearch7 = "B097	97		";
				return addCoverageSearch7;
			}

			String source8 = "NGV Gas";
			if (source.equals(source8)) {
				String addCoverageSearch8 = "B097A 	97A		";
				return addCoverageSearch8;
			}

			String source9 = "Compensation for Assessed Repair Time";
			if (source.equals(source9)) {
				String addCoverageSearch9 = "   	112		";
				return addCoverageSearch9;
			}

			String source10 = "ADD NAMED DRIVER";
			if (source.equals(source10)) {
				String addCoverageSearch10 = " ";
				return addCoverageSearch10;
			}

			String source11 = "Strike, Riot & Civil Commotion";
			if (source.equals(source11)) {
				String addCoverageSearch11 = "B025	25		";
				return addCoverageSearch11;
			} else {
				return "";
			} // for dummy else
		} else {
			return "";
		}

	}

	public String getAdditionalCoverageValueBM(String source) {

		if (source != null) {
			String source1 = "Perlindungan Liabiliti Undang-Undang Penumpang";
			if (source.equals(source1)) {
				String addCoverageSearch1 = "B100	100		";
				return addCoverageSearch1;
			}
			String source2 = "Liabiliti Undang-Undang kepada Penumpang";
			if (source.equals(source2)) {
				String addCoverageSearch2 = "B072	72		";
				return addCoverageSearch2;
			}

			String source3 = "Manfaat Banjir Tambahan";
			if (source.equals(source3)) {
				String addCoverageSearch3 = "B057	57		";
				return addCoverageSearch3;
			}

			String source4 = "Manfaat Banjir Asas";
			if (source.equals(source4)) {
				String addCoverageSearch4 = "C57A	  		";
				return addCoverageSearch4;
			}

			String source5 = "Pelepasan Diskaun Tanpa Tuntutan";
			if (source.equals(source5)) {
				String addCoverageSearch5 = "   	111		";
				return addCoverageSearch5;
			}

			String source6 = "Perlindungan Cermin Kenderaan";
			if (source.equals(source6)) {
				String addCoverageSearch6 = "B089	89		";
				return addCoverageSearch6;
			}

			String source7 = "Eksesori Kenderaan";
			if (source.equals(source7)) {
				String addCoverageSearch7 = "B097	97		";
				return addCoverageSearch7;
			}

			String source8 = "Peralatan Penukaran Gas dan Tangki";
			if (source.equals(source8)) {
				String addCoverageSearch8 = "B097A 	97A		";
				return addCoverageSearch8;
			}

			String source9 = "Pampasan Untuk Pembaikan Taksiran (CART)";
			if (source.equals(source9)) {
				String addCoverageSearch9 = "   	112		";
				return addCoverageSearch9;
			}

			String source10 = "Pemandu Tambahan";
			if (source.equals(source10)) {
				String addCoverageSearch10 = " ";
				return addCoverageSearch10;
			}

			String source11 = "Mogok, Rusuhan dan Kekacauan Awam";
			if (source.equals(source11)) {
				String addCoverageSearch11 = "B025	25		";
				return addCoverageSearch11;
			} else {
				return "";
			} // for dummy else
		} else {
			return "";
		}

	}
}
