package com.etiqa.dsp.dao.common.motor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Properties;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.dsp.dao.common.pojo.MotorInsuranceCustDetails;

import net.sf.jasperreports.engine.JRException;

public class MotorInsuranceAutoAssistForm {
	public String autoAssistFormMethod(MotorInsuranceCustDetails miVo)
			throws ClassNotFoundException, JRException, IOException, SQLException {

		// MotorInsuranceAutoAssistForm autoAssistFormMethod
		String fileName = miVo.getPOLICY_NUMBER() + "-AutoAssistForm.pdf";
		String policyDocCode = "AASTFORM";

		try (InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/dsp/sales/process/email/documents.properties")) {
			Properties prop = new Properties();
			prop.load(in);

			// String PdfFile = prop.getProperty("EPolicyFormPdfFile");

			// String PdfFile = "com/etiqa/dsp/policy/templates/MI_AutoAssistForm_EV.pdf";

			String PdfFile = "";
			if (miVo.getLangVal().equals("lan_en")) {
				PdfFile = "com/etiqa/dsp/policy/templates/MI_AutoAssistForm_EV.pdf";
			} else {
				PdfFile = "com/etiqa/dsp/policy/templates/MI_AutoAssistForm_BV.pdf";
			}
			InputStream input = this.getClass().getClassLoader().getResourceAsStream(PdfFile);
			FileOutputStream fos = new FileOutputStream(prop.getProperty("destinationPath") + fileName, true);
			int b = 0;
			while ((b = input.read()) != -1) {
				fos.write(b);
			}
			System.out.print("AutoAssist Gen Done!");
			System.out.println(prop.getProperty("destinationPath") + fileName + "Destination Path");
			System.out.println(prop.getProperty("sourcePath") + fileName + "Source Path");

			Connection con = ConnectionFactory.getConnection();

			File f = new File(prop.getProperty("sourcePath") + fileName);
			FileInputStream fr = new FileInputStream(f);
			CallableStatement cstmt = con.prepareCall("{CALL DSP_POLICY_DOC_INSERT(?,?,?,?,?)}");

			cstmt.setString(1, miVo.getPOLICY_NUMBER());
			cstmt.setString(2, policyDocCode);
			cstmt.setString(3, fileName);
			cstmt.setString(4, miVo.getCUSTOMER_NAME());
			cstmt.registerOutParameter(5, Types.VARCHAR);
			cstmt.execute();
			String StatusOfInsertion = cstmt.getString(5);

			System.out.println(StatusOfInsertion + " ID inserterd");
			return fileName;

		} catch (Exception e) {
			e.printStackTrace();
			return "failtoStore";
		}

	}
}
