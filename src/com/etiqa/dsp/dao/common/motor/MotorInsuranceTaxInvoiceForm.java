package com.etiqa.dsp.dao.common.motor;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.dsp.dao.common.pojo.MotorInsuranceCustDetails;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.ExporterInput;
import net.sf.jasperreports.export.OutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

public class MotorInsuranceTaxInvoiceForm {
	public String TaxInvoiceFormMethod(MotorInsuranceCustDetails miVo)

			throws ClassNotFoundException, JRException, IOException, SQLException {

		// MotorInsuranceTaxInvoiceForm TaxInvoiceFormMethod
		String fileName = miVo.getPOLICY_NUMBER() + "-TaxInvoiceForm.pdf";
		String policyDocCode = "TAXIFORM";

		Format formatter = new SimpleDateFormat("dd-MMM-yy");
		String sysDate = formatter.format(new Date());
		System.out.println("Today Date:" + sysDate);

		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("P_InvoiceNo1", miVo.getINVOICE_NO() == null ? "" : miVo.getINVOICE_NO());
		parameters.put("P_Date1", sysDate);
		parameters.put("P_Name1", miVo.getCUSTOMER_NAME());
		String AddressVal = "", add1 = "", add2 = "", add3 = "";
		if (miVo.getCUSTOMER_ADDRESS1() != null) {
			add1 = miVo.getCUSTOMER_ADDRESS1() + "\n";
		}
		if (miVo.getCUSTOMER_ADDRESS2() != null) {
			add2 = miVo.getCUSTOMER_ADDRESS2() + "\n";
		}
		if (miVo.getCUSTOMER_ADDRESS3() != null) {
			add3 = miVo.getCUSTOMER_ADDRESS3() + "\n";
		}

		if (miVo.getCUSTOMER_STATE() != null) {
			AddressVal = miVo.getCUSTOMER_POSTCODE() + ",";
		}
		String CountryPincode = miVo.getCUSTOMER_STATE() + "," + "Malaysia";
		String fromDate = miVo.getPOLICY_EFFECTIVE_TIMESTAMP();
		String toDate = miVo.getPOLICY_EXPIRY_TIMESTAMP();
		fromDate = fromDate.substring(0, 2) + "/" + fromDate.substring(2, fromDate.length());
		fromDate = fromDate.substring(0, 5) + "/" + fromDate.substring(5, fromDate.length());
		toDate = toDate.substring(0, 2) + "/" + toDate.substring(2, toDate.length());
		toDate = toDate.substring(0, 5) + "/" + toDate.substring(5, toDate.length());

		parameters.put("P_Address1", add1 + add2 + add3 + AddressVal + CountryPincode);
		parameters.put("P_TypeOfPolicy1", "Motor Insurance");
		parameters.put("P_PolicyNO1", miVo.getPOLICY_NUMBER());
		parameters.put("P_FromDate1", fromDate);
		parameters.put("P_ToDate1", toDate);
		parameters.put("P_GrossPremium1", miVo.getGROSSPREMIUM_FINAL());
		parameters.put("P_Discount1", miVo.getDISCOUNT_AMOUNT());
		parameters.put("P_TotalExGST1", miVo.getPREMIUM_AFTER_DISCOUNT());
		parameters.put("P_Gst1", miVo.getGST_ON_POLICY_PREMIUM());
		parameters.put("P_StampDuty1", miVo.getSTAMP_DUTY_POLICY_PREMIUM());
		Double premiumAfterGst = new Double(miVo.getPREMIUM_AFTER_GST());
		Double StampDuty = new Double(miVo.getSTAMP_DUTY_POLICY_PREMIUM());
		Double TotalDue = premiumAfterGst + StampDuty;
		parameters.put("P_TotalPremium1", String.format("%.2f", TotalDue));
		parameters.put("P_ChkBox", "Y");
		parameters.put("P_AgentCode1", miVo.getAGENT_CODE() == null ? "" : miVo.getAGENT_CODE());
		try (InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/dsp/sales/process/email/documents.properties")) {
			Properties prop = new Properties();
			prop.load(in);

			Connection con = ConnectionFactory.getConnection();
			// String jrxmlFile =
			// "com/etiqa/dsp/policy/templates/MI_TaxInvoicForm_EV.jrxml";
			String jrxmlFile = "";
			if (miVo.getLangVal().equals("lan_en")) {
				jrxmlFile = "com/etiqa/dsp/policy/templates/MI_TaxInvoicForm_EV.jrxml";
			} else {
				jrxmlFile = "com/etiqa/dsp/policy/templates/MI_TaxInvoicForm_BV.jrxml";
			}
			InputStream input = this.getClass().getClassLoader().getResourceAsStream(jrxmlFile);
			JasperReport jasperReport = JasperCompileManager.compileReport(input);
			JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
			// JasperExportManager.exportReportToPdfFile(print,
			// prop.getProperty("destinationPath")+fileName);
			JRPdfExporter exporter = new JRPdfExporter();
			ExporterInput exporterInput = new SimpleExporterInput(print);
			exporter.setExporterInput(exporterInput);
			OutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(
					prop.getProperty("destinationPath") + fileName);
			exporter.setExporterOutput(exporterOutput);
			SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
			exporter.setConfiguration(configuration);
			exporter.exportReport();
			System.out.print("PDPA Pdf Gen Done!");

			System.out.println(prop.getProperty("destinationPath") + fileName + "Destination Path");
			System.out.println(prop.getProperty("sourcePath") + fileName + "Source Path");

			File fileDb = new File(prop.getProperty("sourcePath") + fileName);
			FileInputStream fr = new FileInputStream(fileDb);
			CallableStatement cstmt = con.prepareCall("{CALL DSP_POLICY_DOC_INSERT(?,?,?,?,?)}");

			cstmt.setString(1, miVo.getPOLICY_NUMBER());
			cstmt.setString(2, policyDocCode);
			cstmt.setString(3, fileName);
			cstmt.setString(4, miVo.getCUSTOMER_NAME());
			cstmt.registerOutParameter(5, Types.VARCHAR);
			cstmt.execute();
			String StatusOfInsertion = cstmt.getString(5);

			System.out.println(StatusOfInsertion + " ID inserterd");
			return fileName;

		} catch (Exception e) {
			e.printStackTrace();
			return "failtoStore";
		}

	}

	public String TaxInvoiceDppaFormMethod(MotorInsuranceCustDetails miVo) {
		// MotorInsuranceTaxInvoiceForm TaxInvoiceFormMethod
		String fileName = miVo.getPOLICY_NUMBER() + "-TaxInvoiceForm.pdf";
		String policyDocCode = "TAXWDPPA";

		Format formatter = new SimpleDateFormat("dd-MMM-yy");
		String sysDate = formatter.format(new Date());
		System.out.println("Today Date:" + sysDate);

		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("P_InvoiceNo1", miVo.getINVOICE_NO() == null ? "" : miVo.getINVOICE_NO());
		parameters.put("P_Date1", sysDate);
		parameters.put("P_Name1", miVo.getCUSTOMER_NAME());
		String AddressVal = "", add1 = "", add2 = "", add3 = "";
		if (miVo.getCUSTOMER_ADDRESS1() != null) {
			add1 = miVo.getCUSTOMER_ADDRESS1() + "\n";
		}
		if (miVo.getCUSTOMER_ADDRESS2() != null) {
			add2 = miVo.getCUSTOMER_ADDRESS2() + "\n";
		}
		if (miVo.getCUSTOMER_ADDRESS3() != null) {
			add3 = miVo.getCUSTOMER_ADDRESS3() + "\n";
		}

		if (miVo.getCUSTOMER_STATE() != null) {
			AddressVal = miVo.getCUSTOMER_POSTCODE() + ",";
		}
		String CountryPincode = miVo.getCUSTOMER_STATE() + "," + "Malaysia";
		String fromDate = miVo.getPOLICY_EFFECTIVE_TIMESTAMP();
		String toDate = miVo.getPOLICY_EXPIRY_TIMESTAMP();
		fromDate = fromDate.substring(0, 2) + "/" + fromDate.substring(2, fromDate.length());
		fromDate = fromDate.substring(0, 5) + "/" + fromDate.substring(5, fromDate.length());
		toDate = toDate.substring(0, 2) + "/" + toDate.substring(2, toDate.length());
		toDate = toDate.substring(0, 5) + "/" + toDate.substring(5, toDate.length());

		parameters.put("P_Address1", add1 + add2 + add3 + AddressVal + CountryPincode);
		parameters.put("P_TypeOfPolicy1", "Motor Insurance");
		parameters.put("P_PolicyNO1", miVo.getPOLICY_NUMBER());
		parameters.put("P_FromDate1", fromDate);
		parameters.put("P_ToDate1", toDate);
		parameters.put("P_GrossPremium1", miVo.getGROSSPREMIUM_FINAL());
		parameters.put("P_Discount1", miVo.getDISCOUNT_AMOUNT());
		parameters.put("P_TotalExGST1", miVo.getPREMIUM_AFTER_DISCOUNT());
		parameters.put("P_Gst1", miVo.getGST_ON_POLICY_PREMIUM());
		parameters.put("P_StampDuty1", miVo.getSTAMP_DUTY_POLICY_PREMIUM());
		Double premiumAfterGst = new Double(miVo.getPREMIUM_AFTER_GST());
		Double StampDuty = new Double(miVo.getSTAMP_DUTY_POLICY_PREMIUM());
		Double TotalDue = premiumAfterGst + StampDuty;
		parameters.put("P_TotalPremium1", String.format("%.2f", TotalDue));
		parameters.put("P_ChkBox", "Y");

		parameters.put("P_InvoiceNo2", miVo.getINVOICE_NO());
		parameters.put("P_Date2", sysDate);
		parameters.put("P_Name2", miVo.getCUSTOMER_NAME());
		/*
		 * String AddressVal="",add1="",add2="",add3="";
		 * if(miVo.getCUSTOMER_ADDRESS1()!=null){add1=miVo.getCUSTOMER_ADDRESS1()+"\n";}
		 * if(miVo.getCUSTOMER_ADDRESS2()!=null){add2=miVo.getCUSTOMER_ADDRESS2()+"\n";}
		 * if(miVo.getCUSTOMER_ADDRESS3()!=null){add3=miVo.getCUSTOMER_ADDRESS3()+"\n";}
		 *
		 * if(miVo.getCUSTOMER_STATE()!=null){AddressVal=miVo.getCUSTOMER_STATE()+",";}
		 * String CountryPincode=miVo.getCUSTOMER_POSTCODE()+","+"Malaysia"; String
		 * fromDate= miVo.getPOLICY_EFFECTIVE_TIMESTAMP(); String
		 * toDate=miVo.getPOLICY_EXPIRY_TIMESTAMP(); fromDate = fromDate.substring(0, 2)
		 * + "/" + fromDate.substring(2, fromDate.length()); fromDate=
		 * fromDate.substring(0, 5) + "/" + fromDate.substring(5, fromDate.length());
		 * toDate = toDate.substring(0, 2) + "/" + toDate.substring(2, toDate.length());
		 * toDate= toDate.substring(0, 5) + "/" + toDate.substring(5, toDate.length());
		 */

		parameters.put("P_Address2", add1 + add2 + add3 + AddressVal + CountryPincode);
		// Khas Perlindungan Kemalangan Kereta

		if (miVo.getLangVal().equals("lan_en")) {
			parameters.put("P_TypeOfPolicy2", "Car Accident Protection Special");
		} else {
			parameters.put("P_TypeOfPolicy2", "Khas Perlindungan Kemalangan Kereta");
		}

		parameters.put("P_PolicyNO2", miVo.getDppaPolicyNo());
		parameters.put("P_FromDate2", fromDate);
		parameters.put("P_ToDate2", toDate);
		parameters.put("P_GrossPremium2", miVo.getPASSANGER_PA_PREMIUM());
		parameters.put("P_TotalExGST2", miVo.getPASSENGER_PREMIUM_AFTDIS());
		Double premium = new Double(miVo.getPASSANGER_PA_PREMIUM());
		Double premiumAfterDisc = new Double(miVo.getPASSENGER_PREMIUM_AFTDIS());
		Double TotalDueDppa = premium - premiumAfterDisc;
		parameters.put("P_Discount2", String.format("%.2f", TotalDueDppa));
		parameters.put("P_Gst2", miVo.getGST_PASSANGER_PA_PREMIUM());
		parameters.put("P_StampDuty2", miVo.getSTAMP_DUTY_PASSANGER_PA_DIRECT());
		parameters.put("P_TotalPremium2", miVo.getPASSANGER_PA_PREMIUM_PAYABLE());
		parameters.put("P_AgentCode1", miVo.getAGENT_CODE() == null ? "" : miVo.getAGENT_CODE());
		parameters.put("P_AgentCode2", "N0028758");

		parameters.put("P_ChkBox", "Y");

		try (InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/dsp/sales/process/email/documents.properties")) {
			Properties prop = new Properties();
			prop.load(in);

			Connection con = ConnectionFactory.getConnection();
			// String jrxmlFile =
			// "com/etiqa/dsp/policy/templates/MI_TaxInvoicForm_EV_Both.jrxml";
			String jrxmlFile = "";
			if (miVo.getLangVal().equals("lan_en")) {
				jrxmlFile = "com/etiqa/dsp/policy/templates/MI_TaxInvoicForm_EV_Both.jrxml";
			} else {
				jrxmlFile = "com/etiqa/dsp/policy/templates/MI_TaxInvoicForm_BV_Both.jrxml";
			}
			InputStream input = this.getClass().getClassLoader().getResourceAsStream(jrxmlFile);
			JasperReport jasperReport = JasperCompileManager.compileReport(input);
			JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
			// JasperExportManager.exportReportToPdfFile(print,
			// prop.getProperty("destinationPath")+fileName);
			JRPdfExporter exporter = new JRPdfExporter();
			ExporterInput exporterInput = new SimpleExporterInput(print);
			exporter.setExporterInput(exporterInput);
			OutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(
					prop.getProperty("destinationPath") + fileName);
			exporter.setExporterOutput(exporterOutput);
			SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
			exporter.setConfiguration(configuration);
			exporter.exportReport();
			System.out.print("PDPA Pdf Gen Done!");
			// System.out.println(prop.getProperty("destinationPath")+fileName+"Destination
			// Path");
			// System.out.println(prop.getProperty("sourcePath")+fileName+"Source Path");

			File fileDb = new File(prop.getProperty("sourcePath") + fileName);
			FileInputStream fr = new FileInputStream(fileDb);
			CallableStatement cstmt = con.prepareCall("{CALL DSP_POLICY_DOC_INSERT(?,?,?,?,?)}");

			cstmt.setString(1, miVo.getPOLICY_NUMBER());
			cstmt.setString(2, policyDocCode);
			cstmt.setString(3, fileName);
			cstmt.setString(4, miVo.getCUSTOMER_NAME());
			cstmt.registerOutParameter(5, Types.VARCHAR);
			cstmt.execute();
			String StatusOfInsertion = cstmt.getString(5);

			System.out.println(StatusOfInsertion + " ID inserterd");

			return fileName;

		} catch (Exception e) {
			e.printStackTrace();
			return "failtoStore";
		}
	}
}
