package com.etiqa.dsp.dao.common.motor;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.etiqa.dsp.dao.common.pojo.GeneratingReportsResponseVo;
import com.etiqa.dsp.dao.common.pojo.MotorInsuranceCustDetails;
import com.etiqa.dsp.dao.common.pojo.custQuotPmntPolicyVo;
import com.etiqa.dsp.sales.process.email.DspEmailDispatchProcessor;
import com.etiqa.dsp.sales.process.email.ProductMailTemplateLoad;
import com.etiqa.dsp.sales.process.email.TermLifeProductMailTemplateLoad;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.PdfWriter;

import net.sf.jasperreports.engine.JRException;

public class MotorInsuranceExistsCustEmailService {
	static Properties mailServerProperties;
	static Session getMailSession;
	static MimeMessage generateMailMessage;
	private static Properties activeDirMailProp = new Properties();
	ProductMailTemplateLoad productTemplate = null;

	public String getStatusOfDocExistedCustomer(MotorInsuranceCustDetails micdvo, List<String> list,
			List<MotorInsuranceCustDetails> listOfAdditionalCoverage)
			throws ClassNotFoundException, JRException, IOException, SQLException {

		File statusOfZipFileGen = null;
		String emailStatus = null;
		try (InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/dsp/sales/process/email/documents.properties")) {
			Properties prop = new Properties();
			prop.load(in);
			callPasswordmail(micdvo.getCUSTOMER_NAME(), micdvo.getCUSTOMER_EMAIL());
			String zipFileName = micdvo.getPOLICY_NUMBER() + ".zip";

			File f = new File(prop.getProperty("destinationPath") + zipFileName);
			if (f.exists() && !f.isDirectory()) {
				System.out.println(zipFileName + "Already exists");
				statusOfZipFileGen = zipFilesGenPswd(micdvo, list);

				emailStatus = zipAndEmailService(micdvo, list, listOfAdditionalCoverage, statusOfZipFileGen);
			} else {
				File fBackUpFolder = new File(prop.getProperty("backUpPath") + zipFileName);
				if (fBackUpFolder.exists() && !fBackUpFolder.isDirectory()) {
					System.out.println(zipFileName + "Already exists");
					statusOfZipFileGen = zipFilesGenPswd(micdvo, list);

					emailStatus = zipAndEmailService(micdvo, list, listOfAdditionalCoverage, statusOfZipFileGen);
				} else {
					// Need to create all Doc
					MotorInsuranceReportsGenDao test = new MotorInsuranceReportsGenDaoImpl();

					// ******* Here need to generate docs without insert into db and need to
					// generate in backup folder and before gen confirm with language
					// List<String> status=test.getMIGenReportResponse(micdvo.getQQ_ID());
					// System.out.println("Email Status FOr Created FIles in folder"+status);
				}

				System.out.println("Email Status FOr Existed FIles in folder" + emailStatus);

			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return "success";
	}

	public File zipFilesGenPswd(MotorInsuranceCustDetails cdvo, List<String> FileNamelist) {
		String path = "", SourcePath = "";
		try (InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/dsp/sales/process/email/documents.properties")) {
			Properties prop = new Properties();
			prop.load(in);
			path = prop.getProperty("tempPath");
			SourcePath = prop.getProperty("sourcePath");
		} catch (Exception e) {
			e.printStackTrace();
		}

		File theDir = new File(path + cdvo.getPOLICY_NUMBER());

		// if the directory does not exist, create it
		if (!theDir.exists()) {
			System.out.println("creating directory: " + cdvo.getPOLICY_NUMBER());
			boolean result = false;

			try {
				theDir.mkdir();
				result = true;
			} catch (SecurityException se) {
				// handle it
			}
			if (result) {
				System.out.println("DIR created");
			}
		}
		String dob = cdvo.getCUSTOMER_DOB().replace("/", "");
		System.out.println(dob + "DOB value");

		if (dob.length() == 7) {
			dob = "0" + dob;
		}
		String lastDigitnric = cdvo.getCUSTOMER_NRIC_ID().substring(cdvo.getCUSTOMER_NRIC_ID().length() - 4);
		String pswd = dob + lastDigitnric;
		String USER_PASSWORD = pswd;
		String OWNER_PASSWORD = pswd;

		if (theDir.exists()) {

			try {

				for (String fileName : FileNamelist) {
					String filename = fileName;
					System.out.println("File Name in Zip class: " + filename);
					try {
						PdfReader pdfReader = new PdfReader(SourcePath + filename);
						PdfStamper pdfStamper = new PdfStamper(pdfReader,
								new FileOutputStream(path + cdvo.getPOLICY_NUMBER() + "/" + filename));
						pdfStamper.setEncryption(USER_PASSWORD.getBytes(), OWNER_PASSWORD.getBytes(),
								PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_128);
						pdfStamper.close();
						System.out.println(path + cdvo.getPOLICY_NUMBER() + filename + "File Created");
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (DocumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			System.out.println("folder doesn't exist");
		}

		FileOutputStream fos = null;
		ZipOutputStream zipOut = null;
		FileInputStream fis = null;
		String StatusOfZipGen = "";
		String zipFileName = cdvo.getPOLICY_NUMBER() + ".zip";

		try {

			fos = new FileOutputStream(path + cdvo.getPOLICY_NUMBER() + "/" + zipFileName);
			zipOut = new ZipOutputStream(new BufferedOutputStream(fos));
			for (String fileName : FileNamelist) {
				String filename = fileName;
				System.out.println("File Name in Zip class: " + filename);
				File input = new File(path + cdvo.getPOLICY_NUMBER() + "/" + filename);
				fis = new FileInputStream(input);
				ZipEntry ze = new ZipEntry(input.getName());
				System.out.println("Zipping the file: " + input.getName());
				zipOut.putNextEntry(ze);
				byte[] tmp = new byte[4 * 1024];
				int size = 0;
				while ((size = fis.read(tmp)) != -1) {
					zipOut.write(tmp, 0, size);
				}
				zipOut.flush();
				fis.close();
			}
			zipOut.close();
			System.out.println("Done...Pswd Zipped the files...");
			StatusOfZipGen = "successfully gen pswd zip file";

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			StatusOfZipGen = "FileNotFoundException pswd";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			StatusOfZipGen = "IOException pswd";
		} finally {
			try {
				if (fos != null) {
					fos.close();
				}
			} catch (Exception ex) {

			}
		}

		return theDir;
	}

	public boolean deleteDirectory(File theDir) {
		if (theDir.exists()) {
			File[] files = theDir.listFiles();
			if (null != files) {
				for (int i = 0; i < files.length; i++) {
					if (files[i].isDirectory()) {
						deleteDirectory(files[i]);
					} else {
						files[i].delete();
					}
				}
			}
		}
		return theDir.delete();

	}

	public String zipAndEmailService(MotorInsuranceCustDetails micdvo, List<String> list,
			List<MotorInsuranceCustDetails> listOfAdditionalCoverage, File statusOfZipFileGen) {

		custQuotPmntPolicyVo custVo = new custQuotPmntPolicyVo();
		custVo.setCUSTOMER_NAME(micdvo.getCUSTOMER_NAME());
		custVo.setCUSTOMER_EMAIL(micdvo.getCUSTOMER_EMAIL());
		custVo.setPOLICY_NUMBER(micdvo.getPOLICY_NUMBER());
		custVo.setVehRegNo(micdvo.getREGISTRATION_NUMBER());
		custVo.setPOLICY_EXPIRY_TIMESTAMP(micdvo.getPOLICY_EXPIRY_TIMESTAMP());

		List<GeneratingReportsResponseVo> listFileListForMail = new ArrayList<GeneratingReportsResponseVo>();

		for (String fileName : list) {
			String filename = fileName;
			GeneratingReportsResponseVo grrvo = new GeneratingReportsResponseVo();
			grrvo.setFileName(filename);
			listFileListForMail.add(grrvo);
		}

		DspEmailDispatchProcessor emailProcessor = new DspEmailDispatchProcessor(new TermLifeProductMailTemplateLoad());
		String status = null;
		try {
			status = emailProcessor.emailDispatchProcess(custVo, listFileListForMail, listOfAdditionalCoverage);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(status + "  :email status OF MI");

		// Deleting pswd zip file
		boolean DeleteResult = deleteDirectory(statusOfZipFileGen);
		if (DeleteResult) {
			System.out.println("Deleted Successfully" + statusOfZipFileGen);
		} else {
			System.out.println("Deleted Not Successfully" + statusOfZipFileGen);
		}

		return "Email successfully sent";
	}

	private void callPasswordmail(String name, String emailtosend) {
		try {
			String message = "";
			message += "<b>Dear   " + name + ",</b><br><br><br>";
			message += "Congratulations! Your transaction is successful. We are pleased to extend our warmest welcome to you for being a part of our Etiqa family. <br><br>";
			// message += "We will send another email with compressed attachment file. The
			// attachment password is <b>" +pollicypassword+ "</b><br><br>";
			message += "Your e-Documents are password protected. Your password to access your e-Documents is a combination your date of birth in <ddmmyyy> format and the last 4 digits of your NRIC or ID. <br><br>";
			// message += "Please use password to open your e-Documents. Your password is
			// your date of birth in ddMMyyyy format and combined with last 4 digits of you
			// NRIC.<br><br>";
			// message += "Example: If your Date of Birth is : 06-09-1986 (06-Sep-1986)
			// <br><br>";
			// message += " and your NRIC is : 860906512345<br><br>";
			message += "<b>Example: If your date of birth is 6 September 1986 and your NRIC is 860906-51-2345, your password is 060919862345</b><br><br>";

			message += "Note: This is an auto email notification. Please do not reply this email. You may call our Etiqa Oneline at 1300 13 8888 or email us at info@etiqa.com.my for any enquiries<br><br>";

			message += "Thank you.<br><br>";
			message += "Yours Sincerely,<br>";
			message += "Etiqa Insurance Berhad<br><br>";

			String subject = "Etiqa Motor Insurance e-Policy Password";
			loadEmailProps();
			
			// Assuming you are sending email from localhost
			String host = activeDirMailProp.getProperty("emailhost");
			String fromemail = activeDirMailProp.getProperty("from");
			String port = activeDirMailProp.getProperty("emailport");

			
			  Properties properties = new Properties(); 
			  properties.put("mail.smtp.host",host); 
			  properties.put("mail.smtp.port", port);
			  properties.put("mail.smtp.auth", "false");
			  properties.put("mail.smtp.starttls.enable", "false");
			  properties.put("mail.smtp.ssl.trust", host);
			  Session session = Session.getDefaultInstance(properties);

		
					
			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress("myaccount@etiqa.com.my"));
			InternetAddress[] toAddresses = { new InternetAddress(emailtosend) };
			msg.setRecipients(Message.RecipientType.TO, toAddresses);
			msg.setSubject(subject);
			msg.setSentDate(new Date());
			// set plain text message
			msg.setContent(message, "text/html");
			// sends the e-mail
			Transport.send(msg);
			System.out.println("Successfully to sent email.");
		} catch (Exception ex) {
			System.out.println("Failed to sent email.");
			ex.printStackTrace();
		}
	}
	
	// Load the email properties
		private void loadEmailProps() {
			InputStream infoad = this.getClass().getClassLoader()
					.getResourceAsStream("com/etiqa/dsp/sales/process/email/emailconfig.properties");
			try {
				activeDirMailProp.load(infoad);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
}
