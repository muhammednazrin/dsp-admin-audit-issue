package com.etiqa.dsp.dao.common.motor;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.etiqa.dsp.dao.common.pojo.GeneratingReportsResponseVo;
import com.etiqa.dsp.dao.common.pojo.custQuotPmntPolicyVo;
import com.etiqa.dsp.sales.process.email.ProductMailTemplateLoad;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.PdfWriter;

public class MotorInsuranceZipFileGeneration {
	static Properties mailServerProperties;
	static Session getMailSession;
	static MimeMessage generateMailMessage;
	private static Properties activeDirMailProp = new Properties();
	ProductMailTemplateLoad productTemplate = null;

	public File zipFilesGen(custQuotPmntPolicyVo cdvo, List<GeneratingReportsResponseVo> FileNamelist) {
		FileOutputStream fos = null;
		ZipOutputStream zipOut = null;
		FileInputStream fis = null;
		String StatusOfZipGen = "";
		String zipFileName = cdvo.getPOLICY_NUMBER() + ".zip";
		File StatusOfZipGenPswd = null;
		try (InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/dsp/sales/process/email/documents.properties")) {
			Properties prop = new Properties();
			prop.load(in);

			fos = new FileOutputStream(prop.getProperty("destinationPath") + zipFileName);
			zipOut = new ZipOutputStream(new BufferedOutputStream(fos));
			for (GeneratingReportsResponseVo fileName : FileNamelist) {
				String filename = fileName.getFileName();
				System.out.println("File Name in Zip class: " + filename);
				File input = new File(prop.getProperty("sourcePath") + filename);
				fis = new FileInputStream(input);
				ZipEntry ze = new ZipEntry(input.getName());
				System.out.println("Zipping the file: " + input.getName());
				zipOut.putNextEntry(ze);
				byte[] tmp = new byte[4 * 1024];
				int size = 0;
				while ((size = fis.read(tmp)) != -1) {
					zipOut.write(tmp, 0, size);
				}
				zipOut.flush();
				fis.close();
			}
			zipOut.close();
			System.out.println("Done... Zipped the files...");
			StatusOfZipGen = "successfully gen zip file";

			StatusOfZipGenPswd = zipFilesGenPswd(cdvo, FileNamelist);
			System.out.println(StatusOfZipGenPswd);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			StatusOfZipGen = "FileNotFoundException";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			StatusOfZipGen = "IOException";
		} finally {
			try {
				if (fos != null) {
					fos.close();
				}
			} catch (Exception ex) {

			}
		}

		return StatusOfZipGenPswd;
	}

	public File zipFilesGenPswd(custQuotPmntPolicyVo cdvo, List<GeneratingReportsResponseVo> FileNamelist) {
		String path = "", SourcePath = "";
		try (InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/dsp/sales/process/email/documents.properties")) {
			Properties prop = new Properties();
			prop.load(in);
			path = prop.getProperty("tempPath");
			SourcePath = prop.getProperty("sourcePath");
		} catch (Exception e) {
			e.printStackTrace();
		}

		File theDir = new File(path + cdvo.getPOLICY_NUMBER());

		// if the directory does not exist, create it
		if (!theDir.exists()) {
			System.out.println("creating directory: " + cdvo.getPOLICY_NUMBER());
			boolean result = false;

			try {
				theDir.mkdir();
				result = true;
			} catch (SecurityException se) {
				// handle it
			}
			if (result) {
				System.out.println("DIR created");
			}
		}
		System.out.println("dob" + cdvo.getCUSTOMER_DOB());
		String dob = cdvo.getCUSTOMER_DOB().replace("/", "");
		if (dob.length() == 7) {
			dob = "0" + dob;
		}
		String lastDigitnric = cdvo.getCUSTOMER_NRIC_ID().substring(cdvo.getCUSTOMER_NRIC_ID().length() - 4);
		String pswd = dob + lastDigitnric;
		String USER_PASSWORD = pswd;
		String OWNER_PASSWORD = pswd;

		if (theDir.exists()) {

			try {

				for (GeneratingReportsResponseVo fileName : FileNamelist) {
					String filename = fileName.getFileName();
					System.out.println("File Name in Zip class: " + filename);
					try {
						PdfReader pdfReader = new PdfReader(SourcePath + filename);
						PdfStamper pdfStamper = new PdfStamper(pdfReader,
								new FileOutputStream(path + cdvo.getPOLICY_NUMBER() + "/" + filename));
						pdfStamper.setEncryption(USER_PASSWORD.getBytes(), OWNER_PASSWORD.getBytes(),
								PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_128);
						pdfStamper.close();
						System.out.println(path + cdvo.getPOLICY_NUMBER() + filename + "File Created");
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (DocumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			System.out.println("folder doesn't exist");
		}

		FileOutputStream fos = null;
		ZipOutputStream zipOut = null;
		FileInputStream fis = null;
		String StatusOfZipGen = "";
		String zipFileName = cdvo.getPOLICY_NUMBER() + ".zip";

		try {

			fos = new FileOutputStream(path + cdvo.getPOLICY_NUMBER() + "/" + zipFileName);
			zipOut = new ZipOutputStream(new BufferedOutputStream(fos));
			for (GeneratingReportsResponseVo fileName : FileNamelist) {
				String filename = fileName.getFileName();
				System.out.println("File Name in Zip class: " + filename);
				File input = new File(path + cdvo.getPOLICY_NUMBER() + "/" + filename);
				fis = new FileInputStream(input);
				ZipEntry ze = new ZipEntry(input.getName());
				System.out.println("Zipping the file: " + input.getName());
				zipOut.putNextEntry(ze);
				byte[] tmp = new byte[4 * 1024];
				int size = 0;
				while ((size = fis.read(tmp)) != -1) {
					zipOut.write(tmp, 0, size);
				}
				zipOut.flush();
				fis.close();
			}
			zipOut.close();
			System.out.println("Done...Pswd Zipped the files...");
			StatusOfZipGen = "successfully gen pswd zip file";

			// ******************************** Send to e-mail Customer regarding pswd
			// format******************

			/* if(cdvo.getLangValue().equals("lan_en")){ */
			callPasswordmail(cdvo.getCUSTOMER_NAME(), cdvo.getCUSTOMER_EMAIL(), "");
			/*
			 * }else{
			 * callPasswordmailBM(cdvo.getCUSTOMER_NAME(),cdvo.getCUSTOMER_EMAIL(),""); }
			 */

			// ******************************** Deleting folder from tmp
			// folder******************

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			StatusOfZipGen = "FileNotFoundException pswd";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			StatusOfZipGen = "IOException pswd";
		} finally {
			try {
				if (fos != null) {
					fos.close();
				}
			} catch (Exception ex) {

			}
		}
		/*
		 * FileOutputStream fos = null; ZipOutputStream zipOut = null; FileInputStream
		 * fis = null; String StatusOfZipGen=""; String
		 * zipFileName=cdvo.getPOLICY_NUMBER()+".zip";
		 *
		 * try( InputStream in = this.getClass().getClassLoader().getResourceAsStream(
		 * "com/etiqa/dsp/sales/process/email/documents.properties")){ Properties prop =
		 * new Properties(); prop.load(in);
		 *
		 *
		 * ZipParameters zipParameters = new ZipParameters(); // Set how you want to
		 * encrypt files
		 * zipParameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
		 * zipParameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL); //
		 * Set encryption of files to true zipParameters.setEncryptFiles(true);
		 * //zipParameters. // Set encryption method
		 * zipParameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES); // Set key
		 * strength zipParameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
		 *
		 * String uuid = UUID.randomUUID().toString(); String uipass=(String)
		 * uuid.subSequence(0, 8); System.out.println("user Password = " + uipass); //
		 * Set password zipParameters.setPassword(uipass);
		 *
		 * // Create ZIP file ZipFile zipFile = new
		 * ZipFile(prop.getProperty("destinationPath")+zipFileName);
		 *
		 * for(GeneratingReportsResponseVo fileName : FileNamelist){ String filename=
		 * fileName.getFileName();
		 * System.out.println("File Name in Zip class: "+filename); File input = new
		 * File(prop.getProperty("sourcePath")+filename); zipFile.addFile(input,
		 * zipParameters);
		 *
		 * }
		 *
		 *
		 * callPasswordmail(cdvo.getCUSTOMER_NAME(),cdvo.getCUSTOMER_EMAIL(),uipass);
		 *
		 * System.out.println("Done... Zipped the files...");
		 * StatusOfZipGen="successfully gen zip file"; } catch (FileNotFoundException e)
		 * { e.printStackTrace(); StatusOfZipGen="FileNotFoundException"; } catch
		 * (IOException e) { e.printStackTrace(); StatusOfZipGen="IOException"; } catch
		 * (ZipException e) { // TODO Auto-generated catch block e.printStackTrace(); }
		 * finally{ try{ if(fos != null) fos.close(); } catch(Exception ex){
		 *
		 * } }
		 */
		return theDir;
	}

	public boolean deleteDirectory(File theDir) {
		if (theDir.exists()) {
			File[] files = theDir.listFiles();
			if (null != files) {
				for (int i = 0; i < files.length; i++) {
					if (files[i].isDirectory()) {
						deleteDirectory(files[i]);
					} else {
						files[i].delete();
					}
				}
			}
		}
		return theDir.delete();

	}

	private void callPasswordmail(String name, String emailtosend, String pollicypassword) {
		try {
			String message = "";
			message += "<b>Dear   " + name + ",</b><br><br><br>";
			message += "Congratulations! Your transaction is successful. We are pleased to extend our warmest welcome to you for being a part of our Etiqa family. <br><br>";
			// message += "We will send another email with compressed attachment file. The
			// attachment password is <b>" +pollicypassword+ "</b><br><br>";
			message += "Your e-Documents are password protected. Your password to access your e-Documents is a combination your date of birth in <ddmmyyy> format and the last 4 digits of your NRIC or ID. <br><br>";
			// message += "Please use password to open your e-Documents. Your password is
			// your date of birth in ddMMyyyy format and combined with last 4 digits of you
			// NRIC.<br><br>";
			// message += "Example: If your Date of Birth is : 06-09-1986 (06-Sep-1986)
			// <br><br>";
			// message += " and your NRIC is : 860906512345<br><br>";
			message += "<b>Example: If your date of birth is 6 September 1986 and your NRIC or ID is 860906-51-2345, your password is 060919862345</b><br><br>";

			message += "Note: This is an auto email notification. Please do not reply this email. You may call our Etiqa Oneline at 1300 13 8888 or email us at info@etiqa.com.my for any enquiries<br><br>";

			message += "Thank you.<br><br>";
			message += "Yours Sincerely,<br>";
			message += "Etiqa Insurance Berhad<br><br>";

			String subject = "Etiqa Motor Insurance e-Policy Password";
			loadEmailProps();
		
			// Assuming you are sending email from localhost
			String host = activeDirMailProp.getProperty("emailhost");
			String fromemail = activeDirMailProp.getProperty("from");
			String port = activeDirMailProp.getProperty("emailport");

			
			Properties properties = new Properties(); properties.put("mail.smtp.host",
			 host); properties.put("mail.smtp.port", port);
			 properties.put("mail.smtp.auth", "false");
			 properties.put("mail.smtp.starttls.enable", "false");
			

		
			Session session = Session.getInstance(properties);
			// session.setDebug(true);
			// creates a new e-mail message
			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress("myaccount@etiqa.com.my"));
			InternetAddress[] toAddresses = { new InternetAddress(emailtosend) };
			msg.setRecipients(Message.RecipientType.TO, toAddresses);
			msg.setSubject(subject);
			msg.setSentDate(new Date());
			// set plain text message
			msg.setContent(message, "text/html");
			// sends the e-mail
			Transport.send(msg);
			System.out.println("Successfully to sent email.");
		} catch (Exception ex) {
			System.out.println("Failed to sent email.");
			ex.printStackTrace();
		}
	}

	private void callPasswordmailBM(String name, String emailtosend, String pollicypassword) {
		try {
			String message = "";
			message += "<b>Kepada    " + name + ",</b><br><br><br>";
			message += "Tahniah! Transaksi anda telah berjaya. Kami gembira untuk mengalu-alukan kedatangan anda menjadi sebahagian daripada keluarga Etiqa kami.<br><br>";
			// message += "We will send another email with compressed attachment file. The
			// attachment password is <b>" +pollicypassword+ "</b><br><br>";
			message += "Sila gunakan kata laluan untuk membuka lampiran e-dokumen. Kata laluan anda adalah kombinasi tarikh lahir anda berdasarkan format <hhbbtttt> dan 4 digit terakhir dari nombor kad pengenalan atau ID anda.  <br><br>";
			// message += "Please use password to open your e-Documents. Your password is
			// your date of birth in ddMMyyyy format and combined with last 4 digits of you
			// NRIC.<br><br>";
			// message += "Example: If your Date of Birth is : 06-09-1986 (06-Sep-1986)
			// <br><br>";
			// message += " and your NRIC is : 860906512345<br><br>";
			message += "<b>Contoh: Jika tarikh hari lahir anda adalah 6 September 1986 dan nombor kad pengenalan atau ID anda adalah 860906-51-2345, kata laluan anda adalah 060919862345</b><br><br>";

			message += "Nota: Ini adalah emel pemberitahuan auto. Sila jangan membalas emel ini. Anda boleh menghubungi Etiqa Online kami di talian 1-300-13-8888 atau emel kepada kami di info@etiqa.com.my untuk sebarang pertanyaan.<br><br>";

			message += "Terima Kasih.<br><br>";
			message += "Yang Ikhlas,<br>";
			message += "Etiqa Insurance Berhad<br><br>";

			String subject = "Etiqa Motor Insurance e-Policy Password";
			loadEmailProps();
		
			// Assuming you are sending email from localhost
			String host = activeDirMailProp.getProperty("emailhost");
			String fromemail = activeDirMailProp.getProperty("from");
			String port = activeDirMailProp.getProperty("emailport");

			
			 Properties properties = new Properties(); properties.put("mail.smtp.host",
			 host); properties.put("mail.smtp.port", port);
			  properties.put("mail.smtp.auth", "false");
			 properties.put("mail.smtp.starttls.enable", "false");
		

		
		
			Session session = Session.getInstance(properties);
			// session.setDebug(true);
			// creates a new e-mail message
			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress("myaccount@etiqa.com.my"));
			InternetAddress[] toAddresses = { new InternetAddress(emailtosend) };
			msg.setRecipients(Message.RecipientType.TO, toAddresses);
			msg.setSubject(subject);
			msg.setSentDate(new Date());
			// set plain text message
			msg.setContent(message, "text/html");
			// sends the e-mail
			Transport.send(msg);
			System.out.println("Successfully to sent email.");
		} catch (Exception ex) {
			System.out.println("Failed to sent email.");
			ex.printStackTrace();
		}
	}
	
	// Load the email properties
	private void loadEmailProps() {
		InputStream infoad = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/dsp/sales/process/email/emailconfig.properties");
		try {
			activeDirMailProp.load(infoad);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
