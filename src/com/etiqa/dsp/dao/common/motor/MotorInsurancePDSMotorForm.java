package com.etiqa.dsp.dao.common.motor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.dsp.dao.common.pojo.MotorInsuranceCustDetails;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.ExporterInput;
import net.sf.jasperreports.export.OutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

public class MotorInsurancePDSMotorForm {
	public String pdsMotorFormMethod(MotorInsuranceCustDetails miVo)
			throws ClassNotFoundException, JRException, IOException, SQLException {

		// MotorInsuranceTaxInvoiceForm TaxInvoiceFormMethod
		String fileName = miVo.getPOLICY_NUMBER() + "-PDSMotorForm.pdf";
		String policyDocCode = "PDSMFORM";

		Format formatter = new SimpleDateFormat("dd-MMM-yy");
		String sysDate = formatter.format(new Date());
		System.out.println("Today Date:" + sysDate);

		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("P_Date", sysDate);

		try (InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/dsp/sales/process/email/documents.properties")) {
			Properties prop = new Properties();
			prop.load(in);

			Connection con = ConnectionFactory.getConnection();
			// String jrxmlFile = "com/etiqa/dsp/policy/templates/MI_PDSMotorForm_EV.jrxml";
			String jrxmlFile = "";
			if (miVo.getLangVal().equals("lan_en")) {
				jrxmlFile = "com/etiqa/dsp/policy/templates/MI_PDSMotorForm_EV.jrxml";
			} else {
				jrxmlFile = "com/etiqa/dsp/policy/templates/MI_PDSMotorForm_BV.jrxml";
			}
			InputStream input = this.getClass().getClassLoader().getResourceAsStream(jrxmlFile);
			JasperReport jasperReport = JasperCompileManager.compileReport(input);
			JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
			// JasperExportManager.exportReportToPdfFile(print,
			// prop.getProperty("destinationPath")+fileName);
			JRPdfExporter exporter = new JRPdfExporter();
			ExporterInput exporterInput = new SimpleExporterInput(print);
			exporter.setExporterInput(exporterInput);
			OutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(
					prop.getProperty("destinationPath") + fileName);
			exporter.setExporterOutput(exporterOutput);
			SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
			exporter.setConfiguration(configuration);
			exporter.exportReport();
			System.out.print("PDPA Pdf Gen Done!");

			System.out.println(prop.getProperty("destinationPath") + fileName + "Destination Path");
			System.out.println(prop.getProperty("sourcePath") + fileName + "Source Path");

			File fileDb = new File(prop.getProperty("sourcePath") + fileName);
			FileInputStream fr = new FileInputStream(fileDb);
			CallableStatement cstmt = con.prepareCall("{CALL DSP_POLICY_DOC_INSERT(?,?,?,?,?)}");

			cstmt.setString(1, miVo.getPOLICY_NUMBER());
			cstmt.setString(2, policyDocCode);
			cstmt.setString(3, fileName);
			cstmt.setString(4, miVo.getCUSTOMER_NAME());
			cstmt.registerOutParameter(5, Types.VARCHAR);
			cstmt.execute();
			String StatusOfInsertion = cstmt.getString(5);

			System.out.println(StatusOfInsertion + " ID inserterd");
			return fileName;

		} catch (Exception e) {
			e.printStackTrace();
			return "failtoStore";
		}

		/*
		 * //MotorInsurancePDSMotorForm pdsMotorFormMethod String
		 * fileName=miVo.getPOLICY_NUMBER()+"-PDSMotorForm.pdf"; String
		 * policyDocCode="PDSMFORM";
		 *
		 *
		 * try ( InputStream in = this.getClass().getClassLoader().getResourceAsStream(
		 * "com/etiqa/dsp/sales/process/email/documents.properties")){ Properties prop =
		 * new Properties(); prop.load(in);
		 *
		 *
		 * // String PdfFile = prop.getProperty("EPolicyFormPdfFile"); String PdfFile =
		 * "com/etiqa/dsp/policy/templates/MI_PDSMotorForm_EV.pdf"; InputStream input =
		 * this.getClass().getClassLoader().getResourceAsStream(PdfFile);
		 * FileOutputStream fos = new
		 * FileOutputStream(prop.getProperty("destinationPath")+fileName,true); int b =
		 * 0; while ((b = input.read()) != -1) { fos.write(b); }
		 * System.out.print("PdsMotor Gen Done!");
		 * System.out.println(prop.getProperty("destinationPath")+
		 * fileName+"Destination Path");
		 * System.out.println(prop.getProperty("sourcePath")+fileName+"Source Path");
		 *
		 * Connection con=ConnectionFactory.getConnection();
		 *
		 * File f=new File(prop.getProperty("sourcePath")+fileName); FileInputStream fr=
		 * new FileInputStream(f); CallableStatement cstmt =
		 * con.prepareCall("{CALL DSP_POLICY_DOC_INSERT(?,?,?,?,?)}");
		 *
		 * cstmt.setString(1,miVo.getPOLICY_NUMBER()); cstmt.setString(2,policyDocCode);
		 * cstmt.setString(3,fileName); cstmt.setString(4,miVo.getCUSTOMER_NAME());
		 * cstmt.registerOutParameter(5,Types.VARCHAR); cstmt.execute(); String
		 * StatusOfInsertion = cstmt.getString(5);
		 *
		 *
		 *
		 *
		 * System.out.println(StatusOfInsertion+" ID inserterd"); return fileName;
		 *
		 * } catch ( Exception e) { e.printStackTrace(); return "failtoStore"; }
		 */

	}

	public String pdsMotorDppaFormMethod(MotorInsuranceCustDetails miVo) {

		// MotorInsurancePDSMotorForm pdsMotorFormMethod
		String fileName = miVo.getPOLICY_NUMBER() + "-PDSMotor(CAPS)Form.pdf";
		String policyDocCode = "PDSMDPPA";

		Map<String, Object> parameters = new HashMap<String, Object>();
		Format formatter = new SimpleDateFormat("dd-MMM-yy");
		String sysDate = formatter.format(new Date());
		System.out.println("Today Date:" + sysDate);
		parameters.put("P_Date", sysDate);

		try (InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/dsp/sales/process/email/documents.properties")) {
			Properties prop = new Properties();
			prop.load(in);

			// String jrxmlFile =
			// "com/etiqa/dsp/policy/templates/MI_PDSMotorFormDppa_EV.jrxml";

			String jrxmlFile = "";
			if (miVo.getLangVal().equals("lan_en")) {
				jrxmlFile = "com/etiqa/dsp/policy/templates/MI_PDSMotorFormDppa_EV.jrxml";
			} else {
				jrxmlFile = "com/etiqa/dsp/policy/templates/MI_PDSMotorFormDppa_BV.jrxml";
			}
			InputStream input = this.getClass().getClassLoader().getResourceAsStream(jrxmlFile);
			JasperReport jasperReport = JasperCompileManager.compileReport(input);
			JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
			// JasperExportManager.exportReportToPdfFile(print,
			// prop.getProperty("destinationPath")+fileName);
			JRPdfExporter exporter = new JRPdfExporter();
			ExporterInput exporterInput = new SimpleExporterInput(print);
			exporter.setExporterInput(exporterInput);
			OutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(
					prop.getProperty("destinationPath") + fileName);
			exporter.setExporterOutput(exporterOutput);
			SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
			exporter.setConfiguration(configuration);
			exporter.exportReport();
			System.out.print("PDS Dppa Pdf Gen Done! Before Payment");

			Connection con = ConnectionFactory.getConnection();

			File f = new File(prop.getProperty("sourcePath") + fileName);
			FileInputStream fr = new FileInputStream(f);
			CallableStatement cstmt = con.prepareCall("{CALL DSP_POLICY_DOC_INSERT(?,?,?,?,?)}");

			cstmt.setString(1, miVo.getPOLICY_NUMBER());
			cstmt.setString(2, policyDocCode);
			cstmt.setString(3, fileName);
			cstmt.setString(4, miVo.getCUSTOMER_NAME());
			cstmt.registerOutParameter(5, Types.VARCHAR);
			cstmt.execute();
			String StatusOfInsertion = cstmt.getString(5);

			System.out.println(StatusOfInsertion + " ID inserterd");
			return fileName;

		} catch (Exception e) {
			e.printStackTrace();
			return "failtoStore";
		}
	}

	public String pdsMotorFormMethodBeforePayment(String todayDate, String langValue)
			throws ClassNotFoundException, JRException, IOException, SQLException {

		Format formatter = new SimpleDateFormat("dd-MMM-yy");
		String sysDate = formatter.format(new Date());
		System.out.println("Today Date:" + sysDate);
		String fileName = "";

		if (langValue.equals("lan_en")) {
			fileName = sysDate + "-PDSMotorForm.pdf";
		} else {
			fileName = sysDate + "-PDSMotorForm_BM.pdf";
		}

		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("P_Date", sysDate);

		try (InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/dsp/sales/process/email/documents.properties")) {
			Properties prop = new Properties();
			prop.load(in);
			// *************************************************** Motor PDS Doc Start Here
			// **********************************************************

			File f = new File(prop.getProperty("destinationPath") + fileName);
			if (f.exists() && !f.isDirectory()) {
				System.out.println(fileName + "Already exists");
			} else {
				// String jrxmlFile = "com/etiqa/dsp/policy/templates/MI_PDSMotorForm_EV.jrxml";

				String jrxmlFile = "";
				if (langValue.equals("lan_en")) {
					jrxmlFile = "com/etiqa/dsp/policy/templates/MI_PDSMotorForm_EV.jrxml";
				} else {
					jrxmlFile = "com/etiqa/dsp/policy/templates/MI_PDSMotorForm_BV.jrxml";
				}

				InputStream input = this.getClass().getClassLoader().getResourceAsStream(jrxmlFile);
				JasperReport jasperReport = JasperCompileManager.compileReport(input);
				JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
				// JasperExportManager.exportReportToPdfFile(print,
				// prop.getProperty("destinationPath")+fileName);
				JRPdfExporter exporter = new JRPdfExporter();
				ExporterInput exporterInput = new SimpleExporterInput(print);
				exporter.setExporterInput(exporterInput);
				OutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(
						prop.getProperty("destinationPath") + fileName);
				exporter.setExporterOutput(exporterOutput);
				SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
				exporter.setConfiguration(configuration);
				exporter.exportReport();
				System.out.print("PDPA Pdf Gen Done! Before Payment");

			}
			// *************************************************** Motor PDS Doc End Here
			// **********************************************************
			// ************************************************** Motor PDS DPPA Doc Start
			// Here **********************************************************
			String fileNamePdsDppa = "";
			if (langValue.equals("lan_en")) {
				fileNamePdsDppa = sysDate + "-PDSMotorDppaForm.pdf";
			} else {
				fileNamePdsDppa = sysDate + "-PDSMotorDppaForm_BM.pdf";
			}
			File fpdsDppa = new File(prop.getProperty("destinationPath") + fileNamePdsDppa);
			if (fpdsDppa.exists() && !fpdsDppa.isDirectory()) {
				System.out.println(fileNamePdsDppa + "Already exists");
			} else {
				// String jrxmlFile =
				// "com/etiqa/dsp/policy/templates/MI_PDSMotorFormDppa_EV.jrxml";

				String jrxmlFile = "";
				if (langValue.equals("lan_en")) {
					jrxmlFile = "com/etiqa/dsp/policy/templates/MI_PDSMotorFormDppa_EV.jrxml";
				} else {
					jrxmlFile = "com/etiqa/dsp/policy/templates/MI_PDSMotorFormDppa_BV.jrxml";
				}
				// MI_PDSMotorFormDppa_BV.jrxml
				InputStream input = this.getClass().getClassLoader().getResourceAsStream(jrxmlFile);
				JasperReport jasperReport = JasperCompileManager.compileReport(input);
				JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
				// JasperExportManager.exportReportToPdfFile(print,
				// prop.getProperty("destinationPath")+fileName);
				JRPdfExporter exporter = new JRPdfExporter();
				ExporterInput exporterInput = new SimpleExporterInput(print);
				exporter.setExporterInput(exporterInput);
				OutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(
						prop.getProperty("destinationPath") + fileNamePdsDppa);
				exporter.setExporterOutput(exporterOutput);
				SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
				exporter.setConfiguration(configuration);
				exporter.exportReport();
				System.out.print("PDS Dppa Pdf Gen Done! Before Payment");

			}
			// *************************************************** Motor PDS DPPA Doc End
			// Here **********************************************************
			// *************************************************** Motor Policy Control
			// Static Doc Start Here
			// **********************************************************
			String fileNamePolicyContract = sysDate + "-PolicyContract.pdf";
			if (langValue.equals("lan_en")) {
				fileNamePolicyContract = sysDate + "-PolicyContract.pdf";
			} else {
				fileNamePolicyContract = sysDate + "-PolicyContract_BM.pdf";
			}
			File fPolicyContract = new File(prop.getProperty("destinationPath") + fileNamePolicyContract);
			if (fPolicyContract.exists() && !fPolicyContract.isDirectory()) {
				System.out.println(fPolicyContract + "Already exists Policy Contract");
			} else {

				try {
					// String PdfFile =
					// "com/etiqa/dsp/policy/templates/MI_PolicyContractMotorForm_EV.pdf";
					String PdfFile = "";
					if (langValue.equals("lan_en")) {
						PdfFile = "com/etiqa/dsp/policy/templates/MI_PolicyContractMotorForm_EV.pdf";
					} else {
						PdfFile = "com/etiqa/dsp/policy/templates/MI_PolicyContractMotorForm_BV.pdf";
					}

					InputStream input = this.getClass().getClassLoader().getResourceAsStream(PdfFile);
					FileOutputStream fos = new FileOutputStream(
							prop.getProperty("destinationPath") + fileNamePolicyContract, true);
					int b = 0;
					while ((b = input.read()) != -1) {
						fos.write(b);
					}
					System.out.print("PdsMotor Gen Done!");
					System.out
							.println(prop.getProperty("destinationPath") + fileNamePolicyContract + "Destination Path");
					// *************************************************** Motor Policy Control
					// Static Doc End Here
					// **********************************************************

					return fileName;

				} catch (Exception e) {
					e.printStackTrace();
					return "failtoStore";
				}
			}

			// *************************************************** Motor Policy Control DPPA
			// Static Doc Start Here
			// **********************************************************
			String fileNamePolicyContractCAPS = sysDate + "-PolicyContractCAPS.pdf";
			if (langValue.equals("lan_en")) {
				fileNamePolicyContractCAPS = sysDate + "-PolicyContractCAPS.pdf";
			} else {
				fileNamePolicyContractCAPS = sysDate + "-PolicyContractCAPS_BM.pdf";
			}
			File fPolicyContractCAPS = new File(prop.getProperty("destinationPath") + fileNamePolicyContractCAPS);
			if (fPolicyContractCAPS.exists() && !fPolicyContractCAPS.isDirectory()) {
				System.out.println(fPolicyContractCAPS + "Already exists Policy Contract");
			} else {

				try {
					// String PdfFile =
					// "com/etiqa/dsp/policy/templates/MI_PolicyContractMotorForm_EV.pdf";
					String PdfFileCAPS = "";
					if (langValue.equals("lan_en")) {
						PdfFileCAPS = "com/etiqa/dsp/policy/templates/MI_PolicyContractCAPSMotorForm_EV.pdf";
					} else {
						PdfFileCAPS = "com/etiqa/dsp/policy/templates/MI_PolicyContractCAPSMotorForm_BV.pdf";
					}

					InputStream inputCAPS = this.getClass().getClassLoader().getResourceAsStream(PdfFileCAPS);
					FileOutputStream fosCAPS = new FileOutputStream(
							prop.getProperty("destinationPath") + fileNamePolicyContractCAPS, true);
					int bCAPS = 0;
					while ((bCAPS = inputCAPS.read()) != -1) {
						fosCAPS.write(bCAPS);
					}
					System.out.print("PdsMotor Gen Done!");
					System.out.println(
							prop.getProperty("destinationPath") + fileNamePolicyContractCAPS + "Destination Path");
					// *************************************************** Motor Policy Control DPPA
					// Static Doc End Here
					// **********************************************************

					return fileName;

				} catch (Exception e) {
					e.printStackTrace();
					return "failtoStore";
				}
			}

			return fileName;

		} catch (Exception e) {
			e.printStackTrace();
			return "failtoStore";
		}

	}

}
