package com.etiqa.dsp.dao.common.motor;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.common.DB.DBUtil;
import com.etiqa.dsp.dao.common.pojo.MotorInsuranceCustDetails;

import net.sf.jasperreports.engine.JRException;
import oracle.jdbc.OracleTypes;

public class MotorInsuranceReportsGenDaoImpl implements MotorInsuranceReportsGenDao {

	@Override
	public List<String> getMIGenReportResponse(String QQ_ID, String langValue) {
		Connection connection = null;
		Statement statement = null;
		ResultSet rs = null;
		ResultSet rsOfAdditionalValue = null;
		List<String> list = new ArrayList<String>();
		try {
			connection = ConnectionFactory.getConnection();
			System.out.println(QQ_ID + "Coming from ws input");
			CallableStatement cstmt = connection
					.prepareCall("{CALL DSP_MI_SP_TOTALINFO_SELECT(?,?,?,?,?,?,?,?,?,?,?)}");
			cstmt.setString(1, QQ_ID);
			cstmt.registerOutParameter(2, OracleTypes.CURSOR);
			cstmt.registerOutParameter(3, OracleTypes.VARCHAR);
			cstmt.registerOutParameter(4, OracleTypes.CURSOR);
			cstmt.registerOutParameter(5, OracleTypes.VARCHAR);
			cstmt.registerOutParameter(6, OracleTypes.VARCHAR);
			cstmt.registerOutParameter(7, OracleTypes.VARCHAR);
			cstmt.registerOutParameter(8, OracleTypes.VARCHAR);
			cstmt.registerOutParameter(9, OracleTypes.VARCHAR);
			cstmt.registerOutParameter(10, OracleTypes.VARCHAR);
			cstmt.registerOutParameter(11, OracleTypes.VARCHAR);
			cstmt.execute();

			MotorInsuranceCustDetails micdvo = new MotorInsuranceCustDetails();
			rs = (ResultSet) cstmt.getObject(2);
			micdvo.setINVOICE_NO(cstmt.getString(3));
			rsOfAdditionalValue = (ResultSet) cstmt.getObject(4);
			micdvo.setDppaPolicyNo(cstmt.getString(5));
			micdvo.setVehicle_Type(cstmt.getString(6));
			micdvo.setVEHICLE_SAFTEY(cstmt.getString(7));
			micdvo.setCUSTOMER_OCCUPATION(cstmt.getString(8));

			String antiTheft = cstmt.getString(9);
			String parkOverNight = cstmt.getString(10);

			String[] arrayantiTheft = new String[30];
			arrayantiTheft = antiTheft.split(",");
			String[] arrayparkOverNightTheft = new String[30];
			arrayparkOverNightTheft = parkOverNight.split(",");
			String temp_antiTheft = "";
			String temp_parkOverNight = "";
			for (int i = 0; i < arrayantiTheft.length; i++) {
				if (i != 0) {
					temp_antiTheft = temp_antiTheft + arrayantiTheft[i].trim() + "\n";
				}
			}
			for (int i = 0; i < arrayparkOverNightTheft.length; i++) {
				if (i != 0) {
					temp_parkOverNight = temp_parkOverNight + arrayparkOverNightTheft[i].trim() + "\n";
				}
			}
			micdvo.setVEHICLE_PARK_OVERNEIGHT(temp_parkOverNight);
			micdvo.setVEHICLE_ANTI_THEFT(temp_antiTheft);

			micdvo.setFINANCED_BY(cstmt.getString(11));

			while (rs.next()) {
				// rs.getString(columnLabel)
				micdvo.setCUSTOMER_ID(new Integer(rs.getInt("CUSTOMER_ID")).toString()); // need to convert into String
																							// NOT NULL ENABLE,
				micdvo.setCUSTOMER_ID_TYPE(rs.getString("CUSTOMER_ID_TYPE"));
				micdvo.setCUSTOMER_NRIC_ID(rs.getString(3));
				micdvo.setCUSTOMER_NAME(rs.getString(4));
				micdvo.setCUSTOMER_DOB(rs.getString(5));
				micdvo.setCUSTOMER_GENDER(rs.getString(6));
				micdvo.setCUSTOMER_RACE(rs.getString(7));
				micdvo.setCUSTOMER_RELIGION(rs.getString(8));
				micdvo.setCUSTOMER_NATIONALITY(rs.getString(9));
				micdvo.setCUSTOMER_TITLE(rs.getString(10));
				micdvo.setCUSTOMER_NATIONALITY_RACE(rs.getString(11));
				micdvo.setCUSTOMER_EDU_LEVEL(rs.getString(12));
				micdvo.setCUSTOMER_MARITALSTATUS(rs.getString(13));
				micdvo.setCUSTOMER_SALARY_RANGE(rs.getString(14));
				// micdvo.setCUSTOMER_OCCUPATION(rs.getString(15));
				micdvo.setCUSTOMER_ADDRESS1(rs.getString(16));
				micdvo.setCUSTOMER_ADDRESS2(rs.getString(17));
				micdvo.setCUSTOMER_ADDRESS3(rs.getString(18));
				micdvo.setCUSTOMER_POSTCODE(rs.getString(19));
				micdvo.setCUSTOMER_STATE(rs.getString(20));
				micdvo.setCUSTOMER_COUNTRY(rs.getString(21));
				micdvo.setCUSTOMER_MAIL_ADDRESS1(rs.getString(22));
				micdvo.setCUSTOMER_MAIL_ADDRESS2(rs.getString(23));
				micdvo.setCUSTOMER_MAIL_ADDRESS3(rs.getString(24));
				micdvo.setCUSTOMER_MAIL_POSTCODE(rs.getString(25));
				micdvo.setCUSTOMER_MAIL_STATE(rs.getString(26));
				micdvo.setCUSTOMER_MAIL_COUNTRY(rs.getString(27));

				int noOfChildVal = gettingValueisNullOrNot(28, rs);
				micdvo.setCUSTOMER_NO_CHILDREN(Integer.toString(noOfChildVal)); // need to convert into String(2,0),

				micdvo.setCUSTOMER_MOBILE_NO(rs.getString(29));
				micdvo.setCUSTOMER_EMAIL(rs.getString(30));
				micdvo.setLEADS_FLAG(rs.getString(31));
				micdvo.setSALES_TOOL(rs.getString(32));
				micdvo.setEMAIL_SENT_COUNTER(rs.getString(33));

				/*
				 * int qqIdVal= gettingValueisNullOrNot(34,rs);
				 * micdvo.setQQ_ID(Integer.toString(qqIdVal)); // need to convert into
				 * String(5,0),
				 */
				micdvo.setQQ_ID(QQ_ID);
				micdvo.setCREATE_DATE(rs.getString(35));
				micdvo.setUPDATED_DATE(rs.getString(36));
				micdvo.setCUSTOMER_EMPLOYER(rs.getString(37));
				micdvo.setCUSTOMER_CLIENTTYPE(rs.getString(38));
				micdvo.setCUSTOMER_INDUSTRY(rs.getString(39));

				int IdVal = gettingValueisNullOrNot(40, rs);
				micdvo.setID(Integer.toString(IdVal)); // need to convert into String NOT NULL ENABLE,

				micdvo.setPOLICY_NUMBER(rs.getString(41));
				micdvo.setPRODUCT_CODE(rs.getString(42));
				micdvo.setPRODUCT_PLAN_CODE(rs.getString(43));
				micdvo.setPOLICY_EFFECTIVE_TIMESTAMP(rs.getString(44));

				int SUM_INSUREDVal = gettingValueisNullOrNot(45, rs);
				micdvo.setSUM_INSURED(Integer.toString(SUM_INSUREDVal)); // need to convert into String(18,4),

				micdvo.setPOLICY_STATUS(rs.getString(46));
				micdvo.setPOLICY_TYPE_CODE(rs.getString(47));
				micdvo.setPOLICY_EXPIRY_TIMESTAMP(rs.getString(48));

				int TRANSACTION_IDVal = gettingValueisNullOrNot(49, rs);
				micdvo.setTRANSACTION_ID(Integer.toString(TRANSACTION_IDVal)); // need to convert into String(*,0),

				int DSP_QQ_IDVal = gettingValueisNullOrNot(50, rs);
				micdvo.setDSP_QQ_ID(Integer.toString(DSP_QQ_IDVal)); // need to convert into String(*,0),

				micdvo.setCREATION_TIMESTAMP(rs.getString(51));

				// TL_CHECK_DIGIT 52
				// CHECKDIGIT 53
				// micdvo.setQQ_ID(new Integer(rs.getInt(54)).toString()); // need to convert
				// into String NOT NULL ENABLE,
				// micdvo.setDSP_QQ_ID(new Integer(rs.getInt(55)).toString()); // need to
				// convert into String,

				micdvo.setLEADS_EMAIL_ID(rs.getString(56));
				// micdvo.setPRODUCT_CODE(rs.getString(57));
				micdvo.setREGISTRATION_NUMBER(rs.getString(58));
				micdvo.setVEHICLE_LOCATION(rs.getString(59));

				int PREVIOUS_INSURANCE_COMPANYVal = gettingValueisNullOrNot(60, rs);
				int YEAR_MAKEVal = gettingValueisNullOrNot(64, rs);

				micdvo.setPREVIOUS_INSURANCE_COMPANY(Integer.toString(PREVIOUS_INSURANCE_COMPANYVal)); // need to
																										// convert into
																										// String(4,0),
				micdvo.setBMS_QUESTION_ANS(rs.getString(61));
				micdvo.setVEHICLE_MAKE(rs.getString(62));
				micdvo.setVEHICLE_MODEL(rs.getString(63));
				micdvo.setYEAR_MAKE(Integer.toString(YEAR_MAKEVal)); // need to convert into String,
				micdvo.setVEHICLE_VARIANT(rs.getString(65));
				micdvo.setFUEL_TYPE(rs.getString(66));

				int MARKET_VALUEVal = gettingValueisNullOrNot(67, rs);
				// int FINANCED_BYVal= gettingValueisNullOrNot(70,rs);

				micdvo.setMARKET_VALUE(Integer.toString(MARKET_VALUEVal)); // need to convert into String(19,4),
				micdvo.setENGINE_NUMBER(rs.getString(68));
				micdvo.setCHASSIS_NUMBER(rs.getString(69));
				// micdvo.setFINANCED_BY(rs.getString(69)); // need to convert into String,
				micdvo.setNVIC_CODE(rs.getString(71));

				String PASSENGER_PA_SELECTED = rs.getString(72);
				if (rs.wasNull()) {
					PASSENGER_PA_SELECTED = "";
				}
				String TPCD_PPA_QUESTION_ANS = rs.getString(73);
				if (rs.wasNull()) {
					TPCD_PPA_QUESTION_ANS = "";
				}
				micdvo.setPASSENGER_PA_SELECTED(PASSENGER_PA_SELECTED);
				micdvo.setTPCD_PPA_QUESTION_ANS(TPCD_PPA_QUESTION_ANS);

				Double POLICY_SUM_INSURED = gettingValueisNullOrNotInDouble(74, rs);
				int PREMIUM_ON_SUM_INSURED = gettingValueisNullOrNot(75, rs);

				micdvo.setPOLICY_SUM_INSURED(String.format("%.2f", POLICY_SUM_INSURED)); // need to convert into
																							// String(19,4),
				micdvo.setPREMIUM_ON_SUM_INSURED(Integer.toString(PREMIUM_ON_SUM_INSURED)); // need to convert into
																							// String(19,4),
				micdvo.setDISCOUNT_CODE(rs.getString(76));

				Double GST_ON_POLICY_PREMIUM = gettingValueisNullOrNotInDouble(77, rs);
				Double STAMP_DUTY_POLICY_PREMIUM = gettingValueisNullOrNotInDouble(78, rs);

				micdvo.setGST_ON_POLICY_PREMIUM(String.format("%.2f", GST_ON_POLICY_PREMIUM)); // need to convert into
																								// String(19,4),
				micdvo.setSTAMP_DUTY_POLICY_PREMIUM(String.format("%.2f", STAMP_DUTY_POLICY_PREMIUM)); // need to
																										// convert into
																										// String(19,4),

				Double PASSANGER_PA_PREMIUM = gettingValueisNullOrNotInDouble(79, rs);
				Double GST_PASSANGER_PA_PREMIUM = gettingValueisNullOrNotInDouble(80, rs);

				micdvo.setPASSANGER_PA_PREMIUM(String.format("%.2f", PASSANGER_PA_PREMIUM)); // need to convert into
																								// String(19,4),
				micdvo.setGST_PASSANGER_PA_PREMIUM(String.format("%.2f", GST_PASSANGER_PA_PREMIUM)); // need to convert
																										// into
																										// String(19,4),

				Double STAMP_DUTY_PASSANGER_PA_DIRECT = gettingValueisNullOrNotInDouble(81, rs);
				Double PASSANGER_PA_PREMIUM_PAYABLE = gettingValueisNullOrNotInDouble(82, rs);
				Double TOTAL_PREMIUM_PAYABLE = gettingValueisNullOrNotInDouble(83, rs);
				int PARKING_MAIL_POSTCODE = gettingValueisNullOrNot(86, rs);

				micdvo.setSTAMP_DUTY_PASSANGER_PA_DIRECT(String.format("%.2f", STAMP_DUTY_PASSANGER_PA_DIRECT)); // need
																													// to
																													// convert
																													// into
																													// String(19,4),
				micdvo.setPASSANGER_PA_PREMIUM_PAYABLE(String.format("%.2f", PASSANGER_PA_PREMIUM_PAYABLE)); // need to
																												// convert
																												// into
																												// String(19,4),
				micdvo.setTOTAL_PREMIUM_PAYABLE(String.format("%.2f", TOTAL_PREMIUM_PAYABLE)); // need to convert into
																								// String(19,4),
				micdvo.setPARKING_MAIL_ADDRESS_1(rs.getString(84));
				micdvo.setPARKING_MAIL_ADDRESS_2(rs.getString(85));
				micdvo.setPARKING_MAIL_POSTCODE(Integer.toString(PARKING_MAIL_POSTCODE)); // need to convert into
																							// String,
				micdvo.setPARKING_MAIL_STATE(rs.getString(87));
				// micdvo.setVEHICLE_PARK_OVERNEIGHT(rs.getString(88));
				// micdvo.setVEHICLE_ANTI_THEFT(rs.getString(89));
				// micdvo.setVEHICLE_SAFTEY(rs.getString(90));
				micdvo.setPDS_QUESTION_ANS(rs.getString(91));
				micdvo.setIND_QUESTION_ANS(rs.getString(92));
				micdvo.setPDPA_QUESTION_ANS(rs.getString(93));

				Double NON_CLAIM_DISCOUNT = gettingValueisNullOrNotInDouble(94, rs);
				int EXCESS = gettingValueisNullOrNot(95, rs);

				micdvo.setNON_CLAIM_DISCOUNT(String.format("%.2f", NON_CLAIM_DISCOUNT)); // need to convert into
																							// String(19,4),
				micdvo.setEXCESS(Integer.toString(EXCESS)); // need to convert into String(19,4),
				micdvo.setCOVERAGE_START_DATE(rs.getString(96));
				micdvo.setCOVERAGE_END_DATE(rs.getString(97));
				micdvo.setAGENT_CODE(rs.getString(98));
				micdvo.setOPERATOR_CODE(rs.getString(99));
				micdvo.setIS_PURCHASED(rs.getString(100));
				micdvo.setQUOTATION_STATUS(rs.getString(101));
				micdvo.setLEAD_STATUS(rs.getString(102));
				micdvo.setQUOTATION_TYPE(rs.getString(103));
				micdvo.setQUOTATION_CREATION_DATETIME(rs.getString(104));
				micdvo.setCUSTOMER_IC(rs.getString(105));

				int NO_OF_DRIVERS = gettingValueisNullOrNot(106, rs);
				Double LOADING = gettingValueisNullOrNotInDouble(108, rs);
				int DISCOUNT_AMOUNT = gettingValueisNullOrNot(109, rs);

				micdvo.setNO_OF_DRIVERS(Integer.toString(NO_OF_DRIVERS)); // need to convert into String,
				micdvo.setQUOTATION_UPDATE_DATETIME(rs.getString(107));
				micdvo.setLOADING(String.format("%.2f", LOADING)); // need to convert into String(19,4),

				// micdvo.setPREMIUM_AFTER_DISCOUNT(pREMIUM_AFTER_DISCOUNT);

				Double GROSSPREMIUM_FINAL1 = gettingValueisNullOrNotInDouble(121, rs);
				Double PREMIUM_AFTER_DISCOUNT1 = gettingValueisNullOrNotInDouble(118, rs);
				DecimalFormat df = new DecimalFormat("0.00");
				// System.out.println(df.format(iVal));
				micdvo.setDISCOUNT_AMOUNT(df.format(GROSSPREMIUM_FINAL1 - PREMIUM_AFTER_DISCOUNT1)); // Integer.toString(DISCOUNT_AMOUNT)
																										// need to
																										// convert into
																										// String(19,4),

				micdvo.setPA_ADDRESS_CHECK(rs.getString(110));
				micdvo.setFULL_NAME(rs.getString(111));

				int CUSTOMER_ID = gettingValueisNullOrNot(112, rs);
				// Double ADDITINAL_COVERAGE_PREMIUM= gettingValueisNullOrNotInDouble(114,rs);

				micdvo.setCUSTOMER_ID(Integer.toString(CUSTOMER_ID)); // need to convert into String,
				micdvo.setPOLICY_ELAPSE(rs.getString(113));
				// micdvo.setADDITINAL_COVERAGE_PREMIUM(Double.toString(ADDITINAL_COVERAGE_PREMIUM));
				// // need to convert into String(18,2),

				// QUOTATION_NO 115

				/*
				 * ADDITINAL_CODES VARCHAR2(50) ADDITINAL_SUMCOVERED VARCHAR2(50)
				 * PREMIUM_AFTER_DISCOUNT NUMBER(19,4) NCD_AMOUNT NUMBER(19,4) PREMIUM_AFTER_GST
				 * NUMBER(19,4)
				 */
				micdvo.setADDITINAL_CODES(rs.getString(116));
				micdvo.setADDITINAL_SUMCOVERED(rs.getString(117));
				Double PREMIUM_AFTER_DISCOUNT = gettingValueisNullOrNotInDouble(118, rs);
				micdvo.setPREMIUM_AFTER_DISCOUNT(String.format("%.2f", PREMIUM_AFTER_DISCOUNT));
				Double NCD_AMOUNT = gettingValueisNullOrNotInDouble(119, rs);
				micdvo.setNCD_AMOUNT(String.format("%.2f", NCD_AMOUNT));
				Double PREMIUM_AFTER_GST = gettingValueisNullOrNotInDouble(120, rs);
				micdvo.setPREMIUM_AFTER_GST(String.format("%.2f", PREMIUM_AFTER_GST));

				/*
				 * GROSSPREMIUM_FINAL NUMBER(19,4) PASSENGER_PREMIUM_AFTDIS NUMBER(19,4)
				 * BASIC_PREMIUM NUMBER(19,4) LOADING_PERCENTAGE VARCHAR2(10)
				 */

				Double GROSSPREMIUM_FINAL = gettingValueisNullOrNotInDouble(121, rs);
				micdvo.setGROSSPREMIUM_FINAL(String.format("%.2f", GROSSPREMIUM_FINAL));
				Double PASSENGER_PREMIUM_AFTDIS = gettingValueisNullOrNotInDouble(122, rs);
				micdvo.setPASSENGER_PREMIUM_AFTDIS(String.format("%.2f", PASSENGER_PREMIUM_AFTDIS));
				Double BASIC_PREMIUM = gettingValueisNullOrNotInDouble(123, rs);
				micdvo.setBASIC_PREMIUM(String.format("%.2f", BASIC_PREMIUM));
				Double LOADING_PERCENTAG = gettingValueisNullOrNotInDouble(124, rs);
				micdvo.setLOADING_PERCENTAGE(String.format("%.2f", LOADING_PERCENTAG));
				//
				// PARKING_MAIL_ADDRESS_3 125

				micdvo.setLangVal(rs.getString(126));
				// USER_LANGUAGE 126
				// AGENT_COMMISSION_PER 127
				// AGENT_COMMISSION_AMT 128
				// DISCOUNT_PER 129
				// micdvo.setID(rs.getString(130));
				// micdvo.setNVIC_CODE(rs.getString(131));
				micdvo.setNVIC_GROUP(rs.getString(132));
				micdvo.setMM_CODE(rs.getString(133));
				micdvo.setMAKE_CODE(rs.getString(134));
				micdvo.setMODEL_CODE(rs.getString(135));
				micdvo.setMAKE(rs.getString(136));
				micdvo.setMODEL(rs.getString(137));
				micdvo.setVARIANT(rs.getString(138));
				micdvo.setSERIES(rs.getString(139));
				int YEAR = gettingValueisNullOrNot(140, rs);
				micdvo.setYEAR(Integer.toString(YEAR));
				micdvo.setDESCRIPTION(rs.getString(141));
				micdvo.setENGINE(rs.getString(142));
				micdvo.setSTYLE(rs.getString(143));
				micdvo.setTRANSMISSION(rs.getString(144));
				micdvo.setDRIVEN_WHEEL(rs.getString(145));
				int SEAT = gettingValueisNullOrNot(146, rs);
				int CC = gettingValueisNullOrNot(147, rs);
				micdvo.setSEAT(Integer.toString(SEAT));
				micdvo.setCC(Integer.toString(CC));
				// micdvo.setFUEL_TYPE(rs.getString(148));
				micdvo.setCLASS_CODE(rs.getString(149));
				micdvo.setWD4_FLG(rs.getString(150));
				micdvo.setHP_FLG(rs.getString(151));
				int MARK_UP_PER = gettingValueisNullOrNot(152, rs);
				int MARK_DOWN_PER = gettingValueisNullOrNot(153, rs);

				micdvo.setMARK_UP_PER(Integer.toString(MARK_UP_PER));
				micdvo.setMARK_DOWN_PER(Integer.toString(MARK_DOWN_PER));

				int TOTAL_SUM = gettingValueisNullOrNot(154, rs);
				int MARKET_VALUE1 = gettingValueisNullOrNot(155, rs);

				micdvo.setTOTAL_SUM(Integer.toString(TOTAL_SUM));
				micdvo.setMARKET_VALUE1(Integer.toString(MARKET_VALUE1));

				int MARKET_VALUE2 = gettingValueisNullOrNot(156, rs);
				int MARKET_VALUE3 = gettingValueisNullOrNot(157, rs);

				micdvo.setMARKET_VALUE2(Integer.toString(MARKET_VALUE2));
				micdvo.setMARKET_VALUE3(Integer.toString(MARKET_VALUE3));

				int WINDSCREEN_VALUE1 = gettingValueisNullOrNot(158, rs);
				int WINDSCREEN_VALUE2 = gettingValueisNullOrNot(159, rs);
				int WINDSCREEN_VALUE3 = gettingValueisNullOrNot(160, rs);

				micdvo.setWINDSCREEN_VALUE1(Integer.toString(WINDSCREEN_VALUE1));
				micdvo.setWINDSCREEN_VALUE2(Integer.toString(WINDSCREEN_VALUE2));
				micdvo.setWINDSCREEN_VALUE3(Integer.toString(WINDSCREEN_VALUE3));

				micdvo.setEFFECTIVE_DATE(rs.getString(161));
			}

			List<MotorInsuranceCustDetails> listOfAdditionalCoverage = new ArrayList<MotorInsuranceCustDetails>();
			while (rsOfAdditionalValue.next()) {
				MotorInsuranceCustDetails miAdditionalCoverage = new MotorInsuranceCustDetails();

				miAdditionalCoverage.setAdditionalCoverageCode(rsOfAdditionalValue.getString(1));
				if (rsOfAdditionalValue.getString(1).charAt(0) == 'P') {
					if (micdvo.getLangVal().equals("lan_en")) {
						miAdditionalCoverage.setAdditionalCoverageText("Compensation for Assessed Repair Time");
					} else {
						miAdditionalCoverage.setAdditionalCoverageText("Pampasan Untuk Pembaikan Taksiran (CART)");
					}
				} else {
					miAdditionalCoverage.setAdditionalCoverageText(rsOfAdditionalValue.getString(2));
				}
				miAdditionalCoverage
						.setAdditionalCoverageValue(gettingValueisNullOrNotInString(3, rsOfAdditionalValue));
				miAdditionalCoverage
						.setAdditionalCoverageSumInsuredValue(gettingValueisNullOrNotInString(4, rsOfAdditionalValue));
				listOfAdditionalCoverage.add(miAdditionalCoverage);
			}

			if (micdvo.getPOLICY_NUMBER().equals("") || micdvo.getPOLICY_NUMBER() == null) {
				list.add("Policy No. is null");
				return list;
			} else {
				MotorInsuranceReportsGenAllDoc reportsGen = new MotorInsuranceReportsGenAllDoc();
				// MotorInsurancePDPA reportsGen=new MotorInsurancePDPA();
				list = reportsGen.MotorInsurnaceallDocGenMethod(micdvo, listOfAdditionalCoverage);
				return list;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			list.add("SQL Exp");
			return list;

		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);

		}

	}

	public int gettingValueisNullOrNot(int indexVal, ResultSet rs) {
		int iVal = 0;
		try {
			System.out.println(indexVal + "indexVal");
			iVal = rs.getInt(indexVal);
			if (rs.wasNull()) {
				System.out.println(indexVal + "indexVal  value null or empty");
				iVal = 0;
			}
			System.out.println(iVal);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return iVal;
	}

	public Double gettingValueisNullOrNotInDouble(int indexVal, ResultSet rs) {
		Double iVal = null;
		try {
			System.out.println(indexVal + "indexVal");
			iVal = (double) rs.getDouble(indexVal);
			DecimalFormat df = new DecimalFormat("0.00");
			System.out.println(df.format(iVal));
			String formatVal = df.format(iVal);
			iVal = Double.parseDouble(formatVal);
			if (rs.wasNull()) {
				System.out.println(indexVal + "indexVal  value null or empty In Double");
				iVal = (double) 0;
			}
			System.out.println(iVal);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return iVal;
	}

	public String gettingValueisNullOrNotInString(int indexVal, ResultSet rs) {
		String iVal = null;
		try {
			System.out.println(indexVal + "indexVal");
			iVal = rs.getString(indexVal);
			Double Value = Double.parseDouble(iVal);
			iVal = String.format("%.2f", Value);
			if (rs.wasNull()) {

				System.out.println(indexVal + "indexVal  value null or empty In Double");
				iVal = "0.00";
			}

			System.out.println(iVal);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return iVal;
	}

	public String getStatusOfDocOfExistCust(String VehicleNo, String QQID, String PolicyNo) {
		Connection connection = null;
		Statement statement = null;
		ResultSet rs = null, rsCust = null, rsOfAdditionalValue = null;
		List<String> list = new ArrayList<String>();
		String respStatus = "";
		try {
			connection = ConnectionFactory.getConnection();

			CallableStatement cstmt = connection.prepareCall("{CALL DSP_MI_SP_DOCLIST_FETCH(?,?,?,?,?)}");
			cstmt.setString(1, PolicyNo);
			cstmt.setString(2, QQID);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.registerOutParameter(4, OracleTypes.CURSOR);
			cstmt.registerOutParameter(5, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(3);
			rsCust = (ResultSet) cstmt.getObject(4);
			rsOfAdditionalValue = (ResultSet) cstmt.getObject(5);
			MotorInsuranceCustDetails micdvo = new MotorInsuranceCustDetails();
			while (rs.next()) {
				list.add(rs.getString(1));

			}
			List<MotorInsuranceCustDetails> listOfAdditionalCoverage = new ArrayList<MotorInsuranceCustDetails>();
			while (rsOfAdditionalValue.next()) {
				MotorInsuranceCustDetails miAdditionalCoverage = new MotorInsuranceCustDetails();

				miAdditionalCoverage.setAdditionalCoverageCode(rsOfAdditionalValue.getString(1));
				if (rsOfAdditionalValue.getString(1).charAt(0) == 'P') {
					miAdditionalCoverage.setAdditionalCoverageText("Compensation for Assessed Repair Time");
				} else {
					miAdditionalCoverage.setAdditionalCoverageText(rsOfAdditionalValue.getString(2));
				}
				miAdditionalCoverage
						.setAdditionalCoverageValue(gettingValueisNullOrNotInString(3, rsOfAdditionalValue));
				listOfAdditionalCoverage.add(miAdditionalCoverage);
			}
			micdvo.setQQ_ID(QQID);
			micdvo.setREGISTRATION_NUMBER(VehicleNo);
			micdvo.setPOLICY_NUMBER(PolicyNo);
			String toDate = null;
			while (rsCust.next()) {
				micdvo.setCUSTOMER_NAME(rsCust.getString(1));
				micdvo.setCUSTOMER_DOB(rsCust.getString(2));
				micdvo.setCUSTOMER_EMAIL(rsCust.getString(3));
				micdvo.setPOLICY_EXPIRY_TIMESTAMP(rsCust.getString(4));
				micdvo.setCUSTOMER_NRIC_ID(rsCust.getString(5));
			}

			MotorInsuranceExistsCustEmailService service = new MotorInsuranceExistsCustEmailService();

			try {
				respStatus = service.getStatusOfDocExistedCustomer(micdvo, list, listOfAdditionalCoverage);
			} catch (ClassNotFoundException | JRException | IOException e) {

				e.printStackTrace();
			}
			return respStatus;
		} catch (SQLException e) {

			e.printStackTrace();
			list.add("SQL Exp");
			return respStatus;

		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);

		}
	}
}
