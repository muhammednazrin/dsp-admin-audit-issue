package com.etiqa.dsp.dao.common.motor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.etiqa.dsp.dao.common.pojo.GeneratingReportsResponseVo;
import com.etiqa.dsp.dao.common.pojo.MotorInsuranceCustDetails;
import com.etiqa.dsp.dao.common.pojo.custQuotPmntPolicyVo;
import com.etiqa.dsp.sales.process.email.DspEmailDispatchProcessor;
import com.etiqa.dsp.sales.process.email.TermLifeProductMailTemplateLoad;

public class MotorInsuranceReportsGenAllDoc {
	public List<String> MotorInsurnaceallDocGenMethod(MotorInsuranceCustDetails miVo,
			List<MotorInsuranceCustDetails> listOfAdditionalCoverage) {
		List<String> listOfString = new ArrayList<String>();
		List<GeneratingReportsResponseVo> list = new ArrayList<GeneratingReportsResponseVo>();
		try {

			MotorInsurancePDPAForm pdpa = new MotorInsurancePDPAForm();
			// 1 PDPA Doc
			String pdpafileName = pdpa.PDPAFormMethod(miVo);
			if (pdpafileName.equals("failtoStore")) {
				System.out.println("pdpa fail");
			} else {
				GeneratingReportsResponseVo grrvo = new GeneratingReportsResponseVo();
				grrvo.setDocCode("PDPAFORM");
				grrvo.setFileName(pdpafileName);
				grrvo.setPolicyNo(miVo.getPOLICY_NUMBER());
				list.add(grrvo);
				listOfString.add(pdpafileName + "=" + miVo.getPOLICY_NUMBER());
				System.out.println("pdpa success OF Mi");
			}
			// 2 PDS Motor Doc

			MotorInsurancePDSMotorForm pdsMotor = new MotorInsurancePDSMotorForm();
			String pdsMotorfileName = pdsMotor.pdsMotorFormMethod(miVo);
			if (pdpafileName.equals("failtoStore")) {
				System.out.println("pdsMotor fail");
			} else {
				GeneratingReportsResponseVo grrvo = new GeneratingReportsResponseVo();
				grrvo.setDocCode("PDSMFORM");
				grrvo.setFileName(pdsMotorfileName);
				grrvo.setPolicyNo(miVo.getPOLICY_NUMBER());
				list.add(grrvo);
				listOfString.add(pdsMotorfileName + "=" + miVo.getPOLICY_NUMBER());
				System.out.println("pdsMotor success OF Mi");
			}
			// 3 Auto Assit Doc

			MotorInsuranceAutoAssistForm autoAssistMotor = new MotorInsuranceAutoAssistForm();
			String autoassistfileName = autoAssistMotor.autoAssistFormMethod(miVo);
			if (pdpafileName.equals("failtoStore")) {
				System.out.println("autoAssist fail");
			} else {
				GeneratingReportsResponseVo grrvo = new GeneratingReportsResponseVo();
				grrvo.setDocCode("AASTFORM");
				grrvo.setFileName(autoassistfileName);
				grrvo.setPolicyNo(miVo.getPOLICY_NUMBER());
				list.add(grrvo);
				listOfString.add(autoassistfileName + "=" + miVo.getPOLICY_NUMBER());
				System.out.println("autoAssist success OF Mi");
			}
			// 4 Tax Invoice Doc
			// MotorInsuranceTaxInvoiceForm TaxInvoiceFormMethod
			MotorInsuranceTaxInvoiceForm taxInvoiceMotor = new MotorInsuranceTaxInvoiceForm();
			if (!miVo.getPASSENGER_PA_SELECTED().isEmpty() && !miVo.getTPCD_PPA_QUESTION_ANS().isEmpty()) {
				/*
				 * System.out.println(miVo.getPASSENGER_PA_SELECTED()+"PASSENGER_PA_SELECTED");
				 * System.out.println(miVo.getTPCD_PPA_QUESTION_ANS()+"TPCD_PPA_QUESTION_ANS");
				 * if((!(miVo.getPASSENGER_PA_SELECTED()==null) ||
				 * !miVo.getPASSENGER_PA_SELECTED().equals(""))
				 * &&(!(miVo.getTPCD_PPA_QUESTION_ANS()==null) ||
				 * !miVo.getTPCD_PPA_QUESTION_ANS().equals(""))){
				 * if(!miVo.getPASSENGER_PA_SELECTED().equals("on") &&
				 * !miVo.getTPCD_PPA_QUESTION_ANS().equals("no")){
				 *
				 *
				 * } else{System.out.println("on and yes status dppa");}
				 * }else{System.out.println("is null dppa");}
				 */
			} else {
				System.out.println("is empty dppa");
				String taxInvoicefileName = taxInvoiceMotor.TaxInvoiceFormMethod(miVo);
				if (pdpafileName.equals("failtoStore")) {
					System.out.println("taxInvoice fail");
				} else {
					GeneratingReportsResponseVo grrvo = new GeneratingReportsResponseVo();
					grrvo.setDocCode("TAXIFORM");
					grrvo.setFileName(taxInvoicefileName);
					grrvo.setPolicyNo(miVo.getPOLICY_NUMBER());
					list.add(grrvo);
					listOfString.add(taxInvoicefileName + "=" + miVo.getPOLICY_NUMBER());
					System.out.println("taxInvoice success OF Mi");
				}
			}
			// 5 E-Policy Doc
			// MotorInsuranceEPolicyForm EPolicyFormMethod
			MotorInsuranceEPolicyForm ePolicyMotor = new MotorInsuranceEPolicyForm();
			String ePolicyfileName = ePolicyMotor.EPolicyFormMethod(miVo, listOfAdditionalCoverage);
			if (pdpafileName.equals("failtoStore")) {
				System.out.println("ePolicy fail");
			} else {
				GeneratingReportsResponseVo grrvo = new GeneratingReportsResponseVo();
				grrvo.setDocCode("EPOLFORM");
				grrvo.setFileName(ePolicyfileName);
				grrvo.setPolicyNo(miVo.getPOLICY_NUMBER());
				list.add(grrvo);
				listOfString.add(ePolicyfileName + "=" + miVo.getPOLICY_NUMBER());
				System.out.println("ePolicy success OF Mi");
			}

			// ************************************** dppa code Doc
			// Starts***************************************************

			if (!miVo.getPASSENGER_PA_SELECTED().isEmpty() && !miVo.getTPCD_PPA_QUESTION_ANS().isEmpty()) {
				System.out.println(miVo.getPASSENGER_PA_SELECTED() + "PASSENGER_PA_SELECTED");
				System.out.println(miVo.getTPCD_PPA_QUESTION_ANS() + "TPCD_PPA_QUESTION_ANS");
				if ((!(miVo.getPASSENGER_PA_SELECTED() == null) || !miVo.getPASSENGER_PA_SELECTED().equals(""))
						&& (!(miVo.getTPCD_PPA_QUESTION_ANS() == null)
								|| !miVo.getTPCD_PPA_QUESTION_ANS().equals(""))) {
					if (miVo.getPASSENGER_PA_SELECTED().equals("on") && miVo.getTPCD_PPA_QUESTION_ANS().equals("no")) {
						// dppa option enable TaxInvoice Dppa Starts Here
						// 6 PDS dppa Motor Doc

						String pdsMotordppfileName = pdsMotor.pdsMotorDppaFormMethod(miVo);
						if (pdpafileName.equals("failtoStore")) {
							System.out.println("pdsMotordpp fail");
						} else {
							GeneratingReportsResponseVo grrvo = new GeneratingReportsResponseVo();
							grrvo.setDocCode("PDSMDPPA");
							grrvo.setFileName(pdsMotordppfileName);
							grrvo.setPolicyNo(miVo.getPOLICY_NUMBER());
							list.add(grrvo);
							listOfString.add(pdsMotordppfileName + "=" + miVo.getPOLICY_NUMBER());
							System.out.println("pdsMotordpp success OF Mi");
						}

						// 7 TaxInvoice dppa Motor Doc
						String taxInvoiceDppafileName = taxInvoiceMotor.TaxInvoiceDppaFormMethod(miVo);
						if (taxInvoiceDppafileName.equals("failtoStore")) {
							System.out.println("taxInvoiceDppa fail");
						} else {
							GeneratingReportsResponseVo grrvo = new GeneratingReportsResponseVo();
							grrvo.setDocCode("TAXIDPPA");
							grrvo.setFileName(taxInvoiceDppafileName);
							grrvo.setPolicyNo(miVo.getPOLICY_NUMBER());
							list.add(grrvo);
							listOfString.add(taxInvoiceDppafileName + "=" + miVo.getPOLICY_NUMBER());
							System.out.println("taxInvoiceDppa success OF Mi");
						}
						// 8 Epolicy dppa Motor Doc
						String epolicyDppafileName = ePolicyMotor.EPolicyDppaFormMethod(miVo);
						if (epolicyDppafileName.equals("failtoStore")) {
							System.out.println("EpolicyDppa fail");
						} else {
							GeneratingReportsResponseVo grrvo = new GeneratingReportsResponseVo();
							grrvo.setDocCode("EPOLDPPA");
							grrvo.setFileName(epolicyDppafileName);
							grrvo.setPolicyNo(miVo.getPOLICY_NUMBER());
							list.add(grrvo);
							listOfString.add(epolicyDppafileName + "=" + miVo.getPOLICY_NUMBER());
							System.out.println("ePlolicyDppa success OF Mi");
						}

						// dppa option enable TaxInvoice Dppa Ends Here
					} else {
						System.out.println("on and yes status dppa");
					}
				} else {
					System.out.println("is null dppa");
				}
			} else {
				System.out.println("is empty dppa");
			}
			// ************************************** dppa code Doc
			// Ends***************************************************
			custQuotPmntPolicyVo cdvo = new custQuotPmntPolicyVo();
			cdvo.setCUSTOMER_NAME(miVo.getCUSTOMER_NAME());
			cdvo.setCUSTOMER_EMAIL(miVo.getCUSTOMER_EMAIL());
			cdvo.setPOLICY_NUMBER(miVo.getPOLICY_NUMBER());
			cdvo.setCUSTOMER_DOB(miVo.getCUSTOMER_DOB());
			cdvo.setCUSTOMER_NRIC_ID(miVo.getCUSTOMER_NRIC_ID());
			cdvo.setPOLICY_EXPIRY_TIMESTAMP(miVo.getPOLICY_EXPIRY_TIMESTAMP());
			cdvo.setVehRegNo(miVo.getREGISTRATION_NUMBER());
			cdvo.setLangValue(miVo.getLangVal());
			// Zip File Generation
			MotorInsuranceZipFileGeneration motorZipFileGen = new MotorInsuranceZipFileGeneration();
			File statusOfZipFileGen = motorZipFileGen.zipFilesGen(cdvo, list);
			System.out.println(statusOfZipFileGen + "  :statusOfZipFileGen oF MI  ");
			// Email Calling
			DspEmailDispatchProcessor emailProcessor = new DspEmailDispatchProcessor(
					new TermLifeProductMailTemplateLoad());
			String status = emailProcessor.emailDispatchProcess(cdvo, list, listOfAdditionalCoverage);
			System.out.println(status + "  :email status OF MI");

			// Deleting pswd zip file
			boolean DeleteResult = motorZipFileGen.deleteDirectory(statusOfZipFileGen);
			if (DeleteResult) {
				System.out.println("Deleted Successfully" + statusOfZipFileGen);
			} else {
				System.out.println("Deleted Not Successfully" + statusOfZipFileGen);
			}

			// ********************************************* fna File Gen Start
			// Here******************************************************
			try (InputStream in = this.getClass().getClassLoader()
					.getResourceAsStream("com/etiqa/dsp/sales/process/email/documents.properties")) {
				Properties prop = new Properties();
				prop.load(in);

				System.out.println(list.size());

				for (GeneratingReportsResponseVo fileNameForFna : list) {
					String fileNameforfna = fileNameForFna.getFileName();
					System.out.println("FNa FIle Name: " + fileNameforfna);
					fileNameforfna = fileNameforfna.substring(0, fileNameforfna.length() - 3);
					System.out.println(fileNameforfna + "file Name for fna with out extention");
					File statText = new File(prop.getProperty("sourcePath") + fileNameforfna + "fna");
					FileOutputStream is = new FileOutputStream(statText);
					OutputStreamWriter osw = new OutputStreamWriter(is);
					Writer w = new BufferedWriter(osw);
					w.write("2,12,44,,,47||Conventional,Individual," + cdvo.getCUSTOMER_NRIC_ID() + ","
							+ cdvo.getPOLICY_NUMBER() + "," + cdvo.getPOLICY_NUMBER() + "||EIB||||N||"
							+ cdvo.getPOLICY_NUMBER() + "||" + fileNameforfna + "pdf");
					w.close();
				}
				// prop.getProperty("sourcePath")+fileName

				// Whatever the file path is.

			} catch (IOException e) {
				System.err.println("Problem writing to the file statsTest.txt");
			}

			// ********************************************* fna File Gen End
			// Here******************************************************
			System.out.println("excuted");
			return listOfString;
		} catch (Exception e) {
			e.printStackTrace();
			listOfString.add("Fail" + "=in Main Class");
			return listOfString;
		}
	}

}
