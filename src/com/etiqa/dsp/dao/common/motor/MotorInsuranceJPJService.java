package com.etiqa.dsp.dao.common.motor;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;

public class MotorInsuranceJPJService {

	String MywsdlWebService = "";

	// String MywsdlWebServiceDSP1 ="http://10.252.21.60:7011/CPF-DSP-MOTOR";
	// String MywsdlWebServiceDSP1 ="http://10.252.21.63:7011/CPF-DSP-MOTOR";

	// String MywsdlWebService ="http://10.252.21.60:7011/CPF-DSP-MOTOR"; //prod
	// String MywsdlWebService ="http://192.168.0.248:7011/CPF-DSP-MOTOR"; //uat

	private static Properties dataConfig = new Properties();
	// private static String MywsdlWebService=dataConfig.getProperty("cpf");

	public void JPJInsert(String policyNo, String vehicleNo, String engineNo, String chassisNo, String idNo,
			String start_date) throws Exception {
		String resp = "";

		String companyCodeNo = null;

		if ("K".equalsIgnoreCase(policyNo.substring(0, 1))) {
			MywsdlWebService = "http://cpf:7011/CPF-DSP-MOTOR-TAKAFUL";
			companyCodeNo = "96";

		}
		if ("V".equalsIgnoreCase(policyNo.substring(0, 1))) {
			MywsdlWebService = "http://cpf:7011/CPF-DSP-MOTOR-DETARIFF";
			companyCodeNo = "14";
		}
		// String MywsdlWebService="http://192.168.0.248:7011/CPF-DSP-MOTOR";

		boolean recFound = false;
		// String param="Location";
		try {
			// URL url = new URL(MyLocations+"/JPJIntegrationImplService?WSDL");
			QName servicename = new QName("http://ws.mi.dsp.com/", "JPJIntegrationImplService");
			QName portname = new QName("http://ws.mi.dsp.com/", "JPJIntegrationImplPort");
			Service service = Service.create(null, servicename);
			service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING, MywsdlWebService + "/JPJIntegrationImplService");
			Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class, Service.Mode.MESSAGE);
			MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

			SOAPMessage request = mf.createMessage();
			SOAPPart part = request.getSOAPPart();

			SOAPEnvelope env = part.getEnvelope();
			SOAPBody body = env.getBody();
			// resp = body;
			SOAPElement operation = body.addChildElement("Jpj_Integration", "ws", "http://ws.mi.dsp.com/");
			SOAPElement element_param = operation.addChildElement("compCode");
			element_param.addTextNode(companyCodeNo);
			SOAPElement element_param1 = operation.addChildElement("docNo");
			element_param1.addTextNode(policyNo);
			SOAPElement element_param2 = operation.addChildElement("vehRegNo");
			element_param2.addTextNode(vehicleNo);
			SOAPElement element_param3 = operation.addChildElement("engineNo");
			if (engineNo != null && engineNo != "") {
				element_param3.addTextNode(engineNo);
			} else {
				element_param3.addTextNode("TEST001");
			}
			SOAPElement element_param4 = operation.addChildElement("chassisNo");
			element_param4.addTextNode(chassisNo);
			SOAPElement element_param5 = operation.addChildElement("docType");
			element_param5.addTextNode("0");
			SOAPElement element_param6 = operation.addChildElement("reasonCode");
			element_param6.addTextNode("1");
			SOAPElement element_param7 = operation.addChildElement("idNo");
			element_param7.addTextNode(idNo);

			String day_s = start_date.substring(0, 2);
			String mon_s = start_date.substring(3, 5);
			String yy_s = start_date.substring(6, 10);

			String start_s = yy_s + mon_s + day_s;
			SOAPElement element_param8 = operation.addChildElement("effectDate");
			element_param8.addTextNode(start_s);

			SimpleDateFormat format1 = new SimpleDateFormat();
			Date outDt = new Date();
			format1 = new SimpleDateFormat("dd/MM/yyyy");
			outDt = format1.parse(start_date);
			Calendar c = Calendar.getInstance();
			c.setTime(outDt);
			c.add(Calendar.YEAR, 1);
			c.add(Calendar.DATE, -1);
			outDt = c.getTime();
			String s = format1.format(c.getTime());

			String day = s.substring(0, 2);
			String mon = s.substring(3, 5);
			String yy = s.substring(6, 10);

			String end_date = yy + mon + day;
			SOAPElement element_param9 = operation.addChildElement("expiryDate");
			element_param9.addTextNode(end_date);

			SOAPElement element_param10 = operation.addChildElement("coverType");
			element_param10.addTextNode("0");
			request.saveChanges();

			// resCode=body ;
			String result = null;

			if (request != null) {
				ByteArrayOutputStream baos = null;
				try {
					baos = new ByteArrayOutputStream();
					request.writeTo(baos);
					result = baos.toString();
				} catch (Exception e) {
				} finally {
					if (baos != null) {
						try {
							baos.close();
						} catch (IOException ioe) {
						}
					}
				}
			}
			System.out.println("request=" + result + "JPJ Insert");
			SOAPMessage response = dispatch.invoke(request);

			// session.setAttribute("JPJ","finished");

			SOAPBody responsebody = response.getSOAPBody();
			System.out.println("response=" + response + "JPJ Insert");
			java.util.Iterator otcupdates = responsebody.getChildElements();
			while (otcupdates.hasNext()) {
				SOAPElement otcupdates1 = (SOAPElement) otcupdates.next();

				java.util.Iterator i = otcupdates1.getChildElements();
				while (i.hasNext()) {

					SOAPElement e = (SOAPElement) i.next();
					java.util.Iterator m = e.getChildElements();
					while (m.hasNext()) {
						SOAPElement e2 = (SOAPElement) m.next();

					}
				}
			}
			// System.out.println(responsebody.getElementsByTagName("jsonString").item(0).getTextContent());

		} catch (Exception e) {
			resp = resp + "{\"errorCode\":\"" + e.getMessage() + "\"}";
			e.printStackTrace();
		}

	}//

	public void JPJCancel(String policyNo, String vehicleNo, String engineNo, String chassisNo, String idNo,
			String start_date) throws Exception {
		String resp = "";
		String companyCodeNo = null;

		if ("K".equalsIgnoreCase(policyNo.substring(0, 1))) {
			MywsdlWebService = "http://cpf:7011/CPF-DSP-MOTOR-TAKAFUL";
			companyCodeNo = "96";

		}
		if ("V".equalsIgnoreCase(policyNo.substring(0, 1))) {
			MywsdlWebService = "http://cpf:7011/CPF-DSP-MOTOR-DETARIFF";
			companyCodeNo = "14";

		}
		System.out.println(MywsdlWebService);
		// String MywsdlWebService="http://192.168.0.248:7011/CPF-DSP-MOTOR";

		boolean recFound = false;
		// String param="Location";
		try {
			// URL url = new URL(MyLocations+"/JPJIntegrationImplService?WSDL");
			QName servicename = new QName("http://ws.mi.dsp.com/", "JPJIntegrationImplService");
			QName portname = new QName("http://ws.mi.dsp.com/", "JPJIntegrationImplPort");
			Service service = Service.create(null, servicename);
			service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING, MywsdlWebService + "/JPJIntegrationImplService");
			Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class, Service.Mode.MESSAGE);
			MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

			SOAPMessage request = mf.createMessage();
			SOAPPart part = request.getSOAPPart();

			SOAPEnvelope env = part.getEnvelope();
			SOAPBody body = env.getBody();
			// resp = body;
			SOAPElement operation = body.addChildElement("Jpj_Integration", "ws", "http://ws.mi.dsp.com/");
			SOAPElement element_param = operation.addChildElement("compCode");
			element_param.addTextNode("14");
			SOAPElement element_param1 = operation.addChildElement("docNo");
			element_param1.addTextNode(policyNo);
			SOAPElement element_param2 = operation.addChildElement("vehRegNo");
			element_param2.addTextNode(vehicleNo);
			SOAPElement element_param3 = operation.addChildElement("engineNo");
			if (engineNo != null && engineNo != "") {
				element_param3.addTextNode(engineNo);
			} else {
				element_param3.addTextNode("TEST001");
			}
			SOAPElement element_param4 = operation.addChildElement("chassisNo");
			element_param4.addTextNode(chassisNo);
			SOAPElement element_param5 = operation.addChildElement("docType");
			element_param5.addTextNode("3");
			SOAPElement element_param6 = operation.addChildElement("reasonCode");
			element_param6.addTextNode("4");
			SOAPElement element_param7 = operation.addChildElement("idNo");
			element_param7.addTextNode(idNo);

			String day_s = start_date.substring(0, 2);
			String mon_s = start_date.substring(3, 5);
			String yy_s = start_date.substring(6, 10);

			String start_s = yy_s + mon_s + day_s;
			SOAPElement element_param8 = operation.addChildElement("effectDate");
			element_param8.addTextNode(start_s);

			SimpleDateFormat format1 = new SimpleDateFormat();
			Date outDt = new Date();
			format1 = new SimpleDateFormat("dd/MM/yyyy");
			outDt = format1.parse(start_date);
			Calendar c = Calendar.getInstance();
			c.setTime(outDt);
			c.add(Calendar.YEAR, 1);
			c.add(Calendar.DATE, -1);
			outDt = c.getTime();
			String s = format1.format(c.getTime());

			String day = s.substring(0, 2);
			String mon = s.substring(3, 5);
			String yy = s.substring(6, 10);

			String end_date = yy + mon + day;
			SOAPElement element_param9 = operation.addChildElement("expiryDate");
			element_param9.addTextNode(end_date);

			SOAPElement element_param10 = operation.addChildElement("coverType");
			element_param10.addTextNode("0");
			request.saveChanges();

			// resCode=body ;
			String result = null;

			if (request != null) {
				ByteArrayOutputStream baos = null;
				try {
					baos = new ByteArrayOutputStream();
					request.writeTo(baos);
					result = baos.toString();
				} catch (Exception e) {
				} finally {
					if (baos != null) {
						try {
							baos.close();
						} catch (IOException ioe) {
						}
					}
				}
			}
			System.out.println("request=" + result + "JPJ Cancel");
			SOAPMessage response = dispatch.invoke(request);

			// session.setAttribute("JPJ","finished");

			SOAPBody responsebody = response.getSOAPBody();
			System.out.println("response=" + response + "JPJ Cancel");
			java.util.Iterator otcupdates = responsebody.getChildElements();
			while (otcupdates.hasNext()) {
				SOAPElement otcupdates1 = (SOAPElement) otcupdates.next();

				java.util.Iterator i = otcupdates1.getChildElements();
				while (i.hasNext()) {

					SOAPElement e = (SOAPElement) i.next();
					java.util.Iterator m = e.getChildElements();
					while (m.hasNext()) {
						SOAPElement e2 = (SOAPElement) m.next();

					}
				}
			}
			// System.out.println(responsebody.getElementsByTagName("jsonString").item(0).getTextContent());

		} catch (Exception e) {
			resp = resp + "{\"errorCode\":\"" + e.getMessage() + "\"}";
			e.printStackTrace();
		}

	}//

}
