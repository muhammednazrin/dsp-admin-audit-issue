package com.etiqa.dsp.dao.common.motor;

import java.util.List;

public interface MotorInsuranceReportsGenDao {
	public List<String> getMIGenReportResponse(String QQ_ID, String langValue);
}
