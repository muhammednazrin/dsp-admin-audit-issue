package com.etiqa.dsp.dao.common.motor;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import com.etiqa.common.DB.ConnectionFactory;
import com.etiqa.dsp.dao.common.pojo.MotorInsuranceCustDetails;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.ExporterInput;
import net.sf.jasperreports.export.OutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

public class MotorInsurancePDPAForm {
	public String PDPAFormMethod(MotorInsuranceCustDetails miVo)
			throws ClassNotFoundException, JRException, IOException, SQLException {

		String fileName = miVo.getPOLICY_NUMBER() + "-PDPAForm.pdf";
		String policyDocCode = "PDPAFORM";
		String sysDate = "";
		if (miVo.getLangVal().equals("lan_en")) {
			Format formatter = new SimpleDateFormat("dd-MMM-yyyy");
			sysDate = formatter.format(new Date());
			System.out.println("Today Date:" + sysDate);
		} else {
			Format formatter = new SimpleDateFormat("dd-MMM-yyyy", new Locale("ms", "MY", "MY"));
			sysDate = formatter.format(new Date());
			System.out.println("Today Date:" + sysDate);
		}

		Date date = new Date();
		String formattedDate = "";
		if (miVo.getLangVal().equals("lan_en")) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy  h:mm:ss a");
			formattedDate = sdf.format(date);
		} else {
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy  h:mm:ss a", new Locale("ms", "MY", "MY"));
			formattedDate = sdf.format(date);
		}

		Map<String, Object> parameters = new HashMap<String, Object>();
		String DppaPolicyNo = "", DppaTypePolicy = "";
		if (miVo.getDppaPolicyNo() != null) {
			DppaPolicyNo = " / " + miVo.getDppaPolicyNo();

			if (miVo.getLangVal().equals("lan_en")) {
				DppaTypePolicy = " / Car Accident Protection Special (CAPS)";
			} else {
				DppaTypePolicy = " / Khas Perlindungan Kemalangan Kereta";
			}

		}
		String policyName = "";
		if (miVo.getLangVal().equals("lan_en")) {
			policyName = "Motor Insurance";
		} else {
			policyName = "Insurans Motor";
		}

		parameters.put("P_Name", miVo.getCUSTOMER_NAME() == null ? "" : miVo.getCUSTOMER_NAME());
		parameters.put("P_NRICNo", miVo.getCUSTOMER_NRIC_ID() == null ? "" : miVo.getCUSTOMER_NRIC_ID());
		parameters.put("P_PolicyNo", (miVo.getPOLICY_NUMBER() == null ? "" : miVo.getPOLICY_NUMBER()) + DppaPolicyNo);
		parameters.put("P_TypeOfPolicy", policyName + DppaTypePolicy);
		parameters.put("P_Date", formattedDate);
		parameters.put("P_ChkBox", miVo.getPDPA_QUESTION_ANS() == null ? "N" : miVo.getPDPA_QUESTION_ANS());

		// / miVo.getDppaPolicyNo()

		try (InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/dsp/sales/process/email/documents.properties")) {
			Properties prop = new Properties();
			prop.load(in);

			Connection con = ConnectionFactory.getConnection();
			// String jrxmlFile = "com/etiqa/dsp/policy/templates/MI_PDPAForm_EV.jrxml";
			String jrxmlFile = "";
			if (miVo.getLangVal().equals("lan_en")) {
				jrxmlFile = "com/etiqa/dsp/policy/templates/MI_PDPAForm_EV.jrxml";
			} else {
				jrxmlFile = "com/etiqa/dsp/policy/templates/MI_PDPAForm_BV.jrxml";
			}

			InputStream input = this.getClass().getClassLoader().getResourceAsStream(jrxmlFile);
			JasperReport jasperReport = JasperCompileManager.compileReport(input);
			JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
			// JasperExportManager.exportReportToPdfFile(print,
			// prop.getProperty("destinationPath")+fileName);
			JRPdfExporter exporter = new JRPdfExporter();
			ExporterInput exporterInput = new SimpleExporterInput(print);
			exporter.setExporterInput(exporterInput);
			OutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(
					prop.getProperty("destinationPath") + fileName);
			exporter.setExporterOutput(exporterOutput);
			SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
			exporter.setConfiguration(configuration);
			exporter.exportReport();
			System.out.print("PDPA Pdf Gen Done!");
			// System.out.println(prop.getProperty("destinationPath")+fileName+"Destination
			// Path");
			// System.out.println(prop.getProperty("sourcePath")+fileName+"Source Path");

			File fileDb = new File(prop.getProperty("sourcePath") + fileName);
			FileInputStream fr = new FileInputStream(fileDb);
			CallableStatement cstmt = con.prepareCall("{CALL DSP_POLICY_DOC_INSERT(?,?,?,?,?)}");

			cstmt.setString(1, miVo.getPOLICY_NUMBER());
			cstmt.setString(2, policyDocCode);
			cstmt.setString(3, fileName);
			cstmt.setString(4, miVo.getCUSTOMER_NAME());
			cstmt.registerOutParameter(5, Types.VARCHAR);
			cstmt.execute();
			String StatusOfInsertion = cstmt.getString(5);

			System.out.println(StatusOfInsertion + " ID inserterd");

			return fileName;

		} catch (JRException | SQLException e) {
			e.printStackTrace();
			return "failtoStore";
		}
	}
}
