package com.etiqa.dsp.dao.common.motor;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;

public class MotorInsuranceISMService {
	// String MywsdlWebService="http://192.168.0.248:7011/CPF-DSP-MOTOR";//uat
	// String MywsdlWebService ="http://10.252.21.60:7011/CPF-DSP-MOTOR"; //prod

	String MywsdlWebService = "";

	private static Properties dataConfig = new Properties();
	// private static String MywsdlWebService=dataConfig.getProperty("cpf");

	public void NCDConfirmService(String vehicleNo, String idNo, String preIns, String chassisNo, String qq_id,
			String policyNo) throws Exception {
		String resp = "";
		String insCode = "";

		if ("K".equalsIgnoreCase(policyNo.substring(0, 1))) {
			MywsdlWebService = "http://10.252.21.60:7011/CPF-DSP-MOTOR-TAKAFUL";
			insCode = "813";

		}
		if ("V".equalsIgnoreCase(policyNo.substring(0, 1))) {
			MywsdlWebService = "http://10.252.21.60:7011/CPF-DSP-MOTOR-DETARIFF";
			insCode = "411";
		}

		// System.out.println(MywsdlWebService);

		HashMap<String, String> map_ncd_info = new HashMap<String, String>();
		boolean recFound = false;
		// String param="Location";
		try {

			// if(session.getAttribute("CFO")!=null &&
			// session.getAttribute("CFO")=="finished")
			// {
			// System.out.println("By passing ism cfo for second time for refreshing for
			// vehicle: " + vehicleNo);
			// return;
			// }
			// URL url = new URL(MyLocations+"/JPJIntegrationImplService?WSDL");
			QName servicename = new QName("http://ws.ism.mi.dsp.com/", "ISMNCDCallService");
			QName portname = new QName("http://ws.ism.mi.dsp.com/", "ISMNCDCallPort");
			Service service = Service.create(servicename);
			service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING, MywsdlWebService + "/ISMNCDCallService");
			Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class, Service.Mode.MESSAGE);
			MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

			SOAPMessage request = mf.createMessage();
			SOAPPart part = request.getSOAPPart();

			SOAPEnvelope env = part.getEnvelope();
			SOAPBody body = env.getBody();
			// resp = body;
			SOAPElement operation = body.addChildElement("NCDCallService", "ws", "http://ws.ism.mi.dsp.com/");
			SOAPElement element_param = operation.addChildElement("curVehNo");
			element_param.addTextNode(vehicleNo);
			SOAPElement element_param1 = operation.addChildElement("idNo1");
			element_param1.addTextNode(idNo);
			SOAPElement element_param2 = operation.addChildElement("idNo2");
			element_param2.addTextNode("NA");
			SOAPElement element_param3 = operation.addChildElement("docType");
			element_param3.addTextNode("CFO");

			SOAPElement element_param4 = operation.addChildElement("prevInsCode");
			element_param4.addTextNode(preIns);
			SOAPElement element_param5 = operation.addChildElement("chassisNo");
			element_param5.addTextNode(chassisNo);
			request.saveChanges();

			// resCode=body ;
			String result = null;

			if (request != null) {
				ByteArrayOutputStream baos = null;
				try {
					baos = new ByteArrayOutputStream();
					request.writeTo(baos);
					result = baos.toString();
				} catch (Exception e) {
				} finally {
					if (baos != null) {
						try {
							baos.close();
						} catch (IOException ioe) {
						}
					}
				}
			}
			System.out.println("request=" + result + "NCD Confim Request");
			SOAPMessage response = dispatch.invoke(request);
			SOAPBody responsebody = response.getSOAPBody();
			System.out.println("response=" + response.toString() + "NCD Confim Request");
			// session.setAttribute("CFO","finished");
			java.util.Iterator otcupdates = responsebody.getChildElements();
			while (otcupdates.hasNext()) {
				SOAPElement otcupdates1 = (SOAPElement) otcupdates.next();

				java.util.Iterator i = otcupdates1.getChildElements();
				while (i.hasNext()) {

					SOAPElement e = (SOAPElement) i.next();
					java.util.Iterator m = e.getChildElements();
					while (m.hasNext()) {
						SOAPElement e2 = (SOAPElement) m.next();
						String name = e2.getLocalName();
						String value = e2.getValue();
						map_ncd_info.put(e2.getLocalName(), e2.getValue());
					}
				}
			}
			// System.out.println(responsebody.getElementsByTagName("jsonString").item(0).getTextContent());

		} catch (Exception e) {
			resp = resp + "{\"errorCode\":\"" + e.getMessage() + "\"}";
			e.printStackTrace();
		}
		try {

			// String qq_id=(String)session.getAttribute("qqid");
			if (map_ncd_info.get("curNCDEffDt") != null && map_ncd_info.get("curNCDExpDt") != null
					&& map_ncd_info.get("curNCDLevel") != null) {
				insertNCDRequestResponse(vehicleNo.replaceAll("\\s", ""), idNo, chassisNo, preIns,
						map_ncd_info.get("curNCDEffDt"), map_ncd_info.get("curNCDExpDt"),
						map_ncd_info.get("curNCDLevel"), map_ncd_info.get("errorCode"), map_ncd_info.get("errorDesc"),
						map_ncd_info.get("insCode"), map_ncd_info.get("nxtNCDEffDt"), map_ncd_info.get("nxtNCDLevel"),
						map_ncd_info.get("refNo"), map_ncd_info.get("respCode"), "", "", qq_id);
			}
		} catch (Exception e) {
			resp = resp + "{\"errorCode\":\"" + e.getMessage() + "\"}";
			e.printStackTrace();
		}
	}// End NCd confirmation call

	public void insertNCDRequestResponse(String vehicleRegNo, String nric, String chasisNo, String preInsCode,
			String curNCDEffDt, String curNCDExpDt, String curNCDLevel, String errorCode, String errorDesc,
			String insCode, String nxtNCDEffDt, String nxtNCDLevel, String refNo, String respCode, String subResCode,
			String subResValue, String qq_id) {
		// String vixURL="http://192.168.0.248:7005/MOTOR";

		try {

			// URL url = new URL("http://localhost:9852/ism/vix?wsdl");
			QName servicename = new QName("http://ws.mi.dsp.com/", "NCD_VIX_DataImplService");
			QName portname = new QName("http://ws.mi.dsp.com/", "NCD_VIX_DataImplPort");
			Service service = Service.create(null, servicename);
			service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING, MywsdlWebService + "/NCD_VIX_DataImplService");
			Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class, Service.Mode.MESSAGE);
			MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);

			SOAPMessage request = mf.createMessage();
			SOAPPart part = request.getSOAPPart();

			SOAPEnvelope env = part.getEnvelope();
			SOAPBody body = env.getBody();

			SOAPElement operation = body.addChildElement("NCD_Insert", "ws", "http://ws.mi.dsp.com/");

			SOAPElement root_param = operation.addChildElement("NCD_Info");

			SOAPElement element_param1 = root_param.addChildElement("curNCDEffDt");
			element_param1.addTextNode(curNCDEffDt);
			SOAPElement element_param2 = root_param.addChildElement("curNCDExpDt");
			element_param2.addTextNode(curNCDExpDt);

			SOAPElement element_param3 = root_param.addChildElement("curNCDLevel");
			element_param3.addTextNode(curNCDLevel);
			SOAPElement element_param4 = root_param.addChildElement("curVehNo");
			element_param4.addTextNode(vehicleRegNo);
			SOAPElement element_param5 = root_param.addChildElement("nxtNCDEffDt");
			element_param5.addTextNode(nxtNCDEffDt);
			SOAPElement element_param6 = root_param.addChildElement("nxtNCDLevel");
			element_param6.addTextNode(nxtNCDLevel);
			SOAPElement element_param7 = root_param.addChildElement("preInsCode");
			element_param7.addTextNode(preInsCode);
			SOAPElement element_param8 = root_param.addChildElement("preInsCode");
			element_param8.addTextNode(preInsCode);
			SOAPElement element_param9 = root_param.addChildElement("refNo");
			element_param9.addTextNode(refNo);
			SOAPElement element_param10 = root_param.addChildElement("res_insCode");
			element_param10.addTextNode(insCode);
			SOAPElement element_param11 = root_param.addChildElement("res_preInsCode");
			element_param11.addTextNode(preInsCode);
			SOAPElement element_param12 = root_param.addChildElement("res_refNo");
			element_param12.addTextNode(refNo);
			SOAPElement element_param13 = root_param.addChildElement("respCode");
			element_param13.addTextNode(respCode);
			SOAPElement element_param14 = root_param.addChildElement("idNo1");
			element_param14.addTextNode(nric);
			SOAPElement element_param15 = root_param.addChildElement("curChassisNo");
			element_param15.addTextNode(chasisNo);
			SOAPElement element_param16 = root_param.addChildElement("preInsCode");
			element_param16.addTextNode(preInsCode);
			SOAPElement element_param17 = root_param.addChildElement("qq_id");
			element_param17.addTextNode(qq_id);
			SOAPElement element_param18 = root_param.addChildElement("subRespCode");
			element_param18.addTextNode(subResCode);
			SOAPElement element_param19 = root_param.addChildElement("subRespValue");
			element_param19.addTextNode(subResValue);
			SOAPElement element_param20 = root_param.addChildElement("doctype");
			element_param20.addTextNode("CFO");
			SOAPElement element_param21 = root_param.addChildElement("insCode");
			element_param21.addTextNode(insCode);
			request.saveChanges();
			String result = null;

			if (request != null) {
				ByteArrayOutputStream baos = null;
				try {
					baos = new ByteArrayOutputStream();
					request.writeTo(baos);
					result = baos.toString();
				} catch (Exception e) {
				} finally {
					if (baos != null) {
						try {
							baos.close();
						} catch (IOException ioe) {
						}
					}
				}
			}
			System.out.println(result + "NCD confirm Insert");

			SOAPMessage response = dispatch.invoke(request);
			SOAPBody responsebody = response.getSOAPBody();

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

}
