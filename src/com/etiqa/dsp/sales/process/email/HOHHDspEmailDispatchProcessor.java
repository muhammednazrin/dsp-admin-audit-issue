package com.etiqa.dsp.sales.process.email;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.etiqa.dsp.dao.common.pojo.HOHHcustQuotPmntPolicyVo;
//import com.etiqa.dsp.dao.common.pojo.WTCcustQuotPmntPolicyVo;
//import com.etiqa.dsp.dao.common.pojo.custQuotPmntPolicyVo;
//import com.etiqa.dsp.dao.email.DspEmailTemplateBean;
import com.etiqa.dsp.dao.email.HohhDspEmailTemplateBean;
//import com.etiqa.dsp.dao.email.WtcDspEmailTemplateBean;

public class HOHHDspEmailDispatchProcessor {
	static Properties mailServerProperties;
	static Session getMailSession;
	static MimeMessage generateMailMessage;
	private static Properties activeDirMailProp = new Properties();
	HOHHProductMailTemplateLoad productTemplate = null;

	public HOHHDspEmailDispatchProcessor(HOHHProductMailTemplateLoad wtcProductMailTemplateLoadimpl) {
		this.productTemplate = wtcProductMailTemplateLoadimpl;
	}

	private String dispatchDspEmail(HohhDspEmailTemplateBean templateBean, HOHHcustQuotPmntPolicyVo custVo)
			throws FileNotFoundException {

		String res = null;
		String Subject = templateBean.getTemplateSubject();
		String Content = templateBean.getTemplateBody();

	
		// Assuming you are sending email from localhost
		String host = activeDirMailProp.getProperty("emailhost");
		String fromemail = activeDirMailProp.getProperty("from");
		String port = activeDirMailProp.getProperty("emailport");
		
		// Get system properties
		// SMTP Prod Email---
		Properties prop = System.getProperties();
		// prop.setProperty("mail.smtp.host", host);
		 prop.put("mail.smtp.host",host); 
		 prop.put("mail.smtp.port", port);
		 prop.put("mail.smtp.auth", "false");
		 prop.put("mail.smtp.starttls.enable", "false");
		 prop.put("mail.smtp.ssl.trust", host);
		 Session session = Session.getDefaultInstance(prop);
		// END SMPTP PROD
		// smtp.gmail.com


		try {
			// InternetAddress.parse(emailTo)
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromemail));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(templateBean.getEmailTo()));
			message.setSubject(templateBean.getTemplateSubject());
			message.setContent(templateBean.getTemplateBody(), "text/html; charset=utf-8");

			// messagee.setContent(message, "text/html; charset=utf-8");

			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(templateBean.getTemplateBody());
			messageBodyPart.setContent(templateBean.getTemplateBody(), "text/html; charset=utf-8");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			String filePath = "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs";
			String fileName = custVo.getPOLICY_NUMBER() + "-AllDocuments.zip";

			File file = new File(filePath + "/" + fileName);
			if (!file.exists()) {
				System.out.println("file not exist in eiwdocs");
				String filePath_backup = "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_backup";
				File file_backup = new File(filePath_backup + "/" + fileName);
				if (!file_backup.exists()) {
					System.out.println("file not exist in eiwdocs backup");
					String filePath_temp = "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_backup/admin";
					File file_temp = new File(filePath_temp + "/" + fileName);
					System.out.println("zipfilename------" + filePath_temp + "/" + fileName);
					if (file_temp.exists()) {
						System.out.println("file  exist in eiwdocs admin ");
					} else {
						System.out.println("file not exist in eiwdocs admin");
					}
					addAttachment(multipart, filePath_temp + "/" + fileName, fileName);
					// Send the complete message parts
					message.setContent(multipart);
					Transport.send(message);
					res = "Done";
				} else {
					System.out.println("zipfilename------" + filePath_backup + "/" + fileName);
					addAttachment(multipart, filePath_backup + "/" + fileName, fileName);
					// Send the complete message parts
					message.setContent(multipart);
					Transport.send(message);
					res = "Done";
				}

			} else {

				System.out.println("zipfilename------" + filePath + "/" + fileName);
				addAttachment(multipart, filePath + "/" + fileName, fileName);
				// Send the complete message parts
				message.setContent(multipart);
				Transport.send(message);
				res = "Done";

			}

			// String filePath = "/tmp/"+custVo.getPOLICY_NUMBER()+"/";
			/*
			 * for(GeneratingReportsResponseVo fileName : FileNamelist){ String filename=
			 * fileName.getFileName(); System.out.println("Fin Obj: "+filename);
			 * addAttachment(multipart, filePath+filename,filename); }
			 */
		} catch (MessagingException e) {
			// throw new RuntimeException(e);
			res = e.getMessage();
			// res=RuntimeException(e);
			// Messaging
		}
		return res;
	}

	private static void addAttachment(Multipart multipart, String filename, String singleFileName)
			throws MessagingException {
		DataSource source = new FileDataSource(filename);
		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(singleFileName);
		multipart.addBodyPart(messageBodyPart);
	}

	public String processDspMailConfigurations() {

		return null;
	}

	// Load the email properties
	private void loadEmailProps() {
		InputStream infoad = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/dsp/wcservices/email/emailconfig.properties");
		try {
			activeDirMailProp.load(infoad);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String emailDispatchProcess(HOHHcustQuotPmntPolicyVo custVo) throws FileNotFoundException {
		System.out.println("Email ------------------------sucessful");
		HohhDspEmailTemplateBean templateBean = null;

		// Load mail properties
		loadEmailProps();

		// Load of the Policy
		if (productTemplate != null) {
			templateBean = productTemplate.loadMailTemplate(custVo);
		}
		// send the mail
		String status = dispatchDspEmail(templateBean, custVo);
		return status;
	}

}
