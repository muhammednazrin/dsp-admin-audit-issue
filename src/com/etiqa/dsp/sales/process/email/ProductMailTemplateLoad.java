package com.etiqa.dsp.sales.process.email;

import java.util.List;

import com.etiqa.dsp.dao.common.pojo.MotorInsuranceCustDetails;
import com.etiqa.dsp.dao.common.pojo.custQuotPmntPolicyVo;
import com.etiqa.dsp.dao.email.DspEmailTemplateBean;

public interface ProductMailTemplateLoad {

	public DspEmailTemplateBean loadMailTemplate(custQuotPmntPolicyVo custVo,
			List<MotorInsuranceCustDetails> listOfAdditionalCoverage);

	public DspEmailTemplateBean loadMailTemplateBM(custQuotPmntPolicyVo custVo,
			List<MotorInsuranceCustDetails> listOfAdditionalCoverage);

	public DspEmailTemplateBean loadRejectionMailTemplate(String entityType);
}
