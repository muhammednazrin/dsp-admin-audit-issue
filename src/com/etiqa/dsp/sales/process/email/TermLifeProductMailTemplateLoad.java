package com.etiqa.dsp.sales.process.email;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import com.etiqa.dsp.dao.common.motor.StreamUtil;
import com.etiqa.dsp.dao.common.motor.StreamUtilBM;
import com.etiqa.dsp.dao.common.pojo.MotorInsuranceCustDetails;
import com.etiqa.dsp.dao.common.pojo.custQuotPmntPolicyVo;
import com.etiqa.dsp.dao.email.DspEmailTemplateBean;

public class TermLifeProductMailTemplateLoad implements ProductMailTemplateLoad {

	@Override
	@SuppressWarnings("resource")
	public DspEmailTemplateBean loadMailTemplate(custQuotPmntPolicyVo custVo,
			List<MotorInsuranceCustDetails> listOfAdditionalCoverage) {
		/*
		 * System.out.println(custVo.getCUSTOMER_NAME()+"c name\n"+custVo.
		 * getCUSTOMER_EMAIL()+"email ID"); DspEmailTemplateBean templateBean = new
		 * DspEmailTemplateBean(); String subject = "Etiqa Term Life Inusrance Policy";
		 * String templateBody = "Dear "+custVo.getCUSTOMER_NAME() +
		 * "\n Congratulations on successfully purchasing the insurance Policy no :"
		 * +custVo.getPOLICY_NUMBER()+ "\n\n\nRegards," + "Etiqa Insurance BHD";
		 *
		 * templateBean.setTemplateSubject(subject);
		 * templateBean.setTemplateBody(templateBody);
		 * templateBean.setEmailTo(custVo.getCUSTOMER_EMAIL());
		 *
		 * return templateBean;
		 */
		System.out.println(custVo.getCUSTOMER_NAME() + "c name\n" + custVo.getCUSTOMER_EMAIL() + "email ID");
		DspEmailTemplateBean templateBean = new DspEmailTemplateBean();

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy  h:mm:ss a");
		String formattedDate = sdf.format(date);

		String subject = "Motor Insurance Policy Detail (" + custVo.getPOLICY_NUMBER() + ")";
		String message = "";
		File log = null;
		InputStream stream;
		String filePath = "";
		try (InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/dsp/sales/process/email/documents.properties")) {
			Properties prop = new Properties();
			prop.load(in);
			String htmlFileTemplate = "com/etiqa/dsp/policy/templates/Test.txt";
			InputStream input = this.getClass().getClassLoader().getResourceAsStream(htmlFileTemplate);
			log = StreamUtil.stream2file(input);
			System.out.println(log.getAbsolutePath() + "Absolute Path in Html load");

			String search = "CustomerNameValue";
			String replace = custVo.getCUSTOMER_NAME();
			String search1 = "VehicleNumberValue";
			String replace1 = custVo.getVehRegNo();
			String search2 = "ePolicyNumberValue";
			String replace2 = custVo.getPOLICY_NUMBER();
			String search3 = "ExpiryDateValue";
			String toDate = custVo.getPOLICY_EXPIRY_TIMESTAMP();

			toDate = toDate.substring(0, 2) + "-" + toDate.substring(2, toDate.length());
			toDate = toDate.substring(0, 5) + "-" + toDate.substring(5, toDate.length());

			String replace3 = toDate;

			String addCoverageSearch1 = "TickMarkValue1";
			String source1 = "Passenger Legal Liability Coverage";
			String addCoverageReplace1 = getAdditionalCoverageValue(source1, listOfAdditionalCoverage);

			String addCoverageSearch2 = "TickMarkValue2";
			String source2 = "Legal Liability of Pessenger for Acts of Negligence Coverage";
			String addCoverageReplace2 = getAdditionalCoverageValue(source2, listOfAdditionalCoverage);

			String addCoverageSearch3 = "TickMarkValue3";
			String source3 = "Extended Flood Cover";
			String addCoverageReplace3 = getAdditionalCoverageValue(source3, listOfAdditionalCoverage);

			String addCoverageSearch4 = "TickMarkValue4";
			String source4 = "Basic Flood Cover";
			String addCoverageReplace4 = getAdditionalCoverageValue(source4, listOfAdditionalCoverage);

			String addCoverageSearch5 = "TickMarkValue5";
			String source5 = "NCD Relief";
			String addCoverageReplace5 = getAdditionalCoverageValue(source5, listOfAdditionalCoverage);

			String addCoverageSearch6 = "TickMarkValue6";
			String source6 = "Windscreen Coverage";
			String addCoverageReplace6 = getAdditionalCoverageValue(source6, listOfAdditionalCoverage);

			String addCoverageSearch7 = "TickMarkValue7";
			String source7 = "Vehicle Accessories";
			String addCoverageReplace7 = getAdditionalCoverageValue(source7, listOfAdditionalCoverage);

			String addCoverageSearch8 = "TickMarkValue8";
			String source8 = "NGV Gas";
			String addCoverageReplace8 = getAdditionalCoverageValue(source8, listOfAdditionalCoverage);

			String addCoverageSearch9 = "TickMarkValue9";
			String source9 = "Compensation for Assessed Repair Time";
			String addCoverageReplace9 = getAdditionalCoverageValue(source9, listOfAdditionalCoverage);

			String addCoverageSearch10 = "ExtraTickMark1";
			String source10 = "ADD NAMED DRIVER";
			String addCoverageReplace10 = getAdditionalCoverageValue(source10, listOfAdditionalCoverage);

			String addCoverageSearch11 = "ExtraTickMark2";
			String source11 = "Strike, Riot & Civil Commotion";
			String addCoverageReplace11 = getAdditionalCoverageValue(source11, listOfAdditionalCoverage);

			/*
			 * Passenger Legal Liability Coverage Legal Liability of Pessenger for Acts of
			 * Negligence Coverage Extended Flood Cover Basic Flood Cover NCD Relief
			 * Windscreen Coverage Vehicle Accessories NGV Gas Compensation for Assessed
			 * Repair Time ADD NAMED DRIVER Strike, Riot & Civil Commotion
			 */

			try {
				FileReader fr = new FileReader(log);
				String s;
				String totalStr = "";
				try (BufferedReader br = new BufferedReader(fr)) {

					while ((s = br.readLine()) != null) {
						totalStr += s;
					}
					totalStr = totalStr.replaceAll(search, replace);
					totalStr = totalStr.replaceAll(search1, replace1);
					totalStr = totalStr.replaceAll(search2, replace2);
					totalStr = totalStr.replaceAll(search3, replace3);

					totalStr = totalStr.replaceAll(addCoverageSearch1, addCoverageReplace1);
					totalStr = totalStr.replaceAll(addCoverageSearch2, addCoverageReplace2);
					totalStr = totalStr.replaceAll(addCoverageSearch3, addCoverageReplace3);
					totalStr = totalStr.replaceAll(addCoverageSearch4, addCoverageReplace4);
					totalStr = totalStr.replaceAll(addCoverageSearch5, addCoverageReplace5);
					totalStr = totalStr.replaceAll(addCoverageSearch6, addCoverageReplace6);
					totalStr = totalStr.replaceAll(addCoverageSearch7, addCoverageReplace7);
					totalStr = totalStr.replaceAll(addCoverageSearch8, addCoverageReplace8);
					totalStr = totalStr.replaceAll(addCoverageSearch9, addCoverageReplace9);
					totalStr = totalStr.replaceAll(addCoverageSearch10, addCoverageReplace10);
					totalStr = totalStr.replaceAll(addCoverageSearch11, addCoverageReplace11);
					FileWriter fw = new FileWriter(log);
					fw.write(totalStr);
					fw.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			stream = new FileInputStream(log);
			byte[] b = new byte[stream.available()];
			stream.read(b);
			message = new String(b);
			System.out.println("Successfully Changed Text");
			/*
			 * String Temp=null;
			 *
			 * Temp=search; search=replace; replace=Temp;
			 *
			 * Temp=search1; search1=replace1; replace1=Temp;
			 *
			 * Temp=search2; search2=replace2; replace2=Temp;
			 *
			 * Temp=search3; search3=replace3; replace3=Temp;
			 *
			 * Temp=addCoverageSearch1; addCoverageSearch1=addCoverageReplace1;
			 * addCoverageReplace1=Temp;
			 *
			 * Temp=addCoverageSearch2; addCoverageSearch2=addCoverageReplace2;
			 * addCoverageReplace2=Temp;
			 *
			 * Temp=addCoverageSearch3; addCoverageSearch3=addCoverageReplace3;
			 * addCoverageReplace3=Temp;
			 *
			 * Temp=addCoverageSearch4; addCoverageSearch4=addCoverageReplace4;
			 * addCoverageReplace4=Temp;
			 *
			 * Temp=addCoverageSearch5; addCoverageSearch5=addCoverageReplace5;
			 * addCoverageReplace5=Temp;
			 *
			 * Temp=addCoverageSearch6; addCoverageSearch6=addCoverageReplace6;
			 * addCoverageReplace6=Temp;
			 *
			 * Temp=addCoverageSearch7; addCoverageSearch7=addCoverageReplace7;
			 * addCoverageReplace7=Temp;
			 *
			 * Temp=addCoverageSearch8; addCoverageSearch8=addCoverageReplace8;
			 * addCoverageReplace8=Temp;
			 *
			 * Temp=addCoverageSearch9; addCoverageSearch9=addCoverageReplace9;
			 * addCoverageReplace9=Temp;
			 *
			 * Temp=addCoverageSearch10; addCoverageSearch1=addCoverageReplace10;
			 * addCoverageReplace10=Temp;
			 *
			 * Temp=addCoverageSearch11; addCoverageSearch11=addCoverageReplace11;
			 * addCoverageReplace11=Temp;
			 *
			 *
			 *
			 * try{ FileReader fr = new FileReader(log); String s; String totalStr = ""; try
			 * (BufferedReader br = new BufferedReader(fr)) {
			 *
			 * while ((s = br.readLine()) != null) { totalStr += s; } totalStr =
			 * totalStr.replaceAll(search, replace); totalStr = totalStr.replaceAll(search1,
			 * replace1); totalStr = totalStr.replaceAll(search2, replace2); FileWriter fw =
			 * new FileWriter(log); fw.write(totalStr); fw.close(); } }catch(Exception e){
			 * e.printStackTrace(); }
			 */
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Successfully Undo Text");
		String templateBody = message;
		templateBean.setTemplateSubject(subject);
		templateBean.setTemplateBody(templateBody);
		templateBean.setEmailTo(custVo.getCUSTOMER_EMAIL());
		return templateBean;
	}

	public String getAdditionalCoverageValue(String source, List<MotorInsuranceCustDetails> listOfAdditionalCoverage) {
		String resultOfStatus = null;
		int count = 0;
		if (listOfAdditionalCoverage.size() != 0) {
			for (MotorInsuranceCustDetails AdditionalInfo : listOfAdditionalCoverage) {
				if (AdditionalInfo.getAdditionalCoverageText().trim().equals(source)) {
					count = 1;
				}

				if (count == 1) {
					resultOfStatus = "<img class=\"center\" style=\"outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block;border: 0;height: auto;line-height: 100%;margin: 0 auto;float: none;width: 24px;max-width: 24px\" align=\"center\" border=\"0\" src=\"https://www.motortakaful.com/images/green%20tick.png\" alt=\"Image\" title=\"Image\" width=\"24\">";
				} else {
					resultOfStatus = "N/A";
				}
			}
		} else {
			resultOfStatus = "N/A";
		}
		return resultOfStatus;
	}

	@Override
	public DspEmailTemplateBean loadMailTemplateBM(custQuotPmntPolicyVo custVo,
			List<MotorInsuranceCustDetails> listOfAdditionalCoverage) {
		/*
		 * System.out.println(custVo.getCUSTOMER_NAME()+"c name\n"+custVo.
		 * getCUSTOMER_EMAIL()+"email ID"); DspEmailTemplateBean templateBean = new
		 * DspEmailTemplateBean(); String subject = "Etiqa Term Life Inusrance Policy";
		 * String templateBody = "Dear "+custVo.getCUSTOMER_NAME() +
		 * "\n Congratulations on successfully purchasing the insurance Policy no :"
		 * +custVo.getPOLICY_NUMBER()+ "\n\n\nRegards," + "Etiqa Insurance BHD";
		 *
		 * templateBean.setTemplateSubject(subject);
		 * templateBean.setTemplateBody(templateBody);
		 * templateBean.setEmailTo(custVo.getCUSTOMER_EMAIL());
		 *
		 * return templateBean;
		 */
		System.out.println(custVo.getCUSTOMER_NAME() + "c name\n" + custVo.getCUSTOMER_EMAIL() + "email ID");
		DspEmailTemplateBean templateBean = new DspEmailTemplateBean();

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy  h:mm:ss a");
		String formattedDate = sdf.format(date);

		String subject = "Motor Insurance Policy Detail (" + custVo.getPOLICY_NUMBER() + ")";
		String message = "";
		File log = null;
		InputStream stream;
		String filePath = "";
		try (InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/dsp/sales/process/email/documents.properties")) {
			Properties prop = new Properties();
			prop.load(in);
			String htmlFileTemplate = "com/etiqa/dsp/policy/templates/TestBM.txt";
			InputStream input = this.getClass().getClassLoader().getResourceAsStream(htmlFileTemplate);
			log = StreamUtilBM.stream2file(input);
			System.out.println(log.getAbsolutePath() + "Absolute Path in Html load");

			String search = "CustomerNameValue";
			String replace = custVo.getCUSTOMER_NAME();
			String search1 = "VehicleNumberValue";
			String replace1 = custVo.getVehRegNo();
			String search2 = "ePolicyNumberValue";
			String replace2 = custVo.getPOLICY_NUMBER();
			String search3 = "ExpiryDateValue";
			String toDate = custVo.getPOLICY_EXPIRY_TIMESTAMP();

			toDate = toDate.substring(0, 2) + "-" + toDate.substring(2, toDate.length());
			toDate = toDate.substring(0, 5) + "-" + toDate.substring(5, toDate.length());

			String replace3 = toDate;

			String addCoverageSearch1 = "TickMarkValue1";
			String source1 = "Perlindungan Liabiliti Undang-Undang Penumpang";
			String addCoverageReplace1 = getAdditionalCoverageValue(source1, listOfAdditionalCoverage);

			String addCoverageSearch2 = "TickMarkValue2";
			String source2 = "Liabiliti Undang-Undang kepada Penumpang";
			String addCoverageReplace2 = getAdditionalCoverageValue(source2, listOfAdditionalCoverage);

			String addCoverageSearch3 = "TickMarkValue3";
			String source3 = "Manfaat Banjir Tambahan";
			String addCoverageReplace3 = getAdditionalCoverageValue(source3, listOfAdditionalCoverage);

			String addCoverageSearch4 = "TickMarkValue4";
			String source4 = "Manfaat Banjir Asas";
			String addCoverageReplace4 = getAdditionalCoverageValue(source4, listOfAdditionalCoverage);

			String addCoverageSearch5 = "TickMarkValue5";
			String source5 = "Pelepasan Diskaun Tanpa Tuntutan";
			String addCoverageReplace5 = getAdditionalCoverageValue(source5, listOfAdditionalCoverage);

			String addCoverageSearch6 = "TickMarkValue6";
			String source6 = "Perlindungan Cermin Kenderaan";
			String addCoverageReplace6 = getAdditionalCoverageValue(source6, listOfAdditionalCoverage);

			String addCoverageSearch7 = "TickMarkValue7";
			String source7 = "Eksesori Kenderaan";
			String addCoverageReplace7 = getAdditionalCoverageValue(source7, listOfAdditionalCoverage);

			String addCoverageSearch8 = "TickMarkValue8";
			String source8 = "Peralatan Penukaran Gas dan Tangki";
			String addCoverageReplace8 = getAdditionalCoverageValue(source8, listOfAdditionalCoverage);

			String addCoverageSearch9 = "TickMarkValue9";
			String source9 = "Pampasan Untuk Pembaikan Taksiran (CART)";
			String addCoverageReplace9 = getAdditionalCoverageValue(source9, listOfAdditionalCoverage);

			String addCoverageSearch10 = "ExtraTickMark1";
			String source10 = "Pemandu Tambahan";
			String addCoverageReplace10 = getAdditionalCoverageValue(source10, listOfAdditionalCoverage);

			String addCoverageSearch11 = "ExtraTickMark2";
			String source11 = "Mogok, Rusuhan dan Kekacauan Awam";
			String addCoverageReplace11 = getAdditionalCoverageValue(source11, listOfAdditionalCoverage);

			/*
			 * Passenger Legal Liability Coverage Legal Liability of Pessenger for Acts of
			 * Negligence Coverage Extended Flood Cover Basic Flood Cover NCD Relief
			 * Windscreen Coverage Vehicle Accessories NGV Gas Compensation for Assessed
			 * Repair Time ADD NAMED DRIVER Strike, Riot & Civil Commotion
			 */

			try {
				FileReader fr = new FileReader(log);
				String s;
				String totalStr = "";
				try (BufferedReader br = new BufferedReader(fr)) {

					while ((s = br.readLine()) != null) {
						totalStr += s;
					}
					totalStr = totalStr.replaceAll(search, replace);
					totalStr = totalStr.replaceAll(search1, replace1);
					totalStr = totalStr.replaceAll(search2, replace2);
					totalStr = totalStr.replaceAll(search3, replace3);

					totalStr = totalStr.replaceAll(addCoverageSearch1, addCoverageReplace1);
					totalStr = totalStr.replaceAll(addCoverageSearch2, addCoverageReplace2);
					totalStr = totalStr.replaceAll(addCoverageSearch3, addCoverageReplace3);
					totalStr = totalStr.replaceAll(addCoverageSearch4, addCoverageReplace4);
					totalStr = totalStr.replaceAll(addCoverageSearch5, addCoverageReplace5);
					totalStr = totalStr.replaceAll(addCoverageSearch6, addCoverageReplace6);
					totalStr = totalStr.replaceAll(addCoverageSearch7, addCoverageReplace7);
					totalStr = totalStr.replaceAll(addCoverageSearch8, addCoverageReplace8);
					totalStr = totalStr.replaceAll(addCoverageSearch9, addCoverageReplace9);
					totalStr = totalStr.replaceAll(addCoverageSearch10, addCoverageReplace10);
					totalStr = totalStr.replaceAll(addCoverageSearch11, addCoverageReplace11);
					FileWriter fw = new FileWriter(log);
					fw.write(totalStr);
					fw.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			stream = new FileInputStream(log);
			byte[] b = new byte[stream.available()];
			stream.read(b);
			message = new String(b);
			System.out.println("Successfully Changed Text");
			/*
			 * String Temp=null;
			 *
			 * Temp=search; search=replace; replace=Temp;
			 *
			 * Temp=search1; search1=replace1; replace1=Temp;
			 *
			 * Temp=search2; search2=replace2; replace2=Temp;
			 *
			 * Temp=search3; search3=replace3; replace3=Temp;
			 *
			 * Temp=addCoverageSearch1; addCoverageSearch1=addCoverageReplace1;
			 * addCoverageReplace1=Temp;
			 *
			 * Temp=addCoverageSearch2; addCoverageSearch2=addCoverageReplace2;
			 * addCoverageReplace2=Temp;
			 *
			 * Temp=addCoverageSearch3; addCoverageSearch3=addCoverageReplace3;
			 * addCoverageReplace3=Temp;
			 *
			 * Temp=addCoverageSearch4; addCoverageSearch4=addCoverageReplace4;
			 * addCoverageReplace4=Temp;
			 *
			 * Temp=addCoverageSearch5; addCoverageSearch5=addCoverageReplace5;
			 * addCoverageReplace5=Temp;
			 *
			 * Temp=addCoverageSearch6; addCoverageSearch6=addCoverageReplace6;
			 * addCoverageReplace6=Temp;
			 *
			 * Temp=addCoverageSearch7; addCoverageSearch7=addCoverageReplace7;
			 * addCoverageReplace7=Temp;
			 *
			 * Temp=addCoverageSearch8; addCoverageSearch8=addCoverageReplace8;
			 * addCoverageReplace8=Temp;
			 *
			 * Temp=addCoverageSearch9; addCoverageSearch9=addCoverageReplace9;
			 * addCoverageReplace9=Temp;
			 *
			 * Temp=addCoverageSearch10; addCoverageSearch1=addCoverageReplace10;
			 * addCoverageReplace10=Temp;
			 *
			 * Temp=addCoverageSearch11; addCoverageSearch11=addCoverageReplace11;
			 * addCoverageReplace11=Temp;
			 *
			 *
			 *
			 * try{ FileReader fr = new FileReader(log); String s; String totalStr = ""; try
			 * (BufferedReader br = new BufferedReader(fr)) {
			 *
			 * while ((s = br.readLine()) != null) { totalStr += s; } totalStr =
			 * totalStr.replaceAll(search, replace); totalStr = totalStr.replaceAll(search1,
			 * replace1); totalStr = totalStr.replaceAll(search2, replace2); FileWriter fw =
			 * new FileWriter(log); fw.write(totalStr); fw.close(); } }catch(Exception e){
			 * e.printStackTrace(); }
			 */
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Successfully Undo Text");
		String templateBody = message;
		templateBean.setTemplateSubject(subject);
		templateBean.setTemplateBody(templateBody);
		templateBean.setEmailTo(custVo.getCUSTOMER_EMAIL());
		return templateBean;
	}

	@Override
	public DspEmailTemplateBean loadRejectionMailTemplate(String entityType) {
		System.out.println("loadRejectionMailTemplate ");
		DspEmailTemplateBean templateBean = new DspEmailTemplateBean();

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy  h:mm:ss a");
		String formattedDate = sdf.format(date);

		String subject = "Rejection Policy Report for  " + entityType;
		String message = "";
		message += "<b>Dear admin,</b><br><br><br>";
		message += "Please refer to attachment for the rejection report for product entity: " + entityType;
		message += "<br><br>";
		message += "<br><br>";
		message += "Note: This is an auto email notification. Please do not reply this email. You may call our Etiqa";
		message += "Oneline call centre at 1300 13 8888 or email us at info@etiqa.com.my.<br><br>";

		message += "Thank you.<br><br>";
		message += "Yours Sincerely,<br>";
		message += "Etiqa Life Insurance Berhad<br><br>";

		String templateBody = message;
		templateBean.setTemplateSubject(subject);
		templateBean.setTemplateBody(templateBody);

		return templateBean;

	}
}
