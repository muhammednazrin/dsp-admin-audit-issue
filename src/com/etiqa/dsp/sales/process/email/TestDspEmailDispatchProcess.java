package com.etiqa.dsp.sales.process.email;

import javax.xml.ws.Endpoint;

public class TestDspEmailDispatchProcess {

	public static void main(String args[]) {

		Endpoint.publish("http://localhost:7002/ws/dspemaildispatcher", new DspEmailDispatcher());

	}
}
