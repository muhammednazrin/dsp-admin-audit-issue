package com.etiqa.dsp.sales.process.email;

import com.etiqa.dsp.dao.common.pojo.HOHHcustQuotPmntPolicyVo;
//import com.etiqa.dsp.dao.common.pojo.WTCcustQuotPmntPolicyVo;
import com.etiqa.dsp.dao.email.HohhDspEmailTemplateBean;
//import com.etiqa.dsp.dao.email.DspEmailTemplateBean;
//import com.etiqa.dsp.dao.email.WtcDspEmailTemplateBean;

public class HOHHProductMailTemplateLoadimpl implements HOHHProductMailTemplateLoad {

	@Override
	public HohhDspEmailTemplateBean loadMailTemplate(HOHHcustQuotPmntPolicyVo custVo) {
		HohhDspEmailTemplateBean templateBean = new HohhDspEmailTemplateBean();
		String productvalu = custVo.getHOME_COVERAGE().toString().trim();
		String Productname = "";
		String BMProductname = "";
		if (productvalu.equals("building_content")) {
			Productname = "HOUSEOWNER/HOUSEHOLDER";
			BMProductname = "Empunya Rumah & Isi Rumah";
			System.out.println(Productname);

		}
		if (productvalu.equals("building")) {
			Productname = "HOUSEOWNER";
			BMProductname = "Empunya Rumah";
		}
		if (productvalu.equals("content")) {
			Productname = "HOUSEHOLDER";
			BMProductname = "Isi Rumah";
		}

		String Language = custVo.getHOHH_LANGUAGE().toString().trim();
		System.out.println("language is     :" + Language);
		if (Language.equals("E")) {
			System.out.println(custVo.getCUSTOMER_NAME() + "c name\n" + custVo.getCUSTOMER_EMAIL() + "      email ID");

			String subject = "Etiqa  " + Productname + "  e-Policy No. (" + custVo.getPOLICY_NUMBER() + ")";
			// String templateBody = "Dear "+custVo.getCUSTOMER_NAME()
			// + "\n Congratulations on successfully purchasing the insurance Policy no :
			// "+custVo.getPOLICY_NUMBER()+ "\n\n\nRegards,"
			// + "Etiqa Insurance BHD";
			// String test="test"; Householder E-Policy No. (FF003250) Etiqa
			// Houseowner/Householder e-Policy No. (FF003253)
			// String color="background-color:#FFC000";

			String message = "<i> </i><br>";
			message += "<b>Dear &nbsp;&nbsp;" + custVo.getCUSTOMER_NAME() + ",</b><br><br>";
			message += "Thank you for subscribing " + Productname + " plan.<br>";
			message += "Please find attached a copy of your e-Policy and Tax Invoice/Receipt for your reference:<br><br>";
			message += Productname + "<br>";
			// custVo.getPOLICY_EFFECTIVE_TIMESTAMP()
			// message += "Please keep all original receipts and related documents (i.e.:
			// boarding pass, e-ticket/flight ticket, airline letter/report of luggage
			// delayed/damaged, police report) in case you need to make a claim<br>";
			// message += "Please find attached a copy of your e-Policy and Tax Invoice./
			// Receipt for your reference:<br><br>";
			// message += "World Traveller Care<br><br>";

			message += "<hr>";
			// message += "<br>";
			message += "<table> <col width=" + 250 + ">";

			message += "<tr><td><b>Transaction Date </b></td><td><b>:</b>" + custVo.getTRANSACTION_DATETIME()
					+ "</td></tr><br>";
			message += "<tr><td><b>Transaction No.</b></td><td><b>:</b>" + custVo.getTRANSACTION_ID()
					+ "</td></tr><br>";
			message += "<tr><td><b>Policy Holder Name</b></td><td><b>:</b>" + custVo.getCUSTOMER_NAME()
					+ "</td></tr><br>";
			message += "<tr><td><b>NRIC No.</b></td><td><b>:</b>" + custVo.getCUSTOMER_NRIC_ID() + "</td></tr><br>";
			message += "<tr><td><b>Effective Date</b></td><td><b>:</b>" + custVo.getTRANSACTION_DATETIME()
					+ "</td></tr><br>";
			message += "<tr><td><b>e-Policy No.</b></td><td><b>:</b>" + custVo.getPOLICY_NUMBER() + "</td></tr><br>";
			message += "<tr><td><b>Tax Invoice No </b></td><td><b>:</b>" + custVo.getINVOICE_NO() + "</td></tr><br>";
			message += "<tr><td><b>Payment Mode </b></td><td><b>:</b>" + custVo.getPMNT_GATEWAY_CODE()
					+ "</td></tr><br>";
			message += "<tr><td><b>Premium Amount (RM)</b></td><td><b>:</b>" + custVo.getAMOUNT() + "</td></tr><br>";
			message += "</table> <br><br><br>";

			// message += "Transaction Date
			// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			// <b>:</b>&nbsp;&nbsp; "+custVo.getTRANSACTION_DATETIME()+ "<br>";
			// message += "Transaction No.
			// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			// <b>:</b>&nbsp;&nbsp; "+custVo.getTRANSACTION_ID()+ "<br>";
			// message += "Policy Holder Name
			// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			// <b>:</b>&nbsp;&nbsp;"+custVo.getCUSTOMER_NAME()+"<br>";
			// message += "NRIC No.
			// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>:</b>&nbsp;&nbsp;"+custVo.getCUSTOMER_NRIC_ID()+
			// "<br>";
			//// message += "Plan Name
			// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			// <b>:</b>&nbsp;&nbsp;"+custVo.getCOUNTRY_1()+ "<br>";
			//// message += "Contribution Frequency &nbsp;&nbsp;&nbsp;&nbsp;
			// <b>:</b>&nbsp;&nbsp;"+custVo.getTRIP_TYPE()+ "<br>";
			// message += "Effective Date
			// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>:</b>&nbsp;&nbsp;"+custVo.getTRANSACTION_DATETIME()+
			// "<br>";
			// message += "e-Policy No. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			// <b>:</b>&nbsp;&nbsp;;"+custVo.getPOLICY_NUMBER()+ "<br>";
			// message += "Tax Invoice No
			// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			// <b>:</b>&nbsp;&nbsp;"+custVo.getINVOICE_NO()+ "<br>";
			// message += "Payment Mode
			// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			// <b>:</b>&nbsp;&nbsp;"+custVo.getPMNT_GATEWAY_CODE()+ "<br>";
			// message += "Premium Amount (RM) &nbsp;&nbsp;&nbsp;&nbsp;
			// <b>:</b>&nbsp;&nbsp;"+custVo.getAMOUNT()+"<br><br><br>";
			// message += "Please visit www.etiqa.com.my if you would like to make a
			// nomination for this policy.<br><br><br>";
			// message += "<div style="+color+">Travel with a peace of mind knowing that
			// your house is protected while you�re away with our Houseonwer/Householder
			// home protection plan.</br> "
			// + "Enjoy a 15% discount when you sign up online. Click here to find out more.
			// </br></div><br>";
			// message += "Travel with a peace of mind knowing that your house is protected
			// while you�re away with our Houseonwer/Householder home protection plan.<br>";
			// message += "Enjoy a 15% discount when you sign up online. Click here to find
			// out more.<br>";

			message += "<b><font>Note:  &nbsp; &nbsp;</font></b>";
			message += "This is an auto email notification. Please do not reply this email. You may call our Etiqa Oneline call centre at 1300 13 8888 or email us at  info@etiqa.com.my.<br>";

			message += "Thank you.<br><br>";
			message += "Yours Sincerely,<br><br>";
			message += "Etiqa Insurance Berhad<br><br>";

			templateBean.setTemplateSubject(subject);
			templateBean.setTemplateBody(message);
			templateBean.setEmailTo(custVo.getCUSTOMER_EMAIL());

		} // End of english version

		if (Language.equals("B")) {
			System.out.println(custVo.getCUSTOMER_NAME() + "c name\n" + custVo.getCUSTOMER_EMAIL() + "      email ID");

			String subject = "Etiqa  " + BMProductname + "  No.e-Polisi (" + custVo.getPOLICY_NUMBER() + ")";
			// String templateBody = "Dear "+custVo.getCUSTOMER_NAME()
			// + "\n Congratulations on successfully purchasing the insurance Policy no :
			// "+custVo.getPOLICY_NUMBER()+ "\n\n\nRegards,"
			// + "Etiqa Insurance BHD";
			// String test="test"; Householder E-Policy No. (FF003250) Etiqa
			// Houseowner/Householder e-Policy No. (FF003253)
			// String color="background-color:#FFC000";

			String message = "<i> </i><br>";
			message += "<b>Kepada &nbsp;&nbsp;" + custVo.getCUSTOMER_NAME() + ",</b><br><br>";
			message += "Terima kasih kerana mendaftar pelan" + BMProductname + " Etiqa.<br>";
			message += "Salinan e-Polisi dan Invois Cukai/Resit dilampirkan di sini sebagai rujukan anda.<br><br>";
			message += BMProductname + "<br>";
			// custVo.getPOLICY_EFFECTIVE_TIMESTAMP()
			// message += "Please keep all original receipts and related documents (i.e.:
			// boarding pass, e-ticket/flight ticket, airline letter/report of luggage
			// delayed/damaged, police report) in case you need to make a claim<br>";
			// message += "Please find attached a copy of your e-Policy and Tax Invoice./
			// Receipt for your reference:<br><br>";
			// message += "World Traveller Care<br><br>";

			message += "<hr>";
			// message += "<br>";
			message += "<table> <col width=" + 250 + ">";
			message += "<tr><td><b>Tarikh Transaksi </b></td><td><b>:</b>" + custVo.getTRANSACTION_DATETIME()
					+ "</td></tr><br>";
			message += "<tr><td><b>No. Transaksi</b></td><td><b>:</b>" + custVo.getTRANSACTION_ID() + "</td></tr><br>";
			message += "<tr><td><b>Nama Pemegang Polisi</b></td><td><b>:</b>" + custVo.getCUSTOMER_NAME()
					+ "</td></tr><br>";
			message += "<tr><td><b>No. KP Baru</b></td><td><b>:</b>" + custVo.getCUSTOMER_NRIC_ID() + "</td></tr><br>";
			message += "<tr><td><b>Tarikh Kuatkuasa</b></td><td><b>:</b>" + custVo.getTRANSACTION_DATETIME()
					+ "</td></tr><br>";
			message += "<tr><td><b>No. e-Polisi</b></td><td><b>:</b>" + custVo.getPOLICY_NUMBER() + "</td></tr><br>";
			message += "<tr><td><b>No. Invois Cukai </b></td><td><b>:</b>" + custVo.getINVOICE_NO() + "</td></tr><br>";
			message += "<tr><td><b>Cara Pembayaran </b></td><td><b>:</b>" + custVo.getPMNT_GATEWAY_CODE()
					+ "</td></tr><br>";
			message += "<tr><td><b>Jumlah Premium (RM)</b></td><td><b>:</b>" + custVo.getAMOUNT() + "</td></tr><br>";
			message += "</table><br><br><br>";

			// message += "Transaction Date
			// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			// <b>:</b>&nbsp;&nbsp; "+custVo.getTRANSACTION_DATETIME()+ "<br>";
			// message += "Transaction No.
			// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			// <b>:</b>&nbsp;&nbsp; "+custVo.getTRANSACTION_ID()+ "<br>";
			// message += "Policy Holder Name
			// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			// <b>:</b>&nbsp;&nbsp;"+custVo.getCUSTOMER_NAME()+"<br>";
			// message += "NRIC No.
			// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>:</b>&nbsp;&nbsp;"+custVo.getCUSTOMER_NRIC_ID()+
			// "<br>";
			//// message += "Plan Name
			// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			// <b>:</b>&nbsp;&nbsp;"+custVo.getCOUNTRY_1()+ "<br>";
			//// message += "Contribution Frequency &nbsp;&nbsp;&nbsp;&nbsp;
			// <b>:</b>&nbsp;&nbsp;"+custVo.getTRIP_TYPE()+ "<br>";
			// message += "Effective Date
			// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>:</b>&nbsp;&nbsp;"+custVo.getTRANSACTION_DATETIME()+
			// "<br>";
			// message += "e-Policy No. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			// <b>:</b>&nbsp;&nbsp;;"+custVo.getPOLICY_NUMBER()+ "<br>";
			// message += "Tax Invoice No
			// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			// <b>:</b>&nbsp;&nbsp;"+custVo.getINVOICE_NO()+ "<br>";
			// message += "Payment Mode
			// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			// <b>:</b>&nbsp;&nbsp;"+custVo.getPMNT_GATEWAY_CODE()+ "<br>";
			// message += "Premium Amount (RM) &nbsp;&nbsp;&nbsp;&nbsp;
			// <b>:</b>&nbsp;&nbsp;"+custVo.getAMOUNT()+"<br><br><br>";

			// message += "Please visit www.etiqa.com.my if you would like to make a
			// nomination for this policy.<br><br><br>";
			// message += "<div style="+color+">Travel with a peace of mind knowing that
			// your house is protected while you�re away with our Houseonwer/Householder
			// home protection plan.</br> "
			// + "Enjoy a 15% discount when you sign up online. Click here to find out more.
			// </br></div><br>";
			// message += "Travel with a peace of mind knowing that your house is protected
			// while you�re away with our Houseonwer/Householder home protection plan.<br>";
			// message += "Enjoy a 15% discount when you sign up online. Click here to find
			// out more.<br>";

			message += "<b><font>Note:  &nbsp; &nbsp;</font></b>";
			message += "Ini adalah pemberitahuan e-mel secara automatik. Sila jangan membalas emel ini. Anda boleh menghubungi Etiqa Oneline di talian 1300 13 8888 atau email kepada kami di info@etiqa.com.my.<br>";

			message += "Terima kasih<br><br>";
			message += "Yang ikhlas,<br><br>";
			message += "Etiqa Insurance Berhad<br><br>";

			templateBean.setTemplateSubject(subject);
			templateBean.setTemplateBody(message);
			templateBean.setEmailTo(custVo.getCUSTOMER_EMAIL());

		} // End of english version
		return templateBean;

	}
}