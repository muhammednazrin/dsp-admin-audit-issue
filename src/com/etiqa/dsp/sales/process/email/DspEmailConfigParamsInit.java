package com.etiqa.dsp.sales.process.email;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DspEmailConfigParamsInit {

	private static Properties activeDirMailProp = null;

	public static void loadActiveDirMailProp() {
		InputStream activeDirMailPropInputSteam = DspEmailConfigParamsInit.class.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/dsp/wcservices/email/emailconfig.properties");
		if (activeDirMailProp == null) {
			activeDirMailProp = new Properties();
		}
		try {
			activeDirMailProp.load(activeDirMailPropInputSteam);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void loadEmailProps() {
		InputStream infoad = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/dsp/wcservices/email/emailconfig.properties");
		try {
			activeDirMailProp.load(infoad);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Properties getEmailProperties() {

		return activeDirMailProp;
	}

}
