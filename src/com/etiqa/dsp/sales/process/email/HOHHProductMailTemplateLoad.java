package com.etiqa.dsp.sales.process.email;

import com.etiqa.dsp.dao.common.pojo.HOHHcustQuotPmntPolicyVo;
import com.etiqa.dsp.dao.email.HohhDspEmailTemplateBean;

public interface HOHHProductMailTemplateLoad {

	public HohhDspEmailTemplateBean loadMailTemplate(HOHHcustQuotPmntPolicyVo custVo);

}
