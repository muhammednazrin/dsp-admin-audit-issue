package com.etiqa.dsp.sales.process.email;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.etiqa.dsp.dao.common.pojo.GeneratingReportsResponseVo;
import com.etiqa.dsp.dao.common.pojo.MotorInsuranceCustDetails;
import com.etiqa.dsp.dao.common.pojo.custQuotPmntPolicyVo;

@WebService
public class DspEmailDispatcher {

	@WebMethod
	public String sendDspEmail(@WebParam(name = "policy") custQuotPmntPolicyVo custVo) throws FileNotFoundException {

		DspEmailDispatchProcessor emailProcessor = new DspEmailDispatchProcessor(new TermLifeProductMailTemplateLoad());
		// For syntax purpose created empty array list
		List<GeneratingReportsResponseVo> tempList = new ArrayList<GeneratingReportsResponseVo>();
		List<MotorInsuranceCustDetails> listOfAdditionalCoverage = new ArrayList<MotorInsuranceCustDetails>();
		String status = emailProcessor.emailDispatchProcess(custVo, tempList, listOfAdditionalCoverage);

		return status;

	}

}
