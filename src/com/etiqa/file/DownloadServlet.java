package com.etiqa.file;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DownloadServlet extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/*
		 * Object DOCC=request.getAttribute("DOCC"); System.out.println(DOCC);
		 */

		String filePath = "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs";

		String policyNo = request.getParameter("policyNo");
		String term = request.getParameter("term");

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		// String policyNo="DI20000041252";
		// String
		// filename="/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs/"+policyNo+"-"+term+".pdf";
		String filename = "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs/DI20000041252-e-Policy.pdf";
		// "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs/DI2000004125-e-Policy.pdf";
		// String
		// namw="sftp://wladmin@192.168.0.248:11022/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs/DI20000041252-e-Policy.pdf";
		// String filename = "D:\\WLReportpolderBT2000000623-EReceiptForm.pdf";

		response.setContentType("APPLICATION/OCTET-STREAM");
		System.out.println("filename---------" + filename);
		response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
		java.io.FileInputStream fileInputStream = new java.io.FileInputStream(filename);
		int i;
		while ((i = fileInputStream.read()) != -1) {
			out.write(i);
		}
		fileInputStream.close();
		out.close();
	}

}
