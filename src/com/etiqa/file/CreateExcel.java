package com.etiqa.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.etiqa.DAO.ReportDAO;
import com.etiqa.DAO.ReportDAOImpl;
import com.etiqa.model.report.TransactionalReport;

/**
 * Servlet implementation class CreateExcel
 */
@WebServlet("/createExcel")
public class CreateExcel extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @return
	 * @return
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());

		Random ran = new Random();
		int randomNo = ran.nextInt(6) + 5;

		DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
		Date date = new Date();
		// System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43

		ServletContext servletContext = request.getSession().getServletContext();

		HttpSession session = request.getSession(false);

		/*
		 * String ProductType = (String) session.getAttribute("ProductType"); String
		 * ProductEntity = (String) session.getAttribute("ProductEntity"); String Status
		 * = (String) session.getAttribute("Status"); String PolicyCertificateNo =
		 * (String) session.getAttribute("PolicyCertificateNo"); String NRIC = (String)
		 * session.getAttribute("NRIC"); String ReceiptNo = (String)
		 * session.getAttribute("ReceiptNo"); String DateFrom= (String)
		 * session.getAttribute("DateFrom"); String DateTo= (String)
		 * session.getAttribute("DateTo"); String PlateNo= (String)
		 * session.getAttribute("PlateNo"); String AgentCode= (String)
		 * session.getAttribute("AgentCode");
		 */

		String ProductType = String.valueOf(session.getAttribute("ProductType"));
		String ProductEntity = String.valueOf(session.getAttribute("ProductEntity"));
		String Status = String.valueOf(session.getAttribute("Status"));
		String PolicyCertificateNo = String.valueOf(session.getAttribute("PolicyCertificateNo"));
		String NRIC = String.valueOf(session.getAttribute("NRIC"));
		String ReceiptNo = String.valueOf(session.getAttribute("ReceiptNo"));
		String DateFrom = String.valueOf(session.getAttribute("DateFrom"));
		String DateTo = String.valueOf(session.getAttribute("DateTo"));
		String PlateNo = String.valueOf(session.getAttribute("PlateNo"));
		String AgentCode = String.valueOf(session.getAttribute("AgentCode"));

		int StartRow = 0; // tmp to match with SP
		int EndRow = 0; // tmp to match with SP

		if (session.getAttribute("DateFrom") != null) {

			// System.out.println(request.getContextPath());

			String file = "EtiqaTransactionReport" + dateFormat.format(date) + randomNo + ".xlsx";

			String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
			// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

			System.out.println("relativeWebPath =" + relativeWebPath);

			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Etiqa Transaction Report");

			// header fields based on product
			String[] fields = null;

			// System.out.println(session.getAttribute("ProductType"));
			if (session.getAttribute("ProductType") == "") {

				// fields = new String[] {"No", "Txn Date", "Policy/Cert No", "Plate
				// No","Name","ID No","Agent Name","Agent
				// Code","Product","Travel","Scheme","Days","Buiding","Content",
				// "RSMD","Extended Theft Cover","Payment","EBPG Ref.No","FPX Ref No", "MPAY Ref
				// No","M2U Ref No","EBPG Auth Code","FPX Auth Code","MPAY Auth Code","M2U Auth
				// Code","Tax Inv","Status","Amount"};
				fields = new String[] { "Id", "DSPQQID", "Entity", "Product", "Quotation Creation Date", "Txn Date",
						"Policy No", "Insured name", "Insured Id", "Poi", "Payment mode", "Status", "Txn Amount",
						"Gross Premium", "Discount", "GST", "Tax Invoice No", "CAPS PolicyNo", "CAPS Amount" };

			}

			else if (session.getAttribute("ProductType").equals("MI")
					|| session.getAttribute("ProductType").equals("MT")) {

				fields = new String[] { "Id", "DSPQQID", "Quotation Creation Date", "Txn Id", "Txn Start Date",
						"Txn End Date", "Entity", "Policy No", "CAPS Policy No", "Vehicle No", "Chassis No",
						"Engine No", "Previous Insurance", "Policy Status", "Txn Status", "Abandonment", "Receipt No",
						"EIP Txn Id", "Agent Code", "Agent Name", "Operator Code", "Promo Code", "NCD", "Payment Mode",
						"FPX Ref", "EBPG Ref", "M2U Ref", "No of Seat", "Cubic Capacity", "Make", "Model", "NVIC",
						"Year make", "Period of Insured", "Sum Insured", "SI Proposed", "SI Min", "SI Max", "LLOP",
						"LL2P", "Flood", "NCD Relief", "SRCC", "Windscreen SC", "Windscreen PM", "Vehicle Acc SC",
						"Vehicle Acc PM", "NGV Gas SC", "NGV Gas PM", "CART", "Payment Payable", "Gross Premium",
						"Discount", "GST", "Motor Comm", "CAPS Comm", "No of Drivers", "Driver 1", "Driver 2",
						"Driver 3", "Driver 4", "Driver 5", "Driver 6", "Driver 7", "CAPS", "Excess",
						"Loading Percentage", "Loading", "Insured name", "Id Type", "Insured ID", "Gender", "Age",
						"Email", "Contact No", "Address1", "Address2", "Address3", "Postcode", "State", "RiskAddress1",
						"RiskAddress2", "RiskAddress3", "RiskPostcode", "RiskState", "Tax Invoice No", "JPJ Msg",
						"CARSECM", "ATITFTCD", "GARAGECD"

				};
			}

			else if (session.getAttribute("ProductType").equals("building")
					|| session.getAttribute("ProductType").equals("building_content")
					|| session.getAttribute("ProductType").equals("content")) {

				fields = new String[] { "Id", "DSPQQID", "Quotation Creation Date", "Txn Id", "Txn Start Date",
						"Txn End Date", "Entity", "Name", "PolicyNo", "ProductDesc", "Building Type", "Cons Type",
						"Id Type", "Insured ID", "Email", "Address 1", "Address 2", "Address 3", "PostCode", "State",
						"RiskAddress1", "RiskAddress2", "RiskAddress3", "RiskPostcode", "RiskState", "Contact No",
						"Agent Code", "Agent Name", "Operator Code", "Promo Code", "EffDate", "HO SI", "HH SI",
						"HOHH SI", "Extended Theft", "RSMD", "FPX Ref", "EBPG Ref", "M2U Ref", "Payment Mode",
						"Payment Payable", "Gross Premium", "Discount", "GST", "Comm", "Tax Invoice No", "Txn Status",
						"Abandonment", "PDPA", "UW1", "UW2", "UW3", "UW4", "UW5", "Add Item 1", "Add Item 2",
						"Add Item 3", "Add Item 4", "Add Item 5", "Add Item 6", "Add Item 7", "Add Item 8",
						"Add Item 9", "Add Item 10" };

			}

			else if (session.getAttribute("ProductType").equals("WTC")
					|| session.getAttribute("ProductType").equals("TPT")) {
				fields = new String[] { "Id", "DSPQQID", "Quotation Creation Date", "Txn Id", "Txn Start Date",
						"Txn End Date", "Entity", "Name", "PolicyNo", "ProductDesc", "Plan", "Scheme", "Id Type",
						"Insured ID", "Gender", "Email", "Address 1", "Address 2", "Address 3", "State", "PostCode",
						"Contact No", "Agent Code", "Agent Name", "Operator Code", "Promo Code", "EffDate",
						"Travel Type", "Area Code", "FPX Ref", "EBPG Ref", "M2U Ref", "Payment Mode", "Payment Payable",
						"Gross Premium", "Discount", "GST", "Comm", "Tax Invoice No", "Txn Status", "Abandonment",
						"PDPA", "Spouse", "Spouse Email", "Spouse Id Type", "Spouse Id No", "Spouse DOB",
						"Spouse Gender", "Child 1", "Child 1 Id Type", "Child 1 Id No", "Child 1 DOB", "Child 1 Gender",
						"Child 2", "Child 2 Id Type", "Child 2 Id No", "Child 2 DOB", "Child 2 Gender", "Child 3",
						"Child 3 Id Type", "Child 3 Id No", "Child 3 DOB", "Child 3 Gender", "Child 4",
						"Child 4 Id Type", "Child 4 Id No", "Child 4 DOB", "Child 4 Gender", "Child 5",
						"Child 5 Id Type", "Child 5 Id No", "Child 5 DOB", "Child 5 Gender", "Child 6",
						"Child 6 Id Type", "Child 6 Id No", "Child 6 DOB", "Child 6 Gender", "Child 7",
						"Child 7 Id Type", "Child 7 Id No", "Child 7 DOB", "Child 7 Gender" };

			}

			else if (session.getAttribute("ProductType").equals("TL")) {

				fields = new String[] { "Id", "DSPQQID", "Quotation Creation Date", "Txn Date", "Transaction Start",
						"Transaction End", "Transaction Time Taken", "Product Name", "Policy/Cert No",
						"Coverage Amount", "Coverage Term", "Status", "Abandonment", "Rejection Reason", "Name",
						"NRIC No", "Gender", "Email", "Address", "Postcode", "State", "Mobile No",
						"Annual Premium/Contribution", "Payment Type", "Payment Method", "Payment Mode", "Amount Paid",
						"MPAY Ref No", "MPAY Auth Code", "Promo Code", "Agent Name", "Agent Code", "Operator Code" };

			}
			Row row1 = sheet.createRow(0);

			for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
				Cell cell = row1.createCell(cellIndex);
				cell.setCellValue(fields[cellIndex]);
			}

			String[] fields2 = null;

			if (session.getAttribute("ProductType") == "") {

				ReportDAO dao = new ReportDAOImpl();

				List<TransactionalReport> AllRecordDetail = dao.findByProductType(ProductType, ProductEntity, Status,
						PolicyCertificateNo, DateFrom, DateTo, NRIC, ReceiptNo, StartRow, EndRow, PlateNo, AgentCode);

				int i = 1;

				for (TransactionalReport txn : AllRecordDetail) {
					String txnStatus = "";
					String poi = "";
					String productEntity = "";
					String DSPQQID = "";
					String QQID = "";
					String quotationCreationDate = "";

					DSPQQID = txn.getDSPQQID();
					QQID = txn.getQQID();

					if (txn.getStatus().equals("S")) {

						txnStatus = "Successful";

					} else if (txn.getStatus().equals("F")) {

						txnStatus = "Fail";

					}

					else if (txn.getStatus().equals("AP")) {

						txnStatus = "Attempt Payment";

					}

					else if (txn.getStatus().equals("O")) {

						txnStatus = "Incomplete";

					}

					else if (txn.getStatus().equals("C")) {

						txnStatus = "Cancelled";

					}

					else if (txn.getStatus().equals("R")) {

						txnStatus = "Rejection";

					}

					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

					SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					Date quotationCreationDateTmp = null;

					try {

						quotationCreationDateTmp = formatter.parse(txn.getQuotationCreateDateTime());
						quotationCreationDate = ft.format(quotationCreationDateTmp);
					} catch (ParseException e) {
						e.printStackTrace();
					}

					if (txn.getProductCode().equals("TL")) {

						poi = txn.getTerm() + " years";
						productEntity = "EIB";

					} else {

						poi = txn.getCoverageStartDate() + " - " + txn.getCoverageEndDate();
					}

					if (txn.getInvoiceNo().indexOf("EIB") != -1) {
						productEntity = "EIB";

					} else if (txn.getInvoiceNo().indexOf("ETB") != -1) {

						productEntity = "ETB";
					}

					fields2 = new String[] { Integer.toString(i), txn.getDSPQQID(), productEntity, txn.getProductCode(),
							quotationCreationDate, txn.getTransactionDate(), txn.getPolicyNo(), txn.getCustomerName(),
							txn.getCustomerNRIC(), poi, txn.getPaymentChannel(), txnStatus, txn.getPremiumAmount(),
							txn.getGrossPremiumFinal(), txn.getDiscount(), txn.getGST(), txn.getInvoiceNo(),
							txn.getCAPSPolicyNo(), txn.getCapsAmount() };

					Row row2 = sheet.createRow(i);

					for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
						Cell cell = row2.createCell(cellIndex);
						cell.setCellValue(fields2[cellIndex]);
					}

					i++;
				}
			} else if (session.getAttribute("ProductType").equals("MI")
					|| session.getAttribute("ProductType").equals("MT")) {

				ReportDAO dao = new ReportDAOImpl();

				// List<TransactionalReport> MIRecordDetail = dao.getMIRecordDetail(ProductType,
				// ProductEntity, Status, PolicyCertificateNo, DateFrom, DateTo, NRIC,
				// ReceiptNo, PlateNo,AgentCode);
				List<TransactionalReport> MIRecordDetail = dao.getMotorRecord(ProductEntity, Status,
						PolicyCertificateNo, DateFrom, DateTo, NRIC, ReceiptNo, PlateNo, AgentCode);

				// MotorInsuranceCustDetails MIRecordDetail=dao.getMIRecordDetail(QQID);

				int i = 1;

				for (TransactionalReport motorDetail : MIRecordDetail) {

					String Abandonment = "";
					String EIPTxnId = "";
					String RiskAddress3 = "";
					String txnStatus = "";
					String quotationCreationDate = "";
					String transactionStartDateTime = "";
					String transactionEndDateTime = "";

					if (motorDetail.getStatus().equals("S")) {

						txnStatus = "Successful";

					} else if (motorDetail.getStatus().equals("F")) {

						txnStatus = "Fail";

					}

					else if (motorDetail.getStatus().equals("AP")) {

						txnStatus = "Attempt Payment";

					}

					else if (motorDetail.getStatus().equals("O")) {

						Abandonment = motorDetail.getLastPage();
						txnStatus = "Incomplete";

					}

					else if (motorDetail.getStatus().equals("C")) {

						txnStatus = "Cancelled";

					}

					else if (motorDetail.getStatus().equals("R")) {

						txnStatus = "Rejection : " + motorDetail.getReason();

					}

					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

					SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					Date quotationCreationDateTmp = null;

					try {

						quotationCreationDateTmp = formatter.parse(motorDetail.getQuotationCreateDateTime());
						quotationCreationDate = ft.format(quotationCreationDateTmp);
					} catch (ParseException e) {
						e.printStackTrace();
					}

					if (motorDetail.getStatus().equals("S") || motorDetail.getStatus().equals("F")
							|| motorDetail.getStatus().equals("AP")) {

						transactionStartDateTime = motorDetail.getTransactionDate();
					}

					fields2 = new String[] { Integer.toString(i), motorDetail.getDSPQQID(), quotationCreationDate,
							motorDetail.getTransactionID(), transactionStartDateTime, transactionEndDateTime,
							motorDetail.getProductEntity(), motorDetail.getPolicyNo(), motorDetail.getCAPSPolicyNo(),
							motorDetail.getVehicleNo(), motorDetail.getChassisNo(), motorDetail.getEngineNo(),
							motorDetail.getPreviousInsurance(), motorDetail.getPolicyStatus(), txnStatus, Abandonment,
							motorDetail.getInvoiceNo(), EIPTxnId, motorDetail.getAgentCode(),
							motorDetail.getAgentName(), motorDetail.getOperatorCode(), motorDetail.getDiscountCode(),
							motorDetail.getNonClaimDiscount(), motorDetail.getPaymentChannel(),
							motorDetail.getFPX_RefNo(), motorDetail.getEBPG_RefNo(), motorDetail.getM2U_RefNo(),
							motorDetail.getVehicleSeat(), motorDetail.getVehicleCC(), motorDetail.getVehicleMake(),
							motorDetail.getVehicleModel(), motorDetail.getNVIC(), motorDetail.getYearMake(),
							motorDetail.getPeriodCoverage(), motorDetail.getSumInsured(), motorDetail.getMarketValue(),
							motorDetail.getSIMin(), motorDetail.getSIMax(), motorDetail.getLLOP(),
							motorDetail.getLL2P(), motorDetail.getFloodCover(), motorDetail.getNCDRelief(),
							motorDetail.getSRCC(), motorDetail.getWindscreenSC(), motorDetail.getWindscreenP(),
							motorDetail.getVehicleAccSC(), motorDetail.getVehicleAccP(), motorDetail.getNGVGasSC(),
							motorDetail.getNGVGasP(), motorDetail.getCartPremiumDesc() + motorDetail.getCartPremium(),
							motorDetail.getTotalPremiumPayable(), motorDetail.getGrossPremiumFinal(),
							motorDetail.getDiscount(), motorDetail.getGST(), motorDetail.getAgentCommissionAmount(),
							motorDetail.getCAPSCommissionAmount(), motorDetail.getDriverNo(),
							motorDetail.getFirstDriverInfo(), motorDetail.getSecondDriverInfo(),
							motorDetail.getThirdDriverInfo(), motorDetail.getForthDriverInfo(),
							motorDetail.getFifthDriverInfo(), motorDetail.getSixthDriverInfo(),
							motorDetail.getSeventhDriverInfo(), motorDetail.getPAPremiumPayable(),
							motorDetail.getExcess(), motorDetail.getLoadingPercentage(), motorDetail.getLoading(),
							motorDetail.getFirstDriver(),
							/* motorDetail.getSecondDriver(), */
							motorDetail.getIDType(), motorDetail.getCustomerNRIC(), motorDetail.getCustomerGender(),
							motorDetail.getCustomerAge(), motorDetail.getCustomerEmail(),
							motorDetail.getCustomerMobileNo(), motorDetail.getCustomerAddress1(),
							motorDetail.getCustomerAddress2(), motorDetail.getCustomerAddress3(),
							motorDetail.getCustomerPostcode(), motorDetail.getCustomerState(),
							motorDetail.getParkingAddress1(), motorDetail.getParkingAddress2(), RiskAddress3,
							motorDetail.getParkingPostcode(), motorDetail.getParkingState(), motorDetail.getInvoiceNo(),
							motorDetail.getJPJStatus(), motorDetail.getCARSECM(), motorDetail.getATITFTCD(),
							motorDetail.getGARAGECD() };

					Row row2 = sheet.createRow(i);

					for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
						Cell cell = row2.createCell(cellIndex);
						cell.setCellValue(fields2[cellIndex]);
					}

					i++;

				}

			}

			else if (session.getAttribute("ProductType").equals("building")
					|| session.getAttribute("ProductType").equals("building_content")
					|| session.getAttribute("ProductType").equals("content")) {

				// System.out.println("session.getAttribute('ProductType')"+session.getAttribute("ProductType"));

				ReportDAO dao = new ReportDAOImpl();

				List<TransactionalReport> HOHHRecordDetail = dao.getHOHHRecord(ProductType, ProductEntity, Status,
						PolicyCertificateNo, DateFrom, DateTo, NRIC, ReceiptNo, AgentCode);

				int i = 1;

				for (TransactionalReport HOHHDetail : HOHHRecordDetail) {

					String Abandonment = "";
					String txnStatus = "";
					String quotationCreationDate = "";
					String transactionStartDateTime = "";
					String transactionEndDateTime = "";

					if (HOHHDetail.getStatus().equals("S")) {

						txnStatus = "Successful";

					} else if (HOHHDetail.getStatus().equals("F")) {

						txnStatus = "Fail";

					}

					else if (HOHHDetail.getStatus().equals("AP")) {

						txnStatus = "Attempt Payment";

					}

					else if (HOHHDetail.getStatus().equals("O")) {
						Abandonment = HOHHDetail.getLastPage();

						txnStatus = "Incomplete";

					}

					else if (HOHHDetail.getStatus().equals("C")) {

						txnStatus = "Cancelled";

					}

					else if (HOHHDetail.getStatus().equals("R")) {

						txnStatus = "Rejection";

					}

					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

					SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					Date quotationCreationDateTmp = null;

					try {

						quotationCreationDateTmp = formatter.parse(HOHHDetail.getQuotationCreateDateTime());
						quotationCreationDate = ft.format(quotationCreationDateTmp);
					} catch (ParseException e) {
						e.printStackTrace();
					}

					if (HOHHDetail.getStatus().equals("S") || HOHHDetail.getStatus().equals("F")
							|| HOHHDetail.getStatus().equals("AP")) {

						transactionStartDateTime = HOHHDetail.getTransactionDate();
					}

					fields2 = new String[] { Integer.toString(i), HOHHDetail.getDSPQQID(), quotationCreationDate,
							HOHHDetail.getTransactionID(), transactionStartDateTime, transactionEndDateTime,
							HOHHDetail.getProductEntity(), HOHHDetail.getCustomerName(), HOHHDetail.getPolicyNo(),
							HOHHDetail.getProductType(), HOHHDetail.getHomeType(),
							HOHHDetail.getBuildConstructionType(), HOHHDetail.getIDType(), HOHHDetail.getCustomerNRIC(),
							HOHHDetail.getCustomerEmail(), HOHHDetail.getCustomerAddress1(),
							HOHHDetail.getCustomerAddress2(), HOHHDetail.getCustomerAddress3(),
							HOHHDetail.getCustomerPostcode(), HOHHDetail.getCustomerState(),
							HOHHDetail.getPropertyInsuredAddress1(), HOHHDetail.getPropertyInsuredAddress2(),
							HOHHDetail.getPropertyInsuredAddress3(), HOHHDetail.getPropertyInsuredPostcode(),
							HOHHDetail.getPropertyInsuredState(), HOHHDetail.getCustomerMobileNo(),
							HOHHDetail.getAgentCode(), HOHHDetail.getAgentName(), HOHHDetail.getOperatorCode(),
							HOHHDetail.getDiscountCode(),
							HOHHDetail.getCoverageStartDate() + "-" + HOHHDetail.getCoverageEndDate(),
							HOHHDetail.getHOSI(), HOHHDetail.getHHSI(), HOHHDetail.getHOHHSI(),
							HOHHDetail.getAddBenExtendedTheftAmt(), HOHHDetail.getAddBenRiotStrikeAmt(),
							HOHHDetail.getFPX_RefNo(), HOHHDetail.getEBPG_RefNo(), HOHHDetail.getM2U_RefNo(),
							HOHHDetail.getPaymentChannel(), HOHHDetail.getTotalPremiumPayable(),
							HOHHDetail.getGrossPremiumFinal(), HOHHDetail.getDiscount(), HOHHDetail.getGST(),
							HOHHDetail.getCommission(), HOHHDetail.getInvoiceNo(), txnStatus, Abandonment,
							HOHHDetail.getPDPA(), HOHHDetail.getUW1(), HOHHDetail.getUW2(), HOHHDetail.getUW3(),
							HOHHDetail.getUW4(), HOHHDetail.getUW5(),
							HOHHDetail.getBenefitCode1() + "|" + HOHHDetail.getBenefitText1(),
							HOHHDetail.getBenefitCode2() + "|" + HOHHDetail.getBenefitText2(),
							HOHHDetail.getBenefitCode3() + "|" + HOHHDetail.getBenefitText3(),
							HOHHDetail.getBenefitCode4() + "|" + HOHHDetail.getBenefitText4(),
							HOHHDetail.getBenefitCode5() + "|" + HOHHDetail.getBenefitText5(),
							HOHHDetail.getBenefitCode6() + "|" + HOHHDetail.getBenefitText6(),
							HOHHDetail.getBenefitCode7() + "|" + HOHHDetail.getBenefitText7(),
							HOHHDetail.getBenefitCode8() + "|" + HOHHDetail.getBenefitText8(),
							HOHHDetail.getBenefitCode9() + "|" + HOHHDetail.getBenefitText9(),
							HOHHDetail.getBenefitCode10() + "|" + HOHHDetail.getBenefitText10()

					};

					Row row2 = sheet.createRow(i);

					for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
						Cell cell = row2.createCell(cellIndex);
						cell.setCellValue(fields2[cellIndex]);
					}

					i++;

				}

			}

			else if (session.getAttribute("ProductType").equals("WTC")
					|| session.getAttribute("ProductType").equals("TPT")) {

				ReportDAO dao = new ReportDAOImpl();

				List<TransactionalReport> WTCRecordDetail = dao.getWTCRecord(ProductEntity, Status, PolicyCertificateNo,
						DateFrom, DateTo, NRIC, ReceiptNo, AgentCode);

				int i = 1;

				for (TransactionalReport WTCDetail : WTCRecordDetail) {

					String Abandonment = "";
					String txnStatus = "";
					String quotationCreationDate = "";
					String transactionStartDateTime = "";
					String transactionEndDateTime = "";

					if (WTCDetail.getStatus().equals("S")) {

						txnStatus = "Successful";

					} else if (WTCDetail.getStatus().equals("F")) {

						txnStatus = "Fail";

					}

					else if (WTCDetail.getStatus().equals("AP")) {

						txnStatus = "Attempt Payment";

					}

					else if (WTCDetail.getStatus().equals("O")) {

						Abandonment = WTCDetail.getLastPage();
						txnStatus = "Incomplete";

					}

					else if (WTCDetail.getStatus().equals("C")) {

						txnStatus = "Cancelled";

					}

					else if (WTCDetail.getStatus().equals("R")) {

						txnStatus = "Rejection";

					}

					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

					SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					Date quotationCreationDateTmp = null;

					try {

						quotationCreationDateTmp = formatter.parse(WTCDetail.getQuotationCreateDateTime());
						quotationCreationDate = ft.format(quotationCreationDateTmp);
					} catch (ParseException e) {
						e.printStackTrace();
					}

					if (WTCDetail.getStatus().equals("S") || WTCDetail.getStatus().equals("F")
							|| WTCDetail.getStatus().equals("AP")) {

						transactionStartDateTime = WTCDetail.getTransactionDate();
					}

					fields2 = new String[] { Integer.toString(i), WTCDetail.getDSPQQID(), quotationCreationDate,
							WTCDetail.getTransactionID(), transactionStartDateTime, transactionEndDateTime,
							WTCDetail.getProductEntity(), WTCDetail.getCustomerName(), WTCDetail.getPolicyNo(),
							WTCDetail.getProductCode(), WTCDetail.getOfferedPlanName(), WTCDetail.getTravellingWith(),
							WTCDetail.getIDType(), WTCDetail.getCustomerNRIC(), WTCDetail.getCustomerGender(),
							WTCDetail.getCustomerEmail(), WTCDetail.getCustomerAddress1(),
							WTCDetail.getCustomerAddress2(), WTCDetail.getCustomerAddress3(),
							WTCDetail.getCustomerState(), WTCDetail.getCustomerPostcode(),
							WTCDetail.getCustomerMobileNo(), WTCDetail.getAgentCode(), WTCDetail.getAgentName(),
							WTCDetail.getOperatorCode(), WTCDetail.getDiscountCode(),
							WTCDetail.getCoverageStartDate() + "-" + WTCDetail.getCoverageEndDate(),
							WTCDetail.getTravelAreaType(), WTCDetail.getAreaCode(), WTCDetail.getFPX_RefNo(),
							WTCDetail.getEBPG_RefNo(), WTCDetail.getM2U_RefNo(), WTCDetail.getPaymentChannel(),
							WTCDetail.getTotalPremiumPayable(), WTCDetail.getGrossPremiumFinal(),
							WTCDetail.getDiscount(), WTCDetail.getGST(), WTCDetail.getCommission(),
							WTCDetail.getInvoiceNo(), txnStatus, Abandonment, WTCDetail.getPDPA(),
							WTCDetail.getSpouseName(), WTCDetail.getSpouseEmail(), WTCDetail.getSpouseIDType(),
							WTCDetail.getSpouseIDNumber(), WTCDetail.getSpouseDOB(), WTCDetail.getSpouseGender(),
							WTCDetail.getChildName_1(), WTCDetail.getChildIDType_1(), WTCDetail.getChildIDNumber_1(),
							WTCDetail.getChildDOB_1(), WTCDetail.getChildGender_1(), WTCDetail.getChildName_2(),
							WTCDetail.getChildIDType_2(), WTCDetail.getChildIDNumber_2(), WTCDetail.getChildDOB_2(),
							WTCDetail.getChildGender_2(), WTCDetail.getChildName_3(), WTCDetail.getChildIDType_3(),
							WTCDetail.getChildIDNumber_3(), WTCDetail.getChildDOB_3(), WTCDetail.getChildGender_3(),
							WTCDetail.getChildName_4(), WTCDetail.getChildIDType_4(), WTCDetail.getChildIDNumber_4(),
							WTCDetail.getChildDOB_4(), WTCDetail.getChildGender_4(), WTCDetail.getChildName_5(),
							WTCDetail.getChildIDType_5(), WTCDetail.getChildIDNumber_5(), WTCDetail.getChildDOB_5(),
							WTCDetail.getChildGender_5(), WTCDetail.getChildName_6(), WTCDetail.getChildIDType_6(),
							WTCDetail.getChildIDNumber_6(), WTCDetail.getChildDOB_6(), WTCDetail.getChildGender_6(),
							WTCDetail.getChildName_7(), WTCDetail.getChildIDType_7(), WTCDetail.getChildIDNumber_7(),
							WTCDetail.getChildDOB_7(), WTCDetail.getChildGender_7() };

					Row row2 = sheet.createRow(i);

					for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
						Cell cell = row2.createCell(cellIndex);
						cell.setCellValue(fields2[cellIndex]);
					}

					i++;

				}

			}

			else if (session.getAttribute("ProductType").equals("TL")) {

				System.out.println("TL enter " + session.getAttribute("ProductType"));
				ReportDAO dao = new ReportDAOImpl();
				List<TransactionalReport> TLRecordDetail = dao.getTLRecord(ProductEntity, Status, PolicyCertificateNo,
						DateFrom, DateTo, NRIC, ReceiptNo, AgentCode);

				// List<TransactionalReport> TLRecordDetail = dao.getTLRecordDetail(ProductType,
				// ProductEntity, Status, PolicyCertificateNo, DateFrom, DateTo, NRIC,
				// ReceiptNo, PlateNo,AgentCode);

				int i = 1;

				for (TransactionalReport TLDetail : TLRecordDetail) {

					String Abandonment = "";
					String txnStatus = "";
					String txnTimeTaken = "";
					String transactionDate = "";
					String transactionStartDateTime = "";
					String transactionEndDateTime = "";

					if (TLDetail.getStatus().equals("S")) {

						txnStatus = "Successful";

					} else if (TLDetail.getStatus().equals("F")) {

						txnStatus = "Fail";

					}

					else if (TLDetail.getStatus().equals("AP")) {

						txnStatus = "Attempt Payment";

					}

					else if (TLDetail.getStatus().equals("O")) {

						txnStatus = "Incomplete";
						Abandonment = TLDetail.getLastPage();

					}

					else if (TLDetail.getStatus().equals("C")) {

						txnStatus = "Cancelled";

					}

					else if (TLDetail.getStatus().equals("R")) {

						txnStatus = "Rejection";

					}

					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					Date quotationCreationDateTmp = null;
					String quotationCreationDate = null;
					Date transactionDateTmp = null;

					try {

						quotationCreationDateTmp = formatter.parse(TLDetail.getQuotationCreateDateTime());
						quotationCreationDate = ft.format(quotationCreationDateTmp);
					} catch (ParseException e) {
						e.printStackTrace();
					}

					if (TLDetail.getStatus().equals("S") || TLDetail.getStatus().equals("F")
							|| TLDetail.getStatus().equals("AP")) {

						try {

							transactionDateTmp = ft.parse(TLDetail.getTransactionDate());
							transactionDate = ft.format(transactionDateTmp);
							transactionStartDateTime = TLDetail.getTransactionDate();
							// if(TLDetail.getStatus().equals("S") ||TLDetail.getStatus().equals("F")){

							// Date transactionEndDateTimeTmp=
							// formatter.parse(TLDetail.getQuotationUpdateDateTime());
							// transactionEndDateTime = formatter2.format(transactionEndDateTimeTmp);
							// }

						} catch (ParseException e) {
							e.printStackTrace();
						}

					}

					fields2 = new String[] { Integer.toString(i), TLDetail.getDSPQQID(), quotationCreationDate,
							transactionDate, transactionStartDateTime, transactionEndDateTime, txnTimeTaken,
							TLDetail.getProductType(), TLDetail.getPolicyNo(), TLDetail.getCoverage(),
							TLDetail.getTerm() + " Years", txnStatus, Abandonment, TLDetail.getReason(),
							TLDetail.getCustomerName(), TLDetail.getCustomerNRIC(), TLDetail.getCustomerGender(),
							TLDetail.getCustomerEmail(),
							TLDetail.getCustomerAddress1() + "," + TLDetail.getCustomerAddress2() + ","
									+ TLDetail.getCustomerAddress3(),
							TLDetail.getCustomerPostcode(), TLDetail.getCustomerState(), TLDetail.getCustomerMobileNo(),
							TLDetail.getAnnualPremium(), TLDetail.getPaymentMethod(), TLDetail.getPaymentChannel(),
							TLDetail.getMode(), TLDetail.getPremiumAmount(), TLDetail.getMPAY_RefNo(),
							TLDetail.getMPAY_AuthCode(), TLDetail.getDiscountCode(), TLDetail.getAgentName(),
							TLDetail.getAgentCode(), TLDetail.getOperatorCode() };

					Row row2 = sheet.createRow(i);

					for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
						Cell cell = row2.createCell(cellIndex);
						cell.setCellValue(fields2[cellIndex]);
					}

					i++;

				}

			}

			// System.out.println("txnStatus "+txn.getStatus());

			try {
				FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
				workbook.write(outputStream);
				workbook.close();

				File fileToDownload = new File(relativeWebPath);

				FileInputStream inputStream = new FileInputStream(fileToDownload);
				response.setContentType(servletContext.getMimeType(relativeWebPath));
				response.setContentLength((int) fileToDownload.length());

				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
				response.setHeader(headerKey, headerValue);

				// get output stream of the response
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				// write bytes read from the input stream into the output stream
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}

				outStream.flush();
				outStream.close();
				inputStream.close();

				if (fileToDownload.exists()) {

					boolean success = new File(relativeWebPath).delete();
					if (success) {
						System.out.println("The file " + file + " has been successfully deleted");
					}

				}

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			// System.out.println("Done Download");
		} else {

			System.out.println("session data null");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
