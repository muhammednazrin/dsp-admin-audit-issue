package com.etiqa.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FPXCertRenewal
 */
@WebServlet("/downloadFile")
public class Download extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */

	// private String filePath =
	// "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs";
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String filePath = "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs";

		String policyNo = request.getParameter("policyNo");
		String term = request.getParameter("term");

		// e-Policy.pdf

		String fileName = policyNo + "-" + term + ".pdf";
		System.out.println("PDF PATH : " + filePath);
		System.out.println("PDF FILE : " + fileName);
		if (fileName == null || fileName.equals("")) {
			throw new ServletException("File Name can't be null or empty");
		}
		File file = new File(filePath + "/" + fileName);
		if (!file.exists()) {

			String filePath_tmp = "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_backup/admin";
			File file_tmp = new File(filePath_tmp + "/" + fileName);

			if (!file_tmp.exists()) {

				String filePath_admin = "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_backup";
				String fileAdminStr = filePath_admin + "/" + fileName;
				File file_admin = new File(fileAdminStr);

				// quickfix: check if this is probably an oto360 file (inferred if
				if (!file_admin.exists() && (fileAdminStr.contains("CAPS") || fileAdminStr.contains("DPPA"))) {
					String f = fileAdminStr;
					f = f.replaceAll("CAPS", "OTO360");
					f = f.replaceAll("DPPA", "OTO360");
					f = f.replaceAll("EIW_backup", "EIW_Docs");
					System.out.println(f);
					file_admin = new File(f);
				}

				if (!file_admin.exists()) {
					throw new ServletException("File doesn't exists on server.");

				} else {

					System.out.println("File location on server::" + file_admin.getAbsolutePath());
					ServletContext ctx = getServletContext();
					InputStream fis = new FileInputStream(file_admin);
					String mimeType = ctx.getMimeType(file_admin.getAbsolutePath());
					response.setContentType(mimeType != null ? mimeType : "application/octet-stream");
					response.setContentLength((int) file_admin.length());
					response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

					ServletOutputStream os = response.getOutputStream();
					byte[] bufferData = new byte[1024];
					int read = 0;
					while ((read = fis.read(bufferData)) != -1) {
						os.write(bufferData, 0, read);
					}
					os.flush();
					os.close();
					fis.close();
					System.out.println("File downloaded at client successfully");

				}

			} else {

				System.out.println("File location on server::" + file_tmp.getAbsolutePath());
				ServletContext ctx = getServletContext();
				InputStream fis = new FileInputStream(file_tmp);
				String mimeType = ctx.getMimeType(file_tmp.getAbsolutePath());
				response.setContentType(mimeType != null ? mimeType : "application/octet-stream");
				response.setContentLength((int) file_tmp.length());
				response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

				ServletOutputStream os = response.getOutputStream();
				byte[] bufferData = new byte[1024];
				int read = 0;
				while ((read = fis.read(bufferData)) != -1) {
					os.write(bufferData, 0, read);
				}
				os.flush();
				os.close();
				fis.close();
				System.out.println("File downloaded at client successfully");

			}

		}

		else {

			System.out.println("File location on server::" + file.getAbsolutePath());
			ServletContext ctx = getServletContext();
			InputStream fis = new FileInputStream(file);
			String mimeType = ctx.getMimeType(file.getAbsolutePath());
			response.setContentType(mimeType != null ? mimeType : "application/octet-stream");
			response.setContentLength((int) file.length());
			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

			ServletOutputStream os = response.getOutputStream();
			byte[] bufferData = new byte[1024];
			int read = 0;
			while ((read = fis.read(bufferData)) != -1) {
				os.write(bufferData, 0, read);
			}
			os.flush();
			os.close();
			fis.close();
			System.out.println("File downloaded at client successfully");

		}

	}

}
