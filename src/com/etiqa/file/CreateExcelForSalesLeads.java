package com.etiqa.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.etiqa.DAO.ReportDAO;
import com.etiqa.DAO.ReportDAOImpl;
import com.etiqa.model.report.SalesLeads;

@WebServlet("/createExcelForSalesLeads")
public class CreateExcelForSalesLeads extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * @return
	 * @return
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());

		Random ran = new Random();
		int randomNo = ran.nextInt(6) + 5;

		DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
		Date date = new Date();
		// System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43

		ServletContext servletContext = request.getSession().getServletContext();

		HttpSession session = request.getSession(false);

		String agentName = (String) session.getAttribute("agentName");
		String agentCode = (String) session.getAttribute("agentCode");
		String fromDate = (String) session.getAttribute("fromDate");
		String toDate = (String) session.getAttribute("toDate");

		int StartRow = 0; // tmp to match with SP
		int EndRow = 0; // tmp to match with SP

		// System.out.println(request.getContextPath());

		String file = "SalesLeadsReport" + dateFormat.format(date) + randomNo + ".xlsx";

		String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
		// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

		System.out.println("relativeWebPath =" + relativeWebPath);

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Etiqa Transaction Report");

		// header fields based on product
		String[] fields = null;

		// System.out.println(session.getAttribute("ProductType"));

		// fields = new String[] {"No", "Txn Date", "Policy/Cert No", "Plate
		// No","Name","ID No","Agent Name","Agent
		// Code","Product","Travel","Scheme","Days","Buiding","Content",
		// "RSMD","Extended Theft Cover","Payment","EBPG Ref.No","FPX Ref No", "MPAY Ref
		// No","M2U Ref No","EBPG Auth Code","FPX Auth Code","MPAY Auth Code","M2U Auth
		// Code","Tax Inv","Status","Amount"};
		fields = new String[] { "No", "Date", "Name", "Vehicle Reg NO", "Email", "Mobile No", "Expiry Month",
				"Agent Code", "Agent Name" };

		Row row1 = sheet.createRow(0);

		for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
			Cell cell = row1.createCell(cellIndex);
			cell.setCellValue(fields[cellIndex]);
		}

		String[] fields2 = null;

		ReportDAO dao = new ReportDAOImpl();

		List<SalesLeads> AllRecordDetail = dao.findBySalesLeads(agentName, agentCode, fromDate, toDate, StartRow,
				EndRow);

		int i = 1;

		for (SalesLeads txn : AllRecordDetail) {

			fields2 = new String[] { Integer.toString(i), txn.getCREATE_DATE(), txn.getCUSTOMER_NAME(),
					txn.getVEHREGNO(), txn.getEMAIL(), txn.getPHONENO(), txn.getRDTAXEXPIRY(), txn.getAGENT_CODE(),
					txn.getAGENT_NAME() };

			Row row2 = sheet.createRow(i);

			for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
				Cell cell = row2.createCell(cellIndex);
				cell.setCellValue(fields2[cellIndex]);
			}

			i++;
		}

		// System.out.println("txnStatus "+txn.getStatus());

		try {
			FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
			workbook.write(outputStream);
			workbook.close();

			File fileToDownload = new File(relativeWebPath);

			FileInputStream inputStream = new FileInputStream(fileToDownload);
			response.setContentType(servletContext.getMimeType(relativeWebPath));
			response.setContentLength((int) fileToDownload.length());

			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
			response.setHeader(headerKey, headerValue);

			// get output stream of the response
			OutputStream outStream = response.getOutputStream();

			byte[] buffer = new byte[4096];
			int bytesRead = -1;

			// write bytes read from the input stream into the output stream
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}

			outStream.flush();
			outStream.close();
			inputStream.close();

			if (fileToDownload.exists()) {

				boolean success = new File(relativeWebPath).delete();
				if (success) {
					System.out.println("The file " + file + " has been successfully deleted");
				}

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// System.out.println("Done Download");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
