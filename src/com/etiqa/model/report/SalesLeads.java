package com.etiqa.model.report;

public class SalesLeads {

	private int ID;
	private String CUSTOMER_NAME;
	private String EMAIL;
	private String VEHREGNO;
	private String RDTAXEXPIRY;
	private String VERIFICATIONCODE;
	private String TCFLAG;
	private String CREATE_DATE;
	private String PHONENO;
	private String AGENT_CODE;
	private String AGENT_NAME;

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getCUSTOMER_NAME() {
		return CUSTOMER_NAME;
	}

	public void setCUSTOMER_NAME(String cUSTOMER_NAME) {
		CUSTOMER_NAME = cUSTOMER_NAME;
	}

	public String getEMAIL() {
		return EMAIL;
	}

	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}

	public String getVEHREGNO() {
		return VEHREGNO;
	}

	public void setVEHREGNO(String vEHREGNO) {
		VEHREGNO = vEHREGNO;
	}

	public String getRDTAXEXPIRY() {
		return RDTAXEXPIRY;
	}

	public void setRDTAXEXPIRY(String rDTAXEXPIRY) {
		RDTAXEXPIRY = rDTAXEXPIRY;
	}

	public String getVERIFICATIONCODE() {
		return VERIFICATIONCODE;
	}

	public void setVERIFICATIONCODE(String vERIFICATIONCODE) {
		VERIFICATIONCODE = vERIFICATIONCODE;
	}

	public String getTCFLAG() {
		return TCFLAG;
	}

	public void setTCFLAG(String tCFLAG) {
		TCFLAG = tCFLAG;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getPHONENO() {
		return PHONENO;
	}

	public void setPHONENO(String pHONENO) {
		PHONENO = pHONENO;
	}

	public String getAGENT_CODE() {
		return AGENT_CODE;
	}

	public void setAGENT_CODE(String aGENT_CODE) {
		AGENT_CODE = aGENT_CODE;
	}

	public String getAGENT_NAME() {
		return AGENT_NAME;
	}

	public void setAGENT_NAME(String aGENT_NAME) {
		AGENT_NAME = aGENT_NAME;
	}

}
