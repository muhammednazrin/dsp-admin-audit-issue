package com.etiqa.model.report;

public class AgentPerformanceReport {

	private int id;
	private String salesEntity;
	private String marketerName;
	private String marketerCode;

	private String commission;
	private String salesTarget;
	private String agentName;
	private String agentCode;

	private String DateFrom;
	private String DateTo;
	private String PolicyNo;
	private String CustomerName;
	private String ProductEntity;
	private String ProductType;
	private String Status;
	private String PremiumAmount;
	private String TransactionDate;
	private String TransactionID;
	private String CustomerNRIC;
	private String PaymentMethod;
	private String PaymentRefNo;
	private String PaymentStatus;
	private String Incentive;

	private String BankFee;
	private String TotalNet;
	private String CustomerEmail;
	private String CustomerAddress1;
	private String CustomerAddress2;
	private String CustomerAddress3;
	private String CustomerCity;
	private String CustomerPostcode;
	private String CustomerState;
	private String CustomerCountry;
	private String CustomerMobileNo;
	private String CustomerID;
	private String DSPQQID;
	private String PaymentChannel;
	private String InvoiceNo;
	private String ProductCode;
	private String Term;
	private String Coverage;
	private String Mode;
	private String PaymentAuthCode;
	private String TotalAmount;
	private String PremiumMode;
	private String MPAY_RefNo;
	private String MPAY_AuthCode;
	private String EBPG_RefNo;
	private String EBPG_AuthCode;
	private String M2U_RefNo;
	private String M2U_AuthCode;
	private String FPX_RefNo;
	private String FPX_AuthCode;
	private String Reason;
	private String VehicleNo;
	private String AuditRemarks;
	private String VehicleLoc;
	private String VehicleMake;
	private String VehicleModel;
	private String YearMake;
	private String ChassisNo;
	private String FinancedBy;
	private String CoverageStartDate;
	private String CoverageEndDate;
	private String EngineNo;
	private String NVIC;

	private String DoneStep1;
	private String DoneStep2;

	private String TLCheckDigit;
	private String UWReason;
	private String QQID;
	private String DriverPAPolicyNo;
	private String JPJStatus;
	private String CAPSPolicyNo;
	private int RecordCount;

	public String getMarketerName() {
		return marketerName;
	}

	public void setMarketerName(String marketerName) {
		this.marketerName = marketerName;
	}

	public String getMarketerCode() {
		return marketerCode;
	}

	public void setMarketerCode(String marketerCode) {
		this.marketerCode = marketerCode;
	}

	public String getSalesEntity() {
		return salesEntity;
	}

	public void setSalesEntity(String salesEntity) {
		this.salesEntity = salesEntity;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCommission() {
		return commission;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}

	public String getSalesTarget() {
		return salesTarget;
	}

	public void setSalesTarget(String salesTarget) {
		this.salesTarget = salesTarget;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getDateFrom() {
		return DateFrom;
	}

	public void setDateFrom(String dateFrom) {
		DateFrom = dateFrom;
	}

	public String getDateTo() {
		return DateTo;
	}

	public void setDateTo(String dateTo) {
		DateTo = dateTo;
	}

	public String getPolicyNo() {
		return PolicyNo;
	}

	public void setPolicyNo(String policyNo) {
		PolicyNo = policyNo;
	}

	public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public String getProductEntity() {
		return ProductEntity;
	}

	public void setProductEntity(String productEntity) {
		ProductEntity = productEntity;
	}

	public String getProductType() {
		return ProductType;
	}

	public void setProductType(String productType) {
		ProductType = productType;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getPremiumAmount() {
		return PremiumAmount;
	}

	public void setPremiumAmount(String premiumAmount) {
		PremiumAmount = premiumAmount;
	}

	public String getTransactionDate() {
		return TransactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		TransactionDate = transactionDate;
	}

	public String getTransactionID() {
		return TransactionID;
	}

	public void setTransactionID(String transactionID) {
		TransactionID = transactionID;
	}

	public String getCustomerNRIC() {
		return CustomerNRIC;
	}

	public void setCustomerNRIC(String customerNRIC) {
		CustomerNRIC = customerNRIC;
	}

	public String getPaymentMethod() {
		return PaymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		PaymentMethod = paymentMethod;
	}

	public String getPaymentRefNo() {
		return PaymentRefNo;
	}

	public void setPaymentRefNo(String paymentRefNo) {
		PaymentRefNo = paymentRefNo;
	}

	public String getPaymentStatus() {
		return PaymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		PaymentStatus = paymentStatus;
	}

	public String getIncentive() {
		return Incentive;
	}

	public void setIncentive(String incentive) {
		Incentive = incentive;
	}

	public String getBankFee() {
		return BankFee;
	}

	public void setBankFee(String bankFee) {
		BankFee = bankFee;
	}

	public String getTotalNet() {
		return TotalNet;
	}

	public void setTotalNet(String totalNet) {
		TotalNet = totalNet;
	}

	public String getCustomerEmail() {
		return CustomerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		CustomerEmail = customerEmail;
	}

	public String getCustomerAddress1() {
		return CustomerAddress1;
	}

	public void setCustomerAddress1(String customerAddress1) {
		CustomerAddress1 = customerAddress1;
	}

	public String getCustomerAddress2() {
		return CustomerAddress2;
	}

	public void setCustomerAddress2(String customerAddress2) {
		CustomerAddress2 = customerAddress2;
	}

	public String getCustomerAddress3() {
		return CustomerAddress3;
	}

	public void setCustomerAddress3(String customerAddress3) {
		CustomerAddress3 = customerAddress3;
	}

	public String getCustomerCity() {
		return CustomerCity;
	}

	public void setCustomerCity(String customerCity) {
		CustomerCity = customerCity;
	}

	public String getCustomerPostcode() {
		return CustomerPostcode;
	}

	public void setCustomerPostcode(String customerPostcode) {
		CustomerPostcode = customerPostcode;
	}

	public String getCustomerState() {
		return CustomerState;
	}

	public void setCustomerState(String customerState) {
		CustomerState = customerState;
	}

	public String getCustomerCountry() {
		return CustomerCountry;
	}

	public void setCustomerCountry(String customerCountry) {
		CustomerCountry = customerCountry;
	}

	public String getCustomerMobileNo() {
		return CustomerMobileNo;
	}

	public void setCustomerMobileNo(String customerMobileNo) {
		CustomerMobileNo = customerMobileNo;
	}

	public String getCustomerID() {
		return CustomerID;
	}

	public void setCustomerID(String customerID) {
		CustomerID = customerID;
	}

	public String getDSPQQID() {
		return DSPQQID;
	}

	public void setDSPQQID(String dSPQQID) {
		DSPQQID = dSPQQID;
	}

	public String getPaymentChannel() {
		return PaymentChannel;
	}

	public void setPaymentChannel(String paymentChannel) {
		PaymentChannel = paymentChannel;
	}

	public String getInvoiceNo() {
		return InvoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		InvoiceNo = invoiceNo;
	}

	public String getProductCode() {
		return ProductCode;
	}

	public void setProductCode(String productCode) {
		ProductCode = productCode;
	}

	public String getTerm() {
		return Term;
	}

	public void setTerm(String term) {
		Term = term;
	}

	public String getCoverage() {
		return Coverage;
	}

	public void setCoverage(String coverage) {
		Coverage = coverage;
	}

	public String getMode() {
		return Mode;
	}

	public void setMode(String mode) {
		Mode = mode;
	}

	public String getPaymentAuthCode() {
		return PaymentAuthCode;
	}

	public void setPaymentAuthCode(String paymentAuthCode) {
		PaymentAuthCode = paymentAuthCode;
	}

	public String getTotalAmount() {
		return TotalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		TotalAmount = totalAmount;
	}

	public String getPremiumMode() {
		return PremiumMode;
	}

	public void setPremiumMode(String premiumMode) {
		PremiumMode = premiumMode;
	}

	public String getMPAY_RefNo() {
		return MPAY_RefNo;
	}

	public void setMPAY_RefNo(String mPAY_RefNo) {
		MPAY_RefNo = mPAY_RefNo;
	}

	public String getMPAY_AuthCode() {
		return MPAY_AuthCode;
	}

	public void setMPAY_AuthCode(String mPAY_AuthCode) {
		MPAY_AuthCode = mPAY_AuthCode;
	}

	public String getEBPG_RefNo() {
		return EBPG_RefNo;
	}

	public void setEBPG_RefNo(String eBPG_RefNo) {
		EBPG_RefNo = eBPG_RefNo;
	}

	public String getEBPG_AuthCode() {
		return EBPG_AuthCode;
	}

	public void setEBPG_AuthCode(String eBPG_AuthCode) {
		EBPG_AuthCode = eBPG_AuthCode;
	}

	public String getM2U_RefNo() {
		return M2U_RefNo;
	}

	public void setM2U_RefNo(String m2u_RefNo) {
		M2U_RefNo = m2u_RefNo;
	}

	public String getM2U_AuthCode() {
		return M2U_AuthCode;
	}

	public void setM2U_AuthCode(String m2u_AuthCode) {
		M2U_AuthCode = m2u_AuthCode;
	}

	public String getFPX_RefNo() {
		return FPX_RefNo;
	}

	public void setFPX_RefNo(String fPX_RefNo) {
		FPX_RefNo = fPX_RefNo;
	}

	public String getFPX_AuthCode() {
		return FPX_AuthCode;
	}

	public void setFPX_AuthCode(String fPX_AuthCode) {
		FPX_AuthCode = fPX_AuthCode;
	}

	public String getReason() {
		return Reason;
	}

	public void setReason(String reason) {
		Reason = reason;
	}

	public String getVehicleNo() {
		return VehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		VehicleNo = vehicleNo;
	}

	public String getAuditRemarks() {
		return AuditRemarks;
	}

	public void setAuditRemarks(String auditRemarks) {
		AuditRemarks = auditRemarks;
	}

	public String getVehicleLoc() {
		return VehicleLoc;
	}

	public void setVehicleLoc(String vehicleLoc) {
		VehicleLoc = vehicleLoc;
	}

	public String getVehicleMake() {
		return VehicleMake;
	}

	public void setVehicleMake(String vehicleMake) {
		VehicleMake = vehicleMake;
	}

	public String getVehicleModel() {
		return VehicleModel;
	}

	public void setVehicleModel(String vehicleModel) {
		VehicleModel = vehicleModel;
	}

	public String getYearMake() {
		return YearMake;
	}

	public void setYearMake(String yearMake) {
		YearMake = yearMake;
	}

	public String getChassisNo() {
		return ChassisNo;
	}

	public void setChassisNo(String chassisNo) {
		ChassisNo = chassisNo;
	}

	public String getFinancedBy() {
		return FinancedBy;
	}

	public void setFinancedBy(String financedBy) {
		FinancedBy = financedBy;
	}

	public String getCoverageStartDate() {
		return CoverageStartDate;
	}

	public void setCoverageStartDate(String coverageStartDate) {
		CoverageStartDate = coverageStartDate;
	}

	public String getCoverageEndDate() {
		return CoverageEndDate;
	}

	public void setCoverageEndDate(String coverageEndDate) {
		CoverageEndDate = coverageEndDate;
	}

	public String getEngineNo() {
		return EngineNo;
	}

	public void setEngineNo(String engineNo) {
		EngineNo = engineNo;
	}

	public String getNVIC() {
		return NVIC;
	}

	public void setNVIC(String nVIC) {
		NVIC = nVIC;
	}

	public String getDoneStep1() {
		return DoneStep1;
	}

	public void setDoneStep1(String doneStep1) {
		DoneStep1 = doneStep1;
	}

	public String getDoneStep2() {
		return DoneStep2;
	}

	public void setDoneStep2(String doneStep2) {
		DoneStep2 = doneStep2;
	}

	public String getTLCheckDigit() {
		return TLCheckDigit;
	}

	public void setTLCheckDigit(String tLCheckDigit) {
		TLCheckDigit = tLCheckDigit;
	}

	public String getUWReason() {
		return UWReason;
	}

	public void setUWReason(String uWReason) {
		UWReason = uWReason;
	}

	public String getQQID() {
		return QQID;
	}

	public void setQQID(String qQID) {
		QQID = qQID;
	}

	public String getDriverPAPolicyNo() {
		return DriverPAPolicyNo;
	}

	public void setDriverPAPolicyNo(String driverPAPolicyNo) {
		DriverPAPolicyNo = driverPAPolicyNo;
	}

	public String getJPJStatus() {
		return JPJStatus;
	}

	public void setJPJStatus(String jPJStatus) {
		JPJStatus = jPJStatus;
	}

	public String getCAPSPolicyNo() {
		return CAPSPolicyNo;
	}

	public void setCAPSPolicyNo(String cAPSPolicyNo) {
		CAPSPolicyNo = cAPSPolicyNo;
	}

	public int getRecordCount() {
		return RecordCount;
	}

	public void setRecordCount(int recordCount) {
		RecordCount = recordCount;
	}

}
