package com.etiqa.model.agent;

public class AgentCategory {
	private String AgentCode;
	private String AgentTyp;

	public String getAgentCode() {
		return AgentCode;
	}

	public void setAgentCode(String agentCode) {
		AgentCode = agentCode;
	}

	public String getAgentTyp() {
		return AgentTyp;
	}

	public void setAgentTyp(String agentTyp) {
		AgentTyp = agentTyp;
	}

}
