package com.etiqa.utils;

public class AddCodes {
	private AddCodes() {
	}
	public static final String OData="7";
	public static final String MCode = "2";
	public static final String QCode = "12";
	public static final String WTCCode = "16";
	public static final String WTCRateCode = "5";
	public static final String HOHHCode = "8";
	public static final String HOHHTCode = "6";
	public static final String MIINFOCode = "1";
	public static final String MTINFOCode = "3";
	public static final String ELSINFOCode = "11";
	public static final String HOHHINFOCode = "17";
	public static final String MIRules1 = "9";
	public static final String MIRules2 = "15";
	public static final String MIRules3 = "13";
	public static final String MIRules4 = "14";
	public static final String ELSOData = "10";
	public static final String MIRCode = "4";
	public static final String BOCode = "18";
	public static final String BCCode = "19";
	public static final String BBCode = "20";
	public static final String BBGCode = "21";
	public static final String BPCode = "22";
	public static final String TEZCode = "23";
	public static final String TCPCode = "24";
	public static final String TCPRCode = "25";

}
