package com.etiqa.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatDates {
	public Date stringToDate(String sDate) {
		Date date1 = null;
		try {
			date1 = new SimpleDateFormat("dd/MM/yyyy").parse(sDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return date1;
	}

	public String dateToString(Date sDate) {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		String strDate = dateFormat.format(sDate);

		return strDate;
	}
}
