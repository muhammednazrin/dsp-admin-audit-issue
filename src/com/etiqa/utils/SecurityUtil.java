package com.etiqa.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.regex.Pattern;
import java.text.Normalizer;


public class SecurityUtil {
	private static Properties aduserInfoProp = new Properties();
	
			//"NAkjfrwGsLpRrfOr";

	public static String enciptString(String plainText) {

		SecurityUtil sutil=new SecurityUtil();
		sutil.loadADuserProps();
		String encryptionKey =aduserInfoProp.getProperty("userkey");
		DecriptionEncription advancedEncryptionStandard = new DecriptionEncription(encryptionKey);
		String cipherText = null;

		try {
			cipherText = advancedEncryptionStandard.encrypt(plainText);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cipherText;
	}

	public static String decriptString(String value) {
		SecurityUtil sutil=new SecurityUtil();
		sutil.loadADuserProps();
		String encryptionKey =aduserInfoProp.getProperty("userkey");
		DecriptionEncription advancedEncryptionStandard = new DecriptionEncription(encryptionKey);
		String cipherText = null;

		try {
			cipherText = advancedEncryptionStandard.decrypt(value);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cipherText;
	}
	 public static String stripXSS(String value){
	    	String cleanValue = null;
	    				if (value != null) {
	    					cleanValue = Normalizer.normalize(value, Normalizer.Form.NFD);
	    					// Avoid null characters
	    					cleanValue = cleanValue.replaceAll("\0", "");
	    					// Avoid anything between script tags
	    					Pattern scriptPattern = Pattern.compile("<script>(.*?)</script>", Pattern.CASE_INSENSITIVE);
	    					cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");
	    					// Avoid anything in a src='...' type of expression
	    					scriptPattern = Pattern.compile("src[\r\n]*=[\r\n]*\\\'(.*?)\\\'", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
	    					cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");
	    					scriptPattern = Pattern.compile("src[\r\n]*=[\r\n]*\\\"(.*?)\\\"", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
	    					cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");
	    					//scriptPattern =Pattern.compile("(?:^|\\s)'([^']*?)'(?:$|\\s)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
	    					//cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");
	    					cleanValue=value.replaceAll("\'","");
	    					// Remove any lonesome </script> tag
	    					scriptPattern = Pattern.compile("</script>", Pattern.CASE_INSENSITIVE);
	    					cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");
	    					// Remove any lonesome <script ...> tag
	    					scriptPattern = Pattern.compile("<script(.*?)>", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
	    					cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");
	    					// Avoid eval(...) expressions
	    					scriptPattern = Pattern.compile("eval\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
	    					cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");
	    					// Avoid expression(...) expressions
	    					scriptPattern = Pattern.compile("expression\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
	    					cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");
	    					// Avoid javascript:... expressions
	    					scriptPattern = Pattern.compile("javascript:", Pattern.CASE_INSENSITIVE);
	    					cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");
	    					// Avoid vbscript:... expressions
	    					scriptPattern = Pattern.compile("vbscript:", Pattern.CASE_INSENSITIVE);
	    					cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");
	    					// Avoid onload= expressions
	    					scriptPattern = Pattern.compile("onload(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
	    					cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");
	    	                              // Avoid special characters
	    					scriptPattern = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
	    					cleanValue = scriptPattern.matcher(cleanValue).replaceAll("");
	    				}
	    				return cleanValue;
	    	}
	 public void loadADuserProps() {
		 InputStream infoad = this.getClass().getClassLoader().getResourceAsStream("com/spring/uam/ldap/adconfig.properties");
		 try {
			 aduserInfoProp.load(infoad);
		 } catch (IOException e) {
			e.printStackTrace();
		 }
	 }
}
