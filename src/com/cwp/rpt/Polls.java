package com.cwp.rpt;

import java.util.Date;

public class Polls {
	
	String id;
	String question;
	Date startdate;
	Date ExpiryDate;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}
	public Date getExpiryDate() {
		return ExpiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		ExpiryDate = expiryDate;
	}
	@Override
	public String toString() {
		return "Polls [id=" + id + ", question=" + question + ", startdate=" + startdate + ", ExpiryDate=" + ExpiryDate
				+ "]";
	}


}
