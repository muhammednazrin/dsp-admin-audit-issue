package com.cwp.rpt;

public class ValidationFailedAgentRpt {

	  private String idNumber;
	  private String policyno;
	  private String email;
	  private String mobile;
	  private String responsecode;
	  private String lastdate;
	  private String startdate;
	  private String enddate;
	  private String FailedCustomerName;
	  
	  
	public String getFailedCustomerName() {
		return FailedCustomerName;
	}
	public void setFailedCustomerName(String failedCustomerName) {
		FailedCustomerName = failedCustomerName;
	}
	public String getLastdate() {
			return lastdate;
		}
	public void setLastdate(String lastdate) {
			this.lastdate = lastdate;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	public String getPolicyno() {
		return policyno;
	}
	public void setPolicyno(String policyno) {
		this.policyno = policyno;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getResponsecode() {
		return responsecode;
	}
	public void setResponsecode(String responsecode) {
		this.responsecode = responsecode;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}

}
