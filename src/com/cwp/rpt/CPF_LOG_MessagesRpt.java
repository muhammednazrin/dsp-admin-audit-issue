package com.cwp.rpt;

public class CPF_LOG_MessagesRpt {
	
	private String Message_ID;
	private String Major_Transaction_Id;
	private String Message_Tracking_Id;
	private String Service_Name;
	private String Logging_Point;
	private String Datetime;
	private String Pay_Load;
	
	public String getMessage_ID() {
		return Message_ID;
	}

	public void setMessage_ID(String message_ID) {
		Message_ID = message_ID;
	}
	
	public String getMajor_Transaction_Id() {
		return Major_Transaction_Id;
	}
	
	public void setMajor_Transaction_Id(String major_Transaction_Id) {
		Major_Transaction_Id = major_Transaction_Id;
	}
	
	public String getMessage_Tracking_Id() {
		return Message_Tracking_Id;
	}
	
	public void setMessage_Tracking_Id(String message_Tracking_Id) {
		Message_Tracking_Id = message_Tracking_Id;
	}
	
	public String getService_Name() {
		return Service_Name;
	}
	
	public void setService_Name(String service_Name) {
		Service_Name = service_Name;
	}
	
	public String getLogging_Point() {
		return Logging_Point;
	}
	
	public void setLogging_Point(String logging_Point) {
		Logging_Point = logging_Point;
	}
	
	public String getDatetime() {
		return Datetime;
	}
	
	public void setDatetime(String datetime) {
		Datetime = datetime;
	}
	
	public String getPay_Load() {
		return Pay_Load;
	}
	
	public void setPay_Load(String pay_Load) {
		Pay_Load = pay_Load;
	}

}
