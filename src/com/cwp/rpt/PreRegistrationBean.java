package com.cwp.rpt;

public class PreRegistrationBean {

	  private String icnumber;
	  private String reg_status_Code;
	  private String send_email_Status_code;
	  private String send_otc_status_code;
	  private String datetime;
	public String getIcnumber() {
		return icnumber;
	}
	public void setIcnumber(String icnumber) {
		this.icnumber = icnumber;
	}
	public String getReg_status_Code() {
		return reg_status_Code;
	}
	public void setReg_status_Code(String reg_status_Code) {
		this.reg_status_Code = reg_status_Code;
	}
	public String getSend_email_Status_code() {
		return send_email_Status_code;
	}
	public void setSend_email_Status_code(String send_email_Status_code) {
		this.send_email_Status_code = send_email_Status_code;
	}
	public String getSend_otc_status_code() {
		return send_otc_status_code;
	}
	public void setSend_otc_status_code(String send_otc_status_code) {
		this.send_otc_status_code = send_otc_status_code;
	}
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	
	  
	  
	  
}
