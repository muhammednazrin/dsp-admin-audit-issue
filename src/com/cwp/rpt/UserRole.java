package com.cwp.rpt;

public class UserRole {
	
	private int ID;
	private int RoleID;
	private int UserID;
    private String Isactive;
    private String RoleName;
    private String Assignedby;
    private String AssignedDate;
    private String UserName;
    private String URL;
    
    
    
	public int getUserID() {
		return UserID;
	}
	public void setUserID(int userID) {
		UserID = userID;
	}
	
	public int getRoleID() {
		return RoleID;
	}
	public void setRoleID(int roleID) {
		RoleID = roleID;
	}
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getIsactive() {
		return Isactive;
	}
	public void setIsactive(String isactive) {
		Isactive = isactive;
	}
	public String getRoleName() {
		return RoleName;
	}
	public void setRoleName(String roleName) {
		RoleName = roleName;
	}
	public String getAssignedby() {
		return Assignedby;
	}
	public void setAssignedby(String assignedby) {
		Assignedby = assignedby;
	}
	public String getAssignedDate() {
		return AssignedDate;
	}
	public void setAssignedDate(String assignedDate) {
		AssignedDate = assignedDate;
	}
	public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	
	
	
	
}
