package com.cwp.rpt;

import java.util.Date;

public class UserActivityRpt {

    private String CustomerName;
    private String IC_NUMBER;
    private String LOGINDATETIME;
    private String LOGOUTDATETIME;
    private String DURATION;
    private String LOGIN_FLAG;
    
	public String getCustomerName() {
		return CustomerName;
	}
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	public String getIC_NUMBER() {
		return IC_NUMBER;
	}
	public void setIC_NUMBER(String iC_NUMBER) {
		IC_NUMBER = iC_NUMBER;
	}
	public String getLOGINDATETIME() {
		return LOGINDATETIME;
	}
	public void setLOGINDATETIME(String lOGINDATETIME) {
		LOGINDATETIME = lOGINDATETIME;
	}
	public String getLOGOUTDATETIME() {
		return LOGOUTDATETIME;
	}
	public void setLOGOUTDATETIME(String lOGOUTDATETIME) {
		LOGOUTDATETIME = lOGOUTDATETIME;
	}
	public String getDURATION() {
		return DURATION;
	}
	public void setDURATION(String dURATION) {
		DURATION = dURATION;
	}
	public String getLOGIN_FLAG() {
		return LOGIN_FLAG;
	}
	public void setLOGIN_FLAG(String lOGIN_FLAG) {
		LOGIN_FLAG = lOGIN_FLAG;
	}
	
}
