package com.cwp.rpt;

public class UserRegRpt
{
	      private String CustomerName;
		  private String ID_Type;
		  private String IC_Number;
		  private String Date_Of_Birth;
		  private String Gender;
		  private String Nationality;
		  private String Marital_Status;
		  private String Policy_Number;
		  private String Race;
		  private String Address;
		  private String City;
		  private String State;
		  private String PostCode;
		  private String Country;
		  private String Home;
		  private String Mobile;
		  private String Email_Address;
		  private String Last_Update;
		  private String Education_Level;
		  private String Monthly_Income;
		  private String Pref_Language;
		  private String Insured_Sine_Date;
		  private String Staff_Flag;
		  private String Childrens;
		  private String Religion;
		  private String Response_Code;
		  private String Trx_id;
		  private String UserName;
		  private String Status;
		  private String Last_Activity;
		  private String login_Flag;
		  private String policyno;
		  		
		public String getPolicyno() {
			return policyno;
		}
		
		public void setPolicyno(String policyno) {
			this.policyno = policyno;
		}
		
		public String getCustomerName() {
			return CustomerName;
		}
		public void setCustomerName(String customerName) {
			CustomerName = customerName;
		}
		public String getID_Type() {
			return ID_Type;
		}
		public void setID_Type(String iD_Type) {
			ID_Type = iD_Type;
		}
		public String getIC_Number() {
			return IC_Number;
		}
		public void setIC_Number(String iC_Number) {
			IC_Number = iC_Number;
		}
		public String getDate_Of_Birth() {
			return Date_Of_Birth;
		}
		public void setDate_Of_Birth(String date_Of_Birth) {
			Date_Of_Birth = date_Of_Birth;
		}
		public String getGender() {
			return Gender;
		}
		public void setGender(String gender) {
			Gender = gender;
		}
		public String getNationality() {
			return Nationality;
		}
		public void setNationality(String nationality) {
			Nationality = nationality;
		}
		public String getMarital_Status() {
			return Marital_Status;
		}
		public void setMarital_Status(String marital_Status) {
			Marital_Status = marital_Status;
		}
		public String getRace() {
			return Race;
		}
		public void setRace(String race) {
			Race = race;
		}
		public String getAddress() {
			return Address;
		}
		public void setAddress(String address) {
			Address = address;
		}
		public String getCity() {
			return City;
		}
		public void setCity(String city) {
			City = city;
		}
		public String getState() {
			return State;
		}
		public void setState(String state) {
			State = state;
		}
		public String getPostCode() {
			return PostCode;
		}
		public void setPostCode(String postCode) {
			PostCode = postCode;
		}
		public String getCountry() {
			return Country;
		}
		public void setCountry(String country) {
			Country = country;
		}
		public String getHome() {
			return Home;
		}
		public void setHome(String home) {
			Home = home;
		}
		public String getMobile() {
			return Mobile;
		}
		public void setMobile(String mobile) {
			Mobile = mobile;
		}
		public String getEmail_Address() {
			return Email_Address;
		}
		public void setEmail_Address(String email_Address) {
			Email_Address = email_Address;
		}
		public String getLast_Update() {
			return Last_Update;
		}
		public void setLast_Update(String last_Update) {
			Last_Update = last_Update;
		}
		public String getEducation_Level() {
			return Education_Level;
		}
		public void setEducation_Level(String education_Level) {
			Education_Level = education_Level;
		}
		public String getMonthly_Income() {
			return Monthly_Income;
		}
		public void setMonthly_Income(String monthly_Income) {
			Monthly_Income = monthly_Income;
		}
		public String getPref_Language() {
			return Pref_Language;
		}
		public void setPref_Language(String pref_Language) {
			Pref_Language = pref_Language;
		}
		public String getInsured_Sine_Date() {
			return Insured_Sine_Date;
		}
		public void setInsured_Sine_Date(String insured_Sine_Date) {
			Insured_Sine_Date = insured_Sine_Date;
		}
		public String getStaff_Flag() {
			return Staff_Flag;
		}
		public void setStaff_Flag(String staff_Flag) {
			Staff_Flag = staff_Flag;
		}
		public String getChildrens() {
			return Childrens;
		}
		public void setChildrens(String childrens) {
			Childrens = childrens;
		}
		public String getReligion() {
			return Religion;
		}
		public void setReligion(String religion) {
			Religion = religion;
		}
		public String getResponse_Code() {
			return Response_Code;
		}
		public void setResponse_Code(String response_Code) {
			Response_Code = response_Code;
		}
		public String getTrx_id() {
			return Trx_id;
		}
		public void setTrx_id(String trx_id) {
			Trx_id = trx_id;
		}
		public String getUserName() {
			return UserName;
		}
		public void setUserName(String userName) {
			UserName = userName;
		}
		public String getStatus() {
			return Status;
		}
		public void setStatus(String status) {
			Status = status;
		}
		public String getLast_Activity() {
			return Last_Activity;
		}
		public void setLast_Activity(String last_Activity) {
			Last_Activity = last_Activity;
		}
		public String getLogin_Flag() {
			return login_Flag;
		}
		public void setLogin_Flag(String login_Flag) {
			this.login_Flag = login_Flag;
		}
		public String getPolicy_Number() {
			return Policy_Number;
		}
		public void setPolicy_Number(String policy_Number) {
			Policy_Number = policy_Number;
		}
	
		
}
