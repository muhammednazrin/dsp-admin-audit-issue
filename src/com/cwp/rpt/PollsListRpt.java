package com.cwp.rpt;

import java.util.Date;
import java.util.List;

public class PollsListRpt {
	
 List<Polls> polls;

public List<Polls> getPolls() {
	return polls;
}

public void setPolls(List<Polls> polls) {
	this.polls = polls;
}

@Override
public String toString() {
	return "PollsListRpt [polls=" + polls + "]";
}
 
 
}
