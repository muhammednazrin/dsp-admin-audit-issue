package com.cwp.rpt;

public class UserRoleRpt {

    private String FullName;
    private String UserName;
    private String RoleName;
    private String AssignDate;
    private String AssignedBy;
    private String IsActive;
    
	public String getFullName() {
		return FullName;
	}
	public void setFullName(String fullName) {
		FullName = fullName;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getRoleName() {
		return RoleName;
	}
	public void setRoleName(String roleName) {
		RoleName = roleName;
	}
	public String getAssignDate() {
		return AssignDate;
	}
	public void setAssignDate(String assignDate) {
		AssignDate = assignDate;
	}
	public String getAssignedBy() {
		return AssignedBy;
	}
	public void setAssignedBy(String assignedBy) {
		AssignedBy = assignedBy;
	}
	public String getIsActive() {
		return IsActive;
	}
	public void setIsActive(String isActive) {
		IsActive = isActive;
	}
	
}
