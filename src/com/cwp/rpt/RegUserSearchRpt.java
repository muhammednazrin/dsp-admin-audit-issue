package com.cwp.rpt;

public class RegUserSearchRpt {

	private String CustomerName;
	private String Icnumber;
	private String Policynumber;
    private String Mobile;
	private String EmailAddress;
	private String LastUpdate;
	  
	public String getCustomerName() {
		return CustomerName;
	}
	
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	
	public String getIcnumber() {
		return Icnumber;
	}
	
	public void setIcnumber(String icnumber) {
		Icnumber = icnumber;
	}
	
	public String getPolicynumber() {
		return Policynumber;
	}
	
	public void setPolicynumber(String policynumber) {
		Policynumber = policynumber;
	}
	
	public String getMobile() {
		return Mobile;
	}
	
	public void setMobile(String mobile) {
		Mobile = mobile;
	}
	
	public String getEmailAddress() {
		return EmailAddress;
	}
	
	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}
	
	public String getLastUpdate() {
		return LastUpdate;
	}
	
	public void setLastUpdate(String lastUpdate) {
		LastUpdate = lastUpdate;
	}
	
}
