package com.cwp.rpt;

public class CPF_LOG_SummaryRpt {

	private String Message_Tracking_ID;
	private String Major_Transaction_ID;
	private String Status;
	private String Start_DTM;
	private String Finish_DTM;
	private String Duration;
	private String Client_Name;
	private String User_Name;
	public String getMessage_Tracking_ID() {
		return Message_Tracking_ID;
	}
	public void setMessage_Tracking_ID(String message_Tracking_ID) {
		Message_Tracking_ID = message_Tracking_ID;
	}
	public String getMajor_Transaction_ID() {
		return Major_Transaction_ID;
	}
	public void setMajor_Transaction_ID(String major_Transaction_ID) {
		Major_Transaction_ID = major_Transaction_ID;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getStart_DTM() {
		return Start_DTM;
	}
	public void setStart_DTM(String start_DTM) {
		Start_DTM = start_DTM;
	}
	public String getFinish_DTM() {
		return Finish_DTM;
	}
	public void setFinish_DTM(String finish_DTM) {
		Finish_DTM = finish_DTM;
	}
	public String getDuration() {
		return Duration;
	}
	public void setDuration(String duration) {
		Duration = duration;
	}
	public String getClient_Name() {
		return Client_Name;
	}
	public void setClient_Name(String client_Name) {
		Client_Name = client_Name;
	}
	public String getUser_Name() {
		return User_Name;
	}
	public void setUser_Name(String user_Name) {
		User_Name = user_Name;
	}
	
	
		
	
	
	
	
	
}
