package com.cwp.usermanagement;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwp.admin.User;

	@WebServlet("/EditUserData")
	public class EditUserController extends HttpServlet {
		
		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException 
		{
			List<User> EditmyDataList = new GetUser_DAO().getReportData();
			request.setAttribute("EditmyDataList", EditmyDataList);
			request.getRequestDispatcher("UserDetails.jsp").forward(request, response);
		}

		public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
		{	
			request.getRequestDispatcher("UserDetails.jsp").forward(request, response);
		}
	
	}

