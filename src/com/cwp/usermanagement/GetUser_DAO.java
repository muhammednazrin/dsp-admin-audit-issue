package com.cwp.usermanagement;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.cwp.admin.User;
import com.cwp.dao.ValidationFailedRptDAO;
import com.cwp.rpt.PollsListRpt;
import com.cwp.rpt.ValidationFailedRpt;
import com.cwp.util.DBUtil;

import oracle.jdbc.OracleTypes;

public class GetUser_DAO {

	private static DBUtil db1;
	private static Connection connection;
	
	public List<User> getReportData()
	{
		List<User> myDataList = new ArrayList<User>();
		ResultSet resultSet = null;
		Statement statement = null;
		
		try{
	
			db1 = new DBUtil();
			connection = db1.getConnection();
			
			CallableStatement cstmt = connection.prepareCall("{call Get_User(?)}");		           
	     	cstmt.registerOutParameter(1, OracleTypes.CURSOR);
	        cstmt.execute();
	        resultSet =(ResultSet)cstmt.getObject (1);         
			
	        while(resultSet.next())
			{
	        	User reportData = new User();
	        	reportData.setUsername(resultSet.getString("USERNAME"));
				reportData.setCreateddate(resultSet.getString("CREATEDDATE"));
				reportData.setCreatedby(resultSet.getString("CREATEDBY"));
				reportData.setIsactive(resultSet.getString("ISACTIVE"));
				myDataList.add(reportData);
			}
		}
		catch(Exception e){
			e.printStackTrace(); 
		}
		finally 
		{
			if (resultSet != null) { try { resultSet.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (statement != null) { try { statement.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (connection != null) { try { connection.close(); } catch (SQLException e) { e.printStackTrace(); } }
		}

		return myDataList;
	
	}
	
	public static void main(String args [])
	{
		GetUser_DAO rdao = new GetUser_DAO();
		List<User> releaseDataList = rdao.getReportData();
		Iterator itr = releaseDataList.iterator();
		
		while(itr.hasNext())
		{
			User rd = (User) itr.next();
			
		}
		
	}
	
	
}
