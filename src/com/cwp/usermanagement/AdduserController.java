package com.cwp.usermanagement;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.cwp.util.DBUtil;

@WebServlet("/addsysuser1")
public class AdduserController extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
        response.setHeader("Cache-control", "no-cache, no-store");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type");
        response.setHeader("Access-Control-Max-Age", "86400");
		
		Connection connection = null;
		DBUtil database= new DBUtil();
	   
	    PreparedStatement pstmt = null;
		String strUSERNAME = request.getParameter("txtUserName");
		String strFULLNAME = request.getParameter("txtFULLNAME");
		
		Date create_date= new Date();
		String response_code="0000";
		String trx_id="0000";
		int status=1;
        String respmessage="";
		
		String str = "insert into CWPADMIN_USERS(USERNAME,CREATEDDATE,CREATEDBY,ISACTIVE,FULLNAME) values(?,?,?,?,?)";
		
		try {
			
			try {
				connection = database.getConnection();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			
			pstmt = connection.prepareStatement(str);
			pstmt.setString(1, strUSERNAME); 
			pstmt.setDate(2, java.sql.Date.valueOf(java.time.LocalDate.now())); 
			pstmt.setString(3, "superadmin"); // set input parameter 2
		    pstmt.setInt(4, 1);
		    pstmt.setString(5,strFULLNAME);
		    int rowsInserted = pstmt.executeUpdate(); // execute insert statement
		    System.out.println(rowsInserted);
		    pstmt.close();
	
		    connection.close();
		    
		    if (rowsInserted > 0) {
		        System.out.println("A new user was inserted successfully!");
		        respmessage="Success";
		    }else
		    {
		    	  respmessage="Failed";
		    	
		    }

		}  catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			respmessage="Failed";
		}
		
		JSONObject json = new JSONObject();
		 try {
			json.put("res_code", respmessage);
			System.out.println("JSON");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 out.println(json.toString());
         out.close();
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	
}
