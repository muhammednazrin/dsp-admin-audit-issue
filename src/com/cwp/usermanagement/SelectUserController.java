package com.cwp.usermanagement;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwp.admin.User;

@WebServlet("/GetUserData")
public class SelectUserController extends HttpServlet {
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		List<User> UsermyDataList = new GetUser_DAO().getReportData();
		request.setAttribute("UsersDataList", UsermyDataList);
		request.getRequestDispatcher("AddUser.jsp").forward(request, response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException 
	{
		request.getRequestDispatcher("AddUser.jsp").forward(request, response);
	}
}
