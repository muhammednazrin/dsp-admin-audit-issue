package com.cwp.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;

import com.cwp.dao.PollReportDAO;
import com.cwp.rpt.PollsListRpt;
@RequestMapping("cwp")
@WebServlet("/cwp/PollsReportData")
public class PollsController extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PollsController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json");
        response.setHeader("Cache-control", "no-cache, no-store");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "-1");

        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type");
        response.setHeader("Access-Control-Max-Age", "86400");
		
        PollReportDAO pollReportDAO = new PollReportDAO();
        System.out.println("*****"+(String)request.getParameter("operation"));
		if("pollDetails".equals((String)request.getParameter("operation")) && request.getParameter("operation")!=null){
			String resp= pollReportDAO.getPollDetails((String)request.getParameter("pollID"));
			PrintWriter out= response.getWriter();
			System.out.println("*********"+resp);
			out.print(resp);
		}else if("searchPolls".equals((String)request.getParameter("operation")) && request.getParameter("operation")!=null){
			
			String resp= pollReportDAO.getSearchPolls((String)request.getParameter("searchString"),(String)request.getParameter("searchStartDate"),(String)request.getParameter("searchEndDate"));
			PrintWriter out= response.getWriter();
			System.out.println("*********"+resp);
			out.print(resp);

		}else{

			PollsListRpt listRpt=pollReportDAO.getPollsList();
			request.setAttribute("pollslist", listRpt);
			System.out.println("else part");
			request.getRequestDispatcher("admin-polls.jsp").forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
