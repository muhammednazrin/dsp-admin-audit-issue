package com.cwp.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwp.dao.RolesManagement_DAO;
import com.cwp.rpt.UserRole;

public class MenuController extends HttpServlet{

	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
		String strusername = "faheem";
        List<UserRole> myDataList = new RolesManagement_DAO().getUsersRolesMenu(strusername);
		request.setAttribute("myDataListMenu", myDataList);
        request.getRequestDispatcher("cwpmenu.jsp").forward(request, response);
    }
    
	public void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException
	{
	   	 request.getRequestDispatcher("cwpmenu.jsp").forward(request, response);
	} 
	
	
}
