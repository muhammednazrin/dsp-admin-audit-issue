package com.cwp.controller;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwp.admin.ServiceRequestBO;
import com.cwp.dao.WebSRDAO;

import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("cwp")
@WebServlet("/cwp/getSRReportData")
public class WebSRRequestedController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		String fromdate = request.getParameter("fromdate");
		String todate = request.getParameter("todate");
		SimpleDateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
	
		Date date;
		Date toDateparse;
	
		try 
		{
		
			date = originalFormat.parse(fromdate);
			toDateparse = originalFormat.parse(todate);
			request.setAttribute("fromdate", fromdate);
			request.setAttribute("todate", todate);
			fromdate = targetFormat.format(date);
			todate = targetFormat.format(toDateparse);
     	} 
		catch (ParseException ex) {
			// Handle Exception.
			ex.printStackTrace();
		}

		List<ServiceRequestBO> UserSRDataList = new WebSRDAO().getServiceRequestList(fromdate, todate);
		request.setAttribute("UserSRDataList", UserSRDataList);
		request.getRequestDispatcher("webSRRequested.jsp").forward(request, response);
	
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
		Date date = new Date();
		request.getRequestDispatcher("webSRRequested.jsp").forward(request, response);
	}
}
