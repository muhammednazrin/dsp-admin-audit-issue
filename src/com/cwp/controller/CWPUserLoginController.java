package com.cwp.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cwp.dao.MenuDao;
import com.cwp.dao.MenuDaoImpl;
import com.cwp.pojo.Menu;
import com.etiqa.DAO.UserValidation_DAO;
import com.etiqa.admin.User;
import com.etiqa.utils.SecurityUtil;
import com.spring.utils.sessionInterceptor.SessionInterceptor;

import org.apache.log4j.Logger;

@Controller
public class CWPUserLoginController {
	@RequestMapping(value = "cwp/cwpDashboard")
	public String cwpUserLogin(HttpServletRequest request, Model model, HttpServletResponse response) throws IOException {

		final  Logger logger = Logger.getLogger(CWPUserLoginController.class);
		

		// TODO Auto-generated method stub
		System.out.println("printing for the session interceptor logs"+"login for cwp");
      
       
		response.getWriter().append("Served at: ").append(request.getContextPath());
		String userId = null;
		String role = null;
		String ModuleId = null;
		HttpSession session = request.getSession(true);
		if(request.getParameter("id")!=null){
		String uamReqParam = request.getParameter("id");
		
		session.setAttribute("uamReqParamSession", uamReqParam);

		System.out.println("Encript UAM_Req_Param Outer >>>> " + uamReqParam);
		if (uamReqParam != null) {
			System.out.println("Encript UAM_Req_Param  In >>>> " + uamReqParam);
			String decryptMsg = SecurityUtil.decriptString(uamReqParam);
			System.out.println("Encript UAM_Req_Param " + decryptMsg);
			// split the String for getting userId,roleId,ModuleId
			if (decryptMsg != null) {
				String[] items = decryptMsg.split("\\$");
				userId = items[0].toString();
				role = (items[1].toString());
				ModuleId = items[2].toString();

				System.out.println("User Id :" + userId);
				System.out.println("Role Id :" + role);
				System.out.println("ModuleId :" + ModuleId);
				//session.setAttribute("user", userId);
	         UserValidation_DAO vdao = new UserValidation_DAO();
				
				User muser = new User();
				muser = vdao.getvalidateByUserID(userId);
				session.setAttribute("FullName", muser.getFullName());
				session.setAttribute("logedUser", userId);
				int uId = muser.getID();
				session.setAttribute("user", String.valueOf(uId));
				session.setAttribute("username", muser.getUsername());
				System.out.println("Full Name  : " + muser.getFullName());
				session.setAttribute("user", String.valueOf(uId));

			}
			List<Menu> firstList=new ArrayList<Menu>();
			List<Menu> secondList=new ArrayList<Menu>();
			List<Menu> thirdList=new ArrayList<Menu>();
			List<Menu> fourthList=new ArrayList<Menu>();
			//get Menu List from  Rest service start
			System.out.println("Calling DashBoard Controller Pramaiyan");
			Map<String, List<Menu>> map= getMenu(role, ModuleId);
			
			
			 for (Entry<String, List<Menu>> entry : map.entrySet()) {
		            String key = entry.getKey();
		            List<Menu> values = entry.getValue();
		            
		            if(key=="1")
		            {
		            	firstList.addAll(values);		            	
		            }
		            if(key=="2")
		            {
		            	secondList.addAll(values);		            	
		            }
		            if(key=="3")
		            {
		            	thirdList.addAll(values);		            	
		            }
		            if(key=="4")
		            {
		            	fourthList.addAll(values);		            	
		            } 
			 }
			 
			 	session.setAttribute("level_1",firstList );
				session.setAttribute("level_2",secondList );
				session.setAttribute("level_3",thirdList );
				session.setAttribute("level_4",fourthList );

		} else {
			System.out.println("Encript UAM_Req_Param else  >>>> " + uamReqParam);
		}
		}
		//request.getRequestDispatcher("admin-phone.jsp").forward(request, response); 
	
		return "admin-phone";
		
	}
	public Map getMenuList(String id) throws SQLException {
		
		
		Map levels = new HashMap();
		ArrayList<Menu> menu_first_all = new ArrayList<Menu>();
		ArrayList<Menu> menu_second_all = new ArrayList<Menu>();
		ArrayList<Menu> menu_third_all = new ArrayList<Menu>();
		ArrayList<Menu> menu_fourth_all = new ArrayList<Menu>();
		
		int moduleId = 0;
		String roles = "";
		System.out.println(id + "id");
		String idparams[] = id.split("\\$");
		if (idparams.length == 2) {
			moduleId = Integer.parseInt(idparams[1]);
			roles = idparams[0];
			System.out.println(roles + "," + moduleId);
		//	map = getMenuList(roles, moduleId);
		}
		// String roles="1,10";
		MenuDao menuDao = new MenuDaoImpl();
		String[] params = roles.split(",");
		for (int i = 0; i < params.length; i++) {
			// System.out.println(params[i]+"role id");
			ArrayList<Menu> menus = new ArrayList<Menu>();
			ArrayList<Menu> menu_first = new ArrayList<Menu>();
			ArrayList<Menu> menu_second = new ArrayList<Menu>();
			ArrayList<Menu> menu_third = new ArrayList<Menu>();
			ArrayList<Menu> menu_fourth = new ArrayList<Menu>();
			menus = menuDao.getMenuListCWP(moduleId, Integer.parseInt(params[i]));
			System.out.println(moduleId + "," + params[i] + "menu size:" + menus.size());

			// System.out.println("size "+menus.size());
			for (Menu single : menus) {
				//System.out.println("check parmission  1 :" + single.getPermission_str());
				if (single.getLevelId() == 1) {
					// String permission_str =
					// SecurityUtil.enciptString(single.getPermission_str());
					// single.setPermission_str(permission_str);
					menu_first.add(single);
					// break;
				}
			}
			for (Menu single : menus) {
				//System.out.println("check parmission  2 :" + single.getPermission_str());
				if (single.getLevelId() == 2) {
					// String permission_str =
					// SecurityUtil.enciptString(single.getPermission_str());
					// single.setPermission_str(permission_str);
					menu_second.add(single);
					// break;
				}
			}
			for (Menu single : menus) {
				//System.out.println("check parmission  3 :" + single.getPermission_str());
				if (single.getLevelId() == 3) {
					// String permission_str =
					// SecurityUtil.enciptString(single.getPermission_str());
					// single.setPermission_str(permission_str);
					menu_third.add(single);
					// break;
				}
			}
			for (Menu single : menus) {
				//System.out.println("check parmission  4 :" + single.getPermission_str());
				if (single.getLevelId() == 4) {
					// String permission_str =
					// SecurityUtil.enciptString(single.getPermission_str());
					// single.setPermission_str(permission_str);
					menu_fourth.add(single);
					// break;
				}
			}
			menu_first_all.addAll(menu_first);
			menu_second_all.addAll(menu_second);
			menu_third_all.addAll(menu_third);
			menu_fourth_all.addAll(menu_fourth);
		}
		System.out.println(menu_first_all.size() + "first level size before merge");
		for (int i = 0; i < menu_first_all.size(); i++) {
			System.out.println(
					"Menu Name  and ID " + menu_first_all.get(i).getName() + "," + menu_first_all.get(i).getId());

		}
		Set<Menu> set_first = new HashSet<Menu>(menu_first_all);
		Set<Menu> set_second = new HashSet<Menu>(menu_second_all);
		Set<Menu> set_third = new HashSet<Menu>(menu_third_all);
		Set<Menu> set_fourth = new HashSet<Menu>(menu_fourth_all);
		Iterator<Menu> iterator = set_first.iterator();
		while (iterator.hasNext()) {
			Menu setElement = iterator.next();
			System.out.println(setElement.getName() + "," + setElement.getId());
		}
		// Set myOrderedSet = new LinkedHashSet(set_first);
		System.out.println(set_first.size() + "first level size after merge");
		ArrayList<Menu> menu_first_final = new ArrayList<Menu>(set_first);
		ArrayList<Menu> menu_second_final = new ArrayList<Menu>(set_second);
		ArrayList<Menu> menu_third_final = new ArrayList<Menu>(set_third);
		ArrayList<Menu> menu_fourth_final = new ArrayList<Menu>(set_fourth);
		// sorting menu based on Id
		/*
		 * Collections.sort(menu_first_final, (Menu a, Menu b) ->
		 * a.getId().compareTo(b.getId())); Collections.sort(menu_second_final,
		 * (Menu a, Menu b) -> a.getId().compareTo(b.getId()));
		 * Collections.sort(menu_third_final, (Menu a, Menu b) ->
		 * a.getId().compareTo(b.getId())); Collections.sort(menu_fourth_final,
		 * (Menu a, Menu b) -> a.getId().compareTo(b.getId()));
		 */
		levels.put("first", menu_first_final);
		levels.put("second", menu_second_final);
		levels.put("third", menu_third_final);
		levels.put("fourth", menu_fourth_final);
		return levels;
	}
	private Map<String, List<Menu>> getMenu(String roleid, String moduleId)
	{
		System.out.println("roleid >>>" + roleid);
		System.out.println("moduleId >>>> " + moduleId);
		Menu m = new Menu();

		Map<String, List<Menu>> resp = new HashMap<String, List<Menu>>();	
		// String roleid="6";
		String delimiters = "$";
		// String moduleId="1";
		try {
			resp=getMenuList(roleid + delimiters + moduleId);
			 System.out.println(resp);
			m.setModuleId(Short.valueOf(moduleId.toString()));
			System.out.println("resp  " + resp + "\n");
			 System.out.println("Module Id  : " + m.getModuleId());
			 System.out.println("-------------------------------");
			List<Menu> firstList = resp.get("first");
			List<Menu> secondList = resp.get("second");
			List<Menu> thirdList = resp.get("third");
			List<Menu> fourthList = resp.get("fourth");


			resp.put("1", firstList);
			resp.put("2", secondList);
			resp.put("3", thirdList);
			resp.put("4", fourthList);

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("no Connection");
		}
		return resp;
	}
}
