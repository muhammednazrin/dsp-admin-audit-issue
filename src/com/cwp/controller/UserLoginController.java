package com.cwp.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.cwp.admin.User;
import com.cwp.dao.MenuDao;
import com.cwp.dao.MenuDaoImpl;
import com.cwp.dao.UserValidation_DAO;
import com.cwp.ldap.LDAPAttributesBean;
import com.cwp.ldap.LoginCPF;
import com.cwp.pojo.Menu;
import com.cwp.util.SecurityUtil;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Servlet implementation class UserLoginController
 */
//@WebServlet("/login")
public class UserLoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserLoginController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
/*	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("login for cwp");
		response.getWriter().append("Served at: ").append(request.getContextPath());
		String userId = null;
		String role = null;
		String ModuleId = null;
		HttpSession session = request.getSession(true);
		if(request.getParameter("id")!=null){
		String uamReqParam = request.getParameter("id");
		session.setAttribute("uamReqParamSession", uamReqParam);

		
		if (uamReqParam != null) {
		
			String decryptMsg = SecurityUtil.decriptString(uamReqParam);
	
			// split the String for getting userId,roleId,ModuleId
			if (decryptMsg != null) {
				String[] items = decryptMsg.split("\\$");
				userId = items[0].toString();
				role = (items[1].toString());
				ModuleId = items[2].toString();

				
				//session.setAttribute("user", userId);
	         UserValidation_DAO vdao = new UserValidation_DAO();
				
				User muser = new User();
				muser = vdao.getvalidateByUserID(userId);
				session.setAttribute("FullName", muser.getFullName());
				session.setAttribute("logedUser", userId);
				int uId = muser.getID();
				session.setAttribute("user", String.valueOf(uId));
				session.setAttribute("username", muser.getUsername());
			
				session.setAttribute("user", String.valueOf(uId));

			}
			List<Menu> firstList=new ArrayList<Menu>();
			List<Menu> secondList=new ArrayList<Menu>();
			List<Menu> thirdList=new ArrayList<Menu>();
			List<Menu> fourthList=new ArrayList<Menu>();
			//get Menu List from  Rest service start
		
			Map<String, List<Menu>> map= getMenu(role, ModuleId);
			
			
			 for (Entry<String, List<Menu>> entry : map.entrySet()) {
		            String key = entry.getKey();
		            List<Menu> values = entry.getValue();
		            
		            if(key=="1")
		            {
		            	firstList.addAll(values);		            	
		            }
		            if(key=="2")
		            {
		            	secondList.addAll(values);		            	
		            }
		            if(key=="3")
		            {
		            	thirdList.addAll(values);		            	
		            }
		            if(key=="4")
		            {
		            	fourthList.addAll(values);		            	
		            } 
			 }
			 
			 	session.setAttribute("level_1",firstList );
				session.setAttribute("level_2",secondList );
				session.setAttribute("level_3",thirdList );
				session.setAttribute("level_4",fourthList );

		} else {
			System.out.println("Encript UAM_Req_Param else  >>>> " + uamReqParam);
		}
		}
		request.getRequestDispatcher("admin-phone.jsp").forward(request, response); 
	}*/
/*
private Map<String, List<Menu>> getMenu(String roleid,String moduleId)
	
	
	{
		
		System.out.println("roleid >>>"+roleid);
		System.out.println("moduleId >>>> "+moduleId);
		Menu m = new Menu();
		List<Menu> firstList=new ArrayList<Menu>();
		List<Menu> secondList=new ArrayList<Menu>();
		List<Menu> thirdList=new ArrayList<Menu>();
		List<Menu> fourthList=new ArrayList<Menu>();
	//	RestTemplate rt = new RestTemplate();
		//MultiValueMap<String, Object> mapInput = new LinkedMultiValueMap<String, Object>();		 
		Map<String,List<Menu>> resp = new HashMap<String,List<Menu>>();
		//String roleid="6";
		String delimiters="$";
		//String moduleId="1";
	
		try {	
			
			//Restful service 
			URL url = new URL(
				    "http://172.29.124.1:7010/etiqauam/getMenu/"+roleid+delimiters+moduleId);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");

				if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
				    + conn.getResponseCode());
				}

			
				InputStreamReader in = new InputStreamReader(conn.getInputStream());
	            BufferedReader br = new BufferedReader(in);
	            String output;
	            String output1;
	            
	            StringBuilder sb = new StringBuilder();
	            while ((output = br.readLine()) != null) {
	                System.out.println(output);
	                sb.append(output);
	               
	            }
	            String jsonString =sb.toString();
	            
	          
	          //getting whole json string
	     
	            
	        	ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	    		
	    		Gson gson = new Gson();
	    		resp = (Map<String,List<Menu>>) gson.fromJson(jsonString, resp.getClass());
	    	 
	    		m.setModuleId(Short.valueOf(moduleId));
	    		
	    		System.out.println("resp  " + resp +"\n");
	    		System.out.println("Module Id  : "+ m.getModuleId());
	    		System.out.println("-------------------------------");
	    		
	    		List<Menu> first_temp=resp.get("first");
	    		List<Menu> second_temp=resp.get("second");
	    		List<Menu> third_temp=resp.get("third");
	    		List<Menu> fourth_temp=resp.get("fourth");
	    		
	    	
	    		
	    		if(!first_temp.isEmpty()) {
	    			for (int i=0;i<first_temp.size();i++) {
	    				Menu pojo = mapper.convertValue(first_temp.get(i), Menu.class);
	    				firstList.add(pojo);
	    			}
	    		}
	    		if(!second_temp.isEmpty()) {
	    			for (int i=0;i<second_temp.size();i++) {
	    				Menu pojo = mapper.convertValue(second_temp.get(i), Menu.class);
	    				secondList.add(pojo);
	    			}
	    		}
	    		if(!third_temp.isEmpty()) {
	    			for (int i=0;i<third_temp.size();i++) {
	    				Menu pojo = mapper.convertValue(third_temp.get(i), Menu.class);
	    				thirdList.add(pojo);
	    			}
	    		}
	    		if(!fourth_temp.isEmpty()) {
	    			for (int i=0;i<fourth_temp.size();i++) {
	    				Menu pojo = mapper.convertValue(fourth_temp.get(i), Menu.class);
	    				fourthList.add(pojo);
	    			}
	    		}
	    
	    		
	    		System.out.println("firstList  " + firstList.get(0)); 
	    		//System.out.println("pojo  " + firstList.get(0).getUrl()); 
	    		Collections.sort(firstList, new Comparator<Menu>() {

					@Override
					public int compare(Menu o1, Menu o2) {
						// TODO Auto-generated method stub
						return o1.getId().compareTo(o2.getId());
					}
	    	       });
	    	      
	    		
	    		resp.put("1", firstList);
	    		resp.put("2", secondList);
	    		resp.put("3", thirdList);
	    		resp.put("4", fourthList);
	    		
	            conn.disconnect();
	            
  
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("no Connection");
		}
		return resp;
		
	}*/
/*public Map getMenuList(String id) throws SQLException {
	
	
	Map levels = new HashMap();
	ArrayList<Menu> menu_first_all = new ArrayList<Menu>();
	ArrayList<Menu> menu_second_all = new ArrayList<Menu>();
	ArrayList<Menu> menu_third_all = new ArrayList<Menu>();
	ArrayList<Menu> menu_fourth_all = new ArrayList<Menu>();
	
	int moduleId = 0;
	String roles = "";
	System.out.println(id + "id");
	String idparams[] = id.split("\\$");
	if (idparams.length == 2) {
		moduleId = Integer.parseInt(idparams[1]);
		roles = idparams[0];
		System.out.println(roles + "," + moduleId);
	//	map = getMenuList(roles, moduleId);
	}
	// String roles="1,10";
	MenuDao menuDao = new MenuDaoImpl();
	String[] params = roles.split(",");
	for (int i = 0; i < params.length; i++) {
		// System.out.println(params[i]+"role id");
		ArrayList<Menu> menus = new ArrayList<Menu>();
		ArrayList<Menu> menu_first = new ArrayList<Menu>();
		ArrayList<Menu> menu_second = new ArrayList<Menu>();
		ArrayList<Menu> menu_third = new ArrayList<Menu>();
		ArrayList<Menu> menu_fourth = new ArrayList<Menu>();
		menus = menuDao.getMenuList(moduleId, Integer.parseInt(params[i]));
		System.out.println(moduleId + "," + params[i] + "menu size:" + menus.size());

		// System.out.println("size "+menus.size());
		for (Menu single : menus) {
			//System.out.println("check parmission  1 :" + single.getPermission_str());
			if (single.getLevelId() == 1) {
				// String permission_str =
				// SecurityUtil.enciptString(single.getPermission_str());
				// single.setPermission_str(permission_str);
				menu_first.add(single);
				// break;
			}
		}
		for (Menu single : menus) {
			//System.out.println("check parmission  2 :" + single.getPermission_str());
			if (single.getLevelId() == 2) {
				// String permission_str =
				// SecurityUtil.enciptString(single.getPermission_str());
				// single.setPermission_str(permission_str);
				menu_second.add(single);
				// break;
			}
		}
		for (Menu single : menus) {
			//System.out.println("check parmission  3 :" + single.getPermission_str());
			if (single.getLevelId() == 3) {
				// String permission_str =
				// SecurityUtil.enciptString(single.getPermission_str());
				// single.setPermission_str(permission_str);
				menu_third.add(single);
				// break;
			}
		}
		for (Menu single : menus) {
			//System.out.println("check parmission  4 :" + single.getPermission_str());
			if (single.getLevelId() == 4) {
				// String permission_str =
				// SecurityUtil.enciptString(single.getPermission_str());
				// single.setPermission_str(permission_str);
				menu_fourth.add(single);
				// break;
			}
		}
		menu_first_all.addAll(menu_first);
		menu_second_all.addAll(menu_second);
		menu_third_all.addAll(menu_third);
		menu_fourth_all.addAll(menu_fourth);
	}
	System.out.println(menu_first_all.size() + "first level size before merge");
	for (int i = 0; i < menu_first_all.size(); i++) {
		System.out.println(
				"Menu Name  and ID " + menu_first_all.get(i).getName() + "," + menu_first_all.get(i).getId());

	}
	Set<Menu> set_first = new HashSet<Menu>(menu_first_all);
	Set<Menu> set_second = new HashSet<Menu>(menu_second_all);
	Set<Menu> set_third = new HashSet<Menu>(menu_third_all);
	Set<Menu> set_fourth = new HashSet<Menu>(menu_fourth_all);
	Iterator<Menu> iterator = set_first.iterator();
	while (iterator.hasNext()) {
		Menu setElement = iterator.next();
		System.out.println(setElement.getName() + "," + setElement.getId());
	}
	// Set myOrderedSet = new LinkedHashSet(set_first);
	System.out.println(set_first.size() + "first level size after merge");
	ArrayList<Menu> menu_first_final = new ArrayList<Menu>(set_first);
	ArrayList<Menu> menu_second_final = new ArrayList<Menu>(set_second);
	ArrayList<Menu> menu_third_final = new ArrayList<Menu>(set_third);
	ArrayList<Menu> menu_fourth_final = new ArrayList<Menu>(set_fourth);
	// sorting menu based on Id
	
	 * Collections.sort(menu_first_final, (Menu a, Menu b) ->
	 * a.getId().compareTo(b.getId())); Collections.sort(menu_second_final,
	 * (Menu a, Menu b) -> a.getId().compareTo(b.getId()));
	 * Collections.sort(menu_third_final, (Menu a, Menu b) ->
	 * a.getId().compareTo(b.getId())); Collections.sort(menu_fourth_final,
	 * (Menu a, Menu b) -> a.getId().compareTo(b.getId()));
	 
	levels.put("first", menu_first_final);
	levels.put("second", menu_second_final);
	levels.put("third", menu_third_final);
	levels.put("fourth", menu_fourth_final);
	return levels;
}*/
/*private Map<String, List<Menu>> getMenu(String roleid, String moduleId)
{
	System.out.println("roleid >>>" + roleid);
	System.out.println("moduleId >>>> " + moduleId);
	Menu m = new Menu();

	Map<String, List<Menu>> resp = new HashMap<String, List<Menu>>();	
	// String roleid="6";
	String delimiters = "$";
	// String moduleId="1";
	try {
		resp=getMenuList(roleid + delimiters + moduleId);
		System.out.println(resp);
		m.setModuleId(Short.valueOf(moduleId.toString()));
		System.out.println("resp  " + resp + "\n");
		System.out.println("Module Id  : " + m.getModuleId());
		System.out.println("-------------------------------");
		List<Menu> firstList = resp.get("first");
		List<Menu> secondList = resp.get("second");
		List<Menu> thirdList = resp.get("third");
		List<Menu> fourthList = resp.get("fourth");


		resp.put("1", firstList);
		resp.put("2", secondList);
		resp.put("3", thirdList);
		resp.put("4", fourthList);

	} catch (Exception e) {
		e.printStackTrace();
		System.out.println("no Connection");
	}
	return resp;
}*/


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/*protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		

		HttpSession session1 = request.getSession(false);
		if(session1!=null) {
			session1.invalidate();
		}						
	    
		String userName=request.getParameter("username");	
		
		String pwd=request.getParameter("password");	
		
		LDAPAttributesBean ldap =new LDAPAttributesBean();
		//ldap =LoginCPF.authenticateUser(userName,pwd);
		ldap.setValidated(1);
		if (!ldap.getValidated().equals(0))
		{
		    // Success
			
			
			
				// request.getSession(); // create session before logging in
			 //request.login( userName, pwd );
			  HttpSession session = request.getSession(true); // reuse existing
				// session if exist
				// or create one
				session.setAttribute("user", userName);
				//session.setAttribute("FullName", ldap.getFULNAME());
				session.setAttribute("FullName", userName);
				session.setAttribute("username", userName);
			//	session.setMaxInactiveInterval(30); // 30 seconds
				//request.getSession().setAttribute("userid",userName);
			  request.getRequestDispatcher("admin-phone.jsp").forward(request, response);
						
			  
			  
		}
	else
		{
		    // Fail
			// request.getRequestDispatcher("admin-login.jsp").forward(request, response);
				
		response.sendRedirect("admin-login.jsp?result=fail");
		}

	}
		

}
*/
    
    
    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session1 = request.getSession(false);
		if (session1 != null) {
			session1.invalidate();
		}

		String userName = request.getParameter("username");

		String pwd = request.getParameter("password");

		LDAPAttributesBean ldap = new LDAPAttributesBean();
		// ldap =LoginCPF.authenticateUser(userName,pwd);
		ldap.setValidated(1);
		if (!ldap.getValidated().equals(0)) {
			// Success

			// request.getSession(); // create session before logging in
			// request.login( userName, pwd );
			HttpSession session = request.getSession(true); // reuse existing
			// session if exist
			// or create one
			session.setAttribute("user", userName);
			// session.setAttribute("FullName", ldap.getFULNAME());
			session.setAttribute("FullName", userName);
			session.setAttribute("username", userName);
			// session.setMaxInactiveInterval(30); // 30 seconds
			// request.getSession().setAttribute("userid",userName);
			request.getRequestDispatcher("admin-phone.jsp").forward(request, response);

		} else {
			// Fail
			// request.getRequestDispatcher("admin-login.jsp").forward(request, response);

			response.sendRedirect("admin-login.jsp?result=fail");
		}

	}

}
