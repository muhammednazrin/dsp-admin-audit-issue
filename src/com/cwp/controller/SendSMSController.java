package com.cwp.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cwp.otc.ProceedOTcBean;
import com.cwp.sms.ProceedOTCCPF;
import com.cwp.sms.SMSDriver;

/**
 * Servlet implementation class SendSMSController
 */
@RequestMapping("cwp")
@WebServlet("/cwp/sendsms")
public class SendSMSController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SendSMSController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
        response.setHeader("Cache-control", "no-cache, no-store");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "-1");

        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type");
        response.setHeader("Access-Control-Max-Age", "86400");
		
		
		ProceedOTCCPF  proceedOTCCPF= new ProceedOTCCPF();
		//ProceedOTcBean crgb = new ProceedOTcBean();
		String fromName="Etiqa";
		String message ="etiqa";
		String trx_id="123";
		String icnumber = request.getParameter("icnumber");
		String mobile = request.getParameter("mobile");
		
		
	
		
		ProceedOTcBean crgb=proceedOTCCPF.SendOTC(icnumber, fromName, mobile, message, trx_id);
		
		 JSONObject json = new JSONObject();
		 try {
			json.put("res_code", "success");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 out.println(json.toString());
         
         out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
