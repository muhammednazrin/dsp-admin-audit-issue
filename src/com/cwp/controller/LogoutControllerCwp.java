package com.cwp.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
@WebServlet("/logoutCWP")
public class LogoutControllerCwp extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.setContentType("text/html");
		// PrintWriter out = response.getWriter();
		// out.println("thanq you!!, Your session was destroyed successfully!!");
		HttpSession session = request.getSession(false);
		// session.setAttribute("user", null);
		session.removeAttribute("user");
		
	    request.getSession().removeAttribute("FullName");
		request.getSession().invalidate();
	
		StringBuffer url = request.getRequestURL();
		String uri = request.getRequestURI();
		String ctx = request.getContextPath();
		String base = url.substring(0, url.length() - uri.length() + ctx.length());
		System.out.println(base);
		String redURL=base+"/"+"adminLogout";
		//session.getMaxInactiveInterval();
		//response.sendRedirect("logout.jsp");
		response.sendRedirect(redURL);
	}

}
