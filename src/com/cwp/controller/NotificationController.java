package com.cwp.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwp.admin.NotificationTM;
import com.cwp.dao.NotificationDAO;

public class NotificationController extends HttpServlet {
	 private static String INSERT_OR_EDIT = "/Notification.jsp";
	 private static String LIST_USER = "/NotificationList.jsp";
	 private NotificationDAO dao;
	  
	    public NotificationController() {
	        super();
	        dao = new NotificationDAO();
	    }

	    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        String forward="";
	        String action = request.getParameter("action");

	        if (action.equalsIgnoreCase("delete")){
	            int userId = Integer.parseInt(request.getParameter("NotifyId"));
	            dao.deleteNotification(userId);
	            forward = LIST_USER;
	            request.setAttribute("notifications", dao.getAllNotification());    
	        } else if (action.equalsIgnoreCase("edit")){
	            forward = INSERT_OR_EDIT;
	            int notificationId = Integer.parseInt(request.getParameter("NotifyId"));
	            NotificationTM notify = dao.getnotificationById(notificationId);
	            request.setAttribute("notifyinsert", notify);
	        } else if (action.equalsIgnoreCase("listUser")){
	            forward = LIST_USER;
	            request.setAttribute("notifications", dao.getAllNotification());
	        } else {
	            forward = INSERT_OR_EDIT;
	        }

	        RequestDispatcher view = request.getRequestDispatcher(forward);
	        view.forward(request, response);
	    }
	    
	    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

			   System.out.println("Inside Post");
	    	
	    	NotificationTM notify = new NotificationTM();
	    	notify.setNotifyDesc(request.getParameter("notifyDesc"));
	       // user.setLastName(request.getParameter("lastName"));
	        
	        String Notifyid = request.getParameter("NotifyId");
	        if(Notifyid == null || Notifyid.isEmpty())
	        {
			   System.out.println("Inside Post - INSERT");
	           
			   dao.addNotification(notify);
	        
	        }
	        else
	        {
	        	

	        	System.out.println("Inside Post - Update");
		           
	        	notify.setNotifyid(Integer.parseInt(Notifyid));
	            dao.updateNotification(notify);
	        }
	        
	        System.out.println("Inside Post - Ouside condition");
	           
	        RequestDispatcher view = request.getRequestDispatcher(LIST_USER);
	        request.setAttribute("notifications", dao.getAllNotification());
	        view.forward(request, response);
	    }

}
