package com.cwp.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cwp.admin.UserProfileTM;
import com.cwp.dao.UserProfileTMDAO;
import com.cwp.util.DBUtil;

/**
 * Servlet implementation class UserRegController
 */
@RequestMapping("cwp")
@WebServlet("/cwp/reguser")
public class UserRegController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserRegController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
        response.setHeader("Cache-control", "no-cache, no-store");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "-1");

        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type");
        response.setHeader("Access-Control-Max-Age", "86400");
		
		
		Connection connection = null;
		DBUtil database= new DBUtil();
		JSONObject json = new JSONObject();
		System.out.println(request.getParameter("update") + " insert "+request.getParameter("insert"));
		String update ="0";
		if(request.getParameter("update")!=null){
			update =request.getParameter("update");
		}
		String insert ="0";
		if(request.getParameter("insert")!=null){
			insert =request.getParameter("insert");
		}
	   if(update.equals("1")) {
		   System.out.println(request.getParameter("update") + " update ");
		   UserProfileTMDAO dao=new UserProfileTMDAO();
		  
		   UserProfileTM user =dao.updatePhoneByIC(request.getParameter("icno"), request.getParameter("phnumber"));
		   try {
			json.put("mobile", user.getMobile());
			json.put("icno", user.getIcNumber());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
         	
	   } else if(insert.equals("1")){
		   
		   System.out.println(request.getParameter("update") + " insert else"+request.getParameter("insert"));
	    PreparedStatement pstmt = null;
		String name = request.getParameter("name");
		String idtype = request.getParameter("idtype");
		String icnumber= request.getParameter("icnumber1");
		System.out.println(request.getParameter("icnumber1"));
		String mobilenumber = request.getParameter("mobilenumber");		
		String dob = request.getParameter("dob");
		String address = request.getParameter("address");
		String city= request.getParameter("city");
		String pcode= request.getParameter("pcode");
		String state = request.getParameter("state");
		String email = request.getParameter("email");
		String mincome = request.getParameter("mincome");
		String planguage= request.getParameter("planguage");
		String gender = request.getParameter("gender");
		String nationality = request.getParameter("nationality");
		String maritalstatus = request.getParameter("maritalstatus");
		String race = request.getParameter("race");
		String homenumbe = request.getParameter("homenumbe");
		String homenumber2 = request.getParameter("homenumber2");
		String edulevel = request.getParameter("edulevel");
		String staff= request.getParameter("staff");
		String religion = request.getParameter("religion");
		String childresn = request.getParameter("childresn");
		Date last_update_date= new Date();
		String response_code="0000";
		String trx_id="0000";
		int status=1;

		System.out.println(mobilenumber);

		
	
		//String str = "insert into USER_PROFILE_TM(NAME, IC_NUMBER, MOBILE,EMAIL_ADDRESS,STATUS) values(?, ?, ?,?,?)";
		
		String str = "insert into USER_PROFILE_TM(NAME,ID_TYPE,IC_NUMBER,DATE_OF_BIRTH,GENDER,NATIONALITY,MARITAL_STATUS,RACE,ADDRESS,CITY,STATE,POSTCODE,COUNTRY,HOME,MOBILE,EMAIL_ADDRESS,STATUS,LAST_UPDATE,RESPONSE_CODE) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		try {
			
			try {
				connection = database.getConnection();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			pstmt = connection.prepareStatement(str);
			pstmt.setString(1, name); 
			pstmt.setString(2, idtype); 
		    pstmt.setString(3, icnumber); // set input parameter 2
		    pstmt.setString(4, dob);
		    pstmt.setString(5, gender);
		    pstmt.setString(6, nationality);
		    pstmt.setString(7, maritalstatus);
		    pstmt.setString(8, race);
		    pstmt.setString(9, address);
		    pstmt.setString(10, city);
		    pstmt.setString(11, state);
		    pstmt.setString(12, pcode);
		    pstmt.setString(13, nationality);
		    pstmt.setString(14, homenumbe);
		    pstmt.setString(15, mobilenumber); // set input parameter 3
		    pstmt.setString(16, email);
		    pstmt.setInt(17, 0);
		    pstmt.setDate(18, new java.sql.Date(System.currentTimeMillis()));
		    pstmt.setString(19, "admin");
			pstmt.executeUpdate(); // execute insert statement
			pstmt.close();
			connection.close();
		}  catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	   }
		 try {
				json.put("res_code", "success");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
		 out.println(json.toString());
		 
		 
         
         out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
	}

}
