package com.cwp.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwp.admin.AccountInfoBean;
import com.cwp.admin.AccupdateBean;
import com.cwp.dao.AccupdateDAO;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("cwp")
@WebServlet("/cwp/CWPAccinfo")
public class CwpAccountupdateController extends HttpServlet {
	 private static String INSERT_OR_EDIT = "/Accupdate.jsp";
	 private static String LIST = "/AccupdateList.jsp";
	 private AccupdateDAO dao;
	  
	    public CwpAccountupdateController() {
	        super();
	        dao = new AccupdateDAO();
	    }

	    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        String forward="";
	        String action = request.getParameter("action");
	      
			if((action == null || action.isEmpty())){
				action ="list";
			}
			 System.out.println("action value : " + request.getParameter("action"));
	    //   String action = request.getParameter("action");

	        if (action.equalsIgnoreCase("delete")){
	          //  int accId = Integer.parseInt(request.getParameter("accId"));
	            long ID= Long.parseLong(request.getParameter("accId").trim());	
				
				System.out.println("Getting value : " + ID);
				AccupdateBean accupdate =new AccupdateBean();
				accupdate.setId(ID);
	            dao.cwpdeleteAccinfo(accupdate);
	            forward = LIST;
			     accupdate.setPolicyno("");
			     accupdate.setIc_number("");
			     accupdate.setUpdateby("");
			     
	            request.setAttribute("accountinfo", dao.queryfetchAccInfo(accupdate));    
	        } else if (action.equalsIgnoreCase("edit")){
	            forward = INSERT_OR_EDIT;
	           long ID= Long.parseLong(request.getParameter("accId").trim());
			   System.out.println("Getting value : " + ID);
			   AccupdateBean accupdate =new AccupdateBean();
			    accupdate.setId(ID);
			    accupdate=dao.queryfetchAccInfoByID(accupdate);
	            request.setAttribute("Accinfoinsert", accupdate);
	        } else if (action.equalsIgnoreCase("list")){
	            forward = LIST;
				   AccupdateBean accupdate =new AccupdateBean();
				   List<AccupdateBean> policyList=  new ArrayList<AccupdateBean>();
			     accupdate.setPolicyno("");
			     accupdate.setIc_number("");
			     accupdate.setUpdateby("");
			     policyList =dao.getPolicylist(accupdate);
			     request.setAttribute("policylist", policyList);
	            request.setAttribute("accountinfo", dao.queryfetchAccInfo(accupdate));
	        } else if (action.equalsIgnoreCase("search")){
	            forward = LIST;
				 String policy =request.getParameter("policyno").trim();
				 String icno =request.getParameter("icnumber").trim();
				 String updateby=request.getParameter("custname").trim();
				   AccupdateBean accupdate =new AccupdateBean();
			     accupdate.setPolicyno(policy);
			     accupdate.setIc_number(icno);
			     accupdate.setUpdateby(updateby);
	            request.setAttribute("accountinfo", dao.queryfetchAccInfo(accupdate));
	        }else if (action.equalsIgnoreCase("add")){
	            forward = INSERT_OR_EDIT;
	            AccupdateBean accupdate =new AccupdateBean();
	            List<AccupdateBean> policyList=  new ArrayList<AccupdateBean>();
	            policyList =dao.getPolicylist(accupdate);
			     request.setAttribute("policylist", policyList);
			  
	            
	        }else {
	        	 forward = LIST;
				   AccupdateBean accupdate =new AccupdateBean();
				   List<AccupdateBean> policyList=  new ArrayList<AccupdateBean>();
			     accupdate.setPolicyno("");
			     accupdate.setIc_number("");
			     accupdate.setUpdateby("");
			     policyList =dao.getPolicylist(accupdate);
			    
			     request.setAttribute("policylist", policyList);
	            request.setAttribute("accountinfo", dao.queryfetchAccInfo(accupdate));
	        }

	        RequestDispatcher view = request.getRequestDispatcher(forward);
	        view.forward(request, response);
	    }
	    
	    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

			   System.out.println("Inside Post"  +request.getParameter("action"));
	    	
	    	    AccupdateBean accupdate =new AccupdateBean();
	    	    List<AccupdateBean> policyList=  new ArrayList<AccupdateBean>();
			     policyList =dao.getPolicylist(accupdate);
			     request.setAttribute("policylist", policyList);
	    	    String action="";
	    	    if(request.getParameter("action")!=null){
	    	    	action =request.getParameter("action").trim();
    	    	}
	    	    if(action.equals("search")){
	    	    	   System.out.println("Inside Post"  +request.getParameter("action"));
	    	    	String policy ="";
	    	    	if(request.getParameter("policyno")!=null){
	    	    		policy =request.getParameter("policyno").trim();
	    	    	}
					 String icno ="";
					 if(request.getParameter("icnumber")!=null){
						 icno =request.getParameter("icnumber").trim();
		    	    	}
					 String updateby="";
					 if(request.getParameter("custname")!=null){
						 updateby =request.getParameter("custname").trim();
		    	    	}
				     accupdate.setPolicyno(policy);
				     accupdate.setIc_number(icno);
				     accupdate.setUpdateby(updateby);
				     request.setAttribute("policy",policy );
				     request.setAttribute("icno",icno );
				     request.setAttribute("updateby",updateby );
				    
		            request.setAttribute("accountinfo", dao.queryfetchAccInfo(accupdate));
	    	    } else {
	            String policy =request.getParameter("policyno").trim();
				String icno =request.getParameter("icnumber").trim();
				String bankname =request.getParameter("bankname").trim();
				String bankacc =request.getParameter("bankacc").trim();
				//String branchname =request.getParameter("branchname").trim();
				//String branchcode =request.getParameter("branchcode").trim();
				String branchname ="b";
				String branchcode ="123";
				String updateby =request.getParameter("custname").trim();
				String phno =request.getParameter("phno").trim();
				String email =request.getParameter("email").trim();
				System.out.println("email "+email);
				
			
				accupdate.setPolicyno(policy);
				accupdate.setIc_number(icno);
				accupdate.setBank_name(bankname);
				accupdate.setBranch_name(branchname);
				accupdate.setAcc_no(bankacc);
				accupdate.setBranch_code(branchcode);
				accupdate.setContactno(phno);
				accupdate.setEmail(email);
				accupdate.setUpdateby(updateby);
				
				accupdate = dao.queryUpdateAccno(accupdate);
				accupdate = dao.queryDSPUpdateAccno(accupdate);
	        
	    	  
	           
	      
			 accupdate.setPolicyno("");
			     accupdate.setIc_number("");
			     accupdate.setUpdateby("");
			     request.setAttribute("agentAddedMessage", policy +" data saved successfully!");
	        request.setAttribute("accountinfo", dao.queryfetchAccInfo(accupdate));
	    	    }
	        RequestDispatcher view = request.getRequestDispatcher(LIST);
	        view.forward(request, response);
	    }

}
