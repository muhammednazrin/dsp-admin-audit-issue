package com.cwp.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwp.dao.CPF_LOG_SummaryDetails_DAO;
import com.cwp.dao.CPF_LOG_SummaryDetails_DAO_Admin;
import com.cwp.dao.CPF_LOG_Summary_DAO;
import com.cwp.rpt.CPF_LOG_MessagesRpt;
import com.cwp.rpt.CPF_LOG_SummaryRpt;

@WebServlet("/getLogSummaryAdminDetailsData")
public class CPF_LOG_SummaryDetails_Admin_Controller extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		try{
			
		String TID = request.getParameter("TID");
		
		System.out.println("TID" + TID);
		
		request.setAttribute("TID", TID);
	
		List<CPF_LOG_MessagesRpt> myDataList = new CPF_LOG_SummaryDetails_DAO_Admin().getReportData(TID);
		request.setAttribute("myDataList", myDataList);
	
		System.out.println("request" + request);
		System.out.println("response" + response);	
		
		request.getRequestDispatcher("ViewLogSummaryDetailAdminRpt.jsp").forward(request, response);
		
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
	  }

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try{
			
			String TID = request.getParameter("TID");
			
			System.out.println("TID" + TID);
			
			request.setAttribute("Major_Transaction_Id", TID);
		
			List<CPF_LOG_MessagesRpt> myDataList = new CPF_LOG_SummaryDetails_DAO_Admin().getReportData(TID);
			request.setAttribute("myDataList", myDataList);
		
			System.out.println("request" + request);
			System.out.println("response" + response);	
			
			request.getRequestDispatcher("ViewLogSummaryDetailAdminRpt.jsp").forward(request, response);
			
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
			
	}
	
	
}
