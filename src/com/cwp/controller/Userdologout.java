package com.cwp.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cwp.util.DBUtil;
import com.cwp.util.ExcludeURLs;

@WebServlet("/Userdologout")
public class Userdologout extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
    public Userdologout() {
        super();
      
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost( request,  response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		Connection connection = null;
		DBUtil database= new DBUtil();
		 PreparedStatement pstmt = null;

if (session.getAttribute("logedUser") != null) {
	String userLogId = session.getAttribute("logedUser").toString();
	System.out.println("userLogId  cwp" + userLogId);
	if (!userLogId.isEmpty()) {
		long flag = 0;
				try {
				connection = database.getConnection();				
				String updateTableSQL ="UPDATE DSP_ADM_TBL_USER SET LAST_LOGIN = "+new Date()+",LOGOUT_FLAG="+flag+" WHERE ID ="+userLogId+"";
				pstmt = connection.prepareStatement(updateTableSQL);
				pstmt.executeUpdate(); // execute insert statement
				pstmt.close();
				connection.close();					
		} catch (Exception e) {					
			e.printStackTrace();
		}
		}
		}
		if (session.getAttribute("logedUser") != null) {
			/*if (session.getAttribute("userLogId") != null) {
				String userLogId = session.getAttribute("userLogId").toString();
				System.out.println("  new  data ::  " + userLogId);
				UserLog currentUser = new UserLog();
				currentUser.setLogoutTime(new Date());
				currentUser.setId(Short.parseShort(userLogId));
				userLogMapper.updateByUserLogId(currentUser);
			}*/
			session.setAttribute("userId", null);
			session.setAttribute("userLogId", null);
			session.setAttribute("logedUser", null);
			session.setAttribute("id", null);
			session.setAttribute("user", null);
			session.setAttribute("userLogid", null);
			session.setAttribute("FullName", null);
			session.setAttribute("pfNumber", null);

			System.out.println("Calling doLogout In Claim ");
			session.invalidate();
		}

//		return "redirect:" + ExcludeURLs.UAM_LOGIN_URL_LINK;
		response.sendRedirect(ExcludeURLs.UAM_LOGIN_URL_LINK);
		
	}

}
