package com.cwp.controller;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.cwp.util.DBUtil;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("cwp")
@WebServlet("/cwp/addsysuser")
public class SystemUserRegController {

    public SystemUserRegController() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
        response.setHeader("Cache-control", "no-cache, no-store");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "-1");

        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type");
        response.setHeader("Access-Control-Max-Age", "86400");
		
		
		Connection connection = null;
		DBUtil database= new DBUtil();
	   
	    PreparedStatement pstmt = null;
		String strusername = request.getParameter("txtUserName");
		
		Date createddate= new Date();
		String response_code="0000";
		String trx_id="0000";
		int status=1;

		String str = "insert into CWPADMIN_USER(USERNAME, CREATIONDATE, CREATEDBY, ISACTIVE) values(?,?,?,?)";
		
		try {
			
			try {
				connection = database.getConnection();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			pstmt = connection.prepareStatement(str);
			pstmt.setString(1, strusername); 
			pstmt.setString(2, createddate.toString()); 
		    pstmt.setString(3, "Sysadmin"); 
		    pstmt.setInt(4, 1);
			pstmt.executeUpdate(); // execute insert statement
			pstmt.close();
			connection.close();
		}  catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 JSONObject json = new JSONObject();
		 try {
			json.put("res_code", "success");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 out.println(json.toString());
         
         out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	
}
