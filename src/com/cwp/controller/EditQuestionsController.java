package com.cwp.controller;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwp.admin.NotificationTM;
import com.cwp.admin.Parameters;
import com.cwp.dao.NotificationDAO;
import com.cwp.dao.ParametersDAO;

	public class EditQuestionsController extends HttpServlet {
		
		 /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private static String INSERT_OR_EDIT = "/SecurityQuestion.jsp";
		    private static String LIST_USER = "/maintenanceSQ.jsp";
		    private ParametersDAO dao;
		  public EditQuestionsController() {
		        super();
		        dao = new ParametersDAO();
		    }

		    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		        String forward="";
		        String action = request.getParameter("action");

		      if (action.equalsIgnoreCase("edit")){
		            forward = INSERT_OR_EDIT;
		            int paramId = Integer.parseInt(request.getParameter("id"));
		            Parameters parameters = dao.getSecurityQuestionById(paramId);
		            request.setAttribute("parametersinsert", parameters);
		        } else if (action.equalsIgnoreCase("listUser")){
		            forward = LIST_USER;
		            request.setAttribute("parametersList", dao.getAllSecurityQuestions());
		        } else {
		            forward = INSERT_OR_EDIT;
		        }

		        RequestDispatcher view = request.getRequestDispatcher(forward);
		        view.forward(request, response);
		    }
		    
		    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		    	Parameters parameters = new Parameters();
		    	parameters.setVchdescription(request.getParameter("vchdescription"));
		    	parameters.setVchstatus(request.getParameter("vchstatus"));
		    	System.out.println("vchstatus Farooq Check JEE--- " + request.getParameter("vchstatus"));
		    	System.out.println("vchstatus Farooq Check JEE--- " + request.getParameter("vchdescription"));
		    /*if(request.getParameter("vchstatus").equalsIgnoreCase("Active")){
		    		parameters.setVchstatus("0");
		    	}else{
		    		parameters.setVchstatus("1");
		    	}
*/		    	
		       // user.setLastName(request.getParameter("lastName"));
		       
		     
		        String id = request.getParameter("id");
		        if(id == null || id.isEmpty())
		        {
		           dao.addSecurityQuestions(parameters);
		        }
		        else
		        {
		        	parameters.setId(Integer.parseInt(id));
		            dao.updateSecurityQuestion(parameters);
		        }
		        RequestDispatcher view = request.getRequestDispatcher(LIST_USER);
		        request.setAttribute("parametersList", dao.getAllSecurityQuestions());
		        //request.setAttribute("notifications", dao.getAllNotification());
		        view.forward(request, response);
		    }

	}



