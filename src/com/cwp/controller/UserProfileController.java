package com.cwp.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cwp.admin.UserProfileTM;
import com.cwp.dao.UserProfileTMDAO;

/**
 * Servlet implementation class UserProfileController
 */
@RequestMapping("cwp")
@WebServlet("/cwp/search")
public class UserProfileController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserProfileController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		UserProfileTMDAO dao=new UserProfileTMDAO();
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
        response.setHeader("Cache-control", "no-cache, no-store");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "-1");

        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type");
        response.setHeader("Access-Control-Max-Age", "86400");
       
	
	
	//response.setContentType("text/plain");  // Set content type of the response so that jQuery knows what it can expect.
   // response.setCharacterEncoding("UTF-8"); // You want world domination, huh?
           // Write response body.
     String uri=request.getRequestURI();
	 
	 String forward="";
     String icNumber = request.getParameter("icnumber");
     
  //   request.setAttribute("icNumber", icNumber);
     System.out.println(request.getParameter("icnumber"));
     JSONObject json = new JSONObject();
     try {
        //String uri=request.getRequestURI();
        //if(uri.equals("/etiqa/SaveEditRole"))

       // if (action.equalsIgnoreCase("search")){
            //String icnumber = "790816085098";
            UserProfileTM user=dao.getSearchByIC(icNumber, 123);
            System.out.println(user.getIcNumber());
            if (user.getIcNumber() == null || user.getIcNumber().isEmpty()) {
            	json.put("res_code", "fail");	
            } else {
            	json.put("res_code", "success");
            	json.put("mobile", user.getMobile());
            	json.put("home", user.getHome());
            	json.put("icno", user.getIcNumber());
            }
           
           
				
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
            //response.getWriter().write("{'errorMessage': 'Error'}");
            out.println(json.toString());
           
            out.close();
            
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
