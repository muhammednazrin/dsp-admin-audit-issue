package com.cwp.ldap;

import java.util.Date;

public class LDAPAttributesBean {
	private String CN;
	private Date last_logon;
	private Date last_password_update;
	private Integer wrong_password_attempt;
	private Integer validated;
	private String FULNAME;
	public String getCN() {
		return CN;
	}
	public void setCN(String cN) {
		CN = cN;
	}
	public Date getLast_logon() {
		return last_logon;
	}
	public void setLast_logon(Date last_logon) {
		this.last_logon = last_logon;
	}
	public Date getLast_password_update() {
		return last_password_update;
	}
	public void setLast_password_update(Date last_password_update) {
		this.last_password_update = last_password_update;
	}
	public Integer getWrong_password_attempt() {
		return wrong_password_attempt;
	}
	public void setWrong_password_attempt(Integer wrong_password_attempt) {
		this.wrong_password_attempt = wrong_password_attempt;
	}
	public Integer getValidated() {
		return validated;
	}
	public void setValidated(Integer validated) {
		this.validated = validated;
	}
	public String getFULNAME() {
		return FULNAME;
	}
	public void setFULNAME(String fULNAME) {
		FULNAME = fULNAME;
	}
	public Object getError_code() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
}
