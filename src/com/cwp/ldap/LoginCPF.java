package com.cwp.ldap;

import java.io.IOException;
import java.io.InputStream;
/*import java.rmi.server.UID;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date; */
import java.util.Hashtable;
//import java.util.List;
import java.util.Properties;



//import javax.jws.WebMethod;
//import javax.jws.WebParam;
//import javax.jws.WebResult;
import javax.jws.WebService;
//import javax.naming.Context;
//import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import com.cwp.admin.User;
import com.cwp.dao.UserValidation_DAO;



//import oracle.jdbc.OracleTypes;
//import oracle.jdbc.oracore.OracleType;


public class LoginCPF {
	private static Properties aduserInfoProp = new Properties();
	public static LDAPAttributesBean authenticateUser(String username, String password){
		LDAPAttributesBean ldapAttr = new LDAPAttributesBean();
		
		//String IPAddress = "ad1.etiqa.local";
		//String baseDN = "OU=Internal,OU=ETIQA USERS,DC=etiqa,DC=local";
		
		LoginCPF login =new LoginCPF();
		login.loadADuserProps();
		 String IPAddress = aduserInfoProp.getProperty("hosturl");
        String baseDN = aduserInfoProp.getProperty("AduserbaseDN");
        System.out.println(IPAddress+"#######"+baseDN);
		String CN = "CN="+username;
		String FQDN = CN+","+baseDN;
		SearchControls searchCtls = new SearchControls();
		Attributes attrs = new BasicAttributes(true);
        searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String returnedAtts[]={""};
        searchCtls.setReturningAttributes(returnedAtts);
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
        env.put("java.naming.security.authentication", "simple");
        env.put("java.naming.security.principal", FQDN);
        env.put("java.naming.security.credentials", password);
        //env.put("java.naming.provider.url", "ldap://" + IPAddress + ":389");
        env.put("java.naming.provider.url", "ldaps://" + IPAddress + ":636");
        try {
	        System.out.println("connected to ad..");
			LdapContext ctx = new InitialLdapContext(env, null);
			NamingEnumeration answer = ctx.search(baseDN, CN, searchCtls);
			while (answer.hasMoreElements()) 
            {
				UserValidation_DAO vdao = new UserValidation_DAO();
				
				User muser = new User();
				muser = vdao.getvalidate(username);
				
				if(muser.getIsVarified()==1){
					ldapAttr.setValidated(1);
					ldapAttr.setFULNAME(muser.getFullName());
				}else
				{
					System.out.println("User Validation DAO Issue");
					ldapAttr.setValidated(0);
				}
			
				    SearchResult sr = (SearchResult)answer.next();
					/*
                    String attr2[]={"lastLogonTimestamp","badPwdCount"};
                    attrs = ctx.getAttributes(sr.getName()+","+baseDN,attr2);
                    if (attrs.get("lastLogonTimestamp") != null){
                    	long date = Long.parseLong((String) attrs.get("lastLogonTimestamp").get());
                    	long llastLogonAdjust = 11644473600000L;
                    	ldapAttr.setLast_logon(new Date(date/10000-llastLogonAdjust));
                    	ldapAttr.setCN(CN);
                    }
                	if (attrs.get("badPwdCount") != null){
                    	ldapAttr.setWrong_password_attempt(Integer.parseInt((String)attrs.get("badPwdCount").get()));
                    }
                    */
                    	
            }
			
			ctx.close();
			
        } catch (NamingException e) {
			// TODO Auto-generated catch block
			ldapAttr.setValidated(0);
			System.out.println("Message is " + e.getMessage());
			e.printStackTrace();
		}
		return ldapAttr;
	}

	 public void loadADuserProps() {
		 InputStream infoad = this.getClass().getClassLoader().getResourceAsStream("com/cwp/ldap/adconfig.properties");
		 try {
			 aduserInfoProp.load(infoad);
		 } catch (IOException e) {
			e.printStackTrace();
		 }
	 }
	//public static void main(String args[]){
		//UID uid = new UID();
		//LoginCPF login =new LoginCPF();
		//login.loadADuserProps();
		//LDAPAttributesBean ldap = authenticateUser("2786","Etiqa2016!");
		
		//System.out.println(ldap.getValidated());
//		System.out.println(ldap.getLast_logon());
//		System.out.println(ldap.getWrong_password_attempt());
 
		/*
		 * DB SP Checking 
		 *
		 *
		LoginCPF lpc = new LoginCPF();
		LoginCPFBean mdmInfo = new LoginCPFBean();
		mdmInfo.setIc_number("790816085098");
		mdmInfo.setEmail_address("htanto86@gmail.com");
		mdmInfo.setLast_update(new Date());
		mdmInfo.setName("Hendra Tan");
		mdmInfo.setId_type("KTP");
		mdmInfo.setDate_of_birth("29 Aug 1986");
		mdmInfo.setGender("male");
		mdmInfo.setNationality("Indonesian");
		mdmInfo.setMarital_status("Married");
		mdmInfo.setRace("Chinesse");
		mdmInfo.setAddress("Apart Medit 2");
		mdmInfo.setCity("JKT");
		mdmInfo.setState("Jabar");
		mdmInfo.setPostcode("11323");
		mdmInfo.setCountry("Indonesia");
		mdmInfo.setHome("asdf");
		mdmInfo.setMobile("asdf");

		LoginCPFBean lcbean = lpc.queryDatabaseDashboard(mdmInfo);
		System.out.println(lcbean.getMobile());

/*		LoginCPF login = new LoginCPF();
		LoginCPFBean lcb = new LoginCPFBean();
		login.login("farooq@absecmy.com","Absec@2016", "testing1!");
		System.out.println(lcb.getError_code());
		*/
		
/*		LoginCPFBean lcb = new LoginCPFBean();
		lcb.setTrx_id("123");
		lcb = getMDMUserProfile("790816085098", lcb);
		System.out.println(lcb.getName());
		System.out.println(lcb.getCity());
/*		Get_Customer_Profile_Bean CPB  = new Get_Customer_Profile_Bean();
		Get_ProfileServiceProxy p = new Get_ProfileServiceProxy();
		try {
			CPB = p.getprofile("790816085098","3232");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(CPB.getRescode());
*/		
	//}
}