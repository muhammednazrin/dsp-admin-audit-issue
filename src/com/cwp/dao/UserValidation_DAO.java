package com.cwp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;

import com.cwp.admin.User;
import com.cwp.util.ConnectionFactory;
import com.cwp.util.DBUtil;

public class UserValidation_DAO {

	private static DBUtil db1;
	private static Connection connection;
	
	public User getvalidate(String username) {

		ResultSet rs = null;
		Statement statement = null;
		User muser = new User();
		try {
			db1=new DBUtil();
			connection = db1.getConnection();
			PreparedStatement preparedStatement = connection
					.prepareStatement("select * from CWPADMIN_USERS where username=? and ISACTIVE=1");
			preparedStatement.setString(1, username);
			rs = preparedStatement.executeQuery();
			
			if (rs.next()) {
				muser.setIsVarified(1);
				
				muser.setFullName(rs.getString("FULLNAME"));
				
			}else
			{
				muser.setIsVarified(0);
			}
		}
		catch(Exception e){
			e.printStackTrace(); 
		}
		finally {
			if (rs != null) { try { rs.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (statement != null) { try { statement.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (connection != null) { try { connection.close(); } catch (SQLException e) { e.printStackTrace(); } }
			
		}
		return muser;
	}
	
	//Added By Pramaiyan 
	public User getvalidateByUserID(String userId) {
	Connection connection = null;
	   	connection = ConnectionFactory.getConnection();
	   	
	ResultSet rs = null;
	Statement statement = null;
	User muser = new User();
	try {
	//db1=new DBUtil();
	//connection = db1.getConnection();
	PreparedStatement preparedStatement = connection
	.prepareStatement("select * from dsp_adm_tbl_user where ID=? and status=1");
	preparedStatement.setString(1, userId);
	rs = preparedStatement.executeQuery();
     System.out.println("connection");
	if (rs.next()) {
	muser.setIsVarified(1);
	muser.setFullName(rs.getString("NAME"));
	muser.setUsername(rs.getString("PF_NUMBER"));
	muser.setID(rs.getInt("ID"));
	System.out.println("connection1");

	}else
	{
	muser.setIsVarified(0);
	}
	}
	catch(Exception e){
	e.printStackTrace(); 
	}
	finally {
	if (rs != null) { try { rs.close(); } catch (SQLException e) { e.printStackTrace(); } }
	           if (statement != null) { try { statement.close(); } catch (SQLException e) { e.printStackTrace(); } }
	           if (connection != null) { try { connection.close(); } catch (SQLException e) { e.printStackTrace(); } }

	}
	return muser;
	}
	
	public static void main(String args [])
	{
		UserValidation_DAO vdao = new UserValidation_DAO();
		 User muser = new User();
	    muser =	vdao.getvalidate("00075676");
		
	    System.out.println(muser.getFullName());
	
		
	}
	
}
