package com.cwp.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.cwp.rpt.UserRoleRpt;
import com.cwp.util.DBUtil;

import oracle.jdbc.OracleTypes;

public class UserRoleReport_DAO {

	private static DBUtil db1;
	private static Connection connection;

	public List<UserRoleRpt> getReportData(String fromdt, String todt) {
		List<UserRoleRpt> myDataList = new ArrayList<UserRoleRpt>();
		ResultSet resultSet = null;
		Statement statement = null;

		try {

			db1 = new DBUtil();
			connection = db1.getConnection();

			CallableStatement cstmt = connection.prepareCall("{call GET_User_Roles_Rpt(?,?,?)}");
			cstmt.setString(1, fromdt);
			cstmt.setString(2, todt);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);

			cstmt.execute();
			resultSet = (ResultSet) cstmt.getObject(3);
			while (resultSet.next()) {

				UserRoleRpt reportData = new UserRoleRpt();

				reportData.setFullName(resultSet.getString("FULLNAME"));
				reportData.setUserName(resultSet.getString("USERNAME"));
				reportData.setRoleName(resultSet.getString("ROLENAME"));
				
			    //String adate = resultSet.getString("ASSIGNDATE");
				/*if(adate!=null && adate!="" && adate.equalsIgnoreCase("null") == false)
				{
				String[] tokens = adate.split(".");
				reportData.setAssignDate(tokens[0]);
				System.out.println(tokens[0] + " "  + tokens);
				}else
				{
					reportData.setAssignDate(adate);
				}
				*/
				
				reportData.setAssignDate(resultSet.getString("ASSIGNDATE"));
				reportData.setAssignedBy(resultSet.getString("ASSIGNEDBY"));
				reportData.setIsActive(resultSet.getString("ISACTIVE"));
				
				myDataList.add(reportData);

		   }
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return myDataList;

	}

	public static void main(String args[]) {
		
		UserRoleReport_DAO rdao = new UserRoleReport_DAO();
		List<UserRoleRpt> releaseDataList = rdao.getReportData("2011-08-01", "2019-08-02");
		Iterator itr = releaseDataList.iterator();

		while (itr.hasNext()) {
			
			UserRoleRpt rd = (UserRoleRpt) itr.next();
			
		
		}
	
	}
}
