package com.cwp.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;

import com.cwp.rpt.UserActivityRpt;
import com.cwp.rpt.UserRegRpt;
import com.cwp.util.DBUtil;

import oracle.jdbc.OracleTypes;

public class UserLastActivityRpt_DAO {

	private static DBUtil db1;
	private static Connection connection;

	public List<UserActivityRpt> getUserReportData(String fromdt, String todt) {

		List<UserActivityRpt> UsermyDataList = new ArrayList<UserActivityRpt>();
		ResultSet resultSet = null;
		Statement statement = null;

		try {

			db1 = new DBUtil();
			connection = db1.getConnection();

			CallableStatement cstmt = connection.prepareCall("{Call GET_User_Activity_Rpt(?,?,?)}");

			cstmt.setString(1, fromdt);
			cstmt.setString(2, todt);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();

			resultSet = (ResultSet) cstmt.getObject(3);

			while (resultSet.next()) {

				UserActivityRpt reportData = new UserActivityRpt();

				reportData.setCustomerName(resultSet.getString("NAME"));
				reportData.setIC_NUMBER(resultSet.getString("IC_NUMBER"));

				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				Date strLOGINDATETIME = null;
				Date strLOGOUTDATETIME = null;

				if (resultSet.getString("LOGINDATETIME") != null
						&& resultSet.getString("LOGINDATETIME").equalsIgnoreCase("null") == false
						&& resultSet.getString("LOGOUTDATETIME") != null
						&& resultSet.getString("LOGOUTDATETIME").equalsIgnoreCase("null") == false) {

					String temp_strLOGINDATETIMEa = resultSet.getString("LOGINDATETIME").replace(".", "@");
					String temp_strLOGINDATETIME[] = temp_strLOGINDATETIMEa.split("@");

					System.out.println(temp_strLOGINDATETIME[0] + "" + temp_strLOGINDATETIME[0]);

					strLOGINDATETIME = (Date) format.parse(temp_strLOGINDATETIME[0]);

					String temp_strLOGOUTDATETIMEa = resultSet.getString("LOGOUTDATETIME").replace(".", "@");
					String temp_strLOGOUTDATETIME[] = temp_strLOGOUTDATETIMEa.split("@");
					System.out.println(temp_strLOGOUTDATETIME[0] + "  " + temp_strLOGOUTDATETIME[0]);
					strLOGOUTDATETIME = (Date) format.parse(temp_strLOGOUTDATETIME[0]);

					// long diff = strLOGINDATETIME.getTime() -
					// strLOGOUTDATETIME.getTime();
					long diff = strLOGOUTDATETIME.getTime() - strLOGINDATETIME.getTime();

					long diffSeconds = diff / 1000 % 60;
					long diffMinutes = diff / (60 * 1000) % 60;
					long diffHours = diff / (60 * 60 * 1000) % 24;
					long diffDays = diff / (24 * 60 * 60 * 1000);

					reportData.setDURATION(diffHours + ":" + diffMinutes + ":" + diffSeconds);

					if (strLOGINDATETIME.after(strLOGOUTDATETIME)) {
						reportData.setLOGOUTDATETIME("");
						reportData.setDURATION("");
					} else {
						reportData.setLOGOUTDATETIME(strLOGOUTDATETIME.toString());
					}
					reportData.setLOGINDATETIME(strLOGINDATETIME.toString());

				} else {
					reportData.setDURATION("");
				}

				if (resultSet.getString("LOGINDATETIME") != null
						&& resultSet.getString("LOGINDATETIME").equalsIgnoreCase("null") == false) {
					String temp_strLOGINDATETIMEa = resultSet.getString("LOGINDATETIME").replace(".", "@");
					String temp_strLOGINDATETIME[] = temp_strLOGINDATETIMEa.split("@");
					reportData.setLOGINDATETIME(temp_strLOGINDATETIME[0]);
				} else {
					reportData.setLOGINDATETIME("");
				}

				if (resultSet.getString("LOGOUTDATETIME") != null
						&& resultSet.getString("LOGOUTDATETIME").equalsIgnoreCase("null") == false) {
					String temp_strLOGOUTDATETIMEa = resultSet.getString("LOGOUTDATETIME").replace(".", "@");
					String temp_strLOGOUTDATETIME[] = temp_strLOGOUTDATETIMEa.split("@");
					reportData.setLOGOUTDATETIME(temp_strLOGOUTDATETIME[0]);
				} else {
					reportData.setLOGOUTDATETIME("");
				}

				if (resultSet.getString("LOGIN_FLAG").equals("0")) {
					reportData.setLOGIN_FLAG("Offline");
				} else {
					reportData.setLOGIN_FLAG("Online");
				}
				UsermyDataList.add(reportData);
			}
		} catch (Exception e) {
			System.out.println("ERROR ON CLICK");
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return UsermyDataList;

	}

	public static void main(String args[]) {

		try {

			UserLastActivityRpt_DAO rdao = new UserLastActivityRpt_DAO();
			List<UserActivityRpt> releaseDataList = rdao.getUserReportData("2015-08-01", "2016-08-02");
			Iterator itr = releaseDataList.iterator();

			while (itr.hasNext()) {
				UserActivityRpt rd = (UserActivityRpt) itr.next();
				System.out.println("Results: " + rd.getCustomerName());
				System.out.println(
						"Results:" + rd.getIC_NUMBER() + " " + rd.getLOGINDATETIME() + " " + rd.getLOGOUTDATETIME());
			}

		} catch (Exception ex) {
			System.out.println("Error" + ex.getMessage());
		}
	}

}
