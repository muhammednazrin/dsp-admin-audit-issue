package com.cwp.dao;
import java.sql.SQLException;
import java.util.ArrayList;

import com.cwp.pojo.Menu;
import com.cwp.pojo.RoleModule;

public interface MenuDao {
	public ArrayList<Menu> getMenuListCWP(int module_id,int role_id) throws SQLException;
	public ArrayList<RoleModule> getRoleListByUser(int userId,int moduleId) throws SQLException;
	

}
