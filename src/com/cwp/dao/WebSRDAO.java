package com.cwp.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.cwp.admin.ServiceRequestBO;
import com.cwp.admin.UserProfileTM;
import com.cwp.util.DBUtil;

import oracle.jdbc.OracleTypes;

public class WebSRDAO {
	private static Connection connection;

	public List<ServiceRequestBO> getServiceRequestList(String fromDate, String toDate) {

		List<ServiceRequestBO> list  = new ArrayList<ServiceRequestBO>();
		ResultSet resultSet = null;
		Statement statement = null;

		try {
			connection = DBUtil.getConnection();
			CallableStatement cstmt = connection.prepareCall("{Call GET_WEB_SR_INFO_RPT(?,?,?)}");
			cstmt.setString(1, fromDate);
			cstmt.setString(2, toDate);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.executeUpdate();
			resultSet = (ResultSet) cstmt.getObject(3);
			
			while (resultSet.next()) {
				ServiceRequestBO reportData = new ServiceRequestBO();
				//reportData.reinitializeClass();
				reportData.setCustomerName(resultSet.getString("NAME"));
				reportData.setIcNumber(resultSet.getString("IC_NUMBER"));
				reportData.setEmailAddress(resultSet.getString("EMAIL_ADDRESS"));
				reportData.setMobileNumber(resultSet.getString("MOBILE"));
				reportData.setCategory(resultSet.getString("CATEGORY"));
				reportData.setSubCategory(resultSet.getString("SUBCATEGORY"));
				reportData.setServiceDate(resultSet.getString("SERVICECREATEDATE"));
				System.out.println("CATEGORY:" + hasColumn(resultSet, "CATEGORY"));
				printColumnName(resultSet);
				list.add(reportData);
			}
		} catch (Exception e) {
			System.out.println("ERROR ON CLICK");
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
	
	public List<ServiceRequestBO> getServiceCategory(String fromDate, String toDate, String cat) {

		List<ServiceRequestBO> list  = new ArrayList<ServiceRequestBO>();
		ResultSet resultSet = null;
		Statement statement = null;

		try {
			connection = DBUtil.getConnection();
			CallableStatement cstmt = connection.prepareCall("{Call GET_SR_BY_CATEGORY_Rpt(?,?,?,?)}");
			cstmt.setString(1, fromDate);
			cstmt.setString(2, toDate);
			cstmt.setString(3, cat);
			System.out.println(cat);
			cstmt.registerOutParameter(4, OracleTypes.CURSOR);
			cstmt.executeUpdate();
			resultSet = (ResultSet) cstmt.getObject(4);
			
			while (resultSet.next()) {
				ServiceRequestBO reportData = new ServiceRequestBO();
				reportData.setCustomerName(resultSet.getString("NAME"));
				reportData.setIcNumber(resultSet.getString("IC_NUMBER"));
				reportData.setEmailAddress(resultSet.getString("EMAIL_ADDRESS"));
				reportData.setMobileNumber(resultSet.getString("MOBILE"));
				reportData.setCategory(resultSet.getString("CATEGORY"));
				reportData.setSubCategory(resultSet.getString("SUBCATEGORY"));
				reportData.setServiceDate(resultSet.getString("SERVICECREATEDATE"));
				System.out.println("CATEGORY:" + hasColumn(resultSet, "CATEGORY"));
				printColumnName(resultSet);
				list.add(reportData);
			}
		} catch (Exception e) {
			System.out.println("ERROR ON CLICK");
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	private boolean hasColumn(ResultSet rs, String columnName) throws SQLException {
	    ResultSetMetaData rsmd = rs.getMetaData();
	    int columns = rsmd.getColumnCount();
	    for (int x = 1; x <= columns; x++) {
	        if (columnName.equals(rsmd.getColumnName(x))) {
	            return true;
	        }
	    }
	    return false;
	}
	
	private void printColumnName(ResultSet rs) throws SQLException {
	    ResultSetMetaData rsmd = rs.getMetaData();
	    int columns = rsmd.getColumnCount();
	    for (int x = 1; x <= columns; x++) {
	        System.out.println(x + ". " + rsmd.getColumnName(x));
	    }
	}

	public static void main(String args[]) {
		try {
			WebSRDAO rdao = new WebSRDAO();
			List<ServiceRequestBO> releaseDataList = rdao.getServiceRequestList("2015-10-01", "2016-10-26");
			int i = 0;
			for (ServiceRequestBO sr : releaseDataList) {
				i++;
			System.out.println(i + ". " + sr.toString());
			}

		} catch (Exception ex) {
			System.out.println("Error" + ex.getMessage());
		}
	}

}
