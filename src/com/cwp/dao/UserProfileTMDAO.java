package com.cwp.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.cwp.admin.UserProfileTM;
import com.cwp.util.DBUtil;

import oracle.jdbc.OracleTypes;

public class UserProfileTMDAO {
	
	
	   // private Connection connection;
/*
	    public UserProfileTMDAO() {
	    	Connection connection = null;
	    	try {
	    		
				connection = DBUtil.getConnection();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }*/


	    public UserProfileTM getSearchByIC(String ic, int trx) {
	    	
	    	Connection connection = null;
	    	UserProfileTM user = new UserProfileTM();
		     
	      ResultSet rs = null;
	        try {
	        	DBUtil database= new DBUtil();
	    	    connection = DBUtil.getConnection();
	    	    
	    	    PreparedStatement preparedStatement = connection.
	            prepareStatement("select * from USER_PROFILE_TM WHERE IC_NUMBER=?");
	            preparedStatement.setString(1, ic);
	             rs = preparedStatement.executeQuery();
	    	    
	    	    //   CallableStatement cstmt = connection.prepareCall("{call USER_PROFILE_INFO_BYICNO(?,?)}");		           
	            // cstmt.setString(1, user.getIcNumber());
	            //cstmt.registerOutParameter(2, OracleTypes.CURSOR);
	           
	          //  cstmt.execute();
	            //rs =(ResultSet)cstmt.getObject (2);    
	             System.out.println("icnumber validate");
	            String errmsg ="";
	            if (rs.next()) {
	            	
	            	 System.out.println("icnumber validate test1");
	            	user.setError_message("Success");
	             	user.setError_message("User ID is not Exist");
	             	user.setIcNumber(rs.getString("IC_NUMBER"));
	             	user.setMobile(rs.getString("MOBILE"));
	             	user.setHome(rs.getString("HOME"));
	             	
	             	
	            } else {
	           	 System.out.println("icnumber validate test else");
	            	user.setError_code("D000");
	            	user.setError_code("D0000");
	            
	            }
	    	
	        } catch (Exception e) {
	        	        	
	            e.printStackTrace();
	            
	        }finally {
	                  //cstmt.close();
	                  try {
	                        if(connection!=null)connection.close();
	                  } catch (SQLException e1) {
	                        // TODO Auto-generated catch block
	                        e1.printStackTrace();
	                  }
	                  connection = null;
	                  try {
	                        if(rs!=null)rs.close();
	                       
	                  } catch (SQLException e) {
	                        // TODO Auto-generated catch block
	                        e.printStackTrace();
	                  }
	              
	      } 
	        
			return user;
		
	    }
	    
	    public UserProfileTM updatePhoneByIC(String ic, String mobile) {
			ResultSet rs = null;
			Connection connection = null;
			PreparedStatement preparedStatement =null;
			UserProfileTM user = new UserProfileTM();
			try {
				DBUtil database= new DBUtil();
	    	    connection = DBUtil.getConnection();
				
				preparedStatement = connection
						.prepareStatement("update USER_PROFILE_TM set MOBILE=?,HOME=? where IC_NUMBER=?");
				// Parameters start with 1
				preparedStatement.setString(1, mobile);
				preparedStatement.setString(2, mobile);
				preparedStatement.setString(3, ic);
				preparedStatement.executeUpdate();
				preparedStatement.close();
				user.setIcNumber(ic);
             	user.setMobile(mobile);

			}
			catch(Exception e){
			    try {
					preparedStatement.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				e.printStackTrace(); 
			}
			finally {
				if (rs != null) { try { rs.close(); } catch (SQLException e) { 
					System.out.println("update Error");
					e.printStackTrace(); } }
	            if (preparedStatement!= null) { try { preparedStatement.close(); } catch (SQLException e) { e.printStackTrace(); } }
	            if (connection != null) { try { connection.close(); } catch (SQLException e) { e.printStackTrace(); } }
				
			}
			return user;
		}
	    
}