package com.cwp.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.cwp.rpt.CPF_LOG_MessagesRpt;
import com.cwp.util.DBUtil;

import oracle.jdbc.OracleTypes;

public class CPF_LOG_Messages_DAO {

	private static DBUtil db1;
	private static Connection connection;

	public List<CPF_LOG_MessagesRpt> getReportData(String fromdt, String todt) {
		List<CPF_LOG_MessagesRpt> myDataList = new ArrayList<CPF_LOG_MessagesRpt>();
		ResultSet resultSet = null;
		Statement statement = null;

		try {

			db1 = new DBUtil();
			connection = db1.getConnection();

			CallableStatement cstmt = connection.prepareCall("{call GET_LOG_Messages_INFO(?,?,?)}");
			cstmt.setString(1, fromdt);
			cstmt.setString(2, todt);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);

			cstmt.execute();
			resultSet = (ResultSet) cstmt.getObject(3);
			while (resultSet.next()) {

				CPF_LOG_MessagesRpt reportData = new CPF_LOG_MessagesRpt();

				reportData.setMessage_ID(resultSet.getString("Message_ID"));
				reportData.setMajor_Transaction_Id(resultSet.getString("Major_Transaction_Id"));
				reportData.setMessage_Tracking_Id("Message_Tracking_Id");
				reportData.setService_Name(resultSet.getString("Service_Name"));
				reportData.setLogging_Point(resultSet.getString("Logging_Point"));
				reportData.setDatetime(resultSet.getString("Datetime"));
				reportData.setPay_Load(resultSet.getString("Pay_Load"));

				myDataList.add(reportData);

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return myDataList;

	}

	public static void main(String args[]) {
		try {
			CPF_LOG_Messages_DAO rdao = new CPF_LOG_Messages_DAO();
			List<CPF_LOG_MessagesRpt> releaseDataList = rdao.getReportData("2010-08-01", "2016-12-02");
			Iterator itr = releaseDataList.iterator();

			System.out.println("START");

			while (itr.hasNext()) {
				CPF_LOG_MessagesRpt rd = (CPF_LOG_MessagesRpt) itr.next();
				System.out.println("Message_ID : " + rd.getMessage_ID() + " Major_Transaction_ID : "
						+ rd.getMajor_Transaction_Id());

				System.out.println("Message_Tracking_Id : " + rd.getMessage_Tracking_Id() + " SERVICE_NAME : "
						+ rd.getService_Name());

			}

			System.out.println("END");

		} catch (Exception ex) {

			ex.printStackTrace();

		}
	}

}
