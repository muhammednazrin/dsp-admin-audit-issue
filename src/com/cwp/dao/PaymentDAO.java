package com.cwp.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.cwp.admin.Payment;
import com.cwp.util.DBUtil;

import oracle.jdbc.OracleTypes;

public class PaymentDAO {
	private static Connection connection;

	public List<Payment> getPaymentList(String fromDate, String toDate) {

		List<Payment> list  = new ArrayList<Payment>();
		ResultSet resultSet = null;
		Statement statement = null;

		try {
			connection = DBUtil.getConnection();
			CallableStatement cstmt = connection.prepareCall("{Call GET_PAYMENT_Rpt(?,?,?)}");
			cstmt.setString(1, fromDate);
			cstmt.setString(2, toDate);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.executeUpdate();
			resultSet = (ResultSet) cstmt.getObject(3);
			
			while (resultSet.next()) {
				Payment reportData = new Payment();	
				reportData.setCustomerName(resultSet.getString("NAME"));
				reportData.setIcNumber(resultSet.getString("IC_NUMBER"));
				//reportData.setEmailAddress(resultSet.getString("EMAIL_ADDRESS"));
				reportData.setMobileNumber(resultSet.getString("MOBILE"));
				reportData.setPolicyNumber(resultSet.getString("POLICYNO"));
				reportData.setPaymentDate(resultSet.getString("PAID_DATETIME"));
				reportData.setPaymentAmount(resultSet.getString("AMOUNT"));				
				reportData.setPaymentStatus(resultSet.getString("PMNT_STATUS"));
								
				list.add(reportData);
			}
		} catch (Exception e) {
			System.out.println("ERROR ON CLICK");
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
	

}
