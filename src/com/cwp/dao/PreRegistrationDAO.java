package com.cwp.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.cwp.rpt.PreRegistrationBean;
import com.cwp.rpt.ValidationFailedRpt;
import com.cwp.util.DBUtil;

import oracle.jdbc.OracleTypes;

public class PreRegistrationDAO {

	private static DBUtil db1;
	private static Connection connection;
	
	public List<PreRegistrationBean> getReportData(String fromdt, String todt)
	{
		List<PreRegistrationBean> myDataList = new ArrayList<PreRegistrationBean>();
		ResultSet resultSet = null;
		Statement statement = null;
		
		try{
	
			db1 = new DBUtil();
			connection = db1.getConnection();
			
			/* PreparedStatement preparedStatement = connection.
			 prepareStatement("select * from USER_PROFILE_LOG WHERE CREATEDDATE>='" + fromdt + "' AND CREATEDDATE<='" + todt + "'");
			
			resultSet = preparedStatement.executeQuery();
			*/
			
			  CallableStatement cstmt = connection.prepareCall("{call GET_Pre_Registration_INFO(?,?,?)}");		           
	             cstmt.setString(1, fromdt);
	             cstmt.setString(2, todt);
	             cstmt.registerOutParameter(3, OracleTypes.CURSOR);
	           
	            cstmt.execute();
	            resultSet =(ResultSet)cstmt.getObject (3);      
	            
			while(resultSet.next())
			{
				
				PreRegistrationBean reportData = new PreRegistrationBean();
				
				reportData.setIcnumber(resultSet.getString("ICNUMBER"));
				reportData.setReg_status_Code(resultSet.getString("REGISTRATION_STATUS_CODE"));
				reportData.setSend_email_Status_code(resultSet.getString("SENDEMAIL_STATUS_CODE"));
				reportData.setSend_otc_status_code(resultSet.getString("SEND_OTC_STATUS_CODE"));
				reportData.setDatetime(resultSet.getString("DATETIME"));	

				myDataList.add(reportData);
				
			}
		}
		catch(Exception e){
			e.printStackTrace(); 
		}
		finally 
		{
			if (resultSet != null) { try { resultSet.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (statement != null) { try { statement.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (connection != null) { try { connection.close(); } catch (SQLException e) { e.printStackTrace(); } }
		}
		
		return myDataList;
	
	}
	
	public static void main(String args [])
	{
		ValidationFailedRptDAO rdao = new ValidationFailedRptDAO();
		List<ValidationFailedRpt> releaseDataList = rdao.getReportData("2016-08-01","2016-08-02");
		Iterator itr = releaseDataList.iterator();
		
		while(itr.hasNext())
		{
		
			ValidationFailedRpt rd = (ValidationFailedRpt) itr.next();

			System.out.println(" " + rd.getIdNumber()+ rd.getLastdate());
		
		}
		
	}
	
}
