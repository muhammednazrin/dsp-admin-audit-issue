package com.cwp.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.cwp.rpt.CPF_LOG_SummaryRpt;
import com.cwp.util.DBUtil;

import oracle.jdbc.OracleTypes;

public class CPF_LOG_Summary_DAO {

	private static DBUtil db1;
	private static Connection connection;
	
	public List<CPF_LOG_SummaryRpt> getReportData(String fromdt, String todt)
	{
		List<CPF_LOG_SummaryRpt> myDataList = new ArrayList<CPF_LOG_SummaryRpt>();
		ResultSet resultSet = null;
		Statement statement = null;
		
		try {
	
			db1 = new DBUtil();
			connection = db1.getConnection();
			
			CallableStatement cstmt = connection.prepareCall("{call GET_LOG_SUMMARY_INFO(?,?,?)}");		           
	        cstmt.setString(1, fromdt);
	        cstmt.setString(2, todt);
	        cstmt.registerOutParameter(3, OracleTypes.CURSOR);
	           
	        cstmt.execute();
	        resultSet =(ResultSet)cstmt.getObject (3);         
		
	        while(resultSet.next())
			{
				
				CPF_LOG_SummaryRpt reportData = new CPF_LOG_SummaryRpt();
				
				reportData.setMessage_Tracking_ID(resultSet.getString("Message_Tracking_ID"));
				reportData.setMajor_Transaction_ID(resultSet.getString("Major_Transaction_ID"));
				reportData.setStatus(resultSet.getString("Status"));
				reportData.setStart_DTM(resultSet.getString("Start_DTM"));
				reportData.setFinish_DTM(resultSet.getString("Finish_DTM"));	
				reportData.setDuration(resultSet.getString("Duration"));
				reportData.setClient_Name(resultSet.getString("CLIENT"));
				reportData.setUser_Name(resultSet.getString("USERNAME"));
			
				myDataList.add(reportData);
				
			}
		}
		catch(Exception e){
			e.printStackTrace(); 
		}
		finally 
		{
			if (resultSet != null) { try { resultSet.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (statement != null) { try { statement.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (connection != null) { try { connection.close(); } catch (SQLException e) { e.printStackTrace(); } }
		}
		
		return myDataList;
	
	}
	
	public static void main(String args [])
	{
		
		try
		{
			
		CPF_LOG_Summary_DAO rdao = new CPF_LOG_Summary_DAO();
		List<CPF_LOG_SummaryRpt> releaseDataList = rdao.getReportData("2010-08-01","2016-12-02");
		Iterator itr = releaseDataList.iterator();
		
		System.out.println("START");
		
		while(itr.hasNext())
		{
	        
			CPF_LOG_SummaryRpt rd = (CPF_LOG_SummaryRpt) itr.next();
			System.out.println("Message_Tracking_ID : " + rd.getMessage_Tracking_ID()+ " Major_Transaction_ID : " + rd.getMajor_Transaction_ID());
			System.out.println("GetUser_Name : " + rd.getUser_Name()+ " Client_Name : " + rd.getClient_Name());
			
		}
		System.out.println("END");
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
	}
	
}
