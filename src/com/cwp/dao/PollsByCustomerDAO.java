package com.cwp.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.cwp.admin.PollsCustomer;
import com.cwp.admin.ServiceRequestBO;
import com.cwp.util.DBUtil;

import oracle.jdbc.OracleTypes;


public class PollsByCustomerDAO {

	private static Connection connection;

	public List<PollsCustomer> getCustomerPollsList(String fromDate, String toDate) {

		List<PollsCustomer> list  = new ArrayList<PollsCustomer>();
		ResultSet resultSet = null;
		Statement statement = null;

		try {
			connection = DBUtil.getConnection();
			CallableStatement cstmt = connection.prepareCall("{Call GET_POLL_BY_CUSTOMER_Rpt(?,?,?)}");
			cstmt.setString(1, fromDate);
			cstmt.setString(2, toDate);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.executeUpdate();
			resultSet = (ResultSet) cstmt.getObject(3);
			
			while (resultSet.next()) {
				PollsCustomer reportData = new PollsCustomer();	
				reportData.setCustomerName(resultSet.getString("NAME"));
				reportData.setIcNumber(resultSet.getString("IC_NUMBER"));
				reportData.setEmailAddress(resultSet.getString("EMAIL_ADDRESS"));
				reportData.setMobileNumber(resultSet.getString("MOBILE"));
				reportData.setCreateDate(resultSet.getString("CREATED_DATE"));
				reportData.setAnswerID(resultSet.getString("ANSWERDESC"));
								
				list.add(reportData);
			}
		} catch (Exception e) {
			System.out.println("ERROR ON CLICK");
			e.printStackTrace();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
	
	
	public static void main(String args[]) {
		try {
			PollsByCustomerDAO rdao = new PollsByCustomerDAO();
			
			List<PollsCustomer> releaseDataList = rdao.getCustomerPollsList("2015-10-01", "2016-11-26");
			int i = 0;
			for (PollsCustomer sr : releaseDataList) {
				i++;
			System.out.println(i + ". " + sr.toString());
			}

		} catch (Exception ex) {
			System.out.println("Error" + ex.getMessage());
		}
	}

}
