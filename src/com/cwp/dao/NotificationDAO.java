package com.cwp.dao;

import java.sql.*;
import java.util.*;

import com.cwp.admin.NotificationTM;
import com.cwp.rpt.FailedRegisterRpt;
import com.cwp.util.DBUtil;
public class NotificationDAO {
	private static DBUtil db1;
	private static Connection connection;
	
	public List<NotificationTM> getAllNotification() {
		List<NotificationTM> users = new ArrayList<NotificationTM>();
		ResultSet rs = null;
		Statement statement = null;
		try {
			db1=new DBUtil();
			connection = db1.getConnection();
			statement = connection.createStatement();
			rs = statement.executeQuery("select * from NOTIFICATION_LIST");
			while (rs.next()) {
				NotificationTM notify = new NotificationTM();
				
				notify.setNotifyid(rs.getInt("ID"));
				notify.setNotifyDesc(rs.getString("NOTIFICATION_DESC"));
				notify.setNotifyType(rs.getString("PRIORITY_TYPE"));
				
				users.add(notify);
			}
		   }
			catch(Exception e){
				e.printStackTrace(); 
			}
			finally {
				if (rs != null) { try { rs.close(); } catch (SQLException e) { e.printStackTrace(); } }
	            if (statement != null) { try { statement.close(); } catch (SQLException e) { e.printStackTrace(); } }
	            if (connection != null) { try { connection.close(); } catch (SQLException e) { e.printStackTrace(); } }
				
			}

		return users;
	}
	public void deleteNotification(int NotifyId) {
		ResultSet rs = null;
		Statement statement = null;
		try {
			db1=new DBUtil();
			connection = db1.getConnection();
			
			PreparedStatement preparedStatement = connection
					.prepareStatement("delete from NOTIFICATION_LIST where ID=?");
			// Parameters start with 1
			preparedStatement.setInt(1, NotifyId);
			preparedStatement.executeUpdate();

		}
		catch(Exception e){
			e.printStackTrace(); 
		}
		finally {
			if (rs != null) { try { rs.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (statement != null) { try { statement.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (connection != null) { try { connection.close(); } catch (SQLException e) { e.printStackTrace(); } }
			
		}
	}
	
	public void updateNotification(NotificationTM Notify) {
		ResultSet rs = null;
		Statement statement = null;
		try {
			db1=new DBUtil();
			connection = db1.getConnection();
			PreparedStatement preparedStatement = connection
					.prepareStatement("update NOTIFICATION_LIST set NOTIFICATION_DESC=?,PRIORITY_TYPE=?" +
							"where ID=?");
			// Parameters start with 1
			preparedStatement.setString(1, Notify.getNotifyDesc());
			preparedStatement.setString(2, "General");
			preparedStatement.setInt(3, Notify.getNotifyid());
			
			preparedStatement.executeUpdate();

		}
		catch(Exception e){
			e.printStackTrace(); 
		}
		finally {
			if (rs != null) { try { rs.close(); } catch (SQLException e) { 
				System.out.println("update Error");
				e.printStackTrace(); } }
            if (statement != null) { try { statement.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (connection != null) { try { connection.close(); } catch (SQLException e) { e.printStackTrace(); } }
			
		}
	}
	public NotificationTM getnotificationById(int NotifyId) {
		ResultSet rs = null;
		Statement statement = null;
		NotificationTM notify = new NotificationTM();
		try {
			db1=new DBUtil();
			connection = db1.getConnection();
			PreparedStatement preparedStatement = connection
					.prepareStatement("select * from NOTIFICATION_LIST where ID=?");
			preparedStatement.setInt(1, NotifyId);
			rs = preparedStatement.executeQuery();
			if (rs.next()) {
				notify.setNotifyid(rs.getInt("ID"));
				notify.setNotifyDesc(rs.getString("NOTIFICATION_DESC"));
			
			}

		}
		catch(Exception e){
			e.printStackTrace(); 
		}
		finally {
			if (rs != null) { try { rs.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (statement != null) { try { statement.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (connection != null) { try { connection.close(); } catch (SQLException e) { e.printStackTrace(); } }
			
		}
		return notify;
	}
	public void addNotification(NotificationTM Notify) {
		ResultSet rs = null;
		Statement statement = null;
		try {
			db1=new DBUtil();
			connection = db1.getConnection();
			PreparedStatement preparedStatement = connection
					.prepareStatement("insert into NOTIFICATION_LIST(NOTIFICATION_DESC,PRIORITY_TYPE,IC_NUMBER) values (?, ?,?)");
			// Parameters start with 1
			preparedStatement.setString(1, Notify.getNotifyDesc());
			preparedStatement.setString(2, "General");
			preparedStatement.setString(3, "45454");
			
			preparedStatement.executeUpdate();

		} catch(Exception e){
			e.printStackTrace(); 
		}
		finally {
			if (rs != null) { try { rs.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (statement != null) { try { statement.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (connection != null) { try { connection.close(); } catch (SQLException e) { e.printStackTrace(); } }
			
		}
		
	
	}
	public static void main(String args [])
	{
		NotificationDAO rdao = new NotificationDAO();
		List<NotificationTM> releaseDataList = rdao.getAllNotification();
		Iterator itr = releaseDataList.iterator();
		while(itr.hasNext())
		{
			NotificationTM rd = (NotificationTM) itr.next();
			System.out.println(" " );
		}
		
	}

}
