package com.cwp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

//import com.absec.cpf.CPF_Logging;
import com.cwp.admin.User;
import com.cwp.util.DBUtil;

public class User_DAO {

	private static DBUtil db1;
	private static Connection connection;

	public List<User> getAllUsers() {
		List<User> users = new ArrayList<User>();
		ResultSet rs = null;
		Statement statement = null;
		try {
			db1 = new DBUtil();
			connection = db1.getConnection();
			statement = connection.createStatement();
			rs = statement.executeQuery("select * from CWPADMIN_USERS");

			while (rs.next()) {
				User muser = new User();
				muser.setID(rs.getInt("ID"));
				muser.setUsername(rs.getString("USERNAME"));
				muser.setCreateddate(rs.getString("CREATEDDATE"));
				muser.setCreatedby(rs.getString("CREATEDBY"));
				muser.setIsactive(rs.getString("ISACTIVE"));
				users.add(muser);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}

		return users;
	}

	public void deleteUser(int ID) {

		ResultSet rs = null;
		Statement statement = null;

		try {

			db1 = new DBUtil();
			connection = db1.getConnection();

			PreparedStatement preparedStatement = connection.prepareStatement("delete from CWPADMIN_USERS where ID=?");
			// Parameters start with 1
			preparedStatement.setInt(1, ID);

			preparedStatement.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	public void updateUser(User muser) {

		ResultSet rs = null;
		Statement statement = null;

		try {

			db1 = new DBUtil();
			connection = db1.getConnection();
			PreparedStatement preparedStatement = connection
					.prepareStatement("Update CWPADMIN_USERS set ISACTIVE=?" + "where id=?");
			// Parameters start with 1
			preparedStatement.setString(1, muser.getIsactive());
			preparedStatement.setInt(2, muser.getID());

			System.out.println("ID = " + muser.getID() + " IsActive = " + muser.getIsactive());

			preparedStatement.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					System.out.println("update Error");
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	public User getuserById(int userid) {

		ResultSet rs = null;
		Statement statement = null;
		User muser = new User();
		try {

			db1 = new DBUtil();
			connection = db1.getConnection();
			PreparedStatement preparedStatement = connection
					.prepareStatement("select * from CWPADMIN_USERS where ID=?");
			preparedStatement.setInt(1, userid);
			rs = preparedStatement.executeQuery();

			if (rs.next()) {
				muser.setID(rs.getInt("ID"));
				muser.setUsername(rs.getString("USERNAME"));
				muser.setCreateddate(rs.getString("CREATEDDATE"));
				muser.setCreatedby(rs.getString("CREATEDBY"));
				muser.setIsactive(rs.getString("ISACTIVE"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return muser;

	}

	public void addUser(User muser) {

		//CPF_Logging L = new CPF_Logging();
		ResultSet rs = null;
		Statement statement = null;
		UUID u_MajorTransactionID = UUID.randomUUID();
		String strMajorTransactionID = u_MajorTransactionID.toString();
		UUID u_MsgID = UUID.randomUUID();
		String MessageTrackingID = u_MsgID.toString();

	
		try {

			db1 = new DBUtil();
			connection = db1.getConnection();

			/*L.CPF_LogIncoming(strMajorTransactionID, MessageTrackingID, "CWPADMIN", "CWPADMIN", muser.getUsername(),
					" User " + muser.getUsername() + "Created Successfully.creating by : " + muser.getCreatedby());
*/
			PreparedStatement preparedStatement = connection.prepareStatement(
					"insert into CWPADMIN_USER(USERNAME,CREATEDDATE,CREATEDBY,ISACTIVE) values (?,?,?,?)");
			preparedStatement.setString(1, muser.getUsername());
			preparedStatement.setString(2, muser.getCreateddate());
			preparedStatement.setString(3, muser.getCreatedby());
			preparedStatement.setString(4, muser.getIsactive());
			preparedStatement.executeUpdate();

			/*L.CPF_LogOutgoing(strMajorTransactionID, MessageTrackingID, "CWPADMIN", "CWPADMIN", muser.getUsername(),
					" User " + muser.getUsername() + "Created Successfully.creating by : " + muser.getCreatedby());
*/
		} catch (Exception e) {
			e.printStackTrace();

			/*L.CPF_logError(strMajorTransactionID, MessageTrackingID, "User_DAO.java | AddUser method",
					muser.getUsername(), "Error", e.getMessage());*/

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		/*L.CPF_logFinish(strMajorTransactionID, MessageTrackingID, "CWPADMIN", "CWPADMIN", muser.getUsername(),
				" User " + muser.getUsername() + "Created Successfully.creating by : " + muser.getCreatedby());*/

	}

	public static void main(String args[]) {
		

		String a = "05/09/2016 14:47:33.492000000".replace(".", "@");
		String v[] = a.split("@");
		System.out.println("Split value is " + a + "values  " + v[0]);
	}
}
