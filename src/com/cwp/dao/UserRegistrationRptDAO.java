package com.cwp.dao;

import java.sql.*;
import java.util.*;

import com.cwp.rpt.UserRegRpt;
import com.cwp.util.DBUtil;

import oracle.jdbc.OracleTypes;

public class UserRegistrationRptDAO {
	private static DBUtil db1;
	private static Connection connection;
	
	public List<UserRegRpt> getUserReportData(String fromdt, String todt)
	{
		
		List<UserRegRpt> UsermyDataList = new ArrayList<UserRegRpt>();
		ResultSet resultSet = null;
		Statement statement = null;
		
		try
		{
			
			db1 = new DBUtil();
			connection = db1.getConnection();
			/*
		     PreparedStatement preparedStatement = connection.
			 prepareStatement("SELECT 'NAME',IC_NUMBER,POLICY_NUMBER,LAST_UPDATE,MOBILE,EMAIL_ADDRESS from User_Profile_TM WHERE to_char(User_Profile_TM.Last_Update,'YYYY-MM-DD')>=? AND to_char(User_Profile_TM.Last_Update,'YYYY-MM-DD')<=?");
			 preparedStatement.setString(1, fromdt);
			 preparedStatement.setString(2, todt);
			 resultSet = preparedStatement.executeQuery();
			*/
			
			CallableStatement cstmt = connection.prepareCall("{Call GET_User_INFO_Rpt(?,?,?)}");
			
	        cstmt.setString(1, fromdt);
	        cstmt.setString(2, todt);
	        cstmt.registerOutParameter(3, OracleTypes.CURSOR);
	        cstmt.execute();
	        
	        resultSet =(ResultSet)cstmt.getObject(3);
	        
			while(resultSet.next())
			{
				
	        	UserRegRpt reportData = new UserRegRpt();
				
	        	reportData.setCustomerName(resultSet.getString("NAME"));     
				reportData.setIC_Number(resultSet.getString("IC_NUMBER"));   
				reportData.setPolicy_Number(resultSet.getString("policyno"));	
		 		reportData.setLast_Update(resultSet.getString("LAST_UPDATE"));
				reportData.setMobile(resultSet.getString("MOBILE"));
				reportData.setEmail_Address(resultSet.getString("EMAIL_ADDRESS"));
				reportData.setPolicyno(resultSet.getString("policyno"));
				UsermyDataList.add(reportData);

			}
		
		}
		catch(Exception e)
		{
			System.out.println("ERROR ON CLICK");
			e.printStackTrace(); 	
		}
		finally 
		{
			
			if (resultSet != null) { try { resultSet.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (statement != null) { try { statement.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (connection != null) { try { connection.close(); } catch (SQLException e) { e.printStackTrace(); } }
		
		}
		
		return UsermyDataList;
	}
	
	public static void main(String args [])
	{
		
	  try
	  {

	    UserRegistrationRptDAO rdao = new UserRegistrationRptDAO();
	    List<UserRegRpt> releaseDataList = rdao.getUserReportData("2015-08-01","2016-08-02");
		Iterator itr = releaseDataList.iterator();

        while(itr.hasNext())
	    {
		
        	UserRegRpt rd = (UserRegRpt) itr.next();
			System.out.println("Results: " + rd.getCustomerName());
			System.out.println("Results: " + rd.getIC_Number() +  " " + rd.getLast_Update() + " " + rd.getMobile() + " " + rd.getPolicy_Number() + " " + rd.getEmail_Address());

	    }
		
		}
		catch(Exception ex)
		{
			System.out.println("Error" + ex.getMessage());
		}
	}
}
