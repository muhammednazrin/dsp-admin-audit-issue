package com.cwp.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.cwp.rpt.CPF_LOG_MessagesRpt;
import com.cwp.util.DBUtil;

import oracle.jdbc.OracleTypes;

public class CPF_LOG_SummaryDetails_DAO_Admin {

	private static DBUtil db1;
	private static Connection connection;
	
	public List<CPF_LOG_MessagesRpt> getReportData(String m_MAJOR_TRANSACTION_ID)
	{
		List<CPF_LOG_MessagesRpt> myDataList = new ArrayList<CPF_LOG_MessagesRpt>();
		ResultSet resultSet = null;
		Statement statement = null;
		
		try {
	
			db1 = new DBUtil();
			connection = db1.getConnection();
			
			CallableStatement cstmt = connection.prepareCall("{call GET_LOG_SUMMARY_ADMINDetails(?,?)}");		           
	        cstmt.setString(1, m_MAJOR_TRANSACTION_ID);
	        cstmt.registerOutParameter(2, OracleTypes.CURSOR);
	           
	        cstmt.execute();
	        resultSet =(ResultSet)cstmt.getObject (2);         
		
	        while(resultSet.next())
			{
				
	        	CPF_LOG_MessagesRpt reportData = new CPF_LOG_MessagesRpt();
			
				reportData.setMessage_ID(resultSet.getString("MESSAGE_ID"));
				reportData.setMajor_Transaction_Id(resultSet.getString("MAJOR_TRANSACTION_ID"));
				reportData.setMessage_Tracking_Id(resultSet.getString("MESSAGE_TRACKING_ID"));
				reportData.setService_Name(resultSet.getString("SERVICE_NAME"));
				reportData.setLogging_Point(resultSet.getString("LOGGING_POINT"));	
				reportData.setDatetime(resultSet.getString("DATETIME"));
				reportData.setPay_Load(resultSet.getString("PAY_LOAD"));
				
				myDataList.add(reportData);
				
			}
		
		}
		catch(Exception e)
		{
		   e.printStackTrace(); 
		}
		finally 
		{
		
			if (resultSet != null) { try { resultSet.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (statement != null) { try { statement.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (connection != null) { try { connection.close(); } catch (SQLException e) { e.printStackTrace(); } }
		
		}
		
		return myDataList;
	
	}
	
	public static void main(String args [])
	{
		
		try
		{
			
		CPF_LOG_SummaryDetails_DAO_Admin rdao = new CPF_LOG_SummaryDetails_DAO_Admin();
		List<CPF_LOG_MessagesRpt> releaseDataList = rdao.getReportData("ed29d7d7-edfd-448a-b8b4-a31386e452b4");
		Iterator itr = releaseDataList.iterator();
		
		System.out.println("START");
		
		while(itr.hasNext())
		{
	        
			CPF_LOG_MessagesRpt rd = (CPF_LOG_MessagesRpt) itr.next();
			
			System.out.println("Message_Tracking_ID : " + rd.getMessage_ID()+ " Major_Transaction_ID : " + rd.getMajor_Transaction_Id());
			
			System.out.println("Message_Tracking_Id : " + rd.getMessage_Tracking_Id()+ " Pay_Load : " + rd.getPay_Load());
			
		}

		    System.out.println("END");
		
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
	}

}