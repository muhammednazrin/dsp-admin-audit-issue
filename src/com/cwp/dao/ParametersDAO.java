package com.cwp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.cwp.admin.NotificationTM;
import com.cwp.admin.Parameters;
import com.cwp.util.DBUtil;

public class ParametersDAO {	
		private static DBUtil db1;
		private static Connection connection;		
		public List<Parameters> getAllSecurityQuestions() {
			Connection connection = null;
			List<Parameters> parametersList = new ArrayList<>();
			ResultSet rs = null;
			try {

				DBUtil database = new DBUtil();
				connection = DBUtil.getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement("SELECT id, vchdescription, vchstatus FROM parameters WHERE VCHPARAMETERTYPE = ?");
				preparedStatement.setString(1, "Security");
				rs = preparedStatement.executeQuery();
				while (rs.next()) {
					Parameters parameters = new Parameters();
					parameters.setId(rs.getInt("id"));
					parameters.setVchdescription(rs.getString("vchdescription"));					
					
					System.out.println("faheem1" + rs.getInt("vchstatus"));
					
					if(rs.getInt("vchstatus")==0)
					{
						parameters.setVchstatus("Active");	
					}
					else
					{						
						parameters.setVchstatus("In Active");									
					}				
					System.out.println("faheem1" + parameters.getVchstatus());
					
					
					parametersList.add(parameters);
				}
			} catch (Exception e) {

				e.printStackTrace();
			} finally {
				// cstmt.close();
				try {
					if (connection != null)
						connection.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				connection = null;
				try {
					if (rs != null)
						rs.close();

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			return parametersList;
		}
		
		
		/*public void deleteNotification(int NotifyId) {
			ResultSet rs = null;
			Statement statement = null;
			try {
				db1=new DBUtil();
				connection = db1.getConnection();
				
				PreparedStatement preparedStatement = connection
						.prepareStatement("delete from NOTIFICATION_LIST where ID=?");
				// Parameters start with 1
				preparedStatement.setInt(1, NotifyId);
				preparedStatement.executeUpdate();

			}
			catch(Exception e){
				e.printStackTrace(); 
			}
			finally {
				if (rs != null) { try { rs.close(); } catch (SQLException e) { e.printStackTrace(); } }
	            if (statement != null) { try { statement.close(); } catch (SQLException e) { e.printStackTrace(); } }
	            if (connection != null) { try { connection.close(); } catch (SQLException e) { e.printStackTrace(); } }
				
			}
		}*/
		
		public void updateSecurityQuestion(Parameters parameters) {
			ResultSet rs = null;
			Statement statement = null;
			try {
				db1=new DBUtil();
				connection = db1.getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement("update parameters set VCHDESCRIPTION=?,VCHSTATUS=?, VCHUPDATEBY=?,UPDATEDT=? where ID=?");
				// Parameters start with 1
				preparedStatement.setString(1, parameters.getVchdescription());
				System.out.println("Print Description" +parameters.getVchstatus());
				preparedStatement.setString(2, parameters.getVchstatus());
				System.out.println("Print Farooq hyderabad Status" +parameters.getVchstatus());
				preparedStatement.setString(3, "admin");
				preparedStatement.setDate(4, new java.sql.Date(System.currentTimeMillis()));
				preparedStatement.setInt(5, parameters.getId());
				
				preparedStatement.executeUpdate();

			}
			catch(Exception e){
				e.printStackTrace(); 
			}
			finally {
				if (rs != null) { try { rs.close(); } catch (SQLException e) { 
					System.out.println("update Error");
					e.printStackTrace(); } }
	            if (statement != null) { try { statement.close(); } catch (SQLException e) { e.printStackTrace(); } }
	            if (connection != null) { try { connection.close(); } catch (SQLException e) { e.printStackTrace(); } }
				
			}
		}
		public Parameters getSecurityQuestionById(int id) {
			ResultSet rs = null;
			Statement statement = null;
			Parameters parameters = new Parameters();
			
			//
			
			 try {
			      DBUtil database = new DBUtil();
			      connection = DBUtil.getConnection();

			      PreparedStatement pstm = connection.prepareStatement("select id, vchdescription,vchstatus  from parameters where ID=? and VCHPARAMETERTYPE=?");
			      pstm.setInt(1, id);
			      pstm.setString(2, "Security");
			 
			      rs = pstm.executeQuery();
			 
			      while (rs.next()) {
			         
						parameters.setId(rs.getInt("id"));
						parameters.setVchdescription(rs.getString("vchdescription"));
						
						if(rs.getInt("vchstatus")==0)
						{
							parameters.setVchstatus("Active");	
						}
						else
						{						
							parameters.setVchstatus("In Active");									
						}				
						//System.out.println("faheem1" + parameters.getVchstatus());
						
						
			      }
			      
			      } catch(Exception e){
						e.printStackTrace(); 
					}
					finally {
						if (rs != null) { try { rs.close(); } catch (SQLException e) { e.printStackTrace(); } }
			            if (statement != null) { try { statement.close(); } catch (SQLException e) { e.printStackTrace(); } }
			            if (connection != null) { try { connection.close(); } catch (SQLException e) { e.printStackTrace(); } }
						
					}
			 return parameters;
		}
			

			//
			
		
		public void addSecurityQuestions(Parameters parameters) {
			ResultSet rs = null;
			Statement statement = null;
			//Date date = new Date();
			try {
				db1=new DBUtil();
				connection = db1.getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement("insert into parameters(VCHCODE,VCHDESCRIPTION,VCHPARAMETERTYPE,VCHSTATUS,VCHCREATEBY,CREATEDDATE,VCHUPDATEBY,UPDATEDT) values (?,?,?,?,?,?,?,?)");
				// Parameters start with 1
				preparedStatement.setString(1, "Question");
				preparedStatement.setString(2, parameters.getVchdescription());
				preparedStatement.setString(3, "Security");
				preparedStatement.setString(4, parameters.getVchstatus());
				preparedStatement.setString(5, "admin");
				preparedStatement.setDate(6, new java.sql.Date(System.currentTimeMillis()));
				preparedStatement.setString(7, "admin");
				preparedStatement.setDate(8, new java.sql.Date(System.currentTimeMillis()));
				
				preparedStatement.executeUpdate();

			} catch(Exception e){
				e.printStackTrace(); 
			}
			finally {
				if (rs != null) { try { rs.close(); } catch (SQLException e) { e.printStackTrace(); } }
	            if (statement != null) { try { statement.close(); } catch (SQLException e) { e.printStackTrace(); } }
	            if (connection != null) { try { connection.close(); } catch (SQLException e) { e.printStackTrace(); } }
				
			}
			
		
		}
		public static void main(String args [])
		{
			
			ParametersDAO a = new ParametersDAO();
			a.getAllSecurityQuestions();
			
			NotificationDAO rdao = new NotificationDAO();
			List<NotificationTM> releaseDataList = rdao.getAllNotification();
			Iterator itr = releaseDataList.iterator();
			while(itr.hasNext())
			{
				NotificationTM rd = (NotificationTM) itr.next();
				System.out.println(" " );
			}
			
		}

	
	
	 /*public static Parameters findParameter(String id) {
	      String sql = "select id, vchdescription  from parameters where ID=? and VCHPARAMETERTYPE=?;";
	      Connection connection = null;
	      ResultSet rs = null;
	      Parameters parameters = new Parameters();
	      
	      try {
	      DBUtil database = new DBUtil();
	      connection = DBUtil.getConnection();

	      PreparedStatement pstm = connection.prepareStatement(sql);
	      pstm.setString(1, id);
	      pstm.setString(2, "Security");
	 
	      rs = pstm.executeQuery();
	 
	      while (rs.next()) {
	         
				parameters.setId(rs.getInt("id"));
				parameters.setVchdescription(rs.getString("vchdescription"));
				return parameters;
	      }
	      
	      } catch (Exception e) {

				e.printStackTrace();
			} finally {
				// cstmt.close();
				try {
					if (connection != null)
						connection.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					if (rs != null)
						rs.close();

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

	      return null;
	  }

	public List<Parameters> getAllSecurityQuestions() {

		Connection connection = null;
		List<Parameters> parametersList = new ArrayList<>();
		ResultSet rs = null;

		try {

			DBUtil database = new DBUtil();
			connection = DBUtil.getConnection();

			PreparedStatement preparedStatement = connection
					.prepareStatement("SELECT id, vchdescription, vchstatus FROM parameters WHERE VCHPARAMETERTYPE = ?");
			preparedStatement.setString(1, "Security");
			rs = preparedStatement.executeQuery();

			while (rs.next()) {
				Parameters parameters = new Parameters();
				parameters.setId(rs.getInt("id"));
				parameters.setVchdescription(rs.getString("vchdescription"));
				
				if(rs.getInt("vchstatus")==0)
				{
					parameters.setVchstatus("In Active");	
				}
				else
				{
					
					parameters.setVchstatus("Active");	
								
				}
				
				
				parametersList.add(parameters);
			}
		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			// cstmt.close();
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			connection = null;
			try {
				if (rs != null)
					rs.close();

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return parametersList;
	}
	
	
	public Parameters getUpdateSecurityQuestions(Parameters parameters) {

		Connection connection = null;
		ResultSet rs = null;

		try {

			DBUtil database = new DBUtil();
			connection = DBUtil.getConnection();

			PreparedStatement preparedStatement = connection
					.prepareStatement("update parameters set vchdescription =? , vchstatus=? where id=?");
			preparedStatement.setString(1, parameters.getVchdescription());
			preparedStatement.setString(2, parameters.getVchstatus());
			preparedStatement.setInt(3, parameters.getId());
			preparedStatement.executeUpdate();

			
		} catch (Exception e) {

			e.printStackTrace();
		} 
		return parameters;
	}
	
	public Parameters getInsertSecurityQuestions() {

		Connection connection = null;
		Parameters parameters = new Parameters();
		ResultSet rs = null;

		try {

			DBUtil database = new DBUtil();
			connection = DBUtil.getConnection();

			PreparedStatement preparedStatement = connection
					.prepareStatement("insert into parameters(vchdescription,vchstatus) values(?,?)");
			preparedStatement.setString(1, parameters.getVchdescription());
			preparedStatement.setString(2, parameters.getVchstatus());
			//preparedStatement.setInt(3, parameters.getId());
			preparedStatement.executeUpdate();

			
		} catch (Exception e) {

			e.printStackTrace();
		} 
		return parameters;
	}*/
	

}
