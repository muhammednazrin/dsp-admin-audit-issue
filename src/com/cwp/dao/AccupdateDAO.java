package com.cwp.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cwp.admin.AccountInfoBean;
import com.cwp.admin.AccupdateBean;
import com.cwp.util.DBUtil;
import com.cwp.util.dspdbutil;

import oracle.jdbc.OracleTypes;

public class AccupdateDAO {
	private static DBUtil db1;
	private static Connection connection;
	
	public  AccupdateBean queryUpdateAccno(AccupdateBean custInfo){
        Connection connection = null;
     
        ResultSet rs = null;
        
        try {
        	
    	    connection = db1.getConnection();
    	    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
          
     	    //String todate = dateFormat.format(date);
            CallableStatement cstmt = connection.prepareCall("{call CWP_SP_UPDATE_ACCNO(?,?,?,?,?,?,?,?,?,?)}");
            cstmt.setString(1, custInfo.getPolicyno());
            cstmt.setString(2, custInfo.getIc_number());
            cstmt.setString(3, custInfo.getAcc_no());
            cstmt.setString(4, custInfo.getBank_name());
            cstmt.setString(5, custInfo.getEmail());
            cstmt.setString(6, custInfo.getContactno());
            cstmt.setString(7, custInfo.getUpdateby());
            cstmt.setString(8, custInfo.getBranch_name());
            cstmt.setString(9, custInfo.getBranch_code());
            cstmt.registerOutParameter(10, OracleTypes.CURSOR);  
           
           
            
            cstmt.executeUpdate();
            rs = (ResultSet) cstmt.getObject(10);
            
            while(rs.next())
		    {
            	
		    	
		   
            	custInfo.setPolicyno(rs.getString("POLICYNO"));
            	custInfo.setIc_number(rs.getString("IC_NUMBER"));
            	custInfo.setEmail(rs.getString("EMAIL_ADDRESS"));
            	custInfo.setContactno(rs.getString("CONTACT_NO"));
            	custInfo.setAcc_no(rs.getString("BANK_ACC_NO"));
            	custInfo.setBank_name(rs.getString("BANK_NAME"));
            	custInfo.setBranch_name(rs.getString("BRANCH_NAME"));
            	custInfo.setBranch_code(rs.getString("BRANCH_CODE"));
                
            
		    
		    }
            
            custInfo.setError_code("D0000");
            custInfo.setError_msg("Success");
           
          
        
           
        } catch (Exception e) {
            // a failure occurred log message;
        	System.out.println("[ERROR]"+new Date()+"Exception " +e.getMessage()+" "+ e.getCause());
        	System.out.println("[ERROR] updating Agent profile");
           //	logInfo.setError_code("D7777");
        	 custInfo.setError_code("D7777");
	         custInfo.setError_msg("CWP " +e.getMessage());
            e.printStackTrace();
            }finally {
                  //cstmt.close();
                  try {
                        if(connection!=null)connection.close();
                  } catch (SQLException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                  }
                  connection = null;
                  try {
                        if(rs!=null)rs.close();
                       
                  } catch (SQLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                  }
              
      } 
		return custInfo;
	}
	
	public AccupdateBean queryDSPUpdateAccno(AccupdateBean custInfo){
        Connection connection = null;
     
        ResultSet rs = null;
        
        try {
        	dspdbutil database= new dspdbutil();
    	    connection = database.getConnection();
    	    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
          
     	    //String todate = dateFormat.format(date);
            CallableStatement cstmt = connection.prepareCall("{call DSP_SP_UPDATE_ACCNO(?,?,?,?,?,?,?,?,?)}");
            cstmt.setString(1, custInfo.getPolicyno());
            cstmt.setString(2, custInfo.getIc_number());
            cstmt.setString(3, custInfo.getAcc_no());
            cstmt.setString(4, custInfo.getBank_name());
            cstmt.setString(5, custInfo.getEmail());
            cstmt.setString(6, custInfo.getContactno());
            cstmt.setString(7, custInfo.getUpdateby());
            cstmt.setString(8, custInfo.getBranch_name());
            cstmt.setString(9, custInfo.getBranch_code());
           
      
           
            
            cstmt.executeUpdate();
            custInfo.setError_code("D0000");
            custInfo.setError_msg("Success");
           
          
        
           
        } catch (Exception e) {
            // a failure occurred log message;
        	System.out.println("[ERROR]"+new Date()+"Exception " +e.getMessage()+" "+ e.getCause());
        	System.out.println("[ERROR] updating Agent profile");
           //	logInfo.setError_code("D7777");
        	 custInfo.setError_code("D7777");
	         custInfo.setError_msg("DSP " +e.getMessage());
            e.printStackTrace();
            }finally {
                  //cstmt.close();
                  try {
                        if(connection!=null)connection.close();
                  } catch (SQLException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                  }
                  connection = null;
                  try {
                        if(rs!=null)rs.close();
                       
                  } catch (SQLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                  }
              
      } 
		return custInfo;
	}
	//Delete accinfo
	
	public AccupdateBean dspdeleteAccinfo(AccupdateBean custInfo){
        Connection connection = null;
     
        ResultSet rs = null;
        
        try {
        	dspdbutil database= new dspdbutil();
    	    connection = database.getConnection();
    	    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
          
     	    //String todate = dateFormat.format(date);
            CallableStatement cstmt = connection.prepareCall("{call DSP_SP_DELETE_ACCNO(?,?)}");
            cstmt.setString(1, custInfo.getPolicyno());
            cstmt.setString(2, custInfo.getIc_number());
            
            cstmt.executeUpdate();
            custInfo.setError_code("D0000");
            custInfo.setError_msg("Success");
           
          
        
           
        } catch (Exception e) {
            // a failure occurred log message;
        	System.out.println("[ERROR]"+new Date()+"Exception " +e.getMessage()+" "+ e.getCause());
        	System.out.println("[ERROR] delete accinfo");
           //	logInfo.setError_code("D7777");
        	 custInfo.setError_code("D7777");
	         custInfo.setError_msg("DSP " +e.getMessage());
            e.printStackTrace();
            }finally {
                  //cstmt.close();
                  try {
                        if(connection!=null)connection.close();
                  } catch (SQLException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                  }
                  connection = null;
                  try {
                        if(rs!=null)rs.close();
                       
                  } catch (SQLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                  }
              
      } 
		return custInfo;
	}
	
	public AccupdateBean cwpdeleteAccinfo(AccupdateBean custInfo){
        Connection connection = null;
     
        ResultSet rs = null;
        
        try {
        	dspdbutil database= new dspdbutil();
    	    connection = database.getConnection();
    	    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
          
     	    //String todate = dateFormat.format(date);
            CallableStatement cstmt = connection.prepareCall("{call CWP_SP_DELETE_ACCNO(?,?)}");
            cstmt.setString(1, custInfo.getPolicyno());
            cstmt.setString(2, custInfo.getIc_number());
            
            cstmt.executeUpdate();
            custInfo.setError_code("D0000");
            custInfo.setError_msg("Success");
           
          
        
           
        } catch (Exception e) {
            // a failure occurred log message;
        	System.out.println("[ERROR]"+new Date()+"Exception " +e.getMessage()+" "+ e.getCause());
        	System.out.println("[ERROR] delete accinfo");
           //	logInfo.setError_code("D7777");
        	 custInfo.setError_code("D7777");
	         custInfo.setError_msg("DSP " +e.getMessage());
            e.printStackTrace();
            }finally {
                  //cstmt.close();
                  try {
                        if(connection!=null)connection.close();
                  } catch (SQLException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                  }
                  connection = null;
                  try {
                        if(rs!=null)rs.close();
                       
                  } catch (SQLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                  }
              
      } 
		return custInfo;
	}
	//Fetch Account info
	
	public AccupdateBean queryfetchAccInfoByPolicy(AccupdateBean custInfo){
        Connection connection = null;
     
        ResultSet rs = null;
      //  List<AccountInfoBean> acclist = new ArrayList<AccountInfoBean>();
        
        try {
        	
    	    connection = db1.getConnection();
    	    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
          
     	    //String todate = dateFormat.format(date);
            CallableStatement cstmt = connection.prepareCall("{call CWP_GET_ACCOUNT_INFO_BYPOLICY(?,?,?)}");
            
            cstmt.setString(1, custInfo.getIc_number());
            cstmt.setString(2, custInfo.getPolicyno());
            cstmt.registerOutParameter(3, OracleTypes.CURSOR);  
            
            cstmt.execute();
            
            rs = (ResultSet) cstmt.getObject(3);
            
            while(rs.next())
		    {
            	
		    	
		   
            	custInfo.setPolicyno(rs.getString("POLICYNO"));
            	custInfo.setIc_number(rs.getString("IC_NUMBER"));
            	custInfo.setEmail(rs.getString("EMAIL_ADDRESS"));
            	custInfo.setContactno(rs.getString("CONTACT_NO"));
            	custInfo.setAcc_no(rs.getString("BANK_ACC_NO"));
            	custInfo.setBank_name(rs.getString("BANK_NAME"));
            	custInfo.setBranch_name(rs.getString("BRANCH_NAME"));
            	custInfo.setBranch_code(rs.getString("BRANCH_CODE"));
                
            
		    
		    }
           
            
            
            custInfo.setError_code("D0000");
            custInfo.setError_msg("Success");
           
          
        
           
        } catch (Exception e) {
            // a failure occurred log message;
        	System.out.println("[ERROR]"+new Date()+"Exception " +e.getMessage()+" "+ e.getCause());
        	System.out.println("[ERROR] fetch acc info policyno");
           //	logInfo.setError_code("D7777");
        	 custInfo.setError_code("D7777");
	         custInfo.setError_msg("CWP " +e.getMessage());
            e.printStackTrace();
            }finally {
                  //cstmt.close();
                  try {
                        if(connection!=null)connection.close();
                  } catch (SQLException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                  }
                  connection = null;
                  try {
                        if(rs!=null)rs.close();
                       
                  } catch (SQLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                  }
              
      } 
		return custInfo;
	}
	//Fetch Account info
	
		public AccupdateBean queryfetchAccInfoByID(AccupdateBean custInfo){
	        Connection connection = null;
	     
	        ResultSet rs = null;
	      //  List<AccountInfoBean> acclist = new ArrayList<AccountInfoBean>();
	        
	        try {
	        	
	    	    connection = db1.getConnection();
	    	    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	            Date date = new Date();
	          
	     	    //String todate = dateFormat.format(date);
	            CallableStatement cstmt = connection.prepareCall("{call CWP_GET_ACCOUNT_INFO_BYID(?,?)}");
	            
	            cstmt.setLong(1, custInfo.getId());
	           
	            cstmt.registerOutParameter(2, OracleTypes.CURSOR);  
	            
	            cstmt.execute();
	            
	            rs = (ResultSet) cstmt.getObject(2);
	            
	            while(rs.next())
			    {
	            	
			    	
			   
	            	custInfo.setPolicyno(rs.getString("POLICYNO"));
	            	custInfo.setIc_number(rs.getString("IC_NUMBER"));
	            	custInfo.setEmail(rs.getString("EMAIL_ADDRESS"));
	            	custInfo.setContactno(rs.getString("CONTACT_NO"));
	            	custInfo.setAcc_no(rs.getString("BANK_ACC_NO"));
	            	custInfo.setBank_name(rs.getString("BANK_NAME"));
	            	custInfo.setBranch_name(rs.getString("BRANCH_NAME"));
	            	custInfo.setBranch_code(rs.getString("BRANCH_CODE"));
	            	custInfo.setUpdateby(rs.getString("UPDATE_BY"));
	                
	            
			    
			    }
	           
	            
	            
	            custInfo.setError_code("D0000");
	            custInfo.setError_msg("Success");
	           
	          
	        
	           
	        } catch (Exception e) {
	            // a failure occurred log message;
	        	System.out.println("[ERROR]"+new Date()+"Exception " +e.getMessage()+" "+ e.getCause());
	        	System.out.println("[ERROR] fetch acc info policyno");
	           //	logInfo.setError_code("D7777");
	        	 custInfo.setError_code("D7777");
		         custInfo.setError_msg("CWP " +e.getMessage());
	            e.printStackTrace();
	            }finally {
	                  //cstmt.close();
	                  try {
	                        if(connection!=null)connection.close();
	                  } catch (SQLException e1) {
	                        // TODO Auto-generated catch block
	                        e1.printStackTrace();
	                  }
	                  connection = null;
	                  try {
	                        if(rs!=null)rs.close();
	                       
	                  } catch (SQLException e) {
	                        // TODO Auto-generated catch block
	                        e.printStackTrace();
	                  }
	              
	      } 
			return custInfo;
		}
		//Fetch Account info
		
				public List<AccupdateBean> queryfetchAccInfo(AccupdateBean custInfo){
			        Connection connection = null;
			     
			        ResultSet rs = null;
			        List<AccupdateBean> acclist = new ArrayList<AccupdateBean>();
			        
			        try {
			        	
			    	    connection = db1.getConnection();
			    	    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			            Date date = new Date();
			          
			     	    //String todate = dateFormat.format(date);
			            CallableStatement cstmt = connection.prepareCall("{call CWP_GET_ACCOUNT_INFO_ADMIN(?,?,?,?)}");
			            
			            cstmt.setString(1, custInfo.getIc_number());
			            cstmt.setString(2, custInfo.getPolicyno());
			            cstmt.setString(3, custInfo.getUpdateby());
			          
			            cstmt.registerOutParameter(4, OracleTypes.CURSOR);  
			            
			            cstmt.execute();
			            
			            rs = (ResultSet) cstmt.getObject(4);
			           
			            while(rs.next())
					    {
			            	AccupdateBean accinfo=new AccupdateBean();
					    	
			            	accinfo.setId(Long.parseLong(rs.getString("ID")));
			            	accinfo.setPolicyno(rs.getString("POLICYNO"));
			            	accinfo.setIc_number(rs.getString("IC_NUMBER"));
			            	accinfo.setEmail(rs.getString("EMAIL_ADDRESS"));
			            	accinfo.setContactno(rs.getString("CONTACT_NO"));
			            	accinfo.setAcc_no(rs.getString("BANK_ACC_NO"));
			            	accinfo.setBank_name(rs.getString("BANK_NAME"));
			            	accinfo.setBranch_name(rs.getString("BRANCH_NAME"));
			            	accinfo.setBranch_code(rs.getString("BRANCH_CODE"));
			            	accinfo.setUpdateby(rs.getString("UPDATE_BY"));
		                    
			            	acclist.add(accinfo);
					    
					    }
					 
			           // custInfo.setAccinfo_list(acclist);
					  
			            
			            
			            custInfo.setError_code("D0000");
			            custInfo.setError_msg("Success");
			           
			          
			        
			           
			        } catch (Exception e) {
			            // a failure occurred log message;
			        	System.out.println("[ERROR]"+new Date()+"Exception " +e.getMessage()+" "+ e.getCause());
			        	System.out.println("[ERROR] updating Agent profile");
			           //	logInfo.setError_code("D7777");
			        	 custInfo.setError_code("D7777");
				         custInfo.setError_msg("CWP " +e.getMessage());
			            e.printStackTrace();
			            }finally {
			                  //cstmt.close();
			                  try {
			                        if(connection!=null)connection.close();
			                  } catch (SQLException e1) {
			                        // TODO Auto-generated catch block
			                        e1.printStackTrace();
			                  }
			                  connection = null;
			                  try {
			                        if(rs!=null)rs.close();
			                       
			                  } catch (SQLException e) {
			                        // TODO Auto-generated catch block
			                        e.printStackTrace();
			                  }
			              
			      } 
					return acclist;
				}
		//Fetch policynos
				
				//Fetch Account info
				
				public List<AccupdateBean> getPolicylist(AccupdateBean custInfo){
			        Connection connection = null;
			     
			        ResultSet rs = null;
			        List<AccupdateBean> acclist = new ArrayList<AccupdateBean>();
			        
			        try {
			        	
			        	dspdbutil database= new dspdbutil();
			    	    connection = database.getConnection();
			    	    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			            Date date = new Date();
			          
			     	    //String todate = dateFormat.format(date);
			            CallableStatement cstmt = connection.prepareCall("{call DSP_GET_POLICYNUMBERS(?)}");
			            
			         
			            cstmt.registerOutParameter(1, OracleTypes.CURSOR);  
			            
			            cstmt.execute();
			            
			            rs = (ResultSet) cstmt.getObject(1);
			           
			            while(rs.next())
					    {
			            	AccupdateBean accinfo=new AccupdateBean();
					    	
			            
			            	accinfo.setPolicyno(rs.getString("POLICY_NUMBER").trim());
			            	
		                    
			            	acclist.add(accinfo);
					    
					    }
					 
			           // custInfo.setAccinfo_list(acclist);
					  
			            
			            
			            custInfo.setError_code("D0000");
			            custInfo.setError_msg("Success");
			           
			          
			        
			           
			        } catch (Exception e) {
			            // a failure occurred log message;
			        	System.out.println("[ERROR]"+new Date()+"Exception " +e.getMessage()+" "+ e.getCause());
			        	System.out.println("[ERROR] updating Agent profile");
			           //	logInfo.setError_code("D7777");
			        	 custInfo.setError_code("D7777");
				         custInfo.setError_msg("CWP " +e.getMessage());
			            e.printStackTrace();
			            }finally {
			                  //cstmt.close();
			                  try {
			                        if(connection!=null)connection.close();
			                  } catch (SQLException e1) {
			                        // TODO Auto-generated catch block
			                        e1.printStackTrace();
			                  }
			                  connection = null;
			                  try {
			                        if(rs!=null)rs.close();
			                       
			                  } catch (SQLException e) {
			                        // TODO Auto-generated catch block
			                        e.printStackTrace();
			                  }
			              
			      } 
					return acclist;
				}

}
