package com.cwp.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.cwp.rpt.ValidationFailedAgentRpt;
import com.cwp.util.DBUtil;

import oracle.jdbc.OracleTypes;

public class ValidationFailedAgentRptDAO {


	private static DBUtil db1;
	private static Connection connection;
	
	public List<ValidationFailedAgentRpt> getReportData(String fromdt, String todt)
	{
		List<ValidationFailedAgentRpt> myDataList = new ArrayList<ValidationFailedAgentRpt>();
		ResultSet resultSet = null;
		Statement statement = null;
		
		try{
	
			db1 = new DBUtil();
			connection = db1.getConnection();
			
			/* PreparedStatement preparedStatement = connection.
			 prepareStatement("select * from USER_PROFILE_LOG WHERE CREATEDDATE>='" + fromdt + "' AND CREATEDDATE<='" + todt + "'");
			
			resultSet = preparedStatement.executeQuery();
			*/
			
			  CallableStatement cstmt = connection.prepareCall("{call GET_FAILED_Agent_INFO(?,?,?)}");		           
	             cstmt.setString(1, fromdt);
	             cstmt.setString(2, todt);
	             cstmt.registerOutParameter(3, OracleTypes.CURSOR);
	           
	            cstmt.execute();
	            resultSet =(ResultSet)cstmt.getObject (3);         
			while(resultSet.next())
			{
				
				ValidationFailedAgentRpt reportData = new ValidationFailedAgentRpt();
				
				reportData.setIdNumber(resultSet.getString("ICNUMBER"));
				reportData.setPolicyno(resultSet.getString("POLICYNO"));
				reportData.setEmail(resultSet.getString("EMAIL"));
				reportData.setMobile(resultSet.getString("MOBILE"));
				reportData.setResponsecode(resultSet.getString("RESPONSECODE"));	
				reportData.setLastdate(resultSet.getString("CREATEDDATE"));
				reportData.setFailedCustomerName("TEST");
				myDataList.add(reportData);
				
			}
		}
		catch(Exception e){
			e.printStackTrace(); 
		}
		finally 
		{
			if (resultSet != null) { try { resultSet.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (statement != null) { try { statement.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (connection != null) { try { connection.close(); } catch (SQLException e) { e.printStackTrace(); } }
		}
		
		return myDataList;
	
	}
	
	public static void main(String args [])
	{
		ValidationFailedAgentRptDAO rdao = new ValidationFailedAgentRptDAO();
		List<ValidationFailedAgentRpt> releaseDataList = rdao.getReportData("2011-08-01","2016-08-02");
		Iterator itr = releaseDataList.iterator();
		
		while(itr.hasNext())
		{
		
			ValidationFailedAgentRpt rd = (ValidationFailedAgentRpt)itr.next();

			System.out.println(" " + rd.getIdNumber()+ rd.getLastdate());
		
		}
		
	}
	
	
}
