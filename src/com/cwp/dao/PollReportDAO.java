package com.cwp.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cwp.rpt.Polls;
import com.cwp.rpt.PollsListRpt;
import com.cwp.util.DBUtil;
import com.etiqa.common.DB.cwpdbutil;

import oracle.jdbc.OracleTypes;

public class PollReportDAO {
	
	 public PollsListRpt getPollsList() {
	    	
	    	Connection connection = null;
	    	PollsListRpt pollsListRpt = null;
	    	List<Polls> polls = new ArrayList<Polls>();
	    	Polls poll= null;
	        ResultSet rs = null;
	        
	        try {
	        	
	    	    connection = DBUtil.getConnection();
	    	    PreparedStatement preparedStatement = connection.
	            prepareStatement("select unique(Question),uidpk, CREATED_DATE,Expired_Date from X_QUESTIONMASTER");
	             rs = preparedStatement.executeQuery();
       
	            while(rs.next() ) {
	            	poll= new Polls();
	            	poll.setId(rs.getString("uidpk"));
	            	poll.setQuestion(rs.getString("QUESTION"));
	            	poll.setStartdate(rs.getDate("CREATED_DATE"));
	            	poll.setExpiryDate(rs.getDate("Expired_Date"));
	            	polls.add(poll);
	             }
	            pollsListRpt= new PollsListRpt();
	            pollsListRpt.setPolls(polls);
	    	
	        } catch (Exception e) {
	        	        	
	            e.printStackTrace();
	            
	        }finally {
	                  //cstmt.close();
	                  try {
	                        if(connection!=null)connection.close();
	                  } catch (SQLException e1) {
	                        // TODO Auto-generated catch block
	                        e1.printStackTrace();
	                  }
	                  connection = null;
	                  try {
	                        if(rs!=null)rs.close();
	                       
	                  } catch (SQLException e) {
	                        // TODO Auto-generated catch block
	                        e.printStackTrace();
	                  }
	              
	      } 
	        
			return pollsListRpt;
		
	    }
	 
	 public String getPollDetails(String QId) {
	    	
	    	Connection connection = null;
	    	int i=0;
	    	String data="";

			SimpleDateFormat dateFormatWithTime = new SimpleDateFormat("dd-MMM-yyyy");

			 SimpleDateFormat dateParse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	      ResultSet resultSet = null;
	        try {
	    	    connection = DBUtil.getConnection();
	    	    System.out.println(QId);
	    		String query="Select x.QUESTION,y.answer, x.qcnt,y.res,x.last_entry_date as last_entry_date,x.first_entry_date as first_entry_date From (select q.uidpk,q.question ,MAX(qa.created_date) as last_entry_date, MIN(qa.created_date) as first_entry_date, count(1) qcnt from X_QUESTIONMASTER q inner join X_QUESTIONANSWER qa on q.uidpk=qa.question_uid where q.uidpk=? group by q.uidpk,q.question) x inner join (Select q.uidpk, q.QUESTION,a.answer, count(qa.answer_uid) res From X_QUESTIONMASTER q inner join X_ANSWERMASTER a on q.uidpk=a. QUESTION_uid Left join X_QUESTIONANSWER qa on q.uidpk=qa.QUESTION_UID and a.uidpk=qa.ANSWER_UID group by q.uidpk, q.question,a.answer) y on x.uidpk=y.uidpk order by 1";

	    	    PreparedStatement preparedStatement = connection.prepareStatement(query);
	            preparedStatement.setString(1, QId);
	            resultSet = preparedStatement.executeQuery();

	            while (resultSet.next()) { 

					if(i==0){
					data=data+"{\"isSucess\":\"true\",\"question\":\""+resultSet.getString("QUESTION")+"\",\"first_entry\":\""+dateFormatWithTime.format(dateParse.parse(resultSet.getString("first_entry_date")))+"\",\"last_entry\":\""+dateFormatWithTime.format(dateParse.parse(resultSet.getString("last_entry_date")))+"\",\"total\":\""+resultSet.getString("qcnt")+"\",\"graphData\":[";
					}else{
						data=data+",";
					}
					i++;
					data=data+"{ \"label\": \""+resultSet.getString("answer")+ "\", \"y\": "+resultSet.getString("RES")+"}";
				} 
	            if(i>0){
	            	data= data+"]}";
	            }
	        } catch (Exception e) {
	        	        	
	            e.printStackTrace();
	            
	        }finally {
	                  //cstmt.close();
	                  try {
	                        if(connection!=null)connection.close();
	                  } catch (SQLException e1) {
	                        // TODO Auto-generated catch block
	                        e1.printStackTrace();
	                  }
	                  connection = null;
	                  try {
	                        if(resultSet!=null)resultSet.close();
	                       
	                  } catch (SQLException e) {
	                        // TODO Auto-generated catch block
	                        e.printStackTrace();
	                  }
	              
	      } 
	        
			return data;
		
	    }
	 
	 public String getSearchPolls(String qName, String startDate, String endDate) {
	    	
			int i=0;
	    	String data="";
	    	Connection connection = null;
	    	int j=1;

			ResultSet resultSet = null;
	    	try {
	    	Date date = new Date();
	    	cwpdbutil database = new cwpdbutil();
			connection = database.getConnection();
			
			// String todate = dateFormat.format(date);
			CallableStatement cstmt = connection.prepareCall("{call CWP_GET_POLL_REPORT(?,?,?,?)}");

			cstmt.setString(1, qName);
			cstmt.setString(2, startDate);
			cstmt.setString(3, endDate);
			cstmt.registerOutParameter(4, OracleTypes.CURSOR);

			cstmt.execute();

			resultSet = (ResultSet) cstmt.getObject(3);

			while (resultSet.next()) {

				if(i==0){
					data=data+"{\"isSuccess\":\"true\",\"searchData\":[";
					}else{
						data=data+",";
					}
					i++;
					data=data+"{ \"row\": \""+i+ "\", \"question\": \""+resultSet.getString("QUESTION")+"\", \"startdate\": \""+resultSet.getString("CREATED_DATE")+"\", \"expirydate\": \""+resultSet.getString("Expired_Date")+"\",\"pollid\": \""+resultSet.getString("uidpk")+"\"}";
	             }
	    	if(i>0){
	    		data=data+"]}";
	    	}else{
	    		data=data+"{\"isSuccess\":\"true\",\"searchData\":[]}";
	    	}
		   /*  String query="select * from X_QUESTIONMASTER ";
	      ResultSet resultSet = null;
	      SimpleDateFormat dateFormat= new SimpleDateFormat("dd-MM-YYYY");
	      SimpleDateFormat dateFormat2= new SimpleDateFormat("dd/MM/YYYY");
	     // dateFormat.p
	        try {
	    	    connection = DBUtil.getConnection();
	    	        query= query+" where 1=1";
	    	    	if(qName!=null && qName.length()>0){
	    	    		query=query+" and lower(Question) like '%"+qName.toLowerCase()+"%' ";
	    	    	}
	    	    	if(startDate!=null && startDate.length()>0){
	    	    		query=query+" and CREATED_DATE > '"+dateFormat2.format(new Date(startDate))+"'";
	    	    	}
	    	    	if(endDate!=null && endDate.length()>0){
	    	    		query=query+" and CREATED_DATE < '"+dateFormat2.format(new Date(endDate))+"'";
	    	    	}
	    	    	//System.out.println(query);
	    	    PreparedStatement preparedStatement = connection.prepareStatement(query);
	    	   
	    	    resultSet = preparedStatement.executeQuery();
 
	            while(resultSet.next() ) {
	            	dateFormat2= new SimpleDateFormat("YYYY-MM-DD"); 
					if(i==0){
					data=data+"{\"isSuccess\":\"true\",\"searchData\":[";
					}else{
						data=data+",";
					}
					i++;
					data=data+"{ \"row\": \""+i+ "\", \"question\": \""+resultSet.getString("QUESTION")+"\", \"startdate\": \""+resultSet.getString("CREATED_DATE")+"\", \"expirydate\": \""+resultSet.getString("Expired_Date")+"\",\"pollid\": \""+resultSet.getString("uidpk")+"\"}";
	             }
	    	if(i>0){
	    		data=data+"]}";
	    	}else{
	    		data=data+"{\"isSuccess\":\"true\",\"searchData\":[]}";
	    	}*/
	        } catch (Exception e) {
	        	        	
	            e.printStackTrace();
	            
	        }finally {
	                  //cstmt.close();
	                  try {
	                        if(connection!=null)connection.close();
	                  } catch (SQLException e1) {
	                        // TODO Auto-generated catch block
	                        e1.printStackTrace();
	                  }
	                  connection = null;
	                  try {
	                        if(resultSet!=null)resultSet.close();
	                       
	                  } catch (SQLException e) {
	                        // TODO Auto-generated catch block
	                        e.printStackTrace();
	                  }
	              
	      } 
	        
			return data;
		
	    }

}
