package com.cwp.dao;

import java.sql.*;
import java.util.*;

import com.cwp.rpt.FailedRegisterRpt;
import com.cwp.util.DBUtil;

import oracle.jdbc.OracleTypes;

public class FailedRegisterRptDAO {
	
	private static DBUtil db1;
	private static Connection connection;
	
	public List<FailedRegisterRpt> getReportData(String fromdt, String todt)
	{
		List<FailedRegisterRpt> myDataList = new ArrayList<FailedRegisterRpt>();
		ResultSet resultSet = null;
		Statement statement = null;
		
		try{
	
			db1 = new DBUtil();
			connection = db1.getConnection();
			
			/* PreparedStatement preparedStatement = connection.
			 prepareStatement("select * from USER_PROFILE_LOG WHERE CREATEDDATE>='" + fromdt + "' AND CREATEDDATE<='" + todt + "'");
			
			resultSet = preparedStatement.executeQuery();
			*/
			
			  CallableStatement cstmt = connection.prepareCall("{call GET_FAILED_REGISTER_INFO(?,?,?)}");		           
	             cstmt.setString(1, fromdt);
	             cstmt.setString(2, todt);
	             cstmt.registerOutParameter(3, OracleTypes.CURSOR);
	           
	            cstmt.execute();
	            resultSet =(ResultSet)cstmt.getObject (3);         
			while(resultSet.next())
			{
				
				FailedRegisterRpt reportData = new FailedRegisterRpt();
				
				reportData.setIdNumber(resultSet.getString("ICNUMBER"));
				reportData.setPolicyno(resultSet.getString("POLICYNO"));
				reportData.setEmail(resultSet.getString("EMAIL"));
				reportData.setMobile(resultSet.getString("MOBILE"));
				reportData.setResponsecode(resultSet.getString("RESPONSECODE"));	
				reportData.setLastdate(resultSet.getString("CREATEDDATE"));
				reportData.setFailedCustomerName("TEST");
				myDataList.add(reportData);
				
			}
		}
		catch(Exception e){
			e.printStackTrace(); 
		}
		finally 
		{
			if (resultSet != null) { try { resultSet.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (statement != null) { try { statement.close(); } catch (SQLException e) { e.printStackTrace(); } }
            if (connection != null) { try { connection.close(); } catch (SQLException e) { e.printStackTrace(); } }
		}
		
		return myDataList;
	
	}
	
	public static void main(String args [])
	{
		FailedRegisterRptDAO rdao = new FailedRegisterRptDAO();
		List<FailedRegisterRpt> releaseDataList = rdao.getReportData("2016-08-01","2016-08-02");
		Iterator itr = releaseDataList.iterator();
		while(itr.hasNext())
		{
			FailedRegisterRpt rd = (FailedRegisterRpt) itr.next();
			System.out.println(" " + rd.getIdNumber()+ rd.getLastdate());
		}
		
	}
}
