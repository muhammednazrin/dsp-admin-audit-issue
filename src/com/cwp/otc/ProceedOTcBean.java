package com.cwp.otc;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;



public class ProceedOTcBean {
	
	private String idNumber;
	private int otcode;
	private int otccount;
	private Timestamp expirydt;
	private String error_code;
	private String error_msg;
	private String trxid;
	private int keyinotc;
	private String Fromname;
	private String toNumber;
	private String Message;
	private String smsresponse;
	private Timestamp KeyinOTcDate;
	private Date dtsendotc;
	private int otcid;
	private int regotccount;
	
		
	
	public int getRegotccount() {
		return regotccount;
	}
	public void setRegotccount(int regotccount) {
		this.regotccount = regotccount;
	}
	public int getOtcid() {
		return otcid;
	}
	public void setOtcid(int otcid) {
		this.otcid = otcid;
	}
	public Date getDtsendotc() {
		return dtsendotc;
	}
	public void setDtsendotc(Date dtsendotc) {
		this.dtsendotc = dtsendotc;
	}
	public Timestamp getKeyinOTcDate() {
		return KeyinOTcDate;
	}
	public void setKeyinOTcDate(Timestamp keyinOTcDate) {
		KeyinOTcDate = keyinOTcDate;
	}
	
	public Timestamp getExpirydt() {
		return expirydt;
	}
	public void setExpirydt(Timestamp expirydt) {
		this.expirydt = expirydt;
	}

	public int getOtcode() {
		return otcode;
	}
	public void setOtcode(int otcode) {
		this.otcode = otcode;
	}
	public int getKeyinotc() {
		return keyinotc;
	}
	public void setKeyinotc(int keyinotc) {
		this.keyinotc = keyinotc;
	}
	public String getSmsresponse() {
		return smsresponse;
	}
	public void setSmsresponse(String smsresponse) {
		this.smsresponse = smsresponse;
	}
	public String getFromname() {
		return Fromname;
	}
	public void setFromname(String fromname) {
		Fromname = fromname;
	}
	public String getToNumber() {
		return toNumber;
	}
	public void setToNumber(String toNumber) {
		this.toNumber = toNumber;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}
	
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	
	public int getOtccount() {
		return otccount;
	}
	public void setOtccount(int otccount) {
		this.otccount = otccount;
	}

	public String getError_code() {
		return error_code;
	}
	public void setError_code(String error_code) {
		this.error_code = error_code;
	}
	public String getError_msg() {
		return error_msg;
	}
	public void setError_msg(String error_msg) {
		this.error_msg = error_msg;
	}
	public String getTrxid() {
		return trxid;
	}
	public void setTrxid(String trxid) {
		this.trxid = trxid;
	}
	
	
	

}
