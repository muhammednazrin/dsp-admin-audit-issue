package com.cwp.util;

public class ExcludeURLs {
	
	public static final String MAIN = "/";
	public static final String DOLOGIN = "/login";
	public static final String RESOURCES = "/resources";
	public static final String DASHBOARD = "/dashboard";
	public static final String GETMENU = "/getMenu";
	public static final String SUBMIT_MODULE = "/submitModule";
	public static final String DOLOGOUT = "/doLogout";
	//public static final String UAM_LOGIN_URL_LINK = "https://www.etiqa.com.my/dspadmin/doLogout";
	public static final String UAM_LOGIN_URL_LINK = "localhost://8080/doLogout";
	
	
	

}
