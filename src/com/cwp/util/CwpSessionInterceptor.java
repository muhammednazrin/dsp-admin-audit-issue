package com.cwp.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringEscapeUtils;

import com.cwp.util.XSSFilter.XSSRequestWrapper;

public class CwpSessionInterceptor implements Filter {

	

	@Override
	public void destroy() {
		 System.out.println("XSSPreventionFilter: destroy()");
	}

	@Override
	public void doFilter(ServletRequest requestq, ServletResponse responseq, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest request = (HttpServletRequest) requestq;
        HttpServletResponse response = (HttpServletResponse) responseq;
//		String requestURL=((HttpServletRequest) request).getRequestURI().substring(request.getContextPath().length());
		String path =  request.getRequestURI().substring(request.getContextPath().length());
		System.out.println("request url for CWP :"+path);
		HttpSession session = request.getSession();
		Boolean exception = false;

/*		
		if (path.toLowerCase().contains(ExcludeURLs.DOLOGIN.toLowerCase())
				) {

			exception = true;

		}

		if (exception == false) {
			if (session.getAttribute("CWPmoduleId") == null) {
				System.out.println("logout from claimLogin intercepter moduleId is null ");
//				response.setHeader("Location", ExcludeURLs.UAM_LOGIN_URL_LINK);
				response.sendRedirect(ExcludeURLs.UAM_LOGIN_URL_LINK);
				return;
//				response.sendRedirect(ExcludeURLs.UAM_LOGIN_URL_LINK);
				//chain.doFilter(request, response);
			} 
				Integer moduleid = Integer.valueOf((String) (session.getAttribute("CWPmoduleId")));
				System.out.println("Claim Module Id Is     :" + moduleid);				
				System.out.println("check level   :"+session.getAttribute("level_1"));
				if (session.getAttribute("level_1") == null) {
					System.out.println("level_1  is empty");
//					RequestDispatcher dd = request.getRequestDispatcher("admin-login");
					 response.sendRedirect(request.getContextPath() + "/Userdologout");
					 return;
//					dd.forward(request, response);
				}
		}*/
		      chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		System.out.println("dhana printing this from cwpsession interceptor"+"XSSPreventionFilter: init()");
		
	}


}
