package com.cwp.util;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class DBUtil {
	
	public static Connection getConnection() throws Exception
	  {
		 Context ctx = null;
	     Connection conn = null;
		try
		{
			
		   ctx = new InitialContext();
	            
         DataSource ds = (DataSource) ctx.lookup("jdbc/csuser");
         conn = ds.getConnection();
       //   msg ="connected";
		     return conn;
		}
		catch (SQLException e)
		{
		throw e;
		}
		catch (Exception e)
		{
		throw e;
		}
	  }

	
	
}