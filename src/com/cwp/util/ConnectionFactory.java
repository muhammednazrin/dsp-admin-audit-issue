package com.cwp.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class ConnectionFactory {
	     
	private static Context ctx = null;
	
	private static DataSource ds = null;

	public static Connection getConnection()
	  {
		
	     Connection conn = null;
		try
		{
			if(null == ctx){
				
				ctx = new InitialContext();
		        ds = (DataSource) ctx.lookup("jdbc/dspuser");
		        System.out.println("Context Initialized" + ctx);
		        System.out.println("Datasource Initialized" + ds);
			}
			 
			if(null != ds){
	         conn = ds.getConnection();
			 }
			 
	         if(null == conn){
	        	 System.out.println("ERROR: Unable to Connect to Database. Please make sure that the JNDI configured properly.");
	         }
	         System.out.println("A DB Connection is returned");
		     return conn;
		}
		catch (SQLException e)
		{
		//throw e;
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return conn;
	  } 

   //static reference to itself


}
