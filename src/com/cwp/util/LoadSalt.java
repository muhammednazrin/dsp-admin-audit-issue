package com.cwp.util;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class LoadSalt implements Filter {
   
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException {
    	System.out.println("LoadSalt : START");
    	HttpServletRequest httpReq = (HttpServletRequest) request;
    	HttpServletResponse httpResp = (HttpServletResponse) response;
    	
    	if (!httpReq.getMethod().equalsIgnoreCase("POST") ) {
			// Not a POST - allow the request
			CSRFTokenManager.getTokenForSession(httpReq.getSession());
			//return true;
		} else {
			// This is a POST request - need to check the CSRF token
			String sessionToken = CSRFTokenManager.getTokenForSession(httpReq.getSession());
			String requestToken = CSRFTokenManager.getTokenFromRequest(httpReq);
			if (sessionToken.equals(requestToken)) {
				System.out.println(requestToken);
			} else {
				
				/*httpReq.getSession().invalidate();
				System.out.println("Session Invalidated");
				httpResp.sendRedirect("admin-login.jsp?result=fail");
				((HttpServletResponse) response).sendError(HttpServletResponse.SC_FORBIDDEN, "Bad or missing CSRF value");
				System.out.println("LoadSalt : END");*/
			}
		}
    	
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }
}