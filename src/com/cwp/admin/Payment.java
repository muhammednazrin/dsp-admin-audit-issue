package com.cwp.admin;

public class Payment {
	private String customerName;
	private String mobileNumber;
	private String emailAddress;
	private String icNumber;
	private String paymentDate;
	private String policyNumber;
	private String paymentAmount;
	private String paymentStatus;
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getIcNumber() {
		return icNumber;
	}
	public void setIcNumber(String icNumber) {
		this.icNumber = icNumber;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	public String getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	@Override
	public String toString() {
		return "Payment [customerName=" + customerName + ", mobileNumber=" + mobileNumber + ", emailAddress="
				+ emailAddress + ", icNumber=" + icNumber + ", paymentDate=" + paymentDate + ", policyNumber="
				+ policyNumber + ", paymentAmount=" + paymentAmount + ", paymentStatus=" + paymentStatus + "]";
	}
	
	
	

}
