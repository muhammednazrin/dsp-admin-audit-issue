package com.cwp.admin;

import java.util.Date;

public class Parameters {
	
	private int id;
	
	private String vchcode;
	
	private String vchdescription;
	
	private String vchparametertype;
	
	private String vchstatus;
	
	private String vchvalue;
	
	private String vchcreateby;
	
	private Date createDate;
	
	private String vchupdateby;
	
	private Date updateDate;
	
	public String getStatusDesc(){
		
		if(vchstatus.equals(0)){
			return "In Active";
		}
		
		return "Active";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getVchcode() {
		return vchcode;
	}

	public void setVchcode(String vchcode) {
		this.vchcode = vchcode;
	}

	public String getVchdescription() {
		return vchdescription;
	}

	public void setVchdescription(String vchdescription) {
		this.vchdescription = vchdescription;
	}

	public String getVchparametertype() {
		return vchparametertype;
	}

	public void setVchparametertype(String vchparametertype) {
		this.vchparametertype = vchparametertype;
	}

	public String getVchstatus() {
		return vchstatus;
	}

	public void setVchstatus(String vchstatus) {
		this.vchstatus = vchstatus;
	}

	public String getVchvalue() {
		return vchvalue;
	}

	public void setVchvalue(String vchvalue) {
		this.vchvalue = vchvalue;
	}

	public String getVchcreateby() {
		return vchcreateby;
	}

	public void setVchcreateby(String vchcreateby) {
		this.vchcreateby = vchcreateby;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getVchupdateby() {
		return vchupdateby;
	}

	public void setVchupdateby(String vchupdateby) {
		this.vchupdateby = vchupdateby;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	

}
