package com.cwp.admin;

import java.util.List;

public class AccupdateBean {
	
	private String ic_number;
	private String acc_no;
	private String bank_name;
	private String policyno;
	private String dspqqid;
	private String email;
	private String contactno;
	private String updateby;
	private String branch_name;
	private String branch_code;
	private String error_code;
	private String error_msg;
	private String trx_id;
	private long id;
	
	private List <AccountInfoBean> accinfo_list;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getIc_number() {
		return ic_number;
	}
	public void setIc_number(String ic_number) {
		this.ic_number = ic_number;
	}
	public String getAcc_no() {
		return acc_no;
	}
	public void setAcc_no(String acc_no) {
		this.acc_no = acc_no;
	}
	public String getBank_name() {
		return bank_name;
	}
	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	public String getPolicyno() {
		return policyno;
	}
	public void setPolicyno(String policyno) {
		this.policyno = policyno;
	}
	
	public String getDspqqid() {
		return dspqqid;
	}
	public void setDspqqid(String dspqqid) {
		this.dspqqid = dspqqid;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContactno() {
		return contactno;
	}
	public void setContactno(String contactno) {
		this.contactno = contactno;
	}
	
	public String getUpdateby() {
		return updateby;
	}
	public void setUpdateby(String updateby) {
		this.updateby = updateby;
	}
	
	public String getBranch_name() {
		return branch_name;
	}
	public void setBranch_name(String branch_name) {
		this.branch_name = branch_name;
	}
	public String getBranch_code() {
		return branch_code;
	}
	public void setBranch_code(String branch_code) {
		this.branch_code = branch_code;
	}
	public String getError_code() {
		return error_code;
	}
	public void setError_code(String error_code) {
		this.error_code = error_code;
	}
	public String getError_msg() {
		return error_msg;
	}
	public void setError_msg(String error_msg) {
		this.error_msg = error_msg;
	}
	public String getTrx_id() {
		return trx_id;
	}
	public void setTrx_id(String trx_id) {
		this.trx_id = trx_id;
	}
	public List<AccountInfoBean> getAccinfo_list() {
		return accinfo_list;
	}
	public void setAccinfo_list(List<AccountInfoBean> accinfo_list) {
		this.accinfo_list = accinfo_list;
	}
	
	

}
