package com.cwp.admin;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import com.cwp.util.DBUtil;

@WebServlet("/FileUploadServlet")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
		maxFileSize = 1024 * 1024 * 10, // 10MB
		maxRequestSize = 1024 * 1024 * 50)

public class FileUploadServlet extends HttpServlet {
	    
	    private static final String SAVE_DIR = "../../../../../../../configdsp/CWP/";
	    private static final String DB_SAVE_DIR = "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/configdsp/CWP/";
			//"/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/configdsp/CWP/";

	    protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		String savePath = SAVE_DIR;
		
		File fileSaveDir = new File(savePath);
		
		System.out.println("savePath" + savePath);
		System.out.println("fileSaveDir" + fileSaveDir.toString());
		
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdir();
		}

		Part part = request.getPart("file");
		String fileName = extractFileName(part);
	    String[] output = fileName.split("/");

	    System.out.println("File Name : output[output.length-1] : " + output[output.length-1]);
		
		/*
		 * if you may have more than one files with same name then you can
		 * calculate some random characters and append that characters in
		 * fileName so that it will make your each image name identical.
		 */
		
		part.write(savePath + output[output.length-1]);
		System.out.println("PART PRINT : " + part);
		
		/*
		 * //You need this loop if you submitted more than one file for (Part
		 * part : request.getParts()) { String fileName = extractFileName(part);
		 * part.write(savePath + File.separator + fileName); }
		 */
		
		
		String finalpath = DB_SAVE_DIR + output[output.length-1];

		try {
			logtraker_details_insert(finalpath);
			System.out.print("Record Inserted Successfully");

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		request.getRequestDispatcher("PreRegistration.jsp").forward(request, response);
	}

	private String extractFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] items = contentDisp.split(";");
		for (String s : items) {
			if (s.trim().startsWith("filename")) {
				return s.substring(s.indexOf("=") + 2, s.length() - 1);
			}
		}
		return "";
	}

	private static DBUtil db1;

	private static Connection connection;

	public static void logtraker_details_insert(String Filename) {
		ResultSet rs = null;
		Statement statement = null;
		try {
			db1 = new DBUtil();
			connection = db1.getConnection();
			CallableStatement ps = connection.prepareCall("{call CWP_INSERT_AutoRegistration(?,?)}");
			ps.setString(1, Filename);
			ps.setString(2, "admin");
			ps.executeUpdate();
			System.out.println("Success");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
