package com.cwp.admin;

public class AccountInfoBean {
	
	private String policyno;
	private String icnumber;
	private String bankname;
	private String bankaccno;
	private String branchname;
	private String branchcode;
	private String emailaddress;
	private String contactno;
	private String updateby;
	private Long id;
	
	
	public String getUpdateby() {
		return updateby;
	}
	public void setUpdateby(String updateby) {
		this.updateby = updateby;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPolicyno() {
		return policyno;
	}
	public void setPolicyno(String policyno) {
		this.policyno = policyno;
	}
	public String getIcnumber() {
		return icnumber;
	}
	public void setIcnumber(String icnumber) {
		this.icnumber = icnumber;
	}
	public String getBankname() {
		return bankname;
	}
	public void setBankname(String bankname) {
		this.bankname = bankname;
	}
	public String getBankaccno() {
		return bankaccno;
	}
	public void setBankaccno(String bankaccno) {
		this.bankaccno = bankaccno;
	}
	public String getBranchname() {
		return branchname;
	}
	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}
	public String getBranchcode() {
		return branchcode;
	}
	public void setBranchcode(String branchcode) {
		this.branchcode = branchcode;
	}
	public String getEmailaddress() {
		return emailaddress;
	}
	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}
	public String getContactno() {
		return contactno;
	}
	public void setContactno(String contactno) {
		this.contactno = contactno;
	}
	
	
	

}
