package com.cwp.admin;

public class User {
	private int ID;
    private String Username;
    private String Createddate;
    private String Createdby;
    private String Isactive;
    private String Response;
    private int IsVarified;
    private String FullName; 
    
    
	public int getIsVarified() {
		return IsVarified;
	}
	public void setIsVarified(int isVarified) {
		IsVarified = isVarified;
	}
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getUsername() {
		return Username;
	}
	public void setUsername(String username) {
		Username = username;
	}
	public String getCreateddate() {
		return Createddate;
	}
	public void setCreateddate(String createddate) {
		Createddate = createddate;
	}
	public String getCreatedby() {
		return Createdby;
	}
	public void setCreatedby(String createdby) {
		Createdby = createdby;
	}
	
	public String getResponse() {
		return Response;
	}
	public void setResponse(String response) {
		Response = response;
	}
	public String getIsactive() {
		return Isactive;
	}
	public void setIsactive(String isactive) {
		Isactive = isactive;
	}
	public String getFullName() {
		return FullName;
	}
	public void setFullName(String fullName) {
		FullName = fullName;
	}

    
	
  
    
    
    
}
