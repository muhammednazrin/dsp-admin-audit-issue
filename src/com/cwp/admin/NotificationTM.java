package com.cwp.admin;

public class NotificationTM {
	
	//private int notificationId;
	private String notifyDesc;
	private String notifyType;
	private int notifyid;
	
	
	
	
	public int getNotifyid() {
		return notifyid;
	}
	public void setNotifyid(int notifyid) {
		this.notifyid = notifyid;
	}
	
	public String getNotifyDesc() {
		return notifyDesc;
	}
	public void setNotifyDesc(String notifyDesc) {
		this.notifyDesc = notifyDesc;
	}
	public String getNotifyType() {
		return notifyType;
	}
	public void setNotifyType(String notifyType) {
		this.notifyType = notifyType;
	}
	
	

}
