package com.cwp.admin;

public class ServiceRequestBO {

	private String customerName;
	private String mobileNumber;
	private String emailAddress;
	private String category;
	private String subCategory;
	private String icNumber;
	private String serviceDescription;
	private String serviceRequestNumber;
	private String serviceDate;

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public String getIcNumber() {
		return icNumber;
	}

	public void setIcNumber(String icNumber) {
		this.icNumber = icNumber;
	}

	public String getServiceDescription() {
		return serviceDescription;
	}

	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}

	public String getServiceRequestNumber() {
		return serviceRequestNumber;
	}

	public void setServiceRequestNumber(String serviceRequestNumber) {
		this.serviceRequestNumber = serviceRequestNumber;
	}

	public String getServiceDate() {
		return serviceDate;
	}

	public void setServiceDate(String serviceDate) {
		this.serviceDate = serviceDate;
	}

	public void reinitializeClass() {
		this.customerName = null;
		this.mobileNumber = null;
		this.emailAddress = null;
		this.category = null;
		this.subCategory = null;
		this.icNumber = null;
		this.serviceDescription = null;
		this.serviceRequestNumber = null;
		this.serviceDate = null;
	}

	@Override
	public String toString() {
		return "WebSRReport [customerName=" + customerName + ", mobileNumber=" + mobileNumber + ", emailAddress="
				+ emailAddress + ", category=" + category + ", subCategory=" + subCategory + ", icNumber=" + icNumber
				+ ", serviceDescription=" + serviceDescription + ", serviceRequestNumber=" + serviceRequestNumber
				+ ", serviceDate=" + serviceDate + "]";
	}

}
