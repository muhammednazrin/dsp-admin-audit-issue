package com.cwp.sms;

import java.rmi.server.UID;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.cwp.otc.ProceedOTcBean;
import com.cwp.sms.SMSDriver;
import com.cwp.util.DBUtil;

import oracle.jdbc.OracleTypes;



public class ProceedOTCCPF {
	
	 //INSERT OTC details
	 
	   
		public  ProceedOTcBean SendOTC(String icnumber,
				String fromName,
				String mobile,
				String message,
				String trx_id){
			
		     System.out.println("Faroo SMS Testing" + icnumber);
			ProceedOTcBean crgb =new ProceedOTcBean();
			  UID trxID = new UID();
				
				crgb.setTrxid(trxID.toString());
				crgb.setIdNumber(icnumber);
				  Random r = new Random( System.currentTimeMillis() );
			      int otcgen = ((1 + r.nextInt(2)) * 10000 + r.nextInt(10000));
			     // DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		         //   Date parsed = new Date();
		          
		             message ="Etiqa Customer Web Portal: Activation Code Requested for Registration. AC: "+otcgen;
		           
		                     
			
		            crgb.setFromname(fromName);
		            crgb.setToNumber(mobile);
		            crgb.setMessage(message);
		            SMSDriver sms = new SMSDriver();
		            
		            crgb = sms.SendSMS(crgb.getFromname(), crgb.getToNumber(), crgb.getMessage());
		            if(crgb.getSmsresponse().equalsIgnoreCase("otcsend")) {
		           	 
		           	 crgb.setOtcode(otcgen);
		  		          crgb.setOtccount(1);
		  		          crgb.setRegotccount(1);
		  		        crgb.setIdNumber(icnumber);
		  		        System.out.println(icnumber);
		           	 crgb = queryDatabaseinsertOTC(crgb);
		            
		            }
		            
		          
		             
		             return crgb;
		             
		             /*SMSDriver d = new SMSDriver();
			            
			            String a = d.callSMS(crgb.getFromname(), crgb.getToNumber(), crgb.getMessage());
			           
			            System.out.println(a);
			            
			            if(crgb.getSmsresponse().equalsIgnoreCase("otcsend")) {
			            	 
			            	 crgb.setOtcode(otcgen);
					          crgb.setOtccount(1);
					          crgb.setRegotccount(1);
			            	 crgb = queryDatabaseinsertOTC(crgb);
			             
			             }*/
		}
				
			
				
		
		
		 /* ProceedOTcBean crgb = new ProceedOTcBean();

			UID trxID = new UID();
			if(trx_id == null || "".equalsIgnoreCase(trx_id))
				trx_id = trxID.toString();
			crgb.setTrxid(trx_id);
			crgb.setIdNumber(idno);
			  Random r = new Random( System.currentTimeMillis() );
		      int otcgen = ((1 + r.nextInt(2)) * 10000 + r.nextInt(10000));
		      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	            Date parsed = new Date();
	            if(message != null || "".equalsIgnoreCase(message)){
	            	message ="Etiqa Customer Portal: OTC Requested for Registration. OTC: "+otcgen;
	            }
	            java.sql.Date expirydt= new java.sql.Date(parsed.getTime());
	           
		   // crgb = queryDatabaseICNoVal(crgb);
		  //  if(crgb.getError_code() == "D0000") {
		    	
		    	//Send OTC DRiver
		      crgb.setFromname(fromname);
		      if(crgb.getToNumber() == null || "".equalsIgnoreCase(crgb.getToNumber())){
		         crgb.setToNumber(toNumber);
		      }
		      crgb.setMessage(message);
		      crgb.setIdNumber(idno);
		    
		      //crgb = queryDatabaseOTCInfo(crgb);
				//if(crgb.getError_code() == "D0000"){
					
					 crgb.setOtcode(otcgen);
			          crgb.setOtccount(1);
			          crgb.setIdNumber(idno);
			          //crgb.setRegotccount(regotccount);
			        
			          crgb.setDtsendotc(expirydt);
			
			          crgb = queryDatabaseinsertOTC(crgb);
			          
			          System.out.println(crgb.getFromname());
			          System.out.println(crgb.getToNumber());
			          System.out.println(crgb.getMessage());
			          
		             SMSDriver sms = new SMSDriver();
		             crgb = sms.SendSMS(crgb.getFromname(), crgb.getToNumber(), crgb.getMessage());
		             if(crgb.getSmsresponse().equalsIgnoreCase("otcsend")) {
		                 
		            	 
		            	 crgb =queryDatabaseSuccess(crgb);
		            	 crgb.setError_code("D0000");
		            	 crgb.setError_msg("success");
		            	 
		            	 
				      }
		       //  }
			  
		   // }
*/			
			
	   
	    
	   //Check the success message
		 public static ProceedOTcBean queryDatabaseSuccess(ProceedOTcBean otcInfo){
			   try {
	    	if(otcInfo.getSmsresponse().equalsIgnoreCase("otcsend")) {
	    		
	    		otcInfo.setError_code("D0000");
	    		otcInfo.setError_msg("success");
	    	
	    	}
			   } catch (Exception e) {
		            // a failure occurred log message;
		        	System.out.println("[ERROR]"+new Date()+"Exception " +e.getMessage()+" "+ e.getCause());
		        
		            e.printStackTrace();
		            }finally {
		                  //cstmt.close();
		      } 
				return otcInfo;
	      
	    }
	//Check the ICnumber validation
		 public static ProceedOTcBean queryDatabaseICNoVal(ProceedOTcBean otcInfo){
		        Connection connection = null;
		     
		        ResultSet rs = null;
		        try {
		        	DBUtil database= new DBUtil();
		    	    connection = database.getConnection();
		    	    CallableStatement cstmt = connection.prepareCall("{call USER_PROFILE_INFO_BYICNO(?,?)}");
			           
		            cstmt.setString(1, otcInfo.getIdNumber());
		            cstmt.registerOutParameter(2, OracleTypes.CURSOR);
		           
		            cstmt.execute();
		            rs =(ResultSet)cstmt.getObject (2);
		           
		            
		            DateFormat dateFormat = new SimpleDateFormat("MMM dd,yyyy 'at' HH:mm:ss a");
		            Date date = new Date();
		     	    String todate = dateFormat.format(date);
		            // print the results
		        	
		            String errmsg ="";
		            if (!rs.next() ) {
		            	
		            	otcInfo.setError_code("D0018");
		            	otcInfo.setError_msg("Username is not Exist");
		            	
		            } else {
		       
		        	     otcInfo.setToNumber(rs.getString(15));
		        	     otcInfo.setError_code("D0000");
				         otcInfo.setError_msg("Success");
		            }
		         //System.out.println(errmsg);
		         
		        
		            
		         
		           // rs =(ResultSet)cstmt.getObject (5);
		            
		        
		           
		        } catch (Exception e) {
		            // a failure occurred log message;
		        	System.out.println("[ERROR]"+new Date()+"Exception " +e.getMessage()+" "+ e.getCause());
		        	otcInfo.setError_code("0002");
		        	otcInfo.setError_msg(e.getMessage()+" error insert");
		        	
		            e.printStackTrace();
		            }finally {
		                  //cstmt.close();
		                  try {
		                        if(connection!=null)connection.close();
		                  } catch (SQLException e1) {
		                        // TODO Auto-generated catch block
		                        e1.printStackTrace();
		                  }
		                  connection = null;
		                  try {
		                        if(rs!=null)rs.close();
		                       
		                  } catch (SQLException e) {
		                        // TODO Auto-generated catch block
		                        e.printStackTrace();
		                  }
		              
		      } 
				return otcInfo;
			}
	  
		 
		 
		 //Retrive OTC Information
		 
			public static ProceedOTcBean queryDatabaseOTCInfo(ProceedOTcBean otcInfo){
		        Connection connection = null;
		     
		        ResultSet rs = null;
		        try {
		        	DBUtil database= new DBUtil();
		    	    connection = database.getConnection();
		            CallableStatement cstmt = connection.prepareCall("{call REGENERATE_OTC_INFO(?,?)}");
		            cstmt.setString(1, otcInfo.getIdNumber());
		            cstmt.registerOutParameter(2, OracleTypes.CURSOR);
		           
		            cstmt.execute();
		            int regencnt =0;
		            rs =(ResultSet)cstmt.getObject (2);
		         	//System.out.println("result getting");
		            otcInfo.setRegotccount(1);
		            if (!rs.next() ) {
		           //  System.out.println("result getting1");
		            	
		            	//otcInfo.setError_code("D0000");
			           // otcInfo.setError_msg("Success");
			            
		            }else {
		            	//System.out.println("result getting1" +rs.getInt(4));
		           // while (rs.next()) {
		            	regencnt = rs.getInt(8);
		            	
		            	
		             //}
		        
		          if(regencnt ==3){
		        	  otcInfo.setError_code("D0020");
		        	  otcInfo.setError_msg("You cannot regenerate OTC for more than 3 times a day. Please try again after 24 hours");
		          }
		          else {
		        	  regencnt =regencnt+1;
		            otcInfo.setRegotccount(regencnt);
		            otcInfo.setError_code("D0000");
		            otcInfo.setError_msg("Success");
		          }
		        }
		          
		        } catch (Exception e) {
		            // a failure occurred log message;
		        	System.out.println("[ERROR]"+new Date()+"Exception " +e.getMessage()+" "+ e.getCause());
		        	otcInfo.setError_code("D9999");
		        	otcInfo.setError_msg(e.getMessage());
		            e.printStackTrace();
		            }finally {
		                  //cstmt.close();
		                  try {
		                        if(connection!=null)connection.close();
		                  } catch (SQLException e1) {
		                        // TODO Auto-generated catch block
		                        e1.printStackTrace();
		                  }
		                  connection = null;
		                  try {
		                        if(rs!=null)rs.close();
		                       
		                  } catch (SQLException e) {
		                        // TODO Auto-generated catch block
		                        e.printStackTrace();
		                  }
		              
		      } 
				return otcInfo;
			}
			
		 
	//Insert OTC information to WCS db
		 
		 public static ProceedOTcBean queryDatabaseinsertOTC(ProceedOTcBean otcInfo){
		        Connection connection = null;
		     
		        ResultSet rs = null;
		        try {
		        	DBUtil database= new DBUtil();
		    	    connection = database.getConnection();
		    	    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		            Date date = new Date();
		     	    //String todate = dateFormat.format(date);
		            CallableStatement cstmt = connection.prepareCall("{call INSERT_USER_ACTIVATE_OTC(?,?,?,?)}");
		           
		          
		            Date parsed = otcInfo.getDtsendotc();
		         //   java.sql.Date data = new java.sql.Date(parsed.getTime());
		            cstmt.setString(1, otcInfo.getIdNumber());
		            cstmt.setInt(2, otcInfo.getOtcode());
		            cstmt.setInt(3, otcInfo.getOtccount());
		            cstmt.setInt(4, otcInfo.getRegotccount());
		          
		         
		        
		            cstmt.executeUpdate();
		            otcInfo.setError_code("D0000");
		            otcInfo.setError_msg("Success");
		            
		         
		           // rs =(ResultSet)cstmt.getObject (5);
		            
		        
		           
		        } catch (Exception e) {
		            // a failure occurred log message;
		        	System.out.println("[ERROR]"+new Date()+"Exception " +e.getMessage()+" "+ e.getCause());
		        	otcInfo.setError_code("0002");
		        	otcInfo.setError_msg(e.getMessage());
		            e.printStackTrace();
		            }finally {
		                  //cstmt.close();
		                  try {
		                        if(connection!=null)connection.close();
		                  } catch (SQLException e1) {
		                        // TODO Auto-generated catch block
		                        e1.printStackTrace();
		                  }
		                  connection = null;
		                  try {
		                        if(rs!=null)rs.close();
		                       
		                  } catch (SQLException e) {
		                        // TODO Auto-generated catch block
		                        e.printStackTrace();
		                  }
		              
		      } 
				return otcInfo;
			}
		 
		 
		
		 
	  /*public static void main (String[] args)
		     {
			  ProceedOTcBean crgb =new ProceedOTcBean();
			  UID trxID = new UID();
				
				crgb.setTrxid(trxID.toString());
				crgb.setIdNumber("790816085555");
				  Random r = new Random( System.currentTimeMillis() );
			      int otcgen = ((1 + r.nextInt(2)) * 10000 + r.nextInt(10000));
			      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		            Date parsed = new Date();
		          
		            String message ="Etiqa Customer Portal: OTC Requested for Registration. OTC: "+otcgen;
		           
		            java.sql.Date expirydt= new java.sql.Date(parsed.getTime());		           
			
		            crgb.setFromname("farooq");
		            crgb.setToNumber("0060179879314");
		            crgb.setMessage("");
		            SMSDriver sms = new SMSDriver();
		             crgb = sms.SendSMS(crgb.getFromname(), crgb.getToNumber(), crgb.getMessage());
		             if(crgb.getSmsresponse().equalsIgnoreCase("otcsend")) {
		            	 
		            	 crgb.setOtcode(otcgen);
				          crgb.setOtccount(1);
				          crgb.setRegotccount(1);
		            	 crgb = queryDatabaseinsertOTC(crgb);
		             
		             }
				
				System.out.println(crgb.getError_code()+" "+crgb.getSmsresponse());
		     }*/

}
