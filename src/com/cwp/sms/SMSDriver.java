package com.cwp.sms;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;

import java.util.Properties;

import com.cwp.otc.ProceedOTcBean;

/*
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
*/


import java.io.InputStream;
import java.net.URL;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLConnection;


public class SMSDriver 
{
	private static Properties configProp = new Properties();
	@WebMethod
	@WebResult(name="SendSMS")
	public ProceedOTcBean SendSMS(String FromName,String ToNumber,String Message)
	{
		String result ="";
		  ProceedOTcBean crgb = new ProceedOTcBean();
		  System.out.println("Admin OTC SMS Started:- " +ToNumber);
		result =callSMS(FromName,ToNumber,Message);
		//if(result.contains("com.mashape.unirest.http.HttpResponse")){
		
		if(result.contains("PENDING_ENROUTE")){
		    crgb.setSmsresponse("otcsend");
		}
		return crgb;
	}
	public static String callSMS(String FromName,String ToNumber,String Message)
	{
		
		String retun_response = null;
	/*	try {
			 System.out.println("Admin OTC SMS call sms:- " );
	        //String strFormed = "{\"from\":\""+ FromName + "\",\"to\":\""+ ToNumber + "\",\"text\":\""+ Message + "\"}";
		
	       
	        
			    String strFromName=FromName + " : Admin"; //"eTiQa";
		        //String ToNumber=strMobile;//"60183138006";//"601139839818";
		        //String Message=strMessage; //"Hello Pavan Kumar";
		        
				String strFormed = "{\"from\":\""+ strFromName + "\",\"to\":\""+ ToNumber + "\",\"text\":\""+ Message + "\"}";
				 System.out.println("Admin OTC SMS Printed:- " + strFormed );
				
				 //SMS through OHS
				 String url ="https://10.252.148.21:4443/cgi-bin/smsp.cgi"; 
				
				
				// Direct SMS Infobip access
				//"https://api.infobip.com/sms/1/text/single";// + strFormed;
				URL obj = new URL(url);
			
		                System.out.println("Preparing the request .... ");

				URLConnection con = obj.openConnection();

				//add reuqest header
				
				//con.setRequestMethod("POST");
				con.addRequestProperty("x-auth-token", "5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8");
				con.addRequestProperty("content-type", "application/json");
				con.addRequestProperty("accept", "application/json");
				//con.setRequestProperty("body", strFormed);
			
				//String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";
				
				// Send post request
				con.setDoOutput(true);
				DataOutputStream wr = new DataOutputStream(con.getOutputStream());
				wr.writeBytes(strFormed);
				wr.flush();
				wr.close();

			        System.out.println("Connecting to SMS Gateway .... ");
		
				BufferedReader in = new BufferedReader(
				        new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer respSMS = new StringBuffer();
		
				while ((inputLine = in.readLine()) != null) {
					respSMS.append(inputLine);
				}
				in.close();

				retun_response=respSMS.toString();
			
			
		} 
		catch (Exception e) {
			retun_response=e.getMessage();
			e.printStackTrace();
		}*/
		
		
        retun_response=MySendSMS(FromName,ToNumber,Message);
		
		System.out.println("------------- SMS Response --------------");
		System.out.println(retun_response);
		return retun_response;
	}
	
	
	public static String MySendSMS(String FromName,String ToNumber,String Message)
	{
        String respStr="";
		
		try
		{
			//System.out.println("Email Message: " + Message);
			//String strEmailOutput=SendEmail("pavankumar@absecmy.com","OTC through email",Message);		
			//System.out.println("Email Output: " + strEmailOutput);
			
			/*
	        String FromName="eTiQa-Pavan";
	        String ToNumber="60183138006";
	        String Message="Hello Pavan";
	        */
	        
			String strFormed = "{\"from\":\""+ FromName + "\",\"to\":\""+ ToNumber + "\",\"text\":\""+ Message + "\"}";
			
			/*InputStream inConfig = (new SMSDriver()).getClass().getClassLoader().getResourceAsStream("com/etqa/sms/config.properties");
	        try {
	        	configProp.load(inConfig);
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        */
	       // String smsURL=configProp.getProperty("cwpSmsUrl_OHS1");
			//String smsURL="https://172.29.124.1:4443/cgi-bin/smsp.cgi";
			String smsURL="https://10.252.148.21:4443/cgi-bin/smsp.cgi";
	        System.out.println("----------SMS URL---------");
	        System.out.println("url1 "+smsURL);
			
	        //String strFormed = "?from=" + FromName + "&to=" + ToNumber + "&text="+ Message;
	        
	        System.out.println("----------SMS Parameters ---------");
	        System.out.println(strFormed);
	        
			String url = smsURL; //+ strFormed;
			URL obj = new URL(url);
			
			System.out.println("----------Connecting to SMS URL---------");
			URLConnection con = obj.openConnection();
	
			//add reuqest header
			con.addRequestProperty("x-auth-token", "5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8");
			con.addRequestProperty("content-type", "application/json");
			con.addRequestProperty("accept", "application/json");
			
			//String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";
			
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(strFormed);
			wr.flush();
			wr.close();
	        
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			
			
			System.out.println("----------SMS URL has been called successfully ---------");
						
			respStr=response.toString();
			
			System.out.println("----------SMS URL Response ---------");
			System.out.println(respStr);
			
		}
		catch(Exception ex)
		{
			System.out.println("----------SMS ERROR Response ---------");
			respStr=ex.getMessage();
			
			try
			{
				//System.out.println("Email Message: " + Message);
				//String strEmailOutput=SendEmail("pavankumar@absecmy.com","OTC through email",Message);		
				//System.out.println("Email Output: " + strEmailOutput);
				
				/*
		        String FromName="eTiQa-Pavan";
		        String ToNumber="60183138006";
		        String Message="Hello Pavan";
		        */
		        
				String strFormed = "{\"from\":\""+ FromName + "\",\"to\":\""+ ToNumber + "\",\"text\":\""+ Message + "\"}";
				
			/*	InputStream inConfig = (new SMSDriver()).getClass().getClassLoader().getResourceAsStream("com/ocs/otc/config.properties");
		        try {
		        	configProp.load(inConfig);
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		        
		        String smsURL=configProp.getProperty("cwpSmsUrl_OHS2");
		        */
				String smsURL="https://10.252.148.24:4443/cgi-bin/smsp.cgi";
		        System.out.println("----------SMS URL---------");
		        System.out.println("url2 "+smsURL);
				
		        //String strFormed = "?from=" + FromName + "&to=" + ToNumber + "&text="+ Message;
		        
		        System.out.println("----------SMS Parameters ---------");
		        System.out.println(strFormed);
		        
				String url = smsURL; //+ strFormed;
				URL obj = new URL(url);
				
				System.out.println("----------Connecting to SMS URL---------");
				URLConnection con = obj.openConnection();
		
				//add reuqest header
				con.addRequestProperty("x-auth-token", "5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8");
				con.addRequestProperty("content-type", "application/json");
				con.addRequestProperty("accept", "application/json");
				
				//String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";
				
				// Send post request
				con.setDoOutput(true);
				DataOutputStream wr = new DataOutputStream(con.getOutputStream());
				wr.writeBytes(strFormed);
				wr.flush();
				wr.close();
		        
				BufferedReader in = new BufferedReader(
				        new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
		
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				
				
				System.out.println("----------SMS URL has been called successfully ---------");
							
				respStr=response.toString();
				
				System.out.println("----------SMS URL Response ---------");
				System.out.println(respStr);
				
			}
			catch(Exception ex1)
			{
				System.out.println("----------SMS ERROR Response ---------");
				respStr=ex1.getMessage();
				
				
			}
			
			
		}
		
		System.out.println(respStr);
		
		return respStr;
	}
	public static void main (String[] args)
	{
		//String Results =  callSMS("Absec", "60179879314", "Etiqa SMS");
		ProceedOTcBean result = (new SMSDriver()).SendSMS("eTiQa-Test-SMS", "60162293141", "Test SMS from CPF through CWP");
		   
		///String strEmailOutput=SendEmail("pavankumar@absecmy.com","Test email from CPF-OTC","Test email");		
		//System.out.println("Email Output: " + strEmailOutput);
	    
		System.out.println(result.getMessage());
	}
	

}