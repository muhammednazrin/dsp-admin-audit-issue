package com.spring.VO;

public class DashBoard_Renewal {

	private String fromdate;
	private String F_Renewal_Retention_Motor;
	private String selectvalue;

	// Extra
	private String F_WTC_TotalPoliciesSold;
	private String F_WTC_TotalPoliciesSold_PM;
	private String F_WTC_TotalPoliciesSold_PY;
	private String F_WTC_Sold_Amount;
	private String F_WTC_Sold_Amount_M;
	private String F_WTC_Sold_Amount_Y;
	private String F_WTC_TotalPoliciesSold_EIB;
	private String F_WTC_Sold_Amount_EIB;
	private String F_WTC_TotalPoliciesSold_ETB;
	private String F_WTC_Sold_Amount_ETB;
	private String F_HOHH_TotalPoliciesSold;
	private String F_HOHH_TotalPoliciesSold_PM;
	private String F_HOHH_TotalPoliciesSold_PY;
	private String F_HOHH_Sold_Amount;
	private String F_HOHH_Sold_Amount_M;
	private String F_HOHH_Sold_Amount_Y;
	private String F_HOHH_TotalPoliciesSold_EIB;
	private String F_HOHH_Sold_Amount_EIB;
	private String F_HOHH_TotalPoliciesSold_ETB;
	private String F_HOHH_Sold_Amount_ETB;
	private String F_MI_TotalPoliciesSold;
	private String F_MI_TotalPoliciesSold_PM;
	private String F_MI_TotalPoliciesSold_PY;
	private String F_Motor_Sold_Amount;
	private String F_Motor_Sold_Amount_M;
	private String F_Motor_Sold_Amount_Y;
	private String F_MI_TotalPoliciesSold_EIB;
	private String F_MI_Sold_Amount_EIB;
	private String F_MT_TotalPoliciesSold_ETB;
	private String F_MT_Sold_Amount_ETB;

	private String F_CI_TotalPoliciesSold;
	private String F_CI_TotalPoliciesSold_PM;
	private String F_CI_TotalPoliciesSold_PY;
	private String F_Cancer_Sold_Amount;
	private String F_Cancer_Sold_Amount_M;
	private String F_Cancer_Sold_Amount_Y;
	private String F_CI_TotalPoliciesSold_EIB;
	private String F_CI_Sold_Amount_EIB;
	private String F_CT_TotalPoliciesSold_ETB;
	private String F_CT_Sold_Amount_ETB;

	
	private String F_TL_TotalPoliciesSold;
	private String F_TL_TotalPoliciesSold_PM;
	private String F_TL_TotalPoliciesSold_PY;
	private String F_TL_Sold_Amount;
	private String F_TL_Sold_Amount_M;
	private String F_TL_Sold_Amount_Y;

	// ENTITY CHART
	private String F_Entity_WTC_TPS_PM;
	private String F_Entity_WTC_TPS_PY;
	private String F_Entity_WTCT_TPS_PM;
	private String F_Entity_WTCT_TPS_PY;

	private String F_Entity_HOHH_TPS_PM;
	private String F_Entity_HOHH_TPS_PY;
	private String F_Entity_HOHHT_TPS_PM;
	private String F_Entity_HOHHT_TPS_PY;

	private String F_Entity_MI_TPS_PM;
	private String F_Entity_MI_TPS_PY;
	private String F_Entity_MT_TPS_PM;
	private String F_Entity_MT_TPS_PY;

	private String F_Entity_CI_TPS_PM;
	private String F_Entity_CI_TPS_PY;
	private String F_Entity_CT_TPS_PM;
	private String F_Entity_CT_TPS_PY;
	
	private String F_Entity_TL_TPS_PM;
	private String F_Entity_TL_TPS_PY;
	private String F_Entity_TLT_TPS_PM;
	private String F_Entity_TLT_TPS_PY;

	// Abandonment

	private String F_Aband_TotalSuccess;
	private String F_Aband_TotalCount;

	// Source of Leads

	private String F_SOL_MAIL;
	private String F_SOL_CWP;
	private String F_SOL_Direct;
	private String F_SOL_NonDirect;

	// MOTOR ATTACHMENT RATIO

	private String F_Motor_A_Ratio_01;
	private String F_Motor_A_Ratio_02;
	private String F_Motor_A_Ratio_03;
	private String F_Motor_A_Ratio_04;
	private String F_Motor_A_Ratio_05;
	private String F_Motor_A_Ratio_06;
	private String F_Motor_A_Ratio_07;
	private String F_Motor_A_Ratio_08;
	private String F_Motor_A_Ratio_09;
	private String F_Motor_A_Ratio_10;
	private String F_Motor_A_Ratio_11;

	/// Actual vs Budget(MTD)

	private String F_AVB_WTC_Budget;
	private String F_AVB_HOHH_Budget;
	private String F_AVB_Motor_Budget;
	private String F_AVB_TL_Budget;
	private String F_AVB_Cancer_Budget;

	// Agent Performance

	private String F_AgentPer_JAN;
	private String F_AgentPer_FEB;
	private String F_AgentPer_MAR;
	private String F_AgentPer_APR;
	private String F_AgentPer_MAY;
	private String F_AgentPer_JUN;
	private String F_AgentPer_JUL;
	private String F_AgentPer_AUG;
	private String F_AgentPer_SEP;
	private String F_AgentPer_OCT;
	private String F_AgentPer_NOV;
	private String F_AgentPer_DEC;

	// CROSS SALE RATIO

	private String F_CrossSale_MI_Count;
	private String F_CrossSale_MI_WTC_Count;
	private String F_CrossSale_MI_HOHH_Count;
	private String F_CrossSale_MI_TL_Count;
	private String F_CrossSale_MI_CC_Count;
	
	private String F_CrossSale_WTC_Count;
	private String F_CrossSale_WTC_MI_Count;
	private String F_CrossSale_WTC_HOHH_Count;
	private String F_CrossSale_WTC_TL_Count;
	private String F_CrossSale_WTC_CC_Count;

	private String F_CrossSale_HOHH_Count;
	private String F_CrossSale_HOHH_WTC_Count;
	private String F_CrossSale_HOHH_MI_Count;
	private String F_CrossSale_HOHH_TL_Count;
	private String F_CrossSale_HOHH_CC_Count;

	private String F_CrossSale_TL_Count;
	private String F_CrossSale_TL_WTC_Count;
	private String F_CrossSale_TL_HOHH_Count;
	private String F_CrossSale_TL_MI_Count;
	private String F_CrossSale_TL_CC_Count;
	
	private String F_CrossSale_CC_Count;
	private String F_CrossSale_CC_WTC_Count;
	private String F_CrossSale_CC_HOHH_Count;
	private String F_CrossSale_CC_MI_Count;
	private String F_CrossSale_CC_TL_Count;

	private String F_Upsale_DPPA_Amount;
	private String F_Upsale_CAPS_Amount;

	// Sales Funnel

	private String F_Fun_MI_step1_count;
	private String F_Fun_MI_step2_count;
	private String F_Fun_MI_step3_count;
	private String F_Fun_MI_Lstep1_count;
	private String F_Fun_MI_Lstep2_count;
	private String F_Fun_MI_Lstep3_count;
	private String F_Fun_MI_Lstep1_Percentage;
	private String F_Fun_MI_Lstep2_Percentage;
	private String F_Fun_MI_Lstep3_Percentage;
	private String F_Fun_MI_Success_Tran;

	public String getFromdate() {
		return fromdate;
	}

	public void setFromdate(String fromdate) {
		this.fromdate = fromdate;
	}

	public String getF_Renewal_Retention_Motor() {
		return F_Renewal_Retention_Motor;
	}

	public void setF_Renewal_Retention_Motor(String f_Renewal_Retention_Motor) {
		F_Renewal_Retention_Motor = f_Renewal_Retention_Motor;
	}

	public String getSelectvalue() {
		return selectvalue;
	}

	public void setSelectvalue(String selectvalue) {
		this.selectvalue = selectvalue;
	}

	public String getF_WTC_TotalPoliciesSold() {
		return F_WTC_TotalPoliciesSold;
	}

	public void setF_WTC_TotalPoliciesSold(String f_WTC_TotalPoliciesSold) {
		F_WTC_TotalPoliciesSold = f_WTC_TotalPoliciesSold;
	}

	public String getF_WTC_TotalPoliciesSold_PM() {
		return F_WTC_TotalPoliciesSold_PM;
	}

	public void setF_WTC_TotalPoliciesSold_PM(String f_WTC_TotalPoliciesSold_PM) {
		F_WTC_TotalPoliciesSold_PM = f_WTC_TotalPoliciesSold_PM;
	}

	public String getF_WTC_TotalPoliciesSold_PY() {
		return F_WTC_TotalPoliciesSold_PY;
	}

	public void setF_WTC_TotalPoliciesSold_PY(String f_WTC_TotalPoliciesSold_PY) {
		F_WTC_TotalPoliciesSold_PY = f_WTC_TotalPoliciesSold_PY;
	}

	public String getF_WTC_Sold_Amount() {
		return F_WTC_Sold_Amount;
	}

	public void setF_WTC_Sold_Amount(String f_WTC_Sold_Amount) {
		F_WTC_Sold_Amount = f_WTC_Sold_Amount;
	}

	public String getF_WTC_Sold_Amount_M() {
		return F_WTC_Sold_Amount_M;
	}

	public void setF_WTC_Sold_Amount_M(String f_WTC_Sold_Amount_M) {
		F_WTC_Sold_Amount_M = f_WTC_Sold_Amount_M;
	}

	public String getF_WTC_Sold_Amount_Y() {
		return F_WTC_Sold_Amount_Y;
	}

	public void setF_WTC_Sold_Amount_Y(String f_WTC_Sold_Amount_Y) {
		F_WTC_Sold_Amount_Y = f_WTC_Sold_Amount_Y;
	}

	public String getF_WTC_TotalPoliciesSold_EIB() {
		return F_WTC_TotalPoliciesSold_EIB;
	}

	public void setF_WTC_TotalPoliciesSold_EIB(String f_WTC_TotalPoliciesSold_EIB) {
		F_WTC_TotalPoliciesSold_EIB = f_WTC_TotalPoliciesSold_EIB;
	}

	public String getF_WTC_Sold_Amount_EIB() {
		return F_WTC_Sold_Amount_EIB;
	}

	public void setF_WTC_Sold_Amount_EIB(String f_WTC_Sold_Amount_EIB) {
		F_WTC_Sold_Amount_EIB = f_WTC_Sold_Amount_EIB;
	}

	public String getF_WTC_TotalPoliciesSold_ETB() {
		return F_WTC_TotalPoliciesSold_ETB;
	}

	public void setF_WTC_TotalPoliciesSold_ETB(String f_WTC_TotalPoliciesSold_ETB) {
		F_WTC_TotalPoliciesSold_ETB = f_WTC_TotalPoliciesSold_ETB;
	}

	public String getF_WTC_Sold_Amount_ETB() {
		return F_WTC_Sold_Amount_ETB;
	}

	public void setF_WTC_Sold_Amount_ETB(String f_WTC_Sold_Amount_ETB) {
		F_WTC_Sold_Amount_ETB = f_WTC_Sold_Amount_ETB;
	}

	public String getF_HOHH_TotalPoliciesSold() {
		return F_HOHH_TotalPoliciesSold;
	}

	public void setF_HOHH_TotalPoliciesSold(String f_HOHH_TotalPoliciesSold) {
		F_HOHH_TotalPoliciesSold = f_HOHH_TotalPoliciesSold;
	}

	public String getF_HOHH_TotalPoliciesSold_PM() {
		return F_HOHH_TotalPoliciesSold_PM;
	}

	public void setF_HOHH_TotalPoliciesSold_PM(String f_HOHH_TotalPoliciesSold_PM) {
		F_HOHH_TotalPoliciesSold_PM = f_HOHH_TotalPoliciesSold_PM;
	}

	public String getF_HOHH_TotalPoliciesSold_PY() {
		return F_HOHH_TotalPoliciesSold_PY;
	}

	public void setF_HOHH_TotalPoliciesSold_PY(String f_HOHH_TotalPoliciesSold_PY) {
		F_HOHH_TotalPoliciesSold_PY = f_HOHH_TotalPoliciesSold_PY;
	}

	public String getF_HOHH_Sold_Amount() {
		return F_HOHH_Sold_Amount;
	}

	public void setF_HOHH_Sold_Amount(String f_HOHH_Sold_Amount) {
		F_HOHH_Sold_Amount = f_HOHH_Sold_Amount;
	}

	public String getF_HOHH_Sold_Amount_M() {
		return F_HOHH_Sold_Amount_M;
	}

	public void setF_HOHH_Sold_Amount_M(String f_HOHH_Sold_Amount_M) {
		F_HOHH_Sold_Amount_M = f_HOHH_Sold_Amount_M;
	}

	public String getF_HOHH_Sold_Amount_Y() {
		return F_HOHH_Sold_Amount_Y;
	}

	public void setF_HOHH_Sold_Amount_Y(String f_HOHH_Sold_Amount_Y) {
		F_HOHH_Sold_Amount_Y = f_HOHH_Sold_Amount_Y;
	}

	public String getF_HOHH_TotalPoliciesSold_EIB() {
		return F_HOHH_TotalPoliciesSold_EIB;
	}

	public void setF_HOHH_TotalPoliciesSold_EIB(String f_HOHH_TotalPoliciesSold_EIB) {
		F_HOHH_TotalPoliciesSold_EIB = f_HOHH_TotalPoliciesSold_EIB;
	}

	public String getF_HOHH_Sold_Amount_EIB() {
		return F_HOHH_Sold_Amount_EIB;
	}

	public void setF_HOHH_Sold_Amount_EIB(String f_HOHH_Sold_Amount_EIB) {
		F_HOHH_Sold_Amount_EIB = f_HOHH_Sold_Amount_EIB;
	}

	public String getF_HOHH_TotalPoliciesSold_ETB() {
		return F_HOHH_TotalPoliciesSold_ETB;
	}

	public void setF_HOHH_TotalPoliciesSold_ETB(String f_HOHH_TotalPoliciesSold_ETB) {
		F_HOHH_TotalPoliciesSold_ETB = f_HOHH_TotalPoliciesSold_ETB;
	}

	public String getF_HOHH_Sold_Amount_ETB() {
		return F_HOHH_Sold_Amount_ETB;
	}

	public void setF_HOHH_Sold_Amount_ETB(String f_HOHH_Sold_Amount_ETB) {
		F_HOHH_Sold_Amount_ETB = f_HOHH_Sold_Amount_ETB;
	}

	public String getF_MI_TotalPoliciesSold() {
		return F_MI_TotalPoliciesSold;
	}

	public void setF_MI_TotalPoliciesSold(String f_MI_TotalPoliciesSold) {
		F_MI_TotalPoliciesSold = f_MI_TotalPoliciesSold;
	}

	public String getF_MI_TotalPoliciesSold_PM() {
		return F_MI_TotalPoliciesSold_PM;
	}

	public void setF_MI_TotalPoliciesSold_PM(String f_MI_TotalPoliciesSold_PM) {
		F_MI_TotalPoliciesSold_PM = f_MI_TotalPoliciesSold_PM;
	}

	public String getF_MI_TotalPoliciesSold_PY() {
		return F_MI_TotalPoliciesSold_PY;
	}

	public void setF_MI_TotalPoliciesSold_PY(String f_MI_TotalPoliciesSold_PY) {
		F_MI_TotalPoliciesSold_PY = f_MI_TotalPoliciesSold_PY;
	}

	public String getF_Motor_Sold_Amount() {
		return F_Motor_Sold_Amount;
	}

	public void setF_Motor_Sold_Amount(String f_Motor_Sold_Amount) {
		F_Motor_Sold_Amount = f_Motor_Sold_Amount;
	}

	public String getF_Motor_Sold_Amount_M() {
		return F_Motor_Sold_Amount_M;
	}

	public void setF_Motor_Sold_Amount_M(String f_Motor_Sold_Amount_M) {
		F_Motor_Sold_Amount_M = f_Motor_Sold_Amount_M;
	}

	public String getF_Motor_Sold_Amount_Y() {
		return F_Motor_Sold_Amount_Y;
	}

	public void setF_Motor_Sold_Amount_Y(String f_Motor_Sold_Amount_Y) {
		F_Motor_Sold_Amount_Y = f_Motor_Sold_Amount_Y;
	}

	public String getF_MI_TotalPoliciesSold_EIB() {
		return F_MI_TotalPoliciesSold_EIB;
	}

	public void setF_MI_TotalPoliciesSold_EIB(String f_MI_TotalPoliciesSold_EIB) {
		F_MI_TotalPoliciesSold_EIB = f_MI_TotalPoliciesSold_EIB;
	}

	public String getF_MI_Sold_Amount_EIB() {
		return F_MI_Sold_Amount_EIB;
	}

	public void setF_MI_Sold_Amount_EIB(String f_MI_Sold_Amount_EIB) {
		F_MI_Sold_Amount_EIB = f_MI_Sold_Amount_EIB;
	}

	public String getF_MT_TotalPoliciesSold_ETB() {
		return F_MT_TotalPoliciesSold_ETB;
	}

	public void setF_MT_TotalPoliciesSold_ETB(String f_MT_TotalPoliciesSold_ETB) {
		F_MT_TotalPoliciesSold_ETB = f_MT_TotalPoliciesSold_ETB;
	}

	public String getF_MT_Sold_Amount_ETB() {
		return F_MT_Sold_Amount_ETB;
	}

	public void setF_MT_Sold_Amount_ETB(String f_MT_Sold_Amount_ETB) {
		F_MT_Sold_Amount_ETB = f_MT_Sold_Amount_ETB;
	}

	public String getF_CI_TotalPoliciesSold() {
		return F_CI_TotalPoliciesSold;
	}

	public void setF_CI_TotalPoliciesSold(String f_CI_TotalPoliciesSold) {
		F_CI_TotalPoliciesSold = f_CI_TotalPoliciesSold;
	}

	public String getF_CI_TotalPoliciesSold_PM() {
		return F_CI_TotalPoliciesSold_PM;
	}

	public void setF_CI_TotalPoliciesSold_PM(String f_CI_TotalPoliciesSold_PM) {
		F_CI_TotalPoliciesSold_PM = f_CI_TotalPoliciesSold_PM;
	}

	public String getF_CI_TotalPoliciesSold_PY() {
		return F_CI_TotalPoliciesSold_PY;
	}

	public void setF_CI_TotalPoliciesSold_PY(String f_CI_TotalPoliciesSold_PY) {
		F_CI_TotalPoliciesSold_PY = f_CI_TotalPoliciesSold_PY;
	}

	public String getF_Cancer_Sold_Amount() {
		return F_Cancer_Sold_Amount;
	}

	public void setF_Cancer_Sold_Amount(String f_Cancer_Sold_Amount) {
		F_Cancer_Sold_Amount = f_Cancer_Sold_Amount;
	}

	public String getF_Cancer_Sold_Amount_M() {
		return F_Cancer_Sold_Amount_M;
	}

	public void setF_Cancer_Sold_Amount_M(String f_Cancer_Sold_Amount_M) {
		F_Cancer_Sold_Amount_M = f_Cancer_Sold_Amount_M;
	}

	public String getF_Cancer_Sold_Amount_Y() {
		return F_Cancer_Sold_Amount_Y;
	}

	public void setF_Cancer_Sold_Amount_Y(String f_Cancer_Sold_Amount_Y) {
		F_Cancer_Sold_Amount_Y = f_Cancer_Sold_Amount_Y;
	}

	public String getF_CI_TotalPoliciesSold_EIB() {
		return F_CI_TotalPoliciesSold_EIB;
	}

	public void setF_CI_TotalPoliciesSold_EIB(String f_CI_TotalPoliciesSold_EIB) {
		F_CI_TotalPoliciesSold_EIB = f_CI_TotalPoliciesSold_EIB;
	}

	public String getF_CI_Sold_Amount_EIB() {
		return F_CI_Sold_Amount_EIB;
	}

	public void setF_CI_Sold_Amount_EIB(String f_CI_Sold_Amount_EIB) {
		F_CI_Sold_Amount_EIB = f_CI_Sold_Amount_EIB;
	}

	public String getF_CT_TotalPoliciesSold_ETB() {
		return F_CT_TotalPoliciesSold_ETB;
	}

	public void setF_CT_TotalPoliciesSold_ETB(String f_CT_TotalPoliciesSold_ETB) {
		F_CT_TotalPoliciesSold_ETB = f_CT_TotalPoliciesSold_ETB;
	}

	public String getF_CT_Sold_Amount_ETB() {
		return F_CT_Sold_Amount_ETB;
	}

	public void setF_CT_Sold_Amount_ETB(String f_CT_Sold_Amount_ETB) {
		F_CT_Sold_Amount_ETB = f_CT_Sold_Amount_ETB;
	}

	public String getF_TL_TotalPoliciesSold() {
		return F_TL_TotalPoliciesSold;
	}

	public void setF_TL_TotalPoliciesSold(String f_TL_TotalPoliciesSold) {
		F_TL_TotalPoliciesSold = f_TL_TotalPoliciesSold;
	}

	public String getF_TL_TotalPoliciesSold_PM() {
		return F_TL_TotalPoliciesSold_PM;
	}

	public void setF_TL_TotalPoliciesSold_PM(String f_TL_TotalPoliciesSold_PM) {
		F_TL_TotalPoliciesSold_PM = f_TL_TotalPoliciesSold_PM;
	}

	public String getF_TL_TotalPoliciesSold_PY() {
		return F_TL_TotalPoliciesSold_PY;
	}

	public void setF_TL_TotalPoliciesSold_PY(String f_TL_TotalPoliciesSold_PY) {
		F_TL_TotalPoliciesSold_PY = f_TL_TotalPoliciesSold_PY;
	}

	public String getF_TL_Sold_Amount() {
		return F_TL_Sold_Amount;
	}

	public void setF_TL_Sold_Amount(String f_TL_Sold_Amount) {
		F_TL_Sold_Amount = f_TL_Sold_Amount;
	}

	public String getF_TL_Sold_Amount_M() {
		return F_TL_Sold_Amount_M;
	}

	public void setF_TL_Sold_Amount_M(String f_TL_Sold_Amount_M) {
		F_TL_Sold_Amount_M = f_TL_Sold_Amount_M;
	}

	public String getF_TL_Sold_Amount_Y() {
		return F_TL_Sold_Amount_Y;
	}

	public void setF_TL_Sold_Amount_Y(String f_TL_Sold_Amount_Y) {
		F_TL_Sold_Amount_Y = f_TL_Sold_Amount_Y;
	}

	public String getF_Entity_WTC_TPS_PM() {
		return F_Entity_WTC_TPS_PM;
	}

	public void setF_Entity_WTC_TPS_PM(String f_Entity_WTC_TPS_PM) {
		F_Entity_WTC_TPS_PM = f_Entity_WTC_TPS_PM;
	}

	public String getF_Entity_WTC_TPS_PY() {
		return F_Entity_WTC_TPS_PY;
	}

	public void setF_Entity_WTC_TPS_PY(String f_Entity_WTC_TPS_PY) {
		F_Entity_WTC_TPS_PY = f_Entity_WTC_TPS_PY;
	}

	public String getF_Entity_WTCT_TPS_PM() {
		return F_Entity_WTCT_TPS_PM;
	}

	public void setF_Entity_WTCT_TPS_PM(String f_Entity_WTCT_TPS_PM) {
		F_Entity_WTCT_TPS_PM = f_Entity_WTCT_TPS_PM;
	}

	public String getF_Entity_WTCT_TPS_PY() {
		return F_Entity_WTCT_TPS_PY;
	}

	public void setF_Entity_WTCT_TPS_PY(String f_Entity_WTCT_TPS_PY) {
		F_Entity_WTCT_TPS_PY = f_Entity_WTCT_TPS_PY;
	}

	public String getF_Entity_HOHH_TPS_PM() {
		return F_Entity_HOHH_TPS_PM;
	}

	public void setF_Entity_HOHH_TPS_PM(String f_Entity_HOHH_TPS_PM) {
		F_Entity_HOHH_TPS_PM = f_Entity_HOHH_TPS_PM;
	}

	public String getF_Entity_HOHH_TPS_PY() {
		return F_Entity_HOHH_TPS_PY;
	}

	public void setF_Entity_HOHH_TPS_PY(String f_Entity_HOHH_TPS_PY) {
		F_Entity_HOHH_TPS_PY = f_Entity_HOHH_TPS_PY;
	}

	public String getF_Entity_HOHHT_TPS_PM() {
		return F_Entity_HOHHT_TPS_PM;
	}

	public void setF_Entity_HOHHT_TPS_PM(String f_Entity_HOHHT_TPS_PM) {
		F_Entity_HOHHT_TPS_PM = f_Entity_HOHHT_TPS_PM;
	}

	public String getF_Entity_HOHHT_TPS_PY() {
		return F_Entity_HOHHT_TPS_PY;
	}

	public void setF_Entity_HOHHT_TPS_PY(String f_Entity_HOHHT_TPS_PY) {
		F_Entity_HOHHT_TPS_PY = f_Entity_HOHHT_TPS_PY;
	}

	public String getF_Entity_MI_TPS_PM() {
		return F_Entity_MI_TPS_PM;
	}

	public void setF_Entity_MI_TPS_PM(String f_Entity_MI_TPS_PM) {
		F_Entity_MI_TPS_PM = f_Entity_MI_TPS_PM;
	}

	public String getF_Entity_MI_TPS_PY() {
		return F_Entity_MI_TPS_PY;
	}

	public void setF_Entity_MI_TPS_PY(String f_Entity_MI_TPS_PY) {
		F_Entity_MI_TPS_PY = f_Entity_MI_TPS_PY;
	}

	public String getF_Entity_MT_TPS_PM() {
		return F_Entity_MT_TPS_PM;
	}

	public void setF_Entity_MT_TPS_PM(String f_Entity_MT_TPS_PM) {
		F_Entity_MT_TPS_PM = f_Entity_MT_TPS_PM;
	}

	public String getF_Entity_MT_TPS_PY() {
		return F_Entity_MT_TPS_PY;
	}

	public void setF_Entity_MT_TPS_PY(String f_Entity_MT_TPS_PY) {
		F_Entity_MT_TPS_PY = f_Entity_MT_TPS_PY;
	}
	
	public String getF_Entity_CI_TPS_PM() {
		return F_Entity_CI_TPS_PM;
	}

	public void setF_Entity_CI_TPS_PM(String f_Entity_CI_TPS_PM) {
		F_Entity_CI_TPS_PM = f_Entity_CI_TPS_PM;
	}

	public String getF_Entity_CI_TPS_PY() {
		return F_Entity_CI_TPS_PY;
	}

	public void setF_Entity_CI_TPS_PY(String f_Entity_CI_TPS_PY) {
		F_Entity_CI_TPS_PY = f_Entity_CI_TPS_PY;
	}

	public String getF_Entity_CT_TPS_PM() {
		return F_Entity_CT_TPS_PM;
	}

	public void setF_Entity_CT_TPS_PM(String f_Entity_CT_TPS_PM) {
		F_Entity_CT_TPS_PM = f_Entity_CT_TPS_PM;
	}

	public String getF_Entity_CT_TPS_PY() {
		return F_Entity_CT_TPS_PY;
	}

	public void setF_Entity_CT_TPS_PY(String f_Entity_CT_TPS_PY) {
		F_Entity_CT_TPS_PY = f_Entity_CT_TPS_PY;
	}


	public String getF_Entity_TL_TPS_PM() {
		return F_Entity_TL_TPS_PM;
	}

	public void setF_Entity_TL_TPS_PM(String f_Entity_TL_TPS_PM) {
		F_Entity_TL_TPS_PM = f_Entity_TL_TPS_PM;
	}

	public String getF_Entity_TL_TPS_PY() {
		return F_Entity_TL_TPS_PY;
	}

	public void setF_Entity_TL_TPS_PY(String f_Entity_TL_TPS_PY) {
		F_Entity_TL_TPS_PY = f_Entity_TL_TPS_PY;
	}

	public String getF_Entity_TLT_TPS_PM() {
		return F_Entity_TLT_TPS_PM;
	}

	public void setF_Entity_TLT_TPS_PM(String f_Entity_TLT_TPS_PM) {
		F_Entity_TLT_TPS_PM = f_Entity_TLT_TPS_PM;
	}

	public String getF_Entity_TLT_TPS_PY() {
		return F_Entity_TLT_TPS_PY;
	}

	public void setF_Entity_TLT_TPS_PY(String f_Entity_TLT_TPS_PY) {
		F_Entity_TLT_TPS_PY = f_Entity_TLT_TPS_PY;
	}

	public String getF_Aband_TotalSuccess() {
		return F_Aband_TotalSuccess;
	}

	public void setF_Aband_TotalSuccess(String f_Aband_TotalSuccess) {
		F_Aband_TotalSuccess = f_Aband_TotalSuccess;
	}

	public String getF_Aband_TotalCount() {
		return F_Aband_TotalCount;
	}

	public void setF_Aband_TotalCount(String f_Aband_TotalCount) {
		F_Aband_TotalCount = f_Aband_TotalCount;
	}

	public String getF_SOL_MAIL() {
		return F_SOL_MAIL;
	}

	public void setF_SOL_MAIL(String f_SOL_MAIL) {
		F_SOL_MAIL = f_SOL_MAIL;
	}

	public String getF_SOL_CWP() {
		return F_SOL_CWP;
	}

	public void setF_SOL_CWP(String f_SOL_CWP) {
		F_SOL_CWP = f_SOL_CWP;
	}

	public String getF_SOL_Direct() {
		return F_SOL_Direct;
	}

	public void setF_SOL_Direct(String f_SOL_Direct) {
		F_SOL_Direct = f_SOL_Direct;
	}

	public String getF_SOL_NonDirect() {
		return F_SOL_NonDirect;
	}

	public void setF_SOL_NonDirect(String f_SOL_NonDirect) {
		F_SOL_NonDirect = f_SOL_NonDirect;
	}

	public String getF_Motor_A_Ratio_01() {
		return F_Motor_A_Ratio_01;
	}

	public void setF_Motor_A_Ratio_01(String f_Motor_A_Ratio_01) {
		F_Motor_A_Ratio_01 = f_Motor_A_Ratio_01;
	}

	public String getF_Motor_A_Ratio_02() {
		return F_Motor_A_Ratio_02;
	}

	public void setF_Motor_A_Ratio_02(String f_Motor_A_Ratio_02) {
		F_Motor_A_Ratio_02 = f_Motor_A_Ratio_02;
	}

	public String getF_Motor_A_Ratio_03() {
		return F_Motor_A_Ratio_03;
	}

	public void setF_Motor_A_Ratio_03(String f_Motor_A_Ratio_03) {
		F_Motor_A_Ratio_03 = f_Motor_A_Ratio_03;
	}

	public String getF_Motor_A_Ratio_04() {
		return F_Motor_A_Ratio_04;
	}

	public void setF_Motor_A_Ratio_04(String f_Motor_A_Ratio_04) {
		F_Motor_A_Ratio_04 = f_Motor_A_Ratio_04;
	}

	public String getF_Motor_A_Ratio_05() {
		return F_Motor_A_Ratio_05;
	}

	public void setF_Motor_A_Ratio_05(String f_Motor_A_Ratio_05) {
		F_Motor_A_Ratio_05 = f_Motor_A_Ratio_05;
	}

	public String getF_Motor_A_Ratio_06() {
		return F_Motor_A_Ratio_06;
	}

	public void setF_Motor_A_Ratio_06(String f_Motor_A_Ratio_06) {
		F_Motor_A_Ratio_06 = f_Motor_A_Ratio_06;
	}

	public String getF_Motor_A_Ratio_07() {
		return F_Motor_A_Ratio_07;
	}

	public void setF_Motor_A_Ratio_07(String f_Motor_A_Ratio_07) {
		F_Motor_A_Ratio_07 = f_Motor_A_Ratio_07;
	}

	public String getF_Motor_A_Ratio_08() {
		return F_Motor_A_Ratio_08;
	}

	public void setF_Motor_A_Ratio_08(String f_Motor_A_Ratio_08) {
		F_Motor_A_Ratio_08 = f_Motor_A_Ratio_08;
	}

	public String getF_Motor_A_Ratio_09() {
		return F_Motor_A_Ratio_09;
	}

	public void setF_Motor_A_Ratio_09(String f_Motor_A_Ratio_09) {
		F_Motor_A_Ratio_09 = f_Motor_A_Ratio_09;
	}

	public String getF_Motor_A_Ratio_10() {
		return F_Motor_A_Ratio_10;
	}

	public void setF_Motor_A_Ratio_10(String f_Motor_A_Ratio_10) {
		F_Motor_A_Ratio_10 = f_Motor_A_Ratio_10;
	}

	public String getF_Motor_A_Ratio_11() {
		return F_Motor_A_Ratio_11;
	}

	public void setF_Motor_A_Ratio_11(String f_Motor_A_Ratio_11) {
		F_Motor_A_Ratio_11 = f_Motor_A_Ratio_11;
	}

	public String getF_AVB_WTC_Budget() {
		return F_AVB_WTC_Budget;
	}

	public void setF_AVB_WTC_Budget(String f_AVB_WTC_Budget) {
		F_AVB_WTC_Budget = f_AVB_WTC_Budget;
	}

	public String getF_AVB_HOHH_Budget() {
		return F_AVB_HOHH_Budget;
	}

	public void setF_AVB_HOHH_Budget(String f_AVB_HOHH_Budget) {
		F_AVB_HOHH_Budget = f_AVB_HOHH_Budget;
	}

	public String getF_AVB_Motor_Budget() {
		return F_AVB_Motor_Budget;
	}

	public void setF_AVB_Motor_Budget(String f_AVB_Motor_Budget) {
		F_AVB_Motor_Budget = f_AVB_Motor_Budget;
	}
	
	public String getF_AVB_Cancer_Budget() {
		return F_AVB_Cancer_Budget;
	}

	public void setF_AVB_Cancer_Budget(String f_AVB_Cancer_Budget) {
		F_AVB_Cancer_Budget = f_AVB_Cancer_Budget;
	}

	public String getF_AVB_TL_Budget() {
		return F_AVB_TL_Budget;
	}

	public void setF_AVB_TL_Budget(String f_AVB_TL_Budget) {
		F_AVB_TL_Budget = f_AVB_TL_Budget;
	}

	public String getF_AgentPer_JAN() {
		return F_AgentPer_JAN;
	}

	public void setF_AgentPer_JAN(String f_AgentPer_JAN) {
		F_AgentPer_JAN = f_AgentPer_JAN;
	}

	public String getF_AgentPer_FEB() {
		return F_AgentPer_FEB;
	}

	public void setF_AgentPer_FEB(String f_AgentPer_FEB) {
		F_AgentPer_FEB = f_AgentPer_FEB;
	}

	public String getF_AgentPer_MAR() {
		return F_AgentPer_MAR;
	}

	public void setF_AgentPer_MAR(String f_AgentPer_MAR) {
		F_AgentPer_MAR = f_AgentPer_MAR;
	}

	public String getF_AgentPer_APR() {
		return F_AgentPer_APR;
	}

	public void setF_AgentPer_APR(String f_AgentPer_APR) {
		F_AgentPer_APR = f_AgentPer_APR;
	}

	public String getF_AgentPer_MAY() {
		return F_AgentPer_MAY;
	}

	public void setF_AgentPer_MAY(String f_AgentPer_MAY) {
		F_AgentPer_MAY = f_AgentPer_MAY;
	}

	public String getF_AgentPer_JUN() {
		return F_AgentPer_JUN;
	}

	public void setF_AgentPer_JUN(String f_AgentPer_JUN) {
		F_AgentPer_JUN = f_AgentPer_JUN;
	}

	public String getF_AgentPer_JUL() {
		return F_AgentPer_JUL;
	}

	public void setF_AgentPer_JUL(String f_AgentPer_JUL) {
		F_AgentPer_JUL = f_AgentPer_JUL;
	}

	public String getF_AgentPer_AUG() {
		return F_AgentPer_AUG;
	}

	public void setF_AgentPer_AUG(String f_AgentPer_AUG) {
		F_AgentPer_AUG = f_AgentPer_AUG;
	}

	public String getF_AgentPer_SEP() {
		return F_AgentPer_SEP;
	}

	public void setF_AgentPer_SEP(String f_AgentPer_SEP) {
		F_AgentPer_SEP = f_AgentPer_SEP;
	}

	public String getF_AgentPer_OCT() {
		return F_AgentPer_OCT;
	}

	public void setF_AgentPer_OCT(String f_AgentPer_OCT) {
		F_AgentPer_OCT = f_AgentPer_OCT;
	}

	public String getF_AgentPer_NOV() {
		return F_AgentPer_NOV;
	}

	public void setF_AgentPer_NOV(String f_AgentPer_NOV) {
		F_AgentPer_NOV = f_AgentPer_NOV;
	}

	public String getF_AgentPer_DEC() {
		return F_AgentPer_DEC;
	}

	public void setF_AgentPer_DEC(String f_AgentPer_DEC) {
		F_AgentPer_DEC = f_AgentPer_DEC;
	}

	public String getF_CrossSale_MI_Count() {
		return F_CrossSale_MI_Count;
	}

	public void setF_CrossSale_MI_Count(String f_CrossSale_MI_Count) {
		F_CrossSale_MI_Count = f_CrossSale_MI_Count;
	}

	public String getF_CrossSale_MI_WTC_Count() {
		return F_CrossSale_MI_WTC_Count;
	}

	public void setF_CrossSale_MI_WTC_Count(String f_CrossSale_MI_WTC_Count) {
		F_CrossSale_MI_WTC_Count = f_CrossSale_MI_WTC_Count;
	}

	public String getF_CrossSale_MI_HOHH_Count() {
		return F_CrossSale_MI_HOHH_Count;
	}

	public void setF_CrossSale_MI_HOHH_Count(String f_CrossSale_MI_HOHH_Count) {
		F_CrossSale_MI_HOHH_Count = f_CrossSale_MI_HOHH_Count;
	}

	public String getF_CrossSale_MI_TL_Count() {
		return F_CrossSale_MI_TL_Count;
	}

	public void setF_CrossSale_MI_TL_Count(String f_CrossSale_MI_TL_Count) {
		F_CrossSale_MI_TL_Count = f_CrossSale_MI_TL_Count;
	}
	
	public String getF_CrossSale_MI_CC_Count() {
		return F_CrossSale_MI_CC_Count;
	}

	public void setF_CrossSale_MI_CC_Count(String f_CrossSale_MI_CC_Count) {
		F_CrossSale_MI_CC_Count = f_CrossSale_MI_CC_Count;
	}

	public String getF_CrossSale_WTC_Count() {
		return F_CrossSale_WTC_Count;
	}

	public void setF_CrossSale_WTC_Count(String f_CrossSale_WTC_Count) {
		F_CrossSale_WTC_Count = f_CrossSale_WTC_Count;
	}

	public String getF_CrossSale_WTC_MI_Count() {
		return F_CrossSale_WTC_MI_Count;
	}

	public void setF_CrossSale_WTC_MI_Count(String f_CrossSale_WTC_MI_Count) {
		F_CrossSale_WTC_MI_Count = f_CrossSale_WTC_MI_Count;
	}

	public String getF_CrossSale_WTC_HOHH_Count() {
		return F_CrossSale_WTC_HOHH_Count;
	}

	public void setF_CrossSale_WTC_HOHH_Count(String f_CrossSale_WTC_HOHH_Count) {
		F_CrossSale_WTC_HOHH_Count = f_CrossSale_WTC_HOHH_Count;
	}

	public String getF_CrossSale_WTC_TL_Count() {
		return F_CrossSale_WTC_TL_Count;
	}

	public void setF_CrossSale_WTC_TL_Count(String f_CrossSale_WTC_TL_Count) {
		F_CrossSale_WTC_TL_Count = f_CrossSale_WTC_TL_Count;
	}
	
	public String getF_CrossSale_WTC_CC_Count() {
		return F_CrossSale_WTC_CC_Count;
	}

	public void setF_CrossSale_WTC_CC_Count(String f_CrossSale_WTC_CC_Count) {
		F_CrossSale_WTC_CC_Count = f_CrossSale_WTC_CC_Count;
	}

	public String getF_CrossSale_HOHH_Count() {
		return F_CrossSale_HOHH_Count;
	}

	public void setF_CrossSale_HOHH_Count(String f_CrossSale_HOHH_Count) {
		F_CrossSale_HOHH_Count = f_CrossSale_HOHH_Count;
	}

	public String getF_CrossSale_HOHH_WTC_Count() {
		return F_CrossSale_HOHH_WTC_Count;
	}

	public void setF_CrossSale_HOHH_WTC_Count(String f_CrossSale_HOHH_WTC_Count) {
		F_CrossSale_HOHH_WTC_Count = f_CrossSale_HOHH_WTC_Count;
	}

	public String getF_CrossSale_HOHH_MI_Count() {
		return F_CrossSale_HOHH_MI_Count;
	}

	public void setF_CrossSale_HOHH_MI_Count(String f_CrossSale_HOHH_MI_Count) {
		F_CrossSale_HOHH_MI_Count = f_CrossSale_HOHH_MI_Count;
	}

	public String getF_CrossSale_HOHH_TL_Count() {
		return F_CrossSale_HOHH_TL_Count;
	}

	public void setF_CrossSale_HOHH_TL_Count(String f_CrossSale_HOHH_TL_Count) {
		F_CrossSale_HOHH_TL_Count = f_CrossSale_HOHH_TL_Count;
	}
	
	public String getF_CrossSale_HOHH_CC_Count() {
		return F_CrossSale_HOHH_CC_Count;
	}

	public void setF_CrossSale_HOHH_CC_Count(String f_CrossSale_HOHH_CC_Count) {
		F_CrossSale_HOHH_CC_Count = f_CrossSale_HOHH_CC_Count;
	}


	public String getF_CrossSale_TL_Count() {
		return F_CrossSale_TL_Count;
	}

	public void setF_CrossSale_TL_Count(String f_CrossSale_TL_Count) {
		F_CrossSale_TL_Count = f_CrossSale_TL_Count;
	}

	public String getF_CrossSale_TL_WTC_Count() {
		return F_CrossSale_TL_WTC_Count;
	}

	public void setF_CrossSale_TL_WTC_Count(String f_CrossSale_TL_WTC_Count) {
		F_CrossSale_TL_WTC_Count = f_CrossSale_TL_WTC_Count;
	}

	public String getF_CrossSale_TL_HOHH_Count() {
		return F_CrossSale_TL_HOHH_Count;
	}

	public void setF_CrossSale_TL_HOHH_Count(String f_CrossSale_TL_HOHH_Count) {
		F_CrossSale_TL_HOHH_Count = f_CrossSale_TL_HOHH_Count;
	}

	public String getF_CrossSale_TL_MI_Count() {
		return F_CrossSale_TL_MI_Count;
	}

	public void setF_CrossSale_TL_MI_Count(String f_CrossSale_TL_MI_Count) {
		F_CrossSale_TL_MI_Count = f_CrossSale_TL_MI_Count;
	}
	
	public String getF_CrossSale_TL_CC_Count() {
		return F_CrossSale_TL_CC_Count;
	}

	public void setF_CrossSale_TL_CC_Count(String f_CrossSale_TL_CC_Count) {
		F_CrossSale_TL_CC_Count = f_CrossSale_TL_CC_Count;
	}
	
	public String getF_CrossSale_CC_Count() {
		return F_CrossSale_CC_Count;
	}

	public void setF_CrossSale_CC_Count(String f_CrossSale_CC_Count) {
		F_CrossSale_CC_Count = f_CrossSale_CC_Count;
	}

	public String getF_CrossSale_CC_WTC_Count() {
		return F_CrossSale_CC_WTC_Count;
	}

	public void setF_CrossSale_CC_WTC_Count(String f_CrossSale_CC_WTC_Count) {
		F_CrossSale_CC_WTC_Count = f_CrossSale_CC_WTC_Count;
	}

	public String getF_CrossSale_CC_HOHH_Count() {
		return F_CrossSale_CC_HOHH_Count;
	}

	public void setF_CrossSale_CC_HOHH_Count(String f_CrossSale_CC_HOHH_Count) {
		F_CrossSale_CC_HOHH_Count = f_CrossSale_CC_HOHH_Count;
	}

	public String getF_CrossSale_CC_MI_Count() {
		return F_CrossSale_CC_MI_Count;
	}

	public void setF_CrossSale_CC_MI_Count(String f_CrossSale_CC_MI_Count) {
		F_CrossSale_CC_MI_Count = f_CrossSale_CC_MI_Count;
	}
	
	public String getF_CrossSale_CC_TL_Count() {
		return F_CrossSale_CC_TL_Count;
	}

	public void setF_CrossSale_CC_TL_Count(String f_CrossSale_CC_TL_Count) {
		F_CrossSale_CC_TL_Count = f_CrossSale_CC_TL_Count;
	}

	public String getF_Upsale_DPPA_Amount() {
		return F_Upsale_DPPA_Amount;
	}

	public void setF_Upsale_DPPA_Amount(String f_Upsale_DPPA_Amount) {
		F_Upsale_DPPA_Amount = f_Upsale_DPPA_Amount;
	}

	public String getF_Upsale_CAPS_Amount() {
		return F_Upsale_CAPS_Amount;
	}

	public void setF_Upsale_CAPS_Amount(String f_Upsale_CAPS_Amount) {
		F_Upsale_CAPS_Amount = f_Upsale_CAPS_Amount;
	}

	public String getF_Fun_MI_step1_count() {
		return F_Fun_MI_step1_count;
	}

	public void setF_Fun_MI_step1_count(String f_Fun_MI_step1_count) {
		F_Fun_MI_step1_count = f_Fun_MI_step1_count;
	}

	public String getF_Fun_MI_step2_count() {
		return F_Fun_MI_step2_count;
	}

	public void setF_Fun_MI_step2_count(String f_Fun_MI_step2_count) {
		F_Fun_MI_step2_count = f_Fun_MI_step2_count;
	}

	public String getF_Fun_MI_step3_count() {
		return F_Fun_MI_step3_count;
	}

	public void setF_Fun_MI_step3_count(String f_Fun_MI_step3_count) {
		F_Fun_MI_step3_count = f_Fun_MI_step3_count;
	}

	public String getF_Fun_MI_Lstep1_count() {
		return F_Fun_MI_Lstep1_count;
	}

	public void setF_Fun_MI_Lstep1_count(String f_Fun_MI_Lstep1_count) {
		F_Fun_MI_Lstep1_count = f_Fun_MI_Lstep1_count;
	}

	public String getF_Fun_MI_Lstep2_count() {
		return F_Fun_MI_Lstep2_count;
	}

	public void setF_Fun_MI_Lstep2_count(String f_Fun_MI_Lstep2_count) {
		F_Fun_MI_Lstep2_count = f_Fun_MI_Lstep2_count;
	}

	public String getF_Fun_MI_Lstep3_count() {
		return F_Fun_MI_Lstep3_count;
	}

	public void setF_Fun_MI_Lstep3_count(String f_Fun_MI_Lstep3_count) {
		F_Fun_MI_Lstep3_count = f_Fun_MI_Lstep3_count;
	}

	public String getF_Fun_MI_Lstep1_Percentage() {
		return F_Fun_MI_Lstep1_Percentage;
	}

	public void setF_Fun_MI_Lstep1_Percentage(String f_Fun_MI_Lstep1_Percentage) {
		F_Fun_MI_Lstep1_Percentage = f_Fun_MI_Lstep1_Percentage;
	}

	public String getF_Fun_MI_Lstep2_Percentage() {
		return F_Fun_MI_Lstep2_Percentage;
	}

	public void setF_Fun_MI_Lstep2_Percentage(String f_Fun_MI_Lstep2_Percentage) {
		F_Fun_MI_Lstep2_Percentage = f_Fun_MI_Lstep2_Percentage;
	}

	public String getF_Fun_MI_Lstep3_Percentage() {
		return F_Fun_MI_Lstep3_Percentage;
	}

	public void setF_Fun_MI_Lstep3_Percentage(String f_Fun_MI_Lstep3_Percentage) {
		F_Fun_MI_Lstep3_Percentage = f_Fun_MI_Lstep3_Percentage;
	}

	public String getF_Fun_MI_Success_Tran() {
		return F_Fun_MI_Success_Tran;
	}

	public void setF_Fun_MI_Success_Tran(String f_Fun_MI_Success_Tran) {
		F_Fun_MI_Success_Tran = f_Fun_MI_Success_Tran;
	}

}
