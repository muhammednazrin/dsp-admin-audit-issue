package com.spring.VO;

import java.math.BigDecimal;
import java.util.Date;

public class BuddyPABenefitGrp {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_BUDDY_TBL_BENEFIT_GROUPING.COMBO_ID
	 *
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	private Object comboId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_BUDDY_TBL_BENEFIT_GROUPING.COMPANY_ID
	 *
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	private Object companyId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_BUDDY_TBL_BENEFIT_GROUPING.PLAN_ID
	 *
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	private Object planId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_BUDDY_TBL_BENEFIT_GROUPING.BENEFIT_ID
	 *
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	private Integer benefitId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_BUDDY_TBL_BENEFIT_GROUPING.ADULT_SUM_INSURED
	 *
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	private BigDecimal adultSumInsured;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_BUDDY_TBL_BENEFIT_GROUPING.CHILD_SUM_INSURED
	 *
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	private BigDecimal childSumInsured;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_BUDDY_TBL_BENEFIT_GROUPING.ADULT_LOADING
	 *
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	private BigDecimal adultLoading;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_BUDDY_TBL_BENEFIT_GROUPING.CHILD_LOADING
	 *
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	private BigDecimal childLoading;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_BUDDY_TBL_BENEFIT_GROUPING.ADULT_PREMIUM
	 *
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	private BigDecimal adultPremium;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_BUDDY_TBL_BENEFIT_GROUPING.CHILD_PREMIUM
	 *
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	private BigDecimal childPremium;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_BUDDY_TBL_BENEFIT_GROUPING.ENABLE
	 *
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	private Object enable;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_BUDDY_TBL_BENEFIT_GROUPING.CREATED_DT
	 *
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	private Date createdDt;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_BUDDY_TBL_BENEFIT_GROUPING.CREATED_BY
	 *
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	private Object createdBy;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_BUDDY_TBL_BENEFIT_GROUPING.MODIFIED_DT
	 *
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	private Date modifiedDt;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_BUDDY_TBL_BENEFIT_GROUPING.MODIFIED_BY
	 *
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	private Object modifiedBy;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_BUDDY_TBL_BENEFIT_GROUPING.VERSION
	 *
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	private Short version;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.COMBO_ID
	 *
	 * @return the value of DSP_BUDDY_TBL_BENEFIT_GROUPING.COMBO_ID
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public Object getComboId() {
		return comboId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.COMBO_ID
	 *
	 * @param comboId
	 *            the value for DSP_BUDDY_TBL_BENEFIT_GROUPING.COMBO_ID
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public void setComboId(Object comboId) {
		this.comboId = comboId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.COMPANY_ID
	 *
	 * @return the value of DSP_BUDDY_TBL_BENEFIT_GROUPING.COMPANY_ID
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public Object getCompanyId() {
		return companyId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.COMPANY_ID
	 *
	 * @param companyId
	 *            the value for DSP_BUDDY_TBL_BENEFIT_GROUPING.COMPANY_ID
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public void setCompanyId(Object companyId) {
		this.companyId = companyId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.PLAN_ID
	 *
	 * @return the value of DSP_BUDDY_TBL_BENEFIT_GROUPING.PLAN_ID
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public Object getPlanId() {
		return planId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.PLAN_ID
	 *
	 * @param planId
	 *            the value for DSP_BUDDY_TBL_BENEFIT_GROUPING.PLAN_ID
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public void setPlanId(Object planId) {
		this.planId = planId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.BENEFIT_ID
	 *
	 * @return the value of DSP_BUDDY_TBL_BENEFIT_GROUPING.BENEFIT_ID
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public Integer getBenefitId() {
		return benefitId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.BENEFIT_ID
	 *
	 * @param benefitId
	 *            the value for DSP_BUDDY_TBL_BENEFIT_GROUPING.BENEFIT_ID
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public void setBenefitId(Integer benefitId) {
		this.benefitId = benefitId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.ADULT_SUM_INSURED
	 *
	 * @return the value of DSP_BUDDY_TBL_BENEFIT_GROUPING.ADULT_SUM_INSURED
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public BigDecimal getAdultSumInsured() {
		return adultSumInsured;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.ADULT_SUM_INSURED
	 *
	 * @param adultSumInsured
	 *            the value for DSP_BUDDY_TBL_BENEFIT_GROUPING.ADULT_SUM_INSURED
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public void setAdultSumInsured(BigDecimal adultSumInsured) {
		this.adultSumInsured = adultSumInsured;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.CHILD_SUM_INSURED
	 *
	 * @return the value of DSP_BUDDY_TBL_BENEFIT_GROUPING.CHILD_SUM_INSURED
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public BigDecimal getChildSumInsured() {
		return childSumInsured;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.CHILD_SUM_INSURED
	 *
	 * @param childSumInsured
	 *            the value for DSP_BUDDY_TBL_BENEFIT_GROUPING.CHILD_SUM_INSURED
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public void setChildSumInsured(BigDecimal childSumInsured) {
		this.childSumInsured = childSumInsured;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.ADULT_LOADING
	 *
	 * @return the value of DSP_BUDDY_TBL_BENEFIT_GROUPING.ADULT_LOADING
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public BigDecimal getAdultLoading() {
		return adultLoading;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.ADULT_LOADING
	 *
	 * @param adultLoading
	 *            the value for DSP_BUDDY_TBL_BENEFIT_GROUPING.ADULT_LOADING
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public void setAdultLoading(BigDecimal adultLoading) {
		this.adultLoading = adultLoading;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.CHILD_LOADING
	 *
	 * @return the value of DSP_BUDDY_TBL_BENEFIT_GROUPING.CHILD_LOADING
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public BigDecimal getChildLoading() {
		return childLoading;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.CHILD_LOADING
	 *
	 * @param childLoading
	 *            the value for DSP_BUDDY_TBL_BENEFIT_GROUPING.CHILD_LOADING
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public void setChildLoading(BigDecimal childLoading) {
		this.childLoading = childLoading;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.ADULT_PREMIUM
	 *
	 * @return the value of DSP_BUDDY_TBL_BENEFIT_GROUPING.ADULT_PREMIUM
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public BigDecimal getAdultPremium() {
		return adultPremium;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.ADULT_PREMIUM
	 *
	 * @param adultPremium
	 *            the value for DSP_BUDDY_TBL_BENEFIT_GROUPING.ADULT_PREMIUM
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public void setAdultPremium(BigDecimal adultPremium) {
		this.adultPremium = adultPremium;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.CHILD_PREMIUM
	 *
	 * @return the value of DSP_BUDDY_TBL_BENEFIT_GROUPING.CHILD_PREMIUM
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public BigDecimal getChildPremium() {
		return childPremium;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.CHILD_PREMIUM
	 *
	 * @param childPremium
	 *            the value for DSP_BUDDY_TBL_BENEFIT_GROUPING.CHILD_PREMIUM
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public void setChildPremium(BigDecimal childPremium) {
		this.childPremium = childPremium;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.ENABLE
	 *
	 * @return the value of DSP_BUDDY_TBL_BENEFIT_GROUPING.ENABLE
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public Object getEnable() {
		return enable;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.ENABLE
	 *
	 * @param enable
	 *            the value for DSP_BUDDY_TBL_BENEFIT_GROUPING.ENABLE
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public void setEnable(Object enable) {
		this.enable = enable;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.CREATED_DT
	 *
	 * @return the value of DSP_BUDDY_TBL_BENEFIT_GROUPING.CREATED_DT
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public Date getCreatedDt() {
		return createdDt;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.CREATED_DT
	 *
	 * @param createdDt
	 *            the value for DSP_BUDDY_TBL_BENEFIT_GROUPING.CREATED_DT
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.CREATED_BY
	 *
	 * @return the value of DSP_BUDDY_TBL_BENEFIT_GROUPING.CREATED_BY
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public Object getCreatedBy() {
		return createdBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.CREATED_BY
	 *
	 * @param createdBy
	 *            the value for DSP_BUDDY_TBL_BENEFIT_GROUPING.CREATED_BY
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public void setCreatedBy(Object createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.MODIFIED_DT
	 *
	 * @return the value of DSP_BUDDY_TBL_BENEFIT_GROUPING.MODIFIED_DT
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public Date getModifiedDt() {
		return modifiedDt;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.MODIFIED_DT
	 *
	 * @param modifiedDt
	 *            the value for DSP_BUDDY_TBL_BENEFIT_GROUPING.MODIFIED_DT
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.MODIFIED_BY
	 *
	 * @return the value of DSP_BUDDY_TBL_BENEFIT_GROUPING.MODIFIED_BY
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public Object getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.MODIFIED_BY
	 *
	 * @param modifiedBy
	 *            the value for DSP_BUDDY_TBL_BENEFIT_GROUPING.MODIFIED_BY
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public void setModifiedBy(Object modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.VERSION
	 *
	 * @return the value of DSP_BUDDY_TBL_BENEFIT_GROUPING.VERSION
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public Short getVersion() {
		return version;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_BUDDY_TBL_BENEFIT_GROUPING.VERSION
	 *
	 * @param version
	 *            the value for DSP_BUDDY_TBL_BENEFIT_GROUPING.VERSION
	 * @mbg.generated Sat Jun 30 17:46:33 SGT 2018
	 */
	public void setVersion(Short version) {
		this.version = version;
	}
}