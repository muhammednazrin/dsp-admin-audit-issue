package com.spring.VO;

import java.util.ArrayList;
import java.util.List;

public class AdminParamExample {
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database table DSP_ADM_TBL_PARAM
	 *
	 * @mbggenerated Wed Feb 08 11:07:04 SGT 2017
	 */
	protected String orderByClause;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database table DSP_ADM_TBL_PARAM
	 *
	 * @mbggenerated Wed Feb 08 11:07:04 SGT 2017
	 */
	protected boolean distinct;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database table DSP_ADM_TBL_PARAM
	 *
	 * @mbggenerated Wed Feb 08 11:07:04 SGT 2017
	 */
	protected List<Criteria> oredCriteria;

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_PARAM
	 *
	 * @mbggenerated Wed Feb 08 11:07:04 SGT 2017
	 */
	public AdminParamExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_PARAM
	 *
	 * @mbggenerated Wed Feb 08 11:07:04 SGT 2017
	 */
	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_PARAM
	 *
	 * @mbggenerated Wed Feb 08 11:07:04 SGT 2017
	 */
	public String getOrderByClause() {
		return orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_PARAM
	 *
	 * @mbggenerated Wed Feb 08 11:07:04 SGT 2017
	 */
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_PARAM
	 *
	 * @mbggenerated Wed Feb 08 11:07:04 SGT 2017
	 */
	public boolean isDistinct() {
		return distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_PARAM
	 *
	 * @mbggenerated Wed Feb 08 11:07:04 SGT 2017
	 */
	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_PARAM
	 *
	 * @mbggenerated Wed Feb 08 11:07:04 SGT 2017
	 */
	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_PARAM
	 *
	 * @mbggenerated Wed Feb 08 11:07:04 SGT 2017
	 */
	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_PARAM
	 *
	 * @mbggenerated Wed Feb 08 11:07:04 SGT 2017
	 */
	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_PARAM
	 *
	 * @mbggenerated Wed Feb 08 11:07:04 SGT 2017
	 */
	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_PARAM
	 *
	 * @mbggenerated Wed Feb 08 11:07:04 SGT 2017
	 */
	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the
	 * database table DSP_ADM_TBL_PARAM
	 *
	 * @mbggenerated Wed Feb 08 11:07:04 SGT 2017
	 */
	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("param.ID is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("param.ID is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("param.ID =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("param.ID <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("param.ID >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("param.ID >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("param.ID <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("param.ID <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("param.ID like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("param.ID not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("param.ID in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("param.ID not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("param.ID between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("param.ID not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andParamTypeIsNull() {
			addCriterion("param.PARAM_TYPE is null");
			return (Criteria) this;
		}

		public Criteria andParamTypeIsNotNull() {
			addCriterion("param.PARAM_TYPE is not null");
			return (Criteria) this;
		}

		public Criteria andParamTypeEqualTo(String value) {
			addCriterion("param.PARAM_TYPE =", value, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamTypeNotEqualTo(String value) {
			addCriterion("param.PARAM_TYPE <>", value, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamTypeGreaterThan(String value) {
			addCriterion("param.PARAM_TYPE >", value, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamTypeGreaterThanOrEqualTo(String value) {
			addCriterion("param.PARAM_TYPE >=", value, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamTypeLessThan(String value) {
			addCriterion("param.PARAM_TYPE <", value, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamTypeLessThanOrEqualTo(String value) {
			addCriterion("param.PARAM_TYPE <=", value, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamTypeLike(String value) {
			addCriterion("param.PARAM_TYPE like", value, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamTypeNotLike(String value) {
			addCriterion("param.PARAM_TYPE not like", value, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamTypeIn(List<String> values) {
			addCriterion("param.PARAM_TYPE in", values, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamTypeNotIn(List<String> values) {
			addCriterion("param.PARAM_TYPE not in", values, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamTypeBetween(String value1, String value2) {
			addCriterion("param.PARAM_TYPE between", value1, value2, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamTypeNotBetween(String value1, String value2) {
			addCriterion("param.PARAM_TYPE not between", value1, value2, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamCodeIsNull() {
			addCriterion("param.PARAM_CODE is null");
			return (Criteria) this;
		}

		public Criteria andParamCodeIsNotNull() {
			addCriterion("param.PARAM_CODE is not null");
			return (Criteria) this;
		}

		public Criteria andParamCodeEqualTo(String value) {
			addCriterion("param.PARAM_CODE =", value, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamCodeNotEqualTo(String value) {
			addCriterion("param.PARAM_CODE <>", value, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamCodeGreaterThan(String value) {
			addCriterion("param.PARAM_CODE >", value, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamCodeGreaterThanOrEqualTo(String value) {
			addCriterion("param.PARAM_CODE >=", value, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamCodeLessThan(String value) {
			addCriterion("param.PARAM_CODE <", value, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamCodeLessThanOrEqualTo(String value) {
			addCriterion("param.PARAM_CODE <=", value, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamCodeLike(String value) {
			addCriterion("param.PARAM_CODE like", value, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamCodeNotLike(String value) {
			addCriterion("param.PARAM_CODE not like", value, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamCodeIn(List<String> values) {
			addCriterion("param.PARAM_CODE in", values, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamCodeNotIn(List<String> values) {
			addCriterion("param.PARAM_CODE not in", values, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamCodeBetween(String value1, String value2) {
			addCriterion("param.PARAM_CODE between", value1, value2, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamCodeNotBetween(String value1, String value2) {
			addCriterion("param.PARAM_CODE not between", value1, value2, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamNameIsNull() {
			addCriterion("param.PARAM_NAME is null");
			return (Criteria) this;
		}

		public Criteria andParamNameIsNotNull() {
			addCriterion("param.PARAM_NAME is not null");
			return (Criteria) this;
		}

		public Criteria andParamNameEqualTo(String value) {
			addCriterion("param.PARAM_NAME =", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameNotEqualTo(String value) {
			addCriterion("param.PARAM_NAME <>", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameGreaterThan(String value) {
			addCriterion("param.PARAM_NAME >", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameGreaterThanOrEqualTo(String value) {
			addCriterion("param.PARAM_NAME >=", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameLessThan(String value) {
			addCriterion("param.PARAM_NAME <", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameLessThanOrEqualTo(String value) {
			addCriterion("param.PARAM_NAME <=", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameLike(String value) {
			addCriterion("param.PARAM_NAME like", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameNotLike(String value) {
			addCriterion("param.PARAM_NAME not like", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameIn(List<String> values) {
			addCriterion("param.PARAM_NAME in", values, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameNotIn(List<String> values) {
			addCriterion("param.PARAM_NAME not in", values, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameBetween(String value1, String value2) {
			addCriterion("param.PARAM_NAME between", value1, value2, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameNotBetween(String value1, String value2) {
			addCriterion("param.PARAM_NAME not between", value1, value2, "paramName");
			return (Criteria) this;
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the
	 * database table DSP_ADM_TBL_PARAM
	 *
	 * @mbggenerated do_not_delete_during_merge Wed Feb 08 11:07:04 SGT 2017
	 */
	public static class Criteria extends GeneratedCriteria {

		protected Criteria() {
			super();
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the
	 * database table DSP_ADM_TBL_PARAM
	 *
	 * @mbggenerated Wed Feb 08 11:07:04 SGT 2017
	 */
	public static class Criterion {
		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}
}