package com.spring.VO;

public class Products {
	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_COMMON_TBL_PRODUCTS.DSP_PRODUCT_CODE
	 *
	 * @mbggenerated Thu Jan 19 18:03:56 SGT 2017
	 */
	private String dspProductCode;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_COMMON_TBL_PRODUCTS.DSP_PRODUCT_CODE_NAME
	 *
	 * @mbggenerated Thu Jan 19 18:03:56 SGT 2017
	 */
	private String dspProductCodeName;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_COMMON_TBL_PRODUCTS.PRODUCTS_DOMAIN_CODE
	 *
	 * @mbggenerated Thu Jan 19 18:03:56 SGT 2017
	 */
	private String productsDomainCode;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_COMMON_TBL_PRODUCTS.PRODUCTS_CATEGORY_CODE
	 *
	 * @mbggenerated Thu Jan 19 18:03:56 SGT 2017
	 */
	private String productsCategoryCode;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_COMMON_TBL_PRODUCTS.ANNUAL_SALE_TARGET
	 *
	 * @mbggenerated Thu Jan 19 18:03:56 SGT 2017
	 */
	private String annualSaleTarget;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_COMMON_TBL_PRODUCTS.QUOTATION_VALIDITY
	 *
	 * @mbggenerated Thu Jan 19 18:03:56 SGT 2017
	 */
	private String quotationValidity;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_COMMON_TBL_PRODUCTS.PRODUCT_ENTITY
	 *
	 * @mbggenerated Thu Jan 19 18:03:56 SGT 2017
	 */
	private String productEntity;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_COMMON_TBL_PRODUCTS.DSP_PRODUCT_CODE
	 *
	 * @return the value of DSP_COMMON_TBL_PRODUCTS.DSP_PRODUCT_CODE
	 *
	 * @mbggenerated Thu Jan 19 18:03:56 SGT 2017
	 */
	public String getDspProductCode() {
		return dspProductCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_COMMON_TBL_PRODUCTS.DSP_PRODUCT_CODE
	 *
	 * @param dspProductCode
	 *            the value for DSP_COMMON_TBL_PRODUCTS.DSP_PRODUCT_CODE
	 *
	 * @mbggenerated Thu Jan 19 18:03:56 SGT 2017
	 */
	public void setDspProductCode(String dspProductCode) {
		this.dspProductCode = dspProductCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_COMMON_TBL_PRODUCTS.DSP_PRODUCT_CODE_NAME
	 *
	 * @return the value of DSP_COMMON_TBL_PRODUCTS.DSP_PRODUCT_CODE_NAME
	 *
	 * @mbggenerated Thu Jan 19 18:03:56 SGT 2017
	 */
	public String getDspProductCodeName() {
		return dspProductCodeName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_COMMON_TBL_PRODUCTS.DSP_PRODUCT_CODE_NAME
	 *
	 * @param dspProductCodeName
	 *            the value for DSP_COMMON_TBL_PRODUCTS.DSP_PRODUCT_CODE_NAME
	 *
	 * @mbggenerated Thu Jan 19 18:03:56 SGT 2017
	 */
	public void setDspProductCodeName(String dspProductCodeName) {
		this.dspProductCodeName = dspProductCodeName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_COMMON_TBL_PRODUCTS.PRODUCTS_DOMAIN_CODE
	 *
	 * @return the value of DSP_COMMON_TBL_PRODUCTS.PRODUCTS_DOMAIN_CODE
	 *
	 * @mbggenerated Thu Jan 19 18:03:56 SGT 2017
	 */
	public String getProductsDomainCode() {
		return productsDomainCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_COMMON_TBL_PRODUCTS.PRODUCTS_DOMAIN_CODE
	 *
	 * @param productsDomainCode
	 *            the value for DSP_COMMON_TBL_PRODUCTS.PRODUCTS_DOMAIN_CODE
	 *
	 * @mbggenerated Thu Jan 19 18:03:56 SGT 2017
	 */
	public void setProductsDomainCode(String productsDomainCode) {
		this.productsDomainCode = productsDomainCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_COMMON_TBL_PRODUCTS.PRODUCTS_CATEGORY_CODE
	 *
	 * @return the value of DSP_COMMON_TBL_PRODUCTS.PRODUCTS_CATEGORY_CODE
	 *
	 * @mbggenerated Thu Jan 19 18:03:56 SGT 2017
	 */
	public String getProductsCategoryCode() {
		return productsCategoryCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_COMMON_TBL_PRODUCTS.PRODUCTS_CATEGORY_CODE
	 *
	 * @param productsCategoryCode
	 *            the value for DSP_COMMON_TBL_PRODUCTS.PRODUCTS_CATEGORY_CODE
	 *
	 * @mbggenerated Thu Jan 19 18:03:56 SGT 2017
	 */
	public void setProductsCategoryCode(String productsCategoryCode) {
		this.productsCategoryCode = productsCategoryCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_COMMON_TBL_PRODUCTS.ANNUAL_SALE_TARGET
	 *
	 * @return the value of DSP_COMMON_TBL_PRODUCTS.ANNUAL_SALE_TARGET
	 *
	 * @mbggenerated Thu Jan 19 18:03:56 SGT 2017
	 */
	public String getAnnualSaleTarget() {
		return annualSaleTarget;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_COMMON_TBL_PRODUCTS.ANNUAL_SALE_TARGET
	 *
	 * @param annualSaleTarget
	 *            the value for DSP_COMMON_TBL_PRODUCTS.ANNUAL_SALE_TARGET
	 *
	 * @mbggenerated Thu Jan 19 18:03:56 SGT 2017
	 */
	public void setAnnualSaleTarget(String annualSaleTarget) {
		this.annualSaleTarget = annualSaleTarget;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_COMMON_TBL_PRODUCTS.QUOTATION_VALIDITY
	 *
	 * @return the value of DSP_COMMON_TBL_PRODUCTS.QUOTATION_VALIDITY
	 *
	 * @mbggenerated Thu Jan 19 18:03:56 SGT 2017
	 */
	public String getQuotationValidity() {
		return quotationValidity;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_COMMON_TBL_PRODUCTS.QUOTATION_VALIDITY
	 *
	 * @param quotationValidity
	 *            the value for DSP_COMMON_TBL_PRODUCTS.QUOTATION_VALIDITY
	 *
	 * @mbggenerated Thu Jan 19 18:03:56 SGT 2017
	 */
	public void setQuotationValidity(String quotationValidity) {
		this.quotationValidity = quotationValidity;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_COMMON_TBL_PRODUCTS.PRODUCT_ENTITY
	 *
	 * @return the value of DSP_COMMON_TBL_PRODUCTS.PRODUCT_ENTITY
	 *
	 * @mbggenerated Thu Jan 19 18:03:56 SGT 2017
	 */
	public String getProductEntity() {
		return productEntity;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_COMMON_TBL_PRODUCTS.PRODUCT_ENTITY
	 *
	 * @param productEntity
	 *            the value for DSP_COMMON_TBL_PRODUCTS.PRODUCT_ENTITY
	 *
	 * @mbggenerated Thu Jan 19 18:03:56 SGT 2017
	 */
	public void setProductEntity(String productEntity) {
		this.productEntity = productEntity;
	}
}