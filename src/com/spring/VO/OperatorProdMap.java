package com.spring.VO;

public class OperatorProdMap {
	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_ADM_TBL_OPERATOR_PROD_MAP.OP_ID
	 *
	 * @mbggenerated Thu Mar 16 11:46:58 SGT 2017
	 */
	private String opId;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_ADM_TBL_OPERATOR_PROD_MAP.AGP_ID
	 *
	 * @mbggenerated Thu Mar 16 11:46:58 SGT 2017
	 */
	private String agpId;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_ADM_TBL_OPERATOR_PROD_MAP.PRODUCT_CODE
	 *
	 * @mbggenerated Thu Mar 16 11:46:58 SGT 2017
	 */
	private String productCode;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_ADM_TBL_OPERATOR_PROD_MAP.OP_PROD_REF_URL
	 *
	 * @mbggenerated Thu Mar 16 11:46:58 SGT 2017
	 */
	private String opProdRefUrl;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_ADM_TBL_OPERATOR_PROD_MAP.OPERATOR_UUID
	 *
	 * @mbggenerated Thu Mar 16 11:46:58 SGT 2017
	 */
	private String operatorUuid;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_ADM_TBL_OPERATOR_PROD_MAP.OP_ID
	 *
	 * @return the value of DSP_ADM_TBL_OPERATOR_PROD_MAP.OP_ID
	 *
	 * @mbggenerated Thu Mar 16 11:46:58 SGT 2017
	 */
	public String getOpId() {
		return opId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_ADM_TBL_OPERATOR_PROD_MAP.OP_ID
	 *
	 * @param opId
	 *            the value for DSP_ADM_TBL_OPERATOR_PROD_MAP.OP_ID
	 *
	 * @mbggenerated Thu Mar 16 11:46:58 SGT 2017
	 */
	public void setOpId(String opId) {
		this.opId = opId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_ADM_TBL_OPERATOR_PROD_MAP.AGP_ID
	 *
	 * @return the value of DSP_ADM_TBL_OPERATOR_PROD_MAP.AGP_ID
	 *
	 * @mbggenerated Thu Mar 16 11:46:58 SGT 2017
	 */
	public String getAgpId() {
		return agpId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_ADM_TBL_OPERATOR_PROD_MAP.AGP_ID
	 *
	 * @param agpId
	 *            the value for DSP_ADM_TBL_OPERATOR_PROD_MAP.AGP_ID
	 *
	 * @mbggenerated Thu Mar 16 11:46:58 SGT 2017
	 */
	public void setAgpId(String agpId) {
		this.agpId = agpId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_ADM_TBL_OPERATOR_PROD_MAP.PRODUCT_CODE
	 *
	 * @return the value of DSP_ADM_TBL_OPERATOR_PROD_MAP.PRODUCT_CODE
	 *
	 * @mbggenerated Thu Mar 16 11:46:58 SGT 2017
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_ADM_TBL_OPERATOR_PROD_MAP.PRODUCT_CODE
	 *
	 * @param productCode
	 *            the value for DSP_ADM_TBL_OPERATOR_PROD_MAP.PRODUCT_CODE
	 *
	 * @mbggenerated Thu Mar 16 11:46:58 SGT 2017
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_ADM_TBL_OPERATOR_PROD_MAP.OP_PROD_REF_URL
	 *
	 * @return the value of DSP_ADM_TBL_OPERATOR_PROD_MAP.OP_PROD_REF_URL
	 *
	 * @mbggenerated Thu Mar 16 11:46:58 SGT 2017
	 */
	public String getOpProdRefUrl() {
		return opProdRefUrl;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_ADM_TBL_OPERATOR_PROD_MAP.OP_PROD_REF_URL
	 *
	 * @param opProdRefUrl
	 *            the value for DSP_ADM_TBL_OPERATOR_PROD_MAP.OP_PROD_REF_URL
	 *
	 * @mbggenerated Thu Mar 16 11:46:58 SGT 2017
	 */
	public void setOpProdRefUrl(String opProdRefUrl) {
		this.opProdRefUrl = opProdRefUrl;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_ADM_TBL_OPERATOR_PROD_MAP.OPERATOR_UUID
	 *
	 * @return the value of DSP_ADM_TBL_OPERATOR_PROD_MAP.OPERATOR_UUID
	 *
	 * @mbggenerated Thu Mar 16 11:46:58 SGT 2017
	 */
	public String getOperatorUuid() {
		return operatorUuid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_ADM_TBL_OPERATOR_PROD_MAP.OPERATOR_UUID
	 *
	 * @param operatorUuid
	 *            the value for DSP_ADM_TBL_OPERATOR_PROD_MAP.OPERATOR_UUID
	 *
	 * @mbggenerated Thu Mar 16 11:46:58 SGT 2017
	 */
	public void setOperatorUuid(String operatorUuid) {
		this.operatorUuid = operatorUuid;
	}
}