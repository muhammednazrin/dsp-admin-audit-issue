package com.spring.VO.roadtaxVO;

import java.math.BigDecimal;
import java.util.Date;

public class RdtaxInvoice {
	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_RDTAX_INVOICE.RDTAX_ID
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	private Short rdtaxId;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_RDTAX_INVOICE.RDTAX_INVOICE
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	private String rdtaxInvoice;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_RDTAX_INVOICE.AMOUNT
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	private BigDecimal amount;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_RDTAX_INVOICE.DSP_QQ_ID
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	private String dspQqId;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_RDTAX_INVOICE.QQ_ID
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	private String qqId;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_RDTAX_INVOICE.MAIN_POLICY_NO
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	private String mainPolicyNo;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_RDTAX_INVOICE.CREATED_DATE
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	private Date createdDate;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_RDTAX_INVOICE.TRANSACTION_ID
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	private String transactionId;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_RDTAX_INVOICE.PRODUCT_CODE
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	private String productCode;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_RDTAX_INVOICE.TRACK_STATUS
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	private String trackStatus;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_RDTAX_INVOICE.RDTAX_ID
	 *
	 * @return the value of DSP_MI_TBL_RDTAX_INVOICE.RDTAX_ID
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	public Short getRdtaxId() {
		return rdtaxId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_RDTAX_INVOICE.RDTAX_ID
	 *
	 * @param rdtaxId
	 *            the value for DSP_MI_TBL_RDTAX_INVOICE.RDTAX_ID
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	public void setRdtaxId(Short rdtaxId) {
		this.rdtaxId = rdtaxId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_RDTAX_INVOICE.RDTAX_INVOICE
	 *
	 * @return the value of DSP_MI_TBL_RDTAX_INVOICE.RDTAX_INVOICE
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	public String getRdtaxInvoice() {
		return rdtaxInvoice;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_RDTAX_INVOICE.RDTAX_INVOICE
	 *
	 * @param rdtaxInvoice
	 *            the value for DSP_MI_TBL_RDTAX_INVOICE.RDTAX_INVOICE
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	public void setRdtaxInvoice(String rdtaxInvoice) {
		this.rdtaxInvoice = rdtaxInvoice;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_RDTAX_INVOICE.AMOUNT
	 *
	 * @return the value of DSP_MI_TBL_RDTAX_INVOICE.AMOUNT
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_RDTAX_INVOICE.AMOUNT
	 *
	 * @param amount
	 *            the value for DSP_MI_TBL_RDTAX_INVOICE.AMOUNT
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_RDTAX_INVOICE.DSP_QQ_ID
	 *
	 * @return the value of DSP_MI_TBL_RDTAX_INVOICE.DSP_QQ_ID
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	public String getDspQqId() {
		return dspQqId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_RDTAX_INVOICE.DSP_QQ_ID
	 *
	 * @param dspQqId
	 *            the value for DSP_MI_TBL_RDTAX_INVOICE.DSP_QQ_ID
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	public void setDspQqId(String dspQqId) {
		this.dspQqId = dspQqId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_RDTAX_INVOICE.QQ_ID
	 *
	 * @return the value of DSP_MI_TBL_RDTAX_INVOICE.QQ_ID
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	public String getQqId() {
		return qqId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_RDTAX_INVOICE.QQ_ID
	 *
	 * @param qqId
	 *            the value for DSP_MI_TBL_RDTAX_INVOICE.QQ_ID
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	public void setQqId(String qqId) {
		this.qqId = qqId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_RDTAX_INVOICE.MAIN_POLICY_NO
	 *
	 * @return the value of DSP_MI_TBL_RDTAX_INVOICE.MAIN_POLICY_NO
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	public String getMainPolicyNo() {
		return mainPolicyNo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_RDTAX_INVOICE.MAIN_POLICY_NO
	 *
	 * @param mainPolicyNo
	 *            the value for DSP_MI_TBL_RDTAX_INVOICE.MAIN_POLICY_NO
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	public void setMainPolicyNo(String mainPolicyNo) {
		this.mainPolicyNo = mainPolicyNo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_RDTAX_INVOICE.CREATED_DATE
	 *
	 * @return the value of DSP_MI_TBL_RDTAX_INVOICE.CREATED_DATE
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_RDTAX_INVOICE.CREATED_DATE
	 *
	 * @param createdDate
	 *            the value for DSP_MI_TBL_RDTAX_INVOICE.CREATED_DATE
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_RDTAX_INVOICE.TRANSACTION_ID
	 *
	 * @return the value of DSP_MI_TBL_RDTAX_INVOICE.TRANSACTION_ID
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_RDTAX_INVOICE.TRANSACTION_ID
	 *
	 * @param transactionId
	 *            the value for DSP_MI_TBL_RDTAX_INVOICE.TRANSACTION_ID
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_RDTAX_INVOICE.PRODUCT_CODE
	 *
	 * @return the value of DSP_MI_TBL_RDTAX_INVOICE.PRODUCT_CODE
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_RDTAX_INVOICE.PRODUCT_CODE
	 *
	 * @param productCode
	 *            the value for DSP_MI_TBL_RDTAX_INVOICE.PRODUCT_CODE
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_RDTAX_INVOICE.TRACK_STATUS
	 *
	 * @return the value of DSP_MI_TBL_RDTAX_INVOICE.TRACK_STATUS
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	public String getTrackStatus() {
		return trackStatus;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_RDTAX_INVOICE.TRACK_STATUS
	 *
	 * @param trackStatus
	 *            the value for DSP_MI_TBL_RDTAX_INVOICE.TRACK_STATUS
	 *
	 * @mbg.generated Sat Sep 16 10:55:36 SGT 2017
	 */
	public void setTrackStatus(String trackStatus) {
		this.trackStatus = trackStatus;
	}
}