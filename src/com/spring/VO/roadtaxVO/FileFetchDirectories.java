package com.spring.VO.roadtaxVO;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class FileFetchDirectories {

	public List<ConsignmentFileDetail> fetchDirectories(final File folderdirectories, String UPLOADED_FOLDER) {
		List<ConsignmentFileDetail> consignmentfiles = new ArrayList<ConsignmentFileDetail>();
		List<File> consignmentf = new ArrayList<File>();
		int filecounter = 0;
		Date olddate = null;
		Date newdate = null;

		List<Date> filedate = new ArrayList<Date>();
		Date fileactivedate = new Date();
		File activeFile = null;
		System.out.println("  DDDDDDDDDDDDDDDDDDDDDD /////////////////////////////////      " + folderdirectories);
		for (final File fileEntry : folderdirectories.listFiles()) {
			try {

				if (fileEntry.isDirectory()) {
					fetchDirectories(fileEntry, UPLOADED_FOLDER);
				} else {
					filecounter = filecounter + 1;
					File fileplace = new File(UPLOADED_FOLDER + fileEntry.getName());
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					Date date = sdf.parse(sdf.format(fileplace.lastModified()));

					if (olddate != null) {
						newdate = date;
						Calendar cal1 = Calendar.getInstance();
						cal1.setTime(olddate);
						Calendar cal2 = Calendar.getInstance();
						cal2.setTime(newdate);

						if (cal2.after(cal1)) {
							fileactivedate = date;
							olddate = date;
							activeFile = fileplace;
						} else {
							fileactivedate = olddate;
						}
					} else {
						olddate = date;
						activeFile = fileplace;
					}

					consignmentf.add(fileplace);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		for (int i = 0; i < filecounter; i++) {
			try {
				File consignmentFile = consignmentf.get(i);
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				System.out.println(activeFile.getName() + "   File last modified date  : "
						+ sdf.format(consignmentFile.lastModified()));
				Date date = sdf.parse(sdf.format(consignmentFile.lastModified()));

				ConsignmentFileDetail consignmentfile = new ConsignmentFileDetail();
				consignmentfile.setFilename(consignmentFile.getName());
				consignmentfile.setFilePath(consignmentFile.getAbsolutePath());
				if (activeFile.getName().equalsIgnoreCase(consignmentFile.getName())) {
					consignmentfile.setFilestatus("Active");
				} else {
					consignmentfile.setFilestatus("InActive");
				}
				consignmentfile.setFilecreationdate(sdf.format(consignmentFile.lastModified()));
				consignmentfiles.add(consignmentfile);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return consignmentfiles;

	}

}
