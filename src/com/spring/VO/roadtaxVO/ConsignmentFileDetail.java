package com.spring.VO.roadtaxVO;

public class ConsignmentFileDetail {

	private String filePath;
	private String filename;
	private String filecreationdate;
	private String filestatus;

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFilecreationdate() {
		return filecreationdate;
	}

	public void setFilecreationdate(String filecreationdate) {
		this.filecreationdate = filecreationdate;
	}

	public String getFilestatus() {
		return filestatus;
	}

	public void setFilestatus(String filestatus) {
		this.filestatus = filestatus;
	}

}
