package com.spring.VO.roadtaxVO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ConsignmentNumberMaintenanceExample {
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database table DSP_MI_TBL_RDTAX_CONSIGNMENT
	 *
	 * @mbggenerated Mon Oct 09 13:51:06 SGT 2017
	 */
	protected String orderByClause;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database table DSP_MI_TBL_RDTAX_CONSIGNMENT
	 *
	 * @mbggenerated Mon Oct 09 13:51:06 SGT 2017
	 */
	protected boolean distinct;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database table DSP_MI_TBL_RDTAX_CONSIGNMENT
	 *
	 * @mbggenerated Mon Oct 09 13:51:06 SGT 2017
	 */
	protected List<Criteria> oredCriteria;

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_RDTAX_CONSIGNMENT
	 *
	 * @mbggenerated Mon Oct 09 13:51:06 SGT 2017
	 */
	public ConsignmentNumberMaintenanceExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_RDTAX_CONSIGNMENT
	 *
	 * @mbggenerated Mon Oct 09 13:51:06 SGT 2017
	 */
	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_RDTAX_CONSIGNMENT
	 *
	 * @mbggenerated Mon Oct 09 13:51:06 SGT 2017
	 */
	public String getOrderByClause() {
		return orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_RDTAX_CONSIGNMENT
	 *
	 * @mbggenerated Mon Oct 09 13:51:06 SGT 2017
	 */
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_RDTAX_CONSIGNMENT
	 *
	 * @mbggenerated Mon Oct 09 13:51:06 SGT 2017
	 */
	public boolean isDistinct() {
		return distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_RDTAX_CONSIGNMENT
	 *
	 * @mbggenerated Mon Oct 09 13:51:06 SGT 2017
	 */
	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_RDTAX_CONSIGNMENT
	 *
	 * @mbggenerated Mon Oct 09 13:51:06 SGT 2017
	 */
	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_RDTAX_CONSIGNMENT
	 *
	 * @mbggenerated Mon Oct 09 13:51:06 SGT 2017
	 */
	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_RDTAX_CONSIGNMENT
	 *
	 * @mbggenerated Mon Oct 09 13:51:06 SGT 2017
	 */
	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_RDTAX_CONSIGNMENT
	 *
	 * @mbggenerated Mon Oct 09 13:51:06 SGT 2017
	 */
	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_RDTAX_CONSIGNMENT
	 *
	 * @mbggenerated Mon Oct 09 13:51:06 SGT 2017
	 */
	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the
	 * database table DSP_MI_TBL_RDTAX_CONSIGNMENT
	 *
	 * @mbggenerated Mon Oct 09 13:51:06 SGT 2017
	 */
	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("ID is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("ID is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(BigDecimal value) {
			addCriterion("ID =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(BigDecimal value) {
			addCriterion("ID <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(BigDecimal value) {
			addCriterion("ID >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(BigDecimal value) {
			addCriterion("ID >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(BigDecimal value) {
			addCriterion("ID <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(BigDecimal value) {
			addCriterion("ID <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<BigDecimal> values) {
			addCriterion("ID in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<BigDecimal> values) {
			addCriterion("ID not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("ID between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("ID not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andQqIdIsNull() {
			addCriterion("QQ_ID is null");
			return (Criteria) this;
		}

		public Criteria andQqIdIsNotNull() {
			addCriterion("QQ_ID is not null");
			return (Criteria) this;
		}

		public Criteria andQqIdEqualTo(String value) {
			addCriterion("QQ_ID =", value, "qqId");
			return (Criteria) this;
		}

		public Criteria andQqIdNotEqualTo(String value) {
			addCriterion("QQ_ID <>", value, "qqId");
			return (Criteria) this;
		}

		public Criteria andQqIdGreaterThan(String value) {
			addCriterion("QQ_ID >", value, "qqId");
			return (Criteria) this;
		}

		public Criteria andQqIdGreaterThanOrEqualTo(String value) {
			addCriterion("QQ_ID >=", value, "qqId");
			return (Criteria) this;
		}

		public Criteria andQqIdLessThan(String value) {
			addCriterion("QQ_ID <", value, "qqId");
			return (Criteria) this;
		}

		public Criteria andQqIdLessThanOrEqualTo(String value) {
			addCriterion("QQ_ID <=", value, "qqId");
			return (Criteria) this;
		}

		public Criteria andQqIdLike(String value) {
			addCriterion("QQ_ID like", value, "qqId");
			return (Criteria) this;
		}

		public Criteria andQqIdNotLike(String value) {
			addCriterion("QQ_ID not like", value, "qqId");
			return (Criteria) this;
		}

		public Criteria andQqIdIn(List<String> values) {
			addCriterion("QQ_ID in", values, "qqId");
			return (Criteria) this;
		}

		public Criteria andQqIdNotIn(List<String> values) {
			addCriterion("QQ_ID not in", values, "qqId");
			return (Criteria) this;
		}

		public Criteria andQqIdBetween(String value1, String value2) {
			addCriterion("QQ_ID between", value1, value2, "qqId");
			return (Criteria) this;
		}

		public Criteria andQqIdNotBetween(String value1, String value2) {
			addCriterion("QQ_ID not between", value1, value2, "qqId");
			return (Criteria) this;
		}

		public Criteria andConsignmentNoIsNull() {
			addCriterion("CONSIGNMENT_NO is null");
			return (Criteria) this;
		}

		public Criteria andConsignmentNoIsNotNull() {
			addCriterion("CONSIGNMENT_NO is not null");
			return (Criteria) this;
		}

		public Criteria andConsignmentNoEqualTo(String value) {
			addCriterion("CONSIGNMENT_NO =", value, "consignmentNo");
			return (Criteria) this;
		}

		public Criteria andConsignmentNoNotEqualTo(String value) {
			addCriterion("CONSIGNMENT_NO <>", value, "consignmentNo");
			return (Criteria) this;
		}

		public Criteria andConsignmentNoGreaterThan(String value) {
			addCriterion("CONSIGNMENT_NO >", value, "consignmentNo");
			return (Criteria) this;
		}

		public Criteria andConsignmentNoGreaterThanOrEqualTo(String value) {
			addCriterion("CONSIGNMENT_NO >=", value, "consignmentNo");
			return (Criteria) this;
		}

		public Criteria andConsignmentNoLessThan(String value) {
			addCriterion("CONSIGNMENT_NO <", value, "consignmentNo");
			return (Criteria) this;
		}

		public Criteria andConsignmentNoLessThanOrEqualTo(String value) {
			addCriterion("CONSIGNMENT_NO <=", value, "consignmentNo");
			return (Criteria) this;
		}

		public Criteria andConsignmentNoLike(String value) {
			addCriterion("CONSIGNMENT_NO like", value, "consignmentNo");
			return (Criteria) this;
		}

		public Criteria andConsignmentNoNotLike(String value) {
			addCriterion("CONSIGNMENT_NO not like", value, "consignmentNo");
			return (Criteria) this;
		}

		public Criteria andConsignmentNoIn(List<String> values) {
			addCriterion("CONSIGNMENT_NO in", values, "consignmentNo");
			return (Criteria) this;
		}

		public Criteria andConsignmentNoNotIn(List<String> values) {
			addCriterion("CONSIGNMENT_NO not in", values, "consignmentNo");
			return (Criteria) this;
		}

		public Criteria andConsignmentNoBetween(String value1, String value2) {
			addCriterion("CONSIGNMENT_NO between", value1, value2, "consignmentNo");
			return (Criteria) this;
		}

		public Criteria andConsignmentNoNotBetween(String value1, String value2) {
			addCriterion("CONSIGNMENT_NO not between", value1, value2, "consignmentNo");
			return (Criteria) this;
		}

		public Criteria andIsUsedIsNull() {
			addCriterion("IS_USED is null");
			return (Criteria) this;
		}

		public Criteria andIsUsedIsNotNull() {
			addCriterion("IS_USED is not null");
			return (Criteria) this;
		}

		public Criteria andIsUsedEqualTo(String value) {
			addCriterion("IS_USED =", value, "isUsed");
			return (Criteria) this;
		}

		public Criteria andIsUsedNotEqualTo(String value) {
			addCriterion("IS_USED <>", value, "isUsed");
			return (Criteria) this;
		}

		public Criteria andIsUsedGreaterThan(String value) {
			addCriterion("IS_USED >", value, "isUsed");
			return (Criteria) this;
		}

		public Criteria andIsUsedGreaterThanOrEqualTo(String value) {
			addCriterion("IS_USED >=", value, "isUsed");
			return (Criteria) this;
		}

		public Criteria andIsUsedLessThan(String value) {
			addCriterion("IS_USED <", value, "isUsed");
			return (Criteria) this;
		}

		public Criteria andIsUsedLessThanOrEqualTo(String value) {
			addCriterion("IS_USED <=", value, "isUsed");
			return (Criteria) this;
		}

		public Criteria andIsUsedLike(String value) {
			addCriterion("IS_USED like", value, "isUsed");
			return (Criteria) this;
		}

		public Criteria andIsUsedNotLike(String value) {
			addCriterion("IS_USED not like", value, "isUsed");
			return (Criteria) this;
		}

		public Criteria andIsUsedIn(List<String> values) {
			addCriterion("IS_USED in", values, "isUsed");
			return (Criteria) this;
		}

		public Criteria andIsUsedNotIn(List<String> values) {
			addCriterion("IS_USED not in", values, "isUsed");
			return (Criteria) this;
		}

		public Criteria andIsUsedBetween(String value1, String value2) {
			addCriterion("IS_USED between", value1, value2, "isUsed");
			return (Criteria) this;
		}

		public Criteria andIsUsedNotBetween(String value1, String value2) {
			addCriterion("IS_USED not between", value1, value2, "isUsed");
			return (Criteria) this;
		}

		public Criteria andEntityIsNull() {
			addCriterion("ENTITY is null");
			return (Criteria) this;
		}

		public Criteria andEntityIsNotNull() {
			addCriterion("ENTITY is not null");
			return (Criteria) this;
		}

		public Criteria andEntityEqualTo(String value) {
			addCriterion("ENTITY =", value, "entity");
			return (Criteria) this;
		}

		public Criteria andEntityNotEqualTo(String value) {
			addCriterion("ENTITY <>", value, "entity");
			return (Criteria) this;
		}

		public Criteria andEntityGreaterThan(String value) {
			addCriterion("ENTITY >", value, "entity");
			return (Criteria) this;
		}

		public Criteria andEntityGreaterThanOrEqualTo(String value) {
			addCriterion("ENTITY >=", value, "entity");
			return (Criteria) this;
		}

		public Criteria andEntityLessThan(String value) {
			addCriterion("ENTITY <", value, "entity");
			return (Criteria) this;
		}

		public Criteria andEntityLessThanOrEqualTo(String value) {
			addCriterion("ENTITY <=", value, "entity");
			return (Criteria) this;
		}

		public Criteria andEntityLike(String value) {
			addCriterion("ENTITY like", value, "entity");
			return (Criteria) this;
		}

		public Criteria andEntityNotLike(String value) {
			addCriterion("ENTITY not like", value, "entity");
			return (Criteria) this;
		}

		public Criteria andEntityIn(List<String> values) {
			addCriterion("ENTITY in", values, "entity");
			return (Criteria) this;
		}

		public Criteria andEntityNotIn(List<String> values) {
			addCriterion("ENTITY not in", values, "entity");
			return (Criteria) this;
		}

		public Criteria andEntityBetween(String value1, String value2) {
			addCriterion("ENTITY between", value1, value2, "entity");
			return (Criteria) this;
		}

		public Criteria andEntityNotBetween(String value1, String value2) {
			addCriterion("ENTITY not between", value1, value2, "entity");
			return (Criteria) this;
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the
	 * database table DSP_MI_TBL_RDTAX_CONSIGNMENT
	 *
	 * @mbggenerated do_not_delete_during_merge Mon Oct 09 13:51:06 SGT 2017
	 */
	public static class Criteria extends GeneratedCriteria {

		protected Criteria() {
			super();
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the
	 * database table DSP_MI_TBL_RDTAX_CONSIGNMENT
	 *
	 * @mbggenerated Mon Oct 09 13:51:06 SGT 2017
	 */
	public static class Criterion {
		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}
}