package com.spring.VO.roadtaxVO;

import java.math.BigDecimal;

import com.spring.VO.Customer;

public class RdtaxTrack extends Customer {
	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_RDTAX_TRACK.TRACK_ID
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	private BigDecimal trackId;

	private String remarks;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_RDTAX_TRACK.QQ_ID
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	// private String qqIdTrack;

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_RDTAX_TRACK.DSP_QQ_ID
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	private String dspQqId;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_RDTAX_TRACK.POLICY_NO
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	private String policyNo;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_RDTAX_TRACK.JPJ_STATUS
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	private String jpjStatus;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_RDTAX_TRACK.TRACKING_NO
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	private String trackingNo;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_RDTAX_TRACK.PRINT_STATUS
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	private String printStatus;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_RDTAX_TRACK.REFUND_STATUS
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	private String refundStatus;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_RDTAX_TRACK.TRACK_ID
	 *
	 * @return the value of DSP_MI_TBL_RDTAX_TRACK.TRACK_ID
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	public BigDecimal getTrackId() {
		return trackId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_RDTAX_TRACK.TRACK_ID
	 *
	 * @param trackId
	 *            the value for DSP_MI_TBL_RDTAX_TRACK.TRACK_ID
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	public void setTrackId(BigDecimal trackId) {
		this.trackId = trackId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_RDTAX_TRACK.QQ_ID
	 *
	 * @return the value of DSP_MI_TBL_RDTAX_TRACK.QQ_ID
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	/*
	 * public String getQqIdTrack() { return qqIdTrack; }
	 */

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_RDTAX_TRACK.QQ_ID
	 *
	 * @param qqId
	 *            the value for DSP_MI_TBL_RDTAX_TRACK.QQ_ID
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	/*
	 * public void setQqIdTrack(String qqIdTrack) { this.qqIdTrack = qqIdTrack; }
	 */

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_RDTAX_TRACK.DSP_QQ_ID
	 *
	 * @return the value of DSP_MI_TBL_RDTAX_TRACK.DSP_QQ_ID
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	public String getDspQqId() {
		return dspQqId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_RDTAX_TRACK.DSP_QQ_ID
	 *
	 * @param dspQqId
	 *            the value for DSP_MI_TBL_RDTAX_TRACK.DSP_QQ_ID
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	public void setDspQqId(String dspQqId) {
		this.dspQqId = dspQqId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_RDTAX_TRACK.POLICY_NO
	 *
	 * @return the value of DSP_MI_TBL_RDTAX_TRACK.POLICY_NO
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	public String getPolicyNo() {
		return policyNo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_RDTAX_TRACK.POLICY_NO
	 *
	 * @param policyNo
	 *            the value for DSP_MI_TBL_RDTAX_TRACK.POLICY_NO
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_RDTAX_TRACK.JPJ_STATUS
	 *
	 * @return the value of DSP_MI_TBL_RDTAX_TRACK.JPJ_STATUS
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	public String getJpjStatus() {
		return jpjStatus;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_RDTAX_TRACK.JPJ_STATUS
	 *
	 * @param jpjStatus
	 *            the value for DSP_MI_TBL_RDTAX_TRACK.JPJ_STATUS
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	public void setJpjStatus(String jpjStatus) {
		this.jpjStatus = jpjStatus;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_RDTAX_TRACK.TRACKING_NO
	 *
	 * @return the value of DSP_MI_TBL_RDTAX_TRACK.TRACKING_NO
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	public String getTrackingNo() {
		return trackingNo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_RDTAX_TRACK.TRACKING_NO
	 *
	 * @param trackingNo
	 *            the value for DSP_MI_TBL_RDTAX_TRACK.TRACKING_NO
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_RDTAX_TRACK.PRINT_STATUS
	 *
	 * @return the value of DSP_MI_TBL_RDTAX_TRACK.PRINT_STATUS
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	public String getPrintStatus() {
		return printStatus;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_RDTAX_TRACK.PRINT_STATUS
	 *
	 * @param printStatus
	 *            the value for DSP_MI_TBL_RDTAX_TRACK.PRINT_STATUS
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	public void setPrintStatus(String printStatus) {
		this.printStatus = printStatus;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_RDTAX_TRACK.REFUND_STATUS
	 *
	 * @return the value of DSP_MI_TBL_RDTAX_TRACK.REFUND_STATUS
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	public String getRefundStatus() {
		return refundStatus;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_RDTAX_TRACK.REFUND_STATUS
	 *
	 * @param refundStatus
	 *            the value for DSP_MI_TBL_RDTAX_TRACK.REFUND_STATUS
	 *
	 * @mbggenerated Fri Aug 25 21:23:53 SGT 2017
	 */
	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}
}