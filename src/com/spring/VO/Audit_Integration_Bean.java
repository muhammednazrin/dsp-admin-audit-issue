package com.spring.VO;

public class Audit_Integration_Bean {

	private String policynumber;
	private String recon_id;
	private String dateint;
	private String remarkint;
	private String statuspolm;
	private String dateint2;
	private String remarkint2;
	private String statuspolm2;
	private String updatetimestamp;

	public String getPolicynumber() {
		return policynumber;
	}

	public void setPolicynumber(String policynumber) {
		this.policynumber = policynumber;
	}

	public String getRecon_id() {
		return recon_id;
	}

	public void setRecon_id(String recon_id) {
		this.recon_id = recon_id;
	}

	public String getDateint() {
		return dateint;
	}

	public void setDateint(String dateint) {
		this.dateint = dateint;
	}

	public String getRemarkint() {
		return remarkint;
	}

	public void setRemarkint(String remarkint) {
		this.remarkint = remarkint;
	}

	public String getStatuspolm() {
		return statuspolm;
	}

	public void setStatuspolm(String statuspolm) {
		this.statuspolm = statuspolm;
	}

	public String getDateint2() {
		return dateint2;
	}

	public void setDateint2(String dateint2) {
		this.dateint2 = dateint2;
	}

	public String getRemarkint2() {
		return remarkint2;
	}

	public void setRemarkint2(String remarkint2) {
		this.remarkint2 = remarkint2;
	}

	public String getUpdatetimestamp() {
		return updatetimestamp;
	}

	public void setUpdatetimestamp(String updatetimestamp) {
		this.updatetimestamp = updatetimestamp;
	}

	public String getStatuspolm2() {
		return statuspolm2;
	}

	public void setStatuspolm2(String statuspolm2) {
		this.statuspolm2 = statuspolm2;
	}

}
