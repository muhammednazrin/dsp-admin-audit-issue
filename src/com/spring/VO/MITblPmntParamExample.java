package com.spring.VO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MITblPmntParamExample {
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database table DSP_COMMON_PMNT_GATEWAY_PARAMS
	 *
	 * @mbg.generated Wed Apr 12 10:52:26 SGT 2017
	 */
	protected String orderByClause;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database table DSP_COMMON_PMNT_GATEWAY_PARAMS
	 *
	 * @mbg.generated Wed Apr 12 10:52:26 SGT 2017
	 */
	protected boolean distinct;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database table DSP_COMMON_PMNT_GATEWAY_PARAMS
	 *
	 * @mbg.generated Wed Apr 12 10:52:26 SGT 2017
	 */
	protected List<Criteria> oredCriteria;

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_COMMON_PMNT_GATEWAY_PARAMS
	 *
	 * @mbg.generated Wed Apr 12 10:52:26 SGT 2017
	 */
	public MITblPmntParamExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_COMMON_PMNT_GATEWAY_PARAMS
	 *
	 * @mbg.generated Wed Apr 12 10:52:26 SGT 2017
	 */
	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_COMMON_PMNT_GATEWAY_PARAMS
	 *
	 * @mbg.generated Wed Apr 12 10:52:26 SGT 2017
	 */
	public String getOrderByClause() {
		return orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_COMMON_PMNT_GATEWAY_PARAMS
	 *
	 * @mbg.generated Wed Apr 12 10:52:26 SGT 2017
	 */
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_COMMON_PMNT_GATEWAY_PARAMS
	 *
	 * @mbg.generated Wed Apr 12 10:52:26 SGT 2017
	 */
	public boolean isDistinct() {
		return distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_COMMON_PMNT_GATEWAY_PARAMS
	 *
	 * @mbg.generated Wed Apr 12 10:52:26 SGT 2017
	 */
	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_COMMON_PMNT_GATEWAY_PARAMS
	 *
	 * @mbg.generated Wed Apr 12 10:52:26 SGT 2017
	 */
	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_COMMON_PMNT_GATEWAY_PARAMS
	 *
	 * @mbg.generated Wed Apr 12 10:52:26 SGT 2017
	 */
	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_COMMON_PMNT_GATEWAY_PARAMS
	 *
	 * @mbg.generated Wed Apr 12 10:52:26 SGT 2017
	 */
	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_COMMON_PMNT_GATEWAY_PARAMS
	 *
	 * @mbg.generated Wed Apr 12 10:52:26 SGT 2017
	 */
	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_COMMON_PMNT_GATEWAY_PARAMS
	 *
	 * @mbg.generated Wed Apr 12 10:52:26 SGT 2017
	 */
	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the
	 * database table DSP_COMMON_PMNT_GATEWAY_PARAMS
	 *
	 * @mbg.generated Wed Apr 12 10:52:26 SGT 2017
	 */
	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("mtpmnt.ID is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("mtpmnt.ID is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(BigDecimal value) {
			addCriterion("mtpmnt.ID =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(BigDecimal value) {
			addCriterion("mtpmnt.ID <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(BigDecimal value) {
			addCriterion("mtpmnt.ID >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(BigDecimal value) {
			addCriterion("mtpmnt.ID >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(BigDecimal value) {
			addCriterion("mtpmnt.ID <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(BigDecimal value) {
			addCriterion("mtpmnt.ID <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<BigDecimal> values) {
			addCriterion("mtpmnt.ID in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<BigDecimal> values) {
			addCriterion("mtpmnt.ID not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("mtpmnt.ID between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("mtpmnt.ID not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andPmntGatewayCodeIsNull() {
			addCriterion("mtpmnt.PMNT_GATEWAY_CODE is null");
			return (Criteria) this;
		}

		public Criteria andPmntGatewayCodeIsNotNull() {
			addCriterion("mtpmnt.PMNT_GATEWAY_CODE is not null");
			return (Criteria) this;
		}

		public Criteria andPmntGatewayCodeEqualTo(String value) {
			addCriterion("mtpmnt.PMNT_GATEWAY_CODE =", value, "pmntGatewayCode");
			return (Criteria) this;
		}

		public Criteria andPmntGatewayCodeNotEqualTo(String value) {
			addCriterion("mtpmnt.PMNT_GATEWAY_CODE <>", value, "pmntGatewayCode");
			return (Criteria) this;
		}

		public Criteria andPmntGatewayCodeGreaterThan(String value) {
			addCriterion("mtpmnt.PMNT_GATEWAY_CODE >", value, "pmntGatewayCode");
			return (Criteria) this;
		}

		public Criteria andPmntGatewayCodeGreaterThanOrEqualTo(String value) {
			addCriterion("mtpmnt.PMNT_GATEWAY_CODE >=", value, "pmntGatewayCode");
			return (Criteria) this;
		}

		public Criteria andPmntGatewayCodeLessThan(String value) {
			addCriterion("mtpmnt.PMNT_GATEWAY_CODE <", value, "pmntGatewayCode");
			return (Criteria) this;
		}

		public Criteria andPmntGatewayCodeLessThanOrEqualTo(String value) {
			addCriterion("mtpmnt.PMNT_GATEWAY_CODE <=", value, "pmntGatewayCode");
			return (Criteria) this;
		}

		public Criteria andPmntGatewayCodeLike(String value) {
			addCriterion("mtpmnt.PMNT_GATEWAY_CODE like", value, "pmntGatewayCode");
			return (Criteria) this;
		}

		public Criteria andPmntGatewayCodeNotLike(String value) {
			addCriterion("mtpmnt.PMNT_GATEWAY_CODE not like", value, "pmntGatewayCode");
			return (Criteria) this;
		}

		public Criteria andPmntGatewayCodeIn(List<String> values) {
			addCriterion("mtpmnt.PMNT_GATEWAY_CODE in", values, "pmntGatewayCode");
			return (Criteria) this;
		}

		public Criteria andPmntGatewayCodeNotIn(List<String> values) {
			addCriterion("mtpmnt.PMNT_GATEWAY_CODE not in", values, "pmntGatewayCode");
			return (Criteria) this;
		}

		public Criteria andPmntGatewayCodeBetween(String value1, String value2) {
			addCriterion("mtpmnt.PMNT_GATEWAY_CODE between", value1, value2, "pmntGatewayCode");
			return (Criteria) this;
		}

		public Criteria andPmntGatewayCodeNotBetween(String value1, String value2) {
			addCriterion("mtpmnt.PMNT_GATEWAY_CODE not between", value1, value2, "pmntGatewayCode");
			return (Criteria) this;
		}

		public Criteria andParamNameIsNull() {
			addCriterion("mtpmnt.PARAM_NAME is null");
			return (Criteria) this;
		}

		public Criteria andParamNameIsNotNull() {
			addCriterion("mtpmnt.PARAM_NAME is not null");
			return (Criteria) this;
		}

		public Criteria andParamNameEqualTo(String value) {
			addCriterion("mtpmnt.PARAM_NAME =", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameNotEqualTo(String value) {
			addCriterion("mtpmnt.PARAM_NAME <>", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameGreaterThan(String value) {
			addCriterion("mtpmnt.PARAM_NAME >", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameGreaterThanOrEqualTo(String value) {
			addCriterion("mtpmnt.PARAM_NAME >=", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameLessThan(String value) {
			addCriterion("mtpmnt.PARAM_NAME <", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameLessThanOrEqualTo(String value) {
			addCriterion("mtpmnt.PARAM_NAME <=", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameLike(String value) {
			addCriterion("mtpmnt.PARAM_NAME like", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameNotLike(String value) {
			addCriterion("mtpmnt.PARAM_NAME not like", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameIn(List<String> values) {
			addCriterion("mtpmnt.PARAM_NAME in", values, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameNotIn(List<String> values) {
			addCriterion("mtpmnt.PARAM_NAME not in", values, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameBetween(String value1, String value2) {
			addCriterion("mtpmnt.PARAM_NAME between", value1, value2, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameNotBetween(String value1, String value2) {
			addCriterion("mtpmnt.PARAM_NAME not between", value1, value2, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamTypeIsNull() {
			addCriterion("mtpmnt.PARAM_TYPE is null");
			return (Criteria) this;
		}

		public Criteria andParamTypeIsNotNull() {
			addCriterion("mtpmnt.PARAM_TYPE is not null");
			return (Criteria) this;
		}

		public Criteria andParamTypeEqualTo(String value) {
			addCriterion("mtpmnt.PARAM_TYPE =", value, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamTypeNotEqualTo(String value) {
			addCriterion("mtpmnt.PARAM_TYPE <>", value, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamTypeGreaterThan(String value) {
			addCriterion("mtpmnt.PARAM_TYPE >", value, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamTypeGreaterThanOrEqualTo(String value) {
			addCriterion("mtpmnt.PARAM_TYPE >=", value, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamTypeLessThan(String value) {
			addCriterion("mtpmnt.PARAM_TYPE <", value, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamTypeLessThanOrEqualTo(String value) {
			addCriterion("mtpmnt.PARAM_TYPE <=", value, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamTypeLike(String value) {
			addCriterion("mtpmnt.PARAM_TYPE like", value, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamTypeNotLike(String value) {
			addCriterion("mtpmnt.PARAM_TYPE not like", value, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamTypeIn(List<String> values) {
			addCriterion("mtpmnt.PARAM_TYPE in", values, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamTypeNotIn(List<String> values) {
			addCriterion("mtpmnt.PARAM_TYPE not in", values, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamTypeBetween(String value1, String value2) {
			addCriterion("mtpmnt.PARAM_TYPE between", value1, value2, "paramType");
			return (Criteria) this;
		}

		public Criteria andParamTypeNotBetween(String value1, String value2) {
			addCriterion("mtpmnt.PARAM_TYPE not between", value1, value2, "paramType");
			return (Criteria) this;
		}

		public Criteria andStatusIsNull() {
			addCriterion("mtpmnt.STATUS is null");
			return (Criteria) this;
		}

		public Criteria andStatusIsNotNull() {
			addCriterion("mtpmnt.STATUS is not null");
			return (Criteria) this;
		}

		public Criteria andStatusEqualTo(Short value) {
			addCriterion("mtpmnt.STATUS =", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotEqualTo(Short value) {
			addCriterion("mtpmnt.STATUS <>", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusGreaterThan(Short value) {
			addCriterion("mtpmnt.STATUS >", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusGreaterThanOrEqualTo(Short value) {
			addCriterion("mtpmnt.STATUS >=", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusLessThan(Short value) {
			addCriterion("mtpmnt.STATUS <", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusLessThanOrEqualTo(Short value) {
			addCriterion("mtpmnt.STATUS <=", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusIn(List<Short> values) {
			addCriterion("mtpmnt.STATUS in", values, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotIn(List<Short> values) {
			addCriterion("mtpmnt.STATUS not in", values, "status");
			return (Criteria) this;
		}

		public Criteria andStatusBetween(Short value1, Short value2) {
			addCriterion("mtpmnt.STATUS between", value1, value2, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotBetween(Short value1, Short value2) {
			addCriterion("mtpmnt.STATUS not between", value1, value2, "status");
			return (Criteria) this;
		}

		public Criteria andProductCodeIsNull() {
			addCriterion("mtpmnt.PRODUCT_CODE is null");
			return (Criteria) this;
		}

		public Criteria andProductCodeIsNotNull() {
			addCriterion("mtpmnt.PRODUCT_CODE is not null");
			return (Criteria) this;
		}

		public Criteria andProductCodeEqualTo(String value) {
			addCriterion("mtpmnt.PRODUCT_CODE =", value, "productCode");
			return (Criteria) this;
		}

		public Criteria andProductCodeNotEqualTo(String value) {
			addCriterion("mtpmnt.PRODUCT_CODE <>", value, "productCode");
			return (Criteria) this;
		}

		public Criteria andProductCodeGreaterThan(String value) {
			addCriterion("mtpmnt.PRODUCT_CODE >", value, "productCode");
			return (Criteria) this;
		}

		public Criteria andProductCodeGreaterThanOrEqualTo(String value) {
			addCriterion("mtpmnt.PRODUCT_CODE >=", value, "productCode");
			return (Criteria) this;
		}

		public Criteria andProductCodeLessThan(String value) {
			addCriterion("mtpmnt.PRODUCT_CODE <", value, "productCode");
			return (Criteria) this;
		}

		public Criteria andProductCodeLessThanOrEqualTo(String value) {
			addCriterion("mtpmnt.PRODUCT_CODE <=", value, "productCode");
			return (Criteria) this;
		}

		public Criteria andProductCodeLike(String value) {
			addCriterion("mtpmnt.PRODUCT_CODE like", value, "productCode");
			return (Criteria) this;
		}

		public Criteria andProductCodeNotLike(String value) {
			addCriterion("mtpmnt.PRODUCT_CODE not like", value, "productCode");
			return (Criteria) this;
		}

		public Criteria andProductCodeIn(List<String> values) {
			addCriterion("mtpmnt.PRODUCT_CODE in", values, "productCode");
			return (Criteria) this;
		}

		public Criteria andProductCodeNotIn(List<String> values) {
			addCriterion("mtpmnt.PRODUCT_CODE not in", values, "productCode");
			return (Criteria) this;
		}

		public Criteria andProductCodeBetween(String value1, String value2) {
			addCriterion("mtpmnt.PRODUCT_CODE between", value1, value2, "productCode");
			return (Criteria) this;
		}

		public Criteria andProductCodeNotBetween(String value1, String value2) {
			addCriterion("mtpmnt.PRODUCT_CODE not between", value1, value2, "productCode");
			return (Criteria) this;
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the
	 * database table DSP_COMMON_PMNT_GATEWAY_PARAMS
	 *
	 * @mbg.generated Wed Apr 12 10:52:26 SGT 2017
	 */
	public static class Criterion {
		private String condition;
		private Object value;
		private Object secondValue;
		private boolean noValue;
		private boolean singleValue;
		private boolean betweenValue;
		private boolean listValue;
		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the
	 * database table DSP_COMMON_PMNT_GATEWAY_PARAMS
	 *
	 * @mbg.generated do_not_delete_during_merge Wed Apr 12 10:47:47 SGT 2017
	 */
	public static class Criteria extends GeneratedCriteria {

		protected Criteria() {
			super();
		}
	}
}