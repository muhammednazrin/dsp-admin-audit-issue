package com.spring.VO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class BuddyPABenefitGrpList implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 6975011894419159958L;

	private Object comboId;
	private Object comboName;
	private Object companyId;
	private Object planId;
	private Object planName;
	private Object benefitId;
	private Object benefitName;
	private BigDecimal adultSumInsured;
	private BigDecimal childSumInsured;
	private BigDecimal adultLoading;
	private BigDecimal childLoading;
	private BigDecimal adultPremium;
	private BigDecimal childPremium;
	private Object enable;
	private Date createdDt;
	private Object createdBy;
	private Date modifiedDt;
	private Object modifiedBy;
	private Short version;

	public Object getComboId() {
		return comboId;
	}

	public void setComboId(Object comboId) {
		this.comboId = comboId;
	}

	public Object getComboName() {
		return comboName;
	}

	public void setComboName(Object comboName) {
		this.comboName = comboName;
	}

	public Object getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Object companyId) {
		this.companyId = companyId;
	}

	public Object getPlanId() {
		return planId;
	}

	public void setPlanId(Object planId) {
		this.planId = planId;
	}

	public Object getPlanName() {
		return planName;
	}

	public void setPlanName(Object planName) {
		this.planName = planName;
	}

	public Object getBenefitId() {
		return benefitId;
	}

	public void setBenefitId(Object benefitId) {
		this.benefitId = benefitId;
	}

	public Object getBenefitName() {
		return benefitName;
	}

	public void setBenefitName(Object benefitName) {
		this.benefitName = benefitName;
	}

	public BigDecimal getAdultSumInsured() {
		return adultSumInsured;
	}

	public void setAdultSumInsured(BigDecimal adultSumInsured) {
		this.adultSumInsured = adultSumInsured;
	}

	public BigDecimal getChildSumInsured() {
		return childSumInsured;
	}

	public void setChildSumInsured(BigDecimal childSumInsured) {
		this.childSumInsured = childSumInsured;
	}

	public BigDecimal getAdultLoading() {
		return adultLoading;
	}

	public void setAdultLoading(BigDecimal adultLoading) {
		this.adultLoading = adultLoading;
	}

	public BigDecimal getChildLoading() {
		return childLoading;
	}

	public void setChildLoading(BigDecimal childLoading) {
		this.childLoading = childLoading;
	}

	public BigDecimal getAdultPremium() {
		return adultPremium;
	}

	public void setAdultPremium(BigDecimal adultPremium) {
		this.adultPremium = adultPremium;
	}

	public BigDecimal getChildPremium() {
		return childPremium;
	}

	public void setChildPremium(BigDecimal childPremium) {
		this.childPremium = childPremium;
	}

	public Object getEnable() {
		return enable;
	}

	public void setEnable(Object enable) {
		this.enable = enable;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Object getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Object createdBy) {
		this.createdBy = createdBy;
	}

	public Date getModifiedDt() {
		return modifiedDt;
	}

	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	public Object getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Object modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Short getVersion() {
		return version;
	}

	public void setVersion(Short version) {
		this.version = version;
	}

}
