package com.spring.VO;

import java.math.BigDecimal;
import java.util.Date;

public class InfositeNewsLetter {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DSP_INFOSITE_NEWSLETTER.ID
     *
     * @mbg.generated Mon Jan 28 15:50:40 SGT 2019
     */
    private BigDecimal id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DSP_INFOSITE_NEWSLETTER.EMAIL
     *
     * @mbg.generated Mon Jan 28 15:50:40 SGT 2019
     */
    private String email;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DSP_INFOSITE_NEWSLETTER.CREATE_DATE
     *
     * @mbg.generated Mon Jan 28 15:50:40 SGT 2019
     */
    private Date createDate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column DSP_INFOSITE_NEWSLETTER.UPDATE_DATE
     *
     * @mbg.generated Mon Jan 28 15:50:40 SGT 2019
     */
    private Date updateDate;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DSP_INFOSITE_NEWSLETTER.ID
     *
     * @return the value of DSP_INFOSITE_NEWSLETTER.ID
     *
     * @mbg.generated Mon Jan 28 15:50:40 SGT 2019
     */
    public BigDecimal getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DSP_INFOSITE_NEWSLETTER.ID
     *
     * @param id the value for DSP_INFOSITE_NEWSLETTER.ID
     *
     * @mbg.generated Mon Jan 28 15:50:40 SGT 2019
     */
    public void setId(BigDecimal id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DSP_INFOSITE_NEWSLETTER.EMAIL
     *
     * @return the value of DSP_INFOSITE_NEWSLETTER.EMAIL
     *
     * @mbg.generated Mon Jan 28 15:50:40 SGT 2019
     */
    public String getEmail() {
        return email;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DSP_INFOSITE_NEWSLETTER.EMAIL
     *
     * @param email the value for DSP_INFOSITE_NEWSLETTER.EMAIL
     *
     * @mbg.generated Mon Jan 28 15:50:40 SGT 2019
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DSP_INFOSITE_NEWSLETTER.CREATE_DATE
     *
     * @return the value of DSP_INFOSITE_NEWSLETTER.CREATE_DATE
     *
     * @mbg.generated Mon Jan 28 15:50:40 SGT 2019
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DSP_INFOSITE_NEWSLETTER.CREATE_DATE
     *
     * @param createDate the value for DSP_INFOSITE_NEWSLETTER.CREATE_DATE
     *
     * @mbg.generated Mon Jan 28 15:50:40 SGT 2019
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column DSP_INFOSITE_NEWSLETTER.UPDATE_DATE
     *
     * @return the value of DSP_INFOSITE_NEWSLETTER.UPDATE_DATE
     *
     * @mbg.generated Mon Jan 28 15:50:40 SGT 2019
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column DSP_INFOSITE_NEWSLETTER.UPDATE_DATE
     *
     * @param updateDate the value for DSP_INFOSITE_NEWSLETTER.UPDATE_DATE
     *
     * @mbg.generated Mon Jan 28 15:50:40 SGT 2019
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}