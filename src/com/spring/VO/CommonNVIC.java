package com.spring.VO;

import java.math.BigDecimal;

public class CommonNVIC {
	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.ID
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private Integer id;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.NVIC_CODE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String nvicCode;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.NVIC_GROUP
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String nvicGroup;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.MM_CODE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String mmCode;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.MAKE_CODE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String makeCode;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.MODEL_CODE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String modelCode;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.MAKE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String make;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.MODEL
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String model;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.VARIANT
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String variant;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.SERIES
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String series;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.YEAR
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private Short year;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.DESCRIPTION
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String description;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.ENGINE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String engine;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.STYLE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String style;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.TRANSMISSION
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String transmission;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.DRIVEN_WHEEL
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String drivenWheel;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.SEAT
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private Short seat;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.CC
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private Short cc;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.FUEL_TYPE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String fuelType;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.CLASS_CODE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String classCode;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.WD4_FLG
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String wd4Flg;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.HP_FLG
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String hpFlg;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.MARK_UP_PER
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private BigDecimal markUpPer;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.MARK_DOWN_PER
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private BigDecimal markDownPer;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.TOTAL_SUM
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private BigDecimal totalSum;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.MARKET_VALUE1
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private BigDecimal marketValue1;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.MARKET_VALUE2
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private BigDecimal marketValue2;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.MARKET_VALUE3
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private BigDecimal marketValue3;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.WINDSCREEN_VALUE1
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private BigDecimal windscreenValue1;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.WINDSCREEN_VALUE2
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private BigDecimal windscreenValue2;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.WINDSCREEN_VALUE3
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private BigDecimal windscreenValue3;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.EFFECTIVE_DATE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String effectiveDate;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.PURVAL01
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String purval01;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.PURVAL02
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String purval02;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.PURVAL03
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String purval03;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_NVIC.MODELTYPE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	private String modeltype;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.ID
	 *
	 * @return the value of DSP_MI_TBL_NVIC.ID
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.ID
	 *
	 * @param id
	 *            the value for DSP_MI_TBL_NVIC.ID
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.NVIC_CODE
	 *
	 * @return the value of DSP_MI_TBL_NVIC.NVIC_CODE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getNvicCode() {
		return nvicCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.NVIC_CODE
	 *
	 * @param nvicCode
	 *            the value for DSP_MI_TBL_NVIC.NVIC_CODE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setNvicCode(String nvicCode) {
		this.nvicCode = nvicCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.NVIC_GROUP
	 *
	 * @return the value of DSP_MI_TBL_NVIC.NVIC_GROUP
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getNvicGroup() {
		return nvicGroup;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.NVIC_GROUP
	 *
	 * @param nvicGroup
	 *            the value for DSP_MI_TBL_NVIC.NVIC_GROUP
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setNvicGroup(String nvicGroup) {
		this.nvicGroup = nvicGroup;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.MM_CODE
	 *
	 * @return the value of DSP_MI_TBL_NVIC.MM_CODE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getMmCode() {
		return mmCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.MM_CODE
	 *
	 * @param mmCode
	 *            the value for DSP_MI_TBL_NVIC.MM_CODE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setMmCode(String mmCode) {
		this.mmCode = mmCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.MAKE_CODE
	 *
	 * @return the value of DSP_MI_TBL_NVIC.MAKE_CODE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getMakeCode() {
		return makeCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.MAKE_CODE
	 *
	 * @param makeCode
	 *            the value for DSP_MI_TBL_NVIC.MAKE_CODE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setMakeCode(String makeCode) {
		this.makeCode = makeCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.MODEL_CODE
	 *
	 * @return the value of DSP_MI_TBL_NVIC.MODEL_CODE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getModelCode() {
		return modelCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.MODEL_CODE
	 *
	 * @param modelCode
	 *            the value for DSP_MI_TBL_NVIC.MODEL_CODE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setModelCode(String modelCode) {
		this.modelCode = modelCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.MAKE
	 *
	 * @return the value of DSP_MI_TBL_NVIC.MAKE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getMake() {
		return make;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.MAKE
	 *
	 * @param make
	 *            the value for DSP_MI_TBL_NVIC.MAKE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setMake(String make) {
		this.make = make;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.MODEL
	 *
	 * @return the value of DSP_MI_TBL_NVIC.MODEL
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getModel() {
		return model;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.MODEL
	 *
	 * @param model
	 *            the value for DSP_MI_TBL_NVIC.MODEL
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.VARIANT
	 *
	 * @return the value of DSP_MI_TBL_NVIC.VARIANT
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getVariant() {
		return variant;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.VARIANT
	 *
	 * @param variant
	 *            the value for DSP_MI_TBL_NVIC.VARIANT
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setVariant(String variant) {
		this.variant = variant;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.SERIES
	 *
	 * @return the value of DSP_MI_TBL_NVIC.SERIES
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getSeries() {
		return series;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.SERIES
	 *
	 * @param series
	 *            the value for DSP_MI_TBL_NVIC.SERIES
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setSeries(String series) {
		this.series = series;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.YEAR
	 *
	 * @return the value of DSP_MI_TBL_NVIC.YEAR
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public Short getYear() {
		return year;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.YEAR
	 *
	 * @param year
	 *            the value for DSP_MI_TBL_NVIC.YEAR
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setYear(Short year) {
		this.year = year;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.DESCRIPTION
	 *
	 * @return the value of DSP_MI_TBL_NVIC.DESCRIPTION
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.DESCRIPTION
	 *
	 * @param description
	 *            the value for DSP_MI_TBL_NVIC.DESCRIPTION
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.ENGINE
	 *
	 * @return the value of DSP_MI_TBL_NVIC.ENGINE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getEngine() {
		return engine;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.ENGINE
	 *
	 * @param engine
	 *            the value for DSP_MI_TBL_NVIC.ENGINE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setEngine(String engine) {
		this.engine = engine;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.STYLE
	 *
	 * @return the value of DSP_MI_TBL_NVIC.STYLE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getStyle() {
		return style;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.STYLE
	 *
	 * @param style
	 *            the value for DSP_MI_TBL_NVIC.STYLE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setStyle(String style) {
		this.style = style;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.TRANSMISSION
	 *
	 * @return the value of DSP_MI_TBL_NVIC.TRANSMISSION
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getTransmission() {
		return transmission;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.TRANSMISSION
	 *
	 * @param transmission
	 *            the value for DSP_MI_TBL_NVIC.TRANSMISSION
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setTransmission(String transmission) {
		this.transmission = transmission;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.DRIVEN_WHEEL
	 *
	 * @return the value of DSP_MI_TBL_NVIC.DRIVEN_WHEEL
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getDrivenWheel() {
		return drivenWheel;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.DRIVEN_WHEEL
	 *
	 * @param drivenWheel
	 *            the value for DSP_MI_TBL_NVIC.DRIVEN_WHEEL
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setDrivenWheel(String drivenWheel) {
		this.drivenWheel = drivenWheel;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.SEAT
	 *
	 * @return the value of DSP_MI_TBL_NVIC.SEAT
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public Short getSeat() {
		return seat;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.SEAT
	 *
	 * @param seat
	 *            the value for DSP_MI_TBL_NVIC.SEAT
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setSeat(Short seat) {
		this.seat = seat;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.CC
	 *
	 * @return the value of DSP_MI_TBL_NVIC.CC
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public Short getCc() {
		return cc;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.CC
	 *
	 * @param cc
	 *            the value for DSP_MI_TBL_NVIC.CC
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setCc(Short cc) {
		this.cc = cc;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.FUEL_TYPE
	 *
	 * @return the value of DSP_MI_TBL_NVIC.FUEL_TYPE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getFuelType() {
		return fuelType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.FUEL_TYPE
	 *
	 * @param fuelType
	 *            the value for DSP_MI_TBL_NVIC.FUEL_TYPE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.CLASS_CODE
	 *
	 * @return the value of DSP_MI_TBL_NVIC.CLASS_CODE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getClassCode() {
		return classCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.CLASS_CODE
	 *
	 * @param classCode
	 *            the value for DSP_MI_TBL_NVIC.CLASS_CODE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.WD4_FLG
	 *
	 * @return the value of DSP_MI_TBL_NVIC.WD4_FLG
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getWd4Flg() {
		return wd4Flg;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.WD4_FLG
	 *
	 * @param wd4Flg
	 *            the value for DSP_MI_TBL_NVIC.WD4_FLG
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setWd4Flg(String wd4Flg) {
		this.wd4Flg = wd4Flg;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.HP_FLG
	 *
	 * @return the value of DSP_MI_TBL_NVIC.HP_FLG
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getHpFlg() {
		return hpFlg;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.HP_FLG
	 *
	 * @param hpFlg
	 *            the value for DSP_MI_TBL_NVIC.HP_FLG
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setHpFlg(String hpFlg) {
		this.hpFlg = hpFlg;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.MARK_UP_PER
	 *
	 * @return the value of DSP_MI_TBL_NVIC.MARK_UP_PER
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public BigDecimal getMarkUpPer() {
		return markUpPer;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.MARK_UP_PER
	 *
	 * @param markUpPer
	 *            the value for DSP_MI_TBL_NVIC.MARK_UP_PER
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setMarkUpPer(BigDecimal markUpPer) {
		this.markUpPer = markUpPer;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.MARK_DOWN_PER
	 *
	 * @return the value of DSP_MI_TBL_NVIC.MARK_DOWN_PER
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public BigDecimal getMarkDownPer() {
		return markDownPer;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.MARK_DOWN_PER
	 *
	 * @param markDownPer
	 *            the value for DSP_MI_TBL_NVIC.MARK_DOWN_PER
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setMarkDownPer(BigDecimal markDownPer) {
		this.markDownPer = markDownPer;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.TOTAL_SUM
	 *
	 * @return the value of DSP_MI_TBL_NVIC.TOTAL_SUM
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public BigDecimal getTotalSum() {
		return totalSum;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.TOTAL_SUM
	 *
	 * @param totalSum
	 *            the value for DSP_MI_TBL_NVIC.TOTAL_SUM
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setTotalSum(BigDecimal totalSum) {
		this.totalSum = totalSum;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.MARKET_VALUE1
	 *
	 * @return the value of DSP_MI_TBL_NVIC.MARKET_VALUE1
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public BigDecimal getMarketValue1() {
		return marketValue1;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.MARKET_VALUE1
	 *
	 * @param marketValue1
	 *            the value for DSP_MI_TBL_NVIC.MARKET_VALUE1
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setMarketValue1(BigDecimal marketValue1) {
		this.marketValue1 = marketValue1;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.MARKET_VALUE2
	 *
	 * @return the value of DSP_MI_TBL_NVIC.MARKET_VALUE2
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public BigDecimal getMarketValue2() {
		return marketValue2;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.MARKET_VALUE2
	 *
	 * @param marketValue2
	 *            the value for DSP_MI_TBL_NVIC.MARKET_VALUE2
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setMarketValue2(BigDecimal marketValue2) {
		this.marketValue2 = marketValue2;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.MARKET_VALUE3
	 *
	 * @return the value of DSP_MI_TBL_NVIC.MARKET_VALUE3
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public BigDecimal getMarketValue3() {
		return marketValue3;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.MARKET_VALUE3
	 *
	 * @param marketValue3
	 *            the value for DSP_MI_TBL_NVIC.MARKET_VALUE3
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setMarketValue3(BigDecimal marketValue3) {
		this.marketValue3 = marketValue3;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.WINDSCREEN_VALUE1
	 *
	 * @return the value of DSP_MI_TBL_NVIC.WINDSCREEN_VALUE1
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public BigDecimal getWindscreenValue1() {
		return windscreenValue1;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.WINDSCREEN_VALUE1
	 *
	 * @param windscreenValue1
	 *            the value for DSP_MI_TBL_NVIC.WINDSCREEN_VALUE1
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setWindscreenValue1(BigDecimal windscreenValue1) {
		this.windscreenValue1 = windscreenValue1;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.WINDSCREEN_VALUE2
	 *
	 * @return the value of DSP_MI_TBL_NVIC.WINDSCREEN_VALUE2
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public BigDecimal getWindscreenValue2() {
		return windscreenValue2;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.WINDSCREEN_VALUE2
	 *
	 * @param windscreenValue2
	 *            the value for DSP_MI_TBL_NVIC.WINDSCREEN_VALUE2
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setWindscreenValue2(BigDecimal windscreenValue2) {
		this.windscreenValue2 = windscreenValue2;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.WINDSCREEN_VALUE3
	 *
	 * @return the value of DSP_MI_TBL_NVIC.WINDSCREEN_VALUE3
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public BigDecimal getWindscreenValue3() {
		return windscreenValue3;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.WINDSCREEN_VALUE3
	 *
	 * @param windscreenValue3
	 *            the value for DSP_MI_TBL_NVIC.WINDSCREEN_VALUE3
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setWindscreenValue3(BigDecimal windscreenValue3) {
		this.windscreenValue3 = windscreenValue3;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.EFFECTIVE_DATE
	 *
	 * @return the value of DSP_MI_TBL_NVIC.EFFECTIVE_DATE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.EFFECTIVE_DATE
	 *
	 * @param effectiveDate
	 *            the value for DSP_MI_TBL_NVIC.EFFECTIVE_DATE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.PURVAL01
	 *
	 * @return the value of DSP_MI_TBL_NVIC.PURVAL01
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getPurval01() {
		return purval01;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.PURVAL01
	 *
	 * @param purval01
	 *            the value for DSP_MI_TBL_NVIC.PURVAL01
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setPurval01(String purval01) {
		this.purval01 = purval01;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.PURVAL02
	 *
	 * @return the value of DSP_MI_TBL_NVIC.PURVAL02
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getPurval02() {
		return purval02;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.PURVAL02
	 *
	 * @param purval02
	 *            the value for DSP_MI_TBL_NVIC.PURVAL02
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setPurval02(String purval02) {
		this.purval02 = purval02;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.PURVAL03
	 *
	 * @return the value of DSP_MI_TBL_NVIC.PURVAL03
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getPurval03() {
		return purval03;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.PURVAL03
	 *
	 * @param purval03
	 *            the value for DSP_MI_TBL_NVIC.PURVAL03
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setPurval03(String purval03) {
		this.purval03 = purval03;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_NVIC.MODELTYPE
	 *
	 * @return the value of DSP_MI_TBL_NVIC.MODELTYPE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public String getModeltype() {
		return modeltype;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_NVIC.MODELTYPE
	 *
	 * @param modeltype
	 *            the value for DSP_MI_TBL_NVIC.MODELTYPE
	 *
	 * @mbg.generated Wed Apr 05 14:46:47 SGT 2017
	 */
	public void setModeltype(String modeltype) {
		this.modeltype = modeltype;
	}
}