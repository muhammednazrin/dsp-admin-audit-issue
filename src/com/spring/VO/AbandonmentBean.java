package com.spring.VO;

public class AbandonmentBean {

	private String Fromdate;
	private String Todate;

	private String abandonementdate;
	private String total_count;
	private String total_count_success;
	private String total_month_rm;
	private String percentsuccess;
	private String product_indicator;
	private String start_week;
	private String end_week;
	private String jsonstring;
	private String transactiondate;
	private String totalamount;
	private String policycount;

	public String getPercentsuccess() {
		return percentsuccess;
	}

	public void setPercentsuccess(String percentsuccess) {
		this.percentsuccess = percentsuccess;
	}

	public String getPolicycount() {
		return policycount;
	}

	public void setPolicycount(String policycount) {
		this.policycount = policycount;
	}

	public String getTotalamount() {
		return totalamount;
	}

	public void setTotalamount(String totalamount) {
		this.totalamount = totalamount;
	}

	public String getTransactiondate() {
		return transactiondate;
	}

	public void setTransactiondate(String transactiondate) {
		this.transactiondate = transactiondate;
	}

	public String getProduct_indicator() {
		return product_indicator;
	}

	public void setProduct_indicator(String product_indicator) {
		this.product_indicator = product_indicator;
	}

	public String getAbandonementdate() {
		return abandonementdate;
	}

	public void setAbandonementdate(String abandonementdate) {
		this.abandonementdate = abandonementdate;
	}

	public String getTotal_count() {
		return total_count;
	}

	public void setTotal_count(String total_count) {
		this.total_count = total_count;
	}

	public String getTotal_count_success() {
		return total_count_success;
	}

	public void setTotal_count_success(String total_count_success) {
		this.total_count_success = total_count_success;
	}

	public String getTotal_month_rm() {
		return total_month_rm;
	}

	public void setTotal_month_rm(String total_month_rm) {
		this.total_month_rm = total_month_rm;
	}

	public String getFromdate() {
		return Fromdate;
	}

	public void setFromdate(String fromdate) {
		Fromdate = fromdate;
	}

	public String getTodate() {
		return Todate;
	}

	public void setTodate(String todate) {
		Todate = todate;
	}

	public String getStart_week() {
		return start_week;
	}

	public void setStart_week(String start_week) {
		this.start_week = start_week;
	}

	public String getEnd_week() {
		return end_week;
	}

	public void setEnd_week(String end_week) {
		this.end_week = end_week;
	}

	public String getJsonstring() {
		return jsonstring;
	}

	public void setJsonstring(String jsonstring) {
		this.jsonstring = jsonstring;
	}

}
