package com.spring.VO;

public class HOHHInsProductApprovalRate {

	private String directDiscount;
	private String gst;
	private String minAmountBuild;
	private String maxAmountBuild;
	private String riot;
	private String theft;
	private String stampDuty;
	private String minAmountContent;

	public String getDirectDiscount() {
		return directDiscount;
	}

	public void setDirectDiscount(String directDiscount) {
		this.directDiscount = directDiscount;
	}

	public String getGst() {
		return gst;
	}

	public void setGst(String gst) {
		this.gst = gst;
	}

	public String getMinAmountBuild() {
		return minAmountBuild;
	}

	public void setMinAmountBuild(String minAmountBuild) {
		this.minAmountBuild = minAmountBuild;
	}

	public String getMaxAmountBuild() {
		return maxAmountBuild;
	}

	public void setMaxAmountBuild(String maxAmountBuild) {
		this.maxAmountBuild = maxAmountBuild;
	}

	public String getRiot() {
		return riot;
	}

	public void setRiot(String riot) {
		this.riot = riot;
	}

	public String getTheft() {
		return theft;
	}

	public void setTheft(String theft) {
		this.theft = theft;
	}

	public String getStampDuty() {
		return stampDuty;
	}

	public void setStampDuty(String stampDuty) {
		this.stampDuty = stampDuty;
	}

	public String getMinAmountContent() {
		return minAmountContent;
	}

	public void setMinAmountContent(String minAmountContent) {
		this.minAmountContent = minAmountContent;
	}

	public String getMaxAmountContent() {
		return maxAmountContent;
	}

	public void setMaxAmountContent(String maxAmountContent) {
		this.maxAmountContent = maxAmountContent;
	}

	private String maxAmountContent;

}
