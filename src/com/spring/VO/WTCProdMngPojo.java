package com.spring.VO;

import java.io.Serializable;

public class WTCProdMngPojo implements Serializable {

	private String annualTarget;
	private String validity;

	private String wtcProdType = "0";

	public String getWtcProdType() {
		return wtcProdType;
	}

	public void setWtcProdType(String wtcProdType) {
		this.wtcProdType = wtcProdType;
	}

	private String wtcRecIdDomestic;

	public String getWtcRecIdDomestic() {
		return wtcRecIdDomestic;
	}

	public void setWtcRecIdDomestic(String wtcRecIdDomestic) {
		this.wtcRecIdDomestic = wtcRecIdDomestic;
	}

	public String getWtcdaysrangekeycodeDomestic() {
		return wtcdaysrangekeycodeDomestic;
	}

	public void setWtcdaysrangekeycodeDomestic(String wtcdaysrangekeycodeDomestic) {
		this.wtcdaysrangekeycodeDomestic = wtcdaysrangekeycodeDomestic;
	}

	public String getWtcdestinationcodeareaDomestic() {
		return wtcdestinationcodeareaDomestic;
	}

	public void setWtcdestinationcodeareaDomestic(String wtcdestinationcodeareaDomestic) {
		this.wtcdestinationcodeareaDomestic = wtcdestinationcodeareaDomestic;
	}

	public String getWtcpackagecodeDomestic() {
		return wtcpackagecodeDomestic;
	}

	public void setWtcpackagecodeDomestic(String wtcpackagecodeDomestic) {
		this.wtcpackagecodeDomestic = wtcpackagecodeDomestic;
	}

	public String getWtcpremiumvalDomestic() {
		return wtcpremiumvalDomestic;
	}

	public void setWtcpremiumvalDomestic(String wtcpremiumvalDomestic) {
		this.wtcpremiumvalDomestic = wtcpremiumvalDomestic;
	}

	public String getWtcplancodeDomestic() {
		return wtcplancodeDomestic;
	}

	public void setWtcplancodeDomestic(String wtcplancodeDomestic) {
		this.wtcplancodeDomestic = wtcplancodeDomestic;
	}

	public String getWtctravelwithtypeidDomestic() {
		return wtctravelwithtypeidDomestic;
	}

	public void setWtctravelwithtypeidDomestic(String wtctravelwithtypeidDomestic) {
		this.wtctravelwithtypeidDomestic = wtctravelwithtypeidDomestic;
	}

	public String getWtcRecIdInternational() {
		return wtcRecIdInternational;
	}

	public void setWtcRecIdInternational(String wtcRecIdInternational) {
		this.wtcRecIdInternational = wtcRecIdInternational;
	}

	public String getWtcdaysrangekeycodeInternational() {
		return wtcdaysrangekeycodeInternational;
	}

	public void setWtcdaysrangekeycodeInternational(String wtcdaysrangekeycodeInternational) {
		this.wtcdaysrangekeycodeInternational = wtcdaysrangekeycodeInternational;
	}

	public String getWtcdestinationcodeareaInternational() {
		return wtcdestinationcodeareaInternational;
	}

	public void setWtcdestinationcodeareaInternational(String wtcdestinationcodeareaInternational) {
		this.wtcdestinationcodeareaInternational = wtcdestinationcodeareaInternational;
	}

	public String getWtcpackagecodeInternational() {
		return wtcpackagecodeInternational;
	}

	public void setWtcpackagecodeInternational(String wtcpackagecodeInternational) {
		this.wtcpackagecodeInternational = wtcpackagecodeInternational;
	}

	public String getWtcpremiumvalInternational() {
		return wtcpremiumvalInternational;
	}

	public void setWtcpremiumvalInternational(String wtcpremiumvalInternational) {
		this.wtcpremiumvalInternational = wtcpremiumvalInternational;
	}

	public String getWtcplancodeInternational() {
		return wtcplancodeInternational;
	}

	public void setWtcplancodeInternational(String wtcplancodeInternational) {
		this.wtcplancodeInternational = wtcplancodeInternational;
	}

	public String getWtctravelwithtypeidInternational() {
		return wtctravelwithtypeidInternational;
	}

	public void setWtctravelwithtypeidInternational(String wtctravelwithtypeidInternational) {
		this.wtctravelwithtypeidInternational = wtctravelwithtypeidInternational;
	}

	private String wtcdaysrangekeycodeDomestic;
	private String wtcdestinationcodeareaDomestic;
	private String wtcpackagecodeDomestic;
	private String wtcpremiumvalDomestic;
	private String wtcplancodeDomestic;
	private String wtctravelwithtypeidDomestic;

	private String wtcRecIdInternational;
	private String wtcdaysrangekeycodeInternational;
	private String wtcdestinationcodeareaInternational;
	private String wtcpackagecodeInternational;
	private String wtcpremiumvalInternational;
	private String wtcplancodeInternational;
	private String wtctravelwithtypeidInternational;

	private String wtcDiscount;

	public String getWtcDiscount() {
		return wtcDiscount;
	}

	public void setWtcDiscount(String wtcDiscount) {
		this.wtcDiscount = wtcDiscount;
	}

	public String getWtcGst() {
		return wtcGst;
	}

	public void setWtcGst(String wtcGst) {
		this.wtcGst = wtcGst;
	}

	public String getWtcStampDuty() {
		return wtcStampDuty;
	}

	public void setWtcStampDuty(String wtcStampDuty) {
		this.wtcStampDuty = wtcStampDuty;
	}

	private String wtcGst;
	private String wtcStampDuty;

	public String getAnnualTarget() {
		return annualTarget;
	}

	public void setAnnualTarget(String annualTarget) {
		this.annualTarget = annualTarget;
	}

	public String getValidity() {
		return validity;
	}

	public void setValidity(String validity) {
		this.validity = validity;
	}

}
