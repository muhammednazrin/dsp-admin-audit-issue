package com.spring.VO;

public class BPDashBoard {

	private String fromdate;

	private String F_BPA_TotalPoliciesSold;
	private String F_BPA_TotalPoliciesSold_PM;
	private String F_BPA_TotalPoliciesSold_PY;
	private String F_BPA_Sold_Amount;
	private String F_BPA_Sold_Amount_M;
	private String F_BPA_Sold_Amount_Y;

	private String F_BPA_ETB_TotalPoliciesSold;
	private String F_BPA_ETB_TotalPoliciesSold_PM;
	private String F_BPA_ETB_TotalPoliciesSold_PY;
	private String F_BPA_ETB_Sold_Amount;
	private String F_BPA_ETB_Sold_Amount_M;
	private String F_BPA_ETB_Sold_Amount_Y;

	private String F_BPA_EIB_PPA_TPoliciesSold;
	private String F_BPA_EIB_PPA_TPoliciesSold_PM;
	private String F_BPA_EIB_PPA_TPoliciesSold_PY;
	private String F_BPA_EIB_PPA_Sold_Amount;
	private String F_BPA_EIB_PPA_Sold_Amount_M;
	private String F_BPA_EIB_PPA_Sold_Amount_Y;

	private String F_BPA_ETB_PPA_TPoliciesSold;
	private String F_BPA_ETB_PPA_TPoliciesSold_PM;
	private String F_BPA_ETB_PPA_TPoliciesSold_PY;
	private String F_BPA_ETB_PPA_Sold_Amount;
	private String F_BPA_ETB_PPA_Sold_Amount_M;
	private String F_BPA_ETB_PPA_Sold_Amount_Y;

	private String F_BPA_ETB_Lady_TPolSold;
	private String F_BPA_ETB_Lady_TPolSold_PM;
	private String F_BPA_ETB_Lady_TPolSold_PY;
	private String F_BPA_ETB_Lady_Sold_Amount;
	private String F_BPA_ETB_Lady_Sold_Amount_M;
	private String F_BPA_ETB_Lady_Sold_Amount_Y;

	private String F_BPA_ETB_Hero_TPolSold;
	private String F_BPA_ETB_Hero_TPolSold_PM;
	private String F_BPA_ETB_Hero_TPolSold_PY;
	private String F_BPA_ETB_Hero_Sold_Amount;
	private String F_BPA_ETB_Hero_Sold_Amount_M;
	private String F_BPA_ETB_Hero_Sold_Amount_Y;

	public String getFromdate() {
		return fromdate;
	}

	public void setFromdate(String fromdate) {
		this.fromdate = fromdate;
	}

	public String getF_BPA_TotalPoliciesSold() {
		return F_BPA_TotalPoliciesSold;
	}

	public void setF_BPA_TotalPoliciesSold(String f_BPA_TotalPoliciesSold) {
		F_BPA_TotalPoliciesSold = f_BPA_TotalPoliciesSold;
	}

	public String getF_BPA_TotalPoliciesSold_PM() {
		return F_BPA_TotalPoliciesSold_PM;
	}

	public void setF_BPA_TotalPoliciesSold_PM(String f_BPA_TotalPoliciesSold_PM) {
		F_BPA_TotalPoliciesSold_PM = f_BPA_TotalPoliciesSold_PM;
	}

	public String getF_BPA_TotalPoliciesSold_PY() {
		return F_BPA_TotalPoliciesSold_PY;
	}

	public void setF_BPA_TotalPoliciesSold_PY(String f_BPA_TotalPoliciesSold_PY) {
		F_BPA_TotalPoliciesSold_PY = f_BPA_TotalPoliciesSold_PY;
	}

	public String getF_BPA_Sold_Amount() {
		return F_BPA_Sold_Amount;
	}

	public void setF_BPA_Sold_Amount(String f_BPA_Sold_Amount) {
		F_BPA_Sold_Amount = f_BPA_Sold_Amount;
	}

	public String getF_BPA_Sold_Amount_M() {
		return F_BPA_Sold_Amount_M;
	}

	public void setF_BPA_Sold_Amount_M(String f_BPA_Sold_Amount_M) {
		F_BPA_Sold_Amount_M = f_BPA_Sold_Amount_M;
	}

	public String getF_BPA_Sold_Amount_Y() {
		return F_BPA_Sold_Amount_Y;
	}

	public void setF_BPA_Sold_Amount_Y(String f_BPA_Sold_Amount_Y) {
		F_BPA_Sold_Amount_Y = f_BPA_Sold_Amount_Y;
	}

	public String getF_BPA_ETB_TotalPoliciesSold() {
		return F_BPA_ETB_TotalPoliciesSold;
	}

	public void setF_BPA_ETB_TotalPoliciesSold(String f_BPA_ETB_TotalPoliciesSold) {
		F_BPA_ETB_TotalPoliciesSold = f_BPA_ETB_TotalPoliciesSold;
	}

	public String getF_BPA_ETB_TotalPoliciesSold_PM() {
		return F_BPA_ETB_TotalPoliciesSold_PM;
	}

	public void setF_BPA_ETB_TotalPoliciesSold_PM(String f_BPA_ETB_TotalPoliciesSold_PM) {
		F_BPA_ETB_TotalPoliciesSold_PM = f_BPA_ETB_TotalPoliciesSold_PM;
	}

	public String getF_BPA_ETB_TotalPoliciesSold_PY() {
		return F_BPA_ETB_TotalPoliciesSold_PY;
	}

	public void setF_BPA_ETB_TotalPoliciesSold_PY(String f_BPA_ETB_TotalPoliciesSold_PY) {
		F_BPA_ETB_TotalPoliciesSold_PY = f_BPA_ETB_TotalPoliciesSold_PY;
	}

	public String getF_BPA_ETB_Sold_Amount() {
		return F_BPA_ETB_Sold_Amount;
	}

	public void setF_BPA_ETB_Sold_Amount(String f_BPA_ETB_Sold_Amount) {
		F_BPA_ETB_Sold_Amount = f_BPA_ETB_Sold_Amount;
	}

	public String getF_BPA_ETB_Sold_Amount_M() {
		return F_BPA_ETB_Sold_Amount_M;
	}

	public void setF_BPA_ETB_Sold_Amount_M(String f_BPA_ETB_Sold_Amount_M) {
		F_BPA_ETB_Sold_Amount_M = f_BPA_ETB_Sold_Amount_M;
	}

	public String getF_BPA_ETB_Sold_Amount_Y() {
		return F_BPA_ETB_Sold_Amount_Y;
	}

	public void setF_BPA_ETB_Sold_Amount_Y(String f_BPA_ETB_Sold_Amount_Y) {
		F_BPA_ETB_Sold_Amount_Y = f_BPA_ETB_Sold_Amount_Y;
	}

	public String getF_BPA_EIB_PPA_TPoliciesSold() {
		return F_BPA_EIB_PPA_TPoliciesSold;
	}

	public void setF_BPA_EIB_PPA_TPoliciesSold(String f_BPA_EIB_PPA_TPoliciesSold) {
		F_BPA_EIB_PPA_TPoliciesSold = f_BPA_EIB_PPA_TPoliciesSold;
	}

	public String getF_BPA_EIB_PPA_TPoliciesSold_PM() {
		return F_BPA_EIB_PPA_TPoliciesSold_PM;
	}

	public void setF_BPA_EIB_PPA_TPoliciesSold_PM(String f_BPA_EIB_PPA_TPoliciesSold_PM) {
		F_BPA_EIB_PPA_TPoliciesSold_PM = f_BPA_EIB_PPA_TPoliciesSold_PM;
	}

	public String getF_BPA_EIB_PPA_TPoliciesSold_PY() {
		return F_BPA_EIB_PPA_TPoliciesSold_PY;
	}

	public void setF_BPA_EIB_PPA_TPoliciesSold_PY(String f_BPA_EIB_PPA_TPoliciesSold_PY) {
		F_BPA_EIB_PPA_TPoliciesSold_PY = f_BPA_EIB_PPA_TPoliciesSold_PY;
	}

	public String getF_BPA_EIB_PPA_Sold_Amount() {
		return F_BPA_EIB_PPA_Sold_Amount;
	}

	public void setF_BPA_EIB_PPA_Sold_Amount(String f_BPA_EIB_PPA_Sold_Amount) {
		F_BPA_EIB_PPA_Sold_Amount = f_BPA_EIB_PPA_Sold_Amount;
	}

	public String getF_BPA_EIB_PPA_Sold_Amount_M() {
		return F_BPA_EIB_PPA_Sold_Amount_M;
	}

	public void setF_BPA_EIB_PPA_Sold_Amount_M(String f_BPA_EIB_PPA_Sold_Amount_M) {
		F_BPA_EIB_PPA_Sold_Amount_M = f_BPA_EIB_PPA_Sold_Amount_M;
	}

	public String getF_BPA_EIB_PPA_Sold_Amount_Y() {
		return F_BPA_EIB_PPA_Sold_Amount_Y;
	}

	public void setF_BPA_EIB_PPA_Sold_Amount_Y(String f_BPA_EIB_PPA_Sold_Amount_Y) {
		F_BPA_EIB_PPA_Sold_Amount_Y = f_BPA_EIB_PPA_Sold_Amount_Y;
	}

	public String getF_BPA_ETB_PPA_TPoliciesSold() {
		return F_BPA_ETB_PPA_TPoliciesSold;
	}

	public void setF_BPA_ETB_PPA_TPoliciesSold(String f_BPA_ETB_PPA_TPoliciesSold) {
		F_BPA_ETB_PPA_TPoliciesSold = f_BPA_ETB_PPA_TPoliciesSold;
	}

	public String getF_BPA_ETB_PPA_TPoliciesSold_PM() {
		return F_BPA_ETB_PPA_TPoliciesSold_PM;
	}

	public void setF_BPA_ETB_PPA_TPoliciesSold_PM(String f_BPA_ETB_PPA_TPoliciesSold_PM) {
		F_BPA_ETB_PPA_TPoliciesSold_PM = f_BPA_ETB_PPA_TPoliciesSold_PM;
	}

	public String getF_BPA_ETB_PPA_TPoliciesSold_PY() {
		return F_BPA_ETB_PPA_TPoliciesSold_PY;
	}

	public void setF_BPA_ETB_PPA_TPoliciesSold_PY(String f_BPA_ETB_PPA_TPoliciesSold_PY) {
		F_BPA_ETB_PPA_TPoliciesSold_PY = f_BPA_ETB_PPA_TPoliciesSold_PY;
	}

	public String getF_BPA_ETB_PPA_Sold_Amount() {
		return F_BPA_ETB_PPA_Sold_Amount;
	}

	public void setF_BPA_ETB_PPA_Sold_Amount(String f_BPA_ETB_PPA_Sold_Amount) {
		F_BPA_ETB_PPA_Sold_Amount = f_BPA_ETB_PPA_Sold_Amount;
	}

	public String getF_BPA_ETB_PPA_Sold_Amount_M() {
		return F_BPA_ETB_PPA_Sold_Amount_M;
	}

	public void setF_BPA_ETB_PPA_Sold_Amount_M(String f_BPA_ETB_PPA_Sold_Amount_M) {
		F_BPA_ETB_PPA_Sold_Amount_M = f_BPA_ETB_PPA_Sold_Amount_M;
	}

	public String getF_BPA_ETB_PPA_Sold_Amount_Y() {
		return F_BPA_ETB_PPA_Sold_Amount_Y;
	}

	public void setF_BPA_ETB_PPA_Sold_Amount_Y(String f_BPA_ETB_PPA_Sold_Amount_Y) {
		F_BPA_ETB_PPA_Sold_Amount_Y = f_BPA_ETB_PPA_Sold_Amount_Y;
	}

	public String getF_BPA_ETB_Lady_TPolSold() {
		return F_BPA_ETB_Lady_TPolSold;
	}

	public void setF_BPA_ETB_Lady_TPolSold(String f_BPA_ETB_Lady_TPolSold) {
		F_BPA_ETB_Lady_TPolSold = f_BPA_ETB_Lady_TPolSold;
	}

	public String getF_BPA_ETB_Lady_TPolSold_PM() {
		return F_BPA_ETB_Lady_TPolSold_PM;
	}

	public void setF_BPA_ETB_Lady_TPolSold_PM(String f_BPA_ETB_Lady_TPolSold_PM) {
		F_BPA_ETB_Lady_TPolSold_PM = f_BPA_ETB_Lady_TPolSold_PM;
	}

	public String getF_BPA_ETB_Lady_TPolSold_PY() {
		return F_BPA_ETB_Lady_TPolSold_PY;
	}

	public void setF_BPA_ETB_Lady_TPolSold_PY(String f_BPA_ETB_Lady_TPolSold_PY) {
		F_BPA_ETB_Lady_TPolSold_PY = f_BPA_ETB_Lady_TPolSold_PY;
	}

	public String getF_BPA_ETB_Lady_Sold_Amount() {
		return F_BPA_ETB_Lady_Sold_Amount;
	}

	public void setF_BPA_ETB_Lady_Sold_Amount(String f_BPA_ETB_Lady_Sold_Amount) {
		F_BPA_ETB_Lady_Sold_Amount = f_BPA_ETB_Lady_Sold_Amount;
	}

	public String getF_BPA_ETB_Lady_Sold_Amount_M() {
		return F_BPA_ETB_Lady_Sold_Amount_M;
	}

	public void setF_BPA_ETB_Lady_Sold_Amount_M(String f_BPA_ETB_Lady_Sold_Amount_M) {
		F_BPA_ETB_Lady_Sold_Amount_M = f_BPA_ETB_Lady_Sold_Amount_M;
	}

	public String getF_BPA_ETB_Lady_Sold_Amount_Y() {
		return F_BPA_ETB_Lady_Sold_Amount_Y;
	}

	public void setF_BPA_ETB_Lady_Sold_Amount_Y(String f_BPA_ETB_Lady_Sold_Amount_Y) {
		F_BPA_ETB_Lady_Sold_Amount_Y = f_BPA_ETB_Lady_Sold_Amount_Y;
	}

	public String getF_BPA_ETB_Hero_TPolSold() {
		return F_BPA_ETB_Hero_TPolSold;
	}

	public void setF_BPA_ETB_Hero_TPolSold(String f_BPA_ETB_Hero_TPolSold) {
		F_BPA_ETB_Hero_TPolSold = f_BPA_ETB_Hero_TPolSold;
	}

	public String getF_BPA_ETB_Hero_TPolSold_PM() {
		return F_BPA_ETB_Hero_TPolSold_PM;
	}

	public void setF_BPA_ETB_Hero_TPolSold_PM(String f_BPA_ETB_Hero_TPolSold_PM) {
		F_BPA_ETB_Hero_TPolSold_PM = f_BPA_ETB_Hero_TPolSold_PM;
	}

	public String getF_BPA_ETB_Hero_TPolSold_PY() {
		return F_BPA_ETB_Hero_TPolSold_PY;
	}

	public void setF_BPA_ETB_Hero_TPolSold_PY(String f_BPA_ETB_Hero_TPolSold_PY) {
		F_BPA_ETB_Hero_TPolSold_PY = f_BPA_ETB_Hero_TPolSold_PY;
	}

	public String getF_BPA_ETB_Hero_Sold_Amount() {
		return F_BPA_ETB_Hero_Sold_Amount;
	}

	public void setF_BPA_ETB_Hero_Sold_Amount(String f_BPA_ETB_Hero_Sold_Amount) {
		F_BPA_ETB_Hero_Sold_Amount = f_BPA_ETB_Hero_Sold_Amount;
	}

	public String getF_BPA_ETB_Hero_Sold_Amount_M() {
		return F_BPA_ETB_Hero_Sold_Amount_M;
	}

	public void setF_BPA_ETB_Hero_Sold_Amount_M(String f_BPA_ETB_Hero_Sold_Amount_M) {
		F_BPA_ETB_Hero_Sold_Amount_M = f_BPA_ETB_Hero_Sold_Amount_M;
	}

	public String getF_BPA_ETB_Hero_Sold_Amount_Y() {
		return F_BPA_ETB_Hero_Sold_Amount_Y;
	}

	public void setF_BPA_ETB_Hero_Sold_Amount_Y(String f_BPA_ETB_Hero_Sold_Amount_Y) {
		F_BPA_ETB_Hero_Sold_Amount_Y = f_BPA_ETB_Hero_Sold_Amount_Y;
	}

}
