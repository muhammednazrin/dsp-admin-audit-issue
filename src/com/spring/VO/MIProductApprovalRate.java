package com.spring.VO;

import java.io.Serializable;
import java.util.Date;

public class MIProductApprovalRate implements Serializable {

	private String maxDrivers;
	private Integer sumCoveredMin;
	private Integer sumCoveredMax;
	private String directDiscount;
	private String gst;
	private String capsDiscount;
	private String fiveSeater;
	private String sevenSeater;
	private String stampDuty;
	private String thirdDriver;
	private Integer minAge;
	private Integer maxAge;
	private String capsFee;

	private String motorTakafulSst;
	private Date motorTakafulSstEffectiveDate;
	private String dppa_stampduty;
	private String dppa_sst;
	private Date dppa_sst_effective_date;
	private String oto_stampduty;
	private String oto_sst;
	private Date oto_sst_effective_date;

	public String getCapsFee() {
		return capsFee;
	}

	public void setCapsFee(String capsFee) {
		this.capsFee = capsFee;
	}

	public String getDirectDiscount() {
		return directDiscount;
	}

	public void setDirectDiscount(String directDiscount) {
		this.directDiscount = directDiscount;
	}

	public String getGst() {
		return gst;
	}

	public void setGst(String gst) {
		this.gst = gst;
	}

	public String getCapsDiscount() {
		return capsDiscount;
	}

	public void setCapsDiscount(String capsDiscount) {
		this.capsDiscount = capsDiscount;
	}

	public String getFiveSeater() {
		return fiveSeater;
	}

	public void setFiveSeater(String fiveSeater) {
		this.fiveSeater = fiveSeater;
	}

	public String getSevenSeater() {
		return sevenSeater;
	}

	public void setSevenSeater(String sevenSeater) {
		this.sevenSeater = sevenSeater;
	}

	public String getStampDuty() {
		return stampDuty;
	}

	public void setStampDuty(String stampDuty) {
		this.stampDuty = stampDuty;
	}

	public String getThirdDriver() {
		return thirdDriver;
	}

	public void setThirdDriver(String thirdDriver) {
		this.thirdDriver = thirdDriver;
	}

	public Integer getSumCoveredMax() {
		return sumCoveredMax;
	}

	public void setSumCoveredMax(Integer sumCoveredMax) {
		this.sumCoveredMax = sumCoveredMax;
	}

	public Integer getMinAge() {
		return minAge;
	}

	public void setMinAge(Integer minAge) {
		this.minAge = minAge;
	}

	public Integer getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(Integer maxAge) {
		this.maxAge = maxAge;
	}

	public Integer getSumCoveredMin() {
		return sumCoveredMin;
	}

	public void setSumCoveredMin(Integer sumCoveredMin) {
		this.sumCoveredMin = sumCoveredMin;
	}

	public String getMaxDrivers() {
		return maxDrivers;
	}

	public void setMaxDrivers(String maxDrivers) {
		this.maxDrivers = maxDrivers;
	}

	public void setMaxDrivers(Object object) {
		// TODO Auto-generated method stub

	}

	public String getMotorTakafulSst() {
		return motorTakafulSst;
	}

	public void setMotorTakafulSst(String motorTakafulSst) {
		this.motorTakafulSst = motorTakafulSst;
	}

	public Date getMotorTakafulSstEffectiveDate() {
		return motorTakafulSstEffectiveDate;
	}

	public void setMotorTakafulSstEffectiveDate(Date motorTakafulSstEffectiveDate) {
		this.motorTakafulSstEffectiveDate = motorTakafulSstEffectiveDate;
	}

	public String getDppa_stampduty() {
		return dppa_stampduty;
	}

	public void setDppa_stampduty(String dppa_stampduty) {
		this.dppa_stampduty = dppa_stampduty;
	}

	public String getDppa_sst() {
		return dppa_sst;
	}

	public void setDppa_sst(String dppa_sst) {
		this.dppa_sst = dppa_sst;
	}

	public Date getDppa_sst_effective_date() {
		return dppa_sst_effective_date;
	}

	public void setDppa_sst_effective_date(Date dppa_sst_effective_date) {
		this.dppa_sst_effective_date = dppa_sst_effective_date;
	}

	public String getOto_stampduty() {
		return oto_stampduty;
	}

	public void setOto_stampduty(String oto_stampduty) {
		this.oto_stampduty = oto_stampduty;
	}

	public String getOto_sst() {
		return oto_sst;
	}

	public void setOto_sst(String oto_sst) {
		this.oto_sst = oto_sst;
	}

	public Date getOto_sst_effective_date() {
		return oto_sst_effective_date;
	}

	public void setOto_sst_effective_date(Date oto_sst_effective_date) {
		this.oto_sst_effective_date = oto_sst_effective_date;
	}

}
