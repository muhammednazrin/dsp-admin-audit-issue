package com.spring.VO;

public class TotalSalesBean {

	private String week_no;
	private String date;
	private String month;
	private String amount;
	private String policycount;

	public String getWeek_no() {
		return week_no;
	}

	public void setWeek_no(String week_no) {
		this.week_no = week_no;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPolicycount() {
		return policycount;
	}

	public void setPolicycount(String policycount) {
		this.policycount = policycount;
	}

}
