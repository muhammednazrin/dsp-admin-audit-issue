package com.spring.VO;

public class BuddyPAPlanGrpList {

	private String companyName;
	private String companyID;
	private String comboID;
	private String planID;
	private String planEnable;
	private String comboName;
	private String planName;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyId() {
		return companyID;
	}

	public void setCompanyId(String companyID) {
		this.companyID = companyID;
	}

	public String getComboId() {
		return comboID;
	}

	public void setComboId(String comboID) {
		this.comboID = comboID;
	}

	public String getPlanId() {
		return planID;
	}

	public void setPlanId(String planID) {
		this.planID = planID;
	}

	public String getEnable() {
		return planEnable;
	}

	public void setEnable(String planEnable) {
		this.planEnable = planEnable;
	}

	public String getComboName() {
		return comboName;
	}

	public void setComboName(String comboName) {
		this.comboName = comboName;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

}
