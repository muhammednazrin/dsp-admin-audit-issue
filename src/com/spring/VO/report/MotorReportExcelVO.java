package com.spring.VO.report;

import java.math.BigDecimal;
import java.util.Date;

public class MotorReportExcelVO {

	private Date record_date;
	private String transaction_datetime;
	private String paymentTrxID;
	private String pmnt_status;
	private String amount;
	private String motor_net_premium;
	private String passenger_pa_premium_payable;
	private String grosspremium_final;
	private String discount;
	private String gst;
	private String invoice_no;
	private String policy_number;
	private String caps_dppa;
	private String customer_name;
	private String customer_nric_id;
	private String registration_number;
	private String pmnt_gateway_code;
	private String transaction_id;
	private String auth_id;
	private String bankrefno;
	private String approvalcode;
	private String fpx_fpxtxnid;
	private String fpx_debitauthcode;
	private String product_code;
	private String agent_code;
	private String agent_name;

	private String quotation_status;
	private String last_page;
	private String reason;

	private String trxID;
	private String paramName;
	private String paramValue;

	private int id;
	private String insCompany;

	private String salesEntity;
	private String marketerName;
	private String marketerCode;
	private String agentName;
	private String agentCode;
	private String DateFrom;
	private String DateTo;
	private String PolicyNo;
	private String CustomerName;
	private String ProductEntity;
	private String ProductType;
	private String Status;
	private String PremiumAmount;
	private String TransactionDate;
	private String TransactionID;
	private String CustomerNRIC;
	private String PaymentMethod;
	private String PaymentRefNo;
	private String PaymentStatus;
	private String Incentive;
	private String Commission;
	private String BankFee;
	private String TotalNet;
	private String CustomerEmail;
	private String CustomerAddress1;
	private String CustomerAddress2;
	private String CustomerAddress3;
	private String CustomerCity;
	private String CustomerPostcode;
	private String CustomerState;
	private String CustomerCountry;
	private String CustomerMobileNo;
	private String CustomerID;
	private String DSPQQID;
	private String PaymentChannel;
	private String InvoiceNo;
	private String ProductCode;
	private String Term;
	private String Coverage;
	private String Mode;
	private String PaymentAuthCode;
	private String TotalAmount;
	private String PremiumMode;
	private String MPAY_RefNo;
	private String MPAY_AuthCode;
	private String EBPG_RefNo;
	private String EBPG_AuthCode;
	private String M2U_RefNo;
	private String M2U_AuthCode;
	private String FPX_RefNo;
	private String FPX_AuthCode;
	// private String Reason;
	private String VehicleNo;
	private String AuditRemarks;
	private String VehicleLoc;
	private String VehicleMake;
	private String VehicleModel;
	private String YearMake;
	private String ChassisNo;
	private String FinancedBy;
	private String CoverageStartDate;
	private String CoverageEndDate;
	private String EngineNo;
	private String NVIC;

	private String DoneStep1;
	private String DoneStep2;

	private String TLCheckDigit;
	private String UWReason;
	private String QQID;
	private String DriverPAPolicyNo;
	private String JPJStatus;
	private String CAPSPolicyNo;

	private String HomeType;
	private String BuildConstructionType;
	private String HomeSumInsured;
	private String AddBenRiotStrike;
	private String AddBenExtendedTheft;
	private String AddBenRiotStrikeAmt;
	private String AddBenExtendedTheftAmt;
	private String ContentSumInsured;
	private String AdditionalBenefitCode;
	private String AdditionalBenefitValue;
	private String AdditionalBenefitText;

	private String TravelStartDate;
	private String TravelEndDate;
	private String OfferedPlanName;
	private String TravelAreaType;
	private String TravellingWith;
	private String PlateNo;

	private String HomeCoverage;
	private long TravelDuration;
	private String PreviousInsurance;
	private String capsAmount;
	private BigDecimal motorAmount;

	private String DriversPA;
	private String TPCDPPA;
	private String PAPREMIUM;
	private String MIQUOTATIONSTATUS;

	private String VehicleSeat;
	private String VehicleCC;
	private String MarketValue;
	private String SumInsured;
	private String PremiumSumInsured;
	private String GSTPolicyPremium;
	private String StampDutyPolicyPremium;
	private String PAPremiumPayable;
	private String TotalPremiumPayable;
	private String PremiumAfterDiscount;
	private String PremiumAfterGST;
	private String GrossPremiumFinal;
	private String Discount;
	private String GST;
	private String Excess;
	private String NonClaimDiscount;
	private String ParkingAddress1;
	private String ParkingAddress2;
	private String ParkingPostcode;
	private String ParkingState;
	private String PeriodCoverage;

	private String OperatorCode;
	private String QuotationCreateDateTime;
	private String QuotationUpdateDateTime;
	private String DriverNo;
	private String Loading;
	private String LoadingPercentage;
	private String AdditinalCodes;
	private String NCDAmount;
	private String AgentCommissionAmount;
	private String CAPSCommissionAmount;
	private String DiscountCode;
	private String IDType;
	private String CustomerDOB;
	private String CustomerGender;
	private String PolicyStatus;
	private String LLOP;
	private String LL2P;
	private String FloodCover;
	private String NCDRelief;
	private String SRCC;
	private String WindscreenSC;
	private String WindscreenP;
	private String VehicleAccSC;
	private String VehicleAccP;
	private String NGVGasSC;
	private String NGVGasP;
	private String FirstDriver;
	private String SecondDriver;
	private String CartPremium;
	private String CartPremiumDesc;
	private String MarkUpPer;
	private String SIMin;
	private String SIMax;

	private String FirstDriverInfo;
	private String SecondDriverInfo;
	private String ThirdDriverInfo;
	private String ForthDriverInfo;
	private String FifthDriverInfo;
	private String SixthDriverInfo;
	private String SeventhDriverInfo;

	private String VehicleParkOvernight;
	private String VehicleAntiTheft;
	private String SafetyFeature;

	private String CustomerAge;
	private String CARSECM;
	private String ATITFTCD;
	private String GARAGECD;

	private String BenefitCode1;
	private String BenefitCode2;
	private String BenefitCode3;
	private String BenefitCode4;
	private String BenefitCode5;
	private String BenefitCode6;
	private String BenefitCode7;
	private String BenefitCode8;
	private String BenefitCode9;
	private String BenefitCode10;

	private String BenefitText1;
	private String BenefitText2;
	private String BenefitText3;
	private String BenefitText4;
	private String BenefitText5;
	private String BenefitText6;
	private String BenefitText7;
	private String BenefitText8;
	private String BenefitText9;
	private String BenefitText10;

	private String HOHH_QQ4_UDQ;
	private String UW1;
	private String UW2;
	private String UW3;
	private String UW4;
	private String UW5;

	private String PropertyInsuredAddress1;
	private String PropertyInsuredAddress2;
	private String PropertyInsuredAddress3;
	private String PropertyInsuredPostcode;
	private String PropertyInsuredState;

	private String PDPA;
	private String HOSI;
	private String HHSI;
	private String HOHHSI;

	private String SpouseName;
	private String SpouseEmail;
	private String SpouseIDType;
	private String SpouseIDNumber;
	private String SpouseDOB;
	private String SpouseGender;
	private String ChildName;
	private String ChildIDType;
	private String ChildIDNumber;
	private String ChildDOB;
	private String ChildGender;

	private String ChildName_1;
	private String ChildIDType_1;
	private String ChildIDNumber_1;
	private String ChildDOB_1;
	private String ChildGender_1;

	private String ChildName_2;
	private String ChildIDType_2;
	private String ChildIDNumber_2;
	private String ChildDOB_2;
	private String ChildGender_2;

	private String ChildName_3;
	private String ChildIDType_3;
	private String ChildIDNumber_3;
	private String ChildDOB_3;
	private String ChildGender_3;

	private String ChildName_4;
	private String ChildIDType_4;
	private String ChildIDNumber_4;
	private String ChildDOB_4;
	private String ChildGender_4;

	private String ChildName_5;
	private String ChildIDType_5;
	private String ChildIDNumber_5;
	private String ChildDOB_5;
	private String ChildGender_5;

	private String ChildName_6;
	private String ChildIDType_6;
	private String ChildIDNumber_6;
	private String ChildDOB_6;
	private String ChildGender_6;

	private String ChildName_7;
	private String ChildIDType_7;
	private String ChildIDNumber_7;
	private String ChildDOB_7;
	private String ChildGender_7;

	private String LastPage;
	private String AreaCode;
	private String AnnualPremium;

	// new addons March 2018

	private String sparePartPremiumPaid; // premium paid by customer depending on vehicle age and sum insured
	private String smartKeySumCovered; // smart key sum covered amount
	private String smartKeyPremiumAmt; // premium amt paid by customer
	private String sprayPlanCode; // Either D01A, D01B, D01C
	private String sprayPlanPremiumAmt; // premium amt paid by customer
	private String otoPlan; // Either Silver, Gold, Platinum
	private String otoDiscount; // discount (if applicable)
	private String otoPremiumAmt; // premium amt paid by customer

	// Txn Report
	private String deliveryName;
	private String roadTaxAmount;
	private String rtpf;
	private String rtpfgst;
	private String rtprf;
	private String rtprfgst;
	private String rtdf;
	private String rtdfgst;
	private String rttotalpayable;
	private String period;
	private String address1;
	private String address2;
	private String address3;
	private String postcode;
	private String rtstate;
	private String mobile;
	private String printStatus;
	private String remarks;
	private String consignmentNo;
	private String trackingNo;
	private String bankName;
	private String accountNo;
	private String refundStatus;
	private String rdtaxInvoice;
	private String message;

	private String AgreedValue;
	private String AgreedValueSelection;
	private String BasicPremium;
	private String PassengerPaPremium;
	private String CartPremiumVal;

	private String LOU;
	private String LouPremiumVal;

	private String modeStatus;

	// SST
	private String sst_percentage;
	private String sst_amount;
	private String pfNumber;

	private String recurringTerm;
	private String recurringCardType;
	
	private String EhailPremiumAmt;
	private String EhailStartDate;
	private String EhailEndDate;
	private String EhailDigiLic;
	private String EHailDriver1Name;
	private String EHailDriver1IC;
	private String EHailDriver2Name;
	private String EHailDriver2IC;

	
	
	public String getEhailPremiumAmt() {
		return EhailPremiumAmt;
	}

	public void setEhailPremiumAmt(String ehailPremiumAmt) {
		EhailPremiumAmt = ehailPremiumAmt;
	}

	public String getEhailStartDate() {
		return EhailStartDate;
	}

	public void setEhailStartDate(String ehailStartDate) {
		EhailStartDate = ehailStartDate;
	}

	public String getEhailEndDate() {
		return EhailEndDate;
	}

	public void setEhailEndDate(String ehailEndDate) {
		EhailEndDate = ehailEndDate;
	}

	public String getEhailDigiLic() {
		return EhailDigiLic;
	}

	public void setEhailDigiLic(String ehailDigiLic) {
		EhailDigiLic = ehailDigiLic;
	}

	public String getEHailDriver1Name() {
		return EHailDriver1Name;
	}

	public void setEHailDriver1Name(String eHailDriver1Name) {
		EHailDriver1Name = eHailDriver1Name;
	}

	public String getEHailDriver1IC() {
		return EHailDriver1IC;
	}

	public void setEHailDriver1IC(String eHailDriver1IC) {
		EHailDriver1IC = eHailDriver1IC;
	}

	public String getEHailDriver2Name() {
		return EHailDriver2Name;
	}

	public void setEHailDriver2Name(String eHailDriver2Name) {
		EHailDriver2Name = eHailDriver2Name;
	}

	public String getEHailDriver2IC() {
		return EHailDriver2IC;
	}

	public void setEHailDriver2IC(String eHailDriver2IC) {
		EHailDriver2IC = eHailDriver2IC;
	}

	public String getModeStatus() {
		return modeStatus;
	}

	public void setModeStatus(String modeStatus) {
		this.modeStatus = modeStatus;
	}

	public String getSparePartPremiumPaid() {
		return sparePartPremiumPaid;
	}

	public String getLOU() {
		return LOU;
	}

	public void setLOU(String lOU) {
		LOU = lOU;
	}

	public String getLouPremiumVal() {
		return LouPremiumVal;
	}

	public void setLouPremiumVal(String louPremiumVal) {
		LouPremiumVal = louPremiumVal;
	}

	public void setSparePartPremiumPaid(String sparePartPremiumPaid) {
		this.sparePartPremiumPaid = sparePartPremiumPaid;
	}

	public String getSmartKeySumCovered() {
		return smartKeySumCovered;
	}

	public void setSmartKeySumCovered(String smartKeySumCovered) {
		this.smartKeySumCovered = smartKeySumCovered;
	}

	public String getSmartKeyPremiumAmt() {
		return smartKeyPremiumAmt;
	}

	public void setSmartKeyPremiumAmt(String smartKeyPremiumAmt) {
		this.smartKeyPremiumAmt = smartKeyPremiumAmt;
	}

	public String getSprayPlanCode() {
		return sprayPlanCode;
	}

	public void setSprayPlanCode(String sprayPlanCode) {
		this.sprayPlanCode = sprayPlanCode;
	}

	public String getSprayPlanPremiumAmt() {
		return sprayPlanPremiumAmt;
	}

	public void setSprayPlanPremiumAmt(String sprayPlanPremiumAmt) {
		this.sprayPlanPremiumAmt = sprayPlanPremiumAmt;
	}

	public String getOtoPlan() {
		return otoPlan;
	}

	public void setOtoPlan(String otoPlan) {
		this.otoPlan = otoPlan;
	}

	public String getOtoDiscount() {
		return otoDiscount;
	}

	public void setOtoDiscount(String otoDiscount) {
		this.otoDiscount = otoDiscount;
	}

	public String getOtoPremiumAmt() {
		return otoPremiumAmt;
	}

	public void setOtoPremiumAmt(String otoPremiumAmt) {
		this.otoPremiumAmt = otoPremiumAmt;
	}

	public String getCartPremiumVal() {
		return CartPremiumVal;
	}

	public void setCartPremiumVal(String cartPremiumVal) {
		CartPremiumVal = cartPremiumVal;
	}

	public String getPassengerPaPremium() {
		return PassengerPaPremium;
	}

	public void setPassengerPaPremium(String passengerPaPremium) {
		PassengerPaPremium = passengerPaPremium;
	}

	public String getBasicPremium() {
		return BasicPremium;
	}

	public void setBasicPremium(String basicPremium) {
		BasicPremium = basicPremium;
	}

	public String getAgreedValue() {
		return AgreedValue;
	}

	public void setAgreedValue(String agreedValue) {
		AgreedValue = agreedValue;
	}

	public String getAgreedValueSelection() {
		return AgreedValueSelection;
	}

	public void setAgreedValueSelection(String agreedValueSelection) {
		AgreedValueSelection = agreedValueSelection;
	}

	public String getAnnualPremium() {
		return AnnualPremium;
	}

	public void setAnnualPremium(String annualPremium) {
		AnnualPremium = annualPremium;
	}

	public String getAreaCode() {
		return AreaCode;
	}

	public void setAreaCode(String areaCode) {
		AreaCode = areaCode;
	}

	public String getLastPage() {
		return LastPage;
	}

	public void setLastPage(String lastPage) {
		LastPage = lastPage;
	}

	public String getSpouseName() {
		return SpouseName;
	}

	public void setSpouseName(String spouseName) {
		SpouseName = spouseName;
	}

	public String getSpouseEmail() {
		return SpouseEmail;
	}

	public void setSpouseEmail(String spouseEmail) {
		SpouseEmail = spouseEmail;
	}

	public String getSpouseIDType() {
		return SpouseIDType;
	}

	public void setSpouseIDType(String spouseIDType) {
		SpouseIDType = spouseIDType;
	}

	public String getSpouseIDNumber() {
		return SpouseIDNumber;
	}

	public void setSpouseIDNumber(String spouseIDNumber) {
		SpouseIDNumber = spouseIDNumber;
	}

	public String getSpouseDOB() {
		return SpouseDOB;
	}

	public void setSpouseDOB(String spouseDOB) {
		SpouseDOB = spouseDOB;
	}

	public String getSpouseGender() {
		return SpouseGender;
	}

	public void setSpouseGender(String spouseGender) {
		SpouseGender = spouseGender;
	}

	public String getChildName() {
		return ChildName;
	}

	public void setChildName(String childName) {
		ChildName = childName;
	}

	public String getChildIDType() {
		return ChildIDType;
	}

	public void setChildIDType(String childIDType) {
		ChildIDType = childIDType;
	}

	public String getChildIDNumber() {
		return ChildIDNumber;
	}

	public void setChildIDNumber(String childIDNumber) {
		ChildIDNumber = childIDNumber;
	}

	public String getChildDOB() {
		return ChildDOB;
	}

	public void setChildDOB(String childDOB) {
		ChildDOB = childDOB;
	}

	public String getChildGender() {
		return ChildGender;
	}

	public void setChildGender(String childGender) {
		ChildGender = childGender;
	}

	public String getChildName_1() {
		return ChildName_1;
	}

	public void setChildName_1(String childName_1) {
		ChildName_1 = childName_1;
	}

	public String getChildIDType_1() {
		return ChildIDType_1;
	}

	public void setChildIDType_1(String childIDType_1) {
		ChildIDType_1 = childIDType_1;
	}

	public String getChildIDNumber_1() {
		return ChildIDNumber_1;
	}

	public void setChildIDNumber_1(String childIDNumber_1) {
		ChildIDNumber_1 = childIDNumber_1;
	}

	public String getChildDOB_1() {
		return ChildDOB_1;
	}

	public void setChildDOB_1(String childDOB_1) {
		ChildDOB_1 = childDOB_1;
	}

	public String getChildGender_1() {
		return ChildGender_1;
	}

	public void setChildGender_1(String childGender_1) {
		ChildGender_1 = childGender_1;
	}

	public String getChildName_2() {
		return ChildName_2;
	}

	public void setChildName_2(String childName_2) {
		ChildName_2 = childName_2;
	}

	public String getChildIDType_2() {
		return ChildIDType_2;
	}

	public void setChildIDType_2(String childIDType_2) {
		ChildIDType_2 = childIDType_2;
	}

	public String getChildIDNumber_2() {
		return ChildIDNumber_2;
	}

	public void setChildIDNumber_2(String childIDNumber_2) {
		ChildIDNumber_2 = childIDNumber_2;
	}

	public String getChildDOB_2() {
		return ChildDOB_2;
	}

	public void setChildDOB_2(String childDOB_2) {
		ChildDOB_2 = childDOB_2;
	}

	public String getChildGender_2() {
		return ChildGender_2;
	}

	public void setChildGender_2(String childGender_2) {
		ChildGender_2 = childGender_2;
	}

	public String getChildName_3() {
		return ChildName_3;
	}

	public void setChildName_3(String childName_3) {
		ChildName_3 = childName_3;
	}

	public String getChildIDType_3() {
		return ChildIDType_3;
	}

	public void setChildIDType_3(String childIDType_3) {
		ChildIDType_3 = childIDType_3;
	}

	public String getChildIDNumber_3() {
		return ChildIDNumber_3;
	}

	public void setChildIDNumber_3(String childIDNumber_3) {
		ChildIDNumber_3 = childIDNumber_3;
	}

	public String getChildDOB_3() {
		return ChildDOB_3;
	}

	public void setChildDOB_3(String childDOB_3) {
		ChildDOB_3 = childDOB_3;
	}

	public String getChildGender_3() {
		return ChildGender_3;
	}

	public void setChildGender_3(String childGender_3) {
		ChildGender_3 = childGender_3;
	}

	public String getChildName_4() {
		return ChildName_4;
	}

	public void setChildName_4(String childName_4) {
		ChildName_4 = childName_4;
	}

	public String getChildIDType_4() {
		return ChildIDType_4;
	}

	public void setChildIDType_4(String childIDType_4) {
		ChildIDType_4 = childIDType_4;
	}

	public String getChildIDNumber_4() {
		return ChildIDNumber_4;
	}

	public void setChildIDNumber_4(String childIDNumber_4) {
		ChildIDNumber_4 = childIDNumber_4;
	}

	public String getChildDOB_4() {
		return ChildDOB_4;
	}

	public void setChildDOB_4(String childDOB_4) {
		ChildDOB_4 = childDOB_4;
	}

	public String getChildGender_4() {
		return ChildGender_4;
	}

	public void setChildGender_4(String childGender_4) {
		ChildGender_4 = childGender_4;
	}

	public String getChildName_5() {
		return ChildName_5;
	}

	public void setChildName_5(String childName_5) {
		ChildName_5 = childName_5;
	}

	public String getChildIDType_5() {
		return ChildIDType_5;
	}

	public void setChildIDType_5(String childIDType_5) {
		ChildIDType_5 = childIDType_5;
	}

	public String getChildIDNumber_5() {
		return ChildIDNumber_5;
	}

	public void setChildIDNumber_5(String childIDNumber_5) {
		ChildIDNumber_5 = childIDNumber_5;
	}

	public String getChildDOB_5() {
		return ChildDOB_5;
	}

	public void setChildDOB_5(String childDOB_5) {
		ChildDOB_5 = childDOB_5;
	}

	public String getChildGender_5() {
		return ChildGender_5;
	}

	public void setChildGender_5(String childGender_5) {
		ChildGender_5 = childGender_5;
	}

	public String getChildName_6() {
		return ChildName_6;
	}

	public void setChildName_6(String childName_6) {
		ChildName_6 = childName_6;
	}

	public String getChildIDType_6() {
		return ChildIDType_6;
	}

	public void setChildIDType_6(String childIDType_6) {
		ChildIDType_6 = childIDType_6;
	}

	public String getChildIDNumber_6() {
		return ChildIDNumber_6;
	}

	public void setChildIDNumber_6(String childIDNumber_6) {
		ChildIDNumber_6 = childIDNumber_6;
	}

	public String getChildDOB_6() {
		return ChildDOB_6;
	}

	public void setChildDOB_6(String childDOB_6) {
		ChildDOB_6 = childDOB_6;
	}

	public String getChildGender_6() {
		return ChildGender_6;
	}

	public void setChildGender_6(String childGender_6) {
		ChildGender_6 = childGender_6;
	}

	public String getChildName_7() {
		return ChildName_7;
	}

	public void setChildName_7(String childName_7) {
		ChildName_7 = childName_7;
	}

	public String getChildIDType_7() {
		return ChildIDType_7;
	}

	public void setChildIDType_7(String childIDType_7) {
		ChildIDType_7 = childIDType_7;
	}

	public String getChildIDNumber_7() {
		return ChildIDNumber_7;
	}

	public void setChildIDNumber_7(String childIDNumber_7) {
		ChildIDNumber_7 = childIDNumber_7;
	}

	public String getChildDOB_7() {
		return ChildDOB_7;
	}

	public void setChildDOB_7(String childDOB_7) {
		ChildDOB_7 = childDOB_7;
	}

	public String getChildGender_7() {
		return ChildGender_7;
	}

	public void setChildGender_7(String childGender_7) {
		ChildGender_7 = childGender_7;
	}

	public String getUW1() {
		return UW1;
	}

	public void setUW1(String uW1) {
		UW1 = uW1;
	}

	public String getUW2() {
		return UW2;
	}

	public void setUW2(String uW2) {
		UW2 = uW2;
	}

	public String getUW3() {
		return UW3;
	}

	public void setUW3(String uW3) {
		UW3 = uW3;
	}

	public String getUW4() {
		return UW4;
	}

	public void setUW4(String uW4) {
		UW4 = uW4;
	}

	public String getUW5() {
		return UW5;
	}

	public void setUW5(String uW5) {
		UW5 = uW5;
	}

	public String getHOSI() {
		return HOSI;
	}

	public void setHOSI(String hOSI) {
		HOSI = hOSI;
	}

	public String getHHSI() {
		return HHSI;
	}

	public void setHHSI(String hHSI) {
		HHSI = hHSI;
	}

	public String getHOHHSI() {
		return HOHHSI;
	}

	public void setHOHHSI(String hOHHSI) {
		HOHHSI = hOHHSI;
	}

	public String getPDPA() {
		return PDPA;
	}

	public void setPDPA(String pDPA) {
		PDPA = pDPA;
	}

	public String getPropertyInsuredAddress3() {
		return PropertyInsuredAddress3;
	}

	public void setPropertyInsuredAddress3(String propertyInsuredAddress3) {
		PropertyInsuredAddress3 = propertyInsuredAddress3;
	}

	public String getPropertyInsuredAddress1() {
		return PropertyInsuredAddress1;
	}

	public void setPropertyInsuredAddress1(String propertyInsuredAddress1) {
		PropertyInsuredAddress1 = propertyInsuredAddress1;
	}

	public String getPropertyInsuredAddress2() {
		return PropertyInsuredAddress2;
	}

	public void setPropertyInsuredAddress2(String propertyInsuredAddress2) {
		PropertyInsuredAddress2 = propertyInsuredAddress2;
	}

	public String getPropertyInsuredPostcode() {
		return PropertyInsuredPostcode;
	}

	public void setPropertyInsuredPostcode(String propertyInsuredPostcode) {
		PropertyInsuredPostcode = propertyInsuredPostcode;
	}

	public String getPropertyInsuredState() {
		return PropertyInsuredState;
	}

	public void setPropertyInsuredState(String propertyInsuredState) {
		PropertyInsuredState = propertyInsuredState;
	}

	public String getBenefitCode1() {
		return BenefitCode1;
	}

	public void setBenefitCode1(String benefitCode1) {
		BenefitCode1 = benefitCode1;
	}

	public String getBenefitCode2() {
		return BenefitCode2;
	}

	public void setBenefitCode2(String benefitCode2) {
		BenefitCode2 = benefitCode2;
	}

	public String getBenefitCode3() {
		return BenefitCode3;
	}

	public void setBenefitCode3(String benefitCode3) {
		BenefitCode3 = benefitCode3;
	}

	public String getBenefitCode4() {
		return BenefitCode4;
	}

	public void setBenefitCode4(String benefitCode4) {
		BenefitCode4 = benefitCode4;
	}

	public String getBenefitCode5() {
		return BenefitCode5;
	}

	public void setBenefitCode5(String benefitCode5) {
		BenefitCode5 = benefitCode5;
	}

	public String getBenefitCode6() {
		return BenefitCode6;
	}

	public void setBenefitCode6(String benefitCode6) {
		BenefitCode6 = benefitCode6;
	}

	public String getBenefitCode7() {
		return BenefitCode7;
	}

	public void setBenefitCode7(String benefitCode7) {
		BenefitCode7 = benefitCode7;
	}

	public String getBenefitCode8() {
		return BenefitCode8;
	}

	public void setBenefitCode8(String benefitCode8) {
		BenefitCode8 = benefitCode8;
	}

	public String getBenefitCode9() {
		return BenefitCode9;
	}

	public void setBenefitCode9(String benefitCode9) {
		BenefitCode9 = benefitCode9;
	}

	public String getBenefitCode10() {
		return BenefitCode10;
	}

	public void setBenefitCode10(String benefitCode10) {
		BenefitCode10 = benefitCode10;
	}

	public String getBenefitText1() {
		return BenefitText1;
	}

	public void setBenefitText1(String benefitText1) {
		BenefitText1 = benefitText1;
	}

	public String getBenefitText2() {
		return BenefitText2;
	}

	public void setBenefitText2(String benefitText2) {
		BenefitText2 = benefitText2;
	}

	public String getBenefitText3() {
		return BenefitText3;
	}

	public void setBenefitText3(String benefitText3) {
		BenefitText3 = benefitText3;
	}

	public String getBenefitText4() {
		return BenefitText4;
	}

	public void setBenefitText4(String benefitText4) {
		BenefitText4 = benefitText4;
	}

	public String getBenefitText5() {
		return BenefitText5;
	}

	public void setBenefitText5(String benefitText5) {
		BenefitText5 = benefitText5;
	}

	public String getBenefitText6() {
		return BenefitText6;
	}

	public void setBenefitText6(String benefitText6) {
		BenefitText6 = benefitText6;
	}

	public String getBenefitText7() {
		return BenefitText7;
	}

	public void setBenefitText7(String benefitText7) {
		BenefitText7 = benefitText7;
	}

	public String getBenefitText8() {
		return BenefitText8;
	}

	public void setBenefitText8(String benefitText8) {
		BenefitText8 = benefitText8;
	}

	public String getBenefitText9() {
		return BenefitText9;
	}

	public void setBenefitText9(String benefitText9) {
		BenefitText9 = benefitText9;
	}

	public String getBenefitText10() {
		return BenefitText10;
	}

	public void setBenefitText10(String benefitText10) {
		BenefitText10 = benefitText10;
	}

	public String getHOHH_QQ4_UDQ() {
		return HOHH_QQ4_UDQ;
	}

	public void setHOHH_QQ4_UDQ(String hOHH_QQ4_UDQ) {
		HOHH_QQ4_UDQ = hOHH_QQ4_UDQ;
	}

	public String getCARSECM() {
		return CARSECM;
	}

	public void setCARSECM(String cARSECM) {
		CARSECM = cARSECM;
	}

	public String getATITFTCD() {
		return ATITFTCD;
	}

	public void setATITFTCD(String aTITFTCD) {
		ATITFTCD = aTITFTCD;
	}

	public String getGARAGECD() {
		return GARAGECD;
	}

	public void setGARAGECD(String gARAGECD) {
		GARAGECD = gARAGECD;
	}

	public String getCustomerAge() {
		return CustomerAge;
	}

	public void setCustomerAge(String customerAge) {
		CustomerAge = customerAge;
	}

	public String getVehicleParkOvernight() {
		return VehicleParkOvernight;
	}

	public void setVehicleParkOvernight(String vehicleParkOvernight) {
		VehicleParkOvernight = vehicleParkOvernight;
	}

	public String getVehicleAntiTheft() {
		return VehicleAntiTheft;
	}

	public void setVehicleAntiTheft(String vehicleAntiTheft) {
		VehicleAntiTheft = vehicleAntiTheft;
	}

	public String getSafetyFeature() {
		return SafetyFeature;
	}

	public void setSafetyFeature(String safetyFeature) {
		SafetyFeature = safetyFeature;
	}

	public String getFirstDriverInfo() {
		return FirstDriverInfo;
	}

	public void setFirstDriverInfo(String firstDriverInfo) {
		FirstDriverInfo = firstDriverInfo;
	}

	public String getSecondDriverInfo() {
		return SecondDriverInfo;
	}

	public void setSecondDriverInfo(String secondDriverInfo) {
		SecondDriverInfo = secondDriverInfo;
	}

	public String getThirdDriverInfo() {
		return ThirdDriverInfo;
	}

	public void setThirdDriverInfo(String thirdDriverInfo) {
		ThirdDriverInfo = thirdDriverInfo;
	}

	public String getForthDriverInfo() {
		return ForthDriverInfo;
	}

	public void setForthDriverInfo(String forthDriverInfo) {
		ForthDriverInfo = forthDriverInfo;
	}

	public String getFifthDriverInfo() {
		return FifthDriverInfo;
	}

	public void setFifthDriverInfo(String fifthDriverInfo) {
		FifthDriverInfo = fifthDriverInfo;
	}

	public String getSixthDriverInfo() {
		return SixthDriverInfo;
	}

	public void setSixthDriverInfo(String sixthDriverInfo) {
		SixthDriverInfo = sixthDriverInfo;
	}

	public String getSeventhDriverInfo() {
		return SeventhDriverInfo;
	}

	public void setSeventhDriverInfo(String seventhDriverInfo) {
		SeventhDriverInfo = seventhDriverInfo;
	}

	public String getSIMin() {
		return SIMin;
	}

	public void setSIMin(String sIMin) {
		SIMin = sIMin;
	}

	public String getSIMax() {
		return SIMax;
	}

	public void setSIMax(String sIMax) {
		SIMax = sIMax;
	}

	public String getOperatorCode() {
		return OperatorCode;
	}

	public void setOperatorCode(String operatorCode) {
		OperatorCode = operatorCode;
	}

	public String getQuotationCreateDateTime() {
		return QuotationCreateDateTime;
	}

	public void setQuotationCreateDateTime(String quotationCreateDateTime) {
		QuotationCreateDateTime = quotationCreateDateTime;
	}

	public String getQuotationUpdateDateTime() {
		return QuotationUpdateDateTime;
	}

	public void setQuotationUpdateDateTime(String quotationUpdateDateTime) {
		QuotationUpdateDateTime = quotationUpdateDateTime;
	}

	public String getDriverNo() {
		return DriverNo;
	}

	public void setDriverNo(String driverNo) {
		DriverNo = driverNo;
	}

	public String getLoading() {
		return Loading;
	}

	public void setLoading(String loading) {
		Loading = loading;
	}

	public String getLoadingPercentage() {
		return LoadingPercentage;
	}

	public void setLoadingPercentage(String loadingPercentage) {
		LoadingPercentage = loadingPercentage;
	}

	public String getAdditinalCodes() {
		return AdditinalCodes;
	}

	public void setAdditinalCodes(String additinalCodes) {
		AdditinalCodes = additinalCodes;
	}

	public String getNCDAmount() {
		return NCDAmount;
	}

	public void setNCDAmount(String nCDAmount) {
		NCDAmount = nCDAmount;
	}

	public String getAgentCommissionAmount() {
		return AgentCommissionAmount;
	}

	public void setAgentCommissionAmount(String agentCommissionAmount) {
		AgentCommissionAmount = agentCommissionAmount;
	}

	public String getCAPSCommissionAmount() {
		return CAPSCommissionAmount;
	}

	public void setCAPSCommissionAmount(String cAPSCommissionAmount) {
		CAPSCommissionAmount = cAPSCommissionAmount;
	}

	public String getDiscountCode() {
		return DiscountCode;
	}

	public void setDiscountCode(String discountCode) {
		DiscountCode = discountCode;
	}

	public String getIDType() {
		return IDType;
	}

	public void setIDType(String iDType) {
		IDType = iDType;
	}

	public String getCustomerDOB() {
		return CustomerDOB;
	}

	public void setCustomerDOB(String customerDOB) {
		CustomerDOB = customerDOB;
	}

	public String getCustomerGender() {
		return CustomerGender;
	}

	public void setCustomerGender(String customerGender) {
		CustomerGender = customerGender;
	}

	public String getPolicyStatus() {
		return PolicyStatus;
	}

	public void setPolicyStatus(String policyStatus) {
		PolicyStatus = policyStatus;
	}

	public String getLLOP() {
		return LLOP;
	}

	public void setLLOP(String lLOP) {
		LLOP = lLOP;
	}

	public String getLL2P() {
		return LL2P;
	}

	public void setLL2P(String lL2P) {
		LL2P = lL2P;
	}

	public String getFloodCover() {
		return FloodCover;
	}

	public void setFloodCover(String floodCover) {
		FloodCover = floodCover;
	}

	public String getNCDRelief() {
		return NCDRelief;
	}

	public void setNCDRelief(String nCDRelief) {
		NCDRelief = nCDRelief;
	}

	public String getSRCC() {
		return SRCC;
	}

	public void setSRCC(String sRCC) {
		SRCC = sRCC;
	}

	public String getWindscreenSC() {
		return WindscreenSC;
	}

	public void setWindscreenSC(String windscreenSC) {
		WindscreenSC = windscreenSC;
	}

	public String getWindscreenP() {
		return WindscreenP;
	}

	public void setWindscreenP(String windscreenP) {
		WindscreenP = windscreenP;
	}

	public String getVehicleAccSC() {
		return VehicleAccSC;
	}

	public void setVehicleAccSC(String vehicleAccSC) {
		VehicleAccSC = vehicleAccSC;
	}

	public String getVehicleAccP() {
		return VehicleAccP;
	}

	public void setVehicleAccP(String vehicleAccP) {
		VehicleAccP = vehicleAccP;
	}

	public String getNGVGasSC() {
		return NGVGasSC;
	}

	public void setNGVGasSC(String nGVGasSC) {
		NGVGasSC = nGVGasSC;
	}

	public String getNGVGasP() {
		return NGVGasP;
	}

	public void setNGVGasP(String nGVGasP) {
		NGVGasP = nGVGasP;
	}

	public String getFirstDriver() {
		return FirstDriver;
	}

	public void setFirstDriver(String firstDriver) {
		FirstDriver = firstDriver;
	}

	public String getSecondDriver() {
		return SecondDriver;
	}

	public void setSecondDriver(String secondDriver) {
		SecondDriver = secondDriver;
	}

	public String getCartPremium() {
		return CartPremium;
	}

	public void setCartPremium(String cartPremium) {
		CartPremium = cartPremium;
	}

	public String getCartPremiumDesc() {
		return CartPremiumDesc;
	}

	public void setCartPremiumDesc(String cartPremiumDesc) {
		CartPremiumDesc = cartPremiumDesc;
	}

	public String getMarkUpPer() {
		return MarkUpPer;
	}

	public void setMarkUpPer(String markUpPer) {
		MarkUpPer = markUpPer;
	}

	public String getPeriodCoverage() {
		return PeriodCoverage;
	}

	public void setPeriodCoverage(String periodCoverage) {
		PeriodCoverage = periodCoverage;
	}

	public String getExcess() {
		return Excess;
	}

	public void setExcess(String excess) {
		Excess = excess;
	}

	public String getNonClaimDiscount() {
		return NonClaimDiscount;
	}

	public void setNonClaimDiscount(String nonClaimDiscount) {
		NonClaimDiscount = nonClaimDiscount;
	}

	public String getParkingAddress1() {
		return ParkingAddress1;
	}

	public void setParkingAddress1(String parkingAddress1) {
		ParkingAddress1 = parkingAddress1;
	}

	public String getParkingAddress2() {
		return ParkingAddress2;
	}

	public void setParkingAddress2(String parkingAddress2) {
		ParkingAddress2 = parkingAddress2;
	}

	public String getParkingPostcode() {
		return ParkingPostcode;
	}

	public void setParkingPostcode(String parkingPostcode) {
		ParkingPostcode = parkingPostcode;
	}

	public String getParkingState() {
		return ParkingState;
	}

	public void setParkingState(String parkingState) {
		ParkingState = parkingState;
	}

	public String getGrossPremiumFinal() {
		return GrossPremiumFinal;
	}

	public void setGrossPremiumFinal(String grossPremiumFinal) {
		GrossPremiumFinal = grossPremiumFinal;
	}

	public String getDiscount() {
		return Discount;
	}

	public void setDiscount(String discount) {
		Discount = discount;
	}

	public String getGST() {
		return GST;
	}

	public void setGST(String gST) {
		GST = gST;
	}

	public String getTotalPremiumPayable() {
		return TotalPremiumPayable;
	}

	public void setTotalPremiumPayable(String totalPremiumPayable) {
		TotalPremiumPayable = totalPremiumPayable;
	}

	public String getPremiumAfterDiscount() {
		return PremiumAfterDiscount;
	}

	public void setPremiumAfterDiscount(String premiumAfterDiscount) {
		PremiumAfterDiscount = premiumAfterDiscount;
	}

	public String getPremiumAfterGST() {
		return PremiumAfterGST;
	}

	public void setPremiumAfterGST(String premiumAfterGST) {
		PremiumAfterGST = premiumAfterGST;
	}

	public String getGSTPolicyPremium() {
		return GSTPolicyPremium;
	}

	public void setGSTPolicyPremium(String gSTPolicyPremium) {
		GSTPolicyPremium = gSTPolicyPremium;
	}

	public String getStampDutyPolicyPremium() {
		return StampDutyPolicyPremium;
	}

	public void setStampDutyPolicyPremium(String stampDutyPolicyPremium) {
		StampDutyPolicyPremium = stampDutyPolicyPremium;
	}

	public String getPAPremiumPayable() {
		return PAPremiumPayable;
	}

	public void setPAPremiumPayable(String pAPremiumPayable) {
		PAPremiumPayable = pAPremiumPayable;
	}

	public String getPremiumSumInsured() {
		return PremiumSumInsured;
	}

	public void setPremiumSumInsured(String premiumSumInsured) {
		PremiumSumInsured = premiumSumInsured;
	}

	public String getSumInsured() {
		return SumInsured;
	}

	public void setSumInsured(String sumInsured) {
		SumInsured = sumInsured;
	}

	public String getVehicleSeat() {
		return VehicleSeat;
	}

	public void setVehicleSeat(String vehicleSeat) {
		VehicleSeat = vehicleSeat;
	}

	public String getVehicleCC() {
		return VehicleCC;
	}

	public void setVehicleCC(String vehicleCC) {
		VehicleCC = vehicleCC;
	}

	public String getMarketValue() {
		return MarketValue;
	}

	public void setMarketValue(String marketValue) {
		MarketValue = marketValue;
	}

	public String getTPCDPPA() {
		return TPCDPPA;
	}

	public void setTPCDPPA(String tPCDPPA) {
		TPCDPPA = tPCDPPA;
	}

	public String getPAPREMIUM() {
		return PAPREMIUM;
	}

	public void setPAPREMIUM(String pAPREMIUM) {
		PAPREMIUM = pAPREMIUM;
	}

	public String getMIQUOTATIONSTATUS() {
		return MIQUOTATIONSTATUS;
	}

	public void setMIQUOTATIONSTATUS(String mIQUOTATIONSTATUS) {
		MIQUOTATIONSTATUS = mIQUOTATIONSTATUS;
	}

	public String getDriversPA() {
		return DriversPA;
	}

	public void setDriversPA(String driversPA) {
		DriversPA = driversPA;
	}

	public String getCapsAmount() {
		return capsAmount;
	}

	public void setCapsAmount(String capsAmount) {
		this.capsAmount = capsAmount;
	}

	public BigDecimal getMotorAmount() {
		return motorAmount;
	}

	public void setMotorAmount(BigDecimal overallAmount) {
		this.motorAmount = overallAmount;
	}

	public String getInsCompany() {
		return insCompany;
	}

	public void setInsCompany(String insCompany) {
		this.insCompany = insCompany;
	}

	private int RecordCount;

	public String getHomeCoverage() {
		return HomeCoverage;
	}

	public void setHomeCoverage(String homeCoverage) {
		HomeCoverage = homeCoverage;
	}

	public String getCustomerID() {
		return CustomerID;
	}

	public void setCustomerID(String customerID) {
		CustomerID = customerID;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDateFrom() {
		return DateFrom;
	}

	public void setDateFrom(String dateFrom) {
		DateFrom = dateFrom;
	}

	public String getDateTo() {
		return DateTo;
	}

	public void setDateTo(String dateTo) {
		DateTo = dateTo;
	}

	public String getPolicyNo() {
		return PolicyNo;
	}

	public void setPolicyNo(String policyNo) {
		PolicyNo = policyNo;
	}

	public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public String getProductEntity() {
		return ProductEntity;
	}

	public void setProductEntity(String productEntity) {
		ProductEntity = productEntity;
	}

	public String getProductType() {
		return ProductType;
	}

	public void setProductType(String productType) {
		ProductType = productType;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getPremiumAmount() {
		return PremiumAmount;
	}

	public void setPremiumAmount(String premiumAmount) {
		PremiumAmount = premiumAmount;
	}

	public String getTransactionDate() {
		return TransactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		TransactionDate = transactionDate;
	}

	public String getTransactionID() {
		return TransactionID;
	}

	public void setTransactionID(String transactionID) {
		TransactionID = transactionID;
	}

	public String getCustomerNRIC() {
		return CustomerNRIC;
	}

	public void setCustomerNRIC(String customerNRIC) {
		CustomerNRIC = customerNRIC;
	}

	public String getPaymentMethod() {
		return PaymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		PaymentMethod = paymentMethod;
	}

	public String getPaymentRefNo() {
		return PaymentRefNo;
	}

	public void setPaymentRefNo(String paymentRefNo) {
		PaymentRefNo = paymentRefNo;
	}

	public String getPaymentStatus() {
		return PaymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		PaymentStatus = paymentStatus;
	}

	public String getIncentive() {
		return Incentive;
	}

	public void setIncentive(String incentive) {
		Incentive = incentive;
	}

	public String getCommission() {
		return Commission;
	}

	public void setCommission(String commission) {
		Commission = commission;
	}

	public String getBankFee() {
		return BankFee;
	}

	public void setBankFee(String bankFee) {
		BankFee = bankFee;
	}

	public String getTotalNet() {
		return TotalNet;
	}

	public void setTotalNet(String totalNet) {
		TotalNet = totalNet;
	}

	public String getCustomerEmail() {
		return CustomerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		CustomerEmail = customerEmail;
	}

	public String getCustomerAddress1() {
		return CustomerAddress1;
	}

	public void setCustomerAddress1(String customerAddress1) {
		CustomerAddress1 = customerAddress1;
	}

	public String getCustomerAddress2() {
		return CustomerAddress2;
	}

	public void setCustomerAddress2(String customerAddress2) {
		CustomerAddress2 = customerAddress2;
	}

	public String getCustomerAddress3() {
		return CustomerAddress3;
	}

	public void setCustomerAddress3(String customerAddress3) {
		CustomerAddress3 = customerAddress3;
	}

	public String getCustomerCity() {
		return CustomerCity;
	}

	public void setCustomerCity(String customerCity) {
		CustomerCity = customerCity;
	}

	public String getCustomerPostcode() {
		return CustomerPostcode;
	}

	public void setCustomerPostcode(String customerPostcode) {
		CustomerPostcode = customerPostcode;
	}

	public String getCustomerState() {
		return CustomerState;
	}

	public void setCustomerState(String customerState) {
		CustomerState = customerState;
	}

	public String getCustomerCountry() {
		return CustomerCountry;
	}

	public void setCustomerCountry(String customerCountry) {
		CustomerCountry = customerCountry;
	}

	public String getCustomerMobileNo() {
		return CustomerMobileNo;
	}

	public void setCustomerMobileNo(String customerMobileNo) {
		CustomerMobileNo = customerMobileNo;
	}

	public String getDSPQQID() {
		return DSPQQID;
	}

	public void setDSPQQID(String dSPQQID) {
		DSPQQID = dSPQQID;
	}

	public String getPaymentChannel() {
		return PaymentChannel;
	}

	public void setPaymentChannel(String paymentChannel) {
		PaymentChannel = paymentChannel;
	}

	public String getInvoiceNo() {
		return InvoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		InvoiceNo = invoiceNo;
	}

	public String getProductCode() {
		return ProductCode;
	}

	public void setProductCode(String productCode) {
		ProductCode = productCode;
	}

	public String getTerm() {
		return Term;
	}

	public void setTerm(String term) {
		Term = term;
	}

	public String getCoverage() {
		return Coverage;
	}

	public void setCoverage(String coverage) {
		Coverage = coverage;
	}

	public String getMode() {
		return Mode;
	}

	public void setMode(String mode) {
		Mode = mode;
	}

	public String getPaymentAuthCode() {
		return PaymentAuthCode;
	}

	public void setPaymentAuthCode(String paymentAuthCode) {
		PaymentAuthCode = paymentAuthCode;
	}

	public String getTotalAmount() {
		return TotalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		TotalAmount = totalAmount;
	}

	public String getPremiumMode() {
		return PremiumMode;
	}

	public void setPremiumMode(String premiumMode) {
		PremiumMode = premiumMode;
	}

	public String getMPAY_RefNo() {
		return MPAY_RefNo;
	}

	public void setMPAY_RefNo(String mPAY_RefNo) {
		MPAY_RefNo = mPAY_RefNo;
	}

	public String getMPAY_AuthCode() {
		return MPAY_AuthCode;
	}

	public void setMPAY_AuthCode(String mPAY_AuthCode) {
		MPAY_AuthCode = mPAY_AuthCode;
	}

	public String getEBPG_RefNo() {
		return EBPG_RefNo;
	}

	public void setEBPG_RefNo(String eBPG_RefNo) {
		EBPG_RefNo = eBPG_RefNo;
	}

	public String getEBPG_AuthCode() {
		return EBPG_AuthCode;
	}

	public void setEBPG_AuthCode(String eBPG_AuthCode) {
		EBPG_AuthCode = eBPG_AuthCode;
	}

	public String getM2U_RefNo() {
		return M2U_RefNo;
	}

	public void setM2U_RefNo(String m2u_RefNo) {
		M2U_RefNo = m2u_RefNo;
	}

	public String getM2U_AuthCode() {
		return M2U_AuthCode;
	}

	public void setM2U_AuthCode(String m2u_AuthCode) {
		M2U_AuthCode = m2u_AuthCode;
	}

	public String getFPX_RefNo() {
		return FPX_RefNo;
	}

	public void setFPX_RefNo(String fPX_RefNo) {
		FPX_RefNo = fPX_RefNo;
	}

	public String getFPX_AuthCode() {
		return FPX_AuthCode;
	}

	public void setFPX_AuthCode(String fPX_AuthCode) {
		FPX_AuthCode = fPX_AuthCode;
	}

	public int getRecordCount() {
		return RecordCount;
	}

	public void setRecordCount(int recordCount) {
		RecordCount = recordCount;
	}

	public String getVehicleNo() {
		return VehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		VehicleNo = vehicleNo;
	}

	public String getAuditRemarks() {
		return AuditRemarks;
	}

	public void setAuditRemarks(String auditRemarks) {
		AuditRemarks = auditRemarks;
	}

	public String getVehicleLoc() {
		return VehicleLoc;
	}

	public void setVehicleLoc(String vehicleLoc) {
		VehicleLoc = vehicleLoc;
	}

	public String getVehicleMake() {
		return VehicleMake;
	}

	public void setVehicleMake(String vehicleMake) {
		VehicleMake = vehicleMake;
	}

	public String getVehicleModel() {
		return VehicleModel;
	}

	public void setVehicleModel(String vehicleModel) {
		VehicleModel = vehicleModel;
	}

	public String getYearMake() {
		return YearMake;
	}

	public void setYearMake(String yearMake) {
		YearMake = yearMake;
	}

	public String getChassisNo() {
		return ChassisNo;
	}

	public void setChassisNo(String chassisNo) {
		ChassisNo = chassisNo;
	}

	public String getFinancedBy() {
		return FinancedBy;
	}

	public void setFinancedBy(String financedBy) {
		FinancedBy = financedBy;
	}

	public String getCoverageStartDate() {
		return CoverageStartDate;
	}

	public void setCoverageStartDate(String coverageStartDate) {
		CoverageStartDate = coverageStartDate;
	}

	public String getCoverageEndDate() {
		return CoverageEndDate;
	}

	public void setCoverageEndDate(String coverageEndDate) {
		CoverageEndDate = coverageEndDate;
	}

	public String getEngineNo() {
		return EngineNo;
	}

	public void setEngineNo(String engineNo) {
		EngineNo = engineNo;
	}

	public String getNVIC() {
		return NVIC;
	}

	public void setNVIC(String nVIC) {
		NVIC = nVIC;
	}

	public String getDoneStep1() {
		return DoneStep1;
	}

	public void setDoneStep1(String doneStep1) {
		DoneStep1 = doneStep1;
	}

	public String getDoneStep2() {
		return DoneStep2;
	}

	public void setDoneStep2(String doneStep2) {
		DoneStep2 = doneStep2;
	}

	public String getTLCheckDigit() {
		return TLCheckDigit;
	}

	public void setTLCheckDigit(String tLCheckDigit) {
		TLCheckDigit = tLCheckDigit;
	}

	public String getUWReason() {
		return UWReason;
	}

	public void setUWReason(String uWReason) {
		UWReason = uWReason;
	}

	public String getQQID() {
		return QQID;
	}

	public void setQQID(String qQID) {
		QQID = qQID;
	}

	public String getDriverPAPolicyNo() {
		return DriverPAPolicyNo;
	}

	public void setDriverPAPolicyNo(String driverPAPolicyNo) {
		DriverPAPolicyNo = driverPAPolicyNo;
	}

	public String getJPJStatus() {
		return JPJStatus;
	}

	public void setJPJStatus(String jPJStatus) {
		JPJStatus = jPJStatus;
	}

	public String getCAPSPolicyNo() {
		return CAPSPolicyNo;
	}

	public void setCAPSPolicyNo(String cAPSPolicyNo) {
		CAPSPolicyNo = cAPSPolicyNo;
	}

	public String getHomeType() {
		return HomeType;
	}

	public void setHomeType(String homeType) {
		HomeType = homeType;
	}

	public String getBuildConstructionType() {
		return BuildConstructionType;
	}

	public void setBuildConstructionType(String buildConstructionType) {
		BuildConstructionType = buildConstructionType;
	}

	public String getHomeSumInsured() {
		return HomeSumInsured;
	}

	public void setHomeSumInsured(String homeSumInsured) {
		HomeSumInsured = homeSumInsured;
	}

	public String getAddBenRiotStrike() {
		return AddBenRiotStrike;
	}

	public void setAddBenRiotStrike(String addBenRiotStrike) {
		AddBenRiotStrike = addBenRiotStrike;
	}

	public String getAddBenExtendedTheft() {
		return AddBenExtendedTheft;
	}

	public void setAddBenExtendedTheft(String addBenExtendedTheft) {
		AddBenExtendedTheft = addBenExtendedTheft;
	}

	public String getAddBenRiotStrikeAmt() {
		return AddBenRiotStrikeAmt;
	}

	public void setAddBenRiotStrikeAmt(String addBenRiotStrikeAmt) {
		AddBenRiotStrikeAmt = addBenRiotStrikeAmt;
	}

	public String getAddBenExtendedTheftAmt() {
		return AddBenExtendedTheftAmt;
	}

	public void setAddBenExtendedTheftAmt(String addBenExtendedTheftAmt) {
		AddBenExtendedTheftAmt = addBenExtendedTheftAmt;
	}

	public String getContentSumInsured() {
		return ContentSumInsured;
	}

	public void setContentSumInsured(String contentSumInsured) {
		ContentSumInsured = contentSumInsured;
	}

	public String getAdditionalBenefitCode() {
		return AdditionalBenefitCode;
	}

	public void setAdditionalBenefitCode(String additionalBenefitCode) {
		AdditionalBenefitCode = additionalBenefitCode;
	}

	public String getAdditionalBenefitValue() {
		return AdditionalBenefitValue;
	}

	public void setAdditionalBenefitValue(String additionalBenefitValue) {
		AdditionalBenefitValue = additionalBenefitValue;
	}

	public String getAdditionalBenefitText() {
		return AdditionalBenefitText;
	}

	public void setAdditionalBenefitText(String additionalBenefitText) {
		AdditionalBenefitText = additionalBenefitText;
	}

	public String getTravelStartDate() {
		return TravelStartDate;
	}

	public void setTravelStartDate(String travelStartDate) {
		TravelStartDate = travelStartDate;
	}

	public String getTravelEndDate() {
		return TravelEndDate;
	}

	public void setTravelEndDate(String travelEndDate) {
		TravelEndDate = travelEndDate;
	}

	public String getOfferedPlanName() {
		return OfferedPlanName;
	}

	public void setOfferedPlanName(String offeredPlanName) {
		OfferedPlanName = offeredPlanName;
	}

	public String getTravelAreaType() {
		return TravelAreaType;
	}

	public void setTravelAreaType(String travelAreaType) {
		TravelAreaType = travelAreaType;
	}

	public String getTravellingWith() {
		return TravellingWith;
	}

	public void setTravellingWith(String travellingWith) {
		TravellingWith = travellingWith;
	}

	public long getTravelDuration() {
		return TravelDuration;
	}

	public void setTravelDuration(long diffDays) {
		TravelDuration = diffDays;
	}

	public String getSalesEntity() {
		return salesEntity;
	}

	public void setSalesEntity(String salesEntity) {
		this.salesEntity = salesEntity;
	}

	public String getMarketerName() {
		return marketerName;
	}

	public void setMarketerName(String marketerName) {
		this.marketerName = marketerName;
	}

	public String getMarketerCode() {
		return marketerCode;
	}

	public void setMarketerCode(String marketerCode) {
		this.marketerCode = marketerCode;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getPlateNo() {
		return PlateNo;
	}

	public void setPlateNo(String plateNo) {
		PlateNo = plateNo;
	}

	public String getPreviousInsurance() {
		return PreviousInsurance;
	}

	public void setPreviousInsurance(String previousInsurance) {
		PreviousInsurance = previousInsurance;
	}

	public Date getRecord_date() {
		return record_date;
	}

	public void setRecord_date(Date record_date) {
		this.record_date = record_date;
	}

	public String getTransaction_datetime() {
		return transaction_datetime;
	}

	public void setTransaction_datetime(String transaction_datetime) {
		this.transaction_datetime = transaction_datetime;
	}

	public String getPaymentTrxID() {
		return paymentTrxID;
	}

	public void setPaymentTrxID(String paymentTrxID) {
		this.paymentTrxID = paymentTrxID;
	}

	public String getPmnt_status() {
		return pmnt_status;
	}

	public void setPmnt_status(String pmnt_status) {
		this.pmnt_status = pmnt_status;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getMotor_net_premium() {
		return motor_net_premium;
	}

	public void setMotor_net_premium(String motor_net_premium) {
		this.motor_net_premium = motor_net_premium;
	}

	public String getPassenger_pa_premium_payable() {
		return passenger_pa_premium_payable;
	}

	public void setPassenger_pa_premium_payable(String passenger_pa_premium_payable) {
		this.passenger_pa_premium_payable = passenger_pa_premium_payable;
	}

	public String getGrosspremium_final() {
		return grosspremium_final;
	}

	public void setGrosspremium_final(String grosspremium_final) {
		this.grosspremium_final = grosspremium_final;
	}

	public String getGst() {
		return gst;
	}

	public void setGst(String gst) {
		this.gst = gst;
	}

	public String getInvoice_no() {
		return invoice_no;
	}

	public void setInvoice_no(String invoice_no) {
		this.invoice_no = invoice_no;
	}

	public String getPolicy_number() {
		return policy_number;
	}

	public void setPolicy_number(String policy_number) {
		this.policy_number = policy_number;
	}

	public String getCaps_dppa() {
		return caps_dppa;
	}

	public void setCaps_dppa(String caps_dppa) {
		this.caps_dppa = caps_dppa;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getCustomer_nric_id() {
		return customer_nric_id;
	}

	public void setCustomer_nric_id(String customer_nric_id) {
		this.customer_nric_id = customer_nric_id;
	}

	public String getRegistration_number() {
		return registration_number;
	}

	public void setRegistration_number(String registration_number) {
		this.registration_number = registration_number;
	}

	public String getPmnt_gateway_code() {
		return pmnt_gateway_code;
	}

	public void setPmnt_gateway_code(String pmnt_gateway_code) {
		this.pmnt_gateway_code = pmnt_gateway_code;
	}

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public String getAuth_id() {
		return auth_id;
	}

	public void setAuth_id(String auth_id) {
		this.auth_id = auth_id;
	}

	public String getBankrefno() {
		return bankrefno;
	}

	public void setBankrefno(String bankrefno) {
		this.bankrefno = bankrefno;
	}

	public String getApprovalcode() {
		return approvalcode;
	}

	public void setApprovalcode(String approvalcode) {
		this.approvalcode = approvalcode;
	}

	public String getFpx_fpxtxnid() {
		return fpx_fpxtxnid;
	}

	public void setFpx_fpxtxnid(String fpx_fpxtxnid) {
		this.fpx_fpxtxnid = fpx_fpxtxnid;
	}

	public String getFpx_debitauthcode() {
		return fpx_debitauthcode;
	}

	public void setFpx_debitauthcode(String fpx_debitauthcode) {
		this.fpx_debitauthcode = fpx_debitauthcode;
	}

	public String getProduct_code() {
		return product_code;
	}

	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}

	public String getAgent_code() {
		return agent_code;
	}

	public void setAgent_code(String agent_code) {
		this.agent_code = agent_code;
	}

	public String getAgent_name() {
		return agent_name;
	}

	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}

	public String getQuotation_status() {
		return quotation_status;
	}

	public void setQuotation_status(String quotation_status) {
		this.quotation_status = quotation_status;
	}

	public String getLast_page() {
		return last_page;
	}

	public void setLast_page(String last_page) {
		this.last_page = last_page;
	}

	public String getTrxID() {
		return trxID;
	}

	public void setTrxID(String trxID) {
		this.trxID = trxID;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getDeliveryName() {
		return deliveryName;
	}

	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}

	public String getRoadTaxAmount() {
		return roadTaxAmount;
	}

	public void setRoadTaxAmount(String roadTaxAmount) {
		this.roadTaxAmount = roadTaxAmount;
	}

	public String getRtpf() {
		return rtpf;
	}

	public void setRtpf(String rtpf) {
		this.rtpf = rtpf;
	}

	public String getRtpfgst() {
		return rtpfgst;
	}

	public void setRtpfgst(String rtpfgst) {
		this.rtpfgst = rtpfgst;
	}

	public String getRtprf() {
		return rtprf;
	}

	public void setRtprf(String rtprf) {
		this.rtprf = rtprf;
	}

	public String getRtprfgst() {
		return rtprfgst;
	}

	public void setRtprfgst(String rtprfgst) {
		this.rtprfgst = rtprfgst;
	}

	public String getRtdf() {
		return rtdf;
	}

	public void setRtdf(String rtdf) {
		this.rtdf = rtdf;
	}

	public String getRtdfgst() {
		return rtdfgst;
	}

	public void setRtdfgst(String rtdfgst) {
		this.rtdfgst = rtdfgst;
	}

	public String getRttotalpayable() {
		return rttotalpayable;
	}

	public void setRttotalpayable(String rttotalpayable) {
		this.rttotalpayable = rttotalpayable;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getRtstate() {
		return rtstate;
	}

	public void setRtstate(String rtstate) {
		this.rtstate = rtstate;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPrintStatus() {
		return printStatus;
	}

	public void setPrintStatus(String printStatus) {
		this.printStatus = printStatus;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getConsignmentNo() {
		return consignmentNo;
	}

	public void setConsignmentNo(String consignmentNo) {
		this.consignmentNo = consignmentNo;
	}

	public String getTrackingNo() {
		return trackingNo;
	}

	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getRefundStatus() {
		return refundStatus;
	}

	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}

	public String getRdtaxInvoice() {
		return rdtaxInvoice;
	}

	public void setRdtaxInvoice(String rdtaxInvoice) {
		this.rdtaxInvoice = rdtaxInvoice;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSst_percentage() {
		return sst_percentage;
	}

	public void setSst_percentage(String sst_percentage) {
		this.sst_percentage = sst_percentage;
	}

	public String getSst_amount() {
		return sst_amount;
	}

	public void setSst_amount(String sst_amount) {
		this.sst_amount = sst_amount;
	}

	public String getPfNumber() {
		return pfNumber;
	}

	public void setPfNumber(String pfNumber) {
		this.pfNumber = pfNumber;

	}

	public String getRecurringTerm() {
		return recurringTerm;
	}

	public void setRecurringTerm(String recurringTerm) {
		this.recurringTerm = recurringTerm;
	}

	public String getRecurringCardType() {
		return recurringCardType;
	}

	public void setRecurringCardType(String recurringCardType) {
		this.recurringCardType = recurringCardType;
	}
	
	public String getLouPremiumDesc() {
		return louPremiumDesc;
	}

	public void setLouPremiumDesc(String louPremiumDesc) {
		this.louPremiumDesc = louPremiumDesc;
	}

	private String louPremiumDesc;

}
