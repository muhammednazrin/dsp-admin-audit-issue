package com.spring.VO.report;

import java.math.BigDecimal;
import java.util.Date;

public class MotorReportVO {

	private Date record_date;
	private String transaction_datetime;
	private String paymentTrxID;
	private String pmnt_status;
	private String amount;
	private String motor_net_premium;
	private String passenger_pa_premium_payable;
	private String grosspremium_final;
	private String discount;
	private String gst;
	private String invoice_no;
	private String policy_number;
	private String caps_dppa;
	private String customer_name;
	private String customer_nric_id;
	private String registration_number;
	private String pmnt_gateway_code;
	private String transaction_id;
	private String auth_id;
	private String bankrefno;
	private String approvalcode;
	private String fpx_fpxtxnid;
	private String fpx_debitauthcode;
	private String product_code;
	private String agent_code;
	private String agent_name;

	private String quotation_status;
	private String last_page;
	private String reason;

	private String trxID;
	private String paramName;
	private String paramValue;

	private BigDecimal total_amount;
	private BigDecimal total_gross_amount;
	private BigDecimal total_net_amount;

	private BigDecimal total_premium_after_gst_stamp_duty;
	private BigDecimal total_driver_pa_amount;

	// Txn Report
	private String deliveryName;
	private String roadTaxAmount;
	private String rtpf;
	private String rtpfgst;
	private String rtprf;
	private String rtprfgst;
	private String rtdf;
	private String rtdfgst;
	private String rttotalpayable;
	private String period;
	private String address1;
	private String address2;
	private String address3;
	private String postcode;
	private String rtstate;
	private String mobile;
	private String printStatus;
	private String remarks;
	private String consignmentNo;
	private String trackingNo;
	private String bankName;
	private String accountNo;
	private String refundStatus;
	private String rdtaxInvoice;
	private String message;
	private String modeStatus;
	private String REFNUM;
	private String recurringTerm;
	private String recurringCardType;

	public String getREFNUM() {
		return REFNUM;
	}

	public void setREFNUM(String rEFNUM) {
		REFNUM = rEFNUM;
	}

	public String getModeStatus() {
		return modeStatus;
	}

	public void setModeStatus(String modeStatus) {
		this.modeStatus = modeStatus;
	}

	public Date getRecord_date() {
		return record_date;
	}

	public void setRecord_date(Date record_date) {
		this.record_date = record_date;
	}

	public String getTransaction_datetime() {
		return transaction_datetime;
	}

	public void setTransaction_datetime(String transaction_datetime) {
		this.transaction_datetime = transaction_datetime;
	}

	public String getPaymentTrxID() {
		return paymentTrxID;
	}

	public void setPaymentTrxID(String paymentTrxID) {
		this.paymentTrxID = paymentTrxID;
	}

	public String getPmnt_status() {
		return pmnt_status;
	}

	public void setPmnt_status(String pmnt_status) {
		this.pmnt_status = pmnt_status;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getMotor_net_premium() {
		return motor_net_premium;
	}

	public void setMotor_net_premium(String motor_net_premium) {
		this.motor_net_premium = motor_net_premium;
	}

	public String getPassenger_pa_premium_payable() {
		return passenger_pa_premium_payable;
	}

	public void setPassenger_pa_premium_payable(String passenger_pa_premium_payable) {
		this.passenger_pa_premium_payable = passenger_pa_premium_payable;
	}

	public String getGrosspremium_final() {
		return grosspremium_final;
	}

	public void setGrosspremium_final(String grosspremium_final) {
		this.grosspremium_final = grosspremium_final;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getGst() {
		return gst;
	}

	public void setGst(String gst) {
		this.gst = gst;
	}

	public String getInvoice_no() {
		return invoice_no;
	}

	public void setInvoice_no(String invoice_no) {
		this.invoice_no = invoice_no;
	}

	public String getPolicy_number() {
		return policy_number;
	}

	public void setPolicy_number(String policy_number) {
		this.policy_number = policy_number;
	}

	public String getCaps_dppa() {
		return caps_dppa;
	}

	public void setCaps_dppa(String caps_dppa) {
		this.caps_dppa = caps_dppa;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getCustomer_nric_id() {
		return customer_nric_id;
	}

	public void setCustomer_nric_id(String customer_nric_id) {
		this.customer_nric_id = customer_nric_id;
	}

	public String getRegistration_number() {
		return registration_number;
	}

	public void setRegistration_number(String registration_number) {
		this.registration_number = registration_number;
	}

	public String getPmnt_gateway_code() {
		return pmnt_gateway_code;
	}

	public void setPmnt_gateway_code(String pmnt_gateway_code) {
		this.pmnt_gateway_code = pmnt_gateway_code;
	}

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public String getAuth_id() {
		return auth_id;
	}

	public void setAuth_id(String auth_id) {
		this.auth_id = auth_id;
	}

	public String getBankrefno() {
		return bankrefno;
	}

	public void setBankrefno(String bankrefno) {
		this.bankrefno = bankrefno;
	}

	public String getApprovalcode() {
		return approvalcode;
	}

	public void setApprovalcode(String approvalcode) {
		this.approvalcode = approvalcode;
	}

	public String getFpx_fpxtxnid() {
		return fpx_fpxtxnid;
	}

	public void setFpx_fpxtxnid(String fpx_fpxtxnid) {
		this.fpx_fpxtxnid = fpx_fpxtxnid;
	}

	public String getFpx_debitauthcode() {
		return fpx_debitauthcode;
	}

	public void setFpx_debitauthcode(String fpx_debitauthcode) {
		this.fpx_debitauthcode = fpx_debitauthcode;
	}

	public String getProduct_code() {
		return product_code;
	}

	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}

	public String getAgent_code() {
		return agent_code;
	}

	public void setAgent_code(String agent_code) {
		this.agent_code = agent_code;
	}

	public String getAgent_name() {
		return agent_name;
	}

	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}

	public String getQuotation_status() {
		return quotation_status;
	}

	public void setQuotation_status(String quotation_status) {
		this.quotation_status = quotation_status;
	}

	public String getLast_page() {
		return last_page;
	}

	public void setLast_page(String last_page) {
		this.last_page = last_page;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getTrxID() {
		return trxID;
	}

	public void setTrxID(String trxID) {
		this.trxID = trxID;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public BigDecimal getTotal_amount() {
		return total_amount;
	}

	public void setTotal_amount(BigDecimal total_amount) {
		this.total_amount = total_amount;
	}

	public BigDecimal getTotal_gross_amount() {
		return total_gross_amount;
	}

	public void setTotal_gross_amount(BigDecimal total_gross_amount) {
		this.total_gross_amount = total_gross_amount;
	}

	public BigDecimal getTotal_net_amount() {
		return total_net_amount;
	}

	public void setTotal_net_amount(BigDecimal total_net_amount) {
		this.total_net_amount = total_net_amount;
	}

	public BigDecimal getTotal_premium_after_gst_stamp_duty() {
		return total_premium_after_gst_stamp_duty;
	}

	public void setTotal_premium_after_gst_stamp_duty(BigDecimal total_premium_after_gst_stamp_duty) {
		this.total_premium_after_gst_stamp_duty = total_premium_after_gst_stamp_duty;
	}

	public BigDecimal getTotal_driver_pa_amount() {
		return total_driver_pa_amount;
	}

	public void setTotal_driver_pa_amount(BigDecimal total_driver_pa_amount) {
		this.total_driver_pa_amount = total_driver_pa_amount;
	}

	public String getDeliveryName() {
		return deliveryName;
	}

	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}

	public String getRoadTaxAmount() {
		return roadTaxAmount;
	}

	public void setRoadTaxAmount(String roadTaxAmount) {
		this.roadTaxAmount = roadTaxAmount;
	}

	public String getRtpf() {
		return rtpf;
	}

	public void setRtpf(String rtpf) {
		this.rtpf = rtpf;
	}

	public String getRtpfgst() {
		return rtpfgst;
	}

	public void setRtpfgst(String rtpfgst) {
		this.rtpfgst = rtpfgst;
	}

	public String getRtprf() {
		return rtprf;
	}

	public void setRtprf(String rtprf) {
		this.rtprf = rtprf;
	}

	public String getRtprfgst() {
		return rtprfgst;
	}

	public void setRtprfgst(String rtprfgst) {
		this.rtprfgst = rtprfgst;
	}

	public String getRtdf() {
		return rtdf;
	}

	public void setRtdf(String rtdf) {
		this.rtdf = rtdf;
	}

	public String getRtdfgst() {
		return rtdfgst;
	}

	public void setRtdfgst(String rtdfgst) {
		this.rtdfgst = rtdfgst;
	}

	public String getRttotalpayable() {
		return rttotalpayable;
	}

	public void setRttotalpayable(String rttotalpayable) {
		this.rttotalpayable = rttotalpayable;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getRtstate() {
		return rtstate;
	}

	public void setRtstate(String rtstate) {
		this.rtstate = rtstate;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPrintStatus() {
		return printStatus;
	}

	public void setPrintStatus(String printStatus) {
		this.printStatus = printStatus;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getConsignmentNo() {
		return consignmentNo;
	}

	public void setConsignmentNo(String consignmentNo) {
		this.consignmentNo = consignmentNo;
	}

	public String getTrackingNo() {
		return trackingNo;
	}

	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getRefundStatus() {
		return refundStatus;
	}

	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}

	public String getRdtaxInvoice() {
		return rdtaxInvoice;
	}

	public void setRdtaxInvoice(String rdtaxInvoice) {
		this.rdtaxInvoice = rdtaxInvoice;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getRecurringTerm() {
		return recurringTerm;
	}

	public void setRecurringTerm(String recurringTerm) {
		this.recurringTerm = recurringTerm;
	}

	public String getRecurringCardType() {
		return recurringCardType;
	}

	public void setRecurringCardType(String recurringCardType) {
		this.recurringCardType = recurringCardType;
	}

}
