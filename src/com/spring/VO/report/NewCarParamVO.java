package com.spring.VO.report;

public class NewCarParamVO {

	private String transactionId;
	private String policy;
	private String poiPolicyNo;
	private String invoiceNo;

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getPolicy() {
		return policy;
	}

	public void setPolicy(String policy) {
		this.policy = policy;
	}

	public String getPoiPolicyNo() {
		return poiPolicyNo;
	}

	public void setPoiPolicyNo(String poiPolicyNo) {
		this.poiPolicyNo = poiPolicyNo;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

}
