package com.spring.VO.report;

import java.util.Date;

public class TravelEzyReportExcelVO {
	private Date record_date;
	private String transaction_datetime;
	private String paymentTrxID;
	private String pmnt_status;
	private String quotation_status;
	private String amount;
	private String grosspremium_final;
	private String discount;
	private String taxType;
	private String gst;
	private String nett_premium;
	private String invoice_no;
	private String policy_number;
	private String customer_name;
	private String customer_nric_id;
	private String pmnt_gateway_code;
	private String transaction_id;
	private String auth_id;
	private String bankrefno;
	private String approvalcode;
	private String fpx_fpxtxnid;
	private String fpx_debitauthcode;
	private String product_code;
	private String agent_code;
	private String agent_name;

	private String trip_type;
	private String d_flight_dt;
	private String r_flight_dt;
	private int number_of_companion;
	private String last_page;

	private String trxID;

	private String PDPA;

	private String companion_name;
	private String id_no;

	private String DSPQQID;

	private String customer_ID_type;
	private String customer_gender;
	private String customer_email;
	private String customer_address_1;
	private String customer_address_2;
	private String customer_address_3;
	private String customer_state;
	private String customer_postcode;
	private String customer_mobile_no;

	private String coverage_start_date;
	private String coverage_end_date;

	private String commission;
	private String total_premium_payable;

	public String getTotal_premium_payable() {
		return total_premium_payable;
	}

	public void setTotal_premium_payable(String total_premium_payable) {
		this.total_premium_payable = total_premium_payable;
	}

	public String getCommission() {
		return commission;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}

	public String getCustomer_ID_type() {
		return customer_ID_type;
	}

	public void setCustomer_ID_type(String customer_ID_type) {
		this.customer_ID_type = customer_ID_type;
	}

	public String getCustomer_gender() {
		return customer_gender;
	}

	public void setCustomer_gender(String customer_gender) {
		this.customer_gender = customer_gender;
	}

	public String getCustomer_email() {
		return customer_email;
	}

	public void setCustomer_email(String customer_email) {
		this.customer_email = customer_email;
	}

	public String getCustomer_address_1() {
		return customer_address_1;
	}

	public void setCustomer_address_1(String customer_address_1) {
		this.customer_address_1 = customer_address_1;
	}

	public String getCustomer_address_2() {
		return customer_address_2;
	}

	public void setCustomer_address_2(String customer_address_2) {
		this.customer_address_2 = customer_address_2;
	}

	public String getCustomer_address_3() {
		return customer_address_3;
	}

	public void setCustomer_address_3(String customer_address_3) {
		this.customer_address_3 = customer_address_3;
	}

	public String getCustomer_state() {
		return customer_state;
	}

	public void setCustomer_state(String customer_state) {
		this.customer_state = customer_state;
	}

	public String getCustomer_postcode() {
		return customer_postcode;
	}

	public void setCustomer_postcode(String customer_postcode) {
		this.customer_postcode = customer_postcode;
	}

	public String getCustomer_mobile_no() {
		return customer_mobile_no;
	}

	public void setCustomer_mobile_no(String customer_mobile_no) {
		this.customer_mobile_no = customer_mobile_no;
	}

	public String getCoverage_start_date() {
		return coverage_start_date;
	}

	public void setCoverage_start_date(String coverage_start_date) {
		this.coverage_start_date = coverage_start_date;
	}

	public String getCoverage_end_date() {
		return coverage_end_date;
	}

	public void setCoverage_end_date(String coverage_end_date) {
		this.coverage_end_date = coverage_end_date;
	}

	public String getDSPQQID() {
		return DSPQQID;
	}

	public void setDSPQQID(String dSPQQID) {
		DSPQQID = dSPQQID;
	}

	public String getPDPA() {
		return PDPA;
	}

	public void setPDPA(String pDPA) {
		PDPA = pDPA;
	}

	public String getTrip_type() {
		return trip_type;
	}

	public void setTrip_type(String trip_type) {
		this.trip_type = trip_type;
	}

	public String getDepartDate() {
		return d_flight_dt;
	}

	public void setDepartDate(String d_flight_dt) {
		this.d_flight_dt = d_flight_dt;
	}

	public String getReturnDate() {
		return r_flight_dt;
	}

	public void setReturnDate(String r_flight_dt) {
		this.r_flight_dt = r_flight_dt;
	}

	public Date getRecord_date() {
		return record_date;
	}

	public void setRecord_date(Date record_date) {
		this.record_date = record_date;
	}

	public String getTransaction_datetime() {
		return transaction_datetime;
	}

	public void setTransaction_datetime(String transaction_datetime) {
		this.transaction_datetime = transaction_datetime;
	}

	public String getPaymentTrxID() {
		return paymentTrxID;
	}

	public void setPaymentTrxID(String paymentTrxID) {
		this.paymentTrxID = paymentTrxID;
	}

	public String getPmnt_status() {
		return pmnt_status;
	}

	public void setPmnt_status(String pmnt_status) {
		this.pmnt_status = pmnt_status;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getGrosspremium_final() {
		return grosspremium_final;
	}

	public void setGrosspremium_final(String grosspremium_final) {
		this.grosspremium_final = grosspremium_final;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getTaxType() {
		return taxType;
	}

	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}

	public String getGst() {
		return gst;
	}

	public void setGst(String gst) {
		this.gst = gst;
	}

	public String getInvoice_no() {
		return invoice_no;
	}

	public void setInvoice_no(String invoice_no) {
		this.invoice_no = invoice_no;
	}

	public String getPolicy_number() {
		return policy_number;
	}

	public void setPolicy_number(String policy_number) {
		this.policy_number = policy_number;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getCustomer_nric_id() {
		return customer_nric_id;
	}

	public void setCustomer_nric_id(String customer_nric_id) {
		this.customer_nric_id = customer_nric_id;
	}

	public String getPmnt_gateway_code() {
		return pmnt_gateway_code;
	}

	public void setPmnt_gateway_code(String pmnt_gateway_code) {
		this.pmnt_gateway_code = pmnt_gateway_code;
	}

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public String getAuth_id() {
		return auth_id;
	}

	public void setAuth_id(String auth_id) {
		this.auth_id = auth_id;
	}

	public String getBankrefno() {
		return bankrefno;
	}

	public void setBankrefno(String bankrefno) {
		this.bankrefno = bankrefno;
	}

	public String getApprovalcode() {
		return approvalcode;
	}

	public void setApprovalcode(String approvalcode) {
		this.approvalcode = approvalcode;
	}

	public String getFpx_fpxtxnid() {
		return fpx_fpxtxnid;
	}

	public void setFpx_fpxtxnid(String fpx_fpxtxnid) {
		this.fpx_fpxtxnid = fpx_fpxtxnid;
	}

	public String getFpx_debitauthcode() {
		return fpx_debitauthcode;
	}

	public void setFpx_debitauthcode(String fpx_debitauthcode) {
		this.fpx_debitauthcode = fpx_debitauthcode;
	}

	public String getProduct_code() {
		return product_code;
	}

	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}

	public String getAgent_code() {
		return agent_code;
	}

	public void setAgent_code(String agent_code) {
		this.agent_code = agent_code;
	}

	public String getAgent_name() {
		return agent_name;
	}

	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}

	public String getQuotation_status() {
		return quotation_status;
	}

	public void setQuotation_status(String quotation_status) {
		this.quotation_status = quotation_status;
	}

	public String getTrxID() {
		return trxID;
	}

	public void setTrxID(String trxID) {
		this.trxID = trxID;
	}

	public Integer getNumber_of_companion() {
		return number_of_companion;
	}

	public void setNumber_of_companion(int number_of_companion) {
		this.number_of_companion = number_of_companion;
	}

	public String getCompanion_name() {
		return companion_name;
	}

	public void setCompanion_name(String companion_name) {
		this.companion_name = companion_name;
	}

	public String getId_no() {
		return id_no;
	}

	public void setId_no(String id_no) {
		this.id_no = id_no;
	}

	public String getNettPremium() {
		return nett_premium;
	}

	public void setNettPremium(String nett_premium) {
		this.nett_premium = nett_premium;
	}

	public String getLast_page() {
		return last_page;
	}

	public void setLast_page(String last_page) {
		this.last_page = last_page;
	}

}
