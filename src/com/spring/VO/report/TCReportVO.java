package com.spring.VO.report;

import java.math.BigDecimal;
import java.util.Date;

public class TCReportVO {
	private Date record_date;
	private String transaction_datetime;
	private String paymentTrxID;
	private String pmnt_status;
	private String amount;
	private String grosspremium_final;
	private String discount;
	private String gst;
	private String invoice_no;
	private String policy_number;
	private String customer_name;
	private String customer_nric_id;
	private String pmnt_gateway_code;
	private String transaction_id;
	private String auth_id;
	private String bankrefno;
	private String approvalcode;
	private String fpx_fpxtxnid;
	private String fpx_debitauthcode;
	private String product_code;
	private String agent_code;
	private String agent_name;

	private String quotation_status;
	private String last_page;
	private String reason;

	private String trxID;
	private String paramName;
	private String paramValue;

	private String travel_start_date;
	private String travel_end_date;
	private String offered_plan_name;
	private String travel_area_type;
	private String travlling_with;
	private String travel_duration;

	private String adv_activities;
	private BigDecimal total_amount;
	private BigDecimal total_gross_amount;
	private BigDecimal total_net_amount;

	private BigDecimal total_premium_after_gst_stamp_duty;
	private String modeStatus;

	public String getModeStatus() {
		return modeStatus;
	}

	public void setModeStatus(String modeStatus) {
		this.modeStatus = modeStatus;
	}

	public String getTravel_start_date() {
		return travel_start_date;
	}

	public void setTravel_start_date(String travel_start_date) {
		this.travel_start_date = travel_start_date;
	}

	public String getTravel_end_date() {
		return travel_end_date;
	}

	public void setTravel_end_date(String travel_end_date) {
		this.travel_end_date = travel_end_date;
	}

	public String getTravel_duration() {
		return travel_duration;
	}

	public void setTravel_duration(String travel_duration) {
		this.travel_duration = travel_duration;
	}

	public String getOffered_plan_name() {
		return offered_plan_name;
	}

	public void setOffered_plan_name(String offered_plan_name) {
		this.offered_plan_name = offered_plan_name;
	}

	public String getTravel_area_type() {
		return travel_area_type;
	}

	public void setTravel_area_type(String travel_area_type) {
		this.travel_area_type = travel_area_type;
	}

	public String getTravlling_with() {
		return travlling_with;
	}

	public void setTravlling_with(String travlling_with) {
		this.travlling_with = travlling_with;
	}

	public Date getRecord_date() {
		return record_date;
	}

	public void setRecord_date(Date record_date) {
		this.record_date = record_date;
	}

	public String getTransaction_datetime() {
		return transaction_datetime;
	}

	public void setTransaction_datetime(String transaction_datetime) {
		this.transaction_datetime = transaction_datetime;
	}

	public String getPaymentTrxID() {
		return paymentTrxID;
	}

	public void setPaymentTrxID(String paymentTrxID) {
		this.paymentTrxID = paymentTrxID;
	}

	public String getPmnt_status() {
		return pmnt_status;
	}

	public void setPmnt_status(String pmnt_status) {
		this.pmnt_status = pmnt_status;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getGrosspremium_final() {
		return grosspremium_final;
	}

	public void setGrosspremium_final(String grosspremium_final) {
		this.grosspremium_final = grosspremium_final;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getGst() {
		return gst;
	}

	public void setGst(String gst) {
		this.gst = gst;
	}

	public String getInvoice_no() {
		return invoice_no;
	}

	public void setInvoice_no(String invoice_no) {
		this.invoice_no = invoice_no;
	}

	public String getPolicy_number() {
		return policy_number;
	}

	public void setPolicy_number(String policy_number) {
		this.policy_number = policy_number;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getCustomer_nric_id() {
		return customer_nric_id;
	}

	public void setCustomer_nric_id(String customer_nric_id) {
		this.customer_nric_id = customer_nric_id;
	}

	public String getPmnt_gateway_code() {
		return pmnt_gateway_code;
	}

	public void setPmnt_gateway_code(String pmnt_gateway_code) {
		this.pmnt_gateway_code = pmnt_gateway_code;
	}

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public String getAuth_id() {
		return auth_id;
	}

	public void setAuth_id(String auth_id) {
		this.auth_id = auth_id;
	}

	public String getBankrefno() {
		return bankrefno;
	}

	public void setBankrefno(String bankrefno) {
		this.bankrefno = bankrefno;
	}

	public String getApprovalcode() {
		return approvalcode;
	}

	public void setApprovalcode(String approvalcode) {
		this.approvalcode = approvalcode;
	}

	public String getFpx_fpxtxnid() {
		return fpx_fpxtxnid;
	}

	public void setFpx_fpxtxnid(String fpx_fpxtxnid) {
		this.fpx_fpxtxnid = fpx_fpxtxnid;
	}

	public String getFpx_debitauthcode() {
		return fpx_debitauthcode;
	}

	public void setFpx_debitauthcode(String fpx_debitauthcode) {
		this.fpx_debitauthcode = fpx_debitauthcode;
	}

	public String getProduct_code() {
		return product_code;
	}

	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}

	public String getAgent_code() {
		return agent_code;
	}

	public void setAgent_code(String agent_code) {
		this.agent_code = agent_code;
	}

	public String getAgent_name() {
		return agent_name;
	}

	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}

	public String getQuotation_status() {
		return quotation_status;
	}

	public void setQuotation_status(String quotation_status) {
		this.quotation_status = quotation_status;
	}

	public String getLast_page() {
		return last_page;
	}

	public void setLast_page(String last_page) {
		this.last_page = last_page;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getTrxID() {
		return trxID;
	}

	public void setTrxID(String trxID) {
		this.trxID = trxID;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public BigDecimal getTotal_amount() {
		return total_amount;
	}

	public void setTotal_amount(BigDecimal total_amount) {
		this.total_amount = total_amount;
	}

	public BigDecimal getTotal_gross_amount() {
		return total_gross_amount;
	}

	public void setTotal_gross_amount(BigDecimal total_gross_amount) {
		this.total_gross_amount = total_gross_amount;
	}

	public BigDecimal getTotal_net_amount() {
		return total_net_amount;
	}

	public void setTotal_net_amount(BigDecimal total_net_amount) {
		this.total_net_amount = total_net_amount;
	}

	public BigDecimal getTotal_premium_after_gst_stamp_duty() {
		return total_premium_after_gst_stamp_duty;
	}

	public void setTotal_premium_after_gst_stamp_duty(BigDecimal total_premium_after_gst_stamp_duty) {
		this.total_premium_after_gst_stamp_duty = total_premium_after_gst_stamp_duty;
	}

	public String getAdv_activities() {
		return adv_activities;
	}

	public void setAdv_activities(String adv_activities) {
		this.adv_activities = adv_activities;
	}

}
