package com.spring.VO.report;

import java.util.Date;

public class WTCReportExcelVO {
	private Date record_date;
	private String transaction_datetime;
	private String paymentTrxID;
	private String pmnt_status;
	private String amount;
	private String grosspremium_final;
	private String discount;
	private String gst;
	private String invoice_no;
	private String policy_number;
	private String customer_name;
	private String customer_nric_id;
	private String pmnt_gateway_code;
	private String transaction_id;
	private String auth_id;
	private String bankrefno;
	private String approvalcode;
	private String fpx_fpxtxnid;
	private String fpx_debitauthcode;
	private String product_code;
	private String agent_code;
	private String agent_name;

	private String quotation_status;
	private String last_page;
	private String reason;

	private String trxID;
	private String paramName;
	private String paramValue;

	private String travel_start_date;
	private String travel_end_date;
	private String offered_plan_name;
	private String travel_area_type;
	private String travlling_with;
	private String travel_duration;

	private String PDPA;

	private String spouse_name;
	private String spouse_email;
	private String spouse_ID_type;
	private String spouse_ID_number;
	private String spouse_DOB;
	private String spouse_gender;

	private String child_name;
	private String child_ID_type;
	private String child_ID_number;
	private String child_DOB;
	private String child_gender;

	private String child_name_1;
	private String child_ID_type_1;
	private String child_ID_number_1;
	private String child_DOB_1;
	private String child_gender_1;

	private String child_name_2;
	private String child_ID_type_2;
	private String child_ID_number_2;
	private String child_DOB_2;
	private String child_gender_2;

	private String child_name_3;
	private String child_ID_type_3;
	private String child_ID_number_3;
	private String child_DOB_3;
	private String child_gender_3;

	private String child_name_4;
	private String child_ID_type_4;
	private String child_ID_number_4;
	private String child_DOB_4;
	private String child_gender_4;

	private String child_name_5;
	private String child_ID_type_5;
	private String child_ID_number_5;
	private String child_DOB_5;
	private String child_gender_5;

	private String child_name_6;
	private String child_ID_type_6;
	private String child_ID_number_6;
	private String child_DOB_6;
	private String child_gender_6;

	private String child_name_7;
	private String child_ID_type_7;
	private String child_ID_number_7;
	private String child_DOB_7;
	private String child_gender_7;

	private String area_code;

	private String DSPQQID;

	private String customer_ID_type;
	private String customer_gender;
	private String customer_email;
	private String customer_address_1;
	private String customer_address_2;
	private String customer_address_3;
	private String customer_state;
	private String customer_postcode;
	private String customer_mobile_no;
	private String operator_code;
	private String discount_code;
	private String coverage_start_date;
	private String coverage_end_date;

	private String commission;
	private String total_premium_payable;
	private String modeStatus;

	public String getModeStatus() {
		return modeStatus;
	}

	public void setModeStatus(String modeStatus) {
		this.modeStatus = modeStatus;
	}

	public String getCommission() {
		return commission;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}

	public String getTotal_premium_payable() {
		return total_premium_payable;
	}

	public void setTotal_premium_payable(String total_premium_payable) {
		this.total_premium_payable = total_premium_payable;
	}

	public String getCustomer_ID_type() {
		return customer_ID_type;
	}

	public void setCustomer_ID_type(String customer_ID_type) {
		this.customer_ID_type = customer_ID_type;
	}

	public String getCustomer_gender() {
		return customer_gender;
	}

	public void setCustomer_gender(String customer_gender) {
		this.customer_gender = customer_gender;
	}

	public String getCustomer_email() {
		return customer_email;
	}

	public void setCustomer_email(String customer_email) {
		this.customer_email = customer_email;
	}

	public String getCustomer_address_1() {
		return customer_address_1;
	}

	public void setCustomer_address_1(String customer_address_1) {
		this.customer_address_1 = customer_address_1;
	}

	public String getCustomer_address_2() {
		return customer_address_2;
	}

	public void setCustomer_address_2(String customer_address_2) {
		this.customer_address_2 = customer_address_2;
	}

	public String getCustomer_address_3() {
		return customer_address_3;
	}

	public void setCustomer_address_3(String customer_address_3) {
		this.customer_address_3 = customer_address_3;
	}

	public String getCustomer_state() {
		return customer_state;
	}

	public void setCustomer_state(String customer_state) {
		this.customer_state = customer_state;
	}

	public String getCustomer_postcode() {
		return customer_postcode;
	}

	public void setCustomer_postcode(String customer_postcode) {
		this.customer_postcode = customer_postcode;
	}

	public String getCustomer_mobile_no() {
		return customer_mobile_no;
	}

	public void setCustomer_mobile_no(String customer_mobile_no) {
		this.customer_mobile_no = customer_mobile_no;
	}

	public String getOperator_code() {
		return operator_code;
	}

	public void setOperator_code(String operator_code) {
		this.operator_code = operator_code;
	}

	public String getDiscount_code() {
		return discount_code;
	}

	public void setDiscount_code(String discount_code) {
		this.discount_code = discount_code;
	}

	public String getCoverage_start_date() {
		return coverage_start_date;
	}

	public void setCoverage_start_date(String coverage_start_date) {
		this.coverage_start_date = coverage_start_date;
	}

	public String getCoverage_end_date() {
		return coverage_end_date;
	}

	public void setCoverage_end_date(String coverage_end_date) {
		this.coverage_end_date = coverage_end_date;
	}

	public String getDSPQQID() {
		return DSPQQID;
	}

	public void setDSPQQID(String dSPQQID) {
		DSPQQID = dSPQQID;
	}

	public String getPDPA() {
		return PDPA;
	}

	public void setPDPA(String pDPA) {
		PDPA = pDPA;
	}

	public String getSpouse_name() {
		return spouse_name;
	}

	public void setSpouse_name(String spouse_name) {
		this.spouse_name = spouse_name;
	}

	public String getSpouse_email() {
		return spouse_email;
	}

	public void setSpouse_email(String spouse_email) {
		this.spouse_email = spouse_email;
	}

	public String getSpouse_ID_type() {
		return spouse_ID_type;
	}

	public void setSpouse_ID_type(String spouse_ID_type) {
		this.spouse_ID_type = spouse_ID_type;
	}

	public String getSpouse_ID_number() {
		return spouse_ID_number;
	}

	public void setSpouse_ID_number(String spouse_ID_number) {
		this.spouse_ID_number = spouse_ID_number;
	}

	public String getSpouse_DOB() {
		return spouse_DOB;
	}

	public void setSpouse_DOB(String spouse_DOB) {
		this.spouse_DOB = spouse_DOB;
	}

	public String getSpouse_gender() {
		return spouse_gender;
	}

	public void setSpouse_gender(String spouse_gender) {
		this.spouse_gender = spouse_gender;
	}

	public String getChild_name() {
		return child_name;
	}

	public void setChild_name(String child_name) {
		this.child_name = child_name;
	}

	public String getChild_ID_type() {
		return child_ID_type;
	}

	public void setChild_ID_type(String child_ID_type) {
		this.child_ID_type = child_ID_type;
	}

	public String getChild_ID_number() {
		return child_ID_number;
	}

	public void setChild_ID_number(String child_ID_number) {
		this.child_ID_number = child_ID_number;
	}

	public String getChild_DOB() {
		return child_DOB;
	}

	public void setChild_DOB(String child_DOB) {
		this.child_DOB = child_DOB;
	}

	public String getChild_gender() {
		return child_gender;
	}

	public void setChild_gender(String child_gender) {
		this.child_gender = child_gender;
	}

	public String getChild_name_1() {
		return child_name_1;
	}

	public void setChild_name_1(String child_name_1) {
		this.child_name_1 = child_name_1;
	}

	public String getChild_ID_type_1() {
		return child_ID_type_1;
	}

	public void setChild_ID_type_1(String child_ID_type_1) {
		this.child_ID_type_1 = child_ID_type_1;
	}

	public String getChild_ID_number_1() {
		return child_ID_number_1;
	}

	public void setChild_ID_number_1(String child_ID_number_1) {
		this.child_ID_number_1 = child_ID_number_1;
	}

	public String getChild_DOB_1() {
		return child_DOB_1;
	}

	public void setChild_DOB_1(String child_DOB_1) {
		this.child_DOB_1 = child_DOB_1;
	}

	public String getChild_gender_1() {
		return child_gender_1;
	}

	public void setChild_gender_1(String child_gender_1) {
		this.child_gender_1 = child_gender_1;
	}

	public String getChild_name_2() {
		return child_name_2;
	}

	public void setChild_name_2(String child_name_2) {
		this.child_name_2 = child_name_2;
	}

	public String getChild_ID_type_2() {
		return child_ID_type_2;
	}

	public void setChild_ID_type_2(String child_ID_type_2) {
		this.child_ID_type_2 = child_ID_type_2;
	}

	public String getChild_ID_number_2() {
		return child_ID_number_2;
	}

	public void setChild_ID_number_2(String child_ID_number_2) {
		this.child_ID_number_2 = child_ID_number_2;
	}

	public String getChild_DOB_2() {
		return child_DOB_2;
	}

	public void setChild_DOB_2(String child_DOB_2) {
		this.child_DOB_2 = child_DOB_2;
	}

	public String getChild_gender_2() {
		return child_gender_2;
	}

	public void setChild_gender_2(String child_gender_2) {
		this.child_gender_2 = child_gender_2;
	}

	public String getChild_name_3() {
		return child_name_3;
	}

	public void setChild_name_3(String child_name_3) {
		this.child_name_3 = child_name_3;
	}

	public String getChild_ID_type_3() {
		return child_ID_type_3;
	}

	public void setChild_ID_type_3(String child_ID_type_3) {
		this.child_ID_type_3 = child_ID_type_3;
	}

	public String getChild_ID_number_3() {
		return child_ID_number_3;
	}

	public void setChild_ID_number_3(String child_ID_number_3) {
		this.child_ID_number_3 = child_ID_number_3;
	}

	public String getChild_DOB_3() {
		return child_DOB_3;
	}

	public void setChild_DOB_3(String child_DOB_3) {
		this.child_DOB_3 = child_DOB_3;
	}

	public String getChild_gender_3() {
		return child_gender_3;
	}

	public void setChild_gender_3(String child_gender_3) {
		this.child_gender_3 = child_gender_3;
	}

	public String getChild_name_4() {
		return child_name_4;
	}

	public void setChild_name_4(String child_name_4) {
		this.child_name_4 = child_name_4;
	}

	public String getChild_ID_type_4() {
		return child_ID_type_4;
	}

	public void setChild_ID_type_4(String child_ID_type_4) {
		this.child_ID_type_4 = child_ID_type_4;
	}

	public String getChild_ID_number_4() {
		return child_ID_number_4;
	}

	public void setChild_ID_number_4(String child_ID_number_4) {
		this.child_ID_number_4 = child_ID_number_4;
	}

	public String getChild_DOB_4() {
		return child_DOB_4;
	}

	public void setChild_DOB_4(String child_DOB_4) {
		this.child_DOB_4 = child_DOB_4;
	}

	public String getChild_gender_4() {
		return child_gender_4;
	}

	public void setChild_gender_4(String child_gender_4) {
		this.child_gender_4 = child_gender_4;
	}

	public String getChild_name_5() {
		return child_name_5;
	}

	public void setChild_name_5(String child_name_5) {
		this.child_name_5 = child_name_5;
	}

	public String getChild_ID_type_5() {
		return child_ID_type_5;
	}

	public void setChild_ID_type_5(String child_ID_type_5) {
		this.child_ID_type_5 = child_ID_type_5;
	}

	public String getChild_ID_number_5() {
		return child_ID_number_5;
	}

	public void setChild_ID_number_5(String child_ID_number_5) {
		this.child_ID_number_5 = child_ID_number_5;
	}

	public String getChild_DOB_5() {
		return child_DOB_5;
	}

	public void setChild_DOB_5(String child_DOB_5) {
		this.child_DOB_5 = child_DOB_5;
	}

	public String getChild_gender_5() {
		return child_gender_5;
	}

	public void setChild_gender_5(String child_gender_5) {
		this.child_gender_5 = child_gender_5;
	}

	public String getChild_name_6() {
		return child_name_6;
	}

	public void setChild_name_6(String child_name_6) {
		this.child_name_6 = child_name_6;
	}

	public String getChild_ID_type_6() {
		return child_ID_type_6;
	}

	public void setChild_ID_type_6(String child_ID_type_6) {
		this.child_ID_type_6 = child_ID_type_6;
	}

	public String getChild_ID_number_6() {
		return child_ID_number_6;
	}

	public void setChild_ID_number_6(String child_ID_number_6) {
		this.child_ID_number_6 = child_ID_number_6;
	}

	public String getChild_DOB_6() {
		return child_DOB_6;
	}

	public void setChild_DOB_6(String child_DOB_6) {
		this.child_DOB_6 = child_DOB_6;
	}

	public String getChild_gender_6() {
		return child_gender_6;
	}

	public void setChild_gender_6(String child_gender_6) {
		this.child_gender_6 = child_gender_6;
	}

	public String getChild_name_7() {
		return child_name_7;
	}

	public void setChild_name_7(String child_name_7) {
		this.child_name_7 = child_name_7;
	}

	public String getChild_ID_type_7() {
		return child_ID_type_7;
	}

	public void setChild_ID_type_7(String child_ID_type_7) {
		this.child_ID_type_7 = child_ID_type_7;
	}

	public String getChild_ID_number_7() {
		return child_ID_number_7;
	}

	public void setChild_ID_number_7(String child_ID_number_7) {
		this.child_ID_number_7 = child_ID_number_7;
	}

	public String getChild_DOB_7() {
		return child_DOB_7;
	}

	public void setChild_DOB_7(String child_DOB_7) {
		this.child_DOB_7 = child_DOB_7;
	}

	public String getChild_gender_7() {
		return child_gender_7;
	}

	public void setChild_gender_7(String child_gender_7) {
		this.child_gender_7 = child_gender_7;
	}

	public String getArea_code() {
		return area_code;
	}

	public void setArea_code(String area_code) {
		this.area_code = area_code;
	}

	public String getTravel_start_date() {
		return travel_start_date;
	}

	public void setTravel_start_date(String travel_start_date) {
		this.travel_start_date = travel_start_date;
	}

	public String getTravel_end_date() {
		return travel_end_date;
	}

	public void setTravel_end_date(String travel_end_date) {
		this.travel_end_date = travel_end_date;
	}

	public String getTravel_duration() {
		return travel_duration;
	}

	public void setTravel_duration(String travel_duration) {
		this.travel_duration = travel_duration;
	}

	public String getOffered_plan_name() {
		return offered_plan_name;
	}

	public void setOffered_plan_name(String offered_plan_name) {
		this.offered_plan_name = offered_plan_name;
	}

	public String getTravel_area_type() {
		return travel_area_type;
	}

	public void setTravel_area_type(String travel_area_type) {
		this.travel_area_type = travel_area_type;
	}

	public String getTravlling_with() {
		return travlling_with;
	}

	public void setTravlling_with(String travlling_with) {
		this.travlling_with = travlling_with;
	}

	public Date getRecord_date() {
		return record_date;
	}

	public void setRecord_date(Date record_date) {
		this.record_date = record_date;
	}

	public String getTransaction_datetime() {
		return transaction_datetime;
	}

	public void setTransaction_datetime(String transaction_datetime) {
		this.transaction_datetime = transaction_datetime;
	}

	public String getPaymentTrxID() {
		return paymentTrxID;
	}

	public void setPaymentTrxID(String paymentTrxID) {
		this.paymentTrxID = paymentTrxID;
	}

	public String getPmnt_status() {
		return pmnt_status;
	}

	public void setPmnt_status(String pmnt_status) {
		this.pmnt_status = pmnt_status;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getGrosspremium_final() {
		return grosspremium_final;
	}

	public void setGrosspremium_final(String grosspremium_final) {
		this.grosspremium_final = grosspremium_final;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getGst() {
		return gst;
	}

	public void setGst(String gst) {
		this.gst = gst;
	}

	public String getInvoice_no() {
		return invoice_no;
	}

	public void setInvoice_no(String invoice_no) {
		this.invoice_no = invoice_no;
	}

	public String getPolicy_number() {
		return policy_number;
	}

	public void setPolicy_number(String policy_number) {
		this.policy_number = policy_number;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getCustomer_nric_id() {
		return customer_nric_id;
	}

	public void setCustomer_nric_id(String customer_nric_id) {
		this.customer_nric_id = customer_nric_id;
	}

	public String getPmnt_gateway_code() {
		return pmnt_gateway_code;
	}

	public void setPmnt_gateway_code(String pmnt_gateway_code) {
		this.pmnt_gateway_code = pmnt_gateway_code;
	}

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public String getAuth_id() {
		return auth_id;
	}

	public void setAuth_id(String auth_id) {
		this.auth_id = auth_id;
	}

	public String getBankrefno() {
		return bankrefno;
	}

	public void setBankrefno(String bankrefno) {
		this.bankrefno = bankrefno;
	}

	public String getApprovalcode() {
		return approvalcode;
	}

	public void setApprovalcode(String approvalcode) {
		this.approvalcode = approvalcode;
	}

	public String getFpx_fpxtxnid() {
		return fpx_fpxtxnid;
	}

	public void setFpx_fpxtxnid(String fpx_fpxtxnid) {
		this.fpx_fpxtxnid = fpx_fpxtxnid;
	}

	public String getFpx_debitauthcode() {
		return fpx_debitauthcode;
	}

	public void setFpx_debitauthcode(String fpx_debitauthcode) {
		this.fpx_debitauthcode = fpx_debitauthcode;
	}

	public String getProduct_code() {
		return product_code;
	}

	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}

	public String getAgent_code() {
		return agent_code;
	}

	public void setAgent_code(String agent_code) {
		this.agent_code = agent_code;
	}

	public String getAgent_name() {
		return agent_name;
	}

	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}

	public String getQuotation_status() {
		return quotation_status;
	}

	public void setQuotation_status(String quotation_status) {
		this.quotation_status = quotation_status;
	}

	public String getLast_page() {
		return last_page;
	}

	public void setLast_page(String last_page) {
		this.last_page = last_page;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getTrxID() {
		return trxID;
	}

	public void setTrxID(String trxID) {
		this.trxID = trxID;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

}
