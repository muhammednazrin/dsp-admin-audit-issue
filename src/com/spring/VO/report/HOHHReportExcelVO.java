package com.spring.VO.report;

import java.util.Date;

public class HOHHReportExcelVO {

	private Date record_date;
	private String transaction_datetime;
	private String paymentTrxID;
	private String pmnt_status;
	private String amount;
	private String grosspremium_final;
	private String discount;
	private String gst;
	private String invoice_no;
	private String policy_number;
	private String customer_name;
	private String customer_nric_id;
	private String pmnt_gateway_code;
	private String transaction_id;
	private String auth_id;
	private String bankrefno;
	private String approvalcode;
	private String fpx_fpxtxnid;
	private String fpx_debitauthcode;
	private String product_code;
	private String agent_code;
	private String agent_name;

	private String quotation_status;
	private String last_page;
	private String reason;

	private String trxID;
	private String paramName;
	private String paramValue;

	private String home_sum_insured;
	private String content_sum_insured;
	private String add_ben_riot_strike;
	private String add_ben_extended_theft;
	private String home_coverage;

	private String DSPQQID;
	private String quotation_datetime;
	private String home_type;
	private String build_construction_type;
	private String IDType;
	private String customer_email;
	private String customer_address_1;
	private String customer_address_2;
	private String customer_address_3;
	private String customer_postcode;
	private String customer_state;
	private String customer_mobile_no;
	private String property_insured_address_1;
	private String property_insured_address_2;
	private String property_insured_address;

	// SST
	private String sst_percentage;
	private String sst_amount;

	private String property_insured_postcode;
	private String property_insured_state;
	private String operator_code;
	private String discount_code;
	private String coverage_start_date;
	private String coverage_end_date;
	private String HOSI;
	private String HHSI;
	private String HOHHSI;
	private String add_ben_extended_theft_amt;
	private String add_ben_riot_strike_amt;
	private String PDPA;
	private String UW1;
	private String UW2;
	private String UW3;
	private String UW4;
	private String UW5;
	private String benefit_code_1;
	private String benefit_code_2;
	private String benefit_code_3;
	private String benefit_code_4;
	private String benefit_code_5;
	private String benefit_code_6;
	private String benefit_code_7;
	private String benefit_code_8;
	private String benefit_code_9;
	private String benefit_code_10;
	private String benefit_text_1;
	private String benefit_text_2;
	private String benefit_text_3;
	private String benefit_text_4;
	private String benefit_text_5;
	private String benefit_text_6;
	private String benefit_text_7;
	private String benefit_text_8;
	private String benefit_text_9;
	private String benefit_text_10;
	private String benefit_item_1;
	private String benefit_item_2;
	private String benefit_item_3;
	private String benefit_item_4;
	private String benefit_item_5;
	private String benefit_item_6;
	private String benefit_item_7;
	private String benefit_item_8;
	private String benefit_item_9;
	private String benefit_item_10;

	private String commission;
	private String customer_ID_type;
	private String modeStatus;
	private String pfNumber;

	public String getProperty_insured_address() {
		return property_insured_address;
	}

	public void setProperty_insured_address(String property_insured_address) {
		this.property_insured_address = property_insured_address;
	}

	public String getSst_percentage() {
		return sst_percentage;
	}

	public void setSst_percentage(String sst_percentage) {
		this.sst_percentage = sst_percentage;
	}

	public String getSst_amount() {
		return sst_amount;
	}

	public void setSst_amount(String sst_amount) {
		this.sst_amount = sst_amount;
	}

	public String getModeStatus() {
		return modeStatus;
	}

	public void setModeStatus(String modeStatus) {
		this.modeStatus = modeStatus;
	}

	public Date getRecord_date() {
		return record_date;
	}

	public void setRecord_date(Date record_date) {
		this.record_date = record_date;
	}

	public String getTransaction_datetime() {
		return transaction_datetime;
	}

	public void setTransaction_datetime(String transaction_datetime) {
		this.transaction_datetime = transaction_datetime;
	}

	public String getPaymentTrxID() {
		return paymentTrxID;
	}

	public void setPaymentTrxID(String paymentTrxID) {
		this.paymentTrxID = paymentTrxID;
	}

	public String getPmnt_status() {
		return pmnt_status;
	}

	public void setPmnt_status(String pmnt_status) {
		this.pmnt_status = pmnt_status;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getGrosspremium_final() {
		return grosspremium_final;
	}

	public void setGrosspremium_final(String grosspremium_final) {
		this.grosspremium_final = grosspremium_final;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getGst() {
		return gst;
	}

	public void setGst(String gst) {
		this.gst = gst;
	}

	public String getInvoice_no() {
		return invoice_no;
	}

	public void setInvoice_no(String invoice_no) {
		this.invoice_no = invoice_no;
	}

	public String getPolicy_number() {
		return policy_number;
	}

	public void setPolicy_number(String policy_number) {
		this.policy_number = policy_number;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getCustomer_nric_id() {
		return customer_nric_id;
	}

	public void setCustomer_nric_id(String customer_nric_id) {
		this.customer_nric_id = customer_nric_id;
	}

	public String getPmnt_gateway_code() {
		return pmnt_gateway_code;
	}

	public void setPmnt_gateway_code(String pmnt_gateway_code) {
		this.pmnt_gateway_code = pmnt_gateway_code;
	}

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public String getAuth_id() {
		return auth_id;
	}

	public void setAuth_id(String auth_id) {
		this.auth_id = auth_id;
	}

	public String getBankrefno() {
		return bankrefno;
	}

	public void setBankrefno(String bankrefno) {
		this.bankrefno = bankrefno;
	}

	public String getApprovalcode() {
		return approvalcode;
	}

	public void setApprovalcode(String approvalcode) {
		this.approvalcode = approvalcode;
	}

	public String getFpx_fpxtxnid() {
		return fpx_fpxtxnid;
	}

	public void setFpx_fpxtxnid(String fpx_fpxtxnid) {
		this.fpx_fpxtxnid = fpx_fpxtxnid;
	}

	public String getFpx_debitauthcode() {
		return fpx_debitauthcode;
	}

	public void setFpx_debitauthcode(String fpx_debitauthcode) {
		this.fpx_debitauthcode = fpx_debitauthcode;
	}

	public String getProduct_code() {
		return product_code;
	}

	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}

	public String getAgent_code() {
		return agent_code;
	}

	public void setAgent_code(String agent_code) {
		this.agent_code = agent_code;
	}

	public String getAgent_name() {
		return agent_name;
	}

	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}

	public String getQuotation_status() {
		return quotation_status;
	}

	public void setQuotation_status(String quotation_status) {
		this.quotation_status = quotation_status;
	}

	public String getLast_page() {
		return last_page;
	}

	public void setLast_page(String last_page) {
		this.last_page = last_page;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getTrxID() {
		return trxID;
	}

	public void setTrxID(String trxID) {
		this.trxID = trxID;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public String getHome_sum_insured() {
		return home_sum_insured;
	}

	public void setHome_sum_insured(String home_sum_insured) {
		this.home_sum_insured = home_sum_insured;
	}

	public String getContent_sum_insured() {
		return content_sum_insured;
	}

	public void setContent_sum_insured(String content_sum_insured) {
		this.content_sum_insured = content_sum_insured;
	}

	public String getAdd_ben_riot_strike() {
		return add_ben_riot_strike;
	}

	public void setAdd_ben_riot_strike(String add_ben_riot_strike) {
		this.add_ben_riot_strike = add_ben_riot_strike;
	}

	public String getAdd_ben_extended_theft() {
		return add_ben_extended_theft;
	}

	public void setAdd_ben_extended_theft(String add_ben_extended_theft) {
		this.add_ben_extended_theft = add_ben_extended_theft;
	}

	public String getHome_coverage() {
		return home_coverage;
	}

	public void setHome_coverage(String home_coverage) {
		this.home_coverage = home_coverage;
	}

	public String getDSPQQID() {
		return DSPQQID;
	}

	public void setDSPQQID(String dSPQQID) {
		DSPQQID = dSPQQID;
	}

	public String getQuotation_datetime() {
		return quotation_datetime;
	}

	public void setQuotation_datetime(String quotation_datetime) {
		this.quotation_datetime = quotation_datetime;
	}

	public String getHome_type() {
		return home_type;
	}

	public void setHome_type(String home_type) {
		this.home_type = home_type;
	}

	public String getBuild_construction_type() {
		return build_construction_type;
	}

	public void setBuild_construction_type(String build_construction_type) {
		this.build_construction_type = build_construction_type;
	}

	public String getIDType() {
		return IDType;
	}

	public void setIDType(String iDType) {
		IDType = iDType;
	}

	public String getCustomer_email() {
		return customer_email;
	}

	public void setCustomer_email(String customer_email) {
		this.customer_email = customer_email;
	}

	public String getCustomer_address_1() {
		return customer_address_1;
	}

	public void setCustomer_address_1(String customer_address_1) {
		this.customer_address_1 = customer_address_1;
	}

	public String getCustomer_address_2() {
		return customer_address_2;
	}

	public void setCustomer_address_2(String customer_address_2) {
		this.customer_address_2 = customer_address_2;
	}

	public String getCustomer_address_3() {
		return customer_address_3;
	}

	public void setCustomer_address_3(String customer_address_3) {
		this.customer_address_3 = customer_address_3;
	}

	public String getCustomer_postcode() {
		return customer_postcode;
	}

	public void setCustomer_postcode(String customer_postcode) {
		this.customer_postcode = customer_postcode;
	}

	public String getCustomer_state() {
		return customer_state;
	}

	public void setCustomer_state(String customer_state) {
		this.customer_state = customer_state;
	}

	public String getCustomer_mobile_no() {
		return customer_mobile_no;
	}

	public void setCustomer_mobile_no(String customer_mobile_no) {
		this.customer_mobile_no = customer_mobile_no;
	}

	public String getProperty_insured_address_1() {
		return property_insured_address_1;
	}

	public void setProperty_insured_address_1(String property_insured_address_1) {
		this.property_insured_address_1 = property_insured_address_1;
	}

	public String getProperty_insured_address_2() {
		return property_insured_address_2;
	}

	public void setProperty_insured_address_2(String property_insured_address_2) {
		this.property_insured_address_2 = property_insured_address_2;
	}

	public String getProperty_insured_postcode() {
		return property_insured_postcode;
	}

	public void setProperty_insured_postcode(String property_insured_postcode) {
		this.property_insured_postcode = property_insured_postcode;
	}

	public String getProperty_insured_state() {
		return property_insured_state;
	}

	public void setProperty_insured_state(String property_insured_state) {
		this.property_insured_state = property_insured_state;
	}

	public String getOperator_code() {
		return operator_code;
	}

	public void setOperator_code(String operator_code) {
		this.operator_code = operator_code;
	}

	public String getDiscount_code() {
		return discount_code;
	}

	public void setDiscount_code(String discount_code) {
		this.discount_code = discount_code;
	}

	public String getCoverage_start_date() {
		return coverage_start_date;
	}

	public void setCoverage_start_date(String coverage_start_date) {
		this.coverage_start_date = coverage_start_date;
	}

	public String getCoverage_end_date() {
		return coverage_end_date;
	}

	public void setCoverage_end_date(String coverage_end_date) {
		this.coverage_end_date = coverage_end_date;
	}

	public String getHOSI() {
		return HOSI;
	}

	public void setHOSI(String hOSI) {
		HOSI = hOSI;
	}

	public String getHHSI() {
		return HHSI;
	}

	public void setHHSI(String hHSI) {
		HHSI = hHSI;
	}

	public String getHOHHSI() {
		return HOHHSI;
	}

	public void setHOHHSI(String hOHHSI) {
		HOHHSI = hOHHSI;
	}

	public String getAdd_ben_extended_theft_amt() {
		return add_ben_extended_theft_amt;
	}

	public void setAdd_ben_extended_theft_amt(String add_ben_extended_theft_amt) {
		this.add_ben_extended_theft_amt = add_ben_extended_theft_amt;
	}

	public String getAdd_ben_riot_strike_amt() {
		return add_ben_riot_strike_amt;
	}

	public void setAdd_ben_riot_strike_amt(String add_ben_riot_strike_amt) {
		this.add_ben_riot_strike_amt = add_ben_riot_strike_amt;
	}

	public String getPDPA() {
		return PDPA;
	}

	public void setPDPA(String pDPA) {
		PDPA = pDPA;
	}

	public String getUW1() {
		return UW1;
	}

	public void setUW1(String uW1) {
		UW1 = uW1;
	}

	public String getUW2() {
		return UW2;
	}

	public void setUW2(String uW2) {
		UW2 = uW2;
	}

	public String getUW3() {
		return UW3;
	}

	public void setUW3(String uW3) {
		UW3 = uW3;
	}

	public String getUW4() {
		return UW4;
	}

	public void setUW4(String uW4) {
		UW4 = uW4;
	}

	public String getUW5() {
		return UW5;
	}

	public void setUW5(String uW5) {
		UW5 = uW5;
	}

	public String getBenefit_code_1() {
		return benefit_code_1;
	}

	public void setBenefit_code_1(String benefit_code_1) {
		this.benefit_code_1 = benefit_code_1;
	}

	public String getBenefit_code_2() {
		return benefit_code_2;
	}

	public void setBenefit_code_2(String benefit_code_2) {
		this.benefit_code_2 = benefit_code_2;
	}

	public String getBenefit_code_3() {
		return benefit_code_3;
	}

	public void setBenefit_code_3(String benefit_code_3) {
		this.benefit_code_3 = benefit_code_3;
	}

	public String getBenefit_code_4() {
		return benefit_code_4;
	}

	public void setBenefit_code_4(String benefit_code_4) {
		this.benefit_code_4 = benefit_code_4;
	}

	public String getBenefit_code_5() {
		return benefit_code_5;
	}

	public void setBenefit_code_5(String benefit_code_5) {
		this.benefit_code_5 = benefit_code_5;
	}

	public String getBenefit_code_6() {
		return benefit_code_6;
	}

	public void setBenefit_code_6(String benefit_code_6) {
		this.benefit_code_6 = benefit_code_6;
	}

	public String getBenefit_code_7() {
		return benefit_code_7;
	}

	public void setBenefit_code_7(String benefit_code_7) {
		this.benefit_code_7 = benefit_code_7;
	}

	public String getBenefit_code_8() {
		return benefit_code_8;
	}

	public void setBenefit_code_8(String benefit_code_8) {
		this.benefit_code_8 = benefit_code_8;
	}

	public String getBenefit_code_9() {
		return benefit_code_9;
	}

	public void setBenefit_code_9(String benefit_code_9) {
		this.benefit_code_9 = benefit_code_9;
	}

	public String getBenefit_code_10() {
		return benefit_code_10;
	}

	public void setBenefit_code_10(String benefit_code_10) {
		this.benefit_code_10 = benefit_code_10;
	}

	public String getBenefit_text_1() {
		return benefit_text_1;
	}

	public void setBenefit_text_1(String benefit_text_1) {
		this.benefit_text_1 = benefit_text_1;
	}

	public String getBenefit_text_2() {
		return benefit_text_2;
	}

	public void setBenefit_text_2(String benefit_text_2) {
		this.benefit_text_2 = benefit_text_2;
	}

	public String getBenefit_text_3() {
		return benefit_text_3;
	}

	public void setBenefit_text_3(String benefit_text_3) {
		this.benefit_text_3 = benefit_text_3;
	}

	public String getBenefit_text_4() {
		return benefit_text_4;
	}

	public void setBenefit_text_4(String benefit_text_4) {
		this.benefit_text_4 = benefit_text_4;
	}

	public String getBenefit_text_5() {
		return benefit_text_5;
	}

	public void setBenefit_text_5(String benefit_text_5) {
		this.benefit_text_5 = benefit_text_5;
	}

	public String getBenefit_text_6() {
		return benefit_text_6;
	}

	public void setBenefit_text_6(String benefit_text_6) {
		this.benefit_text_6 = benefit_text_6;
	}

	public String getBenefit_text_7() {
		return benefit_text_7;
	}

	public void setBenefit_text_7(String benefit_text_7) {
		this.benefit_text_7 = benefit_text_7;
	}

	public String getBenefit_text_8() {
		return benefit_text_8;
	}

	public void setBenefit_text_8(String benefit_text_8) {
		this.benefit_text_8 = benefit_text_8;
	}

	public String getBenefit_text_9() {
		return benefit_text_9;
	}

	public void setBenefit_text_9(String benefit_text_9) {
		this.benefit_text_9 = benefit_text_9;
	}

	public String getBenefit_text_10() {
		return benefit_text_10;
	}

	public void setBenefit_text_10(String benefit_text_10) {
		this.benefit_text_10 = benefit_text_10;
	}

	public String getCommission() {
		return commission;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}

	public String getCustomer_ID_type() {
		return customer_ID_type;
	}

	public void setCustomer_ID_type(String customer_ID_type) {
		this.customer_ID_type = customer_ID_type;
	}

	public String getBenefit_item_1() {
		return benefit_item_1;
	}

	public void setBenefit_item_1(String benefit_item_1) {
		this.benefit_item_1 = benefit_item_1;
	}

	public String getBenefit_item_2() {
		return benefit_item_2;
	}

	public void setBenefit_item_2(String benefit_item_2) {
		this.benefit_item_2 = benefit_item_2;
	}

	public String getBenefit_item_3() {
		return benefit_item_3;
	}

	public void setBenefit_item_3(String benefit_item_3) {
		this.benefit_item_3 = benefit_item_3;
	}

	public String getBenefit_item_4() {
		return benefit_item_4;
	}

	public void setBenefit_item_4(String benefit_item_4) {
		this.benefit_item_4 = benefit_item_4;
	}

	public String getBenefit_item_5() {
		return benefit_item_5;
	}

	public void setBenefit_item_5(String benefit_item_5) {
		this.benefit_item_5 = benefit_item_5;
	}

	public String getBenefit_item_6() {
		return benefit_item_6;
	}

	public void setBenefit_item_6(String benefit_item_6) {
		this.benefit_item_6 = benefit_item_6;
	}

	public String getBenefit_item_7() {
		return benefit_item_7;
	}

	public void setBenefit_item_7(String benefit_item_7) {
		this.benefit_item_7 = benefit_item_7;
	}

	public String getBenefit_item_8() {
		return benefit_item_8;
	}

	public void setBenefit_item_8(String benefit_item_8) {
		this.benefit_item_8 = benefit_item_8;
	}

	public String getBenefit_item_9() {
		return benefit_item_9;
	}

	public void setBenefit_item_9(String benefit_item_9) {
		this.benefit_item_9 = benefit_item_9;
	}

	public String getBenefit_item_10() {
		return benefit_item_10;
	}

	public void setBenefit_item_10(String benefit_item_10) {
		this.benefit_item_10 = benefit_item_10;
	}

	public String getPfNumber() {
		return pfNumber;
	}

	public void setPfNumber(String pfNumber) {
		this.pfNumber = pfNumber;
	}
}
