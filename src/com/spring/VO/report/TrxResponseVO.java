package com.spring.VO.report;

import java.sql.Clob;

public class TrxResponseVO {

	private String transactionID;
	private String paramName;
	private String paramValue;
	private Clob paramList;

	public String getTransactionID() {
		return transactionID;
	}

	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public Clob getParamList() {
		return paramList;
	}

	public void setParamList(Clob paramList) {
		this.paramList = paramList;
	}

}
