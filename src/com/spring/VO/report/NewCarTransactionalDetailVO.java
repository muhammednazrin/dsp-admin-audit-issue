package com.spring.VO.report;

public class NewCarTransactionalDetailVO {

	private int id;
	private String insCompany;
	private String salesEntity;
	private String marketerName;
	private String marketerCode;
	private String agentName;
	private String agentCode;
	private String DateFrom;
	private String DateTo;
	private String PolicyNo;
	private String customerName;
	private String ProductEntity;
	private String ProductType;
	private String Status;
	private String PremiumAmount;
	private String TransactionDate;
	private String TransactionID;
	private String customerNRIC;
	private String PaymentMethod;
	private String PaymentRefNo;
	private String PaymentStatus;
	private String Incentive;
	private String Commission;
	private String BankFee;
	private String TotalNet;
	private String customerEmail;
	private String customerAddress1;
	private String customerAddress2;
	private String customerAddress3;
	private String customerCity;
	private String customerPostcode;
	private String customerState;
	private String customerCountry;
	private String customerMobileNo;
	private String customerID;
	private String dspqqid;
	private String PaymentChannel;
	private String InvoiceNo;
	private String ProductCode;
	private String Term;
	private String Coverage;
	private String Mode;
	private String PaymentAuthCode;
	private String TotalAmount;
	private String PremiumMode;
	private String MPAY_RefNo;
	private String MPAY_AuthCode;
	private String EBPG_RefNo;
	private String EBPG_AuthCode;
	private String M2U_RefNo;
	private String M2U_AuthCode;
	private String FPX_RefNo;
	private String FPX_AuthCode;
	private String Reason;
	private String vehicleNumber;
	private String AuditRemarks;
	private String vehicleLocation;
	private String vehicleMake;
	private String VehicleModel;
	private String yearMake;
	private String chassisNo;
	private String financedBy;
	private String CoverageStartDate;
	private String CoverageEndDate;
	private String engineNo;
	private String nvic;
	private String TLCheckDigit;
	private String UWReason;
	private String DriverPAPolicyNo;
	private String JPJStatus;
	private String CAPSPolicyNo;
	private String qqid;
	private String policy_status;
	// Newly added
	private String poiPolicyNo;

	public String getTLCheckDigit() {
		return TLCheckDigit;
	}

	public void setTLCheckDigit(String tLCheckDigit) {
		TLCheckDigit = tLCheckDigit;
	}

	public String getUWReason() {
		return UWReason;
	}

	public void setUWReason(String uWReason) {
		UWReason = uWReason;
	}

	public String getDriverPAPolicyNo() {
		return DriverPAPolicyNo;
	}

	public void setDriverPAPolicyNo(String driverPAPolicyNo) {
		DriverPAPolicyNo = driverPAPolicyNo;
	}

	public String getJPJStatus() {
		return JPJStatus;
	}

	public void setJPJStatus(String jPJStatus) {
		JPJStatus = jPJStatus;
	}

	public String getCAPSPolicyNo() {
		return CAPSPolicyNo;
	}

	public void setCAPSPolicyNo(String cAPSPolicyNo) {
		CAPSPolicyNo = cAPSPolicyNo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getInsCompany() {
		return insCompany;
	}

	public void setInsCompany(String insCompany) {
		this.insCompany = insCompany;
	}

	public String getSalesEntity() {
		return salesEntity;
	}

	public void setSalesEntity(String salesEntity) {
		this.salesEntity = salesEntity;
	}

	public String getMarketerName() {
		return marketerName;
	}

	public void setMarketerName(String marketerName) {
		this.marketerName = marketerName;
	}

	public String getMarketerCode() {
		return marketerCode;
	}

	public void setMarketerCode(String marketerCode) {
		this.marketerCode = marketerCode;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getDateFrom() {
		return DateFrom;
	}

	public void setDateFrom(String dateFrom) {
		DateFrom = dateFrom;
	}

	public String getDateTo() {
		return DateTo;
	}

	public void setDateTo(String dateTo) {
		DateTo = dateTo;
	}

	public String getPolicyNo() {
		return PolicyNo;
	}

	public void setPolicyNo(String policyNo) {
		PolicyNo = policyNo;
	}

	public String getProductEntity() {
		return ProductEntity;
	}

	public void setProductEntity(String productEntity) {
		ProductEntity = productEntity;
	}

	public String getProductType() {
		return ProductType;
	}

	public void setProductType(String productType) {
		ProductType = productType;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getPremiumAmount() {
		return PremiumAmount;
	}

	public void setPremiumAmount(String premiumAmount) {
		PremiumAmount = premiumAmount;
	}

	public String getTransactionDate() {
		return TransactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		TransactionDate = transactionDate;
	}

	public String getTransactionID() {
		return TransactionID;
	}

	public void setTransactionID(String transactionID) {
		TransactionID = transactionID;
	}

	public String getPaymentMethod() {
		return PaymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		PaymentMethod = paymentMethod;
	}

	public String getPaymentRefNo() {
		return PaymentRefNo;
	}

	public String getCustomerNRIC() {
		return customerNRIC;
	}

	public void setCustomerNRIC(String customerNRIC) {
		this.customerNRIC = customerNRIC;
	}

	public void setPaymentRefNo(String paymentRefNo) {
		PaymentRefNo = paymentRefNo;
	}

	public String getPaymentStatus() {
		return PaymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		PaymentStatus = paymentStatus;
	}

	public String getIncentive() {
		return Incentive;
	}

	public void setIncentive(String incentive) {
		Incentive = incentive;
	}

	public String getCommission() {
		return Commission;
	}

	public void setCommission(String commission) {
		Commission = commission;
	}

	public String getBankFee() {
		return BankFee;
	}

	public void setBankFee(String bankFee) {
		BankFee = bankFee;
	}

	public String getTotalNet() {
		return TotalNet;
	}

	public String getDspqqid() {
		return dspqqid;
	}

	public void setDspqqid(String dspqqid) {
		this.dspqqid = dspqqid;
	}

	public void setTotalNet(String totalNet) {
		TotalNet = totalNet;
	}

	public String getPaymentChannel() {
		return PaymentChannel;
	}

	public void setPaymentChannel(String paymentChannel) {
		PaymentChannel = paymentChannel;
	}

	public String getInvoiceNo() {
		return InvoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		InvoiceNo = invoiceNo;
	}

	public String getProductCode() {
		return ProductCode;
	}

	public void setProductCode(String productCode) {
		ProductCode = productCode;
	}

	public String getTerm() {
		return Term;
	}

	public void setTerm(String term) {
		Term = term;
	}

	public String getCoverage() {
		return Coverage;
	}

	public void setCoverage(String coverage) {
		Coverage = coverage;
	}

	public String getMode() {
		return Mode;
	}

	public void setMode(String mode) {
		Mode = mode;
	}

	public String getPaymentAuthCode() {
		return PaymentAuthCode;
	}

	public void setPaymentAuthCode(String paymentAuthCode) {
		PaymentAuthCode = paymentAuthCode;
	}

	public String getTotalAmount() {
		return TotalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		TotalAmount = totalAmount;
	}

	public String getPremiumMode() {
		return PremiumMode;
	}

	public void setPremiumMode(String premiumMode) {
		PremiumMode = premiumMode;
	}

	public String getMPAY_RefNo() {
		return MPAY_RefNo;
	}

	public void setMPAY_RefNo(String mPAY_RefNo) {
		MPAY_RefNo = mPAY_RefNo;
	}

	public String getMPAY_AuthCode() {
		return MPAY_AuthCode;
	}

	public void setMPAY_AuthCode(String mPAY_AuthCode) {
		MPAY_AuthCode = mPAY_AuthCode;
	}

	public String getEBPG_RefNo() {
		return EBPG_RefNo;
	}

	public void setEBPG_RefNo(String eBPG_RefNo) {
		EBPG_RefNo = eBPG_RefNo;
	}

	public String getEBPG_AuthCode() {
		return EBPG_AuthCode;
	}

	public void setEBPG_AuthCode(String eBPG_AuthCode) {
		EBPG_AuthCode = eBPG_AuthCode;
	}

	public String getM2U_RefNo() {
		return M2U_RefNo;
	}

	public void setM2U_RefNo(String m2u_RefNo) {
		M2U_RefNo = m2u_RefNo;
	}

	public String getM2U_AuthCode() {
		return M2U_AuthCode;
	}

	public void setM2U_AuthCode(String m2u_AuthCode) {
		M2U_AuthCode = m2u_AuthCode;
	}

	public String getFPX_RefNo() {
		return FPX_RefNo;
	}

	public void setFPX_RefNo(String fPX_RefNo) {
		FPX_RefNo = fPX_RefNo;
	}

	public String getFPX_AuthCode() {
		return FPX_AuthCode;
	}

	public void setFPX_AuthCode(String fPX_AuthCode) {
		FPX_AuthCode = fPX_AuthCode;
	}

	public String getReason() {
		return Reason;
	}

	public void setReason(String reason) {
		Reason = reason;
	}

	public String getAuditRemarks() {
		return AuditRemarks;
	}

	public void setAuditRemarks(String auditRemarks) {
		AuditRemarks = auditRemarks;
	}

	public String getVehicleMake() {
		return vehicleMake;
	}

	public void setVehicleMake(String vehicleMake) {
		this.vehicleMake = vehicleMake;
	}

	public String getYearMake() {
		return yearMake;
	}

	public void setYearMake(String yearMake) {
		this.yearMake = yearMake;
	}

	public String getChassisNo() {
		return chassisNo;
	}

	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}

	public String getFinancedBy() {
		return financedBy;
	}

	public void setFinancedBy(String financedBy) {
		this.financedBy = financedBy;
	}

	public String getEngineNo() {
		return engineNo;
	}

	public void setEngineNo(String engineNo) {
		this.engineNo = engineNo;
	}

	public String getNvic() {
		return nvic;
	}

	public void setNvic(String nvic) {
		this.nvic = nvic;
	}

	public String getVehicleModel() {
		return VehicleModel;
	}

	public void setVehicleModel(String vehicleModel) {
		VehicleModel = vehicleModel;
	}

	public String getCoverageStartDate() {
		return CoverageStartDate;
	}

	public void setCoverageStartDate(String coverageStartDate) {
		CoverageStartDate = coverageStartDate;
	}

	public String getCoverageEndDate() {
		return CoverageEndDate;
	}

	public void setCoverageEndDate(String coverageEndDate) {
		CoverageEndDate = coverageEndDate;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerAddress1() {
		return customerAddress1;
	}

	public void setCustomerAddress1(String customerAddress1) {
		this.customerAddress1 = customerAddress1;
	}

	public String getCustomerAddress2() {
		return customerAddress2;
	}

	public void setCustomerAddress2(String customerAddress2) {
		this.customerAddress2 = customerAddress2;
	}

	public String getCustomerAddress3() {
		return customerAddress3;
	}

	public void setCustomerAddress3(String customerAddress3) {
		this.customerAddress3 = customerAddress3;
	}

	public String getCustomerCity() {
		return customerCity;
	}

	public void setCustomerCity(String customerCity) {
		this.customerCity = customerCity;
	}

	public String getCustomerPostcode() {
		return customerPostcode;
	}

	public void setCustomerPostcode(String customerPostcode) {
		this.customerPostcode = customerPostcode;
	}

	public String getCustomerState() {
		return customerState;
	}

	public void setCustomerState(String customerState) {
		this.customerState = customerState;
	}

	public String getCustomerCountry() {
		return customerCountry;
	}

	public void setCustomerCountry(String customerCountry) {
		this.customerCountry = customerCountry;
	}

	public String getCustomerMobileNo() {
		return customerMobileNo;
	}

	public void setCustomerMobileNo(String customerMobileNo) {
		this.customerMobileNo = customerMobileNo;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getVehicleLocation() {
		return vehicleLocation;
	}

	public void setVehicleLocation(String vehicleLocation) {
		this.vehicleLocation = vehicleLocation;
	}

	public String getQqid() {
		return qqid;
	}

	public void setQqid(String qqid) {
		this.qqid = qqid;
	}

	public String getPolicy_status() {
		return policy_status;
	}

	public void setPolicy_status(String policy_status) {
		this.policy_status = policy_status;
	}

	public String getPoiPolicyNo() {
		return poiPolicyNo;
	}

	public void setPoiPolicyNo(String poiPolicyNo) {
		this.poiPolicyNo = poiPolicyNo;
	}

}
