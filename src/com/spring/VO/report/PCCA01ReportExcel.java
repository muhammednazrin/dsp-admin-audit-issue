package com.spring.VO.report;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import test.PCCA01ReportExcelTest;
import test.TransSearchObjectTest;

public class PCCA01ReportExcel {
		private Date record_date;
		private String transaction_datetime;
		private String paymentTrxID;
		private String pmnt_status;
		private String amount;
		private String invoice_no;
		private String policy_number;
		private String customer_name;
		private String customer_nric_id;
		private String pmnt_gateway_code;
		private String txn_id;
		private String auth_code;

		private String product_code;
		private String agent_code;
		private String agent_name;

		private String coverage_term;
		private String coverage_amount;
		private String premium_mode;
		private String premium_amount;

		private String quotation_status;
		private String last_page;
		private String reason;

		private String UWReason;
		private String DSPQQID;

		private String CustomerGender;
		private String CustomerEmail;
		private String CustomerAddress1;
		private String CustomerAddress2;
		private String CustomerAddress3;
		private String CustomerCity;
		private String CustomerPostcode;
		private String CustomerState;
		private String CustomerCountry;
		private String CustomerMobileNo;
		private String QuotationCreateDateTime;
		private String QuotationUpdateDateTime;

		private String AgentCode;
		private String AgentName;
		private String OperatorCode;
		private String OperatorName;

		private String DiscountCode;

		private int id;
		private String premium_Annualamount;

		public String getPremium_Annualamount() {
			return premium_Annualamount;
		}

		public void setPremium_Annualamount(String premium_Annualamount) {
			this.premium_Annualamount = premium_Annualamount;
		}

		public String getDiscountCode() {
			return DiscountCode;
		}

		public void setDiscountCode(String discountCode) {
			DiscountCode = discountCode;
		}

		public String getAgentCode() {
			return AgentCode;
		}

		public void setAgentCode(String agentCode) {
			AgentCode = agentCode;
		}

		public String getAgentName() {
			return AgentName;
		}

		public void setAgentName(String agentName) {
			AgentName = agentName;
		}

		public String getOperatorCode() {
			return OperatorCode;
		}

		public void setOperatorCode(String operatorCode) {
			OperatorCode = operatorCode;
		}

		public String getOperatorName() {
			return OperatorName;
		}

		public void setOperatorName(String operatorName) {
			OperatorName = operatorName;
		}

		public String getCustomerEmail() {
			return CustomerEmail;
		}

		public void setCustomerEmail(String customerEmail) {
			CustomerEmail = customerEmail;
		}

		public String getCustomerAddress1() {
			return CustomerAddress1;
		}

		public void setCustomerAddress1(String customerAddress1) {
			CustomerAddress1 = customerAddress1;
		}

		public String getCustomerAddress2() {
			return CustomerAddress2;
		}

		public void setCustomerAddress2(String customerAddress2) {
			CustomerAddress2 = customerAddress2;
		}

		public String getCustomerAddress3() {
			return CustomerAddress3;
		}

		public void setCustomerAddress3(String customerAddress3) {
			CustomerAddress3 = customerAddress3;
		}

		public String getCustomerCity() {
			return CustomerCity;
		}

		public void setCustomerCity(String customerCity) {
			CustomerCity = customerCity;
		}

		public String getCustomerPostcode() {
			return CustomerPostcode;
		}

		public void setCustomerPostcode(String customerPostcode) {
			CustomerPostcode = customerPostcode;
		}

		public String getCustomerState() {
			return CustomerState;
		}

		public void setCustomerState(String customerState) {
			CustomerState = customerState;
		}

		public String getCustomerCountry() {
			return CustomerCountry;
		}

		public void setCustomerCountry(String customerCountry) {
			CustomerCountry = customerCountry;
		}

		public String getCustomerMobileNo() {
			return CustomerMobileNo;
		}

		public void setCustomerMobileNo(String customerMobileNo) {
			CustomerMobileNo = customerMobileNo;
		}

		public String getCustomerGender() {
			return CustomerGender;
		}

		public void setCustomerGender(String customerGender) {
			CustomerGender = customerGender;
		}

		public String getDSPQQID() {
			return DSPQQID;
		}

		public void setDSPQQID(String dSPQQID) {
			DSPQQID = dSPQQID;
		}

		public String getQuotationCreateDateTime() {
			return QuotationCreateDateTime;
		}

		public void setQuotationCreateDateTime(String quotationCreateDateTime) {
			QuotationCreateDateTime = quotationCreateDateTime;
		}

		public String getQuotationUpdateDateTime() {
			return QuotationUpdateDateTime;
		}

		public void setQuotationUpdateDateTime(String quotationUpdateDateTime) {
			QuotationUpdateDateTime = quotationUpdateDateTime;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getUWReason() {
			return UWReason;
		}

		public void setUWReason(String uWReason) {
			UWReason = uWReason;
		}

		public String getQuotation_status() {
			return quotation_status;
		}

		public void setQuotation_status(String quotation_status) {
			this.quotation_status = quotation_status;
		}

		public String getLast_page() {
			return last_page;
		}

		public void setLast_page(String last_page) {
			this.last_page = last_page;
		}

		public String getReason() {
			return reason;
		}

		public void setReason(String reason) {
			this.reason = reason;
		}

		public Date getRecord_date() {
			return record_date;
		}

		public void setRecord_date(Date record_date) {
			this.record_date = record_date;
		}

		public String getTransaction_datetime() {
			return transaction_datetime;
		}

		public void setTransaction_datetime(String transaction_datetime) {
			this.transaction_datetime = transaction_datetime;
		}

		public String getPaymentTrxID() {
			return paymentTrxID;
		}

		public void setPaymentTrxID(String paymentTrxID) {
			this.paymentTrxID = paymentTrxID;
		}

		public String getPmnt_status() {
			return pmnt_status;
		}

		public void setPmnt_status(String pmnt_status) {
			this.pmnt_status = pmnt_status;
		}

		public String getAmount() {
			return amount;
		}

		public void setAmount(String amount) {
			this.amount = amount;
		}

		public String getInvoice_no() {
			return invoice_no;
		}

		public void setInvoice_no(String invoice_no) {
			this.invoice_no = invoice_no;
		}

		public String getPolicy_number() {
			return policy_number;
		}

		public void setPolicy_number(String policy_number) {
			this.policy_number = policy_number;
		}

		public String getCustomer_name() {
			return customer_name;
		}

		public void setCustomer_name(String customer_name) {
			this.customer_name = customer_name;
		}

		public String getCustomer_nric_id() {
			return customer_nric_id;
		}

		public void setCustomer_nric_id(String customer_nric_id) {
			this.customer_nric_id = customer_nric_id;
		}

		public String getPmnt_gateway_code() {
			return pmnt_gateway_code;
		}

		public void setPmnt_gateway_code(String pmnt_gateway_code) {
			this.pmnt_gateway_code = pmnt_gateway_code;
		}

		public String getTxn_id() {
			return txn_id;
		}

		public void setTxn_id(String txn_id) {
			this.txn_id = txn_id;
		}

		public String getAuth_code() {
			return auth_code;
		}

		public void setAuth_code(String auth_code) {
			this.auth_code = auth_code;
		}

		public String getProduct_code() {
			return product_code;
		}

		public void setProduct_code(String product_code) {
			this.product_code = product_code;
		}

		public String getAgent_code() {
			return agent_code;
		}

		public void setAgent_code(String agent_code) {
			this.agent_code = agent_code;
		}

		public String getAgent_name() {
			return agent_name;
		}

		public void setAgent_name(String agent_name) {
			this.agent_name = agent_name;
		}

		public String getCoverage_term() {
			return coverage_term;
		}

		public void setCoverage_term(String coverage_term) {
			this.coverage_term = coverage_term;
		}

		public String getCoverage_amount() {
			return coverage_amount;
		}

		public void setCoverage_amount(String coverage_amount) {
			this.coverage_amount = coverage_amount;
		}

		public String getPremium_mode() {
			return premium_mode;
		}

		public void setPremium_mode(String premium_mode) {
			this.premium_mode = premium_mode;
		}

		public String getPremium_amount() {
			return premium_amount;
		}

		public void setPremium_amount(String premium_amount) {
			this.premium_amount = premium_amount;
		}

		public static List<PCCA01ReportExcelTest> selectPCCA01TransactionalReportAfterPayment(
				TransSearchObjectTest transSearchObject_PCCA01) {
			// TODO Auto-generated method stub
			return null;
		}

		public static List<PCCA01ReportExcelTest> selectPCCA01TransactionalReportBeforePayment1(
				TransSearchObjectTest transSearchObject_PCCA01) {
			// TODO Auto-generated method stub
			return null;
		}

		public static List<PCCA01ReportExcelTest> selectPCCA01TransactionalReportBeforePayment(
				TransSearchObjectTest transSearchObject_PCCA01) {
			// TODO Auto-generated method stub
			return null;
		}

	}