package com.spring.VO.report;

import java.util.List;

public class TransSearchObject {

	private String dateFrom;
	private String dateTo;
	private String productEntity;
	private String productCode;
	private String vehicleNo;
	private String nricNo;
	private String policyNo;
	private List<String> transactionStatus;
	private String invoiceNo;

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public String getProductEntity() {
		return productEntity;
	}

	public void setProductEntity(String productEntity) {
		this.productEntity = productEntity;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getNricNo() {
		return nricNo;
	}

	public void setNricNo(String nricNo) {
		this.nricNo = nricNo;
	}

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public List<String> getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(List<String> transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

}
