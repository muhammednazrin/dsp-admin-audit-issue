package com.spring.VO;

public class MIProductInfoApproval {

	private String annualTarget;
	private String validity;

	public String getAnnualTarget() {
		return annualTarget;
	}

	public void setAnnualTarget(String annualTarget) {
		this.annualTarget = annualTarget;
	}

	public String getValidity() {
		return validity;
	}

	public void setValidity(String validity) {
		this.validity = validity;
	}

}
