package com.spring.VO;

public class SalesLead {
	String Number;
	String Date_Time;
	String Name;
	String VehicleRegNo;
	String Email;
	String RoadTaxExpiryMonth;
	String MobileNo;
	String OtherPhoneNo;
	String AgentName;
	String AgentCode;

	private String leadsAbdStatus;
	private String leadsAbdAgentCode;
	private String leadsAbdAgent;
	private String leadsAbdProduct;
	private String leadsAbdEntity;
	private String leadsAbdToDate;
	private String leadsAbdFromDate;
	private String leadsAbdStep;

	private String DSP_QQ_ID;
	private String LEADS_EMAIL_ID;
	private String PRODUCT_CODE;
	private String QQ_STATUS;
	private String QQ_STEPS;
	private String FULL_NAME;
	private String CREATE_DATE;
	private String CUSTOMER_MOBILE_NO;

	private int step1_total;
	private int step2_total;
	private int step3_total;
	private int step1_lost;
	private int step2_lost;
	private int step3_lost;
	private int totalSuccess;

	public int getTotalSuccess() {
		return totalSuccess;
	}

	public void setTotalSuccess(int totalSuccess) {
		this.totalSuccess = totalSuccess;
	}

	public int getStep1_total() {
		return step1_total;
	}

	public void setStep1_total(int step1_total) {
		this.step1_total = step1_total;
	}

	public int getStep2_total() {
		return step2_total;
	}

	public void setStep2_total(int step2_total) {
		this.step2_total = step2_total;
	}

	public int getStep3_total() {
		return step3_total;
	}

	public void setStep3_total(int step3_total) {
		this.step3_total = step3_total;
	}

	public int getStep1_lost() {
		return step1_lost;
	}

	public void setStep1_lost(int step1_lost) {
		this.step1_lost = step1_lost;
	}

	public int getStep2_lost() {
		return step2_lost;
	}

	public void setStep2_lost(int step2_lost) {
		this.step2_lost = step2_lost;
	}

	public int getStep3_lost() {
		return step3_lost;
	}

	public void setStep3_lost(int step3_lost) {
		this.step3_lost = step3_lost;
	}

	public String getDSP_QQ_ID() {
		return DSP_QQ_ID;
	}

	public void setDSP_QQ_ID(String dSP_QQ_ID) {
		DSP_QQ_ID = dSP_QQ_ID;
	}

	public String getLEADS_EMAIL_ID() {
		return LEADS_EMAIL_ID;
	}

	public void setLEADS_EMAIL_ID(String lEADS_EMAIL_ID) {
		LEADS_EMAIL_ID = lEADS_EMAIL_ID;
	}

	public String getPRODUCT_CODE() {
		return PRODUCT_CODE;
	}

	public void setPRODUCT_CODE(String pRODUCT_CODE) {
		PRODUCT_CODE = pRODUCT_CODE;
	}

	public String getQQ_STATUS() {
		return QQ_STATUS;
	}

	public void setQQ_STATUS(String qQ_STATUS) {
		QQ_STATUS = qQ_STATUS;
	}

	public String getQQ_STEPS() {
		return QQ_STEPS;
	}

	public void setQQ_STEPS(String qQ_STEPS) {
		QQ_STEPS = qQ_STEPS;
	}

	public String getFULL_NAME() {
		return FULL_NAME;
	}

	public void setFULL_NAME(String fULL_NAME) {
		FULL_NAME = fULL_NAME;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getCUSTOMER_MOBILE_NO() {
		return CUSTOMER_MOBILE_NO;
	}

	public void setCUSTOMER_MOBILE_NO(String cUSTOMER_MOBILE_NO) {
		CUSTOMER_MOBILE_NO = cUSTOMER_MOBILE_NO;
	}

	public String getLeadsAbdStatus() {
		return leadsAbdStatus;
	}

	public void setLeadsAbdStatus(String leadsAbdStatus) {
		this.leadsAbdStatus = leadsAbdStatus;
	}

	public String getLeadsAbdAgentCode() {
		return leadsAbdAgentCode;
	}

	public void setLeadsAbdAgentCode(String leadsAbdAgentCode) {
		this.leadsAbdAgentCode = leadsAbdAgentCode;
	}

	public String getLeadsAbdAgent() {
		return leadsAbdAgent;
	}

	public void setLeadsAbdAgent(String leadsAbdAgent) {
		this.leadsAbdAgent = leadsAbdAgent;
	}

	public String getLeadsAbdProduct() {
		return leadsAbdProduct;
	}

	public void setLeadsAbdProduct(String leadsAbdProduct) {
		this.leadsAbdProduct = leadsAbdProduct;
	}

	public String getLeadsAbdEntity() {
		return leadsAbdEntity;
	}

	public void setLeadsAbdEntity(String leadsAbdEntity) {
		this.leadsAbdEntity = leadsAbdEntity;
	}

	public String getLeadsAbdToDate() {
		return leadsAbdToDate;
	}

	public void setLeadsAbdToDate(String leadsAbdToDate) {
		this.leadsAbdToDate = leadsAbdToDate;
	}

	public String getLeadsAbdFromDate() {
		return leadsAbdFromDate;
	}

	public void setLeadsAbdFromDate(String leadsAbdFromDate) {
		this.leadsAbdFromDate = leadsAbdFromDate;
	}

	public String getLeadsAbdStep() {
		return leadsAbdStep;
	}

	public void setLeadsAbdStep(String leadsAbdStep) {
		this.leadsAbdStep = leadsAbdStep;
	}

	public String getNumber() {
		return Number;
	}

	public void setNumber(String number) {
		Number = number;
	}

	public String getDate_Time() {
		return Date_Time;
	}

	public void setDate_Time(String date_Time) {
		Date_Time = date_Time;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getVehicleRegNo() {
		return VehicleRegNo;
	}

	public void setVehicleRegNo(String vehicleRegNo) {
		VehicleRegNo = vehicleRegNo;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getRoadTaxExpiryMonth() {
		return RoadTaxExpiryMonth;
	}

	public void setRoadTaxExpiryMonth(String roadTaxExpiryMonth) {
		RoadTaxExpiryMonth = roadTaxExpiryMonth;
	}

	public String getMobileNo() {
		return MobileNo;
	}

	public void setMobileNo(String mobileNo) {
		MobileNo = mobileNo;
	}

	public String getOtherPhoneNo() {
		return OtherPhoneNo;
	}

	public void setOtherPhoneNo(String otherPhoneNo) {
		OtherPhoneNo = otherPhoneNo;
	}

	public String getAgentName() {
		return AgentName;
	}

	public void setAgentName(String agentName) {
		AgentName = agentName;
	}

	public String getAgentCode() {
		return AgentCode;
	}

	public void setAgentCode(String agentCode) {
		AgentCode = agentCode;
	}

}
