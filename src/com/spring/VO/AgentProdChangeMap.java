package com.spring.VO;

import java.util.ArrayList;
import java.util.List;

public class AgentProdChangeMap {

	List<String> productCodeCheckCounterList = new ArrayList<String>();

	public List<String> getProductCodeCheckCounterList() {
		return productCodeCheckCounterList;
	}

	public void setProductCodeCheckCounterList(List<String> productCodeCheckCounterList) {
		this.productCodeCheckCounterList = productCodeCheckCounterList;
	}

	public List<String> getProductCodeCheckValueList() {
		return productCodeCheckValueList;
	}

	public void setProductCodeCheckValueList(List<String> productCodeCheckValueList) {
		this.productCodeCheckValueList = productCodeCheckValueList;
	}

	public List<String> getAgentBusinessTypeList() {
		return agentBusinessTypeList;
	}

	public void setAgentBusinessTypeList(List<String> agentBusinessTypeList) {
		this.agentBusinessTypeList = agentBusinessTypeList;
	}

	public List<String> getAgentCodeList() {
		return agentCodeList;
	}

	public void setAgentCodeList(List<String> agentCodeList) {
		this.agentCodeList = agentCodeList;
	}

	public List<String> getDiscountList() {
		return discountList;
	}

	public void setDiscountList(List<String> discountList) {
		this.discountList = discountList;
	}

	public List<String> getCommissionList() {
		return commissionList;
	}

	public void setCommissionList(List<String> commissionList) {
		this.commissionList = commissionList;
	}

	List<String> productCodeCheckValueList = new ArrayList<String>();
	List<String> agentBusinessTypeList = new ArrayList<String>();
	List<String> agentCodeList = new ArrayList<String>();
	List<String> discountList = new ArrayList<String>();
	List<String> commissionList = new ArrayList<String>();

}
