package com.spring.VO;

import java.io.Serializable;

public class Dspmiadditionalcover implements Serializable {
	private static final long serialVersionUID = -1363279015615876583L;

	private String CATEGORY_CODE;
	private String CATEGORY_NAME;
	private String ADDITIONAL_COVER_CODE;
	private String ADDITIONAL_COVER_NAME;
	private String ADDITIONAL_COVER_TYPE;
	private String CATEGORY_DISPLAY;
	private String ADDONS_DISPLAY;
	private String MIN_AMOUNT;
	private String MAX_AMOUNT;
	private String COVER_CODE;
	private String FRONT_FLAG;
	private String TOOL_TIP;
	private String ADDONS_OPTION;
	private String DEFAULT_AMOUNT;
	private String STATUS;

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	public String getCATEGORY_CODE() {
		return CATEGORY_CODE;
	}

	public void setCATEGORY_CODE(String cATEGORY_CODE) {
		CATEGORY_CODE = cATEGORY_CODE;
	}

	public String getCATEGORY_NAME() {
		return CATEGORY_NAME;
	}

	public void setCATEGORY_NAME(String cATEGORY_NAME) {
		CATEGORY_NAME = cATEGORY_NAME;
	}

	public String getADDITIONAL_COVER_CODE() {
		return ADDITIONAL_COVER_CODE;
	}

	public void setADDITIONAL_COVER_CODE(String aDDITIONAL_COVER_CODE) {
		ADDITIONAL_COVER_CODE = aDDITIONAL_COVER_CODE;
	}

	public String getADDITIONAL_COVER_NAME() {
		return ADDITIONAL_COVER_NAME;
	}

	public void setADDITIONAL_COVER_NAME(String aDDITIONAL_COVER_NAME) {
		ADDITIONAL_COVER_NAME = aDDITIONAL_COVER_NAME;
	}

	public String getADDITIONAL_COVER_TYPE() {
		return ADDITIONAL_COVER_TYPE;
	}

	public void setADDITIONAL_COVER_TYPE(String aDDITIONAL_COVER_TYPE) {
		ADDITIONAL_COVER_TYPE = aDDITIONAL_COVER_TYPE;
	}

	public String getCATEGORY_DISPLAY() {
		return CATEGORY_DISPLAY;
	}

	public void setCATEGORY_DISPLAY(String cATEGORY_DISPLAY) {
		CATEGORY_DISPLAY = cATEGORY_DISPLAY;
	}

	public String getADDONS_DISPLAY() {
		return ADDONS_DISPLAY;
	}

	public void setADDONS_DISPLAY(String aDDONS_DISPLAY) {
		ADDONS_DISPLAY = aDDONS_DISPLAY;
	}

	public String getMIN_AMOUNT() {
		return MIN_AMOUNT;
	}

	public void setMIN_AMOUNT(String mIN_AMOUNT) {
		MIN_AMOUNT = mIN_AMOUNT;
	}

	public String getMAX_AMOUNT() {
		return MAX_AMOUNT;
	}

	public void setMAX_AMOUNT(String mAX_AMOUNT) {
		MAX_AMOUNT = mAX_AMOUNT;
	}

	public String getCOVER_CODE() {
		return COVER_CODE;
	}

	public void setCOVER_CODE(String cOVER_CODE) {
		COVER_CODE = cOVER_CODE;
	}

	public String getFRONT_FLAG() {
		return FRONT_FLAG;
	}

	public void setFRONT_FLAG(String fRONT_FLAG) {
		FRONT_FLAG = fRONT_FLAG;
	}

	public String getTOOL_TIP() {
		return TOOL_TIP;
	}

	public void setTOOL_TIP(String tOOL_TIP) {
		TOOL_TIP = tOOL_TIP;
	}

	public String getADDONS_OPTION() {
		return ADDONS_OPTION;
	}

	public void setADDONS_OPTION(String aDDONS_OPTION) {
		ADDONS_OPTION = aDDONS_OPTION;
	}

	public String getDEFAULT_AMOUNT() {
		return DEFAULT_AMOUNT;
	}

	public void setDEFAULT_AMOUNT(String dEFAULT_AMOUNT) {
		DEFAULT_AMOUNT = dEFAULT_AMOUNT;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}