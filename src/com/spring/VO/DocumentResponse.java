package com.spring.VO;

import java.util.ArrayList;
import java.util.List;

import com.etiqa.dsp.dao.common.pojo.GeneratingReportsResponseVo;

public class DocumentResponse {

	private String errorcode;
	private String errordesc;
	List<GeneratingReportsResponseVo> responsedoc = new ArrayList<GeneratingReportsResponseVo>();
	
	public String getErrorCode() {
		return errorcode;
	}
	public void setErrorCode(String errorcode) {
		this.errorcode = errorcode;
	}
	
	public String getErrorDesc() {
		return errordesc;
	}
	public void setErrorDesc(String errordesc) {
		this.errordesc = errordesc;
	}
	
	
	public List<GeneratingReportsResponseVo> getresponsedoc() {
		return responsedoc;
	}

	public void setresponsedoc(List<GeneratingReportsResponseVo> responsedoc) {
		this.responsedoc = responsedoc;
	}
}
