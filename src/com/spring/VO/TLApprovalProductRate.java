package com.spring.VO;

public class TLApprovalProductRate {

	private String minAmt;
	private String maxAmt;
	private String stampDuty;
	private String gstVal;
	private String tsarVal;
	private String discVal;
	private String minAge;
	private String maxAge;

	public String getMinAmt() {
		return minAmt;
	}

	public void setMinAmt(String minAmt) {
		this.minAmt = minAmt;
	}

	public String getMaxAmt() {
		return maxAmt;
	}

	public void setMaxAmt(String maxAmt) {
		this.maxAmt = maxAmt;
	}

	public String getStampDuty() {
		return stampDuty;
	}

	public void setStampDuty(String stampDuty) {
		this.stampDuty = stampDuty;
	}

	public String getGstVal() {
		return gstVal;
	}

	public void setGstVal(String gstVal) {
		this.gstVal = gstVal;
	}

	public String getTsarVal() {
		return tsarVal;
	}

	public void setTsarVal(String tsarVal) {
		this.tsarVal = tsarVal;
	}

	public String getDiscVal() {
		return discVal;
	}

	public void setDiscVal(String discVal) {
		this.discVal = discVal;
	}

	public String getMinAge() {
		return minAge;
	}

	public void setMinAge(String minAge) {
		this.minAge = minAge;
	}

	public String getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(String maxAge) {
		this.maxAge = maxAge;
	}

}
