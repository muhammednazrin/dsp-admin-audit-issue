package com.spring.VO;

import java.io.Serializable;

public class TCProdMngPojo implements Serializable {

	private String annualTarget;
	private String validity;

	private String prodType = "0";
	private String recIdDomestic;

	private String daysRangeKeycodeDomestic;
	private String destinationCodeAreaDomestic;
	private String packageCodeDomestic;
	private String premiumValDomestic;
	private String planCodeDomestic;
	private String travelWithTypeIdDomestic;

	private String recIdInternational;
	private String daysRangeKeycodeInternational;
	private String destinationCodeAreaInternational;
	private String packageCodeInternational;
	private String premiumValInternational;
	private String planCodeInternational;
	private String travelWithTypeIdInternational;

	private String discount;
	private String gst;
	private String gstEffDate;
	private String sst;
	private String sstEffDate;
	private String stampDuty;

	public String getProdType() {
		return prodType;
	}

	public void setProdType(String prodType) {
		this.prodType = prodType;
	}

	public String getRecIdDomestic() {
		return recIdDomestic;
	}

	public void setRecIdDomestic(String recIdDomestic) {
		this.recIdDomestic = recIdDomestic;
	}

	public String getDaysRangeKeycodeDomestic() {
		return daysRangeKeycodeDomestic;
	}

	public void setDaysRangeKeycodeDomestic(String daysRangeKeycodeDomestic) {
		this.daysRangeKeycodeDomestic = daysRangeKeycodeDomestic;
	}

	public String getDestinationCodeAreaDomestic() {
		return destinationCodeAreaDomestic;
	}

	public void setDestinationCodeAreaDomestic(String destinationCodeAreaDomestic) {
		this.destinationCodeAreaDomestic = destinationCodeAreaDomestic;
	}

	public String getAnnualTarget() {
		return annualTarget;
	}

	public void setAnnualTarget(String annualTarget) {
		this.annualTarget = annualTarget;
	}

	public String getValidity() {
		return validity;
	}

	public void setValidity(String validity) {
		this.validity = validity;
	}

	public String getPackageCodeDomestic() {
		return packageCodeDomestic;
	}

	public void setPackageCodeDomestic(String packageCodeDomestic) {
		this.packageCodeDomestic = packageCodeDomestic;
	}

	public String getPremiumValDomestic() {
		return premiumValDomestic;
	}

	public void setPremiumValDomestic(String premiumValDomestic) {
		this.premiumValDomestic = premiumValDomestic;
	}

	public String getPlanCodeDomestic() {
		return planCodeDomestic;
	}

	public void setPlanCodeDomestic(String planCodeDomestic) {
		this.planCodeDomestic = planCodeDomestic;
	}

	public String getTravelWithTypeIdDomestic() {
		return travelWithTypeIdDomestic;
	}

	public void setTravelWithTypeIdDomestic(String travelWithTypeIdDomestic) {
		this.travelWithTypeIdDomestic = travelWithTypeIdDomestic;
	}

	public String getRecIdInternational() {
		return recIdInternational;
	}

	public void setRecIdInternational(String recIdInternational) {
		this.recIdInternational = recIdInternational;
	}

	public String getDaysRangeKeycodeInternational() {
		return daysRangeKeycodeInternational;
	}

	public void setDaysRangeKeycodeInternational(String daysRangeKeycodeInternational) {
		this.daysRangeKeycodeInternational = daysRangeKeycodeInternational;
	}

	public String getDestinationCodeAreaInternational() {
		return destinationCodeAreaInternational;
	}

	public void setDestinationCodeAreaInternational(String destinationCodeAreaInternational) {
		this.destinationCodeAreaInternational = destinationCodeAreaInternational;
	}

	public String getPackageCodeInternational() {
		return packageCodeInternational;
	}

	public void setPackageCodeInternational(String packageCodeInternational) {
		this.packageCodeInternational = packageCodeInternational;
	}

	public String getPremiumValInternational() {
		return premiumValInternational;
	}

	public void setPremiumValInternational(String premiumValInternational) {
		this.premiumValInternational = premiumValInternational;
	}

	public String getPlanCodeInternational() {
		return planCodeInternational;
	}

	public void setPlanCodeInternational(String planCodeInternational) {
		this.planCodeInternational = planCodeInternational;
	}

	public String getTravelWithTypeIdInternational() {
		return travelWithTypeIdInternational;
	}

	public void setTravelWithTypeIdInternational(String travelWithTypeIdInternational) {
		this.travelWithTypeIdInternational = travelWithTypeIdInternational;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getGst() {
		return gst;
	}

	public void setGst(String gst) {
		this.gst = gst;
	}

	public String getGstEffDate() {
		return gstEffDate;
	}

	public void setGstEffDate(String gstEffDate) {
		this.gstEffDate = gstEffDate;
	}

	public String getSst() {
		return sst;
	}

	public void setSst(String sst) {
		this.sst = sst;
	}

	public String getSstEffDate() {
		return sstEffDate;
	}

	public void setSstEffDate(String sstEffDate) {
		this.sstEffDate = sstEffDate;
	}

	public String getStampDuty() {
		return stampDuty;
	}

	public void setStampDuty(String stampDuty) {
		this.stampDuty = stampDuty;
	}

}
