package com.spring.VO;

public class MotorCatagory {
	private String CATEGORY_CODE;
	private String CATEGORY_NAME;
	private String CATEGORY_DISPLAY;
	private String ADDONS_OPTION;
	private String STATUS;
	private String SEQUENCE_COUNT;
	private int Sequence;

	public String getADDONS_OPTION() {
		return ADDONS_OPTION;
	}

	public void setADDONS_OPTION(String aDDONS_OPTION) {
		ADDONS_OPTION = aDDONS_OPTION;
	}

	public String getSEQUENCE_COUNT() {
		return SEQUENCE_COUNT;
	}

	public void setSEQUENCE_COUNT(String sEQUENCE_COUNT) {
		SEQUENCE_COUNT = sEQUENCE_COUNT;
	}

	public int getSequence() {
		return Sequence;
	}

	public void setSequence(int sequence) {
		Sequence = sequence;
	}

	public String getCATEGORY_CODE() {
		return CATEGORY_CODE;
	}

	public void setCATEGORY_CODE(String cATEGORY_CODE) {
		CATEGORY_CODE = cATEGORY_CODE;
	}

	public String getCATEGORY_NAME() {
		return CATEGORY_NAME;
	}

	public void setCATEGORY_NAME(String cATEGORY_NAME) {
		CATEGORY_NAME = cATEGORY_NAME;
	}

	public String getCATEGORY_DISPLAY() {
		return CATEGORY_DISPLAY;
	}

	public void setCATEGORY_DISPLAY(String cATEGORY_DISPLAY) {
		CATEGORY_DISPLAY = cATEGORY_DISPLAY;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

}
