package com.spring.VO;

import java.math.BigDecimal;

public class HOHHProductInfoApproval {

	private BigDecimal annualTarget;
	private BigDecimal validity;
	private String newAnnualTarget;
	private String newValidity;

	public String getNewAnnualTarget() {
		return newAnnualTarget;
	}

	public void setNewAnnualTarget(String newAnnualTarget) {
		this.newAnnualTarget = newAnnualTarget;
	}

	public String getNewValidity() {
		return newValidity;
	}

	public void setNewValidity(String newValidity) {
		this.newValidity = newValidity;
	}

	public BigDecimal getAnnualTarget() {
		return annualTarget;
	}

	public void setAnnualTarget(BigDecimal annualTarget) {
		this.annualTarget = annualTarget;
	}

	public BigDecimal getValidity() {
		return validity;
	}

	public void setValidity(BigDecimal validity) {
		this.validity = validity;
	}

}
