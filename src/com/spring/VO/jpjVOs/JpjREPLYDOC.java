package com.spring.VO.jpjVOs;

public class JpjREPLYDOC {
	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_REPLYDOC.ID
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	private Short id;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_REPLYDOC.COMPCODE
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	private String compcode;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_REPLYDOC.DOCNO
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	private String docno;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_REPLYDOC.VEHREGNO
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	private String vehregno;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_REPLYDOC.DOCTYPE
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	private String doctype;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_REPLYDOC.REASONCODE
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	private String reasoncode;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_REPLYDOC.STATUSCODE
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	private String statuscode;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_REPLYDOC.ERRORCODE
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	private String errorcode;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_REPLYDOC.INSERTTIMESTAMP
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	private String inserttimestamp;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_MI_TBL_REPLYDOC.MESSAGE
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	private String message;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_REPLYDOC.ID
	 *
	 * @return the value of DSP_MI_TBL_REPLYDOC.ID
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public Short getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_REPLYDOC.ID
	 *
	 * @param id
	 *            the value for DSP_MI_TBL_REPLYDOC.ID
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public void setId(Short id) {
		this.id = id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_REPLYDOC.COMPCODE
	 *
	 * @return the value of DSP_MI_TBL_REPLYDOC.COMPCODE
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public String getCompcode() {
		return compcode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_REPLYDOC.COMPCODE
	 *
	 * @param compcode
	 *            the value for DSP_MI_TBL_REPLYDOC.COMPCODE
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public void setCompcode(String compcode) {
		this.compcode = compcode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_REPLYDOC.DOCNO
	 *
	 * @return the value of DSP_MI_TBL_REPLYDOC.DOCNO
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public String getDocno() {
		return docno;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_REPLYDOC.DOCNO
	 *
	 * @param docno
	 *            the value for DSP_MI_TBL_REPLYDOC.DOCNO
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public void setDocno(String docno) {
		this.docno = docno;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_REPLYDOC.VEHREGNO
	 *
	 * @return the value of DSP_MI_TBL_REPLYDOC.VEHREGNO
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public String getVehregno() {
		return vehregno;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_REPLYDOC.VEHREGNO
	 *
	 * @param vehregno
	 *            the value for DSP_MI_TBL_REPLYDOC.VEHREGNO
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public void setVehregno(String vehregno) {
		this.vehregno = vehregno;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_REPLYDOC.DOCTYPE
	 *
	 * @return the value of DSP_MI_TBL_REPLYDOC.DOCTYPE
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public String getDoctype() {
		return doctype;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_REPLYDOC.DOCTYPE
	 *
	 * @param doctype
	 *            the value for DSP_MI_TBL_REPLYDOC.DOCTYPE
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public void setDoctype(String doctype) {
		this.doctype = doctype;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_REPLYDOC.REASONCODE
	 *
	 * @return the value of DSP_MI_TBL_REPLYDOC.REASONCODE
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public String getReasoncode() {
		return reasoncode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_REPLYDOC.REASONCODE
	 *
	 * @param reasoncode
	 *            the value for DSP_MI_TBL_REPLYDOC.REASONCODE
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public void setReasoncode(String reasoncode) {
		this.reasoncode = reasoncode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_REPLYDOC.STATUSCODE
	 *
	 * @return the value of DSP_MI_TBL_REPLYDOC.STATUSCODE
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public String getStatuscode() {
		return statuscode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_REPLYDOC.STATUSCODE
	 *
	 * @param statuscode
	 *            the value for DSP_MI_TBL_REPLYDOC.STATUSCODE
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public void setStatuscode(String statuscode) {
		this.statuscode = statuscode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_REPLYDOC.ERRORCODE
	 *
	 * @return the value of DSP_MI_TBL_REPLYDOC.ERRORCODE
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public String getErrorcode() {
		return errorcode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_REPLYDOC.ERRORCODE
	 *
	 * @param errorcode
	 *            the value for DSP_MI_TBL_REPLYDOC.ERRORCODE
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_REPLYDOC.INSERTTIMESTAMP
	 *
	 * @return the value of DSP_MI_TBL_REPLYDOC.INSERTTIMESTAMP
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public String getInserttimestamp() {
		return inserttimestamp;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_REPLYDOC.INSERTTIMESTAMP
	 *
	 * @param inserttimestamp
	 *            the value for DSP_MI_TBL_REPLYDOC.INSERTTIMESTAMP
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public void setInserttimestamp(String inserttimestamp) {
		this.inserttimestamp = inserttimestamp;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_MI_TBL_REPLYDOC.MESSAGE
	 *
	 * @return the value of DSP_MI_TBL_REPLYDOC.MESSAGE
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_MI_TBL_REPLYDOC.MESSAGE
	 *
	 * @param message
	 *            the value for DSP_MI_TBL_REPLYDOC.MESSAGE
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public void setMessage(String message) {
		this.message = message;
	}
}