package com.spring.VO.jpjVOs;

import java.util.ArrayList;
import java.util.List;

public class JpjREPLYDOCExample {
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database table DSP_MI_TBL_REPLYDOC
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	protected String orderByClause;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database table DSP_MI_TBL_REPLYDOC
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	protected boolean distinct;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database table DSP_MI_TBL_REPLYDOC
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	protected List<Criteria> oredCriteria;

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_REPLYDOC
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public JpjREPLYDOCExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_REPLYDOC
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_REPLYDOC
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public String getOrderByClause() {
		return orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_REPLYDOC
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_REPLYDOC
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public boolean isDistinct() {
		return distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_REPLYDOC
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_REPLYDOC
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_REPLYDOC
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_REPLYDOC
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_REPLYDOC
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_REPLYDOC
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the
	 * database table DSP_MI_TBL_REPLYDOC
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("ID is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("ID is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(Short value) {
			addCriterion("ID =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(Short value) {
			addCriterion("ID <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(Short value) {
			addCriterion("ID >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(Short value) {
			addCriterion("ID >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(Short value) {
			addCriterion("ID <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(Short value) {
			addCriterion("ID <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<Short> values) {
			addCriterion("ID in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<Short> values) {
			addCriterion("ID not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(Short value1, Short value2) {
			addCriterion("ID between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(Short value1, Short value2) {
			addCriterion("ID not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andCompcodeIsNull() {
			addCriterion("COMPCODE is null");
			return (Criteria) this;
		}

		public Criteria andCompcodeIsNotNull() {
			addCriterion("COMPCODE is not null");
			return (Criteria) this;
		}

		public Criteria andCompcodeEqualTo(String value) {
			addCriterion("COMPCODE =", value, "compcode");
			return (Criteria) this;
		}

		public Criteria andCompcodeNotEqualTo(String value) {
			addCriterion("COMPCODE <>", value, "compcode");
			return (Criteria) this;
		}

		public Criteria andCompcodeGreaterThan(String value) {
			addCriterion("COMPCODE >", value, "compcode");
			return (Criteria) this;
		}

		public Criteria andCompcodeGreaterThanOrEqualTo(String value) {
			addCriterion("COMPCODE >=", value, "compcode");
			return (Criteria) this;
		}

		public Criteria andCompcodeLessThan(String value) {
			addCriterion("COMPCODE <", value, "compcode");
			return (Criteria) this;
		}

		public Criteria andCompcodeLessThanOrEqualTo(String value) {
			addCriterion("COMPCODE <=", value, "compcode");
			return (Criteria) this;
		}

		public Criteria andCompcodeLike(String value) {
			addCriterion("COMPCODE like", value, "compcode");
			return (Criteria) this;
		}

		public Criteria andCompcodeNotLike(String value) {
			addCriterion("COMPCODE not like", value, "compcode");
			return (Criteria) this;
		}

		public Criteria andCompcodeIn(List<String> values) {
			addCriterion("COMPCODE in", values, "compcode");
			return (Criteria) this;
		}

		public Criteria andCompcodeNotIn(List<String> values) {
			addCriterion("COMPCODE not in", values, "compcode");
			return (Criteria) this;
		}

		public Criteria andCompcodeBetween(String value1, String value2) {
			addCriterion("COMPCODE between", value1, value2, "compcode");
			return (Criteria) this;
		}

		public Criteria andCompcodeNotBetween(String value1, String value2) {
			addCriterion("COMPCODE not between", value1, value2, "compcode");
			return (Criteria) this;
		}

		public Criteria andDocnoIsNull() {
			addCriterion("DOCNO is null");
			return (Criteria) this;
		}

		public Criteria andDocnoIsNotNull() {
			addCriterion("DOCNO is not null");
			return (Criteria) this;
		}

		public Criteria andDocnoEqualTo(String value) {
			addCriterion("DOCNO =", value, "docno");
			return (Criteria) this;
		}

		public Criteria andDocnoNotEqualTo(String value) {
			addCriterion("DOCNO <>", value, "docno");
			return (Criteria) this;
		}

		public Criteria andDocnoGreaterThan(String value) {
			addCriterion("DOCNO >", value, "docno");
			return (Criteria) this;
		}

		public Criteria andDocnoGreaterThanOrEqualTo(String value) {
			addCriterion("DOCNO >=", value, "docno");
			return (Criteria) this;
		}

		public Criteria andDocnoLessThan(String value) {
			addCriterion("DOCNO <", value, "docno");
			return (Criteria) this;
		}

		public Criteria andDocnoLessThanOrEqualTo(String value) {
			addCriterion("DOCNO <=", value, "docno");
			return (Criteria) this;
		}

		public Criteria andDocnoLike(String value) {
			addCriterion("DOCNO like", value, "docno");
			return (Criteria) this;
		}

		public Criteria andDocnoNotLike(String value) {
			addCriterion("DOCNO not like", value, "docno");
			return (Criteria) this;
		}

		public Criteria andDocnoIn(List<String> values) {
			addCriterion("DOCNO in", values, "docno");
			return (Criteria) this;
		}

		public Criteria andDocnoNotIn(List<String> values) {
			addCriterion("DOCNO not in", values, "docno");
			return (Criteria) this;
		}

		public Criteria andDocnoBetween(String value1, String value2) {
			addCriterion("DOCNO between", value1, value2, "docno");
			return (Criteria) this;
		}

		public Criteria andDocnoNotBetween(String value1, String value2) {
			addCriterion("DOCNO not between", value1, value2, "docno");
			return (Criteria) this;
		}

		public Criteria andVehregnoIsNull() {
			addCriterion("VEHREGNO is null");
			return (Criteria) this;
		}

		public Criteria andVehregnoIsNotNull() {
			addCriterion("VEHREGNO is not null");
			return (Criteria) this;
		}

		public Criteria andVehregnoEqualTo(String value) {
			addCriterion("VEHREGNO =", value, "vehregno");
			return (Criteria) this;
		}

		public Criteria andVehregnoNotEqualTo(String value) {
			addCriterion("VEHREGNO <>", value, "vehregno");
			return (Criteria) this;
		}

		public Criteria andVehregnoGreaterThan(String value) {
			addCriterion("VEHREGNO >", value, "vehregno");
			return (Criteria) this;
		}

		public Criteria andVehregnoGreaterThanOrEqualTo(String value) {
			addCriterion("VEHREGNO >=", value, "vehregno");
			return (Criteria) this;
		}

		public Criteria andVehregnoLessThan(String value) {
			addCriterion("VEHREGNO <", value, "vehregno");
			return (Criteria) this;
		}

		public Criteria andVehregnoLessThanOrEqualTo(String value) {
			addCriterion("VEHREGNO <=", value, "vehregno");
			return (Criteria) this;
		}

		public Criteria andVehregnoLike(String value) {
			addCriterion("VEHREGNO like", value, "vehregno");
			return (Criteria) this;
		}

		public Criteria andVehregnoNotLike(String value) {
			addCriterion("VEHREGNO not like", value, "vehregno");
			return (Criteria) this;
		}

		public Criteria andVehregnoIn(List<String> values) {
			addCriterion("VEHREGNO in", values, "vehregno");
			return (Criteria) this;
		}

		public Criteria andVehregnoNotIn(List<String> values) {
			addCriterion("VEHREGNO not in", values, "vehregno");
			return (Criteria) this;
		}

		public Criteria andVehregnoBetween(String value1, String value2) {
			addCriterion("VEHREGNO between", value1, value2, "vehregno");
			return (Criteria) this;
		}

		public Criteria andVehregnoNotBetween(String value1, String value2) {
			addCriterion("VEHREGNO not between", value1, value2, "vehregno");
			return (Criteria) this;
		}

		public Criteria andDoctypeIsNull() {
			addCriterion("DOCTYPE is null");
			return (Criteria) this;
		}

		public Criteria andDoctypeIsNotNull() {
			addCriterion("DOCTYPE is not null");
			return (Criteria) this;
		}

		public Criteria andDoctypeEqualTo(String value) {
			addCriterion("DOCTYPE =", value, "doctype");
			return (Criteria) this;
		}

		public Criteria andDoctypeNotEqualTo(String value) {
			addCriterion("DOCTYPE <>", value, "doctype");
			return (Criteria) this;
		}

		public Criteria andDoctypeGreaterThan(String value) {
			addCriterion("DOCTYPE >", value, "doctype");
			return (Criteria) this;
		}

		public Criteria andDoctypeGreaterThanOrEqualTo(String value) {
			addCriterion("DOCTYPE >=", value, "doctype");
			return (Criteria) this;
		}

		public Criteria andDoctypeLessThan(String value) {
			addCriterion("DOCTYPE <", value, "doctype");
			return (Criteria) this;
		}

		public Criteria andDoctypeLessThanOrEqualTo(String value) {
			addCriterion("DOCTYPE <=", value, "doctype");
			return (Criteria) this;
		}

		public Criteria andDoctypeLike(String value) {
			addCriterion("DOCTYPE like", value, "doctype");
			return (Criteria) this;
		}

		public Criteria andDoctypeNotLike(String value) {
			addCriterion("DOCTYPE not like", value, "doctype");
			return (Criteria) this;
		}

		public Criteria andDoctypeIn(List<String> values) {
			addCriterion("DOCTYPE in", values, "doctype");
			return (Criteria) this;
		}

		public Criteria andDoctypeNotIn(List<String> values) {
			addCriterion("DOCTYPE not in", values, "doctype");
			return (Criteria) this;
		}

		public Criteria andDoctypeBetween(String value1, String value2) {
			addCriterion("DOCTYPE between", value1, value2, "doctype");
			return (Criteria) this;
		}

		public Criteria andDoctypeNotBetween(String value1, String value2) {
			addCriterion("DOCTYPE not between", value1, value2, "doctype");
			return (Criteria) this;
		}

		public Criteria andReasoncodeIsNull() {
			addCriterion("REASONCODE is null");
			return (Criteria) this;
		}

		public Criteria andReasoncodeIsNotNull() {
			addCriterion("REASONCODE is not null");
			return (Criteria) this;
		}

		public Criteria andReasoncodeEqualTo(String value) {
			addCriterion("REASONCODE =", value, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andReasoncodeNotEqualTo(String value) {
			addCriterion("REASONCODE <>", value, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andReasoncodeGreaterThan(String value) {
			addCriterion("REASONCODE >", value, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andReasoncodeGreaterThanOrEqualTo(String value) {
			addCriterion("REASONCODE >=", value, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andReasoncodeLessThan(String value) {
			addCriterion("REASONCODE <", value, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andReasoncodeLessThanOrEqualTo(String value) {
			addCriterion("REASONCODE <=", value, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andReasoncodeLike(String value) {
			addCriterion("REASONCODE like", value, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andReasoncodeNotLike(String value) {
			addCriterion("REASONCODE not like", value, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andReasoncodeIn(List<String> values) {
			addCriterion("REASONCODE in", values, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andReasoncodeNotIn(List<String> values) {
			addCriterion("REASONCODE not in", values, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andReasoncodeBetween(String value1, String value2) {
			addCriterion("REASONCODE between", value1, value2, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andReasoncodeNotBetween(String value1, String value2) {
			addCriterion("REASONCODE not between", value1, value2, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andStatuscodeIsNull() {
			addCriterion("STATUSCODE is null");
			return (Criteria) this;
		}

		public Criteria andStatuscodeIsNotNull() {
			addCriterion("STATUSCODE is not null");
			return (Criteria) this;
		}

		public Criteria andStatuscodeEqualTo(String value) {
			addCriterion("STATUSCODE =", value, "statuscode");
			return (Criteria) this;
		}

		public Criteria andStatuscodeNotEqualTo(String value) {
			addCriterion("STATUSCODE <>", value, "statuscode");
			return (Criteria) this;
		}

		public Criteria andStatuscodeGreaterThan(String value) {
			addCriterion("STATUSCODE >", value, "statuscode");
			return (Criteria) this;
		}

		public Criteria andStatuscodeGreaterThanOrEqualTo(String value) {
			addCriterion("STATUSCODE >=", value, "statuscode");
			return (Criteria) this;
		}

		public Criteria andStatuscodeLessThan(String value) {
			addCriterion("STATUSCODE <", value, "statuscode");
			return (Criteria) this;
		}

		public Criteria andStatuscodeLessThanOrEqualTo(String value) {
			addCriterion("STATUSCODE <=", value, "statuscode");
			return (Criteria) this;
		}

		public Criteria andStatuscodeLike(String value) {
			addCriterion("STATUSCODE like", value, "statuscode");
			return (Criteria) this;
		}

		public Criteria andStatuscodeNotLike(String value) {
			addCriterion("STATUSCODE not like", value, "statuscode");
			return (Criteria) this;
		}

		public Criteria andStatuscodeIn(List<String> values) {
			addCriterion("STATUSCODE in", values, "statuscode");
			return (Criteria) this;
		}

		public Criteria andStatuscodeNotIn(List<String> values) {
			addCriterion("STATUSCODE not in", values, "statuscode");
			return (Criteria) this;
		}

		public Criteria andStatuscodeBetween(String value1, String value2) {
			addCriterion("STATUSCODE between", value1, value2, "statuscode");
			return (Criteria) this;
		}

		public Criteria andStatuscodeNotBetween(String value1, String value2) {
			addCriterion("STATUSCODE not between", value1, value2, "statuscode");
			return (Criteria) this;
		}

		public Criteria andErrorcodeIsNull() {
			addCriterion("ERRORCODE is null");
			return (Criteria) this;
		}

		public Criteria andErrorcodeIsNotNull() {
			addCriterion("ERRORCODE is not null");
			return (Criteria) this;
		}

		public Criteria andErrorcodeEqualTo(String value) {
			addCriterion("ERRORCODE =", value, "errorcode");
			return (Criteria) this;
		}

		public Criteria andErrorcodeNotEqualTo(String value) {
			addCriterion("ERRORCODE <>", value, "errorcode");
			return (Criteria) this;
		}

		public Criteria andErrorcodeGreaterThan(String value) {
			addCriterion("ERRORCODE >", value, "errorcode");
			return (Criteria) this;
		}

		public Criteria andErrorcodeGreaterThanOrEqualTo(String value) {
			addCriterion("ERRORCODE >=", value, "errorcode");
			return (Criteria) this;
		}

		public Criteria andErrorcodeLessThan(String value) {
			addCriterion("ERRORCODE <", value, "errorcode");
			return (Criteria) this;
		}

		public Criteria andErrorcodeLessThanOrEqualTo(String value) {
			addCriterion("ERRORCODE <=", value, "errorcode");
			return (Criteria) this;
		}

		public Criteria andErrorcodeLike(String value) {
			addCriterion("ERRORCODE like", value, "errorcode");
			return (Criteria) this;
		}

		public Criteria andErrorcodeNotLike(String value) {
			addCriterion("ERRORCODE not like", value, "errorcode");
			return (Criteria) this;
		}

		public Criteria andErrorcodeIn(List<String> values) {
			addCriterion("ERRORCODE in", values, "errorcode");
			return (Criteria) this;
		}

		public Criteria andErrorcodeNotIn(List<String> values) {
			addCriterion("ERRORCODE not in", values, "errorcode");
			return (Criteria) this;
		}

		public Criteria andErrorcodeBetween(String value1, String value2) {
			addCriterion("ERRORCODE between", value1, value2, "errorcode");
			return (Criteria) this;
		}

		public Criteria andErrorcodeNotBetween(String value1, String value2) {
			addCriterion("ERRORCODE not between", value1, value2, "errorcode");
			return (Criteria) this;
		}

		public Criteria andInserttimestampIsNull() {
			addCriterion("INSERTTIMESTAMP is null");
			return (Criteria) this;
		}

		public Criteria andInserttimestampIsNotNull() {
			addCriterion("INSERTTIMESTAMP is not null");
			return (Criteria) this;
		}

		public Criteria andInserttimestampEqualTo(String value) {
			addCriterion("INSERTTIMESTAMP =", value, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andInserttimestampNotEqualTo(String value) {
			addCriterion("INSERTTIMESTAMP <>", value, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andInserttimestampGreaterThan(String value) {
			addCriterion("INSERTTIMESTAMP >", value, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andInserttimestampGreaterThanOrEqualTo(String value) {
			addCriterion("INSERTTIMESTAMP >=", value, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andInserttimestampLessThan(String value) {
			addCriterion("INSERTTIMESTAMP <", value, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andInserttimestampLessThanOrEqualTo(String value) {
			addCriterion("INSERTTIMESTAMP <=", value, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andInserttimestampLike(String value) {
			addCriterion("INSERTTIMESTAMP like", value, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andInserttimestampNotLike(String value) {
			addCriterion("INSERTTIMESTAMP not like", value, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andInserttimestampIn(List<String> values) {
			addCriterion("INSERTTIMESTAMP in", values, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andInserttimestampNotIn(List<String> values) {
			addCriterion("INSERTTIMESTAMP not in", values, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andInserttimestampBetween(String value1, String value2) {
			addCriterion("INSERTTIMESTAMP between", value1, value2, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andInserttimestampNotBetween(String value1, String value2) {
			addCriterion("INSERTTIMESTAMP not between", value1, value2, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andMessageIsNull() {
			addCriterion("MESSAGE is null");
			return (Criteria) this;
		}

		public Criteria andMessageIsNotNull() {
			addCriterion("MESSAGE is not null");
			return (Criteria) this;
		}

		public Criteria andMessageEqualTo(String value) {
			addCriterion("MESSAGE =", value, "message");
			return (Criteria) this;
		}

		public Criteria andMessageNotEqualTo(String value) {
			addCriterion("MESSAGE <>", value, "message");
			return (Criteria) this;
		}

		public Criteria andMessageGreaterThan(String value) {
			addCriterion("MESSAGE >", value, "message");
			return (Criteria) this;
		}

		public Criteria andMessageGreaterThanOrEqualTo(String value) {
			addCriterion("MESSAGE >=", value, "message");
			return (Criteria) this;
		}

		public Criteria andMessageLessThan(String value) {
			addCriterion("MESSAGE <", value, "message");
			return (Criteria) this;
		}

		public Criteria andMessageLessThanOrEqualTo(String value) {
			addCriterion("MESSAGE <=", value, "message");
			return (Criteria) this;
		}

		public Criteria andMessageLike(String value) {
			addCriterion("MESSAGE like", value, "message");
			return (Criteria) this;
		}

		public Criteria andMessageNotLike(String value) {
			addCriterion("MESSAGE not like", value, "message");
			return (Criteria) this;
		}

		public Criteria andMessageIn(List<String> values) {
			addCriterion("MESSAGE in", values, "message");
			return (Criteria) this;
		}

		public Criteria andMessageNotIn(List<String> values) {
			addCriterion("MESSAGE not in", values, "message");
			return (Criteria) this;
		}

		public Criteria andMessageBetween(String value1, String value2) {
			addCriterion("MESSAGE between", value1, value2, "message");
			return (Criteria) this;
		}

		public Criteria andMessageNotBetween(String value1, String value2) {
			addCriterion("MESSAGE not between", value1, value2, "message");
			return (Criteria) this;
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the
	 * database table DSP_MI_TBL_REPLYDOC
	 *
	 * @mbggenerated do_not_delete_during_merge Thu Aug 17 13:36:38 SGT 2017
	 */
	public static class Criteria extends GeneratedCriteria {

		protected Criteria() {
			super();
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the
	 * database table DSP_MI_TBL_REPLYDOC
	 *
	 * @mbggenerated Thu Aug 17 13:36:38 SGT 2017
	 */
	public static class Criterion {
		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}
}