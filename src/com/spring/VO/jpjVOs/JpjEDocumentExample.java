package com.spring.VO.jpjVOs;

import java.util.ArrayList;
import java.util.List;

public class JpjEDocumentExample {
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database table DSP_MI_TBL_EDOCUMENT
	 *
	 * @mbggenerated Thu Aug 17 13:35:05 SGT 2017
	 */
	protected String orderByClause;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database table DSP_MI_TBL_EDOCUMENT
	 *
	 * @mbggenerated Thu Aug 17 13:35:05 SGT 2017
	 */
	protected boolean distinct;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database table DSP_MI_TBL_EDOCUMENT
	 *
	 * @mbggenerated Thu Aug 17 13:35:05 SGT 2017
	 */
	protected List<Criteria> oredCriteria;

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_EDOCUMENT
	 *
	 * @mbggenerated Thu Aug 17 13:35:05 SGT 2017
	 */
	public JpjEDocumentExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_EDOCUMENT
	 *
	 * @mbggenerated Thu Aug 17 13:35:05 SGT 2017
	 */
	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_EDOCUMENT
	 *
	 * @mbggenerated Thu Aug 17 13:35:05 SGT 2017
	 */
	public String getOrderByClause() {
		return orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_EDOCUMENT
	 *
	 * @mbggenerated Thu Aug 17 13:35:05 SGT 2017
	 */
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_EDOCUMENT
	 *
	 * @mbggenerated Thu Aug 17 13:35:05 SGT 2017
	 */
	public boolean isDistinct() {
		return distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_EDOCUMENT
	 *
	 * @mbggenerated Thu Aug 17 13:35:05 SGT 2017
	 */
	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_EDOCUMENT
	 *
	 * @mbggenerated Thu Aug 17 13:35:05 SGT 2017
	 */
	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_EDOCUMENT
	 *
	 * @mbggenerated Thu Aug 17 13:35:05 SGT 2017
	 */
	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_EDOCUMENT
	 *
	 * @mbggenerated Thu Aug 17 13:35:05 SGT 2017
	 */
	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_EDOCUMENT
	 *
	 * @mbggenerated Thu Aug 17 13:35:05 SGT 2017
	 */
	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_EDOCUMENT
	 *
	 * @mbggenerated Thu Aug 17 13:35:05 SGT 2017
	 */
	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the
	 * database table DSP_MI_TBL_EDOCUMENT
	 *
	 * @mbggenerated Thu Aug 17 13:35:05 SGT 2017
	 */
	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("ID is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("ID is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(Short value) {
			addCriterion("ID =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(Short value) {
			addCriterion("ID <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(Short value) {
			addCriterion("ID >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(Short value) {
			addCriterion("ID >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(Short value) {
			addCriterion("ID <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(Short value) {
			addCriterion("ID <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<Short> values) {
			addCriterion("ID in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<Short> values) {
			addCriterion("ID not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(Short value1, Short value2) {
			addCriterion("ID between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(Short value1, Short value2) {
			addCriterion("ID not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andCompcodeIsNull() {
			addCriterion("COMPCODE is null");
			return (Criteria) this;
		}

		public Criteria andCompcodeIsNotNull() {
			addCriterion("COMPCODE is not null");
			return (Criteria) this;
		}

		public Criteria andCompcodeEqualTo(String value) {
			addCriterion("COMPCODE =", value, "compcode");
			return (Criteria) this;
		}

		public Criteria andCompcodeNotEqualTo(String value) {
			addCriterion("COMPCODE <>", value, "compcode");
			return (Criteria) this;
		}

		public Criteria andCompcodeGreaterThan(String value) {
			addCriterion("COMPCODE >", value, "compcode");
			return (Criteria) this;
		}

		public Criteria andCompcodeGreaterThanOrEqualTo(String value) {
			addCriterion("COMPCODE >=", value, "compcode");
			return (Criteria) this;
		}

		public Criteria andCompcodeLessThan(String value) {
			addCriterion("COMPCODE <", value, "compcode");
			return (Criteria) this;
		}

		public Criteria andCompcodeLessThanOrEqualTo(String value) {
			addCriterion("COMPCODE <=", value, "compcode");
			return (Criteria) this;
		}

		public Criteria andCompcodeLike(String value) {
			addCriterion("COMPCODE like", value, "compcode");
			return (Criteria) this;
		}

		public Criteria andCompcodeNotLike(String value) {
			addCriterion("COMPCODE not like", value, "compcode");
			return (Criteria) this;
		}

		public Criteria andCompcodeIn(List<String> values) {
			addCriterion("COMPCODE in", values, "compcode");
			return (Criteria) this;
		}

		public Criteria andCompcodeNotIn(List<String> values) {
			addCriterion("COMPCODE not in", values, "compcode");
			return (Criteria) this;
		}

		public Criteria andCompcodeBetween(String value1, String value2) {
			addCriterion("COMPCODE between", value1, value2, "compcode");
			return (Criteria) this;
		}

		public Criteria andCompcodeNotBetween(String value1, String value2) {
			addCriterion("COMPCODE not between", value1, value2, "compcode");
			return (Criteria) this;
		}

		public Criteria andVehregnoIsNull() {
			addCriterion("VEHREGNO is null");
			return (Criteria) this;
		}

		public Criteria andVehregnoIsNotNull() {
			addCriterion("VEHREGNO is not null");
			return (Criteria) this;
		}

		public Criteria andVehregnoEqualTo(String value) {
			addCriterion("VEHREGNO =", value, "vehregno");
			return (Criteria) this;
		}

		public Criteria andVehregnoNotEqualTo(String value) {
			addCriterion("VEHREGNO <>", value, "vehregno");
			return (Criteria) this;
		}

		public Criteria andVehregnoGreaterThan(String value) {
			addCriterion("VEHREGNO >", value, "vehregno");
			return (Criteria) this;
		}

		public Criteria andVehregnoGreaterThanOrEqualTo(String value) {
			addCriterion("VEHREGNO >=", value, "vehregno");
			return (Criteria) this;
		}

		public Criteria andVehregnoLessThan(String value) {
			addCriterion("VEHREGNO <", value, "vehregno");
			return (Criteria) this;
		}

		public Criteria andVehregnoLessThanOrEqualTo(String value) {
			addCriterion("VEHREGNO <=", value, "vehregno");
			return (Criteria) this;
		}

		public Criteria andVehregnoLike(String value) {
			addCriterion("VEHREGNO like", value, "vehregno");
			return (Criteria) this;
		}

		public Criteria andVehregnoNotLike(String value) {
			addCriterion("VEHREGNO not like", value, "vehregno");
			return (Criteria) this;
		}

		public Criteria andVehregnoIn(List<String> values) {
			addCriterion("VEHREGNO in", values, "vehregno");
			return (Criteria) this;
		}

		public Criteria andVehregnoNotIn(List<String> values) {
			addCriterion("VEHREGNO not in", values, "vehregno");
			return (Criteria) this;
		}

		public Criteria andVehregnoBetween(String value1, String value2) {
			addCriterion("VEHREGNO between", value1, value2, "vehregno");
			return (Criteria) this;
		}

		public Criteria andVehregnoNotBetween(String value1, String value2) {
			addCriterion("VEHREGNO not between", value1, value2, "vehregno");
			return (Criteria) this;
		}

		public Criteria andEnginenoIsNull() {
			addCriterion("ENGINENO is null");
			return (Criteria) this;
		}

		public Criteria andEnginenoIsNotNull() {
			addCriterion("ENGINENO is not null");
			return (Criteria) this;
		}

		public Criteria andEnginenoEqualTo(String value) {
			addCriterion("ENGINENO =", value, "engineno");
			return (Criteria) this;
		}

		public Criteria andEnginenoNotEqualTo(String value) {
			addCriterion("ENGINENO <>", value, "engineno");
			return (Criteria) this;
		}

		public Criteria andEnginenoGreaterThan(String value) {
			addCriterion("ENGINENO >", value, "engineno");
			return (Criteria) this;
		}

		public Criteria andEnginenoGreaterThanOrEqualTo(String value) {
			addCriterion("ENGINENO >=", value, "engineno");
			return (Criteria) this;
		}

		public Criteria andEnginenoLessThan(String value) {
			addCriterion("ENGINENO <", value, "engineno");
			return (Criteria) this;
		}

		public Criteria andEnginenoLessThanOrEqualTo(String value) {
			addCriterion("ENGINENO <=", value, "engineno");
			return (Criteria) this;
		}

		public Criteria andEnginenoLike(String value) {
			addCriterion("ENGINENO like", value, "engineno");
			return (Criteria) this;
		}

		public Criteria andEnginenoNotLike(String value) {
			addCriterion("ENGINENO not like", value, "engineno");
			return (Criteria) this;
		}

		public Criteria andEnginenoIn(List<String> values) {
			addCriterion("ENGINENO in", values, "engineno");
			return (Criteria) this;
		}

		public Criteria andEnginenoNotIn(List<String> values) {
			addCriterion("ENGINENO not in", values, "engineno");
			return (Criteria) this;
		}

		public Criteria andEnginenoBetween(String value1, String value2) {
			addCriterion("ENGINENO between", value1, value2, "engineno");
			return (Criteria) this;
		}

		public Criteria andEnginenoNotBetween(String value1, String value2) {
			addCriterion("ENGINENO not between", value1, value2, "engineno");
			return (Criteria) this;
		}

		public Criteria andChassisnoIsNull() {
			addCriterion("CHASSISNO is null");
			return (Criteria) this;
		}

		public Criteria andChassisnoIsNotNull() {
			addCriterion("CHASSISNO is not null");
			return (Criteria) this;
		}

		public Criteria andChassisnoEqualTo(String value) {
			addCriterion("CHASSISNO =", value, "chassisno");
			return (Criteria) this;
		}

		public Criteria andChassisnoNotEqualTo(String value) {
			addCriterion("CHASSISNO <>", value, "chassisno");
			return (Criteria) this;
		}

		public Criteria andChassisnoGreaterThan(String value) {
			addCriterion("CHASSISNO >", value, "chassisno");
			return (Criteria) this;
		}

		public Criteria andChassisnoGreaterThanOrEqualTo(String value) {
			addCriterion("CHASSISNO >=", value, "chassisno");
			return (Criteria) this;
		}

		public Criteria andChassisnoLessThan(String value) {
			addCriterion("CHASSISNO <", value, "chassisno");
			return (Criteria) this;
		}

		public Criteria andChassisnoLessThanOrEqualTo(String value) {
			addCriterion("CHASSISNO <=", value, "chassisno");
			return (Criteria) this;
		}

		public Criteria andChassisnoLike(String value) {
			addCriterion("CHASSISNO like", value, "chassisno");
			return (Criteria) this;
		}

		public Criteria andChassisnoNotLike(String value) {
			addCriterion("CHASSISNO not like", value, "chassisno");
			return (Criteria) this;
		}

		public Criteria andChassisnoIn(List<String> values) {
			addCriterion("CHASSISNO in", values, "chassisno");
			return (Criteria) this;
		}

		public Criteria andChassisnoNotIn(List<String> values) {
			addCriterion("CHASSISNO not in", values, "chassisno");
			return (Criteria) this;
		}

		public Criteria andChassisnoBetween(String value1, String value2) {
			addCriterion("CHASSISNO between", value1, value2, "chassisno");
			return (Criteria) this;
		}

		public Criteria andChassisnoNotBetween(String value1, String value2) {
			addCriterion("CHASSISNO not between", value1, value2, "chassisno");
			return (Criteria) this;
		}

		public Criteria andDoctypeIsNull() {
			addCriterion("DOCTYPE is null");
			return (Criteria) this;
		}

		public Criteria andDoctypeIsNotNull() {
			addCriterion("DOCTYPE is not null");
			return (Criteria) this;
		}

		public Criteria andDoctypeEqualTo(String value) {
			addCriterion("DOCTYPE =", value, "doctype");
			return (Criteria) this;
		}

		public Criteria andDoctypeNotEqualTo(String value) {
			addCriterion("DOCTYPE <>", value, "doctype");
			return (Criteria) this;
		}

		public Criteria andDoctypeGreaterThan(String value) {
			addCriterion("DOCTYPE >", value, "doctype");
			return (Criteria) this;
		}

		public Criteria andDoctypeGreaterThanOrEqualTo(String value) {
			addCriterion("DOCTYPE >=", value, "doctype");
			return (Criteria) this;
		}

		public Criteria andDoctypeLessThan(String value) {
			addCriterion("DOCTYPE <", value, "doctype");
			return (Criteria) this;
		}

		public Criteria andDoctypeLessThanOrEqualTo(String value) {
			addCriterion("DOCTYPE <=", value, "doctype");
			return (Criteria) this;
		}

		public Criteria andDoctypeLike(String value) {
			addCriterion("DOCTYPE like", value, "doctype");
			return (Criteria) this;
		}

		public Criteria andDoctypeNotLike(String value) {
			addCriterion("DOCTYPE not like", value, "doctype");
			return (Criteria) this;
		}

		public Criteria andDoctypeIn(List<String> values) {
			addCriterion("DOCTYPE in", values, "doctype");
			return (Criteria) this;
		}

		public Criteria andDoctypeNotIn(List<String> values) {
			addCriterion("DOCTYPE not in", values, "doctype");
			return (Criteria) this;
		}

		public Criteria andDoctypeBetween(String value1, String value2) {
			addCriterion("DOCTYPE between", value1, value2, "doctype");
			return (Criteria) this;
		}

		public Criteria andDoctypeNotBetween(String value1, String value2) {
			addCriterion("DOCTYPE not between", value1, value2, "doctype");
			return (Criteria) this;
		}

		public Criteria andReasoncodeIsNull() {
			addCriterion("REASONCODE is null");
			return (Criteria) this;
		}

		public Criteria andReasoncodeIsNotNull() {
			addCriterion("REASONCODE is not null");
			return (Criteria) this;
		}

		public Criteria andReasoncodeEqualTo(String value) {
			addCriterion("REASONCODE =", value, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andReasoncodeNotEqualTo(String value) {
			addCriterion("REASONCODE <>", value, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andReasoncodeGreaterThan(String value) {
			addCriterion("REASONCODE >", value, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andReasoncodeGreaterThanOrEqualTo(String value) {
			addCriterion("REASONCODE >=", value, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andReasoncodeLessThan(String value) {
			addCriterion("REASONCODE <", value, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andReasoncodeLessThanOrEqualTo(String value) {
			addCriterion("REASONCODE <=", value, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andReasoncodeLike(String value) {
			addCriterion("REASONCODE like", value, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andReasoncodeNotLike(String value) {
			addCriterion("REASONCODE not like", value, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andReasoncodeIn(List<String> values) {
			addCriterion("REASONCODE in", values, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andReasoncodeNotIn(List<String> values) {
			addCriterion("REASONCODE not in", values, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andReasoncodeBetween(String value1, String value2) {
			addCriterion("REASONCODE between", value1, value2, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andReasoncodeNotBetween(String value1, String value2) {
			addCriterion("REASONCODE not between", value1, value2, "reasoncode");
			return (Criteria) this;
		}

		public Criteria andIdnoIsNull() {
			addCriterion("IDNO is null");
			return (Criteria) this;
		}

		public Criteria andIdnoIsNotNull() {
			addCriterion("IDNO is not null");
			return (Criteria) this;
		}

		public Criteria andIdnoEqualTo(String value) {
			addCriterion("IDNO =", value, "idno");
			return (Criteria) this;
		}

		public Criteria andIdnoNotEqualTo(String value) {
			addCriterion("IDNO <>", value, "idno");
			return (Criteria) this;
		}

		public Criteria andIdnoGreaterThan(String value) {
			addCriterion("IDNO >", value, "idno");
			return (Criteria) this;
		}

		public Criteria andIdnoGreaterThanOrEqualTo(String value) {
			addCriterion("IDNO >=", value, "idno");
			return (Criteria) this;
		}

		public Criteria andIdnoLessThan(String value) {
			addCriterion("IDNO <", value, "idno");
			return (Criteria) this;
		}

		public Criteria andIdnoLessThanOrEqualTo(String value) {
			addCriterion("IDNO <=", value, "idno");
			return (Criteria) this;
		}

		public Criteria andIdnoLike(String value) {
			addCriterion("IDNO like", value, "idno");
			return (Criteria) this;
		}

		public Criteria andIdnoNotLike(String value) {
			addCriterion("IDNO not like", value, "idno");
			return (Criteria) this;
		}

		public Criteria andIdnoIn(List<String> values) {
			addCriterion("IDNO in", values, "idno");
			return (Criteria) this;
		}

		public Criteria andIdnoNotIn(List<String> values) {
			addCriterion("IDNO not in", values, "idno");
			return (Criteria) this;
		}

		public Criteria andIdnoBetween(String value1, String value2) {
			addCriterion("IDNO between", value1, value2, "idno");
			return (Criteria) this;
		}

		public Criteria andIdnoNotBetween(String value1, String value2) {
			addCriterion("IDNO not between", value1, value2, "idno");
			return (Criteria) this;
		}

		public Criteria andIdno2IsNull() {
			addCriterion("IDNO2 is null");
			return (Criteria) this;
		}

		public Criteria andIdno2IsNotNull() {
			addCriterion("IDNO2 is not null");
			return (Criteria) this;
		}

		public Criteria andIdno2EqualTo(String value) {
			addCriterion("IDNO2 =", value, "idno2");
			return (Criteria) this;
		}

		public Criteria andIdno2NotEqualTo(String value) {
			addCriterion("IDNO2 <>", value, "idno2");
			return (Criteria) this;
		}

		public Criteria andIdno2GreaterThan(String value) {
			addCriterion("IDNO2 >", value, "idno2");
			return (Criteria) this;
		}

		public Criteria andIdno2GreaterThanOrEqualTo(String value) {
			addCriterion("IDNO2 >=", value, "idno2");
			return (Criteria) this;
		}

		public Criteria andIdno2LessThan(String value) {
			addCriterion("IDNO2 <", value, "idno2");
			return (Criteria) this;
		}

		public Criteria andIdno2LessThanOrEqualTo(String value) {
			addCriterion("IDNO2 <=", value, "idno2");
			return (Criteria) this;
		}

		public Criteria andIdno2Like(String value) {
			addCriterion("IDNO2 like", value, "idno2");
			return (Criteria) this;
		}

		public Criteria andIdno2NotLike(String value) {
			addCriterion("IDNO2 not like", value, "idno2");
			return (Criteria) this;
		}

		public Criteria andIdno2In(List<String> values) {
			addCriterion("IDNO2 in", values, "idno2");
			return (Criteria) this;
		}

		public Criteria andIdno2NotIn(List<String> values) {
			addCriterion("IDNO2 not in", values, "idno2");
			return (Criteria) this;
		}

		public Criteria andIdno2Between(String value1, String value2) {
			addCriterion("IDNO2 between", value1, value2, "idno2");
			return (Criteria) this;
		}

		public Criteria andIdno2NotBetween(String value1, String value2) {
			addCriterion("IDNO2 not between", value1, value2, "idno2");
			return (Criteria) this;
		}

		public Criteria andEffectdateIsNull() {
			addCriterion("EFFECTDATE is null");
			return (Criteria) this;
		}

		public Criteria andEffectdateIsNotNull() {
			addCriterion("EFFECTDATE is not null");
			return (Criteria) this;
		}

		public Criteria andEffectdateEqualTo(String value) {
			addCriterion("EFFECTDATE =", value, "effectdate");
			return (Criteria) this;
		}

		public Criteria andEffectdateNotEqualTo(String value) {
			addCriterion("EFFECTDATE <>", value, "effectdate");
			return (Criteria) this;
		}

		public Criteria andEffectdateGreaterThan(String value) {
			addCriterion("EFFECTDATE >", value, "effectdate");
			return (Criteria) this;
		}

		public Criteria andEffectdateGreaterThanOrEqualTo(String value) {
			addCriterion("EFFECTDATE >=", value, "effectdate");
			return (Criteria) this;
		}

		public Criteria andEffectdateLessThan(String value) {
			addCriterion("EFFECTDATE <", value, "effectdate");
			return (Criteria) this;
		}

		public Criteria andEffectdateLessThanOrEqualTo(String value) {
			addCriterion("EFFECTDATE <=", value, "effectdate");
			return (Criteria) this;
		}

		public Criteria andEffectdateLike(String value) {
			addCriterion("EFFECTDATE like", value, "effectdate");
			return (Criteria) this;
		}

		public Criteria andEffectdateNotLike(String value) {
			addCriterion("EFFECTDATE not like", value, "effectdate");
			return (Criteria) this;
		}

		public Criteria andEffectdateIn(List<String> values) {
			addCriterion("EFFECTDATE in", values, "effectdate");
			return (Criteria) this;
		}

		public Criteria andEffectdateNotIn(List<String> values) {
			addCriterion("EFFECTDATE not in", values, "effectdate");
			return (Criteria) this;
		}

		public Criteria andEffectdateBetween(String value1, String value2) {
			addCriterion("EFFECTDATE between", value1, value2, "effectdate");
			return (Criteria) this;
		}

		public Criteria andEffectdateNotBetween(String value1, String value2) {
			addCriterion("EFFECTDATE not between", value1, value2, "effectdate");
			return (Criteria) this;
		}

		public Criteria andExpirydateIsNull() {
			addCriterion("EXPIRYDATE is null");
			return (Criteria) this;
		}

		public Criteria andExpirydateIsNotNull() {
			addCriterion("EXPIRYDATE is not null");
			return (Criteria) this;
		}

		public Criteria andExpirydateEqualTo(String value) {
			addCriterion("EXPIRYDATE =", value, "expirydate");
			return (Criteria) this;
		}

		public Criteria andExpirydateNotEqualTo(String value) {
			addCriterion("EXPIRYDATE <>", value, "expirydate");
			return (Criteria) this;
		}

		public Criteria andExpirydateGreaterThan(String value) {
			addCriterion("EXPIRYDATE >", value, "expirydate");
			return (Criteria) this;
		}

		public Criteria andExpirydateGreaterThanOrEqualTo(String value) {
			addCriterion("EXPIRYDATE >=", value, "expirydate");
			return (Criteria) this;
		}

		public Criteria andExpirydateLessThan(String value) {
			addCriterion("EXPIRYDATE <", value, "expirydate");
			return (Criteria) this;
		}

		public Criteria andExpirydateLessThanOrEqualTo(String value) {
			addCriterion("EXPIRYDATE <=", value, "expirydate");
			return (Criteria) this;
		}

		public Criteria andExpirydateLike(String value) {
			addCriterion("EXPIRYDATE like", value, "expirydate");
			return (Criteria) this;
		}

		public Criteria andExpirydateNotLike(String value) {
			addCriterion("EXPIRYDATE not like", value, "expirydate");
			return (Criteria) this;
		}

		public Criteria andExpirydateIn(List<String> values) {
			addCriterion("EXPIRYDATE in", values, "expirydate");
			return (Criteria) this;
		}

		public Criteria andExpirydateNotIn(List<String> values) {
			addCriterion("EXPIRYDATE not in", values, "expirydate");
			return (Criteria) this;
		}

		public Criteria andExpirydateBetween(String value1, String value2) {
			addCriterion("EXPIRYDATE between", value1, value2, "expirydate");
			return (Criteria) this;
		}

		public Criteria andExpirydateNotBetween(String value1, String value2) {
			addCriterion("EXPIRYDATE not between", value1, value2, "expirydate");
			return (Criteria) this;
		}

		public Criteria andCovertypeIsNull() {
			addCriterion("COVERTYPE is null");
			return (Criteria) this;
		}

		public Criteria andCovertypeIsNotNull() {
			addCriterion("COVERTYPE is not null");
			return (Criteria) this;
		}

		public Criteria andCovertypeEqualTo(String value) {
			addCriterion("COVERTYPE =", value, "covertype");
			return (Criteria) this;
		}

		public Criteria andCovertypeNotEqualTo(String value) {
			addCriterion("COVERTYPE <>", value, "covertype");
			return (Criteria) this;
		}

		public Criteria andCovertypeGreaterThan(String value) {
			addCriterion("COVERTYPE >", value, "covertype");
			return (Criteria) this;
		}

		public Criteria andCovertypeGreaterThanOrEqualTo(String value) {
			addCriterion("COVERTYPE >=", value, "covertype");
			return (Criteria) this;
		}

		public Criteria andCovertypeLessThan(String value) {
			addCriterion("COVERTYPE <", value, "covertype");
			return (Criteria) this;
		}

		public Criteria andCovertypeLessThanOrEqualTo(String value) {
			addCriterion("COVERTYPE <=", value, "covertype");
			return (Criteria) this;
		}

		public Criteria andCovertypeLike(String value) {
			addCriterion("COVERTYPE like", value, "covertype");
			return (Criteria) this;
		}

		public Criteria andCovertypeNotLike(String value) {
			addCriterion("COVERTYPE not like", value, "covertype");
			return (Criteria) this;
		}

		public Criteria andCovertypeIn(List<String> values) {
			addCriterion("COVERTYPE in", values, "covertype");
			return (Criteria) this;
		}

		public Criteria andCovertypeNotIn(List<String> values) {
			addCriterion("COVERTYPE not in", values, "covertype");
			return (Criteria) this;
		}

		public Criteria andCovertypeBetween(String value1, String value2) {
			addCriterion("COVERTYPE between", value1, value2, "covertype");
			return (Criteria) this;
		}

		public Criteria andCovertypeNotBetween(String value1, String value2) {
			addCriterion("COVERTYPE not between", value1, value2, "covertype");
			return (Criteria) this;
		}

		public Criteria andPrcstimestampIsNull() {
			addCriterion("PRCSTIMESTAMP is null");
			return (Criteria) this;
		}

		public Criteria andPrcstimestampIsNotNull() {
			addCriterion("PRCSTIMESTAMP is not null");
			return (Criteria) this;
		}

		public Criteria andPrcstimestampEqualTo(String value) {
			addCriterion("PRCSTIMESTAMP =", value, "prcstimestamp");
			return (Criteria) this;
		}

		public Criteria andPrcstimestampNotEqualTo(String value) {
			addCriterion("PRCSTIMESTAMP <>", value, "prcstimestamp");
			return (Criteria) this;
		}

		public Criteria andPrcstimestampGreaterThan(String value) {
			addCriterion("PRCSTIMESTAMP >", value, "prcstimestamp");
			return (Criteria) this;
		}

		public Criteria andPrcstimestampGreaterThanOrEqualTo(String value) {
			addCriterion("PRCSTIMESTAMP >=", value, "prcstimestamp");
			return (Criteria) this;
		}

		public Criteria andPrcstimestampLessThan(String value) {
			addCriterion("PRCSTIMESTAMP <", value, "prcstimestamp");
			return (Criteria) this;
		}

		public Criteria andPrcstimestampLessThanOrEqualTo(String value) {
			addCriterion("PRCSTIMESTAMP <=", value, "prcstimestamp");
			return (Criteria) this;
		}

		public Criteria andPrcstimestampLike(String value) {
			addCriterion("PRCSTIMESTAMP like", value, "prcstimestamp");
			return (Criteria) this;
		}

		public Criteria andPrcstimestampNotLike(String value) {
			addCriterion("PRCSTIMESTAMP not like", value, "prcstimestamp");
			return (Criteria) this;
		}

		public Criteria andPrcstimestampIn(List<String> values) {
			addCriterion("PRCSTIMESTAMP in", values, "prcstimestamp");
			return (Criteria) this;
		}

		public Criteria andPrcstimestampNotIn(List<String> values) {
			addCriterion("PRCSTIMESTAMP not in", values, "prcstimestamp");
			return (Criteria) this;
		}

		public Criteria andPrcstimestampBetween(String value1, String value2) {
			addCriterion("PRCSTIMESTAMP between", value1, value2, "prcstimestamp");
			return (Criteria) this;
		}

		public Criteria andPrcstimestampNotBetween(String value1, String value2) {
			addCriterion("PRCSTIMESTAMP not between", value1, value2, "prcstimestamp");
			return (Criteria) this;
		}

		public Criteria andDocnoIsNull() {
			addCriterion("DOCNO is null");
			return (Criteria) this;
		}

		public Criteria andDocnoIsNotNull() {
			addCriterion("DOCNO is not null");
			return (Criteria) this;
		}

		public Criteria andDocnoEqualTo(String value) {
			addCriterion("DOCNO =", value, "docno");
			return (Criteria) this;
		}

		public Criteria andDocnoNotEqualTo(String value) {
			addCriterion("DOCNO <>", value, "docno");
			return (Criteria) this;
		}

		public Criteria andDocnoGreaterThan(String value) {
			addCriterion("DOCNO >", value, "docno");
			return (Criteria) this;
		}

		public Criteria andDocnoGreaterThanOrEqualTo(String value) {
			addCriterion("DOCNO >=", value, "docno");
			return (Criteria) this;
		}

		public Criteria andDocnoLessThan(String value) {
			addCriterion("DOCNO <", value, "docno");
			return (Criteria) this;
		}

		public Criteria andDocnoLessThanOrEqualTo(String value) {
			addCriterion("DOCNO <=", value, "docno");
			return (Criteria) this;
		}

		public Criteria andDocnoLike(String value) {
			addCriterion("DOCNO like", value, "docno");
			return (Criteria) this;
		}

		public Criteria andDocnoNotLike(String value) {
			addCriterion("DOCNO not like", value, "docno");
			return (Criteria) this;
		}

		public Criteria andDocnoIn(List<String> values) {
			addCriterion("DOCNO in", values, "docno");
			return (Criteria) this;
		}

		public Criteria andDocnoNotIn(List<String> values) {
			addCriterion("DOCNO not in", values, "docno");
			return (Criteria) this;
		}

		public Criteria andDocnoBetween(String value1, String value2) {
			addCriterion("DOCNO between", value1, value2, "docno");
			return (Criteria) this;
		}

		public Criteria andDocnoNotBetween(String value1, String value2) {
			addCriterion("DOCNO not between", value1, value2, "docno");
			return (Criteria) this;
		}

		public Criteria andInserttimestampIsNull() {
			addCriterion("INSERTTIMESTAMP is null");
			return (Criteria) this;
		}

		public Criteria andInserttimestampIsNotNull() {
			addCriterion("INSERTTIMESTAMP is not null");
			return (Criteria) this;
		}

		public Criteria andInserttimestampEqualTo(String value) {
			addCriterion("INSERTTIMESTAMP =", value, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andInserttimestampNotEqualTo(String value) {
			addCriterion("INSERTTIMESTAMP <>", value, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andInserttimestampGreaterThan(String value) {
			addCriterion("INSERTTIMESTAMP >", value, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andInserttimestampGreaterThanOrEqualTo(String value) {
			addCriterion("INSERTTIMESTAMP >=", value, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andInserttimestampLessThan(String value) {
			addCriterion("INSERTTIMESTAMP <", value, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andInserttimestampLessThanOrEqualTo(String value) {
			addCriterion("INSERTTIMESTAMP <=", value, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andInserttimestampLike(String value) {
			addCriterion("INSERTTIMESTAMP like", value, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andInserttimestampNotLike(String value) {
			addCriterion("INSERTTIMESTAMP not like", value, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andInserttimestampIn(List<String> values) {
			addCriterion("INSERTTIMESTAMP in", values, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andInserttimestampNotIn(List<String> values) {
			addCriterion("INSERTTIMESTAMP not in", values, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andInserttimestampBetween(String value1, String value2) {
			addCriterion("INSERTTIMESTAMP between", value1, value2, "inserttimestamp");
			return (Criteria) this;
		}

		public Criteria andInserttimestampNotBetween(String value1, String value2) {
			addCriterion("INSERTTIMESTAMP not between", value1, value2, "inserttimestamp");
			return (Criteria) this;
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the
	 * database table DSP_MI_TBL_EDOCUMENT
	 *
	 * @mbggenerated do_not_delete_during_merge Thu Aug 17 13:35:05 SGT 2017
	 */
	public static class Criteria extends GeneratedCriteria {

		protected Criteria() {
			super();
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the
	 * database table DSP_MI_TBL_EDOCUMENT
	 *
	 * @mbggenerated Thu Aug 17 13:35:05 SGT 2017
	 */
	public static class Criterion {
		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}
}