package com.spring.uam.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class SecurityUtil {
	private static Properties aduserInfoProp = new Properties();
			// "NAkjfrwGsLpRrfOr";

	public static String enciptString(String plainText) {
		SecurityUtil sutil=new SecurityUtil();
		sutil.loadADuserProps();
		String encryptionKey =aduserInfoProp.getProperty("userkey");
		
		
		DecriptionEncription advancedEncryptionStandard = new DecriptionEncription(encryptionKey);
		String cipherText = null;

		try {
			cipherText = advancedEncryptionStandard.encrypt(plainText);

		} catch (Exception e) {
			// TODO Auto-generated catch blockR
			e.printStackTrace();
		}
		return cipherText;
	}

	public static String decriptString(String value) {
		SecurityUtil sutil=new SecurityUtil();
		sutil.loadADuserProps();
		String encryptionKey =aduserInfoProp.getProperty("userkey");
		DecriptionEncription advancedEncryptionStandard = new DecriptionEncription(encryptionKey);
		String cipherText = null;

		try {
			cipherText = advancedEncryptionStandard.decrypt(value);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cipherText;
	}
	 public void loadADuserProps() {
		 InputStream infoad = this.getClass().getClassLoader().getResourceAsStream("com/spring/uam/ldap/adconfig.properties");
		 try {
			 aduserInfoProp.load(infoad);
		 } catch (IOException e) {
			e.printStackTrace();
		 }
	 }
}
