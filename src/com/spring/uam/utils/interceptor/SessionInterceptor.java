package com.spring.uam.utils.interceptor;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.spring.uam.utils.Logouturl;
import com.spring.uam.vo.RoleModule;
import com.spring.utils.ExcludeURLs;

public class SessionInterceptor extends HandlerInterceptorAdapter {

	final static Logger logger = Logger.getLogger(SessionInterceptor.class);
	@Autowired
	private HttpSession session;
//
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		logger.info("Pre handle method - check handling start time");
		List<RoleModule> roleModule_all = new ArrayList<RoleModule>();
		List<Integer> moduleName = new ArrayList<Integer>();
		String path = request.getRequestURI().substring(request.getContextPath().length());
		Boolean exception = false;
		
		logger.info("dhana printing this from session interface uam utils");
	logger.info(ExcludeURLs.DOLOGIN.toLowerCase()+"UAM_DASHBOARD MAIN");
		if (ExcludeURLs.MAIN.equalsIgnoreCase(path)) {
			exception = true;
		}
		if (path.toLowerCase().contains(ExcludeURLs.DOLOGIN.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.DSPADMIN_DASHBOARD.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.DOLOGOUT.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.GETMENU.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.RESOURCES.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.SUBMIT_MODULE.toLowerCase())) {

			exception = true;

		}

		if (exception == false) {
			logger.info(request.getContextPath());
			logger.info(" Request URI " + request.getRequestURI());
			 logger.info(" Request URI 2 " + request.getRequestURI().substring(request.getContextPath().length()));
			session = request.getSession();

			if (session.getAttribute("level_1") == null) {
				logger.info("level_1  is empty");
				// RequestDispatcher dd =
				// request.getRequestDispatcher("admin-login");
				// response.sendRedirect(request.getContextPath() +
				// "/doLogout");
				response.sendRedirect(Logouturl.UAM_URL_LINK_);
				return false;
				// dd.forward(request, response);
			}

		}

		/*
		 * if (session.getAttribute("roleModule_all") != null) { roleModule_all =
		 * (List<RoleModule>) (session.getAttribute("roleModule_all")); for (RoleModule
		 * ro : roleModule_all) { moduleName.add(ro.getModuleId().intValue()); }
		 * logger.info( "chk mdi    :" + moduleName); if
		 * (!moduleName.contains(6)) {
		 *
		 * }
		 *
		 * }
		 */
		return true;

	}

}
