package com.spring.uam.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.uam.vo.Menu;
import com.spring.uam.vo.RoleModule;

import oracle.jdbc.OracleTypes;

@Repository("menuDao")
public class MenuDaoImpl implements MenuDao {
	private static final Logger logger = LoggerFactory.getLogger(MenuDaoImpl.class);

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Autowired
	private DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public ArrayList<Menu> getMenuList(int module_id, int role_id) throws SQLException {
		// TODO Auto-generated method stub
		ArrayList<Menu> objects = new ArrayList<Menu>();

		ResultSet rs = null;

		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			CallableStatement proc = null;
			proc = connection.prepareCall("{CALL DSP_ADM_USER_MENU_LIST(?,?,?)}");
			proc.setInt(1, role_id);
			proc.setInt(2, module_id);
			proc.registerOutParameter(3, OracleTypes.CURSOR);
			proc.execute();

			rs = (ResultSet) proc.getObject(3);
			while (rs.next()) {

				Menu userpojo = new Menu();

				userpojo.setId(rs.getBigDecimal("ID"));
				userpojo.setName(rs.getString("name"));
				userpojo.setParentId((short) rs.getInt("parent_id"));
				userpojo.setLevelId((short) rs.getInt("level_id"));
				// userpojo.setLevelId((short) rs.getInt("level_id"));
				userpojo.setPermission_str(rs.getString("PRMSSN_ID"));
				userpojo.setUrl(rs.getString("menu_url"));
				objects.add(userpojo);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
		}

		return objects;
	}

	@Override
	public ArrayList<RoleModule> getRoleListByUser(int userId, int moduleId) throws SQLException {
		// TODO Auto-generated method stub
		ArrayList<RoleModule> objects = new ArrayList<RoleModule>();

		ResultSet rs = null;
		CallableStatement proc = null;
		Connection connection = null;
		try {
			connection = dataSource.getConnection();

			proc = connection.prepareCall("{CALL DSP_ADM_SP_USER_ROL_LIST(?,?,?)}");
			proc.setInt(1, userId);
			proc.setInt(2, moduleId);
			proc.registerOutParameter(3, OracleTypes.CURSOR);
			proc.execute();

			rs = (ResultSet) proc.getObject(3);
			while (rs.next()) {

				RoleModule userpojo = new RoleModule();

				userpojo.setId(rs.getInt("ID"));
				userpojo.setModuleId((short) rs.getInt("MODULE_ID"));
				userpojo.setRoleId((short) rs.getInt("ROLE_ID"));

				objects.add(userpojo);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			proc.close();
			connection.close();
		}

		return objects;
	}
}
