package com.spring.uam.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import com.spring.uam.vo.Menu;
import com.spring.uam.vo.RoleModule;

public interface MenuDao {
	public ArrayList<Menu> getMenuList(int module_id, int role_id) throws SQLException;

	public ArrayList<RoleModule> getRoleListByUser(int userId, int moduleId) throws SQLException;

}
