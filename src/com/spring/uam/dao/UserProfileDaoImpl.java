package com.spring.uam.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.etiqa.common.DB.ConnectionFactory;
import com.spring.uam.vo.CustomUserPojo;

import oracle.jdbc.OracleTypes;

@Repository("UserProfileDao")
public class UserProfileDaoImpl implements UserProfileDao {

	private static final Logger logger = LoggerFactory.getLogger(UserProfileDaoImpl.class);
	// private DataSource dataSource;

	@Override
	public ArrayList<CustomUserPojo> getRoleModuleList() throws SQLException {
		ArrayList<CustomUserPojo> objects = new ArrayList<CustomUserPojo>();
		Connection connection = null;
		ResultSet rs = null;
		CallableStatement proc = null;
		try {
			connection = ConnectionFactory.getConnection();
			proc = connection.prepareCall("{CALL DSP_ADM_USER_ROLE_MODULELIST(?)}");
			proc.registerOutParameter(1, OracleTypes.CURSOR);
			proc.execute();
			rs = (ResultSet) proc.getObject(1);
			while (rs.next()) {
				CustomUserPojo userpojo = new CustomUserPojo();
				userpojo.setId(rs.getInt("ID"));
				userpojo.setUsermodule(rs.getString("modulename"));
				userpojo.setUserrole(rs.getString("rolename"));
				// userpojo.setName(rs.getString("modulename")+"-"+rs.getString("rolename"));
				userpojo.setName(rs.getString("rolename"));
				objects.add(userpojo);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			proc.close();
			connection.close();
		}

		return objects;
	}

	@Override
	public ArrayList<CustomUserPojo> getSearchUserProfile(String name, String pfnumber, String status,
			String rolemoduleid) throws SQLException {
		ArrayList<CustomUserPojo> objects = new ArrayList<CustomUserPojo>();
		Connection connection = null;
		ResultSet rs = null;
		CallableStatement proc = null;
		try {
			connection = ConnectionFactory.getConnection();

			proc = connection.prepareCall("{CALL DSP_ADM_USER_SEARCH(?,?,?,?,?)}");
			// Display a date in day, month, year format

			proc.setString(1, pfnumber);
			proc.setString(2, status);
			proc.setString(3, name);
			proc.setString(4, rolemoduleid);
			proc.registerOutParameter(5, OracleTypes.CURSOR);
			proc.execute();

			rs = (ResultSet) proc.getObject(5);
			while (rs.next()) {

				CustomUserPojo userpojo = new CustomUserPojo();
				userpojo.setId(rs.getInt("ID"));
				userpojo.setName(rs.getString("NAME"));
				userpojo.setPfNumber(rs.getString("PF_NUMBER"));
				userpojo.setStatus(rs.getString("STATUS"));
				userpojo.setCreateBy(rs.getString("CREATE_BY"));
				userpojo.setUpdateBy(rs.getString("UPDATE_BY"));
				userpojo.setCreateDate(rs.getString("CREATE_DATE"));
				userpojo.setUpdateDate(rs.getString("UPDATE_DATE"));
				userpojo.setLastLogin(rs.getString("LAST_LOGIN"));
				userpojo.setUserrole(rs.getString("ROLE_NAME"));
				// System.out.println("role name :" +
				// rs.getString("ROLE_NAME"));
				objects.add(userpojo);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			proc.close();
			connection.close();
		}

		return objects;
	}

}
