package com.spring.uam.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import com.spring.uam.vo.CustomUserPojo;

public interface UserProfileDao {

	public ArrayList<CustomUserPojo> getRoleModuleList() throws SQLException;

	public ArrayList<CustomUserPojo> getSearchUserProfile(String name, String pfnumber, String status,
			String rolemoduleid) throws SQLException;

}
