package com.spring.uam.ldap;

import java.io.IOException;
import java.io.InputStream;
/*import java.rmi.server.UID;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date; */
import java.util.Hashtable;
import java.util.Properties;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

public class LoginCPF {
	private static Properties aduserInfoProp = new Properties();

	public static LDAPAttributesBean authenticateUser(String username, String password) {
		LDAPAttributesBean ldapAttr = new LDAPAttributesBean();
		
		// String IPAddress = "ad1.etiqa.local";
		// String baseDN = "OU=Internal,OU=ETIQA USERS,DC=etiqa,DC=local";
		LdapContext ctx = null;
		LoginCPF login = new LoginCPF();
		login.loadADuserProps();
		String IPAddress = aduserInfoProp.getProperty("hosturl");
		String baseDN = aduserInfoProp.getProperty("AduserbaseDN");
		System.out.println(IPAddress + "#######" + baseDN);
		String CN = "CN=" + username;
		String FQDN = CN + "," + baseDN;
		SearchControls searchCtls = new SearchControls();
		Attributes attrs = new BasicAttributes(true);
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		String returnedAtts[] = { "" };
		searchCtls.setReturningAttributes(returnedAtts);
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
		env.put("java.naming.security.authentication", "simple");
		env.put("java.naming.security.principal", FQDN);
		env.put("java.naming.security.credentials", password);
		env.put("java.naming.provider.url", "ldap://" + IPAddress + ":389");
		try {
			System.out.println("connected to ad..");
			ctx = new InitialLdapContext(env, null);
			NamingEnumeration answer = ctx.search(baseDN, CN, searchCtls);
			while (answer.hasMoreElements()) {
				/*
				 * UserValidation_DAO vdao = new UserValidation_DAO();
				 *
				 * User muser = new User();
				 */
				System.out.println("connected");
				// muser = vdao.getvalidate(username);
				// System.out.println("user validation" + muser.getIsVarified());

				ldapAttr.setValidated(1);
				System.out.println("user validation" + ldapAttr.getValidated());
				ldapAttr.setFULNAME("eChannel");

				SearchResult sr = (SearchResult) answer.next();

			}

			ctx.close();

		} catch (NamingException e) {
			// TODO Auto-generated catch block
			ldapAttr.setValidated(0);
			System.out.println("Message is " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (ctx != null) {
				try {
					ctx.close();
				} catch (NamingException e) {
					// throw new SystemException(e);
				}
			}
		}
		return ldapAttr;
	}

	public void loadADuserProps() {
		InputStream infoad = this.getClass().getClassLoader()
				.getResourceAsStream("com/spring/uam/ldap/adconfig.properties");
		try {
			aduserInfoProp.load(infoad);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// public static void main(String args[]){
	// UID uid = new UID();
	// LoginCPF login =new LoginCPF();
	// login.loadADuserProps();
	// LDAPAttributesBean ldap = authenticateUser("2786","Etiqa2016!");

	// System.out.println(ldap.getValidated());
	// System.out.println(ldap.getLast_logon());
	// System.out.println(ldap.getWrong_password_attempt());

	/*
	 * DB SP Checking
	 *
	 *
	 * LoginCPF lpc = new LoginCPF(); LoginCPFBean mdmInfo = new LoginCPFBean();
	 * mdmInfo.setIc_number("790816085098");
	 * mdmInfo.setEmail_address("htanto86@gmail.com"); mdmInfo.setLast_update(new
	 * Date()); mdmInfo.setName("Hendra Tan"); mdmInfo.setId_type("KTP");
	 * mdmInfo.setDate_of_birth("29 Aug 1986"); mdmInfo.setGender("male");
	 * mdmInfo.setNationality("Indonesian"); mdmInfo.setMarital_status("Married");
	 * mdmInfo.setRace("Chinesse"); mdmInfo.setAddress("Apart Medit 2");
	 * mdmInfo.setCity("JKT"); mdmInfo.setState("Jabar");
	 * mdmInfo.setPostcode("11323"); mdmInfo.setCountry("Indonesia");
	 * mdmInfo.setHome("asdf"); mdmInfo.setMobile("asdf");
	 *
	 * LoginCPFBean lcbean = lpc.queryDatabaseDashboard(mdmInfo);
	 * System.out.println(lcbean.getMobile());
	 *
	 * /* LoginCPF login = new LoginCPF(); LoginCPFBean lcb = new LoginCPFBean();
	 * login.login("farooq@absecmy.com","Absec@2016", "testing1!");
	 * System.out.println(lcb.getError_code());
	 */

	/*
	 * LoginCPFBean lcb = new LoginCPFBean(); lcb.setTrx_id("123"); lcb =
	 * getMDMUserProfile("790816085098", lcb); System.out.println(lcb.getName());
	 * System.out.println(lcb.getCity()); /* Get_Customer_Profile_Bean CPB = new
	 * Get_Customer_Profile_Bean(); Get_ProfileServiceProxy p = new
	 * Get_ProfileServiceProxy(); try { CPB = p.getprofile("790816085098","3232"); }
	 * catch (RemoteException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } System.out.println(CPB.getRescode());
	 */
	// }
}