package com.spring.uam.ldap;

import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Properties;

import javax.jws.WebService;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
//import javax.naming.directory.SearchControls;
//import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import com.spring.uam.dao.UserValidation_DAO;

//Date: 14/07/2015 7:36pm
//Author: Tulasi
//Version: V1.4

@WebService
public class RegCPF {

	private static Properties aduserProp = new Properties();

	public static LDAPAttributesBean ADUserCreate(String username, String password) {
		LDAPAttributesBean ldapAttr = new LDAPAttributesBean();
		
		 String IPAddress = "ad1.etiqa.local";
		 String baseDN = "OU=Internal,OU=ETIQA USERS,DC=etiqa,DC=local";
		ldapAttr.setUsername(username);
		ldapAttr.setPassword(password);
		ldapAttr.setUserrole(1);
		// Validating Password
		// ldapAttr = UserValidation_DAO.queryDatabasePwValidation(ldapAttr);
		// if(ldapAttr.getError_code().equalsIgnoreCase("D0000")){
		ldapAttr.setUsername(username);
		ldapAttr.setPassword(password);
		// Creating New User in AD...
		ldapAttr = createNewUser(ldapAttr);

		// }

		return ldapAttr;
	}

	public static LDAPAttributesBean ADChangePassword(String username, String Curpassword, String password) {
		LDAPAttributesBean ldapAttr = new LDAPAttributesBean();
		
		String IPAddress = "ad1.etiqa.local";
		 String baseDN = "OU=Internal,OU=ETIQA USERS,DC=etiqa,DC=local";
		ldapAttr.setUsername(username);
		ldapAttr.setCurPassword(Curpassword);
		ldapAttr.setPassword(password);
		ldapAttr.setUserrole(1);
		LoginCPF auth = new LoginCPF();
		// Authenticate with current user and password
		ldapAttr = LoginCPF.authenticateUser(username, Curpassword);
		if (ldapAttr.getValidated() == 1) {
			System.out.println(ldapAttr.getValidated() + "changepw");
			ldapAttr.setUsername(username);
			ldapAttr.setPassword(password);
			ldapAttr.setUserrole(1);
			// Validating Password
			ldapAttr = UserValidation_DAO.queryDatabasePwValidation(ldapAttr);
			if (ldapAttr.getError_code().equalsIgnoreCase("D0000")) {
				ldapAttr.setUsername(username);
				ldapAttr.setPassword(password);

				// Updating AD New Password
				ldapAttr = createUser(ldapAttr);
			}
		} else {
			ldapAttr.setError_code("D0018");
			ldapAttr.setError_msg("Validation failed. Current Password is not matched.");

		}

		return ldapAttr;
	}

	public static LDAPAttributesBean ADForgotpassword(String username, String password) {
		LDAPAttributesBean ldapAttr = new LDAPAttributesBean();
		
		String IPAddress = "ad1.etiqa.local";
		 String baseDN = "OU=Internal,OU=ETIQA USERS,DC=etiqa,DC=local";
		ldapAttr.setUsername(username);
		ldapAttr.setPassword(password);
		ldapAttr.setUserrole(1);
		LoginCPF auth = new LoginCPF();
		// Authenticate with current user and password

		ldapAttr.setUsername(username);
		ldapAttr.setPassword(password);
		ldapAttr.setUserrole(1);
		// Validating Password
		ldapAttr = UserValidation_DAO.queryDatabasePwValidation(ldapAttr);
		if (ldapAttr.getError_code().equalsIgnoreCase("D0000")) {
			ldapAttr.setUsername(username);
			ldapAttr.setPassword(password);

			// Updating AD New Password
			ldapAttr = createUser(ldapAttr);
		}

		return ldapAttr;
	}

	// LDAP Connection to authenticate service

	public static LDAPAttributesBean createUser(LDAPAttributesBean ldapAttr) {

		// LDAPAttributesBean ldapAttr = new LDAPAttributesBean();

		
		InputStream infoad = new RegCPF().getClass().getClassLoader()
				.getResourceAsStream("com/spring/uam/ldap/adconfig.properties");
		try {
			aduserProp.load(infoad);
		} catch (IOException e) {
			e.printStackTrace();
		}

		String adminName = aduserProp.getProperty("adminname");
		String adminPassword = aduserProp.getProperty("admin_pwd");
		String IPAddress = aduserProp.getProperty("hosturl");
		String UserConfig = aduserProp.getProperty("AduserbaseDN");
		System.out.println("adminName" + adminName + " adminPassword " + adminPassword);
		System.out.println("IPAddress" + IPAddress + " " + UserConfig);
		String userName = "CN=" + ldapAttr.getUsername() + "," + UserConfig;

		String newPassword = ldapAttr.getPassword();

		// Access the truststore
		// String keystore ="E:/Java/jdk1.8.0_73/jre/lib/security/cacerts";
		System.out.println("hosturl " + "ldaps://" + IPAddress + ":636");
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
		env.put("java.naming.security.authentication", "simple");
		env.put("java.naming.security.principal", adminName);
		env.put("java.naming.security.credentials", adminPassword);
		env.put("java.naming.provider.url", "ldaps://" + IPAddress + ":636");
		// env.put("java.naming.security.protocol", "ssl");

		try {
			// Create the initial directory context
			LdapContext ctx = new InitialLdapContext(env, null);
			ldapAttr = fetch(ldapAttr, ctx);
			// System.out.println(ldapAttr.getCheckuser());
			// some useful constants from lmaccess.h
			int UF_ACCOUNTDISABLE = 0x0002;
			int UF_PASSWD_NOTREQD = 0x0020;
			int UF_PASSWD_CANT_CHANGE = 0x0040;
			int UF_NORMAL_ACCOUNT = 0x0200;
			int UF_DONT_EXPIRE_PASSWD = 0x10000;
			int UF_PASSWORD_EXPIRED = 0x800000;

			if (ldapAttr.getCheckuser() == 0) {

				Attributes attrs = new BasicAttributes(true);

				// These are the mandatory attributes for a user object
				// Note that Win2K3 will automagically create a random
				// samAccountName if it is not present. (Win2K does not)
				attrs.put("objectClass", "user");
				attrs.put("samAccountName", ldapAttr.getUsername());
				attrs.put("cn", ldapAttr.getUsername());

				// These are some optional (but useful) attributes
				attrs.put("givenName", ldapAttr.getUsername());
				attrs.put("sn", ldapAttr.getUsername());
				// attrs.put("displayName","Tester Cret");
				// attrs.put("description","Kite Flying Research Scientist");
				attrs.put("userPrincipalName", ldapAttr.getUsername());
				// attrs.put("mail","Benjamin.Franklin@antipodes.com");
				// attrs.put("telephoneNumber","1 800 FLY KITE");

				attrs.put("userAccountControl",
						Integer.toString(UF_NORMAL_ACCOUNT + UF_PASSWD_NOTREQD + UF_PASSWORD_EXPIRED));

				// Create the context
				Context result = ctx.createSubcontext(userName, attrs);
			

				// update ther password the first time they login
				ModificationItem[] mods = new ModificationItem[2];

				// Replace the "unicdodePwd" attribute with a new value
				// Password must be both Unicode and a quoted string
				String newQuotedPassword = "\"" + newPassword + "\"";
				byte[] newUnicodePassword = newQuotedPassword.getBytes("UTF-16LE");

				mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
						new BasicAttribute("unicodePwd", newUnicodePassword));
				mods[1] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("userAccountControl",
						Integer.toString(UF_NORMAL_ACCOUNT + UF_PASSWORD_EXPIRED)));

				// Perform the update
				ctx.modifyAttributes(userName, mods);
				System.out.println("Set password & updated userccountControl");

				// now add the user to a group.
				ldapAttr.setValidated(1);
				ldapAttr.setError_code("D0000");
				ldapAttr.setError_msg("success");
			} else {
				System.out.println("updating password...\n");
				String newQuotedPassword = "\"" + newPassword + "\"";
				byte[] newUnicodePassword = newQuotedPassword.getBytes("UTF-16LE");
				ModificationItem[] mods = new ModificationItem[3];
				// mods[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new
				// BasicAttribute("pwdAccountLockedTime"));
				mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("lockoutTime", "0"));
				mods[1] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
						new BasicAttribute("unicodePwd", newUnicodePassword));
				mods[2] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("userAccountControl",
						Integer.toString(UF_NORMAL_ACCOUNT + UF_PASSWORD_EXPIRED)));

				// mods[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new
				// BasicAttribute("passwordRetryCount"));
				// mods[1] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new
				// BasicAttribute("accountUnlockTime"));
				// Perform the update
				ctx.modifyAttributes(userName, mods);
				System.out.println("updated...\n");
				ldapAttr.setValidated(1);
				ldapAttr.setError_code("D0000");
				ldapAttr.setError_msg("updated");
			}

			ctx.close();

		} catch (NamingException e) {
			System.err.println("NamingException Problem updating password: " + e);
			ldapAttr.setValidated(0);
			ldapAttr.setError_code("D9999");
			ldapAttr.setError_msg(e.getMessage());

		}

		catch (IOException e) {
			System.err.println("IOException Problem updating password: " + e);
			ldapAttr.setValidated(0);
			ldapAttr.setError_code("D9999");
			ldapAttr.setError_msg(e.getMessage());

		}

		return ldapAttr;
	}

	// LDAP Connection to authenticate service

	public static LDAPAttributesBean createNewUser(LDAPAttributesBean ldapAttr) {

		// LDAPAttributesBean ldapAttr = new LDAPAttributesBean();

		
		InputStream infoad = new RegCPF().getClass().getClassLoader()
				.getResourceAsStream("com/spring/uam/ldap/adconfig.properties");
		try {
			aduserProp.load(infoad);
		} catch (IOException e) {
			e.printStackTrace();
		}

		String adminName = aduserProp.getProperty("adminname");
		String adminPassword = aduserProp.getProperty("admin_pwd");
		String IPAddress = aduserProp.getProperty("hosturl");
		String UserConfig = aduserProp.getProperty("AduserbaseDN");
		System.out.println("adminName" + adminName + " adminPassword " + adminPassword);
		System.out.println("IPAddress" + IPAddress + " " + UserConfig);
		String userName = "CN=" + ldapAttr.getUsername() + "," + UserConfig;

		String newPassword = ldapAttr.getPassword();

		// Access the truststore
		// String keystore ="E:/Java/jdk1.8.0_73/jre/lib/security/cacerts";
		System.out.println("hosturl " + "ldaps://" + IPAddress + ":636");
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
		env.put("java.naming.security.authentication", "simple");
		env.put("java.naming.security.principal", adminName);
		env.put("java.naming.security.credentials", adminPassword);
		env.put("java.naming.provider.url", "ldaps://" + IPAddress + ":636");
		// env.put("java.naming.security.protocol", "ssl");

		try {
			// Create the initial directory context
			LdapContext ctx = new InitialLdapContext(env, null);
			ldapAttr = fetch(ldapAttr, ctx);
			// System.out.println(ldapAttr.getCheckuser());
			// some useful constants from lmaccess.h
			int UF_ACCOUNTDISABLE = 0x0002;
			int UF_PASSWD_NOTREQD = 0x0020;
			int UF_PASSWD_CANT_CHANGE = 0x0040;
			int UF_NORMAL_ACCOUNT = 0x0200;
			int UF_DONT_EXPIRE_PASSWD = 0x10000;
			int UF_PASSWORD_EXPIRED = 0x800000;

			if (ldapAttr.getCheckuser() == 0) {

				Attributes attrs = new BasicAttributes(true);

				// These are the mandatory attributes for a user object
				// Note that Win2K3 will automagically create a random
				// samAccountName if it is not present. (Win2K does not)
				attrs.put("objectClass", "user");
				attrs.put("samAccountName", ldapAttr.getUsername());
				attrs.put("cn", ldapAttr.getUsername());

				// These are some optional (but useful) attributes
				attrs.put("givenName", ldapAttr.getUsername());
				attrs.put("sn", ldapAttr.getUsername());
				// attrs.put("displayName","Tester Cret");
				// attrs.put("description","Kite Flying Research Scientist");
				attrs.put("userPrincipalName", ldapAttr.getUsername());
				// attrs.put("mail","Benjamin.Franklin@antipodes.com");
				// attrs.put("telephoneNumber","1 800 FLY KITE");

				attrs.put("userAccountControl",
						Integer.toString(UF_NORMAL_ACCOUNT + UF_PASSWD_NOTREQD + UF_PASSWORD_EXPIRED));

				// Create the context
				Context result = ctx.createSubcontext(userName, attrs);
				

				// update ther password the first time they login
				ModificationItem[] mods = new ModificationItem[2];

				// Replace the "unicdodePwd" attribute with a new value
				// Password must be both Unicode and a quoted string
				String newQuotedPassword = "\"" + newPassword + "\"";
				byte[] newUnicodePassword = newQuotedPassword.getBytes("UTF-16LE");

				mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
						new BasicAttribute("unicodePwd", newUnicodePassword));
				mods[1] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("userAccountControl",
						Integer.toString(UF_NORMAL_ACCOUNT + UF_PASSWORD_EXPIRED)));

				// Perform the update
				ctx.modifyAttributes(userName, mods);
				System.out.println("Set password & updated userccountControl");

				// now add the user to a group.
				ldapAttr.setValidated(1);
				ldapAttr.setError_code("D0000");
				ldapAttr.setError_msg("success");
			} else {
				ldapAttr.setValidated(1);
				ldapAttr.setError_code("D0022");
				ldapAttr.setError_msg("Aduser alreay exist");
			}
			ctx.close();

		} catch (NamingException e) {
			System.err.println("NamingException Problem creating object: " + e);
			ldapAttr.setValidated(0);
			ldapAttr.setError_code("D9999");
			ldapAttr.setError_msg(e.getMessage());

		}

		catch (IOException e) {
			System.err.println("IOException Problem creating object: " + e);
			ldapAttr.setValidated(0);
			ldapAttr.setError_code("D9999");
			ldapAttr.setError_msg(e.getMessage());

		}

		return ldapAttr;
	}
	// fetch user

	public static LDAPAttributesBean fetch(LDAPAttributesBean ldapAttr, LdapContext ctx) {

		Attributes attributes = null;
		// DirContext ldContext;
		InputStream infoad = new RegCPF().getClass().getClassLoader()
				.getResourceAsStream("com/spring/uam/ldap/adconfig.properties");
		try {
			aduserProp.load(infoad);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String baseDN = aduserProp.getProperty("AduserbaseDN");
		System.out.println(baseDN + " fetch user");
		// String baseDN = "OU=External,OU=ETIQA USERS,DC=etiqa,DC=local";
		// LDAPAttributesBean ldapAttr = new LDAPAttributesBean();
		try {
			
			DirContext o = (DirContext) ctx.lookup("cn=" + ldapAttr.getUsername() + "," + baseDN);
			// System.out.println("search done\n");
			attributes = o.getAttributes("");
			for (NamingEnumeration ae = attributes.getAll(); ae.hasMoreElements();) {
				Attribute attr = (Attribute) ae.next();
				String attrId = attr.getID();

				for (NamingEnumeration vals = attr.getAll(); vals.hasMore();) {
					String thing = vals.next().toString();
					ldapAttr.setCheckuser(1);
					// System.out.println(thing +" fetch user exist");
					ldapAttr.setError_code("D0000");
					ldapAttr.setError_msg("success");

					// System.out.println(attrId + ": " + thing);
				}
			}
		} catch (Exception e) {
			// System.out.println(" fetch error: " + e.getMessage());
			ldapAttr.setCheckuser(0);
			ldapAttr.setError_code("D9999");
			ldapAttr.setError_msg(e.getMessage());
			// System.exit(-1);
		}
		return ldapAttr;
	}

	public void loadADuserProps() {
		InputStream infoad = this.getClass().getClassLoader()
				.getResourceAsStream("com/spring/uam/ldap/adconfig.properties");
		try {
			aduserProp.load(infoad);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		RegCPF regcpf = new RegCPF();
		// regcpf.loadADProps();
		LDAPAttributesBean ldapAttr = new LDAPAttributesBean();
		ldapAttr.setUsername("Tulasi");
		ldapAttr.setPassword("Etiqa@123");

		ldapAttr = ADUserCreate("Tulasi", "Etiqa@123");

		System.out.println(ldapAttr.getError_code() + " fetch user and Update password");

	}

}