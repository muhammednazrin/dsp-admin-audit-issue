package com.spring.uam.ldap;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.spring.uam.common.pojo.RegisterEmailVo;

public class EmailCPF {
	static Properties mailServerProperties;
	static Session getMailSession;
	static MimeMessage generateMailMessage;
	private static Properties activeDirMailProp = new Properties();
	// ProductMailTemplateLoad productTemplate = null;

	/*
	 * public void DspEmailDispatchProcessor(ProductMailTemplateLoad
	 * productTemplate){ this.productTemplate = productTemplate; }
	 */
	public String generateAndSendEmail(RegisterEmailVo registerEmailVo) throws FileNotFoundException {
		loadEmailProps();
		String res = null;
		String Subject = registerEmailVo.getEmailSubject();
		String Content = registerEmailVo.getTemplateEmail();

		
		// Assuming you are sending email from localhost
		String host = activeDirMailProp.getProperty("emailhost");
		String fromemail = activeDirMailProp.getProperty("from");
		String port = activeDirMailProp.getProperty("emailport");
		
		System.out.println(host);

		// Get system properties
		// SMTP Prod Email---
		Properties prop = System.getProperties();
		//prop.setProperty("mail.smtp.host", host);
		 prop.put("mail.smtp.host",host); 
		 prop.put("mail.smtp.port", port);
		 prop.put("mail.smtp.auth", "false");
		 prop.put("mail.smtp.starttls.enable", "false");
		 prop.put("mail.smtp.ssl.trust", host);
		Session session = Session.getDefaultInstance(prop);
	

		

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromemail));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(registerEmailVo.getUserEmail()));
			message.setSubject(Subject);
			message.setContent(Content, "text/html");

			Transport.send(message);

			res = "Done";
			System.out.println("response uam email " + res);

		} catch (MessagingException e) {
			e.printStackTrace();
			res = "Failed";
		} catch (Exception e) {
			e.printStackTrace();
			res = "Failed";
		}
		return res;
	}

	private void loadEmailProps() {
		InputStream infoad = this.getClass().getClassLoader()
				.getResourceAsStream("com/spring/uam/ldap/emailconfig.properties");
		try {
			activeDirMailProp.load(infoad);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
