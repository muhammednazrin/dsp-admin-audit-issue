package com.spring.uam.ldap;

import java.util.Date;

public class LDAPAttributesBean {
	private String CN;
	private Date last_logon;
	private Date last_password_update;
	private Integer wrong_password_attempt;
	private Integer validated;
	private String FULNAME;
	private int checkuser;
	private String error_code;
	private String error_msg;
	private String password;
	private String username;
	private int userrole;
	private String curPassword;

	public String getCurPassword() {
		return curPassword;
	}

	public void setCurPassword(String curPassword) {
		this.curPassword = curPassword;
	}

	public int getUserrole() {
		return userrole;
	}

	public void setUserrole(int userrole) {
		this.userrole = userrole;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getError_code() {
		return error_code;
	}

	public void setError_code(String error_code) {
		this.error_code = error_code;
	}

	public String getError_msg() {
		return error_msg;
	}

	public void setError_msg(String error_msg) {
		this.error_msg = error_msg;
	}

	public int getCheckuser() {
		return checkuser;
	}

	public void setCheckuser(int checkuser) {
		this.checkuser = checkuser;
	}

	public String getCN() {
		return CN;
	}

	public void setCN(String cN) {
		CN = cN;
	}

	public Date getLast_logon() {
		return last_logon;
	}

	public void setLast_logon(Date last_logon) {
		this.last_logon = last_logon;
	}

	public Date getLast_password_update() {
		return last_password_update;
	}

	public void setLast_password_update(Date last_password_update) {
		this.last_password_update = last_password_update;
	}

	public Integer getWrong_password_attempt() {
		return wrong_password_attempt;
	}

	public void setWrong_password_attempt(Integer wrong_password_attempt) {
		this.wrong_password_attempt = wrong_password_attempt;
	}

	public Integer getValidated() {
		return validated;
	}

	public void setValidated(Integer validated) {
		this.validated = validated;
	}

	public String getFULNAME() {
		return FULNAME;
	}

	public void setFULNAME(String fULNAME) {
		FULNAME = fULNAME;
	}

}
