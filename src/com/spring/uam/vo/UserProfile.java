package com.spring.uam.vo;

import java.math.BigDecimal;
import java.util.Date;

public class UserProfile {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_ADM_TBL_USER.ID
	 *
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	private Short id;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_ADM_TBL_USER.PF_NUMBER
	 *
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	private String pfNumber;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_ADM_TBL_USER.NAME
	 *
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	private String name;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_ADM_TBL_USER.GROUP_ID
	 *
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	private BigDecimal groupId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_ADM_TBL_USER.STATUS
	 *
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	private String status;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_ADM_TBL_USER.CREATE_DATE
	 *
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	private Date createDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_ADM_TBL_USER.UPDATE_DATE
	 *
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	private Date updateDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_ADM_TBL_USER.CREATE_BY
	 *
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	private String createBy;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_ADM_TBL_USER.UPDATE_BY
	 *
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	private String updateBy;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_ADM_TBL_USER.LAST_LOGIN
	 *
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	private Date lastLogin;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_ADM_TBL_USER.EMAIL
	 *
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	private String email;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_ADM_TBL_USER.REMARKS
	 *
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	private String remarks;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_ADM_TBL_USER.MANAGER_PF_NUMBER
	 *
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	private String managerPfNumber;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_ADM_TBL_USER.EMAIL_LINK_STATUS
	 *
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	private String emailLinkStatus;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_ADM_TBL_USER.ID
	 *
	 * @return the value of DSP_ADM_TBL_USER.ID
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	private Date lastLogout;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column DSP_ADM_TBL_USER.LOGOUT_FLAG
	 *
	 * @mbg.generated Wed May 02 17:16:51 SGT 2018
	 */
	private BigDecimal logoutFlag;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_ADM_TBL_USER.ID
	 *
	 * @return the value of DSP_ADM_TBL_USER.ID
	 * @mbg.generated Wed May 02 17:16:51 SGT 2018
	 */
	public Short getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_ADM_TBL_USER.ID
	 *
	 * @param id
	 *            the value for DSP_ADM_TBL_USER.ID
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public void setId(Short id) {
		this.id = id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_ADM_TBL_USER.PF_NUMBER
	 *
	 * @return the value of DSP_ADM_TBL_USER.PF_NUMBER
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public String getPfNumber() {
		return pfNumber;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_ADM_TBL_USER.PF_NUMBER
	 *
	 * @param pfNumber
	 *            the value for DSP_ADM_TBL_USER.PF_NUMBER
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public void setPfNumber(String pfNumber) {
		this.pfNumber = pfNumber;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_ADM_TBL_USER.NAME
	 *
	 * @return the value of DSP_ADM_TBL_USER.NAME
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public String getName() {
		return name;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_ADM_TBL_USER.NAME
	 *
	 * @param name
	 *            the value for DSP_ADM_TBL_USER.NAME
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_ADM_TBL_USER.GROUP_ID
	 *
	 * @return the value of DSP_ADM_TBL_USER.GROUP_ID
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public BigDecimal getGroupId() {
		return groupId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_ADM_TBL_USER.GROUP_ID
	 *
	 * @param groupId
	 *            the value for DSP_ADM_TBL_USER.GROUP_ID
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public void setGroupId(BigDecimal groupId) {
		this.groupId = groupId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_ADM_TBL_USER.STATUS
	 *
	 * @return the value of DSP_ADM_TBL_USER.STATUS
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_ADM_TBL_USER.STATUS
	 *
	 * @param status
	 *            the value for DSP_ADM_TBL_USER.STATUS
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_ADM_TBL_USER.CREATE_DATE
	 *
	 * @return the value of DSP_ADM_TBL_USER.CREATE_DATE
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_ADM_TBL_USER.CREATE_DATE
	 *
	 * @param createDate
	 *            the value for DSP_ADM_TBL_USER.CREATE_DATE
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_ADM_TBL_USER.UPDATE_DATE
	 *
	 * @return the value of DSP_ADM_TBL_USER.UPDATE_DATE
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_ADM_TBL_USER.UPDATE_DATE
	 *
	 * @param updateDate
	 *            the value for DSP_ADM_TBL_USER.UPDATE_DATE
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_ADM_TBL_USER.CREATE_BY
	 *
	 * @return the value of DSP_ADM_TBL_USER.CREATE_BY
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public String getCreateBy() {
		return createBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_ADM_TBL_USER.CREATE_BY
	 *
	 * @param createBy
	 *            the value for DSP_ADM_TBL_USER.CREATE_BY
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_ADM_TBL_USER.UPDATE_BY
	 *
	 * @return the value of DSP_ADM_TBL_USER.UPDATE_BY
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public String getUpdateBy() {
		return updateBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_ADM_TBL_USER.UPDATE_BY
	 *
	 * @param updateBy
	 *            the value for DSP_ADM_TBL_USER.UPDATE_BY
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_ADM_TBL_USER.LAST_LOGIN
	 *
	 * @return the value of DSP_ADM_TBL_USER.LAST_LOGIN
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public Date getLastLogin() {
		return lastLogin;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_ADM_TBL_USER.LAST_LOGIN
	 *
	 * @param lastLogin
	 *            the value for DSP_ADM_TBL_USER.LAST_LOGIN
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_ADM_TBL_USER.EMAIL
	 *
	 * @return the value of DSP_ADM_TBL_USER.EMAIL
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_ADM_TBL_USER.EMAIL
	 *
	 * @param email
	 *            the value for DSP_ADM_TBL_USER.EMAIL
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_ADM_TBL_USER.REMARKS
	 *
	 * @return the value of DSP_ADM_TBL_USER.REMARKS
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_ADM_TBL_USER.REMARKS
	 *
	 * @param remarks
	 *            the value for DSP_ADM_TBL_USER.REMARKS
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_ADM_TBL_USER.MANAGER_PF_NUMBER
	 *
	 * @return the value of DSP_ADM_TBL_USER.MANAGER_PF_NUMBER
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public String getManagerPfNumber() {
		return managerPfNumber;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_ADM_TBL_USER.MANAGER_PF_NUMBER
	 *
	 * @param managerPfNumber
	 *            the value for DSP_ADM_TBL_USER.MANAGER_PF_NUMBER
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public void setManagerPfNumber(String managerPfNumber) {
		this.managerPfNumber = managerPfNumber;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_ADM_TBL_USER.EMAIL_LINK_STATUS
	 *
	 * @return the value of DSP_ADM_TBL_USER.EMAIL_LINK_STATUS
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public String getEmailLinkStatus() {
		return emailLinkStatus;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_ADM_TBL_USER.EMAIL_LINK_STATUS
	 *
	 * @param emailLinkStatus
	 *            the value for DSP_ADM_TBL_USER.EMAIL_LINK_STATUS
	 * @mbg.generated Wed Apr 04 11:28:28 SGT 2018
	 */
	public void setEmailLinkStatus(String emailLinkStatus) {
		this.emailLinkStatus = emailLinkStatus;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_ADM_TBL_USER.LAST_LOGOUT
	 *
	 * @return the value of DSP_ADM_TBL_USER.LAST_LOGOUT
	 * @mbg.generated Wed May 02 17:16:51 SGT 2018
	 */
	public Date getLastLogout() {
		return lastLogout;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_ADM_TBL_USER.LAST_LOGOUT
	 *
	 * @param lastLogout
	 *            the value for DSP_ADM_TBL_USER.LAST_LOGOUT
	 * @mbg.generated Wed May 02 17:16:51 SGT 2018
	 */
	public void setLastLogout(Date lastLogout) {
		this.lastLogout = lastLogout;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column DSP_ADM_TBL_USER.LOGOUT_FLAG
	 *
	 * @return the value of DSP_ADM_TBL_USER.LOGOUT_FLAG
	 * @mbg.generated Wed May 02 17:16:51 SGT 2018
	 */
	public BigDecimal getLogoutFlag() {
		return logoutFlag;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column DSP_ADM_TBL_USER.LOGOUT_FLAG
	 *
	 * @param logoutFlag
	 *            the value for DSP_ADM_TBL_USER.LOGOUT_FLAG
	 * @mbg.generated Wed May 02 17:16:51 SGT 2018
	 */
	public void setLogoutFlag(BigDecimal logoutFlag) {
		this.logoutFlag = logoutFlag;
	}
}