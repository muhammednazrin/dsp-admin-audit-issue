package com.spring.uam.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserRoleExample {
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database table DSP_ADM_TBL_USER_ROLE
	 *
	 * @mbg.generated Wed Feb 21 15:13:35 SGT 2018
	 */
	protected String orderByClause;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database table DSP_ADM_TBL_USER_ROLE
	 *
	 * @mbg.generated Wed Feb 21 15:13:35 SGT 2018
	 */
	protected boolean distinct;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database table DSP_ADM_TBL_USER_ROLE
	 *
	 * @mbg.generated Wed Feb 21 15:13:35 SGT 2018
	 */
	protected List<Criteria> oredCriteria;

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_USER_ROLE
	 *
	 * @mbg.generated Wed Feb 21 15:13:35 SGT 2018
	 */
	public UserRoleExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_USER_ROLE
	 *
	 * @mbg.generated Wed Feb 21 15:13:35 SGT 2018
	 */
	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_USER_ROLE
	 *
	 * @mbg.generated Wed Feb 21 15:13:35 SGT 2018
	 */
	public String getOrderByClause() {
		return orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_USER_ROLE
	 *
	 * @mbg.generated Wed Feb 21 15:13:35 SGT 2018
	 */
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_USER_ROLE
	 *
	 * @mbg.generated Wed Feb 21 15:13:35 SGT 2018
	 */
	public boolean isDistinct() {
		return distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_USER_ROLE
	 *
	 * @mbg.generated Wed Feb 21 15:13:35 SGT 2018
	 */
	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_USER_ROLE
	 *
	 * @mbg.generated Wed Feb 21 15:13:35 SGT 2018
	 */
	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_USER_ROLE
	 *
	 * @mbg.generated Wed Feb 21 15:13:35 SGT 2018
	 */
	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_USER_ROLE
	 *
	 * @mbg.generated Wed Feb 21 15:13:35 SGT 2018
	 */
	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_USER_ROLE
	 *
	 * @mbg.generated Wed Feb 21 15:13:35 SGT 2018
	 */
	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_USER_ROLE
	 *
	 * @mbg.generated Wed Feb 21 15:13:35 SGT 2018
	 */
	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the
	 * database table DSP_ADM_TBL_USER_ROLE
	 *
	 * @mbg.generated Wed Feb 21 15:13:35 SGT 2018
	 */
	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("UserRole.ID is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("UserRole.ID is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(Short value) {
			addCriterion("UserRole.ID =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(Short value) {
			addCriterion("UserRole.ID <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(Short value) {
			addCriterion("UserRole.ID >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(Short value) {
			addCriterion("UserRole.ID >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(Short value) {
			addCriterion("UserRole.ID <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(Short value) {
			addCriterion("UserRole.ID <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<Short> values) {
			addCriterion("UserRole.ID in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<Short> values) {
			addCriterion("UserRole.ID not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(Short value1, Short value2) {
			addCriterion("UserRole.ID between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(Short value1, Short value2) {
			addCriterion("UserRole.ID not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andUserIdIsNull() {
			addCriterion("UserRole.USER_ID is null");
			return (Criteria) this;
		}

		public Criteria andUserIdIsNotNull() {
			addCriterion("UserRole.USER_ID is not null");
			return (Criteria) this;
		}

		public Criteria andUserIdEqualTo(Short value) {
			addCriterion("UserRole.USER_ID =", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotEqualTo(Short value) {
			addCriterion("UserRole.USER_ID <>", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdGreaterThan(Short value) {
			addCriterion("UserRole.USER_ID >", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdGreaterThanOrEqualTo(Short value) {
			addCriterion("UserRole.USER_ID >=", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLessThan(Short value) {
			addCriterion("UserRole.USER_ID <", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLessThanOrEqualTo(Short value) {
			addCriterion("UserRole.USER_ID <=", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdIn(List<Short> values) {
			addCriterion("UserRole.USER_ID in", values, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotIn(List<Short> values) {
			addCriterion("UserRole.USER_ID not in", values, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdBetween(Short value1, Short value2) {
			addCriterion("UserRole.USER_ID between", value1, value2, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotBetween(Short value1, Short value2) {
			addCriterion("UserRole.USER_ID not between", value1, value2, "userId");
			return (Criteria) this;
		}

		public Criteria andRoleModuleIdIsNull() {
			addCriterion("UserRole.ROLE_MODULE_ID is null");
			return (Criteria) this;
		}

		public Criteria andRoleModuleIdIsNotNull() {
			addCriterion("UserRole.ROLE_MODULE_ID is not null");
			return (Criteria) this;
		}

		public Criteria andRoleModuleIdEqualTo(Short value) {
			addCriterion("UserRole.ROLE_MODULE_ID =", value, "roleModuleId");
			return (Criteria) this;
		}

		public Criteria andRoleModuleIdNotEqualTo(Short value) {
			addCriterion("UserRole.ROLE_MODULE_ID <>", value, "roleModuleId");
			return (Criteria) this;
		}

		public Criteria andRoleModuleIdGreaterThan(Short value) {
			addCriterion("UserRole.ROLE_MODULE_ID >", value, "roleModuleId");
			return (Criteria) this;
		}

		public Criteria andRoleModuleIdGreaterThanOrEqualTo(Short value) {
			addCriterion("UserRole.ROLE_MODULE_ID >=", value, "roleModuleId");
			return (Criteria) this;
		}

		public Criteria andRoleModuleIdLessThan(Short value) {
			addCriterion("UserRole.ROLE_MODULE_ID <", value, "roleModuleId");
			return (Criteria) this;
		}

		public Criteria andRoleModuleIdLessThanOrEqualTo(Short value) {
			addCriterion("UserRole.ROLE_MODULE_ID <=", value, "roleModuleId");
			return (Criteria) this;
		}

		public Criteria andRoleModuleIdIn(List<Short> values) {
			addCriterion("UserRole.ROLE_MODULE_ID in", values, "roleModuleId");
			return (Criteria) this;
		}

		public Criteria andRoleModuleIdNotIn(List<Short> values) {
			addCriterion("UserRole.ROLE_MODULE_ID not in", values, "roleModuleId");
			return (Criteria) this;
		}

		public Criteria andRoleModuleIdBetween(Short value1, Short value2) {
			addCriterion("UserRole.ROLE_MODULE_ID between", value1, value2, "roleModuleId");
			return (Criteria) this;
		}

		public Criteria andRoleModuleIdNotBetween(Short value1, Short value2) {
			addCriterion("UserRole.ROLE_MODULE_ID not between", value1, value2, "roleModuleId");
			return (Criteria) this;
		}

		public Criteria andCreateDateIsNull() {
			addCriterion("UserRole.CREATE_DATE is null");
			return (Criteria) this;
		}

		public Criteria andCreateDateIsNotNull() {
			addCriterion("UserRole.CREATE_DATE is not null");
			return (Criteria) this;
		}

		public Criteria andCreateDateEqualTo(Date value) {
			addCriterion("UserRole.CREATE_DATE =", value, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateNotEqualTo(Date value) {
			addCriterion("UserRole.CREATE_DATE <>", value, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateGreaterThan(Date value) {
			addCriterion("UserRole.CREATE_DATE >", value, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
			addCriterion("UserRole.CREATE_DATE >=", value, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateLessThan(Date value) {
			addCriterion("UserRole.CREATE_DATE <", value, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateLessThanOrEqualTo(Date value) {
			addCriterion("UserRole.CREATE_DATE <=", value, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateIn(List<Date> values) {
			addCriterion("UserRole.CREATE_DATE in", values, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateNotIn(List<Date> values) {
			addCriterion("UserRole.CREATE_DATE not in", values, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateBetween(Date value1, Date value2) {
			addCriterion("UserRole.CREATE_DATE between", value1, value2, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateNotBetween(Date value1, Date value2) {
			addCriterion("UserRole.CREATE_DATE not between", value1, value2, "createDate");
			return (Criteria) this;
		}

		public Criteria andUpdateDateIsNull() {
			addCriterion("UserRole.UPDATE_DATE is null");
			return (Criteria) this;
		}

		public Criteria andUpdateDateIsNotNull() {
			addCriterion("UserRole.UPDATE_DATE is not null");
			return (Criteria) this;
		}

		public Criteria andUpdateDateEqualTo(Date value) {
			addCriterion("UserRole.UPDATE_DATE =", value, "updateDate");
			return (Criteria) this;
		}

		public Criteria andUpdateDateNotEqualTo(Date value) {
			addCriterion("UserRole.UPDATE_DATE <>", value, "updateDate");
			return (Criteria) this;
		}

		public Criteria andUpdateDateGreaterThan(Date value) {
			addCriterion("UserRole.UPDATE_DATE >", value, "updateDate");
			return (Criteria) this;
		}

		public Criteria andUpdateDateGreaterThanOrEqualTo(Date value) {
			addCriterion("UserRole.UPDATE_DATE >=", value, "updateDate");
			return (Criteria) this;
		}

		public Criteria andUpdateDateLessThan(Date value) {
			addCriterion("UserRole.UPDATE_DATE <", value, "updateDate");
			return (Criteria) this;
		}

		public Criteria andUpdateDateLessThanOrEqualTo(Date value) {
			addCriterion("UserRole.UPDATE_DATE <=", value, "updateDate");
			return (Criteria) this;
		}

		public Criteria andUpdateDateIn(List<Date> values) {
			addCriterion("UserRole.UPDATE_DATE in", values, "updateDate");
			return (Criteria) this;
		}

		public Criteria andUpdateDateNotIn(List<Date> values) {
			addCriterion("UserRole.UPDATE_DATE not in", values, "updateDate");
			return (Criteria) this;
		}

		public Criteria andUpdateDateBetween(Date value1, Date value2) {
			addCriterion("UserRole.UPDATE_DATE between", value1, value2, "updateDate");
			return (Criteria) this;
		}

		public Criteria andUpdateDateNotBetween(Date value1, Date value2) {
			addCriterion("UserRole.UPDATE_DATE not between", value1, value2, "updateDate");
			return (Criteria) this;
		}

		public Criteria andCreateByIsNull() {
			addCriterion("UserRole.CREATE_BY is null");
			return (Criteria) this;
		}

		public Criteria andCreateByIsNotNull() {
			addCriterion("UserRole.CREATE_BY is not null");
			return (Criteria) this;
		}

		public Criteria andCreateByEqualTo(Short value) {
			addCriterion("UserRole.CREATE_BY =", value, "createBy");
			return (Criteria) this;
		}

		public Criteria andCreateByNotEqualTo(Short value) {
			addCriterion("UserRole.CREATE_BY <>", value, "createBy");
			return (Criteria) this;
		}

		public Criteria andCreateByGreaterThan(Short value) {
			addCriterion("UserRole.CREATE_BY >", value, "createBy");
			return (Criteria) this;
		}

		public Criteria andCreateByGreaterThanOrEqualTo(Short value) {
			addCriterion("UserRole.CREATE_BY >=", value, "createBy");
			return (Criteria) this;
		}

		public Criteria andCreateByLessThan(Short value) {
			addCriterion("UserRole.CREATE_BY <", value, "createBy");
			return (Criteria) this;
		}

		public Criteria andCreateByLessThanOrEqualTo(Short value) {
			addCriterion("UserRole.CREATE_BY <=", value, "createBy");
			return (Criteria) this;
		}

		public Criteria andCreateByIn(List<Short> values) {
			addCriterion("UserRole.CREATE_BY in", values, "createBy");
			return (Criteria) this;
		}

		public Criteria andCreateByNotIn(List<Short> values) {
			addCriterion("UserRole.CREATE_BY not in", values, "createBy");
			return (Criteria) this;
		}

		public Criteria andCreateByBetween(Short value1, Short value2) {
			addCriterion("UserRole.CREATE_BY between", value1, value2, "createBy");
			return (Criteria) this;
		}

		public Criteria andCreateByNotBetween(Short value1, Short value2) {
			addCriterion("UserRole.CREATE_BY not between", value1, value2, "createBy");
			return (Criteria) this;
		}

		public Criteria andUpdateByIsNull() {
			addCriterion("UserRole.UPDATE_BY is null");
			return (Criteria) this;
		}

		public Criteria andUpdateByIsNotNull() {
			addCriterion("UserRole.UPDATE_BY is not null");
			return (Criteria) this;
		}

		public Criteria andUpdateByEqualTo(Short value) {
			addCriterion("UserRole.UPDATE_BY =", value, "updateBy");
			return (Criteria) this;
		}

		public Criteria andUpdateByNotEqualTo(Short value) {
			addCriterion("UserRole.UPDATE_BY <>", value, "updateBy");
			return (Criteria) this;
		}

		public Criteria andUpdateByGreaterThan(Short value) {
			addCriterion("UserRole.UPDATE_BY >", value, "updateBy");
			return (Criteria) this;
		}

		public Criteria andUpdateByGreaterThanOrEqualTo(Short value) {
			addCriterion("UserRole.UPDATE_BY >=", value, "updateBy");
			return (Criteria) this;
		}

		public Criteria andUpdateByLessThan(Short value) {
			addCriterion("UserRole.UPDATE_BY <", value, "updateBy");
			return (Criteria) this;
		}

		public Criteria andUpdateByLessThanOrEqualTo(Short value) {
			addCriterion("UserRole.UPDATE_BY <=", value, "updateBy");
			return (Criteria) this;
		}

		public Criteria andUpdateByIn(List<Short> values) {
			addCriterion("UserRole.UPDATE_BY in", values, "updateBy");
			return (Criteria) this;
		}

		public Criteria andUpdateByNotIn(List<Short> values) {
			addCriterion("UserRole.UPDATE_BY not in", values, "updateBy");
			return (Criteria) this;
		}

		public Criteria andUpdateByBetween(Short value1, Short value2) {
			addCriterion("UserRole.UPDATE_BY between", value1, value2, "updateBy");
			return (Criteria) this;
		}

		public Criteria andUpdateByNotBetween(Short value1, Short value2) {
			addCriterion("UserRole.UPDATE_BY not between", value1, value2, "updateBy");
			return (Criteria) this;
		}

		public Criteria andStatusIsNull() {
			addCriterion("UserRole.STATUS is null");
			return (Criteria) this;
		}

		public Criteria andStatusIsNotNull() {
			addCriterion("UserRole.STATUS is not null");
			return (Criteria) this;
		}

		public Criteria andStatusEqualTo(String value) {
			addCriterion("UserRole.STATUS =", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotEqualTo(String value) {
			addCriterion("UserRole.STATUS <>", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusGreaterThan(String value) {
			addCriterion("UserRole.STATUS >", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusGreaterThanOrEqualTo(String value) {
			addCriterion("UserRole.STATUS >=", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusLessThan(String value) {
			addCriterion("UserRole.STATUS <", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusLessThanOrEqualTo(String value) {
			addCriterion("UserRole.STATUS <=", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusLike(String value) {
			addCriterion("UserRole.STATUS like", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotLike(String value) {
			addCriterion("UserRole.STATUS not like", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusIn(List<String> values) {
			addCriterion("UserRole.STATUS in", values, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotIn(List<String> values) {
			addCriterion("UserRole.STATUS not in", values, "status");
			return (Criteria) this;
		}

		public Criteria andStatusBetween(String value1, String value2) {
			addCriterion("UserRole.STATUS between", value1, value2, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotBetween(String value1, String value2) {
			addCriterion("UserRole.STATUS not between", value1, value2, "status");
			return (Criteria) this;
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the
	 * database table DSP_ADM_TBL_USER_ROLE
	 *
	 * @mbg.generated do_not_delete_during_merge Wed Feb 21 15:13:36 SGT 2018
	 */
	public static class Criteria extends GeneratedCriteria {

		protected Criteria() {
			super();
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the
	 * database table DSP_ADM_TBL_USER_ROLE
	 *
	 * @mbg.generated Wed Feb 21 15:13:36 SGT 2018
	 */
	public static class Criterion {
		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}
}