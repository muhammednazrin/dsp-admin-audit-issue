package com.spring.utils;

public class ExcludeURLs {

	public static final String MAIN = "/";
	public static final String DOLOGIN = "doLogin";
	public static final String RESOURCES = "resources";
	public static final String ASSESTS = "assets";
	public static final String UAM_DASHBOARD = "dashboard";
	public static final String DSPADMIN_DASHBOARD = "getDashboard";
	public static final String GETMENU = "getMenu";
	public static final String SUBMIT_MODULE ="submitModule";
	public static final String DOLOGOUT = "/doLogout";
	public static final String ROADTAX = "/roadTaxLoginDone";
	public static final String ROADTAX_ASSESTS = "roadtax";
	public static final String BANCAPA = "/BancaPAlandingpg";
	public static final String CLAIMS = "/claimLogin";
	public static final String FORGETPASSWORD = "/forgotPassword";
	public static final String SETPASSWORD = "/setpw";
	public static final String SETPASSWORDDone = "/setPwDone";
	public static final String CWPDone = "/modulelist";
	public static final String CWPDASBOARD = "/cwpDashboard";
	public static final String CSS = "css";
	public static final String FONTS = "fonts";
	public static final String IMAGES = "images";
	public static final String JS = "js";
	public static final String PLUGINS = "plugins";
	public static final String POLLSREPORT = "/PollsReportData";
	

}
