package com.spring.utils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

public class Utils {

	private static final Logger logger = Logger.getLogger(Utils.class);

	public static void main(String arg[]) {

		Date date = StringToDate("27/08/2018", "dd/MM/yyyy");
		System.out.println(date);
	}
/////////////////////
	public static Date StringToDate(String dateInString, String dateFormat) {

		Date date = null;
		if (!ServiceValidationUtils.isEmptyStringTrim(dateInString)
				&& !ServiceValidationUtils.isEmptyStringTrim(dateFormat)) {
			SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
			try {
				date = formatter.parse(dateInString);

			} catch (ParseException e) {
				logger.info("Error in Converting String to Date");
				e.printStackTrace();
			}
		}

		return date;

	}

	public static String numberFormat(String val) {
		Double iVal = null;
		iVal = Double.parseDouble(val == null ? "0" : val);
		DecimalFormat df = new DecimalFormat("0,000");
		System.out.println(df.format(iVal));
		String formatVal = df.format(iVal);
		return formatVal;
	}

}
//fgfg
