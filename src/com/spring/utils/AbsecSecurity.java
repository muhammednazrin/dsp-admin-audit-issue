package com.spring.utils;

import java.util.Base64;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AbsecSecurity {

	static String _key = "bokka123bokka123"; // 128 bit key (16 bytes)
	static String _initVector = "dhoomdhaamkarlo0"; // 16 bytes IV

	public static String GetGUID() {
		String str = "";
		str = UUID.randomUUID().toString();
		return str;
	}

	public static String Encrypt(String str) {
		return EncryptMe(_key, _initVector, str);
	}

	public static String Decrypt(String encryptedStr) {
		return DecryptMe(_key, _initVector, encryptedStr);
	}

	private static String EncryptMe(String key, String initVector, String value) {
		String strEncrypted = "";

		try {
			IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

			byte[] encrypted = cipher.doFinal(value.getBytes());

			strEncrypted = StringToHex(Base64.getEncoder().encode(encrypted));

		} catch (Exception ex) {
			strEncrypted = "Error : " + ex.getMessage();
		}

		return strEncrypted;

	}

	private static String StringToHex(byte[] bytes) {
		final StringBuilder builder = new StringBuilder();
		for (byte b : bytes) {
			builder.append(String.format("%02x", b));
		}

		return builder.toString();
	}

	private static String HexToString(String str) {
		int len = str.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(str.charAt(i), 16) << 4) + Character.digit(str.charAt(i + 1), 16));
		}
		return new String(data);
	}

	private static String DecryptMe(String key, String initVector, String encrypted) {
		String strDecrypted = "";
		try {

			String encryptedOriginal = HexToString(encrypted);

			IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

			byte[] original = cipher.doFinal(Base64.getDecoder().decode(encryptedOriginal));

			strDecrypted = new String(original);

		} catch (Exception ex) {
			strDecrypted = "Error : " + ex.getMessage();
		}

		return strDecrypted;

	}

}
