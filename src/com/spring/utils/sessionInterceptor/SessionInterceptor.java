package com.spring.utils.sessionInterceptor;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.spring.utils.ExcludeURLs;

public class SessionInterceptor implements Filter {

	private static final long MAX_INACTIVE_SESSION_TIME = 15 * 10000;
	final static Logger logger = Logger.getLogger(SessionInterceptor.class);

	@Override
	public void destroy() {

	}
	////////////testinghhhhhhhhhhfggdgfdgdgdgdfgdgdfgdgdgd//////////////////////testing //////////////////////////////////ghghhhg
///////////////////////////////////////////////////////////////////htgfjshnhghfgfgfgfggfgfgfgfgfg
	@Override
	public void doFilter(ServletRequest requestq, ServletResponse responseq, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) requestq;
		HttpServletResponse response = (HttpServletResponse) responseq;
		HttpSession session = request.getSession();

		  logger.info("Check module id inside intercepter 1:"+session.getAttribute("ModuleId"));

		  
		  //roleModule//"ModuleId", rmid//hjhjhjh
		// Start------------ Check The Session Validity
		Long timeElapsed = System.currentTimeMillis() - request.getSession().getLastAccessedTime();
		 logger.info("Time since last request in this session: {} ms" +String.valueOf(timeElapsed));
		/*
		 * if (System.currentTimeMillis() - session.getLastAccessedTime() >
		 * MAX_INACTIVE_SESSION_TIME) {
		 * logger.warn("Logging out, due to inactive session"); session.invalidate();
		 * response.sendRedirect(request.getContextPath() + ExcludeURLs.MAIN); return; }
		 */
		// End------------ Check The Session Validity

		String path = request.getRequestURI().substring(request.getContextPath().length());
		 logger.info("path**"+path);
		
		 
		Boolean exception = false;

		// Exempted URLS
		if (ExcludeURLs.MAIN.equalsIgnoreCase(path)) {
			 logger.info("hello console to print the UAM Dashboard"+"exception = true; " );
			 logger.info("ExcludeURLs MAIN**"+ExcludeURLs.MAIN);
			exception = true;
		}
		 logger.info(ExcludeURLs.UAM_DASHBOARD.toLowerCase()+"UAM_DASHBOARD MAIN");
		logger.info(" UAM Dashboard_session interceptor URL Arrangement"+ExcludeURLs.UAM_DASHBOARD.toLowerCase()+"UAM_DASHBOARD MAIN");
		if (path.toLowerCase().contains(ExcludeURLs.UAM_DASHBOARD.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.DSPADMIN_DASHBOARD.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.ROADTAX.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.BANCAPA.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.CLAIMS.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.DOLOGIN.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.RESOURCES.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.ASSESTS.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.ROADTAX_ASSESTS.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.SUBMIT_MODULE.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.DOLOGOUT.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.FORGETPASSWORD.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.SETPASSWORD.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.SETPASSWORDDone.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.CWPDone.toLowerCase())
				
		/*		|| path.toLowerCase().contains(ExcludeURLs.CSS.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.FONTS.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.IMAGES.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.JS.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.PLUGINS.toLowerCase())
				|| path.toLowerCase().contains(ExcludeURLs.POLLSREPORT.toLowerCase())*/  
				) {
			 logger.info("exception ; "+exception );
			exception = true;
		}
		if (exception == false) {
			// ----------------The URL belong to UAM
			if (path.toLowerCase().contains("uam".toLowerCase())) {
				if (session.getAttribute("ModuleId") != null) {
					logger.info("hello console to print the UAM Dashboard"+"check the module ID in session, if not for DSPADMIN then kick the user out");
					Integer moduleid = (Integer) session.getAttribute("ModuleId");
					if (moduleid != 6) {
						 logger.info("hello console to print the UAM Dashboard"+"Module Id is not for UAM " + moduleid);
						response.sendRedirect(request.getContextPath() + ExcludeURLs.DOLOGOUT);
						return ;
					}
				}
				if (session.getAttribute("ModuleId") == null) {
					 logger.info("hello console to print the UAM Dashboard"+"UAM URL Hit directly without click on the module form module list");
					response.sendRedirect(request.getContextPath() + ExcludeURLs.DOLOGOUT);
					return;
				}
			}
			// ----------------The URL belong to UAM
			if (path.toLowerCase().contains("claims".toLowerCase())) {
			logger.info("hello console to print the UAM Dashboard"+"claims module id :"+session.getAttribute("ModuleId"));
				logger.info("hello console to print the UAM Dashboard"+"claims module id :"+session.getAttribute("ModuleId"));
				if (session.getAttribute("ModuleId") != null) {
					// check the module ID in session, if not for DSPADMIN then kick the user out
					Integer moduleid = (Integer) session.getAttribute("ModuleId");
					if (moduleid != 4) {
						 logger.info("hello console to print the UAM Dashboard"+"Module Id is not for claims " + moduleid);
						response.sendRedirect(request.getContextPath() + ExcludeURLs.DOLOGOUT);
						return;
					}
				}
				if (session.getAttribute("ModuleId") == null) {
					 logger.info("hello console to print the UAM Dashboard"+"claims URL Hit directly without click on the module form module list");
					response.sendRedirect(request.getContextPath() + ExcludeURLs.DOLOGOUT);
					return;
				}
			}
			// ----------------The URL belong to Banca
			if (path.toLowerCase().contains("banca".toLowerCase())) {
				 logger.info("banca module id :"+session.getAttribute("ModuleId"));
				 logger.info("banca module id :"+session.getAttribute("ModuleId"));
				if (session.getAttribute("ModuleId") != null) {
					logger.info("check the module ID in session, if not for DSPADMIN then kick the user out");
					Integer moduleid = (Integer) session.getAttribute("ModuleId");
					if (moduleid != 5) {
						 logger.info("Module Id is not for banca " + moduleid);
						response.sendRedirect(request.getContextPath() + ExcludeURLs.DOLOGOUT);
						return;
					}
				}
				if (session.getAttribute("ModuleId") == null) {
					logger.info("banca URL Hit directly without click on the module form module list");
					response.sendRedirect(request.getContextPath() + ExcludeURLs.DOLOGOUT);
					return;
				}
			}
			// ----------------The URL belong to Roadtax
			if (path.toLowerCase().contains("rtx".toLowerCase())) {
				logger.info("rtx module :"+session.getAttribute("ModuleId"));
				logger.info("rtx FullName :"+session.getAttribute("FullName"));

				if (session.getAttribute("moduleId") != null) {
					// check the module ID in session, if not for DSPADMIN then kick the user out
					Integer moduleid = (Integer) session.getAttribute("ModuleId");
					if (moduleid != 3) {
						logger.info("Module Id is not for Roadtax " + moduleid);
						response.sendRedirect(request.getContextPath() + ExcludeURLs.DOLOGOUT);
						return;
					}
				}
				if (session.getAttribute("ModuleId") == null) {
					logger.info("Roadtax URL Hit directly without click on the module form module list");
					response.sendRedirect(request.getContextPath() + ExcludeURLs.DOLOGOUT);
					return;
				}
			}
//  ----------------The URL belong to cwp
			if (path.toLowerCase().contains("cwp".toLowerCase())) {
				logger.info("cwp module :"+session.getAttribute("ModuleId"));
				logger.info("cwp FullName :"+session.getAttribute("FullName"));
				if (session.getAttribute("ModuleId") != null) {
					logger.info("cwp FullName session not null:"+session.getAttribute("ModuleId"));
					// check the module ID in session, if not for DSPADMIN then kick the user out
					Integer moduleid = (Integer) session.getAttribute("ModuleId");
					request.getRequestURI().replaceAll("cwp/", "/");
					logger.info(request+"request in cwp");
					if (moduleid != 2) {
						logger.info("cwp FullName session not 2:"+moduleid);
						 logger.info("Module Id is not for Roadtax " + moduleid);
							logger.info("cwp ContextPath:"+request.getContextPath());
							logger.info("cwp DOLOGOUT:"+ExcludeURLs.DOLOGOUT);
						response.sendRedirect(request.getContextPath() + ExcludeURLs.DOLOGOUT);
						return;
					}
				}
				if (session.getAttribute("ModuleId") == null) {
					logger.info("cwp FullName session  null:"+session.getAttribute("ModuleId"));
					logger.info("Roadtax URL Hit directly without click on the module form module list");
					logger.info("cwp ContextPath:"+request.getContextPath());
					logger.info("cwp DOLOGOUT:"+ExcludeURLs.DOLOGOUT);
					response.sendRedirect(request.getContextPath() + ExcludeURLs.DOLOGOUT);
					return;
				}
			}
			// The URL belong to DSPADMIN
			if (!path.toLowerCase().contains("uam".toLowerCase())
					&& !path.toLowerCase().contains("claims".toLowerCase())
					&& !path.toLowerCase().contains("banca".toLowerCase())
					&& !path.toLowerCase().contains("rtx".toLowerCase())
					&& !path.toLowerCase().contains("cwp".toLowerCase())) {
				if (session.getAttribute("moduleId") != null) {
					// check the module ID in session, if not for DSPADMIN then kick the user out
					Integer moduleid = (Integer) session.getAttribute("ModuleId");
					if (moduleid != 1) {
						 logger.info("Module Id is not for DSPADmin " + moduleid);
						response.sendRedirect(request.getContextPath() + ExcludeURLs.DOLOGOUT);
						return;
					}
				}
				if (session.getAttribute("ModuleId") == null && session.getAttribute("pfnumber") != null) {
					logger.info("DSP ADMIN URL Hit directly without click on the module form module list");
					response.sendRedirect(request.getContextPath() + ExcludeURLs.DOLOGOUT);
					return;
				}

				if (session.getAttribute("pfnumber") == null) {
					logger.info("DSP ADMIN URL Hit directly without click on the module form module list");
					response.sendRedirect(request.getContextPath() + ExcludeURLs.MAIN);
					logger.info("response.sendRedirect(request.getContextPath() + ExcludeURLs.MAIN");
					return;
				}
			}

		}

		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		logger.info("XSSPreventionFilter: init()");
	}

}
