package com.spring.admin;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.etiqa.DAO.UserValidation_DAO;
import com.etiqa.dsp.dao.common.pojo.RegisterEmailVo;
import com.etiqa.model.agent.Agent;
import com.spring.VO.AdminParam;
import com.spring.VO.AdminParamExample;
import com.spring.VO.AgentDocument;
import com.spring.VO.AgentDocumentExample;
import com.spring.VO.AgentLink;
import com.spring.VO.AgentLinkExample;
import com.spring.VO.AgentProdMap;
import com.spring.VO.AgentProdMapExample;
import com.spring.VO.AgentProdMapExample.Criteria;
import com.spring.VO.AgentProfile;
import com.spring.VO.AgentProfileExample;
import com.spring.VO.Approval;
import com.spring.VO.ApprovalExample;
import com.spring.VO.ApprovalLog;
import com.spring.VO.ApprovalLogExample;
import com.spring.VO.CommonQQ;
import com.spring.VO.CommonQQExample;
import com.spring.VO.Products;
import com.spring.VO.ProductsExample;
import com.spring.constant.QQConstant;
import com.spring.mapper.AdminParamMapper;
import com.spring.mapper.AgentDocumentMapper;
import com.spring.mapper.AgentLinkMapper;
import com.spring.mapper.AgentProdMapMapper;
import com.spring.mapper.AgentProfileMapper;
import com.spring.mapper.ApprovalLogMapper;
import com.spring.mapper.ApprovalMapper;
import com.spring.mapper.CommonQQMapper;
import com.spring.mapper.ProductsMapper;
import com.spring.uam.ldap.LDAPAttributesBean;
import com.spring.uam.ldap.RegCPF;
import com.spring.utils.AbsecSecurity;
import com.spring.utils.AdvancedEncryptionStandard;
import com.spring.utils.ServiceValidationUtils;
import com.spring.utils.captcha.BasicExample;
import com.spring.utils.sessionInterceptor.SessionInterceptor;

import test.QQConstantTest;

@Controller
public class AgentController {

	final static Logger logger = Logger.getLogger(AgentController.class);

	// final static Logger logger = Logger.getLogger(SessionInterceptor.class);
	AgentProfileMapper agentProfileMapper;
	ProductsMapper productsMapper;
	AgentProdMapMapper agentProdMapMapper;
	CommonQQMapper commonQQMapper;
	AdminParamMapper adminParamMapper;
	AgentDocumentMapper agentDocumentMapper;
	AgentLinkMapper agentLinkMapper;
	ApprovalLogMapper approvalLogMapper;
	ApprovalMapper approvalMapper;
	private static Properties aduserInfoProp = new Properties();

	@Autowired
	public AgentController(AgentProfileMapper agentProfileMapper, ProductsMapper productsMapper,
			AgentProdMapMapper agentProdMapMapper, CommonQQMapper commonQQMapper, AdminParamMapper adminParamMapper,
			AgentDocumentMapper agentDocumentMapper, AgentLinkMapper agentLinkMapper,
			ApprovalLogMapper approvalLogMapper, ApprovalMapper approvalMapper

	) {
		this.agentProfileMapper = agentProfileMapper;
		this.productsMapper = productsMapper;
		this.agentProdMapMapper = agentProdMapMapper;
		this.commonQQMapper = commonQQMapper;
		this.adminParamMapper = adminParamMapper;
		this.agentDocumentMapper = agentDocumentMapper;
		this.agentLinkMapper = agentLinkMapper;
		this.approvalLogMapper = approvalLogMapper;
		this.approvalMapper = approvalMapper;

	}

	public AgentController(ApprovalLogMapper approvalLogMapper, ApprovalMapper approvalMapper) {
		// TODO Auto-generated constructor stub
	}

	@RequestMapping("/showTranReport")
	public String showTranReport(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		return "admin-report-transaction";

	}

	@RequestMapping("/showAgentReport")
	public String showAgentReport(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		return "admin-report-agent";

	}

	@RequestMapping("/adminLogout")

	public void adminLogout(HttpServletRequest request, Model model, HttpServletResponse response) {

		request.getSession().removeAttribute("user");
		request.getSession().removeAttribute("FullName");
		request.getSession().removeAttribute("logedUser");
		request.getSession().removeAttribute("level_1");
		request.getSession().removeAttribute("level_2");
		request.getSession().removeAttribute("level_3");
		request.getSession().removeAttribute("level_4");

		request.getSession().invalidate();

		// Added by pramaiyan on 130320

		// String redirectToUrl = "uat.etiqa.com.my:4442/etiqauam"; // MSS UAT Public

		String redirectToUrl = "localhost:8081/uam/"; // local
		try {
			// response.sendRedirect("https://" + redirectToUrl);
			response.sendRedirect("http://" + redirectToUrl);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@RequestMapping(value = "/agentChangePw", method = RequestMethod.POST)
	public String agentChangePw(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String agentUserid = "";
		String curpassword = "";
		String newpassword = "";
		logger.info("Agent Controller Spring ");

		if (session.getAttribute("agentidno") != null) {
			agentUserid = (String) session.getAttribute("agentidno");
		}

		curpassword = request.getParameter("curpassword").trim();
		newpassword = request.getParameter("newpassword").trim();

		LDAPAttributesBean ldap = RegCPF.ADChangePassword(agentUserid, curpassword, newpassword);

		if (ldap.getValidated().equals(1) && ldap.getError_code().equals("D0000")) {
			model.addAttribute("changepw_success", "Update Password Successfully");
		} else {
			model.addAttribute("changepw_error", ldap.getError_msg());
		}

		return "agent-changepassword";

	}

	@RequestMapping(value = "/forgetPasswordDone", method = RequestMethod.POST)
	public String forgetPasswordDone(HttpServletRequest request, HttpServletResponse response, Model model) {

		// , @RequestParam String agentID
		String agentUserid = "";
		String email = "";

		logger.info(" Agent Controller Spring ");

		agentUserid = request.getParameter("username");
		email = request.getParameter("email").trim();

		RegisterEmailVo registerEmailVo = new RegisterEmailVo();
		registerEmailVo.setUserName(request.getParameter("username"));
		registerEmailVo.setUserNric(request.getParameter("username"));
		registerEmailVo.setUserPswd("");

		registerEmailVo.setEmailLink("");
		registerEmailVo.setUserEmail(email);
		registerEmailVo.setTemplateName("AgentRegister");
		registerEmailVo = UserValidation_DAO.emailCallingForRegister(registerEmailVo);
		model.addAttribute("forgetpw_success", "Send email Successfully");
		return "agent-forgetpassword";
		// return agentLogin(model);
	}

	@RequestMapping("/forgetPassword")
	public String forgetPassword(Model model) {

		return "agent-forgetpassword";

	}

	// List All Cyber Agents
	@RequestMapping(value = "/listAgents", method = RequestMethod.GET)
	public String list(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		String jobRole = request.getParameter("pid");
		logger.info(jobRole + "jobRole");

		if (jobRole != null && !jobRole.isEmpty()) {
			jobRole = String.valueOf(jobRole.charAt(0));
			session.setAttribute("jobRolesession", jobRole);
		}
		logger.info(jobRole + "jobRole only");
		logger.info("CSRF_TOKEN_FOR_SESSION_ATTR_NAME agent list***** "
				+ (String) session.getAttribute("CSRF_TOKEN_FOR_SESSION_ATTR_NAME"));
		List<AdminParam> agentCategoryList = new ArrayList<AdminParam>();
		List<AdminParam> agentTypeList = new ArrayList<AdminParam>();
		List<AdminParam> agentStatusList = new ArrayList<AdminParam>();
		List<AdminParam> agentIdTypeList = new ArrayList<AdminParam>();
		List<AdminParam> agentPhoneTypeList = new ArrayList<AdminParam>();
		List<AdminParam> agentStateList = new ArrayList<AdminParam>();

		AdminParamExample agentCategoryExample = new AdminParamExample();
		AdminParamExample.Criteria agentCategory_criteria = agentCategoryExample.createCriteria();
		agentCategory_criteria.andParamTypeEqualTo("AGENT_CATEGORY");
		agentCategoryList = adminParamMapper.selectByExample(agentCategoryExample);

		AdminParamExample agentTypeExample = new AdminParamExample();
		AdminParamExample.Criteria agentType_criteria = agentTypeExample.createCriteria();
		agentType_criteria.andParamTypeEqualTo("AGENT_TYPE");
		agentTypeList = adminParamMapper.selectByExample(agentTypeExample);

		AdminParamExample agentStatusExample = new AdminParamExample();
		AdminParamExample.Criteria agentStatus_criteria = agentStatusExample.createCriteria();
		agentStatus_criteria.andParamTypeEqualTo("AGENT_STATUS");
		agentStatusList = adminParamMapper.selectByExample(agentStatusExample);

		AdminParamExample agentIdTypeExample = new AdminParamExample();
		AdminParamExample.Criteria agentIdType_criteria = agentIdTypeExample.createCriteria();
		agentIdType_criteria.andParamTypeEqualTo("AGENT_ID_TYPE");
		agentIdTypeList = adminParamMapper.selectByExample(agentIdTypeExample);

		AdminParamExample agentPhoneTypeExample = new AdminParamExample();
		AdminParamExample.Criteria agentPhoneType_criteria = agentPhoneTypeExample.createCriteria();
		agentPhoneType_criteria.andParamTypeEqualTo("AGENT_PHONE_TYPE");
		agentPhoneTypeList = adminParamMapper.selectByExample(agentPhoneTypeExample);

		AdminParamExample agentStateExample = new AdminParamExample();
		AdminParamExample.Criteria agentState_criteria = agentStateExample.createCriteria();
		agentState_criteria.andParamTypeEqualTo("STATE");
		agentStateList = adminParamMapper.selectByExample(agentStateExample);

		model.addAttribute("agentCategoryList", agentCategoryList);
		model.addAttribute("agentTypeList", agentTypeList);
		model.addAttribute("agentStatusList", agentStatusList);
		model.addAttribute("agentIdTypeList", agentIdTypeList);
		model.addAttribute("agentPhoneTypeList", agentPhoneTypeList);
		model.addAttribute("agentStateList", agentStateList);

		List<AgentProfile> agentProfileList = new ArrayList<AgentProfile>();
		AgentProfileExample agentExample = new AgentProfileExample();
		agentExample.setOrderByClause("ap.ID desc");
		agentProfileList = agentProfileMapper.selectByExample(agentExample);
		model.addAttribute("agentProfileList", agentProfileList);
		return "agent-view-list-agent";
	}

	/*-------------------------------------------------------Start- Search Cyber Agents--------------------------------------------------------*/
	@RequestMapping(value = "/searchAgent", method = RequestMethod.POST)
	public String SearchAgent(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		List<AdminParam> agentCategoryList = new ArrayList<AdminParam>();
		List<AdminParam> agentTypeList = new ArrayList<AdminParam>();
		List<AdminParam> agentStatusList = new ArrayList<AdminParam>();
		List<AdminParam> agentIdTypeList = new ArrayList<AdminParam>();
		List<AdminParam> agentPhoneTypeList = new ArrayList<AdminParam>();
		List<AdminParam> agentStateList = new ArrayList<AdminParam>();

		AdminParamExample agentCategoryExample = new AdminParamExample();
		AdminParamExample.Criteria agentCategory_criteria = agentCategoryExample.createCriteria();
		agentCategory_criteria.andParamTypeEqualTo("AGENT_CATEGORY");
		agentCategoryList = adminParamMapper.selectByExample(agentCategoryExample);

		AdminParamExample agentTypeExample = new AdminParamExample();
		AdminParamExample.Criteria agentType_criteria = agentTypeExample.createCriteria();
		agentType_criteria.andParamTypeEqualTo("AGENT_TYPE");
		agentTypeList = adminParamMapper.selectByExample(agentTypeExample);

		AdminParamExample agentStatusExample = new AdminParamExample();
		AdminParamExample.Criteria agentStatus_criteria = agentStatusExample.createCriteria();
		agentStatus_criteria.andParamTypeEqualTo("AGENT_STATUS");
		agentStatusList = adminParamMapper.selectByExample(agentStatusExample);

		AdminParamExample agentIdTypeExample = new AdminParamExample();
		AdminParamExample.Criteria agentIdType_criteria = agentIdTypeExample.createCriteria();
		agentIdType_criteria.andParamTypeEqualTo("AGENT_ID_TYPE");
		agentIdTypeList = adminParamMapper.selectByExample(agentIdTypeExample);

		AdminParamExample agentPhoneTypeExample = new AdminParamExample();
		AdminParamExample.Criteria agentPhoneType_criteria = agentPhoneTypeExample.createCriteria();
		agentPhoneType_criteria.andParamTypeEqualTo("AGENT_PHONE_TYPE");
		agentPhoneTypeList = adminParamMapper.selectByExample(agentPhoneTypeExample);

		AdminParamExample agentStateExample = new AdminParamExample();
		AdminParamExample.Criteria agentState_criteria = agentStateExample.createCriteria();
		agentState_criteria.andParamTypeEqualTo("STATE");
		agentStateList = adminParamMapper.selectByExample(agentStateExample);

		model.addAttribute("agentCategoryList", agentCategoryList);
		model.addAttribute("agentTypeList", agentTypeList);
		model.addAttribute("agentStatusList", agentStatusList);
		model.addAttribute("agentIdTypeList", agentIdTypeList);
		model.addAttribute("agentPhoneTypeList", agentPhoneTypeList);
		model.addAttribute("agentStateList", agentStateList);

		String agentName = null;
		String agentCode = null;
		String agentCategory = null;
		String insuranceType = null;
		String status = null;
		String dateFrom = null;
		String dateTo = null;

		session.setAttribute("agentName", request.getParameter("agentName").trim());
		session.setAttribute("agentCategory", request.getParameter("agentCategory").trim());
		session.setAttribute("status", request.getParameter("status").trim());
		session.setAttribute("dateFrom", request.getParameter("dateFrom").trim());
		session.setAttribute("dateTo", request.getParameter("dateTo").trim());

		agentName = (String) session.getAttribute("agentName");
		agentCode = (String) session.getAttribute("agentCode");
		agentCategory = (String) session.getAttribute("agentCategory");
		insuranceType = (String) session.getAttribute("insuranceType");
		status = (String) session.getAttribute("status");
		dateFrom = (String) session.getAttribute("dateFrom");
		dateTo = (String) session.getAttribute("dateTo");

		SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/yyyy");

		List<AgentProfile> agentProfileList = new ArrayList<AgentProfile>();

		AgentProfileExample agentExample = new AgentProfileExample();
		AgentProfileExample.Criteria agent_criteria = agentExample.createCriteria();
		agentExample.setOrderByClause("ap.ID desc");
		if (!ServiceValidationUtils.isEmptyStringTrim(dateFrom) && !ServiceValidationUtils.isEmptyStringTrim(dateTo)) {
			Date datefrom = null;
			Date dateto = null;
			try {
				datefrom = format2.parse(dateFrom);
				dateto = format2.parse(dateTo);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				datefrom = null;
				dateto = null;
				e1.printStackTrace();
			}
			logger.info(datefrom);
			logger.info(dateto);
			if (datefrom != null && dateto != null) {
				agent_criteria.andRegistrationDateBetween(datefrom, dateto);
			}
		}

		if (!ServiceValidationUtils.isEmptyStringTrim(agentName)) {
			agent_criteria.andAgentNameLike("%" + agentName + "%");
		}
		if (!ServiceValidationUtils.isEmptyStringTrim(agentCategory)) {
			agent_criteria.andAgentCategoryEqualTo(agentCategory);
		}

		if (!ServiceValidationUtils.isEmptyStringTrim(status)) {
			agent_criteria.andStatusEqualTo(status);
		}

		agentProfileList = agentProfileMapper.selectByExample(agentExample);

		/*
		 * AgentProfile agentProfile= new AgentProfile();
		 * agentProfile.setAgentName(agentName); agentProfileList
		 * =agentProfileMapper.selectByTest(agentProfile);
		 */
		/*
		 * AgentDAO dao = new AgentDAOImpl();// daoFactory.getAgentDAO(); List<Agent>
		 * agent = dao.searchByList( agentName, agentCode, agentCategory, insuranceType,
		 * status, dateFrom, dateTo);
		 */

		// StartRow,
		// EndRow);

		model.addAttribute("agentProfileList", agentProfileList);
		// request.setAttribute("data", agent);
		return "agent-view-list-agent";
	}
	/*-------------------------------------------------------End- Search Cyber Agents--------------------------------------------------------*/

	/**
	 * ------------------------------------------------------------------------------------------------------------------------------------
	 **/

	/*-------------------------------------------------------Start- Add Agent----------------------------------------------------------------*/
	@RequestMapping(value = "/agentRegistration", method = RequestMethod.GET)
	public String agentRegistration(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String captcha_error = (String) request.getAttribute("captcha_error");
		String jobRole = request.getParameter("pid");
		logger.info(jobRole + "jobRole");

		if (jobRole != null && !jobRole.isEmpty()) {
			jobRole = String.valueOf(jobRole.charAt(0));
			session.setAttribute("jobRolesession", jobRole);
		}
		logger.info(jobRole + "jobRole only");
		if (!"y".equalsIgnoreCase(captcha_error)) {
			model.addAttribute("agentProfile", new AgentProfile());
		}

		List<AdminParam> agentCategoryList = new ArrayList<AdminParam>();
		List<AdminParam> agentTypeList = new ArrayList<AdminParam>();
		List<AdminParam> agentStatusList = new ArrayList<AdminParam>();
		List<AdminParam> agentIdTypeList = new ArrayList<AdminParam>();
		List<AdminParam> agentPhoneTypeList = new ArrayList<AdminParam>();
		List<AdminParam> agentStateList = new ArrayList<AdminParam>();

		AdminParamExample agentCategoryExample = new AdminParamExample();
		AdminParamExample.Criteria agentCategory_criteria = agentCategoryExample.createCriteria();
		agentCategory_criteria.andParamTypeEqualTo("AGENT_CATEGORY");
		agentCategoryList = adminParamMapper.selectByExample(agentCategoryExample);

		AdminParamExample agentTypeExample = new AdminParamExample();
		AdminParamExample.Criteria agentType_criteria = agentTypeExample.createCriteria();
		agentType_criteria.andParamTypeEqualTo("AGENT_TYPE");
		agentTypeList = adminParamMapper.selectByExample(agentTypeExample);

		AdminParamExample agentStatusExample = new AdminParamExample();
		AdminParamExample.Criteria agentStatus_criteria = agentStatusExample.createCriteria();
		agentStatus_criteria.andParamTypeEqualTo("AGENT_STATUS");
		agentStatusList = adminParamMapper.selectByExample(agentStatusExample);

		AdminParamExample agentIdTypeExample = new AdminParamExample();
		AdminParamExample.Criteria agentIdType_criteria = agentIdTypeExample.createCriteria();
		agentIdType_criteria.andParamTypeEqualTo("AGENT_ID_TYPE");
		agentIdTypeList = adminParamMapper.selectByExample(agentIdTypeExample);

		AdminParamExample agentPhoneTypeExample = new AdminParamExample();
		AdminParamExample.Criteria agentPhoneType_criteria = agentPhoneTypeExample.createCriteria();
		agentPhoneType_criteria.andParamTypeEqualTo("AGENT_PHONE_TYPE");
		agentPhoneTypeList = adminParamMapper.selectByExample(agentPhoneTypeExample);

		AdminParamExample agentStateExample = new AdminParamExample();
		AdminParamExample.Criteria agentState_criteria = agentStateExample.createCriteria();
		agentState_criteria.andParamTypeEqualTo("STATE");
		agentStateList = adminParamMapper.selectByExample(agentStateExample);

		model.addAttribute("agentCategoryList", agentCategoryList);
		model.addAttribute("agentTypeList", agentTypeList);
		model.addAttribute("agentStatusList", agentStatusList);
		model.addAttribute("agentIdTypeList", agentIdTypeList);
		model.addAttribute("agentPhoneTypeList", agentPhoneTypeList);
		model.addAttribute("agentStateList", agentStateList);

		return "agent-registration";
	}

	@RequestMapping(value = "/saveAgentDone", method = RequestMethod.POST)
	public String save(@ModelAttribute(value = "agentProfile") AgentProfile agentProfile, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam("agentLogo") CommonsMultipartFile file) throws ServletException, IOException {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String d1 = request.getParameter("registrationDate");
		SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-yy");
		Date date = null;

		try {
			date = format1.parse(d1);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// logger.info(format2.format(date));

		agentProfile.setRegistrationDate(date);
		Agent agent = new Agent();
		agentProfile.setEmailPassClicked("n"); // Password Link in Email Not Clicked

		logger.info("calling Ad Creating User " + agent.getUserId() + " " + agent.getPassword());

		BasicExample basicExample = new BasicExample();
		boolean isHuman = isCaptchaValid(request, request.getParameter("captchaCode"));
		if (isHuman) {
			basicExample.setCaptchaCorrect("Correct code");
			basicExample.setCaptchaIncorrect("");
		} else {
			Format formatter = new SimpleDateFormat("dd/MM/yyyy");
			String s = formatter.format(agentProfile.getRegistrationDate());
			model.addAttribute("agentProfile", agentProfile);
			model.addAttribute("s", s);

			request.setAttribute("captcha_error", "y");
			basicExample.setCaptchaCorrect("");
			basicExample.setCaptchaIncorrect("Incorrect code");
			model.addAttribute("basicExample", basicExample);
			basicExample.setCaptchaCode("");
			return agentRegistration(request, response, model);
		}

		agentProfileMapper.insert(agentProfile);

		// agentDocumentMapper

		String[] regDocs = request.getParameterValues("registrationDocument");
		if (regDocs != null) {
			List<AgentDocument> agentDocuments = new ArrayList<AgentDocument>(regDocs.length);
			for (String document : regDocs) {
				AgentDocument agentDocument = new AgentDocument();
				agentDocument.setAgentId(String.valueOf(agentProfile.getId()));
				agentDocument.setDocumentCode(document);
				agentDocuments.add(agentDocument);
				agentDocumentMapper.insert(agentDocument);
				request.setAttribute("message", "New agent added!");
			}

		}

		if (!file.isEmpty()) {

			String path = "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/configdsp/AgentLogos";
			String filename = file.getOriginalFilename();

			logger.info(path + filename);
			try {
				byte barr[] = file.getBytes();

				BufferedOutputStream bout = new BufferedOutputStream(
						new FileOutputStream(path + "/" + String.valueOf(agentProfile.getId())));
				bout.write(barr);
				bout.flush();
				bout.close();

			} catch (Exception e) {
				logger.info(e);
			}
		}
		loadADuserProps();

		String encryptionKey = aduserInfoProp.getProperty("agentEncrypt");
		String plainText = agentProfile.getIdNo();
		AdvancedEncryptionStandard advancedEncryptionStandard = new AdvancedEncryptionStandard(encryptionKey);
		String cipherText = null;
		String decryptedCipherText = null;
		try {
			cipherText = advancedEncryptionStandard.encrypt(plainText);
			// decryptedCipherText = advancedEncryptionStandard.decrypt(cipherText);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		logger.info(plainText);
		logger.info(cipherText);

		StringBuffer url = request.getRequestURL();
		String uri = request.getRequestURI();
		String ctx = request.getContextPath();
		String base = url.substring(0, url.length() - uri.length() + ctx.length());

		// String emailURL=base+"/"+"secure"+"/"+cipherText;
		String emailURL = cipherText;

		logger.info(emailURL);
		logger.info("Email Servicee Starting");
		RegisterEmailVo registerEmailVo = new RegisterEmailVo();
		registerEmailVo.setUserName(agentProfile.getAgentName());
		registerEmailVo.setUserNric(cipherText);
		registerEmailVo.setUserPswd("");
		registerEmailVo.setEmailLink(emailURL); // <--------
		registerEmailVo.setUserEmail(agentProfile.getEmail());
		registerEmailVo.setTemplateName("AgentRegister");
		registerEmailVo = UserValidation_DAO.emailCallingForRegister(registerEmailVo);

		model.addAttribute("agentAddedMessage", "Agent Added Successfully!");

		return list(request, response, model);

	}
	/*-------------------------------------------------------End- Add Agent----------------------------------------------------------------*/

	/**
	 * -----------------------------------------------------------------------------------------------------------------------------------
	 **/

	/*-------------------------------------------------------Start- Update Agent------------------------------------------------------------*/
	@RequestMapping(value = "/updateAgent", method = RequestMethod.POST)
	public String updateAgent(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam String id) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		logger.info("CSRF_TOKEN_FOR_SESSION_ATTR_NAME "
				+ (String) session.getAttribute("CSRF_TOKEN_FOR_SESSION_ATTR_NAME"));

		List<AdminParam> agentCategoryList = new ArrayList<AdminParam>();
		List<AdminParam> agentTypeList = new ArrayList<AdminParam>();
		List<AdminParam> agentStatusList = new ArrayList<AdminParam>();
		List<AdminParam> agentIdTypeList = new ArrayList<AdminParam>();
		List<AdminParam> agentPhoneTypeList = new ArrayList<AdminParam>();
		List<AdminParam> agentStateList = new ArrayList<AdminParam>();

		AdminParamExample agentCategoryExample = new AdminParamExample();
		AdminParamExample.Criteria agentCategory_criteria = agentCategoryExample.createCriteria();
		agentCategory_criteria.andParamTypeEqualTo("AGENT_CATEGORY");
		agentCategoryList = adminParamMapper.selectByExample(agentCategoryExample);

		AdminParamExample agentTypeExample = new AdminParamExample();
		AdminParamExample.Criteria agentType_criteria = agentTypeExample.createCriteria();
		agentType_criteria.andParamTypeEqualTo("AGENT_TYPE");
		agentTypeList = adminParamMapper.selectByExample(agentTypeExample);

		AdminParamExample agentStatusExample = new AdminParamExample();
		AdminParamExample.Criteria agentStatus_criteria = agentStatusExample.createCriteria();
		agentStatus_criteria.andParamTypeEqualTo("AGENT_STATUS");
		agentStatusList = adminParamMapper.selectByExample(agentStatusExample);

		AdminParamExample agentIdTypeExample = new AdminParamExample();
		AdminParamExample.Criteria agentIdType_criteria = agentIdTypeExample.createCriteria();
		agentIdType_criteria.andParamTypeEqualTo("AGENT_ID_TYPE");
		agentIdTypeList = adminParamMapper.selectByExample(agentIdTypeExample);

		AdminParamExample agentPhoneTypeExample = new AdminParamExample();
		AdminParamExample.Criteria agentPhoneType_criteria = agentPhoneTypeExample.createCriteria();
		agentPhoneType_criteria.andParamTypeEqualTo("AGENT_PHONE_TYPE");
		agentPhoneTypeList = adminParamMapper.selectByExample(agentPhoneTypeExample);

		AdminParamExample agentStateExample = new AdminParamExample();
		AdminParamExample.Criteria agentState_criteria = agentStateExample.createCriteria();
		agentState_criteria.andParamTypeEqualTo("STATE");
		agentStateList = adminParamMapper.selectByExample(agentStateExample);

		model.addAttribute("agentCategoryList", agentCategoryList);
		model.addAttribute("agentTypeList", agentTypeList);
		model.addAttribute("agentStatusList", agentStatusList);
		model.addAttribute("agentIdTypeList", agentIdTypeList);
		model.addAttribute("agentPhoneTypeList", agentPhoneTypeList);
		model.addAttribute("agentStateList", agentStateList);

		/*
		 * String id=null; if (request.getParameter("id") != null) { id=
		 * request.getParameter("id"); } else { id=String.valueOf((Integer)
		 * request.getAttribute("id")); }
		 */

		logger.info(id);
		AgentProfile agentProfile = new AgentProfile();
		agentProfile = agentProfileMapper.selectByPrimaryKey(Short.valueOf(id));

		Format formatter = new SimpleDateFormat("dd/MM/yyyy");
		String s = formatter.format(agentProfile.getRegistrationDate());

		logger.info("Date Original  " + agentProfile.getRegistrationDate());
		logger.info("DateFormated  " + s);

		List<AgentDocument> agentDocumentList = new ArrayList<AgentDocument>();
		AgentDocumentExample agentDocumentExample = new AgentDocumentExample();
		AgentDocumentExample.Criteria agentDocument_criteria = agentDocumentExample.createCriteria();
		agentDocument_criteria.andAgentIdEqualTo(String.valueOf(agentProfile.getId()));

		agentDocumentList = agentDocumentMapper.selectByExample(agentDocumentExample);

		model.addAttribute("agentProfile", agentProfile);

		model.addAttribute("agentDocumentList", agentDocumentList);
		model.addAttribute("s", s);
		return "agent-view-list-agent-edit";
	}

	@RequestMapping(value = "/updateAgentDone", method = RequestMethod.POST)
	public String updateAgentDone(@ModelAttribute(value = "agentProfile") AgentProfile agentProfile,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String id = request.getParameter("id");
		agentProfile.setId(new BigDecimal(id));

		String d1 = request.getParameter("registrationDate");
		SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-yy");
		Date date = null;

		try {
			date = format1.parse(d1);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// logger.info(format2.format(date));

		agentProfile.setRegistrationDate(date);

		AgentProfile agentProfileinDB = new AgentProfile();
		agentProfileinDB = agentProfileMapper.selectByPrimaryKey(Short.valueOf(id));
		String updateAction = null;

		logger.info("##### agentProfile.getEmail()" + agentProfile.getEmail());

		logger.info("##### agentProfileinDB.getEmail()" + agentProfileinDB.getEmail());

		// Removed this on 12-10-2017 for email update requirement
		// if (agentProfileinDB.getEmail().equalsIgnoreCase(agentProfile.getEmail())) {
		if (agentProfileinDB.getIdNo().equalsIgnoreCase(agentProfile.getIdNo())) {

			agentProfile.setEmail(agentProfile.getEmail());
			agentProfile.setIdNo(null);

			agentProfileMapper.updateByPrimaryKeySelective(agentProfile);

			AgentDocumentExample agentDocumentExample = new AgentDocumentExample();
			AgentDocumentExample.Criteria agentDocument_criteria = agentDocumentExample.createCriteria();
			agentDocument_criteria.andAgentIdEqualTo(String.valueOf(agentProfile.getId()));
			agentDocumentMapper.deleteByExample(agentDocumentExample);

			String[] regDocs = request.getParameterValues("registrationDocument");

			logger.info("regDocs" + regDocs);

			if (regDocs != null) {
				List<AgentDocument> agentDocuments = new ArrayList<AgentDocument>(regDocs.length);
				for (String document : regDocs) {
					AgentDocument agentDocument = new AgentDocument();
					agentDocument.setAgentId(String.valueOf(agentProfile.getId()));
					agentDocument.setDocumentCode(document);
					agentDocuments.add(agentDocument);
					agentDocumentMapper.insert(agentDocument);
				}

			}
			model.addAttribute("agentUpdatedMessage", "Agent Updated Successfully!");
		} else {
			model.addAttribute("agentUpdatedMessageE", "Agent Not Updated!");
		}
		// }
		// else {
		// model.addAttribute("agentUpdatedMessageE","Agent Not Updated!");
		// }

		return list(request, response, model);

		// forward(request, response, "agent-view-list-agent-edit.jsp");
	}

	@RequestMapping(value = "/resendEmail", method = RequestMethod.POST)
	public String resendEmailDone(@ModelAttribute(value = "agentProfile") AgentProfile agentProfile,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String id = request.getParameter("id");

		AgentProfile agentProfileinDB = new AgentProfile();
		agentProfileinDB = agentProfileMapper.selectByPrimaryKey(Short.valueOf(id));
		String updateAction = null;
		if (agentProfileinDB.getEmail().equalsIgnoreCase(agentProfile.getEmail())) {
			if (agentProfileinDB.getIdNo().equalsIgnoreCase(agentProfile.getIdNo())) {

				agentProfile.setId(new BigDecimal(id));
				agentProfile.setEmailPassClicked("n");
				agentProfileMapper.updateByPrimaryKeySelective(agentProfile);

				AgentDocumentExample agentDocumentExample = new AgentDocumentExample();
				AgentDocumentExample.Criteria agentDocument_criteria = agentDocumentExample.createCriteria();
				agentDocument_criteria.andAgentIdEqualTo(String.valueOf(agentProfile.getId()));
				agentDocumentMapper.deleteByExample(agentDocumentExample);

				String[] regDocs = request.getParameterValues("registrationDocument");
				if (regDocs != null) {
					List<AgentDocument> agentDocuments = new ArrayList<AgentDocument>(regDocs.length);
					for (String document : regDocs) {
						AgentDocument agentDocument = new AgentDocument();
						agentDocument.setAgentId(String.valueOf(agentProfile.getId()));
						agentDocument.setDocumentCode(document);
						agentDocuments.add(agentDocument);
						agentDocumentMapper.insert(agentDocument);
					}

				}

				String encryptionKey = "MZygpewJsCpRrfOr";
				String plainText = agentProfile.getIdNo();
				AdvancedEncryptionStandard advancedEncryptionStandard = new AdvancedEncryptionStandard(encryptionKey);
				String cipherText = null;
				String decryptedCipherText = null;
				try {
					cipherText = advancedEncryptionStandard.encrypt(plainText);
					// decryptedCipherText = advancedEncryptionStandard.decrypt(cipherText);

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				// String emailURL=base+"/"+"secure"+"/"+cipherText;
				String emailURL = cipherText;

				logger.info(emailURL);
				logger.info("Email Servicee Starting");
				RegisterEmailVo registerEmailVo = new RegisterEmailVo();
				registerEmailVo.setUserName(agentProfile.getAgentName());
				registerEmailVo.setUserNric(cipherText);
				registerEmailVo.setUserPswd("");
				registerEmailVo.setEmailLink(emailURL); // <--------
				registerEmailVo.setUserEmail(agentProfile.getEmail());
				registerEmailVo.setTemplateName("AgentRegister");
				registerEmailVo = UserValidation_DAO.emailCallingForRegister(registerEmailVo);

				// model.addAttribute("agentUpdatedMessage","Email Sent Successfully!");

				model.addAttribute("agentUpdatedMessage", "Email Sent Successfully!");
			} else {
				model.addAttribute("agentUpdatedMessageE", "Fail to send Email!");
			}
		} else {
			model.addAttribute("agentUpdatedMessageE", "Fail to send Email!");
		}

		return list(request, response, model);

		// forward(request, response, "agent-view-list-agent-edit.jsp");
	}

	/*-------------------------------------------------------End- Update Agent------------------------------------------------------------*/

	/**
	 * ---------------------------------------------------------------------------------------------------------------------------------
	 **/

	/*-------------------------------------------------------Start- Delete Agent----------------------------------------------------------*/
	@RequestMapping(value = "/deleteAgentDone", method = RequestMethod.POST)
	public String deleteAgentDone(@ModelAttribute(value = "agentProfile") AgentProfile agentProfile,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String id = request.getParameter("id");
		agentProfile.setId(new BigDecimal(id));
		agentProfileMapper.deleteByPrimaryKey(Short.valueOf(request.getParameter("id").trim()));
		AgentDocumentExample agentDocumentExample = new AgentDocumentExample();
		AgentDocumentExample.Criteria agentDocument_criteria = agentDocumentExample.createCriteria();
		agentDocument_criteria.andAgentIdEqualTo(request.getParameter("id").trim());
		agentDocumentMapper.deleteByExample(agentDocumentExample);
		model.addAttribute("agentDeletedMessage", "Agent Deleted Successfully!");
		return list(request, response, model);
	}

	/*-------------------------------------------------------End- Delete Agent----------------------------------------------------------*/

	/**
	 * --------------------------------------------------------------------------------------------------------------------------------
	 **/

	// ----------------------------------------------------------- Ajax Validation
	// Function ---------------------------------------------*/

	@RequestMapping(value = { "/validateAgentEmail" }, method = RequestMethod.GET)
	@ResponseBody
	public String validateAgentEmail(@RequestParam String email, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception {
		String validateEmailFlag = "n";
		List<AgentProfile> agentProfileList = new ArrayList<AgentProfile>();
		AgentProfileExample agentProfileExample = new AgentProfileExample();
		AgentProfileExample.Criteria agent_criteria = agentProfileExample.createCriteria();
		agent_criteria.andEmailEqualTo(email);

		agentProfileList = agentProfileMapper.selectByExample(agentProfileExample);

		if (agentProfileList.size() > 0) {
			validateEmailFlag = "y";
			model.addAttribute("validateEmailFlag", validateEmailFlag);
			logger.info("validateEmailFlag" + validateEmailFlag);
		}
		JSONObject json = new JSONObject();
		json.put("validateEmailFlag", validateEmailFlag);
		return json.toString();
	}

	@RequestMapping(value = { "/validateAgentId" }, method = RequestMethod.GET)
	@ResponseBody
	public String validateAgentId(@RequestParam String idNo, HttpServletRequest request, HttpServletResponse response,
			Model model) throws Exception {
		String validateidNoFlag = "n";
		List<AgentProfile> agentProfileList = new ArrayList<AgentProfile>();
		AgentProfileExample agentProfileExample = new AgentProfileExample();
		AgentProfileExample.Criteria agent_criteria = agentProfileExample.createCriteria();
		agent_criteria.andIdNoEqualTo(idNo);

		agentProfileList = agentProfileMapper.selectByExample(agentProfileExample);

		if (agentProfileList.size() > 0) {
			validateidNoFlag = "y";

		}
		JSONObject json = new JSONObject();
		json.put("validateidNoFlag", validateidNoFlag);
		return json.toString();
	}

	@RequestMapping(value = { "/validateAgentIdNo" }, method = RequestMethod.GET)
	@ResponseBody
	public String validateAgentIdNo(@RequestParam String agentID, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception {

		BigDecimal agentID_bd = new BigDecimal(agentID);
		AgentProfile agentProfile = new AgentProfile();
		agentProfile = agentProfileMapper.selectByPrimaryKey(Short.valueOf(agentID));
		JSONObject json = new JSONObject();
		json.put("agentType", agentProfile.getAgentType());
		return json.toString();
	}

	@RequestMapping(value = { "/getAgentType" }, method = RequestMethod.GET)
	@ResponseBody
	public String getAgentType(@RequestParam String agentID, HttpServletRequest request, HttpServletResponse response,
			Model model) throws Exception {

		BigDecimal agentID_bd = new BigDecimal(agentID);
		AgentProfile agentProfile = new AgentProfile();
		agentProfile = agentProfileMapper.selectByPrimaryKey(Short.valueOf(agentID));
		JSONObject json = new JSONObject();
		json.put("agentType", agentProfile.getAgentType());
		return json.toString();
	}

	@RequestMapping(value = { "/validateAgentCode" }, method = RequestMethod.GET)
	@ResponseBody
	public String validateAgentCode(@RequestParam String agentCode, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception {
		String validateAgentCodeFlag = "n";

		List<AgentProdMap> agentProdMapList = new ArrayList<AgentProdMap>();
		AgentProdMapExample agentProdMapExample = new AgentProdMapExample();
		AgentProdMapExample.Criteria agentProdMap_criteria = agentProdMapExample.createCriteria();
		agentProdMap_criteria.andAgentCodeEqualTo(agentCode);
		agentProdMapList = agentProdMapMapper.selectByExample(agentProdMapExample);

		if (agentProdMapList.size() > 0) {
			validateAgentCodeFlag = "y";
			model.addAttribute("validateAgentCodeFlag", validateAgentCodeFlag);
			logger.info("validateAgentCodeFlag" + validateAgentCodeFlag);
		}
		JSONObject json = new JSONObject();
		json.put("validateAgentCodeFlag", validateAgentCodeFlag);
		return json.toString();
	}
	// ----------------------------------------------------------- Ajax Validation
	// Function ------------------------------------------- */

	/**
	 * --------------------------------------------------------------------------------------------------------------------------------
	 **/

	/*
	 * ----------------------------------------------------------Start-Agent Product
	 * Mapping --------------------------------------------
	 */
	@RequestMapping(value = "/showAgentProductMgm", method = RequestMethod.GET)
	public String showAgentProductMgm(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		List<AgentProfile> agentProfileList = new ArrayList<AgentProfile>();
		AgentProfileExample example = new AgentProfileExample();
		agentProfileList = agentProfileMapper.selectByExample(example);
		model.addAttribute("agentProfile", new AgentProfile());
		model.addAttribute("agentList", agentProfileList);
		model.addAttribute("errorMessage", "");
		return "agent-product-management";

	}

	@RequestMapping(value = "/agentProductSubmitDone", method = RequestMethod.POST)
	public String agentProductSubmitDone(@ModelAttribute(value = "agentProfile") AgentProfile agentProfile,
			BindingResult result, Model model) {

		logger.info(agentProfile.getId() + "  Agent Details  1089 ::::          " + agentProfile.getAgentName());

		ProductsExample productsExample = new ProductsExample();
		ProductsExample.Criteria products_criteria = productsExample.createCriteria();
		if ("ETB".equalsIgnoreCase(agentProfile.getInsuranceType())) {
			logger.info("**********************ETB AgentProfile selection");
			products_criteria.andProductEntityEqualTo(agentProfile.getInsuranceType());
		} else if ("EIB".equalsIgnoreCase(agentProfile.getInsuranceType())) {
			logger.info("******************EIB AgentProfile Selection");
			products_criteria.andProductEntityEqualTo(agentProfile.getInsuranceType());
		}
		products_criteria.andProductEntityIsNotNull();
		productsExample.setOrderByClause("prod.PRODUCT_ENTITY asc");
		List<Products> productsList = new ArrayList<Products>();
		productsList = productsMapper.selectByExample(productsExample);

		List<AgentProfile> agentProfileList = new ArrayList<AgentProfile>();
		AgentProfileExample example = new AgentProfileExample();
		agentProfileList = agentProfileMapper.selectByExample(example);

		List<AgentProdMap> agentProdMapList = new ArrayList<AgentProdMap>();
		AgentProdMapExample agentProdMapExample = new AgentProdMapExample();
		AgentProdMapExample.Criteria agentProdMap_criteria = agentProdMapExample.createCriteria();
		agentProdMap_criteria.andAgpIdEqualTo(Short.parseShort(agentProfile.getId().toString()));
		// agentProdMap_criteria.andAgpIdEqualTo(Short.parseShort(agentProfile.getIdNo()));
		agentProdMapList = agentProdMapMapper.selectByExample(agentProdMapExample);

		logger.info("   agenet profile data  1116 ::::::::::::::             " + agentProfile.getAgentName());

		for (AgentProdMap l : agentProdMapList) {
			logger.info("************************ Agent Prod Map List b4 sending to product Code " + l.getProductCode()
					+ "uuid -----> " + l.getAgentUuid() + "Link -----> " + l.getProdRefUrl());
		}

		model.addAttribute("errorMessage", "");
		model.addAttribute("agentList", agentProfileList);
		model.addAttribute("productsList", productsList);
		model.addAttribute("agentProfile", agentProfile);
		model.addAttribute("agentProdMapList", agentProdMapList);
		model.addAttribute("agentProdMapListSize", agentProdMapList.size());

		return "agent-product-management";

	}

	@RequestMapping(value = "/agentProductSaveDone", method = RequestMethod.POST)
	public String agentProductSaveDone(HttpServletRequest request,
			@ModelAttribute(value = "agentProfile") AgentProfile agentProfile, BindingResult result, Model model) {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		String errorMessage = null;
		String productCodeCheckCounter[] = request.getParameterValues("productCodeCheckCounter");
		String productCodeCheckValue[] = request.getParameterValues("productCodeCheckValue");
		String agentBusinessTypeValue[] = request.getParameterValues("agentBusinessType");
		String in_agentCode[] = request.getParameterValues("in_agentCode");
		String in_discount[] = request.getParameterValues("in_discount");
		String in_commission[] = request.getParameterValues("in_commission");

		// need to print
		logger.info("***************** agentProductSaveDone**" + productCodeCheckCounter + productCodeCheckValue
				+ in_agentCode + in_discount + in_commission);
		List<AdminParam> adminParamList = new ArrayList<AdminParam>();
		logger.info("***************** agentProductSaveDone:adminParamList" + adminParamList.get(0).getId());
		logger.info("***************** agentProductSaveDone:adminParamList" + adminParamList.get(0).getParamCode());
		logger.info("***************** agentProductSaveDone:adminParamList" + adminParamList.get(0).getParamName());

		AdminParamExample serverExample = new AdminParamExample();
		logger.info("***************** agentProductSaveDone:serverExample" + serverExample);
		logger.info("***************** agentProductSaveDone:serverExample" + serverExample.getOrderByClause());
		logger.info("***************** agentProductSaveDone:serverExample" + serverExample.getOredCriteria());

		AdminParamExample.Criteria server_criteria = serverExample.createCriteria();
		server_criteria.andParamTypeEqualTo("SERVER_TYPE");
		adminParamList = adminParamMapper.selectByExample(serverExample);
		logger.info("**************** agentProductSaveDone:adminParamList" + adminParamList);
		logger.info("**************** agentProductSaveDone:adminParamList" + adminParamList.get(0).getId());
		logger.info("**************** agentProductSaveDone:adminParamList" + adminParamList.get(0).getParamCode());
		logger.info("**************** agentProductSaveDone:adminParamList" + adminParamList.get(0).getParamName());
		logger.info("**************** agentProductSaveDone:adminParamList" + adminParamList.get(0).getParamType());

		String environment = null;
		if (adminParamList.size() == 1) {
			environment = adminParamList.get(0).getParamCode();
			logger.info("******************agentProductSaveDone::adminParamList:environment" + environment);
		} else {
			logger.info("******************agentProductSaveDone::adminParamList:environment ---> PROD");
			environment = "PROD";
		}

		AgentProfile agent = new AgentProfile();
		agent = agentProfileMapper.selectByPrimaryKey(Short.parseShort(agentProfile.getId().toString()));

		logger.info("********************* agent.getIsMayaAgent()-----> " + agent.getIsMayaAgent()
				+ " Environment ---> " + environment);

		logger.info("***agentProductSaveDone:agent" + agent);
		List<AgentLink> agentLinkList = new ArrayList<AgentLink>();
		logger.info("agentProductSaveDone:agentLinkList" + agentLinkList);
		logger.info("agentProductSaveDone:agentLinkList" + agentLinkList.get(0).getAgentLink());
		logger.info("agentProductSaveDone:agentLinkList" + agentLinkList.get(0).getDspProductCode());
		logger.info("agentProductSaveDone:agentLinkList" + agentLinkList.get(0).getServer());

		AgentLinkExample agentLinkExample = new AgentLinkExample();
		AgentLinkExample.Criteria agentLink_criteria = agentLinkExample.createCriteria();
		if ("y".equalsIgnoreCase(agent.getIsMayaAgent()) && "PROD".equalsIgnoreCase(environment)) {
			logger.info("***********AgentLinkExample ***yes**,getmayaagent--env--PROD");
			agentLink_criteria.andIsMayaAgentEqualTo("y");
			agentLink_criteria.andServerEqualTo(environment);
		}
		if ("n".equalsIgnoreCase(agent.getIsMayaAgent()) && "PROD".equalsIgnoreCase(environment)) {
			logger.info("***********AgentLinkExample ***NO**,getmayaagent--env--PROD");
			agentLink_criteria.andIsMayaAgentEqualTo("n");
			agentLink_criteria.andServerEqualTo(environment);
		}
		if ("y".equalsIgnoreCase(agent.getIsMayaAgent()) && "UAT".equalsIgnoreCase(environment)) {
			logger.info("***********AgentLinkExample ***yes**,getmayaagent--env--UAT");
			agentLink_criteria.andIsMayaAgentEqualTo("y");
			agentLink_criteria.andServerEqualTo(environment);
		}
		if ("n".equalsIgnoreCase(agent.getIsMayaAgent()) && "UAT".equalsIgnoreCase(environment)) {
			logger.info("***********AgentLinkExample ***yes**,getmayaagent--env--UAT");
			agentLink_criteria.andIsMayaAgentEqualTo("n");
			agentLink_criteria.andServerEqualTo(environment);
		}
		agentLinkList = agentLinkMapper.selectByExample(agentLinkExample);

		String ezylife_link = null;
		String motor_insurance_link = null;
		String motor_takaful_link = null;
		String hohh_insurance_link = null;
		String hohh_takaful_link = null;
		String wtc_link = null;
		String tpt_link = null;
		String ids_link = null;
		String ezySecure_link = null;
		String iSecure_link = null;
		String cppl_link = null;
		String tppl_link = null;
		String cppw_link = null;
		String tppw_link = null;
		String cppc_link = null;
		String tppc_link = null;
		String cppe_link = null;
		String tppe_link = null;
		String bpt_link = null;
		String tzt_link = null;
		String tci_link = null;
		String tct_link = null;
		String ecancercare_insurance_link = null;
		String ecancercare_takaful_link = null;
		String medical_insurance_link = null;
		String medical_takaful_link = null;

		for (AgentLink al : agentLinkList) {
			logger.info("**************** processing Product ----> " + al.getDspProductCode()
					+ "Agent Link fromServer ----> " + al.getAgentLink());
			switch (al.getDspProductCode()) {
			case "TL":
				ezylife_link = al.getAgentLink();
				logger.info("1218:ezylife_link" + ezylife_link);
				break;
			case "MI":
				motor_insurance_link = al.getAgentLink();
				break;
			case "MT":
				motor_takaful_link = al.getAgentLink();
				break;
			case "HOHH":
				hohh_insurance_link = al.getAgentLink();
			case "HOHH-ETB":
				hohh_takaful_link = al.getAgentLink();
				break;
			case "WTC":
				wtc_link = al.getAgentLink();
				break;
			case "TPT":
				tpt_link = al.getAgentLink();
				break;
			case "IDS":
				ids_link = al.getAgentLink();
				break;
			case "EZYTL":
				ezySecure_link = al.getAgentLink();
				break;
			case "ISCTL":
				iSecure_link = al.getAgentLink();
				break;
			case "CPP-L":
				cppl_link = al.getAgentLink();
				break;
			case "CPP-W":
				cppw_link = al.getAgentLink();
				break;
			case "CPP-C":
				cppc_link = al.getAgentLink();
				break;
			case "CPP-E":
				cppe_link = al.getAgentLink();
				break;
			case "TPP-L":
				tppl_link = al.getAgentLink();
				break;
			case "TPP-W":
				tppw_link = al.getAgentLink();
				break;
			case "TPP-C":
				tppc_link = al.getAgentLink();
				break;
			case "TPP-E":
				tppe_link = al.getAgentLink();
				break;
			case "BPT":
				bpt_link = al.getAgentLink();
				break;
			case "TZT":
				tzt_link = al.getAgentLink();
				break;
			case "TCI":
				tci_link = al.getAgentLink();
				break;
			case "TCT":
				tct_link = al.getAgentLink();
				break;
			case QQConstant.CANCERCARE_INSURANCE:
				ecancercare_insurance_link = al.getAgentLink();
				logger.info("****************** 1284:ecancercare_insurance_link" + ecancercare_insurance_link);
				break;

			case QQConstant.CANCERCARE_TAKAFUL:
				ecancercare_takaful_link = al.getAgentLink();
				logger.info("****************** 1284:ecancercare_takaful_link" + ecancercare_takaful_link);
				break;

			/* Dhana added for medical */
			case QQConstant.MEDICALPASS_INSURANCE:
				ecancercare_insurance_link = al.getAgentLink();
				logger.info("****************** 1284:medical_insurance_link" + medical_insurance_link);
				break;

			case QQConstant.MEDICALPASS_TAKAFUL:
				ecancercare_takaful_link = al.getAgentLink();
				logger.info("****************** 1284:medical_takaful_link" + medical_takaful_link);
				break;
			}

			/*
			 * if("TL".equalsIgnoreCase(al.getDspProductCode())) {
			 * ezylife_link=al.getAgentLink(); break; }
			 * if("MI".equalsIgnoreCase(al.getDspProductCode())) {
			 * motor_insurance_link=al.getAgentLink(); break; }
			 * if("MT".equalsIgnoreCase(al.getDspProductCode())) {
			 * motor_takaful_link=al.getAgentLink(); break; }
			 * if("HOHH".equalsIgnoreCase(al.getDspProductCode())) {
			 * hohh_insurance_link=al.getAgentLink(); break; }
			 * if("HOHH-ETB".equalsIgnoreCase(al.getDspProductCode())) {
			 * hohh_takaful_link=al.getAgentLink(); break; }
			 * if("WTC".equalsIgnoreCase(al.getDspProductCode())) {
			 * wtc_link=al.getAgentLink(); break; }
			 * if("TPT".equalsIgnoreCase(al.getDspProductCode())) {
			 * tpt_link=al.getAgentLink(); break; }
			 */

		}

		List<AgentProdMap> agentProdMapListExist = new ArrayList<AgentProdMap>();
		AgentProdMapExample agentProdMapExampleExist = new AgentProdMapExample();
		AgentProdMapExample.Criteria agentProdMap_criteriaExist = agentProdMapExampleExist.createCriteria();
		agentProdMap_criteriaExist.andAgpIdEqualTo(Short.parseShort(agentProfile.getId().toString()));
		// agentProdMap_criteria.andAgpIdEqualTo(Short.parseShort(agentProfile.getIdNo()));
		agentProdMapListExist = agentProdMapMapper.selectByExample(agentProdMapExampleExist);
		logger.info("+++++agentProdMapListExist Agentcode++++" + agentProdMapListExist.get(0).getAgentCode());
		logger.info("++++++++++agentProdMapListExist agentName" + agentProdMapListExist.get(0).getAgentName());
		logger.info("++++++++++++agentProdMapListExist productcode" + agentProdMapListExist.get(0).getProductCode());

		List<AgentProdMap> agentProdMapList = new ArrayList<AgentProdMap>();
		List<String> selectedProducts = new ArrayList<String>();
		if (productCodeCheckCounter != null) {
			selectedProducts = Arrays.asList(productCodeCheckCounter);
			logger.info("***** SelectedProductCount-----> " + selectedProducts.size());

			for (String counter : selectedProducts) { // no need blob pramaiyan

				logger.info("***** Loop For Product -----> " + counter);
				AgentProdMap agentProdMap = new AgentProdMap();
				agentProdMap.setProductCode(productCodeCheckValue[Integer.parseInt(counter) - 1]);
				agentProdMap.setAgentCode(in_agentCode[Integer.parseInt(counter) - 1]);
				agentProdMap.setAgentType(agentBusinessTypeValue[Integer.parseInt(counter) - 1]);

				if ("D".equalsIgnoreCase(agentProdMap.getAgentType())) {
					agentProdMap.setDiscount(in_discount[Integer.parseInt(counter) - 1]);
				}

				if ("ND".equalsIgnoreCase(agentProdMap.getAgentType())) {
					agentProdMap.setCommission(in_commission[Integer.parseInt(counter) - 1]);
				}

				agentProdMap.setAgpId(Short.parseShort(agentProfile.getId().toString()));
				agentProdMap.setAgentEntity(agentProfile.getInsuranceType());
				logger.info(
						"***** selectedProductsagent ProdMap.getProductCode() -----> " + agentProdMap.getProductCode());
				logger.info("******** selectedProducts agentProdMap.getAgentName()" + agentProdMap.getAgentName());
				logger.info("********* selectedProducts agentProdMap.getAgentCode() " + agentProdMap.getAgentCode());

				if ("TL".equalsIgnoreCase(agentProdMap.getProductCode())) {
					String productAlreayAdded = "n";
					for (AgentProdMap apmE : agentProdMapListExist) {
						if ("TL".equalsIgnoreCase(apmE.getProductCode())) {
							productAlreayAdded = "y";
							break;
						}
					}
					if ("n".equalsIgnoreCase(productAlreayAdded)) {
						agentProdMap.setAgentUuid(AbsecSecurity.GetGUID());
						agentProdMap.setProdRefUrl(ezylife_link + agentProdMap.getAgentUuid());
						// agentProdMap.setProdRefUrl("https://www.dspuat.site/getonline/ezylife-cyberagent?code="+agentProdMap.getAgentUuid());
					}
				}

				if (QQConstant.CANCERCARE_INSURANCE.equalsIgnoreCase(agentProdMap.getProductCode())) {
					String productAlreayAdded = "n";
					for (AgentProdMap apmE : agentProdMapListExist) {
						if (QQConstant.CANCERCARE_INSURANCE.equalsIgnoreCase(apmE.getProductCode())) {
							productAlreayAdded = "y";
							logger.info("************product already added yes");
							break;
						}
					}
					if ("n".equalsIgnoreCase(productAlreayAdded)) {
						agentProdMap.setAgentUuid(AbsecSecurity.GetGUID());
						agentProdMap.setProdRefUrl(ecancercare_insurance_link + agentProdMap.getAgentUuid());
						logger.info("**********set ecancercare_insurance_link:" + ecancercare_insurance_link);
						logger.info("**********set agentProdMap.getAgentUuid()" + agentProdMap.getAgentUuid());

					}
				}

				if (QQConstant.CANCERCARE_TAKAFUL.equalsIgnoreCase(agentProdMap.getProductCode())) {
					String productAlreayAdded = "n";
					for (AgentProdMap apmE : agentProdMapListExist) {
						if (QQConstant.CANCERCARE_TAKAFUL.equalsIgnoreCase(apmE.getProductCode())) {
							productAlreayAdded = "y";
							logger.info("************product already added yes PTCA01");
							break;
						}
					}
					if ("n".equalsIgnoreCase(productAlreayAdded)) {
						agentProdMap.setAgentUuid(AbsecSecurity.GetGUID());
						agentProdMap.setProdRefUrl(ecancercare_takaful_link + agentProdMap.getAgentUuid());
						logger.info("**********set ecancercare_takaful_link PTCA01:" + ecancercare_takaful_link);
						logger.info("**********set agentProdMap.getAgentUuid() PTCA01" + agentProdMap.getAgentUuid());

					}
				}

				if (QQConstant.MEDICALPASS_INSURANCE.equalsIgnoreCase(agentProdMap.getProductCode())) {
					String productAlreayAdded = "n";
					for (AgentProdMap apmE : agentProdMapListExist) {
						if (QQConstant.MEDICALPASS_INSURANCE.equalsIgnoreCase(apmE.getProductCode())) {
							productAlreayAdded = "y";
							logger.info("************product already added yes");
							break;
						}
					}
					if ("n".equalsIgnoreCase(productAlreayAdded)) {
						agentProdMap.setAgentUuid(AbsecSecurity.GetGUID());
						agentProdMap.setProdRefUrl(ecancercare_insurance_link + agentProdMap.getAgentUuid());
						logger.info("**********set medical_insurance_link:" + medical_insurance_link);
						logger.info("**********set agentProdMap.getAgentUuid()" + agentProdMap.getAgentUuid());

					}
				}

				if (QQConstant.MEDICALPASS_TAKAFUL.equalsIgnoreCase(agentProdMap.getProductCode())) {
					String productAlreayAdded = "n";
					for (AgentProdMap apmE : agentProdMapListExist) {
						if (QQConstant.MEDICALPASS_TAKAFUL.equalsIgnoreCase(apmE.getProductCode())) {
							productAlreayAdded = "y";
							logger.info("************product already added yes PTCA01");
							break;
						}
					}
					if ("n".equalsIgnoreCase(productAlreayAdded)) {
						agentProdMap.setAgentUuid(AbsecSecurity.GetGUID());
						agentProdMap.setProdRefUrl(medical_takaful_link + agentProdMap.getAgentUuid());
						logger.info("**********set medical_takaful_link MPT:" + ecancercare_takaful_link);
						logger.info("**********set agentProdMap.getAgentUuid() MPT" + agentProdMap.getAgentUuid());

					}
				}

				if ("MI".equalsIgnoreCase(agentProdMap.getProductCode())) {
					String productAlreayAdded = "n";
					for (AgentProdMap apmE : agentProdMapListExist) {
						if ("MI".equalsIgnoreCase(apmE.getProductCode())) {
							productAlreayAdded = "y";
							break;
						}
					}
					if ("n".equalsIgnoreCase(productAlreayAdded)) {
						agentProdMap.setAgentUuid(AbsecSecurity.GetGUID());
						agentProdMap.setProdRefUrl(motor_insurance_link + agentProdMap.getAgentUuid());
					}
				}
				if ("MT".equalsIgnoreCase(agentProdMap.getProductCode())) {
					String productAlreayAdded = "n";
					for (AgentProdMap apmE : agentProdMapListExist) {
						if ("MT".equalsIgnoreCase(apmE.getProductCode())) {
							productAlreayAdded = "y";
							break;
						}
					}
					if ("n".equalsIgnoreCase(productAlreayAdded)) {
						agentProdMap.setAgentUuid(AbsecSecurity.GetGUID());
						agentProdMap.setProdRefUrl(motor_takaful_link + agentProdMap.getAgentUuid());
					}
				}
				/*
				 * if ("MI_D".equalsIgnoreCase(agentProdMap.getProductCode())) { String
				 * productAlreayAdded="n"; for(AgentProdMap apmE: agentProdMapListExist) { if
				 * ("MT".equalsIgnoreCase(apmE.getProductCode())) { productAlreayAdded="y";
				 * break; } } if ("n".equalsIgnoreCase(productAlreayAdded)) {
				 * agentProdMap.setAgentUuid(AbsecSecurity.GetGUID());
				 * agentProdMap.setProdRefUrl(
				 * "https://www.dspuat.site/getonline/motor-detariff-cyberagent?code="+
				 * agentProdMap.getAgentUuid()); } }
				 */

				if ("HOHH".equalsIgnoreCase(agentProdMap.getProductCode())) {
					String productAlreayAdded = "n";
					for (AgentProdMap apmE : agentProdMapListExist) {
						if ("HOHH".equalsIgnoreCase(apmE.getProductCode())) {
							productAlreayAdded = "y";
							break;
						}
					}
					if ("n".equalsIgnoreCase(productAlreayAdded)) {
						agentProdMap.setAgentUuid(AbsecSecurity.GetGUID());
						agentProdMap.setProdRefUrl(hohh_insurance_link + agentProdMap.getAgentUuid());
					}
				}
				if ("HOHH-ETB".equalsIgnoreCase(agentProdMap.getProductCode())) {
					String productAlreayAdded = "n";
					for (AgentProdMap apmE : agentProdMapListExist) {
						if ("HOHH-ETB".equalsIgnoreCase(apmE.getProductCode())) {
							productAlreayAdded = "y";
							break;
						}
					}
					if ("n".equalsIgnoreCase(productAlreayAdded)) {
						agentProdMap.setAgentUuid(AbsecSecurity.GetGUID());
						agentProdMap.setProdRefUrl(hohh_takaful_link + agentProdMap.getAgentUuid());
					}
				}
				if ("WTC".equalsIgnoreCase(agentProdMap.getProductCode())) {
					String productAlreayAdded = "n";
					for (AgentProdMap apmE : agentProdMapListExist) {
						if ("WTC".equalsIgnoreCase(apmE.getProductCode())) {
							productAlreayAdded = "y";
							break;
						}
					}
					if ("n".equalsIgnoreCase(productAlreayAdded)) {
						agentProdMap.setAgentUuid(AbsecSecurity.GetGUID());
						agentProdMap.setProdRefUrl(wtc_link + agentProdMap.getAgentUuid());
					}
				}
				if ("TPT".equalsIgnoreCase(agentProdMap.getProductCode())) {
					String productAlreayAdded = "n";
					for (AgentProdMap apmE : agentProdMapListExist) {
						if ("TPT".equalsIgnoreCase(apmE.getProductCode())) {
							productAlreayAdded = "y";
							break;
						}
					}
					if ("n".equalsIgnoreCase(productAlreayAdded)) {
						agentProdMap.setAgentUuid(AbsecSecurity.GetGUID());
						agentProdMap.setProdRefUrl(tpt_link + agentProdMap.getAgentUuid());
					}
				}
				if ("IDS".equalsIgnoreCase(agentProdMap.getProductCode())) {
					String productAlreayAdded = "n";
					for (AgentProdMap apmE : agentProdMapListExist) {
						if ("IDS".equalsIgnoreCase(apmE.getProductCode())) {
							productAlreayAdded = "y";
							break;
						}
					}
					if ("n".equalsIgnoreCase(productAlreayAdded)) {
						agentProdMap.setAgentUuid(AbsecSecurity.GetGUID());
						agentProdMap.setProdRefUrl(ids_link + agentProdMap.getAgentUuid());
						// agentProdMap.setProdRefUrl("https://www.dspuat.site/getonline/ezylife-cyberagent?code="+agentProdMap.getAgentUuid());
					}
				}

				if ("EZYTL".equalsIgnoreCase(agentProdMap.getProductCode())) {
					String productAlreayAdded = "n";
					for (AgentProdMap apmE : agentProdMapListExist) {
						if ("EZYTL".equalsIgnoreCase(apmE.getProductCode())) {
							productAlreayAdded = "y";
							break;
						}
					}
					if ("n".equalsIgnoreCase(productAlreayAdded)) {
						agentProdMap.setAgentUuid(AbsecSecurity.GetGUID());
						agentProdMap.setProdRefUrl(ezySecure_link + agentProdMap.getAgentUuid());
						// agentProdMap.setProdRefUrl("https://www.dspuat.site/getonline/ezylife-cyberagent?code="+agentProdMap.getAgentUuid());
					}
				}
				if ("ISCTL".equalsIgnoreCase(agentProdMap.getProductCode())) {
					String productAlreayAdded = "n";
					for (AgentProdMap apmE : agentProdMapListExist) {
						if ("ISCTL".equalsIgnoreCase(apmE.getProductCode())) {
							productAlreayAdded = "y";
							break;
						}
					}
					if ("n".equalsIgnoreCase(productAlreayAdded)) {
						agentProdMap.setAgentUuid(AbsecSecurity.GetGUID());
						agentProdMap.setProdRefUrl(iSecure_link + agentProdMap.getAgentUuid());
						// agentProdMap.setProdRefUrl("https://www.dspuat.site/getonline/ezylife-cyberagent?code="+agentProdMap.getAgentUuid());
					}
				}
				// Buddy PA
				if (agentProdMap.getProductCode().toUpperCase().contains("CPP")) {
					String productAlreayAdded = "n";
					for (AgentProdMap apmE : agentProdMapListExist) {
						if (apmE.getProductCode().toUpperCase().contains("CPP")) {
							productAlreayAdded = "y";
							break;
						}
					}
					if ("n".equalsIgnoreCase(productAlreayAdded)) {
						agentProdMap.setAgentUuid(AbsecSecurity.GetGUID());
						switch (agentProdMap.getProductCode().toUpperCase()) {
						case "CPP-L":
							agentProdMap.setProdRefUrl(cppl_link + agentProdMap.getAgentUuid());
							break;
						case "CPP-W":
							agentProdMap.setProdRefUrl(cppw_link + agentProdMap.getAgentUuid());
							break;
						case "CPP-C":
							agentProdMap.setProdRefUrl(cppc_link + agentProdMap.getAgentUuid());
							break;
						case "CPP-E":
							agentProdMap.setProdRefUrl(cppe_link + agentProdMap.getAgentUuid());
							break;
						}
					}
				}
				if (agentProdMap.getProductCode().toUpperCase().contains("TPP")) {
					String productAlreayAdded = "n";
					for (AgentProdMap apmE : agentProdMapListExist) {
						if (apmE.getProductCode().toUpperCase().contains("TPP")) {
							productAlreayAdded = "y";
							break;
						}
					}
					if ("n".equalsIgnoreCase(productAlreayAdded)) {
						agentProdMap.setAgentUuid(AbsecSecurity.GetGUID());
						switch (agentProdMap.getProductCode().toUpperCase()) {
						case "TPP-L":
							agentProdMap.setProdRefUrl(tppl_link + agentProdMap.getAgentUuid());
							break;
						case "TPP-W":
							agentProdMap.setProdRefUrl(tppw_link + agentProdMap.getAgentUuid());
							break;
						case "TPP-C":
							agentProdMap.setProdRefUrl(tppc_link + agentProdMap.getAgentUuid());
							break;
						case "TPP-E":
							agentProdMap.setProdRefUrl(tppe_link + agentProdMap.getAgentUuid());
							break;
						}
					}
				}
				// Travel Ezy
				if ("BPT".equalsIgnoreCase(agentProdMap.getProductCode())) {
					String productAlreayAdded = "n";
					for (AgentProdMap apmE : agentProdMapListExist) {
						if ("BPT".equalsIgnoreCase(apmE.getProductCode())) {
							productAlreayAdded = "y";
							break;
						}
					}
					if ("n".equalsIgnoreCase(productAlreayAdded)) {
						agentProdMap.setAgentUuid(AbsecSecurity.GetGUID());
						agentProdMap.setProdRefUrl(bpt_link + agentProdMap.getAgentUuid());
					}
				}
				if ("TZT".equalsIgnoreCase(agentProdMap.getProductCode())) {
					String productAlreayAdded = "n";
					for (AgentProdMap apmE : agentProdMapListExist) {
						if ("TZT".equalsIgnoreCase(apmE.getProductCode())) {
							productAlreayAdded = "y";
							break;
						}
					}
					if ("n".equalsIgnoreCase(productAlreayAdded)) {
						agentProdMap.setAgentUuid(AbsecSecurity.GetGUID());
						agentProdMap.setProdRefUrl(tzt_link + agentProdMap.getAgentUuid());
					}
				}

				// Trip Care
				if ("TCI".equalsIgnoreCase(agentProdMap.getProductCode())) {
					String productAlreayAdded = "n";
					for (AgentProdMap apmE : agentProdMapListExist) {
						if ("TCI".equalsIgnoreCase(apmE.getProductCode())) {
							productAlreayAdded = "y";
							break;
						}
					}
					if ("n".equalsIgnoreCase(productAlreayAdded)) {
						agentProdMap.setAgentUuid(AbsecSecurity.GetGUID());
						agentProdMap.setProdRefUrl(tci_link + agentProdMap.getAgentUuid());
					}
				}
				if ("TCT".equalsIgnoreCase(agentProdMap.getProductCode())) {
					String productAlreayAdded = "n";
					for (AgentProdMap apmE : agentProdMapListExist) {
						if ("TCT".equalsIgnoreCase(apmE.getProductCode())) {
							productAlreayAdded = "y";
							break;
						}
					}
					if ("n".equalsIgnoreCase(productAlreayAdded)) {
						agentProdMap.setAgentUuid(AbsecSecurity.GetGUID());
						agentProdMap.setProdRefUrl(tct_link + agentProdMap.getAgentUuid());
					}
				}
				logger.info("****************** Adding agentProdMap -----> " + agentProdMap.getAgentUuid()
						+ "agentProdMap.getProdRefUrl() ------> " + agentProdMap.getProdRefUrl());
				agentProdMapList.add(agentProdMap);

			}

			for (AgentProdMap apm : agentProdMapList) {
				AgentProdMapExample agentProdMapExample = new AgentProdMapExample();
				AgentProdMapExample.Criteria agentProdMap_criteria = agentProdMapExample.createCriteria();
				agentProdMap_criteria.andAgpIdEqualTo(Short.parseShort(agentProfile.getId().toString()));
				agentProdMap_criteria.andProductCodeEqualTo(apm.getProductCode());
				int found = agentProdMapMapper.countByExample(agentProdMapExample);

				if (found <= 0) {
					agentProdMapMapper.insert(apm);
					logger.info("agentProdMapMapper.insert(apm);" + agentProdMapMapper.insert(apm));
				} else {
					agentProdMapMapper.updateByExampleSelective(apm, agentProdMapExample);
					logger.info("+++++++++++++agentProdMapMapper"
							+ agentProdMapMapper.updateByExampleSelective(apm, agentProdMapExample));
				}

			}

		} else { // If No Checkbox Selected
			errorMessage = "Please Select At Least One Product";
		}

		// Info Required In Page
		ProductsExample productsExample = new ProductsExample();
		ProductsExample.Criteria products_criteria = productsExample.createCriteria();
		if ("ETB".equalsIgnoreCase(agentProfile.getInsuranceType())) {
			products_criteria.andProductEntityEqualTo(agentProfile.getInsuranceType());
		}
		products_criteria.andProductEntityIsNotNull();
		productsExample.setOrderByClause("prod.PRODUCT_ENTITY asc");
		List<Products> productsList = new ArrayList<Products>();
		productsList = productsMapper.selectByExample(productsExample);

		List<AgentProfile> agentProfileList = new ArrayList<AgentProfile>();
		AgentProfileExample example = new AgentProfileExample();
		agentProfileList = agentProfileMapper.selectByExample(example);
		model.addAttribute("agentList", agentProfileList);

		model.addAttribute("productsList", productsList);
		model.addAttribute("agentProfile", agentProfile);

		model.addAttribute("agentProdMapList", agentProdMapList);
		model.addAttribute("errorMessage", errorMessage);

		logger.info("agentProfileList agentname" + agentProfileList.get(0).getAgentCode());
		logger.info("agentProfileList agent code" + agentProfileList.get(0).getAgentName());
		logger.info("agentProfileList" + agentProfileList.get(0).getAgentType());

		/*
		 * 
		 * logger.info()
		 * agentProdMap.setProductCodeList(Arrays.asList(productCodeCheckValue));
		 * agentProdMap.setAgentCodeList(Arrays.asList(in_agentCode));
		 * agentProdMap.setDiscountList(Arrays.asList(in_discount));
		 * agentProdMap.setCommissionList(Arrays.asList(in_commission));
		 *
		 *
		 *
		 * int endloop = agentProdMap.getProductCodeList().size(); for(int j = 0; j <
		 * endloop; j++) { AgentProdMap agentProdMapInsert=new AgentProdMap(); if
		 * (agentProdMap.getProductCodeList().get(j) != null) {
		 * agentProdMapInsert.setProductCode((String)agentProdMap.getProductCodeList().
		 * get(j));
		 * agentProdMapInsert.setAgentCode((String)agentProdMap.getAgentCodeList().get(j
		 * )); if(agentProdMap.getDiscountList().get(j) != null ||
		 * !"".equals(agentProdMap.getDiscountList().get(j))) {
		 * agentProdMapInsert.setDiscount(Short.parseShort(agentProdMap.getDiscountList(
		 * ).get(j))); } if(agentProdMap.getCommissionList().get(j) != null ||
		 * !"".equals(agentProdMap.getCommissionList().get(j))) {
		 * agentProdMapInsert.setCommission(Short.parseShort(agentProdMap.
		 * getCommissionList().get(j))); }
		 * agentProdMapInsert.setAgpId(Short.parseShort(agentProfileID));
		 * agentProdMapMapper.insert(agentProdMapInsert); logger.info("Comission" +
		 * agentProdMap.getCommissionList().get(j));
		 *
		 * }
		 *
		 * }
		 */

		return agentProductSubmitDone(agentProfile, null, model);
		// return "agent-product-management";

	}
	//

	@RequestMapping(value = "/deleteAgentProductMap", method = RequestMethod.POST)
	public String deleteAgentProductMap(HttpServletRequest request, Model model) throws ParseException {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		String id = request.getParameter("id");
		String agentType = request.getParameter("agentType");
		String insuranceType = request.getParameter("insuranceType");
		String agentcode = request.getParameter("agentCode_delete");
		String productCode = request.getParameter("productCode_delete");

		AgentProdMapExample agentProdMapExample = new AgentProdMapExample();
		AgentProdMapExample.Criteria agentProdMap_criteria = agentProdMapExample.createCriteria();
		agentProdMap_criteria.andAgpIdEqualTo(Short.valueOf(id));
		agentProdMap_criteria.andAgentCodeEqualTo(agentcode);
		if ("MI".equalsIgnoreCase(productCode)) {
			List<String> productsToBeDeleted = new ArrayList<String>();
			productsToBeDeleted.add(productCode);
			productsToBeDeleted.add("CAPS");
			agentProdMap_criteria.andProductCodeIn(productsToBeDeleted);
		} else {
			agentProdMap_criteria.andProductCodeEqualTo(productCode);
		}

		agentProdMapMapper.deleteByExample(agentProdMapExample);

		AgentProfile agentProfile = new AgentProfile();
		agentProfile.setId(new BigDecimal(id));
		agentProfile.setAgentType(agentType);
		agentProfile.setInsuranceType(insuranceType);

		return agentProductSubmitDone(agentProfile, null, model);

	}

	/*
	 * ----------------------------------------------------------End-Agent Product
	 * Mapping --------------------------------------------------------------
	 */

	/*
	 * public Boolean VerifyCaptcha(String captchResponse) {
	 * 
	 * // if (1==1) return true; String respStr = "";
	 * 
	 * if (captchResponse == null || captchResponse.isEmpty()) { return false; } if
	 * (1 == 1) { return true; } try {
	 * 
	 * String secret = "6Lf6uQwUAAAAAHgBl6eRWcgLWyemwzqC5sgy56lL";
	 * 
	 * String remoteip = "";
	 * 
	 * String urlParameters = "?secret=" + secret + "&response=" + captchResponse;
	 * 
	 * String url = "https://www.google.com/recaptcha/api/siteverify" +
	 * urlParameters; URL obj = new URL(url);
	 * 
	 * URLConnection con = obj.openConnection();
	 * 
	 * con.addRequestProperty("authorization", "Basic ZXRpcWE6QWJjZDEyMzQ=");
	 * con.addRequestProperty("content-type", "application/json");
	 * con.addRequestProperty("accept", "application/json");
	 * 
	 * // Send post request con.setDoOutput(true); DataOutputStream wr = new
	 * DataOutputStream(con.getOutputStream()); wr.writeBytes(urlParameters);
	 * wr.flush(); wr.close();
	 * 
	 * BufferedReader inp = new BufferedReader(new
	 * InputStreamReader(con.getInputStream())); String inputLine; StringBuffer
	 * response = new StringBuffer();
	 * 
	 * while ((inputLine = inp.readLine()) != null) { response.append(inputLine); }
	 * inp.close();
	 * 
	 * respStr = response.toString(); } catch (Exception ex) { respStr =
	 * ex.getMessage(); }
	 * 
	 * int p = respStr.indexOf("\"success\": true");
	 * 
	 * return p > 0;
	 * 
	 * }
	 */

	@RequestMapping("/checkDate")
	public String checkDate(Model model) throws ParseException {

		SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
		Date date1 = fmt.parse("25-01-2017");
		Date date2 = fmt.parse("26-01-2017");
		List<CommonQQ> agentProfileList = new ArrayList<CommonQQ>();
		CommonQQExample example = new CommonQQExample();
		CommonQQExample.Criteria qq_criteria = example.createCriteria();
		qq_criteria.andCreatedDateBetween(date1, date2);
		agentProfileList = commonQQMapper.selectByExample(example);

		for (CommonQQ item : agentProfileList) {
			logger.info(item.getCreatedDate());
		}

		return "healthQuestions";

	}

	private boolean isCaptchaValid(HttpServletRequest request, String captchaCode) {
		HttpSession session = request.getSession();
		/*
		 * if ((session != null)) { return true; }
		 */
		// validate the Captcha to check we're not dealing with a bot
		// Captcha captcha = Captcha.load(request, "basicExample");
		// boolean isHuman = captcha.validate(captchaCode);
		/*
		 * if (isHuman) { if (session == null) { session = request.getSession(true); }
		 * session.setAttribute("captchaVerified", true); return true; } else { return
		 * false; }
		 */
		return true;
	}

	/**********************************************************
	 * Start by pramaiyan on 12032018
	 *********************************************************************/

	private byte[] convertToBytes(Object object) throws IOException {
		try (ByteArrayOutputStream bos = new ByteArrayOutputStream(); ObjectOutput out = new ObjectOutputStream(bos)) {
			out.writeObject(object);
			return bos.toByteArray();
		}
	}

	private Object convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
		try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes); ObjectInput in = new ObjectInputStream(bis)) {
			return in.readObject();
		}
	}

	/**********************************************************
	 * End by pramaiyan on 12032018
	 *********************************************************************/

	/*----------------------------------------------------------Start   by pramaiyan on 12032018-------------------------------------------------------------------*/

	@RequestMapping(value = "/agentProductSave", method = RequestMethod.POST)
	public String agentProductSave(HttpServletRequest request,
			@ModelAttribute(value = "agentProfile") AgentProfile agentProfile, BindingResult result, Model model) {

		HttpSession session = request.getSession();
		String logedUser = (String) session.getAttribute("logedUser");

		String loginUser = (String) session.getAttribute("user");
		List<AgentProdMap> listagentprodatachange = new ArrayList<AgentProdMap>();
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		BigDecimal agentid = agentProfile.getId();
		logger.info("**********1872agent id   :::::::::::::::           " + agentid);
		String errorMessage = null;
		String productCodeCheckCounter[] = request.getParameterValues("productCodeCheckCounter");
		String productCodeCheckValue[] = request.getParameterValues("productCodeCheckValue");
		String agentBusinessTypeValue[] = request.getParameterValues("agentBusinessType");
		String in_agentCode[] = request.getParameterValues("in_agentCode");
		String in_discount[] = request.getParameterValues("in_discount");
		String in_commission[] = request.getParameterValues("in_commission");

		ApprovalExample approvalExample = new ApprovalExample();
		com.spring.VO.ApprovalExample.Criteria createCriteria_approvalExample = approvalExample.createCriteria();
		createCriteria_approvalExample.andDescriptionEqualTo("AGENT PRODUCT MAPPING");
		List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

		Integer intSize = Integer.parseInt(productCodeCheckCounter[productCodeCheckCounter.length - 1]);

		for (int i = 0; i < intSize; i++) {
			for (int j = 0; j < productCodeCheckCounter.length; j++) {
				if (productCodeCheckCounter[j].equalsIgnoreCase(String.valueOf(i + 1))) {
					AgentProdMap agentProdMap_Change = new AgentProdMap();
					agentProdMap_Change.setAgentCode(in_agentCode[i]);
					agentProdMap_Change.setAgentEntity(agentProfile.getInsuranceType());
					agentProdMap_Change.setProductCode(productCodeCheckValue[i]);
					agentProdMap_Change.setAgentType(agentBusinessTypeValue[i]);
					agentProdMap_Change.setAgpId(agentid.shortValue());
					agentProdMap_Change.setCommission(in_commission[i]);
					agentProdMap_Change.setDiscount(in_discount[i]);

					listagentprodatachange.add(agentProdMap_Change);
					logger.info("++++++++++++AgentProdMap agentcode" + listagentprodatachange.get(0).getAgentCode());
					logger.info("++++++++++++AgentProdMap agentname" + listagentprodatachange.get(0).getAgentName());
					logger.info(
							"++++++++++++AgentProdMap productcode" + listagentprodatachange.get(0).getProductCode());
					logger.info("++++++++++++AgentProdMap productreffrealurl"
							+ listagentprodatachange.get(0).getProdRefUrl());
				}
			}
		}

		try {

			byte[] agentbytedata_changeData = convertToBytes(listagentprodatachange);
			logger.info("  byte data :::::           " + agentbytedata_changeData.length);

			AgentProdMapExample agentProdMapExample_Orig = new AgentProdMapExample();
			Criteria agentProdcreateCriteria_Orig = agentProdMapExample_Orig.createCriteria();
			agentProdcreateCriteria_Orig.andAgpIdEqualTo(agentid.shortValue());
			List<AgentProdMap> listagentprod_Orig = agentProdMapMapper.selectByExample(agentProdMapExample_Orig);

			byte[] agentbytedata_OrigData = convertToBytes(listagentprod_Orig);
			logger.info("  byte data :::::           " + agentbytedata_OrigData);

			BigDecimal approvalProdId = new BigDecimal(0);
			if (listapproval.size() > 0) {
				approvalProdId = listapproval.get(0).getId();
			}

			ApprovalLog approvalLog = new ApprovalLog();
			approvalLog.setNewContent(agentbytedata_changeData);
			approvalLog.setOriginalContent(agentbytedata_OrigData);
			approvalLog.setApprovalId((short) 7); // from Approval table this value[7] based on menu in feature
			approvalLog.setMaker(Short.parseShort(logedUser)); // session login value
			// approvalLog.setChecker((short)1); //while insert record no need checker value
			approvalLog.setStatus("1");
			approvalLog.setCreateDate(new Date());
			// approvalLog.setUpdateDate(new Date());
			approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
			approvalLogMapper.insert(approvalLog);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ApprovalLogExample approvalexample = new ApprovalLogExample();
		com.spring.VO.ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();
		Integer testint = new Integer(23);
		createCriteria.andIdEqualTo(testint.shortValue());
		List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);

		logger.info("   list size in approval list  :::   " + listapprovallog.size());

		return agentProductSubmitDone(agentProfile, null, model);
	}

	@RequestMapping(value = "/getApprovalList")
	public String getAgentProductMapApproval(HttpServletRequest request, HttpServletResponse response, Model model) {
		
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String logedUser = (String) session.getAttribute("logedUser");
		logger.info("1936:getApprovalList:logedUser" + logedUser);

		ApprovalLogExample approvalLogExample = new ApprovalLogExample();
		ApprovalLogExample.Criteria roleModuleExampleCriteria = approvalLogExample.createCriteria();

		if (null != request.getParameter("appType") && !request.getParameter("appType").isEmpty()) {
			roleModuleExampleCriteria.andAppTypeEqualTo(request.getParameter("appType"));
		}

		if (null != request.getParameter("status") && !request.getParameter("status").isEmpty()) {
			roleModuleExampleCriteria.andStatusEqualTo(request.getParameter("status"));
		}

		if (null != logedUser && !logedUser.isEmpty()) {
			roleModuleExampleCriteria.andMakerNotEqualTo(Short.parseShort(logedUser));
		}
		// get AllList from ApprovalLog table
		List<ApprovalLog> approvalLogList = new ArrayList<ApprovalLog>();
		approvalLogList = approvalLogMapper.selectByExample(approvalLogExample);
		
		logger.info("++++++:getApprovalList" + approvalLogList.get(0).getAgentProdMapList());

		ApprovalExample approvalExample = new ApprovalExample();
		// ApprovalExample.Criteria approvalExample_criteria=
		// approvalExample.createCriteria();
		List<Approval> applicationTypeList = new ArrayList<Approval>();
		applicationTypeList = approvalMapper.selectByExample(approvalExample);
		logger.info("+++++:getApprovalList::applicationTypeList" + applicationTypeList);
		logger.info("+++++:getApprovalList::application  getId  TypeList" + applicationTypeList.get(0).getId());

		model.addAttribute("approvalLogList", approvalLogList);
		logger.info("::getApprovalList:::" + approvalLogList);
		model.addAttribute("approvalLogList   getAgentProdMapList", approvalLogList.get(0).getAgentProdMapList());

		model.addAttribute("applicationTypeList", applicationTypeList);
		logger.info("::getApprovalList:applicationTypeList" + applicationTypeList);
		logger.info("::getApprovalList: applicationTypeList" + applicationTypeList.get(0).getDescription());

		return "AgentProductMapApproval";
	}

	// Functionaluity for Approve and Reject
	@RequestMapping(value = "/approvalAgentProductMap", method = RequestMethod.GET)
	public String approvalStatus(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		logger.info("Reequest " + request.getParameter("id"));

		ApprovalLogExample approvalexample = new ApprovalLogExample();
		com.spring.VO.ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();
		Integer testint = new Integer(request.getParameter("id"));
		createCriteria.andIdEqualTo(testint.shortValue());
		List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
		logger.info("   list size in approval list  :::   " + listapprovallog.size());
		// logger.info("Testing Original Content
		// "+listapprovallog.get(0).getOriginalContent());

		try {
			// original data
			if (listapprovallog.size() > 0) {
				Object originalData = convertFromBytes(listapprovallog.get(0).getOriginalContent());
				List<Object> listOriginalData = new ArrayList<Object>();
				// objecct convert into AgentProdMap
				if (originalData != null) {
					listOriginalData = (List<Object>) originalData;
					logger.info("  list agent prod map size ::::::         " + listOriginalData.size());
					for (int t = 0; t < listOriginalData.size(); t++) {

						
					}
					model.addAttribute("listOriginalData", listOriginalData);
				
					logger.info("-----------------------------");
				} // inner if
			} // outer if

			// Change Data
			if (listapprovallog.size() > 0) {
				Object changeData = convertFromBytes(listapprovallog.get(0).getNewContent());
				List<Object> listChangeData = new ArrayList<Object>();
				// objecct convert into AgentProdMap
				if (changeData != null) {
					listChangeData = (List<Object>) changeData;
					logger.info(" list agent prod map size :::::: " + listChangeData.size());
					for (int t = 0; t < listChangeData.size(); t++) {

						logger.info("New Agent Product: " + ((AgentProdMap) listChangeData.get(t)).getProductCode());
						logger.info("New Agent Code: " + ((AgentProdMap) listChangeData.get(t)).getAgentCode());
						logger.info("New Agent Code: " + ((AgentProdMap) listChangeData.get(t)).getAgentEntity());
						logger.info("New  Agent Name :" + ((AgentProdMap) listChangeData.get(t)).getAgentName());
						logger.info("New Agent Type : " + ((AgentProdMap) listChangeData.get(t)).getAgentType());
						logger.info("New Agent Comm : " + ((AgentProdMap) listChangeData.get(t)).getCommission());
						logger.info("New Agent Dis: " + ((AgentProdMap) listChangeData.get(t)).getDiscount());

					}
					// Collections.sort(arraylist, Collections.reverseOrder());

					model.addAttribute("listChangeData", listChangeData);
					logger.info("-----------------------------");
				}
			}
		} // try end
		catch (Exception e) {
			logger.info("Exception in Convert Object to Byte" + e);
		}

		return "agent-product-approval";
	}

	@RequestMapping(value = "/approveAgentProductMapChangeData", method = RequestMethod.GET)
	public String approveChangeDataAction(HttpServletRequest request, HttpServletResponse response, Model model) {

		AgentProfile agentProfile = new AgentProfile();
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		// log needed
		logger.info("approveAgentProductMapChangeData:user" + loginUser);
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		String aid = request.getParameter("approvalLogId");

		logger.info("aid >>>>>" + aid);

		ApprovalLogExample approvalLogexample = new ApprovalLogExample();
		ApprovalLog alog = new ApprovalLog();
		ApprovalLogExample.Criteria createCriteria = approvalLogexample.createCriteria();

		if (null != aid && !aid.isEmpty()) {
			Integer testint = new Integer(aid);
			createCriteria.andIdEqualTo(testint.shortValue());
			List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalLogexample);
			logger.info("   list size in approval list  :::   " + listapprovallog.size());
			/*
			 * for(int t=0; t < listChangeData.size();t++){
			 * logger.info("New Agent ID: "+((AgentProdMap)listChangeData.get(t)).
			 * getAgpId());
			 * logger.info("New Agent Product: "+((AgentProdMap)listChangeData.get(t)
			 * ).getProductCode());
			 * logger.info("New Agent Code: "+((AgentProdMap)listChangeData.get(t)).
			 * getAgentCode());
			 * logger.info("New Agent Code: "+((AgentProdMap)listChangeData.get(t)).
			 * getAgentEntity());
			 * logger.info("New  Agent Name :"+((AgentProdMap)listChangeData.get(t)).
			 * getAgentName());
			 * logger.info("New Agent Type : "+((AgentProdMap)listChangeData.get(t)).
			 * getAgentType());
			 * logger.info("New Agent Comm : "+((AgentProdMap)listChangeData.get(t)).
			 * getCommission());
			 * logger.info("New Agent Dis: "+((AgentProdMap)listChangeData.get(t)).
			 * getDiscount()); } //for end
			 */

			if (listapprovallog.size() > 0) {
				Object changeData;
				try {
					String status = listapprovallog.get(0).getStatus();
					short alogId = listapprovallog.get(0).getId();
					logger.info("Status  :  " + status);

					changeData = convertFromBytes(listapprovallog.get(0).getNewContent());
					List<AgentProdMap> listChangeData = new ArrayList<AgentProdMap>();
					// objecct convert into AgentProdMap
					if (changeData != null) {
						listChangeData = (List<AgentProdMap>) changeData;
						// model.addAttribute("listChangeData", listChangeData);
					} // if end

					// get Agent profile bsed on getAgpId
					short agentId = listChangeData.get(0).getAgpId();
					agentProfile.setId(new BigDecimal(agentId));

					AgentProfile agent = new AgentProfile();
					agent = agentProfileMapper.selectByPrimaryKey(Short.parseShort(agentProfile.getId().toString()));
					// logger.info("agent"+agent);

					// Existing logic
					List<AdminParam> adminParamList = new ArrayList<AdminParam>();
					AdminParamExample serverExample = new AdminParamExample();
					AdminParamExample.Criteria server_criteria = serverExample.createCriteria();
					server_criteria.andParamTypeEqualTo("SERVER_TYPE");
					adminParamList = adminParamMapper.selectByExample(serverExample);
					String environment = null;
					if (adminParamList.size() == 1) {
						environment = adminParamList.get(0).getParamCode();
					} else {
						environment = "PROD";
					}

					logger.info("*******environment:" + environment);
					List<AgentLink> agentLinkList = new ArrayList<AgentLink>();
					AgentLinkExample agentLinkExample = new AgentLinkExample();
					AgentLinkExample.Criteria agentLink_criteria = agentLinkExample.createCriteria();
					if ("y".equalsIgnoreCase(agent.getIsMayaAgent()) && "PROD".equalsIgnoreCase(environment)) {
						logger.info("*****entering in Y---get maya agent env PROD");
						agentLink_criteria.andIsMayaAgentEqualTo("y");
						agentLink_criteria.andServerEqualTo(environment);
					}
					if ("n".equalsIgnoreCase(agent.getIsMayaAgent()) && "PROD".equalsIgnoreCase(environment)) {
						logger.info("************Entering in N ---mayaagent env PROD");
						agentLink_criteria.andIsMayaAgentEqualTo("n");
						agentLink_criteria.andServerEqualTo(environment);
					}
					if ("y".equalsIgnoreCase(agent.getIsMayaAgent()) && "UAT".equalsIgnoreCase(environment)) {
						logger.info("******************Entering in y----mayaagent env UAT");
						agentLink_criteria.andIsMayaAgentEqualTo("y");
						agentLink_criteria.andServerEqualTo(environment);
					}
					if ("n".equalsIgnoreCase(agent.getIsMayaAgent()) && "UAT".equalsIgnoreCase(environment)) {
						logger.info("************Entering in N -----mayaagent env UAT");
						agentLink_criteria.andIsMayaAgentEqualTo("n");
						agentLink_criteria.andServerEqualTo(environment);
					}
					agentLinkList = agentLinkMapper.selectByExample(agentLinkExample);

					String ezylife_link = null;
					String motor_insurance_link = null;
					String motor_takaful_link = null;
					String hohh_insurance_link = null;
					String hohh_takaful_link = null;
					String wtc_link = null;
					String tpt_link = null;
					String ids_link = null;
					String ezySecure_link = null;
					String iSecure_link = null;
					String cppl_link = null;
					String tppl_link = null;
					String cppw_link = null;
					String tppw_link = null;
					String cppc_link = null;
					String tppc_link = null;
					String cppe_link = null;
					String tppe_link = null;
					String bpt_link = null;
					String tzt_link = null;
					String tci_link = null;
					String tct_link = null;
					String ecancercare_insurance_link = null;
					String ecancercare_takaful_link = null;
					String medical_insurance_link = null;
					String medical_takaful_link = null;

					// logger.info("************ agentLinkList Size ----> "+agentLinkList.size());

					for (AgentLink al : agentLinkList) {
						// logger.info("************ Processing agentLinkList product Code ---->
						// "+al.getDspProductCode());
						switch (al.getDspProductCode()) {
						case "TL":
							ezylife_link = al.getAgentLink();
							logger.info("swtchcase1:ezylife_link++" + ezylife_link);
							break;
						case "MI":
							motor_insurance_link = al.getAgentLink();
							break;
						case "MT":
							motor_takaful_link = al.getAgentLink();
							break;
						case "HOHH":
							hohh_insurance_link = al.getAgentLink();
						case "HOHH-ETB":
							hohh_takaful_link = al.getAgentLink();
							break;
						case "WTC":
							wtc_link = al.getAgentLink();
							break;
						case "TPT":
							tpt_link = al.getAgentLink();
							break;
						case "IDS":
							ids_link = al.getAgentLink();
							break;
						case "EZYTL":
							ezySecure_link = al.getAgentLink();
							break;
						case "ISCTL":
							iSecure_link = al.getAgentLink();
							break;
						case "CPP-L":
							cppl_link = al.getAgentLink();
							break;
						case "CPP-W":
							cppw_link = al.getAgentLink();
							break;
						case "CPP-C":
							cppc_link = al.getAgentLink();
							break;
						case "CPP-E":
							cppe_link = al.getAgentLink();
							break;
						case "TPP-L":
							tppl_link = al.getAgentLink();
							break;
						case "TPP-W":
							tppw_link = al.getAgentLink();
							break;
						case "TPP-C":
							tppc_link = al.getAgentLink();
							break;
						case "TPP-E":
							tppe_link = al.getAgentLink();
							break;
						case "BPT":
							bpt_link = al.getAgentLink();
							break;
						case "TZT":
							tzt_link = al.getAgentLink();
							break;
						case "TCI":
							tci_link = al.getAgentLink();
							break;
						case "TCT":
							tct_link = al.getAgentLink();
							break;

						case QQConstant.CANCERCARE_INSURANCE:
							ecancercare_insurance_link = al.getAgentLink();
							logger.info("switchcase1::ecancercare_insurance_link PTCA01" + ecancercare_insurance_link);// ezylife_link
							break;

						case QQConstant.CANCERCARE_TAKAFUL:
							ecancercare_takaful_link = al.getAgentLink();
							logger.info("switchcase1::ecancercare_takaful_link PTCA01" + ecancercare_takaful_link);// ezylife_link
							break;

						case QQConstant.MEDICALPASS_INSURANCE:
							medical_insurance_link = al.getAgentLink();
							logger.info("switchcase1:%%%%%%%%%%%%%%%%%%%5:medical_insurance_link MP"
									+ medical_insurance_link);// ezylife_link
							break;

						case QQConstant.MEDICALPASS_TAKAFUL:
							medical_takaful_link = al.getAgentLink();
							logger.info(
									"switchcase1:%%%%%%%%%%%%%%%%%%%5:medical_takaful_link MPT" + medical_takaful_link);// ezylife_link
							break;

						}
					}
					// iterator listChangeData list

					for (AgentProdMap apm : listChangeData) {
						AgentProdMapExample agentProdMapExample = new AgentProdMapExample();
						AgentProdMapExample.Criteria agentProdMap_criteria = agentProdMapExample.createCriteria();
						agentProdMap_criteria.andAgpIdEqualTo(Short.parseShort(agentProfile.getId().toString()));
						agentProdMap_criteria.andProductCodeEqualTo(apm.getProductCode());
						int found = agentProdMapMapper.countByExample(agentProdMapExample);
						List<AgentProdMap> agentProdMapListExist = agentProdMapMapper
								.selectByExample(agentProdMapExample);

						logger.info("apm getProductCode: " + apm.getProductCode());
						logger.info("apm getAgentCode: " + apm.getAgentCode());
						logger.info("apm getAgentName :" + apm.getAgentName());
						logger.info("apm getId: " + agentProfile.getId().toString());

						if ("TL".equalsIgnoreCase(apm.getProductCode())) {
							String productAlreayAdded = "n";
							for (AgentProdMap apmE : agentProdMapListExist) {
								if ("TL".equalsIgnoreCase(apmE.getProductCode())) {
									productAlreayAdded = "y";
									break;
								}
							}
							if ("n".equalsIgnoreCase(productAlreayAdded)) {
								apm.setAgentUuid(AbsecSecurity.GetGUID());
								apm.setProdRefUrl(ezylife_link + apm.getAgentUuid());
							}
						}

						else if ("MI".equalsIgnoreCase(apm.getProductCode())) {
							String productAlreayAdded = "n";
							for (AgentProdMap apmE : agentProdMapListExist) {
								if ("MI".equalsIgnoreCase(apmE.getProductCode())) {
									productAlreayAdded = "y";
									break;
								}
							}
							if ("n".equalsIgnoreCase(productAlreayAdded)) {
								apm.setAgentUuid(AbsecSecurity.GetGUID());
								apm.setProdRefUrl(motor_insurance_link + apm.getAgentUuid());
							}
						} else if (QQConstant.CANCERCARE_INSURANCE.equalsIgnoreCase(apm.getProductCode())) {
							String productAlreayAdded = "n";
							for (AgentProdMap apmE : agentProdMapListExist) {
								if (QQConstant.CANCERCARE_INSURANCE.equalsIgnoreCase(apmE.getProductCode())) {
									logger.info("**************** yes apmE.getProductCode():::::::"
											+ apmE.getProductCode());
									logger.info("**********************yes  ecancercare_insurance_link::"
											+ ecancercare_insurance_link);
									productAlreayAdded = "y";
									break;
								}
							}
							if ("n".equalsIgnoreCase(productAlreayAdded)) {
								apm.setAgentUuid(AbsecSecurity.GetGUID());
								apm.setProdRefUrl(ecancercare_insurance_link + apm.getAgentUuid());
								logger.info(
										"********************** No 2394::: apm.getAgentUuid()::" + apm.getAgentUuid());
								logger.info("********************** No 2395ecancercare_insurance_link::"
										+ ecancercare_insurance_link);
							}
						}

						// PTCA01
						else if (QQConstant.CANCERCARE_TAKAFUL.equalsIgnoreCase(apm.getProductCode())) {
							String productAlreayAdded = "n";
							for (AgentProdMap apmE : agentProdMapListExist) {
								if (QQConstant.CANCERCARE_TAKAFUL.equalsIgnoreCase(apmE.getProductCode())) {
									logger.info("**************** yes apmE.getProductCode():::::::"
											+ apmE.getProductCode());
									logger.info("**********************yes  ecancercare_takaful_link::"
											+ ecancercare_takaful_link);
									productAlreayAdded = "y";
									break;
								}
							}
							if ("n".equalsIgnoreCase(productAlreayAdded)) {
								apm.setAgentUuid(AbsecSecurity.GetGUID());
								apm.setProdRefUrl(ecancercare_takaful_link + apm.getAgentUuid());
								logger.info("********************** No 2316::: apm.getAgentUuid():PTCA01:"
										+ apm.getAgentUuid());
								logger.info("********************** No 2317ecancercare_insurance_link:PTCA01:"
										+ ecancercare_takaful_link);
							}
						}

						// medical MP
						else if (QQConstant.MEDICALPASS_INSURANCE.equalsIgnoreCase(apm.getProductCode())) {
							String productAlreayAdded = "n";
							for (AgentProdMap apmE : agentProdMapListExist) {
								if (QQConstant.MEDICALPASS_INSURANCE.equalsIgnoreCase(apmE.getProductCode())) {
									logger.info("**************** yes apmE.getProductCode() MP:::::::"
											+ apmE.getProductCode());
									logger.info("**********************yes  ecancercare_insurance_link MP::"
											+ medical_insurance_link);
									productAlreayAdded = "y";
									break;
								}
							}
							if ("n".equalsIgnoreCase(productAlreayAdded)) {
								apm.setAgentUuid(AbsecSecurity.GetGUID());
								apm.setProdRefUrl(medical_insurance_link + apm.getAgentUuid());
								logger.info("********************** No 2394::: apm.getAgentUuid():MP:"
										+ apm.getAgentUuid());
								logger.info("********************** No 2395ecancercare_insurance_link:MP:"
										+ medical_insurance_link);
							}
						}

						//
						// medical takaful
						else if (QQConstant.MEDICALPASS_TAKAFUL.equalsIgnoreCase(apm.getProductCode())) {
							String productAlreayAdded = "n";
							for (AgentProdMap apmE : agentProdMapListExist) {
								if (QQConstant.MEDICALPASS_TAKAFUL.equalsIgnoreCase(apmE.getProductCode())) {
									logger.info("**************** yes apmE.getProductCode()::::MPT:::"
											+ apmE.getProductCode());
									logger.info("**********************yes  medical_insurance_link:MPT:"
											+ medical_takaful_link);
									productAlreayAdded = "y";
									break;
								}
							}
							if ("n".equalsIgnoreCase(productAlreayAdded)) {
								apm.setAgentUuid(AbsecSecurity.GetGUID());
								apm.setProdRefUrl(medical_takaful_link + apm.getAgentUuid());
								logger.info("********************** No 2394::: apm.getAgentUuid():MPT:"
										+ apm.getAgentUuid());
								logger.info("********************** No 2395medical_insurance_link:MPT:"
										+ medical_takaful_link);
							}
						}

						//

						//
						else if ("MT".equalsIgnoreCase(apm.getProductCode())) {
							String productAlreayAdded = "n";
							for (AgentProdMap apmE : agentProdMapListExist) {
								if ("MT".equalsIgnoreCase(apmE.getProductCode())) {
									productAlreayAdded = "y";
									break;
								}
							}
							if ("n".equalsIgnoreCase(productAlreayAdded)) {
								apm.setAgentUuid(AbsecSecurity.GetGUID());
								apm.setProdRefUrl(motor_takaful_link + apm.getAgentUuid());
							}
						} else if ("HOHH".equalsIgnoreCase(apm.getProductCode())) {
							String productAlreayAdded = "n";
							for (AgentProdMap apmE : agentProdMapListExist) {
								if ("HOHH".equalsIgnoreCase(apmE.getProductCode())) {
									productAlreayAdded = "y";
									break;
								}
							}
							if ("n".equalsIgnoreCase(productAlreayAdded)) {
								apm.setAgentUuid(AbsecSecurity.GetGUID());
								apm.setProdRefUrl(hohh_insurance_link + apm.getAgentUuid());
							}
						} else if ("HOHH-ETB".equalsIgnoreCase(apm.getProductCode())) {
							String productAlreayAdded = "n";
							for (AgentProdMap apmE : agentProdMapListExist) {
								if ("HOHH-ETB".equalsIgnoreCase(apmE.getProductCode())) {
									productAlreayAdded = "y";
									break;
								}
							}
							if ("n".equalsIgnoreCase(productAlreayAdded)) {
								apm.setAgentUuid(AbsecSecurity.GetGUID());
								apm.setProdRefUrl(hohh_takaful_link + apm.getAgentUuid());
							}
						} else if ("WTC".equalsIgnoreCase(apm.getProductCode())) {
							String productAlreayAdded = "n";
							for (AgentProdMap apmE : agentProdMapListExist) {
								if ("WTC".equalsIgnoreCase(apmE.getProductCode())) {
									productAlreayAdded = "y";
									break;
								}
							}
							if ("n".equalsIgnoreCase(productAlreayAdded)) {
								apm.setAgentUuid(AbsecSecurity.GetGUID());
								apm.setProdRefUrl(wtc_link + apm.getAgentUuid());
							}
						} else if ("TPT".equalsIgnoreCase(apm.getProductCode())) {
							String productAlreayAdded = "n";
							for (AgentProdMap apmE : agentProdMapListExist) {
								if ("TPT".equalsIgnoreCase(apmE.getProductCode())) {
									productAlreayAdded = "y";
									break;
								}
							}
							if ("n".equalsIgnoreCase(productAlreayAdded)) {
								apm.setAgentUuid(AbsecSecurity.GetGUID());
								apm.setProdRefUrl(tpt_link + apm.getAgentUuid());
							}
						} else if ("IDS".equalsIgnoreCase(apm.getProductCode())) {
							String productAlreayAdded = "n";
							for (AgentProdMap apmE : agentProdMapListExist) {
								if ("IDS".equalsIgnoreCase(apmE.getProductCode())) {
									productAlreayAdded = "y";
									break;
								}
							}
							if ("n".equalsIgnoreCase(productAlreayAdded)) {
								apm.setAgentUuid(AbsecSecurity.GetGUID());
								apm.setProdRefUrl(ids_link + apm.getAgentUuid());
								// agentProdMap.setProdRefUrl("https://www.dspuat.site/getonline/ezylife-cyberagent?code="+agentProdMap.getAgentUuid());
							}
						} else if ("EZYTL".equalsIgnoreCase(apm.getProductCode())) {
							String productAlreayAdded = "n";
							for (AgentProdMap apmE : agentProdMapListExist) {
								if ("EZYTL".equalsIgnoreCase(apmE.getProductCode())) {
									productAlreayAdded = "y";
									break;
								}
							}
							if ("n".equalsIgnoreCase(productAlreayAdded)) {
								apm.setAgentUuid(AbsecSecurity.GetGUID());
								apm.setProdRefUrl(ezySecure_link + apm.getAgentUuid());
								// agentProdMap.setProdRefUrl("https://www.dspuat.site/getonline/ezylife-cyberagent?code="+agentProdMap.getAgentUuid());
							}
						} else if ("ISCTL".equalsIgnoreCase(apm.getProductCode())) {
							String productAlreayAdded = "n";
							for (AgentProdMap apmE : agentProdMapListExist) {
								if ("ISCTL".equalsIgnoreCase(apmE.getProductCode())) {
									productAlreayAdded = "y";
									break;
								}
							}
							if ("n".equalsIgnoreCase(productAlreayAdded)) {
								apm.setAgentUuid(AbsecSecurity.GetGUID());
								apm.setProdRefUrl(iSecure_link + apm.getAgentUuid());
								// agentProdMap.setProdRefUrl("https://www.dspuat.site/getonline/ezylife-cyberagent?code="+agentProdMap.getAgentUuid());
							}
						} else if (apm.getProductCode().toUpperCase().contains("CPP")) {
							// Buddy PA
							String productAlreayAdded = "n";
							for (AgentProdMap apmE : agentProdMapListExist) {
								if (apmE.getProductCode().toUpperCase().contains("CPP")) {
									productAlreayAdded = "y";
									break;
								}
							}
							if ("n".equalsIgnoreCase(productAlreayAdded)) {
								apm.setAgentUuid(AbsecSecurity.GetGUID());
								switch (apm.getProductCode().toUpperCase()) {
								case "CPP-L":
									apm.setProdRefUrl(cppl_link + apm.getAgentUuid());
									break;
								case "CPP-W":
									apm.setProdRefUrl(cppw_link + apm.getAgentUuid());
									break;
								case "CPP-C":
									apm.setProdRefUrl(cppc_link + apm.getAgentUuid());
									break;
								case "CPP-E":
									apm.setProdRefUrl(cppe_link + apm.getAgentUuid());
									break;
								}
							}
						} else if (apm.getProductCode().toUpperCase().contains("TPP")) {
							String productAlreayAdded = "n";
							for (AgentProdMap apmE : agentProdMapListExist) {
								if (apmE.getProductCode().toUpperCase().contains("TPP")) {
									productAlreayAdded = "y";
									break;
								}
							}
							if ("n".equalsIgnoreCase(productAlreayAdded)) {
								apm.setAgentUuid(AbsecSecurity.GetGUID());
								switch (apm.getProductCode().toUpperCase()) {
								case "TPP-L":
									apm.setProdRefUrl(tppl_link + apm.getAgentUuid());
									break;
								case "TPP-W":
									apm.setProdRefUrl(tppw_link + apm.getAgentUuid());
									break;
								case "TPP-C":
									apm.setProdRefUrl(tppc_link + apm.getAgentUuid());
									break;
								case "TPP-E":
									apm.setProdRefUrl(tppe_link + apm.getAgentUuid());
									break;
								}
							}
						} else if ("BPT".equalsIgnoreCase(apm.getProductCode())) {
							// Travel Ezy
							String productAlreayAdded = "n";
							for (AgentProdMap apmE : agentProdMapListExist) {
								if ("BPT".equalsIgnoreCase(apmE.getProductCode())) {
									productAlreayAdded = "y";
									break;
								}
							}
							if ("n".equalsIgnoreCase(productAlreayAdded)) {
								apm.setAgentUuid(AbsecSecurity.GetGUID());
								apm.setProdRefUrl(bpt_link + apm.getAgentUuid());
							}
						} else if ("TZT".equalsIgnoreCase(apm.getProductCode())) {
							String productAlreayAdded = "n";
							for (AgentProdMap apmE : agentProdMapListExist) {
								if ("TZT".equalsIgnoreCase(apmE.getProductCode())) {
									productAlreayAdded = "y";
									break;
								}
							}
							if ("n".equalsIgnoreCase(productAlreayAdded)) {
								apm.setAgentUuid(AbsecSecurity.GetGUID());
								apm.setProdRefUrl(tzt_link + apm.getAgentUuid());
							}
						} else if ("TCI".equalsIgnoreCase(apm.getProductCode())) {
							// Trip Care
							String productAlreayAdded = "n";
							for (AgentProdMap apmE : agentProdMapListExist) {
								if ("TCI".equalsIgnoreCase(apmE.getProductCode())) {
									productAlreayAdded = "y";
									break;
								}
							}
							if ("n".equalsIgnoreCase(productAlreayAdded)) {
								for (AgentProdMap apmE : agentProdMapListExist) {
									if ("WTC".equalsIgnoreCase(apmE.getProductCode())) {
										apm.setAgentUuid(apmE.getAgentUuid());
										break;
									}
								}
								if (apm.getAgentUuid() == null || apm.getAgentUuid().isEmpty()) {
									apm.setAgentUuid(AbsecSecurity.GetGUID());
								}
								apm.setProdRefUrl(tci_link + apm.getAgentUuid());
							}
						} else if ("TCT".equalsIgnoreCase(apm.getProductCode())) {
							String productAlreayAdded = "n";
							for (AgentProdMap apmE : agentProdMapListExist) {
								if ("TCT".equalsIgnoreCase(apmE.getProductCode())) {
									productAlreayAdded = "y";
									break;
								}
							}
							if ("n".equalsIgnoreCase(productAlreayAdded)) {
								for (AgentProdMap apmE : agentProdMapListExist) {
									if ("TPT".equalsIgnoreCase(apmE.getProductCode())) {
										apm.setAgentUuid(apmE.getAgentUuid());
										break;
									}
								}
								if (apm.getAgentUuid() == null || apm.getAgentUuid().isEmpty()) {
									apm.setAgentUuid(AbsecSecurity.GetGUID());
								}
								apm.setProdRefUrl(tct_link + apm.getAgentUuid());
							}
						}

						if (found <= 0) {
							logger.info("********************** Insert agentProdMapMapper.insert(apm) triggered");
							agentProdMapMapper.insert(apm);

						} else {
							logger.info(
									"********************** Update agentProdMapMapper.updateByExampleSelective(apm, agentProdMapExample) triggered");
							agentProdMapMapper.updateByExampleSelective(apm, agentProdMapExample);

						}
					}

					alog.setId(alogId);
					alog.setStatus("3");
					alog.setChecker((short) 1); // get from the current login session
					alog.setUpdateDate(new Date());
					approvalLogMapper.updateByPrimaryKeySelective(alog);

				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} // if for list
		} // if for id

		// return "agent-product-approval";
		// return "AgentProductMapApproval";
		return "redirect:/getApprovalList";
	}
	// Reject Action

	@RequestMapping(value = "/rejectAgentProductMapChangeData", method = RequestMethod.GET)
	public String rejectChangeDataAction(@ModelAttribute AgentProfile agentProfile, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		ApprovalLogExample approvalLogexample = new ApprovalLogExample();
		ApprovalLog alog = new ApprovalLog();
		ApprovalLogExample.Criteria createCriteria = approvalLogexample.createCriteria();
		String aid = request.getParameter("approvalLogId");
		if (null != aid && !aid.isEmpty()) {
			Integer testint = new Integer(aid);
			createCriteria.andIdEqualTo(testint.shortValue());
			List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalLogexample);
			logger.info("   list size in approval list  :::   " + listapprovallog.size());
			logger.info("   list size in getAgentProdMapList approval list  :::   "
					+ listapprovallog.get(0).getAgentProdMapList());

			short alogId = listapprovallog.get(0).getId();
			alog.setId(alogId);
			alog.setStatus("2");
			alog.setChecker((short) 1); // get from the current login session
			alog.setUpdateDate(new Date());
			approvalLogMapper.updateByPrimaryKeySelective(alog);
			logger.info("+++alog" + alog.getAgentProdMapList());
			logger.info("++++++++alog" + alog.getStatus());
			logger.info("++++++++alog" + alog.getMaker());
			logger.info("++++++++alog" + alog.getChecker());
		}

		return "AgentProductMapApproval";
	}

	// 1703208 Product Rate approval by pramaiyan

	/*
	 * @RequestMapping(value="/getProductRateApproval") public String
	 * getProductRateApproval(HttpServletRequest request, HttpServletResponse
	 * response, Model model) {
	 *
	 * HttpSession session = request.getSession(); String loginUser
	 * =(String)session.getAttribute("user"); if(loginUser == null) { String
	 * sessionexpired = "Session Has Been Expired";
	 * model.addAttribute("sessionexpired", sessionexpired); return "admin-login"; }
	 *
	 *
	 * ApprovalLogExample approvalLogExample = new ApprovalLogExample();
	 * ApprovalLogExample.Criteria roleModuleExampleCriteria =
	 * approvalLogExample.createCriteria();
	 *
	 * if (null != request.getParameter("appType") &&
	 * !request.getParameter("appType").isEmpty()) {
	 * roleModuleExampleCriteria.andAppTypeEqualTo(request.getParameter("appType"));
	 * }
	 *
	 * if (null != request.getParameter("status") &&
	 * !request.getParameter("status").isEmpty()) {
	 * roleModuleExampleCriteria.andStatusEqualTo(request.getParameter("status")); }
	 * List<ApprovalLog> approvalLogList= new ArrayList<ApprovalLog>();
	 * ApprovalExample approvalExample =new ApprovalExample();
	 * //ApprovalExample.Criteria approvalExample_criteria=
	 * approvalExample.createCriteria(); List<Approval> applicationTypeList = new
	 * ArrayList<Approval>();
	 * applicationTypeList=approvalMapper.selectByExample(approvalExample);
	 *
	 * //get AllList from ApprovalLog table
	 * approvalLogList=approvalLogMapper.selectByExample(approvalLogExample);
	 *
	 * model.addAttribute("approvalLogList",approvalLogList);
	 * model.addAttribute("applicationTypeList",applicationTypeList); return
	 * "AgentProductMapApproval"; }
	 */

	/*----------------------------------------------------------End   by pramaiyan on 12032018-------------------------------------------------------------------*/
	public void loadADuserProps() {
		InputStream infoad = this.getClass().getClassLoader()
				.getResourceAsStream("com/spring/uam/ldap/adconfig.properties");
		try {
			aduserInfoProp.load(infoad);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
