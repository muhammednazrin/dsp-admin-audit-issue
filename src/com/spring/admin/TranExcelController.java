package com.spring.admin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
import java.util.ArrayList;
//import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
//import java.util.Date;
//import java.util.HashMap;
import java.util.List;
//import java.util.Map;
import java.util.Locale;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.spring.VO.report.ALLReportVO;
import com.spring.VO.report.BuddyPAReportExcelVO;
import com.spring.VO.report.HOHHReportExcelVO;
import com.spring.VO.report.MPReportExcel;
import com.spring.VO.report.MotorReportExcelVO;
import com.spring.VO.report.TCReportExcelVO;
import com.spring.VO.report.TLReportExcelVO;
import com.spring.VO.report.PCCA01ReportExcel;
//import com.spring.utils.ServiceValidationUtils;
import com.spring.VO.report.TransSearchObject;
import com.spring.VO.report.TravelEzyReportExcelVO;
import com.spring.VO.report.WTCReportExcelVO;
import com.spring.mapper.report.ALLReportMapper;
import com.spring.mapper.report.BuddyPAReportExcelMapper;
import com.spring.mapper.report.HOHHReportExcelMapper;
import com.spring.mapper.report.MPReportExcelMapper;
import com.spring.mapper.report.MotorReportExcelMapper;
import com.spring.mapper.report.TCReportExcelMapper;
import com.spring.mapper.report.TLReportExcelMapper;
import com.spring.mapper.report.PCCA01ReportExcelMapper;
import com.spring.mapper.report.TravelEzyReportExcelMapper;
import com.spring.mapper.report.WTCReportExcelMapper;
import com.spring.utils.ServiceValidationUtils;

@Controller
public class TranExcelController {
	final static Logger logger = Logger.getLogger(TranExcelController.class);
	MotorReportExcelMapper motorReportExcelMapper;
	TLReportExcelMapper tlReportExcelMapper;
	WTCReportExcelMapper wtcReportExcelMapper;
	HOHHReportExcelMapper hohhReportExcelMapper;
	ALLReportMapper allReportMapper;
	BuddyPAReportExcelMapper buddyReportExcelMapper;
	TravelEzyReportExcelMapper travelEzyReportExcelMapper;
	TCReportExcelMapper tcReportExcelMapper;
	PCCA01ReportExcelMapper pcca01ReportExcelMapper;
	MPReportExcelMapper mpReportExcelMapper;
	

	@Autowired
	public TranExcelController(MotorReportExcelMapper motorReportExcelMapper, TLReportExcelMapper tlReportExcelMapper,
			WTCReportExcelMapper wtcReportExcelMapper, HOHHReportExcelMapper hohhReportExcelMapper,
			BuddyPAReportExcelMapper buddyReportExcelMapper, TravelEzyReportExcelMapper travelEzyReportExcelMapper,
			TCReportExcelMapper tcReportExcelMapper, PCCA01ReportExcelMapper pcca01ReportExcelMapper,
			MPReportExcelMapper mpReportExcelMapper,ALLReportMapper allReportMapper) {
		this.motorReportExcelMapper = motorReportExcelMapper;
		this.tlReportExcelMapper = tlReportExcelMapper;
		this.wtcReportExcelMapper = wtcReportExcelMapper;
		this.hohhReportExcelMapper = hohhReportExcelMapper;
		this.allReportMapper = allReportMapper;
		this.buddyReportExcelMapper = buddyReportExcelMapper;
		this.travelEzyReportExcelMapper = travelEzyReportExcelMapper;
		this.tcReportExcelMapper = tcReportExcelMapper;
		this.pcca01ReportExcelMapper = pcca01ReportExcelMapper;
		this.mpReportExcelMapper = mpReportExcelMapper;
	}
/*	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String testMethod()
	{
		logger.info("generateExcel<TxnExcel-admin> => ");
		return "agent_login";
	}*/

	@RequestMapping(value = "/generateExcel", method = RequestMethod.GET)
	public String generateExcel(HttpServletRequest request, HttpServletResponse response, Model model)
			throws ServletException, IOException {

		logger.info("generateExcel<TxnExcel-admin> => ");

		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		ServletContext servletContext = request.getSession().getServletContext();

		String productType = (String) session.getAttribute("ProductType");
		String productEntity = (String) session.getAttribute("ProductEntity");
		String status = (String) session.getAttribute("Status");
		String PolicyCertificateNo = (String) session.getAttribute("PolicyCertificateNo");
		String nRIC = (String) session.getAttribute("NRIC");
		String receiptNo = (String) session.getAttribute("ReceiptNo");
		String dateFrom = (String) session.getAttribute("dateFrom");
		String dateTo = (String) session.getAttribute("dateTo");
		String plateNo = (String) session.getAttribute("PlateNo");

		/*
		 * logger.info("productType  -> "+productType);
		 * logger.info("productEntity  -> "+productEntity);
		 * logger.info("status  -> "+status);
		 * logger.info("PolicyCertificateNo  -> "+PolicyCertificateNo);
		 * logger.info("nRIC  -> "+nRIC);
		 * logger.info("receiptNo  -> "+receiptNo);
		 * logger.info("dateFrom  -> "+dateFrom);
		 * logger.info("dateTo  -> "+dateTo);
		 * logger.info("plateNo  -> "+plateNo);
		 */

		// start : motor excel file
		if(productType.indexOf("MI") != -1 || productType.indexOf("MT") != -1 || productType.indexOf("Mo") != -1) {
			TransSearchObject transSearchObject_Motor = new TransSearchObject();
			List<MotorReportExcelVO> motorReportList = new ArrayList<MotorReportExcelVO>();
			List<MotorReportExcelVO> motorReportList_B4Pay = new ArrayList<MotorReportExcelVO>();
			List<String> transactionStatusList = new ArrayList<String>();

			String query1 = "n";
			String query2 = "n";

			if ("EIB".equalsIgnoreCase(productEntity) && "".equalsIgnoreCase(productType)) {

				transSearchObject_Motor.setProductCode("MI");
			}

			if ("ETB".equalsIgnoreCase(productEntity) && "".equalsIgnoreCase(productType)) {

				transSearchObject_Motor.setProductCode("MT");
			}
			if ("".equalsIgnoreCase(productType) || productType == null) {
				transSearchObject_Motor.setProductCode("MI,MT");
			}

			if ("MI".equalsIgnoreCase(productType) || "MT".equalsIgnoreCase(productType)) {
				transSearchObject_Motor.setProductCode(productType);

			}
			if ("".equalsIgnoreCase(productType) || productType == null) {
				if ("".equalsIgnoreCase(status) || status == null) { // productType empty and transaction status empty
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transSearchObject_Motor.setTransactionStatus(transactionStatusList);
					query1 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_Motor.setTransactionStatus(transactionStatusList);
				}
			} else { // if Product is Chosen
				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_Motor.setTransactionStatus(transactionStatusList);
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_Motor.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus(status);
				}
			}
			if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)) {
				query1 = "y";
			}
			if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
				query2 = "y";
			}

			if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
				transSearchObject_Motor.setPolicyNo(PolicyCertificateNo);
			}
			if (!"".equalsIgnoreCase(nRIC)) {
				transSearchObject_Motor.setNricNo(nRIC);
			}
			if (!"".equalsIgnoreCase(receiptNo)) {
				transSearchObject_Motor.setInvoiceNo(receiptNo);
			}

			if (!ServiceValidationUtils.isEmptyStringTrim(plateNo)) {
				transSearchObject_Motor.setVehicleNo(plateNo);
			}

			transSearchObject_Motor.setDateFrom(dateFrom);
			transSearchObject_Motor.setDateTo(dateTo);

			// if query 1
			if ("y".equalsIgnoreCase(query1)) {
				motorReportList = motorReportExcelMapper
						.selectMotorTransactionalReportAfterPayment(transSearchObject_Motor);

			} // End if query1 is Executed

			// if query 2
			if ("y".equalsIgnoreCase(query2)) {
				motorReportList_B4Pay = motorReportExcelMapper
						.selectMotorTransactionalReportBeforePayment(transSearchObject_Motor);
			}

			motorReportList.addAll(motorReportList_B4Pay);

			Collections.sort(motorReportList, new Comparator<MotorReportExcelVO>() {
				@Override
				public int compare(MotorReportExcelVO o1, MotorReportExcelVO o2) {
					return o1.getRecord_date().compareTo(o2.getRecord_date());
				}
			});

			Collections.reverse(motorReportList);

			if (motorReportList.size() <= 0) {
				model.addAttribute("emptySearch", "No Record Found");
			}
			model.addAttribute("motorReportList", motorReportList);

			// start excel

			Random ran = new Random();
			int randomNo = ran.nextInt(6) + 5;

			DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
			Date date = new Date();

			String file = "EtiqaTransactionReport" + dateFormat.format(date) + randomNo + ".xlsx";

			String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
			// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Etiqa Transaction Report");

			// header fields based on product
			String[] fields = null;

			fields = new String[] { "Id", "DSPQQID", "Quotation Creation Date", "Txn Id", "Txn Start Date",
					"Txn End Date", "Entity", "Policy No", "CAPS Policy No", "Vehicle No", "Chassis No", "Engine No",
					"Previous Insurance", "Policy Status", "Txn Status", "Abandonment", "Receipt No", "EIP Txn Id",
					"Agent Code", "Agent Name", "Operator Code", // check
					"Promo Code", "NCD", "Payment Mode", "FPX Ref", "EBPG Ref", "M2U Ref", "No of Seat",
					"Cubic Capacity", "Make", "Model", "NVIC", "Year make", "Period of Insured", "Sum Insured",
					"SI Proposed", "SI Min", "SI Max", "Market Value", "Agree Value", "LLOP", "LL2P", "Flood",
					"NCD Relief", "SRCC", "Windscreen SC", "Windscreen PM", "Vehicle Acc SC", "Vehicle Acc PM",
					"NGV Gas SC", "NGV Gas PM", "CART", "Cart Premium", "LOU", "LOU Premium",
					// Added for Oto360 - Fixed on Apr 2018
					"Spare Part", "Smart Key SC", "Smart Key PM", "Spray Plan", "Spray PM", "Oto Plan", "Oto Discount",
					"Oto PM",
					//uncomment
				
                   //end
					"Payment Payable", "Basic Premium", "Gross Premium", "Discount", "GST", "SST Percentage",
					"SST Amount", "Motor Net", "Motor Premium", "CAPS", "CAPS Net", "Road Tax Amount",
					/*
					 * "Motor Comm", "CAPS Comm",
					 */
					"No of Drivers", "Driver 1", "Driver 2", "Driver 3", "Driver 4", "Driver 5", "Driver 6", "Driver 7",
					"Excess", "Loading Percentage", "Loading", "Insured name", "Id Type", "Insured ID", "Gender", "Age",
					"Email", "Contact No", "Address1", "Address2", "Address3", "Postcode", "State", "RiskAddress1",
					"RiskAddress2", "RiskAddress3", "RiskPostcode", "RiskState", "Tax Invoice No", "JPJ Msg", "CARSECM",
					"ATITFTCD", "GARAGECD", "Bank Name", "Bank Account Number", "Mode", "PF Number",
					 "E-Hailing Premium","Duration of Coverage","Digital License",
						"E-Hailing Driver 1","NRIC of E-Hailing Driver 1","E-Hailing Driver 2","NRIC of E-Hailing Driver 2"};

			Row row1 = sheet.createRow(0);

			for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
				Cell cell = row1.createCell(cellIndex);
				cell.setCellValue(fields[cellIndex]);
			}

			String[] fields2 = null;

			int i = 1;

			for (MotorReportExcelVO motorDetail : motorReportList) {

				String Abandonment = "";
				String EIPTxnId = "";
				String RiskAddress3 = "";
				String txnStatus = "";
				String quotationCreationDate = "";
				String transactionStartDateTime = "";
				String transactionEndDateTime = "";
				String proEntity = "";
				String paymentChannel = "";
				String SIMin = "";
				String SIMax = "";
				String cartPremium = "";
				String cartPremiumDesc = "";
				String firstDriverInfo = "";
				String secondDriverInfo = "";
				String thirdDriverInfo = "";
				String forthDriverInfo = "";
				String fifthDriverInfo = "";
				String sixthDriverInfo = "";
				String seventhDriverInfo = "";
				String age = "";
				String capsNet = "";
				String siProposed = "";
				String motorNetVal = "";
				String louCode= "";
				String louPremiumDesc = "";

				if (null != motorDetail.getPmnt_gateway_code() && "" != motorDetail.getPmnt_gateway_code()) {
					if (motorDetail.getPmnt_gateway_code().indexOf("EBPG") != -1) {

						paymentChannel = "EBPG";
					}

					else if (motorDetail.getPmnt_gateway_code().indexOf("AMEX") != -1) {

						paymentChannel = "AMEX";
					}

					else if (motorDetail.getPmnt_gateway_code().indexOf("FPX") != -1) {

						paymentChannel = "FPX";
					}

					else if (motorDetail.getPmnt_gateway_code().indexOf("Maybank2U") != -1) {

						paymentChannel = "M2U";
					}
					else if (motorDetail.getPmnt_gateway_code().indexOf("API") != -1) {

						paymentChannel = "MOTORAPI";
					}
					else if (motorDetail.getPmnt_gateway_code().indexOf("EZYPAY") != -1) {

						paymentChannel = "EZYPAY";

						if (motorDetail.getRecurringTerm() != null) {

							paymentChannel = paymentChannel + "-" + motorDetail.getRecurringCardType() + "-"
									+ motorDetail.getRecurringTerm();

						}

					}

				}
				if (motorDetail.getProduct_code().equals("MI")) {

					proEntity = "EGIB";
				}

				else if (motorDetail.getProduct_code().equals("MT"))

				{

					proEntity = "EGTB";

				}

				if (motorDetail.getPmnt_status().equals("S")) {

					txnStatus = "Successful";

				} else if (motorDetail.getPmnt_status().equals("F")) {

					txnStatus = "Fail";

				}

				else if (motorDetail.getPmnt_status().equals("AP")) {

					txnStatus = "Attempt Payment";

				}

				else if (motorDetail.getPmnt_status().equals("O")) {

					Abandonment = motorDetail.getLast_page();
					txnStatus = "Incomplete";

				}

				else if (motorDetail.getPmnt_status().equals("C")) {

					txnStatus = "Cancelled";

				}

				else if (motorDetail.getPmnt_status().equals("R")) {

					txnStatus = "Rejection : " + motorDetail.getReason();

				}

				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				Date quotationCreationDateTmp = null;

				try {
					if (motorDetail.getQuotationCreateDateTime() != null) {
						quotationCreationDateTmp = formatter.parse(motorDetail.getQuotationCreateDateTime());
						quotationCreationDate = ft.format(quotationCreationDateTmp);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}

				if (motorDetail.getPmnt_status().equals("S") || motorDetail.getPmnt_status().equals("F")
						|| motorDetail.getPmnt_status().equals("AP")) {

					transactionStartDateTime = motorDetail.getTransaction_datetime();

				}

				/*
				 * if(null!=motorDetail.getMarketValue() && ""!=motorDetail.getMarketValue()&&
				 * null!=motorDetail.getMarkUpPer() && ""!=motorDetail.getMarkUpPer() ){
				 * DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(Locale.US);
				 * format.setParseBigDecimal(true); BigDecimal amt1= new BigDecimal(0.00);
				 * BigDecimal amt2=new BigDecimal(0.00); BigDecimal amt3=new BigDecimal(100.00);
				 * try { amt1 = (BigDecimal) format.parse(motorDetail.getMarketValue()); amt2 =
				 * (BigDecimal) format.parse(motorDetail.getMarkUpPer());
				 *
				 * BigDecimal tmpAmount = (amt2.multiply(amt1)).divide(amt3); BigDecimal sMin =
				 * amt1.subtract(tmpAmount); BigDecimal sMax = amt1.add(tmpAmount);
				 *
				 * SIMin=String.valueOf(sMin); SIMax=String.valueOf(sMax);
				 *
				 * } catch (ParseException e) { // TODO Auto-generated catch block
				 * e.printStackTrace(); }
				 *
				 * }
				 */
				if (null != motorDetail.getMarketValue() && "" != motorDetail.getAgreedValue()
						&& null != motorDetail.getMarkUpPer() && "" != motorDetail.getAgreedValue()) {

					DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(Locale.US);
					format.setParseBigDecimal(true);

					if (null != motorDetail.getAgreedValueSelection() && "" != motorDetail.getAgreedValueSelection()) {
						logger.info(motorDetail.getAgreedValueSelection() + " :Selected Agreed Value");
						siProposed = motorDetail.getAgreedValue();
						// Selected Agreed Value
						// SI Min -5% of Proposed Agreed value
						// SI Max +20% of proosed agreed value
						BigDecimal amt1 = new BigDecimal(0.00);
						BigDecimal amt2 = new BigDecimal(5.00);
						BigDecimal amt3 = new BigDecimal(20.00);
						BigDecimal amt4 = new BigDecimal(100.00);

						try {
							if (motorDetail.getAgreedValue() != null) {
								amt1 = (BigDecimal) format.parse(motorDetail.getAgreedValue());
							}
							BigDecimal fivePerVal = amt2.multiply(amt1).divide(amt4);
							BigDecimal twentyPerVal = amt3.multiply(amt1).divide(amt4);

							BigDecimal sMin = amt1.subtract(fivePerVal); // SI Min -5% of Proposed Agreed value
							BigDecimal sMax = amt1.add(twentyPerVal);// SI Max +20% of proosed agreed value

							SIMin = String.valueOf(sMin);
							SIMax = String.valueOf(sMax);

						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					} else {
						siProposed = motorDetail.getMarketValue();
						// Selected Market Value
						// SI Min -10% from Proposed value
						// SI Max +10% from proposed value
						BigDecimal amt1 = new BigDecimal(0.00);
						BigDecimal amt2 = new BigDecimal(10.00);
						BigDecimal amt3 = new BigDecimal(100.00);
						try {
							if (motorDetail.getMarketValue() != null) {
								amt1 = (BigDecimal) format.parse(motorDetail.getMarketValue());
							}
							BigDecimal tenPerVal = amt2.multiply(amt1).divide(amt3);

							BigDecimal sMin = amt1.subtract(tenPerVal); // SI Min -10% from Proposed value
							BigDecimal sMax = amt1.add(tenPerVal);// SI Max +10% from proposed value

							SIMin = String.valueOf(sMin);
							SIMax = String.valueOf(sMax);

						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				}
				if (motorDetail.getPassengerPaPremium() != null) {
					DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(Locale.US);
					format.setParseBigDecimal(true);
					BigDecimal amt1 = new BigDecimal(0.00);
					BigDecimal amt2 = new BigDecimal(10.00);
					BigDecimal amt3 = new BigDecimal(100.00);
					BigDecimal amt4 = new BigDecimal(4.00);
					try {
						if (motorDetail.getPassengerPaPremium() != null) {
							amt1 = (BigDecimal) format.parse(motorDetail.getPassengerPaPremium());
						}
						if (!(amt1.compareTo(BigDecimal.ZERO) == 0)) { // Checking Not Equal Condition
							BigDecimal tmpAmount = amt2.multiply(amt1).divide(amt3);
							BigDecimal tmpAmount2 = amt1.subtract(tmpAmount); // subtracting 10% amount
							BigDecimal tmpAmount3 = amt4.add(tmpAmount2);// Adding 4 RM value

							capsNet = String.valueOf(tmpAmount3);
						}

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						capsNet = String.valueOf(amt1);
						e.printStackTrace();
					}

				}

				if (motorDetail.getPremiumAfterDiscount() != null) {
					/*
					 * motorDetail.getDiscount(), motorDetail.getGst(),
					 */
					DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(Locale.US);
					format.setParseBigDecimal(true);
					BigDecimal amt1 = new BigDecimal(0.00);
					BigDecimal amt2 = new BigDecimal(0.00);
					BigDecimal amt3 = new BigDecimal(0.00);
					BigDecimal amt4 = new BigDecimal(10.00);
					try {
						if (motorDetail.getPremiumAfterDiscount() != null) {
							amt1 = (BigDecimal) format.parse(motorDetail.getPremiumAfterDiscount());
						}
						if (motorDetail.getDiscount() != null) {
							amt2 = (BigDecimal) format.parse(motorDetail.getDiscount());
						}
						if (motorDetail.getGst() != null) {
							amt3 = (BigDecimal) format.parse(motorDetail.getGst());
						}
						if (!(amt1.compareTo(BigDecimal.ZERO) == 0)) { // Checking Not Equal Condition
							BigDecimal tmpAmount = amt1.subtract(amt2); // subtracting discount
							BigDecimal tmpAmount2 = tmpAmount.subtract(amt3); // subtracting gst
							BigDecimal tmpAmount3 = tmpAmount2.subtract(amt4);// subtracting 10 RM
							motorNetVal = String.valueOf(tmpAmount3);
						}

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				if (motorDetail.getCartPremium() != null) {

					cartPremium = motorDetail.getCartPremium();

				}

				if (motorDetail.getCartPremiumDesc() != null) {

					String tmpCARTDesc = motorDetail.getCartPremiumDesc().replaceAll(",", "|");
					tmpCARTDesc = tmpCARTDesc.replaceAll("Days", "");
					tmpCARTDesc = tmpCARTDesc.replaceAll("RM", "");
					tmpCARTDesc = tmpCARTDesc.replaceAll("/day", "");

					cartPremiumDesc = tmpCARTDesc + "|";

				}
				String cart = "";
				if (motorDetail.getCartPremiumVal() != null && !motorDetail.getCartPremiumVal().isEmpty()) {
					cart = cartPremiumDesc + cartPremium;
				}

	
				
				if (motorDetail.getLOU() != null) {

					louCode = motorDetail.getLOU();

				}
				  logger.info(motorDetail.getLouPremiumDesc()+"lou desc");
				if (motorDetail.getLouPremiumDesc() != null) {

					String tmpLOUDesc = motorDetail.getLouPremiumDesc().replaceAll(",", "|");
					tmpLOUDesc = tmpLOUDesc.replaceAll("Days", "");
					tmpLOUDesc = tmpLOUDesc.replaceAll("RM", "");
					tmpLOUDesc = tmpLOUDesc.replaceAll("/day", "");

					louPremiumDesc = tmpLOUDesc + "|";

				}
				 logger.info(louPremiumDesc+" louPremiumDesc");
				String lou = "";
				if (motorDetail.getLouPremiumVal() != null && !motorDetail.getLouPremiumVal().isEmpty()) {
					lou = louPremiumDesc + louCode;
				}
				
				
				
				
			
				if (!motorDetail.getFirstDriverInfo().equals("||||||")) {

					firstDriverInfo = motorDetail.getFirstDriverInfo();
				}

				if (!motorDetail.getSecondDriverInfo().equals("||||||")) {

					secondDriverInfo = motorDetail.getSecondDriverInfo();
				}
				if (!motorDetail.getThirdDriverInfo().equals("||||||")) {

					thirdDriverInfo = motorDetail.getThirdDriverInfo();
				}
				if (!motorDetail.getForthDriverInfo().equals("||||||")) {

					forthDriverInfo = motorDetail.getForthDriverInfo();
				}
				if (!motorDetail.getFifthDriverInfo().equals("||||||")) {

					fifthDriverInfo = motorDetail.getFifthDriverInfo();
				}
				if (!motorDetail.getSixthDriverInfo().equals("||||||")) {

					sixthDriverInfo = motorDetail.getSixthDriverInfo();
				}

				if (!motorDetail.getSixthDriverInfo().equals("||||||")) {

					seventhDriverInfo = motorDetail.getSeventhDriverInfo();
				}

				if (null != motorDetail.getCustomerDOB() && "" != motorDetail.getCustomerDOB()
						&& "//" != motorDetail.getCustomerDOB()) {

					try {

						String[] dob_split = motorDetail.getCustomerDOB().split("/");

						LocalDate start = LocalDate.of(Integer.parseInt(dob_split[2]), Integer.parseInt(dob_split[1]),
								Integer.parseInt(dob_split[0]));
						LocalDate end = LocalDate.now(); // use for age-calculation: LocalDate.now()
						long tmpAge = ChronoUnit.YEARS.between(start, end);

						age = new Long(tmpAge).toString();

					}

					catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
				//uncommeted
				String EhailDuration ="";
				if(motorDetail.getEhailStartDate()==null){EhailDuration="";}
				else{EhailDuration=(motorDetail.getEhailStartDate()==null?"":motorDetail.getEhailStartDate())+"-"+(motorDetail.getEhailEndDate()==null?"":motorDetail.getEhailEndDate());}
				//end
				fields2 = new String[] { Integer.toString(i), motorDetail.getDSPQQID(), quotationCreationDate,
						motorDetail.getPaymentTrxID(), transactionStartDateTime, transactionEndDateTime, proEntity,
						motorDetail.getPolicy_number(), motorDetail.getCaps_dppa(),
						motorDetail.getRegistration_number(), motorDetail.getChassisNo(), motorDetail.getEngineNo(),
						motorDetail.getPreviousInsurance(), motorDetail.getPolicyStatus(), txnStatus, Abandonment,
						motorDetail.getInvoice_no(), EIPTxnId, motorDetail.getAgent_code(), motorDetail.getAgent_name(),
						motorDetail.getOperatorCode(), // check
						motorDetail.getDiscountCode(), motorDetail.getNCDAmount(), paymentChannel,
						motorDetail.getFpx_fpxtxnid(), motorDetail.getTransaction_id(), motorDetail.getBankrefno(),
						motorDetail.getVehicleSeat(), motorDetail.getVehicleCC(), motorDetail.getVehicleMake(),
						motorDetail.getVehicleModel(), motorDetail.getNVIC(), motorDetail.getYearMake(),
						motorDetail.getPeriodCoverage(), motorDetail.getSumInsured(), siProposed, SIMin, SIMax,
						motorDetail.getMarketValue(), motorDetail.getAgreedValue(), motorDetail.getLLOP(),
						motorDetail.getLL2P(), motorDetail.getFloodCover(), motorDetail.getNCDRelief(),
						motorDetail.getSRCC(), motorDetail.getWindscreenSC(), motorDetail.getWindscreenP(),
						motorDetail.getVehicleAccSC(), motorDetail.getVehicleAccP(), motorDetail.getNGVGasSC(),
						motorDetail.getNGVGasP(), cart, motorDetail.getCartPremiumVal(), // Cart Premium
						//motorDetail.getLOU(), motorDetail.getLouPremiumVal(), motorDetail.getSparePartPremiumPaid(),
						lou, motorDetail.getLouPremiumVal(), motorDetail.getSparePartPremiumPaid(),
					
						motorDetail.getSmartKeySumCovered(), motorDetail.getSmartKeyPremiumAmt(),
						motorDetail.getSprayPlanCode(), motorDetail.getSprayPlanPremiumAmt(), motorDetail.getOtoPlan(),
						motorDetail.getOtoDiscount(), motorDetail.getOtoPremiumAmt(),
						//uncomment
						
						//end
						motorDetail.getAmount(), motorDetail.getBasicPremium(), // Annual Contribution
						motorDetail.getGrosspremium_final(), motorDetail.getDiscount(), motorDetail.getGst(),
						motorDetail.getSst_percentage(), motorDetail.getSst_amount(), motorNetVal,
						motorDetail.getMotor_net_premium(), motorDetail.getPAPremiumPayable(), capsNet,
						motorDetail.getRoadTaxAmount(),
						// motorDetail.getAgentCommissionAmount(),
						// motorDetail.getCAPSCommissionAmount(),
						motorDetail.getDriverNo(), firstDriverInfo, secondDriverInfo, thirdDriverInfo, forthDriverInfo,
						fifthDriverInfo, sixthDriverInfo, seventhDriverInfo, motorDetail.getExcess(),
						motorDetail.getLoadingPercentage(), motorDetail.getLoading(), motorDetail.getFirstDriver(),
						/* motorDetail.getSecondDriver(), */
						motorDetail.getIDType(), motorDetail.getCustomer_nric_id(), motorDetail.getCustomerGender(),
						age, motorDetail.getCustomerEmail(), motorDetail.getCustomerMobileNo(),
						motorDetail.getCustomerAddress1(), motorDetail.getCustomerAddress2(),
						motorDetail.getCustomerAddress3(), motorDetail.getCustomerPostcode(),
						motorDetail.getCustomerState(), motorDetail.getParkingAddress1(),
						motorDetail.getParkingAddress2(), RiskAddress3, motorDetail.getParkingPostcode(),
						motorDetail.getParkingState(), motorDetail.getInvoice_no(), motorDetail.getJPJStatus(),
						motorDetail.getCARSECM(), motorDetail.getATITFTCD(), motorDetail.getGARAGECD(),
						motorDetail.getBankName(), motorDetail.getAccountNo(), motorDetail.getModeStatus(),
						motorDetail.getPfNumber(),
						motorDetail.getEhailPremiumAmt(),EhailDuration,
						motorDetail.getEhailDigiLic(),motorDetail.getEHailDriver1Name(),motorDetail.getEHailDriver1IC(),
						motorDetail.getEHailDriver2Name(),motorDetail.getEHailDriver2IC()};

				Row row2 = sheet.createRow(i);

				for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
					Cell cell = row2.createCell(cellIndex);
					cell.setCellValue(fields2[cellIndex]);
				}

				i++;

			}

			// logger.info("txnStatus "+txn.getStatus());

			try {
				FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
				workbook.write(outputStream);
				workbook.close();

				File fileToDownload = new File(relativeWebPath);

				FileInputStream inputStream = new FileInputStream(fileToDownload);
				response.setContentType(servletContext.getMimeType(relativeWebPath));
				response.setContentLength((int) fileToDownload.length());

				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
				response.setHeader(headerKey, headerValue);

				// get output stream of the response
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				// write bytes read from the input stream into the output stream
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}

				outStream.flush();
				outStream.close();
				inputStream.close();

				if (fileToDownload.exists()) {

					boolean success = new File(relativeWebPath).delete();
					if (success) {
						logger.info("The file " + file + " has been successfully deleted");
					}

				}

			} catch (FileNotFoundException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			} catch (IOException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			}

		} // end : motor excel file

		// start : Ezylife excel file
		else if (productType.indexOf("TL") != -1 || productType.indexOf("IDS") != -1
				|| productType.indexOf("EZYTL") != -1 || productType.indexOf("ISCTL") != -1) {
			TransSearchObject transSearchObject_TL = new TransSearchObject();
			List<TLReportExcelVO> tlReportList = new ArrayList<TLReportExcelVO>();
			List<TLReportExcelVO> tlReportList_B4Pay = new ArrayList<TLReportExcelVO>();
			List<String> transactionStatusList = new ArrayList<String>();

			String query1 = "n";
			String query2 = "n";

			transSearchObject_TL.setProductCode(productType);

			// if Product is Chosen
			if ("".equalsIgnoreCase(status) || status == null) {
				transactionStatusList.add("S");
				transactionStatusList.add("AP");
				transactionStatusList.add("F");
				transactionStatusList.add("O");
				transactionStatusList.add("R");
				transSearchObject_TL.setTransactionStatus(transactionStatusList);
				// transSearchObject_Motor.setTransactionStatus("'S','AP','F','O','R'");
				query1 = "y";
				query2 = "y";
			} else {
				transactionStatusList.add(status);
				transSearchObject_TL.setTransactionStatus(transactionStatusList);
				// transSearchObject_Motor.setTransactionStatus(status);
			}

			if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)) {
				query1 = "y";
			}
			if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
				query2 = "y";
			}

			if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
				transSearchObject_TL.setPolicyNo(PolicyCertificateNo);
			}
			if (!"".equalsIgnoreCase(nRIC)) {
				transSearchObject_TL.setNricNo(nRIC);
			}
			if (!"".equalsIgnoreCase(receiptNo)) {
				transSearchObject_TL.setInvoiceNo(receiptNo);
			}

			transSearchObject_TL.setDateFrom(dateFrom);
			transSearchObject_TL.setDateTo(dateTo);

			// if query 1
			if ("y".equalsIgnoreCase(query1)) {
				tlReportList = tlReportExcelMapper.selectTLTransactionalReportAfterPayment(transSearchObject_TL);

			} // End if query1 is Executed

			// if query 2
			if ("y".equalsIgnoreCase(query2)) {
				tlReportList_B4Pay = tlReportExcelMapper.selectTLTransactionalReportBeforePayment(transSearchObject_TL);
			}

			tlReportList.addAll(tlReportList_B4Pay);

			Collections.sort(tlReportList, new Comparator<TLReportExcelVO>() {
				@Override
				public int compare(TLReportExcelVO o1, TLReportExcelVO o2) {
					return o1.getRecord_date().compareTo(o2.getRecord_date());
				}
			});

			Collections.reverse(tlReportList);

			if (tlReportList.size() <= 0) {
				model.addAttribute("emptySearch", "No Record Found");
			}
			model.addAttribute("tlReportList", tlReportList);

			// start excel
			Random ran = new Random();
			int randomNo = ran.nextInt(6) + 5;

			DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
			Date date = new Date();

			String file = "EtiqaTransactionReport" + dateFormat.format(date) + randomNo + ".xlsx";

			String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
			// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

			logger.info("relativeWebPath =" + relativeWebPath);

			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Etiqa Transaction Report");

			// header fields based on product
			String[] fields = null;

			fields = new String[] { "Id", "DSPQQID", "Quotation Creation Date", "Txn Date", "Transaction Start",
					"Transaction End", "Transaction Time Taken", "Product Name", "Policy/Cert No", "Coverage Amount",
					"Coverage Term", "Status", "Abandonment", "Rejection Reason", "Name", "NRIC No", "Gender", "Email",
					"Address", "Postcode", "State", "Mobile No", "Annual Premium/Contribution", "Payment Type",
					"Payment Method", "Payment Mode", "Amount Paid", "MPAY Ref No", "MPAY Auth Code", "Promo Code",
					"Agent Name", "Agent Code", "Operator Code" };

			Row row1 = sheet.createRow(0);

			for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
				Cell cell = row1.createCell(cellIndex);
				cell.setCellValue(fields[cellIndex]);
			}

			String[] fields2 = null;

			int i = 1;

			for (TLReportExcelVO TLDetail : tlReportList) {

				String Abandonment = "";

				String txnStatus = "";
				String quotationCreationDate = "";
				String transactionStartDateTime = "";
				String transactionEndDateTime = "";
				String proEntity = "";
				String paymentChannel = "";

				String txnTimeTaken = "";
				String transactionDate = "";

				if (null != TLDetail.getPmnt_gateway_code() && "" != TLDetail.getPmnt_gateway_code()) {
					if (TLDetail.getPmnt_gateway_code().indexOf("EBPG") != -1) {

						paymentChannel = "EBPG";
					} else if (TLDetail.getPmnt_gateway_code().indexOf("AMEX") != -1) {

						paymentChannel = "AMEX";
					} else if (TLDetail.getPmnt_gateway_code().indexOf("FPX") != -1) {

						paymentChannel = "FPX";
					} else if (TLDetail.getPmnt_gateway_code().indexOf("Maybank2U") != -1) {

						paymentChannel = "M2U";
					} else if (TLDetail.getPmnt_gateway_code().indexOf("CREDIT CARD") != -1) {

						paymentChannel = "MPAY";
					}

				}
				if (TLDetail.getProduct_code().equals("TL") || TLDetail.getProduct_code().equals("EZYTL")) {

					proEntity = "EIB";
				} else if (TLDetail.getProduct_code().equals("IDS") || TLDetail.getProduct_code().equals("ISCTL")) {

					proEntity = "EGB";

				}

				if (TLDetail.getPmnt_status().equals("S")) {

					txnStatus = "Successful";

				} else if (TLDetail.getPmnt_status().equals("F")) {

					txnStatus = "Fail";

				} else if (TLDetail.getPmnt_status().equals("AP")) {

					txnStatus = "Attempt Payment";

				} else if (TLDetail.getPmnt_status().equals("O")) {

					Abandonment = TLDetail.getLast_page();
					txnStatus = "Incomplete";

				} else if (TLDetail.getPmnt_status().equals("C")) {

					txnStatus = "Cancelled";

				} else if (TLDetail.getPmnt_status().equals("R")) {

					txnStatus = "Rejection : " + TLDetail.getUWReason();

				}

				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				Date quotationCreationDateTmp = null;

				try {

					quotationCreationDateTmp = formatter.parse(TLDetail.getQuotationCreateDateTime());
					quotationCreationDate = ft.format(quotationCreationDateTmp);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				if (TLDetail.getPmnt_status().equals("S") || TLDetail.getPmnt_status().equals("F")
						|| TLDetail.getPmnt_status().equals("AP")) {

					transactionStartDateTime = TLDetail.getTransaction_datetime();

					/*
					 * try {
					 *
					 * transactionStartDateTime=
					 * ft2.format(format1.parse(transactionStartDateTime));
					 *
					 * } catch (ParseException e) { // TODO Auto-generated catch block
					 * e.printStackTrace(); }
					 */
				}
				String premiumVal = "";
				DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(Locale.US);
				format.setParseBigDecimal(true);
				BigDecimal amt = new BigDecimal(0.00);
				BigDecimal calculateAmt = new BigDecimal(0.00);
				BigDecimal Annualamt = new BigDecimal(1.00);
				BigDecimal MonthlyAmt = new BigDecimal(12.00);

				try {
					amt = (BigDecimal) format.parse(TLDetail.getAmount());

					if (TLDetail.getPremium_mode().equals("annualy")) {
						calculateAmt = Annualamt.multiply(amt);
					} else {
						calculateAmt = MonthlyAmt.multiply(amt);
					}
					premiumVal = String.valueOf(calculateAmt);
				} catch (Exception e) {
					e.printStackTrace();
				}
				fields2 = new String[] { Integer.toString(i), TLDetail.getDSPQQID(), quotationCreationDate,
						transactionDate, transactionStartDateTime, transactionEndDateTime, txnTimeTaken,
						TLDetail.getProduct_code(), TLDetail.getPolicy_number(), TLDetail.getCoverage_amount(),
						TLDetail.getCoverage_term() + " Years", txnStatus, Abandonment, TLDetail.getUWReason(),
						TLDetail.getCustomer_name(), TLDetail.getCustomer_nric_id(), TLDetail.getCustomerGender(),
						TLDetail.getCustomerEmail(),
						TLDetail.getCustomerAddress1() + "," + TLDetail.getCustomerAddress2() + ","
								+ TLDetail.getCustomerAddress3(),
						TLDetail.getCustomerPostcode(), TLDetail.getCustomerState(), TLDetail.getCustomerMobileNo(),
						premiumVal,
						// TLDetail.getPremium_amount(),
						TLDetail.getPmnt_gateway_code(), paymentChannel, TLDetail.getPremium_mode(),
						TLDetail.getAmount(), TLDetail.getTxn_id(), TLDetail.getAuth_code(), TLDetail.getDiscountCode(),
						TLDetail.getAgent_name(), TLDetail.getAgent_code(), TLDetail.getOperatorCode() };

				Row row2 = sheet.createRow(i);

				for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
					Cell cell = row2.createCell(cellIndex);
					cell.setCellValue(fields2[cellIndex]);
				}

				i++;

			}

			// logger.info("txnStatus "+txn.getStatus());
			try {
				FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
				workbook.write(outputStream);
				workbook.close();

				File fileToDownload = new File(relativeWebPath);

				FileInputStream inputStream = new FileInputStream(fileToDownload);
				response.setContentType(servletContext.getMimeType(relativeWebPath));
				response.setContentLength((int) fileToDownload.length());

				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
				response.setHeader(headerKey, headerValue);

				// get output stream of the response
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				// write bytes read from the input stream into the output stream
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}

				outStream.flush();
				outStream.close();
				inputStream.close();

				if (fileToDownload.exists()) {

					boolean success = new File(relativeWebPath).delete();
					if (success) {
						logger.info("The file " + file + " has been successfully deleted");
					}

				}

			} catch (FileNotFoundException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			} catch (IOException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			}
		} // end : ezylife excel file
			// start : IDs excel file
		/*
		 * else if (productType.indexOf("IDS") != -1) { TransSearchObject
		 * transSearchObject_TL = new TransSearchObject(); List<TLReportExcelVO>
		 * tlReportList = new ArrayList<TLReportExcelVO>(); List<TLReportExcelVO>
		 * tlReportList_B4Pay = new ArrayList<TLReportExcelVO>(); List<String>
		 * transactionStatusList = new ArrayList<String>();
		 *
		 * String query1 = "n"; String query2 = "n";
		 *
		 * transSearchObject_TL.setProductCode(productType);
		 *
		 * // if Product is Chosen if ("".equalsIgnoreCase(status) || status == null) {
		 * transactionStatusList.add("S"); transactionStatusList.add("AP");
		 * transactionStatusList.add("F"); transactionStatusList.add("O");
		 * transactionStatusList.add("R");
		 * transSearchObject_TL.setTransactionStatus(transactionStatusList);
		 * //transSearchObject_Motor.setTransactionStatus("'S','AP','F','O','R'");
		 * query1 = "y"; query2 = "y"; } else{ transactionStatusList.add(status);
		 * transSearchObject_TL.setTransactionStatus(transactionStatusList);
		 * //transSearchObject_Motor.setTransactionStatus(status); }
		 *
		 * if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) ||
		 * "AP".equalsIgnoreCase(status)) { query1="y"; } if
		 * ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) { query2="y";
		 * }
		 *
		 * if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
		 * transSearchObject_TL.setPolicyNo(PolicyCertificateNo); } if
		 * (!"".equalsIgnoreCase(nRIC)) { transSearchObject_TL.setNricNo(nRIC); } if
		 * (!"".equalsIgnoreCase(receiptNo)) {
		 * transSearchObject_TL.setInvoiceNo(receiptNo); }
		 *
		 *
		 * transSearchObject_TL.setDateFrom(dateFrom);
		 * transSearchObject_TL.setDateTo(dateTo);
		 *
		 *
		 * // if query 1 if ("y".equalsIgnoreCase(query1)) {
		 * tlReportList=tlReportExcelMapper.selectTLTransactionalReportAfterPayment(
		 * transSearchObject_TL);
		 *
		 * } // End if query1 is Executed
		 *
		 *
		 *
		 * // if query 2 if ("y".equalsIgnoreCase(query2)) {
		 * tlReportList_B4Pay=tlReportExcelMapper.
		 * selectTLTransactionalReportBeforePayment(transSearchObject_TL); }
		 *
		 * tlReportList.addAll(tlReportList_B4Pay);
		 *
		 * Collections.sort(tlReportList, new Comparator<TLReportExcelVO>() {
		 *
		 * @Override public int compare(TLReportExcelVO o1, TLReportExcelVO o2) { return
		 * o1.getRecord_date().compareTo(o2.getRecord_date()); } });
		 *
		 * Collections.reverse(tlReportList);
		 *
		 * if (tlReportList.size() <= 0) { model.addAttribute("emptySearch",
		 * "No Record Found"); } model.addAttribute("tlReportList", tlReportList);
		 *
		 * //start excel
		 *
		 * Random ran = new Random(); int randomNo = ran.nextInt(6) + 5;
		 *
		 * DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS"); Date date =
		 * new Date();
		 *
		 *
		 * String file =
		 * "EtiqaTransactionReport"+dateFormat.format(date)+randomNo+".xlsx";
		 *
		 * String relativeWebPath =
		 * getClass().getClassLoader().getResource("").getPath()+file; //String
		 * absoluteDiskPath = servletContext.getRealPath(relativeWebPath);
		 *
		 * logger.info("relativeWebPath ="+relativeWebPath);
		 *
		 * XSSFWorkbook workbook = new XSSFWorkbook(); XSSFSheet sheet =
		 * workbook.createSheet("Etiqa Transaction Report");
		 *
		 * //header fields based on product String[] fields=null;
		 *
		 *
		 * fields = new String[] { "Id", "DSPQQID", "Quotation Creation Date",
		 * "Txn Date", "Transaction Start", "Transaction End", "Transaction Time Taken",
		 * "Product Name", "Policy/Cert No", "Coverage Amount", "Coverage Term",
		 * "Status", "Abandonment", "Rejection Reason", "Name", "NRIC No", "Gender",
		 * "Email", "Address", "Postcode", "State", "Mobile No",
		 * "Annual Premium/Contribution", "Payment Type", "Payment Method",
		 * "Payment Mode", "Amount Paid", "MPAY Ref No", "MPAY Auth Code", "Promo Code",
		 * "Agent Name", "Agent Code", "Operator Code" };
		 *
		 *
		 * Row row1 = sheet.createRow(0);
		 *
		 * for ( int cellIndex = 0; cellIndex < fields.length; cellIndex++ ) { Cell cell
		 * =row1.createCell( cellIndex ); cell.setCellValue( fields[cellIndex] ); }
		 *
		 *
		 *
		 * String[] fields2 = null;
		 *
		 *
		 * int i=1;
		 *
		 *
		 * for(TLReportExcelVO TLDetail : tlReportList) {
		 *
		 * String Abandonment="";
		 *
		 * String txnStatus=""; String quotationCreationDate=""; String
		 * transactionStartDateTime=""; String transactionEndDateTime=""; String
		 * proEntity=""; String paymentChannel="";
		 *
		 *
		 * String txnTimeTaken=""; String transactionDate="";
		 *
		 *
		 *
		 * if(null!= TLDetail.getPmnt_gateway_code() &&
		 * ""!=TLDetail.getPmnt_gateway_code()){ if
		 * (TLDetail.getPmnt_gateway_code().indexOf("EBPG")!=-1) {
		 *
		 * paymentChannel="EBPG"; }
		 *
		 * else if (TLDetail.getPmnt_gateway_code().indexOf("AMEX")!=-1) {
		 *
		 * paymentChannel="AMEX"; }
		 *
		 * else if (TLDetail.getPmnt_gateway_code().indexOf("FPX")!=-1) {
		 *
		 * paymentChannel="FPX"; }
		 *
		 * else if (TLDetail.getPmnt_gateway_code().indexOf("Maybank2U")!=-1) {
		 *
		 * paymentChannel="M2U"; }
		 *
		 * else if (TLDetail.getPmnt_gateway_code().indexOf("CREDIT CARD")!=-1) {
		 *
		 * paymentChannel="MPAY"; }
		 *
		 *
		 * } if(TLDetail.getProduct_code().equals("MI")){
		 *
		 *
		 * proEntity="EGIB"; }
		 *
		 * else if(TLDetail.getProduct_code().equals("MT"))
		 *
		 * {
		 *
		 * proEntity="EGTB";
		 *
		 *
		 * }
		 *
		 * if(TLDetail.getPmnt_status().equals("S")){
		 *
		 * txnStatus="Successful";
		 *
		 * } else if(TLDetail.getPmnt_status().equals("F")){
		 *
		 * txnStatus="Fail";
		 *
		 * }
		 *
		 * else if(TLDetail.getPmnt_status().equals("AP")){
		 *
		 * txnStatus="Attempt Payment";
		 *
		 * }
		 *
		 *
		 * else if(TLDetail.getPmnt_status().equals("O")){
		 *
		 * Abandonment = TLDetail.getLast_page(); txnStatus="Incomplete";
		 *
		 * }
		 *
		 *
		 * else if(TLDetail.getPmnt_status().equals("C")){
		 *
		 * txnStatus="Cancelled";
		 *
		 * }
		 *
		 *
		 * else if(TLDetail.getPmnt_status().equals("R")){
		 *
		 * txnStatus="Rejection : "+TLDetail.getUWReason();
		 *
		 * }
		 *
		 * SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 *
		 * SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); Date
		 * quotationCreationDateTmp=null;
		 *
		 * try {
		 *
		 * quotationCreationDateTmp =
		 * formatter.parse(TLDetail.getQuotationCreateDateTime());
		 * quotationCreationDate= ft.format(quotationCreationDateTmp); } catch
		 * (ParseException e) { e.printStackTrace(); }
		 *
		 *
		 * if(TLDetail.getPmnt_status().equals("S") ||
		 * TLDetail.getPmnt_status().equals("F") ||
		 * TLDetail.getPmnt_status().equals("AP")){
		 *
		 * transactionStartDateTime = TLDetail.getTransaction_datetime();
		 *
		 *
		 * try {
		 *
		 * transactionStartDateTime=
		 * ft2.format(format1.parse(transactionStartDateTime));
		 *
		 * } catch (ParseException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 *
		 * }
		 *
		 *
		 *
		 * fields2 = new String[] {Integer.toString(i), TLDetail.getDSPQQID(),
		 * quotationCreationDate, transactionDate, transactionStartDateTime,
		 * transactionEndDateTime, txnTimeTaken, TLDetail.getProduct_code(),
		 * TLDetail.getPolicy_number(), TLDetail.getCoverage_amount(),
		 * TLDetail.getCoverage_term()+" Years", txnStatus, Abandonment,
		 * TLDetail.getUWReason(), TLDetail.getCustomer_name(),
		 * TLDetail.getCustomer_nric_id(), TLDetail.getCustomerGender(),
		 * TLDetail.getCustomerEmail(),
		 * TLDetail.getCustomerAddress1()+","+TLDetail.getCustomerAddress2()+","+
		 * TLDetail.getCustomerAddress3(), TLDetail.getCustomerPostcode(),
		 * TLDetail.getCustomerState(), TLDetail.getCustomerMobileNo(),
		 * TLDetail.getPremium_amount(), TLDetail.getPmnt_gateway_code(),
		 * paymentChannel, TLDetail.getPremium_mode(), TLDetail.getAmount(),
		 * TLDetail.getTxn_id(), TLDetail.getAuth_code(), TLDetail.getDiscountCode(),
		 * TLDetail.getAgent_name(), TLDetail.getAgent_code(),
		 * TLDetail.getOperatorCode() };
		 *
		 *
		 *
		 * Row row2 = sheet.createRow(i);
		 *
		 * for ( int cellIndex = 0; cellIndex < fields2.length; cellIndex++ ) { Cell
		 * cell =row2.createCell( cellIndex ); cell.setCellValue( fields2[cellIndex] );
		 * }
		 *
		 * i++;
		 *
		 *
		 *
		 * }
		 *
		 *
		 * //logger.info("txnStatus "+txn.getStatus());
		 *
		 * try { FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
		 * workbook.write(outputStream); workbook.close();
		 *
		 *
		 * File fileToDownload = new File(relativeWebPath);
		 *
		 * FileInputStream inputStream = new FileInputStream(fileToDownload);
		 * response.setContentType(servletContext.getMimeType(relativeWebPath));
		 * response.setContentLength((int) fileToDownload.length());
		 *
		 * String headerKey = "Content-Disposition"; String headerValue =
		 * String.format("attachment; filename=\"%s\"", fileToDownload.getName());
		 * response.setHeader(headerKey, headerValue);
		 *
		 * // get output stream of the response OutputStream outStream =
		 * response.getOutputStream();
		 *
		 * byte[] buffer = new byte[4096]; int bytesRead = -1;
		 *
		 * // write bytes read from the input stream into the output stream while
		 * ((bytesRead = inputStream.read(buffer)) != -1) { outStream.write(buffer, 0,
		 * bytesRead); }
		 *
		 * outStream.flush(); outStream.close(); inputStream.close();
		 *
		 *
		 * if (fileToDownload.exists()){
		 *
		 * boolean success = (new File(relativeWebPath)).delete(); if (success) {
		 * logger.info("The file "+file+" has been successfully deleted"); }
		 *
		 *
		 * }
		 *
		 *
		 * } catch (FileNotFoundException e) { response.getOutputStream().flush();
		 * response.getOutputStream().close();
		 *
		 * e.printStackTrace(); } catch (IOException e) {
		 * response.getOutputStream().flush(); response.getOutputStream().close();
		 *
		 * e.printStackTrace(); }
		 *
		 * } //end : ezylife excel file
		 */
		else if (productType.indexOf("WTC") != -1 || productType.indexOf("TPT") != -1) {

			TransSearchObject transSearchObject_WTC = new TransSearchObject();
			List<WTCReportExcelVO> wtcReportList = new ArrayList<WTCReportExcelVO>();
			List<WTCReportExcelVO> wtcReportList_B4Pay = new ArrayList<WTCReportExcelVO>();
			List<String> transactionStatusList = new ArrayList<String>();

			String query1 = "n";
			String query2 = "n";

			transSearchObject_WTC.setProductCode(productType);

			if ("".equalsIgnoreCase(productType) || productType == null) {
				if ("".equalsIgnoreCase(status) || status == null) { // productType empty and transaction status empty
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transSearchObject_WTC.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus("'S','AP','F'");
					query1 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_WTC.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus(status);
				}
			} else { // if Product is Chosen
				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_WTC.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus("'S','AP','F','O','R'");
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_WTC.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus(status);
				}
			}
			if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)) {
				query1 = "y";
			}
			if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
				query2 = "y";
			}

			if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
				transSearchObject_WTC.setPolicyNo(PolicyCertificateNo);
				query2 = null;
			}
			if (!"".equalsIgnoreCase(nRIC)) {
				transSearchObject_WTC.setNricNo(nRIC);
			}
			if (!"".equalsIgnoreCase(receiptNo)) {
				transSearchObject_WTC.setInvoiceNo(receiptNo);
				query2 = null;
			}

			transSearchObject_WTC.setDateFrom(dateFrom);
			transSearchObject_WTC.setDateTo(dateTo);

			// if query 1
			if ("y".equalsIgnoreCase(query1)) {
				wtcReportList = wtcReportExcelMapper.selectWTCTransactionalReportAfterPayment(transSearchObject_WTC);

			} // End if query1 is Executed

			// if query 2
			if ("y".equalsIgnoreCase(query2)) {
				wtcReportList_B4Pay = wtcReportExcelMapper
						.selectWTCTransactionalReportBeforePayment(transSearchObject_WTC);
			}
			// if Query before payment and after payment then combine
			// if ("y".equalsIgnoreCase(query1) && "y".equalsIgnoreCase(query2)) {
			wtcReportList.addAll(wtcReportList_B4Pay);
			// }
			Collections.sort(wtcReportList, new Comparator<WTCReportExcelVO>() {
				@Override
				public int compare(WTCReportExcelVO o1, WTCReportExcelVO o2) {
					return o1.getRecord_date().compareTo(o2.getRecord_date());
				}
			});

			Collections.reverse(wtcReportList);

			if (wtcReportList.size() <= 0) {
				model.addAttribute("emptySearch", "No Record Found");
			}
			model.addAttribute("reportResult", wtcReportList);

			// start excel

			Random ran = new Random();
			int randomNo = ran.nextInt(6) + 5;

			DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
			Date date = new Date();

			String file = "EtiqaTransactionReport" + dateFormat.format(date) + randomNo + ".xlsx";

			String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
			// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Etiqa Transaction Report");

			// header fields based on product
			String[] fields = null;

			fields = new String[] { "Id", "DSPQQID", "Quotation Creation Date", "Txn Id", "Txn Start Date",
					"Txn End Date", "Entity", "Name", "PolicyNo", "ProductDesc", "Plan", "Scheme", "Id Type",
					"Insured ID", "Gender", "Email", "Address 1", "Address 2", "Address 3", "State", "PostCode",
					"Contact No", "Agent Code", "Agent Name", "Operator Code", "Promo Code", "EffDate", "Travel Type",
					"Area Code", "FPX Ref", "EBPG Ref", "M2U Ref", "Payment Mode", "Payment Payable", "Gross Premium",
					"Discount", "GST", "Comm", "Tax Invoice No", "Txn Status", "Abandonment", "PDPA", "Spouse",
					"Spouse Email", "Spouse Id Type", "Spouse Id No", "Spouse DOB", "Spouse Gender", "Child 1",
					"Child 1 Id Type", "Child 1 Id No", "Child 1 DOB", "Child 1 Gender", "Child 2", "Child 2 Id Type",
					"Child 2 Id No", "Child 2 DOB", "Child 2 Gender", "Child 3", "Child 3 Id Type", "Child 3 Id No",
					"Child 3 DOB", "Child 3 Gender", "Child 4", "Child 4 Id Type", "Child 4 Id No", "Child 4 DOB",
					"Child 4 Gender", "Child 5", "Child 5 Id Type", "Child 5 Id No", "Child 5 DOB", "Child 5 Gender",
					"Child 6", "Child 6 Id Type", "Child 6 Id No", "Child 6 DOB", "Child 6 Gender", "Child 7",
					"Child 7 Id Type", "Child 7 Id No", "Child 7 DOB", "Child 7 Gender", "Mode" };

			Row row1 = sheet.createRow(0);

			for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
				Cell cell = row1.createCell(cellIndex);
				cell.setCellValue(fields[cellIndex]);
			}

			String[] fields2 = null;

			int i = 1;

			for (WTCReportExcelVO WTCDetail : wtcReportList) {

				String Abandonment = "";

				String txnStatus = "";
				String quotationCreationDate = "";
				String transactionStartDateTime = "";
				String transactionEndDateTime = "";
				String proEntity = "";
				String paymentChannel = "";
				String coverageStartDate = "";
				String coverageEndDate = "";

				if (null != WTCDetail.getPmnt_gateway_code() && "" != WTCDetail.getPmnt_gateway_code()) {
					if (WTCDetail.getPmnt_gateway_code().indexOf("EBPG") != -1) {

						paymentChannel = "EBPG";
					}

					else if (WTCDetail.getPmnt_gateway_code().indexOf("AMEX") != -1) {

						paymentChannel = "AMEX";
					}

					else if (WTCDetail.getPmnt_gateway_code().indexOf("FPX") != -1) {

						paymentChannel = "FPX";
					}

					else if (WTCDetail.getPmnt_gateway_code().indexOf("Maybank2U") != -1) {

						paymentChannel = "M2U";
					}

					else if (WTCDetail.getPmnt_gateway_code().indexOf("CREDIT CARD") != -1) {

						paymentChannel = "MPAY";
					}

				}
				if (WTCDetail.getProduct_code().equals("WTC")) {

					proEntity = "EGIB";
				}

				else if (WTCDetail.getProduct_code().equals("TPT"))

				{

					proEntity = "EGTB";

				}

				if (WTCDetail.getPmnt_status().equals("S")) {

					txnStatus = "Successful";

				} else if (WTCDetail.getPmnt_status().equals("F")) {

					txnStatus = "Fail";

				}

				else if (WTCDetail.getPmnt_status().equals("AP")) {

					txnStatus = "Attempt Payment";

				}

				else if (WTCDetail.getPmnt_status().equals("O")) {

					Abandonment = WTCDetail.getLast_page();
					txnStatus = "Incomplete";

				}

				else if (WTCDetail.getPmnt_status().equals("C")) {

					txnStatus = "Cancelled";

				}

				else if (WTCDetail.getPmnt_status().equals("R")) {

					txnStatus = "Rejection : " + WTCDetail.getReason();

				}

				SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

				if (null != WTCDetail.getRecord_date()) {
					quotationCreationDate = ft.format(WTCDetail.getRecord_date());
				}

				if (WTCDetail.getPmnt_status().equals("S") || WTCDetail.getPmnt_status().equals("F")
						|| WTCDetail.getPmnt_status().equals("AP")) {

					transactionStartDateTime = WTCDetail.getTransaction_datetime();

					/*
					 * try {
					 *
					 * transactionStartDateTime=
					 * ft2.format(format1.parse(transactionStartDateTime));
					 *
					 * } catch (ParseException e) { // TODO Auto-generated catch block
					 * e.printStackTrace(); }
					 */

				}

				if (null != WTCDetail.getTravel_start_date()) {

					String valueFromDB = WTCDetail.getTravel_start_date();
					Date d1 = null;
					logger.info(valueFromDB + "valueFromDB");
					try {
						d1 = sdf1.parse(valueFromDB);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
					String startDateWithoutTime = sdf.format(d1);
					// logger.info("sdf.format(d1) " + startDateWithoutTime);

					coverageStartDate = startDateWithoutTime;

				}

				if (null != WTCDetail.getTravel_end_date()) {

					String valueFromDB = WTCDetail.getTravel_end_date();
					Date d1 = null;
					try {
						d1 = sdf1.parse(valueFromDB);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
					String endDateWithoutTime = sdf.format(d1);
					// logger.info("sdf.format(d1) " + startDateWithoutTime);

					coverageEndDate = endDateWithoutTime;

				}

				int countForallChildRecords = 0;

				if (null != WTCDetail.getChild_name()) {

					logger.info(WTCDetail.getChild_name() + "WTCDetail.getChild_name()");
					if (WTCDetail.getChild_name().indexOf("|") != -1) {

						String[] childName = WTCDetail.getChild_name().split("\\|");
						;

						int childCount = childName.length;
						countForallChildRecords = childCount;
						for (int j = 0; j < childCount; j++) {
							logger.info(childName[j] + "childName[j] Child Name");
							if (j == 0) {
								WTCDetail.setChild_name_1(childName[j]);
							}
							if (j == 1) {
								WTCDetail.setChild_name_2(childName[j]);
							}
							if (j == 2) {
								WTCDetail.setChild_name_3(childName[j]);
							}
							if (j == 3) {
								WTCDetail.setChild_name_4(childName[j]);
							}
							if (j == 4) {
								WTCDetail.setChild_name_5(childName[j]);
							}

							if (j == 5) {
								WTCDetail.setChild_name_6(childName[j]);
							}

							if (j == 6) {
								WTCDetail.setChild_name_7(childName[j]);
							}

						}

					} else {
						countForallChildRecords = 1;
						logger.info(WTCDetail.getChild_name() + " Else WTCDetail.getChild_name()");
						WTCDetail.setChild_name_1(WTCDetail.getChild_name());

					}

				}

				logger.info(countForallChildRecords + " countForallChildRecords");
				if (null != WTCDetail.getChild_ID_type()) {

					logger.info(WTCDetail.getChild_ID_type() + "WTCDetail.getChild_ID_type()");
					if (WTCDetail.getChild_ID_type().indexOf("|") != -1) {

						String[] childIDType = WTCDetail.getChild_ID_type().split("\\|");
						;

						int childCount = childIDType.length;

						for (int j = 0; j < countForallChildRecords; j++) {
							logger.info(childIDType[j] + "childIDType[j] ID Type");
							if (j == 0) {
								WTCDetail.setChild_ID_type_1(childIDType[j]);
							}
							if (j == 1) {
								WTCDetail.setChild_ID_type_2(childIDType[j]);
							}
							if (j == 2) {
								WTCDetail.setChild_ID_type_3(childIDType[j]);
							}
							if (j == 3) {
								WTCDetail.setChild_ID_type_4(childIDType[j]);
							}
							if (j == 4) {
								WTCDetail.setChild_ID_type_5(childIDType[j]);
							}

							if (j == 5) {
								WTCDetail.setChild_ID_type_6(childIDType[j]);
							}

							if (j == 6) {
								WTCDetail.setChild_ID_type_7(childIDType[j]);
							}

						}

					} else {
						logger.info(WTCDetail.getChild_ID_type() + " ELse WTCDetail.getChild_ID_type()");

						if (countForallChildRecords == 1) {
							WTCDetail.setChild_ID_type_1(WTCDetail.getChild_ID_type());
						} else {
							for (int j = 0; j < countForallChildRecords; j++) {
								if (j == 0) {
									WTCDetail.setChild_ID_type_1(WTCDetail.getChild_ID_type());
								}
								if (j == 1) {
									WTCDetail.setChild_ID_type_2("");
								}
								if (j == 2) {
									WTCDetail.setChild_ID_type_3("");
								}
								if (j == 3) {
									WTCDetail.setChild_ID_type_4("");
								}
								if (j == 4) {
									WTCDetail.setChild_ID_type_5("");
								}

								if (j == 5) {
									WTCDetail.setChild_ID_type_6("");
								}

								if (j == 6) {
									WTCDetail.setChild_ID_type_7("");
								}

							}

						}
					}

				}

				if (null != WTCDetail.getChild_ID_number()) {

					logger.info(WTCDetail.getChild_ID_number() + " WTCDetail.getChild_ID_number");
					if (WTCDetail.getChild_ID_number().indexOf("|") != -1) {

						String[] childIDNumber = WTCDetail.getChild_ID_number().split("\\|");
						;

						int childCount = childIDNumber.length;

						for (int j = 0; j < countForallChildRecords; j++) {
							logger.info(childIDNumber[j] + " childIDNumber[j] ID No.");
							if (j == 0) {
								WTCDetail.setChild_ID_number_1(childIDNumber[j]);
							}
							if (j == 1) {
								WTCDetail.setChild_ID_number_2(childIDNumber[j]);
							}
							if (j == 2) {
								WTCDetail.setChild_ID_number_3(childIDNumber[j]);
							}
							if (j == 3) {
								WTCDetail.setChild_ID_number_4(childIDNumber[j]);
							}
							if (j == 4) {
								WTCDetail.setChild_ID_number_5(childIDNumber[j]);
							}

							if (j == 5) {
								WTCDetail.setChild_ID_number_6(childIDNumber[j]);
							}

							if (j == 6) {
								WTCDetail.setChild_ID_number_7(childIDNumber[j]);
							}

						}

					} else {
						logger.info(WTCDetail.getChild_ID_number() + "Else  WTCDetail.getChild_ID_number");
						if (countForallChildRecords == 1) {
							WTCDetail.setChild_ID_number_1(WTCDetail.getChild_ID_number());
						} else {
							for (int j = 0; j < countForallChildRecords; j++) {
								if (j == 0) {
									WTCDetail.setChild_ID_number_1(WTCDetail.getChild_ID_number());
								}
								if (j == 1) {
									WTCDetail.setChild_ID_number_2("");
								}
								if (j == 2) {
									WTCDetail.setChild_ID_number_3("");
								}
								if (j == 3) {
									WTCDetail.setChild_ID_number_4("");
								}
								if (j == 4) {
									WTCDetail.setChild_ID_number_5("");
								}

								if (j == 5) {
									WTCDetail.setChild_ID_number_6("");
								}

								if (j == 6) {
									WTCDetail.setChild_ID_number_7("");
								}

							}

						}
					}

				}

				if (null != WTCDetail.getChild_DOB()) {

					if (WTCDetail.getChild_DOB().indexOf("|") != -1) {
						logger.info(WTCDetail.getChild_DOB() + "WTCDetail.getChild_DOB()");
						String[] childDOB = WTCDetail.getChild_DOB().split("\\|");
						;

						int childCount = childDOB.length;

						for (int j = 0; j < countForallChildRecords; j++) {
							logger.info(childDOB[j] + "childDOB[j] DOB");
							if (j == 0) {
								WTCDetail.setChild_DOB_1(childDOB[j]);
							}
							if (j == 1) {
								WTCDetail.setChild_DOB_2(childDOB[j]);
							}
							if (j == 2) {
								WTCDetail.setChild_DOB_3(childDOB[j]);
							}
							if (j == 3) {
								WTCDetail.setChild_DOB_4(childDOB[j]);
							}
							if (j == 4) {
								WTCDetail.setChild_DOB_5(childDOB[j]);
							}

							if (j == 5) {
								WTCDetail.setChild_DOB_6(childDOB[j]);
							}

							if (j == 6) {
								WTCDetail.setChild_DOB_7(childDOB[j]);
							}

						}

					} else {
						logger.info(WTCDetail.getChild_DOB() + "WTCDetail.getChild_DOB()");
						if (countForallChildRecords == 1) {
							WTCDetail.setChild_DOB_1(WTCDetail.getChild_DOB());
						} else {
							for (int j = 0; j < countForallChildRecords; j++) {

								if (j == 0) {
									WTCDetail.setChild_DOB_1(WTCDetail.getChild_DOB());
								}
								if (j == 1) {
									WTCDetail.setChild_DOB_2("");
								}
								if (j == 2) {
									WTCDetail.setChild_DOB_3("");
								}
								if (j == 3) {
									WTCDetail.setChild_DOB_4("");
								}
								if (j == 4) {
									WTCDetail.setChild_DOB_5("");
								}

								if (j == 5) {
									WTCDetail.setChild_DOB_6("");
								}

								if (j == 6) {
									WTCDetail.setChild_DOB_7("");
								}

							}

						}

					}

				}

				if (null != WTCDetail.getChild_gender()) {

					if (WTCDetail.getChild_gender().indexOf("|") != -1) {

						String[] childGender = WTCDetail.getChild_gender().split("\\|");
						;

						int childCount = childGender.length;

						for (int j = 0; j < countForallChildRecords; j++) {

							if (j == 0) {
								WTCDetail.setChild_gender_1(childGender[j]);
							}
							if (j == 1) {
								WTCDetail.setChild_gender_2(childGender[j]);
							}
							if (j == 2) {
								WTCDetail.setChild_gender_3(childGender[j]);
							}
							if (j == 3) {
								WTCDetail.setChild_gender_4(childGender[j]);
							}
							if (j == 4) {
								WTCDetail.setChild_gender_5(childGender[j]);
							}

							if (j == 5) {
								WTCDetail.setChild_gender_6(childGender[j]);
							}

							if (j == 6) {
								WTCDetail.setChild_gender_7(childGender[j]);
							}

						}

					}

					else {

						if (countForallChildRecords == 1) {
							WTCDetail.setChild_gender_1(WTCDetail.getChild_gender());
						} else {
							for (int j = 0; j < countForallChildRecords; j++) {

								if (j == 0) {
									WTCDetail.setChild_gender_1(WTCDetail.getChild_gender());
								}
								if (j == 1) {
									WTCDetail.setChild_gender_2("");
								}
								if (j == 2) {
									WTCDetail.setChild_gender_3("");
								}
								if (j == 3) {
									WTCDetail.setChild_gender_4("");
								}
								if (j == 4) {
									WTCDetail.setChild_gender_5("");
								}

								if (j == 5) {
									WTCDetail.setChild_gender_6("");
								}

								if (j == 6) {
									WTCDetail.setChild_gender_7("");
								}
							}

						}
					}

				}

				fields2 = new String[] { Integer.toString(i), WTCDetail.getDSPQQID(), quotationCreationDate,
						WTCDetail.getPaymentTrxID(), transactionStartDateTime, transactionEndDateTime, proEntity,
						WTCDetail.getCustomer_name(), WTCDetail.getPolicy_number(), WTCDetail.getProduct_code(),
						WTCDetail.getOffered_plan_name(), WTCDetail.getTravlling_with(),
						WTCDetail.getCustomer_ID_type(), WTCDetail.getCustomer_nric_id(),
						WTCDetail.getCustomer_gender(), WTCDetail.getCustomer_email(),
						WTCDetail.getCustomer_address_1(), WTCDetail.getCustomer_address_2(),
						WTCDetail.getCustomer_address_3(), WTCDetail.getCustomer_state(),
						WTCDetail.getCustomer_postcode(), WTCDetail.getCustomer_mobile_no(), WTCDetail.getAgent_code(),
						WTCDetail.getAgent_name(), WTCDetail.getOperator_code(), WTCDetail.getDiscount_code(),
						coverageStartDate + "-" + coverageEndDate, WTCDetail.getTravel_area_type(),
						WTCDetail.getArea_code(), WTCDetail.getFpx_fpxtxnid(), WTCDetail.getTransaction_id(),
						WTCDetail.getBankrefno(), paymentChannel, WTCDetail.getTotal_premium_payable(),
						WTCDetail.getGrosspremium_final(), WTCDetail.getDiscount(), WTCDetail.getGst(),
						WTCDetail.getCommission(), WTCDetail.getInvoice_no(), txnStatus, Abandonment,
						WTCDetail.getPDPA(), WTCDetail.getSpouse_name(), WTCDetail.getSpouse_email(),
						WTCDetail.getSpouse_ID_type(), WTCDetail.getSpouse_ID_number(), WTCDetail.getSpouse_DOB(),
						WTCDetail.getSpouse_gender(), WTCDetail.getChild_name_1(), WTCDetail.getChild_ID_type_1(),
						WTCDetail.getChild_ID_number_1(), WTCDetail.getChild_DOB_1(), WTCDetail.getChild_gender_1(),
						WTCDetail.getChild_name_2(), WTCDetail.getChild_ID_type_2(), WTCDetail.getChild_ID_number_2(),
						WTCDetail.getChild_DOB_2(), WTCDetail.getChild_gender_2(), WTCDetail.getChild_name_3(),
						WTCDetail.getChild_ID_type_3(), WTCDetail.getChild_ID_number_3(), WTCDetail.getChild_DOB_3(),
						WTCDetail.getChild_gender_3(), WTCDetail.getChild_name_4(), WTCDetail.getChild_ID_type_4(),
						WTCDetail.getChild_ID_number_4(), WTCDetail.getChild_DOB_4(), WTCDetail.getChild_gender_4(),
						WTCDetail.getChild_name_5(), WTCDetail.getChild_ID_type_5(), WTCDetail.getChild_ID_number_5(),
						WTCDetail.getChild_DOB_5(), WTCDetail.getChild_gender_5(), WTCDetail.getChild_name_6(),
						WTCDetail.getChild_ID_type_6(), WTCDetail.getChild_ID_number_6(), WTCDetail.getChild_DOB_6(),
						WTCDetail.getChild_gender_6(), WTCDetail.getChild_name_7(), WTCDetail.getChild_ID_type_7(),
						WTCDetail.getChild_ID_number_7(), WTCDetail.getChild_DOB_7(), WTCDetail.getChild_gender_7(),
						WTCDetail.getModeStatus() };

				Row row2 = sheet.createRow(i);

				for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
					Cell cell = row2.createCell(cellIndex);
					cell.setCellValue(fields2[cellIndex]);
				}

				i++;

			}

			// logger.info("txnStatus "+txn.getStatus());

			try {
				FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
				workbook.write(outputStream);
				workbook.close();

				File fileToDownload = new File(relativeWebPath);

				FileInputStream inputStream = new FileInputStream(fileToDownload);
				response.setContentType(servletContext.getMimeType(relativeWebPath));
				response.setContentLength((int) fileToDownload.length());

				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
				response.setHeader(headerKey, headerValue);

				// get output stream of the response
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				// write bytes read from the input stream into the output stream
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}

				outStream.flush();
				outStream.close();
				inputStream.close();

				if (fileToDownload.exists()) {

					boolean success = new File(relativeWebPath).delete();
					if (success) {
						logger.info("The file " + file + " has been successfully deleted");
					}

				}

			} catch (FileNotFoundException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			} catch (IOException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			}

		}

		// end excel

		else if (productType.indexOf("HOHH") != -1) {

			TransSearchObject transSearchObject_HOHH = new TransSearchObject();
			List<HOHHReportExcelVO> hohhReportList = new ArrayList<HOHHReportExcelVO>();
			List<HOHHReportExcelVO> hohhReportList_B4Pay = new ArrayList<HOHHReportExcelVO>();
			List<String> transactionStatusList = new ArrayList<String>();

			String query1 = "n";
			String query2 = "n";

			transSearchObject_HOHH.setProductCode(productType);

			if ("".equalsIgnoreCase(productType) || productType == null) {
				if ("".equalsIgnoreCase(status) || status == null) { // productType empty and transaction status empty
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transSearchObject_HOHH.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus("'S','AP','F'");
					query1 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_HOHH.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus(status);
				}
			} else { // if Product is Chosen
				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_HOHH.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus("'S','AP','F','O','R'");
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_HOHH.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus(status);
				}
			}
			if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)) {
				query1 = "y";
			}
			if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
				query2 = "y";
			}

			if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
				transSearchObject_HOHH.setPolicyNo(PolicyCertificateNo);
				query2 = null;
			}
			if (!"".equalsIgnoreCase(nRIC)) {
				transSearchObject_HOHH.setNricNo(nRIC);
			}
			if (!"".equalsIgnoreCase(receiptNo)) {
				transSearchObject_HOHH.setInvoiceNo(receiptNo);
				query2 = null;
			}

			transSearchObject_HOHH.setDateFrom(dateFrom);
			transSearchObject_HOHH.setDateTo(dateTo);

			// if query 1
			if ("y".equalsIgnoreCase(query1)) {
				hohhReportList = hohhReportExcelMapper
						.selectHOHHTransactionalReportAfterPayment(transSearchObject_HOHH);

			} // End if query1 is Executed

			// if query 2
			if ("y".equalsIgnoreCase(query2)) {
				hohhReportList_B4Pay = hohhReportExcelMapper
						.selectHOHHTransactionalReportBeforePayment(transSearchObject_HOHH);
			}

			hohhReportList.addAll(hohhReportList_B4Pay);

			Collections.sort(hohhReportList, new Comparator<HOHHReportExcelVO>() {
				@Override
				public int compare(HOHHReportExcelVO o1, HOHHReportExcelVO o2) {
					return o1.getRecord_date().compareTo(o2.getRecord_date());
				}
			});

			Collections.reverse(hohhReportList);

			if (hohhReportList.size() <= 0) {
				model.addAttribute("emptySearch", "No Record Found");
			}
			model.addAttribute("reportResult", hohhReportList);

			// start excel

			Random ran = new Random();
			int randomNo = ran.nextInt(6) + 5;

			DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
			Date date = new Date();

			String file = "EtiqaTransactionReport" + dateFormat.format(date) + randomNo + ".xlsx";

			String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
			// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Etiqa Transaction Report");

			// header fields based on product
			String[] fields = null;

			fields = new String[] { "Id", "DSPQQID", "Quotation Creation Date", "Txn Id", "Txn Start Date",
					"Txn End Date", "Entity", "Name", "PolicyNo", "ProductDesc", "Building Type", "Cons Type",
					"Id Type", "Insured ID", "Email", "Address 1", "Address 2", "Address 3", "PostCode", "State",
					"RiskAddress1", "RiskAddress2", "RiskAddress3", "RiskPostcode", "RiskState", "Contact No",
					"Agent Code", "Agent Name", "Operator Code", "Promo Code", "EffDate", "HO SI", "HH SI", "HOHH SI",
					"Extended Theft", "RSMD", "FPX Ref", "EBPG Ref", "M2U Ref", "Payment Mode", "Payment Payable",
					"Gross Premium", "Discount", "GST", "SST Percentage", "SST Amount", "Comm", "Tax Invoice No",
					"Txn Status", "Abandonment", "PDPA", "UW1", "UW2", "UW3", "UW4", "UW5", "Add Item 1", "Add Item 2",
					"Add Item 3", "Add Item 4", "Add Item 5", "Add Item 6", "Add Item 7", "Add Item 8", "Add Item 9",
					"Add Item 10", "Mode", "PF Number" };

			Row row1 = sheet.createRow(0);

			for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
				Cell cell = row1.createCell(cellIndex);
				cell.setCellValue(fields[cellIndex]);
			}

			String[] fields2 = null;

			int i = 1;

			for (HOHHReportExcelVO HOHHDetail : hohhReportList) {

				String Abandonment = "";

				String txnStatus = "";
				String quotationCreationDate = "";
				String transactionStartDateTime = "";
				String transactionEndDateTime = "";
				String proEntity = "";
				String paymentChannel = "";
				String coverageStartDate = "";
				String coverageEndDate = "";

				if (null != HOHHDetail.getPmnt_gateway_code() && "" != HOHHDetail.getPmnt_gateway_code()) {
					if (HOHHDetail.getPmnt_gateway_code().indexOf("EBPG") != -1) {

						paymentChannel = "EBPG";
					}

					else if (HOHHDetail.getPmnt_gateway_code().indexOf("AMEX") != -1) {

						paymentChannel = "AMEX";
					}

					else if (HOHHDetail.getPmnt_gateway_code().indexOf("FPX") != -1) {

						paymentChannel = "FPX";
					}

					else if (HOHHDetail.getPmnt_gateway_code().indexOf("Maybank2U") != -1) {

						paymentChannel = "M2U";
					}

					else if (HOHHDetail.getPmnt_gateway_code().indexOf("CREDIT CARD") != -1) {

						paymentChannel = "MPAY";
					}

				}
				if (HOHHDetail.getProduct_code().equals("HOHH")) {

					proEntity = "EGIB";
				}

				else if (HOHHDetail.getProduct_code().equals("HOHH-ETB"))

				{

					proEntity = "EGTB";

				}

				if (HOHHDetail.getPmnt_status().equals("S")) {

					txnStatus = "Successful";

				} else if (HOHHDetail.getPmnt_status().equals("F")) {

					txnStatus = "Fail";

				}

				else if (HOHHDetail.getPmnt_status().equals("AP")) {

					txnStatus = "Attempt Payment";

				}

				else if (HOHHDetail.getPmnt_status().equals("O")) {

					Abandonment = HOHHDetail.getLast_page();
					txnStatus = "Incomplete";

				}

				else if (HOHHDetail.getPmnt_status().equals("C")) {

					txnStatus = "Cancelled";

				}

				else if (HOHHDetail.getPmnt_status().equals("R")) {

					txnStatus = "Rejection : " + HOHHDetail.getReason();

				}

				if (HOHHDetail.getPmnt_status().equals("S") || HOHHDetail.getPmnt_status().equals("F")
						|| HOHHDetail.getPmnt_status().equals("AP")) {

					transactionStartDateTime = HOHHDetail.getTransaction_datetime();

				}

				fields2 = new String[] { Integer.toString(i), HOHHDetail.getDSPQQID(),
						HOHHDetail.getQuotation_datetime(), HOHHDetail.getPaymentTrxID(), transactionStartDateTime,
						transactionEndDateTime, proEntity, HOHHDetail.getCustomer_name(), HOHHDetail.getPolicy_number(),
						HOHHDetail.getProduct_code(), HOHHDetail.getHome_type(),
						HOHHDetail.getBuild_construction_type(), HOHHDetail.getCustomer_ID_type(),
						HOHHDetail.getCustomer_nric_id(), HOHHDetail.getCustomer_email(),
						HOHHDetail.getCustomer_address_1(), HOHHDetail.getCustomer_address_2(),
						HOHHDetail.getCustomer_address_3(), HOHHDetail.getCustomer_postcode(),
						HOHHDetail.getCustomer_state(), HOHHDetail.getProperty_insured_address(),
						HOHHDetail.getProperty_insured_address_1(), HOHHDetail.getProperty_insured_address_2(),
						HOHHDetail.getProperty_insured_postcode(), HOHHDetail.getProperty_insured_state(),
						HOHHDetail.getCustomer_mobile_no(), HOHHDetail.getAgent_code(), HOHHDetail.getAgent_name(),
						HOHHDetail.getOperator_code(), HOHHDetail.getDiscount_code(),
						HOHHDetail.getCoverage_start_date() + "-" + HOHHDetail.getCoverage_end_date(),
						// HOHHDetail.getHOSI(),
						// HOHHDetail.getHHSI(),
						HOHHDetail.getHome_sum_insured(), HOHHDetail.getContent_sum_insured(), HOHHDetail.getHOHHSI(),
						HOHHDetail.getAdd_ben_extended_theft_amt(), HOHHDetail.getAdd_ben_riot_strike_amt(),
						HOHHDetail.getFpx_fpxtxnid(), HOHHDetail.getTransaction_id(), HOHHDetail.getBankrefno(),
						paymentChannel, HOHHDetail.getAmount(), HOHHDetail.getGrosspremium_final(),
						HOHHDetail.getDiscount(), HOHHDetail.getGst(), HOHHDetail.getSst_percentage(),
						HOHHDetail.getSst_amount(), HOHHDetail.getCommission(), HOHHDetail.getInvoice_no(), txnStatus,
						Abandonment, HOHHDetail.getPDPA(), HOHHDetail.getUW1(), HOHHDetail.getUW2(),
						HOHHDetail.getUW3(), HOHHDetail.getUW4(), HOHHDetail.getUW5(), HOHHDetail.getBenefit_item_1(),
						HOHHDetail.getBenefit_item_2(), HOHHDetail.getBenefit_item_3(), HOHHDetail.getBenefit_item_4(),
						HOHHDetail.getBenefit_item_5(), HOHHDetail.getBenefit_item_6(), HOHHDetail.getBenefit_item_7(),
						HOHHDetail.getBenefit_item_8(), HOHHDetail.getBenefit_item_9(), HOHHDetail.getBenefit_item_10(),
						HOHHDetail.getModeStatus(), HOHHDetail.getPfNumber() };

				Row row2 = sheet.createRow(i);

				for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
					Cell cell = row2.createCell(cellIndex);
					cell.setCellValue(fields2[cellIndex]);
				}

				i++;

			}

			// logger.info("txnStatus "+txn.getStatus());

			try {
				FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
				workbook.write(outputStream);
				workbook.close();

				File fileToDownload = new File(relativeWebPath);

				FileInputStream inputStream = new FileInputStream(fileToDownload);
				response.setContentType(servletContext.getMimeType(relativeWebPath));
				response.setContentLength((int) fileToDownload.length());

				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
				response.setHeader(headerKey, headerValue);

				// get output stream of the response
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				// write bytes read from the input stream into the output stream
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}

				outStream.flush();
				outStream.close();
				inputStream.close();

				if (fileToDownload.exists()) {

					boolean success = new File(relativeWebPath).delete();
					if (success) {
						logger.info("The file " + file + " has been successfully deleted");
					}

				}

			} catch (FileNotFoundException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			} catch (IOException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			}

		}

		// END: HOHH TRANSACTION REPORT

		// Buddy PA
		else if (productType.indexOf("CPP") != -1 || productType.indexOf("TPP") != -1) {

			TransSearchObject transSearchObject_Buddy = new TransSearchObject();
			List<BuddyPAReportExcelVO> buddyReportList = new ArrayList<BuddyPAReportExcelVO>();
			List<BuddyPAReportExcelVO> buddyReportList_B4Pay = new ArrayList<BuddyPAReportExcelVO>();
			List<String> transactionStatusList = new ArrayList<String>();

			String query1 = "n";
			String query2 = "n";

			transSearchObject_Buddy.setProductCode(productType);

			if ("".equalsIgnoreCase(productType) || productType == null) {
				if ("".equalsIgnoreCase(status) || status == null) { // productType empty and transaction status empty
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transSearchObject_Buddy.setTransactionStatus(transactionStatusList);
					query1 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_Buddy.setTransactionStatus(transactionStatusList);
				}
			} else { // if Product is Chosen
				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_Buddy.setTransactionStatus(transactionStatusList);
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_Buddy.setTransactionStatus(transactionStatusList);
				}
			}
			if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)) {
				query1 = "y";
			}
			if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
				query2 = "y";
			}

			if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
				transSearchObject_Buddy.setPolicyNo(PolicyCertificateNo);
				query2 = null;
			}
			if (!"".equalsIgnoreCase(nRIC)) {
				transSearchObject_Buddy.setNricNo(nRIC);
			}
			if (!"".equalsIgnoreCase(receiptNo)) {
				transSearchObject_Buddy.setInvoiceNo(receiptNo);
				query2 = null;
			}

			transSearchObject_Buddy.setDateFrom(dateFrom);
			transSearchObject_Buddy.setDateTo(dateTo);

			// if query 1
			if ("y".equalsIgnoreCase(query1)) {
				buddyReportList = buddyReportExcelMapper.selectTransactionalReportAfterPayment(transSearchObject_Buddy);

			} // End if query1 is Executed

			// if query 2
			if ("y".equalsIgnoreCase(query2)) {
				buddyReportList_B4Pay = buddyReportExcelMapper
						.selectTransactionalReportBeforePayment(transSearchObject_Buddy);
			}
			// if Query before payment and after payment then combine
			// if ("y".equalsIgnoreCase(query1) && "y".equalsIgnoreCase(query2)) {
			buddyReportList.addAll(buddyReportList_B4Pay);
			// }
			Collections.sort(buddyReportList, new Comparator<BuddyPAReportExcelVO>() {
				@Override
				public int compare(BuddyPAReportExcelVO o1, BuddyPAReportExcelVO o2) {
					return o1.getRecord_date().compareTo(o2.getRecord_date());
				}
			});

			Collections.reverse(buddyReportList);

			if (buddyReportList.size() <= 0) {
				model.addAttribute("emptySearch", "No Record Found");
			}
			model.addAttribute("reportResult", buddyReportList);

			// start excel

			Random ran = new Random();
			int randomNo = ran.nextInt(6) + 5;

			DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
			Date date = new Date();

			String file = "EtiqaTransactionReport" + dateFormat.format(date) + randomNo + ".xlsx";

			String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
			// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Etiqa Transaction Report");

			// header fields based on product
			String[] fields = null;

			fields = new String[] { "Id", "DSPQQID", "Quotation Creation Date", "Txn Id", "Txn Start Date",
					"Txn End Date", "Entity", "Name", "PolicyNo", "ProductDesc", "Combo", "Plan", "Id Type",
					"Insured ID", "Gender", "Email", "Address 1", "Address 2", "Address 3", "State", "PostCode",
					"Contact No", "Agent Code", "Agent Name", "Operator Code", "Promo Code", "EffDate", "FPX Ref",
					"EBPG Ref", "M2U Ref", "Payment Mode", "Payment Payable", "Gross Premium", "Discount", "Nett",
					"Tax Type", "Tax Amount", "Comm", "Tax Invoice No", "Txn Status", "Abandonment", "PDPA", "Spouse",
					"Spouse Id Type", "Spouse Id No", "Spouse DOB", "Spouse Gender", "Child 1", "Child 1 Id Type",
					"Child 1 Id No", "Child 1 DOB", "Child 1 Gender", "Child 2", "Child 2 Id Type", "Child 2 Id No",
					"Child 2 DOB", "Child 2 Gender", "Child 3", "Child 3 Id Type", "Child 3 Id No", "Child 3 DOB",
					"Child 3 Gender", "Child 4", "Child 4 Id Type", "Child 4 Id No", "Child 4 DOB", "Child 4 Gender",
					"Child 5", "Child 5 Id Type", "Child 5 Id No", "Child 5 DOB", "Child 5 Gender", "Child 6",
					"Child 6 Id Type", "Child 6 Id No", "Child 6 DOB", "Child 6 Gender", "Child 7", "Child 7 Id Type",
					"Child 7 Id No", "Child 7 DOB", "Child 7 Gender", "Child 8", "Child 8 Id Type", "Child 8 Id No",
					"Child 8 DOB", "Child 8 Gender", "Child 9", "Child 9 Id Type", "Child 9 Id No", "Child 9 DOB",
					"Child 9 Gender", "Child 10", "Child 10 Id Type", "Child 10 Id No", "Child 10 DOB",
					"Child 10 Gender" };

			Row row1 = sheet.createRow(0);

			for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
				Cell cell = row1.createCell(cellIndex);
				cell.setCellValue(fields[cellIndex]);
			}

			String[] fields2 = null;

			int i = 1;

			for (BuddyPAReportExcelVO buddyDetail : buddyReportList) {

				String Abandonment = "";

				String txnStatus = "";
				String quotationCreationDate = "";
				String transactionStartDateTime = "";
				String transactionEndDateTime = "";
				String proEntity = "";
				String paymentChannel = "";
				String coverageStartDate = "";
				String coverageEndDate = "";

				if (null != buddyDetail.getPmnt_gateway_code() && "" != buddyDetail.getPmnt_gateway_code()) {
					if (buddyDetail.getPmnt_gateway_code().indexOf("EBPG") != -1) {

						paymentChannel = "EBPG";
					}

					else if (buddyDetail.getPmnt_gateway_code().indexOf("AMEX") != -1) {

						paymentChannel = "AMEX";
					}

					else if (buddyDetail.getPmnt_gateway_code().indexOf("FPX") != -1) {

						paymentChannel = "FPX";
					}

					else if (buddyDetail.getPmnt_gateway_code().indexOf("Maybank2U") != -1) {

						paymentChannel = "M2U";
					}

					else if (buddyDetail.getPmnt_gateway_code().indexOf("CREDIT CARD") != -1) {

						paymentChannel = "MPAY";
					}

				}
				if (buddyDetail.getProduct_code().equals("CPP")) {

					proEntity = "EGIB";
				}

				else if (buddyDetail.getProduct_code().equals("TPP"))

				{

					proEntity = "EGTB";

				}

				if (buddyDetail.getPmnt_status().equals("S")) {

					txnStatus = "Successful";

				} else if (buddyDetail.getPmnt_status().equals("F")) {

					txnStatus = "Fail";

				}

				else if (buddyDetail.getPmnt_status().equals("AP")) {

					txnStatus = "Attempt Payment";

				}

				else if (buddyDetail.getPmnt_status().equals("O")) {

					Abandonment = buddyDetail.getLast_page();
					txnStatus = "Incomplete";

				}

				else if (buddyDetail.getPmnt_status().equals("C")) {

					txnStatus = "Cancelled";

				}

				else if (buddyDetail.getPmnt_status().equals("R")) {

					txnStatus = "Rejection : " + buddyDetail.getReason();

				}

				SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

				if (null != buddyDetail.getRecord_date()) {
					quotationCreationDate = ft.format(buddyDetail.getRecord_date());
				}

				if (buddyDetail.getPmnt_status().equals("S") || buddyDetail.getPmnt_status().equals("F")
						|| buddyDetail.getPmnt_status().equals("AP")) {

					transactionStartDateTime = buddyDetail.getTransaction_datetime();

				}

				if (!buddyDetail.getPolicy_effective_timestamp().equals(" ")) {

					String valueFromDB = buddyDetail.getPolicy_effective_timestamp().trim();
					logger.info(valueFromDB + "valueFromDB");
					// String startDateWithoutTime = "";
					// try {
					// SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");
					// Date d1 = null;
					// d1 = sdf.parse(valueFromDB);
					// SimpleDateFormat sdfto = new SimpleDateFormat("dd/MM/yyyy");
					// startDateWithoutTime = sdfto.format(d1);
					// } catch (ParseException e) {
					// // TODO Auto-generated catch block
					// e.printStackTrace();
					// }
					// //logger.info("sdf.format(d1) " + startDateWithoutTime);
					//
					// coverageStartDate= startDateWithoutTime;
					coverageStartDate = valueFromDB;

				}

				if (!buddyDetail.getPolicy_expiry_timestamp().equals(" ")) {

					String valueFromDB = buddyDetail.getPolicy_expiry_timestamp().trim();
					logger.info(valueFromDB + "valueFromDB");
					// String endDateWithoutTime = "";
					// try {
					// SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");
					// Date d1 = null;
					// d1 = sdf.parse(valueFromDB);
					// SimpleDateFormat sdfto = new SimpleDateFormat("dd/MM/yyyy");
					// endDateWithoutTime = sdfto.format(d1);
					// } catch (ParseException e) {
					// // TODO Auto-generated catch block
					// e.printStackTrace();
					// }
					// //logger.info("sdf.format(d1) " + startDateWithoutTime);
					//
					// coverageEndDate= endDateWithoutTime;
					coverageEndDate = valueFromDB;

				}

				int countForallChildRecords = 0;

				if (null != buddyDetail.getChild_name()) {

					logger.info(buddyDetail.getChild_name() + "buddyDetail.getChild_name()");
					if (buddyDetail.getChild_name().indexOf("|") != -1) {

						String[] childName = buddyDetail.getChild_name().split("\\|");
						;

						int childCount = childName.length;
						countForallChildRecords = childCount;
						for (int j = 0; j < childCount; j++) {
							if (j == 0) {
								buddyDetail.setChild_name_1(childName[j]);
							}
							if (j == 1) {
								buddyDetail.setChild_name_2(childName[j]);
							}
							if (j == 2) {
								buddyDetail.setChild_name_3(childName[j]);
							}
							if (j == 3) {
								buddyDetail.setChild_name_4(childName[j]);
							}
							if (j == 4) {
								buddyDetail.setChild_name_5(childName[j]);
							}

							if (j == 5) {
								buddyDetail.setChild_name_6(childName[j]);
							}

							if (j == 6) {
								buddyDetail.setChild_name_7(childName[j]);
							}

							if (j == 7) {
								buddyDetail.setChild_name_8(childName[j]);
							}

							if (j == 8) {
								buddyDetail.setChild_name_9(childName[j]);
							}

							if (j == 9) {
								buddyDetail.setChild_name_10(childName[j]);
							}

						}

					} else {
						countForallChildRecords = 1;
						logger.info(buddyDetail.getChild_name() + " Else buddyDetail.getChild_name()");
						buddyDetail.setChild_name_1(buddyDetail.getChild_name());

					}

				}

				logger.info(countForallChildRecords + " countForallChildRecords");
				if (null != buddyDetail.getChild_ID_type()) {

					logger.info(buddyDetail.getChild_ID_type() + "buddyDetail.getChild_ID_type()");
					if (buddyDetail.getChild_ID_type().indexOf("|") != -1) {

						String[] childIDType = buddyDetail.getChild_ID_type().split("\\|");

						int childCount = childIDType.length;

						for (int j = 0; j < countForallChildRecords; j++) {
							if (j == 0) {
								buddyDetail.setChild_ID_type_1(childIDType[j]);
							}
							if (j == 1) {
								buddyDetail.setChild_ID_type_2(childIDType[j]);
							}
							if (j == 2) {
								buddyDetail.setChild_ID_type_3(childIDType[j]);
							}
							if (j == 3) {
								buddyDetail.setChild_ID_type_4(childIDType[j]);
							}
							if (j == 4) {
								buddyDetail.setChild_ID_type_5(childIDType[j]);
							}

							if (j == 5) {
								buddyDetail.setChild_ID_type_6(childIDType[j]);
							}

							if (j == 6) {
								buddyDetail.setChild_ID_type_7(childIDType[j]);
							}

							if (j == 7) {
								buddyDetail.setChild_ID_type_8(childIDType[j]);
							}

							if (j == 8) {
								buddyDetail.setChild_ID_type_9(childIDType[j]);
							}

							if (j == 9) {
								buddyDetail.setChild_ID_type_10(childIDType[j]);
							}

						}

					} else {
						logger.info(buddyDetail.getChild_ID_type() + " Else buddyDetail.getChild_ID_type()");

						if (countForallChildRecords == 1) {
							buddyDetail.setChild_ID_type_1(buddyDetail.getChild_ID_type());
						} else {
							for (int j = 0; j < countForallChildRecords; j++) {
								if (j == 0) {
									buddyDetail.setChild_ID_type_1(buddyDetail.getChild_ID_type());
								}
								if (j == 1) {
									buddyDetail.setChild_ID_type_2("");
								}
								if (j == 2) {
									buddyDetail.setChild_ID_type_3("");
								}
								if (j == 3) {
									buddyDetail.setChild_ID_type_4("");
								}
								if (j == 4) {
									buddyDetail.setChild_ID_type_5("");
								}

								if (j == 5) {
									buddyDetail.setChild_ID_type_6("");
								}

								if (j == 6) {
									buddyDetail.setChild_ID_type_7("");
								}

								if (j == 7) {
									buddyDetail.setChild_ID_type_8("");
								}

								if (j == 8) {
									buddyDetail.setChild_ID_type_9("");
								}

								if (j == 9) {
									buddyDetail.setChild_ID_type_10("");
								}

							}

						}
					}

				}

				if (null != buddyDetail.getChild_ID_number()) {

					logger.info(buddyDetail.getChild_ID_number() + " buddyDetail.getChild_ID_number");
					if (buddyDetail.getChild_ID_number().indexOf("|") != -1) {

						String[] childIDNumber = buddyDetail.getChild_ID_number().split("\\|");

						int childCount = childIDNumber.length;

						for (int j = 0; j < countForallChildRecords; j++) {
							if (j == 0) {
								buddyDetail.setChild_ID_number_1(childIDNumber[j]);
							}
							if (j == 1) {
								buddyDetail.setChild_ID_number_2(childIDNumber[j]);
							}
							if (j == 2) {
								buddyDetail.setChild_ID_number_3(childIDNumber[j]);
							}
							if (j == 3) {
								buddyDetail.setChild_ID_number_4(childIDNumber[j]);
							}
							if (j == 4) {
								buddyDetail.setChild_ID_number_5(childIDNumber[j]);
							}

							if (j == 5) {
								buddyDetail.setChild_ID_number_6(childIDNumber[j]);
							}

							if (j == 6) {
								buddyDetail.setChild_ID_number_7(childIDNumber[j]);
							}

							if (j == 7) {
								buddyDetail.setChild_ID_number_8(childIDNumber[j]);
							}

							if (j == 8) {
								buddyDetail.setChild_ID_number_9(childIDNumber[j]);
							}

							if (j == 9) {
								buddyDetail.setChild_ID_number_10(childIDNumber[j]);
							}

						}

					} else {
						logger.info(buddyDetail.getChild_ID_number() + "Else  buddyDetail.getChild_ID_number");
						if (countForallChildRecords == 1) {
							buddyDetail.setChild_ID_number_1(buddyDetail.getChild_ID_number());
						} else {
							for (int j = 0; j < countForallChildRecords; j++) {
								if (j == 0) {
									buddyDetail.setChild_ID_number_1(buddyDetail.getChild_ID_number());
								}
								if (j == 1) {
									buddyDetail.setChild_ID_number_2("");
								}
								if (j == 2) {
									buddyDetail.setChild_ID_number_3("");
								}
								if (j == 3) {
									buddyDetail.setChild_ID_number_4("");
								}
								if (j == 4) {
									buddyDetail.setChild_ID_number_5("");
								}

								if (j == 5) {
									buddyDetail.setChild_ID_number_6("");
								}

								if (j == 6) {
									buddyDetail.setChild_ID_number_7("");
								}

								if (j == 7) {
									buddyDetail.setChild_ID_number_8("");
								}

								if (j == 8) {
									buddyDetail.setChild_ID_number_9("");
								}

								if (j == 9) {
									buddyDetail.setChild_ID_number_10("");
								}

							}

						}
					}

				}

				if (null != buddyDetail.getChild_DOB()) {

					if (buddyDetail.getChild_DOB().indexOf("|") != -1) {
						logger.info(buddyDetail.getChild_DOB() + "buddyDetail.getChild_DOB()");
						String[] childDOB = buddyDetail.getChild_DOB().split("\\|");

						int childCount = childDOB.length;

						for (int j = 0; j < countForallChildRecords; j++) {
							if (j == 0) {
								buddyDetail.setChild_DOB_1(childDOB[j]);
							}
							if (j == 1) {
								buddyDetail.setChild_DOB_2(childDOB[j]);
							}
							if (j == 2) {
								buddyDetail.setChild_DOB_3(childDOB[j]);
							}
							if (j == 3) {
								buddyDetail.setChild_DOB_4(childDOB[j]);
							}
							if (j == 4) {
								buddyDetail.setChild_DOB_5(childDOB[j]);
							}

							if (j == 5) {
								buddyDetail.setChild_DOB_6(childDOB[j]);
							}

							if (j == 6) {
								buddyDetail.setChild_DOB_7(childDOB[j]);
							}

							if (j == 7) {
								buddyDetail.setChild_DOB_8(childDOB[j]);
							}

							if (j == 8) {
								buddyDetail.setChild_DOB_9(childDOB[j]);
							}

							if (j == 9) {
								buddyDetail.setChild_DOB_10(childDOB[j]);
							}

						}

					} else {
						logger.info(buddyDetail.getChild_DOB() + "buddyDetail.getChild_DOB()");
						if (countForallChildRecords == 1) {
							buddyDetail.setChild_DOB_1(buddyDetail.getChild_DOB());
						} else {
							for (int j = 0; j < countForallChildRecords; j++) {

								if (j == 0) {
									buddyDetail.setChild_DOB_1(buddyDetail.getChild_DOB());
								}
								if (j == 1) {
									buddyDetail.setChild_DOB_2("");
								}
								if (j == 2) {
									buddyDetail.setChild_DOB_3("");
								}
								if (j == 3) {
									buddyDetail.setChild_DOB_4("");
								}
								if (j == 4) {
									buddyDetail.setChild_DOB_5("");
								}

								if (j == 5) {
									buddyDetail.setChild_DOB_6("");
								}

								if (j == 6) {
									buddyDetail.setChild_DOB_7("");
								}

								if (j == 7) {
									buddyDetail.setChild_DOB_8("");
								}

								if (j == 8) {
									buddyDetail.setChild_DOB_9("");
								}

								if (j == 9) {
									buddyDetail.setChild_DOB_10("");
								}

							}

						}

					}

				}

				if (null != buddyDetail.getChild_gender()) {

					if (buddyDetail.getChild_gender().indexOf("|") != -1) {

						String[] childGender = buddyDetail.getChild_gender().split("\\|");

						int childCount = childGender.length;

						for (int j = 0; j < countForallChildRecords; j++) {

							if (j == 0) {
								buddyDetail.setChild_gender_1(childGender[j]);
							}
							if (j == 1) {
								buddyDetail.setChild_gender_2(childGender[j]);
							}
							if (j == 2) {
								buddyDetail.setChild_gender_3(childGender[j]);
							}
							if (j == 3) {
								buddyDetail.setChild_gender_4(childGender[j]);
							}
							if (j == 4) {
								buddyDetail.setChild_gender_5(childGender[j]);
							}

							if (j == 5) {
								buddyDetail.setChild_gender_6(childGender[j]);
							}

							if (j == 6) {
								buddyDetail.setChild_gender_7(childGender[j]);
							}

							if (j == 7) {
								buddyDetail.setChild_gender_8(childGender[j]);
							}

							if (j == 8) {
								buddyDetail.setChild_gender_9(childGender[j]);
							}

							if (j == 9) {
								buddyDetail.setChild_gender_10(childGender[j]);
							}

						}

					}

					else {

						if (countForallChildRecords == 1) {
							buddyDetail.setChild_gender_1(buddyDetail.getChild_gender());
						} else {
							for (int j = 0; j < countForallChildRecords; j++) {

								if (j == 0) {
									buddyDetail.setChild_gender_1(buddyDetail.getChild_gender());
								}
								if (j == 1) {
									buddyDetail.setChild_gender_2("");
								}
								if (j == 2) {
									buddyDetail.setChild_gender_3("");
								}
								if (j == 3) {
									buddyDetail.setChild_gender_4("");
								}
								if (j == 4) {
									buddyDetail.setChild_gender_5("");
								}

								if (j == 5) {
									buddyDetail.setChild_gender_6("");
								}

								if (j == 6) {
									buddyDetail.setChild_gender_7("");
								}

								if (j == 7) {
									buddyDetail.setChild_gender_8("");
								}

								if (j == 8) {
									buddyDetail.setChild_gender_9("");
								}

								if (j == 9) {
									buddyDetail.setChild_gender_10("");
								}
							}
						}
					}
				}

				fields2 = new String[] { Integer.toString(i), buddyDetail.getDSPQQID(), quotationCreationDate,
						buddyDetail.getPaymentTrxID(), transactionStartDateTime, transactionEndDateTime, proEntity,
						buddyDetail.getCustomer_name(), buddyDetail.getPolicy_number(), buddyDetail.getProduct_code(),
						buddyDetail.getBuddy_combo(), buddyDetail.getPlan_name(), buddyDetail.getCustomer_ID_type(),
						buddyDetail.getCustomer_nric_id(), buddyDetail.getCustomer_gender(),
						buddyDetail.getCustomer_email(), buddyDetail.getCustomer_address_1(),
						buddyDetail.getCustomer_address_2(), buddyDetail.getCustomer_address_3(),
						buddyDetail.getCustomer_state(), buddyDetail.getCustomer_postcode(),
						buddyDetail.getCustomer_mobile_no(), buddyDetail.getAgent_code(), buddyDetail.getAgent_name(),
						buddyDetail.getOperator_code(), buddyDetail.getDiscount_code(),
						coverageStartDate + "-" + coverageEndDate, buddyDetail.getFpx_fpxtxnid(),
						buddyDetail.getTransaction_id(), buddyDetail.getBankrefno(), paymentChannel,
						buddyDetail.getAmount(), buddyDetail.getGrosspremium(), buddyDetail.getDiscount(),
						buddyDetail.getNett_premium(), buddyDetail.getTaxType(), buddyDetail.getGst(),
						buddyDetail.getCommission(), buddyDetail.getInvoice_no(), txnStatus, Abandonment,
						buddyDetail.getPDPA(), buddyDetail.getSpouse_name(), buddyDetail.getSpouse_ID_type(),
						buddyDetail.getSpouse_ID_number(), buddyDetail.getSpouse_DOB(), buddyDetail.getSpouse_gender(),
						buddyDetail.getChild_name_1(), buddyDetail.getChild_ID_type_1(),
						buddyDetail.getChild_ID_number_1(), buddyDetail.getChild_DOB_1(),
						buddyDetail.getChild_gender_1(), buddyDetail.getChild_name_2(),
						buddyDetail.getChild_ID_type_2(), buddyDetail.getChild_ID_number_2(),
						buddyDetail.getChild_DOB_2(), buddyDetail.getChild_gender_2(), buddyDetail.getChild_name_3(),
						buddyDetail.getChild_ID_type_3(), buddyDetail.getChild_ID_number_3(),
						buddyDetail.getChild_DOB_3(), buddyDetail.getChild_gender_3(), buddyDetail.getChild_name_4(),
						buddyDetail.getChild_ID_type_4(), buddyDetail.getChild_ID_number_4(),
						buddyDetail.getChild_DOB_4(), buddyDetail.getChild_gender_4(), buddyDetail.getChild_name_5(),
						buddyDetail.getChild_ID_type_5(), buddyDetail.getChild_ID_number_5(),
						buddyDetail.getChild_DOB_5(), buddyDetail.getChild_gender_5(), buddyDetail.getChild_name_6(),
						buddyDetail.getChild_ID_type_6(), buddyDetail.getChild_ID_number_6(),
						buddyDetail.getChild_DOB_6(), buddyDetail.getChild_gender_6(), buddyDetail.getChild_name_7(),
						buddyDetail.getChild_ID_type_7(), buddyDetail.getChild_ID_number_7(),
						buddyDetail.getChild_DOB_7(), buddyDetail.getChild_gender_7(), buddyDetail.getChild_name_8(),
						buddyDetail.getChild_ID_type_8(), buddyDetail.getChild_ID_number_8(),
						buddyDetail.getChild_DOB_8(), buddyDetail.getChild_gender_8(), buddyDetail.getChild_name_9(),
						buddyDetail.getChild_ID_type_9(), buddyDetail.getChild_ID_number_9(),
						buddyDetail.getChild_DOB_9(), buddyDetail.getChild_gender_9(), buddyDetail.getChild_name_10(),
						buddyDetail.getChild_ID_type_10(), buddyDetail.getChild_ID_number_10(),
						buddyDetail.getChild_DOB_10(), buddyDetail.getChild_gender_10() };

				Row row2 = sheet.createRow(i);

				for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
					Cell cell = row2.createCell(cellIndex);
					cell.setCellValue(fields2[cellIndex]);
				}

				i++;

			}

			// logger.info("txnStatus "+txn.getStatus());

			try {
				FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
				workbook.write(outputStream);
				workbook.close();

				File fileToDownload = new File(relativeWebPath);

				FileInputStream inputStream = new FileInputStream(fileToDownload);
				response.setContentType(servletContext.getMimeType(relativeWebPath));
				response.setContentLength((int) fileToDownload.length());

				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
				response.setHeader(headerKey, headerValue);

				// get output stream of the response
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				// write bytes read from the input stream into the output stream
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}

				outStream.flush();
				outStream.close();
				inputStream.close();

				if (fileToDownload.exists()) {

					boolean success = new File(relativeWebPath).delete();
					if (success) {
						logger.info("The file " + file + " has been successfully deleted");
					}
				}
			} catch (FileNotFoundException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();
				e.printStackTrace();
			} catch (IOException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			}

		}
		// END : Buddy PA
		// START : Travel Ezy
		else if (productType.indexOf("BPT") != -1 || productType.indexOf("TZT") != -1) {
			TransSearchObject transSearchObject_TravEz = new TransSearchObject();
			List<TravelEzyReportExcelVO> travEzReportList = new ArrayList<TravelEzyReportExcelVO>();
			List<TravelEzyReportExcelVO> travEzReportList_B4Pay = new ArrayList<TravelEzyReportExcelVO>();
			List<String> transactionStatusList = new ArrayList<String>();

			String query1 = "n";
			String query2 = "n";

			transSearchObject_TravEz.setProductCode(productType);

			if ("".equalsIgnoreCase(productType) || productType == null) {
				if ("".equalsIgnoreCase(status) || status == null) { // productType empty and transaction status empty
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transSearchObject_TravEz.setTransactionStatus(transactionStatusList);
					query1 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_TravEz.setTransactionStatus(transactionStatusList);
				}
			} else { // if Product is Chosen
				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_TravEz.setTransactionStatus(transactionStatusList);
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_TravEz.setTransactionStatus(transactionStatusList);
				}
			}
			if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)) {
				query1 = "y";
			}
			if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
				query2 = "y";
			}

			if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
				transSearchObject_TravEz.setPolicyNo(PolicyCertificateNo);
				query2 = null;
			}
			if (!"".equalsIgnoreCase(nRIC)) {
				transSearchObject_TravEz.setNricNo(nRIC);
			}
			if (!"".equalsIgnoreCase(receiptNo)) {
				transSearchObject_TravEz.setInvoiceNo(receiptNo);
				query2 = null;
			}

			transSearchObject_TravEz.setDateFrom(dateFrom);
			transSearchObject_TravEz.setDateTo(dateTo);

			// if query 1
			if ("y".equalsIgnoreCase(query1)) {
				travEzReportList = travelEzyReportExcelMapper
						.selectTravelEzyTransactionalReportAfterPayment(transSearchObject_TravEz);

			} // End if query1 is Executed

			// if query 2
			if ("y".equalsIgnoreCase(query2)) {
				travEzReportList_B4Pay = travelEzyReportExcelMapper
						.selectTravelEzyTransactionalReportBeforePayment(transSearchObject_TravEz);
			}
			travEzReportList.addAll(travEzReportList_B4Pay);
			Collections.sort(travEzReportList, new Comparator<TravelEzyReportExcelVO>() {
				@Override
				public int compare(TravelEzyReportExcelVO o1, TravelEzyReportExcelVO o2) {
					return o1.getRecord_date().compareTo(o2.getRecord_date());
				}
			});

			Collections.reverse(travEzReportList);

			if (travEzReportList.size() <= 0) {
				model.addAttribute("emptySearch", "No Record Found");
			}
			model.addAttribute("reportResult", travEzReportList);

			// start excel

			Random ran = new Random();
			int randomNo = ran.nextInt(6) + 5;

			DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
			Date date = new Date();

			String file = "EtiqaTransactionReport" + dateFormat.format(date) + randomNo + ".xlsx";

			String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;

			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Etiqa Transaction Report");

			// header fields based on product
			ArrayList<String> fields = new ArrayList<String>();

			fields.add("Id");
			fields.add("DSPQQID");
			fields.add("Quotation Creation Date");
			fields.add("Txn Id");
			fields.add("Txn Start Date");
			fields.add("Txn End Date");
			fields.add("Entity");
			fields.add("Name");
			fields.add("PolicyNo");
			fields.add("ProductDesc");
			fields.add("Id Type");
			fields.add("Insured ID");
			fields.add("Gender");
			fields.add("Email");
			fields.add("Address 1");
			fields.add("Address 2");
			fields.add("Address 3");
			fields.add("State");
			fields.add("PostCode");
			fields.add("Contact No");
			fields.add("Agent Code");
			fields.add("Agent Name");
			fields.add("Travel Type");
			fields.add("Depart Date");
			fields.add("Return Date");
			fields.add("FPX Ref");
			fields.add("EBPG Ref");
			fields.add("M2U Ref");
			fields.add("Payment Mode");
			fields.add("Payment Payable");
			fields.add("Gross Premium");
			fields.add("Discount");
			fields.add("Tax Type");
			fields.add("Tax Amount");
			fields.add("Comm");
			fields.add("Nett");
			fields.add("Tax Invoice No");
			fields.add("Txn Status");
			fields.add("Abandonment");
			fields.add("PDPA");
			fields.add("Number of Companion");
			for (int j = 0; j < 200; j++) {
				fields.add("Companion " + (j + 1));
				fields.add("ID No " + (j + 1));
			}

			Row row1 = sheet.createRow(0);

			for (int cellIndex = 0; cellIndex < fields.size(); cellIndex++) {
				Cell cell = row1.createCell(cellIndex);
				cell.setCellValue(fields.get(cellIndex));
			}

			int i = 1;

			for (TravelEzyReportExcelVO TravEzDetail : travEzReportList) {

				String txnStatus = "";
				String quotationCreationDate = "";
				String transactionStartDateTime = "";
				String transactionEndDateTime = "";
				String proEntity = "";
				String paymentChannel = "";

				String Abandonment = "";

				int numOfCompanions = TravEzDetail.getNumber_of_companion();

				if (null != TravEzDetail.getPmnt_gateway_code() && "" != TravEzDetail.getPmnt_gateway_code()) {
					if (TravEzDetail.getPmnt_gateway_code().indexOf("EBPG") != -1) {

						paymentChannel = "EBPG";
					}

					else if (TravEzDetail.getPmnt_gateway_code().indexOf("AMEX") != -1) {

						paymentChannel = "AMEX";
					}

					else if (TravEzDetail.getPmnt_gateway_code().indexOf("FPX") != -1) {

						paymentChannel = "FPX";
					}

					else if (TravEzDetail.getPmnt_gateway_code().indexOf("Maybank2U") != -1) {

						paymentChannel = "M2U";
					}

					else if (TravEzDetail.getPmnt_gateway_code().indexOf("CREDIT CARD") != -1) {

						paymentChannel = "MPAY";
					}

				}
				if (TravEzDetail.getProduct_code().equals("BPT")) {

					proEntity = "EGIB";
				}

				else if (TravEzDetail.getProduct_code().equals("TZT"))

				{

					proEntity = "EGTB";

				}
				if (TravEzDetail.getPmnt_status().equals("S")) {
					txnStatus = "Successful";
				} else if (TravEzDetail.getPmnt_status().equals("F")) {
					txnStatus = "Fail";
				} else if (TravEzDetail.getPmnt_status().equals("AP")) {
					txnStatus = "Attempt Payment";
				} else if (TravEzDetail.getPmnt_status().equals("O")) {
					Abandonment = TravEzDetail.getLast_page();
					txnStatus = "Incomplete";
				} else if (TravEzDetail.getPmnt_status().equals("C")) {
					txnStatus = "Cancelled";
				} else if (TravEzDetail.getPmnt_status().equals("R")) {
					txnStatus = "Rejection : ";
				}

				SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

				if (null != TravEzDetail.getRecord_date()) {
					quotationCreationDate = ft.format(TravEzDetail.getRecord_date());
				}

				if (TravEzDetail.getPmnt_status().equals("S") || TravEzDetail.getPmnt_status().equals("F")
						|| TravEzDetail.getPmnt_status().equals("AP")) {

					transactionStartDateTime = TravEzDetail.getTransaction_datetime();
				}

				ArrayList<String> companionName = new ArrayList<String>();
				ArrayList<String> idNo = new ArrayList<String>();

				if (null != TravEzDetail.getCompanion_name()) {

					if (numOfCompanions > 1) {

						String[] tmpName = TravEzDetail.getCompanion_name().split("\\|");
						logger.info(tmpName + " tmpName");
						for (int j = 0; j < numOfCompanions; j++) {
							if (tmpName.length > j) {
								companionName.add(tmpName[j]);
							} else {
								companionName.add("");
							}
						}

					} else {
						companionName.add(TravEzDetail.getCompanion_name());

					}

				}

				logger.info(TravEzDetail.getId_no() + " TravEzDetail.getId_no1");
				if (null != TravEzDetail.getId_no()) {

					if (numOfCompanions > 1) {

						String[] tmpIdNo = TravEzDetail.getId_no().split("\\|");
						logger.info(tmpIdNo + " tmpIdNo");
						for (int j = 0; j < numOfCompanions; j++) {
							if (tmpIdNo.length > j) {
								idNo.add(tmpIdNo[j]);
							} else {
								idNo.add("");
							}
						}
					} else {
						idNo.add(TravEzDetail.getId_no());

					}
				}

				ArrayList<String> tmpDetail = new ArrayList<String>();
				tmpDetail.add(Integer.toString(i));
				tmpDetail.add(TravEzDetail.getDSPQQID());
				tmpDetail.add(quotationCreationDate);
				tmpDetail.add(TravEzDetail.getPaymentTrxID());
				tmpDetail.add(transactionStartDateTime);
				tmpDetail.add(transactionEndDateTime);
				tmpDetail.add(proEntity);
				tmpDetail.add(TravEzDetail.getCustomer_name());
				tmpDetail.add(TravEzDetail.getPolicy_number());
				tmpDetail.add(TravEzDetail.getProduct_code());
				tmpDetail.add(TravEzDetail.getCustomer_ID_type());
				tmpDetail.add(TravEzDetail.getCustomer_nric_id());
				tmpDetail.add(TravEzDetail.getCustomer_gender());
				tmpDetail.add(TravEzDetail.getCustomer_email());
				tmpDetail.add(TravEzDetail.getCustomer_address_1());
				tmpDetail.add(TravEzDetail.getCustomer_address_2());
				tmpDetail.add(TravEzDetail.getCustomer_address_3());
				tmpDetail.add(TravEzDetail.getCustomer_state());
				tmpDetail.add(TravEzDetail.getCustomer_postcode());
				tmpDetail.add(TravEzDetail.getCustomer_mobile_no());
				tmpDetail.add(TravEzDetail.getAgent_code());
				tmpDetail.add(TravEzDetail.getAgent_name());
				tmpDetail.add(TravEzDetail.getTrip_type());
				tmpDetail.add(TravEzDetail.getDepartDate());
				tmpDetail.add(TravEzDetail.getReturnDate());
				tmpDetail.add(TravEzDetail.getFpx_fpxtxnid());
				tmpDetail.add(TravEzDetail.getTransaction_id());
				tmpDetail.add(TravEzDetail.getBankrefno());
				tmpDetail.add(paymentChannel);
				tmpDetail.add(TravEzDetail.getAmount());
				tmpDetail.add(TravEzDetail.getGrosspremium_final());
				tmpDetail.add(TravEzDetail.getDiscount());
				tmpDetail.add(TravEzDetail.getTaxType());
				tmpDetail.add(TravEzDetail.getGst());
				tmpDetail.add(TravEzDetail.getCommission());
				tmpDetail.add(TravEzDetail.getNettPremium());
				tmpDetail.add(TravEzDetail.getInvoice_no());
				tmpDetail.add(txnStatus);
				tmpDetail.add(Abandonment);
				tmpDetail.add(TravEzDetail.getPDPA());
				tmpDetail.add(String.valueOf(numOfCompanions));

				logger.info("numOfCompanions " + numOfCompanions);
				for (int j = 0; j < numOfCompanions; j++) {
					if (!companionName.isEmpty() && companionName.size() > j) {
						logger.info(companionName.get(j) + " -------3");
						tmpDetail.add(companionName.get(j));
					}
					if (!idNo.isEmpty() && idNo.size() > j) {
						tmpDetail.add(idNo.get(j));
						logger.info(idNo.get(j) + " -------3");
					}
				}

				Row row2 = sheet.createRow(i);

				for (int cellIndex = 0; cellIndex < tmpDetail.size(); cellIndex++) {
					Cell cell = row2.createCell(cellIndex);
					cell.setCellValue(tmpDetail.get(cellIndex));
				}
				i++;
			}

			try {
				FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
				workbook.write(outputStream);
				workbook.close();

				File fileToDownload = new File(relativeWebPath);

				FileInputStream inputStream = new FileInputStream(fileToDownload);
				response.setContentType(servletContext.getMimeType(relativeWebPath));
				response.setContentLength((int) fileToDownload.length());

				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
				response.setHeader(headerKey, headerValue);

				// get output stream of the response
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				// write bytes read from the input stream into the output stream
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}

				outStream.flush();
				outStream.close();
				inputStream.close();

				if (fileToDownload.exists()) {

					boolean success = new File(relativeWebPath).delete();
					if (success) {
						logger.info("The file " + file + " has been successfully deleted");
					}

				}

			} catch (FileNotFoundException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			} catch (IOException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			}

		}
		// END : Travel Ezy
		// START : TripCare
		else if (productType.indexOf("TCI") != -1 || productType.indexOf("TCT") != -1) {
			TransSearchObject transSearchObject_TC = new TransSearchObject();
			List<TCReportExcelVO> tcReportList = new ArrayList<TCReportExcelVO>();
			List<TCReportExcelVO> tcReportList_B4Pay = new ArrayList<TCReportExcelVO>();
			List<String> transactionStatusList = new ArrayList<String>();

			String query1 = "n";
			String query2 = "n";

			transSearchObject_TC.setProductCode(productType);

			if ("".equalsIgnoreCase(productType) || productType == null) {
				if ("".equalsIgnoreCase(status) || status == null) { // productType empty and transaction status empty
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transSearchObject_TC.setTransactionStatus(transactionStatusList);
					query1 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_TC.setTransactionStatus(transactionStatusList);
				}
			} else { // if Product is Chosen
				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_TC.setTransactionStatus(transactionStatusList);
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_TC.setTransactionStatus(transactionStatusList);
				}
			}
			if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)) {
				query1 = "y";
			}
			if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
				query2 = "y";
			}

			if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
				transSearchObject_TC.setPolicyNo(PolicyCertificateNo);
				query2 = null;
			}
			if (!"".equalsIgnoreCase(nRIC)) {
				transSearchObject_TC.setNricNo(nRIC);
			}
			if (!"".equalsIgnoreCase(receiptNo)) {
				transSearchObject_TC.setInvoiceNo(receiptNo);
				query2 = null;
			}

			transSearchObject_TC.setDateFrom(dateFrom);
			transSearchObject_TC.setDateTo(dateTo);

			// if query 1
			if ("y".equalsIgnoreCase(query1)) {
				tcReportList = tcReportExcelMapper.selectTCTransactionalReportAfterPayment(transSearchObject_TC);

			} // End if query1 is Executed

			// if query 2
			if ("y".equalsIgnoreCase(query2)) {
				tcReportList_B4Pay = tcReportExcelMapper.selectTCTransactionalReportBeforePayment(transSearchObject_TC);
			}
			tcReportList.addAll(tcReportList_B4Pay);
			Collections.sort(tcReportList, new Comparator<TCReportExcelVO>() {
				@Override
				public int compare(TCReportExcelVO o1, TCReportExcelVO o2) {
					return o1.getRecord_date().compareTo(o2.getRecord_date());
				}
			});

			Collections.reverse(tcReportList);

			if (tcReportList.size() <= 0) {
				model.addAttribute("emptySearch", "No Record Found");
			}
			model.addAttribute("reportResult", tcReportList);

			// start excel

			Random ran = new Random();
			int randomNo = ran.nextInt(6) + 5;

			DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
			Date date = new Date();

			String file = "EtiqaTransactionReport" + dateFormat.format(date) + randomNo + ".xlsx";

			String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;

			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Etiqa Transaction Report");

			// header fields based on product
			List<String> fields = new ArrayList<String>();

			fields.add("Id");
			fields.add("DSPQQID");
			fields.add("Quotation Creation Date");
			fields.add("Txn Id");
			fields.add("Txn Start Date");
			fields.add("Txn End Date");
			fields.add("Entity");
			fields.add("Name");
			fields.add("PolicyNo");
			fields.add("ProductDesc");
			fields.add("Plan");
			fields.add("Scheme");
			fields.add("Id Type");
			fields.add("Insured ID");
			fields.add("Gender");
			fields.add("Email");
			fields.add("Address 1");
			fields.add("Address 2");
			fields.add("Address 3");
			fields.add("State");
			fields.add("PostCode");
			fields.add("Contact No");
			fields.add("Agent Code");
			fields.add("Agent Name");
			fields.add("Operator Code");
			fields.add("Promo Code");
			fields.add("EffDate");
			fields.add("Travel Type");
			fields.add("Area Code");
			fields.add("Adventurous Activities");
			fields.add("FPX Ref");
			fields.add("EBPG Ref");
			fields.add("M2U Ref");
			fields.add("Payment Mode");
			fields.add("Payment Payable");
			fields.add("Gross Premium");
			fields.add("Discount");
			fields.add("Tax Type");
			fields.add("Tax Amount");
			fields.add("Comm");
			fields.add("Nett");
			fields.add("Tax Invoice No");
			fields.add("Txn Status");
			fields.add("Abandonment");
			fields.add("PDPA");
			fields.add("Spouse");
			fields.add("Spouse Email");
			fields.add("Spouse Id Type");
			fields.add("Spouse Id No");
			fields.add("Spouse DOB");
			fields.add("Spouse Gender");

			fields.add("Number of Children");
			for (int j = 0; j < 20; j++) {
				fields.add("Child " + (j + 1));
				fields.add("ID Type " + (j + 1));
				fields.add("ID No " + (j + 1));
				fields.add("DOB " + (j + 1));
				fields.add("Gender " + (j + 1));
			}
			fields.add("Mode");
			fields.add("PF Number");
			Row row1 = sheet.createRow(0);

			for (int cellIndex = 0; cellIndex < fields.size(); cellIndex++) {
				Cell cell = row1.createCell(cellIndex);
				cell.setCellValue(fields.get(cellIndex));
			}

			int i = 1;

			for (TCReportExcelVO TCDetail : tcReportList) {

				String Abandonment = "";

				String txnStatus = "";
				String quotationCreationDate = "";
				String transactionStartDateTime = "";
				String transactionEndDateTime = "";
				String proEntity = "";
				String paymentChannel = "";

				if (null != TCDetail.getPmnt_gateway_code() && "" != TCDetail.getPmnt_gateway_code()) {
					if (TCDetail.getPmnt_gateway_code().indexOf("EBPG") != -1) {
						paymentChannel = "EBPG";
					} else if (TCDetail.getPmnt_gateway_code().indexOf("AMEX") != -1) {
						paymentChannel = "AMEX";
					} else if (TCDetail.getPmnt_gateway_code().indexOf("FPX") != -1) {
						paymentChannel = "FPX";
					} else if (TCDetail.getPmnt_gateway_code().indexOf("Maybank2U") != -1) {
						paymentChannel = "M2U";
					} else if (TCDetail.getPmnt_gateway_code().indexOf("CREDIT CARD") != -1) {
						paymentChannel = "MPAY";
					}
					//dhana added
					
					
				}
				if (TCDetail.getProduct_code().equals("TCI")) {
					proEntity = "EGIB";
				} else if (TCDetail.getProduct_code().equals("TCT")) {
					proEntity = "EGTB";
				}

				if (TCDetail.getPmnt_status().equals("S")) {
					txnStatus = "Successful";
				} else if (TCDetail.getPmnt_status().equals("F")) {
					txnStatus = "Fail";
				} else if (TCDetail.getPmnt_status().equals("AP")) {
					txnStatus = "Attempt Payment";
				} else if (TCDetail.getPmnt_status().equals("O")) {
					Abandonment = TCDetail.getLast_page();
					txnStatus = "Incomplete";
				} else if (TCDetail.getPmnt_status().equals("C")) {
					txnStatus = "Cancelled";
				} else if (TCDetail.getPmnt_status().equals("R")) {
					txnStatus = "Rejection : " + TCDetail.getReason();
				}
				SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

				if (null != TCDetail.getRecord_date()) {
					quotationCreationDate = ft.format(TCDetail.getRecord_date());
				}

				if (TCDetail.getPmnt_status().equals("S") || TCDetail.getPmnt_status().equals("F")
						|| TCDetail.getPmnt_status().equals("AP")) {
					transactionStartDateTime = TCDetail.getTransaction_datetime();
				}

				int numOfChildren = 0;
				ArrayList<String> childrenName = new ArrayList<String>();
				ArrayList<String> cIdType = new ArrayList<String>();
				ArrayList<String> cIdNo = new ArrayList<String>();
				ArrayList<String> cDob = new ArrayList<String>();
				ArrayList<String> cGender = new ArrayList<String>();

				logger.info(TCDetail.getChild_name() + " TCDetail.getChild_name()1");
				if (null != TCDetail.getChild_name()) {
					if (TCDetail.getChild_name().indexOf("|") != -1) {
						String[] tmpName = TCDetail.getChild_name().split("\\|");

						int childCount = tmpName.length;
						numOfChildren = childCount;
						for (int j = 0; j < numOfChildren; j++) {
							if (tmpName.length > j) {
								childrenName.add(tmpName[j]);
							} else {
								childrenName.add("-");
							}
						}

					} else {
						numOfChildren = 1;
						childrenName.add(TCDetail.getChild_name());
					}
				}

				logger.info(TCDetail.getChild_ID_type() + " TCDetail.getChild_ID_type");
				if (null != TCDetail.getChild_ID_type()) {
					if (numOfChildren > 1) {
						String[] tmpIdType = TCDetail.getChild_ID_type().split("\\|");
						for (int j = 0; j < numOfChildren; j++) {
							if (tmpIdType.length > j) {
								cIdType.add(tmpIdType[j]);
							} else {
								cIdType.add("-");
							}
						}
					} else {
						cIdType.add(TCDetail.getChild_ID_type());

					}
				}

				logger.info(TCDetail.getChild_ID_number() + " TCDetail.getChild_ID_number");
				if (null != TCDetail.getChild_ID_number()) {

					if (numOfChildren > 1) {

						String[] tmpIdNo = TCDetail.getChild_ID_number().split("\\|");
						for (int j = 0; j < numOfChildren; j++) {
							if (tmpIdNo.length > j) {
								cIdNo.add(tmpIdNo[j]);
							} else {
								cIdNo.add("-");
							}
						}
					} else {
						cIdNo.add(TCDetail.getChild_ID_number());
					}
				}

				logger.info(TCDetail.getChild_DOB() + " TCDetail.getChild_DOB");
				if (null != TCDetail.getChild_DOB()) {

					if (numOfChildren > 1) {

						String[] tmpDob = TCDetail.getChild_DOB().split("\\|");
						for (int j = 0; j < numOfChildren; j++) {
							if (tmpDob.length > j) {
								cDob.add(tmpDob[j]);
							} else {
								cDob.add("-");
							}
						}
					} else {
						cDob.add(TCDetail.getChild_DOB());
					}
				}

				logger.info(TCDetail.getChild_gender() + " TCDetail.getChild_gender");
				if (null != TCDetail.getChild_gender()) {

					if (numOfChildren > 1) {

						String[] tmpGender = TCDetail.getChild_gender().split("\\|");
						for (int j = 0; j < numOfChildren; j++) {
							if (tmpGender.length > j) {
								cGender.add(tmpGender[j]);
							} else {
								cGender.add("-");
							}
						}
					} else {
						cGender.add(TCDetail.getChild_gender());
					}
				}

				List<String> fields2 = new ArrayList<String>();
				fields2.add(Integer.toString(i));
				fields2.add(TCDetail.getDSPQQID());
				fields2.add(quotationCreationDate);
				fields2.add(TCDetail.getPaymentTrxID());
				fields2.add(transactionStartDateTime);
				fields2.add(transactionEndDateTime);
				fields2.add(proEntity);
				fields2.add(TCDetail.getCustomer_name());
				fields2.add(TCDetail.getPolicy_number());
				fields2.add(TCDetail.getProduct_code());
				fields2.add(TCDetail.getOffered_plan_name());
				fields2.add(TCDetail.getTravlling_with());
				fields2.add(TCDetail.getCustomer_ID_type());
				fields2.add(TCDetail.getCustomer_nric_id());
				fields2.add(TCDetail.getCustomer_gender());
				fields2.add(TCDetail.getCustomer_email());
				fields2.add(TCDetail.getCustomer_address_1());
				fields2.add(TCDetail.getCustomer_address_2());
				fields2.add(TCDetail.getCustomer_address_3());
				fields2.add(TCDetail.getCustomer_state());
				fields2.add(TCDetail.getCustomer_postcode());
				fields2.add(TCDetail.getCustomer_mobile_no());
				fields2.add(TCDetail.getAgent_code());
				fields2.add(TCDetail.getAgent_name());
				fields2.add(TCDetail.getOperator_code());
				fields2.add(TCDetail.getDiscount_code());
				fields2.add(TCDetail.getTravel_start_date() + "-" + TCDetail.getTravel_end_date());
				fields2.add(TCDetail.getTravel_area_type());
				fields2.add(TCDetail.getArea_code());
				fields2.add(TCDetail.getAdv_activities());
				fields2.add(TCDetail.getFpx_fpxtxnid());
				fields2.add(TCDetail.getTransaction_id());
				fields2.add(TCDetail.getBankrefno());
				fields2.add(paymentChannel);
				fields2.add(TCDetail.getAmount());
				fields2.add(TCDetail.getGrosspremium_final());
				fields2.add(TCDetail.getDiscount());
				fields2.add(TCDetail.getTaxType());
				fields2.add(TCDetail.getGst());
				fields2.add(TCDetail.getCommission());
				fields2.add(TCDetail.getNett_premium());
				fields2.add(TCDetail.getInvoice_no());
				fields2.add(txnStatus);
				fields2.add(Abandonment);
				fields2.add(TCDetail.getPDPA());
				fields2.add(TCDetail.getSpouse_name());
				fields2.add(TCDetail.getSpouse_email());
				fields2.add(TCDetail.getSpouse_ID_type());
				fields2.add(TCDetail.getSpouse_ID_number());
				fields2.add(TCDetail.getSpouse_DOB());
				fields2.add(TCDetail.getSpouse_gender());
				

				fields2.add(Integer.toString(numOfChildren));
				for (int j = 0; j < 20; j++) {
					if (!childrenName.isEmpty() && childrenName.size() > j) {
						fields2.add(childrenName.get(j));
					} else {
						fields2.add(" ");
					}

					if (!cIdType.isEmpty() && cIdType.size() > j) {
						fields2.add(cIdType.get(j));
					} else {
						fields2.add(" ");
					}

					if (!cIdNo.isEmpty() && cIdNo.size() > j) {
						fields2.add(cIdNo.get(j));
					} else {
						fields2.add(" ");
					}

					if (!cDob.isEmpty() && cDob.size() > j) {
						fields2.add(cDob.get(j));
					} else {
						fields2.add(" ");
					}

					if (!cGender.isEmpty() && cGender.size() > j) {
						fields2.add(cGender.get(j));
					} else {
						fields2.add(" ");
					}
				}

				fields2.add(TCDetail.getModeStatus());
				fields2.add(TCDetail.getPfNumber());
				Row row2 = sheet.createRow(i);

				for (int cellIndex = 0; cellIndex < fields2.size(); cellIndex++) {
					Cell cell = row2.createCell(cellIndex);
					cell.setCellValue(fields2.get(cellIndex));
				}

				i++;
			}

			try {
				FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
				workbook.write(outputStream);
				workbook.close();

				File fileToDownload = new File(relativeWebPath);

				FileInputStream inputStream = new FileInputStream(fileToDownload);
				response.setContentType(servletContext.getMimeType(relativeWebPath));
				response.setContentLength((int) fileToDownload.length());

				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
				response.setHeader(headerKey, headerValue);

				// get output stream of the response
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				// write bytes read from the input stream into the output stream
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}

				outStream.flush();
				outStream.close();
				inputStream.close();

				if (fileToDownload.exists()) {

					boolean success = new File(relativeWebPath).delete();
					if (success) {
						logger.info("The file " + file + " has been successfully deleted");
					}
				}
			} catch (FileNotFoundException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();
				e.printStackTrace();
			} catch (IOException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();
				e.printStackTrace();
			}
		}
		// END : Trip Care

		
		else if (productType.indexOf("PCCA01") != -1 || productType.indexOf("PTCA01") != -1) {
			TransSearchObject transSearchObject_PCCA01 = new TransSearchObject();
			List<PCCA01ReportExcel> pcca01ReportList = new ArrayList<PCCA01ReportExcel>();
			List<PCCA01ReportExcel> pcca01ReportList_B4Pay = new ArrayList<PCCA01ReportExcel>();
			List<String> transactionStatusList = new ArrayList<String>();

			String query1 = "n";
			String query2 = "n";

			transSearchObject_PCCA01.setProductCode(productType);

			// if Product is Chosen
			if ("".equalsIgnoreCase(status) || status == null) {
				transactionStatusList.add("S");
				transactionStatusList.add("AP");
				transactionStatusList.add("F");
				transactionStatusList.add("O");
				transactionStatusList.add("R");
				transSearchObject_PCCA01.setTransactionStatus(transactionStatusList);
				// transSearchObject_Motor.setTransactionStatus("'S','AP','F','O','R'");
				query1 = "y";
				query2 = "y";
			} else {
				transactionStatusList.add(status);
				transSearchObject_PCCA01.setTransactionStatus(transactionStatusList);
				// transSearchObject_Motor.setTransactionStatus(status);
			}

			if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)) {
				query1 = "y";
			}
			if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
				query2 = "y";
			}

			if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
				transSearchObject_PCCA01.setPolicyNo(PolicyCertificateNo);
			}
			if (!"".equalsIgnoreCase(nRIC)) {
				transSearchObject_PCCA01.setNricNo(nRIC);
			}
			if (!"".equalsIgnoreCase(receiptNo)) {
				transSearchObject_PCCA01.setInvoiceNo(receiptNo);
			}

			transSearchObject_PCCA01.setDateFrom(dateFrom);
			transSearchObject_PCCA01.setDateTo(dateTo);

			// if query 1
			if ("y".equalsIgnoreCase(query1)) {
				pcca01ReportList = pcca01ReportExcelMapper.selectPCCA01TransactionalReportAfterPayment(transSearchObject_PCCA01);

			} // End if query1 is Executed

			// if query 2
			if ("y".equalsIgnoreCase(query2)) {
				pcca01ReportList_B4Pay = pcca01ReportExcelMapper.selectPCCA01TransactionalReportBeforePayment(transSearchObject_PCCA01);
			}

			pcca01ReportList.addAll(pcca01ReportList_B4Pay);

			Collections.sort(pcca01ReportList, new Comparator<PCCA01ReportExcel>() {
				@Override
				public int compare(PCCA01ReportExcel o1, PCCA01ReportExcel o2) {
					return o1.getRecord_date().compareTo(o2.getRecord_date());
				}
			});

			Collections.reverse(pcca01ReportList);

			if (pcca01ReportList.size() <= 0) {
				model.addAttribute("emptySearch", "No Record Found");
			}
			model.addAttribute("pcca01ReportList", pcca01ReportList);

			// start excel
			Random ran = new Random();
			int randomNo = ran.nextInt(6) + 5;

			DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
			Date date = new Date();

			String file = "EtiqaTransactionReport" + dateFormat.format(date) + randomNo + ".xlsx";

			String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
			// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

			logger.info("relativeWebPath =" + relativeWebPath);

			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Etiqa Transaction Report");

			// header fields based on product
			String[] fields = null;

			fields = new String[] { "Id", "DSPQQID", "Quotation Creation Date", "Txn Date", "Transaction Start",
					"Transaction End", "Transaction Time Taken", "Product Name", "Policy/Cert No", "Coverage Amount",
					"Coverage Term", "Status", "Abandonment", "Rejection Reason", "Name", "NRIC No", "Gender", "Email",
					"Address", "Postcode", "State", "Mobile No", "Annual Premium/Contribution", "Payment Type",
					"Payment Method", "Payment Mode", "Amount Paid", "MPAY Ref No", "MPAY Auth Code", "Promo Code",
					"Agent Name", "Agent Code", "Operator Code" };

			Row row1 = sheet.createRow(0);

			for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
				Cell cell = row1.createCell(cellIndex);
				cell.setCellValue(fields[cellIndex]);
			}

			String[] fields2 = null;

			int i = 1;

			for (PCCA01ReportExcel PCCA01Detail : pcca01ReportList) {

				String Abandonment = "";

				String txnStatus = "";
				String quotationCreationDate = "";
				String transactionStartDateTime = "";
				String transactionEndDateTime = "";
				String proEntity = "";
				String paymentChannel = "";

				String txnTimeTaken = "";
				String transactionDate = "";

				if (null != PCCA01Detail.getPmnt_gateway_code() && "" != PCCA01Detail.getPmnt_gateway_code()) {
					if (PCCA01Detail.getPmnt_gateway_code().indexOf("EBPG") != -1) {

						paymentChannel = "EBPG";
					} else if (PCCA01Detail.getPmnt_gateway_code().indexOf("AMEX") != -1) {

						paymentChannel = "AMEX";
					} else if (PCCA01Detail.getPmnt_gateway_code().indexOf("FPX") != -1) {

						paymentChannel = "FPX";
					} else if (PCCA01Detail.getPmnt_gateway_code().indexOf("Maybank2U") != -1) {

						paymentChannel = "M2U";
					} else if (PCCA01Detail.getPmnt_gateway_code().indexOf("CREDIT CARD") != -1) {

						paymentChannel = "MPAY";
					}

				}
				if (PCCA01Detail.getProduct_code().equals("PCCA01") ) {

					proEntity = "EIB";
				} else if (PCCA01Detail.getProduct_code().equals("PTCA01") ) {

					proEntity = "EGB";

				}

				if (PCCA01Detail.getPmnt_status().equals("S")) {

					txnStatus = "Successful";

				} else if (PCCA01Detail.getPmnt_status().equals("F")) {

					txnStatus = "Fail";

				} else if (PCCA01Detail.getPmnt_status().equals("AP")) {

					txnStatus = "Attempt Payment";

				} else if (PCCA01Detail.getPmnt_status().equals("O")) {

					Abandonment = PCCA01Detail.getLast_page();
					txnStatus = "Incomplete";

				} else if (PCCA01Detail.getPmnt_status().equals("C")) {

					txnStatus = "Cancelled";

				} else if (PCCA01Detail.getPmnt_status().equals("R")) {

					txnStatus = "Rejection : " + PCCA01Detail.getUWReason();

				}

				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				Date quotationCreationDateTmp = null;

				try {

					quotationCreationDateTmp = formatter.parse(PCCA01Detail.getQuotationCreateDateTime());
					quotationCreationDate = ft.format(quotationCreationDateTmp);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				if (PCCA01Detail.getPmnt_status().equals("S") || PCCA01Detail.getPmnt_status().equals("F")
						|| PCCA01Detail.getPmnt_status().equals("AP")) {

					transactionStartDateTime = PCCA01Detail.getTransaction_datetime();

					/*
					 * try {
					 *
					 * transactionStartDateTime=
					 * ft2.format(format1.parse(transactionStartDateTime));
					 *
					 * } catch (ParseException e) { // TODO Auto-generated catch block
					 * e.printStackTrace(); }
					 */
				}
				String premiumVal = "";
				DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(Locale.US);
				format.setParseBigDecimal(true);
				BigDecimal amt = new BigDecimal(0.00);
				BigDecimal calculateAmt = new BigDecimal(0.00);
				BigDecimal Annualamt = new BigDecimal(1.00);
				BigDecimal MonthlyAmt = new BigDecimal(12.00);

				try {
					amt = (BigDecimal) format.parse(PCCA01Detail.getAmount());

					if (PCCA01Detail.getPremium_mode().equals("annualy")) {
						calculateAmt = Annualamt.multiply(amt);
					} else {
						calculateAmt = MonthlyAmt.multiply(amt);
					}
					premiumVal = String.valueOf(calculateAmt);
				} catch (Exception e) {
					e.printStackTrace();
				}
				fields2 = new String[] { Integer.toString(i), PCCA01Detail.getDSPQQID(), quotationCreationDate,
						transactionDate, transactionStartDateTime, transactionEndDateTime, txnTimeTaken,
						PCCA01Detail.getProduct_code(), PCCA01Detail.getPolicy_number(), PCCA01Detail.getCoverage_amount(),
						PCCA01Detail.getCoverage_term() + " Years", txnStatus, Abandonment, PCCA01Detail.getUWReason(),
						PCCA01Detail.getCustomer_name(), PCCA01Detail.getCustomer_nric_id(), PCCA01Detail.getCustomerGender(),
						PCCA01Detail.getCustomerEmail(),
						PCCA01Detail.getCustomerAddress1() + "," + PCCA01Detail.getCustomerAddress2() + ","
								+ PCCA01Detail.getCustomerAddress3(),
								PCCA01Detail.getCustomerPostcode(), PCCA01Detail.getCustomerState(), PCCA01Detail.getCustomerMobileNo(),
						premiumVal,
						// TLDetail.getPremium_amount(),
						PCCA01Detail.getPmnt_gateway_code(), paymentChannel, PCCA01Detail.getPremium_mode(),
						PCCA01Detail.getAmount(), PCCA01Detail.getTxn_id(), PCCA01Detail.getAuth_code(), PCCA01Detail.getDiscountCode(),
						PCCA01Detail.getAgent_name(), PCCA01Detail.getAgent_code(), PCCA01Detail.getOperatorCode() };
             logger.info("@@@@@@@@@@@@@@@@@@@PCCA01Detail@@@@@@@@@@@@@@@222"+PCCA01Detail);
				Row row2 = sheet.createRow(i);

				for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
					Cell cell = row2.createCell(cellIndex);
					cell.setCellValue(fields2[cellIndex]);
				}

				i++;

			}

			// logger.info("txnStatus "+txn.getStatus());
			try {
				FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
				workbook.write(outputStream);
				workbook.close();

				File fileToDownload = new File(relativeWebPath);

				FileInputStream inputStream = new FileInputStream(fileToDownload);
				response.setContentType(servletContext.getMimeType(relativeWebPath));
				response.setContentLength((int) fileToDownload.length());

				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
				response.setHeader(headerKey, headerValue);

				// get output stream of the response
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				// write bytes read from the input stream into the output stream
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}

				outStream.flush();
				outStream.close();
				inputStream.close();

				if (fileToDownload.exists()) {

					boolean success = new File(relativeWebPath).delete();
					if (success) {
						logger.info("The file " + file + " has been successfully deleted");
					}

				}

			} catch (FileNotFoundException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			} catch (IOException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			}
		}
			
		else if (productType.indexOf("MP") != -1 || productType.indexOf("MPT") != -1) {
			TransSearchObject transSearchObject_MP = new TransSearchObject();
			List<MPReportExcel> mpReportList = new ArrayList<MPReportExcel>();
			List<MPReportExcel> mpReportList_B4Pay = new ArrayList<MPReportExcel>();
			List<String> transactionStatusList = new ArrayList<String>();

			String query1 = "n";
			String query2 = "n";

			transSearchObject_MP.setProductCode(productType);

			// if Product is Chosen
			if ("".equalsIgnoreCase(status) || status == null) {
				transactionStatusList.add("S");
				transactionStatusList.add("AP");
				transactionStatusList.add("F");
				transactionStatusList.add("O");
				transactionStatusList.add("R");
				transSearchObject_MP.setTransactionStatus(transactionStatusList);
				// transSearchObject_Motor.setTransactionStatus("'S','AP','F','O','R'");
				query1 = "y";
				query2 = "y";
			} else {
				transactionStatusList.add(status);
				transSearchObject_MP.setTransactionStatus(transactionStatusList);
				// transSearchObject_Motor.setTransactionStatus(status);
			}

			if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)) {
				query1 = "y";
			}
			if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
				query2 = "y";
			}

			if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
				transSearchObject_MP.setPolicyNo(PolicyCertificateNo);
			}
			if (!"".equalsIgnoreCase(nRIC)) {
				transSearchObject_MP.setNricNo(nRIC);
			}
			if (!"".equalsIgnoreCase(receiptNo)) {
				transSearchObject_MP.setInvoiceNo(receiptNo);
			}

			transSearchObject_MP.setDateFrom(dateFrom);
			transSearchObject_MP.setDateTo(dateTo);

			// if query 1
			if ("y".equalsIgnoreCase(query1)) {
				mpReportList = mpReportExcelMapper.selectMPTransactionalReportAfterPayment(transSearchObject_MP);		
			} // End if query1 is Executed

			// if query 2
			if ("y".equalsIgnoreCase(query2)) {
				mpReportList_B4Pay = mpReportExcelMapper.selectMPTransactionalReportBeforePayment(transSearchObject_MP);
			}

			mpReportList.addAll(mpReportList_B4Pay);

			Collections.sort(mpReportList, new Comparator<MPReportExcel>() {
				@Override
				public int compare(MPReportExcel o1, MPReportExcel o2) {
					return o1.getRecord_date().compareTo(o2.getRecord_date());
				}
			});

			Collections.reverse(mpReportList);

			if (mpReportList.size() <= 0) {
				model.addAttribute("emptySearch", "No Record Found");
			}
			model.addAttribute("mpReportList", mpReportList);

			// start excel
			Random ran = new Random();
			int randomNo = ran.nextInt(6) + 5;

			DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
			Date date = new Date();

			String file = "EtiqaTransactionReport" + dateFormat.format(date) + randomNo + ".xlsx";

			String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
			// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

			logger.info("relativeWebPath =" + relativeWebPath);

			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Etiqa Transaction Report");

			// header fields based on product
			String[] fields = null;

			fields = new String[] { "Id", "DSPQQID", "Quotation Creation Date", "Txn Date", "Transaction Start",
					"Transaction End", "Transaction Time Taken", "Product Name", "Policy/Cert No", "Coverage Amount",
					"Coverage Term", "Status", "Abandonment", "Rejection Reason", "Name", "NRIC No", "Gender", "Email",
					"Address", "Postcode", "State", "Mobile No", "Annual Premium/Contribution", "Payment Type",
					"Payment Method", "Payment Mode", "Amount Paid", "MPAY Ref No", "MPAY Auth Code", "Promo Code",
					"Agent Name", "Agent Code", "Operator Code" };

			Row row1 = sheet.createRow(0);

			for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
				Cell cell = row1.createCell(cellIndex);
				cell.setCellValue(fields[cellIndex]);
			}

			String[] fields2 = null;

			int i = 1;

			for (MPReportExcel MPDetail : mpReportList) {

				String Abandonment = "";

				String txnStatus = "";
				String quotationCreationDate = "";
				String transactionStartDateTime = "";
				String transactionEndDateTime = "";
				String proEntity = "";
				String paymentChannel = "";

				String txnTimeTaken = "";
				String transactionDate = "";

				if (null != MPDetail.getPmnt_gateway_code() && "" != MPDetail.getPmnt_gateway_code()) {
					if (MPDetail.getPmnt_gateway_code().indexOf("EBPG") != -1) {

						paymentChannel = "EBPG";
					} else if (MPDetail.getPmnt_gateway_code().indexOf("AMEX") != -1) {

						paymentChannel = "AMEX";
					} else if (MPDetail.getPmnt_gateway_code().indexOf("FPX") != -1) {

						paymentChannel = "FPX";
					} else if (MPDetail.getPmnt_gateway_code().indexOf("Maybank2U") != -1) {

						paymentChannel = "M2U";
					} else if (MPDetail.getPmnt_gateway_code().indexOf("CREDIT CARD") != -1) {

						paymentChannel = "MPAY";
					}

				}
				if (MPDetail.getProduct_code().indexOf("MP") != -1) {

					proEntity = "EIB";
				} else if (MPDetail.getProduct_code().indexOf("MPT") != -1) {

					proEntity = "EGB";

				}

				if (MPDetail.getPmnt_status().equals("S")) {

					txnStatus = "Successful";

				} else if (MPDetail.getPmnt_status().equals("F")) {

					txnStatus = "Fail";

				} else if (MPDetail.getPmnt_status().equals("AP")) {

					txnStatus = "Attempt Payment";

				} else if (MPDetail.getPmnt_status().equals("O")) {

					Abandonment = MPDetail.getLast_page();
					txnStatus = "Incomplete";

				} else if (MPDetail.getPmnt_status().equals("C")) {

					txnStatus = "Cancelled";

				} else if (MPDetail.getPmnt_status().equals("R")) {

					txnStatus = "Rejection : " + MPDetail.getUWReason();

				}

				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				Date quotationCreationDateTmp = null;

				try {

					quotationCreationDateTmp = formatter.parse(MPDetail.getQuotationCreateDateTime());
					quotationCreationDate = ft.format(quotationCreationDateTmp);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				if (MPDetail.getPmnt_status().equals("S") || MPDetail.getPmnt_status().equals("F")
						|| MPDetail.getPmnt_status().equals("AP")) {

					transactionStartDateTime = MPDetail.getTransaction_datetime();

					/*
					 * try {
					 *
					 * transactionStartDateTime=
					 * ft2.format(format1.parse(transactionStartDateTime));
					 *
					 * } catch (ParseException e) { // TODO Auto-generated catch block
					 * e.printStackTrace(); }
					 */
				}
				String premiumVal = "";
				DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(Locale.US);
				format.setParseBigDecimal(true);
				BigDecimal amt = new BigDecimal(0.00);
				BigDecimal calculateAmt = new BigDecimal(0.00);
				BigDecimal Annualamt = new BigDecimal(1.00);
				BigDecimal MonthlyAmt = new BigDecimal(12.00);

				try {
					amt = (BigDecimal) format.parse(MPDetail.getAmount());

					if (MPDetail.getPremium_mode().equals("annualy")) {
						calculateAmt = Annualamt.multiply(amt);
					} else {
						calculateAmt = MonthlyAmt.multiply(amt);
					}
					premiumVal = String.valueOf(calculateAmt);
				} catch (Exception e) {
					e.printStackTrace();
				}
				fields2 = new String[] { Integer.toString(i), MPDetail.getDSPQQID(), quotationCreationDate,
						transactionDate, transactionStartDateTime, transactionEndDateTime, txnTimeTaken,
						MPDetail.getProduct_code(), MPDetail.getPolicy_number(), MPDetail.getCoverage_amount(),
						MPDetail.getCoverage_term() + " Years", txnStatus, Abandonment, MPDetail.getUWReason(),
						MPDetail.getCustomer_name(), MPDetail.getCustomer_nric_id(), MPDetail.getCustomerGender(),
						MPDetail.getCustomerEmail(),
						MPDetail.getCustomerAddress1() + "," + MPDetail.getCustomerAddress2() + ","
								+ MPDetail.getCustomerAddress3(),
								MPDetail.getCustomerPostcode(), MPDetail.getCustomerState(), MPDetail.getCustomerMobileNo(),
						premiumVal,
						// TLDetail.getPremium_amount(),
						MPDetail.getPmnt_gateway_code(), paymentChannel, MPDetail.getPremium_mode(),
						MPDetail.getAmount(), MPDetail.getTxn_id(), MPDetail.getAuth_code(), MPDetail.getDiscountCode(),
						MPDetail.getAgent_name(),MPDetail.getAgent_code(), MPDetail.getOperatorCode() };

				Row row2 = sheet.createRow(i);

				for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
					Cell cell = row2.createCell(cellIndex);
					cell.setCellValue(fields2[cellIndex]);
				}

				i++;

			}

			// logger.info("txnStatus "+txn.getStatus());
			try {
				FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
				workbook.write(outputStream);
				workbook.close();

				File fileToDownload = new File(relativeWebPath);

				FileInputStream inputStream = new FileInputStream(fileToDownload);
				response.setContentType(servletContext.getMimeType(relativeWebPath));
				response.setContentLength((int) fileToDownload.length());

				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
				response.setHeader(headerKey, headerValue);

				// get output stream of the response
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				// write bytes read from the input stream into the output stream
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}

				outStream.flush();
				outStream.close();
				inputStream.close();

				if (fileToDownload.exists()) {

					boolean success = new File(relativeWebPath).delete();
					if (success) {
						logger.info("The file " + file + " has been successfully deleted");
					}

				}

			} catch (FileNotFoundException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			} catch (IOException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			}
		}

		
		// START: ALL TRANSACTION REPORT
		else if ("".equalsIgnoreCase(productType) || productType == null) {
			TransSearchObject transSearchObject_Motor = new TransSearchObject();
			TransSearchObject transSearchObject_TL = new TransSearchObject();
			TransSearchObject transSearchObject_PCCA01 = new TransSearchObject();
			TransSearchObject transSearchObject_WTC = new TransSearchObject();
			TransSearchObject transSearchObject_HOHH = new TransSearchObject();
			TransSearchObject transSearchObject_Buddy = new TransSearchObject();
			TransSearchObject transSearchObject_TravEz = new TransSearchObject();
			TransSearchObject transSearchObject_TripCare = new TransSearchObject();
			List<ALLReportVO> allReportList = new ArrayList<ALLReportVO>();
			TransSearchObject transSearchObject_Secure = new TransSearchObject();
			List<ALLReportVO> motorReportList = new ArrayList<ALLReportVO>();
			List<ALLReportVO> tlReportList = new ArrayList<ALLReportVO>();
			List<ALLReportVO> pcca01ReportList = new ArrayList<ALLReportVO>();
			List<ALLReportVO> wtcReportList = new ArrayList<ALLReportVO>();
			List<ALLReportVO> hohhReportList = new ArrayList<ALLReportVO>();
			List<String> transactionStatusList = new ArrayList<String>();
			List<ALLReportVO> buddyReportList = new ArrayList<ALLReportVO>();
			List<ALLReportVO> travEzReportList = new ArrayList<ALLReportVO>();
			List<ALLReportVO> secureReportList = new ArrayList<ALLReportVO>();
			List<ALLReportVO> tripCareReportList = new ArrayList<ALLReportVO>();

			if ("EIB".equalsIgnoreCase(productEntity)) {

				transSearchObject_Motor.setProductCode("MI");
				transSearchObject_TL.setProductCode("TL");
				transSearchObject_PCCA01.setProductCode("PCCA01");
				transSearchObject_WTC.setProductCode("WTC");
				transSearchObject_HOHH.setProductCode("HOHH");
				transSearchObject_Buddy.setProductCode("CPP");
				transSearchObject_TravEz.setProductCode("BPT");
				transSearchObject_Secure.setProductCode("EZYTL");
				transSearchObject_TripCare.setProductCode("TCI");
			} else if ("ETB".equalsIgnoreCase(productEntity)) {
				transSearchObject_TL.setProductCode("IDS");
				transSearchObject_PCCA01.setProductCode("PTCA01");
				transSearchObject_Motor.setProductCode("MT");
				transSearchObject_WTC.setProductCode("TPT");
				transSearchObject_HOHH.setProductCode("HOHH-ETB");
				transSearchObject_Buddy.setProductCode("TPP");
				transSearchObject_TravEz.setProductCode("TZT");
				transSearchObject_Secure.setProductCode("ISCTL");
				transSearchObject_TripCare.setProductCode("TCT");
			}

			if ("".equalsIgnoreCase(status) || status == null) {
				transactionStatusList.add("S");
				transactionStatusList.add("AP");
				transactionStatusList.add("F");
				transactionStatusList.add("C");
			} else {
				transactionStatusList.add(status);

			}

			transSearchObject_Motor.setTransactionStatus(transactionStatusList);
			transSearchObject_TL.setTransactionStatus(transactionStatusList);
			transSearchObject_PCCA01.setTransactionStatus(transactionStatusList);
			transSearchObject_WTC.setTransactionStatus(transactionStatusList);
			transSearchObject_HOHH.setTransactionStatus(transactionStatusList);
			transSearchObject_Secure.setTransactionStatus(transactionStatusList);
			transSearchObject_Buddy.setTransactionStatus(transactionStatusList);
			transSearchObject_TravEz.setTransactionStatus(transactionStatusList);
			transSearchObject_TripCare.setTransactionStatus(transactionStatusList);

			if (!"".equalsIgnoreCase(PolicyCertificateNo)) {

				transSearchObject_Motor.setPolicyNo(PolicyCertificateNo);
				transSearchObject_TL.setPolicyNo(PolicyCertificateNo);
				transSearchObject_PCCA01.setPolicyNo(PolicyCertificateNo);
				transSearchObject_WTC.setPolicyNo(PolicyCertificateNo);
				transSearchObject_HOHH.setPolicyNo(PolicyCertificateNo);
				transSearchObject_Buddy.setPolicyNo(PolicyCertificateNo);
				transSearchObject_TravEz.setPolicyNo(PolicyCertificateNo);
				transSearchObject_Secure.setPolicyNo(PolicyCertificateNo);
				transSearchObject_TripCare.setPolicyNo(PolicyCertificateNo);
			}
			if (!"".equalsIgnoreCase(nRIC)) {
				transSearchObject_Motor.setNricNo(nRIC);
				transSearchObject_TL.setNricNo(nRIC);
				transSearchObject_PCCA01.setNricNo(nRIC);
				transSearchObject_WTC.setNricNo(nRIC);
				transSearchObject_HOHH.setNricNo(nRIC);
				transSearchObject_Buddy.setNricNo(nRIC);
				transSearchObject_TravEz.setNricNo(nRIC);
				transSearchObject_Secure.setNricNo(nRIC);
				transSearchObject_TripCare.setNricNo(nRIC);
			}
			if (!"".equalsIgnoreCase(receiptNo)) {
				transSearchObject_Motor.setInvoiceNo(receiptNo);
				transSearchObject_TL.setInvoiceNo(receiptNo);
				transSearchObject_PCCA01.setInvoiceNo(receiptNo);
				transSearchObject_WTC.setInvoiceNo(receiptNo);
				transSearchObject_HOHH.setInvoiceNo(receiptNo);
				transSearchObject_Buddy.setInvoiceNo(receiptNo);
				transSearchObject_TravEz.setInvoiceNo(receiptNo);
				transSearchObject_Secure.setInvoiceNo(receiptNo);
				transSearchObject_TripCare.setInvoiceNo(receiptNo);
			}

			transSearchObject_Motor.setDateFrom(dateFrom);
			transSearchObject_TL.setDateFrom(dateFrom);
			transSearchObject_PCCA01.setDateFrom(dateFrom);
			transSearchObject_WTC.setDateFrom(dateFrom);
			transSearchObject_HOHH.setDateFrom(dateFrom);
			transSearchObject_Buddy.setDateFrom(dateFrom);
			transSearchObject_TravEz.setDateFrom(dateFrom);
			transSearchObject_Secure.setDateFrom(dateFrom);
			transSearchObject_TripCare.setDateFrom(dateFrom);
			transSearchObject_Motor.setDateTo(dateTo);
			transSearchObject_TL.setDateTo(dateTo);
			transSearchObject_PCCA01.setDateTo(dateTo);
			transSearchObject_WTC.setDateTo(dateTo);
			transSearchObject_HOHH.setDateTo(dateTo);
			transSearchObject_Buddy.setDateTo(dateTo);
			transSearchObject_TravEz.setDateTo(dateTo);
			transSearchObject_Secure.setDateTo(dateTo);
			transSearchObject_TripCare.setDateTo(dateTo);

			motorReportList = allReportMapper.selectMotorTransactionalReportAfterPayment(transSearchObject_Motor);
			// if("EIB".equalsIgnoreCase(productEntity)){
			tlReportList = allReportMapper.selectTLTransactionalReportAfterPayment(transSearchObject_TL);
			pcca01ReportList = allReportMapper.selectPCCA01TransactionalReportAfterPayment(transSearchObject_PCCA01);
			// }
			wtcReportList = allReportMapper.selectWTCTransactionalReportAfterPayment(transSearchObject_WTC);
			hohhReportList = allReportMapper.selectHOHHTransactionalReportAfterPayment(transSearchObject_HOHH);
			buddyReportList = allReportMapper.selectBuddyPATransactionalReportAfterPayment(transSearchObject_Buddy);
			travEzReportList = allReportMapper.selectTravelEzyTransactionalReportAfterPayment(transSearchObject_TravEz);
			secureReportList = allReportMapper.selectSECURETransactionalReportAfterPayment(transSearchObject_Secure);
			tripCareReportList = allReportMapper.selectTCTransactionalReportAfterPayment(transSearchObject_TripCare);

			System.out
					.println(transSearchObject_TL.getProductCode() + "tlReportList getProductCode in Tran Excel File");
			logger.info(
					transSearchObject_Secure.getProductCode() + "secureReportList getProductCode in Tran Excel File");
			logger.info(tlReportList.size() + "tlReportList in Tran Excel File");
			logger.info(secureReportList.size() + "secureReportList  in Tran Excel File");
			allReportList.addAll(motorReportList);
			// if("EIB".equalsIgnoreCase(productEntity)){
			allReportList.addAll(tlReportList);
			allReportList.addAll(pcca01ReportList);
			// }
			allReportList.addAll(wtcReportList);
			allReportList.addAll(hohhReportList);
			allReportList.addAll(buddyReportList);
			allReportList.addAll(travEzReportList);
			allReportList.addAll(secureReportList);
			allReportList.addAll(tripCareReportList);
			Collections.sort(allReportList, new Comparator<ALLReportVO>() {
				@Override
				public int compare(ALLReportVO o1, ALLReportVO o2) {
					return o1.getRecord_date().compareTo(o2.getRecord_date());
				}
			});

			Collections.reverse(allReportList);

			if (allReportList.size() <= 0) {
				model.addAttribute("emptySearch", "No Record Found");
			}
			model.addAttribute("reportResult", allReportList);

			// start excel

			Random ran = new Random();
			int randomNo = ran.nextInt(6) + 5;

			DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
			Date date = new Date();

			String file = "EtiqaTransactionReport" + dateFormat.format(date) + randomNo + ".xlsx";

			String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
			// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Etiqa Transaction Report");

			// header fields based on product
			String[] fields = null;

			fields = new String[] { "Id", "DSPQQID", "Entity", "Product", "Quotation Creation Date", "Txn Date",
					"Policy No", "Insured name", "Insured Id", "Poi", "Payment mode", "Status", "Txn Amount",
					"Gross Premium", "Discount", "GST", "Tax Invoice No", "CAPS PolicyNo", "CAPS Amount", "Mode" };

			Row row1 = sheet.createRow(0);

			for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
				Cell cell = row1.createCell(cellIndex);
				cell.setCellValue(fields[cellIndex]);
			}

			String[] fields2 = null;

			int i = 1;

			for (ALLReportVO ALLDetail : allReportList) {

				String txnStatus = "";
				String quotationCreationDate = "";

				String paymentChannel = "";

				if (null != ALLDetail.getPmnt_gateway_code() && "" != ALLDetail.getPmnt_gateway_code()) {
					if (ALLDetail.getPmnt_gateway_code().indexOf("EBPG") != -1) {

						paymentChannel = "EBPG";
					}

					else if (ALLDetail.getPmnt_gateway_code().indexOf("AMEX") != -1) {

						paymentChannel = "AMEX";
					}

					else if (ALLDetail.getPmnt_gateway_code().indexOf("FPX") != -1) {

						paymentChannel = "FPX";
					}

					else if (ALLDetail.getPmnt_gateway_code().indexOf("Maybank2U") != -1) {

						paymentChannel = "M2U";
					}

					else if (ALLDetail.getPmnt_gateway_code().indexOf("CREDIT CARD") != -1) {

						paymentChannel = "MPAY";
					}
					else if (ALLDetail.getPmnt_gateway_code().indexOf("API") != -1) {

						paymentChannel = "MOTORAPI";
					}

				}

				if (ALLDetail.getPmnt_status().equals("S")) {

					txnStatus = "Successful";

				} else if (ALLDetail.getPmnt_status().equals("F")) {

					txnStatus = "Fail";

				}

				else if (ALLDetail.getPmnt_status().equals("AP")) {

					txnStatus = "Attempt Payment";

				}

				else if (ALLDetail.getPmnt_status().equals("C")) {

					txnStatus = "Cancelled";

				}

				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				Date quotationCreationDateTmp = null;

				try {

					quotationCreationDateTmp = formatter.parse(ALLDetail.getQuotation_datetime());
					quotationCreationDate = ft.format(quotationCreationDateTmp);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				fields2 = new String[] { Integer.toString(i), ALLDetail.getDSPQQID(), productEntity,
						ALLDetail.getProduct_code(), quotationCreationDate, ALLDetail.getTransaction_datetime(),
						ALLDetail.getPolicy_number(), ALLDetail.getCustomer_name(), ALLDetail.getCustomer_nric_id(),
						ALLDetail.getPoi(), paymentChannel, txnStatus, ALLDetail.getAmount(),
						ALLDetail.getGrosspremium_final(), ALLDetail.getDiscount(), ALLDetail.getGst(),
						ALLDetail.getInvoice_no(), ALLDetail.getCaps_dppa(),
						ALLDetail.getPassenger_pa_premium_payable(), ALLDetail.getModeStatus() };

				Row row2 = sheet.createRow(i);

				for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
					Cell cell = row2.createCell(cellIndex);
					cell.setCellValue(fields2[cellIndex]);
				}

				i++;

			}

			// logger.info("txnStatus "+txn.getStatus());

			try {
				FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
				workbook.write(outputStream);
				workbook.close();

				File fileToDownload = new File(relativeWebPath);

				FileInputStream inputStream = new FileInputStream(fileToDownload);
				response.setContentType(servletContext.getMimeType(relativeWebPath));
				response.setContentLength((int) fileToDownload.length());

				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
				response.setHeader(headerKey, headerValue);

				// get output stream of the response
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				// write bytes read from the input stream into the output stream
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}

				outStream.flush();
				outStream.close();
				inputStream.close();

				if (fileToDownload.exists()) {

					boolean success = new File(relativeWebPath).delete();
					if (success) {
						logger.info("The file " + file + " has been successfully deleted");
					}

				}

			} catch (FileNotFoundException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			} catch (IOException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			}

		}

		// END: ALL TRANSACTION REPORT

		return "admin-report-transaction1";

	}

}
