package com.spring.admin;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.etiqa.DAO.BuddyPADAO;
import com.etiqa.DAO.BuddyPADAOImpl;
import com.etiqa.utils.FormatDates;
import com.etiqa.utils.convertBytes;
import com.spring.VO.Approval;
import com.spring.VO.ApprovalExample;
import com.spring.VO.ApprovalLog;
import com.spring.VO.BuddyPABenefit;
import com.spring.VO.BuddyPABenefitExample;
import com.spring.VO.BuddyPABenefitGrpList;
import com.spring.VO.BuddyPACombo;
import com.spring.VO.BuddyPAComboExample;
import com.spring.VO.BuddyPAOccupation;
import com.spring.VO.BuddyPAOccupationExample;
import com.spring.VO.BuddyPAPlan;
import com.spring.VO.BuddyPAPlanExample;
import com.spring.VO.BuddyPAPlanGrp;
import com.spring.VO.BuddyPAPlanGrpExample;
import com.spring.VO.BuddyPAPlanGrpList;
import com.spring.VO.BuddyPAProduct;
import com.spring.VO.BuddyPAProductExample;
import com.spring.VO.SysParam;
import com.spring.VO.SysParamExample;
import com.spring.mapper.ApprovalLogMapper;
import com.spring.mapper.ApprovalMapper;
import com.spring.mapper.BuddyPABenefitGrpMapper;
import com.spring.mapper.BuddyPABenefitMapper;
import com.spring.mapper.BuddyPAComboMapper;
import com.spring.mapper.BuddyPAOccupationMapper;
import com.spring.mapper.BuddyPAPlanGrpMapper;
import com.spring.mapper.BuddyPAPlanMapper;
import com.spring.mapper.BuddyPAProductMapper;
import com.spring.mapper.SysParamMapper;

@Controller
public class BuddyPAController {
	SysParamMapper sysParamMapper;
	BuddyPAOccupationMapper buddyPAOccupationMapper;
	BuddyPAProductMapper buddyPAProductMapper;
	BuddyPAPlanMapper buddyPAPlanMapper;
	BuddyPAPlanGrpMapper buddyPAPlanGrpMapper;
	BuddyPABenefitMapper buddyPABenefitMapper;
	BuddyPAComboMapper buddyPAComboMapper;
	BuddyPABenefitGrpMapper buddyPABenefitGrpMapper;
	ApprovalMapper approvalMapper;
	ApprovalLogMapper approvalLogMapper;

	private static final String BUDDY_URL_EN = "https://www.etiqa.com.my/getonline/BuddyPA?cmb=";
	private static final String BUDDY_URL_BM = "https://www.etiqa.com.my/getonline/BuddyPA?cmb=";

	@Autowired
	public BuddyPAController(SysParamMapper sysParamMapper, BuddyPAOccupationMapper buddyPAOccupationMapper,
			BuddyPAProductMapper buddyPAProductMapper, BuddyPAPlanMapper buddyPAPlanMapper,
			BuddyPAPlanGrpMapper buddyPAPlanGrpMapper, BuddyPABenefitMapper buddyPABenefitMapper,
			BuddyPABenefitGrpMapper buddyPABenefitGrpMapper, BuddyPAComboMapper buddyPAComboMapper,
			ApprovalMapper approvalMapper, ApprovalLogMapper approvalLogMapper) {

		this.sysParamMapper = sysParamMapper;
		this.buddyPAOccupationMapper = buddyPAOccupationMapper;
		this.buddyPAProductMapper = buddyPAProductMapper;
		this.buddyPAPlanMapper = buddyPAPlanMapper;
		this.buddyPAPlanGrpMapper = buddyPAPlanGrpMapper;
		this.buddyPABenefitMapper = buddyPABenefitMapper;
		this.buddyPAComboMapper = buddyPAComboMapper;
		this.approvalMapper = approvalMapper;
		this.approvalLogMapper = approvalLogMapper;

	}

	// Terence (22052018)
	/*------------------------------------------------------START - Occupation / System Setup Info---------------------------------------------------------*/
	/*----------START - Occupation List----------*/
	@RequestMapping("/BuddyPAOccupationInfo")
	public String buddyPAOccupationInfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("Buddy PA Occupation Info");
		HttpSession session = request.getSession();

		// list of companies
		List<SysParam> companyList;
		SysParamExample sysParamExample = new SysParamExample();

		SysParamExample.Criteria sysParaminfoCriteriaExist = sysParamExample.createCriteria();
		sysParaminfoCriteriaExist.andParamEqualTo("Company");
		companyList = sysParamMapper.selectByExample(sysParamExample);

		String strCompany = companyList.get(0).getName().toString();
		// check for parameter
		if (request.getParameter("company") != null) {
			strCompany = request.getParameter("company");
		}

		// list of occupation
		List<BuddyPAOccupation> occupationList;
		BuddyPAOccupationExample buddyPAOccupationExample = new BuddyPAOccupationExample();
		BuddyPAOccupationExample.Criteria buddyPAOccCriteria = buddyPAOccupationExample.createCriteria();
		buddyPAOccCriteria.andCompanyIdEqualTo(strCompany);
		buddyPAOccupationExample.setOrderByClause("COMPANY_ID");

		occupationList = buddyPAOccupationMapper.selectByExample(buddyPAOccupationExample);

		model.addAttribute("occupationList", occupationList);
		model.addAttribute("companyList", companyList);
		model.addAttribute("company", strCompany);

		return "/products/buddyPA/buddyPA-occupation-mng-add";

	}
	/*----------END - Occupation List----------*/

	/*----------START - Occupation Add----------*/
	@RequestMapping("/addBuddyPAOccupation")
	public String addBuddyPAOccupation(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("add Occupation");
		String redirctTO = null;
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Buddy PA Controller loginUser" + loginUser);

		// Check If Exists
		List<BuddyPAOccupation> occupationList;
		BuddyPAOccupationExample buddyPAOccupationExample = new BuddyPAOccupationExample();
		BuddyPAOccupationExample.Criteria buddyPAOccupationExampleCriteria = buddyPAOccupationExample.createCriteria();
		buddyPAOccupationExampleCriteria.andCompanyIdEqualTo(request.getParameter("company"));
		buddyPAOccupationExampleCriteria.andOccupationCodeEqualTo(request.getParameter("occupationCode"));
		occupationList = buddyPAOccupationMapper.selectByExample(buddyPAOccupationExample);

		if (occupationList == null || occupationList.isEmpty()) {
			// Start Inserting
			BuddyPAOccupation buddyPAOccupation = new BuddyPAOccupation();
			String strEnable = "N";

			if (request.getParameter("occupationEnable") != null
					&& request.getParameter("occupationEnable").equalsIgnoreCase("on")) {
				strEnable = "Y";
			}

			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			buddyPAOccupation.setCompanyId(request.getParameter("company"));
			buddyPAOccupation.setOccupationCode(request.getParameter("occupationCode"));
			buddyPAOccupation.setDescription(request.getParameter("description"));
			buddyPAOccupation.setOccupationClass(request.getParameter("occupationClass"));
			buddyPAOccupation.setDeclined1(request.getParameter("declinedProduct1"));
			buddyPAOccupation.setDeclined2(request.getParameter("declinedProduct2"));
			buddyPAOccupation.setEnable(strEnable);
			buddyPAOccupation.setCreatedDt(timestamp);
			buddyPAOccupation.setCreatedBy(loginUser);
			buddyPAOccupation.setVersion((short) 1);

			try {
				buddyPAOccupationMapper.insertSelective(buddyPAOccupation);
			} catch (Exception e) {
				System.out.println("Error : Refer stack trace below.");
				e.printStackTrace();
			}

			model.addAttribute("SuccessMsg", "Data Added Successfully!");
		} else {
			model.addAttribute("Errmsg", "Duplicated Entry!");
		}

		redirctTO = buddyPAOccupationInfo(request, response, model);

		return redirctTO;

	}
	/*----------END - Occupation Add----------*/

	/*----------START - Occupation Edit----------*/
	@RequestMapping("/updateBuddyPAOccupation")
	public String updateBuddyPAOccupation(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("update Occupation");
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Buddy PA Controller loginUser" + loginUser);

		String strCompanyID = null;

		if (request.getParameter("updCompany").trim().equals("Insurance")) {
			strCompanyID = "I";
		} else if (request.getParameter("updCompany").trim().equals("Takaful")) {
			strCompanyID = "T";
		}

		// Check If Exists
		List<BuddyPAOccupation> occupationList;
		BuddyPAOccupationExample occupationListExample = new BuddyPAOccupationExample();
		BuddyPAOccupationExample.Criteria buddyPAOccupationExampleCriteria = occupationListExample.createCriteria();
		buddyPAOccupationExampleCriteria.andCompanyIdEqualTo(strCompanyID);
		buddyPAOccupationExampleCriteria.andOccupationCodeEqualTo(request.getParameter("updOccupationCode"));
		occupationList = buddyPAOccupationMapper.selectByExample(occupationListExample);

		System.out.println(strCompanyID);

		if (occupationList.size() == 1) {

			BuddyPAOccupationExample buddyPAOccupationExample = new BuddyPAOccupationExample();
			BuddyPAOccupation buddyPAOccupation = new BuddyPAOccupation();
			BuddyPAOccupationExample.Criteria occupationParamCriteria1 = buddyPAOccupationExample.createCriteria();
			occupationParamCriteria1.andCompanyIdEqualTo(strCompanyID);
			occupationParamCriteria1.andOccupationCodeEqualTo(request.getParameter("updOccupationCode"));

			String strEnable = "N";

			if (request.getParameter("updOccupationEnable") != null
					&& request.getParameter("updOccupationEnable").equalsIgnoreCase("on")) {
				strEnable = "Y";
			}

			Timestamp timestamp = new Timestamp(System.currentTimeMillis());

			buddyPAOccupation.setCompanyId(strCompanyID);
			buddyPAOccupation.setOccupationCode(request.getParameter("updOccupationCode"));
			buddyPAOccupation.setDescription(request.getParameter("updDescription"));
			buddyPAOccupation.setOccupationClass(request.getParameter("updOccupationClass"));
			buddyPAOccupation.setDeclined1(request.getParameter("updDeclinedProduct1"));
			buddyPAOccupation.setDeclined2(request.getParameter("updDeclinedProduct2"));
			buddyPAOccupation.setEnable(strEnable);
			buddyPAOccupation.setModifiedDt(timestamp);
			buddyPAOccupation.setModifiedBy(loginUser);

			int ver = Integer.parseInt(request.getParameter("updOccupationVersion")) + 1;
			buddyPAOccupation.setVersion((short) ver);

			int buddyPAOccupationUpdate1 = buddyPAOccupationMapper.updateByPrimaryKeySelective(buddyPAOccupation);
			// int buddyPAOccupationUpdate1 =
			// buddyPAOccupationMapper.updateByExampleSelective(buddyPAOccupation,
			// buddyPAOccupationExample);
			System.out.println(buddyPAOccupationUpdate1);

			model.addAttribute("SuccessMsg", "Update data successfully");
		} else {
			model.addAttribute("Errmsg", "No Record Found!");
		}

		String returnURL = null;
		returnURL = buddyPAOccupationInfo(request, response, model);

		return returnURL;

	}

	/*----------END - Occupation Edit----------*/
	/*-------------------------------------------------------START - DropDown List------------------------------------------------------------------*/
	@RequestMapping("/occCompany")
	public String occCompany(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("occCompany");

		model.addAttribute("company", request.getParameter("selectedCompany"));

		return "redirect:BuddyPAOccupationInfo";

	}
	/*-------------------------------------------------------END - DropDown List------------------------------------------------------------------*/

	/*----------START - Product Info----------*/
	@RequestMapping("/BuddyPAProductInfo")
	public String buddyPAProductInfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("Buddy PA Product Info ");
		HttpSession session = request.getSession();

		// list of companies
		List<SysParam> companyList;
		SysParamExample sysParamExample = new SysParamExample();
		SysParamExample.Criteria sysParaminfoCriteriaExist = sysParamExample.createCriteria();
		sysParaminfoCriteriaExist.andParamEqualTo("Company");
		companyList = sysParamMapper.selectByExample(sysParamExample);

		String strCompany = companyList.get(0).getName().toString();
		// check for parameter
		if (request.getParameter("company") != null) {
			strCompany = request.getParameter("company");
		}

		// list of product
		List<BuddyPAProduct> productList;
		BuddyPAProductExample buddyPAProductExample = new BuddyPAProductExample();
		BuddyPAProductExample.Criteria productParamCriteria = buddyPAProductExample.createCriteria();
		productParamCriteria.andCompanyIdEqualTo(strCompany);
		productParamCriteria.andProductIdEqualTo(strCompany.equalsIgnoreCase("I") ? "CPP" : "TPP");
		productList = buddyPAProductMapper.selectByExample(buddyPAProductExample);

		model.addAttribute("companyList", companyList);
		model.addAttribute("company", strCompany);

		FormatDates formatDate = new FormatDates();

		if (!productList.isEmpty()) {
			String productID = productList.get(0).getProductId().toString();
			String adultMinAge = productList.get(0).getAdultMinAge().toString();
			String adultMaxAge = productList.get(0).getAdultMaxAge().toString();
			String childMinAge = productList.get(0).getChildMinAgeDays().toString();
			String childMaxAge = productList.get(0).getChildMaxAge().toString();
			String studentMinAge = productList.get(0).getStudentMinAge().toString();
			String studentMaxAge = productList.get(0).getStudentMaxAge().toString();
			String maxChildNo = productList.get(0).getMaxChildNo().toString();
			String discount = productList.get(0).getStaffDiscountPercentage().toString();
			String stampDuty = productList.get(0).getStampDuty().toString();
			String gstEffDate = formatDate.dateToString(productList.get(0).getGstEffDate());
			String sstEffDate = formatDate.dateToString(productList.get(0).getSstEffDate());
			String gst = productList.get(0).getGst().toString();
			String sst = productList.get(0).getSst().toString();
			String version = productList.get(0).getVersion().toString();

			model.addAttribute("productID", productID);
			model.addAttribute("adultMinAge", adultMinAge);
			model.addAttribute("adultMaxAge", adultMaxAge);
			model.addAttribute("childMinAge", childMinAge);
			model.addAttribute("childMaxAge", childMaxAge);
			model.addAttribute("studentMinAge", studentMinAge);
			model.addAttribute("studentMaxAge", studentMaxAge);
			model.addAttribute("maxChildNo", maxChildNo);
			model.addAttribute("discount", discount);
			model.addAttribute("stampDuty", stampDuty);
			model.addAttribute("gst", gst);
			model.addAttribute("gstEffDate", gstEffDate);
			model.addAttribute("sst", sst);
			model.addAttribute("sstEffDate", sstEffDate);
			model.addAttribute("version", version);

			String strExist = null;

			if (productID != null) {
				strExist = "1";
			} else {
				strExist = "0";
			}

			model.addAttribute("exist", strExist);
		} else {
			System.out.println("Product List Size Error");
		}

		return "/products/buddyPA/buddyPA-product-mng-add";

	}
	/*----------END - Product Info----------*/

	/*----------START - Product Edit----------*/
	@RequestMapping("/updateBuddyPAProduct")
	public String updateBuddyPAProduct(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("update Product");
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Buddy PA Controller loginUser" + loginUser);

		BuddyPAProductExample buddyPAProductExample = new BuddyPAProductExample();
		BuddyPAProduct buddyPAProduct = new BuddyPAProduct();
		BuddyPAProductExample.Criteria productParamCriteria = buddyPAProductExample.createCriteria();
		productParamCriteria.andCompanyIdEqualTo(request.getParameter("company"));
		productParamCriteria.andProductIdEqualTo(request.getParameter("productID"));
		List<BuddyPAProduct> productList = new ArrayList<>();

		Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		buddyPAProduct.setCompanyId(request.getParameter("company"));
		buddyPAProduct.setProductId(request.getParameter("productID"));
		buddyPAProduct.setAdultMinAge(request.getParameter("adultMinAge"));
		buddyPAProduct.setAdultMaxAge(request.getParameter("adultMaxAge"));
		buddyPAProduct.setChildMinAgeDays(request.getParameter("childMinAge"));
		buddyPAProduct.setChildMaxAge(request.getParameter("childMaxAge"));
		buddyPAProduct.setStudentMinAge(request.getParameter("studentMinAge"));
		buddyPAProduct.setStudentMaxAge(request.getParameter("studentMaxAge"));

		FormatDates formatDates = new FormatDates();
		int maxChildNo = Integer.parseInt(request.getParameter("maxChildNo"));
		buddyPAProduct.setMaxChildNo((short) maxChildNo);
		BigDecimal discount = new BigDecimal(request.getParameter("discount"));
		buddyPAProduct.setStaffDiscountPercentage(discount);
		BigDecimal stampDuty = new BigDecimal(request.getParameter("stampDuty"));
		buddyPAProduct.setStampDuty(stampDuty);
		Date gstEffDate = formatDates.stringToDate(request.getParameter("gstEffDate"));
		buddyPAProduct.setGstEffDate(gstEffDate);
		BigDecimal gst = new BigDecimal(request.getParameter("gst"));
		buddyPAProduct.setGst(gst);
		Date sstEffDate = formatDates.stringToDate(request.getParameter("sstEffDate"));
		buddyPAProduct.setSstEffDate(sstEffDate);
		BigDecimal sst = new BigDecimal(request.getParameter("sst"));
		buddyPAProduct.setSst(sst);

		int ver = Integer.parseInt(request.getParameter("version")) + 1;
		buddyPAProduct.setVersion((short) ver);

		String strExist = request.getParameter("exist");

		// approval log
		convertBytes process = new convertBytes();

		List<BuddyPAProduct> listBuddyProductChange = new ArrayList<>();
		listBuddyProductChange.add(buddyPAProduct);

		try {
			byte[] changeBlob = process.convertToBytes(listBuddyProductChange);

			ApprovalExample approvalExample = new ApprovalExample();
			com.spring.VO.ApprovalExample.Criteria createCriteriaApprovalExample = approvalExample.createCriteria();
			createCriteriaApprovalExample.andDescriptionEqualTo("BUDDY - CHANGE PRODUCT INFORMATION");
			List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

			System.out.println("BUDDY change Data" + changeBlob);

			BigDecimal approvalProdId = new BigDecimal(0);
			if (!listapproval.isEmpty()) {
				approvalProdId = listapproval.get(0).getId();
			}

			int maker = Integer.parseInt(loginUser);

			// setting all new change value to ApprovalLog POJA and Insert to ApprovalLog
			// table
			ApprovalLog approvalLog = new ApprovalLog();
			approvalLog.setNewContent(changeBlob);
			approvalLog.setApprovalId((short) approvalProdId.intValue()); // from Approval table this value[2] based on
																			// menu in feature
			approvalLog.setMaker((short) maker); // session login value
			// approvalLog.setChecker((short)1); //while insert record no need checker value
			approvalLog.setStatus("1");
			approvalLog.setCreateDate(new Date());
			approvalLog.setUpdateDate(new Date());
			approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));

			if (strExist.trim().equals("1")) {
				productList = buddyPAProductMapper.selectByExample(buddyPAProductExample);
				byte[] originalBlob = process.convertToBytes(productList);
				System.out.println("BUDDY ORI Data" + originalBlob);
				approvalLog.setOriginalContent(originalBlob);
				buddyPAProduct.setModifiedDt(timestamp);
				buddyPAProduct.setModifiedBy(loginUser);
				// int buddyPAProductUpdate1 =
				// buddyPAProductMapper.updateByExampleSelective(buddyPAProduct,
				// buddyPAProductExample);
				// System.out.println("Updating Product " + buddyPAProductUpdate1);
				// model.addAttribute("SuccessMsg", "Update data successfully");
			} else {
				buddyPAProduct.setCreatedDt(timestamp);
				buddyPAProduct.setCreatedBy(loginUser);
				// buddyPAProductMapper.insert(buddyPAProduct);
				// System.out.println("Adding Product");
				// model.addAttribute("SuccessMsg", "Data Added Successfully!");
			}
			int status = approvalLogMapper.insert(approvalLog);
			model.addAttribute("SuccessMsg", "Submitted data successfully");

		} catch (IOException ex) {
			ex.printStackTrace();
		}
		// approval log

		String returnURL = null;

		returnURL = buddyPAProductInfo(request, response, model);
		return returnURL;

	}
	/*----------END - Product Edit----------*/

	/*----------START - Company Index Change----------*/
	@RequestMapping("/companyIndexChange")
	public String companyIndexChange(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("CompanyIndexChange");

		model.addAttribute("company", request.getParameter("selectedCompany"));

		return "redirect: BuddyPAProductInfo";

	}
	/*----------END - Company Index Change----------*/
	/*------------------------------------------------------END - Occupation / System Setup Info---------------------------------------------------------*/

	// Yng Yng (22052018)
	/*------------------------------------------------------START - Plan Info---------------------------------------------------------*/
	/*----------START - Plan List----------*/
	@RequestMapping("/BuddyPAPlanInfo")
	public String buddyPAPlanInfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("Buddy PA Plan Info ");
		HttpSession session = request.getSession();

		List<BuddyPAPlan> planList;
		BuddyPAPlanExample buddyPAPlanExample = new BuddyPAPlanExample();
		buddyPAPlanExample.setOrderByClause("PLAN_ID");

		planList = buddyPAPlanMapper.selectByExample(buddyPAPlanExample);

		model.addAttribute("planList", planList);

		return "/products/buddyPA/buddyPA-plan-mng-add";

	}
	/*----------END - Plan List----------*/

	/*----------START - Plan Add----------*/
	@RequestMapping("/addBuddyPAPlan")
	public String addBuddyPAPlan(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("add Plan");
		String redirctTO = null;
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Buddy PA Controller loginUser" + loginUser);

		List<BuddyPAPlan> planList;
		BuddyPAPlanExample buddyPAPlanExample = new BuddyPAPlanExample();
		BuddyPAPlanExample.Criteria buddyPAPlanExampleCriteria = buddyPAPlanExample.createCriteria();
		buddyPAPlanExampleCriteria.andPlanIdEqualTo(request.getParameter("planID"));

		planList = buddyPAPlanMapper.selectByExample(buddyPAPlanExample);

		if (planList == null || planList.isEmpty()) {
			BuddyPAPlan buddyPAPlan = new BuddyPAPlan();
			String strEnable = "N";

			if (request.getParameter("planEnable") != null
					&& request.getParameter("planEnable").equalsIgnoreCase("on")) {
				strEnable = "Y";
			}

			Timestamp timestamp = new Timestamp(System.currentTimeMillis());

			buddyPAPlan.setPlanId(request.getParameter("planID"));
			buddyPAPlan.setNameEn(request.getParameter("planNameEn"));
			buddyPAPlan.setNameBm(request.getParameter("planNameBm"));
			buddyPAPlan.setEnable(strEnable);
			buddyPAPlan.setVersion((short) 1);
			buddyPAPlan.setCreatedBy(loginUser);
			buddyPAPlan.setCreatedDt(timestamp);

			// approval log
			convertBytes process = new convertBytes();

			List<BuddyPAPlan> listBuddyPlanChange = new ArrayList<>();
			listBuddyPlanChange.add(buddyPAPlan);

			try {
				byte[] changeBlob = process.convertToBytes(listBuddyPlanChange);

				ApprovalExample approvalExample = new ApprovalExample();
				com.spring.VO.ApprovalExample.Criteria createCriteriaApprovalExample = approvalExample.createCriteria();
				createCriteriaApprovalExample.andDescriptionEqualTo("BUDDY - CHANGE PLAN INFORMATION");
				List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

				System.out.println("BUDDY change Data" + changeBlob);

				BigDecimal approvalProdId = new BigDecimal(0);
				if (!listapproval.isEmpty()) {
					approvalProdId = listapproval.get(0).getId();
				}

				int maker = Integer.parseInt(loginUser);

				// setting all new change value to ApprovalLog POJA and Insert to ApprovalLog
				// table
				ApprovalLog approvalLog = new ApprovalLog();
				approvalLog.setNewContent(changeBlob);
				approvalLog.setApprovalId((short) approvalProdId.intValue()); // from Approval table this value[2] based
																				// on menu in feature
				approvalLog.setMaker((short) maker); // session login value
				// approvalLog.setChecker((short)1); //while insert record no need checker value
				approvalLog.setStatus("1");
				approvalLog.setCreateDate(new Date());
				approvalLog.setUpdateDate(new Date());
				approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
				int status = approvalLogMapper.insert(approvalLog);
				model.addAttribute("SuccessMsg", "Submitted data successfully");
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			// approval log

			// try {
			// buddyPAPlanMapper.insertSelective(buddyPAPlan);
			// } catch (Exception e) {
			// System.out.println("Error : Refer stack trace below.");
			// e.printStackTrace();
			// }

			model.addAttribute("SuccessMsg", "Data Added Successfully!");
		} else {
			model.addAttribute("Errmsg", "Duplicated Entry!");
		}

		redirctTO = buddyPAPlanInfo(request, response, model);

		return redirctTO;

	}
	/*----------END - Plan Add----------*/

	/*----------START - Plan Edit----------*/
	@RequestMapping("/updateBuddyPAPlan")
	public String updateBuddyPAPlan(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("update Plan");
		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Buddy PA Controller loginUser" + loginUser);

		List<BuddyPAPlan> planList;
		BuddyPAPlanExample planListExample = new BuddyPAPlanExample();
		BuddyPAPlanExample.Criteria buddyPAPlanExampleCriteria = planListExample.createCriteria();
		// buddyPAPlanExample_criteria.andNameEnEqualTo(request.getParameter("updPlanNameEn"));
		// buddyPAPlanExample_criteria.andNameBmEqualTo(request.getParameter("updPlanNameBm"));
		buddyPAPlanExampleCriteria.andPlanIdEqualTo(request.getParameter("updPlanID"));

		planList = buddyPAPlanMapper.selectByExample(planListExample);

		if (planList != null || !planList.isEmpty()) {

			BuddyPAPlanExample buddyPAPlanExample = new BuddyPAPlanExample();
			BuddyPAPlan buddyPAPlan = new BuddyPAPlan();
			BuddyPAPlanExample.Criteria planParamCriteria1 = buddyPAPlanExample.createCriteria();
			planParamCriteria1.andPlanIdEqualTo(request.getParameter("updPlanID"));
			String strEnable = "N";

			if (request.getParameter("updPlanEnable") != null
					&& request.getParameter("updPlanEnable").equalsIgnoreCase("on")) {
				strEnable = "Y";
			}

			if (!request.getParameter("updPlanNameEn").isEmpty() || !request.getParameter("updPlanNameBm").isEmpty()) {

				Timestamp timestamp = new Timestamp(System.currentTimeMillis());

				buddyPAPlan.setPlanId(request.getParameter("updPlanID"));
				buddyPAPlan.setNameEn(request.getParameter("updPlanNameEn"));
				buddyPAPlan.setNameBm(request.getParameter("updPlanNameBm"));
				buddyPAPlan.setEnable(strEnable);
				buddyPAPlan.setVersion((short) (Integer.parseInt(request.getParameter("updPlanVersion")) + 1));
				buddyPAPlan.setModifiedBy(loginUser);
				buddyPAPlan.setModifiedDt(timestamp);

				// int buddyPAPlanUpdate1 =
				// buddyPAPlanMapper.updateByPrimaryKeySelective(buddyPAPlan);
				// System.out.println(buddyPAPlanUpdate1);

				// approval log
				convertBytes process = new convertBytes();

				List<BuddyPAPlan> listBuddyPlanChange = new ArrayList<>();
				listBuddyPlanChange.add(buddyPAPlan);

				try {
					byte[] changeBlob = process.convertToBytes(listBuddyPlanChange);
					byte[] originalBlob = process.convertToBytes(planList);

					ApprovalExample approvalExample = new ApprovalExample();
					com.spring.VO.ApprovalExample.Criteria createCriteriaApprovalExample = approvalExample
							.createCriteria();
					createCriteriaApprovalExample.andDescriptionEqualTo("BUDDY - CHANGE PLAN INFORMATION");
					List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

					System.out.println("BUDDY ORI Data" + originalBlob);
					System.out.println("BUDDY change Data" + changeBlob);

					BigDecimal approvalProdId = new BigDecimal(0);
					if (!listapproval.isEmpty()) {
						approvalProdId = listapproval.get(0).getId();
					}

					int maker = Integer.parseInt(loginUser);

					// setting all new change value to ApprovalLog POJA and Insert to ApprovalLog
					// table
					ApprovalLog approvalLog = new ApprovalLog();
					approvalLog.setOriginalContent(originalBlob);
					approvalLog.setNewContent(changeBlob);
					approvalLog.setApprovalId((short) approvalProdId.intValue()); // from Approval table this value[2]
																					// based on menu in feature
					approvalLog.setMaker((short) maker); // session login value
					// approvalLog.setChecker((short)1); //while insert record no need checker value
					approvalLog.setStatus("1");
					approvalLog.setCreateDate(new Date());
					approvalLog.setUpdateDate(new Date());
					approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
					int status = approvalLogMapper.insert(approvalLog);
					model.addAttribute("SuccessMsg", "Submitted data successfully");
				} catch (IOException ex) {
					ex.printStackTrace();
				}
				// approval log
			} else {

				model.addAttribute("Errmsg", "Error Message: Please fill in Mandatory Field(s)!");

			}
		} else {

			model.addAttribute("Errmsg", "Error Message: Duplicate Entry!");

		}

		return buddyPAPlanInfo(request, response, model);
	}
	/*----------END - Plan Edit----------*/
	/*------------------------------------------------------END - Plan Info---------------------------------------------------------*/

	/*------------------------------------------------------START - Plan Group Info---------------------------------------------------------*/
	/*----------START - Plan Group List----------*/
	@RequestMapping("/BuddyPAPlanGrpInfo")
	public String buddyPAPlanGrpInfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("Buddy PA Plan Grp Info ");
		HttpSession session = request.getSession();

		// list of companies
		List<SysParam> companyList;
		SysParamExample sysParamExample = new SysParamExample();

		SysParamExample.Criteria sysParaminfoCriteriaExist = sysParamExample.createCriteria();
		sysParaminfoCriteriaExist.andParamEqualTo("Company");
		companyList = sysParamMapper.selectByExample(sysParamExample);
		String strCompany = companyList.get(0).getName().toString();

		// check for parameter
		if (request.getParameter("company") != null) {
			strCompany = request.getParameter("company");
		}

		// list of combos
		List<BuddyPACombo> comboList;
		BuddyPAComboExample buddyPAComboExample = new BuddyPAComboExample();

		BuddyPAComboExample.Criteria buddyPAComboCriteriaExist = buddyPAComboExample.createCriteria();
		buddyPAComboCriteriaExist.andCompanyIdEqualTo(strCompany); // default set to the first on the
																	// list
		comboList = buddyPAComboMapper.selectByExample(buddyPAComboExample);

		// list of plans
		List<BuddyPAPlan> planList;
		BuddyPAPlanExample buddyPAPlanExample = new BuddyPAPlanExample();

		planList = buddyPAPlanMapper.selectByExample(buddyPAPlanExample);

		// plan group list
		List<BuddyPAPlanGrpList> planGrpList;
		BuddyPADAO buddyPADAO = new BuddyPADAOImpl();
		planGrpList = buddyPADAO.getPlanGrpList(strCompany);

		model.addAttribute("companyList", companyList);
		model.addAttribute("comboList", comboList);
		model.addAttribute("planList", planList);
		model.addAttribute("planGrpList", planGrpList);
		model.addAttribute("company", strCompany);

		return "/products/buddyPA/buddyPA-plan-grp-mng-add";

	}
	/*----------END - Plan Group List----------*/

	/*----------START - Plan Group Add----------*/
	@RequestMapping("/addBuddyPAPlanGrp")
	public String addBuddyPAPlanGrp(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("add Plan Grp");
		String redirctTO = null;
		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Buddy PA Controller loginUser" + loginUser);

		List<BuddyPAPlanGrp> planGrpList;
		BuddyPAPlanGrpExample selectPlanGrpExample = new BuddyPAPlanGrpExample();

		BuddyPAPlanGrpExample.Criteria buddyPAPlanGrpCriteriaExist = selectPlanGrpExample.createCriteria();
		buddyPAPlanGrpCriteriaExist.andComboIdEqualTo(request.getParameter("ddlComboList"));
		buddyPAPlanGrpCriteriaExist.andCompanyIdEqualTo(request.getParameter("ddlCompanyList"));
		buddyPAPlanGrpCriteriaExist.andPlanIdEqualTo(request.getParameter("ddlPlanList"));
		planGrpList = buddyPAPlanGrpMapper.selectByExample(selectPlanGrpExample);

		if (planGrpList == null || planGrpList.isEmpty()) {

			BuddyPAPlanGrp buddyPAPlanGrp = new BuddyPAPlanGrp();
			String strEnable = "N";

			if (request.getParameter("planEnable") != null
					&& request.getParameter("planEnable").equalsIgnoreCase("on")) {
				strEnable = "Y";
			}

			if (!request.getParameter("ddlCompanyList").isEmpty() && !request.getParameter("ddlPlanList").isEmpty()
					&& !request.getParameter("ddlComboList").isEmpty()) {
				buddyPAPlanGrp.setCompanyId(request.getParameter("ddlCompanyList"));
				buddyPAPlanGrp.setPlanId(request.getParameter("ddlPlanList"));
				buddyPAPlanGrp.setComboId(request.getParameter("ddlComboList"));
				buddyPAPlanGrp.setEnable(strEnable);

				try {
					buddyPAPlanGrpMapper.insert(buddyPAPlanGrp);
				} catch (Exception e) {
					System.out.println("Error : Refer stack trace below.");
					e.printStackTrace();
				}

				model.addAttribute("SuccessMsg", "Data Added Successfully!");

			}
		} else {

			model.addAttribute("Errmsg", "Duplicated entry!");

		}

		redirctTO = buddyPAPlanGrpInfo(request, response, model);

		return redirctTO;

	}
	/*----------END - Plan Group Add----------*/

	/*----------START - Plan Group Edit----------*/
	@RequestMapping("/updateBuddyPAPlanGrp")
	public String updateBuddyPAPlanGrp(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("update Plan Grp");
		String redirctTO = null;
		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Buddy PA Controller loginUser" + loginUser);

		List<BuddyPAPlanGrp> planGrpList;
		BuddyPAPlanGrpExample selectPlanGrpExample = new BuddyPAPlanGrpExample();

		BuddyPAPlanGrpExample.Criteria buddyPAPlanGrpCriteriaExist = selectPlanGrpExample.createCriteria();
		buddyPAPlanGrpCriteriaExist.andComboIdEqualTo(request.getParameter("updComboList"));
		buddyPAPlanGrpCriteriaExist.andCompanyIdEqualTo(request.getParameter("updCompanyList"));
		buddyPAPlanGrpCriteriaExist.andPlanIdEqualTo(request.getParameter("updPlanList"));
		planGrpList = buddyPAPlanGrpMapper.selectByExample(selectPlanGrpExample);

		if (planGrpList == null || planGrpList.isEmpty()
				|| !planGrpList.get(0).getEnable().equals(request.getParameter("oriEnable"))) {

			BuddyPAPlanGrp buddyPAPlanGrp = new BuddyPAPlanGrp();
			BuddyPAPlanGrpExample buddyPAPlanGrpExample = new BuddyPAPlanGrpExample();
			BuddyPAPlanGrpExample.Criteria planGrpCriteria = buddyPAPlanGrpExample.createCriteria();
			planGrpCriteria.andComboIdEqualTo(request.getParameter("updComboList"));
			planGrpCriteria.andCompanyIdEqualTo(request.getParameter("updCompanyList"));
			planGrpCriteria.andPlanIdEqualTo(request.getParameter("oriPlanId"));
			String strEnable = "N";

			if (request.getParameter("enable") != null && request.getParameter("enable").equalsIgnoreCase("on")) {
				strEnable = "Y";
			}

			buddyPAPlanGrp.setPlanId(request.getParameter("updPlanList"));
			buddyPAPlanGrp.setEnable(strEnable);

			try {
				int buddyPAPlanUpdate1 = buddyPAPlanGrpMapper.updateByExampleSelective(buddyPAPlanGrp,
						buddyPAPlanGrpExample);
				System.out.println(buddyPAPlanUpdate1);
				model.addAttribute("SuccessMsg", "Data Updated Successfully!");
			} catch (Exception e) {
				System.out.println("Error : Refer stack trace below.");
				e.printStackTrace();
			}

		} else {

			model.addAttribute("Errmsg", "Duplicated entry!");

		}
		redirctTO = buddyPAPlanGrpInfo(request, response, model);

		return redirctTO;
	}
	/*-------------------------------------------------------END - Plan Group Edit------------------------------------------------------------------*/

	/*-------------------------------------------------------START - DropDown List------------------------------------------------------------------*/
	@RequestMapping("/pgCompany")
	public String pgCompany(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("pgCompany");

		model.addAttribute("company", request.getParameter("pgCompanyId"));

		return "redirect:BuddyPAPlanGrpInfo";

	}
	/*-------------------------------------------------------END - DropDown List------------------------------------------------------------------*/
	/*-------------------------------------------------------END - Plan Group Info------------------------------------------------------------------*/

	// Kok Wei (21052018)
	/*-------------------------------------------------------END - Benefit Info------------------------------------------------------------------*/
	/*-------------------------------------------------------START - Benefit List------------------------------------------------------------------*/
	@RequestMapping("/BuddyPABenefitList")
	public String buddyPABenefitList(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("Buddy PA Benefit List");

		HttpSession session = request.getSession();

		// List of companies
		List<SysParam> companyList;
		SysParamExample sysParamExample = new SysParamExample();

		SysParamExample.Criteria sysParaminfoCriteriaExist = sysParamExample.createCriteria();
		sysParaminfoCriteriaExist.andParamEqualTo("Company");
		companyList = sysParamMapper.selectByExample(sysParamExample);

		String strCompany = companyList.get(0).getName().toString();
		// check for parameter
		if (request.getParameter("company") != null) {
			strCompany = request.getParameter("company");
		}

		// List of benefit
		List<BuddyPABenefit> benefitList;
		BuddyPABenefitExample buddyPABenefitExample = new BuddyPABenefitExample();
		BuddyPABenefitExample.Criteria buddyPABenefitCriteria = buddyPABenefitExample.createCriteria();
		buddyPABenefitCriteria.andCompanyIdEqualTo(strCompany);

		buddyPABenefitExample.setOrderByClause("BENEFIT_ID");
		benefitList = buddyPABenefitMapper.selectByExample(buddyPABenefitExample);

		// Give data to ID
		model.addAttribute("companyList", companyList);
		model.addAttribute("benefitList", benefitList);
		model.addAttribute("company", strCompany);

		return "/products/buddyPA/buddyPA_benefit_mng_add";

	}
	/*-------------------------------------------------------END - Benefit List------------------------------------------------------------------*/

	/*-------------------------------------------------------START - Benefit Add------------------------------------------------------------------*/
	@RequestMapping("/addBuddyPABenefit")
	public String addBuddyPABenefit(HttpServletRequest request, HttpServletResponse response, Model model) {

		// System.out.println("Add Benefit");
		String redirctTO = null;
		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Buddy PA Controller loginUser" + loginUser);

		// List of the Benefit (For Check Duplicate)
		List<BuddyPABenefit> benefitList;
		BuddyPABenefitExample buddyPABenefitExample = new BuddyPABenefitExample();
		buddyPABenefitExample.setOrderByClause("BENEFIT_ID");
		BuddyPABenefitExample.Criteria buddyPABenefitCriteriaExist = buddyPABenefitExample.createCriteria();
		buddyPABenefitCriteriaExist.andCompanyIdEqualTo(request.getParameter("companyId").trim());
		buddyPABenefitCriteriaExist.andNameEnEqualTo(request.getParameter("benefitNameEn").trim());
		buddyPABenefitCriteriaExist.andNameBmEqualTo(request.getParameter("benefitNameBm").trim());
		benefitList = buddyPABenefitMapper.selectByExample(buddyPABenefitExample);

		if (benefitList == null || benefitList.isEmpty()) {

			BuddyPABenefit buddyPABenefit = new BuddyPABenefit();

			String strInsuredMale = "N";
			String strInsuredFemale = "N";
			String strSpouseMale = "N";
			String strSpouseFemale = "N";
			String strChildEg = "N";
			String strEnable = "N";

			if (request.getParameter("insuredMaleEnable") != null
					&& request.getParameter("insuredMaleEnable").equalsIgnoreCase("on")) {
				strInsuredMale = "Y";
			}
			if (request.getParameter("insuredFemaleEnable") != null
					&& request.getParameter("insuredFemaleEnable").equalsIgnoreCase("on")) {
				strInsuredFemale = "Y";
			}
			if (request.getParameter("spouseMaleEnable") != null
					&& request.getParameter("spouseMaleEnable").equalsIgnoreCase("on")) {
				strSpouseMale = "Y";
			}
			if (request.getParameter("spouseFemaleEnable") != null
					&& request.getParameter("spouseFemaleEnable").equalsIgnoreCase("on")) {
				strSpouseFemale = "Y";
			}
			if (request.getParameter("eligibilityEnable") != null
					&& request.getParameter("eligibilityEnable").equalsIgnoreCase("on")) {
				strChildEg = "Y";
			}
			if (request.getParameter("benefitEnable") != null
					&& request.getParameter("benefitEnable").equalsIgnoreCase("on")) {
				strEnable = "Y";
			}

			Timestamp timestamp = new Timestamp(System.currentTimeMillis());

			buddyPABenefit.setCompanyId(request.getParameter("companyId").trim());
			buddyPABenefit.setNameEn(request.getParameter("benefitNameEn").trim());
			buddyPABenefit.setNameBm(request.getParameter("benefitNameBm").trim());
			buddyPABenefit.setAdultRate(new BigDecimal(request.getParameter("adultRate")));
			buddyPABenefit.setInsuredMElig(strInsuredMale.trim());
			buddyPABenefit.setInsuredFElig(strInsuredFemale.trim());
			buddyPABenefit.setSpouseMElig(strSpouseMale.trim());
			buddyPABenefit.setSpouseFElig(strSpouseFemale.trim());
			buddyPABenefit.setChildRate(new BigDecimal(request.getParameter("childRate")));
			buddyPABenefit.setChildElig(strChildEg.trim());
			buddyPABenefit.setEnable(strEnable.trim());
			buddyPABenefit.setVersion((short) 1);
			buddyPABenefit.setCreatedBy(loginUser);
			buddyPABenefit.setCreatedDt(timestamp);

			// approval log
			convertBytes process = new convertBytes();

			List<BuddyPABenefit> listBuddyBenefitChange = new ArrayList<>();
			listBuddyBenefitChange.add(buddyPABenefit);

			try {
				byte[] changeBlob = process.convertToBytes(listBuddyBenefitChange);

				ApprovalExample approvalExample = new ApprovalExample();
				com.spring.VO.ApprovalExample.Criteria createCriteriaApprovalExample = approvalExample.createCriteria();
				createCriteriaApprovalExample.andDescriptionEqualTo("BUDDY - CHANGE BENEFIT INFORMATION");
				List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

				System.out.println("BUDDY change Data" + changeBlob);

				BigDecimal approvalProdId = new BigDecimal(0);
				if (!listapproval.isEmpty()) {
					approvalProdId = listapproval.get(0).getId();
				}

				int maker = Integer.parseInt(loginUser);

				// setting all new change value to ApprovalLog POJA and Insert to ApprovalLog
				// table
				ApprovalLog approvalLog = new ApprovalLog();
				approvalLog.setNewContent(changeBlob);
				approvalLog.setApprovalId((short) approvalProdId.intValue()); // from Approval table this value[2] based
																				// on menu in feature
				approvalLog.setMaker((short) maker); // session login value
				// approvalLog.setChecker((short)1); //while insert record no need checker value
				approvalLog.setStatus("1");
				approvalLog.setCreateDate(new Date());
				approvalLog.setUpdateDate(new Date());
				approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
				int status = approvalLogMapper.insert(approvalLog);
				model.addAttribute("SuccessMsg", "Submitted data successfully");
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			// approval log

			// try {
			// buddyPABenefitMapper.insertSelective(buddyPABenefit);
			// model.addAttribute("SuccessMsg", "Data Added Successfully!");
			// } catch (Exception e) {
			// // System.out.println("Error : Refer stack trace below.");
			// e.printStackTrace();
			// }

		} else {
			model.addAttribute("Errmsg", "Duplicated Record Entered!");
		}

		// Call Post Back
		redirctTO = buddyPABenefitList(request, response, model);

		return redirctTO;

	}
	/*-------------------------------------------------------END - Benefit Add------------------------------------------------------------------*/

	/*-------------------------------------------------------START - Benefit Edit------------------------------------------------------------------*/
	@RequestMapping("/updateBuddyPABenefit")
	public String editBuddyPABenefit(HttpServletRequest request, HttpServletResponse response, Model model) {

		// System.out.println("Edit Benefit");
		String returnURL = null;
		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Admin Buddy PA Controller loginUser" + loginUser);

		// List of the Benefit (For Check Duplicate)
		List<BuddyPABenefit> benefitList;
		BuddyPABenefitExample buddyPABenefitExample = new BuddyPABenefitExample();
		BuddyPABenefitExample.Criteria buddyPABenefitCriteriaExist = buddyPABenefitExample.createCriteria();
		buddyPABenefitCriteriaExist.andBenefitIdEqualTo(Integer.parseInt(request.getParameter("updBenefitID")));
		benefitList = buddyPABenefitMapper.selectByExample(buddyPABenefitExample);

		if (benefitList != null && !benefitList.isEmpty()) {

			BuddyPABenefit buddyPABenefit = new BuddyPABenefit();

			String strInsuredMale = "N";
			String strInsuredFemale = "N";
			String strSpouseMale = "N";
			String strSpouseFemale = "N";
			String strChildEg = "N";
			String strEnable = "N";

			if (request.getParameter("updInsuredMaleEnable") != null
					&& request.getParameter("updInsuredMaleEnable").equalsIgnoreCase("on")) {
				strInsuredMale = "Y";
			}
			if (request.getParameter("updInsuredFemaleEnable") != null
					&& request.getParameter("updInsuredFemaleEnable").equalsIgnoreCase("on")) {
				strInsuredFemale = "Y";
			}
			if (request.getParameter("updSpouseMaleEnable") != null
					&& request.getParameter("updSpouseMaleEnable").equalsIgnoreCase("on")) {
				strSpouseMale = "Y";
			}
			if (request.getParameter("updSpouseFemaleEnable") != null
					&& request.getParameter("updSpouseFemaleEnable").equalsIgnoreCase("on")) {
				strSpouseFemale = "Y";
			}
			if (request.getParameter("updEligibilityEnable") != null
					&& request.getParameter("updEligibilityEnable").equalsIgnoreCase("on")) {
				strChildEg = "Y";
			}
			if (request.getParameter("updBenefitEnable") != null
					&& request.getParameter("updBenefitEnable").equalsIgnoreCase("on")) {
				strEnable = "Y";
			}

			if (!request.getParameter("updBenefitNameEn").isEmpty()
					&& !request.getParameter("updBenefitNameBm").isEmpty()
					&& !request.getParameter("updAdultRate").isEmpty()
					&& !request.getParameter("updChildRate").isEmpty()) {

				Timestamp timestamp = new Timestamp(System.currentTimeMillis());

				buddyPABenefit.setCompanyId(request.getParameter("updCompany").trim());
				buddyPABenefit.setNameEn(request.getParameter("updBenefitNameEn").trim());
				buddyPABenefit.setNameBm(request.getParameter("updBenefitNameBm").trim());
				buddyPABenefit.setAdultRate(new BigDecimal(request.getParameter("updAdultRate")));
				buddyPABenefit.setInsuredMElig(strInsuredMale.trim());
				buddyPABenefit.setInsuredFElig(strInsuredFemale.trim());
				buddyPABenefit.setSpouseMElig(strSpouseMale.trim());
				buddyPABenefit.setSpouseFElig(strSpouseFemale.trim());
				buddyPABenefit.setChildRate(new BigDecimal(request.getParameter("updChildRate")));
				buddyPABenefit.setChildElig(strChildEg.trim());
				buddyPABenefit.setEnable(strEnable.trim());
				buddyPABenefit.setVersion((short) (benefitList.get(0).getVersion() + 1));
				buddyPABenefit.setModifiedBy(loginUser);
				buddyPABenefit.setModifiedDt(timestamp);
				buddyPABenefit.setBenefitId(Integer.parseInt(request.getParameter("updBenefitID").trim()));

				// approval log
				convertBytes process = new convertBytes();

				List<BuddyPABenefit> listBuddyBenefitChange = new ArrayList<>();
				listBuddyBenefitChange.add(buddyPABenefit);

				try {
					byte[] changeBlob = process.convertToBytes(listBuddyBenefitChange);
					byte[] originalBlob = process.convertToBytes(benefitList);

					ApprovalExample approvalExample = new ApprovalExample();
					com.spring.VO.ApprovalExample.Criteria createCriteriaApprovalExample = approvalExample
							.createCriteria();
					createCriteriaApprovalExample.andDescriptionEqualTo("BUDDY - CHANGE BENEFIT INFORMATION");
					List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

					System.out.println("BUDDY ORI Data" + originalBlob);
					System.out.println("BUDDY change Data" + changeBlob);

					BigDecimal approvalProdId = new BigDecimal(0);
					if (!listapproval.isEmpty()) {
						approvalProdId = listapproval.get(0).getId();
					}

					int maker = Integer.parseInt(loginUser);

					// setting all new change value to ApprovalLog POJA and Insert to ApprovalLog
					// table
					ApprovalLog approvalLog = new ApprovalLog();
					approvalLog.setOriginalContent(originalBlob);
					approvalLog.setNewContent(changeBlob);
					approvalLog.setApprovalId((short) approvalProdId.intValue()); // from Approval table this value[2]
																					// based on menu in feature
					approvalLog.setMaker((short) maker); // session login value
					// approvalLog.setChecker((short)1); //while insert record no need checker value
					approvalLog.setStatus("1");
					approvalLog.setCreateDate(new Date());
					approvalLog.setUpdateDate(new Date());
					approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
					int status = approvalLogMapper.insert(approvalLog);
					model.addAttribute("SuccessMsg", "Submitted data successfully");
				} catch (IOException ex) {
					ex.printStackTrace();
				}
				// approval log

				// try {
				// // Update According to Benefit ID
				// int buddyPABenefitUpdate =
				// buddyPABenefitMapper.updateByExampleSelective(buddyPABenefit,buddyPABenefitExample);
				//
				// model.addAttribute("SuccessMsg", "Update Data Successfully!");
				// // System.out.println(buddyPABenefitUpdate);
				// } catch (Exception e) {
				// // System.out.println("Error : Refer stack trace below.");
				// e.printStackTrace();
				// }
			}

			else {
				model.addAttribute("Errmsg", "Error Message: Please fill in Mandatory Field(s)!");
			}
		} else {
			model.addAttribute("Errmsg", "Error Message: No Record Found!");
		}

		// Call Post Back
		returnURL = buddyPABenefitList(request, response, model);

		return returnURL;

	}

	/*----------END - Benefit Edit----------*/
	/*-------------------------------------------------------START - DropDown List------------------------------------------------------------------*/
	@RequestMapping("/blCompany")
	public String blCompany(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("blCompany");

		model.addAttribute("company", request.getParameter("selectedCompany"));

		return "redirect:BuddyPABenefitList";

	}
	/*-------------------------------------------------------END - DropDown List------------------------------------------------------------------*/
	/*------------------------------------------------------END - Benefit Info---------------------------------------------------------*/

	// Yng Yng (23052018)
	/*------------------------------------------------------START - Benefit Grp Info---------------------------------------------------------*/
	@RequestMapping("/BuddyPABenefitGrpList")
	public String buddyPABenefitGrpList(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("Buddy PA Benefit Grp Info ");
		HttpSession session = request.getSession();

		String strCompany = null;
		String strComboId = null;
		String strPlanId = null;

		// list of companies
		List<SysParam> companyList;
		SysParamExample sysParamExample = new SysParamExample();

		SysParamExample.Criteria sysParaminfoCriteriaExist = sysParamExample.createCriteria();
		sysParaminfoCriteriaExist.andParamEqualTo("Company");
		companyList = sysParamMapper.selectByExample(sysParamExample);

		// check for company parameter
		if (request.getParameter("company") != null) {
			strCompany = request.getParameter("company");
		} else {
			if (companyList != null && !companyList.isEmpty()) {
				strCompany = companyList.get(0).getName().toString();
			}
		}

		// list of combos
		List<BuddyPACombo> comboList;
		BuddyPAComboExample buddyPAComboExample = new BuddyPAComboExample();

		BuddyPAComboExample.Criteria buddyPAComboCriteriaExist = buddyPAComboExample.createCriteria();
		buddyPAComboCriteriaExist.andCompanyIdEqualTo(strCompany); // default set to the first on the
																	// list
		comboList = buddyPAComboMapper.selectByExample(buddyPAComboExample);

		// check for combo parameter
		if (request.getParameter("combo") != null) {
			strComboId = request.getParameter("combo");
		} else {
			if (comboList != null && !comboList.isEmpty()) {
				strComboId = comboList.get(0).getReservedId().toString();
			}
		}

		BuddyPADAO buddyPADAO = new BuddyPADAOImpl();
		// list of plans (only plans grouped with the combo)
		List<BuddyPAPlan> planList;
		planList = buddyPADAO.getPlanList(strCompany, strComboId);

		// check for plan parameter
		if (request.getParameter("plan") != null) {
			strPlanId = request.getParameter("plan");
		} else {
			if (planList != null && !planList.isEmpty()) {
				strPlanId = planList.get(0).getPlanId().toString();
			}
		}

		// benefit list (check box list)
		List<BuddyPABenefit> benefitList;
		benefitList = buddyPADAO.getBenefitList(strCompany, strComboId, strPlanId);

		// benefit list (table list)
		List<BuddyPABenefit> benefitListDist;
		benefitListDist = buddyPADAO.getBenefitListDist(strCompany, strComboId);

		// benefit group list
		List<BuddyPABenefitGrpList> benefitGrpList;
		benefitGrpList = buddyPADAO.getBenefitGrpList(strCompany, strComboId);

		// premium total list
		List<BuddyPABenefitGrpList> premiumTotalList;
		premiumTotalList = buddyPADAO.getPremiumTotal(strCompany, strComboId);

		model.addAttribute("companyList", companyList);
		model.addAttribute("comboList", comboList);
		model.addAttribute("planList", planList);
		model.addAttribute("benefitList", benefitList);
		model.addAttribute("benefitListDist", benefitListDist);
		model.addAttribute("benefitListCount", benefitList.size());
		model.addAttribute("benefitGrpList", benefitGrpList);
		model.addAttribute("benefitGrpListCount", premiumTotalList.size());
		model.addAttribute("premiumTotalList", premiumTotalList);
		model.addAttribute("company", strCompany);
		model.addAttribute("combo", strComboId);
		model.addAttribute("plan", strPlanId);

		return "/products/buddyPA/buddyPA-benefit-grp-add";

	}

	@RequestMapping("/editBuddyPABenefitGrpList")
	public String editBuddyPABenefitGrpList(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("Edit Buddy PA Benefit Grp Info ");
		HttpSession session = request.getSession();

		String strCompany = null;
		String strComboId = null;
		String strComboNm = null;
		String strPlanId = null;
		String strPlanNm = null;

		// list of companies
		List<SysParam> companyList;
		SysParamExample sysParamExample = new SysParamExample();

		SysParamExample.Criteria sysParaminfoCriteriaExist = sysParamExample.createCriteria();
		sysParaminfoCriteriaExist.andParamEqualTo("Company");
		companyList = sysParamMapper.selectByExample(sysParamExample);

		// check for company parameter
		if (request.getParameter("company") != null) {
			strCompany = request.getParameter("company");
		} else {
			if (companyList != null && !companyList.isEmpty()) {
				strCompany = companyList.get(0).getName().toString();
			}
		}

		// list of combos
		List<BuddyPACombo> comboList;
		BuddyPAComboExample buddyPAComboExample = new BuddyPAComboExample();

		BuddyPAComboExample.Criteria buddyPAComboCriteriaExist = buddyPAComboExample.createCriteria();
		buddyPAComboCriteriaExist.andCompanyIdEqualTo(strCompany); // default set to the first on the
																	// list
		comboList = buddyPAComboMapper.selectByExample(buddyPAComboExample);

		// check for combo parameter
		if (request.getParameter("combo") != null) {
			strComboId = request.getParameter("combo");
		} else {
			if (comboList != null && !comboList.isEmpty()) {
				strComboId = comboList.get(0).getReservedId().toString();
			}
		}

		for (int i = 0; i < comboList.size(); i++) {
			if (comboList.get(i).getReservedId().toString().equalsIgnoreCase(strComboId)) {
				strComboNm = comboList.get(i).getName().toString();
				break;
			}
		}

		BuddyPADAO buddyPADAO = new BuddyPADAOImpl();
		// list of plans (only plans grouped with the combo)
		List<BuddyPAPlan> planList;
		planList = buddyPADAO.getPlanList(strCompany, strComboId);

		// check for plan parameter
		if (request.getParameter("plan") != null) {
			strPlanId = request.getParameter("plan");
		} else {
			if (planList != null && !planList.isEmpty()) {
				strPlanId = planList.get(0).getPlanId().toString();
			}
		}

		for (int i = 0; i < planList.size(); i++) {
			if (planList.get(i).getPlanId().toString().equalsIgnoreCase(strPlanId)) {
				strPlanNm = planList.get(i).getNameEn().toString();
				break;
			}
		}

		// benefit list
		List<BuddyPABenefit> benefitList;
		benefitList = buddyPADAO.getBenefitList(strCompany, strComboId, strPlanId);
		// edit - benefit group list
		List<BuddyPABenefitGrpList> editBenefitGrpList;
		// check for plan id parameter
		strPlanId = request.getParameter("editPlan");
		editBenefitGrpList = buddyPADAO.getEditBenefitGrpList(strCompany, strComboId, strPlanId);

		model.addAttribute("companyList", companyList);
		model.addAttribute("comboList", comboList);
		model.addAttribute("planList", planList);
		model.addAttribute("benefitList", benefitList);
		model.addAttribute("benefitListCount", benefitList.size());
		model.addAttribute("company", strCompany);
		model.addAttribute("combo", strComboId);
		model.addAttribute("comboNm", strComboNm);
		model.addAttribute("plan", strPlanId);
		model.addAttribute("planNm", strPlanNm);
		model.addAttribute("editBenefitGrpList", editBenefitGrpList);

		return "/products/buddyPA/buddyPA-benefit-grp-edit";

	}

	/*-- update benefit group list --*/
	@RequestMapping("/updBuddyPABenefitGrpList")
	public String updBuddyPABenefitGrpList(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("Update benefit group");
		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Buddy PA Controller loginUser" + loginUser);

		String strCompany = request.getParameter("bgCompanyId2");
		String strCombo = request.getParameter("bgComboId2");
		String strComboNm = request.getParameter("bgComboId2Nm");
		String strPlanId = request.getParameter("bgPlanId2");
		String strPlanIdNm = request.getParameter("bgPlanId2Nm");
		int count = Integer.parseInt(request.getParameter("bgCount"));
		BuddyPADAO buddyPADAO = new BuddyPADAOImpl();

		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator(',');
		symbols.setDecimalSeparator('.');
		String pattern = "0.0#";
		DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
		decimalFormat.setParseBigDecimal(true);

		List<BuddyPABenefitGrpList> editBenefitGrpList = buddyPADAO.getEditBenefitGrpList(strCompany, strCombo,
				strPlanId);
		List<BuddyPABenefitGrpList> listBuddyBenefitChange = new ArrayList<>();

		for (int i = 0; i < count; i++) {
			String benefitId = request.getParameter("bId" + Integer.toString(i));
			String benefitNm = request.getParameter("bNm" + Integer.toString(i));
			if (benefitId != null) {
				String adultSum = request.getParameter("aSum" + Integer.toString(i));
				String childSum = request.getParameter("cSum" + Integer.toString(i));
				String adultLoad = request.getParameter("aLoad" + Integer.toString(i));
				String childLoad = request.getParameter("cLoad" + Integer.toString(i));
				String version = request.getParameter("v" + Integer.toString(i));

				BuddyPABenefitGrpList item = new BuddyPABenefitGrpList();
				try {
					item.setCompanyId(strCompany);
					item.setComboId(strCombo);
					item.setComboName(strComboNm);
					item.setPlanId(strPlanId);
					item.setPlanName(strPlanIdNm);
					item.setBenefitId(benefitId);
					item.setBenefitName(benefitNm);
					if (!adultSum.equals("")) {
						item.setAdultSumInsured((BigDecimal) decimalFormat.parse(adultSum));
					}
					if (!childSum.equals("")) {
						item.setChildSumInsured((BigDecimal) decimalFormat.parse(childSum));
					}
					if (!adultLoad.equals("")) {
						item.setAdultLoading((BigDecimal) decimalFormat.parse(adultLoad));
					}
					if (!childLoad.equals("")) {
						item.setChildLoading((BigDecimal) decimalFormat.parse(childLoad));
					}
					item.setModifiedBy(loginUser);
					item.setVersion((short) Integer.parseInt(version != null ? version : "1"));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				listBuddyBenefitChange.add(item);
				// msg = buddyPADAO.updBenefitGrouping(item);
			}
		}

		// approval log
		convertBytes process = new convertBytes();

		try {
			byte[] changeBlob = process.convertToBytes(listBuddyBenefitChange);
			byte[] originalBlob = process.convertToBytes(editBenefitGrpList);

			ApprovalExample approvalExample = new ApprovalExample();
			com.spring.VO.ApprovalExample.Criteria createCriteriaApprovalExample = approvalExample.createCriteria();
			createCriteriaApprovalExample.andDescriptionEqualTo("BUDDY - CHANGE BENEFIT GROUPING INFORMATION");
			List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

			System.out.println("BUDDY ORI Data" + originalBlob);
			System.out.println("BUDDY change Data" + changeBlob);

			BigDecimal approvalProdId = new BigDecimal(0);
			if (!listapproval.isEmpty()) {
				approvalProdId = listapproval.get(0).getId();
			}

			int maker = Integer.parseInt(loginUser);

			// setting all new change value to ApprovalLog POJA and Insert to ApprovalLog
			// table
			ApprovalLog approvalLog = new ApprovalLog();
			approvalLog.setOriginalContent(originalBlob);
			approvalLog.setNewContent(changeBlob);
			approvalLog.setApprovalId((short) approvalProdId.intValue()); // from Approval table this value[2] based on
																			// menu in feature
			approvalLog.setMaker((short) maker); // session login value
			// approvalLog.setChecker((short)1); //while insert record no need checker value
			approvalLog.setStatus("1");
			approvalLog.setCreateDate(new Date());
			approvalLog.setUpdateDate(new Date());
			approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
			int status = approvalLogMapper.insert(approvalLog);
			model.addAttribute("SuccessMsg", "Submitted data successfully");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		// approval log

		model.addAttribute("company", strCompany);
		model.addAttribute("combo", strCombo);

		return "redirect:BuddyPABenefitGrpList";

	}

	/*-- update benefit for combo/plan (check box)--*/
	@RequestMapping("/updBuddyPABenefitList")
	public String updBuddyPABenefitList(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("Update benefit grouping ");
		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Buddy PA Controller loginUser" + loginUser);

		String strCompany = request.getParameter("ddlCompanyList");
		String strComboUid = request.getParameter("ddlComboList");
		String strComboId = request.getParameter("ddlComboList");
		String strPlanId = request.getParameter("ddlPlanList");

		System.out.println(strComboId);
		int count = Integer.parseInt(request.getParameter("bgCount")); // total benefits listed
		BuddyPADAO buddyPADAO = new BuddyPADAOImpl();

		// benefit list
		List<BuddyPABenefit> benefitList;
		benefitList = buddyPADAO.getBenefitList(strCompany, strComboId, strPlanId);

		for (int i = 0; i < count; i++) {
			String benefitId = request.getParameter("bnf_" + Integer.toString(i));
			BuddyPABenefitGrpList item = new BuddyPABenefitGrpList();
			item.setCompanyId(strCompany);
			item.setComboId(strComboId);
			item.setPlanId(strPlanId);
			item.setBenefitId(benefitList.get(i).getBenefitId());
			item.setEnable(benefitList.get(i).getBenefitId().toString().equals(benefitId) ? 'Y' : 'N');
			item.setModifiedBy(loginUser);
			buddyPADAO.updBenefitList(item);
		}

		model.addAttribute("company", strCompany);
		model.addAttribute("combo", strComboUid);
		model.addAttribute("comboId", strComboId);
		model.addAttribute("plan", strPlanId);

		return "redirect:BuddyPABenefitGrpList";

	}

	/*----------------------START Drop Down List-----------------------*/
	@RequestMapping("/bgCompany")
	public String bgCompany(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("bgCompany");

		model.addAttribute("company", request.getParameter("bgCompanyId"));
		model.addAttribute("combo", request.getParameter("bgComboId"));
		model.addAttribute("plan", request.getParameter("bgPlanId"));

		return "redirect:BuddyPABenefitGrpList";

	}
	/*----------------------END Drop Down List-----------------------*/

	/*----------------------START Edit List-----------------------*/
	@RequestMapping("/bgEdit")
	public String bgEdit(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("bgEdit");
		HttpSession session = request.getSession();

		model.addAttribute("company", request.getParameter("bgCompanyId"));
		model.addAttribute("combo", request.getParameter("bgComboId"));
		model.addAttribute("editPlan", request.getParameter("bgPlanId"));

		return "redirect:editBuddyPABenefitGrpList";

	}
	/*----------------------END Edit List-----------------------*/
	/*------------------------------------------------------END -  Benefit Grp Info---------------------------------------------------------*/

	// Jimmy (22052018)
	/*------------------------------------------------------START - Combo Info------------------------------------------------------*/
	/*----------START - Combo List----------*/
	@RequestMapping("/BuddyPAComboInfo")
	public String buddyPAComboInfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("Buddy PA Combo Info ");
		HttpSession session = request.getSession();

		String strCompany = "";

		// list of companies
		List<SysParam> companyList;
		SysParamExample sysParamExample = new SysParamExample();

		SysParamExample.Criteria sysParaminfoCriteriaExist = sysParamExample.createCriteria();
		sysParaminfoCriteriaExist.andParamEqualTo("Company");
		companyList = sysParamMapper.selectByExample(sysParamExample);

		// check for company parameter
		if (request.getParameter("company") != null) {
			strCompany = request.getParameter("company");
		} else {
			if (companyList != null && !companyList.isEmpty()) {
				strCompany = companyList.get(0).getName().toString();
			}
		}

		List<BuddyPACombo> comboList;
		BuddyPAComboExample buddyPAComboExample = new BuddyPAComboExample();

		buddyPAComboExample.setOrderByClause("RESERVED_ID");
		BuddyPAComboExample.Criteria buddyPAComboCriteriaExist = buddyPAComboExample.createCriteria();

		buddyPAComboCriteriaExist.andCompanyIdEqualTo(strCompany);

		comboList = buddyPAComboMapper.selectByExample(buddyPAComboExample);

		model.addAttribute("comboList", comboList);
		model.addAttribute("companyList", companyList);
		model.addAttribute("company", strCompany);

		return "/products/buddyPA/buddyPA-combo-mng-info";

	}
	/*----------END - Combo List----------*/

	/*----------START - Combo Add----------*/
	@RequestMapping("/addBuddyPACombo")
	public String addBuddyPACombo(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("add Combo");
		String redirctTO = null;
		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Buddy PA Controller loginUser" + loginUser);

		// List of the Combo (For Check Duplicate)
		List<BuddyPACombo> comboList;
		BuddyPAComboExample buddyPAComboExample = new BuddyPAComboExample();
		buddyPAComboExample.setOrderByClause("RESERVED_ID");
		BuddyPAComboExample.Criteria buddyPAComboCriteriaExist = buddyPAComboExample.createCriteria();
		buddyPAComboCriteriaExist.andCompanyIdEqualTo(request.getParameter("company").trim());
		buddyPAComboCriteriaExist.andReservedIdEqualTo(request.getParameter("reservedId").trim());
		comboList = buddyPAComboMapper.selectByExample(buddyPAComboExample);

		if (comboList == null || comboList.isEmpty()) {

			BuddyPACombo buddyPACombo = new BuddyPACombo();
			BuddyPACombo buddyPAComboDefault = new BuddyPACombo();
			String strEnable = "N";

			if (request.getParameter("enable") != null && request.getParameter("enable").equalsIgnoreCase("on")) {
				strEnable = "Y";
			}

			Timestamp timestamp = new Timestamp(System.currentTimeMillis());

			String urlEn = BUDDY_URL_EN + request.getParameter("reservedId").trim().toLowerCase()
					+ request.getParameter("company").trim().toLowerCase();
			String urlBm = BUDDY_URL_BM + request.getParameter("reservedId").trim().toLowerCase()
					+ request.getParameter("company").trim().toLowerCase();

			buddyPAComboDefault.setCompanyId("-");
			buddyPAComboDefault.setReservedId("-");
			buddyPAComboDefault.setUrlEn("-");
			buddyPAComboDefault.setUrlBm("-");
			buddyPAComboDefault.setName("-");
			buddyPAComboDefault.setEnable("-");
			buddyPAComboDefault.setVersion((short) 0);
			buddyPAComboDefault.setCreatedBy("-");
			buddyPAComboDefault.setCreatedDt(timestamp);

			buddyPACombo.setCompanyId(request.getParameter("company").trim());
			buddyPACombo.setReservedId(request.getParameter("reservedId").trim());
			buddyPACombo.setUrlEn(urlEn);
			buddyPACombo.setUrlBm(urlBm);
			buddyPACombo.setName(request.getParameter("name").trim());
			buddyPACombo.setEnable(strEnable.trim());
			buddyPACombo.setVersion((short) 1);
			buddyPACombo.setCreatedBy(loginUser);
			buddyPACombo.setCreatedDt(timestamp);

			List<BuddyPACombo> listBuddyComboChange = new ArrayList<>();
			List<BuddyPACombo> listBuddyComboOriginal = new ArrayList<>();
			listBuddyComboChange.add(buddyPACombo);
			listBuddyComboOriginal.add(buddyPAComboDefault);

			try {
				// approval log
				convertBytes process = new convertBytes();

				byte[] changeBlob = process.convertToBytes(listBuddyComboChange);
				byte[] originalBlob = process.convertToBytes(listBuddyComboOriginal);

				ApprovalExample approvalExample = new ApprovalExample();
				com.spring.VO.ApprovalExample.Criteria createCriteriaApprovalExample = approvalExample.createCriteria();
				createCriteriaApprovalExample.andDescriptionEqualTo("BUDDY - CHANGE COMBO INFORMATION");
				List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

				System.out.println("ORI Data" + originalBlob);
				System.out.println("change Data" + changeBlob);

				BigDecimal approvalProdId = new BigDecimal(0);
				if (!listapproval.isEmpty()) {
					approvalProdId = listapproval.get(0).getId();
				}

				int maker = Integer.parseInt(loginUser);

				// setting all new change value to ApprovalLog POJA and Insert to ApprovalLog
				// table
				ApprovalLog approvalLog = new ApprovalLog();
				// approvalLog.setOriginalContent(originalBlob);
				approvalLog.setNewContent(changeBlob);
				approvalLog.setApprovalId((short) approvalProdId.intValue()); // from Approval table this value[2] based
																				// on menu in feature
				approvalLog.setMaker((short) maker); // session login value
				// approvalLog.setChecker((short)1); //while insert record no need checker value
				approvalLog.setStatus("1");
				approvalLog.setCreateDate(new Date());
				approvalLog.setUpdateDate(new Date());
				approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
				int status = approvalLogMapper.insert(approvalLog);
				// approval log

				// buddyPAComboMapper.insertSelective(buddyPACombo);
			} catch (Exception e) {
				System.out.println("Error : Refer stack trace below.");
				e.printStackTrace();
			}

			model.addAttribute("SuccessMsg", "Data Added Successfully!");
		} else {
			model.addAttribute("Errmsg", "Duplicated Record Entered!");
		}

		redirctTO = buddyPAComboInfo(request, response, model);

		return redirctTO;

	}
	/*----------END - Combo Add----------*/

	/*----------START - Combo Update----------*/
	@RequestMapping("/updateBuddyPACombo")
	public String updateBuddyPACombo(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("update Combo");
		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		System.out.println("Buddy PA Controller loginUser" + loginUser);

		// List of the Combo (For Check Duplicate)
		List<BuddyPACombo> comboList;
		BuddyPAComboExample buddyPAComboExample = new BuddyPAComboExample();
		BuddyPACombo buddyPACombo = new BuddyPACombo();
		BuddyPAComboExample.Criteria comboParamCriteria1 = buddyPAComboExample.createCriteria();
		comboParamCriteria1.andCompanyIdEqualTo(request.getParameter("updCompanyID"));
		comboParamCriteria1.andReservedIdEqualTo(request.getParameter("updReservedID"));
		comboList = buddyPAComboMapper.selectByExample(buddyPAComboExample);

		if (comboList != null && !comboList.isEmpty()) {

			String strEnable = "N";

			if (request.getParameter("updComboEnable") != null
					&& request.getParameter("updComboEnable").equalsIgnoreCase("on")) {
				strEnable = "Y";
			}

			if (!request.getParameter("updReservedID").isEmpty() && !request.getParameter("updComboName").isEmpty()) {

				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				String urlEn = BUDDY_URL_EN + request.getParameter("updReservedID").trim().toLowerCase()
						+ request.getParameter("updCompanyID").trim().toLowerCase();
				String urlBm = BUDDY_URL_BM + request.getParameter("updReservedID").trim().toLowerCase()
						+ request.getParameter("updCompanyID").trim().toLowerCase();

				buddyPACombo.setCompanyId(request.getParameter("updCompanyID"));
				buddyPACombo.setName(request.getParameter("updComboName"));
				buddyPACombo.setReservedId(request.getParameter("updReservedID"));
				buddyPACombo.setUrlEn(urlEn);
				buddyPACombo.setUrlBm(urlBm);
				buddyPACombo.setEnable(strEnable);
				buddyPACombo.setModifiedBy(loginUser);
				buddyPACombo.setModifiedDt(timestamp);
				buddyPACombo.setVersion((short) (comboList.get(0).getVersion() + 1));

				// int buddyPAComboUpdate1 =
				// buddyPAComboMapper.updateByExampleSelective(buddyPACombo,buddyPAComboExample);

				List<BuddyPACombo> listBuddyComboChange = new ArrayList<>();
				listBuddyComboChange.add(buddyPACombo);

				// approval log
				convertBytes process = new convertBytes();

				try {
					byte[] changeBlob = process.convertToBytes(listBuddyComboChange);
					byte[] originalBlob = process.convertToBytes(comboList);

					ApprovalExample approvalExample = new ApprovalExample();
					com.spring.VO.ApprovalExample.Criteria createCriteriaApprovalExample = approvalExample
							.createCriteria();
					createCriteriaApprovalExample.andDescriptionEqualTo("BUDDY - CHANGE COMBO INFORMATION");
					List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

					System.out.println("ORI Data" + originalBlob);
					System.out.println("change Data" + changeBlob);

					BigDecimal approvalProdId = new BigDecimal(0);
					if (!listapproval.isEmpty()) {
						approvalProdId = listapproval.get(0).getId();
					}

					int maker = Integer.parseInt(loginUser);

					// setting all new change value to ApprovalLog POJA and Insert to ApprovalLog
					// table
					ApprovalLog approvalLog = new ApprovalLog();
					approvalLog.setOriginalContent(originalBlob);
					approvalLog.setNewContent(changeBlob);
					approvalLog.setApprovalId((short) approvalProdId.intValue()); // from Approval table this value[2]
																					// based on menu in feature
					approvalLog.setMaker((short) maker); // session login value
					// approvalLog.setChecker((short)1); //while insert record no need checker value
					approvalLog.setStatus("1");
					approvalLog.setCreateDate(new Date());
					approvalLog.setUpdateDate(new Date());
					approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
					int status = approvalLogMapper.insert(approvalLog);
					model.addAttribute("SuccessMsg", "Submitted data successfully");
					// approval log
				} catch (IOException e) {
					e.printStackTrace();
				}

			} else {

				model.addAttribute("Errmsg", "Error Message: Please fill in Mandatory Field(s)!");

			}
		} else {

			model.addAttribute("Errmsg", "Error Message: Duplicate Entry!");

		}

		// Call Post Back
		return buddyPAComboInfo(request, response, model);
	}
	/*----------END - Combo Update----------*/

	/*----------START - Company Index Change----------*/
	@RequestMapping("/companyIndexCombo")
	public String companyIndexCombo(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("companyIndexCombo");

		model.addAttribute("company", request.getParameter("selectedCompany"));

		return "redirect: BuddyPAComboInfo";

	}
	/*----------END - Company Index Change----------*/
	/*------------------------------------------------------END - Combo Info------------------------------------------------------*/

}