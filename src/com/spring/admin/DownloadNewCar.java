package com.spring.admin;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FPXCertRenewal
 */
@WebServlet("/downloadNewCar")
public class DownloadNewCar extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */

	// private String filePath =
	// "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs";
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		System.out.println("DownloadNewCar<doGet>");

		String filePath = "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs";

		// Testing
		// String filePath = "E:/WLReportpolder";

		String policyNo = request.getParameter("policyNo");
		System.out.println("<policyNo> => " + policyNo);

		String term = request.getParameter("term");
		System.out.println("<term> => " + term);

		// Testing
		// policyNo = "ER035100021MY";

		// String fileName = policyNo+".pdf";

		String fileName = policyNo + "-" + term + ".pdf";

		if (fileName == null || fileName.equals("")) {
			throw new ServletException("File Name can't be null or empty");
		}

		File file = new File(filePath + "/" + fileName);
		System.out.println("<file> => " + file);

		if (!file.exists()) {
			throw new ServletException("File doesn't exists on server############");
		} else {

			System.out.println("File location on server####################3>" + file.getAbsolutePath());

			ServletContext ctx = getServletContext();
			InputStream fis = new FileInputStream(file);
			String mimeType = ctx.getMimeType(file.getAbsolutePath());
			response.setContentType(mimeType != null ? mimeType : "application/octet-stream");
			response.setContentLength((int) file.length());
			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

			ServletOutputStream os = response.getOutputStream();
			byte[] bufferData = new byte[1024];
			int read = 0;
			while ((read = fis.read(bufferData)) != -1) {
				os.write(bufferData, 0, read);
			}
			os.flush();
			os.close();
			fis.close();
			System.out.println("File downloaded at client successfully");

		}
		System.out.println("File downloaded at client successfully######################");
		System.out.println("");
	}
}
