package com.spring.admin;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.etiqa.controller.TransactionalReportController;
import com.etiqa.dsp.sales.process.email.DspEmailDispatchProcessor;
import com.etiqa.dsp.sales.process.email.TermLifeProductMailTemplateLoad;
import com.spring.VO.report.TLReportExcelVO;
import com.spring.VO.report.TransSearchObject;
import com.spring.mapper.report.TLReportExcelMapper;

@Component
public class TransactionRejectionReportDispatcher {
	final static Logger logger = Logger.getLogger(TransactionRejectionReportDispatcher.class);
	TLReportExcelMapper tlReportExcelMapper;

	@Autowired
	public TransactionRejectionReportDispatcher(TLReportExcelMapper tlReportExcelMapper) {
		this.tlReportExcelMapper = tlReportExcelMapper;

	}

	// @Scheduled(fixedRate=3000)
	// public void print() {
	// logger.info("Executed Printing Job..." + new Date());
	// }

	@Scheduled(cron = "0 0 10 * * SAT")
	// @Scheduled(cron = "0 25 11 * * THU")
	// @Scheduled(fixedRate=5000)
	public void sendRejectionReportWeeklyEmail() {
		logger.info("sending rejection report weekly email " + new Date());
		sendRejectionEmail("EIB");
		sendRejectionEmail("ETB");
	}

	private List<TLReportExcelVO> getReportsBasedOnProductType(String productType) {

		String status = "R";

		TransSearchObject transSearchObject_TL = new TransSearchObject();
		List<TLReportExcelVO> tlReportList_B4Pay = new ArrayList<TLReportExcelVO>();
		List<TLReportExcelVO> idsReportList_B4Pay = new ArrayList<TLReportExcelVO>();
		List<String> transactionStatusList = new ArrayList<String>();

		transSearchObject_TL.setProductCode(productType);

		transactionStatusList.add(status);
		transSearchObject_TL.setTransactionStatus(transactionStatusList);

		transSearchObject_TL.setDateFrom(getStartDate());
		transSearchObject_TL.setDateTo(getEndDate());

		tlReportList_B4Pay = tlReportExcelMapper.selectTLTransactionalReportBeforePayment(transSearchObject_TL);

		List<TLReportExcelVO> all_rejection_reports = new ArrayList<TLReportExcelVO>();
		all_rejection_reports.addAll(tlReportList_B4Pay);
		if ("IDS".equals(productType)) {
			idsReportList_B4Pay = tlReportExcelMapper
					.selectEPPIDSTransactionalReportBeforePayment(transSearchObject_TL);
			all_rejection_reports.addAll(idsReportList_B4Pay);
		}
		return all_rejection_reports;
	}

	private String getStartDate() {

		LocalDateTime today = LocalDateTime.now();
		logger.info("Current DateTime=" + today);
		today = LocalDateTime.of(LocalDate.now(), LocalTime.now());
		logger.info("Current DateTime=" + today);

		LocalDateTime localStartDate = today.minusDays(7);
		Date startDate = Date.from(localStartDate.atZone(ZoneId.systemDefault()).toInstant());
		logger.info("7 days before today in date: " + startDate);
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

		return format.format(startDate);
	}

	private String getEndDate() {

		LocalDateTime today = LocalDateTime.now();
		logger.info("Current DateTime=" + today);
		today = LocalDateTime.of(LocalDate.now(), LocalTime.now());
		logger.info("Current DateTime=" + today);

		LocalDateTime localEndDate = today.minusDays(1);
		logger.info("1 days before today will be " + localEndDate);
		Date endDate = Date.from(localEndDate.atZone(ZoneId.systemDefault()).toInstant());
		logger.info("1 days before today in date: " + endDate);

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		return format.format(endDate);

	}

	private void sendRejectionEmail(String productEntity) {

		logger.info("generateExcel<TxnExcel-admin> => ");
		String dateFrom = "", dateTo = "";
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");

		// logger.info("excel productType -> "+productType);
		logger.info("excel productEntity  -> " + productEntity);

		logger.info("excel dateFrom  -> " + dateFrom);
		logger.info("excel dateTo  -> " + dateTo);

		// start : Ezylife excel file
		List<TLReportExcelVO> tlReportList = new ArrayList<TLReportExcelVO>();

		if (productEntity.equals("EIB")) {
			tlReportList.addAll(getReportsBasedOnProductType("TL"));
			tlReportList.addAll(getReportsBasedOnProductType("EZYTL"));

		} else if (productEntity.equals("ETB")) {
			tlReportList.addAll(getReportsBasedOnProductType("IDS"));
			tlReportList.addAll(getReportsBasedOnProductType("ISCTL"));
		}

		Collections.sort(tlReportList, new Comparator<TLReportExcelVO>() {
			@Override
			public int compare(TLReportExcelVO o1, TLReportExcelVO o2) {
				return o1.getRecord_date().compareTo(o2.getRecord_date());
			}
		});

		Collections.reverse(tlReportList);

		Random ran = new Random();
		int randomNo = ran.nextInt(6) + 5;

		DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
		Date date = new Date();

		String file = "EtiqaTransactionReport" + dateFormat.format(date) + randomNo + ".xlsx";

		String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
		// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

		logger.info("relativeWebPath =" + relativeWebPath);

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Etiqa Transaction Report");

		// header fields based on product
		String[] fields = null;

		fields = new String[] { "Id", "DSPQQID", "Quotation Creation Date", "Txn Date", "Transaction Start",
				"Transaction End", "Transaction Time Taken", "Product Name", "Policy/Cert No", "Coverage Amount",
				"Coverage Term", "Status", "Abandonment", "Rejection Reason", "Name", "NRIC No", "Gender", "Email",
				"Address", "Postcode", "State", "Mobile No", "Annual Premium/Contribution", "Payment Type",
				"Payment Method", "Payment Mode", "Amount Paid", "MPAY Ref No", "MPAY Auth Code", "Promo Code",
				"Agent Name", "Agent Code", "Operator Code" };

		Row row1 = sheet.createRow(0);

		for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
			Cell cell = row1.createCell(cellIndex);
			cell.setCellValue(fields[cellIndex]);
		}

		String[] fields2 = null;

		int i = 1;

		for (TLReportExcelVO TLDetail : tlReportList) {

			String Abandonment = "";

			String txnStatus = "";
			String quotationCreationDate = "";
			String transactionStartDateTime = "";
			String transactionEndDateTime = "";
			String proEntity = "";
			String paymentChannel = "";

			String txnTimeTaken = "";
			String transactionDate = "";

			if (null != TLDetail.getPmnt_gateway_code() && "" != TLDetail.getPmnt_gateway_code()) {
				if (TLDetail.getPmnt_gateway_code().indexOf("EBPG") != -1) {
					paymentChannel = "EBPG";
				} else if (TLDetail.getPmnt_gateway_code().indexOf("AMEX") != -1) {
					paymentChannel = "AMEX";
				} else if (TLDetail.getPmnt_gateway_code().indexOf("FPX") != -1) {
					paymentChannel = "FPX";
				} else if (TLDetail.getPmnt_gateway_code().indexOf("Maybank2U") != -1) {
					paymentChannel = "M2U";
				} else if (TLDetail.getPmnt_gateway_code().indexOf("CREDIT CARD") != -1) {
					paymentChannel = "MPAY";
				}
			}
			if (TLDetail.getProduct_code().equals("MI")) {
				proEntity = "EIB";
			} else if (TLDetail.getProduct_code().equals("MT")) {
				proEntity = "ETB";
			}

			if (TLDetail.getPmnt_status().equals("S")) {
				txnStatus = "Successful";
			} else if (TLDetail.getPmnt_status().equals("F")) {
				txnStatus = "Fail";
			}

			else if (TLDetail.getPmnt_status().equals("AP")) {
				txnStatus = "Attempt Payment";
			} else if (TLDetail.getPmnt_status().equals("O")) {
				Abandonment = TLDetail.getLast_page();
				txnStatus = "Incomplete";
			} else if (TLDetail.getPmnt_status().equals("C")) {
				txnStatus = "Cancelled";
			} else if (TLDetail.getPmnt_status().equals("R")) {
				txnStatus = "Rejection : " + TLDetail.getUWReason();
			}

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date quotationCreationDateTmp = null;

			try {

				quotationCreationDateTmp = formatter.parse(TLDetail.getQuotationCreateDateTime());
				quotationCreationDate = ft.format(quotationCreationDateTmp);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			if (TLDetail.getPmnt_status().equals("S") || TLDetail.getPmnt_status().equals("F")
					|| TLDetail.getPmnt_status().equals("AP")) {

				transactionStartDateTime = TLDetail.getTransaction_datetime();

				/*
				 * try {
				 *
				 * transactionStartDateTime=
				 * ft2.format(format1.parse(transactionStartDateTime));
				 *
				 * } catch (ParseException e) { // TODO Auto-generated catch block
				 * e.printStackTrace(); }
				 */

			}

			fields2 = new String[] { Integer.toString(i), TLDetail.getDSPQQID(), quotationCreationDate, transactionDate,
					transactionStartDateTime, transactionEndDateTime, txnTimeTaken, TLDetail.getProduct_code(),
					TLDetail.getPolicy_number(), TLDetail.getCoverage_amount(), TLDetail.getCoverage_term() + " Years",
					txnStatus, Abandonment, TLDetail.getUWReason(), TLDetail.getCustomer_name(),
					TLDetail.getCustomer_nric_id(), TLDetail.getCustomerGender(), TLDetail.getCustomerEmail(),
					TLDetail.getCustomerAddress1() + "," + TLDetail.getCustomerAddress2() + ","
							+ TLDetail.getCustomerAddress3(),
					TLDetail.getCustomerPostcode(), TLDetail.getCustomerState(), TLDetail.getCustomerMobileNo(),
					TLDetail.getPremium_amount(), TLDetail.getPmnt_gateway_code(), paymentChannel,
					TLDetail.getPremium_mode(), TLDetail.getAmount(), TLDetail.getTxn_id(), TLDetail.getAuth_code(),
					TLDetail.getDiscountCode(), TLDetail.getAgent_name(), TLDetail.getAgent_code(),
					TLDetail.getOperatorCode() };

			Row row2 = sheet.createRow(i);

			for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
				Cell cell = row2.createCell(cellIndex);
				cell.setCellValue(fields2[cellIndex]);
			}

			i++;

		}

		// logger.info("txnStatus "+txn.getStatus());

		try {
			FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
			workbook.write(outputStream);
			workbook.close();

			File fileToDownload = new File(relativeWebPath);
			// attach and send the email.

			DspEmailDispatchProcessor emailProcessor = new DspEmailDispatchProcessor(
					new TermLifeProductMailTemplateLoad());
			String emailStatus = emailProcessor.emailRejectionReportProcess(fileToDownload, productEntity);
			logger.info(emailStatus + "  :email status");

			// FileInputStream inputStream = new FileInputStream(fileToDownload);
			// //response.setContentType(servletContext.getMimeType(relativeWebPath));
			// // response.setContentLength((int) fileToDownload.length());
			//
			// String headerKey = "Content-Disposition";
			// String headerValue = String.format("attachment; filename=\"%s\"",
			// fileToDownload.getName());
			// response.setHeader(headerKey, headerValue);

			// get output stream of the response
			// OutputStream outStream = response.getOutputStream();

			// byte[] buffer = new byte[4096];
			// int bytesRead = -1;

			// write bytes read from the input stream into the output stream
			// while ((bytesRead = inputStream.read(buffer)) != -1) {
			// outStream.write(buffer, 0, bytesRead);
			// }
			//
			// outStream.flush();
			// outStream.close();
			// inputStream.close();

			if (fileToDownload.exists()) {

				boolean success = new File(relativeWebPath).delete();
				if (success) {
					logger.info("The file " + file + " has been successfully deleted");
				}

			}

		} catch (FileNotFoundException e) {
			// response.getOutputStream().flush();
			// response.getOutputStream().close();

			e.printStackTrace();
		} catch (IOException e) {
			// response.getOutputStream().flush();
			// response.getOutputStream().close();

			e.printStackTrace();
		}

		// } // end : ezylife excel file

	}
}
