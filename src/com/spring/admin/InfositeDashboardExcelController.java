package com.spring.admin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.spring.VO.InfositeContactInfo;
import com.spring.VO.InfositeContactInfoExample;
import com.spring.VO.InfositeEnquiry;
import com.spring.VO.InfositeEnquiryExample;
import com.spring.VO.InfositeNewsLetter;
import com.spring.VO.InfositeNewsLetterExample;
import com.spring.mapper.InfositeContactInfoMapper;
import com.spring.mapper.InfositeEnquiryMapper;
import com.spring.mapper.InfositeNewsLetterMapper;
import com.spring.utils.ServiceValidationUtils;

@Controller
public class InfositeDashboardExcelController {
	InfositeContactInfoMapper infositeContactInfoMapper;
	InfositeEnquiryMapper infositeEnquiryMapper;
	InfositeNewsLetterMapper infositeNewsLetterMapper;
	
	@Autowired
	public InfositeDashboardExcelController(InfositeContactInfoMapper infositeContactInfoMapper, 
			InfositeEnquiryMapper infositeEnquiryMapper, InfositeNewsLetterMapper infositeNewsLetterMapper
	
	) {
		this.infositeContactInfoMapper = infositeContactInfoMapper;
		this.infositeEnquiryMapper = infositeEnquiryMapper;
		this.infositeNewsLetterMapper = infositeNewsLetterMapper;
		
	}
	
	/**
	 *
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 *             To generate the Txn Report in Excel format and down load the
	 *             report
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/generateInfositeEnquiryExcel", method = RequestMethod.POST)
	public String generateInfositeEnquiryExcel(HttpServletRequest request, HttpServletResponse response, Model model)
			throws ServletException, IOException {

		System.out.println("generateInfositeExcel enquiry calling############# ");

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");

		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		ServletContext servletContext = request.getSession().getServletContext();
		List<String> errorMessages = new ArrayList<String>();
		// String searchPolicyNo= (String)session.getAttribute("policyNumberSession");
		String enquirydate = request.getParameter("enquirydate").trim();
		//String dateTo = request.getParameter("dateTo").trim();
		String dateFrom=""; String dateTo="";
		if(!enquirydate.isEmpty()){
		 String dates[]=enquirydate.split("-"); 
		 dateFrom=dates[0].trim();
		 dateTo =dates[1].trim();
		}
		session.setAttribute("dateFrom", dateFrom);
		session.setAttribute("dateTo", dateTo);
		
		List<InfositeEnquiry> infositeEnquiryList = new ArrayList<InfositeEnquiry>();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

			Date fromDate = null;
			Date toDate = null;

			try {
				fromDate = sdf.parse(dateFrom);
				toDate = sdf.parse(dateTo);

				if (fromDate.compareTo(toDate) > 0) {
					errorMessages.add("Date To Cannot Less Than Date From");
				} else {
					try {
						// in milliseconds
						long diff = toDate.getTime() - fromDate.getTime();
						long diffSeconds = diff / 1000 % 60;
						long diffMinutes = diff / (60 * 1000) % 60;
						long diffHours = diff / (60 * 60 * 1000) % 24;
						long diffDays = diff / (24 * 60 * 60 * 1000);

						if (diffDays > 31) {
							errorMessages.add("The date range cannot exceed than 1 month");
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}

		if (ServiceValidationUtils.isEmptyStringTrim(dateFrom)) {
			errorMessages.add("Date From Cannot Be Empty");
		}
		if (ServiceValidationUtils.isEmptyStringTrim(dateTo)) {
			errorMessages.add("Date To Cannot Be Empty");
		}
		
		if (errorMessages.size() > 0) {
			model.addAttribute("errorMessages", errorMessages);
			return "admin-infosite-dashboard";
		}
					
		InfositeEnquiryExample infositeEnquiryExample = new InfositeEnquiryExample();
		InfositeEnquiryExample.Criteria EnquiryCriteria = infositeEnquiryExample.createCriteria();
		EnquiryCriteria.andCreatedateGreaterThanOrEqualTo(fromDate);
		EnquiryCriteria.andCreatedateLessThanOrEqualTo(toDate);
		
		
		infositeEnquiryList =infositeEnquiryMapper.selectByExample(infositeEnquiryExample);
		
		System.out.println("generateInfositeExcel<size> => " + infositeEnquiryList.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Start Excel
		Random ran = new Random();
		int randomNo = ran.nextInt(6) + 5;

		DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
		Date date = new Date();

		String file = "InfositeEnquiryReport" + dateFormat.format(date) + randomNo + ".xlsx";
		System.out.println("generateInfositeExcel<file> => " + file);

		String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
		// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("InfositeExcel Report");
		System.out.println("generateInfositeExcel<sheet> => " + sheet);

		String reportDateandTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new java.util.Date());

		Row row1 = sheet.createRow(0);
		Cell cellDate = row1.createCell(0);
		cellDate.setCellValue(reportDateandTime);

		// header fields based on product
		String[] fields = null;
		fields = new String[] { "Id", "Name", "Contact Number", "Email", "EnquiryType" ,"ProductType",
				"EnquiryNo","Date", "EtiqaCustomer", "NRIC", "EtiqaCustomer",
				"AgreeFlag", "PdPaFlag","Salutation","Emailflag" };
		Row row2 = sheet.createRow(1);

		for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
			Cell cell = row2.createCell(cellIndex);
			cell.setCellValue(fields[cellIndex]);
		}

		String[] fields2 = null;

		int i = 1;
		int j = 2;
        
		for (InfositeEnquiry infositeEnquiry : infositeEnquiryList) {

			fields2 = new String[] { Integer.toString(i), infositeEnquiry.getName(),
					String.valueOf(infositeEnquiry.getContactNo()), String.valueOf(infositeEnquiry.getContactEmail()),
					String.valueOf(infositeEnquiry.getEnquiryType()), String.valueOf(infositeEnquiry.getEnquiryProduct()),
					String.valueOf(infositeEnquiry.getEnquiryNo()), String.valueOf(infositeEnquiry.getCreatedate()),
					String.valueOf(infositeEnquiry.getEtiqaCustomer()), String.valueOf(infositeEnquiry.getNric()),
					String.valueOf(infositeEnquiry.getPolicyNo()), String.valueOf(infositeEnquiry.getAgreeFlag()),
					String.valueOf(infositeEnquiry.getPdpaFlag()), String.valueOf(infositeEnquiry.getSalutation()),
					String.valueOf(infositeEnquiry.getEmailFlg())};

			Row row3 = sheet.createRow(j);

			for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
				Cell cell = row3.createCell(cellIndex);
				cell.setCellValue(fields2[cellIndex]);
			}
			i++;
			j++;
		}

		FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
		workbook.write(outputStream);
		workbook.close();

		try {
			File fileToDownload = new File(relativeWebPath);
			FileInputStream inputStream = new FileInputStream(fileToDownload);
			response.setContentType(servletContext.getMimeType(relativeWebPath));
			response.setContentLength((int) fileToDownload.length());

			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
			response.setHeader(headerKey, headerValue);

			// get output stream of the response
			OutputStream outStream = response.getOutputStream();

			byte[] buffer = new byte[4096];
			int bytesRead = -1;

			// write bytes read from the input stream into the output stream
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}
			outStream.flush();
			outStream.close();
			inputStream.close();

			if (fileToDownload.exists()) {
				boolean success = new File(relativeWebPath).delete();

				if (success) {
					System.out.println("generateInfositeExcel<" + file + "> has been successfully deleted");
				}
			}
		} catch (FileNotFoundException e) {
			response.getOutputStream().flush();
			response.getOutputStream().close();
			e.printStackTrace();
		} catch (IOException e) {
			response.getOutputStream().flush();
			response.getOutputStream().close();
			e.printStackTrace();
		}
		// End Excel
		

		return "admin-infosite-dashboard";
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/generateInfositecontactExcel", method = RequestMethod.POST)
	public String generateInfositecontactExcel(HttpServletRequest request, HttpServletResponse response, Model model)
			throws ServletException, IOException {

		System.out.println("generateInfositeExcel contact info calling############# ");

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");

		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		ServletContext servletContext = request.getSession().getServletContext();
		List<String> errorMessages = new ArrayList<String>();
		// String searchPolicyNo= (String)session.getAttribute("policyNumberSession");
		String contactdate = request.getParameter("contactdate").trim();
		//String dateTo = request.getParameter("dateTo").trim();
		String dateFrom=""; String dateTo="";
		if(!contactdate.isEmpty()){
		 String dates[]=contactdate.split("-");
		 dateFrom=dates[0].trim();
		 dateTo =dates[1].trim();
		}
		session.setAttribute("dateFrom", dateFrom);
		session.setAttribute("dateTo", dateTo);
		
		List<InfositeContactInfo> infositeContactInfoList = new ArrayList<InfositeContactInfo>();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

			Date fromDate = null;
			Date toDate = null;

			try {
				fromDate = sdf.parse(dateFrom);
				toDate = sdf.parse(dateTo);

				if (fromDate.compareTo(toDate) > 0) {
					errorMessages.add("Date To Cannot Less Than Date From");
				} else {
					try {
						// in milliseconds
						long diff = toDate.getTime() - fromDate.getTime();
						long diffSeconds = diff / 1000 % 60;
						long diffMinutes = diff / (60 * 1000) % 60;
						long diffHours = diff / (60 * 60 * 1000) % 24;
						long diffDays = diff / (24 * 60 * 60 * 1000);

						if (diffDays > 31) {
							errorMessages.add("The date range cannot exceed than 1 month");
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}

		if (ServiceValidationUtils.isEmptyStringTrim(dateFrom)) {
			errorMessages.add("Date From Cannot Be Empty");
		}
		if (ServiceValidationUtils.isEmptyStringTrim(dateTo)) {
			errorMessages.add("Date To Cannot Be Empty");
		}

		if (errorMessages.size() > 0) {
			model.addAttribute("errorMessages", errorMessages);
			return "admin-infosite-dashboard";
		}
		
		InfositeContactInfoExample infositeContactInfoExample = new InfositeContactInfoExample();
		InfositeContactInfoExample.Criteria contactinfoCriteria = infositeContactInfoExample.createCriteria();
		contactinfoCriteria.andCreatedateGreaterThanOrEqualTo(fromDate);
		contactinfoCriteria.andCreatedateLessThanOrEqualTo(toDate);
		
		
		infositeContactInfoList =infositeContactInfoMapper.selectByExample(infositeContactInfoExample);
		
		System.out.println("generateInfositeExcel<size> => " + infositeContactInfoList.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Start Excel
		Random ran = new Random();
		int randomNo = ran.nextInt(6) + 5;

		DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
		Date date = new Date();

		String file = "InfositeContactMeReport" + dateFormat.format(date) + randomNo + ".xlsx";
		System.out.println("generateInfositeExcel<file> => " + file);

		String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
		// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("InfositeExcel Report");
		System.out.println("generateInfositeExcel<sheet> => " + sheet);

		String reportDateandTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new java.util.Date());

		Row row1 = sheet.createRow(0);
		Cell cellDate = row1.createCell(0);
		cellDate.setCellValue(reportDateandTime);

		// header fields based on product
		String[] fields = null;
		fields = new String[] { "Id", "Name", "Contact Number", "Email", "State", "City", "PageFrom", "Date", "EtiqaCustomer",
				"AgreeFlag", "RequestNo","Salutation" };
		Row row2 = sheet.createRow(1);

		for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
			Cell cell = row2.createCell(cellIndex);
			cell.setCellValue(fields[cellIndex]);
		}

		String[] fields2 = null;

		int i = 1;
		int j = 2;
        
		for (InfositeContactInfo infositeContactInfo : infositeContactInfoList) {

			fields2 = new String[] { Integer.toString(i), infositeContactInfo.getName(),
					String.valueOf(infositeContactInfo.getContactNo()), String.valueOf(infositeContactInfo.getContactEmail()),
					String.valueOf(infositeContactInfo.getContactState()), String.valueOf(infositeContactInfo.getContactCity()),
					String.valueOf(infositeContactInfo.getContactForm()), String.valueOf(infositeContactInfo.getCreatedate()),
					String.valueOf(infositeContactInfo.getEtiqaCustomer()), String.valueOf(infositeContactInfo.getAgreeFlag()),
					String.valueOf(infositeContactInfo.getServiceReq()), infositeContactInfo.getSalutation()};

			Row row3 = sheet.createRow(j);

			for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
				Cell cell = row3.createCell(cellIndex);
				cell.setCellValue(fields2[cellIndex]);
			}
			i++;
			j++;
		}

		FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
		workbook.write(outputStream);
		workbook.close();

		try {
			File fileToDownload = new File(relativeWebPath);
			FileInputStream inputStream = new FileInputStream(fileToDownload);
			response.setContentType(servletContext.getMimeType(relativeWebPath));
			response.setContentLength((int) fileToDownload.length());

			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
			response.setHeader(headerKey, headerValue);

			// get output stream of the response
			OutputStream outStream = response.getOutputStream();

			byte[] buffer = new byte[4096];
			int bytesRead = -1;

			// write bytes read from the input stream into the output stream
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}
			outStream.flush();
			outStream.close();
			inputStream.close();

			if (fileToDownload.exists()) {
				boolean success = new File(relativeWebPath).delete();

				if (success) {
					System.out.println("generateInfositeExcel<" + file + "> has been successfully deleted");
				}
			}
		} catch (FileNotFoundException e) {
			response.getOutputStream().flush();
			response.getOutputStream().close();
			e.printStackTrace();
		} catch (IOException e) {
			response.getOutputStream().flush();
			response.getOutputStream().close();
			e.printStackTrace();
		}
		// End Excel
		

		return "admin-infosite-dashboard";
	}
	
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/generateInfositeNewsletterExcel", method = RequestMethod.POST)
	public String generateInfositeNewsletterExcel(HttpServletRequest request, HttpServletResponse response, Model model)
			throws ServletException, IOException {

		System.out.println("generateInfositeExcel  news letter calling############# ");

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");

		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		ServletContext servletContext = request.getSession().getServletContext();
		List<String> errorMessages = new ArrayList<String>();
		// String searchPolicyNo= (String)session.getAttribute("policyNumberSession");
		String newsletterdate = request.getParameter("newsletterdate").trim();
		//String dateTo = request.getParameter("dateTo").trim();
		String dateFrom=""; String dateTo="";
		if(!newsletterdate.isEmpty()){
		 String dates[]=newsletterdate.split("-");
		 dateFrom=dates[0].trim();
		 dateTo =dates[1].trim();
		}
		session.setAttribute("dateFrom", dateFrom);
		session.setAttribute("dateTo", dateTo);
		
		List<InfositeNewsLetter> infositeNewsLetterList = new ArrayList<InfositeNewsLetter>();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

			Date fromDate = null;
			Date toDate = null;

			try {
				fromDate = sdf.parse(dateFrom);
				toDate = sdf.parse(dateTo);

				if (fromDate.compareTo(toDate) > 0) {
					errorMessages.add("Date To Cannot Less Than Date From");
				} else {
					try {
						// in milliseconds
						long diff = toDate.getTime() - fromDate.getTime();
						long diffSeconds = diff / 1000 % 60;
						long diffMinutes = diff / (60 * 1000) % 60;
						long diffHours = diff / (60 * 60 * 1000) % 24;
						long diffDays = diff / (24 * 60 * 60 * 1000);

						if (diffDays > 31) {
							errorMessages.add("The date range cannot exceed than 1 month");
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}

		if (ServiceValidationUtils.isEmptyStringTrim(dateFrom)) {
			errorMessages.add("Date From Cannot Be Empty");
		}
		if (ServiceValidationUtils.isEmptyStringTrim(dateTo)) {
			errorMessages.add("Date To Cannot Be Empty");
		}
		
		if (errorMessages.size() > 0) {
			model.addAttribute("errorMessages", errorMessages);
			return "admin-infosite-dashboard";
		}
					
		InfositeNewsLetterExample infositeNewsLetterExample = new InfositeNewsLetterExample();
		InfositeNewsLetterExample.Criteria NewsLetterCriteria = infositeNewsLetterExample.createCriteria();
		NewsLetterCriteria.andCreateDateGreaterThanOrEqualTo(fromDate);
		NewsLetterCriteria.andCreateDateLessThanOrEqualTo(toDate);
		
		
		infositeNewsLetterList =infositeNewsLetterMapper.selectByExample(infositeNewsLetterExample);
		
		System.out.println("generateInfositeExcel<size> => " + infositeNewsLetterList.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Start Excel
		Random ran = new Random();
		int randomNo = ran.nextInt(6) + 5;

		DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
		Date date = new Date();

		String file = "InfositeNewsLetterReport" + dateFormat.format(date) + randomNo + ".xlsx";
		System.out.println("generateInfositeExcel<file> => " + file);

		String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
		// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("InfositeExcel Report");
		System.out.println("generateInfositeExcel<sheet> => " + sheet);

		String reportDateandTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new java.util.Date());

		Row row1 = sheet.createRow(0);
		Cell cellDate = row1.createCell(0);
		cellDate.setCellValue(reportDateandTime);

		// header fields based on product
		String[] fields = null;
		fields = new String[] { "Id","Email", "CreateDate", "UpdateDate"};
		Row row2 = sheet.createRow(1);

		for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
			Cell cell = row2.createCell(cellIndex);
			cell.setCellValue(fields[cellIndex]);
		}

		String[] fields2 = null;

		int i = 1;
		int j = 2;
        
		for (InfositeNewsLetter infositeNewsLetter : infositeNewsLetterList) {

			fields2 = new String[] { Integer.toString(i), infositeNewsLetter.getEmail(),
					String.valueOf(infositeNewsLetter.getCreateDate()), String.valueOf(infositeNewsLetter.getUpdateDate())};

			Row row3 = sheet.createRow(j);

			for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
				Cell cell = row3.createCell(cellIndex);
				cell.setCellValue(fields2[cellIndex]);
			}
			i++;
			j++;
		}

		FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
		workbook.write(outputStream);
		workbook.close();

		try {
			File fileToDownload = new File(relativeWebPath);
			FileInputStream inputStream = new FileInputStream(fileToDownload);
			response.setContentType(servletContext.getMimeType(relativeWebPath));
			response.setContentLength((int) fileToDownload.length());

			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
			response.setHeader(headerKey, headerValue);

			// get output stream of the response
			OutputStream outStream = response.getOutputStream();

			byte[] buffer = new byte[4096];
			int bytesRead = -1;

			// write bytes read from the input stream into the output stream
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}
			outStream.flush();
			outStream.close();
			inputStream.close();

			if (fileToDownload.exists()) {
				boolean success = new File(relativeWebPath).delete();

				if (success) {
					System.out.println("generateInfositeExcel<" + file + "> has been successfully deleted");
				}
			}
		} catch (FileNotFoundException e) {
			response.getOutputStream().flush();
			response.getOutputStream().close();
			e.printStackTrace();
		} catch (IOException e) {
			response.getOutputStream().flush();
			response.getOutputStream().close();
			e.printStackTrace();
		}
		// End Excel
		

		return "admin-infosite-dashboard";
	}
}