package com.spring.admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.etiqa.utils.convertBytes;
import com.spring.VO.ApprovalLog;
import com.spring.VO.ApprovalLogExample;
import com.spring.VO.TravEzSetup;
import com.spring.VO.TravEzSetupExample;
import com.spring.mapper.ApprovalLogMapper;
import com.spring.mapper.ApprovalMapper;
import com.spring.mapper.TravEzSetupMapper;

@Controller
public class AdminTravelEzyMngController {

	ApprovalMapper approvalMapper;
	ApprovalLogMapper approvalLogMapper;
	TravEzSetupMapper travEzSetupMapper;

	@Autowired
	public AdminTravelEzyMngController(ApprovalMapper approvalMapper, ApprovalLogMapper approvalLogMapper,
			TravEzSetupMapper travEzSetupMapper) {
		this.approvalMapper = approvalMapper;
		this.approvalLogMapper = approvalLogMapper;
		this.travEzSetupMapper = travEzSetupMapper;

	}

	// ---------------------------------------------------------------START Travel
	// Ezy ProductInfo
	// Change------------------------------------------------------------------------------
	@RequestMapping(value = "/approvalTravelEzyProduct", method = RequestMethod.GET)
	public String approvalTravelEzyProduct(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Request " + request.getParameter("id"));
		ApprovalLogExample approvalexample = new ApprovalLogExample();
		com.spring.VO.ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();
		Integer testint = new Integer(request.getParameter("id"));
		createCriteria.andIdEqualTo(testint.shortValue());
		List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
		System.out.println("   list size in approval list  :::   " + listapprovallog.size());

		try {
			// original data
			if (!listapprovallog.isEmpty()) {
				convertBytes process = new convertBytes();
				Object originalData = process.convertFromBytes(listapprovallog.get(0).getOriginalContent());
				List<TravEzSetup> listOriginalData;
				// object convert into Plan Model
				if (originalData != null) {
					listOriginalData = (List<TravEzSetup>) originalData;
					System.out.println(" Original Data List Size ::::::         " + listOriginalData.size());

					TravEzSetup travEzSetup = new TravEzSetup();

					travEzSetup.setCompanyId(listOriginalData.get(0).getCompanyId());
					travEzSetup.setProductCode(listOriginalData.get(0).getProductCode());
					travEzSetup.setStampDuty(listOriginalData.get(0).getStampDuty());
					travEzSetup.setDiscount(listOriginalData.get(0).getDiscount());
					travEzSetup.setPoi(listOriginalData.get(0).getPoi());
					travEzSetup.setPremiumRate(listOriginalData.get(0).getPremiumRate());
					travEzSetup.setGstEffDate(listOriginalData.get(0).getGstEffDate());
					travEzSetup.setSstEffDate(listOriginalData.get(0).getSstEffDate());
					travEzSetup.setGst(listOriginalData.get(0).getGst());
					travEzSetup.setSst(listOriginalData.get(0).getSst());

					List<TravEzSetup> listOriginalProduct = new ArrayList<>();
					listOriginalProduct.add(travEzSetup);
					model.addAttribute("listOriginalTravelEzyProduct", listOriginalProduct);
					System.out.println("-----------------------------");
				} // inner if
			} // outer if

			// Change Plan Info
			if (!listapprovallog.isEmpty()) {
				convertBytes process = new convertBytes();

				Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
				List<TravEzSetup> listChangeData;
				// object convert into AgentProdMap
				if (changeData != null) {
					listChangeData = (List<TravEzSetup>) changeData;
					System.out.println(" Change Data List Size ::::::         " + listChangeData.size());

					TravEzSetup travEzSetup = new TravEzSetup();

					travEzSetup.setCompanyId(listChangeData.get(0).getCompanyId());
					travEzSetup.setProductCode(listChangeData.get(0).getProductCode());
					travEzSetup.setStampDuty(listChangeData.get(0).getStampDuty());
					travEzSetup.setDiscount(listChangeData.get(0).getDiscount());
					travEzSetup.setPoi(listChangeData.get(0).getPoi());
					travEzSetup.setPremiumRate(listChangeData.get(0).getPremiumRate());
					travEzSetup.setGstEffDate(listChangeData.get(0).getGstEffDate());
					travEzSetup.setSstEffDate(listChangeData.get(0).getSstEffDate());
					travEzSetup.setGst(listChangeData.get(0).getGst());
					travEzSetup.setSst(listChangeData.get(0).getSst());

					List<TravEzSetup> listChangeProduct = new ArrayList<>();
					listChangeProduct.add(travEzSetup);
					model.addAttribute("listChangeTravelEzyProduct", listChangeProduct);
					System.out.println("-----------------------------");
				}
			}
		} // try end
		catch (Exception e) {
			System.out.println("Exception in Convert Object to Byte :" + e);
		}

		return "agent-product-approval";
	}

	@RequestMapping(value = "/approveTravelEzyProductChange")
	public String approveTravelEzyProductChange(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("TravelEzy Product Info Change Approve");
		HttpSession session = request.getSession();
		ApprovalLog alog = new ApprovalLog();
		try {
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			System.out.println("Admin TravelEzy Product Controller loginUser" + loginUser);

			// *************Update the new change Plan info in Plan table
			// ********************

			String aid = request.getParameter("approvalLogId");
			System.out.println("aid >>>>>" + aid);
			ApprovalLogExample approvalexample = new ApprovalLogExample();
			ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();

			if (null != aid && !aid.isEmpty()) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
				System.out.println("   list size in approval list  :::   " + listapprovallog.size());

				// Change Data
				if (!listapprovallog.isEmpty()) {
					String status = listapprovallog.get(0).getStatus();
					short alogId = listapprovallog.get(0).getId();
					System.out.println("Status  :  " + status);
					System.out.println("Approvallog Id  :  " + alogId);

					convertBytes process = new convertBytes();
					Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
					List<TravEzSetup> listChangeData;
					// object convert into Travel Ezy
					if (changeData != null) {
						listChangeData = (List<TravEzSetup>) changeData;
						System.out.println("  list agent prod map size ::::::         " + listChangeData.size());

						TravEzSetupExample travEzSetupExample = new TravEzSetupExample();
						TravEzSetup travEzSetup = new TravEzSetup();
						TravEzSetupExample.Criteria productParamCriteria = travEzSetupExample.createCriteria();
						productParamCriteria.andCompanyIdEqualTo(listChangeData.get(0).getCompanyId());
						productParamCriteria.andProductCodeEqualTo(listChangeData.get(0).getProductCode());

						travEzSetup.setCompanyId(listChangeData.get(0).getCompanyId());
						travEzSetup.setProductCode(listChangeData.get(0).getProductCode());
						travEzSetup.setStampDuty(listChangeData.get(0).getStampDuty());
						travEzSetup.setDiscount(listChangeData.get(0).getDiscount());
						travEzSetup.setPoi(listChangeData.get(0).getPoi());
						travEzSetup.setPremiumRate(listChangeData.get(0).getPremiumRate());
						travEzSetup.setGstEffDate(listChangeData.get(0).getGstEffDate());
						travEzSetup.setSstEffDate(listChangeData.get(0).getSstEffDate());
						travEzSetup.setGst(listChangeData.get(0).getGst());
						travEzSetup.setSst(listChangeData.get(0).getSst());
						travEzSetup.setModifiedBy(listChangeData.get(0).getModifiedBy());
						travEzSetup.setModifiedDt(listChangeData.get(0).getModifiedDt());

						// *************Update the new change Plan Info in original table
						// ********************
						travEzSetupMapper.updateByExampleSelective(travEzSetup, travEzSetupExample);

						// Update status in DSP_ADM_TBL_Approval_Log table
						alog.setId(alogId);
						alog.setStatus("3");
						alog.setChecker((short) Integer.parseInt(loginUser)); // get from the current login session
						alog.setUpdateDate(new Date());
						approvalLogMapper.updateByPrimaryKeySelective(alog);
					}
				}
			}

		} // try end

		catch (Exception e) {
			System.out.println("Exception Update new Product Information : " + e);
		}

		return "redirect:/getApprovalList";

	}

	// Reject Action
	@RequestMapping(value = "/rejectTravelEzyProductChange", method = RequestMethod.GET)
	public String rejectTravelEzyProductChange(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Admin Travel Ezy Controller loginUser" + loginUser);

		ApprovalLogExample approvalLogexample = new ApprovalLogExample();
		ApprovalLog alog = new ApprovalLog();
		ApprovalLogExample.Criteria createCriteria = approvalLogexample.createCriteria();
		String aid = request.getParameter("approvalLogId");
		if (null != aid && !aid.isEmpty()) {
			Integer testint = new Integer(aid);
			createCriteria.andIdEqualTo(testint.shortValue());
			List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalLogexample);
			System.out.println("   list size in approval list  :::   " + listapprovallog.size());

			short alogId = listapprovallog.get(0).getId();
			alog.setId(alogId);
			alog.setStatus("2");
			alog.setChecker((short) Integer.parseInt(loginUser)); // get from the current login session
			alog.setUpdateDate(new Date());
			approvalLogMapper.updateByPrimaryKeySelective(alog);
		}

		return "redirect:/getApprovalList";
	}

	// ---------------------------------------------------------------END Travel Ezy
	// Product Information
	// Change------------------------------------------------------------------------------
}// Main class end
