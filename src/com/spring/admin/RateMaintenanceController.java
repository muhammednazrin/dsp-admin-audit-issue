package com.spring.admin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.spring.VO.roadtaxVO.ConsignmentFileDetail;
import com.spring.VO.roadtaxVO.ConsignmentNumberMaintenance;
import com.spring.VO.roadtaxVO.FileFetchDirectories;
import com.spring.VO.roadtaxVO.RdtaxRateMaintenance;
import com.spring.mapper.roadtax.ConsignmentNumberMaintenanceMapper;
import com.spring.mapper.roadtax.RdtaxRateMaintenanceMapper;

@Controller
@RequestMapping("rtx")
public class RateMaintenanceController {
	ConsignmentNumberMaintenanceMapper consignmentNumberMaintenanceMapper;
	RdtaxRateMaintenanceMapper rdtaxRateMaintenanceMapper;

	String UPLOADED_FOLDER = "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs/roadtaxcon/";
	// String UPLOADED_FOLDER = "C:/Users/staff01/Desktop/backup-file-25-9-2017/";

	@Autowired
	public RateMaintenanceController(ConsignmentNumberMaintenanceMapper consignmentNumberMaintenanceMapper,
			RdtaxRateMaintenanceMapper rdtaxRateMaintenanceMapper) {
		this.consignmentNumberMaintenanceMapper = consignmentNumberMaintenanceMapper;
		this.rdtaxRateMaintenanceMapper = rdtaxRateMaintenanceMapper;
	}

	// Added by Arbind for Consignment Number Maintenance
	@RequestMapping("/consignmentNumberMaintenance")
	public String consignmentNumberMaintenance(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadtax-admin-login";
			return "admin-login";
		}

		int usedCN = consignmentNumberMaintenanceMapper.countByExample("Y");
		int remaining = consignmentNumberMaintenanceMapper.countByExample("N");

		model.addAttribute("usedCN", usedCN);
		model.addAttribute("totalCN", usedCN + remaining);
		model.addAttribute("remaining", remaining);

		return "roadtax/pages/consignmentNumberMaintenance";
	}

	@RequestMapping("/getConsignmentRate")
	@ResponseBody
	public String getConsignmentRate(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam("uploadFileConsignment") MultipartFile fileupload) {

		try {
			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");

			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				// return "roadtax/pages/roadtax-admin-login";
				return "admin-login";
			}
			Integer noOfConsignmentList = consignmentNumberMaintenanceMapper.countByExample("N");
			JSONObject responseDetailsJson = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			if (noOfConsignmentList < 0) {
				// If the consignment numbers are not fully used.......
				responseDetailsJson.put("status", "1");
				jsonArray.put(responseDetailsJson);
				String jsonString = jsonArray.toString();
				System.out.println("json string ;;;;;;     444444444444444444" + jsonString);
				request.getRequestDispatcher("/roadtax/pages/consignmentNumberMaintenance.jsp").forward(request,
						response);
			} else {
				// If the consignment numbers not fully used.......
				System.out.println("consignment numbers are fully used");
				responseDetailsJson.put("status", "0");
				return uploadFileControl(request, response, model, fileupload);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		// e.printStackTrace();
		return null;
	}

	@RequestMapping("/uploadFileControl")
	public String uploadFileControl(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam("uploadFileConsignment") MultipartFile fileupload) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		try {
			// if(loginUser == null) {
			// String sessionexpired = "Session Has Been Expired";
			// model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadtax-admin-login";
			// }

			List<ConsignmentNumberMaintenance> ConsignmentList = consignmentNumberMaintenanceMapper
					.selectByPrimaryKey();
			JSONObject responseDetailsJson = new JSONObject();
			JSONArray jsonArray = new JSONArray();

			List<String> consignmentlistdata = new ArrayList<String>();
			for (int i = 0; i < ConsignmentList.size(); i++) {
				consignmentlistdata.add(ConsignmentList.get(i).getConsignmentNo());
			}

			// System.out.println(" consignment numbers :::::::::::::::::::::::::::
			// ----------- "+consignmentlistdata);

			File convFile = new File(fileupload.getOriginalFilename());
			convFile.createNewFile();
			FileOutputStream fos = new FileOutputStream(convFile);
			fos.write(fileupload.getBytes());
			fos.close();

			FileInputStream inputStream = new FileInputStream(convFile);
			Workbook workbook = new XSSFWorkbook(inputStream);
			Sheet firstSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = firstSheet.iterator();
			boolean checkIterationstatus = false;
			List<String> datalist = new ArrayList<String>();
			while (iterator.hasNext()) {
				Row nextRow = iterator.next();
				Iterator<Cell> cellIterator = nextRow.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						ConsignmentNumberMaintenance consignmentnumberrc = new ConsignmentNumberMaintenance();
						consignmentnumberrc.setIsUsed("N");
						consignmentnumberrc.setConsignmentNo(cell.getStringCellValue());
						if (consignmentlistdata.size() > 0) {
							if (consignmentlistdata.contains(cell.getStringCellValue())) {
								checkIterationstatus = true;
								break;
							} else {
								Thread.sleep(20);
								System.out.println("inserting::  " + cell.getStringCellValue());
								consignmentNumberMaintenanceMapper.insert(consignmentnumberrc);
							}
						} else {

							Thread.sleep(20);
							consignmentNumberMaintenanceMapper.insert(consignmentnumberrc);
						}
						break;
					case Cell.CELL_TYPE_BOOLEAN:
						// System.out.print(cell.getBooleanCellValue());
						break;
					case Cell.CELL_TYPE_NUMERIC:
						// System.out.print(cell.getNumericCellValue());
						break;
					}
				}
				System.out.println();
			}

			// if(checkIterationstatus){
			if (!checkIterationstatus) {

				StringJoiner sj = new StringJoiner(" , ");
				MultipartFile file = fileupload;
				if (file.isEmpty()) {
					System.out.println("Please specify a file...");
				}
				try {
					byte[] bytes = file.getBytes();
					Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
					Files.write(path, bytes);
					sj.add(file.getOriginalFilename());
				} catch (IOException e) {
					e.printStackTrace();
				}
				String uploadedFileName = sj.toString();
				if (uploadedFileName.isEmpty()) {
					System.out.println("Please select a file to upload ");
				} else {
					System.out.println("You successfully uploaded ");
				}

			} else {

			}
			// workbook.close();
			inputStream.close();
			request.getRequestDispatcher("/roadtax/pages/consignmentNumberMaintenance.jsp").forward(request, response);

		} catch (Exception e) {

		}
		return null;
	}

	//
	@RequestMapping("/fetchConsignmentFiles")
	@ResponseBody
	public String fetchConsignmentFiles(HttpServletRequest request, Model model) {
		try {
			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				// return "roadtax/pages/roadtax-admin-login";
				return "admin-login";
			}

			File folderpath = new File(UPLOADED_FOLDER);
			List<ConsignmentFileDetail> consignmentList = new FileFetchDirectories().fetchDirectories(folderpath,
					UPLOADED_FOLDER);
			JSONArray responsear = new JSONArray();

			for (int i = 0; i < consignmentList.size(); i++) {
				JSONObject reponseobj = new JSONObject();
				ConsignmentFileDetail consignment = consignmentList.get(i);
				reponseobj.put("fileName", consignment.getFilename());
				reponseobj.put("filePath", consignment.getFilePath());
				reponseobj.put("fileCreationDate", consignment.getFilecreationdate());
				reponseobj.put("fileStatus", consignment.getFilestatus());
				responsear.put(reponseobj);
			}

			return responsear.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

		JSONArray responsear = new JSONArray();
		JSONObject reponseobj = new JSONObject();

		return responsear.toString();

	}

	@RequestMapping("/deleteConsignmentFile")
	@ResponseBody
	public String deleteConsignmentFile(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam("consignmentFile") String consignmentFile) {
		try {
			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				// return "roadtax/pages/roadtax-admin-login";
				return "admin-login";
			}

			System.out.println(" file status :::::   " + consignmentFile);
			JSONArray responsear = new JSONArray();
			JSONObject reponseobj = new JSONObject();
			if (consignmentFile != null) {
				File consignmentfile = new File(UPLOADED_FOLDER + consignmentFile);
				if (consignmentfile.exists()) {
					consignmentfile.delete();
					reponseobj.put("status", "1");
					responsear.put(reponseobj);
				} else {
					reponseobj.put("status", "0");
					responsear.put(reponseobj);
				}
			}

			return responsear.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

		JSONArray responsear = new JSONArray();
		JSONObject reponseobj = new JSONObject();
		return responsear.toString();

	}

	@RequestMapping("/roadTaxRateMaintenance")
	public String roadTaxRateMaintenance(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadtax-admin-login";
			return "admin-login";
		}
		return "roadtax/pages/roadtaxRateMaintenance";
	}

	@RequestMapping("/getRatemaintenenaceReport")
	@ResponseBody
	public String getRatemaintenenaceReport(HttpServletRequest request, Model model, @RequestParam String rigionid,
			@RequestParam String carttype) {

		try {
			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");

			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				// return "roadtax/pages/roadtax-admin-login";
				return "admin-login";
			}
			List<RdtaxRateMaintenance> rdtaxRatemaintenancelist = rdtaxRateMaintenanceMapper.selectByExample(rigionid,
					carttype);
			session.setAttribute("roadTaxRatemaintenanceReportList", rdtaxRatemaintenancelist);
			JSONObject responseDetailsJson = new JSONObject();
			JSONArray jsonArray = new JSONArray();

			for (int i = 0; i < rdtaxRatemaintenancelist.size(); i++) {
				JSONObject formDetailsJson = new JSONObject();
				formDetailsJson.put("cal_id", rdtaxRatemaintenancelist.get(i).getCalId());
				formDetailsJson.put("fromrange", rdtaxRatemaintenancelist.get(i).getFromrange());
				formDetailsJson.put("torange", rdtaxRatemaintenancelist.get(i).getTorange());
				formDetailsJson.put("base_rate", rdtaxRatemaintenancelist.get(i).getBaseRate());
				formDetailsJson.put("progressive_rate", rdtaxRatemaintenancelist.get(i).getProgressiveRate());
				jsonArray.put(formDetailsJson);
			}
			String jsonString = jsonArray.toString();
			// String jsonString = new Gson().toJson(jsonArray);
			return jsonString;

		} catch (Exception e) {
			e.printStackTrace();
		}
		// e.printStackTrace();
		return null;
	}

	@RequestMapping("/redirectAddRateMaintenance")
	public String redirectAddRateMaintenance(HttpServletRequest request, Model model) {

		return "roadtax/pages/roadtaxRateMaintenanceAdd";

	}

	@RequestMapping("/deleteRateMaintenanceRow")
	@ResponseBody
	public String deleteRateMaintenanceRow(HttpServletRequest request, Model model, @RequestParam Integer rowidvalue) {
		String status = null;
		Integer returndata = 0;
		try {
			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				// return "roadtax/pages/roadtax-admin-login";
				return "admin-login";
			}

			returndata = rdtaxRateMaintenanceMapper.deleteByExample(rowidvalue);
			JSONObject jsobj = new JSONObject();
			jsobj.put("status", "1");
			status = jsobj.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;

	}

	@RequestMapping("/addRateMaintenanceForRegion")
	@ResponseBody
	public String addRateMaintenanceForRegion(HttpServletRequest request, Model model,
			@RequestParam String regionidvalue, @RequestParam String cartypeidvalue,
			@RequestParam(value = "fromrangearray[]") String[] fromrangearray,
			@RequestParam(value = "torangearray[]") String[] torangearray,
			@RequestParam(value = "baseratearray[]") String[] baseratearray,
			@RequestParam(value = "progressratearray[]") String[] progressratearray) {

		try {
			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");

			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				// return "roadtax/pages/roadtax-admin-login";
				return "admin-login";
			}
			JSONObject responseDetailsJson = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			List<RdtaxRateMaintenance> rdtaxRatemaintenancelist = rdtaxRateMaintenanceMapper.selectRateMaintenance();

			for (int i = 0; i < fromrangearray.length; i++) {
				for (int j = 0; j < rdtaxRatemaintenancelist.size(); j++) {
					String fromrangename = fromrangearray[i];
					String torangename = torangearray[i];
					String fromrange_table_data = rdtaxRatemaintenancelist.get(j).getFromrange();
					String torange_table_data = rdtaxRatemaintenancelist.get(j).getTorange();

					if (Integer.parseInt(fromrangename) < Integer.parseInt(torangename)) {
						if (Integer.parseInt(fromrangename) > Integer.parseInt(torange_table_data)
								|| Integer.parseInt(fromrangename) < Integer.parseInt(fromrange_table_data)) {

							RdtaxRateMaintenance ratemaintenanceVO = new RdtaxRateMaintenance();
							ratemaintenanceVO.setCcRange(fromrangename + " to " + torangename);
							ratemaintenanceVO.setCcRangeName(fromrangename + " to " + torangename);
							BigDecimal baserate = new BigDecimal(baseratearray[i]);
							ratemaintenanceVO.setBaseRate(baserate);
							BigDecimal progressiverate = new BigDecimal(progressratearray[i]);
							ratemaintenanceVO.setProgressiveRate(progressiverate);
							ratemaintenanceVO.setTotalRange(null);
							ratemaintenanceVO.setCreatedDate(null);
							ratemaintenanceVO.setUpdatedDate(null);
							ratemaintenanceVO.setStatus("ACTIVE");
							BigDecimal regionVal = new BigDecimal(regionidvalue);
							ratemaintenanceVO.setRegion(regionVal);
							ratemaintenanceVO.setSaloon(cartypeidvalue);
							ratemaintenanceVO.setFromrange(fromrangename);
							ratemaintenanceVO.setTorange(torangename);
							rdtaxRateMaintenanceMapper.insert(ratemaintenanceVO);
							break;
						} else {
							responseDetailsJson.put("status", "Overlapping of fromrange between exsting data");
							jsonArray.put(responseDetailsJson);
							String jsonString = jsonArray.toString();
							return jsonString;
						}

					} else {
						responseDetailsJson.put("status", "Fromrange shouldn't be less than torange");
						jsonArray.put(responseDetailsJson);
						String jsonString = jsonArray.toString();
						return jsonString;
					}
				}
			}

			responseDetailsJson.put("status", "1");
			jsonArray.put(responseDetailsJson);

			String jsonString = jsonArray.toString();
			return jsonString;

		} catch (Exception e) {
			e.printStackTrace();
		}
		// e.printStackTrace();
		return null;
	}

	@RequestMapping("/updateRateMaintenanceForRegion")
	@ResponseBody
	public String updateRateMaintenanceForRegion(HttpServletRequest request, Model model,
			@RequestParam String rowidvalue, @RequestParam(value = "datafieldArray[]") String[] datafieldArray,
			@RequestParam(value = "dataArray[]") String[] dataArray) {

		try {
			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");

			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				// return "roadtax/pages/roadtax-admin-login";
				return "admin-login";
			}

			JSONObject responseDetailsJson = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			Map<String, String> datamap = new HashMap<String, String>();
			for (int i = 0; i < dataArray.length; i++) {
				datamap.put(String.valueOf(datafieldArray[i]), String.valueOf(dataArray[i]));
			}

			RdtaxRateMaintenance ratemaintenanceVO = new RdtaxRateMaintenance();
			ratemaintenanceVO
					.setCcRange(datamap.get("fromrange").toString() + " to " + datamap.get("torange").toString());
			ratemaintenanceVO
					.setCcRangeName(datamap.get("fromrange").toString() + " to " + datamap.get("torange").toString());
			BigDecimal baserate = new BigDecimal(datamap.get("base_rate").toString());
			ratemaintenanceVO.setBaseRate(baserate);
			BigDecimal progressiverate = new BigDecimal(datamap.get("progressive_rate").toString());
			ratemaintenanceVO.setProgressiveRate(progressiverate);
			BigDecimal calid = new BigDecimal(datamap.get("cal_id").toString());
			ratemaintenanceVO.setCalId(calid);
			ratemaintenanceVO.setFromrange(datamap.get("fromrange").toString());
			ratemaintenanceVO.setTorange(datamap.get("torange").toString());
			// ratemaintenanceVO.setTotalRange(baserate);
			int resultUpdate = rdtaxRateMaintenanceMapper.updateByExample(ratemaintenanceVO);
			JSONObject formDetailsJson = new JSONObject();
			if (resultUpdate == 1) {
				formDetailsJson.put("status", "success");
			} else {
				formDetailsJson.put("status", "failed");
			}
			jsonArray.put(formDetailsJson);
			String jsonString = jsonArray.toString();
			return jsonString;

		} catch (Exception e) {
			e.printStackTrace();
		}
		// e.printStackTrace();
		return null;
	}

}
