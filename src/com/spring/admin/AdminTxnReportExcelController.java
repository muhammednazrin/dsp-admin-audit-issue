package com.spring.admin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
import java.util.ArrayList;
//import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
//import java.util.Date;
//import java.util.HashMap;
import java.util.List;
//import java.util.Map;
import java.util.Locale;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.spring.VO.report.ALLReportVO;
import com.spring.VO.report.AdminTxnReportVO;
import com.spring.VO.report.HOHHReportExcelVO;
import com.spring.VO.report.TLReportExcelVO;
//import com.spring.utils.ServiceValidationUtils;
import com.spring.VO.report.TransSearchObject;
import com.spring.VO.report.WTCReportExcelVO;
import com.spring.mapper.report.ALLReportMapper;
import com.spring.mapper.report.HOHHReportExcelMapper;
import com.spring.mapper.report.MotorReportExcelMapper;
import com.spring.mapper.report.TLReportExcelMapper;
import com.spring.mapper.report.WTCReportExcelMapper;
import com.spring.utils.ServiceValidationUtils;

@Controller
public class AdminTxnReportExcelController {

	MotorReportExcelMapper motorReportExcelMapper;
	TLReportExcelMapper tlReportExcelMapper;
	WTCReportExcelMapper wtcReportExcelMapper;
	HOHHReportExcelMapper hohhReportExcelMapper;
	ALLReportMapper allReportMapper;

	@Autowired
	public AdminTxnReportExcelController(MotorReportExcelMapper motorReportExcelMapper,
			TLReportExcelMapper tlReportExcelMapper, WTCReportExcelMapper wtcReportExcelMapper,
			HOHHReportExcelMapper hohhReportExcelMapper, ALLReportMapper allReportMapper) {
		this.motorReportExcelMapper = motorReportExcelMapper;
		this.tlReportExcelMapper = tlReportExcelMapper;
		this.wtcReportExcelMapper = wtcReportExcelMapper;
		this.hohhReportExcelMapper = hohhReportExcelMapper;
		this.allReportMapper = allReportMapper;

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/generateAdminTxnExcel", method = RequestMethod.GET)
	public String generateAdminTxnExcel(HttpServletRequest request, HttpServletResponse response, Model model)
			throws ServletException, IOException {

		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		ServletContext servletContext = request.getSession().getServletContext();

		String productType = (String) session.getAttribute("ProductType");
		String productEntity = (String) session.getAttribute("ProductEntity");
		String status = (String) session.getAttribute("Status");
		String PolicyCertificateNo = (String) session.getAttribute("PolicyCertificateNo");
		String nRIC = (String) session.getAttribute("NRIC");
		String receiptNo = (String) session.getAttribute("ReceiptNo");
		String dateFrom = (String) session.getAttribute("dateFrom");
		String dateTo = (String) session.getAttribute("dateTo");
		String plateNo = (String) session.getAttribute("PlateNo");

		/*
		 * System.out.println("productType  -> "+productType);
		 * System.out.println("productEntity  -> "+productEntity);
		 * System.out.println("status  -> "+status);
		 * System.out.println("PolicyCertificateNo  -> "+PolicyCertificateNo);
		 * System.out.println("nRIC  -> "+nRIC);
		 * System.out.println("receiptNo  -> "+receiptNo);
		 * System.out.println("dateFrom  -> "+dateFrom);
		 * System.out.println("dateTo  -> "+dateTo);
		 * System.out.println("plateNo  -> "+plateNo);
		 */

		// start : motor excel file
		if (productType.indexOf("M") != -1) {
			TransSearchObject transSearchObject_Motor = new TransSearchObject();
			List<AdminTxnReportVO> motorReportList = new ArrayList<AdminTxnReportVO>();
			/*
			 * List<MotorReportExcelVO> motorReportList_B4Pay = new
			 * ArrayList<MotorReportExcelVO>();
			 */
			List<String> transactionStatusList = new ArrayList<String>();

			String query1 = "n";
			String query2 = "n";

			if ("EIB".equalsIgnoreCase(productEntity) && "".equalsIgnoreCase(productType)) {

				transSearchObject_Motor.setProductCode("MI");
			}

			if ("ETB".equalsIgnoreCase(productEntity) && "".equalsIgnoreCase(productType)) {

				transSearchObject_Motor.setProductCode("MT");
			}
			if ("".equalsIgnoreCase(productType) || productType == null) {
				transSearchObject_Motor.setProductCode("MI,MT");
			}

			if ("MI".equalsIgnoreCase(productType) || "MT".equalsIgnoreCase(productType)) {
				transSearchObject_Motor.setProductCode(productType);

			}
			if ("".equalsIgnoreCase(productType) || productType == null) {
				if ("".equalsIgnoreCase(status) || status == null) { // productType empty and transaction status empty
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transSearchObject_Motor.setTransactionStatus(transactionStatusList);
					query1 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_Motor.setTransactionStatus(transactionStatusList);
				}
			} else { // if Product is Chosen
				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_Motor.setTransactionStatus(transactionStatusList);
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_Motor.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus(status);
				}
			}
			if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)) {
				query1 = "y";
			}
			if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
				query2 = "y";
			}

			if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
				transSearchObject_Motor.setPolicyNo(PolicyCertificateNo);
			}
			if (!"".equalsIgnoreCase(nRIC)) {
				transSearchObject_Motor.setNricNo(nRIC);
			}
			if (!"".equalsIgnoreCase(receiptNo)) {
				transSearchObject_Motor.setInvoiceNo(receiptNo);
			}

			if (!ServiceValidationUtils.isEmptyStringTrim(plateNo)) {
				transSearchObject_Motor.setVehicleNo(plateNo);
			}

			transSearchObject_Motor.setDateFrom(dateFrom);
			transSearchObject_Motor.setDateTo(dateTo);

			// if query 1
			/*
			 * if ("y".equalsIgnoreCase(query1)) { motorReportList=motorReportExcelMapper.
			 * selectMotorTransactionalReportAfterPayment(transSearchObject_Motor);
			 *
			 * } // End if query1 is Executed
			 *
			 *
			 * // if query 2 if ("y".equalsIgnoreCase(query2)) {
			 * motorReportList_B4Pay=motorReportExcelMapper.
			 * selectMotorTransactionalReportBeforePayment(transSearchObject_Motor); }
			 *
			 * motorReportList.addAll(motorReportList_B4Pay);
			 *
			 * Collections.sort(motorReportList, new Comparator<MotorReportExcelVO>() {
			 *
			 * @Override public int compare(MotorReportExcelVO o1, MotorReportExcelVO o2) {
			 * return o1.getRecord_date().compareTo(o2.getRecord_date()); } });
			 *
			 * Collections.reverse(motorReportList);
			 */

			// List<AdminTxnReportVO> adminTxnReportVOList = new
			// ArrayList<AdminTxnReportVO>();
			if (session.getAttribute("adminTxnReportList") != null
					|| !"".equals(session.getAttribute("adminTxnReportList"))) {
				motorReportList = (ArrayList<AdminTxnReportVO>) session.getAttribute("adminTxnReportList");
				System.out.println("generateAdminTxnExcel<size> => " + motorReportList.size());
			}

			if (motorReportList.size() <= 0) {
				model.addAttribute("emptySearch", "No Record Found");
			}
			model.addAttribute("motorReportList", motorReportList);

			// start excel

			Random ran = new Random();
			int randomNo = ran.nextInt(6) + 5;

			DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
			Date date = new Date();

			String file = "EtiqaTransactionReport" + dateFormat.format(date) + randomNo + ".xlsx";

			String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
			// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Etiqa Transaction Report");

			// header fields based on product
			String[] fields = null;

			fields = new String[] { "Id", "DSPQQID", "Quotation Creation Date", "Txn Id", "Txn Start Date",
					"Txn End Date", "Entity", "Policy No", "CAPS Policy No", "Vehicle No", "Chassis No", "Engine No",
					"Previous Insurance", "Policy Status", "Txn Status", "Abandonment", "Receipt No", "EIP Txn Id",
					"Agent Code", "Agent Name", "Operator Code", "Promo Code", "NCD", "Payment Mode", "FPX Ref",
					"EBPG Ref", "M2U Ref", "No of Seat", "Cubic Capacity", "Make", "Model", "NVIC", "Year make",
					"Period of Insured", "Sum Insured", "SI Proposed", "SI Min", "SI Max", "LLOP", "LL2P", "Flood",
					"NCD Relief", "SRCC", "Windscreen SC", "Windscreen PM", "Vehicle Acc SC", "Vehicle Acc PM",
					"NGV Gas SC", "NGV Gas PM", "CART", "Payment Payable", "Gross Premium", "Discount", "GST",
					"Motor Comm", "CAPS Comm", "No of Drivers", "Driver 1", "Driver 2", "Driver 3", "Driver 4",
					"Driver 5", "Driver 6", "Driver 7", "CAPS", "Excess", "Loading Percentage", "Loading",
					"Insured name", "Id Type", "Insured ID", "Gender", "Age", "Email", "Contact No", "Address1",
					"Address2", "Address3", "Postcode", "State", "RiskAddress1", "RiskAddress2", "RiskAddress3",
					"RiskPostcode", "RiskState", "Tax Invoice No", "JPJ Msg", "CARSECM", "ATITFTCD", "GARAGECD",
					"VPMS Version"
					/*
					 * "RTAttn", "RTAmount", "RTPF", "RTPFGST", "RTPRF", "RTPRFGST", "RTDF",
					 * "RTDFGST", "RTTotalPayable", "RTPeriod", "RTAddress1", "RTAddress2",
					 * "RTAddress3", "RTPostcode", "RTState", "RTContactNo", "RTStatus", "RTRemark",
					 * "RTConsignmentNo", "TrackingNo", "Bank Name", "Bank Account Number",
					 * "RTTaxinvNo", "JPJ Status"
					 */
			};

			Row row1 = sheet.createRow(0);

			for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
				Cell cell = row1.createCell(cellIndex);
				cell.setCellValue(fields[cellIndex]);
			}

			String[] fields2 = null;

			int i = 1;

			for (AdminTxnReportVO motorDetail : motorReportList) {

				String Abandonment = "";
				String EIPTxnId = "";
				String RiskAddress3 = "";
				String txnStatus = "";
				String quotationCreationDate = "";
				String transactionStartDateTime = "";
				String transactionEndDateTime = "";
				String proEntity = "";
				String paymentChannel = "";
				String SIMin = "";
				String SIMax = "";
				String cartPremium = "";
				String cartPremiumDesc = "";
				String firstDriverInfo = "";
				String secondDriverInfo = "";
				String thirdDriverInfo = "";
				String forthDriverInfo = "";
				String fifthDriverInfo = "";
				String sixthDriverInfo = "";
				String seventhDriverInfo = "";
				String age = "";

				if (null != motorDetail.getPmnt_gateway_code() && "" != motorDetail.getPmnt_gateway_code()) {
					if (motorDetail.getPmnt_gateway_code().indexOf("EBPG") != -1) {

						paymentChannel = "EBPG";
					}

					else if (motorDetail.getPmnt_gateway_code().indexOf("AMEX") != -1) {

						paymentChannel = "AMEX";
					}

					else if (motorDetail.getPmnt_gateway_code().indexOf("FPX") != -1) {

						paymentChannel = "FPX";
					}

					else if (motorDetail.getPmnt_gateway_code().indexOf("Maybank2U") != -1) {

						paymentChannel = "M2U";
					}

				}
				if (motorDetail.getProduct_code().equals("MI")) {

					proEntity = "EIB";
				}

				else if (motorDetail.getProduct_code().equals("MT"))

				{

					proEntity = "ETB";

				}

				if (motorDetail.getPmnt_status().equals("S")) {

					txnStatus = "Successful";

				} else if (motorDetail.getPmnt_status().equals("F")) {

					txnStatus = "Fail";

				}

				else if (motorDetail.getPmnt_status().equals("AP")) {

					txnStatus = "Attempt Payment";

				}

				else if (motorDetail.getPmnt_status().equals("O")) {

					Abandonment = motorDetail.getLast_page();
					txnStatus = "Incomplete";

				}

				else if (motorDetail.getPmnt_status().equals("C")) {

					txnStatus = "Cancelled";

				}

				else if (motorDetail.getPmnt_status().equals("R")) {

					txnStatus = "Rejection : " + motorDetail.getReason();

				}

				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				Date quotationCreationDateTmp = null;

				try {
					if (null != motorDetail.getQuotationCreateDateTime()) {
						quotationCreationDateTmp = formatter.parse(motorDetail.getQuotationCreateDateTime());
						quotationCreationDate = ft.format(quotationCreationDateTmp);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}

				if (motorDetail.getPmnt_status().equals("S") || motorDetail.getPmnt_status().equals("F")
						|| motorDetail.getPmnt_status().equals("AP")) {

					transactionStartDateTime = motorDetail.getTransaction_datetime();

				}

				if (null != motorDetail.getMarketValue() && "" != motorDetail.getMarketValue()
						&& null != motorDetail.getMarkUpPer() && "" != motorDetail.getMarkUpPer()) {
					DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(Locale.US);
					format.setParseBigDecimal(true);
					BigDecimal amt1 = new BigDecimal(0.00);
					BigDecimal amt2 = new BigDecimal(0.00);
					BigDecimal amt3 = new BigDecimal(100.00);
					try {
						amt1 = (BigDecimal) format.parse(motorDetail.getMarketValue());
						amt2 = (BigDecimal) format.parse(motorDetail.getMarkUpPer());

						BigDecimal tmpAmount = amt2.multiply(amt1).divide(amt3);
						BigDecimal sMin = amt1.subtract(tmpAmount);
						BigDecimal sMax = amt1.add(tmpAmount);

						SIMin = String.valueOf(sMin);
						SIMax = String.valueOf(sMax);

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				if (motorDetail.getCartPremium() != null) {

					cartPremium = motorDetail.getCartPremium();

				}

				if (motorDetail.getCartPremiumDesc() != null) {

					String tmpCARTDesc = motorDetail.getCartPremiumDesc().replaceAll(",", "|");
					tmpCARTDesc = tmpCARTDesc.replaceAll("Days", "");
					tmpCARTDesc = tmpCARTDesc.replaceAll("RM", "");
					tmpCARTDesc = tmpCARTDesc.replaceAll("/day", "");

					cartPremiumDesc = tmpCARTDesc + "|";

				}

				if (!motorDetail.getFirstDriverInfo().equals("||||||")) {

					firstDriverInfo = motorDetail.getFirstDriverInfo();
				}

				if (!motorDetail.getSecondDriverInfo().equals("||||||")) {

					secondDriverInfo = motorDetail.getSecondDriverInfo();
				}
				if (!motorDetail.getThirdDriverInfo().equals("||||||")) {

					thirdDriverInfo = motorDetail.getThirdDriverInfo();
				}
				if (!motorDetail.getForthDriverInfo().equals("||||||")) {

					forthDriverInfo = motorDetail.getForthDriverInfo();
				}
				if (!motorDetail.getFifthDriverInfo().equals("||||||")) {

					fifthDriverInfo = motorDetail.getFifthDriverInfo();
				}
				if (!motorDetail.getSixthDriverInfo().equals("||||||")) {

					sixthDriverInfo = motorDetail.getSixthDriverInfo();
				}

				if (!motorDetail.getSixthDriverInfo().equals("||||||")) {

					seventhDriverInfo = motorDetail.getSeventhDriverInfo();
				}

				if (null != motorDetail.getCustomerDOB() && "" != motorDetail.getCustomerDOB()
						&& "//" != motorDetail.getCustomerDOB()) {

					try {

						String[] dob_split = motorDetail.getCustomerDOB().split("/");

						LocalDate start = LocalDate.of(Integer.parseInt(dob_split[2]), Integer.parseInt(dob_split[1]),
								Integer.parseInt(dob_split[0]));
						LocalDate end = LocalDate.now(); // use for age-calculation: LocalDate.now()
						long tmpAge = ChronoUnit.YEARS.between(start, end);

						age = new Long(tmpAge).toString();

					}

					catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				fields2 = new String[] { Integer.toString(i), motorDetail.getDSPQQID(), quotationCreationDate,
						motorDetail.getPaymentTrxID(), transactionStartDateTime, transactionEndDateTime, proEntity,
						motorDetail.getPolicy_number(), motorDetail.getCaps_dppa(),
						motorDetail.getRegistration_number(), motorDetail.getChassisNo(), motorDetail.getEngineNo(),
						motorDetail.getPreviousInsurance(), motorDetail.getPolicyStatus(), txnStatus, Abandonment,
						motorDetail.getInvoice_no(), EIPTxnId, motorDetail.getAgent_code(), motorDetail.getAgent_name(),
						motorDetail.getOperatorCode(), motorDetail.getDiscountCode(), motorDetail.getNCDAmount(),
						paymentChannel, motorDetail.getFpx_fpxtxnid(), motorDetail.getTransaction_id(),
						motorDetail.getBankrefno(), motorDetail.getVehicleSeat(), motorDetail.getVehicleCC(),
						motorDetail.getVehicleMake(), motorDetail.getVehicleModel(), motorDetail.getNVIC(),
						motorDetail.getYearMake(), motorDetail.getPeriodCoverage(), motorDetail.getSumInsured(),
						motorDetail.getMarketValue(), SIMin, SIMax, motorDetail.getLLOP(), motorDetail.getLL2P(),
						motorDetail.getFloodCover(), motorDetail.getNCDRelief(), motorDetail.getSRCC(),
						motorDetail.getWindscreenSC(), motorDetail.getWindscreenP(), motorDetail.getVehicleAccSC(),
						motorDetail.getVehicleAccP(), motorDetail.getNGVGasSC(), motorDetail.getNGVGasP(),
						cartPremiumDesc + cartPremium, motorDetail.getTotalPremiumPayable(),
						motorDetail.getGrosspremium_final(), motorDetail.getDiscount(), motorDetail.getGst(),
						motorDetail.getAgentCommissionAmount(), motorDetail.getCAPSCommissionAmount(),
						motorDetail.getDriverNo(), firstDriverInfo, secondDriverInfo, thirdDriverInfo, forthDriverInfo,
						fifthDriverInfo, sixthDriverInfo, seventhDriverInfo, motorDetail.getPAPremiumPayable(),
						motorDetail.getExcess(), motorDetail.getLoadingPercentage(), motorDetail.getLoading(),
						motorDetail.getFirstDriver(),
						/* motorDetail.getSecondDriver(), */
						motorDetail.getIDType(), motorDetail.getCustomer_nric_id(), motorDetail.getCustomerGender(),
						age, motorDetail.getCustomerEmail(), motorDetail.getCustomerMobileNo(),
						motorDetail.getCustomerAddress1(), motorDetail.getCustomerAddress2(),
						motorDetail.getCustomerAddress3(), motorDetail.getCustomerPostcode(),
						motorDetail.getCustomerState(), motorDetail.getParkingAddress1(),
						motorDetail.getParkingAddress2(), RiskAddress3, motorDetail.getParkingPostcode(),
						motorDetail.getParkingState(), motorDetail.getInvoice_no(), motorDetail.getJPJStatus(),
						motorDetail.getCARSECM(), motorDetail.getATITFTCD(), motorDetail.getGARAGECD(),
						motorDetail.getFoMotorVer()
						/*
						 * motorDetail.getDeliveryName(), motorDetail.getRoadTaxAmount(),
						 * motorDetail.getRtpf(), motorDetail.getRtpfgst(), motorDetail.getRtprf(),
						 * motorDetail.getRtprfgst(), motorDetail.getRtdf(), motorDetail.getRtdfgst(),
						 * motorDetail.getRttotalpayable(), motorDetail.getPeriod(),
						 * motorDetail.getAddress1(), motorDetail.getAddress2(),
						 * motorDetail.getAddress3(), motorDetail.getPostcode(),
						 * motorDetail.getRtstate(), motorDetail.getMobile(),
						 * motorDetail.getPrintStatus(), motorDetail.getRemarks(),
						 * motorDetail.getConsignmentNo(), motorDetail.getTrackingNo(),
						 * motorDetail.getBankName(), motorDetail.getAccountNo(),
						 * motorDetail.getRdtaxInvoice(), motorDetail.getMessage()
						 */
				};

				Row row2 = sheet.createRow(i);

				for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
					Cell cell = row2.createCell(cellIndex);
					cell.setCellValue(fields2[cellIndex]);
				}

				i++;

			}

			// System.out.println("txnStatus "+txn.getStatus());

			try {
				FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
				workbook.write(outputStream);
				workbook.close();

				File fileToDownload = new File(relativeWebPath);

				FileInputStream inputStream = new FileInputStream(fileToDownload);
				response.setContentType(servletContext.getMimeType(relativeWebPath));
				response.setContentLength((int) fileToDownload.length());

				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
				response.setHeader(headerKey, headerValue);

				// get output stream of the response
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				// write bytes read from the input stream into the output stream
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}

				outStream.flush();
				outStream.close();
				inputStream.close();

				if (fileToDownload.exists()) {

					boolean success = new File(relativeWebPath).delete();
					if (success) {
						System.out.println("The file " + file + " has been successfully deleted");
					}

				}

			} catch (FileNotFoundException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			} catch (IOException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			}

		} // end : motor excel file

		// start : Ezylife excel file
		else if (productType.indexOf("TL") != -1) {
			TransSearchObject transSearchObject_TL = new TransSearchObject();
			List<TLReportExcelVO> tlReportList = new ArrayList<TLReportExcelVO>();
			List<TLReportExcelVO> tlReportList_B4Pay = new ArrayList<TLReportExcelVO>();
			List<String> transactionStatusList = new ArrayList<String>();

			String query1 = "n";
			String query2 = "n";

			transSearchObject_TL.setProductCode(productType);

			// if Product is Chosen
			if ("".equalsIgnoreCase(status) || status == null) {
				transactionStatusList.add("S");
				transactionStatusList.add("AP");
				transactionStatusList.add("F");
				transactionStatusList.add("O");
				transactionStatusList.add("R");
				transSearchObject_TL.setTransactionStatus(transactionStatusList);
				// transSearchObject_Motor.setTransactionStatus("'S','AP','F','O','R'");
				query1 = "y";
				query2 = "y";
			} else {
				transactionStatusList.add(status);
				transSearchObject_TL.setTransactionStatus(transactionStatusList);
				// transSearchObject_Motor.setTransactionStatus(status);
			}

			if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)) {
				query1 = "y";
			}
			if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
				query2 = "y";
			}

			if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
				transSearchObject_TL.setPolicyNo(PolicyCertificateNo);
			}
			if (!"".equalsIgnoreCase(nRIC)) {
				transSearchObject_TL.setNricNo(nRIC);
			}
			if (!"".equalsIgnoreCase(receiptNo)) {
				transSearchObject_TL.setInvoiceNo(receiptNo);
			}

			transSearchObject_TL.setDateFrom(dateFrom);
			transSearchObject_TL.setDateTo(dateTo);

			// if query 1
			if ("y".equalsIgnoreCase(query1)) {
				tlReportList = tlReportExcelMapper.selectTLTransactionalReportAfterPayment(transSearchObject_TL);

			} // End if query1 is Executed

			// if query 2
			if ("y".equalsIgnoreCase(query2)) {
				tlReportList_B4Pay = tlReportExcelMapper.selectTLTransactionalReportBeforePayment(transSearchObject_TL);
			}

			tlReportList.addAll(tlReportList_B4Pay);

			Collections.sort(tlReportList, new Comparator<TLReportExcelVO>() {
				@Override
				public int compare(TLReportExcelVO o1, TLReportExcelVO o2) {
					return o1.getRecord_date().compareTo(o2.getRecord_date());
				}
			});

			Collections.reverse(tlReportList);

			if (tlReportList.size() <= 0) {
				model.addAttribute("emptySearch", "No Record Found");
			}
			model.addAttribute("tlReportList", tlReportList);

			// start excel

			Random ran = new Random();
			int randomNo = ran.nextInt(6) + 5;

			DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
			Date date = new Date();

			String file = "EtiqaTransactionReport" + dateFormat.format(date) + randomNo + ".xlsx";

			String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
			// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

			System.out.println("relativeWebPath =" + relativeWebPath);

			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Etiqa Transaction Report");

			// header fields based on product
			String[] fields = null;

			fields = new String[] { "Id", "DSPQQID", "Quotation Creation Date", "Txn Date", "Transaction Start",
					"Transaction End", "Transaction Time Taken", "Product Name", "Policy/Cert No", "Coverage Amount",
					"Coverage Term", "Status", "Abandonment", "Rejection Reason", "Name", "NRIC No", "Gender", "Email",
					"Address", "Postcode", "State", "Mobile No", "Annual Premium/Contribution", "Payment Type",
					"Payment Method", "Payment Mode", "Amount Paid", "MPAY Ref No", "MPAY Auth Code", "Promo Code",
					"Agent Name", "Agent Code", "Operator Code" };

			Row row1 = sheet.createRow(0);

			for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
				Cell cell = row1.createCell(cellIndex);
				cell.setCellValue(fields[cellIndex]);
			}

			String[] fields2 = null;

			int i = 1;

			for (TLReportExcelVO TLDetail : tlReportList) {

				String Abandonment = "";

				String txnStatus = "";
				String quotationCreationDate = "";
				String transactionStartDateTime = "";
				String transactionEndDateTime = "";
				String proEntity = "";
				String paymentChannel = "";

				String txnTimeTaken = "";
				String transactionDate = "";

				if (null != TLDetail.getPmnt_gateway_code() && "" != TLDetail.getPmnt_gateway_code()) {
					if (TLDetail.getPmnt_gateway_code().indexOf("EBPG") != -1) {

						paymentChannel = "EBPG";
					}

					else if (TLDetail.getPmnt_gateway_code().indexOf("AMEX") != -1) {

						paymentChannel = "AMEX";
					}

					else if (TLDetail.getPmnt_gateway_code().indexOf("FPX") != -1) {

						paymentChannel = "FPX";
					}

					else if (TLDetail.getPmnt_gateway_code().indexOf("Maybank2U") != -1) {

						paymentChannel = "M2U";
					}

					else if (TLDetail.getPmnt_gateway_code().indexOf("CREDIT CARD") != -1) {

						paymentChannel = "MPAY";
					}

				}
				if (TLDetail.getProduct_code().equals("MI")) {

					proEntity = "EIB";
				}

				else if (TLDetail.getProduct_code().equals("MT"))

				{

					proEntity = "ETB";

				}

				if (TLDetail.getPmnt_status().equals("S")) {

					txnStatus = "Successful";

				} else if (TLDetail.getPmnt_status().equals("F")) {

					txnStatus = "Fail";

				}

				else if (TLDetail.getPmnt_status().equals("AP")) {

					txnStatus = "Attempt Payment";

				}

				else if (TLDetail.getPmnt_status().equals("O")) {

					Abandonment = TLDetail.getLast_page();
					txnStatus = "Incomplete";

				}

				else if (TLDetail.getPmnt_status().equals("C")) {

					txnStatus = "Cancelled";

				}

				else if (TLDetail.getPmnt_status().equals("R")) {

					txnStatus = "Rejection : " + TLDetail.getUWReason();

				}

				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				Date quotationCreationDateTmp = null;

				try {

					quotationCreationDateTmp = formatter.parse(TLDetail.getQuotationCreateDateTime());
					quotationCreationDate = ft.format(quotationCreationDateTmp);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				if (TLDetail.getPmnt_status().equals("S") || TLDetail.getPmnt_status().equals("F")
						|| TLDetail.getPmnt_status().equals("AP")) {

					transactionStartDateTime = TLDetail.getTransaction_datetime();

					/*
					 * try {
					 *
					 * transactionStartDateTime=
					 * ft2.format(format1.parse(transactionStartDateTime));
					 *
					 * } catch (ParseException e) { // TODO Auto-generated catch block
					 * e.printStackTrace(); }
					 */

				}

				fields2 = new String[] { Integer.toString(i), TLDetail.getDSPQQID(), quotationCreationDate,
						transactionDate, transactionStartDateTime, transactionEndDateTime, txnTimeTaken,
						TLDetail.getProduct_code(), TLDetail.getPolicy_number(), TLDetail.getCoverage_amount(),
						TLDetail.getCoverage_term() + " Years", txnStatus, Abandonment, TLDetail.getUWReason(),
						TLDetail.getCustomer_name(), TLDetail.getCustomer_nric_id(), TLDetail.getCustomerGender(),
						TLDetail.getCustomerEmail(),
						TLDetail.getCustomerAddress1() + "," + TLDetail.getCustomerAddress2() + ","
								+ TLDetail.getCustomerAddress3(),
						TLDetail.getCustomerPostcode(), TLDetail.getCustomerState(), TLDetail.getCustomerMobileNo(),
						TLDetail.getPremium_amount(), TLDetail.getPmnt_gateway_code(), paymentChannel,
						TLDetail.getPremium_mode(), TLDetail.getAmount(), TLDetail.getTxn_id(), TLDetail.getAuth_code(),
						TLDetail.getDiscountCode(), TLDetail.getAgent_name(), TLDetail.getAgent_code(),
						TLDetail.getOperatorCode() };

				Row row2 = sheet.createRow(i);

				for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
					Cell cell = row2.createCell(cellIndex);
					cell.setCellValue(fields2[cellIndex]);
				}

				i++;

			}

			// System.out.println("txnStatus "+txn.getStatus());

			try {
				FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
				workbook.write(outputStream);
				workbook.close();

				File fileToDownload = new File(relativeWebPath);

				FileInputStream inputStream = new FileInputStream(fileToDownload);
				response.setContentType(servletContext.getMimeType(relativeWebPath));
				response.setContentLength((int) fileToDownload.length());

				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
				response.setHeader(headerKey, headerValue);

				// get output stream of the response
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				// write bytes read from the input stream into the output stream
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}

				outStream.flush();
				outStream.close();
				inputStream.close();

				if (fileToDownload.exists()) {

					boolean success = new File(relativeWebPath).delete();
					if (success) {
						System.out.println("The file " + file + " has been successfully deleted");
					}

				}

			} catch (FileNotFoundException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			} catch (IOException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			}

		} // end : ezylife excel file

		else if (productType.indexOf("WTC") != -1 || productType.indexOf("TPT") != -1) {

			TransSearchObject transSearchObject_WTC = new TransSearchObject();
			List<WTCReportExcelVO> wtcReportList = new ArrayList<WTCReportExcelVO>();
			List<WTCReportExcelVO> wtcReportList_B4Pay = new ArrayList<WTCReportExcelVO>();
			List<String> transactionStatusList = new ArrayList<String>();

			String query1 = "n";
			String query2 = "n";

			transSearchObject_WTC.setProductCode(productType);

			if ("".equalsIgnoreCase(productType) || productType == null) {
				if ("".equalsIgnoreCase(status) || status == null) { // productType empty and transaction status empty
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transSearchObject_WTC.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus("'S','AP','F'");
					query1 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_WTC.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus(status);
				}
			} else { // if Product is Chosen
				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_WTC.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus("'S','AP','F','O','R'");
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_WTC.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus(status);
				}
			}
			if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)) {
				query1 = "y";
			}
			if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
				query2 = "y";
			}

			if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
				transSearchObject_WTC.setPolicyNo(PolicyCertificateNo);
				query2 = null;
			}
			if (!"".equalsIgnoreCase(nRIC)) {
				transSearchObject_WTC.setNricNo(nRIC);
			}
			if (!"".equalsIgnoreCase(receiptNo)) {
				transSearchObject_WTC.setInvoiceNo(receiptNo);
				query2 = null;
			}

			transSearchObject_WTC.setDateFrom(dateFrom);
			transSearchObject_WTC.setDateTo(dateTo);

			// if query 1
			if ("y".equalsIgnoreCase(query1)) {
				wtcReportList = wtcReportExcelMapper.selectWTCTransactionalReportAfterPayment(transSearchObject_WTC);

			} // End if query1 is Executed

			// if query 2
			if ("y".equalsIgnoreCase(query2)) {
				wtcReportList_B4Pay = wtcReportExcelMapper
						.selectWTCTransactionalReportBeforePayment(transSearchObject_WTC);
			}
			// if Query before payment and after payment then combine
			// if ("y".equalsIgnoreCase(query1) && "y".equalsIgnoreCase(query2)) {
			wtcReportList.addAll(wtcReportList_B4Pay);
			// }
			Collections.sort(wtcReportList, new Comparator<WTCReportExcelVO>() {
				@Override
				public int compare(WTCReportExcelVO o1, WTCReportExcelVO o2) {
					return o1.getRecord_date().compareTo(o2.getRecord_date());
				}
			});

			Collections.reverse(wtcReportList);

			if (wtcReportList.size() <= 0) {
				model.addAttribute("emptySearch", "No Record Found");
			}
			model.addAttribute("reportResult", wtcReportList);

			// start excel

			Random ran = new Random();
			int randomNo = ran.nextInt(6) + 5;

			DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
			Date date = new Date();

			String file = "EtiqaTransactionReport" + dateFormat.format(date) + randomNo + ".xlsx";

			String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
			// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Etiqa Transaction Report");

			// header fields based on product
			String[] fields = null;

			fields = new String[] { "Id", "DSPQQID", "Quotation Creation Date", "Txn Id", "Txn Start Date",
					"Txn End Date", "Entity", "Name", "PolicyNo", "ProductDesc", "Plan", "Scheme", "Id Type",
					"Insured ID", "Gender", "Email", "Address 1", "Address 2", "Address 3", "State", "PostCode",
					"Contact No", "Agent Code", "Agent Name", "Operator Code", "Promo Code", "EffDate", "Travel Type",
					"Area Code", "FPX Ref", "EBPG Ref", "M2U Ref", "Payment Mode", "Payment Payable", "Gross Premium",
					"Discount", "GST", "Comm", "Tax Invoice No", "Txn Status", "Abandonment", "PDPA", "Spouse",
					"Spouse Email", "Spouse Id Type", "Spouse Id No", "Spouse DOB", "Spouse Gender", "Child 1",
					"Child 1 Id Type", "Child 1 Id No", "Child 1 DOB", "Child 1 Gender", "Child 2", "Child 2 Id Type",
					"Child 2 Id No", "Child 2 DOB", "Child 2 Gender", "Child 3", "Child 3 Id Type", "Child 3 Id No",
					"Child 3 DOB", "Child 3 Gender", "Child 4", "Child 4 Id Type", "Child 4 Id No", "Child 4 DOB",
					"Child 4 Gender", "Child 5", "Child 5 Id Type", "Child 5 Id No", "Child 5 DOB", "Child 5 Gender",
					"Child 6", "Child 6 Id Type", "Child 6 Id No", "Child 6 DOB", "Child 6 Gender", "Child 7",
					"Child 7 Id Type", "Child 7 Id No", "Child 7 DOB", "Child 7 Gender" };

			Row row1 = sheet.createRow(0);

			for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
				Cell cell = row1.createCell(cellIndex);
				cell.setCellValue(fields[cellIndex]);
			}

			String[] fields2 = null;

			int i = 1;

			for (WTCReportExcelVO WTCDetail : wtcReportList) {

				String Abandonment = "";

				String txnStatus = "";
				String quotationCreationDate = "";
				String transactionStartDateTime = "";
				String transactionEndDateTime = "";
				String proEntity = "";
				String paymentChannel = "";
				String coverageStartDate = "";
				String coverageEndDate = "";

				if (null != WTCDetail.getPmnt_gateway_code() && "" != WTCDetail.getPmnt_gateway_code()) {
					if (WTCDetail.getPmnt_gateway_code().indexOf("EBPG") != -1) {

						paymentChannel = "EBPG";
					}

					else if (WTCDetail.getPmnt_gateway_code().indexOf("AMEX") != -1) {

						paymentChannel = "AMEX";
					}

					else if (WTCDetail.getPmnt_gateway_code().indexOf("FPX") != -1) {

						paymentChannel = "FPX";
					}

					else if (WTCDetail.getPmnt_gateway_code().indexOf("Maybank2U") != -1) {

						paymentChannel = "M2U";
					}

					else if (WTCDetail.getPmnt_gateway_code().indexOf("CREDIT CARD") != -1) {

						paymentChannel = "MPAY";
					}

				}
				if (WTCDetail.getProduct_code().equals("WTC")) {

					proEntity = "EIB";
				}

				else if (WTCDetail.getProduct_code().equals("TPT"))

				{

					proEntity = "ETB";

				}

				if (WTCDetail.getPmnt_status().equals("S")) {

					txnStatus = "Successful";

				} else if (WTCDetail.getPmnt_status().equals("F")) {

					txnStatus = "Fail";

				}

				else if (WTCDetail.getPmnt_status().equals("AP")) {

					txnStatus = "Attempt Payment";

				}

				else if (WTCDetail.getPmnt_status().equals("O")) {

					Abandonment = WTCDetail.getLast_page();
					txnStatus = "Incomplete";

				}

				else if (WTCDetail.getPmnt_status().equals("C")) {

					txnStatus = "Cancelled";

				}

				else if (WTCDetail.getPmnt_status().equals("R")) {

					txnStatus = "Rejection : " + WTCDetail.getReason();

				}

				SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

				if (null != WTCDetail.getRecord_date()) {
					quotationCreationDate = ft.format(WTCDetail.getRecord_date());
				}

				if (WTCDetail.getPmnt_status().equals("S") || WTCDetail.getPmnt_status().equals("F")
						|| WTCDetail.getPmnt_status().equals("AP")) {

					transactionStartDateTime = WTCDetail.getTransaction_datetime();

					/*
					 * try {
					 *
					 * transactionStartDateTime=
					 * ft2.format(format1.parse(transactionStartDateTime));
					 *
					 * } catch (ParseException e) { // TODO Auto-generated catch block
					 * e.printStackTrace(); }
					 */

				}

				if (null != WTCDetail.getTravel_start_date()) {

					String valueFromDB = WTCDetail.getTravel_start_date();
					Date d1 = null;
					try {
						d1 = sdf1.parse(valueFromDB);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
					String startDateWithoutTime = sdf.format(d1);
					// System.out.println("sdf.format(d1) " + startDateWithoutTime);

					coverageStartDate = startDateWithoutTime;

				}

				if (null != WTCDetail.getTravel_end_date()) {

					String valueFromDB = WTCDetail.getTravel_end_date();
					Date d1 = null;
					try {
						d1 = sdf1.parse(valueFromDB);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
					String endDateWithoutTime = sdf.format(d1);
					// System.out.println("sdf.format(d1) " + startDateWithoutTime);

					coverageEndDate = endDateWithoutTime;

				}

				if (null != WTCDetail.getChild_name()) {

					if (WTCDetail.getChild_name().indexOf("|") != -1) {

						String[] childName = WTCDetail.getChild_name().split("|");

						int childCount = childName.length;

						for (int j = 0; j < childCount; j++) {

							if (j == 0) {
								WTCDetail.setChild_name_1(childName[j]);
							}
							if (j == 1) {
								WTCDetail.setChild_name_2(childName[j]);
							}
							if (j == 2) {
								WTCDetail.setChild_name_3(childName[j]);
							}
							if (j == 3) {
								WTCDetail.setChild_name_4(childName[j]);
							}
							if (j == 4) {
								WTCDetail.setChild_name_5(childName[j]);
							}

							if (j == 5) {
								WTCDetail.setChild_name_6(childName[j]);
							}

							if (j == 6) {
								WTCDetail.setChild_name_7(childName[j]);
							}

						}

					}

					else {
						WTCDetail.setChild_name_1(WTCDetail.getChild_name());

					}

				}

				if (null != WTCDetail.getChild_ID_type()) {

					if (WTCDetail.getChild_ID_type().indexOf("|") != -1) {

						String[] childIDType = WTCDetail.getChild_ID_type().split("|");

						int childCount = childIDType.length;

						for (int j = 0; j < childCount; j++) {

							if (j == 0) {
								WTCDetail.setChild_ID_type_1(childIDType[j]);
							}
							if (j == 1) {
								WTCDetail.setChild_ID_type_2(childIDType[j]);
							}
							if (j == 2) {
								WTCDetail.setChild_ID_type_3(childIDType[j]);
							}
							if (j == 3) {
								WTCDetail.setChild_ID_type_4(childIDType[j]);
							}
							if (j == 4) {
								WTCDetail.setChild_ID_type_5(childIDType[j]);
							}

							if (j == 5) {
								WTCDetail.setChild_ID_type_6(childIDType[j]);
							}

							if (j == 6) {
								WTCDetail.setChild_ID_type_7(childIDType[j]);
							}

						}

					}

					else {
						WTCDetail.setChild_ID_type_1(WTCDetail.getChild_ID_type());

					}

				}

				if (null != WTCDetail.getChild_ID_number()) {

					if (WTCDetail.getChild_ID_number().indexOf("|") != -1) {

						String[] childIDNumber = WTCDetail.getChild_ID_number().split("|");

						int childCount = childIDNumber.length;

						for (int j = 0; j < childCount; j++) {

							if (j == 0) {
								WTCDetail.setChild_ID_number_1(childIDNumber[j]);
							}
							if (j == 1) {
								WTCDetail.setChild_ID_number_2(childIDNumber[j]);
							}
							if (j == 2) {
								WTCDetail.setChild_ID_number_3(childIDNumber[j]);
							}
							if (j == 3) {
								WTCDetail.setChild_ID_number_4(childIDNumber[j]);
							}
							if (j == 4) {
								WTCDetail.setChild_ID_number_5(childIDNumber[j]);
							}

							if (j == 5) {
								WTCDetail.setChild_ID_number_6(childIDNumber[j]);
							}

							if (j == 6) {
								WTCDetail.setChild_ID_number_7(childIDNumber[j]);
							}

						}

					}

					else {
						WTCDetail.setChild_ID_number_1(WTCDetail.getChild_ID_number());

					}

				}

				if (null != WTCDetail.getChild_DOB()) {

					if (WTCDetail.getChild_DOB().indexOf("|") != -1) {

						String[] childDOB = WTCDetail.getChild_DOB().split("|");

						int childCount = childDOB.length;

						for (int j = 0; j < childCount; j++) {

							if (j == 0) {
								WTCDetail.setChild_DOB_1(childDOB[j]);
							}
							if (j == 1) {
								WTCDetail.setChild_DOB_2(childDOB[j]);
							}
							if (j == 2) {
								WTCDetail.setChild_DOB_3(childDOB[j]);
							}
							if (j == 3) {
								WTCDetail.setChild_DOB_4(childDOB[j]);
							}
							if (j == 4) {
								WTCDetail.setChild_DOB_5(childDOB[j]);
							}

							if (j == 5) {
								WTCDetail.setChild_DOB_6(childDOB[j]);
							}

							if (j == 6) {
								WTCDetail.setChild_DOB_7(childDOB[j]);
							}

						}

					}

					else {
						WTCDetail.setChild_DOB_1(WTCDetail.getChild_ID_number());

					}

				}

				if (null != WTCDetail.getChild_gender()) {

					if (WTCDetail.getChild_gender().indexOf("|") != -1) {

						String[] childGender = WTCDetail.getChild_gender().split("|");

						int childCount = childGender.length;

						for (int j = 0; j < childCount; j++) {

							if (j == 0) {
								WTCDetail.setChild_gender_1(childGender[j]);
							}
							if (j == 1) {
								WTCDetail.setChild_gender_2(childGender[j]);
							}
							if (j == 2) {
								WTCDetail.setChild_gender_3(childGender[j]);
							}
							if (j == 3) {
								WTCDetail.setChild_gender_4(childGender[j]);
							}
							if (j == 4) {
								WTCDetail.setChild_gender_5(childGender[j]);
							}

							if (j == 5) {
								WTCDetail.setChild_gender_6(childGender[j]);
							}

							if (j == 6) {
								WTCDetail.setChild_gender_7(childGender[j]);
							}

						}

					}

					else {
						WTCDetail.setChild_gender_1(WTCDetail.getChild_gender());

					}

				}

				fields2 = new String[] { Integer.toString(i), WTCDetail.getDSPQQID(), quotationCreationDate,
						WTCDetail.getPaymentTrxID(), transactionStartDateTime, transactionEndDateTime, proEntity,
						WTCDetail.getCustomer_name(), WTCDetail.getPolicy_number(), WTCDetail.getProduct_code(),
						WTCDetail.getOffered_plan_name(), WTCDetail.getTravlling_with(),
						WTCDetail.getCustomer_ID_type(), WTCDetail.getCustomer_nric_id(),
						WTCDetail.getCustomer_gender(), WTCDetail.getCustomer_email(),
						WTCDetail.getCustomer_address_1(), WTCDetail.getCustomer_address_2(),
						WTCDetail.getCustomer_address_3(), WTCDetail.getCustomer_state(),
						WTCDetail.getCustomer_postcode(), WTCDetail.getCustomer_mobile_no(), WTCDetail.getAgent_code(),
						WTCDetail.getAgent_name(), WTCDetail.getOperator_code(), WTCDetail.getDiscount_code(),
						coverageStartDate + "-" + coverageEndDate, WTCDetail.getTravel_area_type(),
						WTCDetail.getArea_code(), WTCDetail.getFpx_fpxtxnid(), WTCDetail.getTransaction_id(),
						WTCDetail.getBankrefno(), paymentChannel, WTCDetail.getTotal_premium_payable(),
						WTCDetail.getGrosspremium_final(), WTCDetail.getDiscount(), WTCDetail.getGst(),
						WTCDetail.getCommission(), WTCDetail.getInvoice_no(), txnStatus, Abandonment,
						WTCDetail.getPDPA(), WTCDetail.getSpouse_name(), WTCDetail.getSpouse_email(),
						WTCDetail.getSpouse_ID_type(), WTCDetail.getSpouse_ID_number(), WTCDetail.getSpouse_DOB(),
						WTCDetail.getSpouse_gender(), WTCDetail.getChild_name_1(), WTCDetail.getChild_ID_type_1(),
						WTCDetail.getChild_ID_number_1(), WTCDetail.getChild_DOB_1(), WTCDetail.getChild_gender_1(),
						WTCDetail.getChild_name_2(), WTCDetail.getChild_ID_type_2(), WTCDetail.getChild_ID_number_2(),
						WTCDetail.getChild_DOB_2(), WTCDetail.getChild_gender_2(), WTCDetail.getChild_name_3(),
						WTCDetail.getChild_ID_type_3(), WTCDetail.getChild_ID_number_3(), WTCDetail.getChild_DOB_3(),
						WTCDetail.getChild_gender_3(), WTCDetail.getChild_name_4(), WTCDetail.getChild_ID_type_4(),
						WTCDetail.getChild_ID_number_4(), WTCDetail.getChild_DOB_4(), WTCDetail.getChild_gender_4(),
						WTCDetail.getChild_name_5(), WTCDetail.getChild_ID_type_5(), WTCDetail.getChild_ID_number_5(),
						WTCDetail.getChild_DOB_5(), WTCDetail.getChild_gender_5(), WTCDetail.getChild_name_6(),
						WTCDetail.getChild_ID_type_6(), WTCDetail.getChild_ID_number_6(), WTCDetail.getChild_DOB_6(),
						WTCDetail.getChild_gender_6(), WTCDetail.getChild_name_7(), WTCDetail.getChild_ID_type_7(),
						WTCDetail.getChild_ID_number_7(), WTCDetail.getChild_DOB_7(), WTCDetail.getChild_gender_7() };

				Row row2 = sheet.createRow(i);

				for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
					Cell cell = row2.createCell(cellIndex);
					cell.setCellValue(fields2[cellIndex]);
				}

				i++;

			}

			// System.out.println("txnStatus "+txn.getStatus());

			try {
				FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
				workbook.write(outputStream);
				workbook.close();

				File fileToDownload = new File(relativeWebPath);

				FileInputStream inputStream = new FileInputStream(fileToDownload);
				response.setContentType(servletContext.getMimeType(relativeWebPath));
				response.setContentLength((int) fileToDownload.length());

				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
				response.setHeader(headerKey, headerValue);

				// get output stream of the response
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				// write bytes read from the input stream into the output stream
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}

				outStream.flush();
				outStream.close();
				inputStream.close();

				if (fileToDownload.exists()) {

					boolean success = new File(relativeWebPath).delete();
					if (success) {
						System.out.println("The file " + file + " has been successfully deleted");
					}

				}

			} catch (FileNotFoundException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			} catch (IOException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			}

		}

		// end excel

		else if (productType.indexOf("HOHH") != -1) {

			TransSearchObject transSearchObject_HOHH = new TransSearchObject();
			List<HOHHReportExcelVO> hohhReportList = new ArrayList<HOHHReportExcelVO>();
			List<HOHHReportExcelVO> hohhReportList_B4Pay = new ArrayList<HOHHReportExcelVO>();
			List<String> transactionStatusList = new ArrayList<String>();

			String query1 = "n";
			String query2 = "n";

			transSearchObject_HOHH.setProductCode(productType);

			if ("".equalsIgnoreCase(productType) || productType == null) {
				if ("".equalsIgnoreCase(status) || status == null) { // productType empty and transaction status empty
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transSearchObject_HOHH.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus("'S','AP','F'");
					query1 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_HOHH.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus(status);
				}
			} else { // if Product is Chosen
				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_HOHH.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus("'S','AP','F','O','R'");
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_HOHH.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus(status);
				}
			}
			if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)) {
				query1 = "y";
			}
			if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
				query2 = "y";
			}

			if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
				transSearchObject_HOHH.setPolicyNo(PolicyCertificateNo);
				query2 = null;
			}
			if (!"".equalsIgnoreCase(nRIC)) {
				transSearchObject_HOHH.setNricNo(nRIC);
			}
			if (!"".equalsIgnoreCase(receiptNo)) {
				transSearchObject_HOHH.setInvoiceNo(receiptNo);
				query2 = null;
			}

			transSearchObject_HOHH.setDateFrom(dateFrom);
			transSearchObject_HOHH.setDateTo(dateTo);

			// if query 1
			if ("y".equalsIgnoreCase(query1)) {
				hohhReportList = hohhReportExcelMapper
						.selectHOHHTransactionalReportAfterPayment(transSearchObject_HOHH);

			} // End if query1 is Executed

			// if query 2
			if ("y".equalsIgnoreCase(query2)) {
				hohhReportList_B4Pay = hohhReportExcelMapper
						.selectHOHHTransactionalReportBeforePayment(transSearchObject_HOHH);
			}

			hohhReportList.addAll(hohhReportList_B4Pay);

			Collections.sort(hohhReportList, new Comparator<HOHHReportExcelVO>() {
				@Override
				public int compare(HOHHReportExcelVO o1, HOHHReportExcelVO o2) {
					return o1.getRecord_date().compareTo(o2.getRecord_date());
				}
			});

			Collections.reverse(hohhReportList);

			if (hohhReportList.size() <= 0) {
				model.addAttribute("emptySearch", "No Record Found");
			}
			model.addAttribute("reportResult", hohhReportList);

			// start excel

			Random ran = new Random();
			int randomNo = ran.nextInt(6) + 5;

			DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
			Date date = new Date();

			String file = "EtiqaTransactionReport" + dateFormat.format(date) + randomNo + ".xlsx";

			String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
			// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Etiqa Transaction Report");

			// header fields based on product
			String[] fields = null;

			fields = new String[] { "Id", "DSPQQID", "Quotation Creation Date", "Txn Id", "Txn Start Date",
					"Txn End Date", "Entity", "Name", "PolicyNo", "ProductDesc", "Building Type", "Cons Type",
					"Id Type", "Insured ID", "Email", "Address 1", "Address 2", "Address 3", "PostCode", "State",
					"RiskAddress1", "RiskAddress2", "RiskPostcode", "RiskState", "Contact No", "Agent Code",
					"Agent Name", "Operator Code", "Promo Code", "EffDate", "HO SI", "HH SI", "HOHH SI",
					"Extended Theft", "RSMD", "FPX Ref", "EBPG Ref", "M2U Ref", "Payment Mode", "Payment Payable",
					"Gross Premium", "Discount", "GST", "Comm", "Tax Invoice No", "Txn Status", "Abandonment", "PDPA",
					"UW1", "UW2", "UW3", "UW4", "UW5", "Add Item 1", "Add Item 2", "Add Item 3", "Add Item 4",
					"Add Item 5", "Add Item 6", "Add Item 7", "Add Item 8", "Add Item 9", "Add Item 10" };

			Row row1 = sheet.createRow(0);

			for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
				Cell cell = row1.createCell(cellIndex);
				cell.setCellValue(fields[cellIndex]);
			}

			String[] fields2 = null;

			int i = 1;

			for (HOHHReportExcelVO HOHHDetail : hohhReportList) {

				String Abandonment = "";

				String txnStatus = "";
				String quotationCreationDate = "";
				String transactionStartDateTime = "";
				String transactionEndDateTime = "";
				String proEntity = "";
				String paymentChannel = "";
				String coverageStartDate = "";
				String coverageEndDate = "";

				if (null != HOHHDetail.getPmnt_gateway_code() && "" != HOHHDetail.getPmnt_gateway_code()) {
					if (HOHHDetail.getPmnt_gateway_code().indexOf("EBPG") != -1) {

						paymentChannel = "EBPG";
					}

					else if (HOHHDetail.getPmnt_gateway_code().indexOf("AMEX") != -1) {

						paymentChannel = "AMEX";
					}

					else if (HOHHDetail.getPmnt_gateway_code().indexOf("FPX") != -1) {

						paymentChannel = "FPX";
					}

					else if (HOHHDetail.getPmnt_gateway_code().indexOf("Maybank2U") != -1) {

						paymentChannel = "M2U";
					}

					else if (HOHHDetail.getPmnt_gateway_code().indexOf("CREDIT CARD") != -1) {

						paymentChannel = "MPAY";
					}

				}
				if (HOHHDetail.getProduct_code().equals("HOHH")) {

					proEntity = "EIB";
				}

				else if (HOHHDetail.getProduct_code().equals("HOHH-ETB"))

				{

					proEntity = "ETB";

				}

				if (HOHHDetail.getPmnt_status().equals("S")) {

					txnStatus = "Successful";

				} else if (HOHHDetail.getPmnt_status().equals("F")) {

					txnStatus = "Fail";

				}

				else if (HOHHDetail.getPmnt_status().equals("AP")) {

					txnStatus = "Attempt Payment";

				}

				else if (HOHHDetail.getPmnt_status().equals("O")) {

					Abandonment = HOHHDetail.getLast_page();
					txnStatus = "Incomplete";

				}

				else if (HOHHDetail.getPmnt_status().equals("C")) {

					txnStatus = "Cancelled";

				}

				else if (HOHHDetail.getPmnt_status().equals("R")) {

					txnStatus = "Rejection : " + HOHHDetail.getReason();

				}

				if (HOHHDetail.getPmnt_status().equals("S") || HOHHDetail.getPmnt_status().equals("F")
						|| HOHHDetail.getPmnt_status().equals("AP")) {

					transactionStartDateTime = HOHHDetail.getTransaction_datetime();

				}

				fields2 = new String[] { Integer.toString(i), HOHHDetail.getDSPQQID(),
						HOHHDetail.getQuotation_datetime(), HOHHDetail.getPaymentTrxID(), transactionStartDateTime,
						transactionEndDateTime, proEntity, HOHHDetail.getCustomer_name(), HOHHDetail.getPolicy_number(),
						HOHHDetail.getProduct_code(), HOHHDetail.getHome_type(),
						HOHHDetail.getBuild_construction_type(), HOHHDetail.getCustomer_ID_type(),
						HOHHDetail.getCustomer_nric_id(), HOHHDetail.getCustomer_email(),
						HOHHDetail.getCustomer_address_1(), HOHHDetail.getCustomer_address_2(),
						HOHHDetail.getCustomer_address_3(), HOHHDetail.getCustomer_postcode(),
						HOHHDetail.getCustomer_state(), HOHHDetail.getProperty_insured_address_1(),
						HOHHDetail.getProperty_insured_address_2(), HOHHDetail.getProperty_insured_postcode(),
						HOHHDetail.getProperty_insured_state(), HOHHDetail.getCustomer_mobile_no(),
						HOHHDetail.getAgent_code(), HOHHDetail.getAgent_name(), HOHHDetail.getOperator_code(),
						HOHHDetail.getDiscount_code(),
						HOHHDetail.getCoverage_start_date() + "-" + HOHHDetail.getCoverage_end_date(),
						HOHHDetail.getHOSI(), HOHHDetail.getHHSI(), HOHHDetail.getHOHHSI(),
						HOHHDetail.getAdd_ben_extended_theft_amt(), HOHHDetail.getAdd_ben_riot_strike_amt(),
						HOHHDetail.getFpx_fpxtxnid(), HOHHDetail.getTransaction_id(), HOHHDetail.getBankrefno(),
						paymentChannel, HOHHDetail.getAmount(), HOHHDetail.getGrosspremium_final(),
						HOHHDetail.getDiscount(), HOHHDetail.getGst(), HOHHDetail.getCommission(),
						HOHHDetail.getInvoice_no(), txnStatus, Abandonment, HOHHDetail.getPDPA(), HOHHDetail.getUW1(),
						HOHHDetail.getUW2(), HOHHDetail.getUW3(), HOHHDetail.getUW4(), HOHHDetail.getUW5(),
						HOHHDetail.getBenefit_item_1(), HOHHDetail.getBenefit_item_2(), HOHHDetail.getBenefit_item_3(),
						HOHHDetail.getBenefit_item_4(), HOHHDetail.getBenefit_item_5(), HOHHDetail.getBenefit_item_6(),
						HOHHDetail.getBenefit_item_7(), HOHHDetail.getBenefit_item_8(), HOHHDetail.getBenefit_item_9(),
						HOHHDetail.getBenefit_item_10() };

				Row row2 = sheet.createRow(i);

				for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
					Cell cell = row2.createCell(cellIndex);
					cell.setCellValue(fields2[cellIndex]);
				}

				i++;

			}

			// System.out.println("txnStatus "+txn.getStatus());

			try {
				FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
				workbook.write(outputStream);
				workbook.close();

				File fileToDownload = new File(relativeWebPath);

				FileInputStream inputStream = new FileInputStream(fileToDownload);
				response.setContentType(servletContext.getMimeType(relativeWebPath));
				response.setContentLength((int) fileToDownload.length());

				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
				response.setHeader(headerKey, headerValue);

				// get output stream of the response
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				// write bytes read from the input stream into the output stream
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}

				outStream.flush();
				outStream.close();
				inputStream.close();

				if (fileToDownload.exists()) {

					boolean success = new File(relativeWebPath).delete();
					if (success) {
						System.out.println("The file " + file + " has been successfully deleted");
					}

				}

			} catch (FileNotFoundException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			} catch (IOException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			}

		}

		// END: HOHH TRANSACTION REPORT

		// START: ALL TRANSACTION REPORT
		else if ("".equalsIgnoreCase(productType) || productType == null) {
			TransSearchObject transSearchObject_Motor = new TransSearchObject();
			TransSearchObject transSearchObject_TL = new TransSearchObject();
			TransSearchObject transSearchObject_WTC = new TransSearchObject();
			TransSearchObject transSearchObject_HOHH = new TransSearchObject();

			List<ALLReportVO> allReportList = new ArrayList<ALLReportVO>();

			List<ALLReportVO> motorReportList = new ArrayList<ALLReportVO>();
			List<ALLReportVO> tlReportList = new ArrayList<ALLReportVO>();
			List<ALLReportVO> wtcReportList = new ArrayList<ALLReportVO>();
			List<ALLReportVO> hohhReportList = new ArrayList<ALLReportVO>();
			List<String> transactionStatusList = new ArrayList<String>();

			if ("EIB".equalsIgnoreCase(productEntity)) {

				transSearchObject_Motor.setProductCode("MI");
				transSearchObject_TL.setProductCode("TL");
				transSearchObject_WTC.setProductCode("WTC");
				transSearchObject_HOHH.setProductCode("HOHH");

			} else if ("ETB".equalsIgnoreCase(productEntity)) {

				transSearchObject_Motor.setProductCode("MT");
				transSearchObject_WTC.setProductCode("TPT");
				transSearchObject_HOHH.setProductCode("HOHH-ETB");
			}

			if ("".equalsIgnoreCase(status) || status == null) {
				transactionStatusList.add("S");
				transactionStatusList.add("AP");
				transactionStatusList.add("F");
				transactionStatusList.add("C");
			} else {
				transactionStatusList.add(status);

			}

			transSearchObject_Motor.setTransactionStatus(transactionStatusList);
			transSearchObject_TL.setTransactionStatus(transactionStatusList);
			transSearchObject_WTC.setTransactionStatus(transactionStatusList);
			transSearchObject_HOHH.setTransactionStatus(transactionStatusList);

			if (!"".equalsIgnoreCase(PolicyCertificateNo)) {

				transSearchObject_Motor.setPolicyNo(PolicyCertificateNo);
				transSearchObject_TL.setPolicyNo(PolicyCertificateNo);
				transSearchObject_WTC.setPolicyNo(PolicyCertificateNo);
				transSearchObject_HOHH.setPolicyNo(PolicyCertificateNo);
			}
			if (!"".equalsIgnoreCase(nRIC)) {
				transSearchObject_Motor.setNricNo(nRIC);
				transSearchObject_TL.setNricNo(nRIC);
				transSearchObject_WTC.setNricNo(nRIC);
				transSearchObject_HOHH.setNricNo(nRIC);

			}
			if (!"".equalsIgnoreCase(receiptNo)) {
				transSearchObject_Motor.setInvoiceNo(receiptNo);
				transSearchObject_TL.setInvoiceNo(receiptNo);
				transSearchObject_WTC.setInvoiceNo(receiptNo);
				transSearchObject_HOHH.setInvoiceNo(receiptNo);
			}

			transSearchObject_Motor.setDateFrom(dateFrom);
			transSearchObject_TL.setDateFrom(dateFrom);
			transSearchObject_WTC.setDateFrom(dateFrom);
			transSearchObject_HOHH.setDateFrom(dateFrom);

			transSearchObject_Motor.setDateTo(dateTo);
			transSearchObject_TL.setDateTo(dateTo);
			transSearchObject_WTC.setDateTo(dateTo);
			transSearchObject_HOHH.setDateTo(dateTo);

			motorReportList = allReportMapper.selectMotorTransactionalReportAfterPayment(transSearchObject_Motor);
			if ("EIB".equalsIgnoreCase(productEntity)) {
				tlReportList = allReportMapper.selectTLTransactionalReportAfterPayment(transSearchObject_TL);
			}
			wtcReportList = allReportMapper.selectWTCTransactionalReportAfterPayment(transSearchObject_WTC);
			hohhReportList = allReportMapper.selectHOHHTransactionalReportAfterPayment(transSearchObject_HOHH);

			allReportList.addAll(motorReportList);
			if ("EIB".equalsIgnoreCase(productEntity)) {
				allReportList.addAll(tlReportList);
			}
			allReportList.addAll(wtcReportList);
			allReportList.addAll(hohhReportList);

			Collections.sort(allReportList, new Comparator<ALLReportVO>() {
				@Override
				public int compare(ALLReportVO o1, ALLReportVO o2) {
					return o1.getRecord_date().compareTo(o2.getRecord_date());
				}
			});

			Collections.reverse(allReportList);

			if (allReportList.size() <= 0) {
				model.addAttribute("emptySearch", "No Record Found");
			}
			model.addAttribute("reportResult", allReportList);

			// start excel

			Random ran = new Random();
			int randomNo = ran.nextInt(6) + 5;

			DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
			Date date = new Date();

			String file = "EtiqaTransactionReport" + dateFormat.format(date) + randomNo + ".xlsx";

			String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
			// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Etiqa Transaction Report");

			// header fields based on product
			String[] fields = null;

			fields = new String[] { "Id", "DSPQQID", "Entity", "Product", "Quotation Creation Date", "Txn Date",
					"Policy No", "Insured name", "Insured Id", "Poi", "Payment mode", "Status", "Txn Amount",
					"Gross Premium", "Discount", "GST", "Tax Invoice No", "CAPS PolicyNo", "CAPS Amount" };

			Row row1 = sheet.createRow(0);

			for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
				Cell cell = row1.createCell(cellIndex);
				cell.setCellValue(fields[cellIndex]);
			}

			String[] fields2 = null;

			int i = 1;

			for (ALLReportVO ALLDetail : allReportList) {

				String txnStatus = "";
				String quotationCreationDate = "";

				String paymentChannel = "";

				if (null != ALLDetail.getPmnt_gateway_code() && "" != ALLDetail.getPmnt_gateway_code()) {
					if (ALLDetail.getPmnt_gateway_code().indexOf("EBPG") != -1) {

						paymentChannel = "EBPG";
					}

					else if (ALLDetail.getPmnt_gateway_code().indexOf("AMEX") != -1) {

						paymentChannel = "AMEX";
					}

					else if (ALLDetail.getPmnt_gateway_code().indexOf("FPX") != -1) {

						paymentChannel = "FPX";
					}

					else if (ALLDetail.getPmnt_gateway_code().indexOf("Maybank2U") != -1) {

						paymentChannel = "M2U";
					}

					else if (ALLDetail.getPmnt_gateway_code().indexOf("CREDIT CARD") != -1) {

						paymentChannel = "MPAY";
					}

				}

				if (ALLDetail.getPmnt_status().equals("S")) {

					txnStatus = "Successful";

				} else if (ALLDetail.getPmnt_status().equals("F")) {

					txnStatus = "Fail";

				}

				else if (ALLDetail.getPmnt_status().equals("AP")) {

					txnStatus = "Attempt Payment";

				}

				else if (ALLDetail.getPmnt_status().equals("C")) {

					txnStatus = "Cancelled";

				}

				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				Date quotationCreationDateTmp = null;

				try {

					quotationCreationDateTmp = formatter.parse(ALLDetail.getQuotation_datetime());
					quotationCreationDate = ft.format(quotationCreationDateTmp);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				fields2 = new String[] { Integer.toString(i), ALLDetail.getDSPQQID(), productEntity,
						ALLDetail.getProduct_code(), quotationCreationDate, ALLDetail.getTransaction_datetime(),
						ALLDetail.getPolicy_number(), ALLDetail.getCustomer_name(), ALLDetail.getCustomer_nric_id(),
						ALLDetail.getPoi(), paymentChannel, txnStatus, ALLDetail.getAmount(),
						ALLDetail.getGrosspremium_final(), ALLDetail.getDiscount(), ALLDetail.getGst(),
						ALLDetail.getInvoice_no(), ALLDetail.getCaps_dppa(),
						ALLDetail.getPassenger_pa_premium_payable() };

				Row row2 = sheet.createRow(i);

				for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
					Cell cell = row2.createCell(cellIndex);
					cell.setCellValue(fields2[cellIndex]);
				}

				i++;

			}

			// System.out.println("txnStatus "+txn.getStatus());

			try {
				FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
				workbook.write(outputStream);
				workbook.close();

				File fileToDownload = new File(relativeWebPath);

				FileInputStream inputStream = new FileInputStream(fileToDownload);
				response.setContentType(servletContext.getMimeType(relativeWebPath));
				response.setContentLength((int) fileToDownload.length());

				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
				response.setHeader(headerKey, headerValue);

				// get output stream of the response
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				// write bytes read from the input stream into the output stream
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}

				outStream.flush();
				outStream.close();
				inputStream.close();

				if (fileToDownload.exists()) {

					boolean success = new File(relativeWebPath).delete();
					if (success) {
						System.out.println("The file " + file + " has been successfully deleted");
					}

				}

			} catch (FileNotFoundException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			} catch (IOException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			}

		}

		// END: ALL TRANSACTION REPORT

		return "admin-report-transaction1";

	}

}
