package com.spring.admin;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etiqa.utils.FormatDates;
import com.etiqa.utils.convertBytes;
import com.spring.VO.Approval;
import com.spring.VO.ApprovalExample;
import com.spring.VO.ApprovalLog;
import com.spring.VO.DomesticPremium;
import com.spring.VO.DomesticPremiumExample;
import com.spring.VO.InterPremium;
import com.spring.VO.InterPremiumExample;
import com.spring.VO.MITblPmntParam;
import com.spring.VO.MITblPmntParamExample;
import com.spring.VO.TC360Param;
import com.spring.VO.TC360ParamExample;
import com.spring.VO.TCProdMngPojo;
import com.spring.VO.TCSysSetup;
import com.spring.VO.TCSysSetupExample;
import com.spring.VO.TblPdfInfo;
import com.spring.VO.TblPdfInfoExample;
import com.spring.mapper.ApprovalLogMapper;
import com.spring.mapper.ApprovalMapper;
import com.spring.mapper.DomesticPremiumMapper;
import com.spring.mapper.InterPremiumMapper;
import com.spring.mapper.MITblPmntParamMapper;
import com.spring.mapper.TC360ParamMapper;
import com.spring.mapper.TCSysSetupMapper;
import com.spring.mapper.TblPdfInfoMapper;

@Controller
public class TripCareController {
	MITblPmntParamMapper miTblPmntParamMapper;
	TCSysSetupMapper tcSysSetupMapper;
	DomesticPremiumMapper domesticPremiumMapper;
	InterPremiumMapper interPremiumMapper;
	TC360ParamMapper tripCareParamMapper;
	TblPdfInfoMapper tblPdfInfoMapper;
	ApprovalMapper approvalMapper;
	ApprovalLogMapper approvalLogMapper;

	@Autowired
	public TripCareController(MITblPmntParamMapper miTblPmntParamMapper, TCSysSetupMapper tcSysSetupMapper,
			DomesticPremiumMapper domesticPremiumMapper, InterPremiumMapper interPremiumMapper,
			TC360ParamMapper tripCareParamMapper, TblPdfInfoMapper tblPdfInfoMapper, ApprovalMapper approvalMapper,
			ApprovalLogMapper approvalLogMapper) {
		this.miTblPmntParamMapper = miTblPmntParamMapper;
		this.tcSysSetupMapper = tcSysSetupMapper;
		this.domesticPremiumMapper = domesticPremiumMapper;
		this.interPremiumMapper = interPremiumMapper;
		this.tripCareParamMapper = tripCareParamMapper;
		this.tblPdfInfoMapper = tblPdfInfoMapper;
		this.approvalMapper = approvalMapper;
		this.approvalLogMapper = approvalLogMapper;
	}

	/*-------------------------------------------------------Start- Payment Method----------------------------------------------------------*/

	@RequestMapping("/TripCarePaymentMethod")
	public String tripCarePaymentMethod(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("TripCarePaymentMethod function ");

		// Maybank2U
		List<MITblPmntParam> m2ulist;
		MITblPmntParamExample miTblPmntParamExample = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria miPmntParamCriteria = miTblPmntParamExample.createCriteria();
		miPmntParamCriteria.andPmntGatewayCodeEqualTo("M2U");
		miPmntParamCriteria.andProductCodeEqualTo("TCI");
		miPmntParamCriteria.andStatusEqualTo(Short.valueOf("1"));
		m2ulist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample);

		// FPX
		List<MITblPmntParam> fpxlist;
		MITblPmntParamExample miTblPmntParamExample1 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria miPmntParamCriteria1 = miTblPmntParamExample1.createCriteria();
		miPmntParamCriteria1.andPmntGatewayCodeEqualTo("FPX");
		miPmntParamCriteria1.andProductCodeEqualTo("TCI");
		miPmntParamCriteria1.andStatusEqualTo(Short.valueOf("1"));
		fpxlist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample1);

		// EBPG (Visa/Mastercard)
		List<MITblPmntParam> ebpglist;
		MITblPmntParamExample miTblPmntParamExample2 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria miPmntParamCriteria2 = miTblPmntParamExample2.createCriteria();
		miPmntParamCriteria2.andPmntGatewayCodeEqualTo("EBPG");
		miPmntParamCriteria2.andProductCodeEqualTo("TCI");
		miPmntParamCriteria2.andStatusEqualTo(Short.valueOf("1"));
		ebpglist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample2);

		// AMEX
		List<MITblPmntParam> amexlist;
		MITblPmntParamExample miTblPmntParamExample3 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria miPmntParamCriteria3 = miTblPmntParamExample3.createCriteria();
		miPmntParamCriteria3.andPmntGatewayCodeEqualTo("AMEX");
		miPmntParamCriteria3.andProductCodeEqualTo("TCI");
		miPmntParamCriteria3.andStatusEqualTo(Short.valueOf("1"));
		amexlist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample3);

		model.addAttribute("m2ulist", m2ulist);
		model.addAttribute("fpxlist", fpxlist);
		model.addAttribute("ebpglist", ebpglist);
		model.addAttribute("amexlist", amexlist);

		return "/products/tripCare/tripcare-pmnt-method";
	}

	@RequestMapping("/UpdateTripCarePaymentMethod")
	public String updateTripCarePaymentMethod(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("UPDATE TripCarePaymentMethod function ");

		// Maybank2U
		List<MITblPmntParam> m2ulist;
		MITblPmntParamExample miTblPmntParamExample = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria miPmntParamCriteria = miTblPmntParamExample.createCriteria();
		miPmntParamCriteria.andPmntGatewayCodeEqualTo("M2U");
		miPmntParamCriteria.andProductCodeEqualTo("TCI");
		miPmntParamCriteria.andStatusEqualTo(Short.valueOf("1"));
		m2ulist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample);

		// FPX
		List<MITblPmntParam> fpxlist;
		MITblPmntParamExample miTblPmntParamExample1 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria miPmntParamCriteria1 = miTblPmntParamExample1.createCriteria();
		miPmntParamCriteria1.andPmntGatewayCodeEqualTo("FPX");
		miPmntParamCriteria1.andProductCodeEqualTo("TCI");
		miPmntParamCriteria1.andStatusEqualTo(Short.valueOf("1"));
		fpxlist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample1);

		// EBPG (Visa/ Mastercard)
		List<MITblPmntParam> ebpglist;
		MITblPmntParamExample miTblPmntParamExample2 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria miPmntParamCriteria2 = miTblPmntParamExample2.createCriteria();
		miPmntParamCriteria2.andPmntGatewayCodeEqualTo("EBPG");
		miPmntParamCriteria2.andProductCodeEqualTo("TCI");
		miPmntParamCriteria2.andStatusEqualTo(Short.valueOf("1"));
		ebpglist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample2);

		// AMEX
		List<MITblPmntParam> amexlist;
		MITblPmntParamExample miTblPmntParamExample3 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria miPmntParamCriteria3 = miTblPmntParamExample3.createCriteria();
		miPmntParamCriteria3.andPmntGatewayCodeEqualTo("AMEX");
		miPmntParamCriteria3.andProductCodeEqualTo("TCI");
		miPmntParamCriteria3.andStatusEqualTo(Short.valueOf("1"));
		amexlist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample3);

		model.addAttribute("m2ulist", m2ulist);
		model.addAttribute("fpxlist", fpxlist);
		model.addAttribute("ebpglist", ebpglist);
		model.addAttribute("amexlist", amexlist);

		return "/products/tripCare/tripcare-pmnt-method-edit";

	}

	@RequestMapping("/updateDoneTripCarePaymentMethod")
	public String updateDoneTripCarePaymentMethod(
			@ModelAttribute(value = "MITblPmntParam") MITblPmntParam miTblPmntParam, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		System.out.println("updateDoneTripCarePaymentMethod update done funtion ");

		// Maybank2U
		MITblPmntParamExample miTblPmntParamExample = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria miPmntParamCriteria = miTblPmntParamExample.createCriteria();
		miPmntParamCriteria.andPmntGatewayCodeEqualTo("M2U");
		miPmntParamCriteria.andProductCodeEqualTo("TCI");
		String m2u = "0";
		if (request.getParameter("m2u") != null) {
			m2u = request.getParameter("m2u");
			if (m2u.equals("on")) {
				m2u = "1";
			}
		}
		miTblPmntParam.setStatus(Short.valueOf(m2u));
		int miTblPmntParamUpdate = miTblPmntParamMapper.updateByExampleSelective(miTblPmntParam, miTblPmntParamExample);
		System.out.println(miTblPmntParamUpdate + "M2U");

		// FPX
		MITblPmntParamExample miTblPmntParamExample1 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria miPmntParamCriteria1 = miTblPmntParamExample1.createCriteria();
		miPmntParamCriteria1.andPmntGatewayCodeEqualTo("FPX");
		miPmntParamCriteria1.andProductCodeEqualTo("TCI");
		String fpx = "0";
		if (request.getParameter("fpx") != null) {
			fpx = request.getParameter("fpx");
			if (fpx.equals("on")) {
				fpx = "1";
			}
		}
		System.out.println(fpx + " fpx");

		miTblPmntParam.setStatus(Short.valueOf(fpx));
		int miTblPmntParamUpdate1 = miTblPmntParamMapper.updateByExampleSelective(miTblPmntParam,
				miTblPmntParamExample1);
		System.out.println(miTblPmntParamUpdate1 + "FPX");

		// EBPG
		MITblPmntParamExample miTblPmntParamExample2 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria miPmntParamCriteria2 = miTblPmntParamExample2.createCriteria();
		miPmntParamCriteria2.andPmntGatewayCodeEqualTo("EBPG");
		miPmntParamCriteria2.andProductCodeEqualTo("TCI");
		String ebpg = "0";
		if (request.getParameter("ebpg") != null) {
			ebpg = request.getParameter("ebpg");
			if (ebpg.equals("on")) {
				ebpg = "1";
			}
		}
		miTblPmntParam.setStatus(Short.valueOf(ebpg));
		int miTblPmntParamUpdate2 = miTblPmntParamMapper.updateByExampleSelective(miTblPmntParam,
				miTblPmntParamExample2);
		System.out.println(miTblPmntParamUpdate2 + "EBPG");

		// AMEX
		MITblPmntParamExample miTblPmntParamExample3 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria miPmntParamCriteria3 = miTblPmntParamExample3.createCriteria();
		miPmntParamCriteria3.andPmntGatewayCodeEqualTo("AMEX");
		miPmntParamCriteria3.andProductCodeEqualTo("TCI");
		String amex = "0";
		if (request.getParameter("amex") != null) {
			amex = request.getParameter("amex");
			if (amex.equals("on")) {
				amex = "1";
			}
		}
		miTblPmntParam.setStatus(Short.valueOf(amex));
		int miTblPmntParamUpdate3 = miTblPmntParamMapper.updateByExampleSelective(miTblPmntParam,
				miTblPmntParamExample3);
		System.out.println(miTblPmntParamUpdate3 + " AMEX");
		model.addAttribute("updatemessage", "Update data successfully");

		return tripCarePaymentMethod(request, response, model);
	}

	// -------------------------------------------------------End- Payment
	// Method----------------------------------------------------------

	// -------------------------------------------------------Start- Product
	// Rates----------------------------------------------------------

	@RequestMapping("/TripCareProductRates")
	public String tripCareProductRates(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("TripCareProductRates function ");

		try {

			// Domestic Premium values
			List<DomesticPremium> domPremiumList18;
			DomesticPremiumExample domesticPremiumExample = new DomesticPremiumExample();
			DomesticPremiumExample.Criteria domesticPremiumCriteria = domesticPremiumExample.createCriteria();
			domesticPremiumCriteria.andTravelwithTypeIdEqualTo("W01");
			domPremiumList18 = domesticPremiumMapper.selectByExample(domesticPremiumExample);

			List<DomesticPremium> domPremiumList71;
			DomesticPremiumExample domesticPremiumExample1 = new DomesticPremiumExample();
			DomesticPremiumExample.Criteria domesticPremiumCriteria1 = domesticPremiumExample1.createCriteria();
			domesticPremiumCriteria1.andTravelwithTypeIdEqualTo("W07");
			domPremiumList71 = domesticPremiumMapper.selectByExample(domesticPremiumExample1);

			List<DomesticPremium> domPremiumListIndv;
			DomesticPremiumExample domesticPremiumExample2 = new DomesticPremiumExample();
			DomesticPremiumExample.Criteria domesticPremiumCriteria2 = domesticPremiumExample2.createCriteria();
			domesticPremiumCriteria2.andTravelwithTypeIdEqualTo("W03");
			domPremiumListIndv = domesticPremiumMapper.selectByExample(domesticPremiumExample2);

			List<DomesticPremium> domPremiumListFamily;
			DomesticPremiumExample domesticPremiumExample3 = new DomesticPremiumExample();
			DomesticPremiumExample.Criteria domesticPremiumCriteria3 = domesticPremiumExample3.createCriteria();
			domesticPremiumCriteria3.andTravelwithTypeIdEqualTo("W05");
			domPremiumListFamily = domesticPremiumMapper.selectByExample(domesticPremiumExample3);

			// International Premium Values
			List<InterPremium> intPremiumList18;
			InterPremiumExample interPremiumExample = new InterPremiumExample();
			InterPremiumExample.Criteria interPremiumCriteria = interPremiumExample.createCriteria();
			interPremiumCriteria.andTravelwithTypeIdEqualTo("W01");
			intPremiumList18 = interPremiumMapper.selectByExample(interPremiumExample);

			List<InterPremium> intPremiumList71;
			InterPremiumExample interPremiumExample1 = new InterPremiumExample();
			InterPremiumExample.Criteria interPremiumCriteria1 = interPremiumExample1.createCriteria();
			interPremiumCriteria1.andTravelwithTypeIdEqualTo("W07");
			intPremiumList71 = interPremiumMapper.selectByExample(interPremiumExample1);

			List<InterPremium> intPremiumListIndv;
			InterPremiumExample interPremiumExample2 = new InterPremiumExample();
			InterPremiumExample.Criteria interPremiumCriteria2 = interPremiumExample2.createCriteria();
			interPremiumCriteria2.andTravelwithTypeIdEqualTo("W03");
			intPremiumListIndv = interPremiumMapper.selectByExample(interPremiumExample2);

			List<InterPremium> intPremiumListFamily;
			InterPremiumExample interPremiumExample3 = new InterPremiumExample();
			InterPremiumExample.Criteria interPremiumCriteria3 = interPremiumExample3.createCriteria();
			interPremiumCriteria3.andTravelwithTypeIdEqualTo("W05");
			intPremiumListFamily = interPremiumMapper.selectByExample(interPremiumExample3);

			FormatDates formatDate = new FormatDates();
			// Stamp Duty, GST, GstEffDate, SST, SstEffDate, Direct Discount (Online)
			List<TCSysSetup> sysSetupList;
			TCSysSetupExample tripCareParamExample2 = new TCSysSetupExample();
			TCSysSetupExample.Criteria tripCareParamCriteria2 = tripCareParamExample2.createCriteria();
			tripCareParamCriteria2.andCompanyIdEqualTo("I");
			tripCareParamCriteria2.andProductIdEqualTo("TCI");

			sysSetupList = tcSysSetupMapper.selectByExample(tripCareParamExample2);

			model.addAttribute("DomPremiumList18", domPremiumList18);
			model.addAttribute("DomPremiumList71", domPremiumList71);
			model.addAttribute("DomPremiumListIndv", domPremiumListIndv);
			model.addAttribute("DomPremiumListFamily", domPremiumListFamily);

			model.addAttribute("IntPremiumList18", intPremiumList18);
			model.addAttribute("IntPremiumList71", intPremiumList71);
			model.addAttribute("IntPremiumListIndv", intPremiumListIndv);
			model.addAttribute("IntPremiumListFamily", intPremiumListFamily);

			model.addAttribute("stampDutyValue", sysSetupList.get(0).getStampDuty());
			model.addAttribute("gstValue", sysSetupList.get(0).getGst());
			model.addAttribute("gstEffDateValue", formatDate.dateToString(sysSetupList.get(0).getGstEffDate()));
			model.addAttribute("sstValue", sysSetupList.get(0).getSst());
			model.addAttribute("sstEffDateValue", formatDate.dateToString(sysSetupList.get(0).getSstEffDate()));
			model.addAttribute("discountValue", sysSetupList.get(0).getStaffDiscPercent());
			System.out.println("TripCareProductRates discountValue ============================="
					+ sysSetupList.get(0).getStaffDiscPercent());

		} catch (Exception ex) {
			System.out.println("TripCareProductRates function =============================");
			ex.printStackTrace();
		}
		return "/products/tripCare/tripcare-prod-rates";

	}

	@RequestMapping("/UpdateTripCareProdRates")
	public String updateTripCareProdRates(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("UpdateTripCareProdRates function ");

		FormatDates formatDate = new FormatDates();
		// Stamp Duty, GST, GstEffDate, SST, SstEffDate, Direct Discount (Online)
		List<TCSysSetup> sysSetupList;
		TCSysSetupExample tripCareParamExample2 = new TCSysSetupExample();
		TCSysSetupExample.Criteria tripCareParamCriteria2 = tripCareParamExample2.createCriteria();
		tripCareParamCriteria2.andCompanyIdEqualTo("I");
		tripCareParamCriteria2.andProductIdEqualTo("TCI");
		sysSetupList = tcSysSetupMapper.selectByExample(tripCareParamExample2);

		model.addAttribute("stampDutyValue", sysSetupList.get(0).getStampDuty());
		model.addAttribute("gstValue", sysSetupList.get(0).getGst());
		model.addAttribute("gstEffDateValue", formatDate.dateToString(sysSetupList.get(0).getGstEffDate()));
		model.addAttribute("sstValue", sysSetupList.get(0).getSst());
		model.addAttribute("sstEffDateValue", formatDate.dateToString(sysSetupList.get(0).getSstEffDate()));
		model.addAttribute("discountValue", sysSetupList.get(0).getStaffDiscPercent());

		return "/products/tripCare/tripcare-prod-rates-edit";
	}

	@RequestMapping(value = "/UpdateTripCareProdRates", method = RequestMethod.GET)
	public @ResponseBody String updateTripCareProdRates(@RequestParam String pid, @RequestParam String premval,
			HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {

		String premid = null;
		if (request.getParameter("pid") != null) {
			premid = request.getParameter("pid");
		} else {
			premid = String.valueOf(request.getAttribute("pid"));
		}
		System.out.println("id from selection -" + premid);
		DomesticPremium domesticPremium = new DomesticPremium();
		domesticPremium.setId(new BigDecimal(premid));
		Double premium = 0.00;
		if (premval != null) {
			premium = Double.valueOf(premval);

		}
		domesticPremium.setPremiumValue(BigDecimal.valueOf(premium));
		System.out.println("id from Domestic update -" + premval);
		int domesticPremiumUpdate = domesticPremiumMapper.updateByPrimaryKeySelective(domesticPremium);
		System.out.println("id from Domestic update -" + domesticPremiumUpdate);
		String json = "success";
		System.out.println(json);

		return json;
	}

	@RequestMapping(value = "/UpdateTripCareDProdRates", method = RequestMethod.POST)
	public String updateTripCareDProdRates(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String premid = null;
		String premiumval = null;
		if (request.getParameter("pid") != null) {
			premid = request.getParameter("pid");
		} else {
			premid = String.valueOf(request.getAttribute("pid"));
		}
		if (request.getParameter("premiumval") != null) {
			premiumval = request.getParameter("premiumval");
		} else {
			premiumval = String.valueOf(request.getAttribute("premiumval"));
		}
		System.out.println("id from selection -" + premid);
		DomesticPremium domesticPremium = new DomesticPremium();
		domesticPremium.setId(new BigDecimal(premid));
		Double premium = 0.00;
		if (premiumval != null) {
			premium = Double.valueOf(premiumval);
		}
		domesticPremium.setPremiumValue(BigDecimal.valueOf(premium));
		System.out.println("id from Domestic premium -" + premium + "-" + premiumval);
		int domesticPremiumUpdate = domesticPremiumMapper.updateByPrimaryKeySelective(domesticPremium);
		System.out.println("id from Domestic rates update -" + domesticPremiumUpdate);

		model.addAttribute("tcUpdatedMessage", "Domestic Premium Updated Successfully!");

		return tripCareProductRates(request, response, model);
	}

	@RequestMapping(value = "/UpdateTripCareIntProdRates", method = RequestMethod.POST)
	public String updateTripCareIntProdRates(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String premid = null;
		String premiumval = null;
		if (request.getParameter("pintid") != null) {
			premid = request.getParameter("pintid");
		} else {
			premid = String.valueOf(request.getAttribute("pintid"));
		}
		if (request.getParameter("premiumintval") != null) {
			premiumval = request.getParameter("premiumintval");
		} else {
			premiumval = String.valueOf(request.getAttribute("premiumintval"));
		}
		System.out.println("id from selection -" + premid);

		InterPremium interPremium = new InterPremium();
		interPremium.setId(new BigDecimal(premid));
		Double premium = 0.00;
		if (premiumval != null) {
			premium = Double.valueOf(premiumval);

		}
		interPremium.setPremiumValue(BigDecimal.valueOf(premium));

		int interPremiumUpdate = interPremiumMapper.updateByPrimaryKeySelective(interPremium);
		model.addAttribute("tcUpdatedMessage", "TripCare International Premium Updated Successfully!");

		return tripCareProductRates(request, response, model);
	}
	// -------------------------------------------------------End- Product
	// Rates---------------------------------------------------------

	// ------------------------------------------------------START- Product
	// Info---------------------------------------------------------
	@RequestMapping("/TripCareProductInfo")
	public String tripCareProductInfo(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("TripCare Product Info ");

		// Quotation Validity
		List<TC360Param> quotationValidityList;
		TC360ParamExample tripCareParamExample = new TC360ParamExample();
		TC360ParamExample.Criteria tripCareParamCriteria = tripCareParamExample.createCriteria();
		tripCareParamCriteria.andParamTypeEqualTo("TC_quote_validity");
		quotationValidityList = tripCareParamMapper.selectByExample(tripCareParamExample);

		// Annual Sales Target
		List<TC360Param> annualSalesAmntList;
		TC360ParamExample tripCareParamExample1 = new TC360ParamExample();
		TC360ParamExample.Criteria tripCareParamCriteria1 = tripCareParamExample1.createCriteria();
		tripCareParamCriteria1.andParamTypeEqualTo("TC_annual_sales_target");
		annualSalesAmntList = tripCareParamMapper.selectByExample(tripCareParamExample1);

		List<TblPdfInfo> tblPdfInfoList;
		TblPdfInfoExample tblPdfInfoExample = new TblPdfInfoExample();

		TblPdfInfoExample.Criteria miPdfinfoCriteriaExist = tblPdfInfoExample.createCriteria();
		miPdfinfoCriteriaExist.andProductCodeEqualTo("TCI");
		tblPdfInfoExample.setOrderByClause("pds.ID desc");
		tblPdfInfoList = tblPdfInfoMapper.selectByExample(tblPdfInfoExample);

		model.addAttribute("tblPdfInfoList", tblPdfInfoList);

		model.addAttribute("quotationValidityList", quotationValidityList);
		model.addAttribute("annualSalesAmntList", annualSalesAmntList);

		return "/products/tripCare/tripcare-product-info";
	}

	@RequestMapping("/updateTripCareProductInfo")
	public String updateTripCareProductInfo(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("TripCare Product Info ");

		// Quotation Validity
		List<TC360Param> quotationValidityList;
		TC360ParamExample tripCareParamExample = new TC360ParamExample();
		TC360ParamExample.Criteria tripCareParamCriteria = tripCareParamExample.createCriteria();
		tripCareParamCriteria.andParamTypeEqualTo("TC_quote_validity");
		quotationValidityList = tripCareParamMapper.selectByExample(tripCareParamExample);

		// Annual Sales Target
		List<TC360Param> annualSalesAmntList;
		TC360ParamExample tripCareParamExample1 = new TC360ParamExample();
		TC360ParamExample.Criteria tripCareParamCriteria1 = tripCareParamExample1.createCriteria();
		tripCareParamCriteria1.andParamTypeEqualTo("TC_annual_sales_target");
		annualSalesAmntList = tripCareParamMapper.selectByExample(tripCareParamExample1);

		List<TblPdfInfo> tblPdfInfoList;
		TblPdfInfoExample tblPdfInfoExample = new TblPdfInfoExample();
		TblPdfInfoExample.Criteria miPdfinfoCriteriaExist = tblPdfInfoExample.createCriteria();
		miPdfinfoCriteriaExist.andProductCodeEqualTo("TCI");
		tblPdfInfoExample.setOrderByClause("pds.ID desc");
		tblPdfInfoList = tblPdfInfoMapper.selectByExample(tblPdfInfoExample);

		model.addAttribute("tblPdfInfoList", tblPdfInfoList);

		model.addAttribute("quotationValidityList", quotationValidityList);
		model.addAttribute("annualSalesAmntList", annualSalesAmntList);

		return "/products/tripCare/tripcare-product-info-edit";
	}

	public String numberFormat(String val) {
		Double iVal = null;
		iVal = Double.parseDouble(val == null ? "0" : val);
		DecimalFormat df = new DecimalFormat("0,000");
		System.out.println(df.format(iVal));
		return df.format(iVal);
	}

	@RequestMapping("/updateTripCareProductInfoApproval")
	public String updateTripCareProductInfoApproval(HttpServletRequest request, HttpServletResponse response,
			Model model) {
		System.out.println("update Product Info  approval ");

		try {
			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			// Quotation Validity
			List<TC360Param> listParamTripCareChange = new ArrayList<>();
			List<String> listOfTripCareParam = new ArrayList<>();
			listOfTripCareParam.add("TC_quote_validity");
			listOfTripCareParam.add("TC_annual_sales_target");
			TC360ParamExample tripCareParamExample = new TC360ParamExample();
			TC360ParamExample.Criteria tripCareParamCriteria = tripCareParamExample.createCriteria();
			tripCareParamCriteria.andParamTypeIn(listOfTripCareParam);
			List<TC360Param> listParamTripCareOriginal = tripCareParamMapper.selectByExample(tripCareParamExample);

			// Change Blob Storing Starts......
			TC360Param changeBlobParamQuotationData = new TC360Param();
			String quotVal = request.getParameter("quotVal");
			System.out.println(quotVal + "quotVal");
			changeBlobParamQuotationData.setId(listParamTripCareOriginal.get(0).getId());
			changeBlobParamQuotationData.setParamType("TC_quote_validity");
			changeBlobParamQuotationData.setParamCode(quotVal == null ? "0" : quotVal);
			changeBlobParamQuotationData.setParamDesc(quotVal + " days");
			changeBlobParamQuotationData.setParamDescMy(quotVal + " days");

			TC360Param changeBlobParamSalesTargetData = new TC360Param();
			String salesTargetVal = request.getParameter("salesTargetVal");
			System.out.println(salesTargetVal + "salesTargetVal");
			changeBlobParamSalesTargetData.setParamCode(salesTargetVal == null ? "0" : salesTargetVal);
			String formatVal1 = salesTargetVal;
			changeBlobParamSalesTargetData.setId(listParamTripCareOriginal.get(1).getId());
			changeBlobParamSalesTargetData.setParamType("TC_annual_sales_target");
			changeBlobParamSalesTargetData.setParamDesc(formatVal1);
			changeBlobParamSalesTargetData.setParamDescMy(formatVal1);

			listParamTripCareChange.add(changeBlobParamQuotationData);
			listParamTripCareChange.add(changeBlobParamSalesTargetData);

			convertBytes process = new convertBytes();

			byte[] changeBlob = process.convertToBytes(listParamTripCareChange);
			byte[] originalBlob = process.convertToBytes(listParamTripCareOriginal);

			ApprovalExample approvalExample = new ApprovalExample();
			com.spring.VO.ApprovalExample.Criteria createCriteriaApprovalExample = approvalExample.createCriteria();
			createCriteriaApprovalExample.andDescriptionEqualTo("TRIPCARE - CHANGE PRODUCT INFORMATION");
			List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

			System.out.println("ORI Data" + originalBlob);
			System.out.println("change Data" + changeBlob);

			BigDecimal approvalProdId = new BigDecimal(0);
			if (!listapproval.isEmpty()) {
				approvalProdId = listapproval.get(0).getId();
			}

			int maker = Integer.parseInt(loginUser);

			// setting all new change value to ApprovalLog POJA and Insert to ApprovalLog
			// table
			ApprovalLog approvalLog = new ApprovalLog();
			approvalLog.setOriginalContent(originalBlob);
			approvalLog.setNewContent(changeBlob);
			approvalLog.setApprovalId((short) approvalProdId.intValue()); // from Approval table this value[2] based on
																			// menu in feature
			approvalLog.setMaker((short) maker); // session login value
			approvalLog.setStatus("1");
			approvalLog.setCreateDate(new Date());
			approvalLog.setUpdateDate(new Date());
			approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
			int status = approvalLogMapper.insert(approvalLog);

		} catch (Exception e) {
			e.printStackTrace();
		}

		model.addAttribute("updatemessage", "Update data successfully");
		return tripCareProductInfo(request, response, model);
	}

	@RequestMapping("/UpdateTripCareProdRatesForApproval")
	public String updateTripCareProdRatesForApproval(@ModelAttribute(value = "TC360Param") TC360Param tripCareParam,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("UpdateTripCareProdRatesForApproval function ");

		try {
			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			// Original Blob Formation.....
			List<TCSysSetup> listTripCareParamOriginal;
			TCSysSetupExample tripCareParamExample2 = new TCSysSetupExample();
			TCSysSetupExample.Criteria tripCareParamCriteria2 = tripCareParamExample2.createCriteria();
			tripCareParamCriteria2.andCompanyIdEqualTo("I");
			tripCareParamCriteria2.andProductIdEqualTo("TCI");
			listTripCareParamOriginal = tcSysSetupMapper.selectByExample(tripCareParamExample2);

			FormatDates formatDate = new FormatDates();
			// Change Blob Formation.....
			List<TCSysSetup> listTripCareParamChange = new ArrayList<>();
			TCSysSetup tripCareParamChange = new TCSysSetup();
			Date gstEffDate = formatDate.stringToDate(request.getParameter("gstEffDate"));
			Date sstEffDate = formatDate.stringToDate(request.getParameter("sstEffDate"));
			tripCareParamChange.setStampDuty(new BigDecimal(request.getParameter("stampduty")));
			tripCareParamChange.setGst(new BigDecimal(request.getParameter("gst")));
			tripCareParamChange.setSst(new BigDecimal(request.getParameter("sst")));
			tripCareParamChange.setGstEffDate(gstEffDate);
			tripCareParamChange.setSstEffDate(sstEffDate);
			tripCareParamChange.setStaffDiscPercent(new BigDecimal(request.getParameter("discount")));

			listTripCareParamChange.add(tripCareParamChange);

			convertBytes process = new convertBytes();
			byte[] changeBlob = process.convertToBytes(listTripCareParamChange);
			byte[] originalBlob = process.convertToBytes(listTripCareParamOriginal);

			ApprovalExample approvalExample = new ApprovalExample();
			com.spring.VO.ApprovalExample.Criteria createCriteriaApprovalExample = approvalExample.createCriteria();
			createCriteriaApprovalExample.andDescriptionEqualTo("TRIPCARE - CHANGE PRODUCT RATE");
			List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

			System.out.println("ORI Data" + originalBlob);
			System.out.println("change Data" + changeBlob);

			BigDecimal approvalProdId = new BigDecimal(0);
			if (!listapproval.isEmpty()) {
				approvalProdId = listapproval.get(0).getId();
			}

			int maker = Integer.parseInt(loginUser);

			// setting all new change value to ApprovalLog POJA and Insert to ApprovalLog
			// table
			ApprovalLog approvalLog = new ApprovalLog();
			approvalLog.setOriginalContent(originalBlob);
			approvalLog.setNewContent(changeBlob);
			approvalLog.setApprovalId((short) approvalProdId.intValue()); // from Approval table this value[12] based on
																			// menu in feature
			approvalLog.setMaker((short) maker); // session login value
			approvalLog.setStatus("1");
			approvalLog.setNote("Y");
			approvalLog.setCreateDate(new Date());
			approvalLog.setUpdateDate(new Date());
			approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
			int status = approvalLogMapper.insert(approvalLog);

			if (status == 1) {

				model.addAttribute("updatemessage", "Update data successfully");
			}

			// Direct Discount (Online)

		} catch (Exception e) {
			e.printStackTrace();
		}
		return tripCareProductRates(request, response, model);
	}

	@RequestMapping(value = "/UpdateTripCareDProdRatesForApproval", method = RequestMethod.POST)
	public String updateTripCareDProdRatesForApproval(HttpServletRequest request, HttpServletResponse response,
			Model model) {

		try {
			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			String premid = null;
			String premiumval = null;
			if (request.getParameter("pid") != null) {
				premid = request.getParameter("pid");
			} else {
				premid = String.valueOf(request.getAttribute("pid"));
			}
			if (request.getParameter("premiumval") != null) {
				premiumval = request.getParameter("premiumval");
			} else {
				premiumval = String.valueOf(request.getAttribute("premiumval"));
			}
			System.out.println("id from selection -" + premid);

			// Original BLOB Information.....................
			DomesticPremiumExample domesticPremiumExample = new DomesticPremiumExample();
			DomesticPremiumExample.Criteria domesticPremiumExampleCriteria = domesticPremiumExample.createCriteria();
			domesticPremiumExampleCriteria.andIdEqualTo(new BigDecimal(premid));
			List<DomesticPremium> listDomesticPremium = domesticPremiumMapper.selectByExample(domesticPremiumExample);

			// Formation of Change BLOB Formation......
			List<TCProdMngPojo> domesticPremiumListChange = new ArrayList<>();
			TCProdMngPojo domesticPremium = new TCProdMngPojo();
			domesticPremium.setRecIdDomestic(premid);
			Double premium = 0.00;
			if (premiumval != null) {
				premium = Double.valueOf(premiumval);
			}

			domesticPremium.setPremiumValDomestic(String.valueOf(premium));
			domesticPremium.setProdType("D");
			domesticPremium.setDaysRangeKeycodeDomestic(listDomesticPremium.get(0).getDaysRangeKeycode());
			domesticPremium.setDestinationCodeAreaDomestic(listDomesticPremium.get(0).getDestinationCodeArea());
			domesticPremium.setPackageCodeDomestic(listDomesticPremium.get(0).getPackageCode());
			domesticPremium.setPlanCodeDomestic(listDomesticPremium.get(0).getPlanCode());
			domesticPremium.setTravelWithTypeIdDomestic(listDomesticPremium.get(0).getTravelwithTypeId());
			domesticPremiumListChange.add(domesticPremium);

			convertBytes process = new convertBytes();
			byte[] changeBlob = process.convertToBytes(domesticPremiumListChange);
			byte[] originalBlob = process.convertToBytes(listDomesticPremium);

			ApprovalExample approvalExample = new ApprovalExample();
			com.spring.VO.ApprovalExample.Criteria createCriteriaApprovalExample = approvalExample.createCriteria();
			createCriteriaApprovalExample.andDescriptionEqualTo("TRIPCARE - CHANGE PRODUCT RATE");
			List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

			System.out.println("ORI Data" + originalBlob);
			System.out.println("change Data" + changeBlob);

			BigDecimal approvalProdId = new BigDecimal(0);
			if (!listapproval.isEmpty()) {
				approvalProdId = listapproval.get(0).getId();
			}

			int maker = Integer.parseInt(loginUser);

			// setting all new change value to ApprovalLog POJA and Insert to ApprovalLog
			// table
			ApprovalLog approvalLog = new ApprovalLog();
			approvalLog.setOriginalContent(originalBlob);
			approvalLog.setNewContent(changeBlob);
			approvalLog.setApprovalId((short) approvalProdId.intValue()); // from Approval table this value[12] based on
																			// menu in feature
			approvalLog.setMaker((short) maker); // session login value
			approvalLog.setStatus("1");
			approvalLog.setCreateDate(new Date());
			approvalLog.setUpdateDate(new Date());
			approvalLog.setNote("D");
			approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
			int status = approvalLogMapper.insert(approvalLog);

			if (status == 1) {

				model.addAttribute("tcUpdatedMessage", "TripCare Domestic Premium Updated Successfully!");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return tripCareProductRates(request, response, model);
	}

	@RequestMapping(value = "/UpdateTripCareIntProdRatesForApproval", method = RequestMethod.POST)
	public String updateTripCareIntProdRatesForApproval(HttpServletRequest request, HttpServletResponse response,
			Model model) {

		try {
			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			String premid = null;
			String premiumval = null;
			if (request.getParameter("pintid") != null) {
				premid = request.getParameter("pintid");
			} else {
				premid = String.valueOf(request.getAttribute("pintid"));
			}
			if (request.getParameter("premiumintval") != null) {
				premiumval = request.getParameter("premiumintval");
			} else {
				premiumval = String.valueOf(request.getAttribute("premiumintval"));
			}
			System.out.println("id from selection -" + premid);

			// Original BLOB Formation......
			InterPremiumExample interPremiumExampleOriginal = new InterPremiumExample();
			InterPremiumExample.Criteria interPremiumExampleOriginalCriteria = interPremiumExampleOriginal
					.createCriteria();
			interPremiumExampleOriginalCriteria.andIdEqualTo(new BigDecimal(premid));
			List<InterPremium> interPremiumListOriginal = interPremiumMapper
					.selectByExample(interPremiumExampleOriginal);

			// Formation of Change BLOB Formation......
			List<TCProdMngPojo> interPremiumListChange = new ArrayList<>();
			TCProdMngPojo interPremium = new TCProdMngPojo();
			interPremium.setRecIdInternational(premid);
			Double premium = 0.00;
			if (premiumval != null) {
				premium = Double.valueOf(premiumval);
			}

			interPremium.setPremiumValInternational(String.valueOf(premium));
			interPremium.setProdType("INT");
			interPremium.setDaysRangeKeycodeInternational(interPremiumListOriginal.get(0).getDaysRangeKeycode());
			interPremium.setDestinationCodeAreaInternational(interPremiumListOriginal.get(0).getDestinationCodeArea());
			interPremium.setPackageCodeInternational(interPremiumListOriginal.get(0).getPackageCode());
			interPremium.setPlanCodeInternational(interPremiumListOriginal.get(0).getPlanCode());
			interPremium.setTravelWithTypeIdInternational(interPremiumListOriginal.get(0).getTravelwithTypeId());
			interPremiumListChange.add(interPremium);

			convertBytes process = new convertBytes();
			byte[] changeBlob = process.convertToBytes(interPremiumListChange);
			byte[] originalBlob = process.convertToBytes(interPremiumListOriginal);

			System.out.println("ORI Data" + originalBlob);
			System.out.println("change Data" + changeBlob);

			// Fetch approval Id.class..............
			ApprovalExample approvalExample = new ApprovalExample();
			com.spring.VO.ApprovalExample.Criteria createCriteriaApprovalExample = approvalExample.createCriteria();
			createCriteriaApprovalExample.andDescriptionEqualTo("TRIPCARE - CHANGE PRODUCT RATE");
			List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

			BigDecimal approvalProdId = new BigDecimal(0);
			if (!listapproval.isEmpty()) {
				approvalProdId = listapproval.get(0).getId();
			}

			int maker = Integer.parseInt(loginUser);

			// setting all new change value to ApprovalLog POJA and Insert to ApprovalLog
			// table
			ApprovalLog approvalLog = new ApprovalLog();
			approvalLog.setOriginalContent(originalBlob);
			approvalLog.setNewContent(changeBlob);
			approvalLog.setApprovalId((short) approvalProdId.intValue()); // from Approval table this value[12] based on
																			// menu in feature
			approvalLog.setMaker((short) maker); // session login value
			approvalLog.setStatus("1");
			approvalLog.setCreateDate(new Date());
			approvalLog.setUpdateDate(new Date());
			approvalLog.setNote("INT");
			approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
			int status = approvalLogMapper.insert(approvalLog);

			if (status == 1) {
				model.addAttribute("updatemessage", "Update data successfully");
			}

		} catch (Exception e) {

			e.printStackTrace();
		}

		return tripCareProductRates(request, response, model);
	}

	// ------------------------------------------------------END- Product
	// Info---------------------------------------------------------

}