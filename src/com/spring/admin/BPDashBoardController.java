package com.spring.admin;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.VO.BPDashBoard;
import com.spring.mapper.BPDashBoardMapper;

@Controller
public class BPDashBoardController {

	BPDashBoardMapper dashBoardMapper;

	@Autowired
	public BPDashBoardController(BPDashBoardMapper dashBoardMapper) {

		this.dashBoardMapper = dashBoardMapper;

	}

	@RequestMapping(value = "/getBPDashboard")
	public String getBPDashboard(HttpServletRequest request, Model model, HttpServletResponse response) {

		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");

		if (loginUser == null) {

			String sessionexpired = "Session Has Been Expired";

			model.addAttribute("sessionexpired", sessionexpired);

			return "admin-login";

		}

		BPDashBoard mdashBoard = new BPDashBoard();

		try {

			DateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY");
			Date date = new Date();
			String strDate = dateFormat.format(date);

			System.out.println("From Date : " + request.getParameter("fromdate"));

			if (request.getParameter("fromdate") != null) {
				strDate = request.getParameter("fromdate");
			}
			// mdashBoard.setFromdate(strDate);

			mdashBoard.setFromdate(strDate);

			// mdashBoard.setFromdate(fromdate);
			// System.out.println("fromdate from JSP : " + fromdate);

			dashBoardMapper.selectBPDashBoard(mdashBoard);

			System.out.println("BP dashBoard.getFromdate() : " + mdashBoard.getFromdate());

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error");
		}

		model.addAttribute("bpdashBoard", mdashBoard);
		return "admin-bancapa-dashboard";
	}

}
