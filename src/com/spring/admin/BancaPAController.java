package com.spring.admin;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.etiqa.DAO.UserValidation_DAO;
import com.etiqa.admin.User;
import com.etiqa.utils.SecurityUtil;
import com.spring.VO.BancaBranchCode;
import com.spring.VO.BancaBranchCodeExample;
import com.spring.VO.Menu;
import com.spring.mapper.BancaBranchCodeMapper;
import com.spring.service.MenuService;
import com.spring.utils.ServiceValidationUtils;

@Controller
@RequestMapping("banca")
public class BancaPAController {

	@Autowired
	MenuService menuService;
	BancaBranchCodeMapper bancaBranchCodeMapper;

	@Autowired
	public BancaPAController(BancaBranchCodeMapper bancaBranchCodeMapper) {

		this.bancaBranchCodeMapper = bancaBranchCodeMapper;

	}

	@RequestMapping("/BancaPAlandingpg")
	public String bpaLogin(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		// Added by prmaiyan start

		String userId = null;
		String role = null;
		String ModuleId = null;
		List<Menu> firstList = new ArrayList<Menu>();
		List<Menu> secondList = new ArrayList<Menu>();
		List<Menu> thirdList = new ArrayList<Menu>();
		List<Menu> fourthList = new ArrayList<Menu>();
		String uamReqParam = request.getParameter("id");
		session.setAttribute("uamReqParamSession", uamReqParam);

		
		if (uamReqParam != null) {
		
			String decryptMsg = SecurityUtil.decriptString(uamReqParam);
			
			// split the String for getting userId,roleId,ModuleId
			if (decryptMsg != null) {
				String[] items = decryptMsg.split("\\$");
				userId = items[0].toString();
				role = items[1].toString();
				ModuleId = items[2].toString();

				session.setAttribute("moduleId", Integer.parseInt(ModuleId));
			

			}

		} else {
			System.out.println("Encript UAM_Req_Param else  >>>> " + uamReqParam);
		}

		

		User muser = new User();

		muser = UserValidation_DAO.getvalidateByUserID(userId);

		session.setAttribute("FullName", muser.getFullName());
		session.setAttribute("logedUser", userId);
		int uId = muser.getID();
		session.setAttribute("user", String.valueOf(uId));
		

		String loginUser = (String) session.getAttribute("FullName");

		// get Menu List from Rest service start
		System.out.println("Calling DashBoard Controller Pramaiyan");
		Map<String, List<Menu>> map = getMenu(role, ModuleId);

		for (Entry<String, List<Menu>> entry : map.entrySet()) {
			String key = entry.getKey();
			List<Menu> values = entry.getValue();

			if (key == "1") {
				firstList.addAll(values);
			}
			if (key == "2") {
				secondList.addAll(values);
			}
			if (key == "3") {
				thirdList.addAll(values);
			}
			if (key == "4") {
				fourthList.addAll(values);
			}
		}

		session.setAttribute("level_1", firstList);
		session.setAttribute("level_2", secondList);
		session.setAttribute("level_3", thirdList);
		session.setAttribute("level_4", fourthList);

		// Added by prmaiyan END

		return "bancaPA/admin_bancapa_landingpage";
	}

	

	@RequestMapping(value = "/getBancaBranchCode", method = RequestMethod.POST)
	public String bpaLoginDone(HttpServletRequest request, HttpServletResponse response, Model model) {

	

		// Added by prmaiyan END

		BancaBranchCode bancaBranchCode = new BancaBranchCode();
		List<BancaBranchCode> bancaBranchCodeList = new ArrayList<BancaBranchCode>();
		BancaBranchCodeExample bancaBranchCodeExample = new BancaBranchCodeExample();
		bancaBranchCodeExample.setOrderByClause("bpabranch.BRANCH_ID desc");
		bancaBranchCodeList = bancaBranchCodeMapper.selectByExample(bancaBranchCodeExample);

		model.addAttribute("bancaBranchCodeList", bancaBranchCodeList);
		model.addAttribute("bancaBranchCode", bancaBranchCode);

		return "bancaPA/admin-bancapa-branchcode";
		/*
		 * } else return "bancaPA/admin-bancapa-login";
		 */

	}

	@RequestMapping("/bancaBranchCode")
	public String showBranchCode(HttpServletRequest request, Model model) {

		
		HttpSession session = request.getSession();
		// Added by prmaiyan start

		String userId = null;
		String role = null;
		String ModuleId = null;
		List<Menu> firstList = new ArrayList<Menu>();
		List<Menu> secondList = new ArrayList<Menu>();
		List<Menu> thirdList = new ArrayList<Menu>();
		List<Menu> fourthList = new ArrayList<Menu>();
		String uamReqParam = request.getParameter("id");
		session.setAttribute("uamReqParamSession", uamReqParam);

	
		if (uamReqParam != null) {
			
			String decryptMsg = SecurityUtil.decriptString(uamReqParam);
			
			// split the String for getting userId,roleId,ModuleId
			if (decryptMsg != null) {
				String[] items = decryptMsg.split("\\$");
				userId = items[0].toString();
				role = items[1].toString();
				ModuleId = items[2].toString();

				System.out.println("User Id :" + userId);
				System.out.println("Role Id :" + role);
				System.out.println("ModuleId :" + ModuleId);

			}

		} else {
			System.out.println("Encript UAM_Req_Param else  >>>> " + uamReqParam);
		}

	

		User muser = new User();

		muser = UserValidation_DAO.getvalidateByUserID(userId);

		session.setAttribute("FullName", muser.getFullName());
		session.setAttribute("logedUser", userId);
		int uId = muser.getID();
		session.setAttribute("user", String.valueOf(uId));
		System.out.println("Full Name  : " + muser.getFullName());

		String loginUser = (String) session.getAttribute("FullName");

		// get Menu List from Rest service start
		System.out.println("Calling DashBoard Controller Pramaiyan");
		Map<String, List<Menu>> map = getMenu(role, ModuleId);

		for (Entry<String, List<Menu>> entry : map.entrySet()) {
			String key = entry.getKey();
			List<Menu> values = entry.getValue();

			if (key == "1") {
				firstList.addAll(values);
			}
			if (key == "2") {
				secondList.addAll(values);
			}
			if (key == "3") {
				thirdList.addAll(values);
			}
			if (key == "4") {
				fourthList.addAll(values);
			}
		}

		session.setAttribute("level_1", firstList);
		session.setAttribute("level_2", secondList);
		session.setAttribute("level_3", thirdList);
		session.setAttribute("level_4", fourthList);

		// Added by prmaiyan END

		BancaBranchCode bancaBranchCode = new BancaBranchCode();

		List<BancaBranchCode> bancaBranchCodeList = new ArrayList<BancaBranchCode>();
		BancaBranchCodeExample bancaBranchCodeExample = new BancaBranchCodeExample();
		bancaBranchCodeExample.setOrderByClause("bpabranch.BRANCH_ID desc");
		bancaBranchCodeList = bancaBranchCodeMapper.selectByExample(bancaBranchCodeExample);

		model.addAttribute("bancaBranchCodeList", bancaBranchCodeList);
		model.addAttribute("bancaBranchCode", bancaBranchCode);

		return "bancaPA/admin-bancapa-branchcode";

	}

	@RequestMapping("/addBranchCodeDetails")
	public String addBranchCodeDetails(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "bancaPA/admin-bancapa-login";
		}
		BancaBranchCode bancaBranchCode = new BancaBranchCode();
		model.addAttribute("bancaBranchCode", bancaBranchCode);
		return "bancaPA/admin-bancapa-add-branchcode";

	}

	@RequestMapping("/updateBranchCodeStatus/{branchId}")
	public String updateBranchCodeStatus(HttpServletRequest request, Model model, @PathVariable String branchId) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "bancaPA/admin-bancapa-login";
		}

		List<BancaBranchCode> bancaBranchCodeList = new ArrayList<BancaBranchCode>();
		BancaBranchCodeExample bancaBranchCodeExample = new BancaBranchCodeExample();
		BancaBranchCodeExample.Criteria bancaBranchCode_criteria = bancaBranchCodeExample.createCriteria();
		bancaBranchCode_criteria.andBranchIdEqualTo(new BigDecimal(branchId));
		bancaBranchCodeList = bancaBranchCodeMapper.selectByExample(bancaBranchCodeExample);

		BancaBranchCode bancaBranchCode = bancaBranchCodeList.get(0);

		model.addAttribute("bancaBranchCode", bancaBranchCode);
		return "bancaPA/admin-bancapa-branchstatus";

	}

	@RequestMapping(value = "/searchBranchCodeDetails", method = RequestMethod.POST)
	public String searchBranchCodeDetails(@ModelAttribute(value = "bancaBranchCode") BancaBranchCode bancaBranchCode,
			HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");

		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "bancaPA/admin-bancapa-login";
		}

		List<String> errorMessages = new ArrayList<String>();

		try {

			if (errorMessages.size() > 0) {
				model.addAttribute("errorMessages", errorMessages);
				return "bancaPA/admin-bancapa-branchcode";
			}

			List<BancaBranchCode> bancaBranchCodeList = new ArrayList<BancaBranchCode>();
			BancaBranchCodeExample bancaBranchCodeExample = new BancaBranchCodeExample();
			BancaBranchCodeExample.Criteria bancaBranchCode_criteria = bancaBranchCodeExample.createCriteria();

			if (!ServiceValidationUtils.isEmptyStringTrim(bancaBranchCode.getContractType())) {
				bancaBranchCode_criteria.andContractTypeEqualTo(bancaBranchCode.getContractType());
			}

			if (!ServiceValidationUtils.isEmptyStringTrim(bancaBranchCode.getBranchCode())) {
				bancaBranchCode_criteria.andBranchCodeEqualTo(bancaBranchCode.getBranchCode());
			}

			if (!ServiceValidationUtils.isEmptyStringTrim(bancaBranchCode.getAgentNumber())) {
				bancaBranchCode_criteria.andAgentNumberEqualTo(bancaBranchCode.getAgentNumber());
			}

			if (!ServiceValidationUtils.isEmptyStringTrim(bancaBranchCode.getStatus())) {
				bancaBranchCode_criteria.andStatusEqualTo(bancaBranchCode.getStatus());
			}

			bancaBranchCodeList = bancaBranchCodeMapper.selectByExample(bancaBranchCodeExample);

			// if(bancaBranchCodeList.size()>0) {
			model.addAttribute("bancaBranchCodeList", bancaBranchCodeList);
			// }

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "bancaPA/admin-bancapa-branchcode";
		// e.printStackTrace();
	}

	@RequestMapping(value = "/addBranchCodeDetailsDone", method = RequestMethod.POST)
	public String addBranchCodeDetailsDone(@ModelAttribute(value = "bancaBranchCode") BancaBranchCode bancaBranchCode,
			HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");

		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "bancaPA/admin-bancapa-login";
		}

		int errorFlag = 0;
		if (ServiceValidationUtils.isEmptyStringTrim(bancaBranchCode.getBranchCode())) {
			errorFlag = 1;
		}

		if (ServiceValidationUtils.isEmptyStringTrim(bancaBranchCode.getAgentNumber())) {
			errorFlag = 1;
		}

		if (ServiceValidationUtils.isEmptyStringTrim(bancaBranchCode.getAgentNumberBPA())) {
			errorFlag = 1;
		}

		if (bancaBranchCode.getAgentNumber().equals(bancaBranchCode.getAgentNumberBPA())) {
			errorFlag = 2;
		}

		if (errorFlag == 1) {
			model.addAttribute("errorMessages", "Please Enter Branch code or Agent Number");

			return "bancaPA/admin-bancapa-add-branchcode";

		}

		if (errorFlag == 2) {
			model.addAttribute("errorMessages", "Please Enter Different BPA and TPA Agent Number");

			return "bancaPA/admin-bancapa-add-branchcode";

		}

		List<BancaBranchCode> bancaBranchCodeList = new ArrayList<BancaBranchCode>();
		BancaBranchCodeExample bancaBranchCodeExample = new BancaBranchCodeExample();
		BancaBranchCodeExample bancaBranchCodeExampleSelect = new BancaBranchCodeExample();
		BancaBranchCodeExample bancaBranchCodeExampleBPA = new BancaBranchCodeExample();
		BancaBranchCodeExample bancaBranchCodeExampleBranch = new BancaBranchCodeExample();
		BancaBranchCodeExample.Criteria bancaBranchCode_criteria = bancaBranchCodeExample.createCriteria();
		BancaBranchCodeExample.Criteria bancaBranchCode_criteriaBranch = bancaBranchCodeExampleBranch.createCriteria();
		BancaBranchCodeExample.Criteria bancaBranchCode_criteriaBPA = bancaBranchCodeExampleBPA.createCriteria();

		bancaBranchCode_criteriaBranch.andBranchCodeEqualTo(bancaBranchCode.getBranchCode());
		long countBranchCode = bancaBranchCodeMapper.countByExample(bancaBranchCodeExampleBranch);

		bancaBranchCode_criteria.andBranchCodeEqualTo(bancaBranchCode.getBranchCode());
		bancaBranchCode_criteria.andAgentNumberEqualTo(bancaBranchCode.getAgentNumber());
		long countTPA = bancaBranchCodeMapper.countByExample(bancaBranchCodeExample);

		bancaBranchCode_criteriaBPA.andBranchCodeEqualTo(bancaBranchCode.getBranchCode());
		bancaBranchCode_criteriaBPA.andAgentNumberEqualTo(bancaBranchCode.getAgentNumberBPA());
		long countBPA = bancaBranchCodeMapper.countByExample(bancaBranchCodeExampleBPA);

		if (countBranchCode == 2 || countTPA == 1 || countBPA == 1) {
			model.addAttribute("errorMessages", "Branch Code or Agent Number already exits.");
			return "bancaPA/admin-bancapa-add-branchcode";
		}

		try {
			bancaBranchCode.setContractType("TPA");
			bancaBranchCode.setCompany(new BigDecimal("2"));
			bancaBranchCode.setStatus("Y");
			bancaBranchCodeMapper.insert(bancaBranchCode);
			BancaBranchCode bancaBranchCodeBPA = new BancaBranchCode();
			bancaBranchCodeBPA = bancaBranchCode;
			bancaBranchCode.setContractType("BPA");
			bancaBranchCode.setCompany(new BigDecimal("1"));
			bancaBranchCodeBPA.setAgentNumber(bancaBranchCode.getAgentNumberBPA());
			bancaBranchCodeBPA.setStatus("Y");
			bancaBranchCodeMapper.insert(bancaBranchCodeBPA);

			bancaBranchCodeExampleSelect.setOrderByClause("bpabranch.BRANCH_ID desc");
			bancaBranchCodeList = bancaBranchCodeMapper.selectByExample(bancaBranchCodeExampleSelect);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		model.addAttribute("bancaBranchCodeList", bancaBranchCodeList);
		model.addAttribute("bancaBranchCode", new BancaBranchCode());
		return "bancaPA/admin-bancapa-branchcode";
		// e.printStackTrace();
	}

	@RequestMapping(value = "/updateBranchStatusDone", method = RequestMethod.POST)
	public String updateBranchStatusDone(@ModelAttribute(value = "bancaBranchCode") BancaBranchCode bancaBranchCode,
			HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");

		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "bancaPA/admin-bancapa-login";
		}

		// BancaBranchCodeExample bancaBranchCodeExample =new BancaBranchCodeExample();

		model.addAttribute("bancaBranchCode", bancaBranchCode);

		List<BancaBranchCode> bancaBranchCodeList = new ArrayList<BancaBranchCode>();

		try {

			BancaBranchCodeExample bancaBranchCodeExample = new BancaBranchCodeExample();
			BancaBranchCodeExample bancaBranchCodeExampleSelect = new BancaBranchCodeExample();
			BancaBranchCodeExample.Criteria bancaBranchCode_criteria = bancaBranchCodeExample.createCriteria();
			bancaBranchCode_criteria.andBranchIdEqualTo(bancaBranchCode.getBranchId());
			bancaBranchCodeMapper.updateByExampleSelective(bancaBranchCode, bancaBranchCodeExample);

			bancaBranchCodeExampleSelect.setOrderByClause("bpabranch.BRANCH_ID desc");
			bancaBranchCodeList = bancaBranchCodeMapper.selectByExample(bancaBranchCodeExampleSelect);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		model.addAttribute("bancaBranchCodeList", bancaBranchCodeList);
		model.addAttribute("bancaBranchCode", new BancaBranchCode());
		return "bancaPA/admin-bancapa-branchcode";
		// e.printStackTrace();
	}

	@SuppressWarnings("unchecked")
	private Map<String, List<Menu>> getMenu(String roleid, String moduleId) {
		System.out.println("roleid >>>" + roleid);
		System.out.println("moduleId >>>> " + moduleId);
		Menu m = new Menu();
		RestTemplate rt = new RestTemplate();
		MultiValueMap<String, Object> mapInput = new LinkedMultiValueMap<String, Object>();
		Map<String, List<Menu>> resp = new HashMap<String, List<Menu>>();
		// String roleid="6";
		String delimiters = "$";
		// String moduleId="1";
		try {
			resp = menuService.getMenu(roleid + delimiters + moduleId);
			System.out.println(resp);
			m.setModuleId(Integer.parseInt(moduleId));
			
			List<Menu> firstList = resp.get("first");
			List<Menu> secondList = resp.get("second");
			List<Menu> thirdList = resp.get("third");
			List<Menu> fourthList = resp.get("fourth");

			resp.put("1", firstList);
			resp.put("2", secondList);
			resp.put("3", thirdList);
			resp.put("4", fourthList);

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("no Connection");
		}
		return resp;
	}

}
