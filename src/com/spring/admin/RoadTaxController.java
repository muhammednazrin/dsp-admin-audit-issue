package com.spring.admin;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.etiqa.DAO.UserValidation_DAO;
import com.etiqa.admin.User;
/*import com.spring.mapper.roadtax.ConsignmentDAO;
import com.spring.mapper.roadtax.ConsignmentRecord;
import com.spring.mapper.roadtax.RoadTaxConsignmentJEXML;*/
import com.etiqa.email.RoadTaxEmailService;
import com.etiqa.utils.SecurityUtil;
import com.google.gson.Gson;
import com.spring.VO.Menu;
import com.spring.VO.RoadTaxUser;
//import com.ibm.icu.util.BytesTrie.Iterator;
import com.spring.VO.roadtaxVO.RdtaxDeliveryAdd;
import com.spring.VO.roadtaxVO.RdtaxInvoice;
import com.spring.VO.roadtaxVO.RdtaxInvoiceExample;
import com.spring.VO.roadtaxVO.RdtaxTrack;
import com.spring.VO.roadtaxVO.RdtaxTrackExample;
import com.spring.VO.roadtaxVO.RoadTaxConsignment;
import com.spring.VO.roadtaxVO.RoadTaxConsignmentExample;
import com.spring.VO.roadtaxVO.RoadTaxFloat;
import com.spring.VO.roadtaxVO.RoadTaxFloatExample;
import com.spring.VO.roadtaxVO.RoadTaxThreshold;
import com.spring.VO.roadtaxVO.RoadTaxThresholdExample;
import com.spring.VO.roadtaxVO.RoadTaxTxnReport;
import com.spring.VO.roadtaxVO.RoadTaxTxnReportExample;
import com.spring.mapper.roadtax.RdtaxDeliveryAddMapper;
import com.spring.mapper.roadtax.RdtaxInvoiceMapper;
import com.spring.mapper.roadtax.RdtaxTrackMapper;
import com.spring.mapper.roadtax.RoadTaxConsignmentMapper;
import com.spring.mapper.roadtax.RoadTaxFloatMapper;
import com.spring.mapper.roadtax.RoadTaxThresholdMapper;
import com.spring.mapper.roadtax.RoadTaxTxnReportMapper;
import com.spring.service.MenuService;
import com.spring.utils.ServiceValidationUtils;

@Controller
@RequestMapping("rtx")
public class RoadTaxController {
	@Autowired
	MenuService menuService;
	RdtaxInvoiceMapper rdtaxInvoiceMapper;
	RdtaxDeliveryAddMapper rdtaxDeliveryAddMapper;
	RoadTaxTxnReportMapper roadTaxTxnReportMapper;
	RdtaxTrackMapper rdtaxTrackMapper;
	RoadTaxFloatMapper roadTaxFloatMapper;
	RoadTaxConsignmentMapper roadTaxConsignmentMapper;
	RoadTaxThresholdMapper roadTaxThresholdMapper;
	RoadTaxUser rduser = new RoadTaxUser();

	@Autowired
	public RoadTaxController(RdtaxInvoiceMapper rdtaxInvoiceMapper, RdtaxDeliveryAddMapper rdtaxDeliveryAddMapper,
			RoadTaxTxnReportMapper roadTaxTxnReportMapper, RdtaxTrackMapper rdtaxTrackMapper,
			RoadTaxFloatMapper roadTaxFloatMapper, RoadTaxConsignmentMapper roadTaxConsignmentMapper,
			RoadTaxThresholdMapper roadTaxThresholdMapper) {
		this.rdtaxInvoiceMapper = rdtaxInvoiceMapper;
		this.rdtaxDeliveryAddMapper = rdtaxDeliveryAddMapper;
		this.roadTaxTxnReportMapper = roadTaxTxnReportMapper;
		this.rdtaxTrackMapper = rdtaxTrackMapper;
		this.roadTaxFloatMapper = roadTaxFloatMapper;
		this.roadTaxConsignmentMapper = roadTaxConsignmentMapper;
		this.roadTaxThresholdMapper = roadTaxThresholdMapper;
	}

	@RequestMapping("/getRoadTax")
	public String roadTax(HttpServletRequest request, Model model) {

		return "roadtax/pages/roadTaxAdminLogin";
	}

	@RequestMapping("/roadTaxLoginDone")
	public String roadTaxLoginDone(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();
		// new
		try {

			String userId = null;
			String roleId = null;
			String moduleId = null;
			List<Menu> firstList = new ArrayList<Menu>();
			List<Menu> secondList = new ArrayList<Menu>();
			List<Menu> thirdList = new ArrayList<Menu>();
			List<Menu> fourthList = new ArrayList<Menu>();
			String uamReqParam = request.getParameter("id");
			session.setAttribute("uamReqParamSession", uamReqParam);

			System.out.println("Encript UAM_Req_Param Outer >>>> " + uamReqParam);
			if (uamReqParam != null) {
				System.out.println("Encript UAM_Req_Param  In >>>> " + uamReqParam);
				String decryptMsg = SecurityUtil.decriptString(uamReqParam);
				System.out.println("Encript UAM_Req_Param " + decryptMsg);
				// split the String for getting userId,roleId,ModuleId
				if (decryptMsg != null) {
					String[] items = decryptMsg.split("\\$");
					userId = items[0].toString();
					roleId = items[1].toString();
					// if (!ServiceValidationUtils.isEmptyStringTrim(items[2])) {
					// moduleId = Integer.parseInt(items[2]);
					moduleId = items[2].toString();
					// }
					// session.setAttribute("moduleId",moduleId);
					session.setAttribute("moduleId", Integer.parseInt(moduleId));
					System.out.println("User Id :" + userId);
					System.out.println("Role Id :" + roleId);
					System.out.println("ModuleId :" + moduleId);

				}

			} else {
				System.out.println("Encript UAM_Req_Param else  >>>> " + uamReqParam);
			}

			System.out.println("Original " + roleId);

			User muser = new User();

			muser = UserValidation_DAO.getvalidateByUserID(userId);

			session.setAttribute("FullName", muser.getFullName());
			session.setAttribute("logedUser", userId);
			int uId = muser.getID();
			session.setAttribute("user", String.valueOf(uId));
			System.out.println("Full Name  : " + muser.getFullName());

			String loginUser = (String) session.getAttribute("FullName");

			// get Menu List from Rest service start
			System.out.println("Calling DashBoard Controller Pramaiyan");
			Map<String, List<Menu>> map = getMenu(roleId, moduleId);

			for (Entry<String, List<Menu>> entry : map.entrySet()) {
				String key = entry.getKey();
				List<Menu> values = entry.getValue();

				if (key == "1") {
					firstList.addAll(values);
				}
				if (key == "2") {
					secondList.addAll(values);
				}
				if (key == "3") {
					thirdList.addAll(values);
				}
				if (key == "4") {
					fourthList.addAll(values);
				}
			}

			session.setAttribute("level_1", firstList);
			session.setAttribute("level_2", secondList);
			session.setAttribute("level_3", thirdList);
			session.setAttribute("level_4", fourthList);

			// Added by prmaiyan END

			String currUserGroup = rduser.getUserGroup();
			if ("KL".equalsIgnoreCase(currUserGroup) || "SH".equalsIgnoreCase(currUserGroup)) {
				session.setAttribute("user", loginUser);

				if ("KL".equalsIgnoreCase(currUserGroup)) {

					System.out.println("<roadTaxLoginDone><userRole> <3> => " + roleId);

					// session.setAttribute("FullName", "POS KL");
					session.setAttribute("FullName", loginUser);
					session.setAttribute("stateCoverage", "Kuala Lumpur");

					// Newly added
					session.setAttribute("loginRole", currUserGroup);
					model.addAttribute("loginRole", currUserGroup);

				} else {
					System.out.println("<roadTaxLoginDone><userRole> <4> => " + roleId);

					// session.setAttribute("FullName", "POS SHAH ALAM");
					session.setAttribute("FullName", loginUser);
					session.setAttribute("stateCoverage", "Selangor");

					// Newly added
					session.setAttribute("loginRole", currUserGroup);
					model.addAttribute("loginRole", currUserGroup);
				}

				String stateCoverage = (String) session.getAttribute("stateCoverage");
				System.out.println("roadTaxLoginDone<stateCoverage> => " + stateCoverage);

				RdtaxDeliveryAdd rdtaxDeliveryAddMI = new RdtaxDeliveryAdd();
				rdtaxDeliveryAddMI.setEntityId("MI");
				rdtaxDeliveryAddMI.setStateCoverage(stateCoverage);
				RdtaxDeliveryAdd rdtaxDeliveryCntMI = rdtaxDeliveryAddMapper.selectDeliveryCount(rdtaxDeliveryAddMI);
				model.addAttribute("countMI", rdtaxDeliveryCntMI.getDeliveryCount());
				session.setAttribute("countMI", rdtaxDeliveryCntMI.getDeliveryCount());

				RdtaxDeliveryAdd rdtaxDeliveryAddMT = new RdtaxDeliveryAdd();
				rdtaxDeliveryAddMT.setEntityId("MT");
				rdtaxDeliveryAddMT.setStateCoverage(stateCoverage);
				RdtaxDeliveryAdd rdtaxDeliveryCntMT = rdtaxDeliveryAddMapper.selectDeliveryCount(rdtaxDeliveryAddMT);
				model.addAttribute("countMT", rdtaxDeliveryCntMT.getDeliveryCount());
				session.setAttribute("countMT", rdtaxDeliveryCntMT.getDeliveryCount());

				return "roadtax/pages/printing";

			} // else if ("252".equalsIgnoreCase(roleId)) {
			else if (!"KL".equalsIgnoreCase(currUserGroup) && !"SH".equalsIgnoreCase(currUserGroup)) {
				System.out.println("Loing as Road Tax Admin ");
				System.out.println("<roadTaxLoginDone><userRole> <1> => " + roleId);

				// session.setAttribute("user", un);
				// session.setAttribute("FullName", "Etiqa");

				// Pramaiyan modify
				// session.setAttribute("user", String.valueOf(uId));
				// session.setAttribute("FullName",loginUser);

				// End

				// Newly added
				session.setAttribute("loginRole", loginUser);
				model.addAttribute("loginRole", loginUser);

				return "roadtax/pages/roadTaxTransactionalReport";

			} else {

				System.out.println("<userRole> is empty => " + roleId);
				return "/roadTaxLogout";
			}

		} catch (Exception e) {
			errorMessages.add("An error has occurred while establishing a connection to the server !");
			model.addAttribute("errorMessages", errorMessages);
			return "roadtax/pages/printing";
		}

		// Old

		/*
		 * try {
		 *
		 * //Added by prmaiyan start
		 *
		 * String userId = null; String roleId = null; String ModuleId = null;
		 * List<Menu> firstList=new ArrayList<Menu>(); List<Menu> secondList=new
		 * ArrayList<Menu>(); List<Menu> thirdList=new ArrayList<Menu>(); List<Menu>
		 * fourthList=new ArrayList<Menu>(); String uamReqParam =
		 * request.getParameter("id"); session.setAttribute("uamReqParamSession",
		 * uamReqParam);
		 *
		 * System.out.println("Encript UAM_Req_Param Outer >>>> " + uamReqParam); if
		 * (uamReqParam != null) { System.out.println("Encript UAM_Req_Param  In >>>> "
		 * + uamReqParam); String decryptMsg = SecurityUtil.decriptString(uamReqParam);
		 * System.out.println("Encript UAM_Req_Param " + decryptMsg); // split the
		 * String for getting userId,roleId,ModuleId if (decryptMsg != null) { String[]
		 * items = decryptMsg.split("\\$"); userId = items[0].toString(); roleId =
		 * (items[1].toString()); ModuleId = items[2].toString();
		 *
		 * System.out.println("User Id :" + userId); System.out.println("Role Id :" +
		 * roleId); System.out.println("ModuleId :" + ModuleId);
		 *
		 * }
		 *
		 * } else { System.out.println("Encript UAM_Req_Param else  >>>> " +
		 * uamReqParam); }
		 *
		 * System.out.println("Original " + roleId);
		 *
		 * User muser = new User();
		 *
		 * muser = UserValidation_DAO.getvalidateByUserID(userId);
		 *
		 * session.setAttribute("FullName", muser.getFullName());
		 * session.setAttribute("logedUser", userId); int uId = muser.getID();
		 * session.setAttribute("user", String.valueOf(uId));
		 * System.out.println("Full Name  : " + muser.getFullName());
		 *
		 * String loginUser = (String) session.getAttribute("FullName");
		 *
		 *
		 * //get Menu List from Rest service start
		 * System.out.println("Calling DashBoard Controller Pramaiyan"); Map<String,
		 * List<Menu>> map = getMenu(roleId,ModuleId);
		 *
		 * for (Entry<String, List<Menu>> entry : map.entrySet()) { String key =
		 * entry.getKey(); List<Menu> values = entry.getValue();
		 *
		 * if(key=="1") { firstList.addAll(values); } if(key=="2") {
		 * secondList.addAll(values); } if(key=="3") { thirdList.addAll(values); }
		 * if(key=="4") { fourthList.addAll(values); } }
		 *
		 * session.setAttribute("level_1",firstList );
		 * session.setAttribute("level_2",secondList );
		 * session.setAttribute("level_3",thirdList );
		 * session.setAttribute("level_4",fourthList );
		 *
		 *
		 * //Added by prmaiyan END
		 *
		 * String un = "000333"; String pw = "Welcome01"; String userRole = "3";
		 *
		 *
		 * if ("000333".equalsIgnoreCase(un) && "Welcome01".equalsIgnoreCase(pw) ||
		 * "000444".equalsIgnoreCase(un) && "Welcome01".equalsIgnoreCase(pw) ||
		 * "3".equalsIgnoreCase(userRole) || "4".equalsIgnoreCase(userRole) ) {
		 * session.setAttribute("user", un);
		 *
		 * if ("000333".equalsIgnoreCase(un) || "3".equalsIgnoreCase(userRole) ) {
		 *
		 * System.out.println("<roadTaxLoginDone><userRole> <3> => " + userRole);
		 *
		 * session.setAttribute("FullName", "POS KL");
		 * session.setAttribute("stateCoverage", "Kuala Lumpur");
		 *
		 * // Newly added session.setAttribute("loginRole", "3");
		 * model.addAttribute("loginRole", "3");
		 *
		 * } else { System.out.println("<roadTaxLoginDone><userRole> <4> => " +
		 * userRole);
		 *
		 * session.setAttribute("FullName", "POS SHAH ALAM");
		 * session.setAttribute("stateCoverage", "Selangor");
		 *
		 * // Newly added session.setAttribute("loginRole", "4");
		 * model.addAttribute("loginRole", "4"); }
		 *
		 *
		 * String stateCoverage = (String) session.getAttribute("stateCoverage");
		 * System.out.println("roadTaxLoginDone<stateCoverage> => " + stateCoverage);
		 *
		 * RdtaxDeliveryAdd rdtaxDeliveryAddMI = new RdtaxDeliveryAdd();
		 * rdtaxDeliveryAddMI.setEntityId("MI");
		 * rdtaxDeliveryAddMI.setStateCoverage(stateCoverage); RdtaxDeliveryAdd
		 * rdtaxDeliveryCntMI =
		 * rdtaxDeliveryAddMapper.selectDeliveryCount(rdtaxDeliveryAddMI);
		 * model.addAttribute("countMI", rdtaxDeliveryCntMI.getDeliveryCount());
		 * session.setAttribute("countMI", rdtaxDeliveryCntMI.getDeliveryCount());
		 *
		 * RdtaxDeliveryAdd rdtaxDeliveryAddMT = new RdtaxDeliveryAdd();
		 * rdtaxDeliveryAddMT.setEntityId("MT");
		 * rdtaxDeliveryAddMT.setStateCoverage(stateCoverage); RdtaxDeliveryAdd
		 * rdtaxDeliveryCntMT =
		 * rdtaxDeliveryAddMapper.selectDeliveryCount(rdtaxDeliveryAddMT);
		 * model.addAttribute("countMT", rdtaxDeliveryCntMT.getDeliveryCount());
		 * session.setAttribute("countMT", rdtaxDeliveryCntMT.getDeliveryCount());
		 *
		 *
		 * return "roadtax/pages/printing";
		 *
		 * } else if ("000111".equalsIgnoreCase(un) && "Welcome01".equalsIgnoreCase(pw)
		 * || "1".equalsIgnoreCase(userRole)) {
		 *
		 * System.out.println("<roadTaxLoginDone><userRole> <1> => " + userRole);
		 *
		 * //session.setAttribute("user", un); //session.setAttribute("FullName",
		 * "Etiqa");
		 *
		 * //Pramaiyan modify session.setAttribute("user", String.valueOf(uId));
		 * session.setAttribute("FullName",loginUser);
		 *
		 * //End
		 *
		 *
		 * // Newly added session.setAttribute("loginRole", "1");
		 * model.addAttribute("loginRole", "1");
		 *
		 * return "roadtax/pages/roadTaxTransactionalReport";
		 *
		 * } else {
		 *
		 * System.out.println("<userRole> is empty => " + userRole); return
		 * "admin-login"; }
		 *
		 * } catch (Exception e) { errorMessages.
		 * add("An error has occurred while establishing a connection to the server !");
		 * model.addAttribute("errorMessages", errorMessages); return
		 * "roadtax/pages/printing"; }
		 */
	}

	@RequestMapping("/roadTaxLogout")
	public void adminLogout(HttpServletRequest request, HttpServletResponse response, Model model) {

		// Added by pramaiyan

		request.getSession().removeAttribute("user");
		request.getSession().removeAttribute("FullName");
		request.getSession().removeAttribute("logedUser");

		request.getSession().invalidate();

		// Added by pramaiyan on 13032018

		// String redirectToUrl = "www.dspuat.site/etiqauam"; //Absec UAT
		// String redirectToUrl = "172.29.124.1:7011/etiqauam"; //Mss UAT LAN

		String redirectToUrl = "localhost:8081/uam/"; // local
		try {
			// response.sendRedirect("https://" + redirectToUrl);
			response.sendRedirect("http://" + redirectToUrl);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/*
		 * request.getSession().removeAttribute("user");
		 * request.getSession().removeAttribute("FullName");
		 * request.getSession().invalidate()s
		 */

		// return "admin-login";
		// return "";
	}

	@RequestMapping("/printing")
	public String showPrintingReport(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadTaxAdminLogin";
			return "admin-login";
		}

		/*
		 * model.addAttribute("countMI", session.getAttribute("countMI"));
		 * model.addAttribute("countMT", session.getAttribute("countMT"));
		 */

		/**
		 * fetch and display the count of the records in Printing & Delivery both MI and
		 * MT
		 */
		List<String> errorMessages = new ArrayList<String>();
		try {

			String stateCoverage = (String) session.getAttribute("stateCoverage");
			System.out.println("printing<stateCoverage> =>" + stateCoverage);

			RdtaxDeliveryAdd rdtaxDeliveryAddMI = new RdtaxDeliveryAdd();
			rdtaxDeliveryAddMI.setEntityId("MI");
			rdtaxDeliveryAddMI.setStateCoverage(stateCoverage);
			RdtaxDeliveryAdd rdtaxDeliveryCntMI = rdtaxDeliveryAddMapper.selectDeliveryCount(rdtaxDeliveryAddMI);
			model.addAttribute("countMI", rdtaxDeliveryCntMI.getDeliveryCount());
			session.setAttribute("countMI", rdtaxDeliveryCntMI.getDeliveryCount());

			RdtaxDeliveryAdd rdtaxDeliveryAddMT = new RdtaxDeliveryAdd();
			rdtaxDeliveryAddMT.setEntityId("MT");
			rdtaxDeliveryAddMT.setStateCoverage(stateCoverage);
			RdtaxDeliveryAdd rdtaxDeliveryCntMT = rdtaxDeliveryAddMapper.selectDeliveryCount(rdtaxDeliveryAddMT);
			model.addAttribute("countMT", rdtaxDeliveryCntMT.getDeliveryCount());
			session.setAttribute("countMT", rdtaxDeliveryCntMT.getDeliveryCount());

			return "roadtax/pages/printing";
		} catch (Exception e) {
			errorMessages.add("An error has occurred while establishing a connection to the server !");
			model.addAttribute("errorMessages", errorMessages);
			return "roadtax/pages/printing";
		}
	}

	/**
	 * Displaying the records and fetching the float value from
	 * DSP_MI_TBL_RDTAX_FLOAT table
	 *
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/updatePrinting", method = RequestMethod.GET)
	public String updatePrinting(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		String stateCoverage = (String) session.getAttribute("stateCoverage");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadTaxAdminLogin";
			return "admin-login";
		}
		String callMethod = request.getParameter("callMethod");
		session.setAttribute("callMethod", callMethod);
		model.addAttribute("callMethod", callMethod);
		System.out.println("updatePrinting<callMethod> => " + callMethod);

		/**
		 * fetch and display the records in Printing & Delivery both MI and MT
		 */
		RdtaxDeliveryAdd rdtaxDeliveryAdd = new RdtaxDeliveryAdd();
		if (callMethod.equals("MI")) {
			rdtaxDeliveryAdd.setEntityId("MI");
			model.addAttribute("countMI", session.getAttribute("countMI"));
		} else {
			rdtaxDeliveryAdd.setEntityId("MT");
			model.addAttribute("countMT", session.getAttribute("countMT"));
		}
		System.out.println("updatePrinting<" + callMethod + "> => " + stateCoverage);
		rdtaxDeliveryAdd.setStateCoverage(stateCoverage);

		List<RdtaxDeliveryAdd> rdtaxDeliveryAddList = new ArrayList<RdtaxDeliveryAdd>();
		rdtaxDeliveryAddList = rdtaxDeliveryAddMapper.selectRdtaxDeliveryDetails(rdtaxDeliveryAdd);

		if (rdtaxDeliveryAddList.size() > 0) {
			System.out.println("updatePrinting<" + callMethod + "><" + stateCoverage + ">" + "<List size> => "
					+ rdtaxDeliveryAddList.size());
		} else {
			model.addAttribute("emptySearch", "No Record Found");
			System.out.println("updatePrinting<emptySearch> => " + rdtaxDeliveryAddList.size());
		}

		model.addAttribute("rdtaxDeliveryAddList", rdtaxDeliveryAddList);

		/**
		 * To fetch the float value form DSP_MI_TBL_RDTAX_FLOAT table. display the float
		 * value with decimal in Printing & Delivery
		 */
		NumberFormat formatter = new DecimalFormat("#0,000.00");

		RoadTaxFloat roadTaxFloat = new RoadTaxFloat();
		RoadTaxThreshold roadTaxThreshold = new RoadTaxThreshold();

		RoadTaxFloatExample roadTaxFloatExample = new RoadTaxFloatExample();
		RoadTaxFloatExample.Criteria float_criteria = roadTaxFloatExample.createCriteria();
		if (callMethod.equals("MI")) {
			// float_criteria.andEntityEqualTo("EIB");
			roadTaxFloat = roadTaxFloatMapper.selectByPrimaryKey(Short.valueOf("1"));

			if (roadTaxFloat != null) {
				System.out.println("updatePrinting<EIB><roadTaxFloat><CurrentFloatAmount> => "
						+ roadTaxFloat.getCurrentFloatAmount());
				// Todo - Chandra
				RoadTaxEmailService roadTaxEmailService = new RoadTaxEmailService();
				String thresholdEmailStatus = null;

				roadTaxThreshold = roadTaxThresholdMapper.selectByPrimaryKey(Short.valueOf("1"));

				System.out
						.println("updatePrinting<EIB><CurrentFloatAmount> => " + roadTaxFloat.getCurrentFloatAmount());
				System.out.println("updatePrinting<EIB><ThresholdLimit> => " + roadTaxThreshold.getThresholdLimit());

				if (roadTaxFloat.getCurrentFloatAmount().intValue() < 1) {
					model.addAttribute("floatAmoutCheck",
							"Please increase the Float Amount (RM) and amount shoud be greter than 0");

					model.addAttribute("errorMessages",
							"Please increase the Float Amount (RM) and amount shoud be greter than 0");

					return "roadtax/pages/printingUpdate";
				} else {

					if (roadTaxFloat.getCurrentFloatAmount().intValue() < roadTaxThreshold.getThresholdLimit()
							.intValue()) {
						model.addAttribute("errorMessages", "Your float amount has reached threshold limit RM "
								+ roadTaxThreshold.getThresholdLimit().intValue() + ".");

						// E-Mail
						System.out.println("updatePrinting<EIB><EmailTo> => " + roadTaxThreshold.getEmailTo());

						thresholdEmailStatus = roadTaxEmailService.callingRoadTaxThresholdEmailService(
								roadTaxThreshold.getEmailTo(), String.valueOf(roadTaxThreshold.getThresholdLimit()),
								String.valueOf(roadTaxFloat.getCurrentFloatAmount()), "EIB");
						System.out.println("updatePrinting<EIB><thresholdEmailStatus> => " + thresholdEmailStatus);
					}
					model.addAttribute("currentFloatValue", roadTaxFloat.getCurrentFloatAmount());
					model.addAttribute("currentFloatValue",
							formatter.format(Double.parseDouble(String.valueOf(roadTaxFloat.getCurrentFloatAmount()))));
					session.setAttribute("currentFloatValue", roadTaxFloat.getCurrentFloatAmount());

					// fetch and display the count of the records in Printing & Delivery - MI
					RdtaxDeliveryAdd rdtaxDeliveryAddMI = new RdtaxDeliveryAdd();
					rdtaxDeliveryAddMI.setEntityId("MI");
					rdtaxDeliveryAddMI.setStateCoverage(stateCoverage);
					RdtaxDeliveryAdd rdtaxDeliveryCntMI = rdtaxDeliveryAddMapper
							.selectDeliveryCount(rdtaxDeliveryAddMI);
					model.addAttribute("countMI", rdtaxDeliveryCntMI.getDeliveryCount());
					// session.setAttribute("countMI", rdtaxDeliveryCntMI.getDeliveryCount());
				}
			} else {
				double floatIn = 0.0f;
				RoadTaxFloat roadTaxFloatIn = new RoadTaxFloat();
				roadTaxFloatIn.setId(Short.valueOf("1"));
				roadTaxFloatIn.setFloatAmount(BigDecimal.valueOf(floatIn));
				roadTaxFloatIn.setCurrentFloatAmount(BigDecimal.valueOf(floatIn));
				roadTaxFloatIn.setEntity("EIB");
				roadTaxFloatMapper.insert(roadTaxFloatIn);
			}
		} else {
			// float_criteria.andEntityEqualTo("ETB");
			roadTaxFloat = roadTaxFloatMapper.selectByPrimaryKey(Short.valueOf("2"));

			if (roadTaxFloat != null) {
				System.out.println("updatePrinting<ETB><roadTaxFloat><CurrentFloatAmount> => "
						+ roadTaxFloat.getCurrentFloatAmount());

				// Todo - Chandra
				RoadTaxEmailService roadTaxEmailService = new RoadTaxEmailService();
				String thresholdEmailStatus = null;

				roadTaxThreshold = roadTaxThresholdMapper.selectByPrimaryKey(Short.valueOf("2"));

				System.out
						.println("updatePrinting<ETB><CurrentFloatAmount> => " + roadTaxFloat.getCurrentFloatAmount());
				System.out.println("updatePrinting<ETB><ThresholdLimit> => " + roadTaxThreshold.getThresholdLimit());

				if (roadTaxFloat.getCurrentFloatAmount().intValue() < 1) {
					model.addAttribute("floatAmoutCheck",
							"Please increase the Float Amount (RM) and amount shoud be greter than 0");
					model.addAttribute("errorMessages",
							"Please increase the Float Amount (RM) and amount shoud be greter than 0");

					return "roadtax/pages/printingUpdate";
				} else {

					if (roadTaxFloat.getCurrentFloatAmount().intValue() < roadTaxThreshold.getThresholdLimit()
							.intValue()) {

						model.addAttribute("errorMessages", "Your float amount has reached threshold limit RM "
								+ roadTaxThreshold.getThresholdLimit().intValue() + ".");

						// E-Mail
						System.out.println("updatePrinting<ETB><EmailTo> => " + roadTaxThreshold.getEmailTo());

						thresholdEmailStatus = roadTaxEmailService.callingRoadTaxThresholdEmailService(
								roadTaxThreshold.getEmailTo(), String.valueOf(roadTaxThreshold.getThresholdLimit()),
								String.valueOf(roadTaxFloat.getCurrentFloatAmount()), "ETB");
						System.out.println("updatePrinting<ETB><thresholdEmailStatus> => " + thresholdEmailStatus);
					}
					model.addAttribute("currentFloatValue", roadTaxFloat.getCurrentFloatAmount());
					model.addAttribute("currentFloatValue",
							formatter.format(Double.parseDouble(String.valueOf(roadTaxFloat.getCurrentFloatAmount()))));
					session.setAttribute("currentFloatValue", roadTaxFloat.getCurrentFloatAmount());

					// fetch and display the count of the records in Printing & Delivery - MT
					RdtaxDeliveryAdd rdtaxDeliveryAddMT = new RdtaxDeliveryAdd();
					rdtaxDeliveryAddMT.setEntityId("MT");
					rdtaxDeliveryAddMT.setStateCoverage(stateCoverage);
					RdtaxDeliveryAdd rdtaxDeliveryCntMT = rdtaxDeliveryAddMapper
							.selectDeliveryCount(rdtaxDeliveryAddMT);
					model.addAttribute("countMT", rdtaxDeliveryCntMT.getDeliveryCount());
					// session.setAttribute("countMT", rdtaxDeliveryCntMT.getDeliveryCount());
				}
			} else {
				double floatIn = 0.0f;
				RoadTaxFloat roadTaxFloatIn = new RoadTaxFloat();
				roadTaxFloatIn.setId(Short.valueOf("2"));
				roadTaxFloatIn.setFloatAmount(BigDecimal.valueOf(floatIn));
				roadTaxFloatIn.setCurrentFloatAmount(BigDecimal.valueOf(floatIn));
				roadTaxFloatIn.setEntity("ETB");
				roadTaxFloatMapper.insert(roadTaxFloatIn);
			}
		}
		System.out.println();
		return "roadtax/pages/printingUpdate";
	}

	/**
	 * insert the tracking details along with Consignment no in
	 * DSP_MI_TBL_RDTAX_TRACK table
	 *
	 * @param request
	 * @param model
	 * @param qqId
	 * @param dspQqId
	 * @param mainPolicyNo
	 * @param trackingNo
	 * @return
	 */
	@RequestMapping("/saveConsign")
	@ResponseBody
	public String saveConsign(HttpServletRequest request, Model model, @RequestParam String qqId,
			@RequestParam String dspQqId, @RequestParam String mainPolicyNo, @RequestParam String callMethod,
			@RequestParam String address, @RequestParam String deliveryTime, @RequestParam String expDeliveryDtSDD,
			@RequestParam String expDeliveryDtNDD) {

		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadTaxAdminLogin";
			return "admin-login";
		}

		/**
		 * fetching the id from DSP_MI_TBL_RDTAX_CONSIGNMENT table
		 */
		int consignUpdateCnt = 0;
		String id = "";
		String consignmentNo = "";
		List<RoadTaxConsignment> trackConsignmentList = new ArrayList<RoadTaxConsignment>();

		RoadTaxConsignmentExample roadTaxConsignmentExample = new RoadTaxConsignmentExample();
		RoadTaxConsignmentExample.Criteria consignment_criteria = roadTaxConsignmentExample.createCriteria();
		consignment_criteria.andIsUsedEqualTo("N");
		roadTaxConsignmentExample.setOrderByClause("rdtax.ID asc");
		trackConsignmentList = roadTaxConsignmentMapper.selectByExample(roadTaxConsignmentExample);

		/**
		 * inserting the Consignment no in DSP_MI_TBL_RDTAX_TRACK table
		 */
		RdtaxTrack track = new RdtaxTrack();
		track.setQqId(Integer.parseInt(qqId));
		track.setDspQqId(dspQqId.trim());
		track.setPolicyNo(mainPolicyNo);

		if (trackConsignmentList.size() > 0) {
			id = trackConsignmentList.get(0).getId();
			consignmentNo = trackConsignmentList.get(0).getConsignmentNo();
			track.setTrackingNo(consignmentNo);

			int recordCnt = rdtaxTrackMapper.insert(track);
			if (recordCnt > 0) {
				System.out.println("saveConsign<TRACK> => " + qqId + " " + mainPolicyNo + " " + consignmentNo
						+ " Data Inserted Successfully");
				model.addAttribute("saveConsign<TRACK>", "Data Inserted Successfully!");
			}

			// @TODO - Consignment In-Progress

			String MywsdlWebService = "";
			// String companyCodeNo=null;
			String resp = "";
			// companyCodeNo="96";

			// MywsdlWebService = "http://192.168.14.243:7011/ConsignmentNote";
			MywsdlWebService = "http://172.29.124.1:7011/ConsignmentNote";

			// URL url = new URL(MyLocations+"/ConsignmentNoteWebServiceImplService?WSDL");
			QName servicename = new QName("http://Services.Admin.dsp.etiqa.com/",
					"ConsignmentNoteWebServiceImplService");
			QName portname = new QName("http://Services.Admin.dsp.etiqa.com/", "ConsignmentNoteWebServiceImplPort");

			Service service = Service.create(null, servicename);
			service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
					MywsdlWebService + "/ConsignmentNoteWebServiceImplService");
			Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class, Service.Mode.MESSAGE);

			try {
				// MessageFactory mf =
				// MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
				// SOAPMessage soapMessage = mf.createMessage();
				SOAPMessage soapMessage = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL).createMessage();

				// SOAPPart soap = soapMessage.getSOAPPart();
				// SOAPEnvelope env = soap.getEnvelope();
				// SOAPBody body = env.getBody();
				SOAPBody body = soapMessage.getSOAPPart().getEnvelope().getBody();

				// request body;
				SOAPElement operation = body.addChildElement("ConsignmentNote", "ser",
						"http://Services.Admin.dsp.etiqa.com/");
				SOAPElement element_param = operation.addChildElement("CONSQQID").addTextNode(qqId);
				// element_param.addTextNode(qqId);

				// response body;
				// SOAPMessage response = dispatch.invoke(soapMessage);
				// SOAPBody responsebody = response.getSOAPBody();
				SOAPBody responsebody = dispatch.invoke(soapMessage).getSOAPBody();
				System.out.println("saveConsign<responsebody> => " + responsebody);

				if (responsebody != null) {
					Iterator iterator = responsebody.getChildElements();
					while (iterator.hasNext()) {
						String soapResConsign = (String) iterator.next();
						System.out.println("saveConsign<soapResConsign> => " + soapResConsign);
					}
				}
			} catch (SOAPException e) {
				e.printStackTrace();
			} catch (Exception e) {
				resp = resp + "{\"errorCode\":\"" + e.getMessage() + "\"}";
				e.printStackTrace();
			}

			// For testing in Local

			/*
			 * ConsignmentDAO dao=new ConsignmentDAO(); ConsignmentRecord
			 * ConRe=dao.getallConsignmentRecord("15555"); //MSS 17827
			 * RoadTaxConsignmentJEXML jrxml=new RoadTaxConsignmentJEXML(); try {
			 * jrxml.ConsignmentJEXML(ConRe); } catch (ClassNotFoundException e) { // TODO
			 * Auto-generated catch block e.printStackTrace(); } catch (JRException e) { //
			 * TODO Auto-generated catch block e.printStackTrace(); } catch (IOException e)
			 * { // TODO Auto-generated catch block e.printStackTrace(); } catch
			 * (SQLException e) { // TODO Auto-generated catch block e.printStackTrace(); }
			 */

			// End

			/**
			 * update the qq_id and is_used to 'Y' in DSP_MI_TBL_RDTAX_CONSIGNMENT table
			 */
			String emailStatus = "";
			String expDeliveryDt = "";
			RoadTaxConsignment roadTaxConsignment = new RoadTaxConsignment();
			roadTaxConsignment.setQqId(qqId);
			roadTaxConsignment.setIsUsed("Y");
			roadTaxConsignment.setEntity(callMethod);

			RoadTaxConsignmentExample roadTaxConsignmentExample1 = new RoadTaxConsignmentExample();
			RoadTaxConsignmentExample.Criteria roadTaxConsignUpdate_criteria = roadTaxConsignmentExample1
					.createCriteria();
			roadTaxConsignUpdate_criteria.andIdEqualTo(id);

			consignUpdateCnt = roadTaxConsignmentMapper.updateByExampleSelective(roadTaxConsignment,
					roadTaxConsignmentExample1);

			if (consignUpdateCnt > 0) {
				System.out.println("saveConsign<CONSIGNMENT> => " + qqId + " " + consignmentNo + " " + callMethod
						+ " Data updated Successfully");
				model.addAttribute("saveConsign<CONSIGNMENT>", "Consignment data updated Successfully!");

				// Printed - send email with consignment number
				if ("S".equalsIgnoreCase(deliveryTime)) {
					expDeliveryDt = expDeliveryDtSDD.substring(0, 10);

					System.out.println("deliveryTime1 => " + deliveryTime);
					System.out.println("expDeliveryDt1 => " + expDeliveryDt);
				} else {
					expDeliveryDt = expDeliveryDtNDD.substring(0, 10);
					System.out.println("deliveryTime2 => " + deliveryTime);
					System.out.println("expDeliveryDt2 => " + expDeliveryDt);
				}
				// Disable sending email to customer upon successful printing
				/*
				 * emailStatus = generateStatusEmail(request, model, mainPolicyNo, "print",
				 * consignmentNo, address, expDeliveryDt);
				 */
			}
			System.out.println();

			Map<String, String> response = new HashMap<String, String>();
			response.put("result", String.valueOf(consignmentNo));
			String jsonString = new Gson().toJson(response);
			return jsonString;

			/*
			 * Map<String, String> response = new HashMap<String, String>();
			 * response.put("result", String.valueOf(emailStatus)); String jsonString = new
			 * Gson().toJson(response); return jsonString;
			 */

		} else {
			System.out.println("saveConsign<No data found in CONSIGNMENT>");
			Map<String, String> response = new HashMap<String, String>();
			response.put("result", "Error");
			String jsonString = new Gson().toJson(response);
			return jsonString;
		}
	}

	/**
	 *
	 * @param request
	 * @param model
	 * @param qqId
	 * @param dspQqId
	 * @param mainPolicyNo
	 * @param remarks
	 * @param actVal
	 * @param totGST
	 * @return
	 */
	@RequestMapping("/printStatus")
	@ResponseBody
	public String printStatus(HttpServletRequest request, Model model, @RequestParam String qqId,
			@RequestParam String dspQqId, @RequestParam String mainPolicyNo, @RequestParam String remarks,
			@RequestParam String actVal, @RequestParam String totGST, @RequestParam String callMethod) {

		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadTaxAdminLogin";
			return "admin-login";
		}

		/**
		 * update the latest actual value in DSP_MI_TBL_RDTAX_DELIVERY_ADD table
		 */
		RoadTaxTxnReport roadTaxTxnReport = new RoadTaxTxnReport();
		roadTaxTxnReport.setActualAmount(actVal.trim());

		RoadTaxTxnReportExample roadTaxTxnReportExample = new RoadTaxTxnReportExample();
		RoadTaxTxnReportExample.Criteria roadTax_criteria = roadTaxTxnReportExample.createCriteria();
		roadTax_criteria.andQqIdEqualTo(qqId);

		int actValCnt = roadTaxTxnReportMapper.updateByExampleSelective(roadTaxTxnReport, roadTaxTxnReportExample);
		if (actValCnt > 0) {
			System.out.println("printStatus<DELIVERY_ADD> => " + qqId + " " + actVal + " "
					+ " Actual Value Updated Successfully!");
			model.addAttribute("printStatus<DELIVERY_ADD>", "Actual Value Updated Successfully!");
		}

		/**
		 * update the float amount after reducing the fee amount in
		 * DSP_MI_TBL_RDTAX_FLOAT table Float value = floatVal - ( actVal + rtpf +
		 * rtpfgst + rtprf + rtprfgst )
		 */
		double curFloatVal = 0.0f;
		double remFloatVal = 0.0f;

		curFloatVal = Double.parseDouble(session.getAttribute("currentFloatValue").toString());
		remFloatVal = curFloatVal - (Double.parseDouble(actVal) + Double.parseDouble(totGST));

		RoadTaxFloat roadTaxFloat = new RoadTaxFloat();
		roadTaxFloat.setCurrentFloatAmount(BigDecimal.valueOf(remFloatVal));

		RoadTaxFloatExample roadTaxFloatExample = new RoadTaxFloatExample();
		RoadTaxFloatExample.Criteria float_criteria = roadTaxFloatExample.createCriteria();
		if (callMethod.equals("MI")) {
			float_criteria.andIdEqualTo(Short.valueOf("1"));
		} else {
			float_criteria.andIdEqualTo(Short.valueOf("2"));
		}

		int floatValUpdateCnt = roadTaxFloatMapper.updateByExampleSelective(roadTaxFloat, roadTaxFloatExample);
		if (floatValUpdateCnt > 0) {
			System.out.println("printStatus<FLOAT> => " + callMethod + " " + remFloatVal + " "
					+ " Float Value is Updated Successfully!");
			model.addAttribute("printStatus<FLOAT>", "Float Value is Updated Successfully!");
		}

		/**
		 * update the printStatus and remarks DSP_MI_TBL_RDTAX_TRACK table
		 */
		RdtaxTrack track = new RdtaxTrack();
		track.setRemarks(remarks.trim());
		track.setPrintStatus("SUCCESS");
		// track.setRemarks("Printed");

		RdtaxTrackExample rdtaxTrack = new RdtaxTrackExample();
		RdtaxTrackExample.Criteria printStatus_criteria = rdtaxTrack.createCriteria();
		printStatus_criteria.andQqIdEqualTo(qqId.trim());
		printStatus_criteria.andDspQqIdEqualTo(dspQqId.trim());
		printStatus_criteria.andPolicyNoEqualTo(mainPolicyNo.trim());

		int trackCnt = rdtaxTrackMapper.updateByExampleSelective(track, rdtaxTrack);
		if (trackCnt > 0) {
			System.out.println("printStatus<TRACK> => " + qqId + " " + remarks + " Data Updated Successfully!");
			model.addAttribute("printStatus", "Data Updated Successfully!");
		}

		/**
		 * update the trackStatus and remarks DSP_MI_TBL_RDTAX_INVOICE table
		 */
		RdtaxInvoice rdtaxInvoice = new RdtaxInvoice();
		rdtaxInvoice.setTrackStatus("P");

		RdtaxInvoiceExample rdtaxInvoiceExample = new RdtaxInvoiceExample();
		RdtaxInvoiceExample.Criteria rdtaxInvoice_criteria = rdtaxInvoiceExample.createCriteria();
		// rdtaxInvoice_criteria.andDspQqIdEqualTo(qqId);
		// rdtaxInvoice_criteria.andDspQqIdEqualTo(dspQqId);
		rdtaxInvoice_criteria.andMainPolicyNoEqualTo(mainPolicyNo);

		int invoiceCnt = rdtaxInvoiceMapper.updateByExampleSelective(rdtaxInvoice, rdtaxInvoiceExample);
		if (invoiceCnt > 0) {
			System.out.println("printStatus<INVOICE> => " + callMethod + " " + mainPolicyNo
					+ " Track Status Updated Successfully!\n");
			model.addAttribute("printStatus<INVOICE>", "Track Status Updated Successfully!\n");
		}

		Map<String, String> response = new HashMap<String, String>();
		response.put("result", String.valueOf(invoiceCnt));
		String jsonString = new Gson().toJson(response);
		return jsonString;
	}

	/**
	 * Insert the refund Status in DSP_MI_TBL_RDTAX_TRACK table for tracking purpose
	 * and update track Status as 'U' in DSP_MI_TBL_RDTAX_INVOICE for reducing the
	 * displayed records.
	 *
	 * @param request
	 * @param model
	 * @param qqId
	 * @param dspQqId
	 * @param mainPolicyNo
	 * @param remarks
	 * @return
	 */
	@RequestMapping("/callRefundStatus")
	@ResponseBody
	public String refundStatusMI(HttpServletRequest request, Model model, @RequestParam String qqId,
			@RequestParam String dspQqId, @RequestParam String mainPolicyNo, @RequestParam String remarks) {

		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadTaxAdminLogin";
			return "admin-login";
		}
		String emailStatus = null;

		RdtaxTrack track = new RdtaxTrack();
		track.setRemarks(remarks.trim());
		track.setQqId(Integer.parseInt(qqId));
		track.setDspQqId(dspQqId);
		track.setPolicyNo(mainPolicyNo);
		track.setRefundStatus("SUCCESS");
		track.setRemarks("Unable to Print");
		track.setJpjStatus("Refer to JPJ");

		int recordCnt = rdtaxTrackMapper.insert(track);

		if (recordCnt > 0) {
			System.out.println("callRefundStatus<TRACK> => " + mainPolicyNo + " Data Inserted Successfully!");
			model.addAttribute("callRefundStatus<TRACK>", "Data Inserted Successfully!");
		}

		// update track status
		RdtaxInvoice rdtaxInvoice = new RdtaxInvoice();
		rdtaxInvoice.setTrackStatus("U");

		RdtaxInvoiceExample rdtaxInvoiceExample = new RdtaxInvoiceExample();
		RdtaxInvoiceExample.Criteria rdtaxInvoice_criteria = rdtaxInvoiceExample.createCriteria();
		// rdtaxInvoice_criteria.andDspQqIdEqualTo(qqId);
		// rdtaxInvoice_criteria.andDspQqIdEqualTo(dspQqId);
		rdtaxInvoice_criteria.andMainPolicyNoEqualTo(mainPolicyNo);

		int invoiceCnt = rdtaxInvoiceMapper.updateByExampleSelective(rdtaxInvoice, rdtaxInvoiceExample);
		if (invoiceCnt > 0) {

			System.out.println("callRefundStatus<INVOICE> => " + mainPolicyNo + " Track Status Updated Successfully!");
			model.addAttribute("callRefundStatus<INVOICE>", "Track Status Updated Successfully!");
			// Unable to print - send refund email
			// // Disable sending email to customer upon for refund upon failure of printing
			/*
			 * emailStatus = generateStatusEmail(request, model, mainPolicyNo, "uPrint", "",
			 * "", "");
			 */
		}
		System.out.println();

		/*
		 * Map<String, String> response = new HashMap<String, String>();
		 * response.put("result", String.valueOf(recordCnt)); String jsonString = new
		 * Gson().toJson(response); return jsonString;
		 */

		Map<String, String> response = new HashMap<String, String>();
		// response.put("result", String.valueOf(emailStatus));
		response.put("result", String.valueOf(invoiceCnt));
		String jsonString = new Gson().toJson(response);
		return jsonString;
	}

	/* ------------------------------------------------------------------ */
	/* Start - Road Tax Status Email */
	/*------------------------------------------------------------------- */
	/**
	 * Email
	 *
	 * @param request
	 * @param response
	 * @param model
	 * @param policyNumber
	 * @return
	 */
	/*
	 * @RequestMapping(value = "/generateRefundEmail", method = RequestMethod.POST)
	 *
	 * @ResponseBody
	 */ public String generateStatusEmail(HttpServletRequest request, Model model, String policyNumber,
			String roadTaxMailStatus, String consignmentNo, String address, String expDeliveryDt) {

		RoadTaxEmailService roadTaxEmailService = new RoadTaxEmailService();
		String status = null;
		try {

			System.out.println("generateStatusEmail<policyNumber> => " + policyNumber);
			// model.addAttribute("generateRefundEmail<policyNumber>", "Track Status Updated
			// Successfully!");

			status = roadTaxEmailService.callingRoadTaxEmailService(policyNumber, roadTaxMailStatus, consignmentNo,
					address, expDeliveryDt);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (!status.equals("")) {
			System.out.println("generateStatusEmail<status> => " + status);
		}

		/*
		 * Map<String, String> response = new HashMap<String, String>();
		 *
		 * response.put("statusResponse", status); String jsonString = new
		 * Gson().toJson(response); return jsonString;
		 */
		return status;
	}

	/* ------------------------------------------------------------------ */
	/* End - Road Tax Status Email */
	/*------------------------------------------------------------------- */

	/* ------------------------------------------------------------------ */
	/* Start - Road Tax Transaction Report */
	/*------------------------------------------------------------------- */
	@RequestMapping("/roadTaxTxnReport")
	public String showRoadTaxTransactionReport(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadTaxAdminLogin";
			return "admin-login";
		}
		return "roadtax/pages/roadTaxTransactionalReport";
	}

	@RequestMapping(value = "/searchRoadTaxTxnReport", method = RequestMethod.POST)
	public String searchRoadTaxTxnReport(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");

		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadTaxAdminLogin";
			return "admin-login";
		}

		RoadTaxTxnReport roadTaxTxnReport = new RoadTaxTxnReport();

		String dateFrom = request.getParameter("dateFrom").trim();
		String dateTo = request.getParameter("dateTo").trim();
		String NRIC = request.getParameter("NRIC").trim();
		String policyNo = request.getParameter("policyNo").trim();
		String plateNo = request.getParameter("plateNo").trim();
		String roadTaxStatus = request.getParameter("roadTaxStatus").trim();

		session.setAttribute("policyNo", policyNo);
		session.setAttribute("dateFrom", dateFrom);
		session.setAttribute("dateTo", dateTo);
		session.setAttribute("NRIC", NRIC);
		session.setAttribute("plateNo", plateNo);
		session.setAttribute("roadTaxStatus", roadTaxStatus);
		// In road tax report only show record that has been updated in printing and
		// delivery only
		String txnReport = request.getParameter("txnReport");
		if (!ServiceValidationUtils.isEmptyStringTrim(txnReport)) {
			roadTaxTxnReport.setTxnReport(txnReport);
		}
		System.out.println("searchRoadTaxTxnReport<txnReport> =>" + txnReport);

		if (!ServiceValidationUtils.isEmptyStringTrim(dateFrom)) {
			roadTaxTxnReport.setDateFrom(dateFrom);
			//System.out.println("searchRoadTaxTxnReportDateFrom<txnReport> =>" + roadTaxTxnReport.getDateFrom());
		}

		if (!ServiceValidationUtils.isEmptyStringTrim(dateTo)) {
			roadTaxTxnReport.setDateTo(dateTo);
			//System.out.println("searchRoadTaxTxnReportDateTo<txnReport> =>" + roadTaxTxnReport.getDateTo());
		}

		if (!ServiceValidationUtils.isEmptyStringTrim(NRIC)) {
			roadTaxTxnReport.setCustomerNricId(NRIC.trim());
		}

		if (!ServiceValidationUtils.isEmptyStringTrim(policyNo)) {
			roadTaxTxnReport.setPolicyNumber(policyNo.trim());
		}

		if (!ServiceValidationUtils.isEmptyStringTrim(plateNo)) {
			roadTaxTxnReport.setRegNo(plateNo);
		}

		if (!ServiceValidationUtils.isEmptyStringTrim(roadTaxStatus)) {
			if ("Printed".equals(roadTaxStatus)) {
				roadTaxTxnReport.setPrintStatus("SUCCESS");
			}
			if ("UnableToPrint".equals(roadTaxStatus)) {
				roadTaxTxnReport.setRefundStatus("SUCCESS");
			}
		}
		String stateCoverage = (String) session.getAttribute("stateCoverage");
		System.out.println("searchRoadTaxTxnReport<stateCoverage> =>" + stateCoverage);
		roadTaxTxnReport.setStateCoverage(stateCoverage);

		List<String> errorMessages = new ArrayList<String>();

		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			Date fromDate = null;
			Date toDate = null;

			try {
				fromDate = sdf.parse(dateFrom);
				toDate = sdf.parse(dateTo);

				if (fromDate.compareTo(toDate) > 0) {
					errorMessages.add("Date To Cannot Less Than Date From");
				} else {
					try {
						// in milliseconds
						long diff = toDate.getTime() - fromDate.getTime();
						long diffSeconds = diff / 1000 % 60;
						long diffMinutes = diff / (60 * 1000) % 60;
						long diffHours = diff / (60 * 60 * 1000) % 24;
						long diffDays = diff / (24 * 60 * 60 * 1000);

						if (diffDays > 31) {
							errorMessages.add("The date range cannot exceed than 1 month");
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}

			if (ServiceValidationUtils.isEmptyStringTrim(dateFrom)) {
				errorMessages.add("Date From Cannot Be Empty");
			}
			if (ServiceValidationUtils.isEmptyStringTrim(dateTo)) {
				errorMessages.add("Date To Cannot Be Empty");
			}
			if (errorMessages.size() > 0) {
				model.addAttribute("errorMessages", errorMessages);
				return "roadtax/pages/roadTaxTransactionalReport";
			}

			List<RoadTaxTxnReport> roadTaxTxnReportList = new ArrayList<RoadTaxTxnReport>();
			roadTaxTxnReportList = roadTaxTxnReportMapper.searchRoadTaxTxnReportDetails(roadTaxTxnReport);

			if (roadTaxTxnReportList.size() > 0) {
				System.out.println("searchRoadTaxTxnReport<size> =>" + roadTaxTxnReportList.size());
				model.addAttribute("roadTaxTxnReportList", roadTaxTxnReportList);
			}
			session.setAttribute("roadTaxTxnReportList", roadTaxTxnReportList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println();

		// Newly added
		String loginRole = (String) session.getAttribute("loginRole");
		System.out.println("searchRoadTaxTxnReport<loginRole> =>" + loginRole);

		session.setAttribute("loginRole", loginRole);
		model.addAttribute("loginRole", loginRole);

		return "roadtax/pages/roadTaxTransactionalReport";
		// e.printStackTrace();
	}

	@RequestMapping("/updateActualAmount")
	@ResponseBody
	public String saveActualAmount(HttpServletRequest request, Model model, @RequestParam String qqId,
			@RequestParam String actVal) {

		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadTaxAdminLogin";
			return "admin-login";
		}

		RoadTaxTxnReport roadTaxTxnReport = new RoadTaxTxnReport();
		roadTaxTxnReport.setActualAmount(actVal.trim());

		RoadTaxTxnReportExample roadTaxTxnReportExample = new RoadTaxTxnReportExample();
		RoadTaxTxnReportExample.Criteria roadTax_criteria = roadTaxTxnReportExample.createCriteria();
		roadTax_criteria.andQqIdEqualTo(qqId);

		int recordCnt = roadTaxTxnReportMapper.updateByExampleSelective(roadTaxTxnReport, roadTaxTxnReportExample);
		if (recordCnt > 0) {
			System.out.println("updateActualAmount<INVOICE> => " + qqId + "  " + actVal
					+ " Actual Value is Updated Successfully!");
			model.addAttribute("updateActualAmount<INVOICE>", " Actual Value is Updated Successfully!");
		}

		Map<String, String> response = new HashMap<String, String>();
		response.put("result", String.valueOf(recordCnt));
		String jsonString = new Gson().toJson(response);
		return jsonString;
	}

	/* ------------------------------------------------------------------ */
	/* End - Road Tax Transaction Report */
	/*------------------------------------------------------------------- */

	/* ------------------------------------------------------------------ */
	/* Start - Road Tax Transaction Refund Report */
	/*------------------------------------------------------------------- */
	@RequestMapping("/roadTaxRefReport")
	public String showRoadTaxRefundReport(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadTaxAdminLogin";
			return "admin-login";
		}
		return "roadtax/pages/roadTaxRefundReport";
	}

	@RequestMapping(value = "/searchRoadTaxRefundReport", method = RequestMethod.POST)
	public String searchRoadTaxRefundReport(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");

		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadTaxAdminLogin";
			return "admin-login";
		}

		RoadTaxTxnReport roadTaxTxnReport = new RoadTaxTxnReport();

		String dateFrom = request.getParameter("dateFrom").trim();
		String dateTo = request.getParameter("dateTo").trim();
		String NRIC = request.getParameter("NRIC").trim();
		String policyNo = request.getParameter("policyNo").trim();
		String plateNo = request.getParameter("plateNo").trim();
		// to display only Unable to print records
		String refundReport = request.getParameter("refundReport");

		if (!ServiceValidationUtils.isEmptyStringTrim(dateFrom)) {
			roadTaxTxnReport.setDateFrom(dateFrom);
		}

		if (!ServiceValidationUtils.isEmptyStringTrim(dateTo)) {
			roadTaxTxnReport.setDateTo(dateTo);
		}

		if (!ServiceValidationUtils.isEmptyStringTrim(NRIC)) {
			roadTaxTxnReport.setCustomerNricId(NRIC.trim());
		}

		if (!ServiceValidationUtils.isEmptyStringTrim(policyNo)) {
			roadTaxTxnReport.setPolicyNumber(policyNo.trim());
		}

		if (!ServiceValidationUtils.isEmptyStringTrim(plateNo)) {
			roadTaxTxnReport.setRegNo(plateNo);
		}

		if (!ServiceValidationUtils.isEmptyStringTrim(refundReport)) {
			roadTaxTxnReport.setRefundReport("SUCCESS");
		}

		List<String> errorMessages = new ArrayList<String>();

		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			Date fromDate = null;
			Date toDate = null;

			try {
				fromDate = sdf.parse(dateFrom);
				toDate = sdf.parse(dateTo);

				if (fromDate.compareTo(toDate) > 0) {
					errorMessages.add("Date To Cannot Less Than Date From");
				} else {
					try {
						// in milliseconds
						long diff = toDate.getTime() - fromDate.getTime();
						long diffSeconds = diff / 1000 % 60;
						long diffMinutes = diff / (60 * 1000) % 60;
						long diffHours = diff / (60 * 60 * 1000) % 24;
						long diffDays = diff / (24 * 60 * 60 * 1000);

						if (diffDays > 31) {
							errorMessages.add("The date range cannot exceed than 1 month");
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}

			if (ServiceValidationUtils.isEmptyStringTrim(dateFrom)) {
				errorMessages.add("Date From Cannot Be Empty");
			}
			if (ServiceValidationUtils.isEmptyStringTrim(dateTo)) {
				errorMessages.add("Date To Cannot Be Empty");
			}
			if (errorMessages.size() > 0) {
				model.addAttribute("errorMessages", errorMessages);
				return "roadtax/pages/roadTaxTransactionalReport";
			}

			System.out.println("searchRoadTaxRefundReport<refundReport> => " + roadTaxTxnReport.getRefundReport());

			List<RoadTaxTxnReport> roadTaxTxnReportList = new ArrayList<RoadTaxTxnReport>();
			roadTaxTxnReportList = roadTaxTxnReportMapper.searchRoadTaxTxnReportDetails(roadTaxTxnReport);

			if (roadTaxTxnReportList.size() > 0) {
				System.out.println("searchRoadTaxRefundReport<size> => " + roadTaxTxnReportList.size());
				model.addAttribute("roadTaxTxnReportList<size> => ", roadTaxTxnReportList);
			}
			session.setAttribute("roadTaxTxnReportList", roadTaxTxnReportList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println();

		// Newly added
		String loginRole = (String) session.getAttribute("loginRole");
		System.out.println("searchRoadTaxRefundReport<loginRole> =>" + loginRole);

		session.setAttribute("loginRole", loginRole);
		model.addAttribute("loginRole", loginRole);

		return "roadtax/pages/roadTaxRefundReport";
		// e.printStackTrace();
	}

	/**
	 * Email
	 *
	 * @param request
	 * @param response
	 * @param model
	 * @param policyNumber
	 * @return
	 */
	@RequestMapping(value = "/generateRefundEmail", method = RequestMethod.POST)
	@ResponseBody
	public String generateRefundEmail(HttpServletRequest request, Model model, @RequestParam String policyNumber) {

		RoadTaxEmailService roadTaxEmailService = new RoadTaxEmailService();
		String status = null;
		try {

			System.out.println("generateRefundEmail<policyNumber> -> " + policyNumber);
			// model.addAttribute("generateRefundEmail<policyNumber>", "Track Status Updated
			// Successfully!");

			status = roadTaxEmailService.callingRoadTaxEmailService(policyNumber, "uPrint", "", "", "");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (!status.equals("")) {
			System.out.println("generateRefundEmail<status> -> " + status);
		}

		Map<String, String> response = new HashMap<String, String>();

		response.put("statusResponse", status);
		String jsonString = new Gson().toJson(response);
		return jsonString;
	}

	/* ------------------------------------------------------------------ */
	/* End - Road Tax Transaction Refund Report */
	/*------------------------------------------------------------------- */

	/* ------------------------------------------------------------------ */
	/* Start - Road Tax Float Maintenance */
	/*------------------------------------------------------------------- */

	@RequestMapping(value = "/roadTaxFloatMaintenance")
	public String roadTaxFloatMaintenance(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");

		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadTaxAdminLogin";
			return "admin-login";
		}
		RoadTaxFloatExample roadTaxFloatExample = new RoadTaxFloatExample();
		List<RoadTaxFloat> roadTaxFloat = roadTaxFloatMapper.selectByExample(roadTaxFloatExample);
		model.addAttribute("roadTaxFloat", roadTaxFloat);

		return "roadtax/pages/roadTaxFloatMaintenance";
		// e.printStackTrace();
	}

	/**
	 *
	 * @param request
	 * @param model
	 * @param floatReplenishedVal
	 * @return
	 */
	@RequestMapping("/updateFloatReplenishedValue")
	@ResponseBody
	public String updateFloatReplenishedValue(HttpServletRequest request, Model model, @RequestParam String floatAmount,
			@RequestParam String curFloatAmount, @RequestParam String entity) {

		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadTaxAdminLogin";
			return "admin-login";
		}

		double floatAmountDB = 0.0f;
		double curFloatAmountDB = 0.0f;
		double floatAmountJSP = 0.0f;
		// double reloadAmountDB = 0.0f;
		// double curFloatAmountJSP = 0.0f;

		List<RoadTaxFloat> roadTaxFloatDB = new ArrayList<RoadTaxFloat>();
		RoadTaxFloat roadTaxFloat = new RoadTaxFloat();

		RoadTaxFloatExample roadTaxFloatExample = new RoadTaxFloatExample();
		RoadTaxFloatExample.Criteria roadTaxFloatExample_criteria = roadTaxFloatExample.createCriteria();
		roadTaxFloatExample_criteria.andEntityEqualTo(entity);
		roadTaxFloatDB = roadTaxFloatMapper.selectByExample(roadTaxFloatExample);

		Iterator<RoadTaxFloat> itr = roadTaxFloatDB.iterator();

		while (itr.hasNext()) {
			// Object o = itr.next();
			// roadTaxFloat = (RoadTaxFloat) o;
			roadTaxFloat = itr.next();
		}
		// Float Amount and Current Float Amount from DB
		floatAmountDB = Double.parseDouble(roadTaxFloat.getDispFloatAmount().replaceAll("[,]", ""));
		curFloatAmountDB = Double.parseDouble(roadTaxFloat.getDispCurrent().replaceAll("[,]", ""));
		// reloadAmountDB =
		// Double.parseDouble(roadTaxFloat.getDispreloadAmount().replaceAll("[,]", ""));

		System.out.println("updateFloatReplenishedValue<floatAmountDB> =>" + floatAmountDB);
		System.out.println("updateFloatReplenishedValue<curFloatAmountDB> =>" + curFloatAmountDB);

		// Float Amount and Current Float Amount from JSP
		floatAmountJSP = Double.parseDouble(floatAmount.replaceAll("[,]", ""));
		// curFloatAmountJSP = Double.parseDouble(curFloatAmount.replaceAll("[,]", ""));
		System.out.println("updateFloatReplenishedValue<floatAmountJSP> => " + floatAmountJSP);
		// System.out.println("updateFloatReplenishedValue<curFloatAmountJSP> => " +
		// curFloatAmountJSP);

		// Calculation
		floatAmountDB = floatAmountDB + floatAmountJSP;
		curFloatAmountDB = curFloatAmountDB + floatAmountJSP;
		System.out.println("updateFloatReplenishedValue<floatAmountToDB> => " + floatAmountDB);
		System.out.println("updateFloatReplenishedValue<curFloatAmountToDB> => " + curFloatAmountDB);

		RoadTaxFloat roadTaxFloatUpdate = new RoadTaxFloat();
		roadTaxFloatUpdate.setFloatAmount(BigDecimal.valueOf(floatAmountDB));
		roadTaxFloatUpdate.setCurrentFloatAmount(BigDecimal.valueOf(curFloatAmountDB));
		roadTaxFloatUpdate.setDateandtime(new java.util.Date());
		roadTaxFloatUpdate.setReloadAmount(BigDecimal.valueOf(floatAmountJSP));

		// Updated Float Amount and Current Float Amount update into
		// DSP_MI_TBL_RDTAX_FLOAT
		RoadTaxFloatExample roadTaxFloatExampleUpdate = new RoadTaxFloatExample();
		RoadTaxFloatExample.Criteria roadTaxFloatExampleUpdate_criteria = roadTaxFloatExampleUpdate.createCriteria();
		roadTaxFloatExampleUpdate_criteria.andEntityEqualTo(entity);

		int recordCnt = roadTaxFloatMapper.updateByExampleSelective(roadTaxFloatUpdate, roadTaxFloatExampleUpdate);

		if (recordCnt > 0) {
			System.out.println(
					"updateFloatReplenishedValue<DSP_MI_TBL_RDTAX_FLOAT> Float and Current Float amounts are Updated Successfully!\n");
			model.addAttribute("updateFloatReplenishedValue<DSP_MI_TBL_RDTAX_FLOAT>",
					"Float and Current Float amounts are Updated Successfully!\n");
		}

		Map<String, String> response = new HashMap<String, String>();
		response.put("result", String.valueOf(recordCnt));
		String jsonString = new Gson().toJson(response);
		return jsonString;
	}
	/* ------------------------------------------------------------------ */
	/* End - Road Tax Float Maintenance */
	/*------------------------------------------------------------------- */

	/* ------------------------------------------------------------------ */
	/* Start - Road Tax Float Report */
	/*------------------------------------------------------------------- */

	@RequestMapping(value = "/roadTaxFloatReport")
	public String roadTaxFloatReport(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");

		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadTaxAdminLogin";
			return "admin-login";
		}
		RoadTaxFloatExample roadTaxFloatExample = new RoadTaxFloatExample();
		List<RoadTaxFloat> roadTaxFloat = roadTaxFloatMapper.selectByExample(roadTaxFloatExample);
		model.addAttribute("roadTaxFloat", roadTaxFloat);
		request.setAttribute("roadTaxFloat", roadTaxFloat);

		return "roadtax/pages/roadTaxFloatReport";
		// e.printStackTrace();
	}

	@RequestMapping(value = "/floatReportAudit", method = RequestMethod.POST)
	public String floatReportAudit(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");

		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadTaxAdminLogin";
			return "admin-login";
		}

		RoadTaxFloatExample roadTaxFloatExample = new RoadTaxFloatExample();
		List<RoadTaxFloat> roadTaxFloat1 = roadTaxFloatMapper.selectByExample(roadTaxFloatExample);
		model.addAttribute("roadTaxFloat", roadTaxFloat1);
		request.setAttribute("roadTaxFloat", roadTaxFloat1);

		// model.addAttribute("roadTaxFloat", request.getAttribute("roadTaxFloat"));

		// RoadTaxTxnReport roadTaxTxnReport = new RoadTaxTxnReport();

		RoadTaxFloat roadTaxFloat = new RoadTaxFloat();

		String dateFrom = request.getParameter("dateFrom").trim();
		String dateTo = request.getParameter("dateTo").trim();
		String entity = request.getParameter("entity").trim();

		System.out.println("floatReportAudit<dateFrom> => " + dateFrom);
		System.out.println("floatReportAudit<dateTo> => " + dateTo);
		System.out.println("floatReportAudit<entity> => " + entity);

		if (!ServiceValidationUtils.isEmptyStringTrim(dateFrom)) {
			roadTaxFloat.setDateFrom(dateFrom);
		}

		if (!ServiceValidationUtils.isEmptyStringTrim(dateTo)) {
			roadTaxFloat.setDateTo(dateTo);
		}

		if (!ServiceValidationUtils.isEmptyStringTrim(entity)) {
			roadTaxFloat.setEntity(entity);
		}

		List<String> errorMessages = new ArrayList<String>();

		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			Date fromDate = null;
			Date toDate = null;

			try {
				fromDate = sdf.parse(dateFrom);
				toDate = sdf.parse(dateTo);

				if (fromDate.compareTo(toDate) > 0) {
					errorMessages.add("Date To Cannot Less Than Date From");
				} else {
					try {
						// in milliseconds
						long diff = toDate.getTime() - fromDate.getTime();
						long diffSeconds = diff / 1000 % 60;
						long diffMinutes = diff / (60 * 1000) % 60;
						long diffHours = diff / (60 * 60 * 1000) % 24;
						long diffDays = diff / (24 * 60 * 60 * 1000);

						if (diffDays > 31) {
							errorMessages.add("The date range cannot exceed than 1 month");
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}

			if (ServiceValidationUtils.isEmptyStringTrim(dateFrom)) {
				errorMessages.add("Date From Cannot Be Empty");
			}
			if (ServiceValidationUtils.isEmptyStringTrim(dateTo)) {
				errorMessages.add("Date To Cannot Be Empty");
			}
			if (errorMessages.size() > 0) {
				model.addAttribute("errorMessages", errorMessages);
				return "roadtax/pages/roadTaxFloatReport";
			}
			// number of days to add
			Calendar c = Calendar.getInstance();
			c.setTime(sdf.parse(dateTo));
			c.add(Calendar.DATE, 1);
			dateTo = sdf.format(c.getTime());

			RoadTaxFloatExample roadTaxAuditFloatExample = new RoadTaxFloatExample();
			RoadTaxFloatExample.Criteria roadTaxAuditCriteria = roadTaxAuditFloatExample.createCriteria();
			roadTaxAuditCriteria.andDateandtimeBetween(fromDate, sdf.parse(dateTo));
			if (!ServiceValidationUtils.isEmptyStringTrim(entity)) {
				roadTaxAuditCriteria.andEntityEqualTo(entity);
			}
			roadTaxAuditFloatExample.setOrderByClause("rdflt.DATEANDTIME desc");
			List<RoadTaxFloat> roadTaxAuditFloat = roadTaxFloatMapper.selectByAuditExample(roadTaxAuditFloatExample);

			if (roadTaxAuditFloat.size() > 0) {
				System.out.println("floatReportAudit<roadTaxAuditFloat.size> => " + roadTaxAuditFloat.size());
				model.addAttribute("roadTaxAuditFloat", roadTaxAuditFloat);
				request.setAttribute("roadTaxAuditFloat", roadTaxAuditFloat);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println();

		return "roadtax/pages/roadTaxFloatReport";
		// e.printStackTrace();
	}

	/* ------------------------------------------------------------------ */
	/* End - Road Tax Float Report */
	/*------------------------------------------------------------------- */

	/* ------------------------------------------------------------------ */
	/* Start - Float Threshold Alert */
	/*------------------------------------------------------------------- */
	@RequestMapping(value = "/loadFloatThresholdAlertPage")
	public String loadFloatThresholdAlertPage(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");

		/*
		 * if (loginUser == null) { String sessionexpired = "Session Has Been Expired";
		 * model.addAttribute("sessionexpired", sessionexpired); // return
		 * "roadtax/pages/roadTaxAdminLogin"; return "admin-login"; }
		 */
		// to display the Road Tax Threshold records
		RoadTaxThresholdExample roadTaxThresholdExample = new RoadTaxThresholdExample();
		List<RoadTaxThreshold> RoadTaxThreshold = roadTaxThresholdMapper.selectByExample(roadTaxThresholdExample);

		if (RoadTaxThreshold.size() > 0) {
			System.out.println("loadFloatThresholdAlertPage<RoadTaxThreshold.size> => " + RoadTaxThreshold.size());
			model.addAttribute("RoadTaxThreshold", RoadTaxThreshold);
		}

		return "roadtax/pages/roadTaxThresholdAlt";
	}

	@RequestMapping(value = "/saveThresholdAlt")
	public String saveThresholdAlt(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");

		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadTaxAdminLogin";
			return "admin-login";
		}

		String interval = request.getParameter("interval").trim();
		String email = request.getParameter("email").trim();
		String limit = request.getParameter("limit").trim();
		String entity = request.getParameter("entity").trim();

		List<String> errorMessages = new ArrayList<String>();
		List<String> dbErrorMessages = new ArrayList<String>();
		// RoadTaxThreshold roadTaxThreshold = new RoadTaxThreshold();
		// RoadTaxThreshold roadTaxThresholdDB = new RoadTaxThreshold();

		if (ServiceValidationUtils.isEmptyStringTrim(interval)) {
			errorMessages.add("Please select the Interval By Hours !");
		} else if (ServiceValidationUtils.isEmptyStringTrim(limit)) {
			errorMessages.add("Please enter the Threshold Limit [RM] !");
		} else if (ServiceValidationUtils.isEmptyStringTrim(entity)) {
			errorMessages.add("Please select the Entity !");
		} else if (ServiceValidationUtils.isEmptyStringTrim(email)) {
			errorMessages.add("Please enter the EMail To !");
		}

		if (errorMessages.size() > 0) {
			session.setAttribute("interval", interval);
			session.setAttribute("email", email);
			session.setAttribute("limit", limit);
			session.setAttribute("entity", entity);

			model.addAttribute("errorMessages", errorMessages);
			return "roadtax/pages/roadTaxThresholdAlt";
		} else {
			try {
				RoadTaxThreshold roadTaxThreshold = new RoadTaxThreshold();
				RoadTaxThreshold roadTaxThresholdDB = new RoadTaxThreshold();

				/**
				 * Mulitiple Email handling
				 */
				/*
				 * List<RoadTaxThreshold> roadTaxThresholdList = new
				 * ArrayList<RoadTaxThreshold>(); RoadTaxThresholdExample
				 * roadTaxThresholdExample = new RoadTaxThresholdExample();
				 * RoadTaxThresholdExample.Criteria emai_criteria =
				 * roadTaxThresholdExample.createCriteria(); if ( entity.equals("EIB"))
				 * emai_criteria.andEntityEqualTo("EIB"); else
				 * emai_criteria.andEntityEqualTo("ETB");
				 *
				 * roadTaxThresholdList =
				 * roadTaxThresholdMapper.selectByExample(roadTaxThresholdExample);
				 *
				 * System.out.println("saveThresholdAlt<emailDB> => " +
				 * roadTaxThresholdList.get(0).getEmailTo()); email =
				 * email+";"+roadTaxThresholdList.get(0).getEmailTo();
				 * System.out.println("saveThresholdAlt<email-db-plus-input> => " + email);
				 */
				// End

				roadTaxThreshold.setIntervalByHours(Short.parseShort(interval.trim()));
				roadTaxThreshold.setEmailTo(email.trim());
				roadTaxThreshold.setThresholdLimit(BigDecimal.valueOf(Double.valueOf(limit.trim())));
				roadTaxThreshold.setEntity(entity.trim());

				RoadTaxThresholdExample roadTaxThresholdExample1 = new RoadTaxThresholdExample();
				RoadTaxThresholdExample.Criteria thresholdCriteria = roadTaxThresholdExample1.createCriteria();
				thresholdCriteria.andEntityEqualTo(entity);
				/*
				 * List<RoadTaxThreshold> roadTaxThresholdList = roadTaxThresholdMapper
				 * .selectByExample(roadTaxThresholdExample);
				 */

				if ("EIB".equalsIgnoreCase(entity)) {

					// roadTaxThresholdDB = (RoadTaxThreshold)
					// roadTaxThresholdMapper.selectByExample(roadTaxThresholdExample);
					roadTaxThresholdDB = roadTaxThresholdMapper.selectByPrimaryKey(Short.valueOf("1"));

					if (roadTaxThresholdDB != null) {
						int recordCnt = roadTaxThresholdMapper.updateByExampleSelective(roadTaxThreshold,
								roadTaxThresholdExample1);

						if (recordCnt > 0) {
							System.out.println("saveThresholdAlt<THRESHOLD> => " + interval + " " + email + " " + limit
									+ " " + entity + " Data Updated Successfully!");
							model.addAttribute("saveThresholdAlt<THRESHOLD>", "Data Updated Successfully!");
							model.addAttribute("dbErrorMessages", "Data Updated Successfully!");
						}
					} else {
						int recordCnt = roadTaxThresholdMapper.insert(roadTaxThreshold);

						if (recordCnt > 0) {
							System.out.println("saveThresholdAlt<THRESHOLD> => " + interval + " " + email + " " + limit
									+ " " + entity + " Data Inserted Successfully!");
							model.addAttribute("saveThresholdAlt<THRESHOLD>", "Data Inserted Successfully!");
							model.addAttribute("dbErrorMessages", "Data Inserted Successfully!");
						}
					}
				} else {
					// roadTaxThresholdDB = (RoadTaxThreshold)
					// roadTaxThresholdMapper.selectByExample(roadTaxThresholdExample);
					roadTaxThresholdDB = roadTaxThresholdMapper.selectByPrimaryKey(Short.valueOf("2"));

					if (roadTaxThresholdDB != null) {
						int recordCnt = roadTaxThresholdMapper.updateByExampleSelective(roadTaxThreshold,
								roadTaxThresholdExample1);

						if (recordCnt > 0) {
							System.out.println("saveThresholdAlt<THRESHOLD> => " + interval + " " + email + " " + limit
									+ " " + entity + " Data Updated Successfully!");
							model.addAttribute("saveThresholdAlt<THRESHOLD>", "Data Updated Successfully!");
							model.addAttribute("dbErrorMessages", "Data Updated Successfully!");
						}
					} else {
						int recordCnt = roadTaxThresholdMapper.insert(roadTaxThreshold);

						if (recordCnt > 0) {
							System.out.println("saveThresholdAlt<THRESHOLD> => " + interval + " " + email + " " + limit
									+ " " + entity + " Data Inserted Successfully!");
							model.addAttribute("saveThresholdAlt<THRESHOLD>", "Data Inserted Successfully!");
							model.addAttribute("dbErrorMessages", "Data Inserted Successfully!");
						}
					}
				}

			} catch (Exception e) {
				errorMessages.add("An error has occurred while establishing a connection to the server !");
				model.addAttribute("errorMessages", errorMessages);
				// e.printStackTrace();

				return "roadtax/pages/roadTaxThresholdAlt";
			}
		}

		// to display the Road Tax Threshold records
		RoadTaxThresholdExample roadTaxThresholdExample = new RoadTaxThresholdExample();
		List<RoadTaxThreshold> RoadTaxThreshold = roadTaxThresholdMapper.selectByExample(roadTaxThresholdExample);

		if (RoadTaxThreshold.size() > 0) {
			System.out.println("saveThresholdAlt<RoadTaxThreshold.size> => " + RoadTaxThreshold.size());
			model.addAttribute("RoadTaxThreshold", RoadTaxThreshold);
		}

		return "roadtax/pages/roadTaxThresholdAlt";
	}

	/* ------------------------------------------------------------------ */
	/* End - Float Threshold Alert */
	/*------------------------------------------------------------------- */

	/* ------------------------------------------------------------------ */
	/* start - Road Tax Email validation */
	/*------------------------------------------------------------------- */

	@RequestMapping(value = { "/validateRoadTaxEmail" }, method = RequestMethod.GET)
	@ResponseBody
	public String validateRoadTaxEmail(HttpServletRequest request, Model model, @RequestParam String entity,
			@RequestParam String email) throws Exception {

		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadTaxAdminLogin";
			return "admin-login";
		}

		String validateEmailFlag = "n";

		System.out.println("validateRoadTaxEmail<entity> => " + entity);
		System.out.println("validateRoadTaxEmail<emailInput> => " + email);

		List<RoadTaxThreshold> roadTaxThresholdList = new ArrayList<RoadTaxThreshold>();
		RoadTaxThresholdExample roadTaxThresholdExample = new RoadTaxThresholdExample();
		RoadTaxThresholdExample.Criteria emai_criteria = roadTaxThresholdExample.createCriteria();
		if (entity.equals("EIB")) {
			emai_criteria.andEntityEqualTo("EIB");
		} else {
			emai_criteria.andEntityEqualTo("ETB");
		}

		roadTaxThresholdList = roadTaxThresholdMapper.selectByExample(roadTaxThresholdExample);

		System.out.println("validateRoadTaxEmail<emailDB> => " + roadTaxThresholdList.get(0).getEmailTo());

		if (roadTaxThresholdList.size() > 0) {

			String[] emailToSplit = roadTaxThresholdList.get(0).getEmailTo().split(";");
			for (String mailTo : emailToSplit) {
				System.out.println("validateRoadTaxEmail<mailSplit> => " + mailTo);

				if (email.equalsIgnoreCase(mailTo)) {
					validateEmailFlag = "y";
				}
			}
			System.out.println("validateRoadTaxEmail<validateEmailFlag> => " + validateEmailFlag);
			model.addAttribute("validateEmailFlag", validateEmailFlag);

		}
		JSONObject json = new JSONObject();
		json.put("validateEmailFlag", validateEmailFlag);
		return json.toString();
	}
	/* ------------------------------------------------------------------ */
	/* End - Road Tax Email validation */
	/*------------------------------------------------------------------- */

	/* ------------------------------------------------------------------ */
	/* Start - Road Tax PDF download */
	/*------------------------------------------------------------------- */
	@RequestMapping(value = "/generatePDFTxnReport")
	public void generatePDFTxnReport(HttpServletRequest request, HttpServletResponse response, Model model)
			throws IOException {

		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadTaxAdminLogin";
		}

		String stateCoverage = "";
		String postOfficeName = "";
		String roadTaxStatus = "";
		String fileName = "";

		stateCoverage = (String) session.getAttribute("stateCoverage");
		System.out.println("generatePDFTxnReport<stateCoverage> => " + stateCoverage);

		// List of Txn Report
		List<RoadTaxTxnReport> roadTaxTxnReportList = new ArrayList<RoadTaxTxnReport>();
		if (session.getAttribute("roadTaxTxnReportList") != null
				|| !"".equals(session.getAttribute("roadTaxTxnReportList"))) {
			roadTaxTxnReportList = (ArrayList<RoadTaxTxnReport>) session.getAttribute("roadTaxTxnReportList");
			System.out.println("generatePDFTxnReport<size> => " + roadTaxTxnReportList.size());
		}

		roadTaxStatus = (String) session.getAttribute("roadTaxStatus");
		System.out.println("generatePDFTxnReport<roadTaxStatus> => " + roadTaxStatus);

		// File Names
		if ("Printed".equals(roadTaxStatus)) {
			fileName = "Successful.pdf";
		} else {
			fileName = "Unsuccessful.pdf";
		}

		// State Names
		if (stateCoverage == "Kuala Lumpur" || stateCoverage.equalsIgnoreCase("Kuala Lumpur")) {
			postOfficeName = "POS KL";
		} else {
			postOfficeName = "POS SHAH ALAM";
		}
		model.addAttribute("postOfficeName", postOfficeName);

		final ServletContext servletContext = request.getSession().getServletContext();
		final File tempDirectory = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
		final String temperotyFilePath = tempDirectory.getAbsolutePath();

		response.setContentType("application/pdf");
		response.setHeader("Content-disposition", "attachment; filename=" + fileName);

		/*
		 * try { // Passing the list RoadTaxPDFController.createPDF(temperotyFilePath +
		 * "\\" + fileName, postOfficeName, roadTaxTxnReportList, roadTaxStatus);
		 * ByteArrayOutputStream baos = new ByteArrayOutputStream(); baos =
		 * convertPDFToByteArrayOutputStream(temperotyFilePath + "\\" + fileName);
		 * OutputStream os = response.getOutputStream(); baos.writeTo(os); os.flush(); }
		 * catch (Exception e1) { e1.printStackTrace(); }
		 */

	}

	private ByteArrayOutputStream convertPDFToByteArrayOutputStream(String fileName) {

		InputStream inputStream = null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {

			inputStream = new FileInputStream(fileName);
			byte[] buffer = new byte[1024];
			baos = new ByteArrayOutputStream();

			int bytesRead;
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				baos.write(buffer, 0, bytesRead);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return baos;
	}
	/* ------------------------------------------------------------------ */
	/* End - Road Tax PDF download */
	/*------------------------------------------------------------------- */

	// get Menu from Rest service

	@SuppressWarnings("unchecked")
	private Map<String, List<Menu>> getMenu(String roleid, String moduleId) {
		System.out.println("roleid >>>" + roleid);
		System.out.println("moduleId >>>> " + moduleId);
		Menu m = new Menu();
		RestTemplate rt = new RestTemplate();
		MultiValueMap<String, Object> mapInput = new LinkedMultiValueMap<String, Object>();
		Map<String, List<Menu>> resp = new HashMap<String, List<Menu>>();
		// String roleid="6";
		String delimiters = "$";
		// String moduleId="1";
		try {
			resp = menuService.getMenu(roleid + delimiters + moduleId);
			System.out.println(resp);
			m.setModuleId(Integer.parseInt(moduleId));
			System.out.println("resp  " + resp + "\n");
			System.out.println("Module Id  : " + m.getModuleId());
			System.out.println("-------------------------------");
			List<Menu> firstList = resp.get("first");
			List<Menu> secondList = resp.get("second");
			List<Menu> thirdList = resp.get("third");
			List<Menu> fourthList = resp.get("fourth");

			resp.put("1", firstList);
			resp.put("2", secondList);
			resp.put("3", thirdList);
			resp.put("4", fourthList);

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("no Connection");
		}
		return resp;
	}

}
