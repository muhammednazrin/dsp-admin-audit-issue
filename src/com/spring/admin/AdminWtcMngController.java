package com.spring.admin;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.etiqa.utils.convertBytes;
import com.spring.VO.ApprovalLog;
import com.spring.VO.ApprovalLogExample;
import com.spring.VO.CommonTblUnderwritingQA;
import com.spring.VO.WTCProdMngPojo;
import com.spring.VO.WTCTblDometic;
import com.spring.VO.WTCTblDometicExample;
import com.spring.VO.WTCTblIntwithdays;
import com.spring.VO.WTCTblIntwithdaysExample;
import com.spring.VO.WTCTblParam;
import com.spring.VO.WTCTblParamExample;
import com.spring.mapper.ApprovalLogMapper;
import com.spring.mapper.ApprovalMapper;
import com.spring.mapper.CommonTblUnderwritingQAMapper;
import com.spring.mapper.WTCTblDometicMapper;
import com.spring.mapper.WTCTblIntwithdaysMapper;
import com.spring.mapper.WTCTblParamMapper;

@Controller
public class AdminWtcMngController {

	CommonTblUnderwritingQAMapper commonTblUnderwritingQAMapper;
	ApprovalMapper approvalMapper;
	ApprovalLogMapper approvalLogMapper;
	WTCTblParamMapper wTCTblParamMapper;
	WTCTblIntwithdaysMapper wTCTblIntwithdaysMapper;
	WTCTblDometicMapper wTCTblDometicMapper;

	@Autowired
	public AdminWtcMngController(CommonTblUnderwritingQAMapper commonTblUnderwritingQAMapper,
			ApprovalMapper approvalMapper, ApprovalLogMapper approvalLogMapper, WTCTblParamMapper wTCTblParamMapper,
			WTCTblIntwithdaysMapper wTCTblIntwithdaysMapper, WTCTblDometicMapper wTCTblDometicMapper) {
		this.commonTblUnderwritingQAMapper = commonTblUnderwritingQAMapper;
		this.approvalMapper = approvalMapper;
		this.approvalLogMapper = approvalLogMapper;
		this.wTCTblParamMapper = wTCTblParamMapper;
		this.wTCTblIntwithdaysMapper = wTCTblIntwithdaysMapper;
		this.wTCTblDometicMapper = wTCTblDometicMapper;

	}

	// ---------------------------------------------------------------Start By
	// Arbind UNDERWRITTING QUESTION WTC
	// ------------------------------------------------------

	// Display Original and Change Data Functionality

	@RequestMapping(value = "/approvalWTCRulesManagement", method = RequestMethod.GET)
	public String approvalWTCRulesManagement(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Reequest " + request.getParameter("id"));
		ApprovalLogExample approvalexample = new ApprovalLogExample();
		com.spring.VO.ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();
		Integer testint = new Integer(request.getParameter("id"));
		createCriteria.andIdEqualTo(testint.shortValue());
		List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
		System.out.println("   list size in approval list  :::   " + listapprovallog.size());
		// System.out.println("Testing Original Content
		// "+listapprovallog.get(0).getOriginalContent());

		try {
			// original data
			if (listapprovallog.size() > 0) {
				convertBytes process = new convertBytes();
				Object originalData = process.convertFromBytes(listapprovallog.get(0).getOriginalContent());
				List<String> listOriginalData = new ArrayList<String>();
				// objecct convert into AgentProdMap
				if (originalData != null) {
					listOriginalData = (List<String>) originalData;
					System.out.println(" Original Data List Size ::::::         " + listOriginalData.size());

					CommonTblUnderwritingQA qarec = new CommonTblUnderwritingQA();

					qarec.setQuestionDescription(listOriginalData.get(0));
					qarec.setQuestionDescriptionBm(listOriginalData.get(1));
					qarec.setCorrectAnswerCode(listOriginalData.get(2));

					System.out.println("Original Desce" + qarec.getQuestionDescription());
					System.out.println("Original DesceBM" + qarec.getQuestionDescriptionBm());
					System.out.println("Original Code" + qarec.getCorrectAnswerCode());

					List<CommonTblUnderwritingQA> listOriginalMIRules = new ArrayList<CommonTblUnderwritingQA>();
					listOriginalMIRules.add(qarec);
					model.addAttribute("listOriginalWTCRules", listOriginalMIRules);
					System.out.println("-----------------------------");
				} // inner if
			} // outer if

			// Change Rules Data for MI
			if (listapprovallog.size() > 0) {
				convertBytes process = new convertBytes();

				Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
				List<String> listChangeData = new ArrayList<String>();
				// objecct convert into AgentProdMap
				if (changeData != null) {
					listChangeData = (List<String>) changeData;
					System.out.println("ChageData List Size  ::::::         " + listChangeData.size());
					CommonTblUnderwritingQA qarec = new CommonTblUnderwritingQA();

					qarec.setId(Short.parseShort(listChangeData.get(0)));
					qarec.setQuestionDescription(listChangeData.get(1));
					qarec.setQuestionDescriptionBm(listChangeData.get(2));
					qarec.setCorrectAnswerCode(listChangeData.get(3));
					List<CommonTblUnderwritingQA> listChangeDataMIRules = new ArrayList<CommonTblUnderwritingQA>();

					System.out.println(" New ID >>> " + qarec.getId());
					System.out.println("New  Desce" + qarec.getQuestionDescription());
					System.out.println("New DesceBM" + qarec.getQuestionDescriptionBm());
					System.out.println("New  Code" + qarec.getCorrectAnswerCode());

					listChangeDataMIRules.add(qarec);
					model.addAttribute("listChangeDataWTCRules", listChangeDataMIRules);
					System.out.println("-----------------------------");
				}
			}
		} // try end
		catch (Exception e) {
			System.out.println("Exception in Convert Object to Byte" + e);
		}

		return "agent-product-approval";
	}

	@RequestMapping(value = "/approveWTCRuleManagementChange")
	public String approveWTCRuleManagementChange(BindingResult result, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		System.out.println("RulesList funtion ");
		HttpSession session = request.getSession();
		ApprovalLog alog = new ApprovalLog();
		try {
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			// *************Update the new change Rule management in originala table
			// ********************

			String aid = request.getParameter("approvalLogId");
			System.out.println("aid >>>>>" + aid);
			ApprovalLogExample approvalexample = new ApprovalLogExample();
			ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();

			if (null != aid && !aid.isEmpty()) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
				System.out.println("   list size in approval list  :::   " + listapprovallog.size());
				// System.out.println("Testing Original Content
				// "+listapprovallog.get(0).getOriginalContent());

				// Change Data
				if (listapprovallog.size() > 0) {
					String status = listapprovallog.get(0).getStatus();
					short alogId = listapprovallog.get(0).getId();
					System.out.println("Status  :  " + status);
					System.out.println("Approvallog Id  :  " + alogId);

					convertBytes process = new convertBytes();
					Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
					List<String> listChangeData = new ArrayList<String>();
					// objecct convert into AgentProdMap
					if (changeData != null) {
						listChangeData = (List<String>) changeData;
						System.out.println("  list agent prod map size ::::::         " + listChangeData.size());

						CommonTblUnderwritingQA qarec = new CommonTblUnderwritingQA();

						qarec.setQuestionDescription(listChangeData.get(1));
						qarec.setQuestionDescriptionBm(listChangeData.get(2));
						qarec.setCorrectAnswerCode(listChangeData.get(3));
						qarec.setId(Short.parseShort(listChangeData.get(0)));

						// *************Update the new change Rule management in originala table
						// ********************
						commonTblUnderwritingQAMapper.updateByPrimaryKeySelective(qarec);

						// Update status in DSP_AFM_TBL_Approval_Log table
						alog.setId(alogId);
						alog.setStatus("3");
						alog.setChecker((short) 1); // get from the current login session
						alog.setUpdateDate(new Date());
						approvalLogMapper.updateByPrimaryKeySelective(alog);
						// model.addAttribute("nvicUpdatedMessage", " Data Updated Successfully!");

					}
				}
			}

		} // try end

		catch (Exception e) {
			System.out.println("Exception Update new Rule Management " + e);
		}

		return "redirect:/getApprovalList";

	}

	// Reject Action

	@RequestMapping(value = "/rejectWTCRuleManagementChange", method = RequestMethod.GET)
	public String rejectWTCRuleManagementChange(HttpServletRequest request, HttpServletResponse response, Model model,
			BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		ApprovalLogExample approvalLogexample = new ApprovalLogExample();
		ApprovalLog alog = new ApprovalLog();
		ApprovalLogExample.Criteria createCriteria = approvalLogexample.createCriteria();
		String aid = request.getParameter("approvalLogId");
		if (null != aid && !aid.isEmpty()) {
			Integer testint = new Integer(aid);
			createCriteria.andIdEqualTo(testint.shortValue());
			List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalLogexample);
			System.out.println("   list size in approval list  :::   " + listapprovallog.size());

			short alogId = listapprovallog.get(0).getId();
			alog.setId(alogId);
			alog.setStatus("2");
			alog.setChecker((short) 1); // get from the current login session
			alog.setUpdateDate(new Date());
			approvalLogMapper.updateByPrimaryKeySelective(alog);
		}

		return "redirect:/getApprovalList";
	}

	// ---------------------------------------------------------------END By Arbind
	// UNDERWRITTING QUESTION
	// WTC------------------------------------------------------

	// ---------------------------------------------------------------Start By
	// Arbind Product Rate Change Approval and Reject
	// WTC------------------------------------------------------

	// Display Original and Change Data Functionality

	@RequestMapping(value = "/approvalWTCProductrate", method = RequestMethod.GET)
	public String approvalWTCProductrate(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Reequest " + request.getParameter("id"));
		ApprovalLogExample approvalexample = new ApprovalLogExample();
		com.spring.VO.ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();
		Integer testint = new Integer(request.getParameter("id"));
		createCriteria.andIdEqualTo(testint.shortValue());
		List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
		System.out.println("   list size in approval list  :::   " + listapprovallog.size());
		// System.out.println("Testing Original Content
		// "+listapprovallog.get(0).getOriginalContent());

		try {
			// original data
			if (listapprovallog.size() > 0) {

				System.out.println("  called ..   " + listapprovallog.get(0).getNote());

				if (listapprovallog.get(0).getNote().equalsIgnoreCase("Y")) {
					System.out.println("  called here...");
					convertBytes process = new convertBytes();
					Object originalData = process.convertFromBytes(listapprovallog.get(0).getOriginalContent());
					List<WTCTblParam> listOriginalData = new ArrayList<WTCTblParam>();

					// objecct convert into AgentProdMap
					if (originalData != null) {
						listOriginalData = (List<WTCTblParam>) originalData;
						System.out.println(" Original Data List Size ::::::         " + listOriginalData.size());
						for (int t = 0; t < listOriginalData.size(); t++) {
							System.out.println("Original Product info" + listOriginalData.get(t).getParamType());
							System.out.println("Original Product info" + listOriginalData.get(t).getParamDesc());
							System.out.println("Original Product info" + listOriginalData.get(t).getParamDescMy());
						}

						WTCProdMngPojo wtctblparam = new WTCProdMngPojo();
						wtctblparam.setWtcDiscount(listOriginalData.get(0).getParamCode());
						wtctblparam.setWtcGst(listOriginalData.get(1).getParamCode());
						wtctblparam.setWtcStampDuty(listOriginalData.get(2).getParamCode());

						List<WTCProdMngPojo> listOriginalWTCRules = new ArrayList<WTCProdMngPojo>();
						listOriginalWTCRules.add(wtctblparam);
						model.addAttribute("listOriginalWTCProductRate", listOriginalWTCRules);
						System.out.println("-----------------------------");
					}

				} else {
					System.out.println("   travel type  :::::::::         " + listapprovallog.get(0).getNote());

					if (listapprovallog.get(0).getNote().equalsIgnoreCase("INT")) {
						convertBytes process = new convertBytes();
						Object originalData = process.convertFromBytes(listapprovallog.get(0).getOriginalContent());

						List<WTCTblIntwithdays> listOriginalData_ = (List<WTCTblIntwithdays>) originalData;
						System.out.println("Original List Size &&&&&&&&&        " + listOriginalData_.size());

						for (int t = 0; t < listOriginalData_.size(); t++) {

							System.out.println(
									"Original Product Rate  1   " + listOriginalData_.get(t).getDaysRangeKeycode());
							System.out.println(
									"Original Product Rate  1   " + listOriginalData_.get(t).getPremiumValue());
							System.out.println("Original Product Rate  1   " + listOriginalData_.get(t).getPlanCode());

						}
						model.addAttribute("listOriginalWTCProductRate", listOriginalData_);
					} else if (listapprovallog.get(0).getNote().equalsIgnoreCase("D")) {

						convertBytes process = new convertBytes();
						Object originalData = process.convertFromBytes(listapprovallog.get(0).getOriginalContent());

						List<WTCTblDometic> listOriginalData_ = (List<WTCTblDometic>) originalData;
						System.out.println("Original List Size &&&&&&&&&        " + listOriginalData_.size());

						for (int t = 0; t < listOriginalData_.size(); t++) {

							System.out.println(
									"Original Product Rate  1   " + listOriginalData_.get(t).getDaysRangeKeycode());
							System.out.println(
									"Original Product Rate  1   " + listOriginalData_.get(t).getPremiumValue());
							System.out.println("Original Product Rate  1   " + listOriginalData_.get(t).getPlanCode());

						}
						model.addAttribute("listOriginalWTCProductRate", listOriginalData_);

					}
				}
				// inner if
			} // outer if

			// Change Rules Data for MI
			if (listapprovallog.size() > 0) {

				if (listapprovallog.get(0).getNote().equalsIgnoreCase("Y")) {
					convertBytes process = new convertBytes();
					Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
					List<WTCTblParam> listChangeData = new ArrayList<WTCTblParam>();

					// objecct convert into AgentProdMap
					if (changeData != null) {
						listChangeData = (List<WTCTblParam>) changeData;
						System.out.println("ChageData List Size ------------      " + listChangeData.size());

						for (int t = 0; t < listChangeData.size(); t++) {

							System.out.println("Change Product info" + listChangeData.get(t).getParamType());
							System.out.println("Change Product info" + listChangeData.get(t).getParamDesc());
							System.out.println("Change Product info" + listChangeData.get(t).getParamDescMy());

						}

						WTCProdMngPojo wtctblparam = new WTCProdMngPojo();
						wtctblparam.setWtcDiscount(listChangeData.get(2).getParamCode());
						wtctblparam.setWtcGst(listChangeData.get(1).getParamCode());
						wtctblparam.setWtcStampDuty(listChangeData.get(0).getParamCode());

						List<WTCProdMngPojo> listChangeWTCRules = new ArrayList<WTCProdMngPojo>();
						listChangeWTCRules.add(wtctblparam);
						model.addAttribute("listChangeWTCProductRate", listChangeWTCRules);
						model.addAttribute("wtcprodtype", "0");
						System.out.println("-----------------------------");
					}

				} else {
					convertBytes process = new convertBytes();
					Object listChangeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());

					List<WTCProdMngPojo> listChangeData_ = (List<WTCProdMngPojo>) listChangeData;
					System.out.println("ChageData List Size  ##########       " + listChangeData_.size());

					for (int t = 0; t < listChangeData_.size(); t++) {

						System.out.println("Change Product Rate  2   " + listChangeData_.get(t).getWtcRecIdDomestic());
						System.out.println(
								"Change Product Rate  2   " + listChangeData_.get(t).getWtcdaysrangekeycodeDomestic());
						System.out.println(
								"Change Product Rate  2   " + listChangeData_.get(t).getWtcpremiumvalDomestic());

					}

					/*
					 * WTCProdMngPojo wtctblparam = new WTCProdMngPojo();
					 * wtctblparam.setWtcdaysrangekeycodeDomestic(listChangeData_.get(2).
					 * getWtcdaysrangekeycodeDomestic());
					 * wtctblparam.setWtcdestinationcodeareaDomestic(listChangeData_.get(2).
					 * getWtcdestinationcodeareaDomestic());
					 * wtctblparam.setWtcpackagecodeDomestic(listChangeData_.get(2).
					 * getWtcpackagecodeDomestic());
					 * wtctblparam.setWtcplancodeDomestic(listChangeData_.get(2).
					 * getWtcplancodeDomestic());
					 * wtctblparam.setWtcpremiumvalDomestic(listChangeData_.get(2).
					 * getWtcpremiumvalDomestic());
					 * wtctblparam.setwtc(listChangeData_.get(2).getWtcpackagecodeDomestic());
					 */
					// List<WTCProdMngPojo> listChangeWTCRules = new ArrayList<WTCProdMngPojo>();
					// listChangeWTCRules.add(wtctblparam);
					model.addAttribute("listChangeWTCProductRate", listChangeData_);
					model.addAttribute("wtcprodtype", listChangeData_.get(0).getWtcProdType());

				}
			}
		} // try end
		catch (Exception e) {
			System.out.println("Exception in Convert Object to Byte" + e);
		}

		return "agent-product-approval";
	}

	// Display Original and Change Data Functionality
	// Show WTC Product Rate Change Approval Function

	@RequestMapping(value = "/approvalWTCProductrateChange", method = RequestMethod.GET)
	public String approvalWTCProductratechange(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		try {
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			// *************Update the new change in Product Rate in originala table
			// ********************

			String aid = request.getParameter("approvalLogId");
			System.out.println("aid >>>>>" + aid);
			ApprovalLogExample approvalexample = new ApprovalLogExample();
			ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();

			if (null != aid && !aid.isEmpty()) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
				System.out.println("   list size in approval list  :::   " + listapprovallog.size());
				// System.out.println("Testing Original Content
				// "+listapprovallog.get(0).getOriginalContent());

				// Change Data
				if (listapprovallog.size() > 0) {
					String status = listapprovallog.get(0).getStatus();
					short alogId = listapprovallog.get(0).getId();
					System.out.println("Status  :  " + status);
					System.out.println("Approvallog Id  :  " + alogId);
					convertBytes process = new convertBytes();

					if (listapprovallog.get(0).getNote().equalsIgnoreCase("Y")) {

						System.out.println("Product Change Data updated .............   " + alogId);
						if (null != aid && !aid.isEmpty()) {
							testint = new Integer(aid);
							createCriteria.andIdEqualTo(testint.shortValue());
							listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
							System.out.println("   list size in approval list  :::   " + listapprovallog.size());
							// System.out.println("Testing Original Content
							// "+listapprovallog.get(0).getOriginalContent());
							// Change Data
							if (listapprovallog.size() > 0) {
								status = listapprovallog.get(0).getStatus();
								alogId = listapprovallog.get(0).getId();
								System.out.println("Status  :  " + status);
								System.out.println("Approvallog Id  :  " + alogId);

								process = new convertBytes();
								Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
								List<WTCTblParam> listChangeDataWTC = new ArrayList<WTCTblParam>();
								// objecct convert into AgentProdMap
								if (changeData != null) {
									listChangeDataWTC = (List<WTCTblParam>) changeData;
									System.out.println(
											"  list agent prod map size ::::::         " + listChangeDataWTC.size());

									for (int t = 0; t < listChangeDataWTC.size(); t++) {

										if (listChangeDataWTC.get(t).getParamType().equalsIgnoreCase("WTC_Discount")) {

											// Discount
											WTCTblParamExample wtcTblParamExample = new WTCTblParamExample();
											WTCTblParam wtcTblParam = new WTCTblParam();
											WTCTblParamExample.Criteria wtcParam_criteria = wtcTblParamExample
													.createCriteria();
											wtcParam_criteria.andParamTypeEqualTo("WTC_Discount");
											List<WTCTblParam> listA = wTCTblParamMapper
													.selectByExample(wtcTblParamExample);
											String wtcparam_pk = listA.get(0).getId().toString();

											WTCTblParam wtcparam = new WTCTblParam();
											wtcparam.setId(listChangeDataWTC.get(t).getId());
											wtcparam.setParamType(listChangeDataWTC.get(t).getParamType());
											String quotVal = listChangeDataWTC.get(t).getParamCode();
											wtcparam.setParamCode(quotVal == null ? "0" : quotVal);
											// if(listChangeDataWTC.get(t).getParamDesc().contains("RM")){
											// listChangeDataWTC.get(t).getParamDesc().replace("RM", "").trim();
											// }
											wtcparam.setParamDesc(listChangeDataWTC.get(t).getParamDesc());
											wtcparam.setParamDescMy(listChangeDataWTC.get(t).getParamDescMy());
											wtcparam.setId(Short.valueOf(wtcparam_pk));

											// *************Update the new change Product Info in originala table
											// ********************
											wTCTblParamMapper.updateByPrimaryKeySelective(wtcparam);

										} else if (listChangeDataWTC.get(t).getParamType()
												.equalsIgnoreCase("WTC_Gst")) {

											// GST
											WTCTblParamExample wtcTblParamExample = new WTCTblParamExample();
											WTCTblParam wtcTblParam = new WTCTblParam();
											WTCTblParamExample.Criteria wtcParam_criteria = wtcTblParamExample
													.createCriteria();
											wtcParam_criteria.andParamTypeEqualTo("WTC_Gst");
											List<WTCTblParam> listA = wTCTblParamMapper
													.selectByExample(wtcTblParamExample);
											String wtcparam_pk = listA.get(0).getId().toString();

											WTCTblParam wtcparam = new WTCTblParam();
											wtcparam.setId(listChangeDataWTC.get(t).getId());
											wtcparam.setParamType(listChangeDataWTC.get(t).getParamType());
											String salesTargetVal = listChangeDataWTC.get(t).getParamCode();
											wtcparam.setParamCode(salesTargetVal == null ? "0" : salesTargetVal);
											wtcparam.setParamDesc(listChangeDataWTC.get(t).getParamDesc());
											wtcparam.setParamDescMy("RM" + listChangeDataWTC.get(t).getParamDescMy());
											wtcparam.setId(Short.valueOf(wtcparam_pk));

											// *************Update the new change Product Info in originala table
											// ********************
											wTCTblParamMapper.updateByPrimaryKeySelective(wtcparam);

										} else if (listChangeDataWTC.get(t).getParamType()
												.equalsIgnoreCase("WTC_StampDuty")) {

											// Stamp Duty
											WTCTblParamExample wtcTblParamExample = new WTCTblParamExample();
											WTCTblParam wtcTblParam = new WTCTblParam();
											WTCTblParamExample.Criteria wtcParam_criteria = wtcTblParamExample
													.createCriteria();
											wtcParam_criteria.andParamTypeEqualTo("WTC_StampDuty");
											List<WTCTblParam> listA = wTCTblParamMapper
													.selectByExample(wtcTblParamExample);
											String wtcparam_pk = listA.get(0).getId().toString();

											WTCTblParam wtcparam = new WTCTblParam();
											wtcparam.setId(listChangeDataWTC.get(t).getId());
											wtcparam.setParamType(listChangeDataWTC.get(t).getParamType());
											String salesTargetVal = listChangeDataWTC.get(t).getParamCode();
											wtcparam.setParamCode(salesTargetVal == null ? "0" : salesTargetVal);
											wtcparam.setParamDesc(listChangeDataWTC.get(t).getParamDesc());
											wtcparam.setParamDescMy("RM" + listChangeDataWTC.get(t).getParamDescMy());
											wtcparam.setId(Short.valueOf(wtcparam_pk));

											// *************Update the new change Product Info in originala table
											// ********************
											wTCTblParamMapper.updateByPrimaryKeySelective(wtcparam);

										}

									}

									ApprovalLogExample approvalLogexample = new ApprovalLogExample();
									alog = new ApprovalLog();
									createCriteria = approvalLogexample.createCriteria();
									aid = request.getParameter("approvalLogId");
									if (null != aid && !aid.isEmpty()) {
										testint = new Integer(aid);
										createCriteria.andIdEqualTo(testint.shortValue());
										listapprovallog = approvalLogMapper
												.selectByExampleWithBLOBs(approvalLogexample);
										System.out.println(
												"   list size in approval list  :::   " + listapprovallog.size());

										alogId = listapprovallog.get(0).getId();
										alog.setId(alogId);
										alog.setStatus("3");
										alog.setChecker(Short.parseShort(loginUser)); // get from the current login
																						// session
										alog.setUpdateDate(new Date());
										approvalLogMapper.updateByPrimaryKeySelective(alog);
									}

								}
							}

						} // try end

					} else {

						Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
						List<WTCProdMngPojo> listChangeData = new ArrayList<WTCProdMngPojo>();

						// objecct convert into AgentProdMap
						if (changeData != null) {

							listChangeData = (List<WTCProdMngPojo>) changeData;
							System.out.println("  list agent prod map size ::::::         " + listChangeData.size());

							if (listChangeData.get(0).getWtcProdType().equalsIgnoreCase("INT")) {
								// International
								Object originalD = process
										.convertFromBytes(listapprovallog.get(0).getOriginalContent());
								List<WTCTblIntwithdays> listOrgData = new ArrayList<WTCTblIntwithdays>();
								listOrgData = (List<WTCTblIntwithdays>) originalD;

								String wtcintwithdays_pk = listOrgData.get(0).getId().toString();

								WTCTblIntwithdaysExample wTCTblIntwithdaysExample = new WTCTblIntwithdaysExample();
								WTCTblIntwithdaysExample.Criteria wTCTblIntwithdaysExample_criteria = wTCTblIntwithdaysExample
										.createCriteria();
								wTCTblIntwithdaysExample_criteria.andIdEqualTo(Short.valueOf(wtcintwithdays_pk));

								WTCTblIntwithdays wTCTblIntwithdays = new WTCTblIntwithdays();
								wTCTblIntwithdays.setDaysRangeKeycode(
										listChangeData.get(0).getWtcdaysrangekeycodeInternational());
								wTCTblIntwithdays.setDestinationCodeArea(
										listChangeData.get(0).getWtcdestinationcodeareaInternational());
								wTCTblIntwithdays
										.setPackageCode(listChangeData.get(0).getWtcpackagecodeInternational());
								wTCTblIntwithdays.setPlanCode(listChangeData.get(0).getWtcplancodeInternational());
								BigDecimal premiumval = new BigDecimal(
										listChangeData.get(0).getWtcpremiumvalInternational());
								wTCTblIntwithdays.setPremiumValue(premiumval);
								wTCTblIntwithdays.setId(Short.valueOf(wtcintwithdays_pk));
								wTCTblIntwithdays.setTravelwithTypeId(
										listChangeData.get(0).getWtctravelwithtypeidInternational());

								// *************Update the new change Product Info in originala table
								// ********************
								wTCTblIntwithdaysMapper.updateByPrimaryKeySelective(wTCTblIntwithdays);
								System.out.println("International Data updated .............   " + alogId);

							} else if (listChangeData.get(0).getWtcProdType().equalsIgnoreCase("D")) {
								// Domestic

								System.out.println("  domestic.....");

								Object originalD = process
										.convertFromBytes(listapprovallog.get(0).getOriginalContent());
								List<WTCTblDometic> listOrgData = new ArrayList<WTCTblDometic>();
								listOrgData = (List<WTCTblDometic>) originalD;

								System.out.println("  domestic.....1111");
								String wtcdomestic_pk = listOrgData.get(0).getId().toString();

								WTCTblDometicExample wTCTblDometicExample = new WTCTblDometicExample();
								WTCTblDometicExample.Criteria wTCTblDometicExample_criteria = wTCTblDometicExample
										.createCriteria();
								wTCTblDometicExample_criteria.andIdEqualTo(Short.valueOf(wtcdomestic_pk));

								System.out.println("  domestic.....222222");

								WTCTblDometic wTCTblDometic = new WTCTblDometic();
								wTCTblDometic
										.setDaysRangeKeycode(listChangeData.get(0).getWtcdaysrangekeycodeDomestic());
								wTCTblDometic.setDestinationCodeArea(
										listChangeData.get(0).getWtcdestinationcodeareaDomestic());
								wTCTblDometic.setPackageCode(listChangeData.get(0).getWtcpackagecodeDomestic());
								wTCTblDometic.setPlanCode(listChangeData.get(0).getWtcplancodeDomestic());
								BigDecimal premiumval = new BigDecimal(
										listChangeData.get(0).getWtcpremiumvalDomestic());
								wTCTblDometic.setPremiumValue(premiumval);
								wTCTblDometic.setId(Short.valueOf(wtcdomestic_pk));
								wTCTblDometic
										.setTravelwithTypeId(listChangeData.get(0).getWtctravelwithtypeidDomestic());

								System.out.println("  domestic.....33333");

								// *************Update the new change Product Info in originala table
								// ********************
								wTCTblDometicMapper.updateByPrimaryKeySelective(wTCTblDometic);
								System.out.println("Domestic Data updated .............   " + alogId);

							} else {

							}

							ApprovalLogExample approvalLogexample = new ApprovalLogExample();
							alog = new ApprovalLog();
							createCriteria = approvalLogexample.createCriteria();
							aid = request.getParameter("approvalLogId");
							if (null != aid && !aid.isEmpty()) {
								testint = new Integer(aid);
								createCriteria.andIdEqualTo(testint.shortValue());
								listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalLogexample);
								System.out.println("   list size in approval list  :::   " + listapprovallog.size());

								alogId = listapprovallog.get(0).getId();
								alog.setId(alogId);
								alog.setStatus("3");
								alog.setChecker(Short.parseShort(loginUser)); // get from the current login session
								alog.setUpdateDate(new Date());
								approvalLogMapper.updateByPrimaryKeySelective(alog);
							}

						}
					}
				}

			}

		} // try end

		catch (Exception e) {
			System.out.println("Exception Update new Rule Management " + e);
		}

		return "redirect:/getApprovalList";
	}

	// Display Original and Change Data Functionality
	// Show WTC Product Rate Change Reject Function

	@RequestMapping(value = "/rejectWTCProductrateChange", method = RequestMethod.GET)
	public String rejectWTCProductratechange(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		try {
			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			ApprovalLogExample approvalLogexample = new ApprovalLogExample();
			alog = new ApprovalLog();
			ApprovalLogExample.Criteria createCriteria = approvalLogexample.createCriteria();
			String aid = request.getParameter("approvalLogId");
			if (null != aid && !aid.isEmpty()) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalLogexample);
				System.out.println("   list size in approval list  :::   " + listapprovallog.size());

				Short alogId = listapprovallog.get(0).getId();
				alog.setId(alogId);
				alog.setStatus("2");
				alog.setChecker(Short.parseShort(loginUser)); // get from the current login session
				alog.setUpdateDate(new Date());
				approvalLogMapper.updateByPrimaryKeySelective(alog);
			}
		} // try end
		catch (Exception e) {
			System.out.println("Exception in Convert Object to Byte" + e);
		}

		return "redirect:/getApprovalList";
	}

	// ---------------------------------------------------------------End By Arbind
	// Product Rate Change Approval and Reject
	// WTC------------------------------------------------------

	// ---------------------------------------------------------------Start By
	// Arbind Product Info Change Approval and Reject
	// WTC------------------------------------------------------

	// Display Original and Change Data Functionality

	@RequestMapping(value = "/approvalWTCProductInfo", method = RequestMethod.GET)
	public String approvalWTCProductInfo(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Reequest id  for product info ::: " + request.getParameter("id"));
		ApprovalLogExample approvalexample = new ApprovalLogExample();
		com.spring.VO.ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();
		Integer testint = new Integer(request.getParameter("id"));
		createCriteria.andIdEqualTo(testint.shortValue());
		List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
		System.out.println("   list size in approval list  :::   " + listapprovallog.size());
		// System.out.println("Testing Original Content
		// "+listapprovallog.get(0).getOriginalContent());

		try {
			// original data
			if (listapprovallog.size() > 0) {
				convertBytes process = new convertBytes();
				Object originalData = process.convertFromBytes(listapprovallog.get(0).getOriginalContent());
				List<WTCTblParam> listOriginalData = new ArrayList<WTCTblParam>();
				// objecct convert into AgentProdMap
				if (originalData != null) {
					listOriginalData = (List<WTCTblParam>) originalData;
					System.out.println(" Original Data List Size ::::::         " + listOriginalData.size());
					for (int t = 0; t < listOriginalData.size(); t++) {
						System.out.println("Original Product info" + listOriginalData.get(t).getParamType());
						System.out.println("Original Product info" + listOriginalData.get(t).getParamDesc());
						System.out.println("Original Product info" + listOriginalData.get(t).getParamDescMy());
					}

					WTCProdMngPojo wtctblparam = new WTCProdMngPojo();
					wtctblparam.setAnnualTarget(listOriginalData.get(0).getParamCode());
					wtctblparam.setValidity(listOriginalData.get(1).getParamCode());

					List<WTCProdMngPojo> listOriginalWTCRules = new ArrayList<WTCProdMngPojo>();
					listOriginalWTCRules.add(wtctblparam);
					model.addAttribute("listOriginalWTCProductInfo", listOriginalWTCRules);
					System.out.println("-----------------------------");
				} // inner if
			} // outer if

			// Change Rules Data for MI
			if (listapprovallog.size() > 0) {
				convertBytes process = new convertBytes();

				Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
				List<WTCTblParam> listChangeData = new ArrayList<WTCTblParam>();
				// objecct convert into AgentProdMap
				if (changeData != null) {
					listChangeData = (List<WTCTblParam>) changeData;
					System.out.println("ChageData List Size  ::::::         " + listChangeData.size());

					for (int t = 0; t < listChangeData.size(); t++) {

						System.out.println("Change Product info" + listChangeData.get(t).getParamType());
						System.out.println("Change Product info" + listChangeData.get(t).getParamDesc());
						System.out.println("Change Product info" + listChangeData.get(t).getParamDescMy());

					}

					WTCProdMngPojo wtctblparam = new WTCProdMngPojo();
					wtctblparam.setAnnualTarget(listChangeData.get(0).getParamCode());
					wtctblparam.setValidity(listChangeData.get(1).getParamCode());

					List<WTCProdMngPojo> listChangeWTCRules = new ArrayList<WTCProdMngPojo>();
					listChangeWTCRules.add(wtctblparam);
					model.addAttribute("listChangeWTCProductInfo", listChangeWTCRules);
					System.out.println("-----------------------------");
				}
			}
		} // try end
		catch (Exception e) {
			System.out.println("Exception in Convert Object to Byte" + e);
		}

		return "agent-product-approval";
	}

	// Show WTC Product Info Approval Function

	@RequestMapping(value = "/approvalWTCProductInfoChange", method = RequestMethod.GET)
	public String approvalWTCProductInfoChange(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		try {
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			// *************Update the new change Rule management in originala table
			// ********************

			String aid = request.getParameter("approvalLogId");
			System.out.println("aid >>>>>" + aid);
			ApprovalLogExample approvalexample = new ApprovalLogExample();
			ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();

			if (null != aid && !aid.isEmpty()) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
				System.out.println("   list size in approval list  :::   " + listapprovallog.size());
				// System.out.println("Testing Original Content
				// "+listapprovallog.get(0).getOriginalContent());

				// Change Data
				if (listapprovallog.size() > 0) {
					String status = listapprovallog.get(0).getStatus();
					short alogId = listapprovallog.get(0).getId();
					System.out.println("Status  :  " + status);
					System.out.println("Approvallog Id  :  " + alogId);

					convertBytes process = new convertBytes();
					Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
					List<WTCTblParam> listChangeData = new ArrayList<WTCTblParam>();
					// objecct convert into AgentProdMap
					if (changeData != null) {
						listChangeData = (List<WTCTblParam>) changeData;
						System.out.println("  list agent prod map size ::::::         " + listChangeData.size());

						for (int t = 0; t < listChangeData.size(); t++) {

							if (listChangeData.get(t).getParamType().equalsIgnoreCase("WTC_quote_validity")) {
								// Quotaion Validity

								WTCTblParamExample wtcTblParamExample = new WTCTblParamExample();
								WTCTblParam wtcTblParam = new WTCTblParam();
								WTCTblParamExample.Criteria wtcParam_criteria = wtcTblParamExample.createCriteria();
								wtcParam_criteria.andParamTypeEqualTo("WTC_quote_validity");

								WTCTblParam wtcparam = new WTCTblParam();
								wtcparam.setId(listChangeData.get(t).getId());
								wtcparam.setParamType(listChangeData.get(t).getParamType());
								String quotVal = listChangeData.get(t).getParamCode();
								wtcparam.setParamCode(quotVal == null ? "0" : quotVal);
								wtcparam.setParamDesc(listChangeData.get(t).getParamDesc());
								wtcparam.setParamDescMy(listChangeData.get(t).getParamDescMy());

								// *************Update the new change Product Info in originala table
								// ********************
								wTCTblParamMapper.updateByExample(wtcparam, wtcTblParamExample);

							} else if (listChangeData.get(t).getParamType()
									.equalsIgnoreCase("WTC_annual_sales_target")) {

								// Sales Target
								WTCTblParamExample wtcTblParamExample = new WTCTblParamExample();
								WTCTblParam wtcTblParam = new WTCTblParam();
								WTCTblParamExample.Criteria wtcParam_criteria = wtcTblParamExample.createCriteria();
								wtcParam_criteria.andParamTypeEqualTo("WTC_annual_sales_target");

								WTCTblParam wtcparam = new WTCTblParam();
								wtcparam.setId(listChangeData.get(t).getId());
								wtcparam.setParamType(listChangeData.get(t).getParamType());
								String salesTargetVal = listChangeData.get(t).getParamCode();
								wtcparam.setParamCode(salesTargetVal == null ? "0" : salesTargetVal);
								wtcparam.setParamDesc("RM" + listChangeData.get(t).getParamDesc());
								wtcparam.setParamDescMy("RM" + listChangeData.get(t).getParamDescMy());

								// *************Update the new change Product Info in originala table
								// ********************
								wTCTblParamMapper.updateByExample(wtcparam, wtcTblParamExample);

							}

						}

						ApprovalLogExample approvalLogexample = new ApprovalLogExample();
						ApprovalLog alog = new ApprovalLog();
						createCriteria = approvalLogexample.createCriteria();
						aid = request.getParameter("approvalLogId");
						if (null != aid && !aid.isEmpty()) {
							testint = new Integer(aid);
							createCriteria.andIdEqualTo(testint.shortValue());
							listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalLogexample);
							System.out.println("   list size in approval list  :::   " + listapprovallog.size());

							alogId = listapprovallog.get(0).getId();
							alog.setId(alogId);
							alog.setStatus("3");
							alog.setChecker(Short.parseShort(loginUser)); // get from the current login session
							alog.setUpdateDate(new Date());
							approvalLogMapper.updateByPrimaryKeySelective(alog);
						}

					}
				}
			}

		} // try end

		catch (Exception e) {
			System.out.println("Exception Update new Rule Management " + e);
		}

		return "redirect:/getApprovalList";
	}

	// WTC Product Info Reject Function

	@RequestMapping(value = "/rejectWTCProductInfoChange", method = RequestMethod.GET)
	public String rejectWTCProductInfoChange(HttpServletRequest request, HttpServletResponse response, Model model,
			BindingResult result) {

		try {

			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			ApprovalLogExample approvalLogexample = new ApprovalLogExample();
			ApprovalLog alog = new ApprovalLog();
			ApprovalLogExample.Criteria createCriteria = approvalLogexample.createCriteria();
			String aid = request.getParameter("approvalLogId");
			if (null != aid && !aid.isEmpty()) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalLogexample);
				System.out.println("   list size in approval list  :::   " + listapprovallog.size());

				short alogId = listapprovallog.get(0).getId();
				alog.setId(alogId);
				alog.setStatus("2");
				alog.setChecker(Short.parseShort(loginUser)); // get from the current login session
				alog.setUpdateDate(new Date());
				approvalLogMapper.updateByPrimaryKeySelective(alog);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "agent-product-approval";
	}

	// ---------------------------------------------------------------End By Arbind
	// Product Info Change Approval and Reject
	// WTC------------------------------------------------------

}// Main class end
