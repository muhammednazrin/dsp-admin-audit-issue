package com.spring.admin;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.spring.VO.AgentProfile;
import com.spring.VO.Approval;
import com.spring.VO.ApprovalExample;
import com.spring.VO.ApprovalLog;
import com.spring.VO.ApprovalLogExample;
import com.spring.VO.HOHHInsProductApprovalRate;
import com.spring.VO.HOHHParam;
import com.spring.VO.HOHHParamExample;
import com.spring.VO.HOHHProductInfoApproval;
import com.spring.VO.MITblPmntParam;
import com.spring.VO.MITblPmntParamExample;
import com.spring.VO.TblPdfInfo;
import com.spring.VO.TblPdfInfoExample;
import com.spring.mapper.AdminParamMapper;
import com.spring.mapper.AgentDocumentMapper;
import com.spring.mapper.AgentLinkMapper;
import com.spring.mapper.AgentProdMapMapper;
import com.spring.mapper.AgentProfileMapper;
import com.spring.mapper.ApprovalLogMapper;
import com.spring.mapper.ApprovalMapper;
import com.spring.mapper.CommonQQMapper;
import com.spring.mapper.HOHHParamMapper;
import com.spring.mapper.HOHHPaymentParamMapper;
import com.spring.mapper.MITblPmntParamMapper;
import com.spring.mapper.ProductsMapper;
import com.spring.mapper.TblPdfInfoMapper;

@Controller
public class HOHHProdMngController {
	HOHHParamMapper hohharamMapper;
	TblPdfInfoMapper tblPdfInfoMapper;
	HOHHPaymentParamMapper hohhPaymentParamMapper;
	MITblPmntParamMapper miTblPmntParamMapper;
	ApprovalLogMapper approvalLogMapper;
	ApprovalMapper approvalMapper;
	AgentProfileMapper agentProfileMapper;
	ProductsMapper productsMapper;
	AgentProdMapMapper agentProdMapMapper;
	CommonQQMapper commonQQMapper;
	AdminParamMapper adminParamMapper;
	AgentDocumentMapper agentDocumentMapper;
	AgentLinkMapper agentLinkMapper;

	@Autowired
	public HOHHProdMngController(HOHHParamMapper hohharamMapper, TblPdfInfoMapper tblPdfInfoMapper,
			MITblPmntParamMapper miTblPmntParamMapper, ApprovalLogMapper approvalLogMapper,
			ApprovalMapper approvalMapper
	// HOHHPaymentParamMapper hohhPaymentParamMapper
	) {
		this.hohharamMapper = hohharamMapper;
		// this.hohhPaymentParamMapper=hohhPaymentParamMapper;
		this.tblPdfInfoMapper = tblPdfInfoMapper;
		this.miTblPmntParamMapper = miTblPmntParamMapper;
		this.approvalLogMapper = approvalLogMapper;
		this.approvalMapper = approvalMapper;
	}

	@RequestMapping("/HOHHproductrate")
	private String HOHHproductrate(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		// INS GST Amount

		List<HOHHParam> insGSTAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample = new HOHHParamExample();
		HOHHParamExample.Criteria HOHHParam_criteria = HOHHTblParamExample.createCriteria();
		HOHHParam_criteria.andParamNameEqualTo("ins_GST");
		insGSTAmountList = hohharamMapper.selectByExample(HOHHTblParamExample);

		// ins_AdditionalBenefit_ETC Amount

		List<HOHHParam> ins_AdditionalBenefit_ETCAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample1 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_AdditionalBenefit_ETC = HOHHTblParamExample1.createCriteria();
		ins_AdditionalBenefit_ETC.andParamNameEqualTo("ins_AdditionalBenefit_ETC");
		ins_AdditionalBenefit_ETCAmountList = hohharamMapper.selectByExample(HOHHTblParamExample1);

		// ins_AdditionalBenefit_RSMD Amount

		List<HOHHParam> ins_AdditionalBenefit_RSMDAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample2 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_AdditionalBenefit_RSMD = HOHHTblParamExample2.createCriteria();
		ins_AdditionalBenefit_RSMD.andParamNameEqualTo("ins_AdditionalBenefit_RSMD");
		ins_AdditionalBenefit_RSMDAmountList = hohharamMapper.selectByExample(HOHHTblParamExample2);

		// ins_StampDuty Amount

		List<HOHHParam> ins_StampDutyAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample3 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_StampDuty = HOHHTblParamExample3.createCriteria();
		ins_StampDuty.andParamNameEqualTo("ins_StampDuty");
		ins_StampDutyAmountList = hohharamMapper.selectByExample(HOHHTblParamExample3);

		// ins_HC_MinimumCoverageAmount Amount

		List<HOHHParam> ins_HC_MinimumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample4 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HC_MinimumCoverageAmount = HOHHTblParamExample4.createCriteria();
		ins_HC_MinimumCoverageAmount.andParamNameEqualTo("ins_HC_MinimumCoverageAmount");
		ins_HC_MinimumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample4);

		// ins_HB_MaximumCoverageAmount Amount

		List<HOHHParam> ins_HB_MaximumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample5 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HB_MaximumCoverageAmount = HOHHTblParamExample5.createCriteria();
		ins_HB_MaximumCoverageAmount.andParamNameEqualTo("ins_HB_MaximumCoverageAmount");
		ins_HB_MaximumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample5);

		// ins_HB_MinimumCoverageAmount Amount

		List<HOHHParam> ins_HB_MinimumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample6 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HB_MinimumCoverageAmount = HOHHTblParamExample6.createCriteria();
		ins_HB_MinimumCoverageAmount.andParamNameEqualTo("ins_HB_MinimumCoverageAmount");
		ins_HB_MinimumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample6);

		// ins_DirectDiscount Amount

		List<HOHHParam> ins_DirectDiscountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample7 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_DirectDiscount = HOHHTblParamExample7.createCriteria();
		ins_DirectDiscount.andParamNameEqualTo("ins_DirectDiscount");
		ins_DirectDiscountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample7);

		// ins_HC_MaximumCoverageAmount Amount

		List<HOHHParam> ins_HC_MaximumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample8 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HC_MaximumCoverageAmount = HOHHTblParamExample8.createCriteria();
		ins_HC_MaximumCoverageAmount.andParamNameEqualTo("ins_HC_MaximumCoverageAmount");
		ins_HC_MaximumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample8);

		// INS GST Amount

		List<HOHHParam> instGSTAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExamplet = new HOHHParamExample();
		HOHHParamExample.Criteria HOHHParam_criteriat = HOHHTblParamExamplet.createCriteria();
		HOHHParam_criteriat.andParamNameEqualTo("TKFL_GST");
		instGSTAmountList = hohharamMapper.selectByExample(HOHHTblParamExamplet);

		// ins_AdditionalBenefit_ETC Amount

		List<HOHHParam> inst_AdditionalBenefit_ETCAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExamplet1 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_AdditionalBenefit_ETC = HOHHTblParamExamplet1.createCriteria();
		inst_AdditionalBenefit_ETC.andParamNameEqualTo("TKFL_AdditionalBenefit_ETC");
		inst_AdditionalBenefit_ETCAmountList = hohharamMapper.selectByExample(HOHHTblParamExamplet1);

		// ins_AdditionalBenefit_RSMD Amount

		List<HOHHParam> inst_AdditionalBenefit_RSMDAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExamplet2 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_AdditionalBenefit_RSMD = HOHHTblParamExamplet2.createCriteria();
		inst_AdditionalBenefit_RSMD.andParamNameEqualTo("TKFL_AdditionalBenefit_RSMD");
		inst_AdditionalBenefit_RSMDAmountList = hohharamMapper.selectByExample(HOHHTblParamExamplet2);

		// ins_StampDuty Amount

		List<HOHHParam> inst_StampDutyAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExamplet3 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_StampDuty = HOHHTblParamExamplet3.createCriteria();
		inst_StampDuty.andParamNameEqualTo("TKFL_StampDuty");
		inst_StampDutyAmountList = hohharamMapper.selectByExample(HOHHTblParamExamplet3);

		// ins_HC_MinimumCoverageAmount Amount

		List<HOHHParam> inst_HC_MinimumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExamplet4 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_HC_MinimumCoverageAmount = HOHHTblParamExamplet4.createCriteria();
		inst_HC_MinimumCoverageAmount.andParamNameEqualTo("TKFL_HC_MinimumCoverageAmount");
		inst_HC_MinimumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExamplet4);

		// ins_HB_MaximumCoverageAmount Amount

		List<HOHHParam> inst_HB_MaximumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExamplet5 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_HB_MaximumCoverageAmount = HOHHTblParamExamplet5.createCriteria();
		inst_HB_MaximumCoverageAmount.andParamNameEqualTo("TKFL_HB_MaximumCoverageAmount");
		inst_HB_MaximumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExamplet5);

		// ins_HB_MinimumCoverageAmount Amount

		List<HOHHParam> inst_HB_MinimumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExamplet6 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_HB_MinimumCoverageAmount = HOHHTblParamExamplet6.createCriteria();
		inst_HB_MinimumCoverageAmount.andParamNameEqualTo("TKFL_HB_MinimumCoverageAmount");
		inst_HB_MinimumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExamplet6);

		// ins_DirectDiscount Amount

		List<HOHHParam> inst_DirectDiscountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExamplet7 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_DirectDiscount = HOHHTblParamExamplet7.createCriteria();
		inst_DirectDiscount.andParamNameEqualTo("TKFL_DirectDiscount");
		inst_DirectDiscountAmountList = hohharamMapper.selectByExample(HOHHTblParamExamplet7);

		// ins_HC_MaximumCoverageAmount Amount

		List<HOHHParam> inst_HC_MaximumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExamplet8 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_HC_MaximumCoverageAmount = HOHHTblParamExamplet8.createCriteria();
		inst_HC_MaximumCoverageAmount.andParamNameEqualTo("TKFL_HC_MaximumCoverageAmount");
		inst_HC_MaximumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExamplet8);

		model.addAttribute("instGSTAmountList", instGSTAmountList);
		model.addAttribute("inst_AdditionalBenefit_ETCAmountList", inst_AdditionalBenefit_ETCAmountList);
		model.addAttribute("inst_AdditionalBenefit_RSMDAmountList", inst_AdditionalBenefit_RSMDAmountList);
		model.addAttribute("inst_StampDutyAmountList", inst_StampDutyAmountList);
		model.addAttribute("inst_HC_MinimumCoverageAmountAmountList", inst_HC_MinimumCoverageAmountAmountList);
		model.addAttribute("inst_HB_MaximumCoverageAmountAmountList", inst_HB_MaximumCoverageAmountAmountList);
		model.addAttribute("inst_HB_MinimumCoverageAmountAmountList", inst_HB_MinimumCoverageAmountAmountList);
		model.addAttribute("inst_DirectDiscountAmountList", inst_DirectDiscountAmountList);
		model.addAttribute("inst_HC_MaximumCoverageAmountAmountList", inst_HC_MaximumCoverageAmountAmountList);

		model.addAttribute("insGSTAmountList", insGSTAmountList);
		model.addAttribute("ins_AdditionalBenefit_ETCAmountList", ins_AdditionalBenefit_ETCAmountList);
		model.addAttribute("ins_AdditionalBenefit_RSMDAmountList", ins_AdditionalBenefit_RSMDAmountList);
		model.addAttribute("ins_StampDutyAmountList", ins_StampDutyAmountList);
		model.addAttribute("ins_HC_MinimumCoverageAmountAmountList", ins_HC_MinimumCoverageAmountAmountList);
		model.addAttribute("ins_HB_MaximumCoverageAmountAmountList", ins_HB_MaximumCoverageAmountAmountList);
		model.addAttribute("ins_HB_MinimumCoverageAmountAmountList", ins_HB_MinimumCoverageAmountAmountList);
		model.addAttribute("ins_DirectDiscountAmountList", ins_DirectDiscountAmountList);
		model.addAttribute("ins_HC_MaximumCoverageAmountAmountList", ins_HC_MaximumCoverageAmountAmountList);

		return "products/hohh/admin-business-product-management-house-product-rate";
	}

	@RequestMapping("/HOHHproductrateUpdate")
	private String HOHHproductrateUpdate(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		System.out.println("Product rate updated");
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		// INS GST Amount

		HOHHParam tlTblParam4 = new HOHHParam();
		HOHHParamExample HOHHTblParamExample = new HOHHParamExample();
		HOHHParamExample.Criteria HOHHParam_criteria = HOHHTblParamExample.createCriteria();
		HOHHParam_criteria.andParamNameEqualTo("ins_GST");

		String gstVal = request.getParameter("GST");
		BigDecimal decgst = new BigDecimal(gstVal.replaceAll(",", ""));
		System.out.println("GST VALUE   :" + gstVal);
		tlTblParam4.setParamValue(decgst);
		int tlTblParamUpdate4 = hohharamMapper.updateByExampleSelective(tlTblParam4, HOHHTblParamExample);
		System.out.println("Check GST          :" + tlTblParamUpdate4);

		// ins_AdditionalBenefit_ETC Amount

		HOHHParam ins_AdditionalBenefit_ETCAmountList = new HOHHParam();
		HOHHParamExample HOHHTblParamExample1 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_AdditionalBenefit_ETC = HOHHTblParamExample1.createCriteria();
		ins_AdditionalBenefit_ETC.andParamNameEqualTo("ins_AdditionalBenefit_ETC");

		String ETC = request.getParameter("ETC");
		BigDecimal decETC = new BigDecimal(ETC.replaceAll(",", ""));
		System.out.println("ETC VALUE   :" + ETC);
		ins_AdditionalBenefit_ETCAmountList.setParamValue(decETC);
		int ETCETC = hohharamMapper.updateByExampleSelective(ins_AdditionalBenefit_ETCAmountList, HOHHTblParamExample1);
		System.out.println("Check ETC          :" + ETCETC);

		// ins_AdditionalBenefit_RSMD Amount

		HOHHParam ins_AdditionalBenefit_RSMDAmountList = new HOHHParam();
		HOHHParamExample HOHHTblParamExample2 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_AdditionalBenefit_RSMD = HOHHTblParamExample2.createCriteria();
		ins_AdditionalBenefit_RSMD.andParamNameEqualTo("ins_AdditionalBenefit_RSMD");

		String RSMD = request.getParameter("RSMD");
		BigDecimal decRSMD = new BigDecimal(RSMD.replaceAll(",", ""));
		System.out.println("ETC VALUE   :" + RSMD);
		ins_AdditionalBenefit_RSMDAmountList.setParamValue(decRSMD);
		int RSMDRSMD = hohharamMapper.updateByExampleSelective(ins_AdditionalBenefit_RSMDAmountList,
				HOHHTblParamExample2);
		System.out.println("Check RSMD          :" + RSMDRSMD);

		// ins_StampDuty Amount

		HOHHParam ins_StampDutyAmountList = new HOHHParam();
		HOHHParamExample HOHHTblParamExample3 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_StampDuty = HOHHTblParamExample3.createCriteria();
		ins_StampDuty.andParamNameEqualTo("ins_StampDuty");

		String StampDuty = request.getParameter("StampDuty");
		BigDecimal decStampDuty = new BigDecimal(StampDuty.replaceAll(",", ""));
		System.out.println("StampDuty VALUE   :" + StampDuty);
		ins_StampDutyAmountList.setParamValue(decStampDuty);
		int StampDutyStampDuty = hohharamMapper.updateByExampleSelective(ins_StampDutyAmountList, HOHHTblParamExample3);
		System.out.println("Check StampDuty         :" + StampDutyStampDuty);

		// ins_HC_MinimumCoverageAmount Amount

		HOHHParam ins_HC_MinimumCoverageAmountAmountList = new HOHHParam();
		HOHHParamExample HOHHTblParamExample4 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HC_MinimumCoverageAmount = HOHHTblParamExample4.createCriteria();
		ins_HC_MinimumCoverageAmount.andParamNameEqualTo("ins_HC_MinimumCoverageAmount");

		String HCMinimumCoverageAmount = request.getParameter("HCMinimumCoverageAmount");
		BigDecimal decHCMinimumCoverageAmount = new BigDecimal(HCMinimumCoverageAmount.replaceAll(",", ""));
		System.out.println("HCMinimumCoverageAmount VALUE   :" + HCMinimumCoverageAmount);
		ins_HC_MinimumCoverageAmountAmountList.setParamValue(decHCMinimumCoverageAmount);
		int HCMinimumCoverageAmountHCMinimumCoverageAmount = hohharamMapper
				.updateByExampleSelective(ins_HC_MinimumCoverageAmountAmountList, HOHHTblParamExample4);
		System.out.println("Check HCMinimumCoverageAmount         :" + HCMinimumCoverageAmountHCMinimumCoverageAmount);

		// ins_HB_MaximumCoverageAmount Amount

		HOHHParam ins_HB_MaximumCoverageAmountAmountList = new HOHHParam();
		HOHHParamExample HOHHTblParamExample5 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HB_MaximumCoverageAmount = HOHHTblParamExample5.createCriteria();
		ins_HB_MaximumCoverageAmount.andParamNameEqualTo("ins_HB_MaximumCoverageAmount");

		String HBMaximumCoverageAmount = request.getParameter("HBMaximumCoverageAmount");
		BigDecimal decHBMaximumCoverageAmount = new BigDecimal(HBMaximumCoverageAmount.replaceAll(",", ""));
		System.out.println("HCMinimumCoverageAmount VALUE   :" + decHBMaximumCoverageAmount);
		ins_HB_MaximumCoverageAmountAmountList.setParamValue(decHBMaximumCoverageAmount);
		int HBMaximumCoverageAmountHBMaximumCoverageAmount = hohharamMapper
				.updateByExampleSelective(ins_HB_MaximumCoverageAmountAmountList, HOHHTblParamExample5);
		System.out.println("Check HBMaximumCoverageAmount         :" + HBMaximumCoverageAmountHBMaximumCoverageAmount);

		// ins_HB_MinimumCoverageAmount Amount

		HOHHParam ins_HB_MinimumCoverageAmountAmountList = new HOHHParam();
		HOHHParamExample HOHHTblParamExample6 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HB_MinimumCoverageAmount = HOHHTblParamExample6.createCriteria();
		ins_HB_MinimumCoverageAmount.andParamNameEqualTo("ins_HB_MinimumCoverageAmount");

		String HBMinimumCoverageAmount = request.getParameter("HBMinimumCoverageAmount");
		BigDecimal decHBMinimumCoverageAmount = new BigDecimal(HBMinimumCoverageAmount.replaceAll(",", ""));
		System.out.println("HBMinimumCoverageAmount VALUE   :" + decHBMinimumCoverageAmount);
		ins_HB_MinimumCoverageAmountAmountList.setParamValue(decHBMinimumCoverageAmount);
		int HBMinimumCoverageAmountHBMinimumCoverageAmount = hohharamMapper
				.updateByExampleSelective(ins_HB_MinimumCoverageAmountAmountList, HOHHTblParamExample6);
		System.out.println("Check HBMinimumCoverageAmount         :" + HBMinimumCoverageAmountHBMinimumCoverageAmount);

		// ins_DirectDiscount Amount

		HOHHParam ins_DirectDiscountAmountList = new HOHHParam();
		HOHHParamExample HOHHTblParamExample7 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_DirectDiscount = HOHHTblParamExample7.createCriteria();
		ins_DirectDiscount.andParamNameEqualTo("ins_DirectDiscount");

		String discountVal = request.getParameter("discountVal");
		BigDecimal decdiscountVal = new BigDecimal(discountVal.replaceAll(",", ""));
		System.out.println("discountVal VALUE   :" + decdiscountVal);
		ins_DirectDiscountAmountList.setParamValue(decdiscountVal);
		int discountValdiscountVal = hohharamMapper.updateByExampleSelective(ins_DirectDiscountAmountList,
				HOHHTblParamExample7);
		System.out.println("Check discountVal         :" + discountValdiscountVal);

		// ins_HC_MaximumCoverageAmount Amount

		HOHHParam ins_HC_MaximumCoverageAmountAmountList = new HOHHParam();
		HOHHParamExample HOHHTblParamExample8 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HC_MaximumCoverageAmount = HOHHTblParamExample8.createCriteria();
		ins_HC_MaximumCoverageAmount.andParamNameEqualTo("ins_HC_MaximumCoverageAmount");

		String HCMaximumCoverageAmount = request.getParameter("HCMaximumCoverageAmount");
		BigDecimal decHCMaximumCoverageAmount = new BigDecimal(HCMaximumCoverageAmount.replaceAll(",", ""));
		System.out.println("HCMaximumCoverageAmount VALUE   :" + decHCMaximumCoverageAmount);
		ins_HC_MaximumCoverageAmountAmountList.setParamValue(decHCMaximumCoverageAmount);
		int HCMaximumCoverageAmountHCMaximumCoverageAmount = hohharamMapper
				.updateByExampleSelective(ins_HC_MaximumCoverageAmountAmountList, HOHHTblParamExample8);
		System.out.println("Check HCMaximumCoverageAmount         :" + HCMaximumCoverageAmountHCMaximumCoverageAmount);
		model.addAttribute("UpdateMessage", "Update data successfully");
		String returnURL = HOHHproductrate(request, response, model);
		return returnURL;
	}

	@RequestMapping("/HOHHproductinfomation")
	private String HOHHproductinfomation(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();

		System.out.println("Product Info ");

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		// Quotaion Validity

		List<HOHHParam> ins_Annual_SalesTarget = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample = new HOHHParamExample();
		HOHHParamExample.Criteria HOHHParam_criteria = HOHHTblParamExample.createCriteria();
		HOHHParam_criteria.andParamNameEqualTo("ins_Annual_SalesTarget");
		ins_Annual_SalesTarget = hohharamMapper.selectByExample(HOHHTblParamExample);

		// Annual Sales Target

		List<HOHHParam> ins_Quotation_Validity = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample1 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_AdditionalBenefit_ETC = HOHHTblParamExample1.createCriteria();
		ins_AdditionalBenefit_ETC.andParamNameEqualTo("ins_Quotation_Validity");
		ins_Quotation_Validity = hohharamMapper.selectByExample(HOHHTblParamExample1);

		List<TblPdfInfo> tblPdfInfoList = new ArrayList<TblPdfInfo>();
		TblPdfInfoExample tblLoadingExample = new TblPdfInfoExample();
		tblLoadingExample.setOrderByClause("pds.ID desc");
		tblPdfInfoList = tblPdfInfoMapper.selectByExample(tblLoadingExample);

		model.addAttribute("tblPdfInfoList", tblPdfInfoList);
		model.addAttribute("quotationValidityList", ins_Quotation_Validity);
		model.addAttribute("annualSalesAmntList", ins_Annual_SalesTarget);

		return "products/hohh/admin-business-product-management-house-product-infomation";
	}

	@RequestMapping("/HOHHproductinfomationEdit")
	private String HOHHproductinfomationEdit(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		// Quotaion Validity

		List<HOHHParam> ins_Annual_SalesTarget = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample = new HOHHParamExample();
		HOHHParamExample.Criteria HOHHParam_criteria = HOHHTblParamExample.createCriteria();
		HOHHParam_criteria.andParamNameEqualTo("ins_Annual_SalesTarget");
		ins_Annual_SalesTarget = hohharamMapper.selectByExample(HOHHTblParamExample);

		// Annual Sales Target

		List<HOHHParam> ins_Quotation_Validity = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample1 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_AdditionalBenefit_ETC = HOHHTblParamExample1.createCriteria();
		ins_AdditionalBenefit_ETC.andParamNameEqualTo("ins_Quotation_Validity");
		ins_Quotation_Validity = hohharamMapper.selectByExample(HOHHTblParamExample1);

		List<TblPdfInfo> tblPdfInfoList = new ArrayList<TblPdfInfo>();
		TblPdfInfoExample tblLoadingExample = new TblPdfInfoExample();
		tblLoadingExample.setOrderByClause("pds.ID desc");
		tblPdfInfoList = tblPdfInfoMapper.selectByExample(tblLoadingExample);

		model.addAttribute("tblPdfInfoList", tblPdfInfoList);
		model.addAttribute("quotationValidityList", ins_Quotation_Validity);
		model.addAttribute("annualSalesAmntList", ins_Annual_SalesTarget);

		return "products/hohh/admin-business-product-management-house-product-infomation-edit";
	}

	@RequestMapping("/HOHHproductinfomationUpdate")
	private String HOHHproductinfomationUpdate(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		HOHHParamExample HOHHTblParamExample = new HOHHParamExample();
		HOHHParam hohhParam = new HOHHParam();
		HOHHParamExample.Criteria hohhParam_criteria1 = HOHHTblParamExample.createCriteria();
		hohhParam_criteria1.andParamNameEqualTo("ins_Annual_SalesTarget");

		String salesTargetVal = request.getParameter("AnnualSalesTarget");
		System.out.println(salesTargetVal + "salesTargetVal");
		hohhParam.setParamValue(new BigDecimal(salesTargetVal));

		int hohhParamUpdate = hohharamMapper.updateByExampleSelective(hohhParam, HOHHTblParamExample);
		System.out.println(hohhParamUpdate + "salesTargetVal value");

		HOHHParamExample HOHHTblParamExample1 = new HOHHParamExample();
		HOHHParam hohhParam1 = new HOHHParam();
		HOHHParamExample.Criteria hohhParam_criteria2 = HOHHTblParamExample1.createCriteria();
		hohhParam_criteria2.andParamNameEqualTo("ins_Quotation_Validity");

		String validity = request.getParameter("QuotationValidity");
		System.out.println(validity + "validity--->");
		hohhParam1.setParamValue(new BigDecimal(validity));

		int hohhParamUpdate1 = hohharamMapper.updateByExampleSelective(hohhParam1, HOHHTblParamExample1);
		System.out.println(hohhParamUpdate1 + "quote Val value");

		// return
		// "products/hohh/admin-business-product-management-house-product-infomation";

		model.addAttribute("updatemessage", "Update data successfully");
		String returnURL = HOHHproductinfomation(request, response, model);
		return returnURL;
	}

	@RequestMapping(value = "/HOHHdeletePdsDone", method = RequestMethod.POST)
	public String HOHHdeletePdsDone(@ModelAttribute(value = "tblPdfInfo") TblPdfInfo tblPdfInfo, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String id = request.getParameter("id");
		tblPdfInfo.setId(Integer.valueOf(id));
		tblPdfInfoMapper.deleteByPrimaryKey(Short.valueOf(request.getParameter("id").trim()));

		// ******************** file deleting Starts***************************
		// tlFilePath
		String tlFilePath = request.getParameter("tlFilePath");
		if (tlFilePath != null || tlFilePath != "") {
			File file = new File(tlFilePath);

			if (file.delete()) {
				System.out.println(file.getName() + " is deleted!");
			} else {
				System.out.println("Delete operation is failed.");
			}
		}
		// ******************** file deleting Starts***************************
		System.out.println("Successfully Deleted");
		model.addAttribute("DeleteMessage", "Deleted data successfully");
		String redirctTO = HOHHproductinfomationEdit(request, response, model);
		return redirctTO;

	}

	@RequestMapping("/HOHHproductinfomationEditTKFL")
	private String HOHHproductinfomationEditTKFL(HttpServletRequest request, HttpServletResponse response,
			Model model) {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		return "products/hohh/admin-business-product-management-house-product-infomation-edit-takaful";
	}

	@RequestMapping("/HOHHpayment")
	private String HOHHpayment(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		// Maybank2U

		List<MITblPmntParam> m2ulist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria = miTblPmntParamExample.createCriteria();
		MiPmntParam_criteria.andPmntGatewayCodeEqualTo("M2U");
		MiPmntParam_criteria.andProductCodeEqualTo("HOHH");
		MiPmntParam_criteria.andStatusEqualTo(Short.valueOf("1"));
		m2ulist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample);

		// FPX

		List<MITblPmntParam> fpxlist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample1 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria1 = miTblPmntParamExample1.createCriteria();
		MiPmntParam_criteria1.andPmntGatewayCodeEqualTo("FPX");
		MiPmntParam_criteria1.andProductCodeEqualTo("HOHH");
		MiPmntParam_criteria1.andStatusEqualTo(Short.valueOf("1"));
		fpxlist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample1);
		// EBPG (Visa/Mastercard)

		List<MITblPmntParam> ebpglist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample2 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria2 = miTblPmntParamExample2.createCriteria();
		MiPmntParam_criteria2.andPmntGatewayCodeEqualTo("EBPG");
		MiPmntParam_criteria2.andProductCodeEqualTo("HOHH");
		MiPmntParam_criteria2.andStatusEqualTo(Short.valueOf("1"));
		ebpglist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample2);

		// AMEX

		List<MITblPmntParam> amexlist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample3 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria3 = miTblPmntParamExample3.createCriteria();
		MiPmntParam_criteria3.andPmntGatewayCodeEqualTo("AMEX");
		MiPmntParam_criteria3.andProductCodeEqualTo("HOHH");
		MiPmntParam_criteria3.andStatusEqualTo(Short.valueOf("1"));
		amexlist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample3);
		// Takaful.....
		// Maybank2U

		List<MITblPmntParam> m2ulistt = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExamplet = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteriat = miTblPmntParamExamplet.createCriteria();
		MiPmntParam_criteriat.andPmntGatewayCodeEqualTo("M2U");
		MiPmntParam_criteriat.andProductCodeEqualTo("HOHHT");
		MiPmntParam_criteriat.andStatusEqualTo(Short.valueOf("1"));
		m2ulistt = miTblPmntParamMapper.selectByExample(miTblPmntParamExamplet);

		// FPX

		List<MITblPmntParam> fpxlistt = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample1t = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria1t = miTblPmntParamExample1t.createCriteria();
		MiPmntParam_criteria1t.andPmntGatewayCodeEqualTo("FPX");
		MiPmntParam_criteria1t.andProductCodeEqualTo("HOHHT");
		MiPmntParam_criteria1t.andStatusEqualTo(Short.valueOf("1"));
		fpxlistt = miTblPmntParamMapper.selectByExample(miTblPmntParamExample1t);
		// EBPG (Visa/Mastercard)

		List<MITblPmntParam> ebpglistt = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample2t = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria2t = miTblPmntParamExample2t.createCriteria();
		MiPmntParam_criteria2t.andPmntGatewayCodeEqualTo("EBPG");
		MiPmntParam_criteria2t.andProductCodeEqualTo("HOHHT");
		MiPmntParam_criteria2t.andStatusEqualTo(Short.valueOf("1"));
		ebpglistt = miTblPmntParamMapper.selectByExample(miTblPmntParamExample2t);

		// AMEX

		List<MITblPmntParam> amexlistt = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample3t = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria3t = miTblPmntParamExample3t.createCriteria();
		MiPmntParam_criteria3t.andPmntGatewayCodeEqualTo("AMEX");
		MiPmntParam_criteria3t.andProductCodeEqualTo("HOHHT");
		MiPmntParam_criteria3t.andStatusEqualTo(Short.valueOf("1"));
		amexlistt = miTblPmntParamMapper.selectByExample(miTblPmntParamExample3t);

		model.addAttribute("m2ulist", m2ulist);
		model.addAttribute("fpxlist", fpxlist);
		model.addAttribute("ebpglist", ebpglist);
		model.addAttribute("amexlist", amexlist);
		model.addAttribute("m2ulistt", m2ulistt);
		model.addAttribute("fpxlistt", fpxlistt);
		model.addAttribute("ebpglistt", ebpglistt);
		model.addAttribute("amexlistt", amexlistt);

		return "products/hohh/admin-business-product-management-house-payment";
	}

	@RequestMapping("/HOHHpaymentUpdate")
	private String HOHHpaymentUpdate(@ModelAttribute(value = "MITblPmntParam") MITblPmntParam miTblPmntParam,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		// Maybank2U
		MITblPmntParamExample miTblPmntParamExample = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria = miTblPmntParamExample.createCriteria();
		MiPmntParam_criteria.andPmntGatewayCodeEqualTo("M2U");
		MiPmntParam_criteria.andProductCodeEqualTo("HOHH");
		String m2u = "0";
		if (request.getParameter("m2u") != null) {
			m2u = request.getParameter("m2u");
			if (m2u.equals("on")) {
				m2u = "1";
			}
		}
		miTblPmntParam.setStatus(Short.valueOf(m2u));
		int miTblPmntParamUpdate = miTblPmntParamMapper.updateByExampleSelective(miTblPmntParam, miTblPmntParamExample);
		System.out.println(miTblPmntParamUpdate + "M2U");

		// FPX
		MITblPmntParamExample miTblPmntParamExample1 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria1 = miTblPmntParamExample1.createCriteria();
		MiPmntParam_criteria1.andPmntGatewayCodeEqualTo("FPX");
		MiPmntParam_criteria1.andProductCodeEqualTo("HOHH");
		String fpx = "0";
		if (request.getParameter("fpx") != null) {
			fpx = request.getParameter("fpx");
			if (fpx.equals("on")) {
				fpx = "1";
			}
		}
		System.out.println(fpx + " fpx");

		miTblPmntParam.setStatus(Short.valueOf(fpx));
		int miTblPmntParamUpdate1 = miTblPmntParamMapper.updateByExampleSelective(miTblPmntParam,
				miTblPmntParamExample1);
		System.out.println(miTblPmntParamUpdate1 + "FPX");

		// EBPG
		MITblPmntParamExample miTblPmntParamExample2 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria2 = miTblPmntParamExample2.createCriteria();
		MiPmntParam_criteria2.andPmntGatewayCodeEqualTo("EBPG");
		MiPmntParam_criteria2.andProductCodeEqualTo("HOHH");
		String ebpg = "0";
		if (request.getParameter("ebpg") != null) {
			ebpg = request.getParameter("ebpg");
			if (ebpg.equals("on")) {
				ebpg = "1";
			}
		}
		miTblPmntParam.setStatus(Short.valueOf(ebpg));
		int miTblPmntParamUpdate2 = miTblPmntParamMapper.updateByExampleSelective(miTblPmntParam,
				miTblPmntParamExample2);
		System.out.println(miTblPmntParamUpdate2 + "EBPG");

		// AMEX
		MITblPmntParamExample miTblPmntParamExample3 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria3 = miTblPmntParamExample3.createCriteria();
		MiPmntParam_criteria3.andPmntGatewayCodeEqualTo("AMEX");
		MiPmntParam_criteria3.andProductCodeEqualTo("HOHH");
		String amex = "0";
		if (request.getParameter("amex") != null) {
			amex = request.getParameter("amex");
			if (amex.equals("on")) {
				amex = "1";
			}
		}
		miTblPmntParam.setStatus(Short.valueOf(amex));
		int miTblPmntParamUpdate3 = miTblPmntParamMapper.updateByExampleSelective(miTblPmntParam,
				miTblPmntParamExample3);
		System.out.println(miTblPmntParamUpdate3 + " AMEX");
		model.addAttribute("UpdateMessage", "Update data successfully");
		String returnURL = HOHHpayment(request, response, model);

		return returnURL;

	}

	@RequestMapping("/HOHHTpaymentUpdate")
	private String HOHHTpaymentUpdate(@ModelAttribute(value = "MITblPmntParam") MITblPmntParam miTblPmntParam,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		// Maybank2U
		MITblPmntParamExample miTblPmntParamExample = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria = miTblPmntParamExample.createCriteria();
		MiPmntParam_criteria.andPmntGatewayCodeEqualTo("M2U");
		MiPmntParam_criteria.andProductCodeEqualTo("HOHHT");
		String m2u = "0";
		if (request.getParameter("m2u") != null) {
			m2u = request.getParameter("m2u");
			if (m2u.equals("on")) {
				m2u = "1";
			}
		}
		miTblPmntParam.setStatus(Short.valueOf(m2u));
		int miTblPmntParamUpdate = miTblPmntParamMapper.updateByExampleSelective(miTblPmntParam, miTblPmntParamExample);
		System.out.println(miTblPmntParamUpdate + "M2U");

		// FPX
		MITblPmntParamExample miTblPmntParamExample1 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria1 = miTblPmntParamExample1.createCriteria();
		MiPmntParam_criteria1.andPmntGatewayCodeEqualTo("FPX");
		MiPmntParam_criteria1.andProductCodeEqualTo("HOHHT");
		String fpx = "0";
		if (request.getParameter("fpx") != null) {
			fpx = request.getParameter("fpx");
			if (fpx.equals("on")) {
				fpx = "1";
			}
		}
		System.out.println(fpx + " fpx");

		miTblPmntParam.setStatus(Short.valueOf(fpx));
		int miTblPmntParamUpdate1 = miTblPmntParamMapper.updateByExampleSelective(miTblPmntParam,
				miTblPmntParamExample1);
		System.out.println(miTblPmntParamUpdate1 + "FPX");

		// EBPG
		MITblPmntParamExample miTblPmntParamExample2 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria2 = miTblPmntParamExample2.createCriteria();
		MiPmntParam_criteria2.andPmntGatewayCodeEqualTo("EBPG");
		MiPmntParam_criteria2.andProductCodeEqualTo("HOHHT");
		String ebpg = "0";
		if (request.getParameter("ebpg") != null) {
			ebpg = request.getParameter("ebpg");
			if (ebpg.equals("on")) {
				ebpg = "1";
			}
		}
		miTblPmntParam.setStatus(Short.valueOf(ebpg));
		int miTblPmntParamUpdate2 = miTblPmntParamMapper.updateByExampleSelective(miTblPmntParam,
				miTblPmntParamExample2);
		System.out.println(miTblPmntParamUpdate2 + "EBPG");

		// AMEX
		MITblPmntParamExample miTblPmntParamExample3 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria3 = miTblPmntParamExample3.createCriteria();
		MiPmntParam_criteria3.andPmntGatewayCodeEqualTo("AMEX");
		MiPmntParam_criteria3.andProductCodeEqualTo("HOHHT");
		String amex = "0";
		if (request.getParameter("amex") != null) {
			amex = request.getParameter("amex");
			if (amex.equals("on")) {
				amex = "1";
			}
		}
		miTblPmntParam.setStatus(Short.valueOf(amex));
		int miTblPmntParamUpdate3 = miTblPmntParamMapper.updateByExampleSelective(miTblPmntParam,
				miTblPmntParamExample3);
		System.out.println(miTblPmntParamUpdate3 + " AMEX");
		model.addAttribute("UpdateMessage", "Update data successfully");
		String returnURL = HOHHpayment(request, response, model);

		return returnURL;

	}

	@RequestMapping("/HOHHproductrateedit")
	private String HOHHproductrateedit(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		// INS GST Amount

		List<HOHHParam> insGSTAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample = new HOHHParamExample();
		HOHHParamExample.Criteria HOHHParam_criteria = HOHHTblParamExample.createCriteria();
		HOHHParam_criteria.andParamNameEqualTo("ins_GST");
		insGSTAmountList = hohharamMapper.selectByExample(HOHHTblParamExample);

		// ins_AdditionalBenefit_ETC Amount

		List<HOHHParam> ins_AdditionalBenefit_ETCAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample1 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_AdditionalBenefit_ETC = HOHHTblParamExample1.createCriteria();
		ins_AdditionalBenefit_ETC.andParamNameEqualTo("ins_AdditionalBenefit_ETC");
		ins_AdditionalBenefit_ETCAmountList = hohharamMapper.selectByExample(HOHHTblParamExample1);

		// ins_AdditionalBenefit_RSMD Amount

		List<HOHHParam> ins_AdditionalBenefit_RSMDAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample2 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_AdditionalBenefit_RSMD = HOHHTblParamExample2.createCriteria();
		ins_AdditionalBenefit_RSMD.andParamNameEqualTo("ins_AdditionalBenefit_RSMD");
		ins_AdditionalBenefit_RSMDAmountList = hohharamMapper.selectByExample(HOHHTblParamExample2);

		// ins_StampDuty Amount

		List<HOHHParam> ins_StampDutyAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample3 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_StampDuty = HOHHTblParamExample3.createCriteria();
		ins_StampDuty.andParamNameEqualTo("ins_StampDuty");
		ins_StampDutyAmountList = hohharamMapper.selectByExample(HOHHTblParamExample3);

		// ins_HC_MinimumCoverageAmount Amount

		List<HOHHParam> ins_HC_MinimumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample4 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HC_MinimumCoverageAmount = HOHHTblParamExample4.createCriteria();
		ins_HC_MinimumCoverageAmount.andParamNameEqualTo("ins_HC_MinimumCoverageAmount");
		ins_HC_MinimumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample4);

		// ins_HB_MaximumCoverageAmount Amount

		List<HOHHParam> ins_HB_MaximumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample5 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HB_MaximumCoverageAmount = HOHHTblParamExample5.createCriteria();
		ins_HB_MaximumCoverageAmount.andParamNameEqualTo("ins_HB_MaximumCoverageAmount");
		ins_HB_MaximumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample5);

		// ins_HB_MinimumCoverageAmount Amount

		List<HOHHParam> ins_HB_MinimumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample6 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HB_MinimumCoverageAmount = HOHHTblParamExample6.createCriteria();
		ins_HB_MinimumCoverageAmount.andParamNameEqualTo("ins_HB_MinimumCoverageAmount");
		ins_HB_MinimumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample6);

		// ins_DirectDiscount Amount

		List<HOHHParam> ins_DirectDiscountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample7 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_DirectDiscount = HOHHTblParamExample7.createCriteria();
		ins_DirectDiscount.andParamNameEqualTo("ins_DirectDiscount");
		ins_DirectDiscountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample7);

		// ins_HC_MaximumCoverageAmount Amount

		List<HOHHParam> ins_HC_MaximumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample8 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HC_MaximumCoverageAmount = HOHHTblParamExample8.createCriteria();
		ins_HC_MaximumCoverageAmount.andParamNameEqualTo("ins_HC_MaximumCoverageAmount");
		ins_HC_MaximumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample8);

		model.addAttribute("insGSTAmountList", insGSTAmountList);
		model.addAttribute("ins_AdditionalBenefit_ETCAmountList", ins_AdditionalBenefit_ETCAmountList);
		model.addAttribute("ins_AdditionalBenefit_RSMDAmountList", ins_AdditionalBenefit_RSMDAmountList);
		model.addAttribute("ins_StampDutyAmountList", ins_StampDutyAmountList);
		model.addAttribute("ins_HC_MinimumCoverageAmountAmountList", ins_HC_MinimumCoverageAmountAmountList);
		model.addAttribute("ins_HB_MaximumCoverageAmountAmountList", ins_HB_MaximumCoverageAmountAmountList);
		model.addAttribute("ins_HB_MinimumCoverageAmountAmountList", ins_HB_MinimumCoverageAmountAmountList);
		model.addAttribute("ins_DirectDiscountAmountList", ins_DirectDiscountAmountList);
		model.addAttribute("ins_HC_MaximumCoverageAmountAmountList", ins_HC_MaximumCoverageAmountAmountList);

		return "products/hohh/admin-business-product-management-house-product-rate-edit";
	}

	@RequestMapping("/HOHHproductrateTakaful")
	private String HOHHproductrateTakaful(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		// INS GST Amount

		List<HOHHParam> instGSTAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample = new HOHHParamExample();
		HOHHParamExample.Criteria HOHHParam_criteria = HOHHTblParamExample.createCriteria();
		HOHHParam_criteria.andParamNameEqualTo("TKFL_GST");
		instGSTAmountList = hohharamMapper.selectByExample(HOHHTblParamExample);

		// ins_AdditionalBenefit_ETC Amount

		List<HOHHParam> inst_AdditionalBenefit_ETCAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample1 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_AdditionalBenefit_ETC = HOHHTblParamExample1.createCriteria();
		inst_AdditionalBenefit_ETC.andParamNameEqualTo("TKFL_AdditionalBenefit_ETC");
		inst_AdditionalBenefit_ETCAmountList = hohharamMapper.selectByExample(HOHHTblParamExample1);

		// ins_AdditionalBenefit_RSMD Amount

		List<HOHHParam> inst_AdditionalBenefit_RSMDAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample2 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_AdditionalBenefit_RSMD = HOHHTblParamExample2.createCriteria();
		inst_AdditionalBenefit_RSMD.andParamNameEqualTo("TKFL_AdditionalBenefit_RSMD");
		inst_AdditionalBenefit_RSMDAmountList = hohharamMapper.selectByExample(HOHHTblParamExample2);

		// ins_StampDuty Amount

		List<HOHHParam> inst_StampDutyAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample3 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_StampDuty = HOHHTblParamExample3.createCriteria();
		inst_StampDuty.andParamNameEqualTo("TKFL_StampDuty");
		inst_StampDutyAmountList = hohharamMapper.selectByExample(HOHHTblParamExample3);

		// ins_HC_MinimumCoverageAmount Amount

		List<HOHHParam> inst_HC_MinimumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample4 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_HC_MinimumCoverageAmount = HOHHTblParamExample4.createCriteria();
		inst_HC_MinimumCoverageAmount.andParamNameEqualTo("TKFL_HC_MinimumCoverageAmount");
		inst_HC_MinimumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample4);

		// ins_HB_MaximumCoverageAmount Amount

		List<HOHHParam> inst_HB_MaximumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample5 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_HB_MaximumCoverageAmount = HOHHTblParamExample5.createCriteria();
		inst_HB_MaximumCoverageAmount.andParamNameEqualTo("TKFL_HB_MaximumCoverageAmount");
		inst_HB_MaximumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample5);

		// ins_HB_MinimumCoverageAmount Amount

		List<HOHHParam> inst_HB_MinimumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample6 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_HB_MinimumCoverageAmount = HOHHTblParamExample6.createCriteria();
		inst_HB_MinimumCoverageAmount.andParamNameEqualTo("TKFL_HB_MinimumCoverageAmount");
		inst_HB_MinimumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample6);

		// ins_DirectDiscount Amount

		List<HOHHParam> inst_DirectDiscountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample7 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_DirectDiscount = HOHHTblParamExample7.createCriteria();
		inst_DirectDiscount.andParamNameEqualTo("TKFL_DirectDiscount");
		inst_DirectDiscountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample7);

		// ins_HC_MaximumCoverageAmount Amount

		List<HOHHParam> inst_HC_MaximumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample8 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_HC_MaximumCoverageAmount = HOHHTblParamExample8.createCriteria();
		inst_HC_MaximumCoverageAmount.andParamNameEqualTo("TKFL_HC_MaximumCoverageAmount");
		inst_HC_MaximumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample8);

		model.addAttribute("instGSTAmountList", instGSTAmountList);
		model.addAttribute("inst_AdditionalBenefit_ETCAmountList", inst_AdditionalBenefit_ETCAmountList);
		model.addAttribute("inst_AdditionalBenefit_RSMDAmountList", inst_AdditionalBenefit_RSMDAmountList);
		model.addAttribute("inst_StampDutyAmountList", inst_StampDutyAmountList);
		model.addAttribute("inst_HC_MinimumCoverageAmountAmountList", inst_HC_MinimumCoverageAmountAmountList);
		model.addAttribute("inst_HB_MaximumCoverageAmountAmountList", inst_HB_MaximumCoverageAmountAmountList);
		model.addAttribute("inst_HB_MinimumCoverageAmountAmountList", inst_HB_MinimumCoverageAmountAmountList);
		model.addAttribute("inst_DirectDiscountAmountList", inst_DirectDiscountAmountList);
		model.addAttribute("inst_HC_MaximumCoverageAmountAmountList", inst_HC_MaximumCoverageAmountAmountList);

		return "products/hohh/admin-business-product-management-house-product-rate";
	}

	@RequestMapping("/HOHHproductrateeditTKFL")
	private String HOHHproductrateeditTKFL(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		// INS GST Amount

		List<HOHHParam> insGSTAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample = new HOHHParamExample();
		HOHHParamExample.Criteria HOHHParam_criteria = HOHHTblParamExample.createCriteria();
		HOHHParam_criteria.andParamNameEqualTo("TKFL_GST");
		insGSTAmountList = hohharamMapper.selectByExample(HOHHTblParamExample);

		// ins_AdditionalBenefit_ETC Amount

		List<HOHHParam> ins_AdditionalBenefit_ETCAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample1 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_AdditionalBenefit_ETC = HOHHTblParamExample1.createCriteria();
		ins_AdditionalBenefit_ETC.andParamNameEqualTo("TKFL_AdditionalBenefit_ETC");
		ins_AdditionalBenefit_ETCAmountList = hohharamMapper.selectByExample(HOHHTblParamExample1);

		// ins_AdditionalBenefit_RSMD Amount

		List<HOHHParam> ins_AdditionalBenefit_RSMDAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample2 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_AdditionalBenefit_RSMD = HOHHTblParamExample2.createCriteria();
		ins_AdditionalBenefit_RSMD.andParamNameEqualTo("TKFL_AdditionalBenefit_RSMD");
		ins_AdditionalBenefit_RSMDAmountList = hohharamMapper.selectByExample(HOHHTblParamExample2);

		// ins_StampDuty Amount

		List<HOHHParam> ins_StampDutyAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample3 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_StampDuty = HOHHTblParamExample3.createCriteria();
		ins_StampDuty.andParamNameEqualTo("TKFL_StampDuty");
		ins_StampDutyAmountList = hohharamMapper.selectByExample(HOHHTblParamExample3);

		// ins_HC_MinimumCoverageAmount Amount

		List<HOHHParam> ins_HC_MinimumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample4 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HC_MinimumCoverageAmount = HOHHTblParamExample4.createCriteria();
		ins_HC_MinimumCoverageAmount.andParamNameEqualTo("TKFL_HC_MinimumCoverageAmount");
		ins_HC_MinimumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample4);

		// ins_HB_MaximumCoverageAmount Amount

		List<HOHHParam> ins_HB_MaximumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample5 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HB_MaximumCoverageAmount = HOHHTblParamExample5.createCriteria();
		ins_HB_MaximumCoverageAmount.andParamNameEqualTo("TKFL_HB_MaximumCoverageAmount");
		ins_HB_MaximumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample5);

		// ins_HB_MinimumCoverageAmount Amount

		List<HOHHParam> ins_HB_MinimumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample6 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HB_MinimumCoverageAmount = HOHHTblParamExample6.createCriteria();
		ins_HB_MinimumCoverageAmount.andParamNameEqualTo("TKFL_HB_MinimumCoverageAmount");
		ins_HB_MinimumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample6);

		// ins_DirectDiscount Amount

		List<HOHHParam> ins_DirectDiscountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample7 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_DirectDiscount = HOHHTblParamExample7.createCriteria();
		ins_DirectDiscount.andParamNameEqualTo("TKFL_DirectDiscount");
		ins_DirectDiscountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample7);

		// ins_HC_MaximumCoverageAmount Amount

		List<HOHHParam> ins_HC_MaximumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample8 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HC_MaximumCoverageAmount = HOHHTblParamExample8.createCriteria();
		ins_HC_MaximumCoverageAmount.andParamNameEqualTo("TKFL_HC_MaximumCoverageAmount");
		ins_HC_MaximumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample8);

		model.addAttribute("insGSTAmountList", insGSTAmountList);
		model.addAttribute("ins_AdditionalBenefit_ETCAmountList", ins_AdditionalBenefit_ETCAmountList);
		model.addAttribute("ins_AdditionalBenefit_RSMDAmountList", ins_AdditionalBenefit_RSMDAmountList);
		model.addAttribute("ins_StampDutyAmountList", ins_StampDutyAmountList);
		model.addAttribute("ins_HC_MinimumCoverageAmountAmountList", ins_HC_MinimumCoverageAmountAmountList);
		model.addAttribute("ins_HB_MaximumCoverageAmountAmountList", ins_HB_MaximumCoverageAmountAmountList);
		model.addAttribute("ins_HB_MinimumCoverageAmountAmountList", ins_HB_MinimumCoverageAmountAmountList);
		model.addAttribute("ins_DirectDiscountAmountList", ins_DirectDiscountAmountList);
		model.addAttribute("ins_HC_MaximumCoverageAmountAmountList", ins_HC_MaximumCoverageAmountAmountList);

		return "products/hohh/admin-business-product-management-house-product-rate-edit-takaful";
	}

	@RequestMapping("/HOHHproductratetakafulUpdate")
	private String HOHHproductratetakafulUpdate(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		System.out.println("Product Takaful rate updated");
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		// INS GST Amount

		HOHHParam tlTblParam4 = new HOHHParam();
		HOHHParamExample HOHHTblParamExample = new HOHHParamExample();
		HOHHParamExample.Criteria HOHHParam_criteria = HOHHTblParamExample.createCriteria();
		HOHHParam_criteria.andParamNameEqualTo("TKFL_GST");

		String gstVal = request.getParameter("GST");
		System.out.println("GST VALUE   :" + gstVal);
		BigDecimal decgst = new BigDecimal(gstVal.replaceAll(",", ""));
		System.out.println("GST VALUE   :" + gstVal);
		tlTblParam4.setParamValue(decgst);
		int tlTblParamUpdate4 = hohharamMapper.updateByExampleSelective(tlTblParam4, HOHHTblParamExample);
		System.out.println("Check GST          :" + tlTblParamUpdate4);

		// ins_AdditionalBenefit_ETC Amount

		HOHHParam ins_AdditionalBenefit_ETCAmountList = new HOHHParam();
		HOHHParamExample HOHHTblParamExample1 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_AdditionalBenefit_ETC = HOHHTblParamExample1.createCriteria();
		ins_AdditionalBenefit_ETC.andParamNameEqualTo("TKFL_AdditionalBenefit_ETC");

		String ETC = request.getParameter("ETC");
		BigDecimal decETC = new BigDecimal(ETC.replaceAll(",", ""));
		System.out.println("ETC VALUE   :" + ETC);
		ins_AdditionalBenefit_ETCAmountList.setParamValue(decETC);
		int ETCETC = hohharamMapper.updateByExampleSelective(ins_AdditionalBenefit_ETCAmountList, HOHHTblParamExample1);
		System.out.println("Check ETC          :" + ETCETC);

		// ins_AdditionalBenefit_RSMD Amount

		HOHHParam ins_AdditionalBenefit_RSMDAmountList = new HOHHParam();
		HOHHParamExample HOHHTblParamExample2 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_AdditionalBenefit_RSMD = HOHHTblParamExample2.createCriteria();
		ins_AdditionalBenefit_RSMD.andParamNameEqualTo("TKFL_AdditionalBenefit_RSMD");

		String RSMD = request.getParameter("RSMD");
		BigDecimal decRSMD = new BigDecimal(RSMD.replaceAll(",", ""));
		System.out.println("ETC VALUE   :" + RSMD);
		ins_AdditionalBenefit_RSMDAmountList.setParamValue(decRSMD);
		int RSMDRSMD = hohharamMapper.updateByExampleSelective(ins_AdditionalBenefit_RSMDAmountList,
				HOHHTblParamExample2);
		System.out.println("Check RSMD          :" + RSMDRSMD);

		// ins_StampDuty Amount

		HOHHParam ins_StampDutyAmountList = new HOHHParam();
		HOHHParamExample HOHHTblParamExample3 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_StampDuty = HOHHTblParamExample3.createCriteria();
		ins_StampDuty.andParamNameEqualTo("TKFL_StampDuty");

		String StampDuty = request.getParameter("StampDuty");
		BigDecimal decStampDuty = new BigDecimal(StampDuty.replaceAll(",", ""));
		System.out.println("StampDuty VALUE   :" + StampDuty);
		ins_StampDutyAmountList.setParamValue(decStampDuty);
		int StampDutyStampDuty = hohharamMapper.updateByExampleSelective(ins_StampDutyAmountList, HOHHTblParamExample3);
		System.out.println("Check StampDuty         :" + StampDutyStampDuty);

		// ins_HC_MinimumCoverageAmount Amount

		HOHHParam ins_HC_MinimumCoverageAmountAmountList = new HOHHParam();
		HOHHParamExample HOHHTblParamExample4 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HC_MinimumCoverageAmount = HOHHTblParamExample4.createCriteria();
		ins_HC_MinimumCoverageAmount.andParamNameEqualTo("TKFL_HC_MinimumCoverageAmount");

		String HCMinimumCoverageAmount = request.getParameter("HCMinimumCoverageAmount");
		BigDecimal decHCMinimumCoverageAmount = new BigDecimal(HCMinimumCoverageAmount.replaceAll(",", ""));
		System.out.println("HCMinimumCoverageAmount VALUE   :" + HCMinimumCoverageAmount);
		ins_HC_MinimumCoverageAmountAmountList.setParamValue(decHCMinimumCoverageAmount);
		int HCMinimumCoverageAmountHCMinimumCoverageAmount = hohharamMapper
				.updateByExampleSelective(ins_HC_MinimumCoverageAmountAmountList, HOHHTblParamExample4);
		System.out.println("Check HCMinimumCoverageAmount         :" + HCMinimumCoverageAmountHCMinimumCoverageAmount);

		// ins_HB_MaximumCoverageAmount Amount

		HOHHParam ins_HB_MaximumCoverageAmountAmountList = new HOHHParam();
		HOHHParamExample HOHHTblParamExample5 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HB_MaximumCoverageAmount = HOHHTblParamExample5.createCriteria();
		ins_HB_MaximumCoverageAmount.andParamNameEqualTo("TKFL_HB_MaximumCoverageAmount");

		String HBMaximumCoverageAmount = request.getParameter("HBMaximumCoverageAmount");
		BigDecimal decHBMaximumCoverageAmount = new BigDecimal(HBMaximumCoverageAmount.replaceAll(",", ""));
		System.out.println("HCMinimumCoverageAmount VALUE   :" + decHBMaximumCoverageAmount);
		ins_HB_MaximumCoverageAmountAmountList.setParamValue(decHBMaximumCoverageAmount);
		int HBMaximumCoverageAmountHBMaximumCoverageAmount = hohharamMapper
				.updateByExampleSelective(ins_HB_MaximumCoverageAmountAmountList, HOHHTblParamExample5);
		System.out.println("Check HBMaximumCoverageAmount         :" + HBMaximumCoverageAmountHBMaximumCoverageAmount);

		// ins_HB_MinimumCoverageAmount Amount

		HOHHParam ins_HB_MinimumCoverageAmountAmountList = new HOHHParam();
		HOHHParamExample HOHHTblParamExample6 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HB_MinimumCoverageAmount = HOHHTblParamExample6.createCriteria();
		ins_HB_MinimumCoverageAmount.andParamNameEqualTo("TKFL_HB_MinimumCoverageAmount");

		String HBMinimumCoverageAmount = request.getParameter("HBMinimumCoverageAmount");
		BigDecimal decHBMinimumCoverageAmount = new BigDecimal(HBMinimumCoverageAmount.replaceAll(",", ""));
		System.out.println("HBMinimumCoverageAmount VALUE   :" + decHBMinimumCoverageAmount);
		ins_HB_MinimumCoverageAmountAmountList.setParamValue(decHBMinimumCoverageAmount);
		int HBMinimumCoverageAmountHBMinimumCoverageAmount = hohharamMapper
				.updateByExampleSelective(ins_HB_MinimumCoverageAmountAmountList, HOHHTblParamExample6);
		System.out.println("Check HBMinimumCoverageAmount         :" + HBMinimumCoverageAmountHBMinimumCoverageAmount);

		// ins_DirectDiscount Amount

		HOHHParam ins_DirectDiscountAmountList = new HOHHParam();
		HOHHParamExample HOHHTblParamExample7 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_DirectDiscount = HOHHTblParamExample7.createCriteria();
		ins_DirectDiscount.andParamNameEqualTo("TKFL_DirectDiscount");

		String discountVal = request.getParameter("discountVal");
		BigDecimal decdiscountVal = new BigDecimal(discountVal.replaceAll(",", ""));
		System.out.println("discountVal VALUE   :" + decdiscountVal);
		ins_DirectDiscountAmountList.setParamValue(decdiscountVal);
		int discountValdiscountVal = hohharamMapper.updateByExampleSelective(ins_DirectDiscountAmountList,
				HOHHTblParamExample7);
		System.out.println("Check discountVal         :" + discountValdiscountVal);

		// ins_HC_MaximumCoverageAmount Amount

		HOHHParam ins_HC_MaximumCoverageAmountAmountList = new HOHHParam();
		HOHHParamExample HOHHTblParamExample8 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HC_MaximumCoverageAmount = HOHHTblParamExample8.createCriteria();
		ins_HC_MaximumCoverageAmount.andParamNameEqualTo("TKFL_HC_MaximumCoverageAmount");

		String HCMaximumCoverageAmount = request.getParameter("HCMaximumCoverageAmount");
		BigDecimal decHCMaximumCoverageAmount = new BigDecimal(HCMaximumCoverageAmount.replaceAll(",", ""));
		System.out.println("HCMaximumCoverageAmount VALUE   :" + decHCMaximumCoverageAmount);
		ins_HC_MaximumCoverageAmountAmountList.setParamValue(decHCMaximumCoverageAmount);
		int HCMaximumCoverageAmountHCMaximumCoverageAmount = hohharamMapper
				.updateByExampleSelective(ins_HC_MaximumCoverageAmountAmountList, HOHHTblParamExample8);
		System.out.println("Check HCMaximumCoverageAmount         :" + HCMaximumCoverageAmountHCMaximumCoverageAmount);
		model.addAttribute("UpdateMessage", "Update data successfully");
		String returnURL = HOHHproductrate(request, response, model);
		return returnURL;
	}

	@RequestMapping("/HOHHhousepaymentedit")
	private String HOHHhousepaymentedit(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		// Maybank2U

		List<MITblPmntParam> m2ulist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria = miTblPmntParamExample.createCriteria();
		MiPmntParam_criteria.andPmntGatewayCodeEqualTo("M2U");
		MiPmntParam_criteria.andProductCodeEqualTo("HOHH");
		MiPmntParam_criteria.andStatusEqualTo(Short.valueOf("1"));
		m2ulist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample);

		// FPX

		List<MITblPmntParam> fpxlist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample1 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria1 = miTblPmntParamExample1.createCriteria();
		MiPmntParam_criteria1.andPmntGatewayCodeEqualTo("FPX");
		MiPmntParam_criteria1.andProductCodeEqualTo("HOHH");
		MiPmntParam_criteria1.andStatusEqualTo(Short.valueOf("1"));
		fpxlist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample1);
		// EBPG (Visa/ Mastercard)

		List<MITblPmntParam> ebpglist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample2 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria2 = miTblPmntParamExample2.createCriteria();
		MiPmntParam_criteria2.andPmntGatewayCodeEqualTo("EBPG");
		MiPmntParam_criteria2.andProductCodeEqualTo("HOHH");
		MiPmntParam_criteria2.andStatusEqualTo(Short.valueOf("1"));
		ebpglist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample2);

		// AMEX

		List<MITblPmntParam> amexlist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample3 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria3 = miTblPmntParamExample3.createCriteria();
		MiPmntParam_criteria3.andPmntGatewayCodeEqualTo("AMEX");
		MiPmntParam_criteria3.andProductCodeEqualTo("HOHH");
		MiPmntParam_criteria3.andStatusEqualTo(Short.valueOf("1"));
		amexlist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample3);

		model.addAttribute("m2ulist", m2ulist);
		model.addAttribute("fpxlist", fpxlist);
		model.addAttribute("ebpglist", ebpglist);
		model.addAttribute("amexlist", amexlist);
		return "products/hohh/admin-business-product-management-house-payment-edit";
	}

	@RequestMapping("/HOHHhousepaymentedittakaful")
	private String HOHHhousepaymentedittakaful(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		// Maybank2U

		List<MITblPmntParam> m2ulistt = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExamplet = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteriat = miTblPmntParamExamplet.createCriteria();
		MiPmntParam_criteriat.andPmntGatewayCodeEqualTo("M2U");
		MiPmntParam_criteriat.andProductCodeEqualTo("HOHHT");
		MiPmntParam_criteriat.andStatusEqualTo(Short.valueOf("1"));
		m2ulistt = miTblPmntParamMapper.selectByExample(miTblPmntParamExamplet);

		// FPX

		List<MITblPmntParam> fpxlistt = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample1t = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria1t = miTblPmntParamExample1t.createCriteria();
		MiPmntParam_criteria1t.andPmntGatewayCodeEqualTo("FPX");
		MiPmntParam_criteria1t.andProductCodeEqualTo("HOHHT");
		MiPmntParam_criteria1t.andStatusEqualTo(Short.valueOf("1"));
		fpxlistt = miTblPmntParamMapper.selectByExample(miTblPmntParamExample1t);
		// EBPG (Visa/ Mastercard)

		List<MITblPmntParam> ebpglistt = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample2t = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria2t = miTblPmntParamExample2t.createCriteria();
		MiPmntParam_criteria2t.andPmntGatewayCodeEqualTo("EBPG");
		MiPmntParam_criteria2t.andProductCodeEqualTo("HOHHT");
		MiPmntParam_criteria2t.andStatusEqualTo(Short.valueOf("1"));
		ebpglistt = miTblPmntParamMapper.selectByExample(miTblPmntParamExample2t);

		// AMEX

		List<MITblPmntParam> amexlistt = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample3t = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria3t = miTblPmntParamExample3t.createCriteria();
		MiPmntParam_criteria3t.andPmntGatewayCodeEqualTo("AMEX");
		MiPmntParam_criteria3t.andProductCodeEqualTo("HOHHT");
		MiPmntParam_criteria3t.andStatusEqualTo(Short.valueOf("1"));
		amexlistt = miTblPmntParamMapper.selectByExample(miTblPmntParamExample3t);

		model.addAttribute("m2ulistt", m2ulistt);
		model.addAttribute("fpxlistt", fpxlistt);
		model.addAttribute("ebpglistt", ebpglistt);
		model.addAttribute("amexlistt", amexlistt);
		return "products/hohh/admin-business-product-management-house-payment-edit-takaful";
	}

	@RequestMapping(value = "/HOHHuploadPdfFile", method = RequestMethod.POST)
	public String HOHHuploadPdfFile(@RequestParam("PDF") CommonsMultipartFile file, HttpServletRequest request,
			HttpServletResponse response, Model model) {

		String redirctTO = null, fileName = null, filePath = null;
		String fileNameFormat = new SimpleDateFormat("yyyyMMddHHmm'.pdf'").format(new Date());
		fileName = request.getParameter("tlFileName");
		if (!file.isEmpty()) {
			/*
			 * File a = convert(file); a.renameTo(new File("G:\\1702\\" +
			 * fileName+fileNameFormat));
			 */
			try (InputStream in = this.getClass().getClassLoader()
					.getResourceAsStream("com/etiqa/dsp/sales/process/email/documents.properties")) {
				Properties prop = new Properties();
				prop.load(in);

				// ********************** File writing to another locaiton
				// Starts*********************
				filePath = prop.getProperty("TLPdfDestinationPath") + fileName + fileNameFormat;
				InputStream input = new FileInputStream(convert(file));
				FileOutputStream fos = new FileOutputStream(filePath, true);
				int b = 0;
				while ((b = input.read()) != -1) {
					fos.write(b);
				}
				System.out.println("File has been written");
			} catch (Exception e) {
				System.out.println("Could not create file");
			}
			// ********************** File writing to another locaiton
			// Ends*********************

			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}
			TblPdfInfo tblPdfInfo = new TblPdfInfo();

			Timestamp timestamp = new Timestamp(System.currentTimeMillis());

			tblPdfInfo.setProductCode("HOHH");
			tblPdfInfo.setFileName(fileName);
			tblPdfInfo.setFilePath(filePath);
			double bytes = convert(file).length();
			double kilobytes = bytes / 1024;
			DecimalFormat df = new DecimalFormat("0.00");
			tblPdfInfo.setFileSize(df.format(kilobytes));
			tblPdfInfo.setCreatedBy(loginUser);
			tblPdfInfo.setCreatedDate(timestamp);

			tblPdfInfoMapper.insert(tblPdfInfo);

			System.out.println("insertion Successfully Done");
			model.addAttribute("SaveMessage", "Loading Data Added Successfully!");

			redirctTO = HOHHproductinfomation(request, response, model);
		}
		return redirctTO;
	}

	public File convert(MultipartFile file) {
		File convFile = new File(file.getOriginalFilename());
		FileOutputStream fos = null;
		try {
			convFile.createNewFile();
			fos = new FileOutputStream(convFile);
			fos.write(file.getBytes());
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return convFile;
	}

	public String numberFormat(String val) {
		Double iVal = null;
		iVal = Double.parseDouble(val == null ? "0" : val);
		DecimalFormat df = new DecimalFormat("0,000");
		System.out.println(df.format(iVal));
		String formatVal = df.format(iVal);
		return formatVal;
	}

	/* HOHH Product rate approval starts here */

	@RequestMapping("/HOHHproductrateSave")
	private String HOHHproductrateSave(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		System.out.println("Product rate updated");
		String loginUser = (String) session.getAttribute("user");
		String logedUser = (String) session.getAttribute("logedUser");

		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		// INS GST Amount

		List<HOHHParam> insGSTAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample = new HOHHParamExample();
		HOHHParamExample.Criteria HOHHParam_criteria = HOHHTblParamExample.createCriteria();
		HOHHParam_criteria.andParamNameEqualTo("ins_GST");
		insGSTAmountList = hohharamMapper.selectByExample(HOHHTblParamExample);

		// ins_AdditionalBenefit_ETC Amount

		List<HOHHParam> ins_AdditionalBenefit_ETCAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample1 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_AdditionalBenefit_ETC = HOHHTblParamExample1.createCriteria();
		ins_AdditionalBenefit_ETC.andParamNameEqualTo("ins_AdditionalBenefit_ETC");
		ins_AdditionalBenefit_ETCAmountList = hohharamMapper.selectByExample(HOHHTblParamExample1);

		// ins_AdditionalBenefit_RSMD Amount

		List<HOHHParam> ins_AdditionalBenefit_RSMDAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample2 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_AdditionalBenefit_RSMD = HOHHTblParamExample2.createCriteria();
		ins_AdditionalBenefit_RSMD.andParamNameEqualTo("ins_AdditionalBenefit_RSMD");
		ins_AdditionalBenefit_RSMDAmountList = hohharamMapper.selectByExample(HOHHTblParamExample2);

		// ins_StampDuty Amount

		List<HOHHParam> ins_StampDutyAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample3 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_StampDuty = HOHHTblParamExample3.createCriteria();
		ins_StampDuty.andParamNameEqualTo("ins_StampDuty");
		ins_StampDutyAmountList = hohharamMapper.selectByExample(HOHHTblParamExample3);

		// ins_HC_MinimumCoverageAmount Amount

		List<HOHHParam> ins_HC_MinimumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample4 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HC_MinimumCoverageAmount = HOHHTblParamExample4.createCriteria();
		ins_HC_MinimumCoverageAmount.andParamNameEqualTo("ins_HC_MinimumCoverageAmount");
		ins_HC_MinimumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample4);

		// ins_HB_MaximumCoverageAmount Amount

		List<HOHHParam> ins_HB_MaximumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample5 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HB_MaximumCoverageAmount = HOHHTblParamExample5.createCriteria();
		ins_HB_MaximumCoverageAmount.andParamNameEqualTo("ins_HB_MaximumCoverageAmount");
		ins_HB_MaximumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample5);

		// ins_HB_MinimumCoverageAmount Amount

		List<HOHHParam> ins_HB_MinimumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample6 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HB_MinimumCoverageAmount = HOHHTblParamExample6.createCriteria();
		ins_HB_MinimumCoverageAmount.andParamNameEqualTo("ins_HB_MinimumCoverageAmount");
		ins_HB_MinimumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample6);

		// ins_DirectDiscount Amount

		List<HOHHParam> ins_DirectDiscountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample7 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_DirectDiscount = HOHHTblParamExample7.createCriteria();
		ins_DirectDiscount.andParamNameEqualTo("ins_DirectDiscount");
		ins_DirectDiscountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample7);

		// ins_HC_MaximumCoverageAmount Amount

		List<HOHHParam> ins_HC_MaximumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample8 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_HC_MaximumCoverageAmount = HOHHTblParamExample8.createCriteria();
		ins_HC_MaximumCoverageAmount.andParamNameEqualTo("ins_HC_MaximumCoverageAmount");
		ins_HC_MaximumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExample8);

		List<Object> originalList = new ArrayList<Object>();
		originalList.addAll(insGSTAmountList);
		originalList.addAll(ins_AdditionalBenefit_ETCAmountList);
		originalList.addAll(ins_AdditionalBenefit_RSMDAmountList);
		originalList.addAll(ins_StampDutyAmountList);
		originalList.addAll(ins_HC_MinimumCoverageAmountAmountList);
		originalList.addAll(ins_HB_MaximumCoverageAmountAmountList);
		originalList.addAll(ins_HB_MinimumCoverageAmountAmountList);
		originalList.addAll(ins_DirectDiscountAmountList);
		originalList.addAll(ins_HC_MaximumCoverageAmountAmountList);

		String gstVal = request.getParameter("GST");
		BigDecimal decgst = new BigDecimal(gstVal.replaceAll(",", ""));
		String ETC = request.getParameter("ETC");
		BigDecimal decETC = new BigDecimal(ETC.replaceAll(",", ""));
		String RSMD = request.getParameter("RSMD");
		BigDecimal decRSMD = new BigDecimal(RSMD.replaceAll(",", ""));
		String StampDuty = request.getParameter("StampDuty");
		BigDecimal decStampDuty = new BigDecimal(StampDuty.replaceAll(",", ""));
		String HCMinimumCoverageAmount = request.getParameter("HCMinimumCoverageAmount");
		BigDecimal decHCMinimumCoverageAmount = new BigDecimal(HCMinimumCoverageAmount.replaceAll(",", ""));
		String HBMaximumCoverageAmount = request.getParameter("HBMaximumCoverageAmount");
		BigDecimal decHBMaximumCoverageAmount = new BigDecimal(HBMaximumCoverageAmount.replaceAll(",", ""));
		String HBMinimumCoverageAmount = request.getParameter("HBMinimumCoverageAmount");
		BigDecimal decHBMinimumCoverageAmount = new BigDecimal(HBMinimumCoverageAmount.replaceAll(",", ""));
		String discountVal = request.getParameter("discountVal");
		BigDecimal decdiscountVal = new BigDecimal(discountVal.replaceAll(",", ""));
		String HCMaximumCoverageAmount = request.getParameter("HCMaximumCoverageAmount");
		BigDecimal decHCMaximumCoverageAmount = new BigDecimal(HCMaximumCoverageAmount.replaceAll(",", ""));

		List<String> newList = new ArrayList<String>();

		newList.add(decgst.toString());
		newList.add(decETC.toString());
		newList.add(decRSMD.toString());
		newList.add(decStampDuty.toString());
		newList.add(decHCMinimumCoverageAmount.toString());
		newList.add(decHBMaximumCoverageAmount.toString());
		newList.add(decHBMinimumCoverageAmount.toString());
		newList.add(decdiscountVal.toString());
		newList.add(decHCMaximumCoverageAmount.toString());
		newList.add(decETC.toString());

		ApprovalExample approvalExample = new ApprovalExample();
		com.spring.VO.ApprovalExample.Criteria createCriteria_approvalExample = approvalExample.createCriteria();
		createCriteria_approvalExample.andDescriptionEqualTo("HOHH INSURANCE - CHANGE PRODUCT RATE");
		List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

		try {
			byte[] originalData = convertToBytes(originalList);
			byte[] changeData = convertToBytes(newList);

			System.out.println("ORI Data" + originalData);
			System.out.println("change Data" + changeData);

			BigDecimal approvalProdId = new BigDecimal(0);
			if (listapproval.size() > 0) {
				approvalProdId = listapproval.get(0).getId();
			}

			ApprovalLog approvalLog = new ApprovalLog();
			approvalLog.setNewContent(changeData);
			approvalLog.setOriginalContent(originalData);
			approvalLog.setApprovalId((short) 8); // from Approval table this value[7] based on menu in feature
			// approvalLog.setMaker((short)1); // session login value
			approvalLog.setMaker(Short.parseShort(logedUser));
			// approvalLog.setChecker((short)1); //while insert record no need checker value
			approvalLog.setStatus("1");
			approvalLog.setCreateDate(new Date());
			// approvalLog.setUpdateDate(new Date());
			approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
			int rs = approvalLogMapper.insert(approvalLog);

			if (rs == 1) {

				model.addAttribute("UpdateMessage", "Updated data successfully");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("UpdateMessage", "Updated data failed");
		}

		String returnURL = HOHHproductrate(request, response, model);
		return returnURL;
	}

	@RequestMapping("/HOHHproductratetakafulSave")
	private String HOHHproductratetakafulSave(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		System.out.println("Product Takaful rate updated");
		String loginUser = (String) session.getAttribute("user");
		String logedUser = (String) session.getAttribute("logedUser");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		// INS GST Amount

		List<HOHHParam> instGSTAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExamplet = new HOHHParamExample();
		HOHHParamExample.Criteria HOHHParam_criteriat = HOHHTblParamExamplet.createCriteria();
		HOHHParam_criteriat.andParamNameEqualTo("TKFL_GST");
		instGSTAmountList = hohharamMapper.selectByExample(HOHHTblParamExamplet);

		// ins_AdditionalBenefit_ETC Amount

		List<HOHHParam> inst_AdditionalBenefit_ETCAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExamplet1 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_AdditionalBenefit_ETC = HOHHTblParamExamplet1.createCriteria();
		inst_AdditionalBenefit_ETC.andParamNameEqualTo("TKFL_AdditionalBenefit_ETC");
		inst_AdditionalBenefit_ETCAmountList = hohharamMapper.selectByExample(HOHHTblParamExamplet1);

		// ins_AdditionalBenefit_RSMD Amount

		List<HOHHParam> inst_AdditionalBenefit_RSMDAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExamplet2 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_AdditionalBenefit_RSMD = HOHHTblParamExamplet2.createCriteria();
		inst_AdditionalBenefit_RSMD.andParamNameEqualTo("TKFL_AdditionalBenefit_RSMD");
		inst_AdditionalBenefit_RSMDAmountList = hohharamMapper.selectByExample(HOHHTblParamExamplet2);

		// ins_StampDuty Amount

		List<HOHHParam> inst_StampDutyAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExamplet3 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_StampDuty = HOHHTblParamExamplet3.createCriteria();
		inst_StampDuty.andParamNameEqualTo("TKFL_StampDuty");
		inst_StampDutyAmountList = hohharamMapper.selectByExample(HOHHTblParamExamplet3);

		// ins_HC_MinimumCoverageAmount Amount

		List<HOHHParam> inst_HC_MinimumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExamplet4 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_HC_MinimumCoverageAmount = HOHHTblParamExamplet4.createCriteria();
		inst_HC_MinimumCoverageAmount.andParamNameEqualTo("TKFL_HC_MinimumCoverageAmount");
		inst_HC_MinimumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExamplet4);

		// ins_HB_MaximumCoverageAmount Amount

		List<HOHHParam> inst_HB_MaximumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExamplet5 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_HB_MaximumCoverageAmount = HOHHTblParamExamplet5.createCriteria();
		inst_HB_MaximumCoverageAmount.andParamNameEqualTo("TKFL_HB_MaximumCoverageAmount");
		inst_HB_MaximumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExamplet5);

		// ins_HB_MinimumCoverageAmount Amount

		List<HOHHParam> inst_HB_MinimumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExamplet6 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_HB_MinimumCoverageAmount = HOHHTblParamExamplet6.createCriteria();
		inst_HB_MinimumCoverageAmount.andParamNameEqualTo("TKFL_HB_MinimumCoverageAmount");
		inst_HB_MinimumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExamplet6);

		// ins_DirectDiscount Amount

		List<HOHHParam> inst_DirectDiscountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExamplet7 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_DirectDiscount = HOHHTblParamExamplet7.createCriteria();
		inst_DirectDiscount.andParamNameEqualTo("TKFL_DirectDiscount");
		inst_DirectDiscountAmountList = hohharamMapper.selectByExample(HOHHTblParamExamplet7);

		// ins_HC_MaximumCoverageAmount Amount

		List<HOHHParam> inst_HC_MaximumCoverageAmountAmountList = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExamplet8 = new HOHHParamExample();
		HOHHParamExample.Criteria inst_HC_MaximumCoverageAmount = HOHHTblParamExamplet8.createCriteria();
		inst_HC_MaximumCoverageAmount.andParamNameEqualTo("TKFL_HC_MaximumCoverageAmount");
		inst_HC_MaximumCoverageAmountAmountList = hohharamMapper.selectByExample(HOHHTblParamExamplet8);

		List<Object> originalList = new ArrayList<Object>();
		originalList.addAll(instGSTAmountList);
		originalList.addAll(inst_AdditionalBenefit_ETCAmountList);
		originalList.addAll(inst_AdditionalBenefit_RSMDAmountList);
		originalList.addAll(inst_StampDutyAmountList);
		originalList.addAll(inst_HC_MinimumCoverageAmountAmountList);
		originalList.addAll(inst_HB_MaximumCoverageAmountAmountList);
		originalList.addAll(inst_HB_MinimumCoverageAmountAmountList);
		originalList.addAll(inst_DirectDiscountAmountList);
		originalList.addAll(inst_HC_MaximumCoverageAmountAmountList);

		String gstVal = request.getParameter("GST");
		BigDecimal decgst = new BigDecimal(gstVal.replaceAll(",", ""));
		String ETC = request.getParameter("ETC");
		BigDecimal decETC = new BigDecimal(ETC.replaceAll(",", ""));
		String RSMD = request.getParameter("RSMD");
		BigDecimal decRSMD = new BigDecimal(RSMD.replaceAll(",", ""));
		String StampDuty = request.getParameter("StampDuty");
		BigDecimal decStampDuty = new BigDecimal(StampDuty.replaceAll(",", ""));
		String HCMinimumCoverageAmount = request.getParameter("HCMinimumCoverageAmount");
		BigDecimal decHCMinimumCoverageAmount = new BigDecimal(HCMinimumCoverageAmount.replaceAll(",", ""));
		String HBMaximumCoverageAmount = request.getParameter("HBMaximumCoverageAmount");
		BigDecimal decHBMaximumCoverageAmount = new BigDecimal(HBMaximumCoverageAmount.replaceAll(",", ""));
		String HBMinimumCoverageAmount = request.getParameter("HBMinimumCoverageAmount");
		BigDecimal decHBMinimumCoverageAmount = new BigDecimal(HBMinimumCoverageAmount.replaceAll(",", ""));
		String discountVal = request.getParameter("discountVal");
		BigDecimal decdiscountVal = new BigDecimal(discountVal.replaceAll(",", ""));
		String HCMaximumCoverageAmount = request.getParameter("HCMaximumCoverageAmount");
		BigDecimal decHCMaximumCoverageAmount = new BigDecimal(HCMaximumCoverageAmount.replaceAll(",", ""));

		List<String> newList = new ArrayList<String>();

		newList.add(decgst.toString());
		newList.add(decETC.toString());
		newList.add(decRSMD.toString());
		newList.add(decStampDuty.toString());
		newList.add(decHCMinimumCoverageAmount.toString());
		newList.add(decHBMaximumCoverageAmount.toString());
		newList.add(decHBMinimumCoverageAmount.toString());
		newList.add(decdiscountVal.toString());
		newList.add(decHCMaximumCoverageAmount.toString());
		newList.add(decETC.toString());

		ApprovalExample approvalExample = new ApprovalExample();
		com.spring.VO.ApprovalExample.Criteria createCriteria_approvalExample = approvalExample.createCriteria();
		createCriteria_approvalExample.andDescriptionEqualTo("HOHH TAKAFUL - CHANGE PRODUCT RATE");
		List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

		try {
			byte[] originalData = convertToBytes(originalList);
			byte[] changeData = convertToBytes(newList);

			System.out.println("ORI Data" + originalData);
			System.out.println("change Data" + changeData);

			BigDecimal approvalProdId = new BigDecimal(0);
			if (listapproval.size() > 0) {
				approvalProdId = listapproval.get(0).getId();
			}

			ApprovalLog approvalLog = new ApprovalLog();
			approvalLog.setNewContent(changeData);
			approvalLog.setOriginalContent(originalData);
			approvalLog.setApprovalId((short) 6); // from Approval table this value[7] based on menu in feature
			// approvalLog.setMaker((short)1); // session login value
			approvalLog.setMaker(Short.parseShort(logedUser));
			approvalLog.setStatus("1");
			approvalLog.setCreateDate(new Date());
			// approvalLog.setUpdateDate(new Date());
			approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
			int rs = approvalLogMapper.insert(approvalLog);

			if (rs == 1) {
				model.addAttribute("UpdateMessage", "Updated data successfully");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("UpdateMessage", "Updated data failed");
		}

		String returnURL = HOHHproductrate(request, response, model);
		return returnURL;
	}

	private byte[] convertToBytes(Object object) throws IOException {
		try (ByteArrayOutputStream bos = new ByteArrayOutputStream(); ObjectOutput out = new ObjectOutputStream(bos)) {
			out.writeObject(object);
			return bos.toByteArray();
		}
	}

	private Object convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
		try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes); ObjectInput in = new ObjectInputStream(bis)) {
			return in.readObject();
		}
	}

	@RequestMapping(value = "/approvalHOHHInsProduct", method = RequestMethod.GET)
	public String approvalHOHHInsProduct(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Reequest " + request.getParameter("id"));
		ApprovalLogExample approvalexample = new ApprovalLogExample();
		com.spring.VO.ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();
		Integer testint = new Integer(request.getParameter("id"));
		createCriteria.andIdEqualTo(testint.shortValue());
		List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
		System.out.println("   list size in approval list  :::   " + listapprovallog.size());
		// System.out.println("Testing Original Content
		// "+listapprovallog.get(0).getOriginalContent());

		try {
			// original data
			if (listapprovallog.size() > 0) {
				Object originalData = convertFromBytes(listapprovallog.get(0).getOriginalContent());
				List<Object> listOriginalData = new ArrayList<Object>();
				// objecct convert into AgentProdMap
				if (originalData != null) {
					listOriginalData = (List<Object>) originalData;

					HOHHInsProductApprovalRate hohhProductApprovalRate = new HOHHInsProductApprovalRate();
					hohhProductApprovalRate.setGst(((HOHHParam) listOriginalData.get(0)).getParamValue().toString());
					hohhProductApprovalRate.setTheft(((HOHHParam) listOriginalData.get(1)).getParamValue().toString());
					hohhProductApprovalRate.setRiot(((HOHHParam) listOriginalData.get(2)).getParamValue().toString());
					hohhProductApprovalRate
							.setStampDuty(((HOHHParam) listOriginalData.get(3)).getParamValue().toString());
					hohhProductApprovalRate
							.setMinAmountContent(((HOHHParam) listOriginalData.get(4)).getParamValue().toString());
					hohhProductApprovalRate
							.setMaxAmountBuild(((HOHHParam) listOriginalData.get(5)).getParamValue().toString());
					hohhProductApprovalRate
							.setMinAmountBuild(((HOHHParam) listOriginalData.get(6)).getParamValue().toString());
					hohhProductApprovalRate
							.setDirectDiscount(((HOHHParam) listOriginalData.get(7)).getParamValue().toString());
					hohhProductApprovalRate
							.setMaxAmountContent(((HOHHParam) listOriginalData.get(8)).getParamValue().toString());

					List<HOHHInsProductApprovalRate> listHoHHInsOriginalData = new ArrayList<HOHHInsProductApprovalRate>();
					listHoHHInsOriginalData.add(hohhProductApprovalRate);
					model.addAttribute("listHoHHInsOriginalData", listHoHHInsOriginalData);

					System.out.println("-----------------------------");
				} // inner if
			} // outer if

			// Change Data
			if (listapprovallog.size() > 0) {
				Object changeData = convertFromBytes(listapprovallog.get(0).getNewContent());
				List<String> listChangeData = new ArrayList<String>();
				// objecct convert into AgentProdMap
				if (changeData != null) {
					listChangeData = (List<String>) changeData;
					System.out.println("  list agent prod map size ::::::         " + listChangeData.size());

					HOHHInsProductApprovalRate hohhProductApprovalRate = new HOHHInsProductApprovalRate();
					hohhProductApprovalRate.setGst(listChangeData.get(0));
					hohhProductApprovalRate.setTheft(listChangeData.get(1));
					hohhProductApprovalRate.setRiot(listChangeData.get(2));
					hohhProductApprovalRate.setStampDuty(listChangeData.get(3));
					hohhProductApprovalRate.setMinAmountContent(listChangeData.get(4));
					hohhProductApprovalRate.setMaxAmountBuild(listChangeData.get(5));
					hohhProductApprovalRate.setMinAmountBuild(listChangeData.get(6));
					hohhProductApprovalRate.setDirectDiscount(listChangeData.get(7));
					hohhProductApprovalRate.setMaxAmountContent(listChangeData.get(8));

					System.out.println("val-->" + hohhProductApprovalRate.getRiot());

					List<HOHHInsProductApprovalRate> listChangeHOHHInsData = new ArrayList<HOHHInsProductApprovalRate>();

					listChangeHOHHInsData.add(hohhProductApprovalRate);
					model.addAttribute("listChangeHOHHInsData", listChangeHOHHInsData);
					System.out.println("-----------------------------");
				}
			}
		} // try end
		catch (Exception e) {
			System.out.println("Exception in Convert Object to Byte" + e);
		}

		return "agent-product-approval";
	}

	@RequestMapping(value = "/approvalHOHHTakafulProduct", method = RequestMethod.GET)
	public String approvalHOHHTakafulProduct(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Reequest " + request.getParameter("id"));
		ApprovalLogExample approvalexample = new ApprovalLogExample();
		com.spring.VO.ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();
		Integer testint = new Integer(request.getParameter("id"));
		createCriteria.andIdEqualTo(testint.shortValue());
		List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
		System.out.println("   list size in approval list  :::   " + listapprovallog.size());
		// System.out.println("Testing Original Content
		// "+listapprovallog.get(0).getOriginalContent());

		try {
			// original data
			if (listapprovallog.size() > 0) {
				Object originalData = convertFromBytes(listapprovallog.get(0).getOriginalContent());
				List<Object> listOriginalData = new ArrayList<Object>();
				// objecct convert into AgentProdMap
				if (originalData != null) {
					listOriginalData = (List<Object>) originalData;

					HOHHInsProductApprovalRate hohhProductApprovalRate = new HOHHInsProductApprovalRate();
					hohhProductApprovalRate.setGst(((HOHHParam) listOriginalData.get(0)).getParamValue().toString());
					hohhProductApprovalRate.setTheft(((HOHHParam) listOriginalData.get(1)).getParamValue().toString());
					hohhProductApprovalRate.setRiot(((HOHHParam) listOriginalData.get(2)).getParamValue().toString());
					hohhProductApprovalRate
							.setStampDuty(((HOHHParam) listOriginalData.get(3)).getParamValue().toString());
					hohhProductApprovalRate
							.setMinAmountContent(((HOHHParam) listOriginalData.get(4)).getParamValue().toString());
					hohhProductApprovalRate
							.setMaxAmountBuild(((HOHHParam) listOriginalData.get(5)).getParamValue().toString());
					hohhProductApprovalRate
							.setMinAmountBuild(((HOHHParam) listOriginalData.get(6)).getParamValue().toString());
					hohhProductApprovalRate
							.setDirectDiscount(((HOHHParam) listOriginalData.get(7)).getParamValue().toString());
					hohhProductApprovalRate
							.setMaxAmountContent(((HOHHParam) listOriginalData.get(8)).getParamValue().toString());

					List<HOHHInsProductApprovalRate> listHoHHTakafulOriginalData = new ArrayList<HOHHInsProductApprovalRate>();
					listHoHHTakafulOriginalData.add(hohhProductApprovalRate);
					model.addAttribute("listHoHHTakafulOriginalData", listHoHHTakafulOriginalData);

					System.out.println("-----------------------------");
				} // inner if
			} // outer if

			// Change Data
			if (listapprovallog.size() > 0) {
				Object changeData = convertFromBytes(listapprovallog.get(0).getNewContent());
				List<String> listChangeData = new ArrayList<String>();
				// objecct convert into AgentProdMap
				if (changeData != null) {
					listChangeData = (List<String>) changeData;
					System.out.println("  list agent prod map size ::::::         " + listChangeData.size());

					HOHHInsProductApprovalRate hohhProductApprovalRate = new HOHHInsProductApprovalRate();
					hohhProductApprovalRate.setGst(listChangeData.get(0));
					hohhProductApprovalRate.setTheft(listChangeData.get(1));
					hohhProductApprovalRate.setRiot(listChangeData.get(2));
					hohhProductApprovalRate.setStampDuty(listChangeData.get(3));
					hohhProductApprovalRate.setMinAmountContent(listChangeData.get(4));
					hohhProductApprovalRate.setMaxAmountBuild(listChangeData.get(5));
					hohhProductApprovalRate.setMinAmountBuild(listChangeData.get(6));
					hohhProductApprovalRate.setDirectDiscount(listChangeData.get(7));
					hohhProductApprovalRate.setMaxAmountContent(listChangeData.get(8));

					System.out.println("val-->" + hohhProductApprovalRate.getRiot());

					List<HOHHInsProductApprovalRate> listChangeHOHHTakafulData = new ArrayList<HOHHInsProductApprovalRate>();

					listChangeHOHHTakafulData.add(hohhProductApprovalRate);
					model.addAttribute("listChangeHOHHTakafulData", listChangeHOHHTakafulData);
					System.out.println("-----------------------------");
				}
			}
		} // try end
		catch (Exception e) {
			System.out.println("Exception in Convert Object to Byte" + e);
		}

		return "agent-product-approval";
	}

	@RequestMapping("/HOHHproductrateUpdateApproval")
	private String HOHHproductrateUpdateApproval(HttpServletRequest request, HttpServletResponse response,
			Model model) {
		HttpSession session = request.getSession();
		System.out.println("Product rate updated");
		String loginUser = (String) session.getAttribute("user");
		String logedUser = (String) session.getAttribute("logedUser");

		String returnURL = "";
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		try {

			System.out.println("Reequest " + request.getParameter("id"));
			String aid = request.getParameter("approvalLogId");
			System.out.println("aid >>>>>" + aid);

			ApprovalLogExample approvalexample = new ApprovalLogExample();
			ApprovalLog alog = new ApprovalLog();
			ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();

			if (null != aid && !aid.isEmpty()) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
				System.out.println("   list size in approval list  :::   " + listapprovallog.size());
				// System.out.println("Testing Original Content
				// "+listapprovallog.get(0).getOriginalContent());

				// Change Data
				if (listapprovallog.size() > 0) {

					String status = listapprovallog.get(0).getStatus();
					short alogId = listapprovallog.get(0).getId();
					System.out.println("Status  :  " + status);
					System.out.println("Approvallog Id  :  " + alogId);

					Object changeData = convertFromBytes(listapprovallog.get(0).getNewContent());
					List<String> listChangeData = new ArrayList<String>();
					// objecct convert into AgentProdMap
					if (changeData != null) {
						listChangeData = (List<String>) changeData;
						System.out.println("  list agent prod map size ::::::         " + listChangeData.size());

						HOHHInsProductApprovalRate hohhProductApprovalRate = new HOHHInsProductApprovalRate();
						hohhProductApprovalRate.setGst(listChangeData.get(0));
						hohhProductApprovalRate.setTheft(listChangeData.get(1));
						hohhProductApprovalRate.setRiot(listChangeData.get(2));
						hohhProductApprovalRate.setStampDuty(listChangeData.get(3));
						hohhProductApprovalRate.setMinAmountContent(listChangeData.get(4));
						hohhProductApprovalRate.setMaxAmountBuild(listChangeData.get(5));
						hohhProductApprovalRate.setMinAmountBuild(listChangeData.get(6));
						hohhProductApprovalRate.setDirectDiscount(listChangeData.get(7));
						hohhProductApprovalRate.setMaxAmountContent(listChangeData.get(8));

						// INS GST Amount

						HOHHParam tlTblParam4 = new HOHHParam();
						HOHHParamExample HOHHTblParamExample = new HOHHParamExample();
						HOHHParamExample.Criteria HOHHParam_criteria = HOHHTblParamExample.createCriteria();
						HOHHParam_criteria.andParamNameEqualTo("ins_GST");

						String gstVal = hohhProductApprovalRate.getGst();
						BigDecimal decgst = new BigDecimal(gstVal.replaceAll(",", ""));
						System.out.println("GST VALUE   :" + gstVal);
						tlTblParam4.setParamValue(decgst);
						int tlTblParamUpdate4 = hohharamMapper.updateByExampleSelective(tlTblParam4,
								HOHHTblParamExample);
						System.out.println("Check GST          :" + tlTblParamUpdate4);

						// ins_AdditionalBenefit_ETC Amount

						HOHHParam ins_AdditionalBenefit_ETCAmountList = new HOHHParam();
						HOHHParamExample HOHHTblParamExample1 = new HOHHParamExample();
						HOHHParamExample.Criteria ins_AdditionalBenefit_ETC = HOHHTblParamExample1.createCriteria();
						ins_AdditionalBenefit_ETC.andParamNameEqualTo("ins_AdditionalBenefit_ETC");

						String ETC = hohhProductApprovalRate.getTheft();
						BigDecimal decETC = new BigDecimal(ETC.replaceAll(",", ""));
						System.out.println("ETC VALUE   :" + ETC);
						ins_AdditionalBenefit_ETCAmountList.setParamValue(decETC);
						int ETCETC = hohharamMapper.updateByExampleSelective(ins_AdditionalBenefit_ETCAmountList,
								HOHHTblParamExample1);
						System.out.println("Check ETC          :" + ETCETC);

						// ins_AdditionalBenefit_RSMD Amount

						HOHHParam ins_AdditionalBenefit_RSMDAmountList = new HOHHParam();
						HOHHParamExample HOHHTblParamExample2 = new HOHHParamExample();
						HOHHParamExample.Criteria ins_AdditionalBenefit_RSMD = HOHHTblParamExample2.createCriteria();
						ins_AdditionalBenefit_RSMD.andParamNameEqualTo("ins_AdditionalBenefit_RSMD");

						String RSMD = hohhProductApprovalRate.getRiot();
						BigDecimal decRSMD = new BigDecimal(RSMD.replaceAll(",", ""));
						System.out.println("ETC VALUE   :" + RSMD);
						ins_AdditionalBenefit_RSMDAmountList.setParamValue(decRSMD);
						int RSMDRSMD = hohharamMapper.updateByExampleSelective(ins_AdditionalBenefit_RSMDAmountList,
								HOHHTblParamExample2);
						System.out.println("Check RSMD          :" + RSMDRSMD);

						// ins_StampDuty Amount

						HOHHParam ins_StampDutyAmountList = new HOHHParam();
						HOHHParamExample HOHHTblParamExample3 = new HOHHParamExample();
						HOHHParamExample.Criteria ins_StampDuty = HOHHTblParamExample3.createCriteria();
						ins_StampDuty.andParamNameEqualTo("ins_StampDuty");

						String StampDuty = hohhProductApprovalRate.getStampDuty();
						BigDecimal decStampDuty = new BigDecimal(StampDuty.replaceAll(",", ""));
						System.out.println("StampDuty VALUE   :" + StampDuty);
						ins_StampDutyAmountList.setParamValue(decStampDuty);
						int StampDutyStampDuty = hohharamMapper.updateByExampleSelective(ins_StampDutyAmountList,
								HOHHTblParamExample3);
						System.out.println("Check StampDuty         :" + StampDutyStampDuty);

						// ins_HC_MinimumCoverageAmount Amount

						HOHHParam ins_HC_MinimumCoverageAmountAmountList = new HOHHParam();
						HOHHParamExample HOHHTblParamExample4 = new HOHHParamExample();
						HOHHParamExample.Criteria ins_HC_MinimumCoverageAmount = HOHHTblParamExample4.createCriteria();
						ins_HC_MinimumCoverageAmount.andParamNameEqualTo("ins_HC_MinimumCoverageAmount");

						String HCMinimumCoverageAmount = hohhProductApprovalRate.getMinAmountContent();
						BigDecimal decHCMinimumCoverageAmount = new BigDecimal(
								HCMinimumCoverageAmount.replaceAll(",", ""));
						System.out.println("HCMinimumCoverageAmount VALUE   :" + HCMinimumCoverageAmount);
						ins_HC_MinimumCoverageAmountAmountList.setParamValue(decHCMinimumCoverageAmount);
						int HCMinimumCoverageAmountHCMinimumCoverageAmount = hohharamMapper
								.updateByExampleSelective(ins_HC_MinimumCoverageAmountAmountList, HOHHTblParamExample4);
						System.out.println("Check HCMinimumCoverageAmount         :"
								+ HCMinimumCoverageAmountHCMinimumCoverageAmount);

						// ins_HB_MaximumCoverageAmount Amount

						HOHHParam ins_HB_MaximumCoverageAmountAmountList = new HOHHParam();
						HOHHParamExample HOHHTblParamExample5 = new HOHHParamExample();
						HOHHParamExample.Criteria ins_HB_MaximumCoverageAmount = HOHHTblParamExample5.createCriteria();
						ins_HB_MaximumCoverageAmount.andParamNameEqualTo("ins_HB_MaximumCoverageAmount");

						String HBMaximumCoverageAmount = hohhProductApprovalRate.getMaxAmountBuild();
						BigDecimal decHBMaximumCoverageAmount = new BigDecimal(
								HBMaximumCoverageAmount.replaceAll(",", ""));
						System.out.println("HCMinimumCoverageAmount VALUE   :" + decHBMaximumCoverageAmount);
						ins_HB_MaximumCoverageAmountAmountList.setParamValue(decHBMaximumCoverageAmount);
						int HBMaximumCoverageAmountHBMaximumCoverageAmount = hohharamMapper
								.updateByExampleSelective(ins_HB_MaximumCoverageAmountAmountList, HOHHTblParamExample5);
						System.out.println("Check HBMaximumCoverageAmount         :"
								+ HBMaximumCoverageAmountHBMaximumCoverageAmount);

						// ins_HB_MinimumCoverageAmount Amount

						HOHHParam ins_HB_MinimumCoverageAmountAmountList = new HOHHParam();
						HOHHParamExample HOHHTblParamExample6 = new HOHHParamExample();
						HOHHParamExample.Criteria ins_HB_MinimumCoverageAmount = HOHHTblParamExample6.createCriteria();
						ins_HB_MinimumCoverageAmount.andParamNameEqualTo("ins_HB_MinimumCoverageAmount");

						String HBMinimumCoverageAmount = hohhProductApprovalRate.getMinAmountBuild();
						BigDecimal decHBMinimumCoverageAmount = new BigDecimal(
								HBMinimumCoverageAmount.replaceAll(",", ""));
						System.out.println("HBMinimumCoverageAmount VALUE   :" + decHBMinimumCoverageAmount);
						ins_HB_MinimumCoverageAmountAmountList.setParamValue(decHBMinimumCoverageAmount);
						int HBMinimumCoverageAmountHBMinimumCoverageAmount = hohharamMapper
								.updateByExampleSelective(ins_HB_MinimumCoverageAmountAmountList, HOHHTblParamExample6);
						System.out.println("Check HBMinimumCoverageAmount         :"
								+ HBMinimumCoverageAmountHBMinimumCoverageAmount);

						// ins_DirectDiscount Amount

						HOHHParam ins_DirectDiscountAmountList = new HOHHParam();
						HOHHParamExample HOHHTblParamExample7 = new HOHHParamExample();
						HOHHParamExample.Criteria ins_DirectDiscount = HOHHTblParamExample7.createCriteria();
						ins_DirectDiscount.andParamNameEqualTo("ins_DirectDiscount");

						String discountVal = hohhProductApprovalRate.getDirectDiscount();
						BigDecimal decdiscountVal = new BigDecimal(discountVal.replaceAll(",", ""));
						System.out.println("discountVal VALUE   :" + decdiscountVal);
						ins_DirectDiscountAmountList.setParamValue(decdiscountVal);
						int discountValdiscountVal = hohharamMapper
								.updateByExampleSelective(ins_DirectDiscountAmountList, HOHHTblParamExample7);
						System.out.println("Check discountVal         :" + discountValdiscountVal);

						// ins_HC_MaximumCoverageAmount Amount

						HOHHParam ins_HC_MaximumCoverageAmountAmountList = new HOHHParam();
						HOHHParamExample HOHHTblParamExample8 = new HOHHParamExample();
						HOHHParamExample.Criteria ins_HC_MaximumCoverageAmount = HOHHTblParamExample8.createCriteria();
						ins_HC_MaximumCoverageAmount.andParamNameEqualTo("ins_HC_MaximumCoverageAmount");

						String HCMaximumCoverageAmount = hohhProductApprovalRate.getMaxAmountContent();
						BigDecimal decHCMaximumCoverageAmount = new BigDecimal(
								HCMaximumCoverageAmount.replaceAll(",", ""));
						System.out.println("HCMaximumCoverageAmount VALUE   :" + decHCMaximumCoverageAmount);
						ins_HC_MaximumCoverageAmountAmountList.setParamValue(decHCMaximumCoverageAmount);
						int HCMaximumCoverageAmountHCMaximumCoverageAmount = hohharamMapper
								.updateByExampleSelective(ins_HC_MaximumCoverageAmountAmountList, HOHHTblParamExample8);
						System.out.println("Check HcMaximumCoverageAmount         :"
								+ HCMaximumCoverageAmountHCMaximumCoverageAmount);
						// Update status in DSP_AFM_TBL_Approval_Log table
						alog.setId(alogId);
						alog.setStatus("3");
						// alog.setChecker((short) 1); //get from the current login session
						alog.setChecker(Short.parseShort(logedUser));
						alog.setUpdateDate(new Date());
						int rs = approvalLogMapper.updateByPrimaryKeySelective(alog);

						if (rs == 1) {

							model.addAttribute("approvemessage", "Approved Data Successfully");
						}

						AgentController agentController = new AgentController(agentProfileMapper, productsMapper,
								agentProdMapMapper, commonQQMapper, adminParamMapper, agentDocumentMapper,
								agentLinkMapper, approvalLogMapper, approvalMapper);

						returnURL = agentController.getAgentProductMapApproval(request, response, model);

					} // end forchangeData
				} // end for listapprovallog
			} // Fot if aid
		} // Try end
		catch (Exception e) {
			System.out.println("Exception In >> HOHH product rates update done funtion " + e);
			model.addAttribute("approvemessage", "Approve Data Failed");
		}
		return returnURL;
		// return "redirect:/getApprovalList";
	}

	@RequestMapping("/HOHHproductratetakafulUpdateApproval")
	private String HOHHproductratetakafulUpdateApproval(HttpServletRequest request, HttpServletResponse response,
			Model model) {
		HttpSession session = request.getSession();
		System.out.println("Product rate updated");
		String loginUser = (String) session.getAttribute("user");
		String logedUser = (String) session.getAttribute("logedUser");
		String returnURL = "";
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		try {

			System.out.println("Reequest " + request.getParameter("id"));
			String aid = request.getParameter("approvalLogId");
			System.out.println("aid >>>>>" + aid);

			ApprovalLogExample approvalexample = new ApprovalLogExample();
			ApprovalLog alog = new ApprovalLog();
			ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();

			if (null != aid && !aid.isEmpty()) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
				System.out.println("   list size in approval list  :::   " + listapprovallog.size());
				// System.out.println("Testing Original Content
				// "+listapprovallog.get(0).getOriginalContent());

				// Change Data
				if (listapprovallog.size() > 0) {

					String status = listapprovallog.get(0).getStatus();
					short alogId = listapprovallog.get(0).getId();
					System.out.println("Status  :  " + status);
					System.out.println("Approvallog Id  :  " + alogId);

					Object changeData = convertFromBytes(listapprovallog.get(0).getNewContent());
					List<String> listChangeData = new ArrayList<String>();
					// objecct convert into AgentProdMap
					if (changeData != null) {
						listChangeData = (List<String>) changeData;
						System.out.println("  list agent prod map size ::::::         " + listChangeData.size());

						HOHHInsProductApprovalRate hohhProductApprovalRate = new HOHHInsProductApprovalRate();
						hohhProductApprovalRate.setGst(listChangeData.get(0));
						hohhProductApprovalRate.setTheft(listChangeData.get(1));
						hohhProductApprovalRate.setRiot(listChangeData.get(2));
						hohhProductApprovalRate.setStampDuty(listChangeData.get(3));
						hohhProductApprovalRate.setMinAmountContent(listChangeData.get(4));
						hohhProductApprovalRate.setMaxAmountBuild(listChangeData.get(5));
						hohhProductApprovalRate.setMinAmountBuild(listChangeData.get(6));
						hohhProductApprovalRate.setDirectDiscount(listChangeData.get(7));
						hohhProductApprovalRate.setMaxAmountContent(listChangeData.get(8));

						// INS GST Amount

						HOHHParam tlTblParam4 = new HOHHParam();
						HOHHParamExample HOHHTblParamExample = new HOHHParamExample();
						HOHHParamExample.Criteria HOHHParam_criteria = HOHHTblParamExample.createCriteria();
						HOHHParam_criteria.andParamNameEqualTo("TKFL_GST");

						String gstVal = hohhProductApprovalRate.getGst();
						System.out.println("GST VALUE   :" + gstVal);
						BigDecimal decgst = new BigDecimal(gstVal.replaceAll(",", ""));
						System.out.println("GST VALUE   :" + gstVal);
						tlTblParam4.setParamValue(decgst);
						int tlTblParamUpdate4 = hohharamMapper.updateByExampleSelective(tlTblParam4,
								HOHHTblParamExample);
						System.out.println("Check GST          :" + tlTblParamUpdate4);

						// ins_AdditionalBenefit_ETC Amount

						HOHHParam ins_AdditionalBenefit_ETCAmountList = new HOHHParam();
						HOHHParamExample HOHHTblParamExample1 = new HOHHParamExample();
						HOHHParamExample.Criteria ins_AdditionalBenefit_ETC = HOHHTblParamExample1.createCriteria();
						ins_AdditionalBenefit_ETC.andParamNameEqualTo("TKFL_AdditionalBenefit_ETC");

						String ETC = hohhProductApprovalRate.getTheft();
						BigDecimal decETC = new BigDecimal(ETC.replaceAll(",", ""));
						System.out.println("ETC VALUE   :" + ETC);
						ins_AdditionalBenefit_ETCAmountList.setParamValue(decETC);
						int ETCETC = hohharamMapper.updateByExampleSelective(ins_AdditionalBenefit_ETCAmountList,
								HOHHTblParamExample1);
						System.out.println("Check ETC          :" + ETCETC);

						// ins_AdditionalBenefit_RSMD Amount

						HOHHParam ins_AdditionalBenefit_RSMDAmountList = new HOHHParam();
						HOHHParamExample HOHHTblParamExample2 = new HOHHParamExample();
						HOHHParamExample.Criteria ins_AdditionalBenefit_RSMD = HOHHTblParamExample2.createCriteria();
						ins_AdditionalBenefit_RSMD.andParamNameEqualTo("TKFL_AdditionalBenefit_RSMD");

						String RSMD = hohhProductApprovalRate.getRiot();
						BigDecimal decRSMD = new BigDecimal(RSMD.replaceAll(",", ""));
						System.out.println("ETC VALUE   :" + RSMD);
						ins_AdditionalBenefit_RSMDAmountList.setParamValue(decRSMD);
						int RSMDRSMD = hohharamMapper.updateByExampleSelective(ins_AdditionalBenefit_RSMDAmountList,
								HOHHTblParamExample2);
						System.out.println("Check RSMD          :" + RSMDRSMD);

						// ins_StampDuty Amount

						HOHHParam ins_StampDutyAmountList = new HOHHParam();
						HOHHParamExample HOHHTblParamExample3 = new HOHHParamExample();
						HOHHParamExample.Criteria ins_StampDuty = HOHHTblParamExample3.createCriteria();
						ins_StampDuty.andParamNameEqualTo("TKFL_StampDuty");

						String StampDuty = hohhProductApprovalRate.getStampDuty();
						BigDecimal decStampDuty = new BigDecimal(StampDuty.replaceAll(",", ""));
						System.out.println("StampDuty VALUE   :" + StampDuty);
						ins_StampDutyAmountList.setParamValue(decStampDuty);
						int StampDutyStampDuty = hohharamMapper.updateByExampleSelective(ins_StampDutyAmountList,
								HOHHTblParamExample3);
						System.out.println("Check StampDuty         :" + StampDutyStampDuty);

						// ins_HC_MinimumCoverageAmount Amount

						HOHHParam ins_HC_MinimumCoverageAmountAmountList = new HOHHParam();
						HOHHParamExample HOHHTblParamExample4 = new HOHHParamExample();
						HOHHParamExample.Criteria ins_HC_MinimumCoverageAmount = HOHHTblParamExample4.createCriteria();
						ins_HC_MinimumCoverageAmount.andParamNameEqualTo("TKFL_HC_MinimumCoverageAmount");

						String HCMinimumCoverageAmount = hohhProductApprovalRate.getMinAmountContent();
						BigDecimal decHCMinimumCoverageAmount = new BigDecimal(
								HCMinimumCoverageAmount.replaceAll(",", ""));
						System.out.println("HCMinimumCoverageAmount VALUE   :" + HCMinimumCoverageAmount);
						ins_HC_MinimumCoverageAmountAmountList.setParamValue(decHCMinimumCoverageAmount);
						int HCMinimumCoverageAmountHCMinimumCoverageAmount = hohharamMapper
								.updateByExampleSelective(ins_HC_MinimumCoverageAmountAmountList, HOHHTblParamExample4);
						System.out.println("Check HCMinimumCoverageAmount         :"
								+ HCMinimumCoverageAmountHCMinimumCoverageAmount);

						// ins_HB_MaximumCoverageAmount Amount

						HOHHParam ins_HB_MaximumCoverageAmountAmountList = new HOHHParam();
						HOHHParamExample HOHHTblParamExample5 = new HOHHParamExample();
						HOHHParamExample.Criteria ins_HB_MaximumCoverageAmount = HOHHTblParamExample5.createCriteria();
						ins_HB_MaximumCoverageAmount.andParamNameEqualTo("TKFL_HB_MaximumCoverageAmount");

						String HBMaximumCoverageAmount = hohhProductApprovalRate.getMaxAmountBuild();
						BigDecimal decHBMaximumCoverageAmount = new BigDecimal(
								HBMaximumCoverageAmount.replaceAll(",", ""));
						System.out.println("HCMinimumCoverageAmount VALUE   :" + decHBMaximumCoverageAmount);
						ins_HB_MaximumCoverageAmountAmountList.setParamValue(decHBMaximumCoverageAmount);
						int HBMaximumCoverageAmountHBMaximumCoverageAmount = hohharamMapper
								.updateByExampleSelective(ins_HB_MaximumCoverageAmountAmountList, HOHHTblParamExample5);
						System.out.println("Check HBMaximumCoverageAmount         :"
								+ HBMaximumCoverageAmountHBMaximumCoverageAmount);

						// ins_HB_MinimumCoverageAmount Amount

						HOHHParam ins_HB_MinimumCoverageAmountAmountList = new HOHHParam();
						HOHHParamExample HOHHTblParamExample6 = new HOHHParamExample();
						HOHHParamExample.Criteria ins_HB_MinimumCoverageAmount = HOHHTblParamExample6.createCriteria();
						ins_HB_MinimumCoverageAmount.andParamNameEqualTo("TKFL_HB_MinimumCoverageAmount");

						String HBMinimumCoverageAmount = hohhProductApprovalRate.getMinAmountBuild();
						BigDecimal decHBMinimumCoverageAmount = new BigDecimal(
								HBMinimumCoverageAmount.replaceAll(",", ""));
						System.out.println("HBMinimumCoverageAmount VALUE   :" + decHBMinimumCoverageAmount);
						ins_HB_MinimumCoverageAmountAmountList.setParamValue(decHBMinimumCoverageAmount);
						int HBMinimumCoverageAmountHBMinimumCoverageAmount = hohharamMapper
								.updateByExampleSelective(ins_HB_MinimumCoverageAmountAmountList, HOHHTblParamExample6);
						System.out.println("Check HBMinimumCoverageAmount         :"
								+ HBMinimumCoverageAmountHBMinimumCoverageAmount);

						// ins_DirectDiscount Amount

						HOHHParam ins_DirectDiscountAmountList = new HOHHParam();
						HOHHParamExample HOHHTblParamExample7 = new HOHHParamExample();
						HOHHParamExample.Criteria ins_DirectDiscount = HOHHTblParamExample7.createCriteria();
						ins_DirectDiscount.andParamNameEqualTo("TKFL_DirectDiscount");

						String discountVal = hohhProductApprovalRate.getDirectDiscount();
						BigDecimal decdiscountVal = new BigDecimal(discountVal.replaceAll(",", ""));
						System.out.println("discountVal VALUE   :" + decdiscountVal);
						ins_DirectDiscountAmountList.setParamValue(decdiscountVal);
						int discountValdiscountVal = hohharamMapper
								.updateByExampleSelective(ins_DirectDiscountAmountList, HOHHTblParamExample7);
						System.out.println("Check discountVal         :" + discountValdiscountVal);

						// ins_HC_MaximumCoverageAmount Amount

						HOHHParam ins_HC_MaximumCoverageAmountAmountList = new HOHHParam();
						HOHHParamExample HOHHTblParamExample8 = new HOHHParamExample();
						HOHHParamExample.Criteria ins_HC_MaximumCoverageAmount = HOHHTblParamExample8.createCriteria();
						ins_HC_MaximumCoverageAmount.andParamNameEqualTo("TKFL_HC_MaximumCoverageAmount");

						String HCMaximumCoverageAmount = hohhProductApprovalRate.getMinAmountContent();
						BigDecimal decHCMaximumCoverageAmount = new BigDecimal(
								HCMaximumCoverageAmount.replaceAll(",", ""));
						System.out.println("HCMaximumCoverageAmount VALUE   :" + decHCMaximumCoverageAmount);
						ins_HC_MaximumCoverageAmountAmountList.setParamValue(decHCMaximumCoverageAmount);
						int HCMaximumCoverageAmountHCMaximumCoverageAmount = hohharamMapper
								.updateByExampleSelective(ins_HC_MaximumCoverageAmountAmountList, HOHHTblParamExample8);
						System.out.println("Check HCMaximumCoverageAmount         :"
								+ HCMaximumCoverageAmountHCMaximumCoverageAmount);

						// Update status in DSP_AFM_TBL_Approval_Log table
						alog.setId(alogId);
						alog.setStatus("3");
						// alog.setChecker((short) 1); //get from the current login session
						alog.setChecker(Short.parseShort(logedUser));
						alog.setUpdateDate(new Date());
						int rs = approvalLogMapper.updateByPrimaryKeySelective(alog);

						if (rs == 1) {

							model.addAttribute("approvemessage", "Approved Data Successfully");
						}

						AgentController agentController = new AgentController(agentProfileMapper, productsMapper,
								agentProdMapMapper, commonQQMapper, adminParamMapper, agentDocumentMapper,
								agentLinkMapper, approvalLogMapper, approvalMapper);

						returnURL = agentController.getAgentProductMapApproval(request, response, model);

					} // end forchangeData
				} // end for listapprovallog
			} // Fot if aid
		} // Try end
		catch (Exception e) {
			System.out.println("Exception In >> HOHH product rates update done funtion " + e);
			model.addAttribute("approvemessage", "Approve Data Failed");
		}
		return returnURL;

	}

	@RequestMapping(value = "/HOHHproductrateReject", method = RequestMethod.GET)
	public String HOHHproductrateReject(@ModelAttribute AgentProfile agentProfile, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		String logedUser = (String) session.getAttribute("logedUser");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		int rs = 0;
		ApprovalLogExample approvalLogexample = new ApprovalLogExample();
		ApprovalLog alog = new ApprovalLog();
		ApprovalLogExample.Criteria createCriteria = approvalLogexample.createCriteria();
		String aid = request.getParameter("approvalLogId");
		if (null != aid && !aid.isEmpty()) {
			Integer testint = new Integer(aid);
			createCriteria.andIdEqualTo(testint.shortValue());
			List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalLogexample);
			System.out.println("   list size in approval list  :::   " + listapprovallog.size());

			short alogId = listapprovallog.get(0).getId();
			alog.setId(alogId);
			alog.setStatus("2");
			// alog.setChecker((short) 1); //get from the current login session
			alog.setChecker(Short.parseShort(logedUser));
			alog.setUpdateDate(new Date());
			rs = approvalLogMapper.updateByPrimaryKeySelective(alog);
		}

		if (rs == 1) {

			model.addAttribute("rejectmessage", "Rejected Data Successfully");
		}

		else {

			model.addAttribute("rejectmessage", "Reject Data Failed");
		}

		AgentController agentController = new AgentController(agentProfileMapper, productsMapper, agentProdMapMapper,
				commonQQMapper, adminParamMapper, agentDocumentMapper, agentLinkMapper, approvalLogMapper,
				approvalMapper);

		String returnURL = agentController.getAgentProductMapApproval(request, response, model);
		;
		return returnURL;
	}

	@RequestMapping(value = "/HOHHproductratetakafulReject", method = RequestMethod.GET)
	public String HOHHproductratetakafulReject(@ModelAttribute AgentProfile agentProfile, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		String logedUser = (String) session.getAttribute("logedUser");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		int rs = 0;

		ApprovalLogExample approvalLogexample = new ApprovalLogExample();
		ApprovalLog alog = new ApprovalLog();
		ApprovalLogExample.Criteria createCriteria = approvalLogexample.createCriteria();
		String aid = request.getParameter("approvalLogId");
		if (null != aid && !aid.isEmpty()) {
			Integer testint = new Integer(aid);
			createCriteria.andIdEqualTo(testint.shortValue());
			List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalLogexample);
			System.out.println("   list size in approval list  :::   " + listapprovallog.size());

			short alogId = listapprovallog.get(0).getId();
			alog.setId(alogId);
			alog.setStatus("2");
			// alog.setChecker((short) 1); //get from the current login session
			alog.setChecker(Short.parseShort(logedUser));
			alog.setUpdateDate(new Date());
			rs = approvalLogMapper.updateByPrimaryKeySelective(alog);
		}

		if (rs == 1) {

			model.addAttribute("rejectmessage", "Rejected Data Successfully");
		}

		else {

			model.addAttribute("rejectmessage", "Reject Data Failed");
		}

		AgentController agentController = new AgentController(agentProfileMapper, productsMapper, agentProdMapMapper,
				commonQQMapper, adminParamMapper, agentDocumentMapper, agentLinkMapper, approvalLogMapper,
				approvalMapper);

		String returnURL = agentController.getAgentProductMapApproval(request, response, model);
		;
		return returnURL;

	}

	/* HOHH Product rate approval ends here */

	/* HOHH Product info approval starts here */

	@RequestMapping("/HOHHProductInfoUpdateApproval")
	public String HOHHProductInfoUpdateApproval(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("Product Info ");
		HttpSession session = request.getSession();
		String logedUser = (String) session.getAttribute("logedUser");

		// Quotaion Validity

		List<HOHHParam> ins_Annual_SalesTarget = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample = new HOHHParamExample();
		HOHHParamExample.Criteria HOHHParam_criteria = HOHHTblParamExample.createCriteria();
		HOHHParam_criteria.andParamNameEqualTo("ins_Annual_SalesTarget");
		ins_Annual_SalesTarget = hohharamMapper.selectByExample(HOHHTblParamExample);

		// Annual Sales Target

		List<HOHHParam> ins_Quotation_Validity = new ArrayList<HOHHParam>();
		HOHHParamExample HOHHTblParamExample1 = new HOHHParamExample();
		HOHHParamExample.Criteria ins_AdditionalBenefit_ETC = HOHHTblParamExample1.createCriteria();
		ins_AdditionalBenefit_ETC.andParamNameEqualTo("ins_Quotation_Validity");
		ins_Quotation_Validity = hohharamMapper.selectByExample(HOHHTblParamExample1);

		List<Object> originalList = new ArrayList<Object>();
		originalList.addAll(ins_Annual_SalesTarget);
		originalList.addAll(ins_Quotation_Validity);

		String quotVal = request.getParameter("QuotationValidity");
		String salesTargetVal = request.getParameter("AnnualSalesTarget");

		List<String> newList = new ArrayList<String>();

		newList.add(salesTargetVal);
		newList.add(quotVal);

		ApprovalExample approvalExample = new ApprovalExample();
		com.spring.VO.ApprovalExample.Criteria createCriteria_approvalExample = approvalExample.createCriteria();
		createCriteria_approvalExample.andDescriptionEqualTo("HOHH - CHANGE PRODUCT INFORMATION");
		List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

		try {
			byte[] originalData = convertToBytes(originalList);
			byte[] changeData = convertToBytes(newList);

			System.out.println("ORI Data" + originalData);
			System.out.println("change Data" + changeData);

			BigDecimal approvalProdId = new BigDecimal(0);
			if (listapproval.size() > 0) {
				approvalProdId = listapproval.get(0).getId();
			}

			ApprovalLog approvalLog = new ApprovalLog();
			approvalLog.setNewContent(changeData);
			approvalLog.setOriginalContent(originalData);
			approvalLog.setApprovalId((short) 17); // from Approval table this value[7] based on menu in feature
			// approvalLog.setMaker((short)1); // session login value
			approvalLog.setMaker(Short.parseShort(logedUser));
			approvalLog.setStatus("1");
			approvalLog.setCreateDate(new Date());
			// approvalLog.setUpdateDate(new Date());
			approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
			approvalLogMapper.insert(approvalLog);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		model.addAttribute("updatemessage", "Updated data successfully");
		String returnURL = HOHHproductinfomation(request, response, model);
		return returnURL;

	}

	@RequestMapping(value = "/approvalHohhProductInfo", method = RequestMethod.GET)
	public String approvalHohhProductInfo(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Reequest " + request.getParameter("id"));
		ApprovalLogExample approvalexample = new ApprovalLogExample();
		com.spring.VO.ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();
		Integer testint = new Integer(request.getParameter("id"));
		createCriteria.andIdEqualTo(testint.shortValue());
		List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
		System.out.println("   list size in approval list  :::   " + listapprovallog.size());
		// System.out.println("Testing Original Content
		// "+listapprovallog.get(0).getOriginalContent());

		try {
			// original data
			if (listapprovallog.size() > 0) {
				Object originalData = convertFromBytes(listapprovallog.get(0).getOriginalContent());
				List<Object> listOriginalData = new ArrayList<Object>();
				// objecct convert into AgentProdMap
				if (originalData != null) {
					listOriginalData = (List<Object>) originalData;
					System.out.println("  list agent prod map size ::::::         " + listOriginalData.size());

					HOHHProductInfoApproval hohhProductInfoApproval = new HOHHProductInfoApproval();

					hohhProductInfoApproval.setAnnualTarget(((HOHHParam) listOriginalData.get(0)).getParamValue());
					hohhProductInfoApproval.setValidity(((HOHHParam) listOriginalData.get(1)).getParamValue());

					List<HOHHProductInfoApproval> listOriginalHOHHProductInfo = new ArrayList<HOHHProductInfoApproval>();
					listOriginalHOHHProductInfo.add(hohhProductInfoApproval);
					model.addAttribute("listOriginalHOHHProductInfo", listOriginalHOHHProductInfo);

					System.out.println("-----------------------------");
				} // inner if
			} // outer if

			// Change Data
			if (listapprovallog.size() > 0) {
				Object changeData = convertFromBytes(listapprovallog.get(0).getNewContent());
				List<String> listChangeData = new ArrayList<String>();
				// objecct convert into AgentProdMap
				if (changeData != null) {
					listChangeData = (List<String>) changeData;
					System.out.println("  list agent prod map size ::::::         " + listChangeData.size());

					HOHHProductInfoApproval hohhProductInfoApproval = new HOHHProductInfoApproval();

					hohhProductInfoApproval.setNewAnnualTarget(listChangeData.get(0));
					hohhProductInfoApproval.setNewValidity(listChangeData.get(1));

					List<HOHHProductInfoApproval> listChangeHOHHProductInfo = new ArrayList<HOHHProductInfoApproval>();

					listChangeHOHHProductInfo.add(hohhProductInfoApproval);
					model.addAttribute("listChangeHOHHProductInfo", listChangeHOHHProductInfo);
					System.out.println("-----------------------------");
				}
			}
		} // try end
		catch (Exception e) {
			System.out.println("Exception in Convert Object to Byte" + e);
		}

		return "agent-product-approval";
	}

	@RequestMapping("/approveHOHHProductInfoChangeData")
	public String approveHOHHProductInfoChangeData(HttpServletRequest request, HttpServletResponse response,
			Model model) {
		String returnURL = "";

		try {

			System.out.println("Reequest " + request.getParameter("id"));
			String aid = request.getParameter("approvalLogId");
			System.out.println("aid >>>>>" + aid);
			HttpSession session = request.getSession();
			String logedUser = (String) session.getAttribute("logedUser");

			ApprovalLogExample approvalexample = new ApprovalLogExample();
			ApprovalLog alog = new ApprovalLog();
			ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();

			if (null != aid && !aid.isEmpty()) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
				System.out.println("   list size in approval list  :::   " + listapprovallog.size());
				// System.out.println("Testing Original Content
				// "+listapprovallog.get(0).getOriginalContent());

				// Change Data
				if (listapprovallog.size() > 0) {

					String status = listapprovallog.get(0).getStatus();
					short alogId = listapprovallog.get(0).getId();
					System.out.println("Status  :  " + status);
					System.out.println("Approvallog Id  :  " + alogId);

					Object changeData = convertFromBytes(listapprovallog.get(0).getNewContent());
					List<String> listChangeData = new ArrayList<String>();
					// objecct convert into AgentProdMap
					if (changeData != null) {
						listChangeData = (List<String>) changeData;
						System.out.println("  list agent prod map size ::::::         " + listChangeData.size());

						HOHHProductInfoApproval hohhProductInfoApproval = new HOHHProductInfoApproval();

						hohhProductInfoApproval.setNewAnnualTarget(listChangeData.get(0));
						hohhProductInfoApproval.setNewValidity(listChangeData.get(1));

						HOHHParamExample HOHHTblParamExample = new HOHHParamExample();
						HOHHParam hohhParam = new HOHHParam();
						HOHHParamExample.Criteria hohhParam_criteria1 = HOHHTblParamExample.createCriteria();
						hohhParam_criteria1.andParamNameEqualTo("ins_Annual_SalesTarget");

						String salesTargetVal = hohhProductInfoApproval.getNewAnnualTarget();
						System.out.println(salesTargetVal + "salesTargetVal");
						hohhParam.setParamValue(new BigDecimal(salesTargetVal));

						int hohhParamUpdate = hohharamMapper.updateByExampleSelective(hohhParam, HOHHTblParamExample);
						System.out.println(hohhParamUpdate + "salesTargetVal value");

						HOHHParamExample HOHHTblParamExample1 = new HOHHParamExample();
						HOHHParam hohhParam1 = new HOHHParam();
						HOHHParamExample.Criteria hohhParam_criteria2 = HOHHTblParamExample1.createCriteria();
						hohhParam_criteria2.andParamNameEqualTo("ins_Quotation_Validity");

						String validity = hohhProductInfoApproval.getNewValidity();
						System.out.println(validity + "validity--->");
						hohhParam1.setParamValue(new BigDecimal(validity));

						int hohhParamUpdate1 = hohharamMapper.updateByExampleSelective(hohhParam1,
								HOHHTblParamExample1);
						System.out.println(hohhParamUpdate1 + "quote Val value");

						// Update status in DSP_AFM_TBL_Approval_Log table
						alog.setId(alogId);
						alog.setStatus("3");
						// alog.setChecker((short) 1); //get from the current login session
						alog.setChecker(Short.parseShort(logedUser));
						alog.setUpdateDate(new Date());
						int rs = approvalLogMapper.updateByPrimaryKeySelective(alog);

						if (rs == 1) {

							model.addAttribute("approvemessage", "Approved Data Successfully");
						}

						AgentController agentController = new AgentController(agentProfileMapper, productsMapper,
								agentProdMapMapper, commonQQMapper, adminParamMapper, agentDocumentMapper,
								agentLinkMapper, approvalLogMapper, approvalMapper);

						returnURL = agentController.getAgentProductMapApproval(request, response, model);

					} // end forchangeData
				} // end for listapprovallog
			} // Fot if aid
		} // Try end
		catch (Exception e) {
			System.out.println("Exception In >> HOHH product info update done funtion " + e);
			model.addAttribute("approvemessage", "Approve Data Failed");
		}
		return returnURL;

	}

	@RequestMapping(value = "/rejectHOHHProductInfoChangeData", method = RequestMethod.GET)
	public String rejectHOHHProductInfoChangeData(@ModelAttribute AgentProfile agentProfile, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		String logedUser = (String) session.getAttribute("logedUser");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		int rs = 0;

		ApprovalLogExample approvalLogexample = new ApprovalLogExample();
		ApprovalLog alog = new ApprovalLog();
		ApprovalLogExample.Criteria createCriteria = approvalLogexample.createCriteria();
		String aid = request.getParameter("approvalLogId");
		if (null != aid && !aid.isEmpty()) {
			Integer testint = new Integer(aid);
			createCriteria.andIdEqualTo(testint.shortValue());
			List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalLogexample);
			System.out.println("   list size in approval list  :::   " + listapprovallog.size());

			short alogId = listapprovallog.get(0).getId();
			alog.setId(alogId);
			alog.setStatus("2");
			// alog.setChecker((short) 1); //get from the current login session
			alog.setChecker(Short.parseShort(logedUser));
			alog.setUpdateDate(new Date());
			rs = approvalLogMapper.updateByPrimaryKeySelective(alog);
		}

		if (rs == 1) {

			model.addAttribute("rejectmessage", "Rejected Data Successfully");
		}

		else {

			model.addAttribute("rejectmessage", "Reject Data Failed");
		}

		AgentController agentController = new AgentController(agentProfileMapper, productsMapper, agentProdMapMapper,
				commonQQMapper, adminParamMapper, agentDocumentMapper, agentLinkMapper, approvalLogMapper,
				approvalMapper);

		String returnURL = agentController.getAgentProductMapApproval(request, response, model);
		;
		return returnURL;
	}

	/* HOHH Product info approval ends here */

}
