package com.spring.admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.spring.VO.GatewayLog;
import com.spring.VO.GatewayLogExample;
import com.spring.VO.GatewayParam;
import com.spring.VO.GatewayParamExample;
import com.spring.mapper.GatewayLogMapper;
import com.spring.mapper.GatewayParamMapper;

@Controller
public class GatewayController {

	// static final String
	// paymentServiceURL="http://cpf:7011/paymentservice/api/getstatus/connection/";
	static final String PaymentServiceCPF = "http://172.29.124.1:7011/paymentservice/api/getstatus";

	// ApplicationContext context = new
	// ClassPathXmlApplicationContext("Spring-Mail.xml");

	// MailMail mm = (MailMail) context.getBean("mailMail");

	GatewayParamMapper gatewayParamMapper;
	GatewayLogMapper gatewayLogMapper;

	@Autowired
	public GatewayController(GatewayParamMapper gatewayParamMapper, GatewayLogMapper gatewayLogMapper

	) {
		this.gatewayParamMapper = gatewayParamMapper;
		this.gatewayLogMapper = gatewayLogMapper;

	}

	@RequestMapping("/gatewayConnection")
	public String getGatewayConnection(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		List<String> listGateway = new ArrayList<String>();

		listGateway.add("FPX");
		listGateway.add("M2U");
		listGateway.add("EBPG");
		listGateway.add("MPAY");

		GatewayParamExample gatewayStatus = new GatewayParamExample();
		GatewayParamExample.Criteria statusCriteria = gatewayStatus.createCriteria();
		statusCriteria.andPmntGatewayCodeIn(listGateway);
		List<GatewayParam> GatewayStatusList = new ArrayList<GatewayParam>();
		GatewayStatusList = gatewayParamMapper.selectByExample(gatewayStatus);

		// fpx
		GatewayLogExample fpxLog = new GatewayLogExample();
		GatewayLogExample.Criteria fpxLogCriteria = fpxLog.createCriteria();
		fpxLogCriteria.andPmntGatewayCodeEqualTo("fpx");
		List<GatewayLog> FPXLogList = new ArrayList<GatewayLog>();
		fpxLog.setOrderByClause("id desc");
		FPXLogList = gatewayLogMapper.selectByExample(fpxLog);

		// m2u
		GatewayLogExample m2uLog = new GatewayLogExample();
		GatewayLogExample.Criteria m2uLogCriteria = m2uLog.createCriteria();
		m2uLogCriteria.andPmntGatewayCodeEqualTo("m2u");
		List<GatewayLog> M2ULogList = new ArrayList<GatewayLog>();
		m2uLog.setOrderByClause("id desc");
		M2ULogList = gatewayLogMapper.selectByExample(m2uLog);

		// ebpg
		GatewayLogExample ebpgLog = new GatewayLogExample();
		GatewayLogExample.Criteria ebpgLogCriteria = ebpgLog.createCriteria();
		ebpgLogCriteria.andPmntGatewayCodeEqualTo("ebpg");
		List<GatewayLog> EBPGLogList = new ArrayList<GatewayLog>();
		ebpgLog.setOrderByClause("id desc");
		EBPGLogList = gatewayLogMapper.selectByExample(ebpgLog);

		// mpay
		GatewayLogExample mpayLog = new GatewayLogExample();
		GatewayLogExample.Criteria mpayLogCriteria = mpayLog.createCriteria();
		mpayLogCriteria.andPmntGatewayCodeEqualTo("mpay");
		List<GatewayLog> MPAYLogList = new ArrayList<GatewayLog>();
		mpayLog.setOrderByClause("id desc");
		MPAYLogList = gatewayLogMapper.selectByExample(mpayLog);

		model.addAttribute("GatewayStatusList", GatewayStatusList);
		model.addAttribute("FPXLogList", FPXLogList);
		model.addAttribute("M2ULogList", M2ULogList);
		model.addAttribute("EBPGLogList", EBPGLogList);
		model.addAttribute("MPAYLogList", MPAYLogList);

		return "admin-gateway-connection";

	}

	@RequestMapping(value = "/updateGatewayStatus", method = RequestMethod.POST)

	public String updateGatewayStatus(HttpServletRequest req, GatewayParam gatewayParam, Model model)

	{
		HttpSession session = req.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		int fpxStatus = Integer.parseInt(req.getParameter("showFPX"));
		int m2uStatus = Integer.parseInt(req.getParameter("showM2U"));
		int ebpgStatus = Integer.parseInt(req.getParameter("showEBPG"));
		int mpayStatus = Integer.parseInt(req.getParameter("showMPAY"));

		/*
		 * System.out.println("fpxStatus"+fpxStatus);
		 * System.out.println("m2uStatus"+m2uStatus);
		 * System.out.println("ebpgStatus"+ebpgStatus);
		 * System.out.println("mpayStatus"+mpayStatus);
		 */

		// FPX
		gatewayParam.setStatus((short) fpxStatus);
		GatewayParamExample gatewayParamFpx = new GatewayParamExample();
		GatewayParamExample.Criteria criteria = gatewayParamFpx.createCriteria();
		criteria.andPmntGatewayCodeEqualTo("FPX");
		gatewayParamMapper.updateByExampleSelective(gatewayParam, gatewayParamFpx);

		// M2U
		gatewayParam.setStatus((short) m2uStatus);
		GatewayParamExample gatewayParamM2u = new GatewayParamExample();
		GatewayParamExample.Criteria criteria2 = gatewayParamM2u.createCriteria();
		criteria2.andPmntGatewayCodeEqualTo("M2U");
		gatewayParamMapper.updateByExampleSelective(gatewayParam, gatewayParamM2u);

		// EBPG

		List<String> listGatewayEBPG = new ArrayList<String>();

		listGatewayEBPG.add("EBPG");
		listGatewayEBPG.add("AMEX");

		gatewayParam.setStatus((short) ebpgStatus);
		GatewayParamExample gatewayParamEbpg = new GatewayParamExample();
		GatewayParamExample.Criteria criteria3 = gatewayParamEbpg.createCriteria();
		criteria3.andPmntGatewayCodeIn(listGatewayEBPG);
		// criteria3.andPmntGatewayCodeEqualTo("EBPG");
		gatewayParamMapper.updateByExampleSelective(gatewayParam, gatewayParamEbpg);

		// MPAY
		gatewayParam.setStatus((short) mpayStatus);
		GatewayParamExample gatewayParamMpay = new GatewayParamExample();
		GatewayParamExample.Criteria criteria4 = gatewayParamMpay.createCriteria();
		criteria4.andPmntGatewayCodeEqualTo("MPAY");
		gatewayParamMapper.updateByExampleSelective(gatewayParam, gatewayParamMpay);

		return "redirect:/gatewayConnection";
	}

	@RequestMapping(value = "/checkGatewayStatus", method = RequestMethod.POST)

	public String checkGatewayStatus(HttpServletRequest req, Model model,
			@ModelAttribute("gatewayLog") GatewayLog gatewayLog)

	{
		HttpSession session = req.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String errorCode = "";
		String errorMessage = "";
		String gateway = "";
		String url = "";
		String operation = "";
		int code = 0;
		String status = "";

		String gatewayCode = req.getParameter("gateway").trim();
		String jsonResult = null;
		System.out.println("gatewayCode" + gatewayCode);

		Map<String, String> vars = new HashMap<String, String>();
		vars.put("gateway", gatewayCode);
		vars.put("operation", "payment");
		RestTemplate rt = new RestTemplate();

		try {
			jsonResult = rt.getForObject(PaymentServiceCPF + "/connection/{gateway}/{operation}", String.class, vars);
			System.out.println("jsonResult" + jsonResult);
			Gson gson = new Gson();
			Map<String, Object> getResult = new HashMap<String, Object>();
			getResult = gson.fromJson(jsonResult, getResult.getClass());
			System.out.println("getResult map  " + getResult);

			/*
			 * if(null != getResult.get("code")){ code=(Integer) getResult.get("code"); }
			 */

			if (null != getResult.get("errorCode")) {
				errorCode = (String) getResult.get("errorCode");
			}

			if (null != getResult.get("errorMessage")) {
				errorMessage = (String) getResult.get("errorMessage");
			}
			if (null != getResult.get("status")) {
				status = (String) getResult.get("status");
			}

			gateway = (String) getResult.get("gateway");
			url = (String) getResult.get("url");
			operation = (String) getResult.get("operation");

		} catch (Exception e) {

			e.printStackTrace();

		}

		try {
			// insert gateway log

			gatewayLog.setLogData(jsonResult);
			gatewayLog.setPmntGatewayCode(gateway);
			gatewayLog.setStatusCode(status);
			gatewayLog.setCreatedBy(loginUser);
			gatewayLog.setLogDatetime(new Date());
			gatewayLog.setEmailSent("");
			gatewayLogMapper.insert(gatewayLog);
		}

		catch (Exception e) {

			e.printStackTrace();

		}

		if (status != "OK") {

			// send email

			/*
			 * try{ String mailMessage=
			 * "\n Dear support,\n\n We recently have connection issue with your gateway. Kindly advise\n\n Thanks\n "
			 * ; //mm.sendMail(name, mailMessage);
			 *
			 * String toAddr = "nikrohaiza@absecmy.com.my"; String fromAddr =
			 * "nikrohaiza01@gmail.com";
			 *
			 * System.out.println(toAddr); // email subject String subject =
			 * "Connection Issue";
			 *
			 * //mm.test(toAddr, fromAddr, subject, mailMessage);
			 *
			 * } catch (Exception e){
			 *
			 *
			 * e.printStackTrace();
			 *
			 * }
			 *
			 */
		}

		model.addAttribute("errorCode", errorCode);
		model.addAttribute("errorMessage", errorMessage);
		model.addAttribute("code", code);
		model.addAttribute("gateway", gateway);
		model.addAttribute("url", url);
		model.addAttribute("status", status);

		return getGatewayConnection(req, model);

		// return "redirect:/gatewayConnection";
	}

}
