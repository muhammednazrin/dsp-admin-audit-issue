package com.spring.admin;

//import java.text.ParseException;
//import java.text.SimpleDateFormat;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
//import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
//import java.util.Date;
//import java.util.HashMap;
import java.util.List;
//import java.util.Map;
import java.util.Locale;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.spring.VO.report.ALLReportVO;
import com.spring.VO.report.NewCarRegistrationReportExcelVO;
//import com.spring.utils.ServiceValidationUtils;
import com.spring.VO.report.TransSearchObject;
import com.spring.mapper.report.ALLReportMapper;
import com.spring.mapper.report.NewCarRegistrationExcelMapper;
import com.spring.utils.ServiceValidationUtils;

@Controller
public class NciTranExcelController {

	NewCarRegistrationExcelMapper motorReportExcelMapper;
	ALLReportMapper allReportMapper;

	@Autowired
	public NciTranExcelController(NewCarRegistrationExcelMapper motorReportExcelMapper,
			ALLReportMapper allReportMapper) {
		this.motorReportExcelMapper = motorReportExcelMapper;
		this.allReportMapper = allReportMapper;

	}

	@RequestMapping(value = "/generateExcel1", method = RequestMethod.GET)
	public String generateExcel(HttpServletRequest request, HttpServletResponse response, Model model)
			throws ServletException, IOException {

		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		ServletContext servletContext = request.getSession().getServletContext();

		String productType = (String) session.getAttribute("ProductType");
		String productEntity = (String) session.getAttribute("ProductEntity");
		String status = (String) session.getAttribute("Status");
		String PolicyCertificateNo = (String) session.getAttribute("PolicyCertificateNo");
		String nRIC = (String) session.getAttribute("NRIC");
		String receiptNo = (String) session.getAttribute("ReceiptNo");
		String dateFrom = (String) session.getAttribute("dateFrom");
		String dateTo = (String) session.getAttribute("dateTo");
		String plateNo = (String) session.getAttribute("PlateNo");

		System.out.println("productType  -> " + productType);
		System.out.println("productEntity  -> " + productEntity);
		System.out.println("status  -> " + status);
		System.out.println("PolicyCertificateNo  -> " + PolicyCertificateNo);
		System.out.println("nRIC  -> " + nRIC);
		System.out.println("receiptNo  -> " + receiptNo);
		System.out.println("dateFrom  -> " + dateFrom);
		System.out.println("dateTo  -> " + dateTo);
		System.out.println("plateNo  -> " + plateNo);

		// start : motor excel file
		// if(productType.indexOf("NCI")!=-1){
		if ("NCI".equalsIgnoreCase(productType) || "NCT".equalsIgnoreCase(productType)) {
			TransSearchObject transSearchObject_Motor = new TransSearchObject();
			List<NewCarRegistrationReportExcelVO> motorReportList = new ArrayList<NewCarRegistrationReportExcelVO>();
			List<NewCarRegistrationReportExcelVO> motorReportList_B4Pay = new ArrayList<NewCarRegistrationReportExcelVO>();
			List<String> transactionStatusList = new ArrayList<String>();

			String query1 = "n";
			String query2 = "n";

			if ("EIB".equalsIgnoreCase(productEntity) && "".equalsIgnoreCase(productType)) {

				transSearchObject_Motor.setProductCode("NCI");
			}

			if ("ETB".equalsIgnoreCase(productEntity) && "".equalsIgnoreCase(productType)) {

				transSearchObject_Motor.setProductCode("NCT");
			}
			if ("".equalsIgnoreCase(productType) || productType == null) {
				transSearchObject_Motor.setProductCode("NCI,NCT");
			}

			if ("NCI".equalsIgnoreCase(productType) || "NCT".equalsIgnoreCase(productType)) {
				transSearchObject_Motor.setProductCode(productType);

			}
			if ("".equalsIgnoreCase(productType) || productType == null) {
				if ("".equalsIgnoreCase(status) || status == null) { // productType empty and transaction status empty
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transSearchObject_Motor.setTransactionStatus(transactionStatusList);
					query1 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_Motor.setTransactionStatus(transactionStatusList);
				}
			} else { // if Product is Chosen
				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_Motor.setTransactionStatus(transactionStatusList);
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_Motor.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus(status);
				}
			}
			if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)) {
				query1 = "y";
			}
			if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
				query2 = "y";
			}

			if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
				transSearchObject_Motor.setPolicyNo(PolicyCertificateNo);
			}
			if (!"".equalsIgnoreCase(nRIC)) {
				transSearchObject_Motor.setNricNo(nRIC);
			}
			if (!"".equalsIgnoreCase(receiptNo)) {
				transSearchObject_Motor.setInvoiceNo(receiptNo);
			}

			if (!ServiceValidationUtils.isEmptyStringTrim(plateNo)) {
				transSearchObject_Motor.setVehicleNo(plateNo);
			}

			transSearchObject_Motor.setDateFrom(dateFrom);
			transSearchObject_Motor.setDateTo(dateTo);

			// if query 1
			if ("y".equalsIgnoreCase(query1)) {
				motorReportList = motorReportExcelMapper
						.selectNciTransactionalReportAfterPayment(transSearchObject_Motor);

			} // End if query1 is Executed

			// if query 2
			if ("y".equalsIgnoreCase(query2)) {
				motorReportList_B4Pay = motorReportExcelMapper
						.selectNciTransactionalReportBeforePayment(transSearchObject_Motor);
			}

			motorReportList.addAll(motorReportList_B4Pay);

			Collections.sort(motorReportList, new Comparator<NewCarRegistrationReportExcelVO>() {
				@Override
				public int compare(NewCarRegistrationReportExcelVO o1, NewCarRegistrationReportExcelVO o2) {
					return o1.getRecord_date().compareTo(o2.getRecord_date());
				}
			});

			Collections.reverse(motorReportList);

			if (motorReportList.size() <= 0) {
				model.addAttribute("emptySearch", "No Record Found");
			}
			model.addAttribute("motorReportList", motorReportList);

			// start excel

			Random ran = new Random();
			int randomNo = ran.nextInt(6) + 5;

			DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
			Date date = new Date();

			String file = "EtiqaTransactionReport" + dateFormat.format(date) + randomNo + ".xlsx";

			String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
			// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Etiqa Transaction Report");

			// header fields based on product
			String[] fields = null;

			fields = new String[] { "Id", "DSPQQID", "Quotation Creation Date", "Txn Id", "Txn Start Date",
					"Txn End Date", "Entity", "Policy No", "CAPS Policy No", "Vehicle No", "Chassis No", "Engine No",
					"Previous Insurance", "Policy Status", "Txn Status", "Abandonment", "Receipt No", "EIP Txn Id",
					"Agent Code", "Agent Name", "Operator Code", "Promo Code", "NCD", "Payment Mode", "FPX Ref",
					"EBPG Ref", "M2U Ref", "No of Seat", "Cubic Capacity", "Make", "Model", "NVIC", "Year make",
					"Period of Insured", "Sum Insured", "SI Proposed", "SI Min", "SI Max", "LLOP", "LL2P", "Flood",
					"NCD Relief", "SRCC", "Windscreen SC", "Windscreen PM", "Vehicle Acc SC", "Vehicle Acc PM",
					"NGV Gas SC", "NGV Gas PM", "CART", "Payment Payable", "Gross Premium", "Discount", "GST",
					"SST Percentage","SST Amount","Motor Comm", "CAPS Comm", "No of Drivers", "Driver 1", "Driver 2", "Driver 3", "Driver 4",
					"Driver 5", "Driver 6", "Driver 7", "CAPS", "Excess", "Loading Percentage", "Loading",
					"Insured name", "Id Type", "Insured ID", "Gender", "Age", "Email", "Contact No", "Address1",
					"Address2", "Address3", "Postcode", "State", "RiskAddress1", "RiskAddress2", "RiskAddress3",
					"RiskPostcode", "RiskState", "Tax Invoice No", "JPJ Msg", "CARSECM", "ATITFTCD", "GARAGECD" };

			Row row1 = sheet.createRow(0);

			for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
				Cell cell = row1.createCell(cellIndex);
				cell.setCellValue(fields[cellIndex]);
			}

			String[] fields2 = null;

			int i = 1;

			for (NewCarRegistrationReportExcelVO motorDetail : motorReportList) {

				String Abandonment = "";
				String EIPTxnId = "";
				String RiskAddress3 = "";
				String txnStatus = "";
				String policyStatus="";
				String quotationCreationDate = "";
				String transactionStartDateTime = "";
				String transactionEndDateTime = "";
				String proEntity = "";
				String paymentChannel = "";
				String SIMin = "";
				String SIMax = "";
				String cartPremium = "";
				String cartPremiumDesc = "";
				String firstDriverInfo = "";
				String secondDriverInfo = "";
				String thirdDriverInfo = "";
				String forthDriverInfo = "";
				String fifthDriverInfo = "";
				String sixthDriverInfo = "";
				String seventhDriverInfo = "";
				String age = "";

				if (null != motorDetail.getPmnt_gateway_code() && "" != motorDetail.getPmnt_gateway_code()) {
					if (motorDetail.getPmnt_gateway_code().indexOf("EBPG") != -1) {

						paymentChannel = "EBPG";
					}

					else if (motorDetail.getPmnt_gateway_code().indexOf("AMEX") != -1) {

						paymentChannel = "AMEX";
					}

					else if (motorDetail.getPmnt_gateway_code().indexOf("FPX") != -1) {

						paymentChannel = "FPX";
					}

					else if (motorDetail.getPmnt_gateway_code().indexOf("Maybank2U") != -1) {

						paymentChannel = "M2U";
					}

				}
				if (motorDetail.getProduct_code().equals("NCI")) {

					proEntity = "EGIB";
				}

				else if (motorDetail.getProduct_code().equals("NCT"))

				{

					proEntity = "EGTB";

				}
				if (motorDetail.getPmnt_status().equals("S") && motorDetail.getPolicyStatus().equals("CANCEL")) {

					policyStatus = "Manually Cancelled";

				} else{
					policyStatus =motorDetail.getPolicyStatus();
				}
				if (motorDetail.getPmnt_status().equals("S")) {

					txnStatus = "Successful";

				} else if (motorDetail.getPmnt_status().equals("F")) {

					txnStatus = "Fail";

				}

				else if (motorDetail.getPmnt_status().equals("AP")) {

					txnStatus = "Attempt Payment";

				}

				else if (motorDetail.getPmnt_status().equals("O")) {

					Abandonment = motorDetail.getLast_page();
					txnStatus = "Incomplete";

				}

				else if (motorDetail.getPmnt_status().equals("C")) {

					txnStatus = "Cancelled";

				}

				else if (motorDetail.getPmnt_status().equals("R")) {

					txnStatus = "Rejection : " + motorDetail.getReason();

				}

				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				Date quotationCreationDateTmp = null;

				try {

					quotationCreationDateTmp = formatter.parse(motorDetail.getQuotationCreateDateTime());
					quotationCreationDate = ft.format(quotationCreationDateTmp);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				if (motorDetail.getPmnt_status().equals("S") || motorDetail.getPmnt_status().equals("F")
						|| motorDetail.getPmnt_status().equals("AP")) {

					transactionStartDateTime = motorDetail.getTransaction_datetime();

				}

				if (null != motorDetail.getMarketValue() && "" != motorDetail.getMarketValue()
						&& null != motorDetail.getMarkUpPer() && "" != motorDetail.getMarkUpPer()) {
					DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(Locale.US);
					format.setParseBigDecimal(true);
					BigDecimal amt1 = new BigDecimal(0.00);
					BigDecimal amt2 = new BigDecimal(0.00);
					BigDecimal amt3 = new BigDecimal(100.00);
					try {
						amt1 = (BigDecimal) format.parse(motorDetail.getMarketValue());
						amt2 = (BigDecimal) format.parse(motorDetail.getMarkUpPer());

						BigDecimal tmpAmount = amt2.multiply(amt1).divide(amt3);
						BigDecimal sMin = amt1.subtract(tmpAmount);
						BigDecimal sMax = amt1.add(tmpAmount);

						SIMin = String.valueOf(sMin);
						SIMax = String.valueOf(sMax);

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				if (motorDetail.getCartPremium() != null) {

					cartPremium = motorDetail.getCartPremium();

				}

				if (motorDetail.getCartPremiumDesc() != null) {

					String tmpCARTDesc = motorDetail.getCartPremiumDesc().replaceAll(",", "|");
					tmpCARTDesc = tmpCARTDesc.replaceAll("Days", "");
					tmpCARTDesc = tmpCARTDesc.replaceAll("RM", "");
					tmpCARTDesc = tmpCARTDesc.replaceAll("/day", "");

					cartPremiumDesc = tmpCARTDesc + "|";

				}

				if (!motorDetail.getFirstDriverInfo().equals("||||||")) {

					firstDriverInfo = motorDetail.getFirstDriverInfo();
				}

				if (!motorDetail.getSecondDriverInfo().equals("||||||")) {

					secondDriverInfo = motorDetail.getSecondDriverInfo();
				}
				if (!motorDetail.getThirdDriverInfo().equals("||||||")) {

					thirdDriverInfo = motorDetail.getThirdDriverInfo();
				}
				if (!motorDetail.getForthDriverInfo().equals("||||||")) {

					forthDriverInfo = motorDetail.getForthDriverInfo();
				}
				if (!motorDetail.getFifthDriverInfo().equals("||||||")) {

					fifthDriverInfo = motorDetail.getFifthDriverInfo();
				}
				if (!motorDetail.getSixthDriverInfo().equals("||||||")) {

					sixthDriverInfo = motorDetail.getSixthDriverInfo();
				}

				if (!motorDetail.getSixthDriverInfo().equals("||||||")) {

					seventhDriverInfo = motorDetail.getSeventhDriverInfo();
				}

				if (null != motorDetail.getCustomerDOB() && "" != motorDetail.getCustomerDOB()
						&& "//" != motorDetail.getCustomerDOB()) {

					try {

						String[] dob_split = motorDetail.getCustomerDOB().split("/");

						LocalDate start = LocalDate.of(Integer.parseInt(dob_split[2].trim()),
								Integer.parseInt(dob_split[1].trim()), Integer.parseInt(dob_split[0].trim()));
						LocalDate end = LocalDate.now(); // use for age-calculation: LocalDate.now()
						long tmpAge = ChronoUnit.YEARS.between(start, end);
						age = new Long(tmpAge).toString();
					}

					catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				fields2 = new String[] { Integer.toString(i), motorDetail.getDSPQQID(), quotationCreationDate,
						motorDetail.getPaymentTrxID(), transactionStartDateTime, transactionEndDateTime, proEntity,
						motorDetail.getPolicy_number(), motorDetail.getCaps_dppa(),
						motorDetail.getRegistration_number(), motorDetail.getChassisNo(), motorDetail.getEngineNo(),
						motorDetail.getPreviousInsurance(), policyStatus, txnStatus, Abandonment,
						motorDetail.getInvoice_no(), EIPTxnId, motorDetail.getAgent_code(), motorDetail.getAgent_name(),
						motorDetail.getOperatorCode(), motorDetail.getDiscountCode(), motorDetail.getNCDAmount(),
						paymentChannel, motorDetail.getFpx_fpxtxnid(), motorDetail.getTransaction_id(),
						motorDetail.getBankrefno(), motorDetail.getVehicleSeat(), motorDetail.getVehicleCC(),
						motorDetail.getVehicleMake(), motorDetail.getVehicleModel(), motorDetail.getNVIC(),
						motorDetail.getYearMake(), motorDetail.getPeriodCoverage(), motorDetail.getSumInsured(),
						motorDetail.getMarketValue(), SIMin, SIMax, motorDetail.getLLOP(), motorDetail.getLL2P(),
						motorDetail.getFloodCover(), motorDetail.getNCDRelief(), motorDetail.getSRCC(),
						motorDetail.getWindscreenSC(), motorDetail.getWindscreenP(), motorDetail.getVehicleAccSC(),
						motorDetail.getVehicleAccP(), motorDetail.getNGVGasSC(), motorDetail.getNGVGasP(),
						cartPremiumDesc + cartPremium, motorDetail.getTotalPremiumPayable(),
						motorDetail.getGrosspremium_final(), motorDetail.getDiscount(), motorDetail.getGst(),
						motorDetail.getSst_percentage(), motorDetail.getSst_amount(),
						motorDetail.getAgentCommissionAmount(), motorDetail.getCAPSCommissionAmount(),
						motorDetail.getDriverNo(), firstDriverInfo, secondDriverInfo, thirdDriverInfo, forthDriverInfo,
						fifthDriverInfo, sixthDriverInfo, seventhDriverInfo, motorDetail.getPAPremiumPayable(),
						motorDetail.getExcess(), motorDetail.getLoadingPercentage(), motorDetail.getLoading(),
						motorDetail.getCustomer_name(), // BQ
						/* motorDetail.getSecondDriver(), */
						motorDetail.getIDType(), motorDetail.getCustomer_nric_id(), motorDetail.getCustomerGender(),
						age, // BU
						motorDetail.getCustomerEmail(), motorDetail.getCustomerMobileNo(),
						motorDetail.getCustomerAddress1(), motorDetail.getCustomerAddress2(),
						motorDetail.getCustomerAddress3(), motorDetail.getCustomerPostcode(),
						motorDetail.getCustomerState(), motorDetail.getParkingAddress1(),
						motorDetail.getParkingAddress2(), RiskAddress3, motorDetail.getParkingPostcode(),
						motorDetail.getParkingState(), motorDetail.getInvoice_no(), motorDetail.getJPJStatus(),
						motorDetail.getCARSECM(), motorDetail.getATITFTCD(), motorDetail.getGARAGECD() };

				Row row2 = sheet.createRow(i);

				for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
					Cell cell = row2.createCell(cellIndex);
					cell.setCellValue(fields2[cellIndex]);
				}

				i++;

			}

			// System.out.println("txnStatus "+txn.getStatus());

			try {
				FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
				workbook.write(outputStream);
				workbook.close();

				File fileToDownload = new File(relativeWebPath);

				FileInputStream inputStream = new FileInputStream(fileToDownload);
				response.setContentType(servletContext.getMimeType(relativeWebPath));
				response.setContentLength((int) fileToDownload.length());

				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
				response.setHeader(headerKey, headerValue);

				// get output stream of the response
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				// write bytes read from the input stream into the output stream
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}

				outStream.flush();
				outStream.close();
				inputStream.close();

				if (fileToDownload.exists()) {

					boolean success = new File(relativeWebPath).delete();
					if (success) {
						System.out.println("The file " + file + " has been successfully deleted");
					}

				}

			} catch (FileNotFoundException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			} catch (IOException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			}

		} // end : motor excel file

		// start : Ezylife excel file

		// START: ALL TRANSACTION REPORT
		else if ("".equalsIgnoreCase(productType) || productType == null) {
			TransSearchObject transSearchObject_Motor = new TransSearchObject();

			List<ALLReportVO> allReportList = new ArrayList<ALLReportVO>();

			List<ALLReportVO> motorReportList = new ArrayList<ALLReportVO>();

			List<String> transactionStatusList = new ArrayList<String>();

			if ("EIB".equalsIgnoreCase(productEntity)) {

				transSearchObject_Motor.setProductCode("NCI");

			} else if ("ETB".equalsIgnoreCase(productEntity)) {

				transSearchObject_Motor.setProductCode("NCT");

			}

			if ("".equalsIgnoreCase(status) || status == null) {
				transactionStatusList.add("S");
				transactionStatusList.add("AP");
				transactionStatusList.add("F");
				transactionStatusList.add("C");
			} else {
				transactionStatusList.add(status);

			}

			transSearchObject_Motor.setTransactionStatus(transactionStatusList);

			if (!"".equalsIgnoreCase(PolicyCertificateNo)) {

				transSearchObject_Motor.setPolicyNo(PolicyCertificateNo);
			}
			if (!"".equalsIgnoreCase(nRIC)) {
				transSearchObject_Motor.setNricNo(nRIC);

			}
			if (!"".equalsIgnoreCase(receiptNo)) {
				transSearchObject_Motor.setInvoiceNo(receiptNo);

			}

			transSearchObject_Motor.setDateFrom(dateFrom);

			transSearchObject_Motor.setDateTo(dateTo);

			System.out.println("33333333333333333");
			motorReportList = allReportMapper.selectNciTransactionalReportAfterPayment(transSearchObject_Motor);

			allReportList.addAll(motorReportList);

			Collections.sort(allReportList, new Comparator<ALLReportVO>() {
				@Override
				public int compare(ALLReportVO o1, ALLReportVO o2) {
					return o1.getRecord_date().compareTo(o2.getRecord_date());
				}
			});

			Collections.reverse(allReportList);

			if (allReportList.size() <= 0) {
				model.addAttribute("emptySearch", "No Record Found");
			}
			model.addAttribute("reportResult", allReportList);

			// start excel

			Random ran = new Random();
			int randomNo = ran.nextInt(6) + 5;

			DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
			Date date = new Date();

			String file = "EtiqaTransactionReport" + dateFormat.format(date) + randomNo + ".xlsx";

			String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
			// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Etiqa Transaction Report");

			// header fields based on product
			String[] fields = null;

			fields = new String[] { "Id", "DSPQQID", "Entity", "Product", "Quotation Creation Date", "Txn Date",
					"Policy No", "Insured name", "Insured Id", "Poi", "Payment mode", "Status", "Txn Amount",
					"Gross Premium", "Discount", "GST", "Tax Invoice No", "CAPS PolicyNo", "CAPS Amount" };

			Row row1 = sheet.createRow(0);

			for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
				Cell cell = row1.createCell(cellIndex);
				cell.setCellValue(fields[cellIndex]);
			}

			String[] fields2 = null;

			int i = 1;

			for (ALLReportVO ALLDetail : allReportList) {

				String txnStatus = "";
				String quotationCreationDate = "";

				String paymentChannel = "";

				if (null != ALLDetail.getPmnt_gateway_code() && "" != ALLDetail.getPmnt_gateway_code()) {
					if (ALLDetail.getPmnt_gateway_code().indexOf("EBPG") != -1) {

						paymentChannel = "EBPG";
					}

					else if (ALLDetail.getPmnt_gateway_code().indexOf("AMEX") != -1) {

						paymentChannel = "AMEX";
					}

					else if (ALLDetail.getPmnt_gateway_code().indexOf("FPX") != -1) {

						paymentChannel = "FPX";
					}

					else if (ALLDetail.getPmnt_gateway_code().indexOf("Maybank2U") != -1) {

						paymentChannel = "M2U";
					}

					else if (ALLDetail.getPmnt_gateway_code().indexOf("CREDIT CARD") != -1) {

						paymentChannel = "MPAY";
					}

				}

				if (ALLDetail.getPmnt_status().equals("S")) {

					txnStatus = "Successful";

				} else if (ALLDetail.getPmnt_status().equals("F")) {

					txnStatus = "Fail";

				}

				else if (ALLDetail.getPmnt_status().equals("AP")) {

					txnStatus = "Attempt Payment";

				}

				else if (ALLDetail.getPmnt_status().equals("C")) {

					txnStatus = "Cancelled";

				}

				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				Date quotationCreationDateTmp = null;

				try {

					quotationCreationDateTmp = formatter.parse(ALLDetail.getQuotation_datetime());
					quotationCreationDate = ft.format(quotationCreationDateTmp);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				fields2 = new String[] { Integer.toString(i), ALLDetail.getDSPQQID(), productEntity,
						ALLDetail.getProduct_code(), quotationCreationDate, ALLDetail.getTransaction_datetime(),
						ALLDetail.getPolicy_number(), ALLDetail.getCustomer_name(), ALLDetail.getCustomer_nric_id(),
						ALLDetail.getPoi(), paymentChannel, txnStatus, ALLDetail.getAmount(),
						ALLDetail.getGrosspremium_final(), ALLDetail.getDiscount(), ALLDetail.getGst(),
						ALLDetail.getInvoice_no(), ALLDetail.getCaps_dppa(),
						ALLDetail.getPassenger_pa_premium_payable() };

				Row row2 = sheet.createRow(i);

				for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
					Cell cell = row2.createCell(cellIndex);
					cell.setCellValue(fields2[cellIndex]);
				}

				i++;

			}

			// System.out.println("txnStatus "+txn.getStatus());

			try {
				FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
				workbook.write(outputStream);
				workbook.close();

				File fileToDownload = new File(relativeWebPath);

				FileInputStream inputStream = new FileInputStream(fileToDownload);
				response.setContentType(servletContext.getMimeType(relativeWebPath));
				response.setContentLength((int) fileToDownload.length());

				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
				response.setHeader(headerKey, headerValue);

				// get output stream of the response
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				// write bytes read from the input stream into the output stream
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}

				outStream.flush();
				outStream.close();
				inputStream.close();

				if (fileToDownload.exists()) {

					boolean success = new File(relativeWebPath).delete();
					if (success) {
						System.out.println("The file " + file + " has been successfully deleted");
					}

				}

			} catch (FileNotFoundException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			} catch (IOException e) {
				response.getOutputStream().flush();
				response.getOutputStream().close();

				e.printStackTrace();
			}

		}

		// END: ALL TRANSACTION REPORT

		return "admin-report-nci_transaction";

	}

}
