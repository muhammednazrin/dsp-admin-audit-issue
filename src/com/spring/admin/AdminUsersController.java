package com.spring.admin;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.VO.AdminUsersProfile;
import com.spring.VO.AdminUsersProfileExample;
import com.spring.mapper.AdminUsersProfileMapper;

@Controller
public class AdminUsersController {
	AdminUsersProfileMapper adminUsersProfileMapper;

	@Autowired
	public AdminUsersController(AdminUsersProfileMapper adminUsersProfileMapper) {
		this.adminUsersProfileMapper = adminUsersProfileMapper;

	}

	@RequestMapping("/listAdminUsers")
	public String listAdminUsers(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		List<AdminUsersProfile> adminUsers = new ArrayList<AdminUsersProfile>();

		AdminUsersProfileExample adminUsersProfileExample = new AdminUsersProfileExample();
		AdminUsersProfileExample.Criteria adminuser_criteria = adminUsersProfileExample.createCriteria();
		adminUsers = adminUsersProfileMapper.selectByExample(adminUsersProfileExample);

		request.setAttribute("data", adminUsers);
		return "admin-system-user-role";
	}

}
