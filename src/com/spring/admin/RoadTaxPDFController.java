package com.spring.admin;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.spring.VO.roadtaxVO.RoadTaxTxnReport;

public class RoadTaxPDFController {

	private static Font TIME_ROMAN = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
	private static Font TIME_ROMAN_SMALL = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);

	/**
	 * @param args
	 */
	public static Document createPDF(String file, String postOfficeName, List<RoadTaxTxnReport> roadTaxTxnReportList,
			String roadTaxStatus) {

		Document document = null;

		try {
			document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(file));
			document.open();

			addMetaData(document);

			addTitlePage(document, postOfficeName, roadTaxStatus);

			if ("Printed".equals(roadTaxStatus)) {
				createPrintedTable(document, roadTaxTxnReportList);
			} else {
				createUnPrintedTable(document, roadTaxTxnReportList);
			}

			document.close();

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return document;

	}

	private static void addMetaData(Document document) {
		document.addTitle("Generate PDF report");
		document.addSubject("Generate PDF report");
		document.addAuthor("Java Honk");
		document.addCreator("Java Honk");
	}

	private static void addTitlePage(Document document, String postOfficeName, String roadTaxStatus)
			throws DocumentException {

		Paragraph preface = new Paragraph();
		Paragraph nextLine = new Paragraph("   ");

		if ("Printed".equals(roadTaxStatus)) {
			Paragraph p1 = new Paragraph("Summary Report of Successful Roadtax Renewal", TIME_ROMAN);
			p1.setAlignment(Element.ALIGN_CENTER);
			document.add(p1);
			p1.add(new Paragraph(" "));
			document.add(nextLine);
		} else {
			Paragraph p1 = new Paragraph("Summary Report of Unsuccessful Roadtax Renewal", TIME_ROMAN);
			p1.setAlignment(Element.ALIGN_CENTER);
			document.add(p1);
			p1.add(new Paragraph(" "));
			document.add(nextLine);
		}

		// Paragraph preface = new Paragraph();
		creteEmptyLine(preface, 0);
		preface.setAlignment(Element.ALIGN_LEFT);
		preface.add(new Paragraph("Company: Etiqa Insurance / Etiqa Takaful", TIME_ROMAN_SMALL));

		creteEmptyLine(preface, 0);
		preface.setAlignment(Element.ALIGN_LEFT);
		preface.add(new Paragraph("Post Office Code: ", TIME_ROMAN_SMALL));

		creteEmptyLine(preface, 0);
		preface.setAlignment(Element.ALIGN_LEFT);
		preface.add(new Paragraph("Post Office Name: " + postOfficeName, TIME_ROMAN_SMALL));

		creteEmptyLine(preface, 0);
		preface.setAlignment(Element.ALIGN_LEFT);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
		preface.add(new Paragraph("Transaction Date: " + simpleDateFormat.format(new Date()), TIME_ROMAN_SMALL));
		document.add(preface);

	}

	private static void creteEmptyLine(Paragraph paragraph, int number) {
		for (int i = 0; i < number; i++) {
			paragraph.add(new Paragraph(" "));
		}
	}

	/**
	 *
	 * @param document
	 * @param roadTaxTxnReportList
	 * @throws DocumentException
	 *             This method is using to display the Successful cases
	 */
	private static void createPrintedTable(Document document, List<RoadTaxTxnReport> roadTaxTxnReportList)
			throws DocumentException {
		Paragraph paragraph = new Paragraph();
		creteEmptyLine(paragraph, 2);
		document.add(paragraph);
		// PdfPTable table = new PdfPTable(5);
		PdfPTable table = new PdfPTable(new float[] { 27f, 200f, 100f, 100f, 100f });
		Paragraph nextLine = new Paragraph("   ");

		PdfPCell c1 = new PdfPCell(new Phrase("NO"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		// preface.setAlignment(Element.ALIGN_LEFT);
		// preface.add(new Paragraph("Company: Etiqa Insurance / Etiqa Takaful",
		// TIME_ROMAN_SMALL));

		c1 = new PdfPCell(new Phrase("OWNER NAME"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("ID NO*"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("VEHICLE NO"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("ROADTAX (RM)"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		table.setHeaderRows(1);

		/*
		 * for (int i = 0; i < 5; i++) { table.setWidthPercentage(100);
		 * table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		 * table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		 * table.addCell("Java"); table.addCell("Honk"); table.addCell("Success"); }
		 */

		BigDecimal roadTaxAmount = BigDecimal.ZERO;
		int noOfTransactions = 1;

		table.setWidthPercentage(100);
		table.setSpacingAfter(20);
		// table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		// table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

		for (RoadTaxTxnReport roadTaxReport : roadTaxTxnReportList) {

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(String.valueOf(noOfTransactions++));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(roadTaxReport.getDeliveryName());

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(roadTaxReport.getCustomerNricId());

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(roadTaxReport.getRegNo());

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			// table.addCell(rs.getString("road_tax_amount"));
			table.addCell(String.format("%,.2f", roadTaxReport.getRoadTaxAmount()));

			roadTaxAmount = roadTaxAmount.add(roadTaxReport.getRoadTaxAmount());

		}
		--noOfTransactions;
		System.out.println("createTable<Road Tax amount (Sum)> =>" + roadTaxAmount);

		document.add(table);

		// Todo
		// value = Double.parseDouble(new DecimalFormat("##.00").format(value));
		boolean roundCheck = true;
		double printingFee = 0.0f;
		double gst = 0.0f;
		double rounding = 0.0f;
		double subTotal = 0.0f;
		double pfPlusDf = 0.0f;
		double pfPlusDfGST = 0.0f;
		double pfDelPlusGST = 0.0f;
		double totalDeducted = 0.0f;

		double roundedValue = 0.00f;
		double adjustValue = 0.00f;

		double originalValue = 0.00f; // gst

		double roadTaxAmountDisp = 0.0f;

		roadTaxAmountDisp = Double.parseDouble(new DecimalFormat("###.000").format(roadTaxAmount.doubleValue()));
		// Okay

		printingFee = Double.parseDouble(new DecimalFormat("###.000").format(2f * noOfTransactions));
		gst = Double.parseDouble(new DecimalFormat("###.000").format(6f * (2 * noOfTransactions) / 100f));

		System.out.println("generatePDF<printingFee> =>" + printingFee);
		System.out.println("generatePDF<gst> =>" + gst);

		// Rounding
		roundedValue = roundValue(gst);
		adjustValue = gst - roundedValue;
		System.out.println("gst => " + String.format("%.2g%n", gst));
		System.out.println("roundedValue => " + String.format("%.2g%n", roundedValue));
		System.out.println("adjustValue => " + String.format("%.1g%n", adjustValue));

		String adjustValueStr = String.valueOf(adjustValue);

		String adjustValueDisp = String.format("%.1g%n", adjustValue);

		System.out.println("Successful<adjustValueStr> => " + adjustValueStr);

		if (!adjustValueStr.equals("0.0")) {
			if (adjustValueStr.contains("-")) {
				System.out.println("-ve");
				roundCheck = false;
			} else {
				System.out.println("+ve ");
				roundCheck = true;
			}
		}

		/**
		 * Sub total = roadTaxAmount + pf +roundedValue ( difference of GST & Rounding )
		 */
		subTotal = Double
				.parseDouble(new DecimalFormat("##.00").format(roadTaxAmountDisp + printingFee + roundedValue));

		//

		pfPlusDf = 6.77f * noOfTransactions;
		pfPlusDfGST = pfPlusDf * 6 / 100;
		System.out.println("pfPlusDfGST = >" + pfPlusDfGST);

		pfPlusDfGST = Double.parseDouble(new DecimalFormat("#.00").format(pfPlusDfGST));
		System.out.println("pfPlusDfGST === >" + pfPlusDfGST);

		pfDelPlusGST = pfPlusDf + pfPlusDfGST;

		pfDelPlusGST = Double.parseDouble(new DecimalFormat("##.00").format(pfDelPlusGST));

		totalDeducted = Double.parseDouble(new DecimalFormat("##.00").format(subTotal + pfDelPlusGST));

		System.out.println("Successful<roadTaxAmout> => " + String.format("%,.2f", roadTaxAmount));
		System.out.println("Successful<noOfTransactions> => " + noOfTransactions);

		System.out.println("Successful<subTotal> => " + String.format("%,.2f", subTotal));

		System.out.println("Successful<pfDelPlusGST> => " + String.format("%,.2f", pfDelPlusGST));
		System.out.println("Successful<totalDeducted> => " + String.format("%,.2f", totalDeducted));

		Paragraph p6 = new Paragraph(
				"                             		                                                                "
						+ "     TOTAL                            " + String.format("%,.2f", roadTaxAmount));
		document.add(p6);

		document.add(nextLine);
		document.add(nextLine);

		Phrase p7 = new Phrase();
		p7.add(new Chunk("No. of successful transactions:		" + noOfTransactions,
				FontFactory.getFont(FontFactory.TIMES_BOLD)));
		// p7.add(p7);
		document.add(p7);
		document.add(nextLine);

		/**
		 * For Successful cases
		 */

		Paragraph p8 = new Paragraph("RM");
		p8.setAlignment(Element.ALIGN_CENTER);

		PdfPTable T9 = new PdfPTable(3);
		T9.setWidthPercentage(100);
		T9.addCell(getCell("Roadtax", Element.ALIGN_LEFT));
		T9.addCell(getCell(String.format("%,.2f", roadTaxAmount), Element.ALIGN_CENTER));
		T9.addCell(getCell("                  ", Element.ALIGN_RIGHT));
		// document.add(T9);

		Chunk sigUnderline = new Chunk("Add:");
		sigUnderline.setUnderline(0.1f, -2f);
		Paragraph p10 = new Paragraph("");
		p10.add(sigUnderline);

		// Paragraph p11 = new Paragraph("Printing Fee "+2*noOfTransactions);
		// p11.setAlignment(Element.ALIGN_LEFT);

		PdfPTable T11 = new PdfPTable(3);
		T11.setWidthPercentage(100);
		T11.addCell(getCell("Printing Fee", Element.ALIGN_LEFT));
		T11.addCell(getCell(String.format("%,.2f", printingFee), Element.ALIGN_CENTER));
		T11.addCell(getCell("                  ", Element.ALIGN_RIGHT));
		// document.add(T9);

		/*
		 * Paragraph p12 = new
		 * Paragraph("GST                                                              "
		 * +6f*(2*noOfTransactions)/100f); p12.setAlignment(Element.ALIGN_LEFT);
		 */

		PdfPTable T12 = new PdfPTable(3);
		T12.setWidthPercentage(100);
		T12.addCell(getCell("GST", Element.ALIGN_LEFT));
		T12.addCell(getCell(String.format("%,.2f", gst), Element.ALIGN_CENTER));
		T12.addCell(getCell("                  ", Element.ALIGN_RIGHT));
		// document.add(T12);

		Font red = new Font(FontFamily.HELVETICA, 12, Font.NORMAL, BaseColor.RED);
		Font block = new Font(FontFamily.HELVETICA, 12, Font.NORMAL, BaseColor.BLACK);
		// Chunk redText = new Chunk("This text is red. ", red);
		// Paragraph p111 = new Paragraph(redText);
		// document.add(p111);

		Paragraph T13 = new Paragraph(
				new Chunk("Rounding              		                                            ",
						FontFactory.getFont(FontFactory.HELVETICA)));

		if (!adjustValueStr.equals("0.0")) {

			if (roundCheck) {
				// Paragraph T13 = new Paragraph(new Chunk("Rounding ",
				// FontFactory.getFont(FontFactory.COURIER)));
				T13.add(new Chunk("(" + String.valueOf(adjustValueDisp).substring(0, 4) + ")", red));
				T13.setAlignment(Element.ALIGN_LEFT);
			} else {
				// Paragraph T13 = new Paragraph(new Chunk("Rounding ",
				// FontFactory.getFont(FontFactory.COURIER)));
				T13.add(new Chunk(String.valueOf(adjustValueDisp).substring(1, 5), block));
				T13.setAlignment(Element.ALIGN_LEFT);
			}
		} else {
			System.out.println("Do Nothing");
			T13.add(new Chunk("0.00", block));
			T13.setAlignment(Element.ALIGN_LEFT);

		}

		/*
		 * PdfPTable T13 = new PdfPTable(3); T13.setWidthPercentage(100);
		 * T13.addCell(getCell("Rounding", PdfPCell.ALIGN_LEFT));
		 * T13.addCell(getCell(String.format("%,.2f",roadTaxAmount),
		 * PdfPCell.ALIGN_CENTER)); T13.addCell(getCell("                  ",
		 * PdfPCell.ALIGN_RIGHT));
		 */
		// document.add(T13);

		/*
		 * Paragraph p14 = new Paragraph(new
		 * Chunk("Sub total              						",
		 * FontFactory.getFont(FontFactory.TIMES_BOLD))); p14.add(new
		 * Chunk(" "+roadTaxAmount, FontFactory.getFont(FontFactory.COURIER)));
		 * p14.setAlignment(Element.ALIGN_LEFT);
		 */

		PdfPTable T14 = new PdfPTable(3);
		T14.setWidthPercentage(100);
		T14.addCell(getCell("Sub total", Element.ALIGN_LEFT));
		T14.addCell(getCell(String.format("%,.2f", subTotal), Element.ALIGN_CENTER));
		T14.addCell(getCell("                  ", Element.ALIGN_RIGHT));
		// document.add(T14);

		/*
		 * Paragraph p15 = new
		 * Paragraph("Processing fee + Delivery + GST              			"+7.20f*
		 * noOfTransactions); p15.setAlignment(Element.ALIGN_LEFT);
		 */

		PdfPTable T15 = new PdfPTable(3);
		T15.setWidthPercentage(100);
		T15.addCell(getCell("Processing fee+Delivery+GST", Element.ALIGN_LEFT));
		T15.addCell(getCell(String.format("%,.2f", pfDelPlusGST), Element.ALIGN_CENTER));
		T15.addCell(getCell("                  ", Element.ALIGN_RIGHT));
		// document.add(T15);

		/*
		 * Chunk sigUnderline1 = new Chunk("Add:"); sigUnderline1.setUnderline(0.1f,
		 * -2f); Paragraph p11 = new Paragraph(""); p11.add(sigUnderline1);
		 */

		/*
		 * Paragraph p16 = new Paragraph(new
		 * Chunk("Total float to be deducted               		",
		 * FontFactory.getFont(FontFactory.TIMES_BOLD))); p16.add(new
		 * Chunk(" "+roadTaxAmount, FontFactory.getFont(FontFactory.COURIER)));
		 * p16.setAlignment(Element.ALIGN_LEFT);
		 */

		PdfPTable T16 = new PdfPTable(3);
		T16.setWidthPercentage(100);
		T16.addCell(getCell("Total float to be deducted", Element.ALIGN_LEFT));
		T16.addCell(getCell(String.format("%,.2f", totalDeducted), Element.ALIGN_CENTER));
		T16.addCell(getCell("                  ", Element.ALIGN_RIGHT));
		// document.add(T16);

		PdfPTable L1 = new PdfPTable(3);
		L1.setWidthPercentage(100);
		L1.addCell(getCell("", Element.ALIGN_LEFT));
		L1.addCell(getCell("------------", Element.ALIGN_CENTER));
		L1.addCell(getCell("                  ", Element.ALIGN_RIGHT));
		// document.add(L1);
		PdfPTable L2 = new PdfPTable(3);
		L2.setWidthPercentage(100);
		L2.addCell(getCell("", Element.ALIGN_LEFT));
		L2.addCell(getCell("============", Element.ALIGN_CENTER));
		L2.addCell(getCell("                  ", Element.ALIGN_RIGHT));
		// document.add(L2);

		document.add(p8);
		document.add(nextLine);
		document.add(T9);
		document.add(p10);
		document.add(T11);
		document.add(T12);
		document.add(T13);
		document.add(L1);
		document.add(T14);
		document.add(T15);
		document.add(L1);
		document.add(T16);
		document.add(L2);
		document.add(nextLine);

		Paragraph p20 = new Paragraph(
				new Chunk("ID NO* - NRIC / Passport No", FontFactory.getFont(FontFactory.TIMES_BOLD)));
		p20.setAlignment(Element.ALIGN_LEFT);
		document.add(p20);
	}

	/**
	 *
	 * @param document
	 * @param roadTaxTxnReportList
	 * @throws DocumentException
	 *             This method is using to display the Un-successfull records
	 */
	private static void createUnPrintedTable(Document document, List<RoadTaxTxnReport> roadTaxTxnReportList)
			throws DocumentException {
		Paragraph paragraph = new Paragraph();
		creteEmptyLine(paragraph, 2);
		document.add(paragraph);
		// PdfPTable table = new PdfPTable(5);
		PdfPTable table = new PdfPTable(new float[] { 27f, 200f, 100f, 100f, 100f });
		Paragraph nextLine = new Paragraph("   ");

		PdfPCell c1 = new PdfPCell(new Phrase("NO"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("OWNER NAME"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("ID NO*"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("VEHICLE NO"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("ROADTAX (RM)"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		table.setHeaderRows(1);

		BigDecimal roadTaxAmount = BigDecimal.ZERO;
		int noOfTransactions = 1;

		table.setWidthPercentage(100);
		table.setSpacingAfter(20);
		// table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		// table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

		for (RoadTaxTxnReport roadTaxReport : roadTaxTxnReportList) {

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(String.valueOf(noOfTransactions++));

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(roadTaxReport.getDeliveryName());

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(roadTaxReport.getCustomerNricId());

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(roadTaxReport.getRegNo());

			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			// table.addCell(rs.getString("road_tax_amount"));
			table.addCell(String.format("%,.2f", roadTaxReport.getRoadTaxAmount()));

			roadTaxAmount = roadTaxAmount.add(roadTaxReport.getRoadTaxAmount());

		}
		--noOfTransactions;
		System.out.println("createTable<Road Tax amount (Sum)> =>" + roadTaxAmount);

		document.add(table);

		Paragraph p6 = new Paragraph(
				"                             		                                                                "
						+ "     TOTAL                            " + String.format("%,.2f", roadTaxAmount));
		document.add(p6);

		document.add(nextLine);
		document.add(nextLine);

		Phrase p7 = new Phrase();
		p7.add(new Chunk("No. of successful transactions:		" + noOfTransactions,
				FontFactory.getFont(FontFactory.TIMES_BOLD)));
		// p7.add(p7);
		document.add(p7);
		document.add(nextLine);

		Paragraph p20 = new Paragraph(
				new Chunk("ID NO* - NRIC / Passport No", FontFactory.getFont(FontFactory.TIMES_BOLD)));
		p20.setAlignment(Element.ALIGN_LEFT);
		document.add(p20);
	}

	public static PdfPCell getCell(String text, int alignment) {
		PdfPCell cell = new PdfPCell(new Phrase(text));
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(Rectangle.NO_BORDER);
		return cell;
	}

	public static double roundValue(double originalValue) {

		DecimalFormat value = new DecimalFormat("#.#");
		String roundvalueWithourDecimal = value.format(originalValue);

		DecimalFormat value1 = new DecimalFormat("0.00");
		double roundedValue = Double.parseDouble(roundvalueWithourDecimal);
		// System.out.println("roundedValue ==> " + value1.format(roundedValue));
		return roundedValue;
	}

}
