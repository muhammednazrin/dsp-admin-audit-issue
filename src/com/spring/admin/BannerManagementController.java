package com.spring.admin;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.etiqa.common.DB.ConnectionFactory;
import com.google.gson.Gson;
import com.spring.VO.Dspmiadditionalcover;
import com.spring.VO.MotorCatagory;

import oracle.jdbc.OracleTypes;

@Controller
public class BannerManagementController {
	// private static String UPLOADED_FOLDER = "C://upload//";
	private static String UPLOADED_FOLDER = "/Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs/Banner/";
	// private static String GET_UPLOADED_FOLDER =
	// "https://uat.etiqa.com.my:4442/getonline/asset/Banner/";
	private static String GET_UPLOADED_FOLDER = "https://etiqa.com.my/getonline/asset/Banner/";
	// /Weblogic/Middleware/Oracle_Home/user_projects/domains/cwp_domain/EIW_Docs/Banner

	@RequestMapping("/BannerManagement")
	public String BannerManagement(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("BannerManagement funtion ");
		HttpSession session = request.getSession();
		return "admin-banner-management";
	}

	// @ExceptionHandler(MultipartException.class)
	@RequestMapping("/createBannerManagement")
	public String createBannerManagement(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam("input-file-preview") MultipartFile file, @RequestParam("Mobie_ENG") MultipartFile Mobie_ENG,
			@RequestParam("Tab_ENG") MultipartFile Tab_ENG, @RequestParam("Desktop_BM") MultipartFile Desktop_BM,
			@RequestParam("Mobile_BM") MultipartFile Mobile_BM, @RequestParam("Tab_BM") MultipartFile Tab_BM) {
		System.out.println("BannerManagement funtion create ");
		HttpSession session = request.getSession();
		String productEntity = request.getParameter("productEntity");
		String productTypeEIB = request.getParameter("productTypeEIB");
		String productTypeETB = request.getParameter("productTypeETB");
		String image1 = request.getParameter("image1");
		String img_Mobie_ENG = request.getParameter("img_Mobie_ENG");
		String img_Tab_ENG = request.getParameter("img_Tab_ENG");
		String img_Desktop_BM = request.getParameter("img_Desktop_BM");
		String img_Mobile_BM = request.getParameter("img_Mobile_BM");
		String img_Tab_BM = request.getParameter("img_Tab_BM");
		String productagentcode = request.getParameter("productagentcode");
		Map<String, String> bnrFetch = new HashMap<String, String>();
		bnrFetch.put("BANNER_MOB", img_Mobie_ENG);
		bnrFetch.put("BANNER_TAB", img_Tab_ENG);
		bnrFetch.put("BANNER_BR", image1);
		bnrFetch.put("BANNER_MOB_BM", img_Mobile_BM);
		bnrFetch.put("BANNER_TAB_BM", img_Tab_BM);
		bnrFetch.put("BANNER_BR_BM", img_Desktop_BM);

		String productEntity_cat = "";
		if (productEntity.equalsIgnoreCase("EIB")) {
			productEntity_cat = productTypeEIB;
		} else {
			productEntity_cat = productTypeETB;
		}
		System.out.println("productEntity   :" + productEntity);
		System.out.println("productTypeEIB   :" + productTypeEIB);
		System.out.println("image1   :" + UPLOADED_FOLDER + image1);
		System.out.println("productagentcode   :" + productagentcode);

		String bnr_slnumber = null;
		Connection connection = null;
		ResultSet rs = null;
		CallableStatement proc = null;
		try {
			connection = ConnectionFactory.getConnection();
			proc = connection.prepareCall("{CALL DSP_ADM_SP_BANNER_UPLOAD(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			proc.setString(1, productEntity);
			proc.setString(2, productEntity_cat);
			proc.setString(3, image1);
			proc.setString(4, img_Mobie_ENG);
			proc.setString(5, img_Tab_ENG);
			proc.setString(6, img_Desktop_BM);
			proc.setString(7, img_Mobile_BM);
			proc.setString(8, img_Tab_BM);
			proc.setString(9, UPLOADED_FOLDER);
			proc.setString(10, "1");
			proc.setString(11, productagentcode);
			proc.registerOutParameter(12, OracleTypes.VARCHAR);
			proc.registerOutParameter(13, OracleTypes.VARCHAR);
			proc.execute();
			// rs = (ResultSet) proc.getObject(6);
			String result = proc.getString(12);
			System.out.println("RESULT   :" + result);
			bnr_slnumber = proc.getString(13);
			System.out.println("bnr_slnumber   :" + bnr_slnumber);
			bnrFetch.put("BANNER_ID", bnr_slnumber);
			session.setAttribute("banner", bnrFetch);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				proc.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		if (!file.isEmpty()) {
			String path = UPLOADED_FOLDER;
			File path1 = new File(UPLOADED_FOLDER + "/" + bnr_slnumber);
			if (!path1.exists()) {
				path1.mkdirs();
			}
			String filename = file.getOriginalFilename();
			System.out.println(path + filename);
			try {
				byte barr[] = file.getBytes();
				BufferedOutputStream bout = new BufferedOutputStream(new FileOutputStream(path1 + "/" + filename));
				bout.write(barr);
				bout.flush();
				bout.close();
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		if (!Mobie_ENG.isEmpty()) {
			String path = UPLOADED_FOLDER;
			File path1 = new File(UPLOADED_FOLDER + "/" + bnr_slnumber);
			if (!path1.exists()) {
				path1.mkdirs();
			}
			String filename = Mobie_ENG.getOriginalFilename();
			System.out.println(path + filename);
			try {
				byte barr[] = Mobie_ENG.getBytes();
				BufferedOutputStream bout = new BufferedOutputStream(new FileOutputStream(path1 + "/" + filename));
				bout.write(barr);
				bout.flush();
				bout.close();
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		if (!Tab_ENG.isEmpty()) {
			String path = UPLOADED_FOLDER;
			File path1 = new File(UPLOADED_FOLDER + "/" + bnr_slnumber);
			if (!path1.exists()) {
				path1.mkdirs();
			}
			String filename = Tab_ENG.getOriginalFilename();
			System.out.println(path + filename);
			try {
				byte barr[] = Tab_ENG.getBytes();
				BufferedOutputStream bout = new BufferedOutputStream(new FileOutputStream(path1 + "/" + filename));
				bout.write(barr);
				bout.flush();
				bout.close();
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		if (!Desktop_BM.isEmpty()) {
			String path = UPLOADED_FOLDER;
			File path1 = new File(UPLOADED_FOLDER + "/" + bnr_slnumber);
			if (!path1.exists()) {
				path1.mkdirs();
			}
			String filename = Desktop_BM.getOriginalFilename();
			System.out.println(path + filename);
			try {
				byte barr[] = Desktop_BM.getBytes();
				BufferedOutputStream bout = new BufferedOutputStream(new FileOutputStream(path1 + "/" + filename));
				bout.write(barr);
				bout.flush();
				bout.close();
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		if (!Mobile_BM.isEmpty()) {
			String path = UPLOADED_FOLDER;
			File path1 = new File(UPLOADED_FOLDER + "/" + bnr_slnumber);
			if (!path1.exists()) {
				path1.mkdirs();
			}
			String filename = Mobile_BM.getOriginalFilename();
			System.out.println(path + filename);
			try {
				byte barr[] = Mobile_BM.getBytes();
				BufferedOutputStream bout = new BufferedOutputStream(new FileOutputStream(path1 + "/" + filename));
				bout.write(barr);
				bout.flush();
				bout.close();
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		if (!Tab_BM.isEmpty()) {
			String path = UPLOADED_FOLDER;
			File path1 = new File(UPLOADED_FOLDER + "/" + bnr_slnumber);
			if (!path1.exists()) {
				path1.mkdirs();
			}
			String filename = Tab_BM.getOriginalFilename();
			System.out.println(path + filename);
			try {
				byte barr[] = Tab_BM.getBytes();
				BufferedOutputStream bout = new BufferedOutputStream(new FileOutputStream(path1 + "/" + filename));
				bout.write(barr);
				bout.flush();
				bout.close();
			} catch (Exception e) {
				System.out.println(e);
			}
		}

		return "admin-banner-management";

	}

	@RequestMapping("/MotorDynamicContentAdditional")
	public String MotorDynamicContent(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("BannerManagement funtion ");
		HttpSession session = request.getSession();

		return "admin-motor-addons";
	}

	@RequestMapping(value = "/updateMotorAddOn", method = RequestMethod.POST)
	public String updateMotorAddOn(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam("seq2") List<Integer> seq2, @RequestParam("CATEGORY_CODE") List<String> CATEGORY_CODE,
			@RequestParam("seq3") List<Integer> seq3,
			@RequestParam("ADDITIONAL_COVER_CODE") List<String> ADDITIONAL_COVER_CODE,
			@RequestParam("checkbox2_1") List<String> checkbox2_1, @RequestParam("checkbox2") List<String> checkbox2) {
		Connection connection = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		CallableStatement proc = null;

		System.out.println("ADDITIONAL_COVER_CODE            ----- " + ADDITIONAL_COVER_CODE);
		System.out.println("seq3            ----- " + seq3);
		System.out.println("checkbox2_1            ----- " + checkbox2_1);
		System.out.println("CATEGORY_CODE----- " + CATEGORY_CODE);
		System.out.println("seq2---------" + seq2);
		System.out.println("checkbox2----- " + checkbox2);
		Set<Integer> set = new HashSet<Integer>(seq2);
		if (set.size() < seq2.size()) {
			for (int i = 0; i < seq2.size(); i++) {
				// System.out.println("duplicate data "+seq2.get(i));
				String errormessage = "Please select correct sequence";
				// model.addAttribute(errormessage);
				model.addAttribute("errormessage", errormessage);
				// return "admin-motor-addons";
			}
		} else {
			System.out.println(" no duplicate data");
			for (int i = 0; i < CATEGORY_CODE.size(); i++) {
				// System.out.println(CATEGORY_CODE.get(i));
			}
			updateSequence(seq2, CATEGORY_CODE);
			updateSequenceSubCatagory(seq3, ADDITIONAL_COVER_CODE);
			updateCatagoryStatus(checkbox2, CATEGORY_CODE);
			updateCatagoryStatusSubCatagory(checkbox2_1, ADDITIONAL_COVER_CODE);
		}
		Dspmiadditionalcover Dspmiadditionalcover_add;
		List<Dspmiadditionalcover> DspmiadditionalList = new ArrayList<Dspmiadditionalcover>();
		MotorCatagory MotorCatagory_add;
		List<MotorCatagory> MotorCatagory = new ArrayList<MotorCatagory>();
		List<Integer> sequence = new ArrayList<Integer>();
		List<Integer> subsequence = new ArrayList<Integer>();
		String IN_PRODUCT_CODE = "EIB";
		try {
			connection = ConnectionFactory.getConnection();
			proc = connection.prepareCall("{CALL DSP_ADM_SP_MI_ADDI_COVER_PACKA(?,?,?,?)}");
			proc.setString(1, "lan_en");
			proc.setString(2, IN_PRODUCT_CODE);
			proc.registerOutParameter(3, OracleTypes.CURSOR);
			proc.registerOutParameter(4, OracleTypes.CURSOR);
			// proc.registerOutParameter(3, OracleTypes.INTEGER);
			proc.execute();
			rs1 = (ResultSet) proc.getObject(3);
			rs = (ResultSet) proc.getObject(4);
			// System.out.println(rs1);
			while (rs.next()) {
				MotorCatagory_add = new MotorCatagory();
				MotorCatagory_add.setCATEGORY_CODE(rs.getString("CATEGORY_CODE"));
				MotorCatagory_add.setCATEGORY_NAME(rs.getString("CATEGORY_NAME"));
				MotorCatagory_add.setCATEGORY_DISPLAY(rs.getString("CATEGORY_DISPLAY"));
				MotorCatagory_add.setSequence(rs.getInt("CATEGORY_DISPLAY"));
				MotorCatagory_add.setSEQUENCE_COUNT(rs.getString("CATEGORY_DISPLAY"));
				MotorCatagory_add.setSTATUS(rs.getString("STATUS"));
				MotorCatagory_add.setADDONS_OPTION(rs.getString("ADDONS_OPTION"));
				MotorCatagory.add(MotorCatagory_add);
				// System.out.println(rs.getInt("CATEGORY_DISPLAY")+"::::::::"+rs.getString("CATEGORY_CODE")+"
				// "+rs.getString("CATEGORY_NAME")+" "+rs.getString("CATEGORY_DISPLAY")+"
				// "+rs.getString("STATUS"));
			}
			for (MotorCatagory item : MotorCatagory) {
				// System.out.println(item.getSequence());
				sequence.add(item.getSequence());
			}
			model.addAttribute("addtional", MotorCatagory);
			model.addAttribute("SequenceDropdown", sequence);
			while (rs1.next()) {
				// System.out.println(rs1.getString("CATEGORY_CODE")+"::::::::"+rs1.getString("CATEGORY_NAME")+""
				// + " "+rs1.getString("ADDITIONAL_COVER_CODE")+"
				// "+rs1.getString("ADDITIONAL_COVER_NAME")+"
				// "+rs1.getString("ADDITIONAL_COVER_TYPE"));
				Dspmiadditionalcover_add = new Dspmiadditionalcover();
				Dspmiadditionalcover_add.setCATEGORY_CODE(rs1.getString("CATEGORY_CODE"));
				Dspmiadditionalcover_add.setCATEGORY_NAME(rs1.getString("CATEGORY_NAME"));
				Dspmiadditionalcover_add.setADDITIONAL_COVER_CODE(rs1.getString("ADDITIONAL_COVER_CODE"));
				Dspmiadditionalcover_add.setADDITIONAL_COVER_NAME(rs1.getString("ADDITIONAL_COVER_NAME"));
				Dspmiadditionalcover_add.setADDITIONAL_COVER_TYPE(rs1.getString("ADDITIONAL_COVER_TYPE"));
				Dspmiadditionalcover_add.setCATEGORY_DISPLAY(rs1.getString("CATEGORY_DISPLAY"));
				Dspmiadditionalcover_add.setADDONS_DISPLAY(rs1.getString("ADDONS_DISPLAY"));
				Dspmiadditionalcover_add.setMIN_AMOUNT(rs1.getString("MIN_AMOUNT"));
				Dspmiadditionalcover_add.setMAX_AMOUNT(rs1.getString("MAX_AMOUNT"));
				Dspmiadditionalcover_add.setCOVER_CODE(rs1.getString("COVER_CODE"));
				Dspmiadditionalcover_add.setFRONT_FLAG(rs1.getString("FRONT_FLAG"));
				Dspmiadditionalcover_add.setTOOL_TIP(rs1.getString("TOOL_TIP"));
				Dspmiadditionalcover_add.setSTATUS(rs1.getString("STATUS"));
				Dspmiadditionalcover_add.setADDONS_OPTION(rs1.getString("ADDONS_OPTION"));
				Dspmiadditionalcover_add.setDEFAULT_AMOUNT(rs1.getString("DEFAULT_AMOUNT"));
				DspmiadditionalList.add(Dspmiadditionalcover_add);
			}
			subsequence.add(1);
			subsequence.add(2);
			subsequence.add(3);
			subsequence.add(4);
			subsequence.add(5);
			subsequence.add(6);
			subsequence.add(7);
			subsequence.add(8);
			subsequence.add(9);

			model.addAttribute("subsequence", subsequence);
			model.addAttribute("Subcatagorylist", DspmiadditionalList);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				proc.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return "admin-motor-addons";
	}

	@RequestMapping("/fetchMotorAddOn")
	public String fetchMotorAddOn(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("updateMotorAddOn funtion to checkadd onn ");
		// HttpSession session=request.getSession();
		Dspmiadditionalcover Dspmiadditionalcover_add;
		List<Dspmiadditionalcover> DspmiadditionalList = new ArrayList<Dspmiadditionalcover>();
		MotorCatagory MotorCatagory_add;
		List<MotorCatagory> MotorCatagory = new ArrayList<MotorCatagory>();
		List<Integer> sequence = new ArrayList<Integer>();
		List<Integer> subsequence = new ArrayList<Integer>();
		Connection connection = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		CallableStatement proc = null;
		String IN_PRODUCT_CODE = "EIB";
		try {
			connection = ConnectionFactory.getConnection();
			proc = connection.prepareCall("{CALL DSP_ADM_SP_MI_ADDI_COVER_PACKA(?,?,?,?)}");
			proc.setString(1, "lan_en");
			proc.setString(2, IN_PRODUCT_CODE);
			proc.registerOutParameter(3, OracleTypes.CURSOR);
			proc.registerOutParameter(4, OracleTypes.CURSOR);
			// proc.registerOutParameter(3, OracleTypes.INTEGER);
			proc.execute();
			rs1 = (ResultSet) proc.getObject(3);
			rs = (ResultSet) proc.getObject(4);
			// System.out.println(rs1);
			while (rs.next()) {
				MotorCatagory_add = new MotorCatagory();
				MotorCatagory_add.setCATEGORY_CODE(rs.getString("CATEGORY_CODE"));
				MotorCatagory_add.setCATEGORY_NAME(rs.getString("CATEGORY_NAME"));
				MotorCatagory_add.setCATEGORY_DISPLAY(rs.getString("CATEGORY_DISPLAY"));
				MotorCatagory_add.setSequence(rs.getInt("CATEGORY_DISPLAY"));
				MotorCatagory_add.setSEQUENCE_COUNT(rs.getString("CATEGORY_DISPLAY"));
				MotorCatagory_add.setSTATUS(rs.getString("STATUS"));
				MotorCatagory_add.setADDONS_OPTION(rs.getString("ADDONS_OPTION"));
				MotorCatagory.add(MotorCatagory_add);
				// System.out.println(rs.getInt("CATEGORY_DISPLAY")+"::::::::"+rs.getString("CATEGORY_CODE")+"
				// "+rs.getString("CATEGORY_NAME")+" "+rs.getString("CATEGORY_DISPLAY")+"
				// "+rs.getString("STATUS"));
			}
			for (MotorCatagory item : MotorCatagory) {
				// System.out.println(item.getSequence());
				sequence.add(item.getSequence());
			}
			model.addAttribute("addtional", MotorCatagory);
			model.addAttribute("SequenceDropdown", sequence);
			while (rs1.next()) {
				// System.out.println(rs1.getString("CATEGORY_CODE")+"::::::::"+rs1.getString("CATEGORY_NAME")+""
				// + " "+rs1.getString("ADDITIONAL_COVER_CODE")+"
				// "+rs1.getString("ADDITIONAL_COVER_NAME")+"
				// "+rs1.getString("ADDITIONAL_COVER_TYPE"));
				Dspmiadditionalcover_add = new Dspmiadditionalcover();
				Dspmiadditionalcover_add.setCATEGORY_CODE(rs1.getString("CATEGORY_CODE"));
				Dspmiadditionalcover_add.setCATEGORY_NAME(rs1.getString("CATEGORY_NAME"));
				Dspmiadditionalcover_add.setADDITIONAL_COVER_CODE(rs1.getString("ADDITIONAL_COVER_CODE"));
				Dspmiadditionalcover_add.setADDITIONAL_COVER_NAME(rs1.getString("ADDITIONAL_COVER_NAME"));
				Dspmiadditionalcover_add.setADDITIONAL_COVER_TYPE(rs1.getString("ADDITIONAL_COVER_TYPE"));
				Dspmiadditionalcover_add.setCATEGORY_DISPLAY(rs1.getString("CATEGORY_DISPLAY"));
				Dspmiadditionalcover_add.setADDONS_DISPLAY(rs1.getString("ADDONS_DISPLAY"));
				Dspmiadditionalcover_add.setMIN_AMOUNT(rs1.getString("MIN_AMOUNT"));
				Dspmiadditionalcover_add.setMAX_AMOUNT(rs1.getString("MAX_AMOUNT"));
				Dspmiadditionalcover_add.setCOVER_CODE(rs1.getString("COVER_CODE"));
				Dspmiadditionalcover_add.setFRONT_FLAG(rs1.getString("FRONT_FLAG"));
				Dspmiadditionalcover_add.setTOOL_TIP(rs1.getString("TOOL_TIP"));
				Dspmiadditionalcover_add.setSTATUS(rs1.getString("STATUS"));
				Dspmiadditionalcover_add.setADDONS_OPTION(rs1.getString("ADDONS_OPTION"));
				Dspmiadditionalcover_add.setDEFAULT_AMOUNT(rs1.getString("DEFAULT_AMOUNT"));
				DspmiadditionalList.add(Dspmiadditionalcover_add);
			}
			subsequence.add(1);
			subsequence.add(2);
			subsequence.add(3);
			subsequence.add(4);
			subsequence.add(5);
			subsequence.add(6);
			subsequence.add(7);
			subsequence.add(8);
			subsequence.add(9);

			model.addAttribute("subsequence", subsequence);
			model.addAttribute("Subcatagorylist", DspmiadditionalList);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				proc.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return "admin-motor-addons";
	}

	@RequestMapping(value = "/updateMotorAddOnpopupfr", method = RequestMethod.POST)
	@ResponseBody
	public String updateMotorAddOnpopupfr(HttpServletResponse response, HttpServletRequest request) {
		String nameEn = request.getParameter("nameEn");
		String nameBm = request.getParameter("nameBm");
		String productCode = request.getParameter("productCode");
		Connection connection = null;
		CallableStatement proc = null;
		String result = "";
		try {
			connection = ConnectionFactory.getConnection();
			proc = connection.prepareCall("{CALL DSP_ADM_MOTOR_CAT_UPDATE(?,?,?,?)}");
			proc.setString(1, productCode);
			proc.setString(2, nameEn);
			proc.setString(3, nameBm);
			proc.registerOutParameter(4, OracleTypes.INTEGER);
			proc.execute();
			result = proc.getString(4);
			System.out.println("RESULT   :" + result);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				proc.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return nameEn + "   " + nameBm;
	}

	@RequestMapping(value = "/updateMotorAddOnpopupfr_SUB", method = RequestMethod.POST)
	@ResponseBody
	public String updateMotorAddOnpopupfr_SUB(HttpServletResponse response, HttpServletRequest request) {
		String nameEnSB = request.getParameter("nameEnSB");
		String nameBmSB = request.getParameter("nameBmSB");
		String toolTipEnSB = request.getParameter("toolTipEnSB");
		String toolTipBmSB = request.getParameter("toolTipBmSB");
		String minValueSB = request.getParameter("minValueSB");
		String maxValueSB = request.getParameter("maxValueSB");
		String DefultValueSB = request.getParameter("DefultValueSB");
		String productCode = request.getParameter("productCode");

		Connection connection = null;
		CallableStatement proc = null;
		String result = "";
		try {
			connection = ConnectionFactory.getConnection();
			proc = connection.prepareCall("{CALL DSP_ADM_MOTOR_SUBCAT_UPDATE(?,?,?,?,?,?,?,?,?)}");
			proc.setString(1, productCode);
			proc.setString(2, nameEnSB);
			proc.setString(3, nameBmSB);
			proc.setString(4, toolTipEnSB);
			proc.setString(5, toolTipBmSB);
			proc.setString(6, minValueSB);
			proc.setString(7, maxValueSB);
			proc.setString(8, DefultValueSB);
			proc.registerOutParameter(9, OracleTypes.INTEGER);
			proc.execute();
			result = proc.getString(9);
			System.out.println("RESULT   :" + result);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				proc.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@RequestMapping(value = "/fetchMotorAddOnpopupfr", method = RequestMethod.POST)
	@ResponseBody
	public String fetchMotorAddOnpopupfr(HttpServletResponse response, HttpServletRequest request, Model model) {
		String PRODUCT_CODE = request.getParameter("beniftName");
		Map<String, String> CATEGORY_NAME = new HashMap<String, String>();
		Connection connection = null;
		CallableStatement proc = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		String result = "";
		try {
			connection = ConnectionFactory.getConnection();
			proc = connection.prepareCall("{CALL DSP_ADM_SP_MI_FETCH_CATAGORY(?,?)}");
			proc.setString(1, PRODUCT_CODE);
			proc.registerOutParameter(2, OracleTypes.CURSOR);
			// proc.registerOutParameter(3,OracleTypes.CURSOR);
			// proc.registerOutParameter(3, OracleTypes.INTEGER);
			proc.execute();
			rs = (ResultSet) proc.getObject(2);
			// rs1 = (ResultSet) proc.getObject(3);
			while (rs.next()) {
				CATEGORY_NAME.put("CATEGORY_NAME_ENG", rs.getString("CATEGORY_NAME_ENG"));
				CATEGORY_NAME.put("CATEGORY_NAME_BM", rs.getString("CATEGORY_NAME_BM"));
				CATEGORY_NAME.put("PRODUCT_CODE", PRODUCT_CODE);
			}
			System.out.println("RESULT   :" + result);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				proc.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		Gson gson = new Gson();
		String json = gson.toJson(CATEGORY_NAME);
		return json;
	}

	@RequestMapping(value = "/fetchMotorAddOnpopupfr_sub", method = RequestMethod.POST)
	@ResponseBody
	public String fetchMotorAddOnpopupfr_sub(HttpServletResponse response, HttpServletRequest request) {
		String success = request.getParameter("beniftName");
		Map<String, String> SUB_CATEGORY_NAME = new HashMap<String, String>();
		Connection connection = null;
		CallableStatement proc = null;
		ResultSet rs1 = null;
		String result = "";
		try {
			connection = ConnectionFactory.getConnection();
			proc = connection.prepareCall("{CALL DSP_ADM_SP_MI_FETCH_SUB_CATAG(?,?)}");
			proc.setString(1, success);
			proc.registerOutParameter(2, OracleTypes.CURSOR);
			// proc.registerOutParameter(3,OracleTypes.CURSOR);
			// proc.registerOutParameter(3, OracleTypes.INTEGER);
			proc.execute();
			rs1 = (ResultSet) proc.getObject(2);
			// rs1 = (ResultSet) proc.getObject(3);
			while (rs1.next()) {
				SUB_CATEGORY_NAME.put("ADDITIONAL_COVER_CODE", rs1.getString("ADDITIONAL_COVER_CODE"));
				SUB_CATEGORY_NAME.put("ADDITIONAL_COVER_TYPE", rs1.getString("ADDITIONAL_COVER_TYPE"));
				SUB_CATEGORY_NAME.put("ADDITIONAL_COVER_NAME", rs1.getString("ADDITIONAL_COVER_NAME"));
				SUB_CATEGORY_NAME.put("ADDITIONAL_COVER_NAME1", rs1.getString("ADDITIONAL_COVER_NAME1"));
				SUB_CATEGORY_NAME.put("MIN_AMOUNT", rs1.getString("MIN_AMOUNT"));
				SUB_CATEGORY_NAME.put("MAX_AMOUNT", rs1.getString("MAX_AMOUNT"));
				SUB_CATEGORY_NAME.put("COVER_CODE", rs1.getString("COVER_CODE"));
				SUB_CATEGORY_NAME.put("TOOL_TIP", rs1.getString("TOOL_TIP"));
				SUB_CATEGORY_NAME.put("FRONT_FLAG", rs1.getString("FRONT_FLAG"));
				SUB_CATEGORY_NAME.put("TOOL_TIP1", rs1.getString("TOOL_TIP1"));
				SUB_CATEGORY_NAME.put("DEFAULT_AMOUNT", rs1.getString("DEFAULT_AMOUNT"));
				SUB_CATEGORY_NAME.put("productCode", success);
				System.out.println("ADDITIONAL_COVER_CODE   :" + rs1.getString("ADDITIONAL_COVER_CODE"));
			}
			System.out.println("RESULT   :" + result);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				proc.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		Gson gson = new Gson();
		String json = gson.toJson(SUB_CATEGORY_NAME);
		return json;
	}

	@RequestMapping("/dynamicContentManagement")
	public String dynamicContentManagement(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("dynamicContentManagement funtion ");
		return "admin-motor-dynamic-content";
	}

	@RequestMapping("/insertDynamicContentManagement")
	public String insertDynamicContentManagement(HttpServletRequest request, HttpServletResponse response,
			Model model) {
		System.out.println("dynamicContentManagement funtion ");
		HttpSession session = request.getSession();

		String productEntity = request.getParameter("productEntity");
		String productTypeEIB = request.getParameter("productTypeEIB");
		String productTypeETB = request.getParameter("productTypeETB");
		String pages = request.getParameter("pages");
		String area = request.getParameter("area");
		String nameEn = request.getParameter("nameEn");
		String nameBm = request.getParameter("nameBm");

		String productEntity_cat = "";
		if (productEntity.equalsIgnoreCase("EIB")) {
			productEntity_cat = productTypeEIB;
		} else {
			productEntity_cat = productTypeETB;
		}

		Connection connection = null;
		ResultSet rs = null;
		CallableStatement proc = null;
		try {
			connection = ConnectionFactory.getConnection();
			proc = connection.prepareCall("{CALL DSP_ADM_SP_DYNAMIC_CONTENT(?,?,?,?,?,?,?)}");
			proc.setString(1, productEntity);
			proc.setString(2, productEntity_cat);
			proc.setString(3, pages);
			proc.setString(4, area);
			proc.setString(5, nameEn);
			proc.setString(6, nameBm);
			proc.registerOutParameter(7, OracleTypes.VARCHAR);
			proc.execute();
			String result = proc.getString(7);
			System.out.println("RESULT   :" + result);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				proc.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return "admin-motor-dynamic-content";
	}

	/*
	 * @GetMapping("view-poster/{BANNER_BR}") public ResponseEntity<byte[]>
	 * getImage1(@PathVariable(name = "BANNER_BR") String
	 * BANNER_BR,HttpServletRequest request)throws IOException { //
	 * System.out.println("outreachid  :"+BANNER_BR); HttpSession
	 * session=request.getSession(); Map<String, String> banner = new
	 * HashMap<String, String>(); if (session.getAttribute("banner") != null) {
	 * banner = (Map)session.getAttribute("banner"); } String
	 * path=banner.get(BANNER_BR); String BANNER_ID=banner.get("BANNER_ID"); //
	 * System.out.println("path   :"+path+"     BANNER_ID    :"+ BANNER_ID);
	 * //String FILE_PATH = UPLOADED_FOLDER+"img1.JPG"; String FILE_PATH =
	 * UPLOADED_FOLDER+BANNER_ID+"/"+path;
	 * System.out.println("FILE_PATH  :"+FILE_PATH); File img = new File(FILE_PATH);
	 * return ResponseEntity .ok() .contentType(
	 * MediaType.valueOf(FileTypeMap.getDefaultFileTypeMap() .getContentType(img)))
	 * .body(Files.readAllBytes(img.toPath())); }
	 */

	@RequestMapping(value = "/fetchBannerImage", method = RequestMethod.POST)
	public @ResponseBody String fetchBannerImage(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam String productagentcode, @RequestParam String productEntity,
			@RequestParam String productTypeEIB, @RequestParam String productTypeETB) {
		Map<String, String> bnrFetch = new HashMap<String, String>();
		System.out.println("productagentcode   :" + productagentcode);
		System.out.println("productEntity   :" + productEntity);
		System.out.println("productTypeEIB   :" + productTypeEIB);
		System.out.println("productTypeETB   :" + productTypeETB);
		String entity = "";
		if (productEntity.equalsIgnoreCase("EIB")) {
			entity = productTypeEIB;
		} else {
			entity = productTypeETB;
		}
		HttpSession session = request.getSession();
		session.setAttribute("banner", "");
		Connection connection = null;
		ResultSet rs = null;
		CallableStatement proc = null;
		try {
			connection = ConnectionFactory.getConnection();
			proc = connection.prepareCall("{CALL DSP_ADM_SP_BANNER_FETCH(?,?,?,?,?)}");
			proc.setString(1, productEntity);
			proc.setString(2, entity);
			proc.setString(3, productagentcode);
			proc.registerOutParameter(4, OracleTypes.CURSOR);
			proc.registerOutParameter(5, OracleTypes.INTEGER);
			proc.execute();

			rs = (ResultSet) proc.getObject(4);
			String bnr_slnumber = proc.getString(5);
			System.out.println("bnr_slnumber   :" + bnr_slnumber);
			while (rs.next()) {
				bnrFetch.put("BANNER_MOB", rs.getString("BANNER_MOB"));
				bnrFetch.put("BANNER_TAB", rs.getString("BANNER_TAB"));
				bnrFetch.put("BANNER_BR", rs.getString("BANNER_BR"));
				bnrFetch.put("BANNER_MOB_BM", rs.getString("BANNER_MOB_BM"));
				bnrFetch.put("BANNER_TAB_BM", rs.getString("BANNER_TAB_BM"));
				bnrFetch.put("BANNER_BR_BM", rs.getString("BANNER_BR_BM"));
				bnrFetch.put("BANNER_ID", rs.getString("BNR_ID"));
				bnrFetch.put("BANNER_ID_PATH", GET_UPLOADED_FOLDER + rs.getString("BNR_ID"));
				System.out.println(rs.getString("BANNER_MOB"));
			}
			System.out.println("BANNER_ID    :" + bnrFetch.get("BANNER_ID"));

			// model.addAttribute("banner", bnrFetch);
			session.setAttribute("banner", bnrFetch);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				proc.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		Gson gson = new Gson();
		String jsonResult = gson.toJson(bnrFetch);
		return jsonResult;
	}

	public String updateCatagoryStatus(List<String> checkbox2, List<String> cATEGORY_CODE) {
		System.out.println(cATEGORY_CODE.removeAll(checkbox2));
		Connection connection = null;
		CallableStatement proc = null;
		String result = "";
		try {
			for (int i = 0; i < checkbox2.size(); i++) {
				connection = ConnectionFactory.getConnection();
				proc = connection.prepareCall("{CALL DSP_ADM_MOTOR_CAT_UPDAT_STATUS(?,?,?)}");
				proc.setString(1, checkbox2.get(i));
				proc.setString(2, "ACTIVE");
				proc.registerOutParameter(3, OracleTypes.INTEGER);
				proc.execute();
				result = proc.getString(3);
			}
			for (int i = 0; i < cATEGORY_CODE.size(); i++) {
				connection = ConnectionFactory.getConnection();
				proc = connection.prepareCall("{CALL DSP_ADM_MOTOR_CAT_UPDAT_STATUS(?,?,?)}");
				proc.setString(1, cATEGORY_CODE.get(i));
				proc.setString(2, "INACTIVE");
				proc.registerOutParameter(3, OracleTypes.INTEGER);
				proc.execute();
				result = proc.getString(3);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				proc.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public String updateSequence(List<Integer> seq2, List<String> cATEGORY_CODE) {
		Connection connection = null;
		CallableStatement proc = null;
		String result = "";
		try {
			for (int i = 0; i < cATEGORY_CODE.size(); i++) {
				// System.out.println("cATEGORY_CODE.get(i) :"+cATEGORY_CODE.get(i));
				// System.out.println("seq2.get(i) :"+seq2.get(i));
				connection = ConnectionFactory.getConnection();
				proc = connection.prepareCall("{CALL DSP_ADM_MOTOR_CAT_UPDATE_SEQ(?,?,?)}");
				proc.setString(1, cATEGORY_CODE.get(i));
				proc.setInt(2, seq2.get(i));
				proc.registerOutParameter(3, OracleTypes.INTEGER);
				proc.execute();
				result = proc.getString(3);
				// System.out.println("RESULT :"+result);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				proc.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public String updateCatagoryStatusSubCatagory(List<String> checkbox2_1, List<String> aDDITIONAL_COVER_CODE) {
		System.out.println(aDDITIONAL_COVER_CODE.removeAll(checkbox2_1));
		Connection connection = null;
		CallableStatement proc = null;
		String result = "";
		try {
			for (int i = 0; i < checkbox2_1.size(); i++) {
				connection = ConnectionFactory.getConnection();
				proc = connection.prepareCall("{CALL DSP_ADM_MOTOR_CAT_UPDAT_SUB_ST(?,?,?)}");
				proc.setString(1, checkbox2_1.get(i));
				proc.setString(2, "ACTIVE");
				proc.registerOutParameter(3, OracleTypes.INTEGER);
				proc.execute();
				result = proc.getString(3);
			}
			for (int i = 0; i < aDDITIONAL_COVER_CODE.size(); i++) {
				connection = ConnectionFactory.getConnection();
				proc = connection.prepareCall("{CALL DSP_ADM_MOTOR_CAT_UPDAT_SUB_ST(?,?,?)}");
				proc.setString(1, aDDITIONAL_COVER_CODE.get(i));
				proc.setString(2, "INACTIVE");
				proc.registerOutParameter(3, OracleTypes.INTEGER);
				proc.execute();
				result = proc.getString(3);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				proc.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;

	}

	public String updateSequenceSubCatagory(List<Integer> seq3, List<String> aDDITIONAL_COVER_CODE) {
		Connection connection = null;
		CallableStatement proc = null;
		String result = "";
		try {
			for (int i = 0; i < aDDITIONAL_COVER_CODE.size(); i++) {
				// System.out.println("cATEGORY_CODE.get(i) :"+cATEGORY_CODE.get(i));
				// System.out.println("seq2.get(i) :"+seq2.get(i));
				connection = ConnectionFactory.getConnection();
				proc = connection.prepareCall("{CALL DSP_ADM_MOTOR_CAT_UPDAT_SUB_SE(?,?,?)}");
				proc.setString(1, aDDITIONAL_COVER_CODE.get(i));
				proc.setInt(2, seq3.get(i));
				proc.registerOutParameter(3, OracleTypes.INTEGER);
				proc.execute();
				result = proc.getString(3);
				// System.out.println("RESULT :"+result);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				proc.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@RequestMapping(value = "/fetchAgentcode", method = RequestMethod.POST)
	public @ResponseBody String fetchAgentcode(HttpServletRequest request, HttpServletResponse response,
			@RequestParam String agentcode, @RequestParam String productTypeEIB, @RequestParam String productTypeETB,
			@RequestParam String productEntity) {
		// Map<String, String> bnrFetch = new HashMap<String, String>();
		ArrayList<String> bnrFetch = new ArrayList<String>();
		bnrFetch.add("ALL");
		String entity = "";
		if (productEntity.equalsIgnoreCase("EIB")) {
			entity = productTypeEIB;
		} else {
			entity = productTypeETB;
		}
		Connection connection = null;
		ResultSet rs = null;
		PreparedStatement proc = null;
		try {
			connection = ConnectionFactory.getConnection();
			proc = connection.prepareStatement(
					"select * from DSP_ADM_TBL_AGENT_PROD_MAP where PRODUCT_CODE=? AND upper(AGENT_CODE) LIKE ? or lower(AGENT_CODE) LIKE ?");
			// PreparedStatement stmt=con.prepareStatement("insert into Emp values(?,?)");
			// // AGENT_CODE
			proc.setString(1, entity);
			proc.setString(2, agentcode + "%");
			proc.setString(3, agentcode + "%");
			proc.execute();
			rs = proc.executeQuery();
			while (rs.next()) {
				bnrFetch.add(rs.getString(2));
				System.out.println(rs.getInt(1) + " " + rs.getString(2));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				proc.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		Gson gson = new Gson();
		String jsonResult = gson.toJson(bnrFetch);
		return jsonResult;
	}

	@RequestMapping(value = "/fetchContentDescription", method = RequestMethod.POST)
	public @ResponseBody String fetchContentDescription(HttpServletRequest request, HttpServletResponse response,
			@RequestParam String area, @RequestParam String pages, @RequestParam String productTypeEIB,
			@RequestParam String productTypeETB, @RequestParam String productEntity) {
		ArrayList<String> bnrFetch = new ArrayList<String>();
		String entity = "";
		if (productEntity.equalsIgnoreCase("EIB")) {
			entity = productTypeEIB;
		} else {
			entity = productTypeETB;
		}
		Connection connection = null;
		ResultSet rs = null;
		PreparedStatement proc = null;
		try {
			connection = ConnectionFactory.getConnection();
			proc = connection.prepareStatement(
					"select * from DSP_MI_TBL_DYNAMIC_CONTENT where ENTITY=? and SALES_FUNNEL=? and PAGE=? and AREA=? and STATUS=?");
			proc.setString(1, productEntity);
			proc.setString(2, entity);
			proc.setString(3, pages);
			proc.setString(4, area);
			proc.setString(5, "1");
			proc.execute();
			rs = proc.executeQuery();
			while (rs.next()) {
				bnrFetch.add(rs.getString(6));
				bnrFetch.add(rs.getString(7));
				System.out.println(rs.getInt(1) + " " + rs.getString(2));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				proc.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		Gson gson = new Gson();
		String jsonResult = gson.toJson(bnrFetch);
		return jsonResult;
	}

	@RequestMapping(value = "/deleteContentDescription", method = RequestMethod.POST)
	public @ResponseBody String deleteContentDescription(HttpServletRequest request, HttpServletResponse response,
			@RequestParam String area, @RequestParam String pages, @RequestParam String productTypeEIB,
			@RequestParam String productTypeETB, @RequestParam String productEntity) {
		ArrayList<String> bnrFetch = new ArrayList<String>();
		String entity = "";
		if (productEntity.equalsIgnoreCase("EIB")) {
			entity = productTypeEIB;
		} else {
			entity = productTypeETB;
		}
		Connection connection = null;
		ResultSet rs = null;
		PreparedStatement proc = null;
		try {
			connection = ConnectionFactory.getConnection();
			proc = connection.prepareStatement(
					"UPDATE DSP_MI_TBL_DYNAMIC_CONTENT set STATUS=? where ENTITY=? and SALES_FUNNEL=? and PAGE=? and AREA=?");
			proc.setString(1, "0");
			proc.setString(2, productEntity);
			proc.setString(3, entity);
			proc.setString(4, pages);
			proc.setString(5, area);

			proc.executeUpdate();
			// rs=proc.executeUpdate();
			/*
			 * while(rs.next()){ bnrFetch.add(rs.getString(6));
			 * bnrFetch.add(rs.getString(7));
			 * System.out.println(rs.getInt(1)+" "+rs.getString(2)); }
			 */

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				proc.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		Gson gson = new Gson();
		String jsonResult = gson.toJson(bnrFetch);
		return jsonResult;
	}

}
