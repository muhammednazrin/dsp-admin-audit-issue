package com.spring.admin;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.etiqa.utils.convertBytes;
import com.spring.VO.AgentProfile;
import com.spring.VO.Approval;
import com.spring.VO.ApprovalExample;
import com.spring.VO.ApprovalLog;
import com.spring.VO.ApprovalLogExample;
import com.spring.VO.CommonTblUnderwritingQA;
import com.spring.VO.CommonTblUnderwritingQAExample;
import com.spring.VO.MIProductApprovalRate;
import com.spring.mapper.AdminParamMapper;
import com.spring.mapper.AgentDocumentMapper;
import com.spring.mapper.AgentLinkMapper;
import com.spring.mapper.AgentProdMapMapper;
import com.spring.mapper.AgentProfileMapper;
import com.spring.mapper.ApprovalLogMapper;
import com.spring.mapper.ApprovalMapper;
import com.spring.mapper.CommonQQMapper;
import com.spring.mapper.CommonTblUnderwritingQAMapper;
import com.spring.mapper.ProductsMapper;
import com.spring.mapper.WTCTblDometicMapper;
import com.spring.mapper.WTCTblIntwithdaysMapper;
import com.spring.mapper.WTCTblParamMapper;
import com.spring.utils.ServiceValidationUtils;

@Controller
public class AdminRulesMngController {

	CommonTblUnderwritingQAMapper commonTblUnderwritingQAMapper;
	ApprovalMapper approvalMapper;
	ApprovalLogMapper approvalLogMapper;
	AgentProfileMapper agentProfileMapper;
	ProductsMapper productsMapper;
	AgentProdMapMapper agentProdMapMapper;
	CommonQQMapper commonQQMapper;
	AdminParamMapper adminParamMapper;
	AgentDocumentMapper agentDocumentMapper;
	AgentLinkMapper agentLinkMapper;
	WTCTblParamMapper wTCTblParamMapper;
	WTCTblIntwithdaysMapper wTCTblIntwithdaysMapper;
	WTCTblDometicMapper wTCTblDometicMapper;

	@Autowired
	public AdminRulesMngController(CommonTblUnderwritingQAMapper commonTblUnderwritingQAMapper,
			ApprovalMapper approvalMapper, ApprovalLogMapper approvalLogMapper, WTCTblParamMapper wTCTblParamMapper,
			WTCTblIntwithdaysMapper wTCTblIntwithdaysMapper, WTCTblDometicMapper wTCTblDometicMapper) {
		this.commonTblUnderwritingQAMapper = commonTblUnderwritingQAMapper;
		this.approvalMapper = approvalMapper;
		this.approvalLogMapper = approvalLogMapper;
		this.wTCTblParamMapper = wTCTblParamMapper;
		this.wTCTblIntwithdaysMapper = wTCTblIntwithdaysMapper;
		this.wTCTblDometicMapper = wTCTblDometicMapper;

	}

	@RequestMapping("/RulesList")
	public String RulesList(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("RulesList funtion ");
		HttpSession session = request.getSession();
		// session.setAttribute("user", "000777");
		String loginUser = (String) session.getAttribute("user");
		System.out.println("  user  session  :::    " + loginUser);
		/*
		 * if(loginUser == null) { String sessionexpired = "Session Has Been Expired";
		 * model.addAttribute("sessionexpired", sessionexpired); return "admin-login"; }
		 */

		return "products/admin-business-mng-rules";
	}

	@RequestMapping("/AddRules")
	public String AddRules(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("AddRules funtion ");
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		return "products/admin-business-mng-rules-add";
	}

	@RequestMapping(value = "/SaveRulesDone", method = RequestMethod.POST)
	public String SaveRulesDone(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("RulesList funtion ");
		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		CommonTblUnderwritingQA commonTblUnderwritingQA = new CommonTblUnderwritingQA();

		commonTblUnderwritingQA.setQuestionDescription(request.getParameter("questionDescription"));
		commonTblUnderwritingQA.setQuestionDescriptionBm(request.getParameter("questionDescriptionBm"));
		commonTblUnderwritingQA.setCorrectAnswerCode(request.getParameter("correctAnswerCode"));

		commonTblUnderwritingQAMapper.insert(commonTblUnderwritingQA);
		model.addAttribute("nvicAddedMessage", " Data Saved Successfully!");
		return RulesList(request, response, model);
	}

	@RequestMapping(value = "/ViewRules", method = RequestMethod.POST)
	public String ViewRules(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("RulesList view ");
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		/*
		 * if(loginUser == null) { String sessionexpired = "Session Has Been Expired";
		 * model.addAttribute("sessionexpired", sessionexpired); return "admin-login"; }
		 */
		String prodcode = "";
		if (request.getParameter("prodcode") != null) {
			prodcode = request.getParameter("prodcode");
		}

		String pid = request.getParameter("pid");
		System.out.println("Permission Is d >>>>>>>" + pid);

		System.out.println("prodcode" + prodcode);
		List<CommonTblUnderwritingQA> qaList = new ArrayList<CommonTblUnderwritingQA>();
		CommonTblUnderwritingQAExample commonTblUnderwritingQAExample = new CommonTblUnderwritingQAExample();
		CommonTblUnderwritingQAExample.Criteria UnderwritingQA_Criteria = commonTblUnderwritingQAExample
				.createCriteria();
		commonTblUnderwritingQAExample.setOrderByClause("cqa.ID");
		if (!ServiceValidationUtils.isEmptyStringTrim(prodcode)) {
			UnderwritingQA_Criteria.andProductCodeEqualTo(prodcode);
			System.out.println("prodcode" + prodcode);
		}

		qaList = commonTblUnderwritingQAMapper.selectByExample(commonTblUnderwritingQAExample);
		if (qaList.isEmpty()) {
			model.addAttribute("norecords", "No records found");
			System.out.println("norecords-");
		}
		String prodname = "";
		if (prodcode.equals("HOHH")) {
			prodname = "Houseowner / Householder";
		} else if (prodcode.equals("TL")) {
			prodname = "Ezy-Life";
		} else if (prodcode.equals("IDS")) {
			prodname = "I-DoubleSecure";
		} else if (prodcode.equals("MI")) {
			prodname = "Motor";
		} else if (prodcode.equals("EZYTL")) {
			prodname = "Ezy-Secure";
		} else if (prodcode.equals("ISCTL")) {
			prodname = "I-Secure";
		} else if (prodcode.equals("WTC")) {
			prodname = "WTC";
		}
		model.addAttribute("prodname", prodname);
		model.addAttribute("qaList", qaList);
		return "products/admin-business-mng-rules-view";
	}

	@RequestMapping(value = "/UpdateRules", method = RequestMethod.POST)
	public String UpdateRules(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("RulesList funtion ");
		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		String id = null;
		if (request.getParameter("id") != null) {
			id = request.getParameter("id");
		} else {
			id = String.valueOf(request.getAttribute("id"));
		}
		CommonTblUnderwritingQA qarec = new CommonTblUnderwritingQA();
		qarec = commonTblUnderwritingQAMapper.selectByPrimaryKey(Short.valueOf(id));
		model.addAttribute("qarec", qarec);
		String prodname = "";
		if (qarec != null) {
			if (qarec.getProductCode().equals("HOHH")) {
				prodname = "Houseowner / Householder";
			} else if (qarec.getProductCode().equals("TL")) {
				prodname = "Ezy-Life";
			} else if (qarec.getProductCode().equals("IDS")) {
				prodname = "I-DoubleSecure";
			} else if (qarec.getProductCode().equals("MI")) {
				prodname = "Motor";
			} else if (qarec.getProductCode().equals("EZYTL")) {
				prodname = "Ezy-Secure";
			} else if (qarec.getProductCode().equals("ISCTL")) {
				prodname = "I-Secure";
			} else if (qarec.getProductCode().equals("WTC")) {
				prodname = "WTC";
			}
		}

		model.addAttribute("prodname", prodname);
		return "products/admin-business-mng-rules-edit";
	}

	// This request URL NOT USED because need approval permission . modified by
	// pramaiyan on 20032018
	@RequestMapping(value = "/UpdateRulesDone", method = RequestMethod.POST)
	public String UpdateRulesDone(
			@ModelAttribute(value = "CommonTblUnderwritingQA") CommonTblUnderwritingQA commonTblUnderwritingQA,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("RulesList funtion ");
		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		String id = null;
		if (request.getParameter("id") != null) {
			id = request.getParameter("id");
		} else {
			id = String.valueOf(request.getAttribute("id"));
		}

		commonTblUnderwritingQA.setId(Short.valueOf(id));
		commonTblUnderwritingQAMapper.updateByPrimaryKeySelective(commonTblUnderwritingQA);
		model.addAttribute("nvicUpdatedMessage", " Data Updated Successfully!");
		return RulesList(request, response, model);
	}

	// Display Original and Change Data Functionality

	@RequestMapping(value = "/approvalMIRulesManagement", method = RequestMethod.GET)
	public String approvalStatus(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Reequest " + request.getParameter("id"));
		ApprovalLogExample approvalexample = new ApprovalLogExample();
		com.spring.VO.ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();
		Integer testint = new Integer(request.getParameter("id"));
		createCriteria.andIdEqualTo(testint.shortValue());
		List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
		System.out.println("   list size in approval list  :::   " + listapprovallog.size());
		// System.out.println("Testing Original Content
		// "+listapprovallog.get(0).getOriginalContent());

		try {
			// original data
			if (listapprovallog.size() > 0) {
				convertBytes process = new convertBytes();
				Object originalData = process.convertFromBytes(listapprovallog.get(0).getOriginalContent());
				List<String> listOriginalData = new ArrayList<String>();
				// objecct convert into AgentProdMap
				if (originalData != null) {
					listOriginalData = (List<String>) originalData;
					System.out.println(" Original Data List Size ::::::         " + listOriginalData.size());

					CommonTblUnderwritingQA qarec = new CommonTblUnderwritingQA();

					qarec.setQuestionDescription(listOriginalData.get(0));
					qarec.setQuestionDescriptionBm(listOriginalData.get(1));
					qarec.setCorrectAnswerCode(listOriginalData.get(2));

					System.out.println("Original Desce" + qarec.getQuestionDescription());
					System.out.println("Original DesceBM" + qarec.getQuestionDescriptionBm());
					System.out.println("Original Code" + qarec.getCorrectAnswerCode());

					List<CommonTblUnderwritingQA> listOriginalMIRules = new ArrayList<CommonTblUnderwritingQA>();
					listOriginalMIRules.add(qarec);
					model.addAttribute("listOriginalMIRules", listOriginalMIRules);
					System.out.println("-----------------------------");
				} // inner if
			} // outer if

			// Change Rules Data for MI
			if (listapprovallog.size() > 0) {
				convertBytes process = new convertBytes();

				Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
				List<String> listChangeData = new ArrayList<String>();
				// objecct convert into AgentProdMap
				if (changeData != null) {
					listChangeData = (List<String>) changeData;

					CommonTblUnderwritingQA qarec = new CommonTblUnderwritingQA();

					qarec.setId(Short.parseShort(listChangeData.get(0)));
					qarec.setQuestionDescription(listChangeData.get(1));
					qarec.setQuestionDescriptionBm(listChangeData.get(2));
					qarec.setCorrectAnswerCode(listChangeData.get(3));

					System.out.println("ChageData List Size  ::::::         " + listChangeData.size());

					List<CommonTblUnderwritingQA> listChangeDataMIRules = new ArrayList<CommonTblUnderwritingQA>();

					System.out.println(" New ID >>> " + qarec.getId());
					System.out.println("New  Desce" + qarec.getQuestionDescription());
					System.out.println("New DesceBM" + qarec.getQuestionDescriptionBm());
					System.out.println("New  Code" + qarec.getCorrectAnswerCode());

					listChangeDataMIRules.add(qarec);
					model.addAttribute("listChangeDataMIRules", listChangeDataMIRules);
					System.out.println("-----------------------------");
				}
			}
		} // try end
		catch (Exception e) {
			System.out.println("Exception in Convert Object to Byte" + e);
		}

		return "agent-product-approval";
	}
	// approveMIRuleManagementChange

	// approveMIRuleManagementChange

	@RequestMapping(value = "/approveMIRuleManagementChange")
	public String approveMIRuleManagementChange(
			@ModelAttribute(value = "CommonTblUnderwritingQA") CommonTblUnderwritingQA commonTblUnderwritingQA,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("RulesList funtion ");
		HttpSession session = request.getSession();
		ApprovalLog alog = new ApprovalLog();

		String returnURL = "";
		try {
			String loginUser = (String) session.getAttribute("user");
			String logedUser = (String) session.getAttribute("logedUser");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			// *************Update the new change Rule management in originala table
			// ********************

			String aid = request.getParameter("approvalLogId");
			System.out.println("aid >>>>>" + aid);
			ApprovalLogExample approvalexample = new ApprovalLogExample();
			ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();

			if (null != aid && !aid.isEmpty()) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
				System.out.println("   list size in approval list  :::   " + listapprovallog.size());
				// System.out.println("Testing Original Content
				// "+listapprovallog.get(0).getOriginalContent());

				// Change Data
				if (listapprovallog.size() > 0) {
					String status = listapprovallog.get(0).getStatus();
					short alogId = listapprovallog.get(0).getId();
					System.out.println("Status  :  " + status);
					System.out.println("Approvallog Id  :  " + alogId);

					convertBytes process = new convertBytes();
					Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
					List<String> listChangeData = new ArrayList<String>();
					// objecct convert into AgentProdMap
					if (changeData != null) {
						listChangeData = (List<String>) changeData;
						System.out.println("  list agent prod map size ::::::         " + listChangeData.size());
						MIProductApprovalRate miProductApprovalRate = new MIProductApprovalRate();

						CommonTblUnderwritingQA qarec = new CommonTblUnderwritingQA();

						qarec.setQuestionDescription(listChangeData.get(1));
						qarec.setQuestionDescriptionBm(listChangeData.get(2));
						qarec.setCorrectAnswerCode(listChangeData.get(3));
						qarec.setId(Short.parseShort(listChangeData.get(0)));

						// *************Update the new change Rule management in originala table
						// ********************
						commonTblUnderwritingQAMapper.updateByPrimaryKeySelective(qarec);

						// Update status in DSP_AFM_TBL_Approval_Log table
						alog.setId(alogId);
						alog.setStatus("3");
						// alog.setChecker((short) 1); //get from the current login session
						alog.setChecker(Short.parseShort(logedUser));
						alog.setUpdateDate(new Date());
						int rs = approvalLogMapper.updateByPrimaryKeySelective(alog);
						if (rs == 1) {

							model.addAttribute("approvemessage", "Approved Data Successfully");
						}

						AgentController agentController = new AgentController(agentProfileMapper, productsMapper,
								agentProdMapMapper, commonQQMapper, adminParamMapper, agentDocumentMapper,
								agentLinkMapper, approvalLogMapper, approvalMapper);

						returnURL = agentController.getAgentProductMapApproval(request, response, model);

					}
				}
			}

		} // try end

		catch (Exception e) {
			System.out.println("Exception Update new Rule Management " + e);
			model.addAttribute("approvemessage", "Approve Data Failed");
		}

		return returnURL;

	}

	// Reject Action

	@RequestMapping(value = "/rejectMIRuleManagementChange", method = RequestMethod.GET)
	public String rejectChangeDataAction(@ModelAttribute AgentProfile agentProfile, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		String logedUser = (String) session.getAttribute("logedUser");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		int rs = 0;

		ApprovalLogExample approvalLogexample = new ApprovalLogExample();
		ApprovalLog alog = new ApprovalLog();
		ApprovalLogExample.Criteria createCriteria = approvalLogexample.createCriteria();
		String aid = request.getParameter("approvalLogId");
		if (null != aid && !aid.isEmpty()) {
			Integer testint = new Integer(aid);
			createCriteria.andIdEqualTo(testint.shortValue());
			List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalLogexample);
			System.out.println("   list size in approval list  :::   " + listapprovallog.size());

			short alogId = listapprovallog.get(0).getId();
			alog.setId(alogId);
			alog.setStatus("2");
			// alog.setChecker((short) 1); //get from the current login session
			alog.setChecker(Short.parseShort(logedUser));
			alog.setUpdateDate(new Date());
			rs = approvalLogMapper.updateByPrimaryKeySelective(alog);
		}

		if (rs == 1) {

			model.addAttribute("rejectmessage", "Rejected Data Successfully");
		}

		else {

			model.addAttribute("rejectmessage", "Reject Data Failed");
		}

		AgentController agentController = new AgentController(agentProfileMapper, productsMapper, agentProdMapMapper,
				commonQQMapper, adminParamMapper, agentDocumentMapper, agentLinkMapper, approvalLogMapper,
				approvalMapper);

		String returnURL = agentController.getAgentProductMapApproval(request, response, model);
		;
		return returnURL;
	}

	// ---------------------------------------------------------------END By
	// Pramaiyan UNDERWRITTING QUESTION
	// ------------------------------------------------------

	@RequestMapping(value = "/approvalMIRules", method = RequestMethod.POST)
	public String approvalMIRules(
			@ModelAttribute(value = "CommonTblUnderwritingQA") CommonTblUnderwritingQA commonTblUnderwritingQA,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("RulesList funtion ");
		HttpSession session = request.getSession();

		String loginUser = (String) session.getAttribute("user");
		String logedUser = (String) session.getAttribute("logedUser");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		String id = null;
		if (request.getParameter("id") != null) {
			id = request.getParameter("id");
		} else {
			id = String.valueOf(request.getAttribute("id"));
		}

		// getting Original value and move to list
		CommonTblUnderwritingQA qarec = new CommonTblUnderwritingQA();
		qarec = commonTblUnderwritingQAMapper.selectByPrimaryKey(Short.valueOf(id));

		String prodname = "";
		if (qarec != null) {
			if (qarec.getProductCode().equals("HOHH")) {
				prodname = "HOHH - RULES MANAGEMENT";
			} else if (qarec.getProductCode().equals("TL")) {
				prodname = "EZY-LIFE SECURE - RULES MANAGEMENT";
			} else if (qarec.getProductCode().equals("IDS")) {
				prodname = "I-DOUBLE SECURE - RULES MANAGEMENT";
			} else if (qarec.getProductCode().equals("MI")) {
				prodname = "MOTOR INSURANCE -RULES MANAGEMENT";
			} else if (qarec.getProductCode().equals("WTC")) {
				prodname = "WTC/T - RULES MANAGEMENT";
			}
		}
		/*
		 * System.out.println("Desc "+qarec.getQuestionDescription());
		 * System.out.println("Dec BM"+qarec.getQuestionDescriptionBm());
		 * System.out.println("Correct Answer"+qarec.getCorrectAnswerCode());
		 */
		List<String> originalRulesListMI = new ArrayList<String>();
		List<String> changeRulesListMI = new ArrayList<String>();
		// Add Original value to list
		if (qarec != null) {

			originalRulesListMI.add(qarec.getQuestionDescription());
			originalRulesListMI.add(qarec.getQuestionDescriptionBm());
			originalRulesListMI.add(qarec.getCorrectAnswerCode());
		}

		// System.out.println("questionDescription"+request.getParameter("questionDescription"));
		// System.out.println("questionDescriptionBM"+request.getParameter("questionDescriptionBm"));
		// System.out.println("Answer"+request.getParameter("correctAnswerCode"));

		// getting request paramaeter from admin-business-mng-rules-edit.jsp

		String qDes = request.getParameter("questionDescription");
		String qDesMY = request.getParameter("questionDescriptionBm");
		String answer = request.getParameter("correctAnswerCode");
		String uid = request.getParameter("id");
		System.out.println("Pramaiyan123 Id" + uid);
		// Add Change value to List

		changeRulesListMI.add(String.valueOf(uid));
		changeRulesListMI.add(qDes);
		changeRulesListMI.add(qDesMY);
		changeRulesListMI.add(answer);

		// getting Approval Id from DSP_ADM_TBL_APPROVAL based on DESCRIPTION
		ApprovalExample approvalExample = new ApprovalExample();
		com.spring.VO.ApprovalExample.Criteria createCriteria_approvalExample = approvalExample.createCriteria();
		createCriteria_approvalExample.andDescriptionEqualTo(prodname);
		List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

		try {

			// convert to BLOB
			convertBytes process = new convertBytes();
			byte[] originalData = process.convertToBytes(originalRulesListMI);
			byte[] changeData = process.convertToBytes(changeRulesListMI);

			BigDecimal approvalProdId = new BigDecimal(0);
			if (listapproval.size() > 0) {
				approvalProdId = listapproval.get(0).getId();
			}

			// setting all new change value to ApprovalLog POJA and Insert to ApprovalLog
			// table
			ApprovalLog approvalLog = new ApprovalLog();
			approvalLog.setOriginalContent(originalData);
			approvalLog.setNewContent(changeData);
			approvalLog.setApprovalId((short) 2); // from Approval table this value[2] based on menu in feature
			// approvalLog.setMaker((short)1); // session login value
			approvalLog.setMaker(Short.parseShort(logedUser));
			approvalLog.setStatus("1");
			approvalLog.setCreateDate(new Date());
			// approvalLog.setUpdateDate(new Date());
			approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
			int rs = approvalLogMapper.insert(approvalLog);

			System.out.println("ORI Data" + originalData);
			System.out.println("change Data" + changeData);

			if (rs == 1) {

				model.addAttribute("nvicUpdatedMessage", " Data Updated Successfully!");
			}

		} // try end
		catch (Exception e) {
			System.out.println("Exception in Convert Object to Byte for MI Rules Management" + e);

			model.addAttribute("nvicUpdatedMessage", " Data Updated Failed!");
		}

		return RulesList(request, response, model);
	}

}// Main class end
