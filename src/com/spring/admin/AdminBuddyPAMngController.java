package com.spring.admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.etiqa.DAO.BuddyPADAO;
import com.etiqa.DAO.BuddyPADAOImpl;
import com.etiqa.utils.convertBytes;
import com.spring.VO.ApprovalLog;
import com.spring.VO.ApprovalLogExample;
import com.spring.VO.BuddyPABenefit;
import com.spring.VO.BuddyPABenefitExample;
import com.spring.VO.BuddyPABenefitGrpList;
import com.spring.VO.BuddyPACombo;
import com.spring.VO.BuddyPAComboExample;
import com.spring.VO.BuddyPAPlan;
import com.spring.VO.BuddyPAProduct;
import com.spring.VO.BuddyPAProductExample;
import com.spring.mapper.ApprovalLogMapper;
import com.spring.mapper.ApprovalMapper;
import com.spring.mapper.BuddyPABenefitGrpMapper;
import com.spring.mapper.BuddyPABenefitMapper;
import com.spring.mapper.BuddyPAComboMapper;
import com.spring.mapper.BuddyPAPlanGrpMapper;
import com.spring.mapper.BuddyPAPlanMapper;
import com.spring.mapper.BuddyPAProductMapper;

@Controller
public class AdminBuddyPAMngController {

	ApprovalMapper approvalMapper;
	ApprovalLogMapper approvalLogMapper;
	BuddyPAProductMapper buddyPAProductMapper;
	BuddyPAPlanMapper buddyPAPlanMapper;
	BuddyPAPlanGrpMapper buddyPAPlanGrpMapper;
	BuddyPAComboMapper buddyPAComboMapper;
	BuddyPABenefitMapper buddyPABenefitMapper;
	BuddyPABenefitGrpMapper buddyPABenefitGrpMapper;

	@Autowired
	public AdminBuddyPAMngController(ApprovalMapper approvalMapper, ApprovalLogMapper approvalLogMapper,
			BuddyPAPlanMapper buddyPAPlanMapper, BuddyPAPlanGrpMapper buddyPAPlanGrpMapper,
			BuddyPAComboMapper buddyPAComboMapper, BuddyPABenefitMapper buddyPABenefitMapper,
			BuddyPABenefitGrpMapper buddyPABenefitGrpMapper, BuddyPAProductMapper buddyPAProductMapper) {
		this.approvalMapper = approvalMapper;
		this.approvalLogMapper = approvalLogMapper;
		this.buddyPAProductMapper = buddyPAProductMapper;
		this.buddyPAPlanMapper = buddyPAPlanMapper;
		this.buddyPAPlanGrpMapper = buddyPAPlanGrpMapper;
		this.buddyPAComboMapper = buddyPAComboMapper;
		this.buddyPABenefitMapper = buddyPABenefitMapper;
		this.buddyPABenefitGrpMapper = buddyPABenefitGrpMapper;

	}

	// ---------------------------------------------------------------START BuddyPA
	// ProductInfo
	// Change------------------------------------------------------------------------------
	@RequestMapping(value = "/approvalBuddyPAProduct", method = RequestMethod.GET)
	public String approvalBuddyPAProduct(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Request " + request.getParameter("id"));
		ApprovalLogExample approvalexample = new ApprovalLogExample();
		com.spring.VO.ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();
		Integer testint = new Integer(request.getParameter("id"));
		createCriteria.andIdEqualTo(testint.shortValue());
		List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
		System.out.println("   list size in approval list  :::   " + listapprovallog.size());

		try {
			// original data
			if (!listapprovallog.isEmpty()) {
				convertBytes process = new convertBytes();
				Object originalData = process.convertFromBytes(listapprovallog.get(0).getOriginalContent());
				List<BuddyPAProduct> listOriginalData;
				// object convert into Plan Model
				if (originalData != null) {
					listOriginalData = (List<BuddyPAProduct>) originalData;
					System.out.println(" Original Data List Size ::::::         " + listOriginalData.size());

					BuddyPAProduct buddyPAProduct = new BuddyPAProduct();

					buddyPAProduct.setCompanyId(listOriginalData.get(0).getCompanyId());
					buddyPAProduct.setProductId(listOriginalData.get(0).getProductId());
					buddyPAProduct.setStampDuty(listOriginalData.get(0).getStampDuty());
					buddyPAProduct.setAdultMaxAge(listOriginalData.get(0).getAdultMaxAge());
					buddyPAProduct.setAdultMinAge(listOriginalData.get(0).getAdultMinAge());
					buddyPAProduct.setChildMaxAge(listOriginalData.get(0).getChildMaxAge());
					buddyPAProduct.setChildMinAgeDays(listOriginalData.get(0).getChildMinAgeDays());
					buddyPAProduct.setStudentMaxAge(listOriginalData.get(0).getStudentMaxAge());
					buddyPAProduct.setStudentMinAge(listOriginalData.get(0).getStudentMinAge());
					buddyPAProduct.setMaxChildNo(listOriginalData.get(0).getMaxChildNo());
					buddyPAProduct.setStaffDiscountPercentage(listOriginalData.get(0).getStaffDiscountPercentage());
					buddyPAProduct.setGstEffDate(listOriginalData.get(0).getGstEffDate());
					buddyPAProduct.setSstEffDate(listOriginalData.get(0).getSstEffDate());
					buddyPAProduct.setGst(listOriginalData.get(0).getGst());
					buddyPAProduct.setSst(listOriginalData.get(0).getSst());

					List<BuddyPAProduct> listOriginalProduct = new ArrayList<>();
					listOriginalProduct.add(buddyPAProduct);
					model.addAttribute("listOriginalBuddyProduct", listOriginalProduct);
					System.out.println("-----------------------------");
				} // inner if
			} // outer if

			// Change Plan Info
			if (!listapprovallog.isEmpty()) {
				convertBytes process = new convertBytes();

				Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
				List<BuddyPAProduct> listChangeData;
				// object convert into AgentProdMap
				if (changeData != null) {
					listChangeData = (List<BuddyPAProduct>) changeData;
					System.out.println(" Original Data List Size ::::::         " + listChangeData.size());

					BuddyPAProduct buddyPAProduct = new BuddyPAProduct();

					buddyPAProduct.setCompanyId(listChangeData.get(0).getCompanyId());
					buddyPAProduct.setProductId(listChangeData.get(0).getProductId());
					buddyPAProduct.setStampDuty(listChangeData.get(0).getStampDuty());
					buddyPAProduct.setAdultMaxAge(listChangeData.get(0).getAdultMaxAge());
					buddyPAProduct.setAdultMinAge(listChangeData.get(0).getAdultMinAge());
					buddyPAProduct.setChildMaxAge(listChangeData.get(0).getChildMaxAge());
					buddyPAProduct.setChildMinAgeDays(listChangeData.get(0).getChildMinAgeDays());
					buddyPAProduct.setStudentMaxAge(listChangeData.get(0).getStudentMaxAge());
					buddyPAProduct.setStudentMinAge(listChangeData.get(0).getStudentMinAge());
					buddyPAProduct.setMaxChildNo(listChangeData.get(0).getMaxChildNo());
					buddyPAProduct.setStaffDiscountPercentage(listChangeData.get(0).getStaffDiscountPercentage());
					buddyPAProduct.setGstEffDate(listChangeData.get(0).getGstEffDate());
					buddyPAProduct.setSstEffDate(listChangeData.get(0).getSstEffDate());
					buddyPAProduct.setGst(listChangeData.get(0).getGst());
					buddyPAProduct.setSst(listChangeData.get(0).getSst());

					List<BuddyPAProduct> listChangeProduct = new ArrayList<>();
					listChangeProduct.add(buddyPAProduct);
					model.addAttribute("listChangeBuddyProduct", listChangeProduct);
					System.out.println("-----------------------------");
				}
			}
		} // try end
		catch (Exception e) {
			System.out.println("Exception in Convert Object to Byte :" + e);
		}

		return "agent-product-approval";
	}

	@RequestMapping(value = "/approveBuddyPAProductChange")
	public String approveBuddyPAProductChange(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("BuddyPA Plan Info Change Approve");
		HttpSession session = request.getSession();
		ApprovalLog alog = new ApprovalLog();
		try {
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			System.out.println("Admin Buddy PA Controller loginUser" + loginUser);

			// *************Update the new change Plan info in Plan table
			// ********************

			String aid = request.getParameter("approvalLogId");
			System.out.println("aid >>>>>" + aid);
			ApprovalLogExample approvalexample = new ApprovalLogExample();
			ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();

			if (null != aid && !aid.isEmpty()) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
				System.out.println("   list size in approval list  :::   " + listapprovallog.size());

				// Change Data
				if (!listapprovallog.isEmpty()) {
					String status = listapprovallog.get(0).getStatus();
					short alogId = listapprovallog.get(0).getId();
					System.out.println("Status  :  " + status);
					System.out.println("Approvallog Id  :  " + alogId);

					convertBytes process = new convertBytes();
					Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
					List<BuddyPAProduct> listChangeData;
					// object convert into BuddyPAPlan
					if (changeData != null) {
						listChangeData = (List<BuddyPAProduct>) changeData;
						System.out.println("  list agent prod map size ::::::         " + listChangeData.size());

						BuddyPAProductExample buddyPAProductExample = new BuddyPAProductExample();
						BuddyPAProduct buddyPAProduct = new BuddyPAProduct();
						BuddyPAProductExample.Criteria productParamCriteria = buddyPAProductExample.createCriteria();
						productParamCriteria.andCompanyIdEqualTo(listChangeData.get(0).getCompanyId());
						productParamCriteria.andProductIdEqualTo(listChangeData.get(0).getProductId());

						buddyPAProduct.setCompanyId(listChangeData.get(0).getCompanyId());
						buddyPAProduct.setProductId(listChangeData.get(0).getProductId());
						buddyPAProduct.setStampDuty(listChangeData.get(0).getStampDuty());
						buddyPAProduct.setAdultMaxAge(listChangeData.get(0).getAdultMaxAge());
						buddyPAProduct.setAdultMinAge(listChangeData.get(0).getAdultMinAge());
						buddyPAProduct.setChildMaxAge(listChangeData.get(0).getChildMaxAge());
						buddyPAProduct.setChildMinAgeDays(listChangeData.get(0).getChildMinAgeDays());
						buddyPAProduct.setStudentMaxAge(listChangeData.get(0).getStudentMaxAge());
						buddyPAProduct.setStudentMinAge(listChangeData.get(0).getStudentMinAge());
						buddyPAProduct.setMaxChildNo(listChangeData.get(0).getMaxChildNo());
						buddyPAProduct.setStaffDiscountPercentage(listChangeData.get(0).getStaffDiscountPercentage());
						buddyPAProduct.setGstEffDate(listChangeData.get(0).getGstEffDate());
						buddyPAProduct.setSstEffDate(listChangeData.get(0).getSstEffDate());
						buddyPAProduct.setGst(listChangeData.get(0).getGst());
						buddyPAProduct.setSst(listChangeData.get(0).getSst());
						buddyPAProduct.setModifiedBy(listChangeData.get(0).getModifiedBy());
						buddyPAProduct.setModifiedDt(listChangeData.get(0).getModifiedDt());

						// *************Update the new change Plan Info in original table
						// ********************
						buddyPAProductMapper.updateByExampleSelective(buddyPAProduct, buddyPAProductExample);

						// Update status in DSP_ADM_TBL_Approval_Log table
						alog.setId(alogId);
						alog.setStatus("3");
						alog.setChecker((short) Integer.parseInt(loginUser)); // get from the current login session
						alog.setUpdateDate(new Date());
						approvalLogMapper.updateByPrimaryKeySelective(alog);
					}
				}
			}

		} // try end

		catch (Exception e) {
			System.out.println("Exception Update new Product Information : " + e);
		}

		return "redirect:/getApprovalList";

	}
	// ---------------------------------------------------------------END BuddyPA
	// Product Information
	// Change------------------------------------------------------------------------------

	// ---------------------------------------------------------------START BuddyPA
	// Plan Information
	// Change------------------------------------------------------------------------------
	@RequestMapping(value = "/approvalBuddyPAPlan", method = RequestMethod.GET)
	public String approvalBuddyPAPlan(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Request " + request.getParameter("id"));
		ApprovalLogExample approvalexample = new ApprovalLogExample();
		com.spring.VO.ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();
		Integer testint = new Integer(request.getParameter("id"));
		createCriteria.andIdEqualTo(testint.shortValue());
		List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
		System.out.println("   list size in approval list  :::   " + listapprovallog.size());

		try {
			// original data
			if (!listapprovallog.isEmpty()) {
				convertBytes process = new convertBytes();
				List<BuddyPAPlan> listOriginalData;
				Object originalData = null;

				if (listapprovallog.get(0).getOriginalContent() != null) {
					originalData = process.convertFromBytes(listapprovallog.get(0).getOriginalContent());
					// object convert into Plan Model
					if (originalData != null) {
						listOriginalData = (List<BuddyPAPlan>) originalData;
						System.out.println(" Original Data List Size ::::::         " + listOriginalData.size());

						BuddyPAPlan buddyPAPlan = new BuddyPAPlan();

						buddyPAPlan.setPlanId(listOriginalData.get(0).getPlanId());
						buddyPAPlan.setNameEn(listOriginalData.get(0).getNameEn());
						buddyPAPlan.setNameBm(listOriginalData.get(0).getNameBm());
						buddyPAPlan.setEnable(listOriginalData.get(0).getEnable());

						List<BuddyPAPlan> listOriginalPlan = new ArrayList<>();
						listOriginalPlan.add(buddyPAPlan);
						model.addAttribute("listOriginalBuddyPlan", listOriginalPlan);
						System.out.println("-----------------------------");
					} // inner if
				}
			} // outer if

			// Change Plan Info
			if (!listapprovallog.isEmpty()) {
				convertBytes process = new convertBytes();

				Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
				List<BuddyPAPlan> listChangeData;
				// object convert into AgentProdMap
				if (changeData != null) {
					listChangeData = (List<BuddyPAPlan>) changeData;
					System.out.println("ChangeData List Size  ::::::         " + listChangeData.size());
					BuddyPAPlan buddyPAPlan = new BuddyPAPlan();

					buddyPAPlan.setPlanId(listChangeData.get(0).getPlanId());
					buddyPAPlan.setNameEn(listChangeData.get(0).getNameEn());
					buddyPAPlan.setNameBm(listChangeData.get(0).getNameBm());
					buddyPAPlan.setEnable(listChangeData.get(0).getEnable());

					List<BuddyPAPlan> listChangePlan = new ArrayList<>();
					listChangePlan.add(buddyPAPlan);
					model.addAttribute("listChangeBuddyPlan", listChangePlan);
					System.out.println("-----------------------------");
				}
			}
		} // try end
		catch (Exception e) {
			System.out.println("Exception in Convert Object to Byte :" + e);
		}

		return "agent-product-approval";
	}

	@RequestMapping(value = "/approveBuddyPAPlanChange")
	public String approveBuddyPAPlanChange(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("BuddyPA Plan Info Change Approve");
		HttpSession session = request.getSession();
		ApprovalLog alog = new ApprovalLog();
		try {
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			System.out.println("Admin Buddy PA Controller loginUser" + loginUser);

			// *************Update the new change Plan info in Plan table
			// ********************

			String aid = request.getParameter("approvalLogId");
			System.out.println("aid >>>>>" + aid);
			ApprovalLogExample approvalexample = new ApprovalLogExample();
			ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();

			if (null != aid && !aid.isEmpty()) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
				System.out.println("   list size in approval list  :::   " + listapprovallog.size());

				// Change Data
				if (!listapprovallog.isEmpty()) {
					String status = listapprovallog.get(0).getStatus();
					short alogId = listapprovallog.get(0).getId();
					System.out.println("Status  :  " + status);
					System.out.println("Approvallog Id  :  " + alogId);

					convertBytes process = new convertBytes();
					Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
					Object oriData = null;
					if (listapprovallog.get(0).getOriginalContent() != null) {
						oriData = process.convertFromBytes(listapprovallog.get(0).getOriginalContent());
					}
					List<BuddyPAPlan> listChangeData;
					// object convert into BuddyPAPlan
					if (changeData != null) {
						listChangeData = (List<BuddyPAPlan>) changeData;
						System.out.println("  list agent prod map size ::::::         " + listChangeData.size());

						BuddyPAPlan buddyPAPlan = new BuddyPAPlan();

						buddyPAPlan.setPlanId(listChangeData.get(0).getPlanId());
						buddyPAPlan.setNameEn(listChangeData.get(0).getNameEn());
						buddyPAPlan.setNameBm(listChangeData.get(0).getNameBm());
						buddyPAPlan.setEnable(listChangeData.get(0).getEnable());
						buddyPAPlan.setVersion(listChangeData.get(0).getVersion());
						if (oriData != null) {
							buddyPAPlan.setModifiedBy(listChangeData.get(0).getModifiedBy());
							buddyPAPlan.setModifiedDt(listChangeData.get(0).getModifiedDt());

							// *************Update the new change Plan Info in original table
							// ********************
							buddyPAPlanMapper.updateByPrimaryKeySelective(buddyPAPlan);
							System.out.println(":::: BUDDY PA UPDATE PLAN");
						} else {
							buddyPAPlan.setCreatedBy(listChangeData.get(0).getCreatedBy());
							buddyPAPlan.setCreatedDt(listChangeData.get(0).getCreatedDt());

							buddyPAPlanMapper.insertSelective(buddyPAPlan);
							System.out.println(":::: BUDDY PA INSERT PLAN");
						}

						// Update status in DSP_ADM_TBL_Approval_Log table
						alog.setId(alogId);
						alog.setStatus("3");
						alog.setChecker((short) Integer.parseInt(loginUser)); // get from the current login session
						alog.setUpdateDate(new Date());
						approvalLogMapper.updateByPrimaryKeySelective(alog);
					}
				}
			}

		} // try end

		catch (Exception e) {
			System.out.println("Exception Update new Plan Information : " + e);
		}

		return "redirect:/getApprovalList";

	}

	// ---------------------------------------------------------------END BuddyPA
	// Plan Information
	// Change------------------------------------------------------------------------------

	// ---------------------------------------------------------------START BuddyPA
	// Combo
	// Change------------------------------------------------------------------------------
	@RequestMapping(value = "/approvalBuddyPACombo", method = RequestMethod.GET)
	public String approvalBuddyPACombo(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Request " + request.getParameter("id"));
		ApprovalLogExample approvalexample = new ApprovalLogExample();
		com.spring.VO.ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();
		Integer testint = new Integer(request.getParameter("id"));
		createCriteria.andIdEqualTo(testint.shortValue());
		List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
		System.out.println("   list size in approval list  :::   " + listapprovallog.size());

		try {
			// original data
			if (!listapprovallog.isEmpty()) {
				convertBytes process = new convertBytes();
				Object originalData = null;
				if (listapprovallog.get(0).getOriginalContent() != null) {
					originalData = process.convertFromBytes(listapprovallog.get(0).getOriginalContent());
					List<BuddyPACombo> listOriginalData;
					// object convert into Combo Model
					if (originalData != null) {
						listOriginalData = (List<BuddyPACombo>) originalData;
						System.out.println(" Original Data List Size ::::::         " + listOriginalData.size());

						BuddyPACombo buddyPACombo = new BuddyPACombo();

						buddyPACombo.setCompanyId(listOriginalData.get(0).getCompanyId());
						buddyPACombo.setName(listOriginalData.get(0).getName());
						buddyPACombo.setReservedId(listOriginalData.get(0).getReservedId());
						buddyPACombo.setUrlBm(listOriginalData.get(0).getUrlBm());
						buddyPACombo.setUrlEn(listOriginalData.get(0).getUrlEn());
						buddyPACombo.setEnable(listOriginalData.get(0).getEnable());

						List<BuddyPACombo> listOriginalCombo = new ArrayList<>();
						listOriginalCombo.add(buddyPACombo);
						model.addAttribute("listOriginalBuddyCombo", listOriginalCombo);
						System.out.println("-----------------------------");
					} // inner if
				}
			} // outer if

			// Change Combo Info
			if (!listapprovallog.isEmpty()) {
				convertBytes process = new convertBytes();

				Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
				List<BuddyPACombo> listChangeData;
				// object convert into AgentProdMap
				if (changeData != null) {
					listChangeData = (List<BuddyPACombo>) changeData;
					System.out.println("ChangeData List Size  ::::::         " + listChangeData.size());
					BuddyPACombo buddyPACombo = new BuddyPACombo();

					buddyPACombo.setCompanyId(listChangeData.get(0).getCompanyId());
					buddyPACombo.setName(listChangeData.get(0).getName());
					buddyPACombo.setReservedId(listChangeData.get(0).getReservedId());
					buddyPACombo.setUrlBm(listChangeData.get(0).getUrlBm());
					buddyPACombo.setUrlEn(listChangeData.get(0).getUrlEn());
					buddyPACombo.setEnable(listChangeData.get(0).getEnable());

					List<BuddyPACombo> listChangeCombo = new ArrayList<>();
					listChangeCombo.add(buddyPACombo);
					model.addAttribute("listChangeBuddyCombo", listChangeCombo);
					System.out.println("-----------------------------");
				}
			}
		} // try end
		catch (Exception e) {
			System.out.println("Exception in Convert Object to Byte :" + e);
		}

		return "agent-product-approval";
	}

	@RequestMapping(value = "/approveBuddyPAComboChange")
	public String approveBuddyPAComboChange(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("BuddyPA Combo Info Change Approve");
		HttpSession session = request.getSession();
		ApprovalLog alog = new ApprovalLog();
		try {
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			System.out.println("Admin Buddy PA Controller loginUser" + loginUser);

			// *************Update the new change Plan info in Plan table
			// ********************

			String aid = request.getParameter("approvalLogId");
			System.out.println("aid >>>>>" + aid);
			ApprovalLogExample approvalexample = new ApprovalLogExample();
			ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();

			if (null != aid && !aid.isEmpty()) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
				System.out.println("   list size in approval list  :::   " + listapprovallog.size());

				// Change Data
				if (!listapprovallog.isEmpty()) {
					String status = listapprovallog.get(0).getStatus();
					short alogId = listapprovallog.get(0).getId();
					System.out.println("Status  :  " + status);
					System.out.println("Approvallog Id  :  " + alogId);

					convertBytes process = new convertBytes();
					Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
					Object oriData = null;
					if (listapprovallog.get(0).getOriginalContent() != null) {
						oriData = process.convertFromBytes(listapprovallog.get(0).getOriginalContent());
					}
					List<BuddyPACombo> listChangeData;
					// object convert into BuddyPACombo
					if (changeData != null) {
						listChangeData = (List<BuddyPACombo>) changeData;
						System.out.println("  list agent prod map size ::::::         " + listChangeData.size());

						BuddyPACombo buddyPACombo = new BuddyPACombo();
						BuddyPAComboExample buddyPAComboExample = new BuddyPAComboExample();
						BuddyPAComboExample.Criteria buddyPAComboCriteria = buddyPAComboExample.createCriteria();
						buddyPAComboCriteria.andCompanyIdEqualTo(listChangeData.get(0).getCompanyId());
						buddyPAComboCriteria.andReservedIdEqualTo(listChangeData.get(0).getReservedId());

						buddyPACombo.setCompanyId(listChangeData.get(0).getCompanyId());
						buddyPACombo.setName(listChangeData.get(0).getName());
						buddyPACombo.setReservedId(listChangeData.get(0).getReservedId());
						buddyPACombo.setUrlBm(listChangeData.get(0).getUrlBm());
						buddyPACombo.setUrlEn(listChangeData.get(0).getUrlEn());
						buddyPACombo.setEnable(listChangeData.get(0).getEnable());
						buddyPACombo.setVersion(listChangeData.get(0).getVersion());

						if (oriData != null) {
							buddyPACombo.setModifiedBy(listChangeData.get(0).getModifiedBy());
							buddyPACombo.setModifiedDt(listChangeData.get(0).getModifiedDt());
							// *************Update the new change Combo Info in original table
							// ********************
							buddyPAComboMapper.updateByExampleSelective(buddyPACombo, buddyPAComboExample);
							System.out.println(":::: BUDDY PA UPDATE COMBO");
						} else {
							buddyPACombo.setCreatedBy(listChangeData.get(0).getCreatedBy());
							buddyPACombo.setCreatedDt(listChangeData.get(0).getCreatedDt());
							buddyPAComboMapper.insertSelective(buddyPACombo);
							System.out.println(":::: BUDDY PA INSERT COMBO");
						}
						// Update status in DSP_ADM_TBL_Approval_Log table
						alog.setId(alogId);
						alog.setStatus("3");
						alog.setChecker((short) Integer.parseInt(loginUser)); // get from the current login session
						alog.setUpdateDate(new Date());
						approvalLogMapper.updateByPrimaryKeySelective(alog);
					}
				}
			}

		} // try end

		catch (Exception e) {
			System.out.println("Exception Update new Combo Information : " + e);
		}

		return "redirect:/getApprovalList";

	}

	// ---------------------------------------------------------------END BuddyPA
	// Combo
	// Change------------------------------------------------------------------------------

	// ---------------------------------------------------------------START BuddyPA
	// Benefit List
	// Change------------------------------------------------------------------------------
	@RequestMapping(value = "/approvalBuddyPABenefit", method = RequestMethod.GET)
	public String approvalBuddyPABenefit(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Request " + request.getParameter("id"));
		ApprovalLogExample approvalexample = new ApprovalLogExample();
		com.spring.VO.ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();
		Integer testint = new Integer(request.getParameter("id"));
		createCriteria.andIdEqualTo(testint.shortValue());
		List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
		System.out.println("   list size in approval list  :::   " + listapprovallog.size());

		try {
			// original data
			if (!listapprovallog.isEmpty()) {
				convertBytes process = new convertBytes();
				Object originalData = null;
				if (listapprovallog.get(0).getOriginalContent() != null) {
					originalData = process.convertFromBytes(listapprovallog.get(0).getOriginalContent());

					List<BuddyPABenefit> listOriginalData;
					// object convert into Benefit Model
					if (originalData != null) {
						listOriginalData = (List<BuddyPABenefit>) originalData;
						System.out.println(" Original Data List Size ::::::         " + listOriginalData.size());

						BuddyPABenefit buddyPABenefit = new BuddyPABenefit();

						buddyPABenefit.setBenefitId(listOriginalData.get(0).getBenefitId());
						buddyPABenefit.setCompanyId(listOriginalData.get(0).getCompanyId());
						buddyPABenefit.setNameEn(listOriginalData.get(0).getNameEn());
						buddyPABenefit.setNameBm(listOriginalData.get(0).getNameBm());
						buddyPABenefit.setAdultRate(listOriginalData.get(0).getAdultRate());
						buddyPABenefit.setInsuredMElig(listOriginalData.get(0).getInsuredMElig());
						buddyPABenefit.setInsuredFElig(listOriginalData.get(0).getInsuredFElig());
						buddyPABenefit.setSpouseMElig(listOriginalData.get(0).getSpouseMElig());
						buddyPABenefit.setSpouseFElig(listOriginalData.get(0).getSpouseFElig());
						buddyPABenefit.setChildRate(listOriginalData.get(0).getChildRate());
						buddyPABenefit.setChildElig(listOriginalData.get(0).getChildElig());
						buddyPABenefit.setEnable(listOriginalData.get(0).getEnable());

						List<BuddyPABenefit> listOriginalBenefit = new ArrayList<>();
						listOriginalBenefit.add(buddyPABenefit);
						model.addAttribute("listOriginalBuddyBenefit", listOriginalBenefit);
						System.out.println("-----------------------------");
					} // inner if
				}
			} // outer if

			// Change Plan Info
			if (!listapprovallog.isEmpty()) {
				convertBytes process = new convertBytes();

				Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
				List<BuddyPABenefit> listChangeData;
				// object convert into AgentProdMap
				if (changeData != null) {
					listChangeData = (List<BuddyPABenefit>) changeData;
					System.out.println("ChangeData List Size  ::::::         " + listChangeData.size());
					BuddyPABenefit buddyPABenefit = new BuddyPABenefit();

					buddyPABenefit.setBenefitId(listChangeData.get(0).getBenefitId());
					buddyPABenefit.setCompanyId(listChangeData.get(0).getCompanyId());
					buddyPABenefit.setNameEn(listChangeData.get(0).getNameEn());
					buddyPABenefit.setNameBm(listChangeData.get(0).getNameBm());
					buddyPABenefit.setAdultRate(listChangeData.get(0).getAdultRate());
					buddyPABenefit.setInsuredMElig(listChangeData.get(0).getInsuredMElig());
					buddyPABenefit.setInsuredFElig(listChangeData.get(0).getInsuredFElig());
					buddyPABenefit.setSpouseMElig(listChangeData.get(0).getSpouseMElig());
					buddyPABenefit.setSpouseFElig(listChangeData.get(0).getSpouseFElig());
					buddyPABenefit.setChildRate(listChangeData.get(0).getChildRate());
					buddyPABenefit.setChildElig(listChangeData.get(0).getChildElig());
					buddyPABenefit.setEnable(listChangeData.get(0).getEnable());

					List<BuddyPABenefit> listChangeBenefit = new ArrayList<>();
					listChangeBenefit.add(buddyPABenefit);
					model.addAttribute("listChangeBuddyBenefit", listChangeBenefit);
					System.out.println("-----------------------------");
				}
			}
		} // try end
		catch (Exception e) {
			System.out.println("Exception in Convert Object to Byte :" + e);
		}

		return "agent-product-approval";
	}

	@RequestMapping(value = "/approveBuddyPABenefitChange")
	public String approveBuddyPABenefitChange(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("BuddyPA Plan Info Change Approve");
		HttpSession session = request.getSession();
		ApprovalLog alog = new ApprovalLog();
		try {
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			System.out.println("Admin Buddy PA Controller loginUser" + loginUser);

			// *************Update the new change Plan info in Plan table
			// ********************

			String aid = request.getParameter("approvalLogId");
			System.out.println("aid >>>>>" + aid);
			ApprovalLogExample approvalexample = new ApprovalLogExample();
			ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();

			if (null != aid && !aid.isEmpty()) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
				System.out.println("   list size in approval list  :::   " + listapprovallog.size());

				// Change Data
				if (!listapprovallog.isEmpty()) {
					String status = listapprovallog.get(0).getStatus();
					short alogId = listapprovallog.get(0).getId();
					System.out.println("Status  :  " + status);
					System.out.println("Approvallog Id  :  " + alogId);

					convertBytes process = new convertBytes();
					Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
					List<BuddyPABenefit> listChangeData;
					Object oriData = null;
					if (listapprovallog.get(0).getOriginalContent() != null) {
						oriData = process.convertFromBytes(listapprovallog.get(0).getOriginalContent());
					}
					// object convert into BuddyPAPlan
					if (changeData != null) {
						listChangeData = (List<BuddyPABenefit>) changeData;
						System.out.println("  list agent prod map size ::::::         " + listChangeData.size());

						BuddyPABenefit buddyPABenefit = new BuddyPABenefit();

						buddyPABenefit.setCompanyId(listChangeData.get(0).getCompanyId());
						buddyPABenefit.setNameEn(listChangeData.get(0).getNameEn());
						buddyPABenefit.setNameBm(listChangeData.get(0).getNameBm());
						buddyPABenefit.setAdultRate(listChangeData.get(0).getAdultRate());
						buddyPABenefit.setInsuredMElig(listChangeData.get(0).getInsuredMElig());
						buddyPABenefit.setInsuredFElig(listChangeData.get(0).getInsuredFElig());
						buddyPABenefit.setSpouseMElig(listChangeData.get(0).getSpouseMElig());
						buddyPABenefit.setSpouseFElig(listChangeData.get(0).getSpouseFElig());
						buddyPABenefit.setChildRate(listChangeData.get(0).getChildRate());
						buddyPABenefit.setChildElig(listChangeData.get(0).getChildElig());
						buddyPABenefit.setEnable(listChangeData.get(0).getEnable());

						if (oriData != null) {
							System.out.println(":::: BUDDY PA UPDATE BENEFIT START ");
							BuddyPABenefitExample buddyPABenefitExample = new BuddyPABenefitExample();
							BuddyPABenefitExample.Criteria buddyPABenefitCriteria = buddyPABenefitExample
									.createCriteria();
							buddyPABenefitCriteria.andBenefitIdEqualTo(listChangeData.get(0).getBenefitId());
							buddyPABenefitCriteria.andCompanyIdEqualTo(listChangeData.get(0).getCompanyId());

							buddyPABenefit.setVersion(listChangeData.get(0).getVersion());
							buddyPABenefit.setBenefitId(listChangeData.get(0).getBenefitId());
							buddyPABenefit.setModifiedBy(listChangeData.get(0).getModifiedBy());
							buddyPABenefit.setModifiedDt(listChangeData.get(0).getModifiedDt());
							// *************Update the new change Benefit Info in original table
							// ********************
							buddyPABenefitMapper.updateByExampleSelective(buddyPABenefit, buddyPABenefitExample);
							System.out.println(":::: BUDDY PA UPDATE BENEFIT END");
						} else {
							System.out.println(":::: BUDDY PA INSERT BENEFIT ");
							buddyPABenefit.setCreatedBy(listChangeData.get(0).getCreatedBy());
							buddyPABenefit.setCreatedDt(listChangeData.get(0).getCreatedDt());
							buddyPABenefitMapper.insertSelective(buddyPABenefit);
							System.out.println(":::: BUDDY PA INSERT BENEFIT END");
						}

						// Update status in DSP_ADM_TBL_Approval_Log table
						alog.setId(alogId);
						alog.setStatus("3");
						alog.setChecker((short) Integer.parseInt(loginUser)); // get from the current login session
						alog.setUpdateDate(new Date());
						approvalLogMapper.updateByPrimaryKeySelective(alog);
					}
				}
			}

		} // try end

		catch (Exception e) {
			System.out.println("Exception Update new Benefit : " + e);
		}

		return "redirect:/getApprovalList";

	}

	// ---------------------------------------------------------------END BuddyPA
	// Benefit List
	// Change------------------------------------------------------------------------------

	// ---------------------------------------------------------------START BuddyPA
	// Benefit Group
	// Change------------------------------------------------------------------------------
	@RequestMapping(value = "/approvalBuddyPABenefitGrp", method = RequestMethod.GET)
	public String approvalBuddyPABenefitGrp(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Request " + request.getParameter("id"));
		ApprovalLogExample approvalexample = new ApprovalLogExample();
		com.spring.VO.ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();
		Integer testint = new Integer(request.getParameter("id"));
		createCriteria.andIdEqualTo(testint.shortValue());
		List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
		System.out.println("   list size in approval list  :::   " + listapprovallog.size());

		try {
			// original data
			if (!listapprovallog.isEmpty()) {
				convertBytes process = new convertBytes();
				Object originalData = null;
				if (listapprovallog.get(0).getOriginalContent() != null) {
					originalData = process.convertFromBytes(listapprovallog.get(0).getOriginalContent());
					List<BuddyPABenefitGrpList> listOriginalData;
					// object convert into Plan Model
					if (originalData != null) {
						listOriginalData = (List<BuddyPABenefitGrpList>) originalData;
						System.out.println(" Original Data List Size ::::::         " + listOriginalData.size());

						List<BuddyPABenefitGrpList> listOriginalBenefitGrp = new ArrayList<>();
						for (int i = 0; i < listOriginalData.size(); i++) {
							BuddyPABenefitGrpList buddyPABenefitGrp = new BuddyPABenefitGrpList();

							buddyPABenefitGrp.setBenefitId(listOriginalData.get(i).getBenefitId());
							buddyPABenefitGrp.setBenefitName(listOriginalData.get(i).getBenefitName());
							buddyPABenefitGrp.setCompanyId(listOriginalData.get(i).getCompanyId());
							buddyPABenefitGrp.setPlanId(listOriginalData.get(i).getPlanId());
							buddyPABenefitGrp.setPlanName(listOriginalData.get(i).getPlanName());
							buddyPABenefitGrp.setComboId(listOriginalData.get(i).getComboId());
							buddyPABenefitGrp.setComboName(listOriginalData.get(i).getComboName());
							buddyPABenefitGrp.setAdultSumInsured(listOriginalData.get(i).getAdultSumInsured());
							buddyPABenefitGrp.setAdultLoading(listOriginalData.get(i).getAdultLoading());
							buddyPABenefitGrp.setChildSumInsured(listOriginalData.get(i).getChildSumInsured());
							buddyPABenefitGrp.setChildLoading(listOriginalData.get(i).getChildLoading());
							buddyPABenefitGrp.setEnable(listOriginalData.get(i).getEnable());

							listOriginalBenefitGrp.add(buddyPABenefitGrp);
						}
						model.addAttribute("listOriginalBuddyBenefitGrp", listOriginalBenefitGrp);
						System.out.println("-----------------------------");
					} // inner if
				}
			} // outer if

			// Change Plan Info
			if (!listapprovallog.isEmpty()) {
				convertBytes process = new convertBytes();

				Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
				List<BuddyPABenefitGrpList> listChangeData;
				// object convert into AgentProdMap
				if (changeData != null) {
					listChangeData = (List<BuddyPABenefitGrpList>) changeData;
					System.out.println("ChangeData List Size  ::::::         " + listChangeData.size());
					List<BuddyPABenefitGrpList> listChangeBenefitGrp = new ArrayList<>();

					for (int i = 0; i < listChangeData.size(); i++) {
						BuddyPABenefitGrpList buddyPABenefitGrp = new BuddyPABenefitGrpList();

						buddyPABenefitGrp.setBenefitId(listChangeData.get(i).getBenefitId());
						buddyPABenefitGrp.setBenefitName(listChangeData.get(i).getBenefitName());
						buddyPABenefitGrp.setCompanyId(listChangeData.get(i).getCompanyId());
						buddyPABenefitGrp.setPlanId(listChangeData.get(i).getPlanId());
						buddyPABenefitGrp.setPlanName(listChangeData.get(i).getPlanName());
						buddyPABenefitGrp.setComboId(listChangeData.get(i).getComboId());
						buddyPABenefitGrp.setComboName(listChangeData.get(i).getComboName());
						buddyPABenefitGrp.setAdultSumInsured(listChangeData.get(i).getAdultSumInsured());
						buddyPABenefitGrp.setAdultLoading(listChangeData.get(i).getAdultLoading());
						buddyPABenefitGrp.setChildSumInsured(listChangeData.get(i).getChildSumInsured());
						buddyPABenefitGrp.setChildLoading(listChangeData.get(i).getChildLoading());
						buddyPABenefitGrp.setEnable(listChangeData.get(i).getEnable());

						listChangeBenefitGrp.add(buddyPABenefitGrp);
					}
					model.addAttribute("listChangeBuddyBenefitGrp", listChangeBenefitGrp);
					System.out.println("-----------------------------");
				}
			}
		} // try end
		catch (Exception e) {
			System.out.println("Exception in Convert Object to Byte :" + e);
		}

		return "agent-product-approval";
	}

	@RequestMapping(value = "/approveBuddyPABenefitGrpChange")
	public String approveBuddyPABenefitGrpChange(HttpServletRequest request, HttpServletResponse response,
			Model model) {
		System.out.println("BuddyPA Plan Info Change Approve");
		HttpSession session = request.getSession();
		ApprovalLog alog = new ApprovalLog();
		try {
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			System.out.println("Admin Buddy PA Controller loginUser" + loginUser);

			// *************Update the new change Plan info in Plan table
			// ********************

			String aid = request.getParameter("approvalLogId");
			System.out.println("aid >>>>>" + aid);
			ApprovalLogExample approvalexample = new ApprovalLogExample();
			ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();

			if (null != aid && !aid.isEmpty()) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
				System.out.println("   list size in approval list  :::   " + listapprovallog.size());

				// Change Data
				if (!listapprovallog.isEmpty()) {
					String status = listapprovallog.get(0).getStatus();
					short alogId = listapprovallog.get(0).getId();
					System.out.println("Status  :  " + status);
					System.out.println("Approvallog Id  :  " + alogId);

					convertBytes process = new convertBytes();
					Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());

					List<BuddyPABenefitGrpList> listChangeData;
					// object convert into BuddyPAPlan
					if (changeData != null) {
						listChangeData = (List<BuddyPABenefitGrpList>) changeData;
						System.out.println("  list agent prod map size ::::::         " + listChangeData.size());

						BuddyPADAO buddyPADAO = new BuddyPADAOImpl();

						for (int i = 0; i < listChangeData.size(); i++) {
							BuddyPABenefitGrpList buddyPABenefitGrp = new BuddyPABenefitGrpList();

							buddyPABenefitGrp.setBenefitId(listChangeData.get(i).getBenefitId());
							buddyPABenefitGrp.setBenefitName(listChangeData.get(i).getBenefitName());
							buddyPABenefitGrp.setCompanyId(listChangeData.get(i).getCompanyId());
							buddyPABenefitGrp.setPlanId(listChangeData.get(i).getPlanId());
							buddyPABenefitGrp.setPlanName(listChangeData.get(i).getPlanName());
							buddyPABenefitGrp.setComboId(listChangeData.get(i).getComboId());
							buddyPABenefitGrp.setComboName(listChangeData.get(i).getComboName());
							buddyPABenefitGrp.setAdultSumInsured(listChangeData.get(i).getAdultSumInsured());
							buddyPABenefitGrp.setAdultLoading(listChangeData.get(i).getAdultLoading());
							buddyPABenefitGrp.setChildSumInsured(listChangeData.get(i).getChildSumInsured());
							buddyPABenefitGrp.setChildLoading(listChangeData.get(i).getChildLoading());
							buddyPABenefitGrp.setEnable(listChangeData.get(i).getEnable());
							buddyPABenefitGrp.setVersion(listChangeData.get(i).getVersion());

							buddyPABenefitGrp.setModifiedBy(listChangeData.get(i).getModifiedBy());
							buddyPABenefitGrp.setModifiedDt(listChangeData.get(i).getModifiedDt());
							// *************Update the new change Plan Info in original table
							// ********************
							String msg = buddyPADAO.updBenefitGrouping(buddyPABenefitGrp);
							System.out.println(":::: BUDDY PA UPDATE BENEFIT GRP >>>>>"
									+ listChangeData.get(i).getBenefitId() + " " + msg);
						}
						// Update status in DSP_ADM_TBL_Approval_Log table
						alog.setId(alogId);
						alog.setStatus("3");
						alog.setChecker((short) Integer.parseInt(loginUser)); // get from the current login session
						alog.setUpdateDate(new Date());
						approvalLogMapper.updateByPrimaryKeySelective(alog);
					}
				}
			}

		} // try end

		catch (Exception e) {
			System.out.println("Exception Update new Plan Information : " + e);
		}

		return "redirect:/getApprovalList";

	}

	// ---------------------------------------------------------------END BuddyPA
	// Benefit Group
	// Change------------------------------------------------------------------------------

	// Reject Action
	@RequestMapping(value = "/rejectBuddyPAChange", method = RequestMethod.GET)
	public String rejectBuddyPAChange(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Admin Buddy PA Controller loginUser" + loginUser);

		ApprovalLogExample approvalLogexample = new ApprovalLogExample();
		ApprovalLog alog = new ApprovalLog();
		ApprovalLogExample.Criteria createCriteria = approvalLogexample.createCriteria();
		String aid = request.getParameter("approvalLogId");
		if (null != aid && !aid.isEmpty()) {
			Integer testint = new Integer(aid);
			createCriteria.andIdEqualTo(testint.shortValue());
			List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalLogexample);
			System.out.println("   list size in approval list  :::   " + listapprovallog.size());

			short alogId = listapprovallog.get(0).getId();
			alog.setId(alogId);
			alog.setStatus("2");
			alog.setChecker((short) Integer.parseInt(loginUser)); // get from the current login session
			alog.setUpdateDate(new Date());
			approvalLogMapper.updateByPrimaryKeySelective(alog);
		}

		return "redirect:/getApprovalList";
	}
}// Main class end
