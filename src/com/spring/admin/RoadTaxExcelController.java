package com.spring.admin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.spring.VO.roadtaxVO.RoadTaxTxnReport;
import com.spring.mapper.roadtax.RdtaxDeliveryAddMapper;
import com.spring.mapper.roadtax.RdtaxInvoiceMapper;
import com.spring.mapper.roadtax.RdtaxTrackMapper;
import com.spring.mapper.roadtax.RoadTaxConsignmentMapper;
import com.spring.mapper.roadtax.RoadTaxFloatMapper;
import com.spring.mapper.roadtax.RoadTaxThresholdMapper;
import com.spring.mapper.roadtax.RoadTaxTxnReportMapper;

@Controller
public class RoadTaxExcelController {
	RdtaxInvoiceMapper rdtaxInvoiceMapper;
	RdtaxDeliveryAddMapper rdtaxDeliveryAddMapper;
	RoadTaxTxnReportMapper roadTaxTxnReportMapper;
	RdtaxTrackMapper rdtaxTrackMapper;
	RoadTaxFloatMapper roadTaxFloatMapper;
	RoadTaxConsignmentMapper roadTaxConsignmentMapper;
	RoadTaxThresholdMapper roadTaxThresholdMapper;

	@Autowired
	public RoadTaxExcelController(RdtaxInvoiceMapper rdtaxInvoiceMapper, RdtaxDeliveryAddMapper rdtaxDeliveryAddMapper,
			RoadTaxTxnReportMapper roadTaxTxnReportMapper, RdtaxTrackMapper rdtaxTrackMapper,
			RoadTaxFloatMapper roadTaxFloatMapper, RoadTaxConsignmentMapper roadTaxConsignmentMapper,
			RoadTaxThresholdMapper roadTaxThresholdMapper) {
		this.rdtaxInvoiceMapper = rdtaxInvoiceMapper;
		this.rdtaxDeliveryAddMapper = rdtaxDeliveryAddMapper;
		this.roadTaxTxnReportMapper = roadTaxTxnReportMapper;
		this.rdtaxTrackMapper = rdtaxTrackMapper;
		this.roadTaxFloatMapper = roadTaxFloatMapper;
		this.roadTaxConsignmentMapper = roadTaxConsignmentMapper;
		this.roadTaxThresholdMapper = roadTaxThresholdMapper;
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 *             To generate the Txn Report in Excel format and down load the
	 *             report
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/generateExcelTxnReport", method = RequestMethod.GET)
	public String generateExcel(HttpServletRequest request, HttpServletResponse response, Model model)
			throws ServletException, IOException {

		System.out.println("generateExcelTxnReport calling############# ");

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");

		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "roadtax/pages/roadTaxAdminLogin";
		}
		ServletContext servletContext = request.getSession().getServletContext();
		// String searchPolicyNo= (String)session.getAttribute("policyNumberSession");

		List<RoadTaxTxnReport> roadTaxTxnReportList = new ArrayList<RoadTaxTxnReport>();
		if (session.getAttribute("roadTaxTxnReportList") != null
				|| !"".equals(session.getAttribute("roadTaxTxnReportList"))) {
			roadTaxTxnReportList = (ArrayList<RoadTaxTxnReport>) session.getAttribute("roadTaxTxnReportList");
			System.out.println("generateExcelTxnReport<size> => " + roadTaxTxnReportList.size());
		}

		// Start Excel
		Random ran = new Random();
		int randomNo = ran.nextInt(6) + 5;

		DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
		Date date = new Date();

		String file = "RoadTaxTransactionReport" + dateFormat.format(date) + randomNo + ".xlsx";
		System.out.println("generateExcelTxnReport<file> => " + file);

		String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
		// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Trasaction Report");
		System.out.println("generateExcelTxnReport<sheet> => " + sheet);

		String reportDateandTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new java.util.Date());

		Row row1 = sheet.createRow(0);
		Cell cellDate = row1.createCell(0);
		cellDate.setCellValue(reportDateandTime);

		// header fields based on product
		String[] fields = null;
		fields = new String[] { "Id", "RTAttn", "RTAmount", "RTPF", "RTPFGST", "RTPRF", "RTPRFGST", "RTDF", "RTDFGST",
				"RTTotalPayable", "RTActual", "RTPeriod", "RTAddress1", "RTAddress2", "RTAddress3", "RTPostcode",
				"RTState", "RTContactNo", "RTStatus", "RTRemark", "RTConsignmentNo", "RPartPostOff", "Date and Time",
				"Policy No", "Insured Name", "NRIC", "Car Registration Number", "Aging(Days)" };
		Row row2 = sheet.createRow(1);

		for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
			Cell cell = row2.createCell(cellIndex);
			cell.setCellValue(fields[cellIndex]);
		}

		String[] fields2 = null;

		int i = 1;
		int j = 2;

		for (RoadTaxTxnReport roadTaxReport : roadTaxTxnReportList) {

			fields2 = new String[] { Integer.toString(i), roadTaxReport.getDeliveryName(),
					String.valueOf(roadTaxReport.getRoadTaxAmount()), String.valueOf(roadTaxReport.getRtpf()),
					String.valueOf(roadTaxReport.getRtpfgst()), String.valueOf(roadTaxReport.getRtprf()),
					String.valueOf(roadTaxReport.getRtprfgst()), String.valueOf(roadTaxReport.getRtdf()),
					String.valueOf(roadTaxReport.getRtdfgst()), String.valueOf(roadTaxReport.getRttotalpayable()),
					String.valueOf(roadTaxReport.getActualAmount()), roadTaxReport.getPeriod(),
					roadTaxReport.getAddress1(), roadTaxReport.getAddress2(), roadTaxReport.getAddress3(),
					roadTaxReport.getPostcode(), roadTaxReport.getRtstate(), roadTaxReport.getMobile(),
					roadTaxReport.getPrintStatus(), roadTaxReport.getRemarks(), roadTaxReport.getTrackingNo(),
					roadTaxReport.getRpartpostoff(), roadTaxReport.getDateAndTime(), roadTaxReport.getPolicyNumber(),
					roadTaxReport.getCustomerName(), roadTaxReport.getCustomerNricId(), roadTaxReport.getRegNo(),
					roadTaxReport.getAging() };

			Row row3 = sheet.createRow(j);

			for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
				Cell cell = row3.createCell(cellIndex);
				cell.setCellValue(fields2[cellIndex]);
			}
			i++;
			j++;
		}

		FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
		workbook.write(outputStream);
		workbook.close();

		try {
			File fileToDownload = new File(relativeWebPath);
			FileInputStream inputStream = new FileInputStream(fileToDownload);
			response.setContentType(servletContext.getMimeType(relativeWebPath));
			response.setContentLength((int) fileToDownload.length());

			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
			response.setHeader(headerKey, headerValue);

			// get output stream of the response
			OutputStream outStream = response.getOutputStream();

			byte[] buffer = new byte[4096];
			int bytesRead = -1;

			// write bytes read from the input stream into the output stream
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}
			outStream.flush();
			outStream.close();
			inputStream.close();

			if (fileToDownload.exists()) {
				boolean success = new File(relativeWebPath).delete();

				if (success) {
					System.out.println("generateExcelTxnReport<" + file + "> has been successfully deleted");
				}
			}
		} catch (FileNotFoundException e) {
			response.getOutputStream().flush();
			response.getOutputStream().close();
			e.printStackTrace();
		} catch (IOException e) {
			response.getOutputStream().flush();
			response.getOutputStream().close();
			e.printStackTrace();
		}
		// End Excel
		System.out.println();

		return "roadtax/pages/roadTaxTransactionalReport";
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 *             To generate the Refund Report in Excel format and down load the
	 *             report
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/generateExcelRefundReport", method = RequestMethod.GET)
	public String generateExcelRefund(HttpServletRequest request, HttpServletResponse response, Model model)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");

		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "roadtax/pages/roadTaxAdminLogin";
		}
		ServletContext servletContext = request.getSession().getServletContext();
		// String searchPolicyNo= (String)session.getAttribute("policyNumberSession");

		List<RoadTaxTxnReport> roadTaxTxnReportList = new ArrayList<RoadTaxTxnReport>();
		if (session.getAttribute("roadTaxTxnReportList") != null
				|| !"".equals(session.getAttribute("roadTaxTxnReportList"))) {
			roadTaxTxnReportList = (ArrayList<RoadTaxTxnReport>) session.getAttribute("roadTaxTxnReportList");

			System.out.println("generateExcelRefundReport<size> => " + roadTaxTxnReportList.size());
		}

		// Start Excel
		Random ran = new Random();
		int randomNo = ran.nextInt(6) + 5;

		DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
		Date date = new Date();

		String file = "RoadTaxTransactionReport" + dateFormat.format(date) + randomNo + ".xlsx";

		String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
		// String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Refund Report");

		// String reportDateandTime = new
		// SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());

		// header fields based on product
		String[] fields = null;
		fields = new String[] { "Id", "Date and Time", "Policy No", "Insured Name", "NRIC", "Car Reg.No",
				"Road Tax Period (Month)", "Name of Recipient", "Address1", "Address2", "Address3", "Postcode",
				"Mobile No.", "Aging(Days)", "Printing Status", "Road Tax Amount Collected (RM)",
				"Actual Road Tax Amount (RM)", "Remarks (For failed printing etc.)", "Consignment Note Tracking No." };
		Row row1 = sheet.createRow(0);

		for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
			Cell cell = row1.createCell(cellIndex);
			cell.setCellValue(fields[cellIndex]);
		}

		String[] fields2 = null;
		// String quotationCreationDate = "";

		int i = 1;

		for (RoadTaxTxnReport roadTaxReport : roadTaxTxnReportList) {
			/*
			 * SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			 *
			 * SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); Date
			 * quotationCreationDateTmp=null;
			 *
			 * try {
			 *
			 * quotationCreationDateTmp = formatter.parse(roadTaxReport.getDateAndTime());
			 * quotationCreationDate= ft.format(quotationCreationDateTmp); } catch
			 * (ParseException e) { e.printStackTrace(); }
			 */

			fields2 = new String[] { Integer.toString(i), roadTaxReport.getDateAndTime(),
					roadTaxReport.getPolicyNumber(), roadTaxReport.getCustomerName(), roadTaxReport.getCustomerNricId(),
					roadTaxReport.getRegNo(), roadTaxReport.getPeriod(), roadTaxReport.getDeliveryName(),
					roadTaxReport.getAddress1(), roadTaxReport.getAddress2(), roadTaxReport.getAddress3(),
					roadTaxReport.getPostcode(), roadTaxReport.getMobile(), roadTaxReport.getAging(),
					roadTaxReport.getPrintStatus(), String.valueOf(roadTaxReport.getTotalRoadTax()),
					String.valueOf(roadTaxReport.getRoadTaxAmount()), roadTaxReport.getRemarks(),
					roadTaxReport.getTrackingNo() };

			Row row2 = sheet.createRow(i);

			for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
				Cell cell = row2.createCell(cellIndex);
				cell.setCellValue(fields2[cellIndex]);
			}

			i++;
		}

		FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
		workbook.write(outputStream);
		workbook.close();

		try {
			File fileToDownload = new File(relativeWebPath);
			FileInputStream inputStream = new FileInputStream(fileToDownload);
			response.setContentType(servletContext.getMimeType(relativeWebPath));
			response.setContentLength((int) fileToDownload.length());

			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
			response.setHeader(headerKey, headerValue);

			// get output stream of the response
			OutputStream outStream = response.getOutputStream();

			byte[] buffer = new byte[4096];
			int bytesRead = -1;

			// write bytes read from the input stream into the output stream
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}
			outStream.flush();
			outStream.close();
			inputStream.close();

			if (fileToDownload.exists()) {
				boolean success = new File(relativeWebPath).delete();

				if (success) {
					System.out.println("generateExcelRefundReport<" + file + "> has been successfully deleted");
				}
			}
		} catch (FileNotFoundException e) {
			response.getOutputStream().flush();
			response.getOutputStream().close();
			e.printStackTrace();
		} catch (IOException e) {
			response.getOutputStream().flush();
			response.getOutputStream().close();
			e.printStackTrace();
		}
		System.out.println();
		// End Excel
		return "roadtax/pages/roadTaxRefundReport";
	}

}
