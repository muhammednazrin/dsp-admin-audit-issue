package com.spring.admin;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.spring.VO.report.ALLReportVO;
import com.spring.VO.report.BuddyPAReportVO;
import com.spring.VO.report.HOHHReportVO;
import com.spring.VO.report.MPReport;
import com.spring.VO.report.MotorReportVO;
import com.spring.VO.report.TCReportVO;
import com.spring.VO.report.TLReportVO;
import com.spring.VO.report.TransSearchObject;
import com.spring.VO.report.TravelEzyReportVO;
import com.spring.VO.report.WTCReportVO;
import com.spring.VO.report.PCCA01Report;
import com.spring.mapper.CustomerMapper;
import com.spring.mapper.report.ALLReportMapper;
import com.spring.mapper.report.BuddyPAReportMapper;
import com.spring.mapper.report.HOHHReportMapper;
import com.spring.mapper.report.MPReportMapper;
import com.spring.mapper.report.MotorReportMapper;
import com.spring.mapper.report.NewCarTransactionalDetailMapper;
import com.spring.mapper.report.TCReportMapper;
import com.spring.mapper.report.TLReportMapper;
import com.spring.mapper.report.TravelEzyReportMapper;
import com.spring.mapper.report.WTCReportMapper;
import com.spring.mapper.report.PCCA01ReportMapper;
import com.spring.utils.ServiceValidationUtils;

@Controller
public class TranReportController {
	final static Logger logger = Logger.getLogger(TranReportController.class);

	MotorReportMapper motorReportMapper;
	TLReportMapper tlReportMapper;
	WTCReportMapper wtcReportMapper;
	HOHHReportMapper hohhReportMapper;
	ALLReportMapper allReportMapper;
	NewCarTransactionalDetailMapper newCarTransactionalDetailMapper;
	CustomerMapper customerMapper;
	BuddyPAReportMapper buddyPAReportMapper;
	TravelEzyReportMapper travEzReportMapper;
	TCReportMapper tcReportMapper;
	PCCA01ReportMapper pcca01ReportMapper;
	MPReportMapper mpReportMapper;

	@Autowired
	public TranReportController(MotorReportMapper motorReportMapper, TLReportMapper tlReportMapper,
			WTCReportMapper wtcReportMapper, HOHHReportMapper hohhReportMapper, ALLReportMapper allReportMapper,
			NewCarTransactionalDetailMapper newCarTransactionalDetailMapper, CustomerMapper customerMapper,
			BuddyPAReportMapper buddyPAReportMapper, TravelEzyReportMapper travEzReportMapper,
			TCReportMapper tcReportMapper,PCCA01ReportMapper pcca01ReportMapper,MPReportMapper mpReportMapper) {
		this.motorReportMapper = motorReportMapper;
		this.tlReportMapper = tlReportMapper;
		this.wtcReportMapper = wtcReportMapper;
		this.hohhReportMapper = hohhReportMapper;
		this.allReportMapper = allReportMapper;
		this.newCarTransactionalDetailMapper = newCarTransactionalDetailMapper;
		this.customerMapper = customerMapper;
		this.buddyPAReportMapper = buddyPAReportMapper;
		this.travEzReportMapper = travEzReportMapper;
		this.tcReportMapper = tcReportMapper;
		this.pcca01ReportMapper = pcca01ReportMapper;
		this.mpReportMapper = mpReportMapper;
	}



	public TranReportController() {
		// TODO Auto-generated constructor stub
	}



	@RequestMapping("/showTxnReport")
	public String showTxnReport(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		return "admin-report-transaction1";

	}

	@RequestMapping("/showTxnReport1")
	public String showTranReport(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		return "admin-report-transaction";

	}

	@RequestMapping(value = "/searchReportDone", method = RequestMethod.POST)
	public String searchReportDone(HttpServletRequest request, Model model) {

		DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(Locale.US);
		format.setParseBigDecimal(true);
		BigDecimal amt1 = new BigDecimal(0.00);
		BigDecimal amt2 = new BigDecimal(0.00);
		BigDecimal amt3 = new BigDecimal(0.00);
		BigDecimal amt4 = new BigDecimal(0.00);
		BigDecimal amt5 = new BigDecimal(0.00);
		BigDecimal amt6 = new BigDecimal(0.00);
		BigDecimal amt7 = new BigDecimal(0.00);
		BigDecimal amt8 = new BigDecimal(0.00);
		BigDecimal amt9 = new BigDecimal(0.00);
		BigDecimal grossAmt1_GST_STAMP_DUTY = new BigDecimal(0.00);
		BigDecimal grossAmt1_PA = new BigDecimal(0.00);
		BigDecimal grossAmt2_GST_STAMP_DUTY = new BigDecimal(0.00);
		BigDecimal grossAmt2_PA = new BigDecimal(0.00);

		BigDecimal netAmt1 = new BigDecimal(0.00);
		BigDecimal netAmt2 = new BigDecimal(0.00);

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String productType = request.getParameter("productType").trim();
		String productEntity = request.getParameter("productEntity").trim();
		String status = request.getParameter("status").trim();
		String PolicyCertificateNo = request.getParameter("policyNo").trim();
		String nRIC = request.getParameter("NRIC").trim();
		String receiptNo = request.getParameter("receiptNo").trim();
		String plateNo = request.getParameter("plateNo").trim();
		String dateFrom = request.getParameter("dateFrom").trim();
		String dateTo = request.getParameter("dateTo").trim();
		List<String> errorMessages = new ArrayList<String>();

		session.setAttribute("ProductType", request.getParameter("productType").trim());
		session.setAttribute("ProductEntity", request.getParameter("productEntity").trim());
		session.setAttribute("Status", request.getParameter("status").trim());
		session.setAttribute("PolicyCertificateNo", request.getParameter("policyNo").trim());
		session.setAttribute("NRIC", request.getParameter("NRIC").trim());
		session.setAttribute("PlateNo", request.getParameter("plateNo").trim());
		session.setAttribute("ReceiptNo", request.getParameter("receiptNo").trim());
		session.setAttribute("dateFrom", request.getParameter("dateFrom").trim());
		session.setAttribute("dateTo", request.getParameter("dateTo").trim());

		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			Date fromDate = null;
			Date toDate = null;
			try {
				fromDate = sdf.parse(dateFrom);

				toDate = sdf.parse(dateTo);

				if (fromDate.compareTo(toDate) > 0) {
					errorMessages.add("Date To Cannot Less Than Date From");
				}

				else {

					try {
						// in milliseconds
						long diff = toDate.getTime() - fromDate.getTime();

						long diffSeconds = diff / 1000 % 60;
						long diffMinutes = diff / (60 * 1000) % 60;
						long diffHours = diff / (60 * 60 * 1000) % 24;
						long diffDays = diff / (24 * 60 * 60 * 1000);

						if (diffDays > 31) {

							errorMessages.add("The date range cannot exceed than 1 month");

						}

					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (ServiceValidationUtils.isEmptyStringTrim(dateFrom)) {
				errorMessages.add("Date From Cannot Be Empty");
			}

			if (ServiceValidationUtils.isEmptyStringTrim(dateTo)) {
				errorMessages.add("Date To Cannot Be Empty");
			}

			if (ServiceValidationUtils.isEmptyStringTrim(productEntity)) {
				errorMessages.add("Please Select Product Entity");
			}

			else if (ServiceValidationUtils.isEmptyStringTrim(productType) && "O".equalsIgnoreCase(status)) {
				errorMessages.add("Please Select Product Type");
			}

			else if (ServiceValidationUtils.isEmptyStringTrim(productType) && "R".equalsIgnoreCase(status)) {
				errorMessages.add("Please Select Product Type");
			}

			if (errorMessages.size() > 0) {
				model.addAttribute("errorMessages", errorMessages);
				return "admin-report-transaction1";
			}

			logger.info("***************************"+productType);
		
			// START: MOTOR TRANSACTION REPORT
			if (productType.indexOf("MI") != -1 || productType.indexOf("MT") != -1) {
				TransSearchObject transSearchObject_Motor = new TransSearchObject();
				List<MotorReportVO> motorReportList = new ArrayList<MotorReportVO>();
				List<MotorReportVO> motorReportList_B4Pay = new ArrayList<MotorReportVO>();
				List<String> transactionStatusList = new ArrayList<String>();

				String query1 = "n";
				String query2 = "n";

				if ("EIB".equalsIgnoreCase(productEntity) && "".equalsIgnoreCase(productType)) {

					transSearchObject_Motor.setProductCode("MI");
				}

				if ("ETB".equalsIgnoreCase(productEntity) && "".equalsIgnoreCase(productType)) {

					transSearchObject_Motor.setProductCode("MT");
				}
				if ("".equalsIgnoreCase(productType) || productType == null) {
					transSearchObject_Motor.setProductCode("MI,MT");
				}

				if ("MI".equalsIgnoreCase(productType) || "MT".equalsIgnoreCase(productType)) {
					transSearchObject_Motor.setProductCode(productType);

				}
				// if Product is Chosen
				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("C");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_Motor.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus("'S','AP','F','O','R'");
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_Motor.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus(status);
				}

				if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)
						|| "C".equalsIgnoreCase(status)) {
					query1 = "y";
				}
				if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
					query2 = "y";
				}

				if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
					transSearchObject_Motor.setPolicyNo(PolicyCertificateNo);
					query2 = null;
				}
				if (!"".equalsIgnoreCase(nRIC)) {
					transSearchObject_Motor.setNricNo(nRIC);
				}
				if (!"".equalsIgnoreCase(receiptNo)) {
					transSearchObject_Motor.setInvoiceNo(receiptNo);
					query2 = null;
				}
				if (!ServiceValidationUtils.isEmptyStringTrim(plateNo)) {
					transSearchObject_Motor.setVehicleNo(plateNo);
				}

				transSearchObject_Motor.setDateFrom(dateFrom);
				transSearchObject_Motor.setDateTo(dateTo);

				// if query 1
				if ("y".equalsIgnoreCase(query1)) {
					motorReportList = motorReportMapper
							.selectMotorTransactionalReportAfterPayment(transSearchObject_Motor);

					if (motorReportList.size() > 0) {

						amt1 = motorReportList.get(0).getTotal_amount();
						grossAmt1_GST_STAMP_DUTY = motorReportList.get(0).getTotal_premium_after_gst_stamp_duty();
						grossAmt1_PA = motorReportList.get(0).getTotal_driver_pa_amount();
						netAmt1 = motorReportList.get(0).getTotal_net_amount();
					}

				} // End if query1 is Executed

				// if query 2
				if ("y".equalsIgnoreCase(query2)) {
					motorReportList_B4Pay = motorReportMapper
							.selectMotorTransactionalReportBeforePayment(transSearchObject_Motor);
					if (motorReportList_B4Pay.size() > 0) {

						amt2 = motorReportList_B4Pay.get(0).getTotal_amount();
						grossAmt2_GST_STAMP_DUTY = motorReportList_B4Pay.get(0).getTotal_premium_after_gst_stamp_duty();
						grossAmt2_PA = motorReportList_B4Pay.get(0).getTotal_driver_pa_amount();
						netAmt2 = motorReportList_B4Pay.get(0).getTotal_net_amount();

					}

				}

				motorReportList.addAll(motorReportList_B4Pay);

				Collections.sort(motorReportList, new Comparator<MotorReportVO>() {
					@Override
					public int compare(MotorReportVO o1, MotorReportVO o2) {
						return o1.getRecord_date().compareTo(o2.getRecord_date());
					}
				});

				Collections.reverse(motorReportList);

				if (motorReportList.size() <= 0) {
					model.addAttribute("emptySearch", "No Record Found");
					model.addAttribute("totalAmount", "0.00");
					model.addAttribute("totalGross", "0.00");
					model.addAttribute("totalNet", "0.00");
				}

				else {

					BigDecimal totalAmount = amt1.add(amt2);
					BigDecimal totalGross1 = grossAmt1_GST_STAMP_DUTY.add(grossAmt1_PA);
					BigDecimal totalGross2 = grossAmt2_GST_STAMP_DUTY.add(grossAmt2_PA);
					BigDecimal totalGross = totalGross1.add(totalGross2);
					BigDecimal totalNet = netAmt1.add(netAmt2);

					model.addAttribute("totalAmount", totalAmount);
					model.addAttribute("totalGross", totalGross);
					model.addAttribute("totalNet", totalNet);

				}
				model.addAttribute("reportResult", motorReportList);
				
				

			}

			// END: MOTOR TRANSACTION REPORT

			// START: EZYLIFE/TL TRANSACTION REPORT
			else if (productType.indexOf("TL") != -1 || productType.indexOf("IDS") != -1
					|| productType.indexOf("EZYTL") != -1 || productType.indexOf("ISCTL") != -1) {
				TransSearchObject transSearchObject_TL = new TransSearchObject();
				List<TLReportVO> tlReportList = new ArrayList<TLReportVO>();
				List<TLReportVO> tlReportList_B4Pay = new ArrayList<TLReportVO>();
				List<String> transactionStatusList = new ArrayList<String>();

				String query1 = "n";
				String query2 = "n";
				transSearchObject_TL.setProductCode(productType);
				logger.info("productType>>>>>>>>>>>>" + productType);

				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("C");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_TL.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus("'S','AP','F','O','R'");
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_TL.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus(status);
				}

				if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)
						|| "C".equalsIgnoreCase(status)) {
					query1 = "y";
				}
				if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
					query2 = "y";
				}

				if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
					transSearchObject_TL.setPolicyNo(PolicyCertificateNo);
					query2 = null;
				}
				if (!"".equalsIgnoreCase(nRIC)) {
					transSearchObject_TL.setNricNo(nRIC);
				}
				if (!"".equalsIgnoreCase(receiptNo)) {
					transSearchObject_TL.setInvoiceNo(receiptNo);
					query2 = null;
				}

				transSearchObject_TL.setDateFrom(dateFrom);
				transSearchObject_TL.setDateTo(dateTo);

				// if query 1
				if ("y".equalsIgnoreCase(query1)) {
					tlReportList = tlReportMapper.selectTLTransactionalReportAfterPayment(transSearchObject_TL);
					if (tlReportList.size() > 0) {
						amt1 = tlReportList.get(0).getTotal_amount();
					}

				} // End if query1 is Executed

				// if query 2
				if ("y".equalsIgnoreCase(query2)) {
					tlReportList_B4Pay = tlReportMapper.selectTLTransactionalReportBeforePayment(transSearchObject_TL);
					logger.info("inside search report tlReportList_B4Pay size before include epp length "
							+ tlReportList_B4Pay.size());
					if (productType.indexOf("IDS") != -1) {
						List<TLReportVO> idsReportList_B4Pay = new ArrayList<TLReportVO>();
						try {
							idsReportList_B4Pay = tlReportMapper
									.selectEPPIDSTransactionalReportBeforePayment(transSearchObject_TL);

						} catch (PersistenceException e) {
							e.printStackTrace();
							// final Throwable cause = e.getCause();
							// if (cause instanceof PSQLException) {
							// // handle the exception
							// }
						}
						logger.info("inside search report tlReportList_B4Pay size epp length "
								+ idsReportList_B4Pay.size());

						tlReportList_B4Pay.addAll(idsReportList_B4Pay);
					}

					if (tlReportList_B4Pay.size() > 0) {

						amt2 = tlReportList_B4Pay.get(0).getTotal_amount();
					}
				}

				tlReportList.addAll(tlReportList_B4Pay);

				Collections.sort(tlReportList, new Comparator<TLReportVO>() {
					@Override
					public int compare(TLReportVO o1, TLReportVO o2) {
						return o1.getRecord_date().compareTo(o2.getRecord_date());
					}
				});

				Collections.reverse(tlReportList);

				if (tlReportList.size() <= 0) {
					model.addAttribute("emptySearch", "No Record Found");
					model.addAttribute("totalAmount", "0.00");
					model.addAttribute("totalGross", "0.00");
					model.addAttribute("totalNet", "0.00");
				} else {

					BigDecimal totalAmount = amt1.add(amt2);
					model.addAttribute("totalAmount", totalAmount);
					model.addAttribute("totalGross", totalAmount);
					model.addAttribute("totalNet", "");
				}
				model.addAttribute("reportResult", tlReportList);
				logger.info("AGENTCODE"+tlReportList.get(0).getAgent_code());
				logger.info("Agentname"+tlReportList.get(0).getAgent_name());
				logger.info("aagent product code"+tlReportList.get(0).getProduct_code());
				
			}

			// END: EZYLIFE TRANSACTION REPORT
			// START: WTC TRANSACTION REPORT
			else if (productType.indexOf("WTC") != -1 || productType.indexOf("TPT") != -1) {
				TransSearchObject transSearchObject_WTC = new TransSearchObject();
				List<WTCReportVO> wtcReportList = new ArrayList<WTCReportVO>();
				List<WTCReportVO> wtcReportList_B4Pay = new ArrayList<WTCReportVO>();
				List<String> transactionStatusList = new ArrayList<String>();
				String query1 = "n";
				String query2 = "n";

				transSearchObject_WTC.setProductCode(productType);

				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("C");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_WTC.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus("'S','AP','F','O','R'");
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_WTC.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus(status);
				}

				if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)
						|| "C".equalsIgnoreCase(status)) {
					query1 = "y";
				}
				if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
					query2 = "y";
				}

				if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
					transSearchObject_WTC.setPolicyNo(PolicyCertificateNo);
					query2 = null;
				}
				if (!"".equalsIgnoreCase(nRIC)) {
					transSearchObject_WTC.setNricNo(nRIC);
				}
				if (!"".equalsIgnoreCase(receiptNo)) {
					transSearchObject_WTC.setInvoiceNo(receiptNo);
					query2 = null;
				}

				transSearchObject_WTC.setDateFrom(dateFrom);
				transSearchObject_WTC.setDateTo(dateTo);

				// if query 1
				if ("y".equalsIgnoreCase(query1)) {
					wtcReportList = wtcReportMapper.selectWTCTransactionalReportAfterPayment(transSearchObject_WTC);

					if (wtcReportList.size() > 0) {
						amt1 = wtcReportList.get(0).getTotal_amount();
						amt1 = wtcReportList.get(0).getTotal_amount();
						grossAmt1_GST_STAMP_DUTY = wtcReportList.get(0).getTotal_premium_after_gst_stamp_duty();
						netAmt1 = wtcReportList.get(0).getTotal_net_amount();
					}

				} // End if query1 is Executed

				// if query 2
				if ("y".equalsIgnoreCase(query2)) {
					if (wtcReportList_B4Pay.size() > 0) {
						wtcReportList_B4Pay = wtcReportMapper
								.selectWTCTransactionalReportBeforePayment(transSearchObject_WTC);
						amt2 = wtcReportList_B4Pay.get(0).getTotal_amount();
						grossAmt2_GST_STAMP_DUTY = wtcReportList_B4Pay.get(0).getTotal_premium_after_gst_stamp_duty();
						netAmt2 = wtcReportList_B4Pay.get(0).getTotal_net_amount();
					}

				}

				wtcReportList.addAll(wtcReportList_B4Pay);

				Collections.sort(wtcReportList, new Comparator<WTCReportVO>() {
					@Override
					public int compare(WTCReportVO o1, WTCReportVO o2) {
						return o1.getRecord_date().compareTo(o2.getRecord_date());
					}
				});

				Collections.reverse(wtcReportList);

				if (wtcReportList.size() <= 0) {

					model.addAttribute("emptySearch", "No Record Found");
					model.addAttribute("totalAmount", "0.00");
					model.addAttribute("totalGross", "0.00");
					model.addAttribute("totalNet", "0.00");
				}

				else {

					BigDecimal totalAmount = amt1.add(amt2);
					BigDecimal totalGross1 = grossAmt1_GST_STAMP_DUTY;
					BigDecimal totalGross2 = grossAmt2_GST_STAMP_DUTY;
					BigDecimal totalGross = totalGross1.add(totalGross2);
					BigDecimal totalNet = netAmt1.add(netAmt2);

					model.addAttribute("totalAmount", totalAmount);
					model.addAttribute("totalGross", totalGross);
					model.addAttribute("totalNet", totalNet);

				}
				model.addAttribute("reportResult", wtcReportList);

			}

			// END: WTC TRANSACTION REPORT

			// START: HOHH TRANSACTION REPORT
			else if (productType.indexOf("HOHH") != -1) {
				TransSearchObject transSearchObject_HOHH = new TransSearchObject();
				List<HOHHReportVO> hohhReportList = new ArrayList<HOHHReportVO>();
				List<HOHHReportVO> hohhReportList_B4Pay = new ArrayList<HOHHReportVO>();
				List<String> transactionStatusList = new ArrayList<String>();

				String query1 = "n";
				String query2 = "n";

				transSearchObject_HOHH.setProductCode(productType);
				logger.info(productType + "productType in hohh");
				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("C");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_HOHH.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus("'S','AP','F','O','R'");
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_HOHH.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus(status);
				}

				if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)
						|| "C".equalsIgnoreCase(status)) {
					query1 = "y";
				}
				if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
					query2 = "y";
				}

				if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
					transSearchObject_HOHH.setPolicyNo(PolicyCertificateNo);
					query2 = null;
				}
				if (!"".equalsIgnoreCase(nRIC)) {
					transSearchObject_HOHH.setNricNo(nRIC);
				}
				if (!"".equalsIgnoreCase(receiptNo)) {
					transSearchObject_HOHH.setInvoiceNo(receiptNo);
					query2 = null;
				}

				transSearchObject_HOHH.setDateFrom(dateFrom);
				transSearchObject_HOHH.setDateTo(dateTo);

				// if query 1
				if ("y".equalsIgnoreCase(query1)) {
					hohhReportList = hohhReportMapper.selectHOHHTransactionalReportAfterPayment(transSearchObject_HOHH);
					//logger.info(hohhReportList.get(0).getPaymentTrxID() + "payment Tran ID");
					if (hohhReportList.size() > 0) {
						amt1 = hohhReportList.get(0).getTotal_amount();
						grossAmt1_GST_STAMP_DUTY = hohhReportList.get(0).getTotal_premium_after_gst_stamp_duty();
						netAmt1 = hohhReportList.get(0).getTotal_net_amount();
					}

				} // End if query1 is Executed

				// if query 2
				if ("y".equalsIgnoreCase(query2)) {
					hohhReportList_B4Pay = hohhReportMapper
							.selectHOHHTransactionalReportBeforePayment(transSearchObject_HOHH);
					if (hohhReportList_B4Pay.size() > 0) {
						amt2 = hohhReportList_B4Pay.get(0).getTotal_amount();
						grossAmt2_GST_STAMP_DUTY = hohhReportList_B4Pay.get(0).getTotal_premium_after_gst_stamp_duty();
						netAmt2 = hohhReportList_B4Pay.get(0).getTotal_net_amount();
					}
				}

				hohhReportList.addAll(hohhReportList_B4Pay);

				Collections.sort(hohhReportList, new Comparator<HOHHReportVO>() {
					@Override
					public int compare(HOHHReportVO o1, HOHHReportVO o2) {
						return o1.getRecord_date().compareTo(o2.getRecord_date());
					}
				});

				Collections.reverse(hohhReportList);

				if (hohhReportList.size() <= 0) {
					model.addAttribute("emptySearch", "No Record Found");
					model.addAttribute("totalAmount", "0.00");
					model.addAttribute("totalGross", "0.00");
					model.addAttribute("totalNet", "0.00");
				}

				else {

					BigDecimal totalAmount = amt1.add(amt2);
					BigDecimal totalGross1 = grossAmt1_GST_STAMP_DUTY;
					BigDecimal totalGross2 = grossAmt2_GST_STAMP_DUTY;
					BigDecimal totalGross = totalGross1.add(totalGross2);
					BigDecimal totalNet = netAmt1.add(netAmt2);
					totalGross = totalGross.add(totalNet);

					model.addAttribute("totalAmount", totalAmount);
					model.addAttribute("totalGross", totalGross);
					model.addAttribute("totalNet", totalNet);

				}
				model.addAttribute("reportResult", hohhReportList);

			}

			// END: HOHH TRANSACTION REPORT

			// START: BUDDY PA REPORT
			else if (productType.indexOf("CPP") != -1 || productType.indexOf("TPP") != -1) {
				TransSearchObject transSearchObject_BuddyPA = new TransSearchObject();
				List<BuddyPAReportVO> buddyPAReportList = new ArrayList<BuddyPAReportVO>();
				List<BuddyPAReportVO> buddyPAReportList_B4Pay = new ArrayList<BuddyPAReportVO>();
				List<String> transactionStatusList = new ArrayList<String>();
				String query1 = "n";
				String query2 = "n";

				transSearchObject_BuddyPA.setProductCode(productType);

				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("C");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_BuddyPA.setTransactionStatus(transactionStatusList);
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_BuddyPA.setTransactionStatus(transactionStatusList);
				}

				if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)
						|| "C".equalsIgnoreCase(status)) {
					query1 = "y";
				}
				if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
					query2 = "y";
				}

				if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
					transSearchObject_BuddyPA.setPolicyNo(PolicyCertificateNo);
					query2 = null;
				}
				if (!"".equalsIgnoreCase(nRIC)) {
					transSearchObject_BuddyPA.setNricNo(nRIC);
				}
				if (!"".equalsIgnoreCase(receiptNo)) {
					transSearchObject_BuddyPA.setInvoiceNo(receiptNo);
					query2 = null;
				}

				transSearchObject_BuddyPA.setDateFrom(dateFrom);
				transSearchObject_BuddyPA.setDateTo(dateTo);

				// if query 1
				if ("y".equalsIgnoreCase(query1)) {
					buddyPAReportList = buddyPAReportMapper
							.selectBuddyPATransactionalReportAfterPayment(transSearchObject_BuddyPA);

					if (buddyPAReportList.size() > 0) {
						amt1 = buddyPAReportList.get(0).getTotal_amount();
						grossAmt1_GST_STAMP_DUTY = buddyPAReportList.get(0).getTotal_premium_after_gst_stamp_duty();
						netAmt1 = buddyPAReportList.get(0).getTotal_net_amount();
					}

				} // End if query1 is Executed

				// if query 2
				if ("y".equalsIgnoreCase(query2)) {
					buddyPAReportList_B4Pay = buddyPAReportMapper
							.selectBuddyPATransactionalReportBeforePayment(transSearchObject_BuddyPA);
					if (buddyPAReportList_B4Pay.size() > 0) {
						amt2 = buddyPAReportList_B4Pay.get(0).getTotal_amount();
						grossAmt2_GST_STAMP_DUTY = buddyPAReportList_B4Pay.get(0)
								.getTotal_premium_after_gst_stamp_duty();
						netAmt2 = buddyPAReportList_B4Pay.get(0).getTotal_net_amount();
					}

				}

				buddyPAReportList.addAll(buddyPAReportList_B4Pay);

				Collections.sort(buddyPAReportList, new Comparator<BuddyPAReportVO>() {
					@Override
					public int compare(BuddyPAReportVO o1, BuddyPAReportVO o2) {
						return o1.getRecord_date().compareTo(o2.getRecord_date());
					}
				});

				Collections.reverse(buddyPAReportList);

				if (buddyPAReportList.size() <= 0) {

					model.addAttribute("emptySearch", "No Record Found");
					model.addAttribute("totalAmount", "0.00");
					model.addAttribute("totalGross", "0.00");
					model.addAttribute("totalNet", "0.00");
				}

				else {

					BigDecimal totalAmount = amt1.add(amt2);
					BigDecimal totalGross1 = grossAmt1_GST_STAMP_DUTY;
					BigDecimal totalGross2 = grossAmt2_GST_STAMP_DUTY;
					BigDecimal totalGross = totalGross1.add(totalGross2);
					BigDecimal totalNet = netAmt1.add(netAmt2);

					model.addAttribute("totalAmount", totalAmount);
					model.addAttribute("totalGross", totalGross);
					model.addAttribute("totalNet", totalNet);

				}
				model.addAttribute("reportResult", buddyPAReportList);

			}

			// END: BUDDY PA REPORT
			// START: TRAVEL EZY REPORT
			else if (productType.indexOf("BPT") != -1 || productType.indexOf("TZT") != -1) {
				TransSearchObject transSearchObject_TravEz = new TransSearchObject();
				List<TravelEzyReportVO> travEzReportList = new ArrayList<TravelEzyReportVO>();
				List<TravelEzyReportVO> travEzReportList_B4Pay = new ArrayList<TravelEzyReportVO>();
				List<String> transactionStatusList = new ArrayList<String>();
				String query1 = "n";
				String query2 = "n";

				transSearchObject_TravEz.setProductCode(productType);

				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("C");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_TravEz.setTransactionStatus(transactionStatusList);
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_TravEz.setTransactionStatus(transactionStatusList);
				}

				if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)
						|| "C".equalsIgnoreCase(status)) {
					query1 = "y";
				}
				if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
					query2 = "y";
				}

				if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
					transSearchObject_TravEz.setPolicyNo(PolicyCertificateNo);
					query2 = null;
				}
				if (!"".equalsIgnoreCase(nRIC)) {
					transSearchObject_TravEz.setNricNo(nRIC);
				}
				if (!"".equalsIgnoreCase(receiptNo)) {
					transSearchObject_TravEz.setInvoiceNo(receiptNo);
					query2 = null;
				}

				transSearchObject_TravEz.setDateFrom(dateFrom);
				transSearchObject_TravEz.setDateTo(dateTo);

				// if query 1
				if ("y".equalsIgnoreCase(query1)) {
					travEzReportList = travEzReportMapper
							.selectTravelEzyTransactionalReportAfterPayment(transSearchObject_TravEz);

					if (travEzReportList.size() > 0) {
						amt1 = travEzReportList.get(0).getTotal_amount();
						grossAmt1_GST_STAMP_DUTY = travEzReportList.get(0).getTotal_premium_after_gst_stamp_duty();
						netAmt1 = travEzReportList.get(0).getTotal_net_amount();
					}

				} // End if query1 is Executed

				// if query 2
				if ("y".equalsIgnoreCase(query2)) {
					travEzReportList_B4Pay = travEzReportMapper
							.selectTravelEzyTransactionalReportBeforePayment(transSearchObject_TravEz);
					if (travEzReportList_B4Pay.size() > 0) {
						amt2 = travEzReportList_B4Pay.get(0).getTotal_amount();
						grossAmt2_GST_STAMP_DUTY = travEzReportList_B4Pay.get(0)
								.getTotal_premium_after_gst_stamp_duty();
						netAmt2 = travEzReportList_B4Pay.get(0).getTotal_net_amount();
					}

				}

				travEzReportList.addAll(travEzReportList_B4Pay);

				Collections.sort(travEzReportList, new Comparator<TravelEzyReportVO>() {
					@Override
					public int compare(TravelEzyReportVO o1, TravelEzyReportVO o2) {
						return o1.getRecord_date().compareTo(o2.getRecord_date());
					}
				});

				Collections.reverse(travEzReportList);

				if (travEzReportList.size() <= 0) {

					model.addAttribute("emptySearch", "No Record Found");
					model.addAttribute("totalAmount", "0.00");
					model.addAttribute("totalGross", "0.00");
					model.addAttribute("totalNet", "0.00");
				}

				else {

					BigDecimal totalAmount = amt1.add(amt2);
					BigDecimal totalGross1 = grossAmt1_GST_STAMP_DUTY;
					BigDecimal totalGross2 = grossAmt2_GST_STAMP_DUTY;
					BigDecimal totalGross = totalGross1.add(totalGross2);
					BigDecimal totalNet = netAmt1.add(netAmt2);

					model.addAttribute("totalAmount", totalAmount);
					model.addAttribute("totalGross", totalGross);
					model.addAttribute("totalNet", totalNet);

				}
				model.addAttribute("reportResult", travEzReportList);

			}

			// END: TRAVEL EZY REPORT

			// START: TripCare TRANSACTION REPORT
			else if (productType.indexOf("TCI") != -1 || productType.indexOf("TCT") != -1) {
				TransSearchObject transSearchObject_TC = new TransSearchObject();
				List<TCReportVO> tcReportList = new ArrayList<TCReportVO>();
				List<TCReportVO> tcReportList_B4Pay = new ArrayList<TCReportVO>();
				List<String> transactionStatusList = new ArrayList<String>();
				String query1 = "n";
				String query2 = "n";
				

				transSearchObject_TC.setProductCode(productType);
				logger.info("productType>>>>>>>>>>>>" + productType);

				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("C");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_TC.setTransactionStatus(transactionStatusList);
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_TC.setTransactionStatus(transactionStatusList);
				}

				if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)
						|| "C".equalsIgnoreCase(status)) {

					query1 = "y";
				}
				if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
					query2 = "y";
				}

				if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
					transSearchObject_TC.setPolicyNo(PolicyCertificateNo);
					query2 = null;
				}
				if (!"".equalsIgnoreCase(nRIC)) {
					transSearchObject_TC.setNricNo(nRIC);
				}
				if (!"".equalsIgnoreCase(receiptNo)) {
					transSearchObject_TC.setInvoiceNo(receiptNo);
					query2 = null;
				}
				logger.info("   check tripcare report - :::   " + query2);

				transSearchObject_TC.setDateFrom(dateFrom);
				transSearchObject_TC.setDateTo(dateTo);

				// if query 1
				if ("y".equalsIgnoreCase(query1)) {
					tcReportList = tcReportMapper.selectTCTransactionalReportAfterPayment(transSearchObject_TC);

					if (tcReportList.size() > 0) {
						amt1 = tcReportList.get(0).getTotal_amount();
						amt1 = tcReportList.get(0).getTotal_amount();
						grossAmt1_GST_STAMP_DUTY = tcReportList.get(0).getTotal_premium_after_gst_stamp_duty();
						netAmt1 = tcReportList.get(0).getTotal_net_amount();
					}

				} // End if query1 is Executed

				// if query 2
				if ("y".equalsIgnoreCase(query2)) {
					tcReportList_B4Pay = tcReportMapper.selectTCTransactionalReportBeforePayment(transSearchObject_TC);

					if (tcReportList_B4Pay.size() > 0) {
						amt2 = tcReportList_B4Pay.get(0).getTotal_amount();
						grossAmt2_GST_STAMP_DUTY = tcReportList_B4Pay.get(0).getTotal_premium_after_gst_stamp_duty();
						netAmt2 = tcReportList_B4Pay.get(0).getTotal_net_amount();
					}

				}

				tcReportList.addAll(tcReportList_B4Pay);

				Collections.sort(tcReportList, new Comparator<TCReportVO>() {
					@Override
					public int compare(TCReportVO o1, TCReportVO o2) {
						return o1.getRecord_date().compareTo(o2.getRecord_date());
					}
				});

				Collections.reverse(tcReportList);

				if (tcReportList.size() <= 0) {

					model.addAttribute("emptySearch", "No Record Found");
					model.addAttribute("totalAmount", "0.00");
					model.addAttribute("totalGross", "0.00");
					model.addAttribute("totalNet", "0.00");
				}

				else {

					BigDecimal totalAmount = amt1.add(amt2);
					BigDecimal totalGross1 = grossAmt1_GST_STAMP_DUTY;
					BigDecimal totalGross2 = grossAmt2_GST_STAMP_DUTY;
					BigDecimal totalGross = totalGross1.add(totalGross2);
					BigDecimal totalNet = netAmt1.add(netAmt2);

					model.addAttribute("totalAmount", totalAmount);
					model.addAttribute("totalGross", totalGross);
					model.addAttribute("totalNet", totalNet);

				}
				logger.info("reportResult----->"+tcReportList);
				model.addAttribute("reportResult", tcReportList);
				logger.info("Agent_Code----->"+tcReportList.get(0).getAgent_code());
				logger.info("Agent_Name------->"+tcReportList.get(0).getAgent_name());
				logger.info("Agent_product_code----->"+tcReportList.get(0).getProduct_code());
				logger.info("Auth code----> "+tcReportList.get(0).getAuth_id());
				logger.info("payment ---> "+tcReportList.get(0).getBankrefno());
				logger.info("pmnt_gateway_code ---> "+tcReportList.get(0).getPmnt_gateway_code());
				logger.info("getBankrefno ---> "+tcReportList.get(0).getBankrefno());
				logger.info("getFpx_debitauthcode ---> "+tcReportList.get(0).getFpx_debitauthcode());
				logger.info("getPmnt_gateway_code ---> "+tcReportList.get(0).getPmnt_gateway_code());
				logger.info("getTransaction_id ---> "+tcReportList.get(0).getTransaction_id());
				logger.info("payment mode ----> "+tcReportList.get(0).getParamName());
				
				
				//pmnt_gateway_code
				

			}

			// END: TripCare TRANSACTION REPORT
			//START : CANCER CARE TRANSACTION REPORT
			
		//START : CANCER CARE TRANSACTION REPORT
			
			else if (productType.indexOf("PCCA01") != -1 || productType.indexOf("PTCA01") != -1) {
				
				TransSearchObject transSearchObject_PCCA = new TransSearchObject();
				List<PCCA01Report> pccaReportList = new ArrayList<PCCA01Report>();
				List<PCCA01Report> pccaReportList_B4Pay = new ArrayList<PCCA01Report>();
				List<String> transactionStatusList = new ArrayList<String>();

				String query1 = "n";
				String query2 = "n";
				transSearchObject_PCCA.setProductCode(productType);
				logger.info("productType>>>>>>productTypePCCA01>>>>>>" + productType);

				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("C");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_PCCA.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus("'S','AP','F','O','R'");
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_PCCA.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus(status);
				}

				if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)
						|| "C".equalsIgnoreCase(status)) {
					query1 = "y";
				}
				if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
					query2 = "y";
				}

				if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
					transSearchObject_PCCA.setPolicyNo(PolicyCertificateNo);
					query2 = null;
				}
				if (!"".equalsIgnoreCase(nRIC)) {
					transSearchObject_PCCA.setNricNo(nRIC);
				}
				if (!"".equalsIgnoreCase(receiptNo)) {
					transSearchObject_PCCA.setInvoiceNo(receiptNo);
					query2 = null;
				}

				logger.info("   check PCCA report  :::   " + query2);
				
				transSearchObject_PCCA.setDateFrom(dateFrom);
				transSearchObject_PCCA.setDateTo(dateTo);

				// if query 1
				if ("y".equalsIgnoreCase(query1)) {
					//testing
					pccaReportList = pcca01ReportMapper.selectPCCA01TransactionalReportAfterPayment(transSearchObject_PCCA);
					if (pccaReportList.size() > 0) {
						amt1 = pccaReportList.get(0).getTotal_amount();
					}

				} // End if query1 is Executed

				// if query 2
				if ("y".equalsIgnoreCase(query2)) {
					pccaReportList_B4Pay = pcca01ReportMapper.selectPCCA01TransactionalReportBeforePayment(transSearchObject_PCCA);
					logger.info("inside search report pccaReportList_B4Pay size before include epp length "
							+ pccaReportList_B4Pay.size());
					if (productType.indexOf("PTCA01") != -1) {
						List<PCCA01Report> ptcaReportList_B4Pay = new ArrayList<PCCA01Report>();
						try {
							pccaReportList_B4Pay = pcca01ReportMapper
									.selectPCCA01TransactionalReportBeforePayment(transSearchObject_PCCA);

						} catch (PersistenceException e) {
							e.printStackTrace();
							// final Throwable cause = e.getCause();
							// if (cause instanceof PSQLException) {
							// // handle the exception
							// }
						}
						logger.info("inside search report PTCAReportList_B4Pay size epp length "
								+ pccaReportList_B4Pay.size());

						pccaReportList_B4Pay.addAll(ptcaReportList_B4Pay);
					}

					if (pccaReportList_B4Pay.size() > 0) {

						amt2 = pccaReportList_B4Pay.get(0).getTotal_amount();
					}
				}

				pccaReportList.addAll(pccaReportList_B4Pay);

				Collections.sort(pccaReportList, new Comparator<PCCA01Report>() {
					@Override
					public int compare(PCCA01Report o1, PCCA01Report o2) {
						return o1.getRecord_date().compareTo(o2.getRecord_date());
					}
				});

				Collections.reverse(pccaReportList);

				if (pccaReportList.size() <= 0) {
					model.addAttribute("emptySearch", "No Record Found");
					model.addAttribute("totalAmount", "0.00");
					model.addAttribute("totalGross", "0.00");
					model.addAttribute("totalNet", "0.00");
				} else {

					BigDecimal totalAmount = amt1.add(amt2);
					model.addAttribute("totalAmount", totalAmount);
					model.addAttribute("totalGross", totalAmount);
					model.addAttribute("totalNet", "");
				}
				logger.info("Report result list printin------------>"+pccaReportList);
				model.addAttribute("reportResult", pccaReportList);
				logger.info("Agent_Code -------> "+pccaReportList.get(0).getAgent_code());
				logger.info("Agent_Name------>"+pccaReportList.get(0).getAgent_name());
				logger.info("Agent_product_code---->"+pccaReportList.get(0).getProduct_code());
				logger.info("Agent Authcode-->"+pccaReportList.get(0).getAuth_code());
				logger.info("Agent txsnidmapy--->"+pccaReportList.get(0).getTxn_id());
			}

			//END:CANCER CARE REPORT
			

			//END:CANCER CARE REPORT
			
			else if (productType.indexOf("MP") != -1 || productType.indexOf("MPT") != -1) {
				TransSearchObject transSearchObject_MP = new TransSearchObject();
				List<MPReport> mpReportList = new ArrayList<MPReport>();
				List<MPReport> mpReportList_B4Pay = new ArrayList<MPReport>();
				List<MPReport>pctmReportList=new ArrayList<MPReport>();
				List<String> transactionStatusList = new ArrayList<String>();

				String query1 = "n";
				String query2 = "n";
			
					transSearchObject_MP.setProductCode(productType);
			
				logger.info("productType>>>>>>>>>>>>" + transSearchObject_MP.getProductCode());
				logger.info("STSUS>>>>>>**+tesT@@@@@test+++ing@@@@@Tting+**>>>>>>" + status);
				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("C");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_MP.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus("'S','AP','F','O','R'");
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_MP.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus(status);
				}

				if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)
						|| "C".equalsIgnoreCase(status)) {
					query1 = "y";
				}
				if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
					query2 = "y";
				}

				if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
					transSearchObject_MP.setPolicyNo(PolicyCertificateNo);
					query2 = null;
				}
				if (!"".equalsIgnoreCase(nRIC)) {
					transSearchObject_MP.setNricNo(nRIC);
				}
				if (!"".equalsIgnoreCase(receiptNo)) {
					transSearchObject_MP.setInvoiceNo(receiptNo);
					query2 = null;
				}
				logger.info("   check MP report  :::   " + query2);
				transSearchObject_MP.setDateFrom(dateFrom);
				transSearchObject_MP.setDateTo(dateTo);

				// if query 1
				if ("y".equalsIgnoreCase(query1)) {
					mpReportList = mpReportMapper.selectMPTransactionalReportAfterPayment(transSearchObject_MP);
					
					if (mpReportList.size() > 0) {
						  
						logger.info("productType>>>8888888888888888 >>>>>>>>" + productType);
						amt1 = mpReportList.get(0).getTotal_amount();
					}

				} // End if query1 is Executed

				// if query 2
				if ("y".equalsIgnoreCase(query2)) {
					mpReportList_B4Pay = mpReportMapper.selectMPTransactionalReportBeforePayment(transSearchObject_MP);
					logger.info("+++++++++inside search report mpReportList_B4Pay size before include epp length ----->"
							+ mpReportList_B4Pay.size());
							
				/* 	for(int i=0;i<mpReportList_B4Pay.size();i++)
					{
						MPReport mpreport=mpReportList_B4Pay.get(i);
						logger.info("*********** mpReportList_B4Pay agentcode---> "+ mpReportList_B4Pay.get(i).getAgent_code()+"agentName----> "+mpReportList_B4Pay.get(i).getAgent_name());
						
					} */
					
					
					if (productType.indexOf("MPT") != -1) {
						List<MPReport> mptReportList_B4Pay = new ArrayList<MPReport>();
						try {
							mpReportList_B4Pay = mpReportMapper
									.selectMPTransactionalReportBeforePayment(transSearchObject_MP);

						} catch (PersistenceException e) {
							e.printStackTrace();
							// final Throwable cause = e.getCause();
							// if (cause instanceof PSQLException) {
							// // handle the exception
							// }
						}
						logger.info("***********8inside search report mptReportList_B4Pay size epp length---> "
								+ mpReportList_B4Pay.size());

						mpReportList_B4Pay.addAll(mptReportList_B4Pay);
					}

					if (mpReportList_B4Pay.size() > 0) {

						amt2 = mpReportList_B4Pay.get(0).getTotal_amount();
					}
				}

				mpReportList.addAll(mpReportList_B4Pay);
				
				for(int i=0;i<=mpReportList_B4Pay.size();i++)
				{
					MPReport mpreport=mpReportList.get(i);
					logger.info("*********** mpReportList_B4Pay agentcode---> "+ mpReportList.get(i).getAgent_code()+"agentName----> "+mpReportList.get(i).getAgent_name());
					
				}

				Collections.sort(mpReportList, new Comparator<MPReport>() {
					@Override
					public int compare(MPReport o1, MPReport o2) {
						return o1.getRecord_date().compareTo(o2.getRecord_date());
					}
				});

				Collections.reverse(mpReportList);

				if (mpReportList.size() <= 0) {
					model.addAttribute("emptySearch", "No Record Found");
					model.addAttribute("totalAmount", "0.00");
					model.addAttribute("totalGross", "0.00");
					model.addAttribute("totalNet", "0.00");
				} else {

					BigDecimal totalAmount = amt1.add(amt2);
					model.addAttribute("totalAmount", totalAmount);
					model.addAttribute("totalGross", totalAmount);
					model.addAttribute("totalNet", "");
				}
				
				
				logger.info("Report result list printin------------>"+mpReportList);
				model.addAttribute("reportResult", mpReportList);
				logger.info("Agent_Code -------> "+mpReportList.get(0).getAgent_code());
				logger.info("Agent_Name------>"+mpReportList.get(0).getAgent_name());
				logger.info("Agent_product_code---->"+mpReportList.get(0).getProduct_code());
				logger.info("AGENT Authcode===>"+mpReportList.get(0).getAuth_code());
				logger.info("Agent Mapy==="+mpReportList.get(0).getTxn_id());
				
			
				
			}
			
			
			
			
			//end:medical report
			// START: ALL TRANSACTION REPORT
			else if ("".equalsIgnoreCase(productType) || productType == null) {
				TransSearchObject transSearchObject_Motor = new TransSearchObject();
				TransSearchObject transSearchObject_TL = new TransSearchObject();
				TransSearchObject transSearchObject_PCCA01 = new TransSearchObject();
				TransSearchObject transSearchObject_MP = new TransSearchObject();
				TransSearchObject transSearchObject_WTC = new TransSearchObject();
				TransSearchObject transSearchObject_HOHH = new TransSearchObject();
				// TransSearchObject transSearchObject_IDS = new TransSearchObject();
				// //Pramaiyan
				TransSearchObject transSearchObject_BuddyPA = new TransSearchObject();// BuddyPA
				TransSearchObject transSearchObject_TravEz = new TransSearchObject();// Travel Ez
				
				List<ALLReportVO> allReportList = new ArrayList<ALLReportVO>();
				TransSearchObject transSearchObject_Secure = new TransSearchObject();
				TransSearchObject transSearchObject_TripCare = new TransSearchObject(); // Trip Care

				List<ALLReportVO> motorReportList = new ArrayList<ALLReportVO>();
				List<ALLReportVO> tlReportList = new ArrayList<ALLReportVO>();
				List<ALLReportVO> pccaReportList = new ArrayList<ALLReportVO>();
				List<ALLReportVO> mpReportList = new ArrayList<ALLReportVO>();
				List<ALLReportVO> wtcReportList = new ArrayList<ALLReportVO>();
				List<ALLReportVO> hohhReportList = new ArrayList<ALLReportVO>();
				List<ALLReportVO> idsReportList = new ArrayList<ALLReportVO>();// pramaiyan
				List<ALLReportVO> ptcaReportList = new ArrayList<ALLReportVO>();
				List<ALLReportVO> mptReportList = new ArrayList<ALLReportVO>();
				List<ALLReportVO> buddyPAReportList = new ArrayList<ALLReportVO>();// BuddyPA
				List<ALLReportVO> travEzReportList = new ArrayList<ALLReportVO>();// Travel Ez
				List<String> transactionStatusList = new ArrayList<String>();
				List<ALLReportVO> secureReportList = new ArrayList<ALLReportVO>();
				List<ALLReportVO> tripCareReportList = new ArrayList<ALLReportVO>(); // Trip Care

				if ("EIB".equalsIgnoreCase(productEntity)) {

					transSearchObject_Motor.setProductCode("MI");
					transSearchObject_TL.setProductCode("TL");
					transSearchObject_PCCA01.setProductCode("PCCAO1");
					transSearchObject_MP.setProductCode("MP");
					transSearchObject_WTC.setProductCode("WTC");
					transSearchObject_HOHH.setProductCode("HOHH");
					transSearchObject_BuddyPA.setProductCode("CPP");
					transSearchObject_TravEz.setProductCode("BPT");
					transSearchObject_Secure.setProductCode("EZYTL");
					transSearchObject_TripCare.setProductCode("TCI");
				} else if ("ETB".equalsIgnoreCase(productEntity)) {
					transSearchObject_TL.setProductCode("IDS");
					transSearchObject_PCCA01.setProductCode("PTCAO1");
					transSearchObject_MP.setProductCode("MPT");
					transSearchObject_Motor.setProductCode("MT");
					transSearchObject_WTC.setProductCode("TPT");
					transSearchObject_HOHH.setProductCode("HOHH-ETB");
					// Added on 14-11-2017 for IDS by pramaiyan
					transSearchObject_BuddyPA.setProductCode("TPP");
					transSearchObject_TravEz.setProductCode("TZT");
					transSearchObject_Secure.setProductCode("ISCTL");
					transSearchObject_TripCare.setProductCode("TCT");
				}

				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("C");
				} else {
					transactionStatusList.add(status);

				}

				transSearchObject_Motor.setTransactionStatus(transactionStatusList);
				transSearchObject_TL.setTransactionStatus(transactionStatusList);
				transSearchObject_PCCA01.setTransactionStatus(transactionStatusList);
				transSearchObject_MP.setTransactionStatus(transactionStatusList);
				transSearchObject_WTC.setTransactionStatus(transactionStatusList);
				transSearchObject_HOHH.setTransactionStatus(transactionStatusList);
				// pramaiyan
				// Added on 14-11-2017 for IDS
				transSearchObject_Secure.setTransactionStatus(transactionStatusList);
				transSearchObject_BuddyPA.setTransactionStatus(transactionStatusList); // BuddyPA
				transSearchObject_TravEz.setTransactionStatus(transactionStatusList);
				transSearchObject_TripCare.setTransactionStatus(transactionStatusList);

				if (!"".equalsIgnoreCase(PolicyCertificateNo)) {

					transSearchObject_Motor.setPolicyNo(PolicyCertificateNo);
					transSearchObject_TL.setPolicyNo(PolicyCertificateNo);
					transSearchObject_PCCA01.setPolicyNo(PolicyCertificateNo);
					transSearchObject_MP.setPolicyNo(PolicyCertificateNo);
					transSearchObject_WTC.setPolicyNo(PolicyCertificateNo);
					transSearchObject_HOHH.setPolicyNo(PolicyCertificateNo);
					transSearchObject_BuddyPA.setPolicyNo(PolicyCertificateNo);
					transSearchObject_TravEz.setPolicyNo(PolicyCertificateNo);
					transSearchObject_Secure.setPolicyNo(PolicyCertificateNo);
					transSearchObject_TripCare.setPolicyNo(PolicyCertificateNo);
				}
				if (!"".equalsIgnoreCase(nRIC)) {
					transSearchObject_Motor.setNricNo(nRIC);
					transSearchObject_TL.setNricNo(nRIC);
					transSearchObject_PCCA01.setNricNo(nRIC);
					transSearchObject_MP.setNricNo(nRIC);
					transSearchObject_WTC.setNricNo(nRIC);
					transSearchObject_HOHH.setNricNo(nRIC);
					transSearchObject_BuddyPA.setNricNo(nRIC);
					transSearchObject_TravEz.setNricNo(nRIC);
					transSearchObject_Secure.setNricNo(nRIC);
					transSearchObject_TripCare.setNricNo(nRIC);
				}
				if (!"".equalsIgnoreCase(receiptNo)) {
					transSearchObject_Motor.setInvoiceNo(receiptNo);
					transSearchObject_TL.setInvoiceNo(receiptNo);
					transSearchObject_PCCA01.setInvoiceNo(receiptNo);
					transSearchObject_MP.setInvoiceNo(receiptNo);
					transSearchObject_WTC.setInvoiceNo(receiptNo);
					transSearchObject_HOHH.setInvoiceNo(receiptNo);
					transSearchObject_BuddyPA.setInvoiceNo(receiptNo);
					transSearchObject_TravEz.setInvoiceNo(receiptNo);
					transSearchObject_Secure.setInvoiceNo(receiptNo);
					transSearchObject_TripCare.setInvoiceNo(receiptNo);
				}

				logger.info("Date From param check : " + dateFrom);
				transSearchObject_Motor.setDateFrom(dateFrom);
				transSearchObject_TL.setDateFrom(dateFrom);
				transSearchObject_PCCA01.setDateFrom(dateFrom);
				transSearchObject_MP.setDateFrom(dateFrom);
				transSearchObject_WTC.setDateFrom(dateFrom);
				transSearchObject_HOHH.setDateFrom(dateFrom);
				transSearchObject_Secure.setDateFrom(dateFrom);// pramaiyan
				transSearchObject_BuddyPA.setDateFrom(dateFrom);// BuddyPA
				transSearchObject_TravEz.setDateFrom(dateFrom);// Travel Ezy
				transSearchObject_TripCare.setDateFrom(dateFrom);// Trip Care

				logger.info("Date To param check : " + dateTo);
				transSearchObject_Motor.setDateTo(dateTo);
				transSearchObject_TL.setDateTo(dateTo);
				transSearchObject_PCCA01.setDateTo(dateTo);
				transSearchObject_MP.setDateTo(dateTo);
				transSearchObject_WTC.setDateTo(dateTo);
				transSearchObject_HOHH.setDateTo(dateTo);
				transSearchObject_Secure.setDateTo(dateTo);// pramaiyan
				transSearchObject_BuddyPA.setDateTo(dateTo);// BuddyPA
				transSearchObject_TravEz.setDateTo(dateTo); // Travel Ezy
				transSearchObject_TripCare.setDateTo(dateTo);// Trip Care
				// Added on 14-11-2017 for IDS

				motorReportList = allReportMapper.selectMotorTransactionalReportAfterPayment(transSearchObject_Motor);

				if (motorReportList.size() > 0) {
					amt1 = motorReportList.get(0).getTotal_amount();
				}

				// if("EIB".equalsIgnoreCase(productEntity)){
				tlReportList = allReportMapper.selectTLTransactionalReportAfterPayment(transSearchObject_TL);
				logger.info(tlReportList.size() + "tlReportList");
				if (tlReportList.size() > 0) {
					amt2 = tlReportList.get(0).getTotal_amount();
				}

				// }
				wtcReportList = allReportMapper.selectWTCTransactionalReportAfterPayment(transSearchObject_WTC);
				if (wtcReportList.size() > 0) {
					amt3 = wtcReportList.get(0).getTotal_amount();

				}

				hohhReportList = allReportMapper.selectHOHHTransactionalReportAfterPayment(transSearchObject_HOHH);
				// Start Added by Pramaiyan Added on 14-11-2017 for IDS
				secureReportList = allReportMapper
						.selectSECURETransactionalReportAfterPayment(transSearchObject_Secure);
				logger.info(secureReportList.size() + "secureReportList");
				if (secureReportList.size() > 0) {
					amt4 = secureReportList.get(0).getTotal_amount();
					logger.info("idsReportList>>>>>>>>" + secureReportList.size());

				}
				// End by pramaiyan

				if (hohhReportList.size() > 0) {
					amt4 = hohhReportList.get(0).getTotal_amount();

				}
				// Buddy PA
				buddyPAReportList = allReportMapper
						.selectBuddyPATransactionalReportAfterPayment(transSearchObject_BuddyPA);
				if (buddyPAReportList.size() > 0) {
					amt5 = buddyPAReportList.get(0).getTotal_amount();
				}
				// end Buddy PA
				// Travel Ezy
				travEzReportList = allReportMapper
						.selectTravelEzyTransactionalReportAfterPayment(transSearchObject_TravEz);
				if (travEzReportList.size() > 0) {
					amt6 = travEzReportList.get(0).getTotal_amount();
				}
				// end Travel Ezy

				// Trip care
				tripCareReportList = allReportMapper
						.selectTCTransactionalReportAfterPayment(transSearchObject_TripCare);
				logger.info("tripCareReportList ::::::::::::::::::" + tripCareReportList.size());
				if (tripCareReportList.size() > 0) {
					amt7 = tripCareReportList.get(0).getTotal_amount();

				}
				
				pccaReportList = allReportMapper.selectPCCA01TransactionalReportAfterPayment(transSearchObject_PCCA01);
				logger.info(pccaReportList.size() + "pccaReportList");
				if (pccaReportList.size() > 0) {
					amt8 = pccaReportList.get(0).getTotal_amount();
				}
				
				mpReportList = allReportMapper.selectMPTransactionalReportAfterPayment(transSearchObject_MP);
				logger.info(mpReportList.size() + "mpReportList");
				if (mpReportList.size() > 0) {
					amt9 = mpReportList.get(0).getTotal_amount();
				}
				
				// End Trip Care
				allReportList.addAll(motorReportList);
				// if("EIB".equalsIgnoreCase(productEntity)){
				allReportList.addAll(tlReportList);
				// }
				// Added on 14-11-2017 for IDS
				allReportList.addAll(idsReportList);
				allReportList.addAll(wtcReportList);
				allReportList.addAll(hohhReportList);
				allReportList.addAll(buddyPAReportList);
				allReportList.addAll(travEzReportList);
				allReportList.addAll(secureReportList);
				allReportList.addAll(tripCareReportList);
				allReportList.addAll(pccaReportList);
				allReportList.addAll(ptcaReportList);
				allReportList.addAll(mpReportList);
				allReportList.addAll(mptReportList);
				
				Collections.sort(allReportList, new Comparator<ALLReportVO>() {
					@Override
					public int compare(ALLReportVO o1, ALLReportVO o2) {
						return o1.getRecord_date().compareTo(o2.getRecord_date());
					}
				});

				Collections.reverse(allReportList);

				if (allReportList.size() <= 0) {
					model.addAttribute("emptySearch", "No Record Found");
					model.addAttribute("totalAmount", "0.00");
					model.addAttribute("totalGross", "0.00");
					model.addAttribute("totalNet", "0.00");

				} else {

					BigDecimal totalAmount = amt1.add(amt2);
					totalAmount = totalAmount.add(amt3);
					totalAmount = totalAmount.add(amt4);
					totalAmount = totalAmount.add(amt5);
					totalAmount = totalAmount.add(amt6);
					totalAmount = totalAmount.add(amt7);
					totalAmount = totalAmount.add(amt8);
					model.addAttribute("totalAmount", totalAmount);
					model.addAttribute("totalGross", totalAmount);
					model.addAttribute("totalNet", "");

				}

				model.addAttribute("reportResult", allReportList);
				logger.info("AGENTCODE"+allReportList.get(0).getAgent_code());
				logger.info("Agentname"+allReportList.get(0).getAgent_name());
				logger.info("aagent product code"+allReportList.get(0).getProduct_code());

			}
			return "admin-report-transaction1";
		}

		// END: ALL TRANSACTION REPORT

		catch (Exception e) {
			//errorMessages.add("Error occured during generating report");
			errorMessages.add("No record found");
			model.addAttribute("errorMessages", errorMessages);
			logger.info("======== Testing All Reports Log ========");
			logger.info("\n" + e.toString() + "\n");
			logger.info("======== Testing All Reports Log ========");
			e.printStackTrace();
			return "admin-report-transaction1";
			// e.printStackTrace();
		}

	}

}
