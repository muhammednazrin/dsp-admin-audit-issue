package com.spring.admin;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.etiqa.DAO.MotorDAO;
import com.etiqa.DAO.MotorDAOImpl;
import com.spring.VO.AgentProfile;
import com.spring.VO.Approval;
import com.spring.VO.ApprovalExample;
import com.spring.VO.ApprovalLog;
import com.spring.VO.ApprovalLogExample;
import com.spring.VO.CommonNVIC;
import com.spring.VO.CommonNVICExample;
import com.spring.VO.CommonTaxType;
import com.spring.VO.DspMiTblOto;
import com.spring.VO.DspTblRangeRules;
import com.spring.VO.DspTblRangeRulesExample;
import com.spring.VO.MIProductApprovalRate;
import com.spring.VO.MIProductInfoApproval;
import com.spring.VO.MITblExcess;
import com.spring.VO.MITblExcessExample;
import com.spring.VO.MITblLoading;
import com.spring.VO.MITblLoadingExample;
import com.spring.VO.MITblParam;
import com.spring.VO.MITblParamExample;
import com.spring.VO.MITblPmntParam;
import com.spring.VO.MITblPmntParamExample;
import com.spring.VO.TblPdfInfo;
import com.spring.VO.TblPdfInfoExample;
import com.spring.mapper.AdminParamMapper;
import com.spring.mapper.AgentDocumentMapper;
import com.spring.mapper.AgentLinkMapper;
import com.spring.mapper.AgentProdMapMapper;
import com.spring.mapper.AgentProfileMapper;
import com.spring.mapper.ApprovalLogMapper;
import com.spring.mapper.ApprovalMapper;
import com.spring.mapper.CommonNVICMapper;
import com.spring.mapper.CommonQQMapper;
import com.spring.mapper.DspTblRangeRulesMapper;
import com.spring.mapper.MITblExcessMapper;
import com.spring.mapper.MITblLoadingMapper;
import com.spring.mapper.MITblParamMapper;
import com.spring.mapper.MITblPmntParamMapper;
import com.spring.mapper.ProductsMapper;
import com.spring.mapper.TblPdfInfoMapper;
import com.spring.service.MotorProductMngService;
import com.spring.uam.utils.ServiceValidationUtils;
import com.spring.utils.Utils;

@Controller
public class MTProdMngController {
	CommonNVICMapper commonNvicMapper;
	DspTblRangeRulesMapper dspTblRangeRulesMapper;
	MITblParamMapper miTblParamMapper;
	MITblPmntParamMapper miTblPmntParamMapper;
	MITblExcessMapper miTblExcessMapper;
	MITblLoadingMapper miTblLoadingMapper;
	TblPdfInfoMapper tblPdfInfoMapper;
	ApprovalLogMapper approvalLogMapper;
	ApprovalMapper approvalMapper;
	AgentProfileMapper agentProfileMapper;
	ProductsMapper productsMapper;
	AgentProdMapMapper agentProdMapMapper;
	CommonQQMapper commonQQMapper;
	AdminParamMapper adminParamMapper;
	AgentDocumentMapper agentDocumentMapper;
	AgentLinkMapper agentLinkMapper;
	// Service
	MotorProductMngService motorProductMngService;

	@Autowired
	public MTProdMngController(CommonNVICMapper commonNvicMapper, DspTblRangeRulesMapper dspTblRangeRulesMapper,
			MITblParamMapper miTblParamMapper, MITblPmntParamMapper miTblPmntParamMapper,
			MITblExcessMapper miTblExcessMapper, MITblLoadingMapper miTblLoadingMapper,
			TblPdfInfoMapper tblPdfInfoMapper, ApprovalLogMapper approvalLogMapper, ApprovalMapper approvalMapper,
			MotorProductMngService motorProductMngService) {
		this.commonNvicMapper = commonNvicMapper;
		this.dspTblRangeRulesMapper = dspTblRangeRulesMapper;
		this.miTblParamMapper = miTblParamMapper;
		this.miTblPmntParamMapper = miTblPmntParamMapper;
		this.miTblExcessMapper = miTblExcessMapper;
		this.miTblLoadingMapper = miTblLoadingMapper;
		this.tblPdfInfoMapper = tblPdfInfoMapper;
		this.approvalLogMapper = approvalLogMapper;
		this.approvalMapper = approvalMapper;
		this.motorProductMngService = motorProductMngService;
	}

	/******************************************************************************************************************************/
	/* Start-NVIC Methods */
	/******************************************************************************************************************************/
	@RequestMapping("/MTNvicList")
	public String MTNvicList(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("NvicList funtion ");
		HttpSession session = request.getSession();
		List<CommonNVIC> mnvicList = new ArrayList<CommonNVIC>();
		CommonNVICExample nvicExample = new CommonNVICExample();
		// CommonNVICExample.Criteria nvic_criteria= nvicExample.createCriteria();
		// nvic_criteria.andNvicCodeEqualTo("EG405G");
		nvicExample.setOrderByClause("mtNVIC.ID desc");

		mnvicList = commonNvicMapper.selectByExample(nvicExample);
		model.addAttribute("mnvicList", mnvicList);

		return "products/motor/mt-prod-mng-nvic-list";

	}

	@RequestMapping(value = "/updateMtNvicList", method = RequestMethod.POST)
	public String updateNvicList(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		String id = null;
		if (request.getParameter("id") != null) {
			id = request.getParameter("id");
		} else {
			id = String.valueOf(request.getAttribute("id"));
		}

		System.out.println(id);

		List<CommonNVIC> mnvicList = new ArrayList<CommonNVIC>();
		CommonNVIC nvicData = new CommonNVIC();
		// CommonNVICExample.Criteria nvic_criteria= nvicExample.createCriteria();
		// nvic_criteria.andIdEqualTo(Integer.valueOf(id));
		// nvicExample.setOrderByClause("mtNVIC.ID desc");
		nvicData = commonNvicMapper.selectByPrimaryKey(Integer.valueOf(id));

		model.addAttribute("nvicData", nvicData);

		return "products/motor/mt-prod-mng-nvic-list-edit";
	}

	@RequestMapping(value = "/updateMtNvicDone", method = RequestMethod.POST)
	public String updateNvicDone(@ModelAttribute(value = "CommonNVIC") CommonNVIC commonNVIC, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String id = request.getParameter("id");
		commonNVIC.setId(Integer.valueOf(id));

		commonNvicMapper.updateByPrimaryKeySelective(commonNVIC);

		model.addAttribute("nvicUpdatedMessage", "Nvic Data Updated Successfully!");

		return MTNvicList(request, response, model);

	}

	/*-------------------------------------------------------Start- Delete NVIC Data----------------------------------------------------------*/
	@RequestMapping(value = "/deleteMtNvicDone", method = RequestMethod.POST)
	public String deleteNvicDone(@ModelAttribute(value = "CommonNVIC") CommonNVIC commonNVIC, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String id = request.getParameter("id");
		commonNVIC.setId(Integer.valueOf(id));
		commonNvicMapper.deleteByPrimaryKey(Integer.valueOf(request.getParameter("id").trim()));

		model.addAttribute("nvicDeletedMessage", "Nvic data Deleted Successfully!");
		return MTNvicList(request, response, model);
	}

	/*-------------------------------------------------------End- Delete NVIC Data----------------------------------------------------------*/

	/******************************************************************************************************************************/
	/* End-NVIC Methods */
	/******************************************************************************************************************************/

	/*-----------------------------------------------------------------------------------------------------------------------------*/

	/******************************************************************************************************************************/
	/* Start-Motor Product Rates Methods */
	/******************************************************************************************************************************/

	/*
	 * Update Motor Product Rates - Display the static Page, This methods currently
	 * not in used
	 */

	@RequestMapping("/MTProductRates")
	public String MTProductRates(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("ProductRates funtion ");

		HttpSession session = request.getSession();

		// Maximum No. of Drivers

		List<MITblParam> driversList = new ArrayList<MITblParam>();
		MITblParamExample dspmiTblParamExample = new MITblParamExample();
		MITblParamExample.Criteria dspMiParam_criteria = dspmiTblParamExample.createCriteria();
		dspMiParam_criteria.andNameEqualTo("MT_no_of_drivers");
		driversList = miTblParamMapper.selectByExample(dspmiTblParamExample);

		// Amount for every 3rd driver or more

		List<MITblParam> driverAmountList = new ArrayList<MITblParam>();
		MITblParamExample dspMiTblParamExample1 = new MITblParamExample();
		MITblParamExample.Criteria dspMiParam_criteria1 = dspMiTblParamExample1.createCriteria();
		dspMiParam_criteria1.andNameEqualTo("MT_driver_amount");
		driverAmountList = miTblParamMapper.selectByExample(dspMiTblParamExample1);

		// 5 seater including driver

		List<MITblParam> driver5seaterList = new ArrayList<MITblParam>();
		MITblParamExample dspMiTblParamExample2 = new MITblParamExample();
		MITblParamExample.Criteria dspMiParam_criteria2 = dspMiTblParamExample2.createCriteria();
		dspMiParam_criteria2.andNameEqualTo("MT_5seater");
		driver5seaterList = miTblParamMapper.selectByExample(dspMiTblParamExample2);

		// 7 seater including driver

		List<MITblParam> driver7seaterList = new ArrayList<MITblParam>();
		MITblParamExample dspMiTblParamExample3 = new MITblParamExample();
		MITblParamExample.Criteria dspTlParam_criteria3 = dspMiTblParamExample3.createCriteria();
		dspTlParam_criteria3.andNameEqualTo("MT_7seater");
		driver7seaterList = miTblParamMapper.selectByExample(dspMiTblParamExample3);

		// Stamp Duty

		List<MITblParam> stampDutyList = new ArrayList<MITblParam>();
		MITblParamExample dspMiTblParamExample4 = new MITblParamExample();
		MITblParamExample.Criteria dspMiParam_criteria4 = dspMiTblParamExample4.createCriteria();
		dspMiParam_criteria4.andNameEqualTo("DPPA_StampDuty");
		stampDutyList = miTblParamMapper.selectByExample(dspMiTblParamExample4);

		// GST

		List<MITblParam> gstValueList = new ArrayList<MITblParam>();
		MITblParamExample dspMiTblParamExample5 = new MITblParamExample();
		MITblParamExample.Criteria dspMiParam_criteria5 = dspMiTblParamExample5.createCriteria();
		dspMiParam_criteria5.andNameEqualTo("MT_GST");
		gstValueList = miTblParamMapper.selectByExample(dspMiTblParamExample5);

		// Direct Discount (Online)

		List<MITblParam> discountValueList = new ArrayList<MITblParam>();
		MITblParamExample dspMiTblParamExample6 = new MITblParamExample();
		MITblParamExample.Criteria dspMiParam_criteria6 = dspMiTblParamExample6.createCriteria();
		dspMiParam_criteria6.andNameEqualTo("MT_Discount");
		discountValueList = miTblParamMapper.selectByExample(dspMiTblParamExample6);

		// CAPS Discount (Online)

		List<MITblParam> capsdiscountValueList = new ArrayList<MITblParam>();
		MITblParamExample dspMiTblParamExample7 = new MITblParamExample();
		MITblParamExample.Criteria dspMiParam_criteria7 = dspMiTblParamExample7.createCriteria();
		dspMiParam_criteria7.andNameEqualTo("MT_CAPS_Discount");
		capsdiscountValueList = miTblParamMapper.selectByExample(dspMiTblParamExample7);

		// CAPS FEE

		List<MITblParam> capsfeeValueList = new ArrayList<MITblParam>();
		MITblParamExample dspMiTblParamExample8 = new MITblParamExample();
		MITblParamExample.Criteria dspMiParam_criteria8 = dspMiTblParamExample8.createCriteria();
		dspMiParam_criteria8.andNameEqualTo("MT_DPPA_FEE");
		capsfeeValueList = miTblParamMapper.selectByExample(dspMiTblParamExample8);

		// Drivers Minimum Age Maximum Age

		List<DspTblRangeRules> driverAgeList = new ArrayList<DspTblRangeRules>();
		DspTblRangeRulesExample dspTblRangeRulesExample1 = new DspTblRangeRulesExample();
		DspTblRangeRulesExample.Criteria dspTlRangeRules_criteria1 = dspTblRangeRulesExample1.createCriteria();
		dspTlRangeRules_criteria1.andRuleCodeEqualTo("DriversAgeLimit");
		dspTlRangeRules_criteria1.andProductCodeEqualTo("MI");
		driverAgeList = dspTblRangeRulesMapper.selectByExample(dspTblRangeRulesExample1);

		// Sum insured Minimum and maximum
		List<DspTblRangeRules> suminsuredList = new ArrayList<DspTblRangeRules>();
		DspTblRangeRulesExample dspTblRangeRulesExample2 = new DspTblRangeRulesExample();
		DspTblRangeRulesExample.Criteria dspTlRangeRules_criteria2 = dspTblRangeRulesExample2.createCriteria();
		dspTlRangeRules_criteria2.andRuleCodeEqualTo("SumInsuredLimit");
		dspTlRangeRules_criteria2.andProductCodeEqualTo("MI");
		suminsuredList = dspTblRangeRulesMapper.selectByExample(dspTblRangeRulesExample2);

		model.addAttribute("driversList", driversList);
		model.addAttribute("driverAmountList", driverAmountList);
		model.addAttribute("driver5seaterList", driver5seaterList);
		model.addAttribute("driver7seaterList", driver7seaterList);
		model.addAttribute("stampDutyList", stampDutyList);
		model.addAttribute("gstValueList", gstValueList);
		model.addAttribute("discountValueList", discountValueList);
		model.addAttribute("capsdiscountValueList", capsdiscountValueList);
		model.addAttribute("driverAgeList", driverAgeList);
		model.addAttribute("suminsuredList", suminsuredList);
		model.addAttribute("capsfeeValueList", capsfeeValueList);
		return "/products/motor/mt-prod-mng-prod-rates";

	}

	/*
	 * Update Motor Product Rates - Display Page, This is where the user can change
	 * the values
	 */
	@RequestMapping("/updateMTProductRates")
	public String UpdateMTProductRates(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("UPDATE MT funtion ");
		HttpSession session = request.getSession();
		List<MITblParam> driversList = new ArrayList<MITblParam>();
		driversList = motorProductMngService.getMotorMultipleParam("MT_no_of_drivers");

		List<MITblParam> driverAmountList = new ArrayList<MITblParam>();
		driverAmountList = motorProductMngService.getMotorMultipleParam("MT_driver_amount");

		List<MITblParam> driver5seaterList = new ArrayList<MITblParam>();
		driver5seaterList = motorProductMngService.getMotorMultipleParam("MT_5seater");

		List<MITblParam> driver7seaterList = new ArrayList<MITblParam>();
		driver7seaterList = motorProductMngService.getMotorMultipleParam("MT_7seater");

		List<MITblParam> stampDutyList = new ArrayList<MITblParam>();
		stampDutyList = motorProductMngService.getMotorMultipleParam("DPPA_StampDuty");

		// GST
		List<MITblParam> gstValueList = new ArrayList<MITblParam>();
		gstValueList = motorProductMngService.getMotorMultipleParam("MT_GST");

		// Direct Discount (Online)
		List<MITblParam> discountValueList = new ArrayList<MITblParam>();
		discountValueList = motorProductMngService.getMotorMultipleParam("MT_Discount");
		// CAPS Discount (Online)

		List<MITblParam> capsdiscountValueList = new ArrayList<MITblParam>();
		capsdiscountValueList = motorProductMngService.getMotorMultipleParam("MT_CAPS_Discount");

		List<MITblParam> capsfeeValueList = new ArrayList<MITblParam>();
		capsfeeValueList = motorProductMngService.getMotorMultipleParam("MT_DPPA_FEE");

		// Drivers Minimum Age Maximum Age
		List<DspTblRangeRules> driverAgeList = motorProductMngService.getMotorRangeRuleParam("MI", "DriversAgeLimit");

		// Sum insured Minimum and maximum
		List<DspTblRangeRules> suminsuredList = motorProductMngService.getMotorRangeRuleParam("MI", "SumInsuredLimit");

		CommonTaxType motorSstParam = motorProductMngService.getTaxDetails("MT", "SST");
		CommonTaxType dppaSstParam = motorProductMngService.getTaxDetails("DPPA", "SST");
		CommonTaxType otoSstParam = motorProductMngService.getTaxDetails("OTO", "SST");
		DspMiTblOto otoStampDutyParam = motorProductMngService.getOTOStampDuty("MT", "Active");
		MITblParam dppaStampDuty = motorProductMngService.getMotorSingleParam("DPPA_StampDuty", 1);

		model.addAttribute("driversList", driversList);
		model.addAttribute("driverAmountList", driverAmountList);
		model.addAttribute("driver5seaterList", driver5seaterList);
		model.addAttribute("driver7seaterList", driver7seaterList);
		model.addAttribute("stampDutyList", stampDutyList);
		model.addAttribute("gstValueList", gstValueList);
		model.addAttribute("discountValueList", discountValueList);
		model.addAttribute("capsdiscountValueList", capsdiscountValueList);
		model.addAttribute("driverAgeList", driverAgeList);
		model.addAttribute("suminsuredList", suminsuredList);
		model.addAttribute("capsfeeValueList", capsfeeValueList);

		model.addAttribute("motorSstParam", motorSstParam);
		model.addAttribute("dppaSstParam", dppaSstParam);
		model.addAttribute("dppaStampDuty", dppaStampDuty);
		model.addAttribute("otoSstParam", otoSstParam);
		model.addAttribute("otoStampDutyParam", otoStampDutyParam);
		return "/products/motor/mt-prod-mng-prod-rates-edit";
	}

	/*
	 * Update Motor Product Rates - Send to Approver, This is where the user will
	 * save the changes
	 */
	// ************************ Start- Send Motor Product Rates to Approver
	// **********************************************
	@RequestMapping("/MTProductRateSave") // When update button click mt-prod-mng-prod-rates-edit.jsp
	public String MIProductRateSave(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("ProductRates funtion ");
		HttpSession session = request.getSession();
		String logedUser = (String) session.getAttribute("logedUser");

		List<MITblParam> driversList = new ArrayList<MITblParam>();
		driversList = motorProductMngService.getMotorMultipleParam("MT_no_of_drivers");

		List<MITblParam> driverAmountList = new ArrayList<MITblParam>();
		driverAmountList = motorProductMngService.getMotorMultipleParam("MT_driver_amount");

		List<MITblParam> driver5seaterList = new ArrayList<MITblParam>();
		driver5seaterList = motorProductMngService.getMotorMultipleParam("MT_5seater");

		List<MITblParam> driver7seaterList = new ArrayList<MITblParam>();
		driver7seaterList = motorProductMngService.getMotorMultipleParam("MT_7seater");

		List<MITblParam> stampDutyList = new ArrayList<MITblParam>();
		stampDutyList = motorProductMngService.getMotorMultipleParam("DPPA_StampDuty");

		// GST
		List<MITblParam> gstValueList = new ArrayList<MITblParam>();
		gstValueList = motorProductMngService.getMotorMultipleParam("MT_GST");

		// Direct Discount (Online)
		List<MITblParam> discountValueList = new ArrayList<MITblParam>();
		discountValueList = motorProductMngService.getMotorMultipleParam("MT_Discount");
		// CAPS Discount (Online)

		List<MITblParam> capsdiscountValueList = new ArrayList<MITblParam>();
		capsdiscountValueList = motorProductMngService.getMotorMultipleParam("MT_CAPS_Discount");

		List<MITblParam> capsfeeValueList = new ArrayList<MITblParam>();
		capsfeeValueList = motorProductMngService.getMotorMultipleParam("MT_DPPA_FEE");

		// Drivers Minimum Age Maximum Age
		List<DspTblRangeRules> driverAgeList = motorProductMngService.getMotorRangeRuleParam("MI", "DriversAgeLimit");

		// Sum insured Minimum and maximum
		List<DspTblRangeRules> suminsuredList = motorProductMngService.getMotorRangeRuleParam("MI", "SumInsuredLimit");

		CommonTaxType motorSstParam = motorProductMngService.getTaxDetails("MT", "SST");
		CommonTaxType dppaSstParam = motorProductMngService.getTaxDetails("DPPA", "SST");
		CommonTaxType otoSstParam = motorProductMngService.getTaxDetails("OTO", "SST");
		DspMiTblOto otoStampDutyParam = motorProductMngService.getOTOStampDuty("MT", "Active");
		MITblParam dppaStampDuty = motorProductMngService.getMotorSingleParam("DPPA_StampDuty", 1);

		MIProductApprovalRate originalMotorRatesValues = new MIProductApprovalRate();
		originalMotorRatesValues.setMaxDrivers(driversList.get(0).getDescription());
		originalMotorRatesValues.setThirdDriver(driverAmountList.get(0).getDescription());
		originalMotorRatesValues.setFiveSeater(driver5seaterList.get(0).getDescription());
		originalMotorRatesValues.setSevenSeater(driver7seaterList.get(0).getDescription());
		// miProductApprovalRate.setStampDuty(((MITblParam)listOriginalData.get(4)).getDescription());
		// miProductApprovalRate.setGst(((MITblParam)listOriginalData.get(5)).getDescription());
		originalMotorRatesValues.setDirectDiscount(discountValueList.get(0).getDescription());
		originalMotorRatesValues.setCapsDiscount(capsdiscountValueList.get(0).getDescription());
		originalMotorRatesValues.setMinAge(driverAgeList.get(0).getRuleCodeMinValue());
		originalMotorRatesValues.setMaxAge(driverAgeList.get(0).getRuleCodeMaxValue());
		originalMotorRatesValues.setSumCoveredMin(suminsuredList.get(0).getRuleCodeMinValue());
		originalMotorRatesValues.setSumCoveredMax(suminsuredList.get(0).getRuleCodeMinValue());
		originalMotorRatesValues.setMotorTakafulSst(motorSstParam.getTaxPercentage());
		originalMotorRatesValues.setMotorTakafulSstEffectiveDate(motorSstParam.getEffectiveDate());
		originalMotorRatesValues.setDppa_sst(dppaSstParam.getTaxPercentage());
		originalMotorRatesValues.setDppa_sst_effective_date(dppaSstParam.getEffectiveDate());
		originalMotorRatesValues.setDppa_stampduty(dppaStampDuty.getDescription());
		originalMotorRatesValues.setOto_sst(otoSstParam.getTaxPercentage());
		originalMotorRatesValues.setOto_sst_effective_date(otoSstParam.getEffectiveDate());
		originalMotorRatesValues.setOto_stampduty(otoStampDutyParam.getStampDuty().toString());

		// getting new change from page
		String noofdrivers = request.getParameter("noofdrivers");
		String driveramt = request.getParameter("driveramt");
		String driver5seater = request.getParameter("5seater");
		String driver7seater = request.getParameter("7seater");
		String stampduty = request.getParameter("stampduty");
		String gst = request.getParameter("gst");
		String discount = request.getParameter("discount");
		String capsdiscount = request.getParameter("capsdiscount");
		String capsfee = request.getParameter("capsfee");
		String sst = request.getParameter("sst");
		String sst_effective_date = request.getParameter("sst_effective_date");
		String dppa_stampduty = request.getParameter("dppa_stampduty");
		String dppa_sst = request.getParameter("dppa_sst");
		String dppa_sst_effective_date = request.getParameter("dppa_sst_effective_date");
		String oto_stampduty = request.getParameter("oto_stampduty");
		String oto_sst = request.getParameter("oto_sst");
		String oto_sst_effective_date = request.getParameter("oto_sst_effective_date");

		Integer minAge = 0;
		if (!ServiceValidationUtils.isEmptyStringTrim(request.getParameter("mindriverage"))) {
			minAge = Integer.valueOf(request.getParameter("mindriverage"));
		}
		Integer maxAge = 0;
		if (!ServiceValidationUtils.isEmptyStringTrim(request.getParameter("maxdriverage"))) {
			maxAge = Integer.valueOf(request.getParameter("maxdriverage"));
		}
		Integer minSumcovered = 0;
		if (!ServiceValidationUtils.isEmptyStringTrim(request.getParameter("minSumcovered"))) {
			minSumcovered = Integer.valueOf(request.getParameter("minSumcovered"));
		}
		Integer maxSumcovered = 0;
		if (request.getParameter("maxSumcovered") != null) {
			maxSumcovered = Integer.valueOf(request.getParameter("maxSumcovered"));
		}

		MIProductApprovalRate updatedMotorRatesValues = new MIProductApprovalRate();
		updatedMotorRatesValues.setMaxDrivers(noofdrivers);
		updatedMotorRatesValues.setThirdDriver(driveramt);
		updatedMotorRatesValues.setFiveSeater(driver5seater);
		updatedMotorRatesValues.setSevenSeater(driver7seater);
		// miProductApprovalRate.setStampDuty(((MITblParam)listOriginalData.get(4)).getDescription());
		// miProductApprovalRate.setGst(((MITblParam)listOriginalData.get(5)).getDescription());
		updatedMotorRatesValues.setDirectDiscount(discount);
		updatedMotorRatesValues.setCapsDiscount(capsdiscount);
		updatedMotorRatesValues.setMinAge(minAge);
		updatedMotorRatesValues.setMaxAge(maxAge);
		updatedMotorRatesValues.setSumCoveredMin(minSumcovered);
		updatedMotorRatesValues.setSumCoveredMax(maxSumcovered);
		updatedMotorRatesValues.setMotorTakafulSst(sst);
		updatedMotorRatesValues.setMotorTakafulSstEffectiveDate(Utils.StringToDate(sst_effective_date, "dd/MM/yyyy"));
		updatedMotorRatesValues.setDppa_sst(dppa_sst);
		updatedMotorRatesValues.setDppa_sst_effective_date(Utils.StringToDate(dppa_sst_effective_date, "dd/MM/yyyy"));
		updatedMotorRatesValues.setDppa_stampduty(dppa_stampduty);
		updatedMotorRatesValues.setOto_sst(oto_sst);
		updatedMotorRatesValues.setOto_sst_effective_date(Utils.StringToDate(oto_sst_effective_date, "dd/MM/yyyy"));
		updatedMotorRatesValues.setOto_stampduty(oto_stampduty);

		// getting Approval Id from DSP_ADM_TBL_APPROVAL based on DESCRIPTION
		ApprovalExample approvalExample = new ApprovalExample();
		com.spring.VO.ApprovalExample.Criteria createCriteria_approvalExample = approvalExample.createCriteria();
		createCriteria_approvalExample.andDescriptionEqualTo("MOTOR TAKAFUL - CHANGE PRODUCT RATE");
		List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

		try {

			byte[] originalData = convertToBytes(originalMotorRatesValues);
			byte[] changeData = convertToBytes(updatedMotorRatesValues);

			BigDecimal applicationTypeId = new BigDecimal(0);
			if (listapproval.size() > 0) {
				applicationTypeId = listapproval.get(0).getId();
			}

			String pfNumber = (String) session.getAttribute("pfnumber");

			ApprovalLog approvalLog = new ApprovalLog();
			approvalLog.setNewContent(changeData);
			approvalLog.setOriginalContent(originalData);
			approvalLog.setMaker(Short.parseShort(pfNumber));
			approvalLog.setStatus("1");
			approvalLog.setCreateDate(new Date());
			approvalLog.setAppType(applicationTypeId.toString());
			int rs = approvalLogMapper.insert(approvalLog);

			if (rs == 1) {
				model.addAttribute("updatemessage", "Updated data successfully");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("updatemessage", "Update data failed");
		}
		String returnURL = MTProductRates(request, response, model);
		return returnURL;

	}
	// *************************************************** End- Send Motor-Product
	// Rates to Approver **********************************************

	/*
	 * Update Motor Product Rates - Send to Approver, This is where the approver see
	 * the Motor Rates old values and new values
	 */
	// ***************************************************Start- Approver-Display
	// Motor Product Rates*********************************************

	@RequestMapping(value = "/approvalMTProduct", method = RequestMethod.GET)
	public String approvalStatus(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Reequest " + request.getParameter("id"));
		ApprovalLogExample approvalexample = new ApprovalLogExample();
		ApprovalLogExample.Criteria criteria = approvalexample.createCriteria();
		Integer testint = new Integer(request.getParameter("id"));
		criteria.andIdEqualTo(testint.shortValue());
		List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
		System.out.println("   list size in approval list  :::   " + listapprovallog.size());
		try {
			if (listapprovallog.size() > 0) {
				Object originalData = convertFromBytes(listapprovallog.get(0).getOriginalContent());
				MIProductApprovalRate originalMotorRatesValues = new MIProductApprovalRate();
				if (originalMotorRatesValues != null) {
					originalMotorRatesValues = (MIProductApprovalRate) originalData;
					model.addAttribute("originalMotorRatesValues", originalMotorRatesValues);
					System.out.println("-----------------------------");
				} // inner if
			} // outer if

			// Change Data
			if (listapprovallog.size() > 0) {
				Object changeData = convertFromBytes(listapprovallog.get(0).getNewContent());
				MIProductApprovalRate updatedMotorRatesValues = new MIProductApprovalRate();
				// objecct convert into AgentProdMap
				if (changeData != null) {
					updatedMotorRatesValues = (MIProductApprovalRate) changeData;
					model.addAttribute("updatedMotorRatesValues", updatedMotorRatesValues);
					System.out.println("-----------------------------");
				}
			}
		} // try end
		catch (Exception e) {
			System.out.println("Exception in Convert Object to Byte" + e);
		}
		return "agent-product-approval";
	}

	// ***************************************************End- Approver- Display
	// Motor Product Rates *************************************************

	/*
	 * Update Motor Product Rates - Approver click on approve, This is where the
	 * approver approve the changes
	 */
	// ***************************************************Start- Approve- Motor
	// Product Rates ********************************************************
	@RequestMapping("/approveMTProductRateChangeData")
	public String approveMTProductRateChangeData(HttpServletRequest request, HttpServletResponse response,
			Model model) {
		String returnURL = "";
		HttpSession session = request.getSession();
		try {
			String aid = request.getParameter("approvalLogId");
			ApprovalLogExample approvalexample = new ApprovalLogExample();
			ApprovalLog alog = new ApprovalLog();
			ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();
			if (!ServiceValidationUtils.isEmptyStringTrim(aid)) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
				// Change Data
				if (listapprovallog.size() > 0) {
					String status = listapprovallog.get(0).getStatus();
					Short alogId = listapprovallog.get(0).getId();
					Object changeData = convertFromBytes(listapprovallog.get(0).getNewContent());
					MIProductApprovalRate updatedMotorRatesValues = new MIProductApprovalRate();
					if (changeData != null) {
						updatedMotorRatesValues = (MIProductApprovalRate) changeData;

						// Maximum No. of Drivers
						MITblParam noofdrivers = new MITblParam();
						noofdrivers.setDescription(updatedMotorRatesValues.getMaxDrivers());
						noofdrivers.setDescription(updatedMotorRatesValues.getMaxDrivers());
						noofdrivers.setDescription1(updatedMotorRatesValues.getMaxDrivers());
						motorProductMngService.updateMotorParam("MT_driver_amount", noofdrivers);

						// 5 seater including driver
						MITblParam driver5seater = new MITblParam();
						driver5seater.setCode(updatedMotorRatesValues.getFiveSeater());
						driver5seater.setDescription(updatedMotorRatesValues.getFiveSeater());
						driver5seater.setDescription1(updatedMotorRatesValues.getFiveSeater());
						motorProductMngService.updateMotorParam("MT_5seater", driver5seater);

						// 7 seater including driver
						MITblParam driver7seater = new MITblParam();
						driver7seater.setCode(updatedMotorRatesValues.getSevenSeater());
						driver7seater.setDescription(updatedMotorRatesValues.getSevenSeater());
						driver7seater.setDescription1(updatedMotorRatesValues.getSevenSeater());
						motorProductMngService.updateMotorParam("MT_7seater", driver7seater);

						// Direct Discount (Online)
						MITblParam discount = new MITblParam();
						discount.setCode(updatedMotorRatesValues.getDirectDiscount());
						discount.setDescription(updatedMotorRatesValues.getDirectDiscount());
						discount.setDescription1(updatedMotorRatesValues.getDirectDiscount());
						motorProductMngService.updateMotorParam("MT_Discount", discount);

						// Drivers Minimum Age Maximum Age
						DspTblRangeRules driversAgeLimit = new DspTblRangeRules();
						driversAgeLimit.setRuleCodeMinValue(updatedMotorRatesValues.getMinAge());
						driversAgeLimit.setRuleCodeMaxValue(updatedMotorRatesValues.getMaxAge());
						motorProductMngService.updateMotorRangeRuleParam("DriversAgeLimit", "MI", driversAgeLimit);

						// Sum insured Minimum and maximum
						DspTblRangeRules sumInsuredLimit = new DspTblRangeRules();
						sumInsuredLimit.setRuleCodeMinValue(updatedMotorRatesValues.getSumCoveredMin());
						sumInsuredLimit.setRuleCodeMaxValue(updatedMotorRatesValues.getSumCoveredMax());
						motorProductMngService.updateMotorRangeRuleParam("SumInsuredLimit", "MI", sumInsuredLimit);

						// Motor SST
						CommonTaxType taxTypeObj = new CommonTaxType();
						taxTypeObj.setTaxPercentage(updatedMotorRatesValues.getMotorTakafulSst());
						taxTypeObj.setEffectiveDate(updatedMotorRatesValues.getMotorTakafulSstEffectiveDate());
						motorProductMngService.updateTaxDetails("MT", "SST", taxTypeObj);
						// DPPA SST
						CommonTaxType dppataxTypeObj = new CommonTaxType();
						dppataxTypeObj.setTaxPercentage(updatedMotorRatesValues.getDppa_sst());
						dppataxTypeObj.setEffectiveDate(updatedMotorRatesValues.getDppa_sst_effective_date());
						motorProductMngService.updateTaxDetails("DPPA", "SST", dppataxTypeObj);
						// OTO360 SST
						CommonTaxType ototaxTypeObj = new CommonTaxType();
						ototaxTypeObj.setTaxPercentage(updatedMotorRatesValues.getOto_sst());
						ototaxTypeObj.setEffectiveDate(updatedMotorRatesValues.getOto_sst_effective_date());
						motorProductMngService.updateTaxDetails("OTO", "SST", ototaxTypeObj);

						MITblParam dppa_StampDuty = new MITblParam();
						dppa_StampDuty.setCode(updatedMotorRatesValues.getDppa_stampduty());
						dppa_StampDuty.setDescription(updatedMotorRatesValues.getDppa_stampduty());
						dppa_StampDuty.setDescription1(updatedMotorRatesValues.getDppa_stampduty());
						motorProductMngService.updateMotorParam("DPPA_StampDuty", dppa_StampDuty);

						DspMiTblOto dspMiTblOto = new DspMiTblOto();
						dspMiTblOto.setStampDuty(new BigDecimal(updatedMotorRatesValues.getOto_stampduty()));
						motorProductMngService.updateOTOStampDuty("MT", "ACTIVE", dspMiTblOto);

						String pfNumber = (String) session.getAttribute("pfnumber");

						// Update status in DSP_AFM_TBL_Approval_Log table
						alog.setId(alogId);
						alog.setStatus("3");
						// alog.setChecker((short) 1); //get from the current login session
						alog.setChecker(Short.parseShort(pfNumber));
						alog.setUpdateDate(new Date());
						int rs = approvalLogMapper.updateByPrimaryKeySelective(alog);

						if (rs == 1) {
							model.addAttribute("approvemessage", "Approved Data Successfully");
						}
						AgentController agentController = new AgentController(agentProfileMapper, productsMapper,
								agentProdMapMapper, commonQQMapper, adminParamMapper, agentDocumentMapper,
								agentLinkMapper, approvalLogMapper, approvalMapper);

						returnURL = agentController.getAgentProductMapApproval(request, response, model);
					}
				}
			}

		} catch (Exception e) {
			System.out.println("Exception In >> MI product rates update done funtion " + e);
			model.addAttribute("approvemessage", "Approve Data Failed");
		}

		return approvalStatus(request, response, model);

	}
	// ***************************************************End- Approve- Motor
	// Product Rates ********************************************************

	/*
	 * Update Motor Product Rates - Approver click on reject, This is where the
	 * approver reject the changes
	 */
	// ***************************************************Start- Reject- Motor
	// Product Rates ********************************************************
	// Reject MT Product Rate
	@RequestMapping(value = "/rejectMTProductRateChangeData", method = RequestMethod.GET)
	public String rejectMIProductInfoChangeData(@ModelAttribute AgentProfile agentProfile, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		String logedUser = (String) session.getAttribute("logedUser");
		String returnURL = "";
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		ApprovalLogExample approvalLogexample = new ApprovalLogExample();
		ApprovalLog alog = new ApprovalLog();
		ApprovalLogExample.Criteria createCriteria = approvalLogexample.createCriteria();
		String aid = request.getParameter("approvalLogId");
		if (null != aid && !aid.isEmpty()) {
			Integer testint = new Integer(aid);
			createCriteria.andIdEqualTo(testint.shortValue());
			List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalLogexample);
			System.out.println("   list size in approval list  :::   " + listapprovallog.size());
			String pfNumber = (String) session.getAttribute("pfnumber");
			Short alogId = listapprovallog.get(0).getId();
			alog.setId(alogId);
			alog.setStatus("2");
			// alog.setChecker((short) 1); //get from the current login session
			alog.setChecker(Short.parseShort(pfNumber));
			alog.setUpdateDate(new Date());
			int rs = approvalLogMapper.updateByPrimaryKeySelective(alog);
			if (rs == 1) {

				model.addAttribute("rejectmessage", "Rejected Data Successfully");
			} else {
				model.addAttribute("rejectmessage", "Reject Data Failed");
			}

			AgentController agentController = new AgentController(agentProfileMapper, productsMapper,
					agentProdMapMapper, commonQQMapper, adminParamMapper, agentDocumentMapper, agentLinkMapper,
					approvalLogMapper, approvalMapper);

			returnURL = agentController.getAgentProductMapApproval(request, response, model);
		}

		return returnURL;
	}

	// ***************************************************End- Reject- Motor Product
	// Rates ********************************************************

	/******************************************************************************************************************************/
	/* End-Motor Product Rates Methods */
	/******************************************************************************************************************************/

	@RequestMapping(value = "/approvalMTProductInfo", method = RequestMethod.GET)
	public String approvalMTProductInfo(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Reequest " + request.getParameter("id"));
		ApprovalLogExample approvalexample = new ApprovalLogExample();
		com.spring.VO.ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();
		Integer testint = new Integer(request.getParameter("id"));
		createCriteria.andIdEqualTo(testint.shortValue());
		List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
		System.out.println("   list size in approval list  :::   " + listapprovallog.size());
		// System.out.println("Testing Original Content
		// "+listapprovallog.get(0).getOriginalContent());

		try {
			// original data
			if (listapprovallog.size() > 0) {
				Object originalData = convertFromBytes(listapprovallog.get(0).getOriginalContent());
				List<Object> listOriginalData = new ArrayList<Object>();
				// objecct convert into AgentProdMap
				if (originalData != null) {
					listOriginalData = (List<Object>) originalData;
					System.out.println("  list agent prod map size ::::::         " + listOriginalData.size());

					MIProductInfoApproval miProductInfoApproval = new MIProductInfoApproval();
					miProductInfoApproval.setAnnualTarget(((MITblParam) listOriginalData.get(1)).getCode());
					miProductInfoApproval.setValidity(((MITblParam) listOriginalData.get(0)).getCode());

					List<MIProductInfoApproval> listOriginalMTProductInfo = new ArrayList<MIProductInfoApproval>();
					listOriginalMTProductInfo.add(miProductInfoApproval);
					model.addAttribute("listOriginalMTProductInfo", listOriginalMTProductInfo);

					System.out.println("-----------------------------");
				} // inner if
			} // outer if

			// Change Data
			if (listapprovallog.size() > 0) {
				Object changeData = convertFromBytes(listapprovallog.get(0).getNewContent());
				List<String> listChangeData = new ArrayList<String>();
				// objecct convert into AgentProdMap
				if (changeData != null) {
					listChangeData = (List<String>) changeData;
					System.out.println("  list agent prod map size ::::::         " + listChangeData.size());

					MIProductInfoApproval miProductInfoApproval = new MIProductInfoApproval();

					miProductInfoApproval.setAnnualTarget(listChangeData.get(1));
					miProductInfoApproval.setValidity(listChangeData.get(0));

					List<MIProductInfoApproval> listChangeMTProductInfo = new ArrayList<MIProductInfoApproval>();

					listChangeMTProductInfo.add(miProductInfoApproval);
					model.addAttribute("listChangeMTProductInfo", listChangeMTProductInfo);
					System.out.println("-----------------------------");
				}
			}
		} // try end
		catch (Exception e) {
			System.out.println("Exception in Convert Object to Byte" + e);
		}

		return "agent-product-approval";
	}

	// This Url Mapping not used because need approval permission before update by
	// pramaiyan
	@RequestMapping("/updateDoneMTProductRates")
	public String updateDoneMTProductRates(@ModelAttribute(value = "DspTblRangeRules") DspTblRangeRules tblRangeRules,
			@ModelAttribute(value = "MITblParam") MITblParam miTblParam, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		System.out.println("MT product rates update done funtion ");

		HttpSession session = request.getSession();

		// Maximum No. of Drivers

		MITblParamExample dspmiTblParamExample = new MITblParamExample();
		MITblParamExample.Criteria dspMiParam_criteria = dspmiTblParamExample.createCriteria();
		dspMiParam_criteria.andNameEqualTo("MT_no_of_drivers");
		String noofdrivers = request.getParameter("noofdrivers");
		System.out.println(noofdrivers + "noofdrivers");
		miTblParam.setCode(noofdrivers == null ? "0" : noofdrivers);
		String formatVal = noofdrivers;
		miTblParam.setDescription(formatVal);
		miTblParam.setDescription1(formatVal);
		int miTblParamUpdate = miTblParamMapper.updateByExampleSelective(miTblParam, dspmiTblParamExample);
		System.out.println(miTblParamUpdate + "noofdrivers");

		// Amount for every 3rd driver or more

		MITblParamExample dspmiTblParamExample1 = new MITblParamExample();
		MITblParamExample.Criteria dspMiParam_criteria1 = dspmiTblParamExample1.createCriteria();
		dspMiParam_criteria1.andNameEqualTo("MT_driver_amount");
		String driveramt = request.getParameter("driveramt");
		System.out.println(driveramt + "driveramt");
		miTblParam.setCode(driveramt == null ? "0" : driveramt);
		String formatVal1 = driveramt;
		miTblParam.setDescription(formatVal1);
		miTblParam.setDescription1(formatVal1);
		int miTblParamUpdate1 = miTblParamMapper.updateByExampleSelective(miTblParam, dspmiTblParamExample1);
		System.out.println(miTblParamUpdate1 + "driveramt");

		// 5 seater including driver

		MITblParamExample dspmiTblParamExample2 = new MITblParamExample();
		MITblParamExample.Criteria dspMiParam_criteria2 = dspmiTblParamExample2.createCriteria();
		dspMiParam_criteria2.andNameEqualTo("MT_5seater");
		String driver5seater = request.getParameter("5seater");
		System.out.println(driver5seater + "5seater");
		miTblParam.setCode(driver5seater == null ? "0" : driver5seater);
		String formatVal2 = driver5seater;
		miTblParam.setDescription(formatVal2);
		miTblParam.setDescription1(formatVal2);
		int miTblParamUpdate2 = miTblParamMapper.updateByExampleSelective(miTblParam, dspmiTblParamExample2);
		System.out.println(miTblParamUpdate2 + " driver5seater");

		// 7 seater including driver

		MITblParamExample dspmiTblParamExample3 = new MITblParamExample();
		MITblParamExample.Criteria dspMiParam_criteria3 = dspmiTblParamExample3.createCriteria();
		dspMiParam_criteria3.andNameEqualTo("MT_7seater");
		String driver7seater = request.getParameter("7seater");
		System.out.println(driver7seater + "7seater");
		miTblParam.setCode(driver7seater == null ? "0" : driver7seater);
		String formatVal3 = driver7seater;
		miTblParam.setDescription(formatVal3);
		miTblParam.setDescription1(formatVal3);
		int miTblParamUpdate3 = miTblParamMapper.updateByExampleSelective(miTblParam, dspmiTblParamExample3);
		System.out.println(miTblParamUpdate3 + " driver7seater");

		// Stamp duty
		MITblParamExample dspmiTblParamExample4 = new MITblParamExample();
		MITblParamExample.Criteria dspMiParam_criteria4 = dspmiTblParamExample4.createCriteria();
		dspMiParam_criteria4.andNameEqualTo("MT_StampDuty");
		String stampduty = request.getParameter("stampduty");
		System.out.println(stampduty + "stampduty");
		miTblParam.setCode(stampduty == null ? "0" : stampduty);
		String formatVal4 = stampduty;
		miTblParam.setDescription(formatVal4);
		miTblParam.setDescription1(formatVal4);
		int miTblParamUpdate4 = miTblParamMapper.updateByExampleSelective(miTblParam, dspmiTblParamExample4);
		System.out.println(miTblParamUpdate4 + " stampduty");

		// GST

		MITblParamExample dspmiTblParamExample5 = new MITblParamExample();
		MITblParamExample.Criteria dspMiParam_criteria5 = dspmiTblParamExample5.createCriteria();
		dspMiParam_criteria5.andNameEqualTo("MT_GST");
		String gst = request.getParameter("gst");
		System.out.println(gst + "gst");
		miTblParam.setCode(gst == null ? "0" : gst);
		String formatVal5 = gst;
		miTblParam.setDescription(formatVal5);
		miTblParam.setDescription1(formatVal5);
		int miTblParamUpdate5 = miTblParamMapper.updateByExampleSelective(miTblParam, dspmiTblParamExample5);
		System.out.println(miTblParamUpdate5 + " gst");

		// Direct Discount (Online)

		MITblParamExample dspmiTblParamExample6 = new MITblParamExample();
		MITblParamExample.Criteria dspMiParam_criteria6 = dspmiTblParamExample6.createCriteria();
		dspMiParam_criteria6.andNameEqualTo("MT_Discount");
		String discount = request.getParameter("discount");
		System.out.println(discount + "discount");
		miTblParam.setCode(discount == null ? "0" : discount);
		String formatVal6 = discount;
		miTblParam.setDescription(formatVal6);
		miTblParam.setDescription1(formatVal6);
		int miTblParamUpdate6 = miTblParamMapper.updateByExampleSelective(miTblParam, dspmiTblParamExample6);
		System.out.println(miTblParamUpdate6 + " discount");

		// CAPS Discount (Online)

		MITblParamExample dspmiTblParamExample7 = new MITblParamExample();
		MITblParamExample.Criteria dspMiParam_criteria7 = dspmiTblParamExample7.createCriteria();
		dspMiParam_criteria7.andNameEqualTo("MT_CAPS_Discount");
		String capsdiscount = request.getParameter("capsdiscount");
		System.out.println(capsdiscount + "capsdiscount");
		miTblParam.setCode(capsdiscount == null ? "0" : capsdiscount);
		String formatVal7 = capsdiscount;
		miTblParam.setDescription(formatVal7);
		miTblParam.setDescription1(formatVal7);
		int miTblParamUpdate7 = miTblParamMapper.updateByExampleSelective(miTblParam, dspmiTblParamExample7);
		System.out.println(miTblParamUpdate7 + " capsdiscount");

		// Caps Fee
		MITblParamExample dspmiTblParamExample8 = new MITblParamExample();
		MITblParamExample.Criteria dspMiParam_criteria8 = dspmiTblParamExample8.createCriteria();
		dspMiParam_criteria8.andNameEqualTo("MT_DPPA_FEE");
		String capsfee = request.getParameter("capsfee");
		System.out.println(capsfee + "capsfee");
		miTblParam.setCode(capsfee == null ? "0" : capsfee);
		String formatVal8 = capsfee;
		miTblParam.setDescription(formatVal8);
		miTblParam.setDescription1(formatVal8);
		int miTblParamUpdate8 = miTblParamMapper.updateByExampleSelective(miTblParam, dspmiTblParamExample8);
		System.out.println(miTblParamUpdate8 + " capsfee");

		// Drivers Minimum Age Maximum Age

		DspTblRangeRulesExample dspTblRangeRulesExample1 = new DspTblRangeRulesExample();
		DspTblRangeRulesExample.Criteria dspTlRangeRules_criteria1 = dspTblRangeRulesExample1.createCriteria();
		dspTlRangeRules_criteria1.andRuleCodeEqualTo("DriversAgeLimit");
		dspTlRangeRules_criteria1.andProductCodeEqualTo("MI");
		String minAge = "0";
		if (request.getParameter("mindriverage") != null) {
			minAge = request.getParameter("mindriverage");
		}
		String maxAge = "0";
		if (request.getParameter("maxdriverage") != null) {
			maxAge = request.getParameter("maxdriverage");
		}
		System.out.println(minAge + " minAgeVal  maxAgeVal " + maxAge);

		tblRangeRules.setRuleCodeMinValue(Integer.valueOf(minAge));
		tblRangeRules.setRuleCodeMaxValue(Integer.valueOf(maxAge));
		int rangeRuleUpdate = dspTblRangeRulesMapper.updateByExampleSelective(tblRangeRules, dspTblRangeRulesExample1);
		System.out.println(rangeRuleUpdate + "Updation of range rules");

		// Sum insured Minimum and maximum

		DspTblRangeRulesExample dspTblRangeRulesExample2 = new DspTblRangeRulesExample();
		DspTblRangeRulesExample.Criteria dspTlRangeRules_criteria2 = dspTblRangeRulesExample2.createCriteria();
		dspTlRangeRules_criteria2.andRuleCodeEqualTo("SumInsuredLimit");
		dspTlRangeRules_criteria2.andProductCodeEqualTo("MI");

		String minLimit = "0";
		if (request.getParameter("minSumcovered") != null) {
			minLimit = request.getParameter("minSumcovered");
		}
		String maxLimit = "0";
		if (request.getParameter("maxSumcovered") != null) {
			maxLimit = request.getParameter("maxSumcovered");
		}
		System.out.println(minLimit + "minSumcovered  maxSumcovered" + maxLimit);
		tblRangeRules.setRuleCodeMinValue(Integer.valueOf(minLimit));
		tblRangeRules.setRuleCodeMaxValue(Integer.valueOf(maxLimit));
		int rangeRuleUpdate1 = dspTblRangeRulesMapper.updateByExampleSelective(tblRangeRules, dspTblRangeRulesExample2);
		System.out.println(rangeRuleUpdate1 + "Updation of range rules");

		model.addAttribute("updatemessage", "update data successfully");
		String returnURL = MTProductRates(request, response, model);
		return returnURL;
	}

	public String numberFormat(String val) {
		Double iVal = null;
		iVal = Double.parseDouble(val == null ? "0" : val);
		DecimalFormat df = new DecimalFormat("0,000");
		System.out.println(df.format(iVal));
		String formatVal = df.format(iVal);
		return formatVal;
	}

	/*-------------------------------------------------------End- Product Rates----------------------------------------------------------*/

	/*-------------------------------------------------------Start- Motor Takaful Payment Method----------------------------------------------------------*/

	@RequestMapping("/MTPaymentMethod")
	public String MTPaymentMethod(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("MiPaymentMethod funtion ");

		HttpSession session = request.getSession();

		// Maybank2U

		List<MITblPmntParam> m2ulist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria = miTblPmntParamExample.createCriteria();
		MiPmntParam_criteria.andPmntGatewayCodeEqualTo("M2U");
		MiPmntParam_criteria.andProductCodeEqualTo("MT");
		MiPmntParam_criteria.andStatusEqualTo(Short.valueOf("1"));
		m2ulist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample);

		// FPX

		List<MITblPmntParam> fpxlist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample1 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria1 = miTblPmntParamExample1.createCriteria();
		MiPmntParam_criteria1.andPmntGatewayCodeEqualTo("FPX");
		MiPmntParam_criteria1.andProductCodeEqualTo("MT");
		MiPmntParam_criteria1.andStatusEqualTo(Short.valueOf("1"));
		fpxlist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample1);
		// EBPG (Visa/Mastercard)

		List<MITblPmntParam> ebpglist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample2 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria2 = miTblPmntParamExample2.createCriteria();
		MiPmntParam_criteria2.andPmntGatewayCodeEqualTo("EBPG");
		MiPmntParam_criteria2.andProductCodeEqualTo("MT");
		MiPmntParam_criteria2.andStatusEqualTo(Short.valueOf("1"));
		ebpglist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample2);

		// AMEX

		List<MITblPmntParam> amexlist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample3 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria3 = miTblPmntParamExample3.createCriteria();
		MiPmntParam_criteria3.andPmntGatewayCodeEqualTo("AMEX");
		MiPmntParam_criteria3.andProductCodeEqualTo("MT");
		MiPmntParam_criteria3.andStatusEqualTo(Short.valueOf("1"));
		amexlist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample3);

		model.addAttribute("m2ulist", m2ulist);
		model.addAttribute("fpxlist", fpxlist);
		model.addAttribute("ebpglist", ebpglist);
		model.addAttribute("amexlist", amexlist);

		return "/products/motor/mt-prod-mng-pmnt-method";

	}

	@RequestMapping("/UpdateMTPaymentMethod")
	public String UpdateMTPaymentMethod(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("UPDATE PaymentMethod funtion ");

		HttpSession session = request.getSession();

		// Maybank2U

		List<MITblPmntParam> m2ulist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria = miTblPmntParamExample.createCriteria();
		MiPmntParam_criteria.andPmntGatewayCodeEqualTo("M2U");
		MiPmntParam_criteria.andProductCodeEqualTo("MT");
		MiPmntParam_criteria.andStatusEqualTo(Short.valueOf("1"));
		m2ulist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample);

		// FPX

		List<MITblPmntParam> fpxlist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample1 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria1 = miTblPmntParamExample1.createCriteria();
		MiPmntParam_criteria1.andPmntGatewayCodeEqualTo("FPX");
		MiPmntParam_criteria1.andProductCodeEqualTo("MT");
		MiPmntParam_criteria1.andStatusEqualTo(Short.valueOf("1"));
		fpxlist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample1);
		// EBPG (Visa/ Mastercard)

		List<MITblPmntParam> ebpglist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample2 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria2 = miTblPmntParamExample2.createCriteria();
		MiPmntParam_criteria2.andPmntGatewayCodeEqualTo("EBPG");
		MiPmntParam_criteria2.andProductCodeEqualTo("MT");
		MiPmntParam_criteria2.andStatusEqualTo(Short.valueOf("1"));
		ebpglist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample2);

		// AMEX

		List<MITblPmntParam> amexlist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample3 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria3 = miTblPmntParamExample3.createCriteria();
		MiPmntParam_criteria3.andPmntGatewayCodeEqualTo("AMEX");
		MiPmntParam_criteria3.andProductCodeEqualTo("MT");
		MiPmntParam_criteria3.andStatusEqualTo(Short.valueOf("1"));
		amexlist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample3);

		model.addAttribute("m2ulist", m2ulist);
		model.addAttribute("fpxlist", fpxlist);
		model.addAttribute("ebpglist", ebpglist);
		model.addAttribute("amexlist", amexlist);

		return "/products/motor/mt-prod-mng-pmnt-method-edit";

	}

	@RequestMapping("/updateDoneMTPaymentMethod")
	public String updateDoneMTPaymentMethod(@ModelAttribute(value = "MITblPmntParam") MITblPmntParam miTblPmntParam,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("MIPaymentMethod update done funtion ");

		HttpSession session = request.getSession();

		// Maybank2U
		MITblPmntParamExample miTblPmntParamExample = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria = miTblPmntParamExample.createCriteria();
		MiPmntParam_criteria.andPmntGatewayCodeEqualTo("M2U");
		MiPmntParam_criteria.andProductCodeEqualTo("MT");
		String m2u = "0";
		if (request.getParameter("m2u") != null) {
			m2u = request.getParameter("m2u");
			if (m2u.equals("on")) {
				m2u = "1";
			}
		}
		miTblPmntParam.setStatus(Short.valueOf(m2u));
		int miTblPmntParamUpdate = miTblPmntParamMapper.updateByExampleSelective(miTblPmntParam, miTblPmntParamExample);
		System.out.println(miTblPmntParamUpdate + "M2U");

		// FPX
		MITblPmntParamExample miTblPmntParamExample1 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria1 = miTblPmntParamExample1.createCriteria();
		MiPmntParam_criteria1.andPmntGatewayCodeEqualTo("FPX");
		MiPmntParam_criteria1.andProductCodeEqualTo("MT");
		String fpx = "0";
		if (request.getParameter("fpx") != null) {
			fpx = request.getParameter("fpx");
			if (fpx.equals("on")) {
				fpx = "1";
			}
		}
		System.out.println(fpx + " fpx");

		miTblPmntParam.setStatus(Short.valueOf(fpx));
		int miTblPmntParamUpdate1 = miTblPmntParamMapper.updateByExampleSelective(miTblPmntParam,
				miTblPmntParamExample1);
		System.out.println(miTblPmntParamUpdate1 + "FPX");

		// EBPG
		MITblPmntParamExample miTblPmntParamExample2 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria2 = miTblPmntParamExample2.createCriteria();
		MiPmntParam_criteria2.andPmntGatewayCodeEqualTo("EBPG");
		MiPmntParam_criteria2.andProductCodeEqualTo("MT");
		String ebpg = "0";
		if (request.getParameter("ebpg") != null) {
			ebpg = request.getParameter("ebpg");
			if (ebpg.equals("on")) {
				ebpg = "1";
			}
		}
		miTblPmntParam.setStatus(Short.valueOf(ebpg));
		int miTblPmntParamUpdate2 = miTblPmntParamMapper.updateByExampleSelective(miTblPmntParam,
				miTblPmntParamExample2);
		System.out.println(miTblPmntParamUpdate2 + "EBPG");

		// AMEX
		MITblPmntParamExample miTblPmntParamExample3 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria3 = miTblPmntParamExample3.createCriteria();
		MiPmntParam_criteria3.andPmntGatewayCodeEqualTo("AMEX");
		MiPmntParam_criteria3.andProductCodeEqualTo("MT");
		String amex = "0";
		if (request.getParameter("amex") != null) {
			amex = request.getParameter("amex");
			if (amex.equals("on")) {
				amex = "1";
			}
		}
		miTblPmntParam.setStatus(Short.valueOf(amex));
		int miTblPmntParamUpdate3 = miTblPmntParamMapper.updateByExampleSelective(miTblPmntParam,
				miTblPmntParamExample3);
		System.out.println(miTblPmntParamUpdate3 + " AMEX");
		model.addAttribute("updatemessage", "update data successfully");
		String returnURL = MTPaymentMethod(request, response, model);

		return returnURL;
	}

	/*-------------------------------------------------------End- Motor Takaful Payment Method----------------------------------------------------------*/

	/*------------------------------------------------------Start- EXCESS----------------------------------------------------------*/

	@RequestMapping("/MTExcessList")
	public String MTExcessList(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("MTExcessList funtion ");
		HttpSession session = request.getSession();
		List<MITblExcess> ExcessList = new ArrayList<MITblExcess>();
		MITblExcessExample tblExcessExample = new MITblExcessExample();
		MITblExcessExample.Criteria miExcess_criteriaExist = tblExcessExample.createCriteria();
		miExcess_criteriaExist.andProductCodeEqualTo("MT");
		tblExcessExample.setOrderByClause("mex.ID desc");
		ExcessList = miTblExcessMapper.selectByExample(tblExcessExample);

		MotorDAO motorDAO = new MotorDAOImpl();

		model.addAttribute("excessList", ExcessList);

		return "products/motor/mt-prod-mng-excess";

	}

	@RequestMapping("/UpdateMTExcess")
	public String UpdateExcess(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("MTExcessList funtion ");
		HttpSession session = request.getSession();
		List<MITblExcess> ExcessList = new ArrayList<MITblExcess>();
		MITblExcessExample tblExcessExample = new MITblExcessExample();

		MITblExcessExample.Criteria miExcess_criteriaExist = tblExcessExample.createCriteria();
		miExcess_criteriaExist.andProductCodeEqualTo("MT");
		tblExcessExample.setOrderByClause("mex.ID desc");
		ExcessList = miTblExcessMapper.selectByExample(tblExcessExample);
		MotorDAO motorDAO = new MotorDAOImpl();

		model.addAttribute("excessList", ExcessList);

		return "products/motor/mt-prod-mng-excess-edit";

	}

	@RequestMapping("/updateDoneMTExcess")
	public String updateDoneMTExcess(@ModelAttribute(value = "MITblExcess") MITblExcess miTblExcess,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("MT Excess done funtion ");

		HttpSession session = request.getSession();

		// Maximum No. of Drivers

		String recCount[] = request.getParameterValues("recCount");
		String id[] = request.getParameterValues("exid");
		String mincc[] = request.getParameterValues("mincc");
		String maxcc[] = request.getParameterValues("maxcc");
		String minsum[] = request.getParameterValues("minsum");
		String maxsum[] = request.getParameterValues("maxsum");
		String totexcess[] = request.getParameterValues("totexcess");
		String totmax[] = request.getParameterValues("totmax");
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		List<MITblExcess> miTblExcessList = new ArrayList<MITblExcess>();
		List<String> selectedProducts = new ArrayList<String>();
		if (recCount != null) {
			selectedProducts = Arrays.asList(recCount);

			for (String counter : selectedProducts) {

				miTblExcess = new MITblExcess();
				;
				miTblExcess.setMincc(mincc[Integer.parseInt(counter) - 1]);
				System.out.println(mincc[Integer.parseInt(counter) - 1] + " mincc");
				miTblExcess.setMaxcc(maxcc[Integer.parseInt(counter) - 1]);
				System.out.println(maxcc[Integer.parseInt(counter) - 1] + " maxcc");
				miTblExcess.setMinSuminsured(minsum[Integer.parseInt(counter) - 1]);
				miTblExcess.setMaxSuminsured(maxsum[Integer.parseInt(counter) - 1]);
				miTblExcess.setTotExcess(totexcess[Integer.parseInt(counter) - 1]);
				miTblExcess.setTotMax(totmax[Integer.parseInt(counter) - 1]);
				miTblExcess.setId(Short.valueOf(id[Integer.parseInt(counter) - 1]));
				miTblExcess.setProductCode("MT");
				miTblExcess.setUpdateDate(timestamp);
				miTblExcessList.add(miTblExcess);
			}
			for (MITblExcess apm : miTblExcessList) {
				MITblExcessExample miTblExcessExample = new MITblExcessExample();
				MITblExcessExample.Criteria miExcess_criteria = miTblExcessExample.createCriteria();

				System.out.println(apm.getId() + " id");
				miExcess_criteria.andIdEqualTo(apm.getId());
				miExcess_criteria.andProductCodeEqualTo("MT");

				miTblExcessMapper.updateByExampleSelective(apm, miTblExcessExample);

			}
		}

		model.addAttribute("updatemessage", "Update data successfully");
		return MTExcessList(request, response, model);

	}

	@RequestMapping("/AddMTExcess")
	public String AddMTExcess(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("AddExcess funtion ");
		HttpSession session = request.getSession();

		return "products/motor/mt-prod-mng-excess-add";

	}

	@RequestMapping(value = "/saveMTExcessDone", method = RequestMethod.POST)
	public String saveMTExcessDone(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		MITblExcess miTblExcess = new MITblExcess();
		MITblExcessExample tblExcessExample = new MITblExcessExample();
		short id = 0;
		// id= miTblExcessMapper.maxidByExample(tblExcessExample);

		Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		// List<CommonNVIC> mnvicList= new ArrayList<CommonNVIC>();
		// miTblExcess.setId(Short.valueOf(id));
		miTblExcess.setMincc(request.getParameter("mincc"));
		miTblExcess.setMaxcc(request.getParameter("maxcc"));
		miTblExcess.setMinSuminsured(request.getParameter("minsuminsured"));
		miTblExcess.setMaxSuminsured(request.getParameter("maxsuminsured"));
		miTblExcess.setTotExcess(request.getParameter("totexcess"));
		miTblExcess.setTotMax(request.getParameter("totmax"));
		miTblExcess.setProductCode("MT");
		miTblExcess.setCreateDate(timestamp);

		miTblExcessMapper.insert(miTblExcess);
		model.addAttribute("SaveMessage", "Excess Data Added Successfully!");

		return MTExcessList(request, response, model);
	}

	@RequestMapping(value = "/deleteMTExcess", method = RequestMethod.POST)
	public String deleteMTExcess(@ModelAttribute(value = "MITblExcess") MITblExcess miTblExcess, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String id = request.getParameter("exid");
		System.out.println(id + " id");
		miTblExcess.setId(Short.valueOf(id));
		miTblExcessMapper.deleteByPrimaryKey(Short.valueOf(id.trim()));

		model.addAttribute("DeletedMessage", "Excess data Deleted Successfully!");
		return MTExcessList(request, response, model);
	}
	/*------------------------------------------------------END- EXCESS----------------------------------------------------------*/
	/*------------------------------------------------------Start- Loading----------------------------------------------------------*/

	@RequestMapping("/MTLoadingList")
	public String MTLoadingList(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("MTLoadingList funtion ");
		HttpSession session = request.getSession();
		List<MITblLoading> LoadingList = new ArrayList<MITblLoading>();
		MITblLoadingExample tblLoadingExample = new MITblLoadingExample();
		MITblLoadingExample.Criteria miLoading_criteriaExist = tblLoadingExample.createCriteria();
		miLoading_criteriaExist.andProductCodeEqualTo("MT");
		tblLoadingExample.setOrderByClause("mld.ID desc");
		LoadingList = miTblLoadingMapper.selectByExample(tblLoadingExample);
		MotorDAO motorDAO = new MotorDAOImpl();

		model.addAttribute("loadingList", LoadingList);

		return "products/motor/mt-prod-mng-loading";

	}

	@RequestMapping("/UpdateMTLoading")
	public String UpdateMTLoading(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("UpdateMTLoading funtion ");
		HttpSession session = request.getSession();
		List<MITblLoading> LoadingList = new ArrayList<MITblLoading>();
		MITblLoadingExample tblLoadingExample = new MITblLoadingExample();

		MITblLoadingExample.Criteria miLoading_criteriaExist = tblLoadingExample.createCriteria();
		miLoading_criteriaExist.andProductCodeEqualTo("MT");
		tblLoadingExample.setOrderByClause("mld.ID desc");
		LoadingList = miTblLoadingMapper.selectByExample(tblLoadingExample);
		// MotorDAO motorDAO =new MotorDAOImpl();

		model.addAttribute("loadingList", LoadingList);

		return "products/motor/mt-prod-mng-loading-edit";

	}

	@RequestMapping("/updateDoneMTLoading")
	public String updateDoneMTLoading(@ModelAttribute(value = "MITblLoading") MITblLoading miTblLoading,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("MT Loading done funtion ");

		HttpSession session = request.getSession();

		// Maximum No. of Drivers

		String recCount[] = request.getParameterValues("recCount");
		String id[] = request.getParameterValues("loadingid");
		String minsuminsured[] = request.getParameterValues("minsuminsured");
		String minvehicleAge[] = request.getParameterValues("minvehicleAge");
		String mindriverAge[] = request.getParameterValues("mindriverAge");
		String maxsuminsured[] = request.getParameterValues("maxsuminsured");
		String maxvehicleAge[] = request.getParameterValues("maxvehicleAge");
		String maxdriverAge[] = request.getParameterValues("maxdriverAge");
		String percentage[] = request.getParameterValues("percentage");

		List<MITblLoading> miTblLoadingList = new ArrayList<MITblLoading>();
		List<String> selectedProducts = new ArrayList<String>();
		if (recCount != null) {
			selectedProducts = Arrays.asList(recCount);
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			for (String counter : selectedProducts) {

				miTblLoading = new MITblLoading();
				;
				miTblLoading.setMinSuminsured(minsuminsured[Integer.parseInt(counter) - 1]);
				System.out.println(minsuminsured[Integer.parseInt(counter) - 1] + " minsuminsured");
				miTblLoading.setMinVehicleAge(minvehicleAge[Integer.parseInt(counter) - 1]);
				System.out.println(minvehicleAge[Integer.parseInt(counter) - 1] + " vehicleAge");
				miTblLoading.setMinDriverAge(mindriverAge[Integer.parseInt(counter) - 1]);
				miTblLoading.setPercentage(percentage[Integer.parseInt(counter) - 1]);
				miTblLoading.setMaxSuminsured(maxsuminsured[Integer.parseInt(counter) - 1]);
				miTblLoading.setMaxVehicleAge(maxvehicleAge[Integer.parseInt(counter) - 1]);
				System.out.println(maxvehicleAge[Integer.parseInt(counter) - 1] + " maxvehicleAge");
				miTblLoading.setMaxDriverAge(maxdriverAge[Integer.parseInt(counter) - 1]);

				miTblLoading.setId(Short.valueOf(id[Integer.parseInt(counter) - 1]));
				miTblLoading.setProductCode("MT");
				miTblLoading.setUpdateDate(timestamp);
				miTblLoadingList.add(miTblLoading);
			}
			for (MITblLoading apm : miTblLoadingList) {
				MITblLoadingExample miTblLoadingExample = new MITblLoadingExample();
				MITblLoadingExample.Criteria miLoading_criteria = miTblLoadingExample.createCriteria();

				System.out.println(apm.getId() + " id");
				miLoading_criteria.andIdEqualTo(apm.getId());
				miLoading_criteria.andProductCodeEqualTo("MT");

				miTblLoadingMapper.updateByExampleSelective(apm, miTblLoadingExample);

			}
		}
		model.addAttribute("updatemessage", "update data successfully");
		return MTLoadingList(request, response, model);

	}

	@RequestMapping("/AddMTLoading")
	public String AddMTLoading(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("AddLoading funtion ");
		HttpSession session = request.getSession();

		return "products/motor/mt-prod-mng-loading-add";

	}

	@RequestMapping(value = "/saveMTLoadingDone", method = RequestMethod.POST)
	public String saveMTLoadingDone(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		MITblLoading miTblLoading = new MITblLoading();
		MITblLoadingExample tblLoadingExample = new MITblLoadingExample();
		short id = 0;
		// id= miTblLoadingMapper.maxidByExample(tblLoadingExample);

		Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		// List<CommonNVIC> mnvicList= new ArrayList<CommonNVIC>();
		// miTblLoading.setId(Short.valueOf(id));
		miTblLoading.setMinSuminsured(request.getParameter("minsuminsured"));
		miTblLoading.setMinVehicleAge(request.getParameter("minvehicleAge"));
		miTblLoading.setMinDriverAge(request.getParameter("mindriverAge"));
		miTblLoading.setPercentage(request.getParameter("percentage"));
		miTblLoading.setMaxSuminsured(request.getParameter("maxsuminsured"));
		miTblLoading.setMaxVehicleAge(request.getParameter("maxvehicleAge"));
		miTblLoading.setMaxDriverAge(request.getParameter("maxdriverAge"));
		miTblLoading.setProductCode("MT");
		miTblLoading.setCreateDate(timestamp);

		miTblLoadingMapper.insert(miTblLoading);
		model.addAttribute("SaveMessage", "Loading Data Added Successfully!");

		return MTLoadingList(request, response, model);
	}

	@RequestMapping(value = "/deleteMTLoading", method = RequestMethod.POST)
	public String deleteMTLoading(@ModelAttribute(value = "MITblLoading") MITblLoading miTblLoading,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String id = request.getParameter("loadingid");
		System.out.println(id + " id");
		miTblLoading.setId(Short.valueOf(id));
		miTblLoadingMapper.deleteByPrimaryKey(Short.valueOf(id.trim()));

		model.addAttribute("DeletedMessage", "Loading data Deleted Successfully!");
		return MTLoadingList(request, response, model);
	}

	/*------------------------------------------------------END -Loading----------------------------------------------------------*/
	/*------------------------------------------------------START- Product Info---------------------------------------------------------*/
	@RequestMapping("/MTproductinfo")
	public String MTproductinfo(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("MT Product Info ");

		HttpSession session = request.getSession();

		// Quotaion Validity

		List<MITblParam> quotationValidityList = new ArrayList<MITblParam>();
		MITblParamExample miTblParamExample = new MITblParamExample();
		MITblParamExample.Criteria miParam_criteria = miTblParamExample.createCriteria();
		miParam_criteria.andNameEqualTo("MI_quote_validity");
		quotationValidityList = miTblParamMapper.selectByExample(miTblParamExample);

		// Annual Sales Target

		List<MITblParam> annualSalesAmntList = new ArrayList<MITblParam>();
		MITblParamExample miTblParamExample1 = new MITblParamExample();
		MITblParamExample.Criteria miParam_criteria1 = miTblParamExample1.createCriteria();
		miParam_criteria1.andNameEqualTo("MI_annual_sales_target");
		annualSalesAmntList = miTblParamMapper.selectByExample(miTblParamExample1);

		List<TblPdfInfo> tblPdfInfoList = new ArrayList<TblPdfInfo>();
		TblPdfInfoExample tblPdfInfoExample = new TblPdfInfoExample();

		TblPdfInfoExample.Criteria miPdfinfo_criteriaExist = tblPdfInfoExample.createCriteria();
		miPdfinfo_criteriaExist.andProductCodeEqualTo("MT");
		tblPdfInfoExample.setOrderByClause("pds.ID desc");
		// SalesLeadsExample.Criteria salesLeads_criteria=
		// salesLeadsExample.createCriteria();
		tblPdfInfoList = tblPdfInfoMapper.selectByExample(tblPdfInfoExample);

		model.addAttribute("tblPdfInfoList", tblPdfInfoList);

		model.addAttribute("quotationValidityList", quotationValidityList);
		model.addAttribute("annualSalesAmntList", annualSalesAmntList);

		return "/products/motor/mt-prod-mng-product-info";

	}

	@RequestMapping("/updateMTproductinfo")
	public String updateMTproductinfo(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("Product Info ");

		HttpSession session = request.getSession();

		// Quotaion Validity

		List<MITblParam> quotationValidityList = new ArrayList<MITblParam>();
		MITblParamExample miTblParamExample = new MITblParamExample();
		MITblParamExample.Criteria miParam_criteria = miTblParamExample.createCriteria();
		miParam_criteria.andNameEqualTo("MI_quote_validity");
		quotationValidityList = miTblParamMapper.selectByExample(miTblParamExample);

		// Annual Sales Target

		List<MITblParam> annualSalesAmntList = new ArrayList<MITblParam>();
		MITblParamExample miTblParamExample1 = new MITblParamExample();
		MITblParamExample.Criteria miParam_criteria1 = miTblParamExample1.createCriteria();
		miParam_criteria1.andNameEqualTo("MI_annual_sales_target");
		annualSalesAmntList = miTblParamMapper.selectByExample(miTblParamExample1);

		List<TblPdfInfo> tblPdfInfoList = new ArrayList<TblPdfInfo>();
		TblPdfInfoExample tblPdfInfoExample = new TblPdfInfoExample();
		TblPdfInfoExample.Criteria miPdfinfo_criteriaExist = tblPdfInfoExample.createCriteria();
		miPdfinfo_criteriaExist.andProductCodeEqualTo("MT");
		tblPdfInfoExample.setOrderByClause("pds.ID desc");
		// SalesLeadsExample.Criteria salesLeads_criteria=
		// salesLeadsExample.createCriteria();
		tblPdfInfoList = tblPdfInfoMapper.selectByExample(tblPdfInfoExample);

		model.addAttribute("tblPdfInfoList", tblPdfInfoList);

		model.addAttribute("quotationValidityList", quotationValidityList);
		model.addAttribute("annualSalesAmntList", annualSalesAmntList);

		return "/products/motor/mt-prod-mng-product-info-edit";

	}

	@RequestMapping("/updateDoneMTproductinfo")
	public String updateDoneMTproductinfo(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("Product Info ");

		HttpSession session = request.getSession();

		// Quotaion Validity

		MITblParamExample miTblParamExample = new MITblParamExample();
		MITblParam miTblParam = new MITblParam();
		MITblParamExample.Criteria miParam_criteria = miTblParamExample.createCriteria();
		miParam_criteria.andNameEqualTo("MI_quote_validity");

		String quotVal = request.getParameter("quotVal");
		System.out.println(quotVal + "quotVal");
		miTblParam.setCode(quotVal == null ? "0" : quotVal);
		String formatVal = numberFormat(quotVal);
		miTblParam.setDescription(quotVal + " days");
		miTblParam.setDescription1(quotVal + " days");
		int miTblParamUpdate = miTblParamMapper.updateByExampleSelective(miTblParam, miTblParamExample);
		System.out.println(miTblParamUpdate + "quotVal value");

		// Annual Sales Target

		MITblParamExample miTblParamExample1 = new MITblParamExample();
		MITblParam miTblParam1 = new MITblParam();
		MITblParamExample.Criteria miParam_criteria1 = miTblParamExample1.createCriteria();
		miParam_criteria1.andNameEqualTo("MI_annual_sales_target");

		String salesTargetVal = request.getParameter("salesTargetVal");
		System.out.println(salesTargetVal + "salesTargetVal");
		miTblParam1.setCode(salesTargetVal == null ? "0" : salesTargetVal);
		String formatVal1 = numberFormat(salesTargetVal);
		miTblParam1.setDescription("RM" + formatVal1);
		miTblParam1.setDescription1("RM" + formatVal1);
		int miTblParamUpdate1 = miTblParamMapper.updateByExampleSelective(miTblParam1, miTblParamExample1);
		System.out.println(miTblParamUpdate1 + "salesTargetVal value");

		model.addAttribute("updatemessage", "update data successfully");
		String returnURL = MTproductinfo(request, response, model);
		return returnURL;

	}

	@RequestMapping(value = "/uploadMTPdfFile", method = RequestMethod.POST)
	public String uploadMTPdfFile(@RequestParam("PDF") CommonsMultipartFile file, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		String redirctTO = null, fileName = null, filePath = null;
		String fileNameFormat = new SimpleDateFormat("yyyyMMddHHmm'.pdf'").format(new Date());
		fileName = request.getParameter("miFileName");
		if (!file.isEmpty()) {
			/*
			 * File a = convert(file); a.renameTo(new File("G:\\1702\\" +
			 * fileName+fileNameFormat));
			 */
			try (InputStream in = this.getClass().getClassLoader()
					.getResourceAsStream("com/etiqa/dsp/sales/process/email/documents.properties")) {
				Properties prop = new Properties();
				prop.load(in);

				// ********************** File writing to another locaiton
				// Starts*********************
				filePath = prop.getProperty("TLPdfDestinationPath") + fileName + fileNameFormat;
				InputStream input = new FileInputStream(convert(file));
				FileOutputStream fos = new FileOutputStream(filePath, true);
				int b = 0;
				while ((b = input.read()) != -1) {
					fos.write(b);
				}
				System.out.println("File has been written");
			} catch (Exception e) {
				System.out.println("Could not create file");
			}
			// ********************** File writing to another locaiton
			// Ends*********************

			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}
			TblPdfInfo tblPdfInfo = new TblPdfInfo();

			Timestamp timestamp = new Timestamp(System.currentTimeMillis());

			tblPdfInfo.setProductCode("MT");
			tblPdfInfo.setFileName(fileName);
			tblPdfInfo.setFilePath(filePath);
			double bytes = convert(file).length();
			double kilobytes = bytes / 1024;
			DecimalFormat df = new DecimalFormat("0.00");
			tblPdfInfo.setFileSize(df.format(kilobytes));
			tblPdfInfo.setCreatedBy(loginUser);
			tblPdfInfo.setCreatedDate(timestamp);

			tblPdfInfoMapper.insert(tblPdfInfo);

			System.out.println("insertion Successfully Done");
			model.addAttribute("SaveMessage", "Loading Data Added Successfully!");

			redirctTO = MTproductinfo(request, response, model);
		}
		return redirctTO;
	}

	/*-------------------------------------------------------Start- Delete PDS----------------------------------------------------------*/
	@RequestMapping(value = "/deleteMTPdsDone", method = RequestMethod.POST)
	public String deleteMTPdsDone(@ModelAttribute(value = "tblPdfInfo") TblPdfInfo tblPdfInfo, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String id = request.getParameter("id");
		tblPdfInfo.setId(Integer.valueOf(id));
		tblPdfInfoMapper.deleteByPrimaryKey(Short.valueOf(request.getParameter("id").trim()));

		// ******************** file deleting Starts***************************
		// miFilePath
		String miFilePath = request.getParameter("miFilePath");
		if (miFilePath != null || miFilePath != "") {
			File file = new File(miFilePath);

			if (file.delete()) {
				System.out.println(file.getName() + " is deleted!");
			} else {
				System.out.println("Delete operation is failed.");
			}
		}
		// ******************** file deleting Starts***************************
		System.out.println("Successfully Deleted");
		model.addAttribute("DeletedMessage", "Data deleted Successfully!");
		String redirctTO = updateMTproductinfo(request, response, model);
		return redirctTO;

	}

	/*-------------------------------------------------------End- Delete PDS----------------------------------------------------------*/

	@RequestMapping(value = "/MTDownloadPdfFile", method = RequestMethod.POST)
	// @RequestMapping("/DownloadMTPdfFile")
	public void MTDownloadPdfFile(HttpServletRequest request, HttpServletResponse response, Model model) {

		String miFilePath = request.getParameter("mtFilePathDownload");
		String FileName = request.getParameter("mtFileName");
		if (miFilePath != null || miFilePath != "") {
			File pdfFile = new File(miFilePath);

			response.setContentType("application/pdf");
			response.addHeader("Content-Disposition", "attachment; filename=" + FileName + ".pdf");
			response.setContentLength((int) pdfFile.length());

			FileInputStream fileInputStream;
			try {
				fileInputStream = new FileInputStream(pdfFile);
				OutputStream responseOutputStream = response.getOutputStream();
				int bytes;
				while ((bytes = fileInputStream.read()) != -1) {
					responseOutputStream.write(bytes);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	public File convert(MultipartFile file) {
		File convFile = new File(file.getOriginalFilename());
		FileOutputStream fos = null;
		try {
			convFile.createNewFile();
			fos = new FileOutputStream(convFile);
			fos.write(file.getBytes());
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return convFile;
	}
	/*------------------------------------------------------END- Product Info---------------------------------------------------------*/

	/* Approval Product Info Starts Here */

	@RequestMapping("/updateDoneMTproductinfoApproval")
	public String updateDoneMTproductinfoApproval(HttpServletRequest request, HttpServletResponse response,
			Model model) {

		HttpSession session = request.getSession();
		String logedUser = (String) session.getAttribute("logedUser");

		// Quotaion Validity

		List<MITblParam> quotationValidityList = new ArrayList<MITblParam>();
		MITblParamExample miTblParamExample = new MITblParamExample();
		MITblParamExample.Criteria miParam_criteria = miTblParamExample.createCriteria();
		miParam_criteria.andNameEqualTo("MI_quote_validity");
		quotationValidityList = miTblParamMapper.selectByExample(miTblParamExample);

		// Annual Sales Target

		List<MITblParam> annualSalesAmntList = new ArrayList<MITblParam>();
		MITblParamExample miTblParamExample1 = new MITblParamExample();
		MITblParamExample.Criteria miParam_criteria1 = miTblParamExample1.createCriteria();
		miParam_criteria1.andNameEqualTo("MI_annual_sales_target");
		annualSalesAmntList = miTblParamMapper.selectByExample(miTblParamExample1);

		List<Object> originalList = new ArrayList<Object>();
		originalList.addAll(quotationValidityList);
		originalList.addAll(annualSalesAmntList);

		String quotVal = request.getParameter("quotVal");
		String salesTargetVal = request.getParameter("salesTargetVal");

		List<String> newList = new ArrayList<String>();

		newList.add(quotVal);
		newList.add(salesTargetVal);

		ApprovalExample approvalExample = new ApprovalExample();
		com.spring.VO.ApprovalExample.Criteria createCriteria_approvalExample = approvalExample.createCriteria();
		createCriteria_approvalExample.andDescriptionEqualTo("MOTOR TAKAFUL - CHANGE PRODUCT INFORMATION");
		List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

		try {
			byte[] originalData = convertToBytes(originalList);
			byte[] changeData = convertToBytes(newList);

			System.out.println("ORI Data" + originalData);
			System.out.println("change Data" + changeData);

			BigDecimal approvalProdId = new BigDecimal(0);
			if (listapproval.size() > 0) {
				approvalProdId = listapproval.get(0).getId();
			}

			String pfNumber = (String) session.getAttribute("pfnumber");

			ApprovalLog approvalLog = new ApprovalLog();
			approvalLog.setNewContent(changeData);
			approvalLog.setOriginalContent(originalData);
			// approvalLog.setApprovalId((short)3); //from Approval table this value[7]
			// based on menu in feature
			// approvalLog.setMaker((short)1); // session login value
			approvalLog.setMaker(Short.parseShort(pfNumber));
			approvalLog.setStatus("1");
			approvalLog.setCreateDate(new Date());
			// approvalLog.setUpdateDate(new Date());
			approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
			int rs = approvalLogMapper.insert(approvalLog);

			if (rs == 1) {

				model.addAttribute("updatemessage", "Updated data successfully");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("updatemessage", "Update data failed");
		}

		String returnURL = MTproductinfo(request, response, model);
		return returnURL;

	}

	private byte[] convertToBytes(Object object) throws IOException {
		try (ByteArrayOutputStream bos = new ByteArrayOutputStream(); ObjectOutput out = new ObjectOutputStream(bos)) {
			out.writeObject(object);
			return bos.toByteArray();
		}
	}

	private Object convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
		try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes); ObjectInput in = new ObjectInputStream(bis)) {
			return in.readObject();
		}
	}

	@RequestMapping("/approveMTProductInfoChangeData")
	public String approveMTProductInfoChangeData(HttpServletRequest request, HttpServletResponse response,
			Model model) {
		String returnURL = "";
		System.out.println("MI product rates update done funtion ");
		HttpSession session = request.getSession();
		String logedUser = (String) session.getAttribute("logedUser");

		try {

			System.out.println("Reequest " + request.getParameter("id"));
			String aid = request.getParameter("approvalLogId");
			System.out.println("aid >>>>>" + aid);

			ApprovalLogExample approvalexample = new ApprovalLogExample();
			ApprovalLog alog = new ApprovalLog();
			ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();

			if (null != aid && !aid.isEmpty()) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
				System.out.println("   list size in approval list  :::   " + listapprovallog.size());
				// System.out.println("Testing Original Content
				// "+listapprovallog.get(0).getOriginalContent());

				// Change Data
				if (listapprovallog.size() > 0) {

					String status = listapprovallog.get(0).getStatus();
					Short alogId = listapprovallog.get(0).getId();
					System.out.println("Status  :  " + status);
					System.out.println("Approvallog Id  :  " + alogId);

					Object changeData = convertFromBytes(listapprovallog.get(0).getNewContent());
					List<String> listChangeData = new ArrayList<String>();
					// objecct convert into AgentProdMap
					if (changeData != null) {
						listChangeData = (List<String>) changeData;
						System.out.println("  list agent prod map size ::::::         " + listChangeData.size());
						MIProductInfoApproval miProductInfoApproval = new MIProductInfoApproval();

						miProductInfoApproval.setAnnualTarget(listChangeData.get(1));
						miProductInfoApproval.setValidity(listChangeData.get(0));

						MITblParamExample miTblParamExample = new MITblParamExample();
						MITblParam miTblParam = new MITblParam();
						MITblParamExample.Criteria miParam_criteria = miTblParamExample.createCriteria();
						miParam_criteria.andNameEqualTo("MI_quote_validity");

						String quotVal = miProductInfoApproval.getValidity();
						System.out.println(quotVal + "quotVal");
						miTblParam.setCode(quotVal == null ? "0" : quotVal);
						String formatVal = quotVal;
						miTblParam.setDescription(quotVal + " days");
						miTblParam.setDescription1(quotVal + " days");
						int miTblParamUpdate = miTblParamMapper.updateByExampleSelective(miTblParam, miTblParamExample);
						System.out.println(miTblParamUpdate + "quotVal value");

						// Annual Sales Target

						MITblParamExample miTblParamExample1 = new MITblParamExample();
						MITblParam miTblParam1 = new MITblParam();
						MITblParamExample.Criteria miParam_criteria1 = miTblParamExample1.createCriteria();
						miParam_criteria1.andNameEqualTo("MI_annual_sales_target");

						String salesTargetVal = miProductInfoApproval.getAnnualTarget();
						System.out.println(salesTargetVal + "salesTargetVal");
						miTblParam1.setCode(salesTargetVal == null ? "0" : salesTargetVal);
						String formatVal1 = salesTargetVal;
						miTblParam1.setDescription("RM" + formatVal1);
						miTblParam1.setDescription1("RM" + formatVal1);
						int miTblParamUpdate1 = miTblParamMapper.updateByExampleSelective(miTblParam1,
								miTblParamExample1);
						System.out.println(miTblParamUpdate1 + "salesTargetVal value");

						String pfNumber = (String) session.getAttribute("pfnumber");
						// Update status in DSP_AFM_TBL_Approval_Log table
						alog.setId(alogId);
						alog.setStatus("3");
						alog.setChecker(Short.parseShort(pfNumber));
						alog.setUpdateDate(new Date());
						int rs = approvalLogMapper.updateByPrimaryKeySelective(alog);

						if (rs == 1) {

							model.addAttribute("approvemessage", "Approved Data Successfully");
						}

						AgentController agentController = new AgentController(agentProfileMapper, productsMapper,
								agentProdMapMapper, commonQQMapper, adminParamMapper, agentDocumentMapper,
								agentLinkMapper, approvalLogMapper, approvalMapper);

						returnURL = agentController.getAgentProductMapApproval(request, response, model);

					} // end forchangeData
				} // end for listapprovallog
			} // Fot if aid
		} // Try end
		catch (Exception e) {
			System.out.println("Exception In >> MI product rates update done funtion " + e);
			model.addAttribute("approvemessage", "Approved Data Failed");
		}

		return returnURL;

	}

	/* Approval Product Info Ends Here */

	@RequestMapping(value = "/rejectMTProductInfoChangeData", method = RequestMethod.GET)
	public String rejectMTProductInfoChangeData(@ModelAttribute AgentProfile agentProfile, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		String logedUser = (String) session.getAttribute("logedUser");
		String returnURL = "";
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		ApprovalLogExample approvalLogexample = new ApprovalLogExample();
		ApprovalLog alog = new ApprovalLog();
		ApprovalLogExample.Criteria createCriteria = approvalLogexample.createCriteria();
		String aid = request.getParameter("approvalLogId");
		if (null != aid && !aid.isEmpty()) {
			Integer testint = new Integer(aid);
			createCriteria.andIdEqualTo(testint.shortValue());
			List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalLogexample);
			System.out.println("   list size in approval list  :::   " + listapprovallog.size());

			String pfNumber = (String) session.getAttribute("pfnumber");
			Short alogId = listapprovallog.get(0).getId();
			alog.setId(alogId);
			alog.setStatus("2");
			// alog.setChecker((short) 1); //get from the current login session
			alog.setChecker(Short.parseShort(pfNumber));
			alog.setUpdateDate(new Date());
			int rs = approvalLogMapper.updateByPrimaryKeySelective(alog);

			if (rs == 1) {

				model.addAttribute("rejectmessage", "Rejected Data Successfully");
			}

			else {
				model.addAttribute("rejectmessage", "Reject Data Failed");
			}

			AgentController agentController = new AgentController(agentProfileMapper, productsMapper,
					agentProdMapMapper, commonQQMapper, adminParamMapper, agentDocumentMapper, agentLinkMapper,
					approvalLogMapper, approvalMapper);

			returnURL = agentController.getAgentProductMapApproval(request, response, model);

		}

		return returnURL;
	}
}
