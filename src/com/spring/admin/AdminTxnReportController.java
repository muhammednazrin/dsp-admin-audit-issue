package com.spring.admin;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.spring.VO.report.ALLReportVO;
import com.spring.VO.report.AdminTxnReportVO;
import com.spring.VO.report.HOHHReportVO;
import com.spring.VO.report.TLReportVO;
import com.spring.VO.report.TransSearchObject;
import com.spring.VO.report.WTCReportVO;
import com.spring.mapper.CustomerMapper;
import com.spring.mapper.report.ALLReportMapper;
import com.spring.mapper.report.AdminTxnReportMapper;
import com.spring.mapper.report.HOHHReportMapper;
import com.spring.mapper.report.MotorReportMapper;
import com.spring.mapper.report.NewCarTransactionalDetailMapper;
import com.spring.mapper.report.TLReportMapper;
import com.spring.mapper.report.WTCReportMapper;
import com.spring.utils.ServiceValidationUtils;

@Controller
public class AdminTxnReportController {

	MotorReportMapper motorReportMapper;
	TLReportMapper tlReportMapper;
	WTCReportMapper wtcReportMapper;
	HOHHReportMapper hohhReportMapper;
	ALLReportMapper allReportMapper;
	NewCarTransactionalDetailMapper newCarTransactionalDetailMapper;
	CustomerMapper customerMapper;
	AdminTxnReportMapper adminTxnReportMapper;

	@Autowired
	public AdminTxnReportController(MotorReportMapper motorReportMapper, TLReportMapper tlReportMapper,
			WTCReportMapper wtcReportMapper, HOHHReportMapper hohhReportMapper, ALLReportMapper allReportMapper,
			NewCarTransactionalDetailMapper newCarTransactionalDetailMapper, CustomerMapper customerMapper,
			AdminTxnReportMapper adminTxnReportMapper) {
		this.motorReportMapper = motorReportMapper;
		this.tlReportMapper = tlReportMapper;
		this.wtcReportMapper = wtcReportMapper;
		this.hohhReportMapper = hohhReportMapper;
		this.allReportMapper = allReportMapper;
		this.newCarTransactionalDetailMapper = newCarTransactionalDetailMapper;
		this.customerMapper = customerMapper;
		this.adminTxnReportMapper = adminTxnReportMapper;
	}

	@RequestMapping("/getAdminTxnReport")
	public String showTxnReport(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		return "adminTxnReport";

	}

	/*
	 * @RequestMapping("/showTxnReport1") public String
	 * showTranReport(HttpServletRequest request, Model model) {
	 *
	 * HttpSession session = request.getSession(); String loginUser
	 * =(String)session.getAttribute("user"); if(loginUser == null) { String
	 * sessionexpired = "Session Has Been Expired";
	 * model.addAttribute("sessionexpired", sessionexpired); return "admin-login"; }
	 * return "admin-report-transaction";
	 *
	 * }
	 */

	@RequestMapping(value = "/adminTxnReportDone", method = RequestMethod.POST)
	public String searchReportDone(HttpServletRequest request, Model model) {

		DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(Locale.US);
		format.setParseBigDecimal(true);
		BigDecimal amt1 = new BigDecimal(0.00);
		BigDecimal amt2 = new BigDecimal(0.00);
		BigDecimal amt3 = new BigDecimal(0.00);
		BigDecimal amt4 = new BigDecimal(0.00);

		BigDecimal grossAmt1_GST_STAMP_DUTY = new BigDecimal(0.00);
		BigDecimal grossAmt1_PA = new BigDecimal(0.00);
		BigDecimal grossAmt2_GST_STAMP_DUTY = new BigDecimal(0.00);
		BigDecimal grossAmt2_PA = new BigDecimal(0.00);

		BigDecimal netAmt1 = new BigDecimal(0.00);
		BigDecimal netAmt2 = new BigDecimal(0.00);

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String productType = request.getParameter("productType").trim();
		String productEntity = request.getParameter("productEntity").trim();
		String status = request.getParameter("status").trim();
		String PolicyCertificateNo = request.getParameter("policyNo").trim();
		String nRIC = request.getParameter("NRIC").trim();
		String receiptNo = request.getParameter("receiptNo").trim();
		String plateNo = request.getParameter("plateNo").trim();
		String dateFrom = request.getParameter("dateFrom").trim();
		String dateTo = request.getParameter("dateTo").trim();
		List<String> errorMessages = new ArrayList<String>();

		session.setAttribute("ProductType", request.getParameter("productType").trim());
		session.setAttribute("ProductEntity", request.getParameter("productEntity").trim());
		session.setAttribute("Status", request.getParameter("status").trim());
		session.setAttribute("PolicyCertificateNo", request.getParameter("policyNo").trim());
		session.setAttribute("NRIC", request.getParameter("NRIC").trim());
		session.setAttribute("PlateNo", request.getParameter("plateNo").trim());
		session.setAttribute("ReceiptNo", request.getParameter("receiptNo").trim());
		session.setAttribute("dateFrom", request.getParameter("dateFrom").trim());
		session.setAttribute("dateTo", request.getParameter("dateTo").trim());

		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			Date fromDate = null;
			Date toDate = null;
			try {
				fromDate = sdf.parse(dateFrom);

				toDate = sdf.parse(dateTo);

				if (fromDate.compareTo(toDate) > 0) {
					errorMessages.add("Date To Cannot Less Than Date From");
				}

				else {

					try {
						// in milliseconds
						long diff = toDate.getTime() - fromDate.getTime();

						long diffSeconds = diff / 1000 % 60;
						long diffMinutes = diff / (60 * 1000) % 60;
						long diffHours = diff / (60 * 60 * 1000) % 24;
						long diffDays = diff / (24 * 60 * 60 * 1000);

						if (diffDays > 3100) {

							errorMessages.add("The date range cannot exceed than 1 month");

						}

					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (ServiceValidationUtils.isEmptyStringTrim(dateFrom)) {
				errorMessages.add("Date From Cannot Be Empty");
			}

			if (ServiceValidationUtils.isEmptyStringTrim(dateTo)) {
				errorMessages.add("Date To Cannot Be Empty");
			}

			if (ServiceValidationUtils.isEmptyStringTrim(productEntity)) {
				errorMessages.add("Please Select Product Entity");
			}

			else if (ServiceValidationUtils.isEmptyStringTrim(productType) && "O".equalsIgnoreCase(status)) {
				errorMessages.add("Please Select Product Type");
			}

			else if (ServiceValidationUtils.isEmptyStringTrim(productType) && "R".equalsIgnoreCase(status)) {
				errorMessages.add("Please Select Product Type");
			}

			if (errorMessages.size() > 0) {
				model.addAttribute("errorMessages", errorMessages);
				return "adminTxnReport";
			}

			// START: MOTOR TRANSACTION REPORT
			if (productType.indexOf("M") != -1) {
				TransSearchObject transSearchObject_Motor = new TransSearchObject();
				List<AdminTxnReportVO> motorReportList = new ArrayList<AdminTxnReportVO>();
				List<AdminTxnReportVO> motorReportList_B4Pay = new ArrayList<AdminTxnReportVO>();
				List<String> transactionStatusList = new ArrayList<String>();

				String query1 = "n";
				String query2 = "n";

				if ("EIB".equalsIgnoreCase(productEntity) && "".equalsIgnoreCase(productType)) {

					transSearchObject_Motor.setProductCode("MI");
				}

				if ("ETB".equalsIgnoreCase(productEntity) && "".equalsIgnoreCase(productType)) {

					transSearchObject_Motor.setProductCode("MT");
				}
				if ("".equalsIgnoreCase(productType) || productType == null) {
					transSearchObject_Motor.setProductCode("MI,MT");
				}

				if ("MI".equalsIgnoreCase(productType) || "MT".equalsIgnoreCase(productType)) {
					transSearchObject_Motor.setProductCode(productType);

				}
				// if Product is Chosen
				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("C");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_Motor.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus("'S','AP','F','O','R'");
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_Motor.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus(status);
				}

				if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)
						|| "C".equalsIgnoreCase(status)) {
					query1 = "y";
				}
				if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
					query2 = "y";
				}

				if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
					transSearchObject_Motor.setPolicyNo(PolicyCertificateNo);
					query2 = null;
				}
				if (!"".equalsIgnoreCase(nRIC)) {
					transSearchObject_Motor.setNricNo(nRIC);
				}
				if (!"".equalsIgnoreCase(receiptNo)) {
					transSearchObject_Motor.setInvoiceNo(receiptNo);
					query2 = null;
				}
				if (!ServiceValidationUtils.isEmptyStringTrim(plateNo)) {
					transSearchObject_Motor.setVehicleNo(plateNo);
				}

				transSearchObject_Motor.setDateFrom(dateFrom);
				transSearchObject_Motor.setDateTo(dateTo);

				// if query 1
				if ("y".equalsIgnoreCase(query1)) {

					motorReportList = adminTxnReportMapper
							.selectMotorTransactionalReportAfterPayment(transSearchObject_Motor);

					if (motorReportList.size() > 0) {
						System.out.println(
								"adminTxnReportMapper.selectMotorTransactionalReportAfterPayment 11111 calling");
						amt1 = motorReportList.get(0).getTotal_amount();
						grossAmt1_GST_STAMP_DUTY = motorReportList.get(0).getTotal_premium_after_gst_stamp_duty();
						grossAmt1_PA = motorReportList.get(0).getTotal_driver_pa_amount();
						netAmt1 = motorReportList.get(0).getTotal_net_amount();
					}

				} // End if query1 is Executed

				// if query 2
				if ("y".equalsIgnoreCase(query2)) {

					motorReportList_B4Pay = adminTxnReportMapper
							.selectMotorTransactionalReportBeforePayment(transSearchObject_Motor);
					if (motorReportList_B4Pay.size() > 0) {
						System.out.println(
								"adminTxnReportMapper.selectMotorTransactionalReportBeforePayment 22222 calling");
						amt2 = motorReportList_B4Pay.get(0).getTotal_amount();
						grossAmt2_GST_STAMP_DUTY = motorReportList_B4Pay.get(0).getTotal_premium_after_gst_stamp_duty();
						grossAmt2_PA = motorReportList_B4Pay.get(0).getTotal_driver_pa_amount();
						netAmt2 = motorReportList_B4Pay.get(0).getTotal_net_amount();

					}

				}

				motorReportList.addAll(motorReportList_B4Pay);

				Collections.sort(motorReportList, new Comparator<AdminTxnReportVO>() {
					@Override
					public int compare(AdminTxnReportVO o1, AdminTxnReportVO o2) {
						return o1.getRecord_date().compareTo(o2.getRecord_date());
					}
				});

				Collections.reverse(motorReportList);

				if (motorReportList.size() <= 0) {
					model.addAttribute("emptySearch", "No Record Found");
					model.addAttribute("totalAmount", "0.00");
					model.addAttribute("totalGross", "0.00");
					model.addAttribute("totalNet", "0.00");
				}

				else {

					BigDecimal totalAmount = amt1.add(amt2);
					BigDecimal totalGross1 = grossAmt1_GST_STAMP_DUTY.add(grossAmt1_PA);
					BigDecimal totalGross2 = grossAmt2_GST_STAMP_DUTY.add(grossAmt2_PA);
					BigDecimal totalGross = totalGross1.add(totalGross2);
					BigDecimal totalNet = netAmt1.add(netAmt2);

					model.addAttribute("totalAmount", totalAmount);
					model.addAttribute("totalGross", totalGross);
					model.addAttribute("totalNet", totalNet);

				}

				model.addAttribute("reportResult", motorReportList);
				session.setAttribute("adminTxnReportList", motorReportList);

			}

			// END: MOTOR TRANSACTION REPORT

			// START: EZYLIFE/TL TRANSACTION REPORT
			else if (productType.indexOf("TL") != -1) {
				TransSearchObject transSearchObject_TL = new TransSearchObject();
				List<TLReportVO> tlReportList = new ArrayList<TLReportVO>();
				List<TLReportVO> tlReportList_B4Pay = new ArrayList<TLReportVO>();
				List<String> transactionStatusList = new ArrayList<String>();

				String query1 = "n";
				String query2 = "n";
				transSearchObject_TL.setProductCode("TL");

				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("C");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_TL.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus("'S','AP','F','O','R'");
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_TL.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus(status);
				}

				if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)
						|| "C".equalsIgnoreCase(status)) {
					query1 = "y";
				}
				if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
					query2 = "y";
				}

				if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
					transSearchObject_TL.setPolicyNo(PolicyCertificateNo);
					query2 = null;
				}
				if (!"".equalsIgnoreCase(nRIC)) {
					transSearchObject_TL.setNricNo(nRIC);
				}
				if (!"".equalsIgnoreCase(receiptNo)) {
					transSearchObject_TL.setInvoiceNo(receiptNo);
					query2 = null;
				}

				transSearchObject_TL.setDateFrom(dateFrom);
				transSearchObject_TL.setDateTo(dateTo);

				// if query 1
				if ("y".equalsIgnoreCase(query1)) {

					tlReportList = tlReportMapper.selectTLTransactionalReportAfterPayment(transSearchObject_TL);
					if (tlReportList.size() > 0) {
						System.out.println("motorReportMapper.selectTLTransactionalReportAfterPayment 33333 calling");
						amt1 = tlReportList.get(0).getTotal_amount();
					}

				} // End if query1 is Executed

				// if query 2
				if ("y".equalsIgnoreCase(query2)) {

					tlReportList_B4Pay = tlReportMapper.selectTLTransactionalReportBeforePayment(transSearchObject_TL);
					if (tlReportList_B4Pay.size() > 0) {
						System.out.println("motorReportMapper.selectTLTransactionalReportBeforePayment 44444 calling");
						amt2 = tlReportList_B4Pay.get(0).getTotal_amount();
					}
				}

				tlReportList.addAll(tlReportList_B4Pay);

				Collections.sort(tlReportList, new Comparator<TLReportVO>() {
					@Override
					public int compare(TLReportVO o1, TLReportVO o2) {
						return o1.getRecord_date().compareTo(o2.getRecord_date());
					}
				});

				Collections.reverse(tlReportList);

				if (tlReportList.size() <= 0) {
					model.addAttribute("emptySearch", "No Record Found");
					model.addAttribute("totalAmount", "0.00");
					model.addAttribute("totalGross", "0.00");
					model.addAttribute("totalNet", "0.00");
				} else {

					BigDecimal totalAmount = amt1.add(amt2);
					model.addAttribute("totalAmount", totalAmount);
					model.addAttribute("totalGross", totalAmount);
					model.addAttribute("totalNet", "");
				}
				model.addAttribute("reportResult", tlReportList);
			}

			// END: EZYLIFE TRANSACTION REPORT
			// START: WTC TRANSACTION REPORT
			else if (productType.indexOf("WTC") != -1 || productType.indexOf("TPT") != -1) {
				TransSearchObject transSearchObject_WTC = new TransSearchObject();
				List<WTCReportVO> wtcReportList = new ArrayList<WTCReportVO>();
				List<WTCReportVO> wtcReportList_B4Pay = new ArrayList<WTCReportVO>();
				List<String> transactionStatusList = new ArrayList<String>();
				String query1 = "n";
				String query2 = "n";

				transSearchObject_WTC.setProductCode(productType);

				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("C");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_WTC.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus("'S','AP','F','O','R'");
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_WTC.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus(status);
				}

				if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)
						|| "C".equalsIgnoreCase(status)) {
					query1 = "y";
				}
				if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
					query2 = "y";
				}

				if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
					transSearchObject_WTC.setPolicyNo(PolicyCertificateNo);
					query2 = null;
				}
				if (!"".equalsIgnoreCase(nRIC)) {
					transSearchObject_WTC.setNricNo(nRIC);
				}
				if (!"".equalsIgnoreCase(receiptNo)) {
					transSearchObject_WTC.setInvoiceNo(receiptNo);
					query2 = null;
				}

				transSearchObject_WTC.setDateFrom(dateFrom);
				transSearchObject_WTC.setDateTo(dateTo);

				// if query 1
				if ("y".equalsIgnoreCase(query1)) {

					wtcReportList = wtcReportMapper.selectWTCTransactionalReportAfterPayment(transSearchObject_WTC);

					if (wtcReportList.size() > 0) {
						System.out.println("motorReportMapper.selectWTCTransactionalReportAfterPayment 55555 calling");
						amt1 = wtcReportList.get(0).getTotal_amount();
						amt1 = wtcReportList.get(0).getTotal_amount();
						grossAmt1_GST_STAMP_DUTY = wtcReportList.get(0).getTotal_premium_after_gst_stamp_duty();
						netAmt1 = wtcReportList.get(0).getTotal_net_amount();
					}

				} // End if query1 is Executed

				// if query 2
				if ("y".equalsIgnoreCase(query2)) {
					if (wtcReportList_B4Pay.size() > 0) {
						System.out.println("motorReportMapper.selectWTCTransactionalReportBeforePayment 66666 calling");
						wtcReportList_B4Pay = wtcReportMapper
								.selectWTCTransactionalReportBeforePayment(transSearchObject_WTC);
						amt2 = wtcReportList_B4Pay.get(0).getTotal_amount();
						grossAmt2_GST_STAMP_DUTY = wtcReportList_B4Pay.get(0).getTotal_premium_after_gst_stamp_duty();
						netAmt2 = wtcReportList_B4Pay.get(0).getTotal_net_amount();
					}

				}

				wtcReportList.addAll(wtcReportList_B4Pay);

				Collections.sort(wtcReportList, new Comparator<WTCReportVO>() {
					@Override
					public int compare(WTCReportVO o1, WTCReportVO o2) {
						return o1.getRecord_date().compareTo(o2.getRecord_date());
					}
				});

				Collections.reverse(wtcReportList);

				if (wtcReportList.size() <= 0) {

					model.addAttribute("emptySearch", "No Record Found");
					model.addAttribute("totalAmount", "0.00");
					model.addAttribute("totalGross", "0.00");
					model.addAttribute("totalNet", "0.00");
				}

				else {

					BigDecimal totalAmount = amt1.add(amt2);
					BigDecimal totalGross1 = grossAmt1_GST_STAMP_DUTY;
					BigDecimal totalGross2 = grossAmt2_GST_STAMP_DUTY;
					BigDecimal totalGross = totalGross1.add(totalGross2);
					BigDecimal totalNet = netAmt1.add(netAmt2);

					model.addAttribute("totalAmount", totalAmount);
					model.addAttribute("totalGross", totalGross);
					model.addAttribute("totalNet", totalNet);

				}
				model.addAttribute("reportResult", wtcReportList);

			}

			// END: WTC TRANSACTION REPORT

			// START: HOHH TRANSACTION REPORT
			else if (productType.indexOf("HOHH") != -1) {
				TransSearchObject transSearchObject_HOHH = new TransSearchObject();
				List<HOHHReportVO> hohhReportList = new ArrayList<HOHHReportVO>();
				List<HOHHReportVO> hohhReportList_B4Pay = new ArrayList<HOHHReportVO>();
				List<String> transactionStatusList = new ArrayList<String>();

				String query1 = "n";
				String query2 = "n";

				transSearchObject_HOHH.setProductCode(productType);

				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("C");
					transactionStatusList.add("O");
					transactionStatusList.add("R");
					transSearchObject_HOHH.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus("'S','AP','F','O','R'");
					query1 = "y";
					query2 = "y";
				} else {
					transactionStatusList.add(status);
					transSearchObject_HOHH.setTransactionStatus(transactionStatusList);
					// transSearchObject_Motor.setTransactionStatus(status);
				}

				if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)
						|| "C".equalsIgnoreCase(status)) {
					query1 = "y";
				}
				if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
					query2 = "y";
				}

				if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
					transSearchObject_HOHH.setPolicyNo(PolicyCertificateNo);
					query2 = null;
				}
				if (!"".equalsIgnoreCase(nRIC)) {
					transSearchObject_HOHH.setNricNo(nRIC);
				}
				if (!"".equalsIgnoreCase(receiptNo)) {
					transSearchObject_HOHH.setInvoiceNo(receiptNo);
					query2 = null;
				}

				transSearchObject_HOHH.setDateFrom(dateFrom);
				transSearchObject_HOHH.setDateTo(dateTo);

				// if query 1
				if ("y".equalsIgnoreCase(query1)) {
					System.out.println("motorReportMapper.selectHOHHTransactionalReportAfterPayment 77777 calling");
					hohhReportList = hohhReportMapper.selectHOHHTransactionalReportAfterPayment(transSearchObject_HOHH);

					if (hohhReportList.size() > 0) {
						amt1 = hohhReportList.get(0).getTotal_amount();
						grossAmt1_GST_STAMP_DUTY = hohhReportList.get(0).getTotal_premium_after_gst_stamp_duty();
						netAmt1 = hohhReportList.get(0).getTotal_net_amount();
					}

				} // End if query1 is Executed

				// if query 2
				if ("y".equalsIgnoreCase(query2)) {
					System.out.println("motorReportMapper.selectHOHHTransactionalReportBeforePayment 88888 calling");
					hohhReportList_B4Pay = hohhReportMapper
							.selectHOHHTransactionalReportBeforePayment(transSearchObject_HOHH);
					if (hohhReportList_B4Pay.size() > 0) {
						amt2 = hohhReportList_B4Pay.get(0).getTotal_amount();
						grossAmt2_GST_STAMP_DUTY = hohhReportList_B4Pay.get(0).getTotal_premium_after_gst_stamp_duty();
						netAmt2 = hohhReportList_B4Pay.get(0).getTotal_net_amount();
					}
				}

				hohhReportList.addAll(hohhReportList_B4Pay);

				Collections.sort(hohhReportList, new Comparator<HOHHReportVO>() {
					@Override
					public int compare(HOHHReportVO o1, HOHHReportVO o2) {
						return o1.getRecord_date().compareTo(o2.getRecord_date());
					}
				});

				Collections.reverse(hohhReportList);

				if (hohhReportList.size() <= 0) {
					model.addAttribute("emptySearch", "No Record Found");
					model.addAttribute("totalAmount", "0.00");
					model.addAttribute("totalGross", "0.00");
					model.addAttribute("totalNet", "0.00");
				}

				else {

					BigDecimal totalAmount = amt1.add(amt2);
					BigDecimal totalGross1 = grossAmt1_GST_STAMP_DUTY;
					BigDecimal totalGross2 = grossAmt2_GST_STAMP_DUTY;
					BigDecimal totalGross = totalGross1.add(totalGross2);
					BigDecimal totalNet = netAmt1.add(netAmt2);
					totalGross = totalGross.add(totalNet);

					model.addAttribute("totalAmount", totalAmount);
					model.addAttribute("totalGross", totalGross);
					model.addAttribute("totalNet", totalNet);

				}
				model.addAttribute("reportResult", hohhReportList);

			}

			// END: HOHH TRANSACTION REPORT

			// START: ALL TRANSACTION REPORT
			else if ("".equalsIgnoreCase(productType) || productType == null) {
				TransSearchObject transSearchObject_Motor = new TransSearchObject();
				TransSearchObject transSearchObject_TL = new TransSearchObject();
				TransSearchObject transSearchObject_WTC = new TransSearchObject();
				TransSearchObject transSearchObject_HOHH = new TransSearchObject();

				List<ALLReportVO> allReportList = new ArrayList<ALLReportVO>();

				List<ALLReportVO> motorReportList = new ArrayList<ALLReportVO>();
				List<ALLReportVO> tlReportList = new ArrayList<ALLReportVO>();
				List<ALLReportVO> wtcReportList = new ArrayList<ALLReportVO>();
				List<ALLReportVO> hohhReportList = new ArrayList<ALLReportVO>();
				List<String> transactionStatusList = new ArrayList<String>();

				if ("EIB".equalsIgnoreCase(productEntity)) {

					transSearchObject_Motor.setProductCode("MI");
					transSearchObject_TL.setProductCode("TL");
					transSearchObject_WTC.setProductCode("WTC");
					transSearchObject_HOHH.setProductCode("HOHH");

				} else if ("ETB".equalsIgnoreCase(productEntity)) {

					transSearchObject_Motor.setProductCode("MT");
					transSearchObject_WTC.setProductCode("TPT");
					transSearchObject_HOHH.setProductCode("HOHH-ETB");
				}

				if ("".equalsIgnoreCase(status) || status == null) {
					transactionStatusList.add("S");
					transactionStatusList.add("AP");
					transactionStatusList.add("F");
					transactionStatusList.add("C");
				} else {
					transactionStatusList.add(status);

				}

				transSearchObject_Motor.setTransactionStatus(transactionStatusList);
				transSearchObject_TL.setTransactionStatus(transactionStatusList);
				transSearchObject_WTC.setTransactionStatus(transactionStatusList);
				transSearchObject_HOHH.setTransactionStatus(transactionStatusList);

				if (!"".equalsIgnoreCase(PolicyCertificateNo)) {

					transSearchObject_Motor.setPolicyNo(PolicyCertificateNo);
					transSearchObject_TL.setPolicyNo(PolicyCertificateNo);
					transSearchObject_WTC.setPolicyNo(PolicyCertificateNo);
					transSearchObject_HOHH.setPolicyNo(PolicyCertificateNo);
				}
				if (!"".equalsIgnoreCase(nRIC)) {
					transSearchObject_Motor.setNricNo(nRIC);
					transSearchObject_TL.setNricNo(nRIC);
					transSearchObject_WTC.setNricNo(nRIC);
					transSearchObject_HOHH.setNricNo(nRIC);

				}
				if (!"".equalsIgnoreCase(receiptNo)) {
					transSearchObject_Motor.setInvoiceNo(receiptNo);
					transSearchObject_TL.setInvoiceNo(receiptNo);
					transSearchObject_WTC.setInvoiceNo(receiptNo);
					transSearchObject_HOHH.setInvoiceNo(receiptNo);
				}

				transSearchObject_Motor.setDateFrom(dateFrom);
				transSearchObject_TL.setDateFrom(dateFrom);
				transSearchObject_WTC.setDateFrom(dateFrom);
				transSearchObject_HOHH.setDateFrom(dateFrom);

				transSearchObject_Motor.setDateTo(dateTo);
				transSearchObject_TL.setDateTo(dateTo);
				transSearchObject_WTC.setDateTo(dateTo);
				transSearchObject_HOHH.setDateTo(dateTo);

				System.out.println("motorReportMapper.selectMotorTransactionalReportAfterPayment 99999 calling");
				motorReportList = allReportMapper.selectMotorTransactionalReportAfterPayment(transSearchObject_Motor);

				if (motorReportList.size() > 0) {
					amt1 = motorReportList.get(0).getTotal_amount();
				}

				if ("EIB".equalsIgnoreCase(productEntity)) {
					System.out.println("motorReportMapper.selectTLTransactionalReportAfterPayment 101010 calling");
					tlReportList = allReportMapper.selectTLTransactionalReportAfterPayment(transSearchObject_TL);

					if (tlReportList.size() > 0) {
						amt2 = tlReportList.get(0).getTotal_amount();
					}

				}
				System.out.println("motorReportMapper.selectWTCTransactionalReportAfterPayment 111111 calling");
				wtcReportList = allReportMapper.selectWTCTransactionalReportAfterPayment(transSearchObject_WTC);
				if (wtcReportList.size() > 0) {
					amt3 = wtcReportList.get(0).getTotal_amount();

				}
				System.out.println("motorReportMapper.selectHOHHTransactionalReportAfterPayment 121212 calling");
				hohhReportList = allReportMapper.selectHOHHTransactionalReportAfterPayment(transSearchObject_HOHH);

				if (hohhReportList.size() > 0) {
					amt4 = hohhReportList.get(0).getTotal_amount();

				}

				allReportList.addAll(motorReportList);
				if ("EIB".equalsIgnoreCase(productEntity)) {
					allReportList.addAll(tlReportList);
				}
				allReportList.addAll(wtcReportList);
				allReportList.addAll(hohhReportList);

				Collections.sort(allReportList, new Comparator<ALLReportVO>() {
					@Override
					public int compare(ALLReportVO o1, ALLReportVO o2) {
						return o1.getRecord_date().compareTo(o2.getRecord_date());
					}
				});

				Collections.reverse(allReportList);

				if (allReportList.size() <= 0) {
					model.addAttribute("emptySearch", "No Record Found");
					model.addAttribute("totalAmount", "0.00");
					model.addAttribute("totalGross", "0.00");
					model.addAttribute("totalNet", "0.00");

				} else {

					BigDecimal totalAmount = amt1.add(amt2);
					totalAmount = totalAmount.add(amt3);
					totalAmount = totalAmount.add(amt4);

					model.addAttribute("totalAmount", totalAmount);
					model.addAttribute("totalGross", totalAmount);
					model.addAttribute("totalNet", "");

				}

				model.addAttribute("reportResult", allReportList);

			}
			return "adminTxnReport";
		}

		// END: ALL TRANSACTION REPORT

		catch (Exception e) {
			errorMessages.add("Error occured during generating report");
			model.addAttribute("errorMessages", errorMessages);
			return "adminTxnReport";
			// e.printStackTrace();
		}

	}

}
