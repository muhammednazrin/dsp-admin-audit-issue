package com.spring.admin;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.etiqa.DAO.LeadsAbdDAO;
import com.etiqa.DAO.LeadsAbdDAOImpl;
import com.spring.VO.MtRoadTaxRenewal;
import com.spring.VO.MtRoadTaxRenewalExample;
import com.spring.VO.SalesLead;
import com.spring.mapper.AdminParamMapper;
import com.spring.mapper.MtRoadTaxRenewalMapper;
import com.spring.utils.ServiceValidationUtils;

@Controller
public class LeadsManagementController {
	// SalesLeadsMapper
	MtRoadTaxRenewalMapper mtRoadTaxRenewalMapper;
	AdminParamMapper adminParamMapper;

	@Autowired
	public LeadsManagementController(AdminParamMapper adminParamMapper, MtRoadTaxRenewalMapper mtRoadTaxRenewalMapper) {
		this.adminParamMapper = adminParamMapper;
		this.mtRoadTaxRenewalMapper = mtRoadTaxRenewalMapper;

	}

	@RequestMapping("/salesLeadsReport")
	public String SalesLeadsReport(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("SalesLeadsReport funtion ");

		HttpSession session = request.getSession();
		List<MtRoadTaxRenewal> mtRoadTaxRenewalList = new ArrayList<MtRoadTaxRenewal>();
		MtRoadTaxRenewalExample mtRoadTaxRenewalExample = new MtRoadTaxRenewalExample();
		mtRoadTaxRenewalExample.setOrderByClause("mrtax.ID desc");
		// SalesLeadsExample.Criteria salesLeads_criteria=
		// salesLeadsExample.createCriteria();
		mtRoadTaxRenewalList = mtRoadTaxRenewalMapper.selectByExampleALL();
		model.addAttribute("mtRoadTaxRenewalList", mtRoadTaxRenewalList);

		// request.setAttribute("data", salesLeads);
		return "leadsSalesLeads";

	}

	@RequestMapping("/searchSalesLeads")
	public String searchSalesLeads(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		// fromDate toDate agentCode agentName
		String agentName = null;
		String agentCode = null;
		String fromDate = null;
		String toDate = null;

		session.setAttribute("agentName", request.getParameter("agentName").trim());
		session.setAttribute("agentCode", request.getParameter("agentCode").trim());
		session.setAttribute("fromDate", request.getParameter("fromDate").trim());
		session.setAttribute("toDate", request.getParameter("toDate").trim());

		agentName = (String) session.getAttribute("agentName");
		agentCode = (String) session.getAttribute("agentCode");
		fromDate = (String) session.getAttribute("fromDate");
		toDate = (String) session.getAttribute("toDate");

		SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/yyyy");

		List<MtRoadTaxRenewal> mtRoadTaxRenewalList = new ArrayList<MtRoadTaxRenewal>();

		MtRoadTaxRenewalExample mtRoadTaxRenewalExample = new MtRoadTaxRenewalExample();
		MtRoadTaxRenewalExample.Criteria salesLeads_criteria = mtRoadTaxRenewalExample.createCriteria();
		mtRoadTaxRenewalExample.setOrderByClause("mrtax.ID desc");

		if (!ServiceValidationUtils.isEmptyStringTrim(fromDate) && !ServiceValidationUtils.isEmptyStringTrim(toDate)) {
			Date datefrom = null;
			Date dateto = null;
			System.out.println(fromDate + "1st");
			System.out.println(toDate + "1st");
			try {
				datefrom = format2.parse(fromDate);
				dateto = format2.parse(toDate);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.out.println(datefrom);
			System.out.println(dateto);
			salesLeads_criteria.andCreateDateBetween(datefrom, dateto);
		}

		if (!ServiceValidationUtils.isEmptyStringTrim(agentName)) {
			salesLeads_criteria.andAgentNameLike("%" + agentName + "%");
		}

		if (!ServiceValidationUtils.isEmptyStringTrim(agentCode)) {
			salesLeads_criteria.andAgentCodeLike("%" + agentCode + "%");
		}
		mtRoadTaxRenewalList = mtRoadTaxRenewalMapper.selectByExample(mtRoadTaxRenewalExample);
		System.out.println("HELLO1");
		model.addAttribute("mtRoadTaxRenewalList", mtRoadTaxRenewalList);
		System.out.println("HELLO2");
		return "leadsSalesLeads";
	}

	@RequestMapping(value = "/uploadCSVFile", method = RequestMethod.POST)
	public String uploadFileHandler(@RequestParam("CSV") CommonsMultipartFile file, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		String redirctTO = null;
		if (!file.isEmpty()) {

			try {
				byte[] bytes = file.getBytes();
				String completeData = new String(bytes);

				System.out.println("completeData-------------" + completeData);
				String[] rowscount = completeData.split("\n");
				int countline = rowscount.length;

				for (int i = 1; i < countline; i++) {
					MtRoadTaxRenewal sLP = new MtRoadTaxRenewal();
					String data = rowscount[i];
					System.out.println("rows-------------" + rowscount[i]);
					String[] columns = data.split(",");
					// sLP.setId(Short.valueOf(columns[0]));
					// Date date = (Date)formatter.parse(columns[1]);
					sLP.setCreateDate(dateFormat(columns[1]));
					sLP.setCustomerName(columns[2]);
					sLP.setVehregno(columns[3]);
					sLP.setEmail(columns[4]);
					String expDate = columns[5];
					/*
					 * Date dateExp = formatterDate.parse(expDate); String dateExp1=
					 * formatter1.format(dateExp);
					 * sLP.setRdtaxexpiry(formatterDate1.parse(dateExp1));
					 */

					// sLP.setRdtaxexpiry(dateFormat(columns[5]));
					sLP.setRdtaxexpiry(columns[5]);

					sLP.setPhoneno(columns[6]);
					/*
					 * sLP.setOtherPhoneNo(columns[7]); sLP.setOtherPhoneNo(columns[8]);
					 */
					sLP.setAgentName(columns[8]);
					sLP.setAgentCode(columns[9]);
					System.out.println(
							"Coulmn 1= " + columns[0] + "   Coulmn 4= " + columns[4] + " , Column 5=" + columns[5]);

					// MtRoadTaxRenewalExample MtRoadTaxRenewalExample = new
					// MtRoadTaxRenewalExample ();
					mtRoadTaxRenewalMapper.insert(sLP);

				}
			} catch (Exception e) {
				System.out.println("Error");
				e.printStackTrace();
			}
			redirctTO = SalesLeadsReport(request, response, model);
		}
		return redirctTO;
	}

	public Date dateFormat(String dateVal) {

		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat df1 = new SimpleDateFormat("dd/MM/yyyy");
		Date startDate = null;
		try {
			startDate = df.parse(dateVal);
			String newDateString = df1.format(startDate);
			System.out.println(newDateString);
			startDate = df1.parse(newDateString);
			System.out.println(startDate);
		} catch (ParseException e) {

		}

		return startDate;
	}

	// ************************************** Leads Abandonment
	// *********************************
	/*
	 * leadsAbdStatus leadsAbdAgentCode leadsAbdAgent leadsAbdProduct leadsAbdEntity
	 * leadsAbdToDate leadsAbdFromDate leadsAbdStep
	 */
	// searchAbandonment

	@RequestMapping("/leadsAbandonmentRep")
	private String leadsAbandonmentRep(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		String strLeadsAbdEntity = (String) session.getAttribute("LeadsAbdEntity");

		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		if (strLeadsAbdEntity == null) {
			session.setAttribute("LeadsAbdEntity", "");
		}

		return "leadsAbandonmentRep";
	}

	@RequestMapping("/searchAbandonment")
	private String searchAbandonment(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		LeadsAbdDAO leadsAbdDao = new LeadsAbdDAOImpl();

		SalesLead salesLead = new SalesLead();

		salesLead.setLeadsAbdStep(request.getParameter("leadsAbdStep"));
		System.out.println("###leadsAbdStep :" + request.getParameter("leadsAbdStep"));
		// salesLead.setLeadsAbdEntity(request.getParameter("leadsAbdEntity"));
		salesLead.setLeadsAbdProduct(request.getParameter("leadsAbdProduct"));
		System.out.println("###leadsAbdProduct : " + request.getParameter("leadsAbdProduct"));

		salesLead.setLeadsAbdAgent(request.getParameter("leadsAbdAgent"));
		System.out.println("###leadsAbdAgent : " + request.getParameter("leadsAbdAgent"));

		salesLead.setLeadsAbdAgentCode(request.getParameter("leadsAbdAgentCode"));
		System.out.println("###leadsAbdAgentCode : " + request.getParameter("leadsAbdAgentCode"));

		salesLead.setLeadsAbdStatus(request.getParameter("leadsAbdStatus"));
		System.out.println("###leadsAbdStatus : " + request.getParameter("leadsAbdStatus"));

		salesLead.setLeadsAbdFromDate(request.getParameter("leadsAbdFromDate"));
		System.out.println("###leadsAbdFromDate : " + request.getParameter("leadsAbdFromDate"));

		salesLead.setLeadsAbdToDate(request.getParameter("leadsAbdToDate"));
		System.out.println("###leadsAbdToDate : " + request.getParameter("leadsAbdToDate"));

		List<SalesLead> leadsAbdList = leadsAbdDao.getLeadsAbdSearchRecords(salesLead);

		// request.setAttribute("data", leadsAbdList);
		System.out.println(leadsAbdList);
		for (SalesLead lead : leadsAbdList) {
			System.out.println("testing controller");
			System.out.println(lead.getDSP_QQ_ID() + ":" + lead.getLEADS_EMAIL_ID() + ":" + lead.getPRODUCT_CODE() + ":"
					+ lead.getQQ_STEPS() + ":" + lead.getFULL_NAME() + ":" + lead.getCREATE_DATE() + ":"
					+ lead.getQQ_STATUS());

		}
		/*
		 * List<SalesLead> listScenario1 = new ArrayList<SalesLead>();
		 *
		 * int size = listScenario1.size(); int step1_total = 0; int step2_total= 0; int
		 * step3_total= 0; int step1_lost= 0; int step2_lost= 0; int step3_lost= 0; int
		 * totalSuccess= 0; for (SalesLead salesLeadVo : listScenario1) {
		 *
		 * if (--size == 0) { // Last item. step1_total=salesLeadVo.getStep1_total();
		 * step2_total=salesLeadVo.getStep2_total();
		 * step3_total=salesLeadVo.getStep3_total();
		 * step1_lost=salesLeadVo.getStep1_lost();
		 * step2_lost=salesLeadVo.getStep2_lost();
		 * step3_lost=salesLeadVo.getStep3_lost();
		 * totalSuccess=salesLeadVo.getTotalSuccess(); } }
		 *
		 *
		 * model.addAttribute("step1_total",step1_total);
		 * model.addAttribute("step2_total",step2_total);
		 * model.addAttribute("step3_total",step3_total);
		 * model.addAttribute("step1_lost",step1_lost);
		 * model.addAttribute("step2_lost",step2_lost);
		 * model.addAttribute("step3_lost",step3_lost);
		 * model.addAttribute("totalSuccess",totalSuccess);
		 */
		model.addAttribute("leadsAbdReportList", leadsAbdList);

		System.out.println("## leadsAbdEntity :" + request.getParameter("leadsAbdEntity").trim());

		session.setAttribute("ProductType", request.getParameter("leadsAbdProduct").trim());

		if (request.getParameter("leadsAbdEntity").equals("") || request.getParameter("leadsAbdEntity").equals(null)) {
			session.setAttribute("LeadsAbdEntity", "");

		} else if (request.getParameter("leadsAbdEntity").equals("EIB")) {
			session.setAttribute("LeadsAbdEntity", "EIB");

		} else if (request.getParameter("leadsAbdEntity").equals("EIT")) {
			session.setAttribute("LeadsAbdEntity", "EIT");
		} else {
			session.setAttribute("LeadsAbdEntity", "");
		}

		/*
		 * if (request.getParameter("leadsAbdProduct").equals("TL") ||
		 * request.getParameter("leadsAbdProduct").equals("HOHH") ||
		 * request.getParameter("leadsAbdProduct").equals("MI") ||
		 * request.getParameter("leadsAbdProduct").equals("WTC")) {
		 *
		 * session.setAttribute("LeadsAbdEntity", "EIB");
		 * System.out.println("##I AM IN IF CONDITION EIB");
		 *
		 * } else { session.setAttribute("LeadsAbdEntity", "EIT");
		 * System.out.println("##I AM IN ELSE CONDITION EIT"); }
		 */

		session.setAttribute("leadsAbdAgent", request.getParameter("leadsAbdAgent").trim());
		session.setAttribute("leadsAbdAgentCode", request.getParameter("leadsAbdAgentCode").trim());
		session.setAttribute("leadsAbdStatus", request.getParameter("leadsAbdStatus").trim());
		session.setAttribute("leadsAbdStep", request.getParameter("leadsAbdStep").trim());
		session.setAttribute("leadsAbdFromDate", request.getParameter("leadsAbdFromDate").trim());
		session.setAttribute("leadsAbdToDate", request.getParameter("leadsAbdToDate").trim());

		int str_step_1_percentage = 0;
		int str_step_2_percentage = 0;
		int str_step_3_percentage = 0;

		try {

			System.out.println("##Array size: " + leadsAbdList.size());

			System.out.println("##Step 1 Total : " + leadsAbdList.get(leadsAbdList.size() - 1).getStep1_total());
			System.out.println("##Step 1 lost : " + leadsAbdList.get(leadsAbdList.size() - 1).getStep1_lost());

			str_step_1_percentage = leadsAbdList.get(leadsAbdList.size() - 1).getStep1_total()
					/ leadsAbdList.get(leadsAbdList.size() - 1).getStep1_lost() * 100;
			str_step_2_percentage = leadsAbdList.get(leadsAbdList.size() - 1).getStep2_total()
					/ leadsAbdList.get(leadsAbdList.size() - 1).getStep2_lost() * 100;
			str_step_3_percentage = leadsAbdList.get(leadsAbdList.size() - 1).getStep3_total()
					/ leadsAbdList.get(leadsAbdList.size() - 1).getStep3_lost() * 100;

		} catch (Exception ex) {

		}

		session.setAttribute("step_1_percentage", str_step_1_percentage);
		session.setAttribute("step_2_percentage", str_step_2_percentage);
		session.setAttribute("step_3_percentage", str_step_3_percentage);

		System.out.println("##Session set #ProductType : " + request.getParameter("leadsAbdProduct"));
		System.out.println("##Session set #LeadsAbdEntity : " + request.getParameter("leadsAbdEntity"));
		System.out.println("##Session set #leadsAbdAgent : " + request.getParameter("leadsAbdAgent"));
		System.out.println("##Session set #leadsAbdAgentCode : " + request.getParameter("leadsAbdAgentCode"));

		System.out.println("##Session set #leadsAbdStep : " + request.getParameter("leadsAbdStep"));

		System.out.println("##Session set #leadsAbdStatus : " + request.getParameter("leadsAbdStatus"));
		System.out.println("##Session set #leadsAbdFromDate : " + request.getParameter("leadsAbdFromDate"));

		return "leadsAbandonmentRep";
	}

}
