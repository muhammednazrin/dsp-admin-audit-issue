package com.spring.admin;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.VO.AuditLog;
import com.spring.VO.AuditLogExample;
import com.spring.mapper.AuditLogMapper;

@Controller
public class AuditLogController {

	AuditLogMapper auditLogMapper;

	@Autowired
	public AuditLogController(AuditLogMapper auditLogMapper

	) {
		this.auditLogMapper = auditLogMapper;

	}

	// view audit log report
	@RequestMapping(value = "/auditLogList")

	public String auditLogList(HttpServletRequest request, Model model) {

		// audit log list
		AuditLogExample auditLogList = new AuditLogExample();
		List<AuditLog> AuditLogReport = new ArrayList<AuditLog>();
		AuditLogReport = auditLogMapper.selectByExample(auditLogList);

		model.addAttribute("AuditLogReport", AuditLogReport);

		return "admin-system-audit-trail";
	}

}
