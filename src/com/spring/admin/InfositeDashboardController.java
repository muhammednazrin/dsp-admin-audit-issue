package com.spring.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.mapper.ApprovalLogMapper;
import com.spring.mapper.ApprovalMapper;
import com.spring.mapper.HOHHParamMapper;
import com.spring.mapper.InfositeContactInfoMapper;
import com.spring.mapper.InfositeEnquiryMapper;
import com.spring.mapper.InfositeNewsLetterMapper;
import com.spring.mapper.MITblPmntParamMapper;
import com.spring.mapper.TblPdfInfoMapper;

@Controller
public class InfositeDashboardController {
	
	InfositeContactInfoMapper infositeContactInfoMapper;
	InfositeEnquiryMapper infositeEnquiryMapper;
	InfositeNewsLetterMapper infositeNewsLetterMapper;
	
	@Autowired
	public InfositeDashboardController(InfositeContactInfoMapper infositeContactInfoMapper, 
			InfositeEnquiryMapper infositeEnquiryMapper, InfositeNewsLetterMapper infositeNewsLetterMapper
	
	) {
		this.infositeContactInfoMapper = infositeContactInfoMapper;
		this.infositeEnquiryMapper = infositeEnquiryMapper;
		this.infositeNewsLetterMapper = infositeNewsLetterMapper;
		
	}
	
	@RequestMapping("/showInfositeDashboard")
	public String showInfositeDashboard(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		return "admin-infosite-dashboard";

	}
}
