package com.spring.admin;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etiqa.DAO.NciDAO;
import com.etiqa.DAO.NciDAOImpl;
import com.etiqa.dsp.dao.common.motor.MotorInsuranceJPJService;
import com.google.gson.Gson;
import com.spring.VO.AuditLog;
import com.spring.VO.CommonPayment;
import com.spring.VO.CommonPolicy;
import com.spring.VO.CommonPolicyExample;
import com.spring.VO.Customer;
import com.spring.VO.jpjVOs.JpjEDocument;
import com.spring.VO.jpjVOs.JpjREPLYDOC;
import com.spring.VO.jpjVOs.JpjREPLYDOCExample;
import com.spring.VO.newCarVOs.NciQQ;
import com.spring.VO.report.NewCarParamVO;
import com.spring.VO.report.NewCarRegistrationReportVO;
import com.spring.VO.report.NewCarTransactionalDetailVO;
import com.spring.VO.report.TransSearchObject;
import com.spring.mapper.AuditLogMapper;
import com.spring.mapper.CommonPaymentMapper;
import com.spring.mapper.CommonPolicyMapper;
import com.spring.mapper.CommonQQMapper;
import com.spring.mapper.CustomerMapper;
import com.spring.mapper.jpjMappers.JpjEDocumentMapper;
import com.spring.mapper.jpjMappers.JpjREPLYDOCMapper;
import com.spring.mapper.newCarMappers.NciQQMapper;
import com.spring.mapper.report.ALLReportMapper;
import com.spring.mapper.report.NewCarRegistrationMapper;
import com.spring.mapper.report.NewCarTransactionalDetailMapper;
import com.spring.utils.ServiceValidationUtils;

@Controller
public class NciTranReportController {
	final static Logger logger = Logger.getLogger(NciTranReportController.class);

	NewCarRegistrationMapper newCarRegistrationMapper;
	ALLReportMapper allReportMapper;
	CustomerMapper customerMapper;
	JpjEDocumentMapper jpjEDocumentMapper;
	NewCarTransactionalDetailMapper newCarTransactionalDetailMapper;
	JpjREPLYDOCMapper jpjREPLYDOCMapper;
	CommonPolicyMapper commonPolicyMapper;
	NciQQMapper nciQQMapper;
	CommonQQMapper commonQQMapper;
	CommonPaymentMapper commonPaymentMapper;
	AuditLogMapper auditLogMapper;

	@Autowired
	public NciTranReportController(NewCarRegistrationMapper newCarRegistrationMapper, ALLReportMapper allReportMapper,
			CustomerMapper customerMapper, JpjEDocumentMapper jpjEDocumentMapper,
			NewCarTransactionalDetailMapper newCarTransactionalDetailMapper, JpjREPLYDOCMapper jpjREPLYDOCMapper,
			CommonPolicyMapper commonPolicyMapper, NciQQMapper nciQQMapper, CommonQQMapper commonQQMapper,
			CommonPaymentMapper commonPaymentMapper, AuditLogMapper auditLogMapper) {
		this.newCarRegistrationMapper = newCarRegistrationMapper;
		this.allReportMapper = allReportMapper;
		this.customerMapper = customerMapper;
		this.jpjEDocumentMapper = jpjEDocumentMapper;
		this.newCarTransactionalDetailMapper = newCarTransactionalDetailMapper;
		this.jpjREPLYDOCMapper = jpjREPLYDOCMapper;
		this.commonPolicyMapper = commonPolicyMapper;
		this.nciQQMapper = nciQQMapper;
		this.commonQQMapper = commonQQMapper;
		this.commonPaymentMapper = commonPaymentMapper;
		this.auditLogMapper = auditLogMapper;

	}

	@RequestMapping("/showTxnReport2")
	public String showTransactionReport(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
		return "admin-report-nci_transaction";

	}

	@RequestMapping(value = "/nciSearchReportDone", method = RequestMethod.POST)
	public String nciSearchReportDone(HttpServletRequest request, Model model) {

		DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(Locale.US);
		format.setParseBigDecimal(true);
		BigDecimal amt1 = new BigDecimal(0.00);
		BigDecimal amt2 = new BigDecimal(0.00);
		BigDecimal amt3 = new BigDecimal(0.00);
		BigDecimal amt4 = new BigDecimal(0.00);

		BigDecimal grossAmt1_GST_STAMP_DUTY = new BigDecimal(0.00);
		BigDecimal grossAmt1_PA = new BigDecimal(0.00);
		BigDecimal grossAmt2_GST_STAMP_DUTY = new BigDecimal(0.00);
		BigDecimal grossAmt2_PA = new BigDecimal(0.00);

		BigDecimal netAmt1 = new BigDecimal(0.00);
		BigDecimal netAmt2 = new BigDecimal(0.00);

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String productType = request.getParameter("productType").trim();
		String productEntity = request.getParameter("productEntity").trim();
		String status = request.getParameter("status").trim();
		String PolicyCertificateNo = request.getParameter("policyNo").trim();
		String nRIC = request.getParameter("NRIC").trim();
		String receiptNo = request.getParameter("receiptNo").trim();
		String plateNo = request.getParameter("plateNo").trim();
		String dateFrom = request.getParameter("dateFrom").trim();
		String dateTo = request.getParameter("dateTo").trim();
		List<String> errorMessages = new ArrayList<String>();

		session.setAttribute("ProductType", request.getParameter("productType").trim());
		session.setAttribute("ProductEntity", request.getParameter("productEntity").trim());
		session.setAttribute("Status", request.getParameter("status").trim());
		session.setAttribute("PolicyCertificateNo", request.getParameter("policyNo").trim());
		session.setAttribute("NRIC", request.getParameter("NRIC").trim());
		session.setAttribute("PlateNo", request.getParameter("plateNo").trim());
		session.setAttribute("ReceiptNo", request.getParameter("receiptNo").trim());
		session.setAttribute("dateFrom", request.getParameter("dateFrom").trim());
		session.setAttribute("dateTo", request.getParameter("dateTo").trim());

		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			Date fromDate = null;
			Date toDate = null;
			try {
				fromDate = sdf.parse(dateFrom);

				toDate = sdf.parse(dateTo);

				if (fromDate.compareTo(toDate) > 0) {
					errorMessages.add("Date To Cannot Less Than Date From");
				}

				else {

					try {
						// in milliseconds
						long diff = toDate.getTime() - fromDate.getTime();

						long diffSeconds = diff / 1000 % 60;
						long diffMinutes = diff / (60 * 1000) % 60;
						long diffHours = diff / (60 * 60 * 1000) % 24;
						long diffDays = diff / (24 * 60 * 60 * 1000);

						if (diffDays > 31) {

							errorMessages.add("The date range cannot exceed than 1 month");

						}

					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (ServiceValidationUtils.isEmptyStringTrim(dateFrom)) {
				errorMessages.add("Date From Cannot Be Empty");
			}

			if (ServiceValidationUtils.isEmptyStringTrim(dateTo)) {
				errorMessages.add("Date To Cannot Be Empty");
			}

			if (ServiceValidationUtils.isEmptyStringTrim(productEntity)) {
				errorMessages.add("Please Select Product Entity");
			}

			else if (ServiceValidationUtils.isEmptyStringTrim(productType) && "O".equalsIgnoreCase(status)) {
				errorMessages.add("Please Select Product Type");
			}

			else if (ServiceValidationUtils.isEmptyStringTrim(productType) && "R".equalsIgnoreCase(status)) {
				errorMessages.add("Please Select Product Type");
			}

			if (errorMessages.size() > 0) {
				model.addAttribute("errorMessages", errorMessages);
				return "admin-report-nci_transaction";
			}

			// number of days to add
			Calendar c = Calendar.getInstance();
			c.setTime(sdf.parse(dateTo));
			c.add(Calendar.DATE, 1);
			dateTo = sdf.format(c.getTime());

			// START: MOTOR TRANSACTION REPORT
			TransSearchObject transSearchObject_Motor = new TransSearchObject();
			List<NewCarRegistrationReportVO> motorReportList = new ArrayList<NewCarRegistrationReportVO>();
			List<NewCarRegistrationReportVO> motorReportList_B4Pay = new ArrayList<NewCarRegistrationReportVO>();
			List<String> transactionStatusList = new ArrayList<String>();
			logger.info("NCI report start........");
			String query1 = "n";
			String query2 = "n";

			if ("EIB".equalsIgnoreCase(productEntity) && "".equalsIgnoreCase(productType)) {

				transSearchObject_Motor.setProductCode("NCI");
			}

			if ("ETB".equalsIgnoreCase(productEntity) && "".equalsIgnoreCase(productType)) {

				transSearchObject_Motor.setProductCode("NCT");
			}
			if ("".equalsIgnoreCase(productType) || productType == null) {
				transSearchObject_Motor.setProductCode("NCI,NCT");
			}

			if ("NCI".equalsIgnoreCase(productType) || "NCT".equalsIgnoreCase(productType)) {
				transSearchObject_Motor.setProductCode(productType);

			}
			// if Product is Chosen
			logger.info("status "+status);
			if ("".equalsIgnoreCase(status) || status == null) {
				transactionStatusList.add("S");
				transactionStatusList.add("AP");
				transactionStatusList.add("F");
				transactionStatusList.add("C");
				transactionStatusList.add("O");
				transactionStatusList.add("R");
				transSearchObject_Motor.setTransactionStatus(transactionStatusList);
				// transSearchObject_Motor.setTransactionStatus("'S','AP','F','O','R'");
				query1 = "y";
				query2 = "y";
			} else {
				transactionStatusList.add(status);
				transSearchObject_Motor.setTransactionStatus(transactionStatusList);
				// transSearchObject_Motor.setTransactionStatus(status);
			}

			if ("S".equalsIgnoreCase(status) || "F".equalsIgnoreCase(status) || "AP".equalsIgnoreCase(status)
					|| "C".equalsIgnoreCase(status)) {
				query1 = "y";
			}
			if ("O".equalsIgnoreCase(status) || "R".equalsIgnoreCase(status)) {
				query2 = "y";
			}

			if (!"".equalsIgnoreCase(PolicyCertificateNo)) {
				transSearchObject_Motor.setPolicyNo(PolicyCertificateNo);
				query2 = null;
			}
			if (!"".equalsIgnoreCase(nRIC)) {
				transSearchObject_Motor.setNricNo(nRIC);
			}
			if (!"".equalsIgnoreCase(receiptNo)) {
				transSearchObject_Motor.setInvoiceNo(receiptNo);
				query2 = null;
			}
			if (!ServiceValidationUtils.isEmptyStringTrim(plateNo)) {
				transSearchObject_Motor.setVehicleNo(plateNo);
			}

			transSearchObject_Motor.setDateFrom(dateFrom);
			transSearchObject_Motor.setDateTo(dateTo);

			// if query 1
			if ("y".equalsIgnoreCase(query1)) {
				motorReportList = newCarRegistrationMapper
						.selectNciTransactionalReportAfterPayment(transSearchObject_Motor);
				logger.info("NCI report selectNciTransactionalReportAfterPayment---- ");
				if (motorReportList.size() > 0) {

					amt1 = motorReportList.get(0).getTotal_amount();
					grossAmt1_GST_STAMP_DUTY = motorReportList.get(0).getTotal_premium_after_gst_stamp_duty();
					grossAmt1_PA = motorReportList.get(0).getTotal_driver_pa_amount();
					netAmt1 = motorReportList.get(0).getTotal_net_amount();
				}

			} // End if query1 is Executed

			// if query 2
			if ("y".equalsIgnoreCase(query2)) {
				motorReportList_B4Pay = newCarRegistrationMapper
						.selectNciTransactionalReportBeforePayment(transSearchObject_Motor);
				if (motorReportList_B4Pay.size() > 0) {

					amt2 = motorReportList_B4Pay.get(0).getTotal_amount();
					grossAmt2_GST_STAMP_DUTY = motorReportList_B4Pay.get(0).getTotal_premium_after_gst_stamp_duty();
					grossAmt2_PA = motorReportList_B4Pay.get(0).getTotal_driver_pa_amount();
					netAmt2 = motorReportList_B4Pay.get(0).getTotal_net_amount();

				}

			}

			motorReportList.addAll(motorReportList_B4Pay);

			Collections.sort(motorReportList, new Comparator<NewCarRegistrationReportVO>() {
				@Override
				public int compare(NewCarRegistrationReportVO o1, NewCarRegistrationReportVO o2) {
					return o1.getRecord_date().compareTo(o2.getRecord_date());
				}
			});

			Collections.reverse(motorReportList);

			if (motorReportList.size() <= 0) {
				model.addAttribute("emptySearch", "No Record Found");
				model.addAttribute("totalAmount", "0.00");
				model.addAttribute("totalGross", "0.00");
				model.addAttribute("totalNet", "0.00");
			}

			else {

				BigDecimal totalAmount = amt1.add(amt2);
				BigDecimal totalGross1 = grossAmt1_GST_STAMP_DUTY.add(grossAmt1_PA);
				BigDecimal totalGross2 = grossAmt2_GST_STAMP_DUTY.add(grossAmt2_PA);
				BigDecimal totalGross = totalGross1.add(totalGross2);
				BigDecimal totalNet = netAmt1.add(netAmt2);

				model.addAttribute("totalAmount", totalAmount);
				model.addAttribute("totalGross", totalGross);
				model.addAttribute("totalNet", totalNet);

			}

			model.addAttribute("reportResult", motorReportList);
			logger.info("NCI report result" + motorReportList);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "admin-report-nci_transaction";
		// e.printStackTrace();

	}

	/**
	 * ----------------------------------------------New Car
	 * Functions--------------------------------------------
	 **/
	@RequestMapping(value = "/newCarDetails/{paymentTrxID}", method = RequestMethod.GET)
	private String transactionalDetail(HttpServletRequest request, Model model, @PathVariable String paymentTrxID) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		NewCarParamVO newCarParamVO = new NewCarParamVO();
		newCarParamVO.setTransactionId(paymentTrxID);
		// Newly added
		if (!"".equalsIgnoreCase((String) session.getAttribute("maxPcyNo"))) {
			String maxPcyNo = (String) session.getAttribute("maxPcyNo");
			logger.info("NciTxnReport<transactionalDetail><maxPcyNo> ==> " + maxPcyNo);
			logger.info("NciTxnReport<transactionalDetail><paymentTrxID> => " + paymentTrxID);

			newCarParamVO.setPolicy(maxPcyNo);
			// newly added
			newCarParamVO.setTransactionId(paymentTrxID);

			model.addAttribute("maxPcyNo", maxPcyNo);
			session.setAttribute("maxPcyNo", maxPcyNo);
		}
		// End

		NewCarTransactionalDetailVO tranDetail = newCarTransactionalDetailMapper
				.selectTransactionalDetails(newCarParamVO);
	//	tranDetail.setPolicyNo(newCarParamVO.getPolicy());
		
		// tranDetail.setCoverageStartDate(formatCovargeDateWithSlash(tranDetail.getCoverageStartDate()));
		// tranDetail.setCoverageEndDate(formatCovargeDateWithSlash(tranDetail.getCoverageEndDate()));

		// Newly added
		/*
		 * if(!"".equalsIgnoreCase((String) session.getAttribute("maxPcyNo"))) {
		 * tranDetail.setPolicyNo(tranDetail.getPoiPolicyNo());
		 * 
		 * if("SUCCESS".equalsIgnoreCase((String) session.getAttribute("maxPcyStatus")))
		 * { tranDetail.setPolicy_status("SUCCESS"); } }
		 */
		// End
		model.addAttribute("tranDetail", tranDetail);
		Customer customer = new Customer();
		customer.setCustomerAddress1(tranDetail.getCustomerAddress1());
		customer.setCustomerAddress2(tranDetail.getCustomerAddress2());
		customer.setCustomerAddress3(tranDetail.getCustomerAddress3());
		customer.setCustomerEmail(tranDetail.getCustomerEmail());
		customer.setCustomerName(tranDetail.getCustomerName());
		customer.setCustomerPostcode(tranDetail.getCustomerPostcode());
		customer.setCustomerMobileNo(tranDetail.getCustomerMobileNo());
		customer.setCustomerNricId(tranDetail.getCustomerNRIC());
		customer.setCustomerId(Long.valueOf(tranDetail.getCustomerID()));
		customer.setCustomerState(tranDetail.getCustomerState());
		model.addAttribute("customer", customer);
		model.addAttribute("paymentTrxID", paymentTrxID);

		// Newly added
		if (!"".equalsIgnoreCase((String) session.getAttribute("maxPcyNo"))) {
			//tranDetail.setPolicyNo(tranDetail.getPoiPolicyNo());

			if ("SUCCESS".equalsIgnoreCase((String) session.getAttribute("maxPcyStatus"))) {
				tranDetail.setPolicy_status("SUCCESS");

				logger.info("NciTxnReport<transactionalDetail><policyNo> => " + tranDetail.getPolicyNo());
				logger.info(
						"NciTxnReport<transactionalDetail><transactionID> => " + tranDetail.getTransactionID());

				logger.info(
						"NciTxnReport<transactionalDetail><CoverageStartDate> => " + tranDetail.getCoverageStartDate());
				logger.info(
						"NciTxnReport<transactionalDetail><CoverageEndDate> => " + tranDetail.getCoverageEndDate());

				// List<CommonPolicy> commonPolicyList= new ArrayList<CommonPolicy>();
				CommonPolicyExample commonPolicyExample = new CommonPolicyExample();
				CommonPolicyExample.Criteria commonPolicy_criteria = commonPolicyExample.createCriteria();

				commonPolicy_criteria.andPolicyNumberEqualTo(tranDetail.getPolicyNo());
				commonPolicy_criteria.andTransactionIdEqualTo(new BigDecimal(tranDetail.getTransactionID()));

				if ("K".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
					commonPolicy_criteria.andProductCodeEqualTo("NCT");
				}
				if ("V".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
					commonPolicy_criteria.andProductCodeEqualTo("NCI");
				}
				commonPolicy_criteria.andPolicyStatusEqualTo("SUCCESS");

				CommonPolicy commonPolicy = new CommonPolicy();
				commonPolicy.setPolicyEffectiveTimestamp(tranDetail.getCoverageStartDate());
				commonPolicy.setPolicyExpiryTimestamp(tranDetail.getCoverageEndDate());
				// commonPolicy.setPolicyStatus("SUCCESS");

				int intResult = commonPolicyMapper.updateByExampleSelective(commonPolicy, commonPolicyExample);

				if (intResult > 0) {
					logger.info(
							"NciTxnReport<transactionalDetail><ePolicyNo-e-Policy/e-Certificate Information>  in if loop ###");
					generateDocumets(tranDetail.getPolicyNo(), tranDetail.getQqid(), tranDetail.getTransactionID());

					logger.info("NciTxnReport<transactionalDetail><policyNo> ===> " + tranDetail.getPolicyNo());
					logger.info(
							"NciTxnReport<transactionalDetail><policy_status> ===> " + tranDetail.getPolicy_status());
				}
			}
		}
		// End

		return "admin-newcar-transaction-details";
	}

	@RequestMapping(value = "/newCarDetails/{paymentTrxID}/{policy}", method = RequestMethod.GET)
	private String transactionalDetail2(HttpServletRequest request, Model model, @PathVariable String paymentTrxID,
			@PathVariable String policy) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		NewCarParamVO newCarParamVO = new NewCarParamVO();
		newCarParamVO.setTransactionId(paymentTrxID);
		newCarParamVO.setPolicy(policy);
		NewCarTransactionalDetailVO tranDetail = newCarTransactionalDetailMapper
				.selectTransactionalDetails(newCarParamVO);
		tranDetail.setCoverageStartDate(formatCovargeDateWithSlash(tranDetail.getCoverageStartDate()));
		tranDetail.setCoverageEndDate(formatCovargeDateWithSlash(tranDetail.getCoverageEndDate()));
		model.addAttribute("tranDetail", tranDetail);
		Customer customer = new Customer();
		customer.setCustomerAddress1(tranDetail.getCustomerAddress1());
		customer.setCustomerAddress2(tranDetail.getCustomerAddress2());
		customer.setCustomerAddress3(tranDetail.getCustomerAddress3());
		customer.setCustomerEmail(tranDetail.getCustomerEmail());
		customer.setCustomerName(tranDetail.getCustomerName());
		customer.setCustomerPostcode(tranDetail.getCustomerPostcode());
		customer.setCustomerMobileNo(tranDetail.getCustomerMobileNo());
		customer.setCustomerNricId(tranDetail.getCustomerNRIC());
		customer.setCustomerId(Long.valueOf(tranDetail.getCustomerID()));
		customer.setCustomerState(tranDetail.getCustomerState());
		model.addAttribute("customer", customer);
		model.addAttribute("paymentTrxID", paymentTrxID);

		// TODO
		// logger.info("transactionalDetail2<policyNo> =>
		// "+tranDetail.getPolicyNo());

		List<CommonPolicy> commonPaymentList = new ArrayList<CommonPolicy>();

		CommonPolicyExample commonPolicyExample1 = new CommonPolicyExample();
		CommonPolicyExample.Criteria commonPolicyExample1_criteris = commonPolicyExample1.createCriteria();
		commonPolicyExample1_criteris
				.andDspQqIdEqualTo(BigDecimal.valueOf(Double.parseDouble(tranDetail.getDspqqid())));
		commonPolicyExample1.setOrderByClause("POLICY_NUMBER desc");

		commonPaymentList = commonPolicyMapper.selectByExample(commonPolicyExample1);

		if (commonPaymentList.size() > 0) {
			logger.info("transactionalDetail2<maxPcyNo> => " + commonPaymentList.get(0).getPolicyNumber());
			logger.info("transactionalDetail2<policyStatus> => " + commonPaymentList.get(0).getPolicyStatus());

			model.addAttribute("maxPcyNo", commonPaymentList.get(0).getPolicyNumber());
			session.setAttribute("maxPcyNo", commonPaymentList.get(0).getPolicyNumber());

			session.setAttribute("maxPcyStatus", commonPaymentList.get(0).getPolicyStatus());
		}
		// End

		return "admin-newcar-transaction-details";
	}

	@RequestMapping(value = "/newcarCustomerUpdate", method = RequestMethod.POST)
	@ResponseBody
	public String updateCustomerInfo(HttpServletRequest request, Model model,
			@ModelAttribute(value = "customer") Customer customer, BindingResult result,
			@RequestParam String paymentTrxID, @RequestParam String dspqqid, @RequestParam String policyNo) {
		model.addAttribute("customer", customer);

		NewCarParamVO newCarParamVO = new NewCarParamVO();
		newCarParamVO.setTransactionId(paymentTrxID);
		newCarParamVO.setPolicy(policyNo);
		NewCarTransactionalDetailVO tranDetail = newCarTransactionalDetailMapper
				.selectTransactionalDetails(newCarParamVO);
		tranDetail.setCoverageStartDate(formatCovargeDateWithSlash(tranDetail.getCoverageStartDate()));
		tranDetail.setCoverageEndDate(formatCovargeDateWithSlash(tranDetail.getCoverageEndDate()));
		int queryResult = 0;
		/*
		 * List<Customer> customerList= new ArrayList<Customer>(); CustomerExample
		 * customerExample =new CustomerExample(); CustomerExample.Criteria
		 * customer_criteria=customerExample.createCriteria();
		 * customer_criteria.andCustomerNricIdNotEqualTo(customer.getCustomerNricId());
		 * int customerExist=customerMapper.countByExample(customerExample);
		 * if(customerExist >=1) {
		 * customerList=customerMapper.selectByExample(customerExample); CommonQQExample
		 * commonQQExample= new CommonQQExample(); CommonQQExample.Criteria
		 * commonQQ_criteria=commonQQExample.createCriteria();
		 * commonQQ_criteria.andDspQqIdEqualTo(Short.valueOf(dspqqid)); CommonQQ
		 * commonQQ= new CommonQQ(); commonQQ.setDspQqId(new BigDecimal(dspqqid));
		 * commonQQ.setCustomerId(new BigDecimal(customerList.get(0).getCustomerId()));
		 * queryResult =commonQQMapper.updateByPrimaryKeySelective(commonQQ); } else {
		 */
		queryResult = customerMapper.updateByPrimaryKeySelective(customer);
		// }
		// Send to JPJ
		String startDate = null;
		String endDate = null;
		if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getCoverageStartDate())) {
			String day = tranDetail.getCoverageStartDate().substring(0, 2);
			String mon = tranDetail.getCoverageStartDate().substring(2, 4);
			String yy = tranDetail.getCoverageStartDate().substring(4, 8);
			startDate = yy + mon + day;
		}
		if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getCoverageEndDate())) {
			String day = tranDetail.getCoverageEndDate().substring(0, 2);
			String mon = tranDetail.getCoverageEndDate().substring(2, 4);
			String yy = tranDetail.getCoverageEndDate().substring(4, 8);
			endDate = yy + mon + day;
		}

		JpjEDocument jpjEDocument = new JpjEDocument();
		if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getVehicleNumber())) {
			jpjEDocument.setVehregno(tranDetail.getVehicleNumber());
		} else {
			jpjEDocument.setVehregno("NA");
		}

		if ("K".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
			jpjEDocument.setCompcode("96");
		}
		if ("V".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
			jpjEDocument.setCompcode("14");
		}

		jpjEDocument.setEngineno(tranDetail.getEngineNo());
		jpjEDocument.setChassisno(tranDetail.getChassisNo());
		jpjEDocument.setDoctype("2");
		jpjEDocument.setReasoncode("3");
		jpjEDocument.setIdno(tranDetail.getCustomerNRIC());
		jpjEDocument.setEffectdate(startDate);
		jpjEDocument.setExpirydate(endDate);
		jpjEDocument.setIdno2("NA");
		jpjEDocument.setCovertype("0");
		jpjEDocument.setDocno(tranDetail.getPolicyNo());
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		// Get the date today using Calendar object.
		Date today = Calendar.getInstance().getTime();
		// Using DateFormat format method we can create a string
		// representation of a date with the defined format.
		String reportDate = df.format(today);
		jpjEDocument.setInserttimestamp(reportDate);
		jpjEDocumentMapper.insert(jpjEDocument);

		Map<String, String> response = new HashMap<String, String>();
		List<JpjREPLYDOC> jpjREPLYDOCList = new ArrayList<JpjREPLYDOC>();
		JpjREPLYDOCExample jpjREPLYDOCExample = new JpjREPLYDOCExample();
		JpjREPLYDOCExample.Criteria jpjREPLYDOC_criteria = jpjREPLYDOCExample.createCriteria();
		jpjREPLYDOC_criteria.andDocnoEqualTo(tranDetail.getPolicyNo());
		jpjREPLYDOC_criteria.andDoctypeEqualTo("2");
		jpjREPLYDOC_criteria.andReasoncodeEqualTo("3");
		jpjREPLYDOC_criteria.andStatuscodeEqualTo("01");
		jpjREPLYDOCExample.setOrderByClause("ID desc");

		/*
		 * long startTime = System.currentTimeMillis(); //fetch starting time int
		 * flag=1; while(flag==1 && (System.currentTimeMillis()-startTime)<100000) {
		 * jpjREPLYDOCList=jpjREPLYDOCMapper.selectByExample(jpjREPLYDOCExample);
		 * if(jpjREPLYDOCList.size() > 0) {
		 * if("01".equalsIgnoreCase(jpjREPLYDOCList.get(0).getStatuscode())) { flag=2;
		 * logger.info("i " + jpjREPLYDOCList.get(0).getStatuscode());
		 * logger.info(flag); response.put("jpjResponse",
		 * jpjREPLYDOCList.get(0).getStatuscode()); }
		 * 
		 * } }
		 */
		response.put("jpjResponse", "01");
		response.put("paymentTrxID", paymentTrxID);
		response.put("policyNo", policyNo);
		String jsonString = new Gson().toJson(response);
		return jsonString;
	}
	@RequestMapping(value = "/newcarTranUpdate", method = RequestMethod.POST)
	@ResponseBody
	public String updateTranInfo(HttpServletRequest request, Model model,
			@RequestParam String paymentTrxID,@RequestParam String dspqqid, @RequestParam String status, @RequestParam String remark) {
		 logger.info("Original Status"+dspqqid);logger.info("Original Status"+status);logger.info("Original Status"+remark);
			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			NewCarParamVO newCarParamVO = new NewCarParamVO();
			newCarParamVO.setTransactionId(paymentTrxID);
			NewCarTransactionalDetailVO tranDetail = newCarTransactionalDetailMapper
					.selectTransactionalDetails(newCarParamVO);
			Map<String, String> response = new HashMap<String, String>();

			try {

				 logger.info("Original Status"+tranDetail.getPaymentStatus());

				if (tranDetail.getPaymentStatus().trim().equals(status.trim())) {

					response.put("resCode", "E1");
					response.put("resDesc", "Please choose new payment status");

				}

				else if (ServiceValidationUtils.isEmptyStringTrim(remark.trim())) {

					response.put("resCode", "E2");
					response.put("resDesc", "Please fill in the Remarks");

				}

				else {

					AuditLog auditLog = new AuditLog();
					auditLog.setTransactionId(paymentTrxID);
					auditLog.setActionby(loginUser);
					auditLog.setAction("E");
					auditLog.setRemarks(remark.trim());
					auditLog.setPredata(tranDetail.getPaymentStatus());
					auditLog.setPostdata(status);
					auditLog.setCreatedate(new Date());
					auditLog.setCreatedby((short) 1);
					auditLog.setModuleId(1);

					auditLogMapper.insert(auditLog);

					// Update Payment table
					CommonPayment commonPaymentUpdate = new CommonPayment();
					commonPaymentUpdate.setPmntStatus(status);
					commonPaymentUpdate.setTransactionId(paymentTrxID);
					commonPaymentMapper.updateByPrimaryKeySelective(commonPaymentUpdate);

					logger.info("status" + status);
					logger.info("paymentTrxID" + paymentTrxID);

					// Update Audit Log table

					if (status.equals("S")) {

						// generate policy no

						NciDAO nciDAO = new NciDAOImpl();
						String newPolicyNo = "";
						try {
							newPolicyNo = nciDAO.NCI_policy_Insert(tranDetail.getDspqqid(), tranDetail.getTransactionID());
							response.put("policyNo", newPolicyNo);
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						// generate edocument

						if (!ServiceValidationUtils.isEmptyStringTrim(newPolicyNo)) {

							String documenturl = "";
							try

							{

								if ("K".equalsIgnoreCase(newPolicyNo.substring(0, 1))) {
									// documenturl="http://cpf:7011/NEWCARTAKAFUL_EDOCS";
									documenturl = "http://cpf:7011/CPF-DSP-NCI-DOCS";
								} else if ("V".equalsIgnoreCase(newPolicyNo.substring(0, 1))) {
									// documenturl="http://cpf:7011/NEWCARINSURANCE_EDOCS";
									documenturl = "http://cpf:7011/CPF-DSP-NCT-DOCS";

								}

								QName servicename = new QName("http://motor.wcservices.dsp.etiqa.com/",
										"MotorInsuranceDocGenWSImplService");
								QName portname = new QName("http://motor.wcservices.dsp.etiqa.com/",
										"MotorInsuranceDocGenWSImplPort");
								Service service = Service.create(null, servicename);
								service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
										documenturl + "/MotorInsuranceDocGenWSImplService");
								Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
										Service.Mode.MESSAGE);
								MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
								SOAPMessage request1 = mf.createMessage();
								SOAPPart part = request1.getSOAPPart();
								SOAPEnvelope env = part.getEnvelope();
								SOAPBody body = env.getBody();
								// resp = body;
								SOAPElement operation = body.addChildElement("getStatus", "mot",
										"http://motor.wcservices.dsp.etiqa.com/");
								SOAPElement element_param = operation.addChildElement("QQID");
								element_param.addTextNode(tranDetail.getQqid());
								SOAPElement element_param_lang = operation.addChildElement("langValue");
								element_param_lang.addTextNode("lan_en");

								request1.saveChanges();
								String result = null;

								if (request1 != null) {
									ByteArrayOutputStream baos = null;
									try {
										baos = new ByteArrayOutputStream();
										request1.writeTo(baos);
										result = baos.toString();
									} catch (Exception e) {
									} finally {
										if (baos != null) {
											try {
												baos.close();
											} catch (IOException ioe) {
											}
										}
									}
								}
								logger.info(result + "document insert");
								// resCode=body ;

								SOAPMessage response1 = dispatch.invoke(request1);
								SOAPBody responsebody = response1.getSOAPBody();
								java.util.Iterator otcupdates = responsebody.getChildElements();
								while (otcupdates.hasNext()) {
									SOAPElement otcupdates1 = (SOAPElement) otcupdates.next();

									java.util.Iterator i = otcupdates1.getChildElements();
									while (i.hasNext()) {

										SOAPElement e = (SOAPElement) i.next();
										java.util.Iterator m = e.getChildElements();
										while (m.hasNext()) {
											SOAPElement e2 = (SOAPElement) m.next();

										}
									}
								}

							} catch (Exception e) {
								e.printStackTrace();

							}

						}

						// JPJ send

						// NewCarParamVO newCarParamVO=new NewCarParamVO();
						// newCarParamVO.setTransactionId(paymentTrxID);
						// newCarParamVO.setPolicy(policyNo);
						try {
							// NewCarTransactionalDetailVO
							// tranDetail=newCarTransactionalDetailMapper.selectTransactionalDetails(newCarParamVO);
							tranDetail.setCoverageStartDate(formatCovargeDateWithSlash(tranDetail.getCoverageStartDate()));
							tranDetail.setCoverageEndDate(formatCovargeDateWithSlash(tranDetail.getCoverageEndDate()));

							if (!ServiceValidationUtils.isEmptyStringTrim(newPolicyNo)) {

								String startDate = null;
								String endDate = null;
								if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getCoverageStartDate())) {
									String day = tranDetail.getCoverageStartDate().substring(0, 2);
									String mon = tranDetail.getCoverageStartDate().substring(2, 4);
									String yy = tranDetail.getCoverageStartDate().substring(4, 8);
									startDate = yy + mon + day;
								}
								if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getCoverageEndDate())) {
									String day = tranDetail.getCoverageEndDate().substring(0, 2);
									String mon = tranDetail.getCoverageEndDate().substring(2, 4);
									String yy = tranDetail.getCoverageEndDate().substring(4, 8);
									endDate = yy + mon + day;
								}

								JpjEDocument jpjEDocument = new JpjEDocument();
								if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getVehicleNumber())) {
									jpjEDocument.setVehregno(tranDetail.getVehicleNumber());
								} else {
									jpjEDocument.setVehregno("NA");
								}

								if ("K".equalsIgnoreCase(newPolicyNo.substring(0, 1))) {
									jpjEDocument.setCompcode("96");
								}
								if ("V".equalsIgnoreCase(newPolicyNo.substring(0, 1))) {
									jpjEDocument.setCompcode("14");
								}
								jpjEDocument.setEngineno(tranDetail.getEngineNo());
								jpjEDocument.setChassisno(tranDetail.getChassisNo());
								jpjEDocument.setDoctype("0");
								jpjEDocument.setReasoncode("0");
								jpjEDocument.setIdno(tranDetail.getCustomerNRIC());
								jpjEDocument.setEffectdate(startDate);
								jpjEDocument.setExpirydate(endDate);
								jpjEDocument.setIdno2("NA");
								jpjEDocument.setCovertype("0");
								jpjEDocument.setDocno(newPolicyNo);
								DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
								// Get the date today using Calendar object.
								Date today = Calendar.getInstance().getTime();
								// Using DateFormat format method we can create a string
								// representation of a date with the defined format.
								String reportDate = df.format(today);
								jpjEDocument.setInserttimestamp(reportDate);
								jpjEDocumentMapper.insert(jpjEDocument);

							}
						} catch (Exception e) {

							e.printStackTrace();

						}

						// call ISM request

					} else if (status.equals("F") || status.equals("C")) {

						if (tranDetail.getPaymentStatus().trim().equals("S") && !tranDetail.getPolicyNo().equals(null)
								&& !tranDetail.getPolicyNo().equals("")) {
							// cancel Policy
							try {
								List<CommonPolicy> commonPolicyList = new ArrayList<CommonPolicy>();
								CommonPolicyExample commonPolicyExample = new CommonPolicyExample();
								CommonPolicyExample.Criteria commonPolicy_criteria = commonPolicyExample.createCriteria();
								commonPolicy_criteria.andPolicyNumberEqualTo(tranDetail.getPolicyNo());
								commonPolicy_criteria.andDspQqIdEqualTo(new BigDecimal(dspqqid));
								if ("K".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
									commonPolicy_criteria.andProductCodeEqualTo("NCT");
								}
								if ("V".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
									commonPolicy_criteria.andProductCodeEqualTo("NCI");
								}
								commonPolicy_criteria.andPolicyStatusEqualTo("SUCCESS");
								CommonPolicy commonPolicy = new CommonPolicy();
								commonPolicy.setPolicyStatus("CANCEL");
								int cancelResult = commonPolicyMapper.updateByExampleSelective(commonPolicy,
										commonPolicyExample);

							}

							catch (Exception e) {

								e.printStackTrace();

							}

							// cancel JPJ

							try {

								String startDate = null;
								String endDate = null;
								if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getCoverageStartDate())) {
									String day = tranDetail.getCoverageStartDate().substring(0, 2);
									String mon = tranDetail.getCoverageStartDate().substring(2, 4);
									String yy = tranDetail.getCoverageStartDate().substring(4, 8);
									startDate = yy + mon + day;
								}
								if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getCoverageEndDate())) {
									String day = tranDetail.getCoverageEndDate().substring(0, 2);
									String mon = tranDetail.getCoverageEndDate().substring(2, 4);
									String yy = tranDetail.getCoverageEndDate().substring(4, 8);
									endDate = yy + mon + day;
								}

								JpjEDocument jpjEDocument = new JpjEDocument();
								if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getVehicleNumber())) {
									jpjEDocument.setVehregno(tranDetail.getVehicleNumber());
								} else {
									jpjEDocument.setVehregno("NA");
								}

								if ("K".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
									jpjEDocument.setCompcode("96");
								}
								if ("V".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
									jpjEDocument.setCompcode("14");
								}
								jpjEDocument.setEngineno(tranDetail.getEngineNo());
								jpjEDocument.setChassisno(tranDetail.getChassisNo());
								jpjEDocument.setDoctype("3");
								jpjEDocument.setReasoncode("0");
								jpjEDocument.setIdno(tranDetail.getCustomerNRIC());
								jpjEDocument.setEffectdate(startDate);
								jpjEDocument.setExpirydate(endDate);
								jpjEDocument.setIdno2("NA");
								jpjEDocument.setCovertype("0");
								jpjEDocument.setDocno(tranDetail.getPolicyNo());
								DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
								// Get the date today using Calendar object.
								Date today = Calendar.getInstance().getTime();
								// Using DateFormat format method we can create a string
								// representation of a date with the defined format.
								String reportDate = df.format(today);
								jpjEDocument.setInserttimestamp(reportDate);
								jpjEDocumentMapper.insert(jpjEDocument);

							}

							catch (Exception e) {

								e.printStackTrace();

							}

							// cancel ISM

						}

					}

					response.put("resCode", "00");
					response.put("resDesc", "Payment status updated");

				}

			} catch (Exception e) {

				response.put("resCode", "E3");
				response.put("resDesc", "Error occur during update the payment status");
				e.printStackTrace();

			}

			String jsonString = new Gson().toJson(response);
			return jsonString;
	}
	@RequestMapping(value = "/newcarChassisEngineUpdate", method = RequestMethod.POST)
	@ResponseBody
	public String newcarChassisEngineUpdate(HttpServletRequest request, Model model, @RequestParam String paymentTrxID,
			@RequestParam String dspqqid, @RequestParam String policyNo, @RequestParam String chassisNo,
			@RequestParam String engineNo) {
		NewCarParamVO newCarParamVO = new NewCarParamVO();
		newCarParamVO.setTransactionId(paymentTrxID);
		newCarParamVO.setPolicy(policyNo);
		NewCarTransactionalDetailVO tranDetail = newCarTransactionalDetailMapper
				.selectTransactionalDetails(newCarParamVO);
		tranDetail.setCoverageStartDate(formatCovargeDateWithSlash(tranDetail.getCoverageStartDate()));
		tranDetail.setCoverageEndDate(formatCovargeDateWithSlash(tranDetail.getCoverageEndDate()));
		Map<String, String> response = new HashMap<String, String>();
		// Start- Check if the policy is canceled
		List<CommonPolicy> commonPolicyList = new ArrayList<CommonPolicy>();
		CommonPolicyExample commonPolicyExample = new CommonPolicyExample();
		CommonPolicyExample.Criteria commonPolicy_criteria = commonPolicyExample.createCriteria();
		commonPolicy_criteria.andPolicyNumberEqualTo(tranDetail.getPolicyNo());
		commonPolicy_criteria.andDspQqIdEqualTo(new BigDecimal(dspqqid.trim()));
		if ("K".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
			commonPolicy_criteria.andProductCodeEqualTo("NCT");
		}
		if ("V".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
			commonPolicy_criteria.andProductCodeEqualTo("NCI");
		}
		commonPolicy_criteria.andPolicyStatusEqualTo("CANCEL");
		commonPolicyList = commonPolicyMapper.selectByExample(commonPolicyExample);

		if (commonPolicyList.size() == 0) {
			response.put("message", "You Must Cancel the Policy First");
			String jsonString = new Gson().toJson(response);
			return jsonString;
		}
		// End- Check if the policy is canceled

		// Start- Update NCIQQTable and Generate New Policy
		NciQQ nciQQ = new NciQQ();
		nciQQ.setQqId(Short.valueOf(tranDetail.getQqid()));
		nciQQ.setChassisNumber(chassisNo);
		nciQQ.setEngineNumber(engineNo);
		nciQQMapper.updateByPrimaryKeySelective(nciQQ);
		// Generate New Policy
		NciDAO nciDAO = new NciDAOImpl();
		String newPolicyNo = "0";
		try {
			newPolicyNo = nciDAO.NCI_policy_Insert(tranDetail.getDspqqid(), tranDetail.getTransactionID());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// End- Update NCIQQTable and Generate New Policy

		// JPJ Cancel
		String startDate = null;
		String endDate = null;
		if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getCoverageStartDate())) {
			String day = tranDetail.getCoverageStartDate().substring(0, 2);
			String mon = tranDetail.getCoverageStartDate().substring(2, 4);
			String yy = tranDetail.getCoverageStartDate().substring(4, 8);
			startDate = yy + mon + day;
		}
		if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getCoverageEndDate())) {
			String day = tranDetail.getCoverageEndDate().substring(0, 2);
			String mon = tranDetail.getCoverageEndDate().substring(2, 4);
			String yy = tranDetail.getCoverageEndDate().substring(4, 8);
			endDate = yy + mon + day;
		}

		JpjEDocument jpjEDocument = new JpjEDocument();
		if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getVehicleNumber())) {
			jpjEDocument.setVehregno(tranDetail.getVehicleNumber());
		} else {
			jpjEDocument.setVehregno("NA");
		}

		if ("K".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
			jpjEDocument.setCompcode("96");
		}
		if ("V".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
			jpjEDocument.setCompcode("14");
		}
		jpjEDocument.setEngineno(tranDetail.getEngineNo());
		jpjEDocument.setChassisno(tranDetail.getChassisNo());
		jpjEDocument.setDoctype("0");
		jpjEDocument.setReasoncode("0");
		jpjEDocument.setIdno(tranDetail.getCustomerNRIC());
		jpjEDocument.setEffectdate(startDate);
		jpjEDocument.setExpirydate(endDate);
		jpjEDocument.setIdno2("NA");
		jpjEDocument.setCovertype("0");
		jpjEDocument.setDocno(tranDetail.getPolicyNo());
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		// Get the date today using Calendar object.
		Date today = Calendar.getInstance().getTime();
		// Using DateFormat format method we can create a string
		// representation of a date with the defined format.
		String reportDate = df.format(today);
		jpjEDocument.setInserttimestamp(reportDate);
		jpjEDocumentMapper.insert(jpjEDocument);
		// Get JPJ Reply

		// Get JPJ Reply
		List<JpjREPLYDOC> jpjREPLYDOCList = new ArrayList<JpjREPLYDOC>();
		JpjREPLYDOCExample jpjREPLYDOCExample = new JpjREPLYDOCExample();
		JpjREPLYDOCExample.Criteria jpjREPLYDOC_criteria = jpjREPLYDOCExample.createCriteria();
		jpjREPLYDOC_criteria.andDocnoEqualTo(tranDetail.getPolicyNo());
		jpjREPLYDOC_criteria.andDoctypeEqualTo("0");
		jpjREPLYDOC_criteria.andReasoncodeEqualTo("0");
		jpjREPLYDOC_criteria.andStatuscodeEqualTo("01");
		jpjREPLYDOCExample.setOrderByClause("ID desc");

		/*
		 * long startTime = System.currentTimeMillis(); //fetch starting time int
		 * flag=1; while(flag==1 && (System.currentTimeMillis()-startTime)<100000) {
		 * jpjREPLYDOCList=jpjREPLYDOCMapper.selectByExample(jpjREPLYDOCExample);
		 * if(jpjREPLYDOCList.size() > 0) {
		 * if("01".equalsIgnoreCase(jpjREPLYDOCList.get(0).getStatuscode())) { flag=2;
		 * logger.info("i " + jpjREPLYDOCList.get(0).getStatuscode());
		 * logger.info(flag); response.put("jpjResponse",
		 * jpjREPLYDOCList.get(0).getStatuscode()); }
		 * 
		 * } }
		 */
		// Todo
		generateDocumets(tranDetail.getPolicyNo(), tranDetail.getQqid(), tranDetail.getTransactionID());

		response.put("jpjResponse", "01");
		response.put("paymentTrxID", paymentTrxID);
		response.put("policyNo", newPolicyNo);
		String jsonString = new Gson().toJson(response);
		return jsonString;
	}

	@RequestMapping(value = "/updatePOI", method = RequestMethod.POST)
	private String waitingForJpjReply(HttpServletRequest request, Model model, @RequestParam String coverageStartDate,
			@RequestParam String vehicleNumber, @RequestParam String paymentTrxID, @RequestParam String qqid) {

		String sDate1 = coverageStartDate;
		Date date1;
		String coverageEndDate = null;
		try {
			date1 = new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
			logger.info(sDate1 + "\t" + date1);
			Calendar dateMoreOneYear = Calendar.getInstance();
			dateMoreOneYear.setTime(date1);
			dateMoreOneYear.add(Calendar.YEAR, 1);
			dateMoreOneYear.add(Calendar.DAY_OF_MONTH, -1);
			dateMoreOneYear.toString();
			coverageEndDate = new SimpleDateFormat("dd/MM/yyyy").format(dateMoreOneYear.getTime());
			logger.info(sDate1 + "\t" + coverageEndDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String coverageStartDateFormated = formatCovargeDateWithoutSlash(coverageStartDate);
		String coverageEndDateFormated = formatCovargeDateWithoutSlash(coverageEndDate);
		NciQQ nciQQ = new NciQQ();
		nciQQ.setQqId(Short.valueOf(qqid));
		nciQQ.setCoverageStartDate(coverageStartDateFormated);
		nciQQ.setCoverageEndDate(coverageEndDateFormated);
		nciQQ.setRegistrationNumber(vehicleNumber);
		nciQQMapper.updateByPrimaryKeySelective(nciQQ);
		return transactionalDetail(request, model, paymentTrxID);
	}

	@RequestMapping(value = "/newcarCancelPolicy", method = RequestMethod.POST)
	@ResponseBody
	private String newcarCancelPolicy(HttpServletRequest request, Model model, @RequestParam String paymentTrxID,
			@RequestParam String dspqqid, @RequestParam String policyNo) {
		NewCarParamVO newCarParamVO = new NewCarParamVO();
		newCarParamVO.setTransactionId(paymentTrxID);
		newCarParamVO.setPolicy(policyNo);
		NewCarTransactionalDetailVO tranDetail = newCarTransactionalDetailMapper
				.selectTransactionalDetails(newCarParamVO);
		tranDetail.setCoverageStartDate(formatCovargeDateWithSlash(tranDetail.getCoverageStartDate()));
		tranDetail.setCoverageEndDate(formatCovargeDateWithSlash(tranDetail.getCoverageEndDate()));
		Map<String, String> response = new HashMap<String, String>();
		// Cancel Policy
		List<CommonPolicy> commonPolicyList = new ArrayList<CommonPolicy>();
		CommonPolicyExample commonPolicyExample = new CommonPolicyExample();
		CommonPolicyExample.Criteria commonPolicy_criteria = commonPolicyExample.createCriteria();
		commonPolicy_criteria.andPolicyNumberEqualTo(tranDetail.getPolicyNo());
		commonPolicy_criteria.andDspQqIdEqualTo(new BigDecimal(dspqqid));
		if ("K".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
			commonPolicy_criteria.andProductCodeEqualTo("NCT");
		}
		if ("V".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
			commonPolicy_criteria.andProductCodeEqualTo("NCI");
		}
		commonPolicy_criteria.andPolicyStatusEqualTo("SUCCESS");
		CommonPolicy commonPolicy = new CommonPolicy();
		commonPolicy.setPolicyStatus("CANCEL");
		int cancelResult = commonPolicyMapper.updateByExampleSelective(commonPolicy, commonPolicyExample);

		if (cancelResult == 0) {
			model.addAttribute("error", "Failed to Cancel Policy");
			return transactionalDetail(request, model, paymentTrxID);
		}

		// JPJ Cancel
		String startDate = null;
		String endDate = null;
		if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getCoverageStartDate())) {
			String day = tranDetail.getCoverageStartDate().substring(0, 2);
			String mon = tranDetail.getCoverageStartDate().substring(2, 4);
			String yy = tranDetail.getCoverageStartDate().substring(4, 8);
			startDate = yy + mon + day;
		}
		if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getCoverageEndDate())) {
			String day = tranDetail.getCoverageEndDate().substring(0, 2);
			String mon = tranDetail.getCoverageEndDate().substring(2, 4);
			String yy = tranDetail.getCoverageEndDate().substring(4, 8);
			endDate = yy + mon + day;
		}

		JpjEDocument jpjEDocument = new JpjEDocument();
		if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getVehicleNumber())) {
			jpjEDocument.setVehregno(tranDetail.getVehicleNumber());
		} else {
			jpjEDocument.setVehregno("NA");
		}

		if ("K".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
			jpjEDocument.setCompcode("96");
		}
		if ("V".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
			jpjEDocument.setCompcode("14");
		}
		jpjEDocument.setEngineno(tranDetail.getEngineNo());
		jpjEDocument.setChassisno(tranDetail.getChassisNo());
		jpjEDocument.setDoctype("3");
		jpjEDocument.setReasoncode("0");
		jpjEDocument.setIdno(tranDetail.getCustomerNRIC());
		jpjEDocument.setEffectdate(startDate);
		jpjEDocument.setExpirydate(endDate);
		jpjEDocument.setIdno2("NA");
		jpjEDocument.setCovertype("0");
		jpjEDocument.setDocno(tranDetail.getPolicyNo());
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		// Get the date today using Calendar object.
		Date today = Calendar.getInstance().getTime();
		// Using DateFormat format method we can create a string
		// representation of a date with the defined format.
		String reportDate = df.format(today);
		jpjEDocument.setInserttimestamp(reportDate);
		jpjEDocumentMapper.insert(jpjEDocument);
		// Get JPJ Reply
		List<JpjREPLYDOC> jpjREPLYDOCList = new ArrayList<JpjREPLYDOC>();
		JpjREPLYDOCExample jpjREPLYDOCExample = new JpjREPLYDOCExample();
		JpjREPLYDOCExample.Criteria jpjREPLYDOC_criteria = jpjREPLYDOCExample.createCriteria();
		jpjREPLYDOC_criteria.andDocnoEqualTo(tranDetail.getPolicyNo());
		jpjREPLYDOC_criteria.andDoctypeEqualTo("3");
		jpjREPLYDOC_criteria.andReasoncodeEqualTo("0");
		jpjREPLYDOC_criteria.andStatuscodeEqualTo("01");
		jpjREPLYDOCExample.setOrderByClause("ID desc");

		/*
		 * long startTime = System.currentTimeMillis(); //fetch starting time int
		 * flag=1; while(flag==1 && (System.currentTimeMillis()-startTime)<100000) {
		 * jpjREPLYDOCList=jpjREPLYDOCMapper.selectByExample(jpjREPLYDOCExample);
		 * if(jpjREPLYDOCList.size() > 0) {
		 * if("01".equalsIgnoreCase(jpjREPLYDOCList.get(0).getStatuscode())) { flag=2;
		 * logger.info("i " + jpjREPLYDOCList.get(0).getStatuscode());
		 * logger.info(flag); response.put("jpjResponse",
		 * jpjREPLYDOCList.get(0).getStatuscode()); } else { response.put("jpjResponse",
		 * jpjREPLYDOCList.get(0).getStatuscode()); }
		 * 
		 * } }
		 * 
		 * if (flag==1) {
		 * commonPolicy_criteria.andPolicyNumberEqualTo(tranDetail.getPolicyNo());
		 * commonPolicy_criteria.andDspQqIdEqualTo(new BigDecimal(dspqqid)); if
		 * ("K".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
		 * commonPolicy_criteria.andProductCodeEqualTo("NCT"); } if
		 * ("V".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
		 * commonPolicy_criteria.andProductCodeEqualTo("NCI"); }
		 * commonPolicy_criteria.andPolicyStatusEqualTo("CANCEL");
		 * commonPolicy.setPolicyStatus("SUCCESS"); int
		 * successResult=commonPolicyMapper.updateByExampleSelective(commonPolicy,
		 * commonPolicyExample); }
		 */
		response.put("jpjResponse", "01");
		response.put("paymentTrxID", paymentTrxID);
		response.put("policyNo", policyNo);
		String jsonString = new Gson().toJson(response);
		return jsonString;
	}

	@RequestMapping(value = "/newcarResendJPJ", method = RequestMethod.POST)
	@ResponseBody
	private String newcarResendJPJ(HttpServletRequest request, Model model, @RequestParam String paymentTrxID,
			@RequestParam String dspqqid, @RequestParam String policyNo) {
		NewCarParamVO newCarParamVO = new NewCarParamVO();
		newCarParamVO.setTransactionId(paymentTrxID);
		newCarParamVO.setPolicy(policyNo);
		NewCarTransactionalDetailVO tranDetail = newCarTransactionalDetailMapper
				.selectTransactionalDetails(newCarParamVO);
		tranDetail.setCoverageStartDate(formatCovargeDateWithSlash(tranDetail.getCoverageStartDate()));
		tranDetail.setCoverageEndDate(formatCovargeDateWithSlash(tranDetail.getCoverageEndDate()));
		Map<String, String> response = new HashMap<String, String>();

		// JPJ Resend
		String startDate = null;
		String endDate = null;
		if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getCoverageStartDate())) {
			String day = tranDetail.getCoverageStartDate().substring(0, 2);
			String mon = tranDetail.getCoverageStartDate().substring(2, 4);
			String yy = tranDetail.getCoverageStartDate().substring(4, 8);
			startDate = yy + mon + day;
		}
		if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getCoverageEndDate())) {
			String day = tranDetail.getCoverageEndDate().substring(0, 2);
			String mon = tranDetail.getCoverageEndDate().substring(2, 4);
			String yy = tranDetail.getCoverageEndDate().substring(4, 8);
			endDate = yy + mon + day;
		}

		JpjEDocument jpjEDocument = new JpjEDocument();
		if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getVehicleNumber())) {
			jpjEDocument.setVehregno(tranDetail.getVehicleNumber());
		} else {
			jpjEDocument.setVehregno("NA");
		}

		if ("K".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
			jpjEDocument.setCompcode("96");
		}
		if ("V".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
			jpjEDocument.setCompcode("14");
		}
		jpjEDocument.setEngineno(tranDetail.getEngineNo());
		jpjEDocument.setChassisno(tranDetail.getChassisNo());
		jpjEDocument.setDoctype("0");
		jpjEDocument.setReasoncode("0");
		jpjEDocument.setIdno(tranDetail.getCustomerNRIC());
		jpjEDocument.setEffectdate(startDate);
		jpjEDocument.setExpirydate(endDate);
		jpjEDocument.setIdno2("NA");
		jpjEDocument.setCovertype("0");
		jpjEDocument.setDocno(tranDetail.getPolicyNo());
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		// Get the date today using Calendar object.
		Date today = Calendar.getInstance().getTime();
		// Using DateFormat format method we can create a string
		// representation of a date with the defined format.
		String reportDate = df.format(today);
		jpjEDocument.setInserttimestamp(reportDate);
		jpjEDocumentMapper.insert(jpjEDocument);
		// Get JPJ Reply
		List<JpjREPLYDOC> jpjREPLYDOCList = new ArrayList<JpjREPLYDOC>();
		JpjREPLYDOCExample jpjREPLYDOCExample = new JpjREPLYDOCExample();
		JpjREPLYDOCExample.Criteria jpjREPLYDOC_criteria = jpjREPLYDOCExample.createCriteria();
		jpjREPLYDOC_criteria.andDocnoEqualTo(tranDetail.getPolicyNo());
		jpjREPLYDOC_criteria.andDoctypeEqualTo("0");
		jpjREPLYDOC_criteria.andReasoncodeEqualTo("0");
		jpjREPLYDOC_criteria.andStatuscodeEqualTo("01");
		jpjREPLYDOCExample.setOrderByClause("ID desc");

		/*
		 * long startTime = System.currentTimeMillis(); //fetch starting time int
		 * flag=1; while(flag==1 && (System.currentTimeMillis()-startTime)<100000) {
		 * jpjREPLYDOCList=jpjREPLYDOCMapper.selectByExample(jpjREPLYDOCExample);
		 * if(jpjREPLYDOCList.size() > 0) {
		 * if("01".equalsIgnoreCase(jpjREPLYDOCList.get(0).getStatuscode())) { flag=2;
		 * logger.info("i " + jpjREPLYDOCList.get(0).getStatuscode());
		 * logger.info(flag); response.put("jpjResponse",
		 * jpjREPLYDOCList.get(0).getStatuscode()); } else { response.put("jpjResponse",
		 * jpjREPLYDOCList.get(0).getStatuscode()); }
		 * 
		 * } }
		 * 
		 * if (flag==1) {
		 * commonPolicy_criteria.andPolicyNumberEqualTo(tranDetail.getPolicyNo());
		 * commonPolicy_criteria.andDspQqIdEqualTo(new BigDecimal(dspqqid)); if
		 * ("K".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
		 * commonPolicy_criteria.andProductCodeEqualTo("NCT"); } if
		 * ("V".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
		 * commonPolicy_criteria.andProductCodeEqualTo("NCI"); }
		 * commonPolicy_criteria.andPolicyStatusEqualTo("CANCEL");
		 * commonPolicy.setPolicyStatus("SUCCESS"); int
		 * successResult=commonPolicyMapper.updateByExampleSelective(commonPolicy,
		 * commonPolicyExample); }
		 */
		response.put("jpjResponse", "01");
		response.put("paymentTrxID", paymentTrxID);
		response.put("policyNo", policyNo);
		String jsonString = new Gson().toJson(response);
		return jsonString;
	}

	@RequestMapping(value = "/newcarUpdateVehInfo", method = RequestMethod.POST)
	private String newcarUpdateVehInfo(HttpServletRequest request, Model model, @RequestParam String paymentTrxID) {

		// Check Policy is canceled of not, if cancel can update the engine no and
		// chassis no else must prompt the user
		// asking "You have to cancel the policy before update, do you want to cancel
		// the policy "

		MotorInsuranceJPJService MotorInsuranceJPJService = new MotorInsuranceJPJService();
		// String test =
		// newCarTransactionalDetailMapper.updateCustomerInfo(newCarTransactionalDetailVO);
		return transactionalDetail(request, model, paymentTrxID);
	}

	private String formatCovargeDateWithSlash(String dateString) {

		String resultDate = "0";
		if (!ServiceValidationUtils.isEmptyStringTrim(dateString)) {
			String day = dateString.substring(0, 2);
			String mon = dateString.substring(2, 4);
			String yyyy = dateString.substring(4, 8);
			resultDate = day + "/" + mon + "/" + yyyy;
		}

		return resultDate;

	}

	private String formatCovargeDateWithoutSlash(String dateString) {

		String resultDate = "0";
		if (!ServiceValidationUtils.isEmptyStringTrim(dateString)) {
			String[] array = dateString.split("\\/");
			String day = array[0];
			String mon = array[1];
			String yyyy = array[2];
			resultDate = day + mon + yyyy;
		}
		return resultDate;

	}

	@RequestMapping(value = "/newcarTxnStatusUpdate", method = RequestMethod.POST)
	@ResponseBody
	private String newcarTxnStatusUpdate(HttpServletRequest request, @RequestParam String paymentTrxID,
			@RequestParam String dspqqid, @RequestParam String status, @RequestParam String remark, Model model)

	{
		 logger.info("Original Status"+dspqqid);logger.info("Original Status"+status);logger.info("Original Status"+remark);
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		/*if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}
*/
		NewCarParamVO newCarParamVO = new NewCarParamVO();
		newCarParamVO.setTransactionId(paymentTrxID);
		NewCarTransactionalDetailVO tranDetail = newCarTransactionalDetailMapper
				.selectTransactionalDetails(newCarParamVO);
		Map<String, String> response = new HashMap<String, String>();

		try {

			 logger.info("Original Status"+tranDetail.getPaymentStatus());

			if (tranDetail.getPaymentStatus().trim().equals(status.trim())) {

				response.put("resCode", "E1");
				response.put("resDesc", "Please choose new payment status");

			}

			else if (ServiceValidationUtils.isEmptyStringTrim(remark.trim())) {

				response.put("resCode", "E2");
				response.put("resDesc", "Please fill in the Remarks");

			}

			else {

				AuditLog auditLog = new AuditLog();
				auditLog.setTransactionId(paymentTrxID);
				auditLog.setActionby(loginUser);
				auditLog.setAction("E");
				auditLog.setRemarks(remark.trim());
				auditLog.setPredata(tranDetail.getPaymentStatus());
				auditLog.setPostdata(status);
				auditLog.setCreatedate(new Date());
				auditLog.setCreatedby((short) 1);
				auditLog.setModuleId(1);

				auditLogMapper.insert(auditLog);

				// Update Payment table
				CommonPayment commonPaymentUpdate = new CommonPayment();
				commonPaymentUpdate.setPmntStatus(status);
				commonPaymentUpdate.setTransactionId(paymentTrxID);
				commonPaymentMapper.updateByPrimaryKeySelective(commonPaymentUpdate);

				logger.info("status" + status);
				logger.info("paymentTrxID" + paymentTrxID);

				// Update Audit Log table

				if (status.equals("S")) {

					// generate policy no

					NciDAO nciDAO = new NciDAOImpl();
					String newPolicyNo = "";
					try {
						newPolicyNo = nciDAO.NCI_policy_Insert(tranDetail.getDspqqid(), tranDetail.getTransactionID());
						response.put("policyNo", newPolicyNo);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					// generate edocument

					if (!ServiceValidationUtils.isEmptyStringTrim(newPolicyNo)) {

						String documenturl = "";
						try

						{

							if ("K".equalsIgnoreCase(newPolicyNo.substring(0, 1))) {
								// documenturl="http://cpf:7011/NEWCARTAKAFUL_EDOCS";
								documenturl = "http://cpf:7011/NEW_CAR_INSURANCE_EDOCS";
							} else if ("V".equalsIgnoreCase(newPolicyNo.substring(0, 1))) {
								// documenturl="http://cpf:7011/NEWCARINSURANCE_EDOCS";
								documenturl = "http://cpf:7011/CPF-DSP-NCT-DOCS";

							}

							QName servicename = new QName("http://motor.wcservices.dsp.etiqa.com/",
									"MotorInsuranceDocGenWSImplService");
							QName portname = new QName("http://motor.wcservices.dsp.etiqa.com/",
									"MotorInsuranceDocGenWSImplPort");
							Service service = Service.create(null, servicename);
							service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
									documenturl + "/MotorInsuranceDocGenWSImplService");
							Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
									Service.Mode.MESSAGE);
							MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
							SOAPMessage request1 = mf.createMessage();
							SOAPPart part = request1.getSOAPPart();
							SOAPEnvelope env = part.getEnvelope();
							SOAPBody body = env.getBody();
							// resp = body;
							SOAPElement operation = body.addChildElement("getStatus", "mot",
									"http://motor.wcservices.dsp.etiqa.com/");
							SOAPElement element_param = operation.addChildElement("QQID");
							element_param.addTextNode(tranDetail.getQqid());
							SOAPElement element_param_lang = operation.addChildElement("langValue");
							element_param_lang.addTextNode("lan_en");

							request1.saveChanges();
							String result = null;

							if (request1 != null) {
								ByteArrayOutputStream baos = null;
								try {
									baos = new ByteArrayOutputStream();
									request1.writeTo(baos);
									result = baos.toString();
								} catch (Exception e) {
								} finally {
									if (baos != null) {
										try {
											baos.close();
										} catch (IOException ioe) {
										}
									}
								}
							}
							logger.info(result + "document insert");
							// resCode=body ;

							SOAPMessage response1 = dispatch.invoke(request1);
							SOAPBody responsebody = response1.getSOAPBody();
							java.util.Iterator otcupdates = responsebody.getChildElements();
							while (otcupdates.hasNext()) {
								SOAPElement otcupdates1 = (SOAPElement) otcupdates.next();

								java.util.Iterator i = otcupdates1.getChildElements();
								while (i.hasNext()) {

									SOAPElement e = (SOAPElement) i.next();
									java.util.Iterator m = e.getChildElements();
									while (m.hasNext()) {
										SOAPElement e2 = (SOAPElement) m.next();

									}
								}
							}

						} catch (Exception e) {
							e.printStackTrace();

						}

					}

					// JPJ send

					// NewCarParamVO newCarParamVO=new NewCarParamVO();
					// newCarParamVO.setTransactionId(paymentTrxID);
					// newCarParamVO.setPolicy(policyNo);
					try {
						// NewCarTransactionalDetailVO
						// tranDetail=newCarTransactionalDetailMapper.selectTransactionalDetails(newCarParamVO);
						tranDetail.setCoverageStartDate(formatCovargeDateWithSlash(tranDetail.getCoverageStartDate()));
						tranDetail.setCoverageEndDate(formatCovargeDateWithSlash(tranDetail.getCoverageEndDate()));

						if (!ServiceValidationUtils.isEmptyStringTrim(newPolicyNo)) {

							String startDate = null;
							String endDate = null;
							if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getCoverageStartDate())) {
								String day = tranDetail.getCoverageStartDate().substring(0, 2);
								String mon = tranDetail.getCoverageStartDate().substring(2, 4);
								String yy = tranDetail.getCoverageStartDate().substring(4, 8);
								startDate = yy + mon + day;
							}
							if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getCoverageEndDate())) {
								String day = tranDetail.getCoverageEndDate().substring(0, 2);
								String mon = tranDetail.getCoverageEndDate().substring(2, 4);
								String yy = tranDetail.getCoverageEndDate().substring(4, 8);
								endDate = yy + mon + day;
							}

							JpjEDocument jpjEDocument = new JpjEDocument();
							if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getVehicleNumber())) {
								jpjEDocument.setVehregno(tranDetail.getVehicleNumber());
							} else {
								jpjEDocument.setVehregno("NA");
							}

							if ("K".equalsIgnoreCase(newPolicyNo.substring(0, 1))) {
								jpjEDocument.setCompcode("96");
							}
							if ("V".equalsIgnoreCase(newPolicyNo.substring(0, 1))) {
								jpjEDocument.setCompcode("14");
							}
							jpjEDocument.setEngineno(tranDetail.getEngineNo());
							jpjEDocument.setChassisno(tranDetail.getChassisNo());
							jpjEDocument.setDoctype("0");
							jpjEDocument.setReasoncode("0");
							jpjEDocument.setIdno(tranDetail.getCustomerNRIC());
							jpjEDocument.setEffectdate(startDate);
							jpjEDocument.setExpirydate(endDate);
							jpjEDocument.setIdno2("NA");
							jpjEDocument.setCovertype("0");
							jpjEDocument.setDocno(newPolicyNo);
							DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
							// Get the date today using Calendar object.
							Date today = Calendar.getInstance().getTime();
							// Using DateFormat format method we can create a string
							// representation of a date with the defined format.
							String reportDate = df.format(today);
							jpjEDocument.setInserttimestamp(reportDate);
							jpjEDocumentMapper.insert(jpjEDocument);

						}
					} catch (Exception e) {

						e.printStackTrace();

					}

					// call ISM request

				} else if (status.equals("F") || status.equals("C")) {

					if (tranDetail.getPaymentStatus().trim().equals("S") && !tranDetail.getPolicyNo().equals(null)
							&& !tranDetail.getPolicyNo().equals("")) {
						// cancel Policy
						try {
							List<CommonPolicy> commonPolicyList = new ArrayList<CommonPolicy>();
							CommonPolicyExample commonPolicyExample = new CommonPolicyExample();
							CommonPolicyExample.Criteria commonPolicy_criteria = commonPolicyExample.createCriteria();
							commonPolicy_criteria.andPolicyNumberEqualTo(tranDetail.getPolicyNo());
							commonPolicy_criteria.andDspQqIdEqualTo(new BigDecimal(dspqqid));
							if ("K".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
								commonPolicy_criteria.andProductCodeEqualTo("NCT");
							}
							if ("V".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
								commonPolicy_criteria.andProductCodeEqualTo("NCI");
							}
							commonPolicy_criteria.andPolicyStatusEqualTo("SUCCESS");
							CommonPolicy commonPolicy = new CommonPolicy();
							commonPolicy.setPolicyStatus("CANCEL");
							int cancelResult = commonPolicyMapper.updateByExampleSelective(commonPolicy,
									commonPolicyExample);

						}

						catch (Exception e) {

							e.printStackTrace();

						}

						// cancel JPJ

						try {

							String startDate = null;
							String endDate = null;
							if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getCoverageStartDate())) {
								String day = tranDetail.getCoverageStartDate().substring(0, 2);
								String mon = tranDetail.getCoverageStartDate().substring(2, 4);
								String yy = tranDetail.getCoverageStartDate().substring(4, 8);
								startDate = yy + mon + day;
							}
							if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getCoverageEndDate())) {
								String day = tranDetail.getCoverageEndDate().substring(0, 2);
								String mon = tranDetail.getCoverageEndDate().substring(2, 4);
								String yy = tranDetail.getCoverageEndDate().substring(4, 8);
								endDate = yy + mon + day;
							}

							JpjEDocument jpjEDocument = new JpjEDocument();
							if (!ServiceValidationUtils.isEmptyStringTrim(tranDetail.getVehicleNumber())) {
								jpjEDocument.setVehregno(tranDetail.getVehicleNumber());
							} else {
								jpjEDocument.setVehregno("NA");
							}

							if ("K".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
								jpjEDocument.setCompcode("96");
							}
							if ("V".equalsIgnoreCase(tranDetail.getPolicyNo().substring(0, 1))) {
								jpjEDocument.setCompcode("14");
							}
							jpjEDocument.setEngineno(tranDetail.getEngineNo());
							jpjEDocument.setChassisno(tranDetail.getChassisNo());
							jpjEDocument.setDoctype("3");
							jpjEDocument.setReasoncode("0");
							jpjEDocument.setIdno(tranDetail.getCustomerNRIC());
							jpjEDocument.setEffectdate(startDate);
							jpjEDocument.setExpirydate(endDate);
							jpjEDocument.setIdno2("NA");
							jpjEDocument.setCovertype("0");
							jpjEDocument.setDocno(tranDetail.getPolicyNo());
							DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
							// Get the date today using Calendar object.
							Date today = Calendar.getInstance().getTime();
							// Using DateFormat format method we can create a string
							// representation of a date with the defined format.
							String reportDate = df.format(today);
							jpjEDocument.setInserttimestamp(reportDate);
							jpjEDocumentMapper.insert(jpjEDocument);

						}

						catch (Exception e) {

							e.printStackTrace();

						}

						// cancel ISM

					}

				}

				response.put("resCode", "00");
				response.put("resDesc", "Payment status updated");

			}

		} catch (Exception e) {

			response.put("resCode", "E3");
			response.put("resDesc", "Error occur during update the payment status");
			e.printStackTrace();

		}

		String jsonString = new Gson().toJson(response);
		return jsonString;
	}

	/* ------------------------------------------------------------------ */
	/* TODO - Resent the documents */
	/*------------------------------------------------------------------- */
	public void generateDocumets(String policyNo, String qqID, String id) {

		logger.info("NciTranReportController<generateDocumets>");

		String redirect = "";

		logger.info("	<policyNo> => " + policyNo);
		logger.info("	<qqID> => " + qqID); // 1915
		logger.info("	<id> => " + id);

		// redirect =
		// "${pageContext.request.contextPath}/newCarDetails/'"+id+'/'+policyNo;
		// redirect = "newCarDetails/"+id+"/"+policyNo;

		// logger.info("Send email for POLICY NO = "+policyNo+" and
		// transaction_id = "+id);

		String documenturl = "";

		if (policyNo.indexOf("V") != -1) {
			documenturl = "http://cpf:7011/NEW_CAR_INSURANCE_EDOCS";
		}

		else {
			documenturl = "http://cpf:7011/CPF-DSP-NCT-DOCS";

		}
		try {
			// http://192.168.0.247:7005/CPF20092016/MotorInsuranceDocGenWSImplService?WSDL
			// URL url = new URL(documenturl+"/MotorQQDataImplService?WSDL");
			QName servicename = new QName("http://motor.wcservices.dsp.etiqa.com/",
					"MotorInsuranceDocGenWSImplService");
			QName portname = new QName("http://motor.wcservices.dsp.etiqa.com/", "MotorInsuranceDocGenWSImplPort");
			Service service = Service.create(null, servicename);
			service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING,
					documenturl + "/MotorInsuranceDocGenWSImplService");
			Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class, Service.Mode.MESSAGE);
			MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
			SOAPMessage request1 = mf.createMessage();
			SOAPPart part = request1.getSOAPPart();
			SOAPEnvelope env = part.getEnvelope();
			SOAPBody body = env.getBody();
			// resp = body;
			SOAPElement operation = body.addChildElement("getStatus", "mot", "http://motor.wcservices.dsp.etiqa.com/");
			SOAPElement element_param = operation.addChildElement("QQID");
			element_param.addTextNode(qqID);
			SOAPElement element_param_lang = operation.addChildElement("langValue");
			element_param_lang.addTextNode("lan_en");

			request1.saveChanges();
			String result = null;

			if (request1 != null) {
				ByteArrayOutputStream baos = null;
				try {
					baos = new ByteArrayOutputStream();
					request1.writeTo(baos);
					result = baos.toString();
				} catch (Exception e) {
				} finally {
					if (baos != null) {
						try {
							baos.close();
						} catch (IOException ioe) {
						}
					}
				}
			}
			// logger.info(result+"document insert");
			// resCode=body ;

			SOAPMessage response1 = dispatch.invoke(request1);
			SOAPBody responsebody = response1.getSOAPBody();
			logger.info("<generateDocumets>regenerate doc Motor" + responsebody);

		} catch (Exception e) {

			// emailStatus = "Fail";
			// resp = resp + "{\"errorCode\":\""+e.getMessage()+"\"}";
			e.printStackTrace();
		}

		// logger.info(sendEmailStatus);
		/*
		 * ReportDAO dao = new ReportDAOImpl(); List<TransactionalReport> TranDetail =
		 * dao.getTransactionDetail(id); request.setAttribute("data", TranDetail);
		 * request.setAttribute("statusEm", emailStatus)
		 */;

		// forward(request, response, redirect);

	}

	/* ------------------------------------------------------------------ */
	/* End - Resent the documents */
	/*------------------------------------------------------------------- */

}
