package com.spring.admin;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import com.etiqa.DAO.DashboardDAO;
import com.etiqa.DAO.UserValidation_DAO;
import com.etiqa.admin.User;
import com.etiqa.utils.SecurityUtil;
import com.spring.VO.DashBoard;
import com.spring.VO.DashBoard_BSummary;
import com.spring.VO.Menu;
import com.spring.admin.UAMControllers.LoginController;
import com.spring.mapper.DashBoardMapper;
import com.spring.mapper.uam.UserProfileMapper;
import com.spring.service.MenuService;
import com.spring.utils.Logouturl;
import com.spring.utils.ServiceValidationUtils;

@Controller
public class DashBoardController {
	final static Logger logger = Logger.getLogger(DashBoardController.class);

	@Autowired
	MenuService menuService;
	DashBoardMapper dashBoardMapper;
	UserProfileMapper userProfileMapper;

	@Autowired
	DashboardDAO dashboardDAO;

	@Autowired
	public DashBoardController(DashBoardMapper dashBoardMapper, UserProfileMapper userProfileMapper) {
		this.dashBoardMapper = dashBoardMapper;
		this.userProfileMapper = userProfileMapper;
	}

	@RequestMapping(value = "/getDashboard")
	public String getDashboard(HttpServletRequest request, Model model, HttpServletResponse response) {

		HttpSession session = request.getSession();

		String str_selectchart = "";

		DashBoard dashBoard = new DashBoard();
		DashBoard_BSummary dashBoardBM = new DashBoard_BSummary();

		String DataListMIPerformance = "";
		String DataListHOHHPerformance = "";
		String DataListWTCPerformance = "";
		String DataListTLPerformance = "";

		// Added by Pramaiyan Start
		String userId = null;
		String role = null;
		String ModuleId = null;
		String userLogId = null;
		List<Menu> firstList = new ArrayList<Menu>();
		List<Menu> secondList = new ArrayList<Menu>();
		List<Menu> thirdList = new ArrayList<Menu>();
		List<Menu> fourthList = new ArrayList<Menu>();
		String uamReqParam = request.getParameter("id");
		session.setAttribute("uamReqParamSession", uamReqParam);
		logger.info("uamReqParam     :" + uamReqParam);

		session.setAttribute("dsp_getDashboard", "dsp_getDashboard");

		logger.info("Encript UAM_Req_Param Outer >>>> " + uamReqParam);
		if (uamReqParam != null) {
			logger.info("Encript UAM_Req_Param  In >>>> " + uamReqParam);
			String decryptMsg = SecurityUtil.decriptString(uamReqParam);
			logger.info("Encript UAM_Req_Param " + decryptMsg);
			// split the String for getting userId,roleId,ModuleId
			if (decryptMsg != null) {
				String[] items = decryptMsg.split("\\$");
				userId = items[0].toString();
				role = items[1].toString();
				ModuleId = items[2].toString();
				userLogId = items[3].toString();
				session.setAttribute("dspadminModuleId", ModuleId);
				session.setAttribute("userLogId", userLogId);
			}

		} else {
			logger.info("Encript UAM_Req_Param else  >>>> " + uamReqParam);
		}

		logger.info("Original " + role);

		if (ServiceValidationUtils.isEmptyStringTrim(uamReqParam)) {
			try {
				logger.info("log out from dspadmin dashbord   ");
				response.sendRedirect(Logouturl.UAM_LOGIN_URL_LINK);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		User muser = new User();
		muser = UserValidation_DAO.getvalidateByUserID(userId);

		session.setAttribute("FullName", muser.getFullName());
		session.setAttribute("logedUser", userId);
		int uId = muser.getID();
		session.setAttribute("user", String.valueOf(uId));
		session.setAttribute("actionby", muser.getUsername());
		logger.info("Full Name  : " + muser.getFullName());

		//String loginUser = (String) session.getAttribute("FullName");
		String loginUser = (String) session.getAttribute("userloginName");
		logger.info("+++++###################+++++Dhana printing to get the fullname of user:"+loginUser);
		

		// get Menu List from Rest service start
		logger.info("Calling DashBoard Controller Pramaiyan");
		Map<String, List<Menu>> map = getMenu(role, ModuleId);

		for (Entry<String, List<Menu>> entry : map.entrySet()) {
			String key = entry.getKey();
			List<Menu> values = entry.getValue();

			if (key == "1") {
				firstList.addAll(values);
			}
			if (key == "2") {
				secondList.addAll(values);
			}
			if (key == "3") {
				thirdList.addAll(values);
			}
			if (key == "4") {
				fourthList.addAll(values);
			}
		}

		session.setAttribute("level_1", firstList);
		session.setAttribute("level_2", secondList);
		session.setAttribute("level_3", thirdList);
		session.setAttribute("level_4", fourthList);

		// End by Pramaiyan

		try {

			DateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY");
			Date date = new Date();
			String strDate = "";
			str_selectchart = request.getParameter("business_summary");
			if (request.getParameter("fromdate") != null) {
				strDate = request.getParameter("fromdate");
			} else {
				strDate = dateFormat.format(date);
			}

			str_selectchart = "business_summary";
			dashBoardBM.setFromdate(strDate);
			dashBoardBM.setSelectvalue(str_selectchart);
			/* dashBoardMapper.select_B_Sum(dashBoardBM); */
			model.addAttribute("selectvalue", dashBoardBM.getSelectvalue());
			model.addAttribute("fromdate", strDate);
			// model.addAttribute("dashBoard", dashBoardBM);
			logger.info("In Business Summary  : " + str_selectchart);

			// Current Month Performace Trend
			// DataListMIPerformance = dashboardDAO.getMIPerformance(strDate);
			// DataListHOHHPerformance = dashboardDAO.getHOHHPerformance(strDate);
			// DataListWTCPerformance = dashboardDAO.getWTCPerformance(strDate);
			// DataListTLPerformance = dashboardDAO.getTLPerformance(strDate);

		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Error");
		}

		// model.addAttribute("DataListMIPerformance", DataListMIPerformance);
		// model.addAttribute("DataListWTCPerformance", DataListWTCPerformance);
		// model.addAttribute("DataListHOHHPerformance", DataListHOHHPerformance);
		// model.addAttribute("DataListTLPerformance", DataListTLPerformance);
		return "admin-dashboard";
	}
@RequestMapping(value = "cwp/getCwpDashboard")
	public String getCwpDashboard(HttpServletRequest request, Model model, HttpServletResponse response) {

		HttpSession session = request.getSession();

		String str_selectchart = "";

		DashBoard dashBoard = new DashBoard();
		DashBoard_BSummary dashBoardBM = new DashBoard_BSummary();

		String DataListMIPerformance = "";
		String DataListHOHHPerformance = "";
		String DataListWTCPerformance = "";
		String DataListTLPerformance = "";

		// Added by Pramaiyan Start
		String userId = null;
		String role = null;
		String ModuleId = null;
		String userLogId = null;
		List<Menu> firstList = new ArrayList<Menu>();
		List<Menu> secondList = new ArrayList<Menu>();
		List<Menu> thirdList = new ArrayList<Menu>();
		List<Menu> fourthList = new ArrayList<Menu>();
		String uamReqParam = request.getParameter("id");
		session.setAttribute("uamReqParamSession", uamReqParam);
		logger.info("uamReqParam     :" + uamReqParam);

		session.setAttribute("dsp_getDashboard", "dsp_getDashboard");

		logger.info("Encript UAM_Req_Param Outer >>>> " + uamReqParam);
		if (uamReqParam != null) {
			logger.info("Encript UAM_Req_Param  In >>>> " + uamReqParam);
			String decryptMsg = SecurityUtil.decriptString(uamReqParam);
			logger.info("Encript UAM_Req_Param " + decryptMsg);
			// split the String for getting userId,roleId,ModuleId
			if (decryptMsg != null) {
				String[] items = decryptMsg.split("\\$");
				userId = items[0].toString();
				role = items[1].toString();
				ModuleId = items[2].toString();
				userLogId = items[3].toString();
				session.setAttribute("dspadminModuleId", ModuleId);
				session.setAttribute("userLogId", userLogId);
			}

		} else {
			logger.info("Encript UAM_Req_Param else  >>>> " + uamReqParam);
		}

		logger.info("Original " + role);

		if (ServiceValidationUtils.isEmptyStringTrim(uamReqParam)) {
			try {
				logger.info("log out from dspadmin dashbord   ");
				response.sendRedirect(Logouturl.UAM_LOGIN_URL_LINK);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		User muser = new User();
		muser = UserValidation_DAO.getvalidateByUserID(userId);

		session.setAttribute("FullName", muser.getFullName());
		session.setAttribute("logedUser", userId);
		int uId = muser.getID();
		session.setAttribute("user", String.valueOf(uId));
		session.setAttribute("actionby", muser.getUsername());
		logger.info("Full Name  : " + muser.getFullName());

		String loginUser = (String) session.getAttribute("FullName");

		// get Menu List from Rest service start
		logger.info("Calling DashBoard Controller Pramaiyan");
		Map<String, List<Menu>> map = getMenu(role, ModuleId);

		for (Entry<String, List<Menu>> entry : map.entrySet()) {
			String key = entry.getKey();
			List<Menu> values = entry.getValue();

			if (key == "1") {
				firstList.addAll(values);
			}
			if (key == "2") {
				secondList.addAll(values);
			}
			if (key == "3") {
				thirdList.addAll(values);
			}
			if (key == "4") {
				fourthList.addAll(values);
			}
		}

		session.setAttribute("level_1", firstList);
		session.setAttribute("level_2", secondList);
		session.setAttribute("level_3", thirdList);
		session.setAttribute("level_4", fourthList);

		// End by Pramaiyan

		try {

			DateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY");
			Date date = new Date();
			String strDate = "";
			str_selectchart = request.getParameter("business_summary");
			if (request.getParameter("fromdate") != null) {
				strDate = request.getParameter("fromdate");
			} else {
				strDate = dateFormat.format(date);
			}

			str_selectchart = "business_summary";
			dashBoardBM.setFromdate(strDate);
			dashBoardBM.setSelectvalue(str_selectchart);
			/* dashBoardMapper.select_B_Sum(dashBoardBM); */
			model.addAttribute("selectvalue", dashBoardBM.getSelectvalue());
			model.addAttribute("fromdate", strDate);
			// model.addAttribute("dashBoard", dashBoardBM);
			logger.info("In Business Summary  : " + str_selectchart);

			// Current Month Performace Trend
			// DataListMIPerformance = dashboardDAO.getMIPerformance(strDate);
			// DataListHOHHPerformance = dashboardDAO.getHOHHPerformance(strDate);
			// DataListWTCPerformance = dashboardDAO.getWTCPerformance(strDate);
			// DataListTLPerformance = dashboardDAO.getTLPerformance(strDate);

		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Error");
		}

		// model.addAttribute("DataListMIPerformance", DataListMIPerformance);
		// model.addAttribute("DataListWTCPerformance", DataListWTCPerformance);
		// model.addAttribute("DataListHOHHPerformance", DataListHOHHPerformance);
		// model.addAttribute("DataListTLPerformance", DataListTLPerformance);
		return "cwp/admin-phone";
	}

	@RequestMapping(value = "/reload_dashboard")
	public String reload_dashboard(HttpServletRequest request, Model model, HttpServletResponse response) {

		HttpSession session = request.getSession();

		String str_selectchart = "";

		DashBoard dashBoard = new DashBoard();
		DashBoard_BSummary dashBoardBM = new DashBoard_BSummary();

		String DataListMIPerformance = "";
		String DataListHOHHPerformance = "";
		String DataListWTCPerformance = "";
		String DataListTLPerformance = "";
		//String DataListCCPerformance = "";

		try {

			DateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY");
			Date date = new Date();
			String strDate = "";
			str_selectchart = request.getParameter("business_summary");
			if (request.getParameter("fromdate") != null) {
				strDate = request.getParameter("fromdate");
			} else {
				strDate = dateFormat.format(date);
			}

			if (request.getParameter("selectchart") != null) {
				str_selectchart = request.getParameter("selectchart");
				logger.info(" We are in IF : " + request.getParameter("selectchart"));
			} else {
				str_selectchart = "business_summary";
				logger.info(" We are in ELSE : " + str_selectchart);
			}

			if (str_selectchart.equals("business_summary")) {
				dashBoardBM.setFromdate(strDate);
				dashBoardBM.setSelectvalue(str_selectchart);
				dashBoardMapper.select_B_Sum(dashBoardBM);
				model.addAttribute("selectvalue", dashBoardBM.getSelectvalue());
				model.addAttribute("fromdate", strDate);
				model.addAttribute("dashBoard", dashBoardBM);
				logger.info("In Business Summary  : " + str_selectchart);
			} else {
				dashBoard.setFromdate(strDate);
				dashBoard.setSelectvalue(str_selectchart);
				dashBoardMapper.selectDashBoard1(dashBoard);
				model.addAttribute("selectvalue", dashBoard.getSelectvalue());
				model.addAttribute("fromdate", strDate);
				model.addAttribute("dashBoard", dashBoard);
				logger.info("dashBoard selected  : " + str_selectchart);
			}

			// Current Month Performace Trend
			DataListMIPerformance = dashboardDAO.getMIPerformance(strDate);
			DataListHOHHPerformance = dashboardDAO.getHOHHPerformance(strDate);
			//DataListCCPerformance = dashboardDAO.getCCPerformance(strDate);
			DataListWTCPerformance = dashboardDAO.getWTCPerformance(strDate);
			DataListTLPerformance = dashboardDAO.getTLPerformance(strDate);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Error");
		}

		model.addAttribute("DataListMIPerformance", DataListMIPerformance);
		model.addAttribute("DataListWTCPerformance", DataListWTCPerformance);
		model.addAttribute("DataListHOHHPerformance", DataListHOHHPerformance);
		model.addAttribute("DataListTLPerformance", DataListTLPerformance);
		//model.addAttribute("DataListCCPerformance", DataListCCPerformance);
		return "admin-dashboard";
	}

	@SuppressWarnings("unchecked")
	private Map<String, List<Menu>> getMenu(String roleid, String moduleId) {
		logger.info("roleid >>>" + roleid);
		logger.info("moduleId >>>> " + moduleId);
		Menu m = new Menu();
		RestTemplate rt = new RestTemplate();
		MultiValueMap<String, Object> mapInput = new LinkedMultiValueMap<String, Object>();
		Map<String, List<Menu>> resp = new HashMap<String, List<Menu>>();
		// String roleid="6";
		String delimiters = "$";
		// String moduleId="1";
		try {
			resp = menuService.getMenu(roleid + delimiters + moduleId);
			logger.info(resp);
			m.setModuleId(Integer.parseInt(moduleId));
			List<Menu> firstList = resp.get("first");
			List<Menu> secondList = resp.get("second");
			List<Menu> thirdList = resp.get("third");
			List<Menu> fourthList = resp.get("fourth");

			resp.put("1", firstList);
			resp.put("2", secondList);
			resp.put("3", thirdList);
			resp.put("4", fourthList);

		} catch (Exception e) {
			e.printStackTrace();
			logger.info("no Connection");
		}
		return resp;
	}

}