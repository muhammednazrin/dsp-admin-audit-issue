package com.spring.admin;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.etiqa.utils.FormatDates;
import com.etiqa.utils.convertBytes;
import com.spring.VO.Approval;
import com.spring.VO.ApprovalExample;
import com.spring.VO.ApprovalLog;
import com.spring.VO.SysParam;
import com.spring.VO.SysParamExample;
import com.spring.VO.TravEzSetup;
import com.spring.VO.TravEzSetupExample;
import com.spring.mapper.ApprovalLogMapper;
import com.spring.mapper.ApprovalMapper;
import com.spring.mapper.SysParamMapper;
import com.spring.mapper.TravEzSetupMapper;

@Controller
public class TravelEzyController {
	TravEzSetupMapper travEzSetupMapper;
	ApprovalMapper approvalMapper;
	ApprovalLogMapper approvalLogMapper;
	SysParamMapper sysParamMapper;

	@Autowired
	public TravelEzyController(TravEzSetupMapper travEzSetupMapper, SysParamMapper sysParamMapper,
			ApprovalMapper approvalMapper, ApprovalLogMapper approvalLogMapper) {

		this.travEzSetupMapper = travEzSetupMapper;
		this.approvalMapper = approvalMapper;
		this.approvalLogMapper = approvalLogMapper;
		this.sysParamMapper = sysParamMapper;

	}

	/*----------START - Product Info----------*/
	@RequestMapping("/TravelEzyProductInfo")
	public String travelEzyProductInfo(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("Travel Ezy Product Info ");

		// list of companies
		List<SysParam> companyList;
		SysParamExample sysParamExample = new SysParamExample();
		SysParamExample.Criteria sysParaminfoCriteriaExist = sysParamExample.createCriteria();
		sysParaminfoCriteriaExist.andParamEqualTo("Company");
		companyList = sysParamMapper.selectByExample(sysParamExample);

		String strCompany = companyList.get(0).getName().toString().toLowerCase();
		// check for parameter
		if (request.getParameter("company") != null) {
			strCompany = request.getParameter("company").toLowerCase();
		}
		String prodCode = strCompany.equalsIgnoreCase("I") ? "BPT" : "TZT";
		System.out.println("strCompany : " + strCompany);
		System.out.println("prodCode : " + prodCode);

		// list of product
		List<TravEzSetup> productList;
		TravEzSetupExample travEzSetupExample = new TravEzSetupExample();
		TravEzSetupExample.Criteria productParamCriteria = travEzSetupExample.createCriteria();
		productParamCriteria.andCompanyIdEqualTo(strCompany);
		productParamCriteria.andProductCodeEqualTo(prodCode);
		productList = travEzSetupMapper.selectByExample(travEzSetupExample);

		model.addAttribute("companyList", companyList);
		model.addAttribute("company", strCompany);

		FormatDates formatDate = new FormatDates();

		if (!productList.isEmpty()) {
			String productCode = productList.get(0).getProductCode().toString();
			String discount = productList.get(0).getDiscount().toString();
			String stampDuty = productList.get(0).getStampDuty().toString();
			String poi = productList.get(0).getPoi().toString();
			String gstEffDate = formatDate.dateToString(productList.get(0).getGstEffDate());
			String sstEffDate = formatDate.dateToString(productList.get(0).getSstEffDate());
			String gst = productList.get(0).getGst().toString();
			String sst = productList.get(0).getSst().toString();
			String premiumRate = productList.get(0).getPremiumRate().toString();
			String version = productList.get(0).getVersion().toString();

			model.addAttribute("productCode", productCode);
			model.addAttribute("discount", discount);
			model.addAttribute("stampDuty", stampDuty);
			model.addAttribute("poi", poi);
			model.addAttribute("gstEffDate", gstEffDate);
			model.addAttribute("sstEffDate", sstEffDate);
			model.addAttribute("gst", gst);
			model.addAttribute("sst", sst);
			model.addAttribute("premiumRate", premiumRate);
			model.addAttribute("version", version);

			String strExist = null;

			if (productCode != null) {
				strExist = "1";
			} else {
				strExist = "0";
			}

			model.addAttribute("exist", strExist);
		} else {
			System.out.println("Product List Size Error");
		}

		return "/products/travelEzy/TravelEzyAdmin";

	}
	/*----------END - Product Info----------*/

	/*----------START - Product Edit----------*/
	@RequestMapping("/updateTravelEzyProduct")
	public String updateTravelEzyProduct(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("update Travel Ezy Product");
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Travel Ezy Controller loginUser" + loginUser);

		TravEzSetupExample travEzSetupExample = new TravEzSetupExample();
		TravEzSetup travEzSetup = new TravEzSetup();
		TravEzSetupExample.Criteria productParamCriteria = travEzSetupExample.createCriteria();
		productParamCriteria.andCompanyIdEqualTo(request.getParameter("company").toLowerCase());
		productParamCriteria.andProductCodeEqualTo(request.getParameter("productCode"));
		List<TravEzSetup> productList = new ArrayList<>();

		Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		travEzSetup.setCompanyId(request.getParameter("company").toLowerCase());
		travEzSetup.setProductCode(request.getParameter("productCode"));

		FormatDates formatDate = new FormatDates();
		Date gstEffDate = formatDate.stringToDate(request.getParameter("gstEffDate"));
		Date sstEffDate = formatDate.stringToDate(request.getParameter("sstEffDate"));

		BigDecimal discount = new BigDecimal(request.getParameter("discount"));
		travEzSetup.setDiscount(discount);
		BigDecimal premiumRate = new BigDecimal(request.getParameter("premiumRate"));
		travEzSetup.setPremiumRate(premiumRate);
		BigDecimal stampDuty = new BigDecimal(request.getParameter("stampDuty"));
		travEzSetup.setStampDuty(stampDuty);
		BigDecimal poi = new BigDecimal(request.getParameter("poi"));
		travEzSetup.setPoi(poi);
		travEzSetup.setGstEffDate(gstEffDate);
		travEzSetup.setSstEffDate(sstEffDate);
		travEzSetup.setGst((short) Integer.parseInt(request.getParameter("gst")));
		travEzSetup.setSst((short) Integer.parseInt(request.getParameter("sst")));

		int ver = Integer.parseInt(request.getParameter("version")) + 1;
		travEzSetup.setVersion((short) ver);

		String strExist = request.getParameter("exist");

		// approval log
		convertBytes process = new convertBytes();

		List<TravEzSetup> listTravEzProductChange = new ArrayList<>();
		listTravEzProductChange.add(travEzSetup);

		try {
			byte[] changeBlob = process.convertToBytes(listTravEzProductChange);

			ApprovalExample approvalExample = new ApprovalExample();
			com.spring.VO.ApprovalExample.Criteria createCriteriaApprovalExample = approvalExample.createCriteria();
			createCriteriaApprovalExample.andDescriptionEqualTo("TRAVEL EZY - CHANGE PRODUCT INFORMATION");
			List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

			System.out.println("TRAVEZY change Data" + changeBlob);

			BigDecimal approvalProdId = new BigDecimal(0);
			if (!listapproval.isEmpty()) {
				approvalProdId = listapproval.get(0).getId();
			}

			int maker = Integer.parseInt(loginUser);

			// setting all new change value to ApprovalLog POJA and Insert to ApprovalLog
			// table
			ApprovalLog approvalLog = new ApprovalLog();
			approvalLog.setNewContent(changeBlob);
			approvalLog.setApprovalId((short) approvalProdId.intValue()); // from Approval table this value[2] based on
																			// menu in feature
			approvalLog.setMaker((short) maker); // session login value
			// approvalLog.setChecker((short)1); //while insert record no need checker value
			approvalLog.setStatus("1");
			approvalLog.setCreateDate(new Date());
			approvalLog.setUpdateDate(new Date());
			approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));

			if (strExist.trim().equals("1")) {
				productList = travEzSetupMapper.selectByExample(travEzSetupExample);
				byte[] originalBlob = process.convertToBytes(productList);
				System.out.println("TRAVEZY ORI Data" + originalBlob);
				approvalLog.setOriginalContent(originalBlob);
				travEzSetup.setModifiedDt(timestamp);
				travEzSetup.setModifiedBy(loginUser);
				// int travEzSetupMapperUpdate1 =
				// travEzSetupMapper.updateByExampleSelective(travEzSetup,
				// travEzSetupExample);
				// System.out.println("Updating Product " + travEzSetupMapperUpdate1);
				// model.addAttribute("SuccessMsg", "Update data successfully");
			} else {
				travEzSetup.setCreatedDt(timestamp);
				travEzSetup.setCreatedBy(loginUser);
				// travEzSetupMapper.insert(travEzSetup);
				// System.out.println("Adding Product");
				// model.addAttribute("SuccessMsg", "Data Added Successfully!");
			}
			int status = approvalLogMapper.insert(approvalLog);
			model.addAttribute("SuccessMsg", "Submitted data successfully");

		} catch (IOException ex) {
			ex.printStackTrace();
		}
		// approval log

		String returnURL = null;

		returnURL = travelEzyProductInfo(request, response, model);
		return returnURL;

	}
	/*----------END - Product Edit----------*/

	/*----------START - Company Index Change----------*/
	@RequestMapping("/tezCompanyIndexChange")
	public String tezCompanyIndexChange(HttpServletRequest request, HttpServletResponse response, Model model) {

		System.out.println("tezCompanyIndexChange");

		model.addAttribute("company", request.getParameter("selectedCompany"));

		return "redirect: TravelEzyProductInfo";

	}
	/*----------END - Company Index Change----------*/
	/*------------------------------------------------------END - Occupation / System Setup Info---------------------------------------------------------*/
}