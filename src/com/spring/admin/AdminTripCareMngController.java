package com.spring.admin;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.etiqa.utils.FormatDates;
import com.etiqa.utils.convertBytes;
import com.spring.VO.ApprovalLog;
import com.spring.VO.ApprovalLogExample;
import com.spring.VO.DomesticPremium;
import com.spring.VO.DomesticPremiumExample;
import com.spring.VO.InterPremium;
import com.spring.VO.InterPremiumExample;
import com.spring.VO.TC360Param;
import com.spring.VO.TC360ParamExample;
import com.spring.VO.TCProdMngPojo;
import com.spring.VO.TCSysSetup;
import com.spring.VO.TCSysSetupExample;
import com.spring.mapper.ApprovalLogMapper;
import com.spring.mapper.ApprovalMapper;
import com.spring.mapper.CommonTblUnderwritingQAMapper;
import com.spring.mapper.DomesticPremiumMapper;
import com.spring.mapper.InterPremiumMapper;
import com.spring.mapper.TC360ParamMapper;
import com.spring.mapper.TCSysSetupMapper;

@Controller
public class AdminTripCareMngController {

	CommonTblUnderwritingQAMapper commonTblUnderwritingQAMapper;
	ApprovalMapper approvalMapper;
	ApprovalLogMapper approvalLogMapper;
	TC360ParamMapper tripCareParamMapper;
	TCSysSetupMapper tcSysSetupMapper;
	InterPremiumMapper interPremiumMapper;
	DomesticPremiumMapper domesticPremiumMapper;

	@Autowired
	public AdminTripCareMngController(CommonTblUnderwritingQAMapper commonTblUnderwritingQAMapper,
			ApprovalMapper approvalMapper, ApprovalLogMapper approvalLogMapper, TC360ParamMapper tripCareParamMapper,
			TCSysSetupMapper tcSysSetupMapper, InterPremiumMapper interPremiumMapper,
			DomesticPremiumMapper domesticPremiumMapper) {
		this.commonTblUnderwritingQAMapper = commonTblUnderwritingQAMapper;
		this.approvalMapper = approvalMapper;
		this.approvalLogMapper = approvalLogMapper;
		this.tripCareParamMapper = tripCareParamMapper;
		this.tcSysSetupMapper = tcSysSetupMapper;
		this.interPremiumMapper = interPremiumMapper;
		this.domesticPremiumMapper = domesticPremiumMapper;
	}

	// Display Original and Change Data Functionality

	@RequestMapping(value = "/approvalTCProductRate", method = RequestMethod.GET)
	public String approvalTCProductRate(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Request " + request.getParameter("id"));
		ApprovalLogExample approvalexample = new ApprovalLogExample();
		com.spring.VO.ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();
		Integer testint = new Integer(request.getParameter("id"));
		createCriteria.andIdEqualTo(testint.shortValue());
		List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
		System.out.println("   list size in approval list  :::   " + listapprovallog.size());

		FormatDates formatDate = new FormatDates();
		try {
			// original data
			if (!listapprovallog.isEmpty()) {

				System.out.println("  called ..   " + listapprovallog.get(0).getNote());

				if (listapprovallog.get(0).getNote().equalsIgnoreCase("Y")) {
					System.out.println("  called here...");
					convertBytes process = new convertBytes();
					Object originalData = process.convertFromBytes(listapprovallog.get(0).getOriginalContent());
					List<TCSysSetup> listOriginalData;

					// object convert into AgentProdMap
					if (originalData != null) {
						listOriginalData = (List<TCSysSetup>) originalData;
						System.out.println(" Original Data List Size ::::::         " + listOriginalData.size());

						TCProdMngPojo tcSysSetup = new TCProdMngPojo();
						tcSysSetup.setDiscount(listOriginalData.get(0).getStaffDiscPercent().toString());
						tcSysSetup.setGstEffDate(formatDate.dateToString(listOriginalData.get(0).getGstEffDate()));
						tcSysSetup.setGst(listOriginalData.get(0).getGst().toString());
						tcSysSetup.setSstEffDate(formatDate.dateToString(listOriginalData.get(0).getSstEffDate()));
						tcSysSetup.setSst(listOriginalData.get(0).getSst().toString());
						tcSysSetup.setStampDuty(listOriginalData.get(0).getStampDuty().toString());

						List<TCProdMngPojo> listOriginalTCRates = new ArrayList<>();
						listOriginalTCRates.add(tcSysSetup);
						model.addAttribute("listOriginalTCProductRate", listOriginalTCRates);
						model.addAttribute("tcprodtype", "0");
					}

				} else {
					System.out.println("   travel type  :::::::::         " + listapprovallog.get(0).getNote());

					if (listapprovallog.get(0).getNote().equalsIgnoreCase("INT")) {
						convertBytes process = new convertBytes();
						Object originalData = process.convertFromBytes(listapprovallog.get(0).getOriginalContent());

						List<InterPremium> listOriginalDataInterPremium = (List<InterPremium>) originalData;
						System.out
								.println("Original List Size &&&&&&&&&        " + listOriginalDataInterPremium.size());

						for (int t = 0; t < listOriginalDataInterPremium.size(); t++) {

							System.out.println("Original Product Rate  1   "
									+ listOriginalDataInterPremium.get(t).getDaysRangeKeycode());
							System.out.println("Original Product Rate  1   "
									+ listOriginalDataInterPremium.get(t).getPremiumValue());
							System.out.println(
									"Original Product Rate  1   " + listOriginalDataInterPremium.get(t).getPlanCode());

						}
						model.addAttribute("listOriginalTCProductRate", listOriginalDataInterPremium);
					} else if (listapprovallog.get(0).getNote().equalsIgnoreCase("D")) {

						convertBytes process = new convertBytes();
						Object originalData = process.convertFromBytes(listapprovallog.get(0).getOriginalContent());
						List<DomesticPremium> listOriginalDataDomPremium = (List<DomesticPremium>) originalData;
						System.out.println("Original List Size &&&&&&&&&        " + listOriginalDataDomPremium.size());

						for (int t = 0; t < listOriginalDataDomPremium.size(); t++) {

							System.out.println("Original Product Rate  1   "
									+ listOriginalDataDomPremium.get(t).getDaysRangeKeycode());
							System.out.println("Original Product Rate  1   "
									+ listOriginalDataDomPremium.get(t).getPremiumValue());
							System.out.println(
									"Original Product Rate  1   " + listOriginalDataDomPremium.get(t).getPlanCode());

						}
						model.addAttribute("listOriginalTCProductRate", listOriginalDataDomPremium);
					}
				}
				// inner if
			} // outer if

			// Change Rules Data for MI
			if (!listapprovallog.isEmpty()) {

				if (listapprovallog.get(0).getNote().equalsIgnoreCase("Y")) {
					convertBytes process = new convertBytes();
					Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
					List<TCSysSetup> listChangeData;

					// object convert into AgentProdMap
					if (changeData != null) {
						listChangeData = (List<TCSysSetup>) changeData;
						System.out.println("ChangeData List Size ------------      " + listChangeData.size());

						TCProdMngPojo tcSysSetup = new TCProdMngPojo();
						tcSysSetup.setDiscount(listChangeData.get(0).getStaffDiscPercent().toString());
						tcSysSetup.setGstEffDate(formatDate.dateToString(listChangeData.get(0).getGstEffDate()));
						tcSysSetup.setGst(listChangeData.get(0).getGst().toString());
						tcSysSetup.setSstEffDate(formatDate.dateToString(listChangeData.get(0).getSstEffDate()));
						tcSysSetup.setSst(listChangeData.get(0).getSst().toString());
						tcSysSetup.setStampDuty(listChangeData.get(0).getStampDuty().toString());

						List<TCProdMngPojo> listChangeTCRates = new ArrayList<>();
						listChangeTCRates.add(tcSysSetup);
						model.addAttribute("listChangeTCProductRate", listChangeTCRates);
						model.addAttribute("tcprodtype", "0");
					}

				} else {
					convertBytes process = new convertBytes();
					Object listChangeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());

					List<TCProdMngPojo> listChangeDataProd = (List<TCProdMngPojo>) listChangeData;
					System.out.println("ChangeData List Size  ##########       " + listChangeDataProd.size());

					for (int t = 0; t < listChangeDataProd.size(); t++) {

						System.out.println("Change Product Rate  2   " + listChangeDataProd.get(t).getRecIdDomestic());
						System.out.println(
								"Change Product Rate  2   " + listChangeDataProd.get(t).getDaysRangeKeycodeDomestic());
						System.out.println(
								"Change Product Rate  2   " + listChangeDataProd.get(t).getPremiumValDomestic());

					}

					model.addAttribute("listChangeTCProductRate", listChangeDataProd);
					model.addAttribute("tcprodtype", listChangeDataProd.get(0).getProdType());
					System.out.println("----------------------------- 1111" + listChangeDataProd.get(0).getProdType());

				}
			}
		} // try end
		catch (Exception e) {
			System.out.println("Exception in Convert Object to Byte" + e);
		}

		return "agent-product-approval";
	}

	// Display Original and Change Data Functionality
	// Show TripCare Product Rate Change Approval Function

	@RequestMapping(value = "/approvalTCProductRateChange", method = RequestMethod.GET)
	public String approvalTCProductRateChange(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		try {
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			// *************Update the new change in Product Rate in original table
			// ********************

			String aid = request.getParameter("approvalLogId");
			System.out.println("aid >>>>>" + aid);
			ApprovalLogExample approvalexample = new ApprovalLogExample();
			ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();

			if (null != aid && !aid.isEmpty()) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
				System.out.println("   list size in approval list  :::   " + listapprovallog.size());

				// Change Data
				if (!listapprovallog.isEmpty()) {
					String status = listapprovallog.get(0).getStatus();
					short alogId = listapprovallog.get(0).getId();
					System.out.println("Status  :  " + status);
					System.out.println("Approvallog Id  :  " + alogId);
					convertBytes process = new convertBytes();

					if (listapprovallog.get(0).getNote().equalsIgnoreCase("Y")) {

						System.out.println("Product Change Data updated .............   " + alogId);
						if (!aid.isEmpty()) {
							testint = new Integer(aid);
							createCriteria.andIdEqualTo(testint.shortValue());
							listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
							System.out.println("   list size in approval list  :::   " + listapprovallog.size());
							// Change Data
							if (!listapprovallog.isEmpty()) {
								status = listapprovallog.get(0).getStatus();
								alogId = listapprovallog.get(0).getId();
								System.out.println("Status  :  " + status);
								System.out.println("Approvallog Id  :  " + alogId);

								process = new convertBytes();
								Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
								List<TCSysSetup> listChangeDataTripCare;
								// object convert into AgentProdMap
								if (changeData != null) {
									listChangeDataTripCare = (List<TCSysSetup>) changeData;
									System.out.println("  list agent prod map size ::::::         "
											+ listChangeDataTripCare.size());

									List<Object> companyList = new ArrayList<>();
									companyList.add("I");
									companyList.add("T");

									List<Object> productList = new ArrayList<>();
									productList.add("TCI");
									productList.add("TCT");

									TCSysSetupExample tcSysSetupExample = new TCSysSetupExample();
									TCSysSetup tcSysSetup = new TCSysSetup();
									TCSysSetupExample.Criteria tcSysSetupCriteria = tcSysSetupExample.createCriteria();
									tcSysSetupCriteria.andCompanyIdIn(companyList);
									tcSysSetupCriteria.andProductIdIn(productList);

									tcSysSetup.setStampDuty(listChangeDataTripCare.get(0).getStampDuty());
									tcSysSetup.setStaffDiscPercent(listChangeDataTripCare.get(0).getStaffDiscPercent());
									tcSysSetup.setGstEffDate(listChangeDataTripCare.get(0).getGstEffDate());
									tcSysSetup.setSstEffDate(listChangeDataTripCare.get(0).getSstEffDate());
									tcSysSetup.setGst(listChangeDataTripCare.get(0).getGst());
									tcSysSetup.setSst(listChangeDataTripCare.get(0).getSst());
									tcSysSetup.setModifiedBy(listChangeDataTripCare.get(0).getModifiedBy());
									tcSysSetup.setModifiedDt(listChangeDataTripCare.get(0).getModifiedDt());

									// *************Update the new change Product Info in original table
									// ********************
									tcSysSetupMapper.updateByExampleSelective(tcSysSetup, tcSysSetupExample);

									ApprovalLogExample approvalLogexample = new ApprovalLogExample();
									alog = new ApprovalLog();
									createCriteria = approvalLogexample.createCriteria();
									aid = request.getParameter("approvalLogId");
									if (null != aid && !aid.isEmpty()) {
										testint = new Integer(aid);
										createCriteria.andIdEqualTo(testint.shortValue());
										listapprovallog = approvalLogMapper
												.selectByExampleWithBLOBs(approvalLogexample);
										System.out.println(
												"   list size in approval list  :::   " + listapprovallog.size());

										alogId = listapprovallog.get(0).getId();
										alog.setId(alogId);
										alog.setStatus("3");
										alog.setChecker(Short.parseShort(loginUser)); // get the current login session
										alog.setUpdateDate(new Date());
										approvalLogMapper.updateByPrimaryKeySelective(alog);
									}

								}
							}

						} // try end

					} else {

						Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
						List<TCProdMngPojo> listChangeData;

						// object convert into AgentProdMap
						if (changeData != null) {

							listChangeData = (List<TCProdMngPojo>) changeData;
							System.out.println("  list agent prod map size ::::::         " + listChangeData.size());

							if (listChangeData.get(0).getProdType().equalsIgnoreCase("INT")) {
								// International
								Object originalD = process
										.convertFromBytes(listapprovallog.get(0).getOriginalContent());
								List<InterPremium> listOrgData;
								listOrgData = (List<InterPremium>) originalD;

								String interPremiumPk = listOrgData.get(0).getId().toString();

								InterPremiumExample interPremiumExample = new InterPremiumExample();
								InterPremiumExample.Criteria interPremiumExampleCriteria = interPremiumExample
										.createCriteria();
								interPremiumExampleCriteria.andIdEqualTo(new BigDecimal(interPremiumPk));

								InterPremium interPremium = new InterPremium();
								interPremium
										.setDaysRangeKeycode(listChangeData.get(0).getDaysRangeKeycodeInternational());
								interPremium.setDestinationCodeArea(
										listChangeData.get(0).getDestinationCodeAreaInternational());
								interPremium.setPackageCode(listChangeData.get(0).getPackageCodeInternational());
								interPremium.setPlanCode(listChangeData.get(0).getPlanCodeInternational());
								BigDecimal premiumval = new BigDecimal(
										listChangeData.get(0).getPremiumValInternational());
								interPremium.setPremiumValue(premiumval);
								interPremium.setId(new BigDecimal(interPremiumPk));
								interPremium
										.setTravelwithTypeId(listChangeData.get(0).getTravelWithTypeIdInternational());

								// *************Update the new change Product Info in original table
								// ********************
								interPremiumMapper.updateByPrimaryKeySelective(interPremium);
								System.out.println("International Data updated .............   " + alogId);

							} else if (listChangeData.get(0).getProdType().equalsIgnoreCase("D")) {
								// Domestic

								System.out.println("  domestic.....");

								Object originalD = process
										.convertFromBytes(listapprovallog.get(0).getOriginalContent());
								List<DomesticPremium> listOrgData;
								listOrgData = (List<DomesticPremium>) originalD;

								System.out.println("  domestic.....1111");
								String domesticPremiumPk = listOrgData.get(0).getId().toString();

								DomesticPremiumExample domesticPremiumExample = new DomesticPremiumExample();
								DomesticPremiumExample.Criteria domesticPremiumExampleCriteria = domesticPremiumExample
										.createCriteria();
								domesticPremiumExampleCriteria.andIdEqualTo(new BigDecimal(domesticPremiumPk));

								System.out.println("  domestic.....222222");

								DomesticPremium domesticPremium = new DomesticPremium();
								domesticPremium
										.setDaysRangeKeycode(listChangeData.get(0).getDaysRangeKeycodeDomestic());
								domesticPremium
										.setDestinationCodeArea(listChangeData.get(0).getDestinationCodeAreaDomestic());
								domesticPremium.setPackageCode(listChangeData.get(0).getPackageCodeDomestic());
								domesticPremium.setPlanCode(listChangeData.get(0).getPlanCodeDomestic());
								BigDecimal premiumval = new BigDecimal(listChangeData.get(0).getPremiumValDomestic());
								domesticPremium.setPremiumValue(premiumval);
								domesticPremium.setId(new BigDecimal(domesticPremiumPk));
								domesticPremium
										.setTravelwithTypeId(listChangeData.get(0).getTravelWithTypeIdDomestic());

								System.out.println("  domesticPremium TripCare");

								// *************Update the new change Product Info in original table
								// ********************
								domesticPremiumMapper.updateByPrimaryKeySelective(domesticPremium);
								System.out.println("Domestic Data updated .............   " + alogId);

							}

							ApprovalLogExample approvalLogexample = new ApprovalLogExample();
							alog = new ApprovalLog();
							createCriteria = approvalLogexample.createCriteria();
							aid = request.getParameter("approvalLogId");
							if (null != aid && !aid.isEmpty()) {
								testint = new Integer(aid);
								createCriteria.andIdEqualTo(testint.shortValue());
								listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalLogexample);
								System.out.println("   list size in approval list  :::   " + listapprovallog.size());

								alogId = listapprovallog.get(0).getId();
								alog.setId(alogId);
								alog.setStatus("3");
								alog.setChecker(Short.parseShort(loginUser)); // get from the current login session
								alog.setUpdateDate(new Date());
								approvalLogMapper.updateByPrimaryKeySelective(alog);
							}

						}
					}
				}

			}

		} // try end

		catch (Exception e) {
			System.out.println("Exception Update new product rate " + e);
		}

		return "redirect:/getApprovalList";
	}

	// Display Original and Change Data Functionality
	// Show TripCare Product Rate Change Reject Function

	@RequestMapping(value = "/rejectTCProductRateChange", method = RequestMethod.GET)
	public String rejectTCProductRateChange(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		try {
			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			ApprovalLogExample approvalLogexample = new ApprovalLogExample();
			alog = new ApprovalLog();
			ApprovalLogExample.Criteria createCriteria = approvalLogexample.createCriteria();
			String aid = request.getParameter("approvalLogId");
			if (null != aid && !aid.isEmpty()) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalLogexample);
				System.out.println("   list size in approval list  :::   " + listapprovallog.size());

				Short alogId = listapprovallog.get(0).getId();
				alog.setId(alogId);
				alog.setStatus("2");
				alog.setChecker(Short.parseShort(loginUser)); // get from the current login session
				alog.setUpdateDate(new Date());
				approvalLogMapper.updateByPrimaryKeySelective(alog);
			}
		} // try end
		catch (Exception e) {
			System.out.println("Exception in Convert Object to Byte" + e);
		}

		return "redirect:/getApprovalList";
	}

	// Display Original and Change Data Functionality

	@RequestMapping(value = "/approvalTCProductInfo", method = RequestMethod.GET)
	public String approvalTCProductInfo(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Request id  for product info ::: " + request.getParameter("id"));
		ApprovalLogExample approvalexample = new ApprovalLogExample();
		com.spring.VO.ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();
		Integer testint = new Integer(request.getParameter("id"));
		createCriteria.andIdEqualTo(testint.shortValue());
		List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
		System.out.println("   list size in approval list  :::   " + listapprovallog.size());

		try {
			// original data
			if (!listapprovallog.isEmpty()) {
				convertBytes process = new convertBytes();
				Object originalData = process.convertFromBytes(listapprovallog.get(0).getOriginalContent());
				List<TC360Param> listOriginalData;
				// object convert into AgentProdMap
				if (originalData != null) {
					listOriginalData = (List<TC360Param>) originalData;
					System.out.println(" Original Data List Size ::::::         " + listOriginalData.size());
					for (int t = 0; t < listOriginalData.size(); t++) {
						System.out.println("Original Product info" + listOriginalData.get(t).getParamType());
						System.out.println("Original Product info" + listOriginalData.get(t).getParamDesc());
						System.out.println("Original Product info" + listOriginalData.get(t).getParamDescMy());
					}

					TCProdMngPojo tripCareParam = new TCProdMngPojo();
					tripCareParam.setAnnualTarget(listOriginalData.get(0).getParamCode());
					tripCareParam.setValidity(listOriginalData.get(1).getParamCode());

					List<TCProdMngPojo> listOriginalProdInfo = new ArrayList<>();
					listOriginalProdInfo.add(tripCareParam);
					model.addAttribute("listOriginalTCProductInfo", listOriginalProdInfo);
				} // inner if
			} // outer if

			// Change Rules Data for MI
			if (!listapprovallog.isEmpty()) {
				convertBytes process = new convertBytes();

				Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
				List<TC360Param> listChangeData;
				// object convert into AgentProdMap
				if (changeData != null) {
					listChangeData = (List<TC360Param>) changeData;
					System.out.println("ChageData List Size  ::::::         " + listChangeData.size());

					for (int t = 0; t < listChangeData.size(); t++) {

						System.out.println("Change Product info" + listChangeData.get(t).getParamType());
						System.out.println("Change Product info" + listChangeData.get(t).getParamDesc());
						System.out.println("Change Product info" + listChangeData.get(t).getParamDescMy());

					}

					TCProdMngPojo tripCareParam = new TCProdMngPojo();
					tripCareParam.setAnnualTarget(listChangeData.get(0).getParamCode());
					tripCareParam.setValidity(listChangeData.get(1).getParamCode());

					List<TCProdMngPojo> listChangeTripCareRules = new ArrayList<>();
					listChangeTripCareRules.add(tripCareParam);
					model.addAttribute("listChangeTCProductInfo", listChangeTripCareRules);
				}
			}
		} // try end
		catch (Exception e) {
			System.out.println("Exception in Convert Object to Byte" + e);
		}

		return "agent-product-approval";
	}

	// Show TripCare Product Info Approval Function

	@RequestMapping(value = "/approvalTCProductInfoChange", method = RequestMethod.GET)
	public String approvalTCProductInfoChange(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		try {
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			// *************Update the new change Rule management in original table
			// ********************

			String aid = request.getParameter("approvalLogId");
			System.out.println("aid >>>>>" + aid);
			ApprovalLogExample approvalexample = new ApprovalLogExample();
			ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();

			if (null != aid && !aid.isEmpty()) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
				System.out.println("   list size in approval list  :::   " + listapprovallog.size());

				// Change Data
				if (!listapprovallog.isEmpty()) {
					String status = listapprovallog.get(0).getStatus();
					short alogId = listapprovallog.get(0).getId();
					System.out.println("Status  :  " + status);
					System.out.println("Approvallog Id  :  " + alogId);

					convertBytes process = new convertBytes();
					Object changeData = process.convertFromBytes(listapprovallog.get(0).getNewContent());
					List<TC360Param> listChangeData;
					// object convert into AgentProdMap
					if (changeData != null) {
						listChangeData = (List<TC360Param>) changeData;
						System.out.println("  list agent prod map size ::::::         " + listChangeData.size());

						for (int t = 0; t < listChangeData.size(); t++) {

							if (listChangeData.get(t).getParamType().equalsIgnoreCase("TC_quote_validity")) {
								// Quotation Validity

								TC360ParamExample tripCareParamExample = new TC360ParamExample();
								TC360ParamExample.Criteria tripCareParamCriteria = tripCareParamExample
										.createCriteria();
								tripCareParamCriteria.andParamTypeEqualTo("TC_quote_validity");

								TC360Param tripCareParam2 = new TC360Param();
								tripCareParam2.setId(listChangeData.get(t).getId());
								tripCareParam2.setParamType(listChangeData.get(t).getParamType());
								String quotVal = listChangeData.get(t).getParamCode();
								tripCareParam2.setParamCode(quotVal == null ? "0" : quotVal);
								tripCareParam2.setParamDesc(listChangeData.get(t).getParamDesc());
								tripCareParam2.setParamDescMy(listChangeData.get(t).getParamDescMy());

								// *************Update the new change Product Info in original table
								// ********************
								tripCareParamMapper.updateByExample(tripCareParam2, tripCareParamExample);

							} else if (listChangeData.get(t).getParamType()
									.equalsIgnoreCase("TC_annual_sales_target")) {

								// Sales Target
								TC360ParamExample tripCareParamExample = new TC360ParamExample();
								TC360ParamExample.Criteria tripCareParamCriteria = tripCareParamExample
										.createCriteria();
								tripCareParamCriteria.andParamTypeEqualTo("TC_annual_sales_target");

								TC360Param tripCareParam2 = new TC360Param();
								tripCareParam2.setId(listChangeData.get(t).getId());
								tripCareParam2.setParamType(listChangeData.get(t).getParamType());
								String salesTargetVal = listChangeData.get(t).getParamCode();
								tripCareParam2.setParamCode(salesTargetVal == null ? "0" : salesTargetVal);
								tripCareParam2.setParamDesc(listChangeData.get(t).getParamDesc());
								tripCareParam2.setParamDescMy(listChangeData.get(t).getParamDescMy());

								// *************Update the new change Product Info in original table
								// ********************
								tripCareParamMapper.updateByExample(tripCareParam2, tripCareParamExample);

							}

						}

						ApprovalLogExample approvalLogexample = new ApprovalLogExample();
						ApprovalLog alog = new ApprovalLog();
						createCriteria = approvalLogexample.createCriteria();
						aid = request.getParameter("approvalLogId");
						if (null != aid && !aid.isEmpty()) {
							testint = new Integer(aid);
							createCriteria.andIdEqualTo(testint.shortValue());
							listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalLogexample);
							System.out.println("   list size in approval list  :::   " + listapprovallog.size());

							alogId = listapprovallog.get(0).getId();
							alog.setId(alogId);
							alog.setStatus("3");
							alog.setChecker(Short.parseShort(loginUser)); // get from the current login session
							alog.setUpdateDate(new Date());
							approvalLogMapper.updateByPrimaryKeySelective(alog);
						}
					}
				}
			}
		} // try end

		catch (Exception e) {
			System.out.println("Exception Update new Product Info " + e);
		}

		return "redirect:/getApprovalList";
	}

	// TripCare Product Info Reject Function

	@RequestMapping(value = "/rejectTCProductInfoChange", method = RequestMethod.GET)
	public String rejectTCProductInfoChange(HttpServletRequest request, HttpServletResponse response, Model model,
			BindingResult result) {

		try {

			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			ApprovalLogExample approvalLogexample = new ApprovalLogExample();
			ApprovalLog alog = new ApprovalLog();
			ApprovalLogExample.Criteria createCriteria = approvalLogexample.createCriteria();
			String aid = request.getParameter("approvalLogId");
			if (null != aid && !aid.isEmpty()) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalLogexample);
				System.out.println("   list size in approval list  :::   " + listapprovallog.size());

				short alogId = listapprovallog.get(0).getId();
				alog.setId(alogId);
				alog.setStatus("2");
				alog.setChecker(Short.parseShort(loginUser)); // get from the current login session
				alog.setUpdateDate(new Date());
				approvalLogMapper.updateByPrimaryKeySelective(alog);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "agent-product-approval";
	}

}// Main class end
