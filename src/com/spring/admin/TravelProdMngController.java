package com.spring.admin;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.spring.VO.Approval;
import com.spring.VO.ApprovalExample;
import com.spring.VO.ApprovalLog;
import com.spring.VO.MITblPmntParam;
import com.spring.VO.MITblPmntParamExample;
import com.spring.VO.TblPdfInfo;
import com.spring.VO.TblPdfInfoExample;
import com.spring.VO.WTCProdMngPojo;
import com.spring.VO.WTCTblDometic;
import com.spring.VO.WTCTblDometicExample;
import com.spring.VO.WTCTblIntwithdays;
import com.spring.VO.WTCTblIntwithdaysExample;
import com.spring.VO.WTCTblParam;
import com.spring.VO.WTCTblParamExample;
import com.spring.VO.WTCTblParamExample.Criteria;
import com.spring.mapper.ApprovalLogMapper;
import com.spring.mapper.ApprovalMapper;
import com.spring.mapper.MITblPmntParamMapper;
import com.spring.mapper.TblPdfInfoMapper;
import com.spring.mapper.WTCTblDometicMapper;
import com.spring.mapper.WTCTblIntwithdaysMapper;
import com.spring.mapper.WTCTblParamMapper;

@Controller
public class TravelProdMngController {
	MITblPmntParamMapper miTblPmntParamMapper;
	WTCTblDometicMapper wtcTblDometicMapper;
	WTCTblIntwithdaysMapper wtcTblIntwithdaysMapper;
	WTCTblParamMapper wtcTblParamMapper;
	TblPdfInfoMapper tblPdfInfoMapper;
	ApprovalMapper approvalMapper;
	ApprovalLogMapper approvalLogMapper;

	@Autowired
	public TravelProdMngController(MITblPmntParamMapper miTblPmntParamMapper, WTCTblDometicMapper wtcTblDometicMapper,
			WTCTblIntwithdaysMapper wtcTblIntwithdaysMapper, WTCTblParamMapper wtcTblParamMapper,
			TblPdfInfoMapper tblPdfInfoMapper, ApprovalMapper approvalMapper, ApprovalLogMapper approvalLogMapper) {
		this.miTblPmntParamMapper = miTblPmntParamMapper;
		this.wtcTblDometicMapper = wtcTblDometicMapper;
		this.wtcTblIntwithdaysMapper = wtcTblIntwithdaysMapper;
		this.wtcTblParamMapper = wtcTblParamMapper;
		this.tblPdfInfoMapper = tblPdfInfoMapper;
		this.approvalMapper = approvalMapper;
		this.approvalLogMapper = approvalLogMapper;

	}

	/*-------------------------------------------------------Start- Payment Method----------------------------------------------------------*/

	@RequestMapping("/TravelPaymentMethod")
	public String TravelPaymentMethod(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("TravelPaymentMethod funtion ");

		HttpSession session = request.getSession();

		// Maybank2U

		List<MITblPmntParam> m2ulist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria = miTblPmntParamExample.createCriteria();
		MiPmntParam_criteria.andPmntGatewayCodeEqualTo("M2U");
		MiPmntParam_criteria.andProductCodeEqualTo("WTC");
		MiPmntParam_criteria.andStatusEqualTo(Short.valueOf("1"));
		m2ulist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample);

		// FPX

		List<MITblPmntParam> fpxlist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample1 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria1 = miTblPmntParamExample1.createCriteria();
		MiPmntParam_criteria1.andPmntGatewayCodeEqualTo("FPX");
		MiPmntParam_criteria1.andProductCodeEqualTo("WTC");
		MiPmntParam_criteria1.andStatusEqualTo(Short.valueOf("1"));
		fpxlist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample1);
		// EBPG (Visa/Mastercard)

		List<MITblPmntParam> ebpglist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample2 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria2 = miTblPmntParamExample2.createCriteria();
		MiPmntParam_criteria2.andPmntGatewayCodeEqualTo("EBPG");
		MiPmntParam_criteria2.andProductCodeEqualTo("WTC");
		MiPmntParam_criteria2.andStatusEqualTo(Short.valueOf("1"));
		ebpglist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample2);

		// AMEX

		List<MITblPmntParam> amexlist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample3 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria3 = miTblPmntParamExample3.createCriteria();
		MiPmntParam_criteria3.andPmntGatewayCodeEqualTo("AMEX");
		MiPmntParam_criteria3.andProductCodeEqualTo("WTC");
		MiPmntParam_criteria3.andStatusEqualTo(Short.valueOf("1"));
		amexlist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample3);

		model.addAttribute("m2ulist", m2ulist);
		model.addAttribute("fpxlist", fpxlist);
		model.addAttribute("ebpglist", ebpglist);
		model.addAttribute("amexlist", amexlist);

		return "/products/travel/travel-prod-mng-pmnt-method";

	}

	@RequestMapping("/UpdateTravelPaymentMethod")
	public String UpdateTravelPaymentMethod(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("UPDATE TravelPaymentMethod funtion ");

		HttpSession session = request.getSession();

		// Maybank2U

		List<MITblPmntParam> m2ulist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria = miTblPmntParamExample.createCriteria();
		MiPmntParam_criteria.andPmntGatewayCodeEqualTo("M2U");
		MiPmntParam_criteria.andProductCodeEqualTo("WTC");
		MiPmntParam_criteria.andStatusEqualTo(Short.valueOf("1"));
		m2ulist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample);

		// FPX

		List<MITblPmntParam> fpxlist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample1 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria1 = miTblPmntParamExample1.createCriteria();
		MiPmntParam_criteria1.andPmntGatewayCodeEqualTo("FPX");
		MiPmntParam_criteria1.andProductCodeEqualTo("WTC");
		MiPmntParam_criteria1.andStatusEqualTo(Short.valueOf("1"));
		fpxlist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample1);
		// EBPG (Visa/ Mastercard)

		List<MITblPmntParam> ebpglist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample2 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria2 = miTblPmntParamExample2.createCriteria();
		MiPmntParam_criteria2.andPmntGatewayCodeEqualTo("EBPG");
		MiPmntParam_criteria2.andProductCodeEqualTo("WTC");
		MiPmntParam_criteria2.andStatusEqualTo(Short.valueOf("1"));
		ebpglist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample2);

		// AMEX

		List<MITblPmntParam> amexlist = new ArrayList<MITblPmntParam>();
		MITblPmntParamExample miTblPmntParamExample3 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria3 = miTblPmntParamExample3.createCriteria();
		MiPmntParam_criteria3.andPmntGatewayCodeEqualTo("AMEX");
		MiPmntParam_criteria3.andProductCodeEqualTo("WTC");
		MiPmntParam_criteria3.andStatusEqualTo(Short.valueOf("1"));
		amexlist = miTblPmntParamMapper.selectByExample(miTblPmntParamExample3);

		model.addAttribute("m2ulist", m2ulist);
		model.addAttribute("fpxlist", fpxlist);
		model.addAttribute("ebpglist", ebpglist);
		model.addAttribute("amexlist", amexlist);

		return "/products/travel/travel-prod-mng-pmnt-method-edit";

	}

	@RequestMapping("/updateDoneTravelPaymentMethod")
	public String updateDoneTravelPaymentMethod(@ModelAttribute(value = "MITblPmntParam") MITblPmntParam miTblPmntParam,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("updateDoneTravelPaymentMethod update done funtion ");

		HttpSession session = request.getSession();

		// Maybank2U
		MITblPmntParamExample miTblPmntParamExample = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria = miTblPmntParamExample.createCriteria();
		MiPmntParam_criteria.andPmntGatewayCodeEqualTo("M2U");
		MiPmntParam_criteria.andProductCodeEqualTo("WTC");
		String m2u = "0";
		if (request.getParameter("m2u") != null) {
			m2u = request.getParameter("m2u");
			if (m2u.equals("on")) {
				m2u = "1";
			}
		}
		miTblPmntParam.setStatus(Short.valueOf(m2u));
		int miTblPmntParamUpdate = miTblPmntParamMapper.updateByExampleSelective(miTblPmntParam, miTblPmntParamExample);
		System.out.println(miTblPmntParamUpdate + "M2U");

		// FPX
		MITblPmntParamExample miTblPmntParamExample1 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria1 = miTblPmntParamExample1.createCriteria();
		MiPmntParam_criteria1.andPmntGatewayCodeEqualTo("FPX");
		MiPmntParam_criteria1.andProductCodeEqualTo("WTC");
		String fpx = "0";
		if (request.getParameter("fpx") != null) {
			fpx = request.getParameter("fpx");
			if (fpx.equals("on")) {
				fpx = "1";
			}
		}
		System.out.println(fpx + " fpx");

		miTblPmntParam.setStatus(Short.valueOf(fpx));
		int miTblPmntParamUpdate1 = miTblPmntParamMapper.updateByExampleSelective(miTblPmntParam,
				miTblPmntParamExample1);
		System.out.println(miTblPmntParamUpdate1 + "FPX");

		// EBPG
		MITblPmntParamExample miTblPmntParamExample2 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria2 = miTblPmntParamExample2.createCriteria();
		MiPmntParam_criteria2.andPmntGatewayCodeEqualTo("EBPG");
		MiPmntParam_criteria2.andProductCodeEqualTo("WTC");
		String ebpg = "0";
		if (request.getParameter("ebpg") != null) {
			ebpg = request.getParameter("ebpg");
			if (ebpg.equals("on")) {
				ebpg = "1";
			}
		}
		miTblPmntParam.setStatus(Short.valueOf(ebpg));
		int miTblPmntParamUpdate2 = miTblPmntParamMapper.updateByExampleSelective(miTblPmntParam,
				miTblPmntParamExample2);
		System.out.println(miTblPmntParamUpdate2 + "EBPG");

		// AMEX
		MITblPmntParamExample miTblPmntParamExample3 = new MITblPmntParamExample();
		MITblPmntParamExample.Criteria MiPmntParam_criteria3 = miTblPmntParamExample3.createCriteria();
		MiPmntParam_criteria3.andPmntGatewayCodeEqualTo("AMEX");
		MiPmntParam_criteria3.andProductCodeEqualTo("WTC");
		String amex = "0";
		if (request.getParameter("amex") != null) {
			amex = request.getParameter("amex");
			if (amex.equals("on")) {
				amex = "1";
			}
		}
		miTblPmntParam.setStatus(Short.valueOf(amex));
		int miTblPmntParamUpdate3 = miTblPmntParamMapper.updateByExampleSelective(miTblPmntParam,
				miTblPmntParamExample3);
		System.out.println(miTblPmntParamUpdate3 + " AMEX");
		model.addAttribute("updatemessage", "Update data successfully");
		String returnURL = TravelPaymentMethod(request, response, model);

		return returnURL;
	}

	/*-------------------------------------------------------End- Payment Method----------------------------------------------------------*/

	/*-------------------------------------------------------Start- Product Rates----------------------------------------------------------*/

	@RequestMapping("/TravelProductRates")
	public String TravelProductRates(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("TravelProductRates funtion ");

		HttpSession session = request.getSession();

		// Domestic Premium values

		List<WTCTblDometic> DomPremiumList18 = new ArrayList<WTCTblDometic>();
		WTCTblDometicExample wtcTblDometicExample = new WTCTblDometicExample();
		WTCTblDometicExample.Criteria WTCTblDometic_criteria = wtcTblDometicExample.createCriteria();
		// WTCTblDometic_criteria.andDaysRangeKeycodeEqualTo("D1_5");
		WTCTblDometic_criteria.andTravelwithTypeIdEqualTo("W01");
		DomPremiumList18 = wtcTblDometicMapper.selectByExample(wtcTblDometicExample);

		List<WTCTblDometic> DomPremiumList71 = new ArrayList<WTCTblDometic>();
		WTCTblDometicExample wtcTblDometicExample1 = new WTCTblDometicExample();
		WTCTblDometicExample.Criteria WTCTblDometic_criteria1 = wtcTblDometicExample1.createCriteria();
		// WTCTblDometic_criteria.andDaysRangeKeycodeEqualTo("D1_5");
		WTCTblDometic_criteria1.andTravelwithTypeIdEqualTo("W07");
		DomPremiumList71 = wtcTblDometicMapper.selectByExample(wtcTblDometicExample1);

		List<WTCTblDometic> DomPremiumListIndv = new ArrayList<WTCTblDometic>();
		WTCTblDometicExample wtcTblDometicExample2 = new WTCTblDometicExample();
		WTCTblDometicExample.Criteria WTCTblDometic_criteria2 = wtcTblDometicExample2.createCriteria();
		// WTCTblDometic_criteria.andDaysRangeKeycodeEqualTo("D1_5");
		WTCTblDometic_criteria2.andTravelwithTypeIdEqualTo("W03");
		DomPremiumListIndv = wtcTblDometicMapper.selectByExample(wtcTblDometicExample2);

		List<WTCTblDometic> DomPremiumListFamily = new ArrayList<WTCTblDometic>();
		WTCTblDometicExample wtcTblDometicExample3 = new WTCTblDometicExample();
		WTCTblDometicExample.Criteria WTCTblDometic_criteria3 = wtcTblDometicExample3.createCriteria();
		// WTCTblDometic_criteria.andDaysRangeKeycodeEqualTo("D1_5");
		WTCTblDometic_criteria3.andTravelwithTypeIdEqualTo("W05");
		DomPremiumListFamily = wtcTblDometicMapper.selectByExample(wtcTblDometicExample3);

		// International Premium Values

		List<WTCTblIntwithdays> IntPremiumList18 = new ArrayList<WTCTblIntwithdays>();
		WTCTblIntwithdaysExample wtcTblIntwithdaysExample = new WTCTblIntwithdaysExample();
		WTCTblIntwithdaysExample.Criteria WTCTblIntwithdays_criteria = wtcTblIntwithdaysExample.createCriteria();
		// WTCTblDometic_criteria.andDaysRangeKeycodeEqualTo("D1_5");
		WTCTblIntwithdays_criteria.andTravelwithTypeIdEqualTo("W01");
		IntPremiumList18 = wtcTblIntwithdaysMapper.selectByExample(wtcTblIntwithdaysExample);

		List<WTCTblIntwithdays> IntPremiumList71 = new ArrayList<WTCTblIntwithdays>();
		WTCTblIntwithdaysExample wtcTblIntwithdaysExample1 = new WTCTblIntwithdaysExample();
		WTCTblIntwithdaysExample.Criteria WTCTblIntwithdays_criteria1 = wtcTblIntwithdaysExample1.createCriteria();
		// WTCTblDometic_criteria.andDaysRangeKeycodeEqualTo("D1_5");
		WTCTblIntwithdays_criteria1.andTravelwithTypeIdEqualTo("W07");
		IntPremiumList71 = wtcTblIntwithdaysMapper.selectByExample(wtcTblIntwithdaysExample1);

		List<WTCTblIntwithdays> IntPremiumListIndv = new ArrayList<WTCTblIntwithdays>();
		WTCTblIntwithdaysExample wtcTblIntwithdaysExample2 = new WTCTblIntwithdaysExample();
		WTCTblIntwithdaysExample.Criteria WTCTblIntwithdays_criteria2 = wtcTblIntwithdaysExample2.createCriteria();
		// WTCTblDometic_criteria.andDaysRangeKeycodeEqualTo("D1_5");
		WTCTblIntwithdays_criteria2.andTravelwithTypeIdEqualTo("W03");
		IntPremiumListIndv = wtcTblIntwithdaysMapper.selectByExample(wtcTblIntwithdaysExample2);

		List<WTCTblIntwithdays> IntPremiumListFamily = new ArrayList<WTCTblIntwithdays>();
		WTCTblIntwithdaysExample wtcTblIntwithdaysExample3 = new WTCTblIntwithdaysExample();
		WTCTblIntwithdaysExample.Criteria WTCTblIntwithdays_criteria3 = wtcTblIntwithdaysExample3.createCriteria();
		// WTCTblDometic_criteria.andDaysRangeKeycodeEqualTo("D1_5");
		WTCTblIntwithdays_criteria3.andTravelwithTypeIdEqualTo("W05");
		IntPremiumListFamily = wtcTblIntwithdaysMapper.selectByExample(wtcTblIntwithdaysExample3);

		// Stamp Duty

		List<WTCTblParam> stampDutyList = new ArrayList<WTCTblParam>();
		WTCTblParamExample dspWtcTblParamExample = new WTCTblParamExample();
		WTCTblParamExample.Criteria dspWtcParam_criteria = dspWtcTblParamExample.createCriteria();
		dspWtcParam_criteria.andParamTypeEqualTo("WTC_StampDuty");
		stampDutyList = wtcTblParamMapper.selectByExample(dspWtcTblParamExample);

		// GST

		List<WTCTblParam> gstValueList = new ArrayList<WTCTblParam>();
		WTCTblParamExample dspWtcTblParamExample1 = new WTCTblParamExample();
		WTCTblParamExample.Criteria dspWtcParam_criteria1 = dspWtcTblParamExample1.createCriteria();
		dspWtcParam_criteria1.andParamTypeEqualTo("WTC_Gst");
		gstValueList = wtcTblParamMapper.selectByExample(dspWtcTblParamExample1);

		// Direct Discount (Online)

		List<WTCTblParam> discountValueList = new ArrayList<WTCTblParam>();
		WTCTblParamExample dspWtcTblParamExample2 = new WTCTblParamExample();
		WTCTblParamExample.Criteria dspWtcParam_criteria2 = dspWtcTblParamExample2.createCriteria();
		dspWtcParam_criteria2.andParamTypeEqualTo("WTC_Discount");
		discountValueList = wtcTblParamMapper.selectByExample(dspWtcTblParamExample2);

		model.addAttribute("DomPremiumList18", DomPremiumList18);
		model.addAttribute("DomPremiumList71", DomPremiumList71);
		model.addAttribute("DomPremiumListIndv", DomPremiumListIndv);
		model.addAttribute("DomPremiumListFamily", DomPremiumListFamily);

		model.addAttribute("IntPremiumList18", IntPremiumList18);
		model.addAttribute("IntPremiumList71", IntPremiumList71);
		model.addAttribute("IntPremiumListIndv", IntPremiumListIndv);
		model.addAttribute("IntPremiumListFamily", IntPremiumListFamily);

		model.addAttribute("stampDutyList", stampDutyList);
		model.addAttribute("gstValueList", gstValueList);
		model.addAttribute("discountValueList", discountValueList);

		return "/products/travel/travel-prod-mng-prod-rates";

	}

	@RequestMapping("/UpdateTravelProdRates")
	public String UpdateTravelProdRates(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("UpdateTravelProdRates funtion ");

		HttpSession session = request.getSession();

		// Stamp Duty

		List<WTCTblParam> stampDutyList = new ArrayList<WTCTblParam>();
		WTCTblParamExample dspWtcTblParamExample = new WTCTblParamExample();
		WTCTblParamExample.Criteria dspWtcParam_criteria = dspWtcTblParamExample.createCriteria();
		dspWtcParam_criteria.andParamTypeEqualTo("WTC_StampDuty");
		stampDutyList = wtcTblParamMapper.selectByExample(dspWtcTblParamExample);

		// GST

		List<WTCTblParam> gstValueList = new ArrayList<WTCTblParam>();
		WTCTblParamExample dspWtcTblParamExample1 = new WTCTblParamExample();
		WTCTblParamExample.Criteria dspWtcParam_criteria1 = dspWtcTblParamExample1.createCriteria();
		dspWtcParam_criteria1.andParamTypeEqualTo("WTC_Gst");
		gstValueList = wtcTblParamMapper.selectByExample(dspWtcTblParamExample1);

		// Direct Discount (Online)

		List<WTCTblParam> discountValueList = new ArrayList<WTCTblParam>();
		WTCTblParamExample dspWtcTblParamExample2 = new WTCTblParamExample();
		WTCTblParamExample.Criteria dspWtcParam_criteria2 = dspWtcTblParamExample2.createCriteria();
		dspWtcParam_criteria2.andParamTypeEqualTo("WTC_Discount");
		discountValueList = wtcTblParamMapper.selectByExample(dspWtcTblParamExample2);

		model.addAttribute("stampDutyList", stampDutyList);
		model.addAttribute("gstValueList", gstValueList);
		model.addAttribute("discountValueList", discountValueList);

		return "/products/travel/travel-prod-mng-prod-rates-edit";

	}

	@RequestMapping("/UpdateTravelProdRatesDone")
	public String UpdateTravelProdRatesDone(@ModelAttribute(value = "WTCTblParam") WTCTblParam wtcTblParam,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("UpdateTravelProdRates funtion ");

		HttpSession session = request.getSession();

		// Stamp Duty

		WTCTblParamExample wtcTblParamExample2 = new WTCTblParamExample();
		WTCTblParamExample.Criteria wtcParam_criteria2 = wtcTblParamExample2.createCriteria();
		wtcParam_criteria2.andParamTypeEqualTo("WTC_StampDuty");
		String wtcsduty = request.getParameter("stampduty");
		System.out.println(wtcsduty + "wtcsduty");
		wtcTblParam.setParamCode(wtcsduty == null ? "0" : wtcsduty);
		String formatsduty = wtcsduty;
		wtcTblParam.setParamDesc(formatsduty);
		wtcTblParam.setParamDescMy(formatsduty);
		int wtcTblParamUpdate2 = wtcTblParamMapper.updateByExampleSelective(wtcTblParam, wtcTblParamExample2);
		System.out.println(wtcTblParamUpdate2 + "wtcsduty");

		// GST

		WTCTblParamExample wtcTblParamExample1 = new WTCTblParamExample();
		WTCTblParamExample.Criteria wtcParam_criteria1 = wtcTblParamExample1.createCriteria();
		wtcParam_criteria1.andParamTypeEqualTo("WTC_Gst");
		String wtcgst = request.getParameter("gst");
		System.out.println(wtcgst + "wtcgst");
		wtcTblParam.setParamCode(wtcgst == null ? "0" : wtcgst);
		String formatgst = wtcgst;
		wtcTblParam.setParamDesc(formatgst);
		wtcTblParam.setParamDescMy(formatgst);
		int wtcTblParamUpdate1 = wtcTblParamMapper.updateByExampleSelective(wtcTblParam, wtcTblParamExample1);
		System.out.println(wtcTblParamUpdate1 + "wtcgst");

		// Direct Discount (Online)

		WTCTblParamExample wtcTblParamExample = new WTCTblParamExample();
		WTCTblParamExample.Criteria wtcParam_criteria = wtcTblParamExample.createCriteria();
		wtcParam_criteria.andParamTypeEqualTo("WTC_Discount");
		String discount = request.getParameter("discount");
		System.out.println(discount + "discount");
		wtcTblParam.setParamCode(discount == null ? "0" : discount);
		String formatVal = discount;
		wtcTblParam.setParamDesc(formatVal);
		wtcTblParam.setParamDescMy(formatVal);
		int wtcTblParamUpdate = wtcTblParamMapper.updateByExampleSelective(wtcTblParam, wtcTblParamExample);
		System.out.println(wtcTblParamUpdate + "discount");
		model.addAttribute("wtcUpdatedprMessage", "Product Rates Updated Successfully!");
		String returnURL = TravelProductRates(request, response, model);

		return returnURL;

	}

	@RequestMapping(value = "/UpdateWTCProdRates", method = RequestMethod.GET)
	public @ResponseBody String UpdateWTCProdRates(@RequestParam String pid, @RequestParam String premval,
			HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {

		String premid = null;
		if (request.getParameter("pid") != null) {
			premid = request.getParameter("pid");
		} else {
			premid = String.valueOf(request.getAttribute("pid"));
		}
		System.out.println("id from selection -" + premid);
		WTCTblDometic wtcTblDometic = new WTCTblDometic();
		wtcTblDometic.setId(Short.valueOf(premid));
		Double premium = 0.00;
		if (premval != null) {
			premium = Double.valueOf(premval);

		}
		wtcTblDometic.setPremiumValue(BigDecimal.valueOf(premium));
		System.out.println("id from Domestic update -" + premval);
		int wtcTblDometicUpdate = wtcTblDometicMapper.updateByPrimaryKeySelective(wtcTblDometic);
		System.out.println("id from Domestic update -" + wtcTblDometicUpdate);
		String json = "success";
		System.out.println(json);
		/*
		 * System.out.println(name); String result = "Time for " + name + " is " + new
		 * Date().toString();
		 */

		return json;
	}

	@RequestMapping(value = "/UpdateWTCDProdRates", method = RequestMethod.POST)
	public String UpdateWTCDProdRates(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String premid = null;
		String premiumval = null;
		if (request.getParameter("pid") != null) {
			premid = request.getParameter("pid");
		} else {
			premid = String.valueOf(request.getAttribute("pid"));
		}
		if (request.getParameter("premiumval") != null) {
			premiumval = request.getParameter("premiumval");
		} else {
			premiumval = String.valueOf(request.getAttribute("premiumval"));
		}
		System.out.println("id from selection -" + premid);
		WTCTblDometic wtcTblDometic = new WTCTblDometic();
		wtcTblDometic.setId(Short.valueOf(premid));
		Double premium = 0.00;
		if (premiumval != null) {
			premium = Double.valueOf(premiumval);

		}
		wtcTblDometic.setPremiumValue(BigDecimal.valueOf(premium));
		System.out.println("id from Domestic premium -" + premium + "-" + premiumval);
		int wtcTblDometicUpdate = wtcTblDometicMapper.updateByPrimaryKeySelective(wtcTblDometic);
		System.out.println("id from Domestic rates update -" + wtcTblDometicUpdate);

		model.addAttribute("wtcUpdatedMessage", "WTC Premium Updated Successfully!");

		return TravelProductRates(request, response, model);

	}

	@RequestMapping(value = "/UpdateWTCIntProdRates", method = RequestMethod.POST)
	public String UpdateWTCIntProdRates(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String premid = null;
		String premiumval = null;
		if (request.getParameter("pintid") != null) {
			premid = request.getParameter("pintid");
		} else {
			premid = String.valueOf(request.getAttribute("pintid"));
		}
		if (request.getParameter("premiumintval") != null) {
			premiumval = request.getParameter("premiumintval");
		} else {
			premiumval = String.valueOf(request.getAttribute("premiumintval"));
		}
		System.out.println("id from selection -" + premid);

		WTCTblIntwithdays wtcTblIntwithdays = new WTCTblIntwithdays();
		wtcTblIntwithdays.setId(Short.valueOf(premid));
		Double premium = 0.00;
		if (premiumval != null) {
			premium = Double.valueOf(premiumval);

		}
		wtcTblIntwithdays.setPremiumValue(BigDecimal.valueOf(premium));

		// int
		// wtcTblDometicUpdate=wtcTblIntwithdaysMapper.updateByExampleSelective(wtcTblIntwithdays,
		// null);

		int wtcTblDometicUpdate = wtcTblIntwithdaysMapper.updateByPrimaryKeySelective(wtcTblIntwithdays);
		model.addAttribute("wtcUpdatedMessage", "WTC Premium Updated Successfully!");

		return TravelProductRates(request, response, model);

	}

	/*-------------------------------------------------------End- Product Rates---------------------------------------------------------*/
	/*------------------------------------------------------START- Product Info---------------------------------------------------------*/
	@RequestMapping("/Travelproductinfo")
	public String Travelproductinfo(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("WTC Product Info ");

		HttpSession session = request.getSession();

		// Quotaion Validity

		List<WTCTblParam> quotationValidityList = new ArrayList<WTCTblParam>();
		WTCTblParamExample wtcTblParamExample = new WTCTblParamExample();
		WTCTblParamExample.Criteria wtcParam_criteria = wtcTblParamExample.createCriteria();
		wtcParam_criteria.andParamTypeEqualTo("WTC_quote_validity");
		quotationValidityList = wtcTblParamMapper.selectByExample(wtcTblParamExample);

		// Annual Sales Target

		List<WTCTblParam> annualSalesAmntList = new ArrayList<WTCTblParam>();
		WTCTblParamExample wtcTblParamExample1 = new WTCTblParamExample();
		WTCTblParamExample.Criteria wtcParam_criteria1 = wtcTblParamExample1.createCriteria();
		wtcParam_criteria1.andParamTypeEqualTo("WTC_annual_sales_target");
		annualSalesAmntList = wtcTblParamMapper.selectByExample(wtcTblParamExample1);

		List<TblPdfInfo> tblPdfInfoList = new ArrayList<TblPdfInfo>();
		TblPdfInfoExample tblPdfInfoExample = new TblPdfInfoExample();

		TblPdfInfoExample.Criteria miPdfinfo_criteriaExist = tblPdfInfoExample.createCriteria();
		miPdfinfo_criteriaExist.andProductCodeEqualTo("WTC");
		tblPdfInfoExample.setOrderByClause("pds.ID desc");
		// SalesLeadsExample.Criteria salesLeads_criteria=
		// salesLeadsExample.createCriteria();
		tblPdfInfoList = tblPdfInfoMapper.selectByExample(tblPdfInfoExample);

		model.addAttribute("tblPdfInfoList", tblPdfInfoList);

		model.addAttribute("quotationValidityList", quotationValidityList);
		model.addAttribute("annualSalesAmntList", annualSalesAmntList);

		return "/products/travel/travel-prod-mng-product-info";

	}

	@RequestMapping("/updateWTCproductinfo")
	public String updateWTCproductinfo(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("WTC Product Info ");

		HttpSession session = request.getSession();

		// Quotaion Validity

		List<WTCTblParam> quotationValidityList = new ArrayList<WTCTblParam>();
		WTCTblParamExample wtcTblParamExample = new WTCTblParamExample();
		WTCTblParamExample.Criteria wtcParam_criteria = wtcTblParamExample.createCriteria();
		wtcParam_criteria.andParamTypeEqualTo("WTC_quote_validity");
		quotationValidityList = wtcTblParamMapper.selectByExample(wtcTblParamExample);

		// Annual Sales Target

		List<WTCTblParam> annualSalesAmntList = new ArrayList<WTCTblParam>();
		WTCTblParamExample wtcTblParamExample1 = new WTCTblParamExample();
		WTCTblParamExample.Criteria wtcParam_criteria1 = wtcTblParamExample1.createCriteria();
		wtcParam_criteria1.andParamTypeEqualTo("WTC_annual_sales_target");
		annualSalesAmntList = wtcTblParamMapper.selectByExample(wtcTblParamExample1);

		List<TblPdfInfo> tblPdfInfoList = new ArrayList<TblPdfInfo>();
		TblPdfInfoExample tblPdfInfoExample = new TblPdfInfoExample();
		TblPdfInfoExample.Criteria miPdfinfo_criteriaExist = tblPdfInfoExample.createCriteria();
		miPdfinfo_criteriaExist.andProductCodeEqualTo("WTC");
		tblPdfInfoExample.setOrderByClause("pds.ID desc");
		// SalesLeadsExample.Criteria salesLeads_criteria=
		// salesLeadsExample.createCriteria();
		tblPdfInfoList = tblPdfInfoMapper.selectByExample(tblPdfInfoExample);

		model.addAttribute("tblPdfInfoList", tblPdfInfoList);

		model.addAttribute("quotationValidityList", quotationValidityList);
		model.addAttribute("annualSalesAmntList", annualSalesAmntList);

		return "/products/travel/travel-prod-mng-product-info-edit";

	}

	@RequestMapping("/updateDoneWTCproductinfo")
	public String updateDoneWTCproductinfo(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("update Product Info ");

		HttpSession session = request.getSession();

		// Quotaion Validity

		WTCTblParamExample wtcTblParamExample = new WTCTblParamExample();
		WTCTblParam wtcTblParam = new WTCTblParam();
		WTCTblParamExample.Criteria wtcParam_criteria = wtcTblParamExample.createCriteria();
		wtcParam_criteria.andParamTypeEqualTo("WTC_quote_validity");

		String quotVal = request.getParameter("quotVal");
		System.out.println(quotVal + "quotVal");
		wtcTblParam.setParamCode(quotVal == null ? "0" : quotVal);
		String formatVal = numberFormat(quotVal);
		wtcTblParam.setParamDesc(quotVal + " days");
		wtcTblParam.setParamDescMy(quotVal + " days");
		int wtcTblParamUpdate = wtcTblParamMapper.updateByExampleSelective(wtcTblParam, wtcTblParamExample);
		System.out.println(wtcTblParamUpdate + "quotVal value");

		// Annual Sales Target

		WTCTblParamExample wtcTblParamExample1 = new WTCTblParamExample();
		WTCTblParam wtcTblParam1 = new WTCTblParam();
		WTCTblParamExample.Criteria wtcParam_criteria1 = wtcTblParamExample1.createCriteria();
		wtcParam_criteria1.andParamTypeEqualTo("WTC_annual_sales_target");

		String salesTargetVal = request.getParameter("salesTargetVal");
		System.out.println(salesTargetVal + "salesTargetVal");
		wtcTblParam1.setParamCode(salesTargetVal == null ? "0" : salesTargetVal);
		// String formatVal1=numberFormat(salesTargetVal);
		String formatVal1 = salesTargetVal;
		wtcTblParam1.setParamDesc("RM" + formatVal1);
		wtcTblParam1.setParamDescMy("RM" + formatVal1);
		int wtcTblParamUpdate1 = wtcTblParamMapper.updateByExampleSelective(wtcTblParam1, wtcTblParamExample1);
		System.out.println(wtcTblParamUpdate1 + "salesTargetVal value");

		model.addAttribute("updatemessage", "Update data successfully");
		String returnURL = Travelproductinfo(request, response, model);
		return returnURL;

	}

	@RequestMapping(value = "/uploadWTCPdfFile", method = RequestMethod.POST)
	public String uploadWTCPdfFile(@RequestParam("PDF") CommonsMultipartFile file, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		String redirctTO = null, fileName = null, filePath = null;
		String fileNameFormat = new SimpleDateFormat("yyyyMMddHHmm'.pdf'").format(new Date());
		fileName = request.getParameter("WTCFileName");
		if (!file.isEmpty()) {
			/*
			 * File a = convert(file); a.renameTo(new File("G:\\1702\\" +
			 * fileName+fileNameFormat));
			 */
			try (InputStream in = this.getClass().getClassLoader()
					.getResourceAsStream("com/etiqa/dsp/sales/process/email/documents.properties")) {
				Properties prop = new Properties();
				prop.load(in);

				// ********************** File writing to another locaiton
				// Starts*********************
				filePath = prop.getProperty("TLPdfDestinationPath") + fileName + fileNameFormat;
				InputStream input = new FileInputStream(convert(file));
				FileOutputStream fos = new FileOutputStream(filePath, true);
				int b = 0;
				while ((b = input.read()) != -1) {
					fos.write(b);
				}
				System.out.println("File has been written" + filePath);
			} catch (Exception e) {
				System.out.println("Could not create file");
			}
			// ********************** File writing to another locaiton
			// Ends*********************

			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}
			TblPdfInfo tblPdfInfo = new TblPdfInfo();

			Timestamp timestamp = new Timestamp(System.currentTimeMillis());

			tblPdfInfo.setProductCode("WTC");
			tblPdfInfo.setFileName(fileName);
			tblPdfInfo.setFilePath(filePath);
			double bytes = convert(file).length();
			double kilobytes = bytes / 1024;
			DecimalFormat df = new DecimalFormat("0.00");
			tblPdfInfo.setFileSize(df.format(kilobytes));
			tblPdfInfo.setCreatedBy(loginUser);
			tblPdfInfo.setCreatedDate(timestamp);

			tblPdfInfoMapper.insert(tblPdfInfo);

			System.out.println("insertion Successfully Done");
			model.addAttribute("SaveMessage", "Data Added Successfully!");

			redirctTO = Travelproductinfo(request, response, model);
		}
		return redirctTO;
	}

	/*-------------------------------------------------------Start- Delete PDS----------------------------------------------------------*/
	@RequestMapping(value = "/deleteWTCPdsDone", method = RequestMethod.POST)
	public String deleteWTCPdsDone(@ModelAttribute(value = "tblPdfInfo") TblPdfInfo tblPdfInfo, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String id = request.getParameter("id");
		System.out.println(id + " id is deleted!");
		tblPdfInfo.setId(Integer.valueOf(id));
		tblPdfInfoMapper.deleteByPrimaryKey(Short.valueOf(request.getParameter("id").trim()));

		// ******************** file deleting Starts***************************
		// miFilePath
		String wtcFilePath = request.getParameter("wtcFilePath");
		if (wtcFilePath != null || wtcFilePath != "") {
			File file = new File(wtcFilePath);

			if (file.delete()) {
				System.out.println(file.getName() + " is deleted!");
			} else {
				System.out.println("Delete operation is failed.");
			}
		}
		// ******************** file deleting Starts***************************
		System.out.println("Successfully Deleted");
		model.addAttribute("deletemessage", "Delete data successfully");
		String redirctTO = updateWTCproductinfo(request, response, model);
		return redirctTO;

	}

	/*-------------------------------------------------------End- Delete PDS----------------------------------------------------------*/

	@RequestMapping(value = "/WTCDownloadPdfFile", method = RequestMethod.POST)
	// @RequestMapping("/WTCDownloadPdfFile")
	public void WTCDownloadPdfFile(HttpServletRequest request, HttpServletResponse response, Model model) {

		String wtcFilePath = request.getParameter("wtcFilePathDownload");
		System.out.println("wtcFilePath." + wtcFilePath);
		if (wtcFilePath != null || wtcFilePath != "") {
			File pdfFile = new File(wtcFilePath);

			response.setContentType("application/pdf");
			response.addHeader("Content-Disposition", "attachment; filename=" + "Test.pdf");
			response.setContentLength((int) pdfFile.length());

			FileInputStream fileInputStream;
			try {
				fileInputStream = new FileInputStream(pdfFile);
				OutputStream responseOutputStream = response.getOutputStream();
				int bytes;
				while ((bytes = fileInputStream.read()) != -1) {
					responseOutputStream.write(bytes);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	public File convert(MultipartFile file) {
		File convFile = new File(file.getOriginalFilename());
		FileOutputStream fos = null;
		try {
			convFile.createNewFile();
			fos = new FileOutputStream(convFile);
			fos.write(file.getBytes());
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return convFile;
	}

	public String numberFormat(String val) {
		Double iVal = null;
		iVal = Double.parseDouble(val == null ? "0" : val);
		DecimalFormat df = new DecimalFormat("0,000");
		System.out.println(df.format(iVal));
		String formatVal = df.format(iVal);
		return formatVal;
	}

	/**********************************************************
	 * Start by Arbind
	 *********************************************************************/

	@RequestMapping("/updateWTCproductinfoapproval")
	public String updateWTCproductinfoapproval(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("update Product Info  approval ");

		try {

			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}
			// Quotaion Validity
			List<WTCTblParam> listParamWtc_Change = new ArrayList<WTCTblParam>();
			List<String> listofparamType = new ArrayList<String>();
			listofparamType.add("WTC_quote_validity");
			listofparamType.add("WTC_annual_sales_target");
			WTCTblParamExample wtcTblParamExample = new WTCTblParamExample();
			WTCTblParamExample.Criteria wtcParam_criteria = wtcTblParamExample.createCriteria();
			wtcParam_criteria.andParamTypeIn(listofparamType);
			List<WTCTblParam> listParamWtc_Original = wtcTblParamMapper.selectByExample(wtcTblParamExample);

			// Change Blob Storing Starts......
			WTCTblParam wtcChangeBlobParamquotationData = new WTCTblParam();
			String quotVal = request.getParameter("quotVal");
			System.out.println(quotVal + "quotVal");
			wtcChangeBlobParamquotationData.setId(listParamWtc_Original.get(0).getId());
			wtcChangeBlobParamquotationData.setParamType("WTC_quote_validity");
			wtcChangeBlobParamquotationData.setParamCode(quotVal == null ? "0" : quotVal);
			wtcChangeBlobParamquotationData.setParamDesc(quotVal + " days");
			wtcChangeBlobParamquotationData.setParamDescMy(quotVal + " days");

			WTCTblParam wtcChangeBlobParamSalestargetData = new WTCTblParam();
			String salesTargetVal = request.getParameter("salesTargetVal");
			System.out.println(salesTargetVal + "salesTargetVal");
			wtcChangeBlobParamSalestargetData.setParamCode(salesTargetVal == null ? "0" : salesTargetVal);
			// String formatVal1=numberFormat(salesTargetVal);
			String formatVal1 = salesTargetVal;
			wtcChangeBlobParamSalestargetData.setId(listParamWtc_Original.get(1).getId());
			wtcChangeBlobParamSalestargetData.setParamType("WTC_annual_sales_target");
			wtcChangeBlobParamSalestargetData.setParamDesc("RM" + formatVal1);
			wtcChangeBlobParamSalestargetData.setParamDescMy("RM" + formatVal1);

			listParamWtc_Change.add(wtcChangeBlobParamquotationData);
			listParamWtc_Change.add(wtcChangeBlobParamSalestargetData);

			byte[] changeBlob = convertToBytes(listParamWtc_Change);
			byte[] originalBlob = convertToBytes(listParamWtc_Original);

			ApprovalExample approvalExample = new ApprovalExample();
			com.spring.VO.ApprovalExample.Criteria createCriteria_approvalExample = approvalExample.createCriteria();
			createCriteria_approvalExample.andDescriptionEqualTo("WTC/T - CHANGE  PRODUCT  INFORMATION");
			List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

			System.out.println("ORI Data" + originalBlob);
			System.out.println("change Data" + changeBlob);

			BigDecimal approvalProdId = new BigDecimal(0);
			if (listapproval.size() > 0) {
				approvalProdId = listapproval.get(0).getId();
			}

			int maker = Integer.valueOf(loginUser);

			// setting all new change value to ApprovalLog POJA and Insert to ApprovalLog
			// table
			ApprovalLog approvalLog = new ApprovalLog();
			approvalLog.setOriginalContent(originalBlob);
			approvalLog.setNewContent(changeBlob);
			approvalLog.setApprovalId((short) approvalProdId.intValue()); // from Approval table this value[2] based on
																			// menu in feature
			approvalLog.setMaker((short) maker); // session login value
			// approvalLog.setChecker((short)1); //while insert record no need checker value
			approvalLog.setStatus("1");
			approvalLog.setCreateDate(new Date());
			approvalLog.setUpdateDate(new Date());
			approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
			int status = approvalLogMapper.insert(approvalLog);

		} catch (Exception e) {
			e.printStackTrace();
		}

		model.addAttribute("updatemessage", "Update data successfully");
		String returnURL = Travelproductinfo(request, response, model);
		return returnURL;

	}

	private byte[] convertToBytes(Object object) throws IOException {
		try (ByteArrayOutputStream bos = new ByteArrayOutputStream(); ObjectOutput out = new ObjectOutputStream(bos)) {
			out.writeObject(object);
			return bos.toByteArray();
		}
	}

	private Object convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
		try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes); ObjectInput in = new ObjectInputStream(bis)) {
			return in.readObject();
		}
	}

	@RequestMapping("/UpdateTravelProdRatesForApproval")
	public String UpdateTravelProdRatesForApproval(@ModelAttribute(value = "WTCTblParam") WTCTblParam wtcTblParam,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("UpdateTravelProdRates funtion ");

		try {

			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}
			// Stamp Duty

			List<String> listWtcTblParam = new ArrayList<String>();
			listWtcTblParam.add("WTC_StampDuty");
			listWtcTblParam.add("WTC_Gst");
			listWtcTblParam.add("WTC_Discount");

			// Original Blob Formation.....
			WTCTblParamExample wtcTblParamExample = new WTCTblParamExample();
			Criteria createCriteria_wtcTblParamExample = wtcTblParamExample.createCriteria();
			createCriteria_wtcTblParamExample.andParamTypeIn(listWtcTblParam);
			List<WTCTblParam> listwtcparamOriginal = wtcTblParamMapper.selectByExample(wtcTblParamExample);

			// Change Blob Formation.....
			List<WTCTblParam> listwtcparamChange = new ArrayList<WTCTblParam>();
			WTCTblParam wtcTblParamChangestampduty = new WTCTblParam();
			String wtcsduty = request.getParameter("stampduty");
			System.out.println(wtcsduty + "wtcsduty");
			wtcTblParamChangestampduty.setId(listwtcparamOriginal.get(0).getId());
			wtcTblParamChangestampduty.setParamCode(wtcsduty == null ? "0" : wtcsduty);
			String formatsduty = wtcsduty;
			wtcTblParamChangestampduty.setParamDesc(formatsduty);
			wtcTblParamChangestampduty.setParamDescMy(formatsduty);
			wtcTblParamChangestampduty.setParamType("WTC_StampDuty");

			WTCTblParam wtcTblParamChangegst = new WTCTblParam();
			String wtcgst = request.getParameter("gst");
			System.out.println(wtcgst + "wtcgst");
			wtcTblParamChangegst.setId(listwtcparamOriginal.get(1).getId());
			wtcTblParamChangegst.setParamCode(wtcgst == null ? "0" : wtcgst);
			String formatgst = wtcgst;
			wtcTblParamChangegst.setParamDesc(formatgst);
			wtcTblParamChangegst.setParamDescMy(formatgst);
			wtcTblParamChangegst.setParamType("WTC_Gst");

			WTCTblParam wtcTblParamChangediscount = new WTCTblParam();
			String discount = request.getParameter("discount");
			System.out.println(discount + "discount");
			wtcTblParamChangediscount.setParamCode(discount == null ? "0" : discount);
			String formatVal = discount;
			wtcTblParamChangediscount.setId(listwtcparamOriginal.get(2).getId());
			wtcTblParamChangediscount.setParamDesc(formatVal);
			wtcTblParamChangediscount.setParamDescMy(formatVal);
			wtcTblParamChangediscount.setParamType("WTC_Discount");

			listwtcparamChange.add(wtcTblParamChangestampduty);
			listwtcparamChange.add(wtcTblParamChangegst);
			listwtcparamChange.add(wtcTblParamChangediscount);

			byte[] changeBlob = convertToBytes(listwtcparamChange);
			byte[] originalBlob = convertToBytes(listwtcparamOriginal);

			ApprovalExample approvalExample = new ApprovalExample();
			com.spring.VO.ApprovalExample.Criteria createCriteria_approvalExample = approvalExample.createCriteria();
			createCriteria_approvalExample.andDescriptionEqualTo("WTC/T - CHANGE PRODUCT RATE");
			List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

			System.out.println("ORI Data" + originalBlob);
			System.out.println("change Data" + changeBlob);

			BigDecimal approvalProdId = new BigDecimal(0);
			if (listapproval.size() > 0) {
				approvalProdId = listapproval.get(0).getId();
			}

			int maker = Integer.valueOf(loginUser);

			// setting all new change value to ApprovalLog POJA and Insert to ApprovalLog
			// table
			ApprovalLog approvalLog = new ApprovalLog();
			approvalLog.setOriginalContent(originalBlob);
			approvalLog.setNewContent(changeBlob);
			approvalLog.setApprovalId((short) approvalProdId.intValue()); // from Approval table this value[12] based on
																			// menu in feature
			approvalLog.setMaker((short) maker); // session login value
			// approvalLog.setChecker((short)1); //while insert record no need checker value
			approvalLog.setStatus("1");
			approvalLog.setNote("Y");
			approvalLog.setCreateDate(new Date());
			approvalLog.setUpdateDate(new Date());
			approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
			int status = approvalLogMapper.insert(approvalLog);

			if (status == 1) {

				model.addAttribute("updatemessage", "Update data successfully");

			}

			// Direct Discount (Online)

		} catch (Exception e) {
			e.printStackTrace();
		}
		String returnURL = TravelProductRates(request, response, model);
		return returnURL;

	}

	@RequestMapping(value = "/UpdateWTCDProdRatesForApproval", method = RequestMethod.POST)
	public String UpdateWTCDProdRatesForApproval(HttpServletRequest request, HttpServletResponse response,
			Model model) {

		try {
			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			String premid = null;
			String premiumval = null;
			if (request.getParameter("pid") != null) {
				premid = request.getParameter("pid");
			} else {
				premid = String.valueOf(request.getAttribute("pid"));
			}
			if (request.getParameter("premiumval") != null) {
				premiumval = request.getParameter("premiumval");
			} else {
				premiumval = String.valueOf(request.getAttribute("premiumval"));
			}
			System.out.println("id from selection -" + premid);

			// Original BLOB Information.....................
			WTCTblDometicExample wtcTblDometicExample = new WTCTblDometicExample();
			com.spring.VO.WTCTblDometicExample.Criteria wtcTblDometicExample_createCriteria = wtcTblDometicExample
					.createCriteria();
			wtcTblDometicExample_createCriteria.andIdEqualTo(Short.valueOf(premid));
			List<WTCTblDometic> listWtcDomestic = wtcTblDometicMapper.selectByExample(wtcTblDometicExample);

			// List<WTCProdMngPojo> wtcDomesticTblListOrig = new
			// ArrayList<WTCProdMngPojo>();
			// WTCProdMngPojo wtcTblDometicOrig=new WTCProdMngPojo();
			// wtcTblDometicOrig.setWtcRecIdDomestic(String.valueOf(listWtcDomestic.get(0).getId()));
			// wtcTblDometicOrig.setWtcpremiumvalDomestic(String.valueOf(listWtcDomestic.get(0).getPremiumValue()));
			// wtcTblDometicOrig.setWtcProdType("D");
			// wtcTblDometicOrig.setWtcdaysrangekeycodeDomestic(listWtcDomestic.get(0).getDaysRangeKeycode());
			// wtcTblDometicOrig.setWtcdestinationcodeareaDomestic(listWtcDomestic.get(0).getDestinationCodeArea());
			// wtcTblDometicOrig.setWtcpackagecodeDomestic(listWtcDomestic.get(0).getPackageCode());
			// wtcTblDometicOrig.setWtcplancodeDomestic(listWtcDomestic.get(0).getPlanCode());
			// wtcTblDometicOrig.setWtctravelwithtypeidDomestic(listWtcDomestic.get(0).getTravelwithTypeId());
			// wtcDomesticTblListOrig.add(wtcTblDometicOrig);

			// Formation of Change BLOB Formation......
			List<WTCProdMngPojo> wtcDomesticTblListChange = new ArrayList<WTCProdMngPojo>();
			WTCProdMngPojo wtcTblDometic = new WTCProdMngPojo();
			wtcTblDometic.setWtcRecIdDomestic(premid);
			Double premium = 0.00;
			if (premiumval != null) {
				premium = Double.valueOf(premiumval);
			}

			wtcTblDometic.setWtcpremiumvalDomestic(String.valueOf(premium));
			wtcTblDometic.setWtcProdType("D");
			wtcTblDometic.setWtcdaysrangekeycodeDomestic(listWtcDomestic.get(0).getDaysRangeKeycode());
			wtcTblDometic.setWtcdestinationcodeareaDomestic(listWtcDomestic.get(0).getDestinationCodeArea());
			wtcTblDometic.setWtcpackagecodeDomestic(listWtcDomestic.get(0).getPackageCode());
			wtcTblDometic.setWtcplancodeDomestic(listWtcDomestic.get(0).getPlanCode());
			wtcTblDometic.setWtctravelwithtypeidDomestic(listWtcDomestic.get(0).getTravelwithTypeId());
			wtcDomesticTblListChange.add(wtcTblDometic);

			byte[] changeBlob = convertToBytes(wtcDomesticTblListChange);
			byte[] originalBlob = convertToBytes(listWtcDomestic);

			ApprovalExample approvalExample = new ApprovalExample();
			com.spring.VO.ApprovalExample.Criteria createCriteria_approvalExample = approvalExample.createCriteria();
			createCriteria_approvalExample.andDescriptionEqualTo("WTC/T - CHANGE PRODUCT RATE");
			List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

			System.out.println("ORI Data" + originalBlob);
			System.out.println("change Data" + changeBlob);

			BigDecimal approvalProdId = new BigDecimal(0);
			if (listapproval.size() > 0) {
				approvalProdId = listapproval.get(0).getId();
			}

			int maker = Integer.valueOf(loginUser);

			// setting all new change value to ApprovalLog POJA and Insert to ApprovalLog
			// table
			ApprovalLog approvalLog = new ApprovalLog();
			approvalLog.setOriginalContent(originalBlob);
			approvalLog.setNewContent(changeBlob);
			approvalLog.setApprovalId((short) approvalProdId.intValue()); // from Approval table this value[12] based on
																			// menu in feature
			approvalLog.setMaker((short) maker); // session login value
			// approvalLog.setChecker((short)1); //while insert record no need checker value
			approvalLog.setStatus("1");
			approvalLog.setCreateDate(new Date());
			approvalLog.setUpdateDate(new Date());
			approvalLog.setNote("D");
			approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
			int status = approvalLogMapper.insert(approvalLog);

			if (status == 1) {

				model.addAttribute("wtcUpdatedMessage", "WTC Premium Updated Successfully!");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		//
		// System.out.println("id from Domestic premium -"+ premium + "-"+premiumval);
		// int
		// wtcTblDometicUpdate=wtcTblDometicMapper.updateByPrimaryKeySelective(wtcTblDometic);
		// System.out.println("id from Domestic rates update -"+ wtcTblDometicUpdate);

		String returnURL = TravelProductRates(request, response, model);
		return returnURL;

	}

	@RequestMapping(value = "/UpdateWTCIntProdRatesForApproval", method = RequestMethod.POST)
	public String UpdateWTCIntProdRatesForApproval(HttpServletRequest request, HttpServletResponse response,
			Model model) {

		try {
			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}

			String premid = null;
			String premiumval = null;
			if (request.getParameter("pintid") != null) {
				premid = request.getParameter("pintid");
			} else {
				premid = String.valueOf(request.getAttribute("pintid"));
			}
			if (request.getParameter("premiumintval") != null) {
				premiumval = request.getParameter("premiumintval");
			} else {
				premiumval = String.valueOf(request.getAttribute("premiumintval"));
			}
			System.out.println("id from selection -" + premid);

			// Original BLOB Formation......
			WTCTblIntwithdaysExample wtcTblIntwithdaysExampleOrg = new WTCTblIntwithdaysExample();
			com.spring.VO.WTCTblIntwithdaysExample.Criteria wtcTblIntwithdaysExampleOrg_createCriteria = wtcTblIntwithdaysExampleOrg
					.createCriteria();
			wtcTblIntwithdaysExampleOrg_createCriteria.andIdEqualTo(Short.valueOf(premid));
			List<WTCTblIntwithdays> wtcTblWithDaysListOriginal = wtcTblIntwithdaysMapper
					.selectByExample(wtcTblIntwithdaysExampleOrg);

			// Formation of Change BLOB Formation......
			List<WTCProdMngPojo> wtcInternationalTblListChange = new ArrayList<WTCProdMngPojo>();
			WTCProdMngPojo wtcTblInt = new WTCProdMngPojo();
			wtcTblInt.setWtcRecIdInternational(premid);
			Double premium = 0.00;
			if (premiumval != null) {
				premium = Double.valueOf(premiumval);
			}

			wtcTblInt.setWtcpremiumvalInternational(String.valueOf(premium));
			wtcTblInt.setWtcProdType("INT");
			wtcTblInt.setWtcdaysrangekeycodeInternational(wtcTblWithDaysListOriginal.get(0).getDaysRangeKeycode());
			wtcTblInt
					.setWtcdestinationcodeareaInternational(wtcTblWithDaysListOriginal.get(0).getDestinationCodeArea());
			wtcTblInt.setWtcpackagecodeInternational(wtcTblWithDaysListOriginal.get(0).getPackageCode());
			wtcTblInt.setWtcplancodeInternational(wtcTblWithDaysListOriginal.get(0).getPlanCode());
			wtcTblInt.setWtctravelwithtypeidInternational(wtcTblWithDaysListOriginal.get(0).getTravelwithTypeId());
			wtcInternationalTblListChange.add(wtcTblInt);

			System.out.println("   -----------------       " + premid);
			System.out.println("   -----------------       " + wtcTblWithDaysListOriginal.get(0).getDaysRangeKeycode());
			System.out.println(
					"   -----------------       " + wtcTblWithDaysListOriginal.get(0).getDestinationCodeArea());
			System.out.println("   -----------------       " + wtcTblWithDaysListOriginal.get(0).getPackageCode());
			System.out.println("   -----------------       " + wtcTblWithDaysListOriginal.get(0).getPlanCode());
			System.out.println("   -----------------       " + wtcTblWithDaysListOriginal.get(0).getTravelwithTypeId());
			System.out.println("   -----------------       " + premium);
			//
			// System.out.println(wtcTblWithDaysListOriginal+" :::::::::::::: chnage
			// ::::::::::: "+wtcTblWithDaysListChange);

			byte[] changeBlob = convertToBytes(wtcInternationalTblListChange);
			byte[] originalBlob = convertToBytes(wtcTblWithDaysListOriginal);

			System.out.println("ORI Data" + originalBlob);
			System.out.println("change Data" + changeBlob);

			// Fetch approval Id.class..............
			ApprovalExample approvalExample = new ApprovalExample();
			com.spring.VO.ApprovalExample.Criteria createCriteria_approvalExample = approvalExample.createCriteria();
			createCriteria_approvalExample.andDescriptionEqualTo("WTC/T - CHANGE PRODUCT RATE");
			List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

			BigDecimal approvalProdId = new BigDecimal(0);
			if (listapproval.size() > 0) {
				approvalProdId = listapproval.get(0).getId();
			}

			int maker = Integer.valueOf(loginUser);

			// setting all new change value to ApprovalLog POJA and Insert to ApprovalLog
			// table
			ApprovalLog approvalLog = new ApprovalLog();
			approvalLog.setOriginalContent(originalBlob);
			approvalLog.setNewContent(changeBlob);
			approvalLog.setApprovalId((short) approvalProdId.intValue()); // from Approval table this value[12] based on
																			// menu in feature
			approvalLog.setMaker((short) maker); // session login value
			// approvalLog.setChecker((short)1); //while insert record no need checker value
			approvalLog.setStatus("1");
			approvalLog.setCreateDate(new Date());
			approvalLog.setUpdateDate(new Date());
			approvalLog.setNote("INT");
			approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
			int status = approvalLogMapper.insert(approvalLog);

			if (status == 1) {
				model.addAttribute("updatemessage", "Update data successfully");

			}

		} catch (Exception e) {

			e.printStackTrace();
		}

		// int
		// wtcTblDometicUpdate=wtcTblIntwithdaysMapper.updateByPrimaryKeySelective(wtcTblIntwithdays);
		// model.addAttribute("wtcUpdatedMessage","WTC Premium Updated Successfully!");

		String returnURL = TravelProductRates(request, response, model);
		return returnURL;

	}

	/**********************************************************
	 * End by Arbind
	 *********************************************************************/

	/*------------------------------------------------------END- Product Info---------------------------------------------------------*/

}

/*
 * package com.spring.admin;
 *
 * import java.text.DecimalFormat; import java.io.ByteArrayInputStream; import
 * java.io.ByteArrayOutputStream; import java.io.File; import
 * java.io.FileInputStream; import java.io.FileOutputStream; import
 * java.io.IOException; import java.io.InputStream; import java.io.ObjectInput;
 * import java.io.ObjectInputStream; import java.io.ObjectOutput; import
 * java.io.ObjectOutputStream; import java.io.OutputStream; import
 * java.math.BigDecimal; import java.sql.Timestamp; import java.text.DateFormat;
 * import java.text.ParseException; import java.text.SimpleDateFormat; import
 * java.util.ArrayList; import java.util.Arrays; import java.util.Date; import
 * java.util.List; import java.util.Properties;
 *
 * import javax.servlet.http.HttpServletRequest; import
 * javax.servlet.http.HttpServletResponse; import
 * javax.servlet.http.HttpSession;
 *
 * import org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.stereotype.Controller; import
 * org.springframework.ui.Model; import
 * org.springframework.validation.BindingResult; import
 * org.springframework.web.bind.annotation.ModelAttribute; import
 * org.springframework.web.bind.annotation.RequestMapping; import
 * org.springframework.web.bind.annotation.RequestMethod; import
 * org.springframework.web.bind.annotation.RequestParam; import
 * org.springframework.web.bind.annotation.ResponseBody; import
 * org.springframework.web.multipart.MultipartFile; import
 * org.springframework.web.multipart.commons.CommonsMultipartFile;
 *
 * import com.etiqa.DAO.MotorDAO; import com.etiqa.DAO.MotorDAOImpl; import
 * com.fasterxml.jackson.annotation.JsonTypeInfo.Id; import
 * com.google.gson.Gson; import com.spring.VO.Approval; import
 * com.spring.VO.ApprovalExample; import com.spring.VO.ApprovalLog; import
 * com.spring.VO.CommonNVIC; import com.spring.VO.MITblParam; import
 * com.spring.VO.MITblParamExample; import com.spring.VO.MITblPmntParam; import
 * com.spring.VO.MITblPmntParamExample; import com.spring.VO.TblPdfInfo; import
 * com.spring.VO.TblPdfInfoExample; import com.spring.VO.WTCProdMngPojo; import
 * com.spring.VO.WTCTblDometic; import com.spring.VO.WTCTblDometicExample;
 * import com.spring.VO.WTCTblIntwithdays; import
 * com.spring.VO.WTCTblIntwithdaysExample; import com.spring.VO.WTCTblParam;
 * import com.spring.VO.WTCTblParamExample; import
 * com.spring.VO.WTCTblParamExample.Criteria; import
 * com.spring.mapper.AdminParamMapper; import
 * com.spring.mapper.AgentDocumentMapper; import
 * com.spring.mapper.AgentLinkMapper; import
 * com.spring.mapper.AgentProdMapMapper; import
 * com.spring.mapper.AgentProfileMapper; import
 * com.spring.mapper.ApprovalLogMapper; import com.spring.mapper.ApprovalMapper;
 * import com.spring.mapper.CommonQQMapper; import
 * com.spring.mapper.MITblPmntParamMapper; import
 * com.spring.mapper.ProductsMapper; import com.spring.mapper.TblPdfInfoMapper;
 * import com.spring.mapper.WTCTblDometicMapper; import
 * com.spring.mapper.WTCTblIntwithdaysMapper; import
 * com.spring.mapper.WTCTblParamMapper;
 *
 * @Controller public class TravelProdMngController { MITblPmntParamMapper
 * miTblPmntParamMapper; WTCTblDometicMapper wtcTblDometicMapper;
 * WTCTblIntwithdaysMapper wtcTblIntwithdaysMapper; WTCTblParamMapper
 * wtcTblParamMapper; TblPdfInfoMapper tblPdfInfoMapper; ApprovalMapper
 * approvalMapper; ApprovalLogMapper approvalLogMapper; AgentProfileMapper
 * agentProfileMapper; ProductsMapper productsMapper; AgentProdMapMapper
 * agentProdMapMapper; CommonQQMapper commonQQMapper; AdminParamMapper
 * adminParamMapper; AgentDocumentMapper agentDocumentMapper; AgentLinkMapper
 * agentLinkMapper;
 *
 * @Autowired public TravelProdMngController(MITblPmntParamMapper
 * miTblPmntParamMapper, WTCTblDometicMapper wtcTblDometicMapper,
 * WTCTblIntwithdaysMapper wtcTblIntwithdaysMapper, WTCTblParamMapper
 * wtcTblParamMapper, TblPdfInfoMapper tblPdfInfoMapper,ApprovalMapper
 * approvalMapper,ApprovalLogMapper approvalLogMapper) {
 * this.miTblPmntParamMapper=miTblPmntParamMapper; this.wtcTblDometicMapper
 * =wtcTblDometicMapper; this.wtcTblIntwithdaysMapper =wtcTblIntwithdaysMapper;
 * this.wtcTblParamMapper=wtcTblParamMapper;
 * this.tblPdfInfoMapper=tblPdfInfoMapper; this.approvalMapper = approvalMapper;
 * this.approvalLogMapper = approvalLogMapper;
 *
 * }
 *
 *
 * -------------------------------------------------------Start- Payment
 * Method----------------------------------------------------------
 *
 * @RequestMapping("/TravelPaymentMethod") public String
 * TravelPaymentMethod(HttpServletRequest request,HttpServletResponse
 * response,Model model) { System.out.println("TravelPaymentMethod funtion ");
 *
 *
 *
 * HttpSession session=request.getSession();
 *
 * //Maybank2U
 *
 * List<MITblPmntParam> m2ulist= new ArrayList<MITblPmntParam>();
 * MITblPmntParamExample miTblPmntParamExample =new MITblPmntParamExample();
 * MITblPmntParamExample.Criteria
 * MiPmntParam_criteria=miTblPmntParamExample.createCriteria();
 * MiPmntParam_criteria.andPmntGatewayCodeEqualTo("M2U");
 * MiPmntParam_criteria.andProductCodeEqualTo("WTC");
 * MiPmntParam_criteria.andStatusEqualTo(Short.valueOf("1"));
 * m2ulist=miTblPmntParamMapper.selectByExample(miTblPmntParamExample);
 *
 * //FPX
 *
 * List<MITblPmntParam> fpxlist= new ArrayList<MITblPmntParam>();
 * MITblPmntParamExample miTblPmntParamExample1 =new MITblPmntParamExample();
 * MITblPmntParamExample.Criteria
 * MiPmntParam_criteria1=miTblPmntParamExample1.createCriteria();
 * MiPmntParam_criteria1.andPmntGatewayCodeEqualTo("FPX");
 * MiPmntParam_criteria1.andProductCodeEqualTo("WTC");
 * MiPmntParam_criteria1.andStatusEqualTo(Short.valueOf("1"));
 * fpxlist=miTblPmntParamMapper.selectByExample(miTblPmntParamExample1); //EBPG
 * (Visa/Mastercard)
 *
 * List<MITblPmntParam> ebpglist= new ArrayList<MITblPmntParam>();
 * MITblPmntParamExample miTblPmntParamExample2 =new MITblPmntParamExample();
 * MITblPmntParamExample.Criteria
 * MiPmntParam_criteria2=miTblPmntParamExample2.createCriteria();
 * MiPmntParam_criteria2.andPmntGatewayCodeEqualTo("EBPG");
 * MiPmntParam_criteria2.andProductCodeEqualTo("WTC");
 * MiPmntParam_criteria2.andStatusEqualTo(Short.valueOf("1"));
 * ebpglist=miTblPmntParamMapper.selectByExample(miTblPmntParamExample2);
 *
 * //AMEX
 *
 * List<MITblPmntParam> amexlist= new ArrayList<MITblPmntParam>();
 * MITblPmntParamExample miTblPmntParamExample3 =new MITblPmntParamExample();
 * MITblPmntParamExample.Criteria
 * MiPmntParam_criteria3=miTblPmntParamExample3.createCriteria();
 * MiPmntParam_criteria3.andPmntGatewayCodeEqualTo("AMEX");
 * MiPmntParam_criteria3.andProductCodeEqualTo("WTC");
 * MiPmntParam_criteria3.andStatusEqualTo(Short.valueOf("1"));
 * amexlist=miTblPmntParamMapper.selectByExample(miTblPmntParamExample3);
 *
 *
 *
 * model.addAttribute("m2ulist",m2ulist); model.addAttribute("fpxlist",fpxlist);
 * model.addAttribute("ebpglist",ebpglist);
 * model.addAttribute("amexlist",amexlist);
 *
 *
 * return "/products/travel/travel-prod-mng-pmnt-method";
 *
 * }
 *
 * @RequestMapping("/UpdateTravelPaymentMethod") public String
 * UpdateTravelPaymentMethod(HttpServletRequest request,HttpServletResponse
 * response,Model model) {
 * System.out.println("UPDATE TravelPaymentMethod funtion ");
 *
 *
 *
 * HttpSession session=request.getSession();
 *
 *
 * //Maybank2U
 *
 * List<MITblPmntParam> m2ulist= new ArrayList<MITblPmntParam>();
 * MITblPmntParamExample miTblPmntParamExample =new MITblPmntParamExample();
 * MITblPmntParamExample.Criteria
 * MiPmntParam_criteria=miTblPmntParamExample.createCriteria();
 * MiPmntParam_criteria.andPmntGatewayCodeEqualTo("M2U");
 * MiPmntParam_criteria.andProductCodeEqualTo("WTC");
 * MiPmntParam_criteria.andStatusEqualTo(Short.valueOf("1"));
 * m2ulist=miTblPmntParamMapper.selectByExample(miTblPmntParamExample);
 *
 * //FPX
 *
 * List<MITblPmntParam> fpxlist= new ArrayList<MITblPmntParam>();
 * MITblPmntParamExample miTblPmntParamExample1 =new MITblPmntParamExample();
 * MITblPmntParamExample.Criteria
 * MiPmntParam_criteria1=miTblPmntParamExample1.createCriteria();
 * MiPmntParam_criteria1.andPmntGatewayCodeEqualTo("FPX");
 * MiPmntParam_criteria1.andProductCodeEqualTo("WTC");
 * MiPmntParam_criteria1.andStatusEqualTo(Short.valueOf("1"));
 * fpxlist=miTblPmntParamMapper.selectByExample(miTblPmntParamExample1); //EBPG
 * (Visa/ Mastercard)
 *
 * List<MITblPmntParam> ebpglist= new ArrayList<MITblPmntParam>();
 * MITblPmntParamExample miTblPmntParamExample2 =new MITblPmntParamExample();
 * MITblPmntParamExample.Criteria
 * MiPmntParam_criteria2=miTblPmntParamExample2.createCriteria();
 * MiPmntParam_criteria2.andPmntGatewayCodeEqualTo("EBPG");
 * MiPmntParam_criteria2.andProductCodeEqualTo("WTC");
 * MiPmntParam_criteria2.andStatusEqualTo(Short.valueOf("1"));
 * ebpglist=miTblPmntParamMapper.selectByExample(miTblPmntParamExample2);
 *
 * //AMEX
 *
 * List<MITblPmntParam> amexlist= new ArrayList<MITblPmntParam>();
 * MITblPmntParamExample miTblPmntParamExample3 =new MITblPmntParamExample();
 * MITblPmntParamExample.Criteria
 * MiPmntParam_criteria3=miTblPmntParamExample3.createCriteria();
 * MiPmntParam_criteria3.andPmntGatewayCodeEqualTo("AMEX");
 * MiPmntParam_criteria3.andProductCodeEqualTo("WTC");
 * MiPmntParam_criteria3.andStatusEqualTo(Short.valueOf("1"));
 * amexlist=miTblPmntParamMapper.selectByExample(miTblPmntParamExample3);
 *
 *
 * model.addAttribute("m2ulist",m2ulist); model.addAttribute("fpxlist",fpxlist);
 * model.addAttribute("ebpglist",ebpglist);
 * model.addAttribute("amexlist",amexlist);
 *
 *
 *
 * return "/products/travel/travel-prod-mng-pmnt-method-edit";
 *
 * }
 *
 * @RequestMapping("/updateDoneTravelPaymentMethod") public String
 * updateDoneTravelPaymentMethod(@ModelAttribute(value="MITblPmntParam")
 * MITblPmntParam miTblPmntParam, HttpServletRequest request,HttpServletResponse
 * response,Model model) {
 * System.out.println("updateDoneTravelPaymentMethod update done funtion ");
 *
 *
 *
 * HttpSession session=request.getSession();
 *
 *
 *
 * //Maybank2U MITblPmntParamExample miTblPmntParamExample =new
 * MITblPmntParamExample(); MITblPmntParamExample.Criteria
 * MiPmntParam_criteria=miTblPmntParamExample.createCriteria();
 * MiPmntParam_criteria.andPmntGatewayCodeEqualTo("M2U");
 * MiPmntParam_criteria.andProductCodeEqualTo("WTC"); String m2u = "0"; if
 * (request.getParameter("m2u") != null) { m2u=request.getParameter("m2u");
 * if(m2u.equals("on")){ m2u="1"; } }
 * miTblPmntParam.setStatus(Short.valueOf(m2u)); int
 * miTblPmntParamUpdate=miTblPmntParamMapper.updateByExampleSelective(
 * miTblPmntParam, miTblPmntParamExample);
 * System.out.println(miTblPmntParamUpdate+"M2U");
 *
 *
 * //FPX MITblPmntParamExample miTblPmntParamExample1 =new
 * MITblPmntParamExample(); MITblPmntParamExample.Criteria
 * MiPmntParam_criteria1=miTblPmntParamExample1.createCriteria();
 * MiPmntParam_criteria1.andPmntGatewayCodeEqualTo("FPX");
 * MiPmntParam_criteria1.andProductCodeEqualTo("WTC"); String fpx = "0"; if
 * (request.getParameter("fpx") != null) { fpx=request.getParameter("fpx");
 * if(fpx.equals("on")){ fpx="1"; } } System.out.println(fpx+" fpx");
 *
 * miTblPmntParam.setStatus(Short.valueOf(fpx)); int
 * miTblPmntParamUpdate1=miTblPmntParamMapper.updateByExampleSelective(
 * miTblPmntParam, miTblPmntParamExample1);
 * System.out.println(miTblPmntParamUpdate1+"FPX");
 *
 * //EBPG MITblPmntParamExample miTblPmntParamExample2 =new
 * MITblPmntParamExample(); MITblPmntParamExample.Criteria
 * MiPmntParam_criteria2=miTblPmntParamExample2.createCriteria();
 * MiPmntParam_criteria2.andPmntGatewayCodeEqualTo("EBPG");
 * MiPmntParam_criteria2.andProductCodeEqualTo("WTC"); String ebpg = "0"; if
 * (request.getParameter("ebpg") != null) { ebpg=request.getParameter("ebpg");
 * if(ebpg.equals("on")){ ebpg="1"; } }
 * miTblPmntParam.setStatus(Short.valueOf(ebpg)); int
 * miTblPmntParamUpdate2=miTblPmntParamMapper.updateByExampleSelective(
 * miTblPmntParam, miTblPmntParamExample2);
 * System.out.println(miTblPmntParamUpdate2+"EBPG");
 *
 *
 * //AMEX MITblPmntParamExample miTblPmntParamExample3 =new
 * MITblPmntParamExample(); MITblPmntParamExample.Criteria
 * MiPmntParam_criteria3=miTblPmntParamExample3.createCriteria();
 * MiPmntParam_criteria3.andPmntGatewayCodeEqualTo("AMEX");
 * MiPmntParam_criteria3.andProductCodeEqualTo("WTC"); String amex = "0"; if
 * (request.getParameter("amex") != null) { amex=request.getParameter("amex");
 * if(amex.equals("on")){ amex="1"; } }
 * miTblPmntParam.setStatus(Short.valueOf(amex)); int
 * miTblPmntParamUpdate3=miTblPmntParamMapper.updateByExampleSelective(
 * miTblPmntParam, miTblPmntParamExample3);
 * System.out.println(miTblPmntParamUpdate3+" AMEX");
 * model.addAttribute("updatemessage","Update data successfully"); String
 * returnURL=TravelPaymentMethod(request,response,model);
 *
 *
 * return returnURL; }
 *
 *
 *
 * -------------------------------------------------------End- Payment
 * Method----------------------------------------------------------
 *
 * -------------------------------------------------------Start- Product
 * Rates----------------------------------------------------------
 *
 * @RequestMapping("/TravelProductRates") public String
 * TravelProductRates(HttpServletRequest request,HttpServletResponse
 * response,Model model) { System.out.println("TravelProductRates funtion ");
 *
 *
 *
 * HttpSession session=request.getSession();
 *
 * //Domestic Premium values
 *
 * List<WTCTblDometic> DomPremiumList18= new ArrayList<WTCTblDometic>();
 * WTCTblDometicExample wtcTblDometicExample =new WTCTblDometicExample();
 * WTCTblDometicExample.Criteria
 * WTCTblDometic_criteria=wtcTblDometicExample.createCriteria();
 * //WTCTblDometic_criteria.andDaysRangeKeycodeEqualTo("D1_5");
 * WTCTblDometic_criteria.andTravelwithTypeIdEqualTo("W01");
 * DomPremiumList18=wtcTblDometicMapper.selectByExample(wtcTblDometicExample);
 *
 * List<WTCTblDometic> DomPremiumList71= new ArrayList<WTCTblDometic>();
 * WTCTblDometicExample wtcTblDometicExample1 =new WTCTblDometicExample();
 * WTCTblDometicExample.Criteria
 * WTCTblDometic_criteria1=wtcTblDometicExample1.createCriteria();
 * //WTCTblDometic_criteria.andDaysRangeKeycodeEqualTo("D1_5");
 * WTCTblDometic_criteria1.andTravelwithTypeIdEqualTo("W07");
 * DomPremiumList71=wtcTblDometicMapper.selectByExample(wtcTblDometicExample1);
 *
 * List<WTCTblDometic> DomPremiumListIndv= new ArrayList<WTCTblDometic>();
 * WTCTblDometicExample wtcTblDometicExample2 =new WTCTblDometicExample();
 * WTCTblDometicExample.Criteria
 * WTCTblDometic_criteria2=wtcTblDometicExample2.createCriteria();
 * //WTCTblDometic_criteria.andDaysRangeKeycodeEqualTo("D1_5");
 * WTCTblDometic_criteria2.andTravelwithTypeIdEqualTo("W03");
 * DomPremiumListIndv=wtcTblDometicMapper.selectByExample(wtcTblDometicExample2)
 * ;
 *
 * List<WTCTblDometic> DomPremiumListFamily= new ArrayList<WTCTblDometic>();
 * WTCTblDometicExample wtcTblDometicExample3 =new WTCTblDometicExample();
 * WTCTblDometicExample.Criteria
 * WTCTblDometic_criteria3=wtcTblDometicExample3.createCriteria();
 * //WTCTblDometic_criteria.andDaysRangeKeycodeEqualTo("D1_5");
 * WTCTblDometic_criteria3.andTravelwithTypeIdEqualTo("W05");
 * DomPremiumListFamily=wtcTblDometicMapper.selectByExample(
 * wtcTblDometicExample3);
 *
 *
 * //International Premium Values
 *
 * List<WTCTblIntwithdays> IntPremiumList18= new ArrayList<WTCTblIntwithdays>();
 * WTCTblIntwithdaysExample wtcTblIntwithdaysExample =new
 * WTCTblIntwithdaysExample(); WTCTblIntwithdaysExample.Criteria
 * WTCTblIntwithdays_criteria=wtcTblIntwithdaysExample.createCriteria();
 * //WTCTblDometic_criteria.andDaysRangeKeycodeEqualTo("D1_5");
 * WTCTblIntwithdays_criteria.andTravelwithTypeIdEqualTo("W01");
 * IntPremiumList18=wtcTblIntwithdaysMapper.selectByExample(
 * wtcTblIntwithdaysExample);
 *
 * List<WTCTblIntwithdays> IntPremiumList71= new ArrayList<WTCTblIntwithdays>();
 * WTCTblIntwithdaysExample wtcTblIntwithdaysExample1 =new
 * WTCTblIntwithdaysExample(); WTCTblIntwithdaysExample.Criteria
 * WTCTblIntwithdays_criteria1=wtcTblIntwithdaysExample1.createCriteria();
 * //WTCTblDometic_criteria.andDaysRangeKeycodeEqualTo("D1_5");
 * WTCTblIntwithdays_criteria1.andTravelwithTypeIdEqualTo("W07");
 * IntPremiumList71=wtcTblIntwithdaysMapper.selectByExample(
 * wtcTblIntwithdaysExample1);
 *
 * List<WTCTblIntwithdays> IntPremiumListIndv= new
 * ArrayList<WTCTblIntwithdays>(); WTCTblIntwithdaysExample
 * wtcTblIntwithdaysExample2 =new WTCTblIntwithdaysExample();
 * WTCTblIntwithdaysExample.Criteria
 * WTCTblIntwithdays_criteria2=wtcTblIntwithdaysExample2.createCriteria();
 * //WTCTblDometic_criteria.andDaysRangeKeycodeEqualTo("D1_5");
 * WTCTblIntwithdays_criteria2.andTravelwithTypeIdEqualTo("W03");
 * IntPremiumListIndv=wtcTblIntwithdaysMapper.selectByExample(
 * wtcTblIntwithdaysExample2);
 *
 * List<WTCTblIntwithdays> IntPremiumListFamily= new
 * ArrayList<WTCTblIntwithdays>(); WTCTblIntwithdaysExample
 * wtcTblIntwithdaysExample3 =new WTCTblIntwithdaysExample();
 * WTCTblIntwithdaysExample.Criteria
 * WTCTblIntwithdays_criteria3=wtcTblIntwithdaysExample3.createCriteria();
 * //WTCTblDometic_criteria.andDaysRangeKeycodeEqualTo("D1_5");
 * WTCTblIntwithdays_criteria3.andTravelwithTypeIdEqualTo("W05");
 * IntPremiumListFamily=wtcTblIntwithdaysMapper.selectByExample(
 * wtcTblIntwithdaysExample3);
 *
 * //Stamp Duty
 *
 * List<WTCTblParam> stampDutyList= new ArrayList<WTCTblParam>();
 * WTCTblParamExample dspWtcTblParamExample =new WTCTblParamExample();
 * WTCTblParamExample.Criteria
 * dspWtcParam_criteria=dspWtcTblParamExample.createCriteria();
 * dspWtcParam_criteria.andParamTypeEqualTo("WTC_StampDuty");
 * stampDutyList=wtcTblParamMapper.selectByExample(dspWtcTblParamExample);
 *
 * //GST
 *
 * List<WTCTblParam> gstValueList= new ArrayList<WTCTblParam>();
 * WTCTblParamExample dspWtcTblParamExample1 =new WTCTblParamExample();
 * WTCTblParamExample.Criteria
 * dspWtcParam_criteria1=dspWtcTblParamExample1.createCriteria();
 * dspWtcParam_criteria1.andParamTypeEqualTo("WTC_Gst");
 * gstValueList=wtcTblParamMapper.selectByExample(dspWtcTblParamExample1);
 *
 * //Direct Discount (Online)
 *
 * List<WTCTblParam> discountValueList= new ArrayList<WTCTblParam>();
 * WTCTblParamExample dspWtcTblParamExample2 =new WTCTblParamExample();
 * WTCTblParamExample.Criteria
 * dspWtcParam_criteria2=dspWtcTblParamExample2.createCriteria();
 * dspWtcParam_criteria2.andParamTypeEqualTo("WTC_Discount");
 * discountValueList=wtcTblParamMapper.selectByExample(dspWtcTblParamExample2);
 *
 * model.addAttribute("DomPremiumList18",DomPremiumList18);
 * model.addAttribute("DomPremiumList71",DomPremiumList71);
 * model.addAttribute("DomPremiumListIndv",DomPremiumListIndv);
 * model.addAttribute("DomPremiumListFamily",DomPremiumListFamily);
 *
 * model.addAttribute("IntPremiumList18",IntPremiumList18);
 * model.addAttribute("IntPremiumList71",IntPremiumList71);
 * model.addAttribute("IntPremiumListIndv",IntPremiumListIndv);
 * model.addAttribute("IntPremiumListFamily",IntPremiumListFamily);
 *
 * model.addAttribute("stampDutyList",stampDutyList);
 * model.addAttribute("gstValueList",gstValueList);
 * model.addAttribute("discountValueList",discountValueList);
 *
 * return "/products/travel/travel-prod-mng-prod-rates";
 *
 * }
 *
 * @RequestMapping("/UpdateTravelProdRates") public String
 * UpdateTravelProdRates(HttpServletRequest request,HttpServletResponse
 * response,Model model) { System.out.println("UpdateTravelProdRates funtion ");
 *
 *
 *
 * HttpSession session=request.getSession();
 *
 *
 *
 * //Stamp Duty
 *
 * List<WTCTblParam> stampDutyList= new ArrayList<WTCTblParam>();
 * WTCTblParamExample dspWtcTblParamExample =new WTCTblParamExample();
 * WTCTblParamExample.Criteria
 * dspWtcParam_criteria=dspWtcTblParamExample.createCriteria();
 * dspWtcParam_criteria.andParamTypeEqualTo("WTC_StampDuty");
 * stampDutyList=wtcTblParamMapper.selectByExample(dspWtcTblParamExample);
 *
 * //GST
 *
 * List<WTCTblParam> gstValueList= new ArrayList<WTCTblParam>();
 * WTCTblParamExample dspWtcTblParamExample1 =new WTCTblParamExample();
 * WTCTblParamExample.Criteria
 * dspWtcParam_criteria1=dspWtcTblParamExample1.createCriteria();
 * dspWtcParam_criteria1.andParamTypeEqualTo("WTC_Gst");
 * gstValueList=wtcTblParamMapper.selectByExample(dspWtcTblParamExample1);
 *
 * //Direct Discount (Online)
 *
 * List<WTCTblParam> discountValueList= new ArrayList<WTCTblParam>();
 * WTCTblParamExample dspWtcTblParamExample2 =new WTCTblParamExample();
 * WTCTblParamExample.Criteria
 * dspWtcParam_criteria2=dspWtcTblParamExample2.createCriteria();
 * dspWtcParam_criteria2.andParamTypeEqualTo("WTC_Discount");
 * discountValueList=wtcTblParamMapper.selectByExample(dspWtcTblParamExample2);
 *
 *
 * model.addAttribute("stampDutyList",stampDutyList);
 * model.addAttribute("gstValueList",gstValueList);
 * model.addAttribute("discountValueList",discountValueList);
 *
 * return "/products/travel/travel-prod-mng-prod-rates-edit";
 *
 * }
 *
 * @RequestMapping("/UpdateTravelProdRatesDone") public String
 * UpdateTravelProdRatesDone(@ModelAttribute(value="WTCTblParam") WTCTblParam
 * wtcTblParam, HttpServletRequest request,HttpServletResponse response,Model
 * model) { System.out.println("UpdateTravelProdRates funtion ");
 *
 *
 *
 * HttpSession session=request.getSession();
 *
 *
 *
 * //Stamp Duty
 *
 *
 * WTCTblParamExample wtcTblParamExample2 =new WTCTblParamExample();
 * WTCTblParamExample.Criteria
 * wtcParam_criteria2=wtcTblParamExample2.createCriteria();
 * wtcParam_criteria2.andParamTypeEqualTo("WTC_StampDuty"); String wtcsduty =
 * request.getParameter("stampduty"); System.out.println(wtcsduty+"wtcsduty");
 * wtcTblParam.setParamCode(wtcsduty==null?"0":wtcsduty); String
 * formatsduty=wtcsduty; wtcTblParam.setParamDesc(formatsduty);
 * wtcTblParam.setParamDescMy(formatsduty); int
 * wtcTblParamUpdate2=wtcTblParamMapper.updateByExampleSelective(wtcTblParam,
 * wtcTblParamExample2); System.out.println(wtcTblParamUpdate2+"wtcsduty");
 *
 * //GST
 *
 *
 * WTCTblParamExample wtcTblParamExample1 =new WTCTblParamExample();
 * WTCTblParamExample.Criteria
 * wtcParam_criteria1=wtcTblParamExample1.createCriteria();
 * wtcParam_criteria1.andParamTypeEqualTo("WTC_Gst"); String wtcgst =
 * request.getParameter("gst"); System.out.println(wtcgst+"wtcgst");
 * wtcTblParam.setParamCode(wtcgst==null?"0":wtcgst); String formatgst=wtcgst;
 * wtcTblParam.setParamDesc(formatgst); wtcTblParam.setParamDescMy(formatgst);
 * int
 * wtcTblParamUpdate1=wtcTblParamMapper.updateByExampleSelective(wtcTblParam,
 * wtcTblParamExample1); System.out.println(wtcTblParamUpdate1+"wtcgst");
 *
 * //Direct Discount (Online)
 *
 * WTCTblParamExample wtcTblParamExample =new WTCTblParamExample();
 * WTCTblParamExample.Criteria
 * wtcParam_criteria=wtcTblParamExample.createCriteria();
 * wtcParam_criteria.andParamTypeEqualTo("WTC_Discount"); String discount =
 * request.getParameter("discount"); System.out.println(discount+"discount");
 * wtcTblParam.setParamCode(discount==null?"0":discount); String
 * formatVal=discount; wtcTblParam.setParamDesc(formatVal);
 * wtcTblParam.setParamDescMy(formatVal); int
 * wtcTblParamUpdate=wtcTblParamMapper.updateByExampleSelective(wtcTblParam,
 * wtcTblParamExample); System.out.println(wtcTblParamUpdate+"discount");
 * model.addAttribute(
 * "wtcUpdatedprMessage","Product Rates Updated Successfully!"); String
 * returnURL=TravelProductRates(request,response,model);
 *
 *
 * return returnURL;
 *
 * }
 *
 * @RequestMapping(value = "/UpdateWTCProdRates", method=RequestMethod.GET)
 * public @ResponseBody String UpdateWTCProdRates(@RequestParam String
 * pid,@RequestParam String premval, HttpServletRequest request,
 * HttpServletResponse response, Model model) throws Exception {
 *
 *
 * String premid=null; if (request.getParameter("pid") != null) { premid=
 * request.getParameter("pid"); } else { premid=String.valueOf((Integer)
 * request.getAttribute("pid")); } System.out.println("id from selection -"+
 * premid); WTCTblDometic wtcTblDometic=new WTCTblDometic();
 * wtcTblDometic.setId(Short.valueOf(premid)); Double premium =0.00;
 * if(premval!=null){ premium = Double.valueOf(premval);
 *
 * } wtcTblDometic.setPremiumValue(BigDecimal.valueOf(premium));
 * System.out.println("id from Domestic update -"+ premval); int
 * wtcTblDometicUpdate=wtcTblDometicMapper.updateByPrimaryKeySelective(
 * wtcTblDometic); System.out.println("id from Domestic update -"+
 * wtcTblDometicUpdate); String json ="success"; System.out.println(json);
 * System.out.println(name); String result = "Time for " + name + " is " + new
 * Date().toString();
 *
 * return json; }
 *
 * @RequestMapping(value ="/UpdateWTCDProdRates", method=RequestMethod.POST)
 * public String UpdateWTCDProdRates(HttpServletRequest request,
 * HttpServletResponse response, Model model) {
 *
 * HttpSession session = request.getSession(); String loginUser
 * =(String)session.getAttribute("user"); if(loginUser == null) { String
 * sessionexpired = "Session Has Been Expired";
 * model.addAttribute("sessionexpired", sessionexpired); return "admin-login"; }
 *
 * String premid=null; String premiumval=null; if (request.getParameter("pid")
 * != null) { premid= request.getParameter("pid"); } else {
 * premid=String.valueOf((Integer) request.getAttribute("pid")); } if
 * (request.getParameter("premiumval") != null) { premiumval=
 * request.getParameter("premiumval"); } else {
 * premiumval=String.valueOf((Integer) request.getAttribute("premiumval")); }
 * System.out.println("id from selection -"+ premid); WTCTblDometic
 * wtcTblDometic=new WTCTblDometic();
 * wtcTblDometic.setId(Short.valueOf(premid)); Double premium =0.00;
 * if(premiumval!=null){ premium = Double.valueOf(premiumval);
 *
 * } wtcTblDometic.setPremiumValue(BigDecimal.valueOf(premium));
 * System.out.println("id from Domestic premium -"+ premium + "-"+premiumval);
 * int wtcTblDometicUpdate=wtcTblDometicMapper.updateByPrimaryKeySelective(
 * wtcTblDometic); System.out.println("id from Domestic rates update -"+
 * wtcTblDometicUpdate);
 *
 * model.addAttribute("wtcUpdatedMessage","WTC Premium Updated Successfully!");
 *
 * return TravelProductRates(request, response, model);
 *
 *
 *
 * }
 *
 * @RequestMapping(value ="/UpdateWTCIntProdRates", method=RequestMethod.POST)
 * public String UpdateWTCIntProdRates(HttpServletRequest request,
 * HttpServletResponse response, Model model) {
 *
 * HttpSession session = request.getSession(); String loginUser
 * =(String)session.getAttribute("user"); if(loginUser == null) { String
 * sessionexpired = "Session Has Been Expired";
 * model.addAttribute("sessionexpired", sessionexpired); return "admin-login"; }
 *
 * String premid=null; String premiumval=null; if
 * (request.getParameter("pintid") != null) { premid=
 * request.getParameter("pintid"); } else { premid=String.valueOf((Integer)
 * request.getAttribute("pintid")); } if (request.getParameter("premiumintval")
 * != null) { premiumval= request.getParameter("premiumintval"); } else {
 * premiumval=String.valueOf((Integer) request.getAttribute("premiumintval")); }
 * System.out.println("id from selection -"+ premid);
 *
 * WTCTblIntwithdays wtcTblIntwithdays=new WTCTblIntwithdays();
 * wtcTblIntwithdays.setId(Short.valueOf(premid)); Double premium =0.00;
 * if(premiumval!=null){ premium = Double.valueOf(premiumval);
 *
 * } wtcTblIntwithdays.setPremiumValue(BigDecimal.valueOf(premium));
 *
 * //int wtcTblDometicUpdate=wtcTblIntwithdaysMapper.updateByExampleSelective(
 * wtcTblIntwithdays, null);
 *
 * int wtcTblDometicUpdate=wtcTblIntwithdaysMapper.updateByPrimaryKeySelective(
 * wtcTblIntwithdays);
 * model.addAttribute("wtcUpdatedMessage","WTC Premium Updated Successfully!");
 *
 * return TravelProductRates(request, response, model);
 *
 *
 *
 * } -------------------------------------------------------End- Product
 * Rates---------------------------------------------------------
 * ------------------------------------------------------START- Product
 * Info---------------------------------------------------------
 *
 * @RequestMapping("/Travelproductinfo") public String
 * Travelproductinfo(HttpServletRequest request,HttpServletResponse
 * response,Model model) { System.out.println("WTC Product Info ");
 *
 *
 *
 * HttpSession session=request.getSession();
 *
 * //Quotaion Validity
 *
 * List<WTCTblParam> quotationValidityList= new ArrayList<WTCTblParam>();
 * WTCTblParamExample wtcTblParamExample =new WTCTblParamExample();
 * WTCTblParamExample.Criteria
 * wtcParam_criteria=wtcTblParamExample.createCriteria();
 * wtcParam_criteria.andParamTypeEqualTo("WTC_quote_validity");
 * quotationValidityList=wtcTblParamMapper.selectByExample(wtcTblParamExample);
 *
 * //Annual Sales Target
 *
 * List<WTCTblParam> annualSalesAmntList= new ArrayList<WTCTblParam>();
 * WTCTblParamExample wtcTblParamExample1 =new WTCTblParamExample();
 * WTCTblParamExample.Criteria
 * wtcParam_criteria1=wtcTblParamExample1.createCriteria();
 * wtcParam_criteria1.andParamTypeEqualTo("WTC_annual_sales_target");
 * annualSalesAmntList=wtcTblParamMapper.selectByExample(wtcTblParamExample1);
 *
 * List<TblPdfInfo> tblPdfInfoList= new ArrayList<TblPdfInfo>();
 * TblPdfInfoExample tblPdfInfoExample =new TblPdfInfoExample();
 *
 * TblPdfInfoExample.Criteria miPdfinfo_criteriaExist=
 * tblPdfInfoExample.createCriteria();
 * miPdfinfo_criteriaExist.andProductCodeEqualTo("WTC");
 * tblPdfInfoExample.setOrderByClause("pds.ID desc");
 * //SalesLeadsExample.Criteria salesLeads_criteria=
 * salesLeadsExample.createCriteria();
 * tblPdfInfoList=tblPdfInfoMapper.selectByExample(tblPdfInfoExample);
 *
 * model.addAttribute("tblPdfInfoList",tblPdfInfoList);
 *
 * model.addAttribute("quotationValidityList",quotationValidityList);
 * model.addAttribute("annualSalesAmntList",annualSalesAmntList);
 *
 *
 *
 * return "/products/travel/travel-prod-mng-product-info";
 *
 * }
 *
 * @RequestMapping("/updateWTCproductinfo") public String
 * updateWTCproductinfo(HttpServletRequest request,HttpServletResponse
 * response,Model model) { System.out.println("WTC Product Info ");
 *
 *
 *
 * HttpSession session=request.getSession();
 *
 * //Quotaion Validity
 *
 * List<WTCTblParam> quotationValidityList= new ArrayList<WTCTblParam>();
 * WTCTblParamExample wtcTblParamExample =new WTCTblParamExample();
 * WTCTblParamExample.Criteria
 * wtcParam_criteria=wtcTblParamExample.createCriteria();
 * wtcParam_criteria.andParamTypeEqualTo("WTC_quote_validity");
 * quotationValidityList=wtcTblParamMapper.selectByExample(wtcTblParamExample);
 *
 * //Annual Sales Target
 *
 * List<WTCTblParam> annualSalesAmntList= new ArrayList<WTCTblParam>();
 * WTCTblParamExample wtcTblParamExample1 =new WTCTblParamExample();
 * WTCTblParamExample.Criteria
 * wtcParam_criteria1=wtcTblParamExample1.createCriteria();
 * wtcParam_criteria1.andParamTypeEqualTo("WTC_annual_sales_target");
 * annualSalesAmntList=wtcTblParamMapper.selectByExample(wtcTblParamExample1);
 *
 * List<TblPdfInfo> tblPdfInfoList= new ArrayList<TblPdfInfo>();
 * TblPdfInfoExample tblPdfInfoExample =new TblPdfInfoExample();
 * TblPdfInfoExample.Criteria miPdfinfo_criteriaExist=
 * tblPdfInfoExample.createCriteria();
 * miPdfinfo_criteriaExist.andProductCodeEqualTo("WTC");
 * tblPdfInfoExample.setOrderByClause("pds.ID desc");
 * //SalesLeadsExample.Criteria salesLeads_criteria=
 * salesLeadsExample.createCriteria();
 * tblPdfInfoList=tblPdfInfoMapper.selectByExample(tblPdfInfoExample);
 *
 * model.addAttribute("tblPdfInfoList",tblPdfInfoList);
 *
 * model.addAttribute("quotationValidityList",quotationValidityList);
 * model.addAttribute("annualSalesAmntList",annualSalesAmntList);
 *
 *
 *
 * return "/products/travel/travel-prod-mng-product-info-edit";
 *
 * }
 *
 * @RequestMapping("/updateDoneWTCproductinfo") public String
 * updateDoneWTCproductinfo(HttpServletRequest request,HttpServletResponse
 * response,Model model) { System.out.println("update Product Info ");
 *
 * HttpSession session=request.getSession();
 *
 * //Quotaion Validity
 *
 * WTCTblParamExample wtcTblParamExample =new WTCTblParamExample(); WTCTblParam
 * wtcTblParam=new WTCTblParam(); WTCTblParamExample.Criteria
 * wtcParam_criteria=wtcTblParamExample.createCriteria();
 * wtcParam_criteria.andParamTypeEqualTo("WTC_quote_validity");
 *
 * String quotVal = request.getParameter("quotVal");
 * System.out.println(quotVal+"quotVal");
 * wtcTblParam.setParamCode(quotVal==null?"0":quotVal); String
 * formatVal=numberFormat(quotVal); wtcTblParam.setParamDesc(quotVal+" days");
 * wtcTblParam.setParamDescMy(quotVal+" days"); int
 * wtcTblParamUpdate=wtcTblParamMapper.updateByExampleSelective(wtcTblParam,
 * wtcTblParamExample); System.out.println(wtcTblParamUpdate+"quotVal value");
 *
 * //Annual Sales Target
 *
 * WTCTblParamExample wtcTblParamExample1 =new WTCTblParamExample(); WTCTblParam
 * wtcTblParam1=new WTCTblParam(); WTCTblParamExample.Criteria
 * wtcParam_criteria1=wtcTblParamExample1.createCriteria();
 * wtcParam_criteria1.andParamTypeEqualTo("WTC_annual_sales_target");
 *
 * String salesTargetVal = request.getParameter("salesTargetVal");
 * System.out.println(salesTargetVal+"salesTargetVal");
 * wtcTblParam1.setParamCode(salesTargetVal==null?"0":salesTargetVal); //String
 * formatVal1=numberFormat(salesTargetVal); String formatVal1=salesTargetVal;
 * wtcTblParam1.setParamDesc("RM"+formatVal1);
 * wtcTblParam1.setParamDescMy("RM"+formatVal1); int
 * wtcTblParamUpdate1=wtcTblParamMapper.updateByExampleSelective(wtcTblParam1,
 * wtcTblParamExample1);
 * System.out.println(wtcTblParamUpdate1+"salesTargetVal value");
 *
 * model.addAttribute("updatemessage","Update data successfully"); String
 * returnURL=Travelproductinfo(request,response,model); return returnURL;
 *
 * }
 *
 *
 * @RequestMapping(value = "/uploadWTCPdfFile", method = RequestMethod.POST)
 * public String uploadWTCPdfFile(@RequestParam("PDF") CommonsMultipartFile
 * file,HttpServletRequest request, HttpServletResponse response, Model model) {
 * String redirctTO=null,fileName=null,filePath=null; String fileNameFormat =
 * new SimpleDateFormat("yyyyMMddHHmm'.pdf'").format(new Date());
 * fileName=request.getParameter("WTCFileName"); if (!file.isEmpty()) { File a =
 * convert(file); a.renameTo(new File("G:\\1702\\" + fileName+fileNameFormat));
 * try ( InputStream in = this.getClass().getClassLoader().getResourceAsStream(
 * "com/etiqa/dsp/sales/process/email/documents.properties")){ Properties prop =
 * new Properties(); prop.load(in);
 *
 * //********************** File writing to another locaiton
 * Starts*********************
 * filePath=prop.getProperty("TLPdfDestinationPath")+fileName+fileNameFormat;
 * InputStream input = new FileInputStream(convert(file)); FileOutputStream fos
 * = new FileOutputStream(filePath,true); int b = 0; while ((b = input.read())
 * != -1) { fos.write(b); }
 * System.out.println("File has been written"+filePath); }catch(Exception e){
 * System.out.println("Could not create file"); } //********************** File
 * writing to another locaiton Ends*********************
 *
 * HttpSession session = request.getSession(); String loginUser
 * =(String)session.getAttribute("user"); if(loginUser == null) { String
 * sessionexpired = "Session Has Been Expired";
 * model.addAttribute("sessionexpired", sessionexpired); return "admin-login"; }
 * TblPdfInfo tblPdfInfo =new TblPdfInfo();
 *
 *
 * Timestamp timestamp = new Timestamp(System.currentTimeMillis());
 *
 * tblPdfInfo.setProductCode("WTC"); tblPdfInfo.setFileName(fileName);
 * tblPdfInfo.setFilePath(filePath); double bytes = convert(file).length();
 * double kilobytes = (bytes / 1024); DecimalFormat df = new
 * DecimalFormat("0.00"); tblPdfInfo.setFileSize(df.format(kilobytes));
 * tblPdfInfo.setCreatedBy(loginUser); tblPdfInfo.setCreatedDate(timestamp);
 *
 * tblPdfInfoMapper.insert(tblPdfInfo);
 *
 * System.out.println("insertion Successfully Done");
 * model.addAttribute("SaveMessage","Data Added Successfully!");
 *
 * redirctTO= Travelproductinfo(request,response,model); } return redirctTO; }
 *
 * -------------------------------------------------------Start- Delete
 * PDS----------------------------------------------------------
 *
 * @RequestMapping(value ="/deleteWTCPdsDone", method=RequestMethod.POST) public
 * String deleteWTCPdsDone(@ModelAttribute(value="tblPdfInfo") TblPdfInfo
 * tblPdfInfo, BindingResult result, HttpServletRequest request,
 * HttpServletResponse response, Model model) {
 *
 * HttpSession session = request.getSession(); String loginUser
 * =(String)session.getAttribute("user"); if(loginUser == null) { String
 * sessionexpired = "Session Has Been Expired";
 * model.addAttribute("sessionexpired", sessionexpired); return "admin-login"; }
 *
 * String id = request.getParameter("id"); System.out.println(id +
 * " id is deleted!"); tblPdfInfo.setId(Integer.valueOf(id));
 * tblPdfInfoMapper.deleteByPrimaryKey(Short.valueOf(request.getParameter("id").
 * trim()));
 *
 * //******************** file deleting Starts***************************
 * //miFilePath String wtcFilePath = request.getParameter("wtcFilePath");
 * if(wtcFilePath!=null || wtcFilePath!=""){ File file = new File(wtcFilePath);
 *
 * if(file.delete()){ System.out.println(file.getName() + " is deleted!");
 * }else{ System.out.println("Delete operation is failed."); } }
 * //******************** file deleting Starts***************************
 * System.out.println("Successfully Deleted");
 * model.addAttribute("deletemessage","Delete data successfully"); String
 * redirctTO= updateWTCproductinfo(request,response,model); return redirctTO;
 *
 * }
 *
 * -------------------------------------------------------End- Delete
 * PDS----------------------------------------------------------
 *
 * @RequestMapping(value = "/WTCDownloadPdfFile", method = RequestMethod.POST)
 * // @RequestMapping("/WTCDownloadPdfFile") public void WTCDownloadPdfFile(
 * HttpServletRequest request, HttpServletResponse response, Model model ) {
 *
 * String wtcFilePath = request.getParameter("wtcFilePathDownload");
 * System.out.println("wtcFilePath."+wtcFilePath); if(wtcFilePath!=null ||
 * wtcFilePath!=""){ File pdfFile = new File(wtcFilePath);
 *
 * response.setContentType("application/pdf");
 * response.addHeader("Content-Disposition", "attachment; filename=" +
 * "Test.pdf"); response.setContentLength((int) pdfFile.length());
 *
 * FileInputStream fileInputStream; try { fileInputStream = new
 * FileInputStream(pdfFile); OutputStream responseOutputStream =
 * response.getOutputStream(); int bytes; while ((bytes =
 * fileInputStream.read()) != -1) { responseOutputStream.write(bytes); } } catch
 * (IOException e) { // TODO Auto-generated catch block e.printStackTrace(); }
 *
 *
 * }
 *
 *
 * } public File convert(MultipartFile file) { File convFile = new
 * File(file.getOriginalFilename()); FileOutputStream fos = null; try {
 * convFile.createNewFile(); fos = new FileOutputStream(convFile);
 * fos.write(file.getBytes()); fos.close(); } catch (IOException e) { // TODO
 * Auto-generated catch block e.printStackTrace(); }
 *
 * return convFile; } public String numberFormat(String val){ Double iVal =null;
 * iVal = Double.parseDouble(val==null?"0":val); DecimalFormat df = new
 * DecimalFormat("0,000"); System.out.println(df.format(iVal)); String
 * formatVal=df.format(iVal); return formatVal; }
 *
 *
 *
 *
 *//**********************************************************
	 * Start by Arbind
	 *********************************************************************/
/*
 *
 *
 *
 * @RequestMapping("/updateWTCproductinfoapproval") public String
 * updateWTCproductinfoapproval(HttpServletRequest request,HttpServletResponse
 * response,Model model) { System.out.println("update Product Info  approval ");
 *
 * try{
 *
 * HttpSession session = request.getSession(); String loginUser
 * =(String)session.getAttribute("user"); String logedUser
 * =(String)session.getAttribute("logedUser"); if(loginUser == null) { String
 * sessionexpired = "Session Has Been Expired";
 * model.addAttribute("sessionexpired", sessionexpired); return "admin-login"; }
 * //Quotaion Validity List<WTCTblParam> listParamWtc_Change = new
 * ArrayList<WTCTblParam>(); List<String> listofparamType = new
 * ArrayList<String>(); listofparamType.add("WTC_quote_validity");
 * listofparamType.add("WTC_annual_sales_target"); WTCTblParamExample
 * wtcTblParamExample =new WTCTblParamExample(); WTCTblParamExample.Criteria
 * wtcParam_criteria=wtcTblParamExample.createCriteria();
 * wtcParam_criteria.andParamTypeIn(listofparamType); List<WTCTblParam>
 * listParamWtc_Original =
 * wtcTblParamMapper.selectByExample(wtcTblParamExample);
 *
 *
 * // Change Blob Storing Starts...... WTCTblParam
 * wtcChangeBlobParamquotationData = new WTCTblParam(); String quotVal =
 * request.getParameter("quotVal"); System.out.println(quotVal+"quotVal");
 * wtcChangeBlobParamquotationData.setId(listParamWtc_Original.get(0).getId());
 * wtcChangeBlobParamquotationData.setParamType("WTC_quote_validity");
 * wtcChangeBlobParamquotationData.setParamCode(quotVal==null?"0":quotVal);
 * wtcChangeBlobParamquotationData.setParamDesc(quotVal+" days");
 * wtcChangeBlobParamquotationData.setParamDescMy(quotVal+" days");
 *
 *
 * WTCTblParam wtcChangeBlobParamSalestargetData = new WTCTblParam(); String
 * salesTargetVal = request.getParameter("salesTargetVal");
 * System.out.println(salesTargetVal+"salesTargetVal");
 * wtcChangeBlobParamSalestargetData.setParamCode(salesTargetVal==null?"0":
 * salesTargetVal); //String formatVal1=numberFormat(salesTargetVal); String
 * formatVal1=salesTargetVal;
 * wtcChangeBlobParamSalestargetData.setId(listParamWtc_Original.get(1).getId())
 * ; wtcChangeBlobParamSalestargetData.setParamType("WTC_annual_sales_target");
 * wtcChangeBlobParamSalestargetData.setParamDesc("RM"+formatVal1);
 * wtcChangeBlobParamSalestargetData.setParamDescMy("RM"+formatVal1);
 *
 * listParamWtc_Change.add(wtcChangeBlobParamquotationData);
 * listParamWtc_Change.add(wtcChangeBlobParamSalestargetData);
 *
 * byte[] changeBlob = convertToBytes(listParamWtc_Change); byte[] originalBlob
 * = convertToBytes(listParamWtc_Original);
 *
 * ApprovalExample approvalExample = new ApprovalExample();
 * com.spring.VO.ApprovalExample.Criteria createCriteria_approvalExample =
 * approvalExample.createCriteria(); createCriteria_approvalExample.
 * andDescriptionEqualTo("WTC/T - CHANGE  PRODUCT  INFORMATION"); List<Approval>
 * listapproval = approvalMapper.selectByExample(approvalExample);
 *
 * System.out.println("ORI Data"+originalBlob);
 * System.out.println("change Data"+changeBlob);
 *
 * BigDecimal approvalProdId = new BigDecimal(0); if(listapproval.size() > 0){
 * approvalProdId = listapproval.get(0).getId(); }
 *
 *
 * int maker = Integer.valueOf(loginUser);
 *
 * //setting all new change value to ApprovalLog POJA and Insert to ApprovalLog
 * table ApprovalLog approvalLog = new ApprovalLog();
 * approvalLog.setOriginalContent(originalBlob);
 * approvalLog.setNewContent(changeBlob);
 * approvalLog.setApprovalId((short)approvalProdId.intValue()); //from Approval
 * table this value[2] based on menu in feature
 * //approvalLog.setMaker((short)maker); // session login value
 * approvalLog.setMaker(Short.parseShort(logedUser));
 * approvalLog.setStatus("1"); approvalLog.setCreateDate(new Date());
 * approvalLog.setUpdateDate(new Date());
 * approvalLog.setAppType(String.valueOf(approvalProdId.intValue())); int status
 * = approvalLogMapper.insert(approvalLog);
 *
 * }catch(Exception e){ e.printStackTrace(); }
 *
 * model.addAttribute("updatemessage","Updated data successfully"); String
 * returnURL=Travelproductinfo(request,response,model); return returnURL;
 *
 * }
 *
 *
 * private byte[] convertToBytes(Object object) throws IOException { try
 * (ByteArrayOutputStream bos = new ByteArrayOutputStream(); ObjectOutput out =
 * new ObjectOutputStream(bos)) { out.writeObject(object); return
 * bos.toByteArray(); } }
 *
 * private Object convertFromBytes(byte[] bytes) throws IOException,
 * ClassNotFoundException { try (ByteArrayInputStream bis = new
 * ByteArrayInputStream(bytes); ObjectInput in = new ObjectInputStream(bis)) {
 * return in.readObject(); } }
 *
 *
 *
 * @RequestMapping("/UpdateTravelProdRatesForApproval") public String
 * UpdateTravelProdRatesForApproval(@ModelAttribute(value="WTCTblParam")
 * WTCTblParam wtcTblParam, HttpServletRequest request,HttpServletResponse
 * response,Model model) { System.out.println("UpdateTravelProdRates funtion ");
 *
 * try{
 *
 * HttpSession session = request.getSession(); String loginUser
 * =(String)session.getAttribute("user"); if(loginUser == null) { String
 * sessionexpired = "Session Has Been Expired";
 * model.addAttribute("sessionexpired", sessionexpired); return "admin-login"; }
 * //Stamp Duty
 *
 * List<String> listWtcTblParam = new ArrayList<String>();
 * listWtcTblParam.add("WTC_StampDuty"); listWtcTblParam.add("WTC_Gst");
 * listWtcTblParam.add("WTC_Discount");
 *
 * //Original Blob Formation..... WTCTblParamExample wtcTblParamExample =new
 * WTCTblParamExample(); Criteria createCriteria_wtcTblParamExample =
 * wtcTblParamExample.createCriteria();
 * createCriteria_wtcTblParamExample.andParamTypeIn(listWtcTblParam);
 * List<WTCTblParam> listwtcparamOriginal =
 * wtcTblParamMapper.selectByExample(wtcTblParamExample);
 *
 *
 * //Change Blob Formation..... List<WTCTblParam> listwtcparamChange = new
 * ArrayList<WTCTblParam>(); WTCTblParam wtcTblParamChangestampduty =new
 * WTCTblParam(); String wtcsduty = request.getParameter("stampduty");
 * System.out.println(wtcsduty+"wtcsduty");
 * wtcTblParamChangestampduty.setId(listwtcparamOriginal.get(0).getId());
 * wtcTblParamChangestampduty.setParamCode(wtcsduty==null?"0":wtcsduty); String
 * formatsduty=wtcsduty; wtcTblParamChangestampduty.setParamDesc(formatsduty);
 * wtcTblParamChangestampduty.setParamDescMy(formatsduty);
 *
 *
 * WTCTblParam wtcTblParamChangegst =new WTCTblParam(); String wtcgst =
 * request.getParameter("gst"); System.out.println(wtcgst+"wtcgst");
 * wtcTblParamChangegst.setId(listwtcparamOriginal.get(1).getId());
 * wtcTblParamChangegst.setParamCode(wtcgst==null?"0":wtcgst); String
 * formatgst=wtcgst; wtcTblParamChangegst.setParamDesc(formatgst);
 * wtcTblParamChangegst.setParamDescMy(formatgst);
 *
 *
 * WTCTblParam wtcTblParamChangediscount =new WTCTblParam(); String discount =
 * request.getParameter("discount"); System.out.println(discount+"discount");
 * wtcTblParamChangediscount.setParamCode(discount==null?"0":discount); String
 * formatVal=discount;
 * wtcTblParamChangediscount.setId(listwtcparamOriginal.get(2).getId());
 * wtcTblParamChangediscount.setParamDesc(formatVal);
 * wtcTblParamChangediscount.setParamDescMy(formatVal);
 *
 * listwtcparamChange.add(wtcTblParamChangestampduty);
 * listwtcparamChange.add(wtcTblParamChangegst);
 * listwtcparamChange.add(wtcTblParamChangediscount);
 *
 *
 * byte[] changeBlob = convertToBytes(listwtcparamChange); byte[] originalBlob =
 * convertToBytes(listwtcparamOriginal);
 *
 * ApprovalExample approvalExample = new ApprovalExample();
 * com.spring.VO.ApprovalExample.Criteria createCriteria_approvalExample =
 * approvalExample.createCriteria(); createCriteria_approvalExample.
 * andDescriptionEqualTo("WTC/T - CHANGE PRODUCT RATE"); List<Approval>
 * listapproval = approvalMapper.selectByExample(approvalExample);
 *
 * System.out.println("ORI Data"+originalBlob);
 * System.out.println("change Data"+changeBlob);
 *
 * BigDecimal approvalProdId = new BigDecimal(0); if(listapproval.size() > 0){
 * approvalProdId = listapproval.get(0).getId(); }
 *
 * int maker = Integer.valueOf(loginUser);
 *
 * //setting all new change value to ApprovalLog POJA and Insert to ApprovalLog
 * table ApprovalLog approvalLog = new ApprovalLog();
 * approvalLog.setOriginalContent(originalBlob);
 * approvalLog.setNewContent(changeBlob);
 * approvalLog.setApprovalId((short)approvalProdId.intValue()); //from Approval
 * table this value[12] based on menu in feature
 * approvalLog.setMaker((short)maker); // session login value
 * //approvalLog.setChecker((short)1); //while insert record no need checker
 * value approvalLog.setStatus("1"); approvalLog.setCreateDate(new Date());
 * approvalLog.setUpdateDate(new Date());
 * approvalLog.setAppType(String.valueOf(approvalProdId.intValue())); int status
 * = approvalLogMapper.insert(approvalLog);
 *
 * if(status == 1){
 *
 *
 * model.addAttribute("updatemessage","Updated data successfully");
 *
 *
 * }
 *
 * //Direct Discount (Online)
 *
 * }catch(Exception e){ e.printStackTrace(); } String
 * returnURL=TravelProductRates(request,response,model); return returnURL;
 *
 * }
 *
 *
 *
 *
 * @RequestMapping(value ="/UpdateWTCDProdRatesForApproval",
 * method=RequestMethod.POST) public String
 * UpdateWTCDProdRatesForApproval(HttpServletRequest request,
 * HttpServletResponse response, Model model) {
 *
 * try{ HttpSession session = request.getSession(); String loginUser
 * =(String)session.getAttribute("user"); String logedUser
 * =(String)session.getAttribute("logedUser"); if(loginUser == null) { String
 * sessionexpired = "Session Has Been Expired";
 * model.addAttribute("sessionexpired", sessionexpired); return "admin-login"; }
 *
 * String premid=null; String premiumval=null; if (request.getParameter("pid")
 * != null) { premid= request.getParameter("pid"); } else {
 * premid=String.valueOf((Integer) request.getAttribute("pid")); } if
 * (request.getParameter("premiumval") != null) { premiumval=
 * request.getParameter("premiumval"); } else {
 * premiumval=String.valueOf((Integer) request.getAttribute("premiumval")); }
 * System.out.println("id from selection -"+ premid);
 *
 * //Original BLOB Information..................... WTCTblDometicExample
 * wtcTblDometicExample = new WTCTblDometicExample();
 * com.spring.VO.WTCTblDometicExample.Criteria
 * wtcTblDometicExample_createCriteria = wtcTblDometicExample.createCriteria();
 * wtcTblDometicExample_createCriteria.andIdEqualTo(Short.valueOf(premid));
 * List<WTCTblDometic> listWtcDomestic =
 * wtcTblDometicMapper.selectByExample(wtcTblDometicExample);
 *
 * List<WTCProdMngPojo> wtcDomesticTblListOrig = new
 * ArrayList<WTCProdMngPojo>(); WTCProdMngPojo wtcTblDometicOrig=new
 * WTCProdMngPojo();
 * wtcTblDometicOrig.setWtcRecIdDomestic(String.valueOf(listWtcDomestic.get(0).
 * getId()));
 * wtcTblDometicOrig.setWtcpremiumvalDomestic(String.valueOf(listWtcDomestic.get
 * (0).getPremiumValue())); wtcTblDometicOrig.setWtcProdType("D");
 * wtcTblDometicOrig.setWtcdaysrangekeycodeDomestic(listWtcDomestic.get(0).
 * getDaysRangeKeycode());
 * wtcTblDometicOrig.setWtcdestinationcodeareaDomestic(listWtcDomestic.get(0).
 * getDestinationCodeArea());
 * wtcTblDometicOrig.setWtcpackagecodeDomestic(listWtcDomestic.get(0).
 * getPackageCode());
 * wtcTblDometicOrig.setWtcplancodeDomestic(listWtcDomestic.get(0).getPlanCode()
 * ); wtcTblDometicOrig.setWtctravelwithtypeidDomestic(listWtcDomestic.get(0).
 * getTravelwithTypeId()); wtcDomesticTblListOrig.add(wtcTblDometicOrig);
 *
 *
 * // Formation of Change BLOB Formation...... List<WTCProdMngPojo>
 * wtcDomesticTblListChange = new ArrayList<WTCProdMngPojo>(); WTCProdMngPojo
 * wtcTblDometic=new WTCProdMngPojo();
 * wtcTblDometic.setWtcRecIdDomestic(premid); Double premium =0.00;
 * if(premiumval!=null){ premium = Double.valueOf(premiumval); }
 *
 * wtcTblDometic.setWtcpremiumvalDomestic(String.valueOf(premium));
 * wtcTblDometic.setWtcProdType("D");
 * wtcTblDometic.setWtcdaysrangekeycodeDomestic(listWtcDomestic.get(0).
 * getDaysRangeKeycode());
 * wtcTblDometic.setWtcdestinationcodeareaDomestic(listWtcDomestic.get(0).
 * getDestinationCodeArea());
 * wtcTblDometic.setWtcpackagecodeDomestic(listWtcDomestic.get(0).getPackageCode
 * ());
 * wtcTblDometic.setWtcplancodeDomestic(listWtcDomestic.get(0).getPlanCode());
 * wtcTblDometic.setWtctravelwithtypeidDomestic(listWtcDomestic.get(0).
 * getTravelwithTypeId()); wtcDomesticTblListChange.add(wtcTblDometic);
 *
 * byte[] changeBlob = convertToBytes(wtcDomesticTblListChange); byte[]
 * originalBlob = convertToBytes(wtcDomesticTblListOrig);
 *
 * ApprovalExample approvalExample = new ApprovalExample();
 * com.spring.VO.ApprovalExample.Criteria createCriteria_approvalExample =
 * approvalExample.createCriteria(); createCriteria_approvalExample.
 * andDescriptionEqualTo("WTC/T - CHANGE PRODUCT RATE"); List<Approval>
 * listapproval = approvalMapper.selectByExample(approvalExample);
 *
 * System.out.println("ORI Data"+originalBlob);
 * System.out.println("change Data"+changeBlob);
 *
 * BigDecimal approvalProdId = new BigDecimal(0); if(listapproval.size() > 0){
 * approvalProdId = listapproval.get(0).getId(); }
 *
 * int maker = Integer.valueOf(loginUser);
 *
 * //setting all new change value to ApprovalLog POJA and Insert to ApprovalLog
 * table ApprovalLog approvalLog = new ApprovalLog();
 * approvalLog.setOriginalContent(originalBlob);
 * approvalLog.setNewContent(changeBlob);
 * approvalLog.setApprovalId((short)approvalProdId.intValue()); //from Approval
 * table this value[12] based on menu in feature
 * //approvalLog.setMaker((short)maker); // session login value
 * approvalLog.setMaker(Short.parseShort(logedUser));
 * approvalLog.setStatus("1"); approvalLog.setCreateDate(new Date());
 * approvalLog.setUpdateDate(new Date()); approvalLog.setNote("D");
 * approvalLog.setAppType(String.valueOf(approvalProdId.intValue())); int status
 * = approvalLogMapper.insert(approvalLog);
 *
 * if(status == 1){
 *
 * model.addAttribute("wtcUpdatedMessage","WTC Premium Updated Successfully!");
 *
 *
 * }
 *
 * }catch(Exception e){ e.printStackTrace(); } // //
 * System.out.println("id from Domestic premium -"+ premium + "-"+premiumval);
 * // int wtcTblDometicUpdate=wtcTblDometicMapper.updateByPrimaryKeySelective(
 * wtcTblDometic); // System.out.println("id from Domestic rates update -"+
 * wtcTblDometicUpdate);
 *
 * String returnURL=TravelProductRates(request,response,model); return
 * returnURL;
 *
 * }
 *
 *
 *
 * @RequestMapping(value ="/UpdateWTCIntProdRatesForApproval",
 * method=RequestMethod.POST) public String
 * UpdateWTCIntProdRatesForApproval(HttpServletRequest request,
 * HttpServletResponse response, Model model) {
 *
 * try{ HttpSession session = request.getSession(); String loginUser
 * =(String)session.getAttribute("user"); if(loginUser == null) { String
 * sessionexpired = "Session Has Been Expired";
 * model.addAttribute("sessionexpired", sessionexpired); return "admin-login"; }
 *
 * String premid=null; String premiumval=null; if
 * (request.getParameter("pintid") != null) { premid=
 * request.getParameter("pintid"); } else { premid=String.valueOf((Integer)
 * request.getAttribute("pintid")); } if (request.getParameter("premiumintval")
 * != null) { premiumval= request.getParameter("premiumintval"); } else {
 * premiumval=String.valueOf((Integer) request.getAttribute("premiumintval")); }
 * System.out.println("id from selection -"+ premid);
 *
 *
 * // Original BLOB Formation...... WTCTblIntwithdaysExample
 * wtcTblIntwithdaysExampleOrg = new WTCTblIntwithdaysExample();
 * com.spring.VO.WTCTblIntwithdaysExample.Criteria
 * wtcTblIntwithdaysExampleOrg_createCriteria =
 * wtcTblIntwithdaysExampleOrg.createCriteria();
 * wtcTblIntwithdaysExampleOrg_createCriteria.andIdEqualTo(Short.valueOf(premid)
 * ); List<WTCTblIntwithdays> wtcTblWithDaysListOriginal =
 * wtcTblIntwithdaysMapper.selectByExample(wtcTblIntwithdaysExampleOrg);
 *
 *
 * // Formation of Change BLOB Formation...... List<WTCProdMngPojo>
 * wtcInternationalTblListChange = new ArrayList<WTCProdMngPojo>();
 * WTCProdMngPojo wtcTblInt=new WTCProdMngPojo();
 * wtcTblInt.setWtcRecIdInternational(premid); Double premium =0.00;
 * if(premiumval!=null){ premium = Double.valueOf(premiumval); }
 *
 * wtcTblInt.setWtcpremiumvalInternational(String.valueOf(premium));
 * wtcTblInt.setWtcProdType("INT");
 * wtcTblInt.setWtcdaysrangekeycodeInternational(wtcTblWithDaysListOriginal.get(
 * 0).getDaysRangeKeycode());
 * wtcTblInt.setWtcdestinationcodeareaInternational(wtcTblWithDaysListOriginal.
 * get(0).getDestinationCodeArea());
 * wtcTblInt.setWtcpackagecodeInternational(wtcTblWithDaysListOriginal.get(0).
 * getPackageCode());
 * wtcTblInt.setWtcplancodeInternational(wtcTblWithDaysListOriginal.get(0).
 * getPlanCode());
 * wtcTblInt.setWtctravelwithtypeidInternational(wtcTblWithDaysListOriginal.get(
 * 0).getTravelwithTypeId()); wtcInternationalTblListChange.add(wtcTblInt);
 *
 *
 * System.out.println("   -----------------       "+premid);
 * System.out.println("   -----------------       "+wtcTblWithDaysListOriginal.
 * get(0).getDaysRangeKeycode());
 * System.out.println("   -----------------       "+wtcTblWithDaysListOriginal.
 * get(0).getDestinationCodeArea());
 * System.out.println("   -----------------       "+wtcTblWithDaysListOriginal.
 * get(0).getPackageCode());
 * System.out.println("   -----------------       "+wtcTblWithDaysListOriginal.
 * get(0).getPlanCode());
 * System.out.println("   -----------------       "+wtcTblWithDaysListOriginal.
 * get(0).getTravelwithTypeId());
 * System.out.println("   -----------------       "+premium); // //
 * System.out.println(
 * wtcTblWithDaysListOriginal+"  ::::::::::::::    chnage  :::::::::::       "
 * +wtcTblWithDaysListChange);
 *
 * byte[] changeBlob = convertToBytes(wtcInternationalTblListChange); byte[]
 * originalBlob = convertToBytes(wtcTblWithDaysListOriginal);
 *
 * System.out.println("ORI Data"+originalBlob);
 * System.out.println("change Data"+changeBlob);
 *
 * //Fetch approval Id.class.............. ApprovalExample approvalExample = new
 * ApprovalExample(); com.spring.VO.ApprovalExample.Criteria
 * createCriteria_approvalExample = approvalExample.createCriteria();
 * createCriteria_approvalExample.
 * andDescriptionEqualTo("WTC/T - CHANGE PRODUCT RATE"); List<Approval>
 * listapproval = approvalMapper.selectByExample(approvalExample);
 *
 * BigDecimal approvalProdId = new BigDecimal(0); if(listapproval.size() > 0){
 * approvalProdId = listapproval.get(0).getId(); }
 *
 * int maker = Integer.valueOf(loginUser);
 *
 * //setting all new change value to ApprovalLog POJA and Insert to ApprovalLog
 * table ApprovalLog approvalLog = new ApprovalLog();
 * approvalLog.setOriginalContent(originalBlob);
 * approvalLog.setNewContent(changeBlob);
 * approvalLog.setApprovalId((short)approvalProdId.intValue()); //from Approval
 * table this value[12] based on menu in feature
 * approvalLog.setMaker((short)maker); // session login value
 * //approvalLog.setChecker((short)1); //while insert record no need checker
 * value approvalLog.setStatus("1"); approvalLog.setCreateDate(new Date());
 * approvalLog.setUpdateDate(new Date()); approvalLog.setNote("INT");
 * approvalLog.setAppType(String.valueOf(approvalProdId.intValue())); int status
 * = approvalLogMapper.insert(approvalLog);
 *
 * if(status == 1){
 * model.addAttribute("updatemessage","Update data successfully");
 *
 *
 * }
 *
 * }catch(Exception e){
 *
 * e.printStackTrace(); }
 *
 * //int
 * wtcTblDometicUpdate=wtcTblIntwithdaysMapper.updateByPrimaryKeySelective(
 * wtcTblIntwithdays);
 * //model.addAttribute("wtcUpdatedMessage","WTC Premium Updated Successfully!"
 * );
 *
 * String returnURL=TravelProductRates(request,response,model); return
 * returnURL;
 *
 * }
 *
 *
 *
 *//**********************************************************
	 * End by Arbind
	 *********************************************************************//*
																			 *
																			 *
																			 *
																			 *
																			 *
																			 *
																			 *
																			 *
																			 *
																			 *
																			 *
																			 * -----------------------------------------
																			 * -------------END- Product
																			 * Info-------------------------------------
																			 * --------------------
																			 *
																			 *
																			 * }
																			 */