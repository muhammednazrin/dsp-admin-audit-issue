package com.spring.admin;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.etiqa.common.DB.ConnectionFactory;
import com.spring.VO.roadtaxVO.RoadTaxPos;
import com.spring.VO.roadtaxVO.RoadTaxPosExample;
import com.spring.mapper.roadtax.RoadTaxPosMapper;
import com.spring.utils.ServiceValidationUtils;

@Controller
public class RoadTaxPosController {

	RoadTaxPosMapper roadTaxPosMapper;

	@Autowired
	public RoadTaxPosController(RoadTaxPosMapper roadTaxPosMapper

	) {

		this.roadTaxPosMapper = roadTaxPosMapper;

	}

	@RequestMapping("/roadTaxPostCode")
	public String roadTaxPostCode(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadtax-admin-login";
			return "admin-login";
		}

		RoadTaxPos roadTaxPos = new RoadTaxPos();

		List<RoadTaxPos> roadTaxPosList = new ArrayList<RoadTaxPos>();
		RoadTaxPosExample roadTaxPosExample = new RoadTaxPosExample();
		roadTaxPosExample.setOrderByClause("rtpos.POSTOFFICE_ID desc");
		roadTaxPosList = roadTaxPosMapper.selectByExample(roadTaxPosExample);

		model.addAttribute("roadTaxPosList", roadTaxPosList);
		model.addAttribute("roadTaxPos", roadTaxPos);
		return "roadtax/pages/roadTaxPos";
	}

	@RequestMapping("/addRoadTaxPos")
	public String addBranchCodeDetails(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadtax-admin-login";
			return "admin-login";
		}
		RoadTaxPos roadTaxPos = new RoadTaxPos();
		model.addAttribute("roadTaxPos", roadTaxPos);
		return "roadtax/pages/roadtax-pos-add";

	}

	@RequestMapping("/updateRoadTaxPos/{postofficeId}")
	public String updateRoadTaxPos(HttpServletRequest request, Model model, @PathVariable String postofficeId) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadtax-admin-login";
			return "admin-login";
		}

		List<RoadTaxPos> roadTaxPosList = new ArrayList<RoadTaxPos>();
		RoadTaxPosExample roadTaxPosExample = new RoadTaxPosExample();
		RoadTaxPosExample.Criteria roadTax_Criteria = roadTaxPosExample.createCriteria();
		roadTax_Criteria.andPostofficeIdEqualTo(new BigDecimal(postofficeId));
		roadTaxPosList = roadTaxPosMapper.selectByExample(roadTaxPosExample);

		RoadTaxPos roadTaxPos = roadTaxPosList.get(0);

		model.addAttribute("roadTaxPosList", roadTaxPosList);
		model.addAttribute("roadTaxPos", roadTaxPos);
		return "roadtax/pages/roadtax-pos-update";

	}

	@RequestMapping("/searchRoadTaxPosDetails")
	public String searchRoadTaxPosDetails(@ModelAttribute(value = "roadTaxPos") RoadTaxPos roadTaxPos,
			HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadtax-admin-login";
			return "admin-login";
		}
		List<String> errorMessages = new ArrayList<String>();

		try {

			if (errorMessages.size() > 0) {
				model.addAttribute("errorMessages", errorMessages);
				return "roadtax/pages/roadTaxPos";
			}

			List<RoadTaxPos> roadTaxPosList = new ArrayList<RoadTaxPos>();
			RoadTaxPosExample roadTaxPosExample = new RoadTaxPosExample();
			RoadTaxPosExample.Criteria roadTaxPos_criteria = roadTaxPosExample.createCriteria();

			if (!ServiceValidationUtils.isEmptyStringTrim(roadTaxPos.getStateCoverage())) {
				roadTaxPos_criteria.andStateCoverageEqualTo(roadTaxPos.getStateCoverage());
			}

			if (!ServiceValidationUtils.isEmptyStringTrim(roadTaxPos.getPostcodeArea())) {
				roadTaxPos_criteria.andPostcodeAreaEqualTo(roadTaxPos.getPostcodeArea());
			}

			roadTaxPosExample.setOrderByClause("rtpos.POSTOFFICE_ID desc");
			roadTaxPosList = roadTaxPosMapper.selectByExample(roadTaxPosExample);

			model.addAttribute("roadTaxPosList", roadTaxPosList);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "roadtax/pages/roadTaxPos";
	}

	@RequestMapping(value = "/addroadTaxPosDone", method = RequestMethod.POST)
	public String addroadTaxPosDone(@ModelAttribute(value = "roadTaxPos") RoadTaxPos roadTaxPos,
			HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");

		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadtax-admin-login";
			return "admin-login";
		}

		int errorFlag = 0;
		if (ServiceValidationUtils.isEmptyStringTrim(roadTaxPos.getStateCoverage())) {
			errorFlag = 1;
		}

		if (ServiceValidationUtils.isEmptyStringTrim(roadTaxPos.getPostcodeArea())) {
			errorFlag = 1;
		}

		if (ServiceValidationUtils.isEmptyStringTrim(roadTaxPos.getPostofficeName())) {
			errorFlag = 1;
		}

		if (ServiceValidationUtils.isEmptyStringTrim(roadTaxPos.getPostcodeFrom())) {
			errorFlag = 1;
		}

		if (ServiceValidationUtils.isEmptyStringTrim(roadTaxPos.getPostcodeTo())) {
			errorFlag = 1;
		}

		if (ServiceValidationUtils.isEmptyBigDecimal(roadTaxPos.getPrintingCharge())) {
			errorFlag = 1;
		}

		if (ServiceValidationUtils.isEmptyBigDecimal(roadTaxPos.getDeliveryCharge())) {
			errorFlag = 1;
		}

		if (ServiceValidationUtils.isEmptyBigDecimal(roadTaxPos.getProcessingCharge())) {
			errorFlag = 1;
		}

		if (ServiceValidationUtils.isEmptyStringTrim(roadTaxPos.getGst())) {
			errorFlag = 1;
		}

		if (ServiceValidationUtils.isEmptyStringTrim(roadTaxPos.getDeliveryService())) {
			errorFlag = 1;
		}

		if (!roadTaxPos.getPostcodeFrom().isEmpty() && !roadTaxPos.getPostcodeTo().isEmpty()
				&& roadTaxPos.getPostcodeFrom().equals(roadTaxPos.getPostcodeTo())) {
			errorFlag = 2;
		}

		if (errorFlag == 1) {
			model.addAttribute("errorMessages", "Please Enter Required  Fields");

			return "roadtax/pages/roadtax-pos-add";

		}

		if (errorFlag == 2) {
			model.addAttribute("errorMessages", "Please Enter Different From and To Post Code Coverage");

			return "roadtax/pages/roadtax-pos-add";

		}

		List<RoadTaxPos> roadTaxPosList = new ArrayList<RoadTaxPos>();
		RoadTaxPosExample roadTaxPosExampleFromRange = new RoadTaxPosExample();
		RoadTaxPosExample roadTaxPosExampleToRange = new RoadTaxPosExample();
		RoadTaxPosExample roadTaxPosExampleSelect = new RoadTaxPosExample();
		RoadTaxPosExample roadTaxPosExampleRange = new RoadTaxPosExample();

		RoadTaxPosExample.Criteria roadTaxFromCriteria = roadTaxPosExampleFromRange.createCriteria();
		RoadTaxPosExample.Criteria roadTaxToCriteria = roadTaxPosExampleToRange.createCriteria();
		RoadTaxPosExample.Criteria roadTaxRangeCriteria = roadTaxPosExampleRange.createCriteria();

		roadTaxFromCriteria.andPostcodeFromRange(roadTaxPos.getPostcodeFrom());
		roadTaxFromCriteria.andDeliveryServiceEqualTo(roadTaxPos.getDeliveryService());
		long countFrom = roadTaxPosMapper.countByExample(roadTaxPosExampleFromRange);

		roadTaxToCriteria.andPostcodeToRange(roadTaxPos.getPostcodeTo());
		roadTaxToCriteria.andDeliveryServiceEqualTo(roadTaxPos.getDeliveryService());
		long countTo = roadTaxPosMapper.countByExample(roadTaxPosExampleToRange);

		roadTaxRangeCriteria.andPostcodeFromEqualTo(roadTaxPos.getPostcodeFrom());
		roadTaxRangeCriteria.andPostcodeToEqualTo(roadTaxPos.getPostcodeTo());
		roadTaxRangeCriteria.andDeliveryServiceEqualTo(roadTaxPos.getDeliveryService());

		long countRange = roadTaxPosMapper.countByExample(roadTaxPosExampleRange);

		System.out.println("Count From---->" + countFrom);
		System.out.println("Count To---->" + countTo);
		System.out.println("Count Range---->" + countRange);

		String area = "";
		BigDecimal a = new BigDecimal(roadTaxPos.getPostcodeTo());
		BigDecimal b = new BigDecimal(1);
		BigDecimal toPostCode = a.add(b);
		System.out.println("toPostCode--------->" + toPostCode);

		if (roadTaxPos.getStateCoverage().equals("Selangor")) {

			area = "SGR";
		}

		if (roadTaxPos.getStateCoverage().equals("Kuala Lumpur")) {

			area = "KUL";
		}

		System.out.println("Area------------>" + area);

		if (countFrom == 1 || countTo == 1 || countRange == 1) {
			model.addAttribute("errorMessages", "Post Code Coverage Range Already Exists.");
			return "roadtax/pages/roadtax-pos-add";
		}

		try {

			roadTaxPos.setStatus("ACTIVE");
			roadTaxPosMapper.insert(roadTaxPos);
			roadTaxPosExampleSelect.setOrderByClause("rtpos.POSTOFFICE_ID desc");
			roadTaxPosList = roadTaxPosMapper.selectByExample(roadTaxPosExampleSelect);

			Connection con = ConnectionFactory.getConnection();

			CallableStatement cstmt = con.prepareCall("{CALL DSP_SP_POSTCODES_INSERT(?,?,?,?,?,?)}");

			cstmt.setString(1, roadTaxPos.getPostcodeFrom());
			cstmt.setString(2, toPostCode.toString());
			cstmt.setString(3, area);
			cstmt.setString(4, roadTaxPos.getStateCoverage());
			cstmt.setString(5, roadTaxPos.getPostcodeTo());
			cstmt.registerOutParameter(6, Types.VARCHAR);
			cstmt.execute();
			String StatusOfInsertion = cstmt.getString(6);

			System.out.println(StatusOfInsertion + " ID inserterd");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		model.addAttribute("roadTaxPosList", roadTaxPosList);
		model.addAttribute("roadTaxPos", new RoadTaxPos());
		return "roadtax/pages/roadTaxPos";
		// e.printStackTrace();
	}

	@RequestMapping(value = "/updateroadTaxPosDone", method = RequestMethod.POST)
	public String updateroadTaxPosDone(@ModelAttribute(value = "roadTaxPos") RoadTaxPos roadTaxPos,
			HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");

		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadtax-admin-login";
			return "admin-login";
		}

		long countFrom = 0, countTo = 0;

		if (!roadTaxPos.getPostcodeFrom().isEmpty() && !roadTaxPos.getPostcodeTo().isEmpty()
				&& roadTaxPos.getPostcodeFrom().equals(roadTaxPos.getPostcodeTo())) {
			model.addAttribute("errorMessages", "Please Enter Different From and To Post Code Coverage");

			return "roadtax/pages/roadtax-pos-update";
		}

		System.out.println("PostCodeFrom----------->" + roadTaxPos.getPostcodeFrom());
		System.out.println("PostFrom----------->" + roadTaxPos.getPostFrom());

		/*
		 * if(!roadTaxPos.getPostcodeFrom().equals(roadTaxPos.getPostFrom())) {
		 * RoadTaxPosExample roadTaxPosExampleFromRange =new RoadTaxPosExample();
		 * RoadTaxPosExample.Criteria roadTaxFromCriteria=
		 * roadTaxPosExampleFromRange.createCriteria();
		 *
		 * roadTaxFromCriteria.andPostcodeFromRange(roadTaxPos.getPostcodeFrom());
		 * roadTaxFromCriteria.andDeliveryServiceEqualTo(roadTaxPos.getDeliveryService()
		 * ); countFrom=roadTaxPosMapper.countByExample(roadTaxPosExampleFromRange);
		 *
		 * }
		 *
		 * if(!roadTaxPos.getPostcodeTo().equals(roadTaxPos.getPostTo())) {
		 *
		 * RoadTaxPosExample roadTaxPosExampleToRange =new RoadTaxPosExample();
		 * RoadTaxPosExample.Criteria roadTaxToCriteria=
		 * roadTaxPosExampleToRange.createCriteria();
		 *
		 * roadTaxToCriteria.andPostcodeToRange(roadTaxPos.getPostcodeTo());
		 * roadTaxToCriteria.andDeliveryServiceEqualTo(roadTaxPos.getDeliveryService());
		 * countTo=roadTaxPosMapper.countByExample(roadTaxPosExampleToRange);
		 *
		 * }
		 *
		 *
		 * System.out.println("Count From---->"+countFrom);
		 * System.out.println("Count To---->"+countTo);
		 *
		 * if (countFrom == 1 || countTo==1){
		 * model.addAttribute("errorMessages","Post Code Coverage Range Already Exists."
		 * );
		 *
		 * List<RoadTaxPos> roadTaxPosList = new ArrayList<RoadTaxPos>();
		 *
		 *
		 * RoadTaxPosExample roadTaxPosExample =new RoadTaxPosExample();
		 * System.out.println("psotID----------->"+roadTaxPos.getPostofficeId());
		 *
		 * RoadTaxPosExample.Criteria roadTax_Criteria=
		 * roadTaxPosExample.createCriteria();
		 * roadTax_Criteria.andPostofficeIdEqualTo(roadTaxPos.getPostofficeId());
		 * roadTaxPosList= roadTaxPosMapper.selectByExample(roadTaxPosExample);
		 *
		 * roadTaxPos= roadTaxPosList.get(0);
		 *
		 * model.addAttribute("roadTaxPosList", roadTaxPosList);
		 * model.addAttribute("roadTaxPos",roadTaxPos);
		 * System.out.println("Test----------->"+roadTaxPosList); return
		 * "roadtax/pages/roadtax-pos-update"; }
		 */
		model.addAttribute("roadTaxPos", roadTaxPos);

		List<RoadTaxPos> roadTaxPosList = new ArrayList<RoadTaxPos>();

		try {

			RoadTaxPosExample roadTaxPosExample = new RoadTaxPosExample();
			RoadTaxPosExample roadTaxPosExampleSelect = new RoadTaxPosExample();
			RoadTaxPosExample.Criteria roadTaxPos_criteria = roadTaxPosExample.createCriteria();
			roadTaxPos_criteria.andPostofficeIdEqualTo(roadTaxPos.getPostofficeId());
			roadTaxPosMapper.updateByExampleSelective(roadTaxPos, roadTaxPosExample);

			roadTaxPosExampleSelect.setOrderByClause("rtpos.POSTOFFICE_ID desc");
			roadTaxPosList = roadTaxPosMapper.selectByExample(roadTaxPosExampleSelect);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		model.addAttribute("roadTaxPosList", roadTaxPosList);
		model.addAttribute("roadTaxPos", new RoadTaxPos());
		return "roadtax/pages/roadTaxPos";
		// e.printStackTrace();
	}

	@RequestMapping(value = "/deleteRoadTaxPos", method = RequestMethod.POST)
	public String deleteRoadTaxPos(@ModelAttribute(value = "roadTaxPos") RoadTaxPos roadTaxPos,
			HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");

		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			// return "roadtax/pages/roadtax-admin-login";
			return "admin-login";
		}

		model.addAttribute("roadTaxPos", roadTaxPos);

		List<RoadTaxPos> roadTaxPosList = new ArrayList<RoadTaxPos>();

		try {

			System.out.println("ID-->" + request.getParameter("postid"));

			BigDecimal b = new BigDecimal(request.getParameter("postid"));

			RoadTaxPosExample roadTaxPosExample = new RoadTaxPosExample();
			RoadTaxPosExample roadTaxPosExampleSelect = new RoadTaxPosExample();
			RoadTaxPosExample.Criteria roadTaxPos_criteria = roadTaxPosExample.createCriteria();
			roadTaxPos_criteria.andPostofficeIdEqualTo(b);
			roadTaxPosMapper.deleteByExample(roadTaxPosExample);

			roadTaxPosExampleSelect.setOrderByClause("rtpos.POSTOFFICE_ID desc");
			roadTaxPosList = roadTaxPosMapper.selectByExample(roadTaxPosExampleSelect);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		model.addAttribute("roadTaxPosList", roadTaxPosList);
		model.addAttribute("roadTaxPos", new RoadTaxPos());
		return "roadtax/pages/roadTaxPos";
		// e.printStackTrace();
	}
}
