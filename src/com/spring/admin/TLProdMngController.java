package com.spring.admin;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.spring.VO.AgentProfile;
import com.spring.VO.Approval;
import com.spring.VO.ApprovalExample;
import com.spring.VO.ApprovalLog;
import com.spring.VO.ApprovalLogExample;
import com.spring.VO.DspTblRangeRules;
import com.spring.VO.DspTblRangeRulesExample;
import com.spring.VO.DspTlTblParam;
import com.spring.VO.DspTlTblParamExample;
import com.spring.VO.MIProductInfoApproval;
import com.spring.VO.TLApprovalProductRate;
import com.spring.VO.TblPdfInfo;
import com.spring.VO.TblPdfInfoExample;
import com.spring.mapper.AdminParamMapper;
import com.spring.mapper.AgentDocumentMapper;
import com.spring.mapper.AgentLinkMapper;
import com.spring.mapper.AgentProdMapMapper;
import com.spring.mapper.AgentProfileMapper;
import com.spring.mapper.ApprovalLogMapper;
import com.spring.mapper.ApprovalMapper;
import com.spring.mapper.CommonQQMapper;
import com.spring.mapper.DspTblRangeRulesMapper;
import com.spring.mapper.DspTlTblParamMapper;
import com.spring.mapper.ProductsMapper;
import com.spring.mapper.TblPdfInfoMapper;

@Controller
public class TLProdMngController {
	DspTblRangeRulesMapper dspTblRangeRulesMapper;
	DspTlTblParamMapper dspTlTblParamMapper;
	TblPdfInfoMapper tblPdfInfoMapper;
	ApprovalLogMapper approvalLogMapper;
	ApprovalMapper approvalMapper;
	AgentProfileMapper agentProfileMapper;
	ProductsMapper productsMapper;
	AgentProdMapMapper agentProdMapMapper;
	CommonQQMapper commonQQMapper;
	AdminParamMapper adminParamMapper;
	AgentDocumentMapper agentDocumentMapper;
	AgentLinkMapper agentLinkMapper;

	@Autowired
	public TLProdMngController(DspTblRangeRulesMapper dspTblRangeRulesMapper, DspTlTblParamMapper dspTlTblParamMapper,
			TblPdfInfoMapper tblPdfInfoMapper, ApprovalLogMapper approvalLogMapper, ApprovalMapper approvalMapper) {
		this.dspTblRangeRulesMapper = dspTblRangeRulesMapper;
		this.dspTlTblParamMapper = dspTlTblParamMapper;
		this.tblPdfInfoMapper = tblPdfInfoMapper;
		this.approvalLogMapper = approvalLogMapper;
		this.approvalMapper = approvalMapper;

	}

	@RequestMapping("/tlprodmngproductpayment")
	public String adminLogin(HttpServletRequest request, Model model) {

		/*
		 * HttpSession session = request.getSession(); session.removeAttribute("user");
		 * session.removeAttribute("FullName"); session.invalidate();
		 */
		return "/products/termLife/admin-business-product-management-term-payment";

	}

	@RequestMapping("/tlprodmngproductrates")
	public String TlProductRates(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("SalesLeadsReport funtion ");

		HttpSession session = request.getSession();

		// Minimum Benefit Amount

		List<DspTlTblParam> minBenAmountList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria = dspTlTblParamExample.createCriteria();
		dspTlParam_criteria.andParamTypeEqualTo("pre_ins_cov_start");
		minBenAmountList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample);

		// Maximum Benefit Amount

		List<DspTlTblParam> maxBenAmountList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample1 = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria1 = dspTlTblParamExample1.createCriteria();
		dspTlParam_criteria1.andParamTypeEqualTo("pre_ins_cov_end");
		maxBenAmountList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample1);

		// Total Sum Assured at Risk (TSAR)

		List<DspTlTblParam> tsarValueList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample2 = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria2 = dspTlTblParamExample2.createCriteria();
		dspTlParam_criteria2.andParamTypeEqualTo("TL_tsarvalue");
		tsarValueList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample2);

		// Stamp Duty

		List<DspTlTblParam> stampDutyList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample3 = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria3 = dspTlTblParamExample3.createCriteria();
		dspTlParam_criteria3.andParamTypeEqualTo("TL_StampDuty");
		stampDutyList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample3);

		// GST

		List<DspTlTblParam> gstValueList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample4 = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria4 = dspTlTblParamExample4.createCriteria();
		dspTlParam_criteria4.andParamTypeEqualTo("TL_GST");
		gstValueList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample4);

		// Direct Discount (Online)

		List<DspTlTblParam> discountValueList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample5 = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria5 = dspTlTblParamExample5.createCriteria();
		dspTlParam_criteria5.andParamTypeEqualTo("TL_Discount");
		discountValueList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample5);

		// Minimum Age Maximum Age

		List<DspTblRangeRules> dspTblRangeRulesList = new ArrayList<DspTblRangeRules>();
		DspTblRangeRulesExample dspTblRangeRulesExample = new DspTblRangeRulesExample();
		DspTblRangeRulesExample.Criteria dspTlRangeRules_criteria = dspTblRangeRulesExample.createCriteria();
		dspTlRangeRules_criteria.andRuleCodeEqualTo("DOBRange");
		dspTlRangeRules_criteria.andProductCodeEqualTo("TL");
		dspTblRangeRulesList = dspTblRangeRulesMapper.selectByExample(dspTblRangeRulesExample);

		model.addAttribute("minBenAmountList", minBenAmountList);
		model.addAttribute("maxBenAmountList", maxBenAmountList);
		model.addAttribute("tsarValueList", tsarValueList);
		model.addAttribute("stampDutyList", stampDutyList);
		model.addAttribute("gstValueList", gstValueList);
		model.addAttribute("discountValueList", discountValueList);
		model.addAttribute("dspTblRangeRulesList", dspTblRangeRulesList);

		return "/products/termLife/admin-business-product-management-term-product-rate";

	}

	@RequestMapping(value = "/updateTLProductRates", method = RequestMethod.GET)
	public String UpdateTLProductRates(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("SalesLeadsReport funtion ");
		HttpSession session = request.getSession();

		// Minimum Benefit Amount

		List<DspTlTblParam> minBenAmountList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria = dspTlTblParamExample.createCriteria();
		dspTlParam_criteria.andParamTypeEqualTo("pre_ins_cov_start");
		minBenAmountList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample);

		// Maximum Benefit Amount

		List<DspTlTblParam> maxBenAmountList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample1 = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria1 = dspTlTblParamExample1.createCriteria();
		dspTlParam_criteria1.andParamTypeEqualTo("pre_ins_cov_end");
		maxBenAmountList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample1);

		// Total Sum Assured at Risk (TSAR)

		List<DspTlTblParam> tsarValueList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample2 = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria2 = dspTlTblParamExample2.createCriteria();
		dspTlParam_criteria2.andParamTypeEqualTo("TL_tsarvalue");
		tsarValueList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample2);

		// Stamp Duty

		List<DspTlTblParam> stampDutyList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample3 = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria3 = dspTlTblParamExample3.createCriteria();
		dspTlParam_criteria3.andParamTypeEqualTo("TL_StampDuty");
		stampDutyList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample3);

		// GST

		List<DspTlTblParam> gstValueList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample4 = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria4 = dspTlTblParamExample4.createCriteria();
		dspTlParam_criteria4.andParamTypeEqualTo("TL_GST");
		gstValueList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample4);

		// Direct Discount (Online)

		List<DspTlTblParam> discountValueList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample5 = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria5 = dspTlTblParamExample5.createCriteria();
		dspTlParam_criteria5.andParamTypeEqualTo("TL_Discount");
		discountValueList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample5);

		// Minimum Age Maximum Age

		List<DspTblRangeRules> dspTblRangeRulesList = new ArrayList<DspTblRangeRules>();
		DspTblRangeRulesExample dspTblRangeRulesExample = new DspTblRangeRulesExample();
		DspTblRangeRulesExample.Criteria dspTlRangeRules_criteria = dspTblRangeRulesExample.createCriteria();
		dspTlRangeRules_criteria.andRuleCodeEqualTo("DOBRange");
		dspTlRangeRules_criteria.andProductCodeEqualTo("TL");
		dspTblRangeRulesList = dspTblRangeRulesMapper.selectByExample(dspTblRangeRulesExample);

		model.addAttribute("minBenAmountList", minBenAmountList);
		model.addAttribute("maxBenAmountList", maxBenAmountList);
		model.addAttribute("tsarValueList", tsarValueList);
		model.addAttribute("stampDutyList", stampDutyList);
		model.addAttribute("gstValueList", gstValueList);
		model.addAttribute("discountValueList", discountValueList);
		model.addAttribute("dspTblRangeRulesList", dspTblRangeRulesList);

		return "/products/termLife/admin-business-product-management-term-product-rate-edit";

	}

	@RequestMapping(value = "updateDoneTLProductRates", method = RequestMethod.POST)
	public String updateDoneTLProductRates(@ModelAttribute(value = "DspTblRangeRules") DspTblRangeRules tblRangeRules,
			@ModelAttribute(value = "DspTlTblParam") DspTlTblParam tlTblParam, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		System.out.println("SalesLeadsReport update done funtion ");

		HttpSession session = request.getSession();

		// Minimum Benefit Amount
		// discountVal gstVal stampDutyVal minAgeVal minAmntVal maxAmntVal tsarVal
		// maxAgeVal

		DspTlTblParamExample dspTlTblParamExample = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria = dspTlTblParamExample.createCriteria();
		dspTlParam_criteria.andParamTypeEqualTo("pre_ins_cov_start");
		String minAmnt = request.getParameter("minAmntVal");
		System.out.println(minAmnt + "minAmnt");
		tlTblParam.setParamCode(minAmnt == null ? "0" : minAmnt);
		String formatVal = numberFormat(minAmnt);
		tlTblParam.setParamDesc("RM" + formatVal);
		tlTblParam.setParamDescEn("RM" + formatVal);
		int tlTblParamUpdate = dspTlTblParamMapper.updateByExampleSelective(tlTblParam, dspTlTblParamExample);
		System.out.println(tlTblParamUpdate + "min value");

		// Maximum Benefit Amount

		DspTlTblParamExample dspTlTblParamExample1 = new DspTlTblParamExample();
		DspTlTblParam tlTblParam1 = new DspTlTblParam();
		DspTlTblParamExample.Criteria dspTlParam_criteria1 = dspTlTblParamExample1.createCriteria();
		dspTlParam_criteria1.andParamTypeEqualTo("pre_ins_cov_end");

		String maxAmnt = request.getParameter("maxAmntVal");
		System.out.println(maxAmnt + "maxAmnt");
		tlTblParam1.setParamCode(maxAmnt == null ? "0" : maxAmnt);
		String formatVal1 = numberFormat(maxAmnt);
		tlTblParam1.setParamDesc("RM" + formatVal1);
		tlTblParam1.setParamDescEn("RM" + formatVal1);
		int tlTblParamUpdate1 = dspTlTblParamMapper.updateByExampleSelective(tlTblParam1, dspTlTblParamExample1);
		System.out.println(tlTblParamUpdate1 + "max value");

		// Total Sum Assured at Risk (TSAR)

		DspTlTblParamExample dspTlTblParamExample2 = new DspTlTblParamExample();
		DspTlTblParam tlTblParam2 = new DspTlTblParam();
		DspTlTblParamExample.Criteria dspTlParam_criteria2 = dspTlTblParamExample2.createCriteria();
		dspTlParam_criteria2.andParamTypeEqualTo("TL_tsarvalue");

		String tsarVal = request.getParameter("tsarVal");
		System.out.println(tsarVal + "tsarVal");
		tlTblParam2.setParamCode(tsarVal == null ? "0" : tsarVal);
		String formatVal2 = numberFormat(tsarVal);
		tlTblParam2.setParamDesc("RM" + formatVal2);
		tlTblParam2.setParamDescEn("RM" + formatVal2);
		int tlTblParamUpdate2 = dspTlTblParamMapper.updateByExampleSelective(tlTblParam2, dspTlTblParamExample2);
		System.out.println(tlTblParamUpdate2 + "tsarVal value");

		// Stamp Duty

		DspTlTblParamExample dspTlTblParamExample3 = new DspTlTblParamExample();
		DspTlTblParam tlTblParam3 = new DspTlTblParam();
		DspTlTblParamExample.Criteria dspTlParam_criteria3 = dspTlTblParamExample3.createCriteria();
		dspTlParam_criteria3.andParamTypeEqualTo("TL_StampDuty");

		String stampDuty = request.getParameter("stampDutyVal");
		System.out.println(stampDuty + "stampDuty");
		tlTblParam3.setParamCode(stampDuty == null ? "0" : stampDuty);
		String formatVal3 = numberFormat(stampDuty);
		tlTblParam3.setParamDesc("RM" + stampDuty);
		tlTblParam3.setParamDescEn("RM" + stampDuty);
		int tlTblParamUpdate3 = dspTlTblParamMapper.updateByExampleSelective(tlTblParam3, dspTlTblParamExample3);
		System.out.println(tlTblParamUpdate3 + "stampDuty value");

		// GST

		DspTlTblParamExample dspTlTblParamExample4 = new DspTlTblParamExample();
		DspTlTblParam tlTblParam4 = new DspTlTblParam();
		DspTlTblParamExample.Criteria dspTlParam_criteria4 = dspTlTblParamExample4.createCriteria();
		dspTlParam_criteria4.andParamTypeEqualTo("TL_GST");

		String gstVal = request.getParameter("gstVal");
		System.out.println(gstVal + "gstVal");
		tlTblParam4.setParamCode(gstVal == null ? "0" : gstVal);
		String formatVal4 = numberFormat(gstVal);
		tlTblParam4.setParamDesc(gstVal + " %");
		tlTblParam4.setParamDescEn(gstVal + " %");
		int tlTblParamUpdate4 = dspTlTblParamMapper.updateByExampleSelective(tlTblParam4, dspTlTblParamExample4);
		System.out.println(tlTblParamUpdate4 + "gstVal value");

		// Direct Discount (Online)

		DspTlTblParamExample dspTlTblParamExample5 = new DspTlTblParamExample();
		DspTlTblParam tlTblParam5 = new DspTlTblParam();
		DspTlTblParamExample.Criteria dspTlParam_criteria5 = dspTlTblParamExample5.createCriteria();
		dspTlParam_criteria5.andParamTypeEqualTo("TL_Discount");

		String discVal = request.getParameter("discountVal");
		System.out.println(discVal + "discVal");
		tlTblParam5.setParamCode(discVal == null ? "0" : discVal);
		String formatVal5 = numberFormat(discVal);
		tlTblParam5.setParamDesc(discVal + " %");
		tlTblParam5.setParamDescEn(discVal + " %");
		int tlTblParamUpdate5 = dspTlTblParamMapper.updateByExampleSelective(tlTblParam5, dspTlTblParamExample5);
		System.out.println(tlTblParamUpdate5 + "discVal value");

		// Minimum Age Maximum Age
		DspTblRangeRulesExample dspTblRangeRulesExample = new DspTblRangeRulesExample();
		DspTblRangeRulesExample.Criteria dspTlRangeRules_criteria = dspTblRangeRulesExample.createCriteria();
		dspTlRangeRules_criteria.andRuleCodeEqualTo("DOBRange");
		dspTlRangeRules_criteria.andProductCodeEqualTo("TL");
		String minAge = request.getParameter("minAgeVal");
		String maxAge = request.getParameter("maxAgeVal");
		System.out.println(minAge + "minAgeVal  maxAgeVal" + maxAge);
		tblRangeRules.setRuleCodeMinValue(Integer.valueOf(minAge == null ? "0" : minAge));
		tblRangeRules.setRuleCodeMaxValue(Integer.valueOf(maxAge == null ? "0" : maxAge));
		int rangeRuleUpdate = dspTblRangeRulesMapper.updateByExampleSelective(tblRangeRules, dspTblRangeRulesExample);
		System.out.println(rangeRuleUpdate + "Updation of range rules");
		model.addAttribute("updatemessage", "Update data successfully");

		String returnURL = TlProductRates(request, response, model);

		return returnURL;

	}

	public String numberFormat(String val) {
		Double iVal = null;
		iVal = Double.parseDouble(val == null ? "0" : val);
		DecimalFormat df = new DecimalFormat("0,000");
		System.out.println(df.format(iVal));
		String formatVal = df.format(iVal);
		return formatVal;
	}

	@RequestMapping("/tlprodmngproductinfo")
	public String TlProductInfo(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("Product Info ");

		HttpSession session = request.getSession();

		// Quotaion Validity

		List<DspTlTblParam> quotationValidityList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria = dspTlTblParamExample.createCriteria();
		dspTlParam_criteria.andParamTypeEqualTo("TL_quote_validity");
		quotationValidityList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample);

		// Annual Sales Target

		List<DspTlTblParam> annualSalesAmntList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample1 = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria1 = dspTlTblParamExample1.createCriteria();
		dspTlParam_criteria1.andParamTypeEqualTo("TL_annual_sales_target");
		annualSalesAmntList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample1);

		List<TblPdfInfo> tblPdfInfoList = new ArrayList<TblPdfInfo>();
		TblPdfInfoExample tblLoadingExample = new TblPdfInfoExample();
		tblLoadingExample.setOrderByClause("pds.ID desc");
		// SalesLeadsExample.Criteria salesLeads_criteria=
		// salesLeadsExample.createCriteria();
		tblPdfInfoList = tblPdfInfoMapper.selectByExample(tblLoadingExample);

		model.addAttribute("tblPdfInfoList", tblPdfInfoList);

		model.addAttribute("quotationValidityList", quotationValidityList);
		model.addAttribute("annualSalesAmntList", annualSalesAmntList);

		return "/products/termLife/admin-business-product-management-term-product-infomation";

	}

	@RequestMapping("/updateTlprodmngproductinfo")
	public String updateTlprodmngproductinfo(HttpServletRequest request, HttpServletResponse response, Model model) {
		System.out.println("Product Info ");

		HttpSession session = request.getSession();

		// Quotaion Validity

		List<DspTlTblParam> quotationValidityList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria = dspTlTblParamExample.createCriteria();
		dspTlParam_criteria.andParamTypeEqualTo("TL_quote_validity");
		quotationValidityList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample);

		// Annual Sales Target

		List<DspTlTblParam> annualSalesAmntList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample1 = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria1 = dspTlTblParamExample1.createCriteria();
		dspTlParam_criteria1.andParamTypeEqualTo("TL_annual_sales_target");
		annualSalesAmntList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample1);

		List<TblPdfInfo> tblPdfInfoList = new ArrayList<TblPdfInfo>();
		TblPdfInfoExample tblLoadingExample = new TblPdfInfoExample();
		tblLoadingExample.setOrderByClause("pds.ID desc");
		// SalesLeadsExample.Criteria salesLeads_criteria=
		// salesLeadsExample.createCriteria();
		tblPdfInfoList = tblPdfInfoMapper.selectByExample(tblLoadingExample);

		model.addAttribute("tblPdfInfoList", tblPdfInfoList);

		model.addAttribute("quotationValidityList", quotationValidityList);
		model.addAttribute("annualSalesAmntList", annualSalesAmntList);

		return "/products/termLife/admin-business-product-management-term-product-infomation-edit";

	}

	@RequestMapping("/updateDoneTlprodmngproductinfo")
	public String updateDoneTlprodmngproductinfo(HttpServletRequest request, HttpServletResponse response,
			Model model) {
		System.out.println("Product Info ");

		HttpSession session = request.getSession();

		// Quotaion Validity

		DspTlTblParamExample dspTlTblParamExample = new DspTlTblParamExample();
		DspTlTblParam tlTblParam = new DspTlTblParam();
		DspTlTblParamExample.Criteria dspTlParam_criteria = dspTlTblParamExample.createCriteria();
		dspTlParam_criteria.andParamTypeEqualTo("TL_quote_validity");

		String quotVal = request.getParameter("quotVal");
		System.out.println(quotVal + "quotVal");
		tlTblParam.setParamCode(quotVal == null ? "0" : quotVal);
		String formatVal = numberFormat(quotVal);
		tlTblParam.setParamDesc(quotVal + " days");
		tlTblParam.setParamDescEn(quotVal + " days");
		int tlTblParamUpdate = dspTlTblParamMapper.updateByExampleSelective(tlTblParam, dspTlTblParamExample);
		System.out.println(tlTblParamUpdate + "quotVal value");

		// Annual Sales Target

		DspTlTblParamExample dspTlTblParamExample1 = new DspTlTblParamExample();
		DspTlTblParam tlTblParam1 = new DspTlTblParam();
		DspTlTblParamExample.Criteria dspTlParam_criteria1 = dspTlTblParamExample1.createCriteria();
		dspTlParam_criteria1.andParamTypeEqualTo("TL_annual_sales_target");

		String salesTargetVal = request.getParameter("salesTargetVal");
		System.out.println(salesTargetVal + "salesTargetVal");
		tlTblParam1.setParamCode(salesTargetVal == null ? "0" : salesTargetVal);
		String formatVal1 = numberFormat(salesTargetVal);
		tlTblParam1.setParamDesc("RM" + formatVal1);
		tlTblParam1.setParamDescEn("RM" + formatVal1);
		int tlTblParamUpdate1 = dspTlTblParamMapper.updateByExampleSelective(tlTblParam1, dspTlTblParamExample1);
		System.out.println(tlTblParamUpdate1 + "salesTargetVal value");

		model.addAttribute("updatemessage", "Update data successfully");
		String returnURL = TlProductInfo(request, response, model);
		return returnURL;

	}

	@RequestMapping(value = "/uploadTLPdfFile", method = RequestMethod.POST)
	public String uploadFileHandler(@RequestParam("PDF") CommonsMultipartFile file, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		String redirctTO = null, fileName = null, filePath = null;
		String fileNameFormat = new SimpleDateFormat("yyyyMMddHHmm'.pdf'").format(new Date());
		fileName = request.getParameter("tlFileName");
		if (!file.isEmpty()) {
			/*
			 * File a = convert(file); a.renameTo(new File("G:\\1702\\" +
			 * fileName+fileNameFormat));
			 */
			try (InputStream in = this.getClass().getClassLoader()
					.getResourceAsStream("com/etiqa/dsp/sales/process/email/documents.properties")) {
				Properties prop = new Properties();
				prop.load(in);

				// ********************** File writing to another locaiton
				// Starts*********************
				filePath = prop.getProperty("TLPdfDestinationPath") + fileName + fileNameFormat;
				InputStream input = new FileInputStream(convert(file));
				FileOutputStream fos = new FileOutputStream(filePath, true);
				int b = 0;
				while ((b = input.read()) != -1) {
					fos.write(b);
				}
				System.out.println("File has been written");
			} catch (Exception e) {
				System.out.println("Could not create file");
			}
			// ********************** File writing to another locaiton
			// Ends*********************

			HttpSession session = request.getSession();
			String loginUser = (String) session.getAttribute("user");
			if (loginUser == null) {
				String sessionexpired = "Session Has Been Expired";
				model.addAttribute("sessionexpired", sessionexpired);
				return "admin-login";
			}
			TblPdfInfo tblPdfInfo = new TblPdfInfo();

			Timestamp timestamp = new Timestamp(System.currentTimeMillis());

			tblPdfInfo.setProductCode("TL");
			tblPdfInfo.setFileName(fileName);
			tblPdfInfo.setFilePath(filePath);
			double bytes = convert(file).length();
			double kilobytes = bytes / 1024;
			DecimalFormat df = new DecimalFormat("0.00");
			tblPdfInfo.setFileSize(df.format(kilobytes));
			tblPdfInfo.setCreatedBy(loginUser);
			tblPdfInfo.setCreatedDate(timestamp);

			tblPdfInfoMapper.insert(tblPdfInfo);

			System.out.println("insertion Successfully Done");
			model.addAttribute("SaveMessage", "Data Added Successfully!");

			redirctTO = TlProductInfo(request, response, model);
		}
		return redirctTO;
	}

	/*-------------------------------------------------------Start- Delete PDS----------------------------------------------------------*/
	@RequestMapping(value = "/deleteTlPdsDone", method = RequestMethod.POST)
	public String deleteAgentDone(@ModelAttribute(value = "tblPdfInfo") TblPdfInfo tblPdfInfo, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String id = request.getParameter("id");
		tblPdfInfo.setId(Integer.valueOf(id));
		tblPdfInfoMapper.deleteByPrimaryKey(Short.valueOf(request.getParameter("id").trim()));

		// ******************** file deleting Starts***************************
		// tlFilePath
		String tlFilePath = request.getParameter("tlFilePath");
		if (tlFilePath != null || !tlFilePath.isEmpty()) {
			File file = new File(tlFilePath);

			if (file.delete()) {
				System.out.println(file.getName() + " is deleted!");
			} else {
				System.out.println("Delete operation is failed.");
			}
		}
		// ******************** file deleting Starts***************************
		System.out.println("Successfully Deleted");
		model.addAttribute("deletemessage", "Delete data successfully");
		String redirctTO = updateTlprodmngproductinfo(request, response, model);
		return redirctTO;

	}

	/*-------------------------------------------------------End- Delete PDS----------------------------------------------------------*/

	@RequestMapping(value = "/DownloadPdfFile", method = RequestMethod.POST)
	// @RequestMapping("/DownloadPdfFile")
	public void DownloadPdfFile(HttpServletRequest request, HttpServletResponse response, Model model,
			@ModelAttribute(value = "tblPdfInfo") TblPdfInfo tblPdfInfo) {

		String id = request.getParameter("id");
		tblPdfInfo.setId(Integer.valueOf(id));
		// tblPdfInfoMapper.deleteByPrimaryKey(Short.valueOf(request.getParameter("id").trim()));
		/*
		 * tblPdfInfoMapper.selectByPrimaryKey(Short.valueOf(request.getParameter("id").
		 * trim())); System.out.println("Id value for "+id);
		 *
		 * TblPdfInfo tblPdf = new TblPdfInfo();
		 */

		String tlFilePath = request.getParameter("tlFilePathDownload");
		String FileName = request.getParameter("tlFileName");
		/*
		 * String tlFilePath = tblPdf.getFilePath(); String FileName =
		 * tblPdf.getFileName();
		 */
		System.out.println("tlFilePath value for " + tlFilePath);
		System.out.println("FileName  " + FileName);

		if (tlFilePath != null || !tlFilePath.isEmpty()) {
			File pdfFile = new File(tlFilePath);

			response.setContentType("application/pdf");
			response.addHeader("Content-Disposition", "attachment; filename=" + FileName + ".pdf");
			response.setContentLength((int) pdfFile.length());

			FileInputStream fileInputStream;
			try {
				fileInputStream = new FileInputStream(pdfFile);
				OutputStream responseOutputStream = response.getOutputStream();
				int bytes;
				while ((bytes = fileInputStream.read()) != -1) {
					responseOutputStream.write(bytes);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	public File convert(MultipartFile file) {
		File convFile = new File(file.getOriginalFilename());
		FileOutputStream fos = null;
		try {
			convFile.createNewFile();
			fos = new FileOutputStream(convFile);
			fos.write(file.getBytes());
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return convFile;
	}

	/* Approval Product Information Starts Here */

	@RequestMapping("/updateDoneTlprodmngproductinfoapproval")
	public String updateDoneTlprodmngproductinfoapproval(HttpServletRequest request, HttpServletResponse response,
			Model model) {
		System.out.println("Product Info ");

		HttpSession session = request.getSession();
		String logedUser = (String) session.getAttribute("logedUser");

		// Quotaion Validity

		List<DspTlTblParam> quotationValidityList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria = dspTlTblParamExample.createCriteria();
		dspTlParam_criteria.andParamTypeEqualTo("TL_quote_validity");
		quotationValidityList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample);

		// Annual Sales Target

		List<DspTlTblParam> annualSalesAmntList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample1 = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria1 = dspTlTblParamExample1.createCriteria();
		dspTlParam_criteria1.andParamTypeEqualTo("TL_annual_sales_target");
		annualSalesAmntList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample1);

		List<Object> originalList = new ArrayList<Object>();
		originalList.addAll(quotationValidityList);
		originalList.addAll(annualSalesAmntList);

		String quotVal = request.getParameter("quotVal");
		String salesTargetVal = request.getParameter("salesTargetVal");

		List<String> newList = new ArrayList<String>();

		newList.add(quotVal);
		newList.add(salesTargetVal);

		ApprovalExample approvalExample = new ApprovalExample();
		com.spring.VO.ApprovalExample.Criteria createCriteria_approvalExample = approvalExample.createCriteria();
		createCriteria_approvalExample.andDescriptionEqualTo("EZY-LIFE SECURE - CHANGE PRODUCT INFORMATION");
		List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

		try {
			byte[] originalData = convertToBytes(originalList);
			byte[] changeData = convertToBytes(newList);

			System.out.println("ORI Data" + originalData);
			System.out.println("change Data" + changeData);

			BigDecimal approvalProdId = new BigDecimal(0);
			if (listapproval.size() > 0) {
				approvalProdId = listapproval.get(0).getId();
			}

			ApprovalLog approvalLog = new ApprovalLog();
			approvalLog.setNewContent(changeData);
			approvalLog.setOriginalContent(originalData);
			approvalLog.setApprovalId((short) 11); // from Approval table this value[7] based on menu in feature
			// approvalLog.setMaker((short)1); // session login value
			approvalLog.setMaker(Short.parseShort(logedUser));
			approvalLog.setStatus("1");
			approvalLog.setCreateDate(new Date());
			// approvalLog.setUpdateDate(new Date());
			approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
			int rs = approvalLogMapper.insert(approvalLog);

			if (rs == 1) {
				model.addAttribute("updatemessage", "Updated data successfully");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("updatemessage", "Updated data failed");
		}

		String returnURL = TlProductInfo(request, response, model);
		return returnURL;

	}

	private byte[] convertToBytes(Object object) throws IOException {
		try (ByteArrayOutputStream bos = new ByteArrayOutputStream(); ObjectOutput out = new ObjectOutputStream(bos)) {
			out.writeObject(object);
			return bos.toByteArray();
		}
	}

	private Object convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
		try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes); ObjectInput in = new ObjectInputStream(bis)) {
			return in.readObject();
		}
	}

	@RequestMapping(value = "/approvalEasyLifeProductInfo", method = RequestMethod.GET)
	public String approvalEasyLifeProductInfo(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Reequest " + request.getParameter("id"));
		ApprovalLogExample approvalexample = new ApprovalLogExample();
		com.spring.VO.ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();
		Integer testint = new Integer(request.getParameter("id"));
		createCriteria.andIdEqualTo(testint.shortValue());
		List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
		System.out.println("   list size in approval list  :::   " + listapprovallog.size());
		// System.out.println("Testing Original Content
		// "+listapprovallog.get(0).getOriginalContent());

		try {
			// original data
			if (listapprovallog.size() > 0) {
				Object originalData = convertFromBytes(listapprovallog.get(0).getOriginalContent());
				List<Object> listOriginalData = new ArrayList<Object>();
				// objecct convert into AgentProdMap
				if (originalData != null) {
					listOriginalData = (List<Object>) originalData;
					System.out.println("  list agent prod map size ::::::         " + listOriginalData.size());

					MIProductInfoApproval miProductInfoApproval = new MIProductInfoApproval();
					miProductInfoApproval.setAnnualTarget(((DspTlTblParam) listOriginalData.get(1)).getParamCode());
					miProductInfoApproval.setValidity(((DspTlTblParam) listOriginalData.get(0)).getParamCode());

					List<MIProductInfoApproval> listOriginalELSProductInfo = new ArrayList<MIProductInfoApproval>();
					listOriginalELSProductInfo.add(miProductInfoApproval);
					model.addAttribute("listOriginalELSProductInfo", listOriginalELSProductInfo);

					System.out.println("-----------------------------");
				} // inner if
			} // outer if

			// Change Data
			if (listapprovallog.size() > 0) {
				Object changeData = convertFromBytes(listapprovallog.get(0).getNewContent());
				List<String> listChangeData = new ArrayList<String>();
				// objecct convert into AgentProdMap
				if (changeData != null) {
					listChangeData = (List<String>) changeData;
					System.out.println("  list agent prod map size ::::::         " + listChangeData.size());

					MIProductInfoApproval miProductInfoApproval = new MIProductInfoApproval();

					miProductInfoApproval.setAnnualTarget(listChangeData.get(1));
					miProductInfoApproval.setValidity(listChangeData.get(0));

					List<MIProductInfoApproval> listChangeELSProductInfo = new ArrayList<MIProductInfoApproval>();

					listChangeELSProductInfo.add(miProductInfoApproval);
					model.addAttribute("listChangeELSProductInfo", listChangeELSProductInfo);
					System.out.println("-----------------------------");
				}
			}
		} // try end
		catch (Exception e) {
			System.out.println("Exception in Convert Object to Byte" + e);
		}

		return "agent-product-approval";
	}

	@RequestMapping("/approveELSProductInfoChangeData")
	public String approveELSProductInfoChangeData(HttpServletRequest request, HttpServletResponse response,
			Model model) {
		String returnURL = "";

		HttpSession session = request.getSession();
		String logedUser = (String) session.getAttribute("logedUser");

		try {

			System.out.println("Reequest " + request.getParameter("id"));
			String aid = request.getParameter("approvalLogId");
			System.out.println("aid >>>>>" + aid);

			ApprovalLogExample approvalexample = new ApprovalLogExample();
			ApprovalLog alog = new ApprovalLog();
			ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();

			if (null != aid && !aid.isEmpty()) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
				System.out.println("   list size in approval list  :::   " + listapprovallog.size());
				// System.out.println("Testing Original Content
				// "+listapprovallog.get(0).getOriginalContent());

				// Change Data
				if (listapprovallog.size() > 0) {

					String status = listapprovallog.get(0).getStatus();
					short alogId = listapprovallog.get(0).getId();
					System.out.println("Status  :  " + status);
					System.out.println("Approvallog Id  :  " + alogId);

					Object changeData = convertFromBytes(listapprovallog.get(0).getNewContent());
					List<String> listChangeData = new ArrayList<String>();
					// objecct convert into AgentProdMap
					if (changeData != null) {
						listChangeData = (List<String>) changeData;
						System.out.println("  list agent prod map size ::::::         " + listChangeData.size());
						MIProductInfoApproval miProductInfoApproval = new MIProductInfoApproval();

						miProductInfoApproval.setAnnualTarget(listChangeData.get(1));
						miProductInfoApproval.setValidity(listChangeData.get(0));
						// Quotaion Validity

						DspTlTblParamExample dspTlTblParamExample = new DspTlTblParamExample();
						DspTlTblParam tlTblParam = new DspTlTblParam();
						DspTlTblParamExample.Criteria dspTlParam_criteria = dspTlTblParamExample.createCriteria();
						dspTlParam_criteria.andParamTypeEqualTo("TL_quote_validity");

						String quotVal = miProductInfoApproval.getValidity();
						System.out.println(quotVal + "quotVal");
						tlTblParam.setParamCode(quotVal == null ? "0" : quotVal);
						String formatVal = numberFormat(quotVal);
						tlTblParam.setParamDesc(quotVal + " days");
						tlTblParam.setParamDescEn(quotVal + " days");
						int tlTblParamUpdate = dspTlTblParamMapper.updateByExampleSelective(tlTblParam,
								dspTlTblParamExample);
						System.out.println(tlTblParamUpdate + "quotVal value");

						// Annual Sales Target

						DspTlTblParamExample dspTlTblParamExample1 = new DspTlTblParamExample();
						DspTlTblParam tlTblParam1 = new DspTlTblParam();
						DspTlTblParamExample.Criteria dspTlParam_criteria1 = dspTlTblParamExample1.createCriteria();
						dspTlParam_criteria1.andParamTypeEqualTo("TL_annual_sales_target");

						String salesTargetVal = miProductInfoApproval.getAnnualTarget();
						System.out.println(salesTargetVal + "salesTargetVal");
						tlTblParam1.setParamCode(salesTargetVal == null ? "0" : salesTargetVal);
						// String formatVal1=numberFormat(salesTargetVal);
						String formatVal1 = salesTargetVal;
						tlTblParam1.setParamDesc("RM" + formatVal1);
						tlTblParam1.setParamDescEn("RM" + formatVal1);
						int tlTblParamUpdate1 = dspTlTblParamMapper.updateByExampleSelective(tlTblParam1,
								dspTlTblParamExample1);
						System.out.println(tlTblParamUpdate1 + "salesTargetVal value");

						// Update status in DSP_AFM_TBL_Approval_Log table
						alog.setId(alogId);
						alog.setStatus("3");
						// alog.setChecker((short) 1); //get from the current login session
						alog.setChecker(Short.parseShort(logedUser));
						alog.setUpdateDate(new Date());
						int rs = approvalLogMapper.updateByPrimaryKeySelective(alog);

						if (rs == 1) {

							model.addAttribute("approvemessage", "Approved Data Successfully");
						}

						AgentController agentController = new AgentController(agentProfileMapper, productsMapper,
								agentProdMapMapper, commonQQMapper, adminParamMapper, agentDocumentMapper,
								agentLinkMapper, approvalLogMapper, approvalMapper);

						returnURL = agentController.getAgentProductMapApproval(request, response, model);

					} // end forchangeData
				} // end for listapprovallog
			} // Fot if aid
		} // Try end
		catch (Exception e) {
			System.out.println("Exception In >> MI product rates update done funtion " + e);
			model.addAttribute("approvemessage", "Approve Data Failed");
		}
		return returnURL;

	}

	@RequestMapping(value = "/rejectELSProductInfoChangeData", method = RequestMethod.GET)
	public String rejectELSProductInfoChangeData(@ModelAttribute AgentProfile agentProfile, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		String logedUser = (String) session.getAttribute("logedUser");
		int rs = 0;
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		ApprovalLogExample approvalLogexample = new ApprovalLogExample();
		ApprovalLog alog = new ApprovalLog();
		ApprovalLogExample.Criteria createCriteria = approvalLogexample.createCriteria();
		String aid = request.getParameter("approvalLogId");
		if (null != aid && !aid.isEmpty()) {
			Integer testint = new Integer(aid);
			createCriteria.andIdEqualTo(testint.shortValue());
			List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalLogexample);
			System.out.println("   list size in approval list  :::   " + listapprovallog.size());

			short alogId = listapprovallog.get(0).getId();
			alog.setId(alogId);
			alog.setStatus("2");
			// alog.setChecker((short) 1); //get from the current login session
			alog.setChecker(Short.parseShort(logedUser));
			alog.setUpdateDate(new Date());
			rs = approvalLogMapper.updateByPrimaryKeySelective(alog);
		}

		if (rs == 1) {

			model.addAttribute("rejectmessage", "Rejected Data Successfully");
		}

		else {

			model.addAttribute("rejectmessage", "Reject Data Failed");
		}

		AgentController agentController = new AgentController(agentProfileMapper, productsMapper, agentProdMapMapper,
				commonQQMapper, adminParamMapper, agentDocumentMapper, agentLinkMapper, approvalLogMapper,
				approvalMapper);

		String returnURL = agentController.getAgentProductMapApproval(request, response, model);
		;
		return returnURL;
	}

	/* Approval Product Information Ends Here */

	/* Approval Product Rate Starts Here */

	@RequestMapping(value = "updateDoneTLProductRatesApproval", method = RequestMethod.POST)
	public String updateDoneTLProductRatesApproval(
			@ModelAttribute(value = "DspTblRangeRules") DspTblRangeRules tblRangeRules,
			@ModelAttribute(value = "DspTlTblParam") DspTlTblParam tlTblParam, HttpServletRequest request,
			HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String logedUser = (String) session.getAttribute("logedUser");
		// Minimum Benefit Amount

		List<DspTlTblParam> minBenAmountList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria = dspTlTblParamExample.createCriteria();
		dspTlParam_criteria.andParamTypeEqualTo("pre_ins_cov_start");
		minBenAmountList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample);

		// Maximum Benefit Amount

		List<DspTlTblParam> maxBenAmountList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample1 = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria1 = dspTlTblParamExample1.createCriteria();
		dspTlParam_criteria1.andParamTypeEqualTo("pre_ins_cov_end");
		maxBenAmountList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample1);

		// Total Sum Assured at Risk (TSAR)

		List<DspTlTblParam> tsarValueList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample2 = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria2 = dspTlTblParamExample2.createCriteria();
		dspTlParam_criteria2.andParamTypeEqualTo("TL_tsarvalue");
		tsarValueList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample2);

		// Stamp Duty

		List<DspTlTblParam> stampDutyList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample3 = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria3 = dspTlTblParamExample3.createCriteria();
		dspTlParam_criteria3.andParamTypeEqualTo("TL_StampDuty");
		stampDutyList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample3);

		// GST

		List<DspTlTblParam> gstValueList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample4 = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria4 = dspTlTblParamExample4.createCriteria();
		dspTlParam_criteria4.andParamTypeEqualTo("TL_GST");
		gstValueList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample4);

		// Direct Discount (Online)

		List<DspTlTblParam> discountValueList = new ArrayList<DspTlTblParam>();
		DspTlTblParamExample dspTlTblParamExample5 = new DspTlTblParamExample();
		DspTlTblParamExample.Criteria dspTlParam_criteria5 = dspTlTblParamExample5.createCriteria();
		dspTlParam_criteria5.andParamTypeEqualTo("TL_Discount");
		discountValueList = dspTlTblParamMapper.selectByExample(dspTlTblParamExample5);

		// Minimum Age Maximum Age

		List<DspTblRangeRules> dspTblRangeRulesList = new ArrayList<DspTblRangeRules>();
		DspTblRangeRulesExample dspTblRangeRulesExample = new DspTblRangeRulesExample();
		DspTblRangeRulesExample.Criteria dspTlRangeRules_criteria = dspTblRangeRulesExample.createCriteria();
		dspTlRangeRules_criteria.andRuleCodeEqualTo("DOBRange");
		dspTlRangeRules_criteria.andProductCodeEqualTo("TL");
		dspTblRangeRulesList = dspTblRangeRulesMapper.selectByExample(dspTblRangeRulesExample);

		List<Object> originalList = new ArrayList<Object>();
		originalList.addAll(minBenAmountList);
		originalList.addAll(maxBenAmountList);
		originalList.addAll(tsarValueList);
		originalList.addAll(stampDutyList);
		originalList.addAll(gstValueList);
		originalList.addAll(discountValueList);
		originalList.addAll(dspTblRangeRulesList);

		String minAmnt = request.getParameter("minAmntVal");
		String maxAmnt = request.getParameter("maxAmntVal");
		String tsarVal = request.getParameter("tsarVal");
		String stampDuty = request.getParameter("stampDutyVal");
		String gstVal = request.getParameter("gstVal");
		String discVal = request.getParameter("discountVal");
		String minAge = request.getParameter("minAgeVal");
		String maxAge = request.getParameter("maxAgeVal");

		List<String> newList = new ArrayList<String>();

		newList.add(minAmnt);
		newList.add(maxAmnt);
		newList.add(tsarVal);
		newList.add(stampDuty);
		newList.add(gstVal);
		newList.add(discVal);
		newList.add(minAge);
		newList.add(maxAge);

		ApprovalExample approvalExample = new ApprovalExample();
		com.spring.VO.ApprovalExample.Criteria createCriteria_approvalExample = approvalExample.createCriteria();
		createCriteria_approvalExample.andDescriptionEqualTo("EZY-LIFE SECURE - CHANGE PRODUCT RATE");
		List<Approval> listapproval = approvalMapper.selectByExample(approvalExample);

		try {
			byte[] originalData = convertToBytes(originalList);
			byte[] changeData = convertToBytes(newList);

			System.out.println("ORI Data" + originalData);
			System.out.println("change Data" + changeData);

			BigDecimal approvalProdId = new BigDecimal(0);
			if (listapproval.size() > 0) {
				approvalProdId = listapproval.get(0).getId();
			}

			ApprovalLog approvalLog = new ApprovalLog();
			approvalLog.setNewContent(changeData);
			approvalLog.setOriginalContent(originalData);
			approvalLog.setApprovalId((short) 10); // from Approval table this value[7] based on menu in feature
			// approvalLog.setMaker((short)1); // session login value
			approvalLog.setMaker(Short.parseShort(logedUser));
			approvalLog.setStatus("1");
			approvalLog.setCreateDate(new Date());
			// approvalLog.setUpdateDate(new Date());
			approvalLog.setAppType(String.valueOf(approvalProdId.intValue()));
			int rs = approvalLogMapper.insert(approvalLog);
			if (rs == 1) {
				model.addAttribute("updatemessage", "Update data successfully");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("updatemessage", "Update data failed");
		}

		String returnURL = TlProductRates(request, response, model);

		return returnURL;

	}

	@RequestMapping(value = "/approvalEasyLifeProductRate", method = RequestMethod.GET)
	public String approvalEasyLifeProductRate(@ModelAttribute ApprovalLog alog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println("Reequest " + request.getParameter("id"));
		ApprovalLogExample approvalexample = new ApprovalLogExample();
		com.spring.VO.ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();
		Integer testint = new Integer(request.getParameter("id"));
		createCriteria.andIdEqualTo(testint.shortValue());
		List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
		System.out.println("   list size in approval list  :::   " + listapprovallog.size());
		// System.out.println("Testing Original Content
		// "+listapprovallog.get(0).getOriginalContent());

		try {
			// original data
			if (listapprovallog.size() > 0) {
				Object originalData = convertFromBytes(listapprovallog.get(0).getOriginalContent());
				List<Object> listOriginalData = new ArrayList<Object>();
				// objecct convert into AgentProdMap
				if (originalData != null) {
					listOriginalData = (List<Object>) originalData;

					TLApprovalProductRate tlProductApprovalRate = new TLApprovalProductRate();
					tlProductApprovalRate
							.setMinAmt(((DspTlTblParam) listOriginalData.get(0)).getParamCode().toString());
					tlProductApprovalRate
							.setMaxAmt(((DspTlTblParam) listOriginalData.get(1)).getParamCode().toString());
					tlProductApprovalRate
							.setTsarVal(((DspTlTblParam) listOriginalData.get(2)).getParamCode().toString());
					tlProductApprovalRate
							.setStampDuty(((DspTlTblParam) listOriginalData.get(3)).getParamCode().toString());
					tlProductApprovalRate
							.setGstVal(((DspTlTblParam) listOriginalData.get(4)).getParamCode().toString());
					tlProductApprovalRate
							.setDiscVal(((DspTlTblParam) listOriginalData.get(5)).getParamCode().toString());
					tlProductApprovalRate
							.setMinAge(((DspTblRangeRules) listOriginalData.get(6)).getRuleCodeMinValue().toString());
					tlProductApprovalRate
							.setMaxAge(((DspTblRangeRules) listOriginalData.get(6)).getRuleCodeMaxValue().toString());

					List<TLApprovalProductRate> listELSOriginalData = new ArrayList<TLApprovalProductRate>();
					listELSOriginalData.add(tlProductApprovalRate);
					model.addAttribute("listELSOriginalData", listELSOriginalData);

					System.out.println("-----------------------------");
				} // inner if
			} // outer if

			// Change Data
			if (listapprovallog.size() > 0) {
				Object changeData = convertFromBytes(listapprovallog.get(0).getNewContent());
				List<String> listChangeData = new ArrayList<String>();
				// objecct convert into AgentProdMap
				if (changeData != null) {
					listChangeData = (List<String>) changeData;
					System.out.println("  list agent prod map size ::::::         " + listChangeData.size());

					TLApprovalProductRate tlProductApprovalRate = new TLApprovalProductRate();
					tlProductApprovalRate.setMinAmt(listChangeData.get(0));
					tlProductApprovalRate.setMaxAmt(listChangeData.get(1));
					tlProductApprovalRate.setTsarVal(listChangeData.get(2));
					tlProductApprovalRate.setStampDuty(listChangeData.get(3));
					tlProductApprovalRate.setGstVal(listChangeData.get(4));
					tlProductApprovalRate.setDiscVal(listChangeData.get(5));
					tlProductApprovalRate.setMinAge(listChangeData.get(6));
					tlProductApprovalRate.setMaxAge(listChangeData.get(7));

					List<TLApprovalProductRate> listChangeELSData = new ArrayList<TLApprovalProductRate>();

					listChangeELSData.add(tlProductApprovalRate);
					model.addAttribute("listChangeELSData", listChangeELSData);
					System.out.println("-----------------------------");
				}
			}
		} // try end
		catch (Exception e) {
			System.out.println("Exception in Convert Object to Byte" + e);
		}

		return "agent-product-approval";
	}

	@RequestMapping("/approveELSProductRateChangeData")
	private String approveELSProductRateChangeData(
			@ModelAttribute(value = "DspTblRangeRules") DspTblRangeRules tblRangeRules,
			@ModelAttribute(value = "DspTlTblParam") DspTlTblParam tlTblParam, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		System.out.println("Product rate updated");
		String loginUser = (String) session.getAttribute("user");
		String logedUser = (String) session.getAttribute("logedUser");
		String returnURL = "";
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		try {

			System.out.println("Reequest " + request.getParameter("id"));
			String aid = request.getParameter("approvalLogId");
			System.out.println("aid >>>>>" + aid);

			ApprovalLogExample approvalexample = new ApprovalLogExample();
			ApprovalLog alog = new ApprovalLog();
			ApprovalLogExample.Criteria createCriteria = approvalexample.createCriteria();

			if (null != aid && !aid.isEmpty()) {
				Integer testint = new Integer(aid);
				createCriteria.andIdEqualTo(testint.shortValue());
				List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalexample);
				System.out.println("   list size in approval list  :::   " + listapprovallog.size());
				// System.out.println("Testing Original Content
				// "+listapprovallog.get(0).getOriginalContent());

				// Change Data
				if (listapprovallog.size() > 0) {

					String status = listapprovallog.get(0).getStatus();
					short alogId = listapprovallog.get(0).getId();
					System.out.println("Status  :  " + status);
					System.out.println("Approvallog Id  :  " + alogId);

					Object changeData = convertFromBytes(listapprovallog.get(0).getNewContent());
					List<String> listChangeData = new ArrayList<String>();
					// objecct convert into AgentProdMap
					if (changeData != null) {
						listChangeData = (List<String>) changeData;
						System.out.println("  list agent prod map size ::::::         " + listChangeData.size());

						TLApprovalProductRate tlProductApprovalRate = new TLApprovalProductRate();
						tlProductApprovalRate.setMinAmt(listChangeData.get(0));
						tlProductApprovalRate.setMaxAmt(listChangeData.get(1));
						tlProductApprovalRate.setTsarVal(listChangeData.get(2));
						tlProductApprovalRate.setStampDuty(listChangeData.get(3));
						tlProductApprovalRate.setGstVal(listChangeData.get(4));
						tlProductApprovalRate.setDiscVal(listChangeData.get(5));
						tlProductApprovalRate.setMinAge(listChangeData.get(6));
						tlProductApprovalRate.setMaxAge(listChangeData.get(7));

						// Minimum Benefit Amount
						// discountVal gstVal stampDutyVal minAgeVal minAmntVal maxAmntVal tsarVal
						// maxAgeVal

						DspTlTblParamExample dspTlTblParamExample = new DspTlTblParamExample();
						DspTlTblParamExample.Criteria dspTlParam_criteria = dspTlTblParamExample.createCriteria();
						dspTlParam_criteria.andParamTypeEqualTo("pre_ins_cov_start");
						String minAmnt = tlProductApprovalRate.getMinAmt();
						System.out.println(minAmnt + "minAmnt");
						tlTblParam.setParamCode(minAmnt == null ? "0" : minAmnt);
						String formatVal = numberFormat(minAmnt);
						tlTblParam.setParamDesc("RM" + formatVal);
						tlTblParam.setParamDescEn("RM" + formatVal);
						int tlTblParamUpdate = dspTlTblParamMapper.updateByExampleSelective(tlTblParam,
								dspTlTblParamExample);
						System.out.println(tlTblParamUpdate + "min value");

						// Maximum Benefit Amount

						DspTlTblParamExample dspTlTblParamExample1 = new DspTlTblParamExample();
						DspTlTblParam tlTblParam1 = new DspTlTblParam();
						DspTlTblParamExample.Criteria dspTlParam_criteria1 = dspTlTblParamExample1.createCriteria();
						dspTlParam_criteria1.andParamTypeEqualTo("pre_ins_cov_end");

						String maxAmnt = tlProductApprovalRate.getMaxAmt();
						System.out.println(maxAmnt + "maxAmnt");
						tlTblParam1.setParamCode(maxAmnt == null ? "0" : maxAmnt);
						String formatVal1 = numberFormat(maxAmnt);
						tlTblParam1.setParamDesc("RM" + formatVal1);
						tlTblParam1.setParamDescEn("RM" + formatVal1);
						int tlTblParamUpdate1 = dspTlTblParamMapper.updateByExampleSelective(tlTblParam1,
								dspTlTblParamExample1);
						System.out.println(tlTblParamUpdate1 + "max value");

						// Total Sum Assured at Risk (TSAR)

						DspTlTblParamExample dspTlTblParamExample2 = new DspTlTblParamExample();
						DspTlTblParam tlTblParam2 = new DspTlTblParam();
						DspTlTblParamExample.Criteria dspTlParam_criteria2 = dspTlTblParamExample2.createCriteria();
						dspTlParam_criteria2.andParamTypeEqualTo("TL_tsarvalue");

						String tsarVal = tlProductApprovalRate.getTsarVal();
						System.out.println(tsarVal + "tsarVal");
						tlTblParam2.setParamCode(tsarVal == null ? "0" : tsarVal);
						String formatVal2 = numberFormat(tsarVal);
						tlTblParam2.setParamDesc("RM" + formatVal2);
						tlTblParam2.setParamDescEn("RM" + formatVal2);
						int tlTblParamUpdate2 = dspTlTblParamMapper.updateByExampleSelective(tlTblParam2,
								dspTlTblParamExample2);
						System.out.println(tlTblParamUpdate2 + "tsarVal value");

						// Stamp Duty

						DspTlTblParamExample dspTlTblParamExample3 = new DspTlTblParamExample();
						DspTlTblParam tlTblParam3 = new DspTlTblParam();
						DspTlTblParamExample.Criteria dspTlParam_criteria3 = dspTlTblParamExample3.createCriteria();
						dspTlParam_criteria3.andParamTypeEqualTo("TL_StampDuty");

						String stampDuty = tlProductApprovalRate.getStampDuty();
						System.out.println(stampDuty + "stampDuty");
						tlTblParam3.setParamCode(stampDuty == null ? "0" : stampDuty);
						String formatVal3 = numberFormat(stampDuty);
						tlTblParam3.setParamDesc("RM" + stampDuty);
						tlTblParam3.setParamDescEn("RM" + stampDuty);
						int tlTblParamUpdate3 = dspTlTblParamMapper.updateByExampleSelective(tlTblParam3,
								dspTlTblParamExample3);
						System.out.println(tlTblParamUpdate3 + "stampDuty value");

						// GST

						DspTlTblParamExample dspTlTblParamExample4 = new DspTlTblParamExample();
						DspTlTblParam tlTblParam4 = new DspTlTblParam();
						DspTlTblParamExample.Criteria dspTlParam_criteria4 = dspTlTblParamExample4.createCriteria();
						dspTlParam_criteria4.andParamTypeEqualTo("TL_GST");

						String gstVal = tlProductApprovalRate.getGstVal();
						System.out.println(gstVal + "gstVal");
						tlTblParam4.setParamCode(gstVal == null ? "0" : gstVal);
						String formatVal4 = numberFormat(gstVal);
						tlTblParam4.setParamDesc(gstVal + " %");
						tlTblParam4.setParamDescEn(gstVal + " %");
						int tlTblParamUpdate4 = dspTlTblParamMapper.updateByExampleSelective(tlTblParam4,
								dspTlTblParamExample4);
						System.out.println(tlTblParamUpdate4 + "gstVal value");

						// Direct Discount (Online)

						DspTlTblParamExample dspTlTblParamExample5 = new DspTlTblParamExample();
						DspTlTblParam tlTblParam5 = new DspTlTblParam();
						DspTlTblParamExample.Criteria dspTlParam_criteria5 = dspTlTblParamExample5.createCriteria();
						dspTlParam_criteria5.andParamTypeEqualTo("TL_Discount");

						String discVal = tlProductApprovalRate.getDiscVal();
						System.out.println(discVal + "discVal");
						tlTblParam5.setParamCode(discVal == null ? "0" : discVal);
						String formatVal5 = numberFormat(discVal);
						tlTblParam5.setParamDesc(discVal + " %");
						tlTblParam5.setParamDescEn(discVal + " %");
						int tlTblParamUpdate5 = dspTlTblParamMapper.updateByExampleSelective(tlTblParam5,
								dspTlTblParamExample5);
						System.out.println(tlTblParamUpdate5 + "discVal value");

						// Minimum Age Maximum Age
						DspTblRangeRulesExample dspTblRangeRulesExample = new DspTblRangeRulesExample();
						DspTblRangeRulesExample.Criteria dspTlRangeRules_criteria = dspTblRangeRulesExample
								.createCriteria();
						dspTlRangeRules_criteria.andRuleCodeEqualTo("DOBRange");
						dspTlRangeRules_criteria.andProductCodeEqualTo("TL");
						String minAge = tlProductApprovalRate.getMinAge();
						String maxAge = tlProductApprovalRate.getMaxAge();
						System.out.println(minAge + "minAgeVal  maxAgeVal" + maxAge);
						tblRangeRules.setRuleCodeMinValue(Integer.valueOf(minAge == null ? "0" : minAge));
						tblRangeRules.setRuleCodeMaxValue(Integer.valueOf(maxAge == null ? "0" : maxAge));
						int rangeRuleUpdate = dspTblRangeRulesMapper.updateByExampleSelective(tblRangeRules,
								dspTblRangeRulesExample);
						System.out.println(rangeRuleUpdate + "Updation of range rules");

						// Update status in DSP_AFM_TBL_Approval_Log table
						alog.setId(alogId);
						alog.setStatus("3");
						// alog.setChecker((short) 1); //get from the current login session
						alog.setChecker(Short.parseShort(logedUser));
						alog.setUpdateDate(new Date());
						int rs = approvalLogMapper.updateByPrimaryKeySelective(alog);

						if (rs == 1) {

							model.addAttribute("approvemessage", "Approved Data Successfully");
						}

						AgentController agentController = new AgentController(agentProfileMapper, productsMapper,
								agentProdMapMapper, commonQQMapper, adminParamMapper, agentDocumentMapper,
								agentLinkMapper, approvalLogMapper, approvalMapper);

						returnURL = agentController.getAgentProductMapApproval(request, response, model);

					} // end forchangeData
				} // end for listapprovallog
			} // Fot if aid
		} // Try end
		catch (Exception e) {
			System.out.println("Exception In >> TL product rates update done funtion " + e);
			model.addAttribute("approvemessage", "Approve Data Failed");
		}
		return returnURL;

	}

	@RequestMapping(value = "/rejectELSProductRateChangeData", method = RequestMethod.GET)
	public String rejectELSProductRateChangeData(@ModelAttribute AgentProfile agentProfile, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		String logedUser = (String) session.getAttribute("logedUser");
		int rs = 0;
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		ApprovalLogExample approvalLogexample = new ApprovalLogExample();
		ApprovalLog alog = new ApprovalLog();
		ApprovalLogExample.Criteria createCriteria = approvalLogexample.createCriteria();
		String aid = request.getParameter("approvalLogId");
		if (null != aid && !aid.isEmpty()) {
			Integer testint = new Integer(aid);
			createCriteria.andIdEqualTo(testint.shortValue());
			List<ApprovalLog> listapprovallog = approvalLogMapper.selectByExampleWithBLOBs(approvalLogexample);
			System.out.println("   list size in approval list  :::   " + listapprovallog.size());

			short alogId = listapprovallog.get(0).getId();
			alog.setId(alogId);
			alog.setStatus("2");
			// alog.setChecker((short) 1); //get from the current login session
			alog.setChecker(Short.parseShort(logedUser));
			alog.setUpdateDate(new Date());
			rs = approvalLogMapper.updateByPrimaryKeySelective(alog);
		}

		if (rs == 1) {

			model.addAttribute("rejectmessage", "Rejected Data Successfully");
		}

		else {

			model.addAttribute("rejectmessage", "Reject Data Failed");
		}

		AgentController agentController = new AgentController(agentProfileMapper, productsMapper, agentProdMapMapper,
				commonQQMapper, adminParamMapper, agentDocumentMapper, agentLinkMapper, approvalLogMapper,
				approvalMapper);

		String returnURL = agentController.getAgentProductMapApproval(request, response, model);
		;
		return returnURL;
	}

	/* Approval Product Rate Ends Here */
}
