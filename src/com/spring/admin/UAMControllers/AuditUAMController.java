package com.spring.admin.UAMControllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.mapper.uam.audit.AuditMenuMapper;
import com.spring.mapper.uam.audit.AuditRoleMapper;
import com.spring.mapper.uam.audit.AuditUserMapper;
import com.spring.uam.vo.audit.AuditMenu;
import com.spring.uam.vo.audit.AuditMenuExample;
import com.spring.uam.vo.audit.AuditRole;
import com.spring.uam.vo.audit.AuditRoleExample;
import com.spring.uam.vo.audit.AuditUser;
import com.spring.uam.vo.audit.AuditUserExample;

@Controller
@RequestMapping("uam")
public class AuditUAMController {

	AuditUserMapper auditUserMapper;
	AuditRoleMapper auditRoleMapper;
	AuditMenuMapper auditMenuMapper;

	@Autowired
	public AuditUAMController(AuditUserMapper auditUserMapper, AuditRoleMapper auditRoleMapper,
			AuditMenuMapper auditMenuMapper

	) {
		this.auditUserMapper = auditUserMapper;
		this.auditRoleMapper = auditRoleMapper;
		this.auditMenuMapper = auditMenuMapper;
	}

	/* User Audit Trail Starts Here */
	@RequestMapping(value = "/userAuditTrail")
	public String user(Model model) {

		return "auditUserManagement";
	}

	@RequestMapping(value = "/searchUserAudit")
	public String userAuditProfile(HttpServletRequest request, HttpServletResponse response, Model model)
			throws ParseException {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String message = "Session Has Been Expired";
			model.addAttribute("error", message);
			return "redirect:/";
		}

		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String fdate = request.getParameter("fromdate");
		Date date1 = null;
		Date date2 = null;
		if (null != fdate) {
			date1 = formatter.parse(fdate);
		}

		String ttdate = request.getParameter("todate");
		if (null != ttdate) {
			date2 = formatter.parse(ttdate);
		}

		AuditUserExample auditUserExample = new AuditUserExample();

		if (null != date1 && null != date2) {
			AuditUserExample.Criteria auditUserCriteria = auditUserExample.createCriteria();

			if (!request.getParameter("fromdate").isEmpty() && !request.getParameter("todate").isEmpty()) {
				// auditUserCriteria.andCreateDateBetween(date1, date2);
				// auditUserCriteria.andDateWhenBetween(date1, date2);

			}

		}

		AuditUserExample.Criteria auditUserCriteria = auditUserExample.createCriteria();
		auditUserCriteria.andGroupIdIsNotNull();

		List<AuditUser> auditUserList = new ArrayList<AuditUser>();
		auditUserExample.setOrderByClause("CREATE_DATE desc");
		auditUserList = auditUserMapper.selectByExample(auditUserExample);

		model.addAttribute("auditUserList", auditUserList);
		return "uam/auditUserManagement";
	}

	/* User Audit Trail Ends Here */

	/* Role Audit Trail Starts Here */

	@RequestMapping(value = "/roleAuditTrail")
	public String role(Model model) {

		return "auditRoleManagement";
	}

	@RequestMapping("/searchRoleAudit")
	public String roleAuditProfile(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String message = "Session Has Been Expired";
			model.addAttribute("error", message);
			return "redirect:/";
		}

		AuditRoleExample auditRoleExample = new AuditRoleExample();

		if (null != request.getParameter("fromdate") && null != request.getParameter("todate")) {
			AuditRoleExample.Criteria auditRoleCriteria = auditRoleExample.createCriteria();

			if (!request.getParameter("fromdate").isEmpty() && !request.getParameter("todate").isEmpty()) {
				auditRoleCriteria.andDateWhenBetween(request.getParameter("fromdate"), request.getParameter("todate"));

			}

		}

		List<AuditRole> auditRoleList = new ArrayList<AuditRole>();
		auditRoleList = auditRoleMapper.selectByExample(auditRoleExample);
		model.addAttribute("auditRoleList", auditRoleList);

		return "uam/auditRoleManagement";
	}

	/* Role Audit Trail Ends Here */

	/* Menu Audit Trail Starts Here */

	@RequestMapping(value = "/menuAuditTrail")
	public String menu(Model model) {

		return "auditMenuManagement";
	}

	@RequestMapping("/searchMenuAudit")
	public String menuAuditProfile(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		loginUser = "888";
		if (loginUser == null) {
			String message = "Session Has Been Expired";
			model.addAttribute("error", message);
			return "redirect:/";
		}

		AuditMenuExample auditMenuExample = new AuditMenuExample();

		if (null != request.getParameter("fromdate") && null != request.getParameter("todate")) {
			AuditMenuExample.Criteria auditMenuCriteria = auditMenuExample.createCriteria();

			if (!request.getParameter("fromdate").isEmpty() && !request.getParameter("todate").isEmpty()) {
				auditMenuCriteria.andDateWhenBetween(request.getParameter("fromdate"), request.getParameter("todate"));

			}

		}

		List<AuditMenu> auditMenuList = new ArrayList<AuditMenu>();
		auditMenuList = auditMenuMapper.selectByExample(auditMenuExample);
		model.addAttribute("auditMenuList", auditMenuList);

		return "uam/auditMenuManagement";
	}

	/* Menu Audit Trail Ends Here */
}
