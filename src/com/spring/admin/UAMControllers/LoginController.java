package com.spring.admin.UAMControllers;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.spring.mapper.uam.ModuleMapper;
import com.spring.mapper.uam.RoleModuleMapper;
import com.spring.mapper.uam.UserLogMapper;
import com.spring.mapper.uam.UserProfileMapper;
import com.spring.mapper.uam.UserRoleMapper;
import com.spring.service.MenuService;
import com.spring.uam.ldap.LDAPAttributesBean;
import com.spring.uam.ldap.LoginCPF;
import com.spring.uam.utils.SecurityUtil;
import com.spring.uam.vo.Module;
import com.spring.uam.vo.RoleModule;
import com.spring.uam.vo.RoleModuleExample;
import com.spring.uam.vo.UserLog;
import com.spring.uam.vo.UserProfile;
import com.spring.uam.vo.UserProfileExample;
import com.spring.uam.vo.UserRole;
import com.spring.uam.vo.UserRoleExample;
import com.spring.utils.sessionInterceptor.SessionInterceptor;

@Controller
public class LoginController {
	////////////////////////////////////////// login//////////////////////////////////////////////////////////////

	
	final static Logger logger = Logger.getLogger(LoginController.class);
	
	UserProfileMapper userProfileMapper;
	UserLogMapper userLogMapper;
	UserRoleMapper userRoleMapper;
	RoleModuleMapper roleModuleMapper;
	ModuleMapper moduleMapper;

	@Autowired
	MenuService menuService;

	@Autowired
	public LoginController(UserProfileMapper userProfileMapper, UserLogMapper userLogMapper,
			UserRoleMapper userRoleMapper, RoleModuleMapper roleModuleMapper, ModuleMapper moduleMapper) {
		this.userProfileMapper = userProfileMapper;
		this.userLogMapper = userLogMapper;
		this.userRoleMapper = userRoleMapper;
		this.roleModuleMapper = roleModuleMapper;
		this.moduleMapper = moduleMapper;
	}

	public LoginController() {
		// TODO Auto-generated constructor stub
	}

	@RequestMapping(value = "/")
	public String login(Model model) {
		return "uam/admin-login";
	}

	// ------------------------------------------------------------------------
	@RequestMapping(value = "/doLogin", method = RequestMethod.POST)
	public String doLogin(HttpServletRequest request, HttpServletResponse response, Model model) {
		final String userName = request.getParameter("username");
		final String pwd = request.getParameter("password");
		// With AD FOR PRODUCTION
	LDAPAttributesBean ldap = LoginCPF.authenticateUser(userName, pwd);
		// Without AD FOR UAT
		//LDAPAttributesBean ldap = new LDAPAttributesBean();
		//ldap.setValidated(1);  
			logger.info("ldap " + ldap.getValidated());
		if (ldap.getValidated() == 0) {
			unauthorizedUser(userName);
			String alertMessage = "Invalid Credentials. Please Verify PF No and Password";
			model.addAttribute("alertMessage", alertMessage);
			return "uam/admin-login";
		}

		HttpSession session = request.getSession();
		session.setAttribute("userId", null);
		session.setAttribute("id", null);
		session.setAttribute("user", null);
		session.setAttribute("userLogid", null);
		session.setAttribute("FullName", null);
		session.setAttribute("pfNumber", null);
		session.setAttribute("userloginName", null);
		session.setAttribute("lastLoginTime", null);
		session.setAttribute("userRole", String.valueOf(ldap.getUserrole()));
			logger.info("----------------After Clear Session------------------------- ");

		session.setAttribute("user", userName);
		session.setAttribute("password", pwd);
		session.setAttribute("pfnumber", userName);
		UserProfileExample user = new UserProfileExample();
		UserProfileExample.Criteria criteria = user.createCriteria();
		criteria.andPfNumberEqualTo((String) session.getAttribute("user"));
		criteria.andStatusEqualTo("1"); // active only
		List<UserProfile> userDetail = new ArrayList<UserProfile>();
		userDetail = userProfileMapper.selectByExample(user);

		String lastLoginTime = null;
		String userloginName = null;
		Short id = null;
		String name = null;
		String pfNumber = null;
		// if user not exist
		if (userDetail.size() <= 0 || userDetail.size() > 1) {
			unauthorizedUser(userName);
			String alertMessage = "Invalid Credentials. Please Verify PF No and Password";
			model.addAttribute("alertMessage", alertMessage);
			return "uam/admin-login";
		}

		UserProfile userProfile = new UserProfile();
		userProfile = userDetail.get(0);

		lastLoginTime = String.valueOf(userProfile.getLastLogin());
		userloginName = String.valueOf(userProfile.getName());
		logger.info("check login time :  " + lastLoginTime);
		logger.info("userloginName :  " + userloginName);
		id = userProfile.getId();
		name = userProfile.getName();
		pfNumber = userProfile.getPfNumber();
		BigDecimal logoutflag = userProfile.getLogoutFlag();

		long minutes = 0;
		if (userProfile.getLastLogin() != null) {
			long diff = new Date().getTime() - userProfile.getLastLogin().getTime();
			minutes = TimeUnit.MILLISECONDS.toMinutes(diff);
		}

	logger.info(" minutes, " + minutes);
		logger.info(logoutflag + "logoutflag");
		if (minutes > 5) {
			long flag = 0;
			userProfile.setLogoutFlag(BigDecimal.valueOf(flag));
			logoutflag = userProfile.getLogoutFlag();
		}

		// Check for Multiple Login
		if (logoutflag.intValue() != 0) {
			unauthorizedUser(userName);
			String alertMessage = "Multiple Login to same user is not allowed";
			model.addAttribute("alertMessage", alertMessage);
			return "uam/admin-login";
		}

		if (lastLoginTime.equals(null)) {
			logger.info("Login  Time If ");
			session.setAttribute("lastLoginTime", "First Time Login");
			session.setAttribute("userloginName", userloginName);
		} else {
			logger.info("Login  Time else ");
			session.setAttribute("lastLoginTime", lastLoginTime);
			session.setAttribute("userloginName", userloginName);
		}
		session.setAttribute("id", id);
		session.setAttribute("FullName", name);
		session.setAttribute("pfNumber", pfNumber);

		// Update Login Time
		UserProfile currentUser = new UserProfile();
		long loginflag = 1;
		logger.info("Before Id " + id);
		currentUser.setId((short) id);
		currentUser.setLastLogin(new Date());
		currentUser.setLogoutFlag(BigDecimal.valueOf(loginflag));
		userProfileMapper.updateByPrimaryKeySelective(currentUser);
		UserLog ulog = new UserLog();
		ulog.setUserId((short) id);
		ulog.setLoginTime(new Date());
		ulog.setLogDate(new Date());
		ulog.setFullName(name);
		ulog.setLoginFlag("0");
		userLogMapper.insert(ulog);
		// geting last Inserted Id from DSP_ADM_TBL_USER_LOG table
		int userLogid = ulog.getId();
		session.setAttribute("userLogid", userLogid);
		///////////// *************** to get Menu ********************************/
		List<RoleModule> roleModule_all = new ArrayList<RoleModule>();
		UserRoleExample example = new UserRoleExample();
		UserRoleExample.Criteria userRolecriteria = example.createCriteria();
		userRolecriteria.andStatusEqualTo("1");
		userRolecriteria.andUserIdEqualTo(id);
		session.setAttribute("userId", id);
		logger.info( "user id**"+id);
		List<UserRole> userRole = userRoleMapper.selectByExample(example);

		if (userRole.size() <= 0) {
			unauthorizedUser(userName);
			String alertMessage = "No Role Assigned to User";
			model.addAttribute("alertMessage", alertMessage);
			return "uam/admin-login";
		}

		for (int i = 0; i < userRole.size(); i++) {
			List<RoleModule> roleModule = new ArrayList<RoleModule>();
			RoleModuleExample roleModuleexample = new RoleModuleExample();
			RoleModuleExample.Criteria roleModulecriteria = roleModuleexample.createCriteria();
			short rmid = userRole.get(i).getRoleModuleId();
			roleModulecriteria.andStatusEqualTo("1");
			roleModulecriteria.andRoleIdEqualTo(rmid);
			Long roleNameCountList = roleModuleMapper.countByExample(roleModuleexample);
			logger.info("RoleNameCountList*** : " + roleNameCountList);
			logger.info("Get Role Module Id*** " + rmid);
			roleModule = roleModuleMapper.selectByExample(roleModuleexample);
			logger.info("roleModule size***: " + roleModule.size());
			for (int w = 0; w < roleModule.size(); w++)

			{
					logger.info("roleModule List " + roleModule);
					logger.info("w  =" + w);
				short mid = roleModule.get(w).getModuleId();
					logger.info("ModuleId**" +rmid);
					session.setAttribute("ModuleId", rmid);//setting moduleid
				Module mo = moduleMapper.selectByPrimaryKey(mid);

				roleModule.get(w).setModuleName(mo.getName());
				roleModule.get(w).setModuleURL(mo.getUrl());
					logger.info("Module Name ***:" + mo.getName());
					logger.info("Module Url*** : " + mo.getUrl());
					logger.info("roleModule_all size ***: " + roleModule_all.size());
			}
			session.setAttribute("rolemodule", roleModule);
				logger.info("dhana printing the role module:"+roleModule);
			roleModule_all.addAll(roleModule);
		}
		if (roleModule_all != null) {
			if (roleModule_all.size() > 0) {
				List<RoleModule> noRepeat = new ArrayList<RoleModule>();
				for (RoleModule event : roleModule_all) {
					boolean isFound = false;
					for (RoleModule e : noRepeat) {
						if (e.getModuleName().trim().equalsIgnoreCase(event.getModuleName().trim())) {
							isFound = true;
							break;
						}
					}
					if (!isFound) {
						noRepeat.add(event);
					}
				}
				model.addAttribute("roleModule_all", noRepeat);
				session.setAttribute("roleModule_all", noRepeat);
				logger.info("noRepeat:" +noRepeat);
				logger.info("roleModule_all**"+roleModule_all);
			}
		}
		return "uam/moduleList";
	}

	// ------------------------------------------------------------------------------------------------------
	@RequestMapping(value = "/doLogout", method = RequestMethod.GET)
	public String doLogout(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		try{
		// tulASI part
		if (session.getAttribute("userId") != null) {
			String userLogId = session.getAttribute("userId").toString();
				logger.info("userLogId " + userLogId);
			if (!userLogId.isEmpty()) {
				long flag = 0;
				UserProfile currentUser1 = new UserProfile();
				currentUser1.setId(Short.valueOf(userLogId));
				currentUser1.setLastLogout(new Date());
				currentUser1.setLogoutFlag(BigDecimal.valueOf(flag));
				userProfileMapper.updateByPrimaryKeySelective(currentUser1);
			}
		}
		if(session != null && !session.equals("")){
				logger.info(session+"session checking function");
		if (session.getAttribute("userId") != null) {
			short id = Short.parseShort(session.getAttribute("userId").toString());
			if (session.getAttribute("userLogid") != null) {
				String userLogId = session.getAttribute("userLogid").toString();
				UserLog currentUser = new UserLog();
				currentUser.setLogoutTime(new Date());
				currentUser.setId(Short.parseShort(userLogId));
				userLogMapper.updateByUserLogId(currentUser);
			}
			session.setAttribute("userId", null);
			session.setAttribute("id", null);
			session.setAttribute("user", null);
			session.setAttribute("userLogid", null);
			session.setAttribute("FullName", null);
			session.setAttribute("pfNumber", null);
			session.setAttribute("userloginName", null);
			session.setAttribute("lastLoginTime", null);

			session.invalidate();
		}
		}
		}catch(Exception e){
				logger.info("session expired");
		}
		return "uam/admin-login";
	}

	// -------------------------------------------------------------------------------------------
	@RequestMapping(value = "/submitModule", method = RequestMethod.GET)
	public String submitModule(HttpServletRequest request, HttpServletResponse response, Model model)
			 {
		HttpSession session = request.getSession();
		String param = request.getParameter("operation");
			logger.info("param: " + param);
			logger.info("messhelloage");
		if (param == null) {
			return "redirect:/errorPage";
		}
		if (param != null) {
			String params[] = param.split("\\$");
			if (params.length > 1) {
				session.setAttribute("param", param);
					logger.info("dhana printing from logiccontroller:param"+param);
					logger.info(params[0]);
				int rmid = Integer.parseInt(params[0]);
				//session.setAttribute("moduleId", module_id);
				session.setAttribute("ModuleId", rmid);
				//session.setAttribute("moduleId",rmid);
				
				
					logger.info("dhana printing from logiccontroller:module_id"+rmid);
					logger.info("dhana printing from logiccontroller:module_id"+rmid);
				String url = params[1];
					logger.info("url Test : "+url);
				Map userMenus = new HashMap();
				int userId = 0;
				//////////// ********* GET ROLES BASED ON USER ID AND MODULE ID
				//////////// **********************//
				if (session.getAttribute("userId") != null) {
					userId = (Short) session.getAttribute("userId");
				}
				List<RoleModule> userRole = new ArrayList<RoleModule>();
				String roles = "";
				//userRole = menuService.getRoleListByUser(userId, module_id);rmid
				userRole = menuService.getRoleListByUser(userId, rmid);
					logger.info("userRole        :" + userRole);
				for (int i = 0; i < userRole.size(); i++) {
					roles = roles + userRole.get(i).getRoleId().toString() + ",";
					session.setAttribute("userRole", userRole.get(0).getRoleId().toString());
						logger.info("roles    :" + roles);
				}
				
				  for (int i = 0; i < userRole.size(); i++) { roles = roles +
				  userRole.get(i).getRoleId().toString() + ",";
				  	logger.info("roles  +%%%%%%%%%%%%%%%%%  :"+roles); }
				
				if (roles != null) {
					if (roles.substring(roles.length() - 1).equals(",")) {
						roles = roles.substring(0, roles.length() - 1);
					}
				}
				String userLoginid = "";
				if (session.getAttribute("userLogid") != null) {
					userLoginid = session.getAttribute("userLogid").toString();
				}
				//String passTextURL = userId + "$" + roles + "$" + module_id + "$" + userLoginid;//
				String passTextURL = userId + "$" + roles + "$" + rmid + "$" + userLoginid;
					logger.info(passTextURL + ":before encription and decription");
				String encript = SecurityUtil.enciptString(passTextURL);
					logger.info(encript+":after encription");
				 String decript=SecurityUtil.decriptString(encript);
				 	logger.info(decript+":after decription");
				return "redirect:" + url + "?id=" + encript;
			}

		}
		return "uam/success";
	}

	@RequestMapping(value = "/modulelist", method = RequestMethod.GET)
	public String modulelist(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		if (session.getAttribute("roleModule_all") != null) {
				logger.info(session.getAttribute("userLogid") + ":before encription and decription");
			return "uam/moduleList";
		}
		return "uam/admin-login";
	}
	private void unauthorizedUser(String username) {
		String flag = "1";
		UserLog ulog = new UserLog();
		ulog.setLoginTime(new Date());
		ulog.setLogDate(new Date());
		ulog.setFullName(username);
		ulog.setLoginFlag(flag);
		userLogMapper.insertfalseLogin(ulog);
	}

}
