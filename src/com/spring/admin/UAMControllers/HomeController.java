package com.spring.admin.UAMControllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	/*
	 * @RequestMapping(value = "/", method = RequestMethod.GET) =======
	 *//*
		 * @RequestMapping(value = "/", method = RequestMethod.GET) >>>>>>>
		 * refs/heads/dynamicMenu public String home(Locale locale, Model model) {
		 * logger.info("Welcome home! The client locale is {}.", locale);
		 *
		 * Date date = new Date(); DateFormat dateFormat =
		 * DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		 *
		 * String formattedDate = dateFormat.format(date);
		 *
		 * model.addAttribute("serverTime", formattedDate );
		 *
		 * return "home"; <<<<<<< HEAD }
		 */

}
