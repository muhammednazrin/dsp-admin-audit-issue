package com.spring.admin.UAMControllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spring.VO.AdminParam;
import com.spring.VO.AdminParamExample;
import com.spring.mapper.AdminParamMapper;
import com.spring.mapper.uam.MenuMapper;
import com.spring.mapper.uam.ModuleMapper;
import com.spring.mapper.uam.ModulePrmssnMapper;
import com.spring.mapper.uam.PrmssnMapper;
import com.spring.mapper.uam.RoleMapper;
import com.spring.mapper.uam.RoleMenuMapper;
import com.spring.mapper.uam.RoleModuleMapper;
import com.spring.mapper.uam.UserGroupMapper;
import com.spring.mapper.uam.UserLogMapper;
import com.spring.mapper.uam.UserProfileMapper;
import com.spring.mapper.uam.UserRoleMapper;
import com.spring.service.MenuService;
import com.spring.uam.common.pojo.RegisterEmailVo;
import com.spring.uam.dao.UserProfileDaoImpl;
import com.spring.uam.dao.UserValidation_DAO;
import com.spring.uam.ldap.LDAPAttributesBean;
import com.spring.uam.ldap.RegCPF;
import com.spring.uam.utils.AdvancedEncryptionStandard;
import com.spring.uam.utils.ServiceValidationUtils;
import com.spring.uam.vo.CustomUserPojo;
import com.spring.uam.vo.Menu;
import com.spring.uam.vo.MenuExample;
import com.spring.uam.vo.Module;
import com.spring.uam.vo.ModuleExample;
import com.spring.uam.vo.ModulePrmssn;
import com.spring.uam.vo.ModulePrmssnExample;
import com.spring.uam.vo.Role;
import com.spring.uam.vo.RoleExample;
import com.spring.uam.vo.RoleMenu;
import com.spring.uam.vo.RoleMenuExample;
import com.spring.uam.vo.RoleModule;
import com.spring.uam.vo.RoleModuleExample;
import com.spring.uam.vo.UserGroup;
import com.spring.uam.vo.UserGroupModule;
import com.spring.uam.vo.UserLog;
import com.spring.uam.vo.UserLogExample;
import com.spring.uam.vo.UserProfile;
import com.spring.uam.vo.UserProfileExample;
import com.spring.uam.vo.UserRole;
import com.spring.uam.vo.UserRoleExample;

@Controller
@RequestMapping("uam")
public class UserController {
	UserProfileMapper userProfileMapper;
	UserRoleMapper userRoleMapper;
	UserLogMapper userLogMapper;
	ModuleMapper moduleMapper;
	PrmssnMapper prmssnMapper;
	UserGroupMapper userGroupMapper;
	RoleMapper roleMapper;
	ModulePrmssnMapper modulePrmssnMapper;
	AdminParamMapper adminParamMapper;
	MenuMapper menuMapper;
	RoleModuleMapper roleModuleMapper;
	RoleMenuMapper roleMenuMapper;

	@Autowired
	MenuService menuService;

	@Autowired
	public UserController(UserProfileMapper userProfileMapper, UserRoleMapper userRoleMapper,
			UserLogMapper userLogMapper, ModuleMapper moduleMapper, PrmssnMapper prmssnMapper, RoleMapper roleMapper,
			ModulePrmssnMapper modulePrmssnMapper, AdminParamMapper adminParamMapper, MenuMapper menuMapper,
			RoleModuleMapper roleModuleMapper, RoleMenuMapper roleMenuMapper, UserGroupMapper userGroupMapper) {
		this.userProfileMapper = userProfileMapper;
		this.userRoleMapper = userRoleMapper;
		this.moduleMapper = moduleMapper;
		this.prmssnMapper = prmssnMapper;
		this.roleMapper = roleMapper;
		this.modulePrmssnMapper = modulePrmssnMapper;
		this.adminParamMapper = adminParamMapper;
		this.menuMapper = menuMapper;
		this.roleModuleMapper = roleModuleMapper;
		this.roleMenuMapper = roleMenuMapper;
		this.userGroupMapper = userGroupMapper;
		this.userLogMapper = userLogMapper;
	}

	/*************************************************************************************************************************/

	// searchLogin Management List Audit Pramaiyan
	@RequestMapping(value = "/getAuditLoginManagement")
	public String searchLoginAudit(@ModelAttribute UserLog userLog, HttpServletRequest request,
			HttpServletResponse response, Model model, BindingResult result) throws ParseException {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String message = "Session Has Been Expired";
			model.addAttribute("error", message);
			return "redirect:/";
		}
		UserLogExample UserLogExample = new UserLogExample();
		UserLogExample.Criteria auditLoginCriteria = UserLogExample.createCriteria();
		// System.out.println("out From " + request.getParameter("fromdate"));
		// System.out.println("out TO " + request.getParameter("todate"));
		// String to date conversion
		if (null != request.getParameter("fromdate") && null != request.getParameter("todate")) {
			if (!request.getParameter("fromdate").isEmpty() && !request.getParameter("todate").isEmpty()) {
				// System.out.println("From " + request.getParameter("fromdate"));
				// System.out.println("TO " + request.getParameter("todate"));
				DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				String fromDate = request.getParameter("fromdate");
				String toDate = request.getParameter("todate");
				String str_fromDate = fromDate + " 00:00:00";
				String str_toDate = toDate + " 23:59:59";
				Date date1 = formatter.parse(str_fromDate);
				Date date2 = formatter.parse(str_toDate);
				// System.out.println("Empty Checking ");
				auditLoginCriteria.andLogDateBetween(date1, date2);
			}
		}
		if (null != request.getParameter("pfNumber") && !request.getParameter("pfNumber").isEmpty()) {
			int userid = 0;
			UserProfileExample user = new UserProfileExample();
			UserProfileExample.Criteria criteria = user.createCriteria();
			criteria.andPfNumberEqualTo(request.getParameter("pfNumber"));
			List<UserProfile> UserDetail = new ArrayList<UserProfile>();
			UserDetail = userProfileMapper.selectByExample(user);
			if (UserDetail.size() > 0) {
				userid = UserDetail.get(0).getId();
			}
			auditLoginCriteria.andUserIdEqualTo((short) userid);
		}
		List<UserLog> auditUserLogList = new ArrayList<UserLog>();
		List<String> userName = new ArrayList<String>();
		auditUserLogList = userLogMapper.selectAuditLoginDetails(UserLogExample);
		model.addAttribute("auditUserLogList", auditUserLogList);
		// System.out.println("auditUserLogList>>>>>>>>" + auditUserLogList);
		return "uam/auditLoginManagement";
	}

	////////////////////////////////////////// dashboard/////////////////////////////////////////////////////////////////
	@RequestMapping("/dashboard")
	public String dashboard(HttpServletRequest request, HttpServletResponse response, Model model) throws SQLException {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String message = "Session Has Been Expired";
			model.addAttribute("error", message);
			return "redirect:/";
		}
		/// *************** to get dynamic
		/// menu********************************////////////
		String param = (String) session.getAttribute("param");

		if ("".equalsIgnoreCase(param) || ServiceValidationUtils.isEmptyStringTrim(param)) {
			System.out.println("Param value is empty --> logout");
			// return doLogout(request, response, model);
		}

		ArrayList<Menu> menu_first = new ArrayList<Menu>();
		ArrayList<Menu> menu_second = new ArrayList<Menu>();
		ArrayList<Menu> menu_third = new ArrayList<Menu>();
		ArrayList<Menu> menu_fourth = new ArrayList<Menu>();
		Map userMenus = new HashMap();
		int role_id = 0;
		int module_id = 0;
		int userId = 0;
		//////////// ********* GET ROLES BASED ON USER ID AND MODULE ID
		//////////// **********************//
		if (param != null) {
			if (param != null) {
				String params[] = param.split("\\$");
				if (params.length > 0) {
					module_id = Integer.parseInt(params[0]);
				}
			}
		}

		session.setAttribute("moduleId", module_id);
		if (session.getAttribute("userId") != null) {
			userId = (Short) session.getAttribute("userId");
		}
		List<RoleModule> userRole = new ArrayList<RoleModule>();
		String roles = "";
		userRole = menuService.getRoleListByUser(userId, module_id);
		for (int i = 0; i < userRole.size(); i++) {
			roles = roles + userRole.get(i).getRoleId().toString() + ",";
		}
		System.out.println(roles + "roles");
		if (roles.substring(roles.length() - 1).equals(",")) {
			roles = roles.substring(0, roles.length() - 1);
		}
		// userMenus=getMenuList("1",module_id);
		userMenus = menuService.getMenuList(roles, module_id);
		menu_first = (ArrayList<Menu>) userMenus.get("first");
		menu_second = (ArrayList<Menu>) userMenus.get("second");
		menu_third = (ArrayList<Menu>) userMenus.get("third");
		menu_fourth = (ArrayList<Menu>) userMenus.get("fourth");
		session.setAttribute("level_1", menu_first);
		session.setAttribute("level_2", menu_second);
		session.setAttribute("level_3", menu_third);
		session.setAttribute("level_4", menu_fourth);
		System.out.println("level_1" + menu_first);
		System.out.println("level_2" + menu_second);
		System.out.println("level_3" + menu_third);
		System.out.println("level_4" + menu_fourth);
		if (menu_first != null) {
			System.out.println("size1 " + menu_first.size());
		}
		if (menu_second != null) {
			System.out.println("size2 " + menu_second.size());
		}
		if (menu_third != null) {
			System.out.println("size3 " + menu_third.size());
		}
		if (menu_fourth != null) {
			System.out.println("size4 " + menu_fourth.size());
		}

		return "uam/admin-dashboard";
	}

	////////////////////////////////////////// user roles
	////////////////////////////////////////// profile////////////////////////////////////////////////////
	@RequestMapping("/userRole")
	public String userRoleProfile(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String message = "Session Has Been Expired";
			model.addAttribute("error", message);
			return "redirect:/";
		}
		// Status List
		AdminParamExample statusList = new AdminParamExample();
		AdminParamExample.Criteria statusListCriteria = statusList.createCriteria();
		statusListCriteria.andParamTypeEqualTo("STATUS");
		List<AdminParam> StatusList = new ArrayList<AdminParam>();
		StatusList = adminParamMapper.selectByExample(statusList);
		// Module List - active only : to display in create user role form
		ModuleExample moduleList = new ModuleExample();
		ModuleExample.Criteria statusModuleCriteria = moduleList.createCriteria();
		statusModuleCriteria.andStatusEqualTo("1");
		moduleList.setOrderByClause("name");
		List<Module> ModuleList = new ArrayList<Module>();
		ModuleList = moduleMapper.selectByExample(moduleList);
		// Role List - active only : to display in create user role form
		RoleExample roleList = new RoleExample();
		RoleExample.Criteria statusRoleCriteria = roleList.createCriteria();
		statusRoleCriteria.andStatusEqualTo("1");
		roleList.setOrderByClause("name");
		List<Role> RoleList = new ArrayList<Role>();
		RoleList = roleMapper.selectByExample(roleList);
		// getting User Role list All for search crietria
		RoleModuleExample roleModuleExample = new RoleModuleExample();
		RoleModuleExample.Criteria roleModuleExampleCriteria = roleModuleExample.createCriteria();
		List<RoleModule> RoleModuleList = new ArrayList<RoleModule>();
		if (null != request.getParameter("module") && !request.getParameter("module").isEmpty()) { // module_id
																									// in
																									// DSP_ADM_TBL_ROLE_MODULE
			roleModuleExampleCriteria.andModuleIdEqualTo((short) Integer.parseInt(request.getParameter("module")));
		}
		if (null != request.getParameter("role") && !request.getParameter("role").isEmpty()) { // id
																								// in
																								// DSP_ADM_TBL_ROLE_MODULE
			roleModuleExampleCriteria.andRoleIdEqualTo((short) Integer.parseInt(request.getParameter("role")));
		}
		if (null != request.getParameter("status") && !request.getParameter("status").isEmpty()) // status
																									// in
																									// DSP_ADM_TBL_ROLE_MODULE
		{
			roleModuleExampleCriteria.andStatusEqualTo(request.getParameter("status"));
		}
		System.out.println("Module Id>>>>> " + request.getParameter("module"));
		System.out.println("Role Id >>>> " + request.getParameter("role"));
		System.out.println("Status  >>>" + request.getParameter("status"));
		RoleModuleList = roleModuleMapper.getRoleModuleByCriteria(roleModuleExample);
		System.out.println("RoleModuleList123" + RoleModuleList);
		model.addAttribute("RoleModuleList", RoleModuleList);
		model.addAttribute("StatusList", StatusList);
		model.addAttribute("ModuleList", ModuleList);
		model.addAttribute("RoleList", RoleList);
		return "uam/admin-system-group-profile";
	}

	@RequestMapping("/userProfile")
	public String userProfile(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String message = "Session Has Been Expired";
			model.addAttribute("error", message);
			return "redirect:/";
		}
		System.out.println("userprofile method start");
		List<CustomUserPojo> UserList = new ArrayList<CustomUserPojo>();
		UserProfileDaoImpl userProfileDao = new UserProfileDaoImpl();
		String name = "";
		String status = "";
		String pfnumber = "";
		String rolemodule = "";
		if (request.getParameter("pfNumber") != null) {
			pfnumber = request.getParameter("pfNumber");
		}
		if (request.getParameter("status") != null) {
			status = request.getParameter("status");
		}
		if (request.getParameter("name") != null) {
			name = request.getParameter("name");
		}
		if (request.getParameter("selectroleID") != null) {
			rolemodule = request.getParameter("selectroleID");
		}
		try {
			UserList = userProfileDao.getSearchUserProfile(name, pfnumber, status, rolemodule);
			System.out.println("UserListByRole");
			if (!UserList.isEmpty()) {
				System.out.println("UserListByRole length " + UserList.size());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("UserList" + UserList);
		// Status List
		AdminParamExample statusList = new AdminParamExample();
		AdminParamExample.Criteria statusListCriteria = statusList.createCriteria();
		statusListCriteria.andParamTypeEqualTo("STATUS");
		List<AdminParam> StatusList = new ArrayList<AdminParam>();
		StatusList = adminParamMapper.selectByExample(statusList);
		model.addAttribute("StatusList", StatusList);
		List<CustomUserPojo> RoleModuleList = new ArrayList<CustomUserPojo>();
		try {
			RoleModuleList = userProfileDao.getRoleModuleList();
			// System.out.println("roles list after dao" +RoleModuleList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.addAttribute("UserList", UserList);
		model.addAttribute("StatusList", StatusList);
		model.addAttribute("UserRoleSearch", RoleModuleList);
		return "uam/admin-system-user-profile";
	}

	// Start user Role Management by pramaiyan
	// create User Role on 05032018
	@RequestMapping("/createRole")
	public String createRole(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String message = "Session Has Been Expired";
			model.addAttribute("error", message);
			return "redirect:/";
		}
		// Role List - active only : to display in create user role form
		RoleExample roleList = new RoleExample();
		RoleExample.Criteria statusRoleCriteria = roleList.createCriteria();
		statusRoleCriteria.andStatusEqualTo("1");
		roleList.setOrderByClause("name");
		List<Role> RoleList = new ArrayList<Role>();
		RoleList = roleMapper.selectByExample(roleList);
		model.addAttribute("RoleList", RoleList);
		return "uam/admin-system-role-create";
	}

	// create Role Name Action on 05032018
	@RequestMapping("/createRoleNameAction")
	public String createRoleAction(@ModelAttribute Role role, HttpServletRequest request, HttpServletResponse response,
			Model model, BindingResult result) {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String message = "Session Has Been Expired";
			model.addAttribute("error", message);
			return "redirect:/";
		}
		int rs = 0;
		String rolenNameReq = request.getParameter("roleName");
		System.out.println("rolenNameReq" + rolenNameReq);
		// Find Duplicate Role Name Id exist or not in Role table
		RoleExample roleaNameCountExample = new RoleExample();
		RoleExample.Criteria roleCountCriteria = roleaNameCountExample.createCriteria();
		roleCountCriteria.andNameEqualTo(rolenNameReq.toUpperCase());
		Long roleNameCountList = roleMapper.countByExample(roleaNameCountExample);
		System.out.println("RoleNameCountList : " + roleNameCountList);
		if (roleNameCountList == 0) {
			role.setName(rolenNameReq.toUpperCase().trim());
			role.setStatus("1");
			role.setCreateBy(loginUser);
			role.setCreateDate(new Date());
			// insert Rolename into Role table
			rs = roleMapper.insert(role);
		}
		if (rs == 1) {
			model.addAttribute("savemessage", " Inserted Data Successfully");
		} else {
			model.addAttribute("savemessage", " Insert Data Failed");
		}
		return createRole(request, null, model);
	}

	// validateCreateRoleName by pramaiyan on 13032018 //validateCreateRoleName
	@RequestMapping(value = "/validateCreateRoleName", method = RequestMethod.POST)
	@ResponseBody
	public void validateRoleName(HttpServletResponse response, HttpServletRequest request) {
		long roleNameCountList = 0;
		String rolenNameReq = request.getParameter("roleName");
		System.out.println("Calling RoleName Validation");
		;
		System.out.println("rolenNameReq" + rolenNameReq);
		// if (!rolenNameReq.trim().isEmpty() && !rolenNameReq.trim().isEmpty())
		// {
		if (!rolenNameReq.trim().isEmpty()) {
			// Find Duplicate Role Id and Module Id exist or not in RoleModule
			// table
			// Find Duplicate Role Name Id exist or not in Role table
			RoleExample roleaNameCountExample = new RoleExample();
			RoleExample.Criteria roleCountCriteria = roleaNameCountExample.createCriteria();
			roleCountCriteria.andNameEqualTo(rolenNameReq.toUpperCase());
			roleNameCountList = roleMapper.countByExample(roleaNameCountExample);
			System.out.println("RoleNameCountList : " + roleNameCountList);
		} else {
			roleNameCountList = -1;
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		// create Json Object
		JSONObject json = new JSONObject();
		try {
			json.put("count", roleNameCountList);
			if (roleNameCountList == 0) {
				json.put("recordFound", false);
			} else {
				json.put("recordFound", true);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// finally output the json string
		out.print(json.toString());
		System.out.println("json.toString()" + json.toString());
	}

	// Create Create Role Access Controls by pramaiyan
	@RequestMapping(value = "/createUserRole")
	public String userGroupProfile(@ModelAttribute RoleModule roleModule, HttpServletResponse response,
			HttpServletRequest request, Model model, BindingResult result) {
		// System.out.println("Calling createUserRole ");
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		int rs = 0;
		if (loginUser == null) {
			String message = "Session Has Been Expired";
			model.addAttribute("error", message);
			return "redirect:/";
		}
		String moduleId = request.getParameter("moduleDesc");
		String role = request.getParameter("role");
		roleModule.setModuleId(Short.parseShort(moduleId));
		roleModule.setRoleId(Short.parseShort(role));
		roleModule.setStatus("1");
		roleModule.setCreateDate(new Date());
		roleModule.setUpdateDate(new Date());
		roleModule.setCreateBy(loginUser);
		// Find Duplicate Role Id and Module Id exist or not in RoleModule table
		RoleModuleExample roleandModuleIdCountExample = new RoleModuleExample();
		RoleModuleExample.Criteria roleModuleCountCriteria = roleandModuleIdCountExample.createCriteria();
		roleModuleCountCriteria.andModuleIdEqualTo(Short.parseShort(moduleId));
		roleModuleCountCriteria.andRoleIdEqualTo(Short.parseShort(role));
		Long roleModuleIdCount = roleModuleMapper.countByExample(roleandModuleIdCountExample);
		System.out.println("roleModuleIdCount : " + roleModuleIdCount);
		if (roleModuleIdCount == 0) {
			// insert RoleId and ModuleId in RoleModule table
			rs = roleModuleMapper.insert(roleModule);
			if (rs == 1) {
				model.addAttribute("savemessage", " Inserted Data Successfully");
			} else {
				model.addAttribute("savemessage", " Insert Data Failed");
			}
		} else {
			return "redirect:/userRole";
		}
		// geting last Inserted row Id from RoleModule table
		int rolemoduleid = roleModule.getId();
		System.out.println("role module id---------->" + rolemoduleid);
		System.out.println("rs---------->" + rs);
		// model.addAttribute("id", rolemoduleid);
		roleModule.setRoleModuleId(Integer.toString(rolemoduleid));
		// return "redirect:/updateUserRole";
		return updateUserRole(roleModule, null, response, request, model, result);
	}

	// Start Validation by pramaiyan
	@RequestMapping(value = "/validateCreateRoleModule", method = RequestMethod.POST)
	@ResponseBody
	public void validateCreateGroup(HttpServletResponse response, HttpServletRequest request) {
		long roleModuleIdCount = 0;
		String moduleId = request.getParameter("moduleDesc");
		String role = request.getParameter("role");
		System.out.println("Calling  validateCreateGroup ");
		if (!moduleId.trim().isEmpty() && !moduleId.trim().isEmpty()) {
			// Find Duplicate Role Id and Module Id exist or not in RoleModule
			// table
			RoleModuleExample roleandModuleIdCountExample = new RoleModuleExample();
			RoleModuleExample.Criteria roleModuleCountCriteria = roleandModuleIdCountExample.createCriteria();
			roleModuleCountCriteria.andModuleIdEqualTo(Short.parseShort(moduleId));
			roleModuleCountCriteria.andRoleIdEqualTo(Short.parseShort(role));
			roleModuleIdCount = roleModuleMapper.countByExample(roleandModuleIdCountExample);
		} else {
			roleModuleIdCount = -1;
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// create Json Object
		JSONObject json = new JSONObject();
		// put some value pairs into the JSON object .
		try {
			json.put("count", roleModuleIdCount);
			if (moduleId.isEmpty()) {
				json.put("requiredModule", true);
			} else {
				json.put("requiredModule", false);
			}
			if (role.isEmpty()) {
				json.put("requiredRole", true);
			} else {
				json.put("requiredRole", false);
			}
			if (roleModuleIdCount == 0) {
				json.put("recordFound", false);
			} else {
				json.put("recordFound", true);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// finally output the json string
		out.print(json.toString());
		System.out.println("json.toString()" + json.toString());
	}

	// updateUserRole by Pramaiyan fetching data
	@RequestMapping(value = "/updateUserRole", method = RequestMethod.GET)
	public String updateUserRole(@ModelAttribute RoleModule roleModule, @ModelAttribute Module module,
			HttpServletResponse response, HttpServletRequest request, Model model, BindingResult result) {
		// System.out.println("Calling updateUserRole ");
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String message = "Session Has Been Expired";
			model.addAttribute("error", message);
			return "redirect:/";
		}
		String reqID = request.getParameter("id"); // hidden value from
													// admin-system-group-profile.jsp
		if (reqID == null) {
			reqID = roleModule.getRoleModuleId();
		}
		System.out.println("Request ID-->" + reqID);
		RoleModuleExample roleModuleExample = new RoleModuleExample();
		RoleModuleExample.Criteria roleModuleIdriteria = roleModuleExample.createCriteria();
		roleModuleIdriteria.andIdEqualTo(Short.parseShort(reqID));
		List<RoleModule> roleModuleList = new ArrayList<RoleModule>();
		roleModuleList = roleModuleMapper.selectByExample2(roleModuleExample);
		// System.out.println("RoleModuleList123 : "+roleModuleList.size());
		// System.out.println("RoleModuleList1234: "+roleModuleList);
		short roleId = roleModuleList.get(0).getRoleId(); // hidden value from
															// admin-system-group-profile-update.jsp
		short moduleId = roleModuleList.get(0).getModuleId(); // hidden value
																// from
																// admin-system-group-profile-update.jsp
		System.out.println("roleId >>>>>>>>>>>>" + roleId);
		// create a list based on getModuleId and getRoleId for check box
		// checking in jsp

		RoleMenuExample roleMenuExample = new RoleMenuExample();
		RoleMenuExample.Criteria roleMenuIdriteria = roleMenuExample.createCriteria();
		roleMenuIdriteria.andModuleIdEqualTo(moduleId);
		roleMenuIdriteria.andRoleIdEqualTo(roleId);
		List<RoleMenu> roleMenuList = new ArrayList<RoleMenu>();
		roleMenuList = roleMenuMapper.selectByExample(roleMenuExample);
		JSONObject json = new JSONObject();
		// put some value pairs into the JSON object .
		try {

			for (int i = 0; i < roleMenuList.size(); i++) {
				System.out.println("Get Menuid and PrmssnId   >>>>>>>>  : " + roleMenuList.get(i).getMenuId().toString()
						+ ":::" + roleMenuList.get(i).getPrmssnId().toString());
				json.put(roleMenuList.get(i).getMenuId().toString(), roleMenuList.get(i).getPrmssnId().toString());
			}
			System.out.println("(json.toString());" + json.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*
		 * for(int i=0; i<roleMenuList.size();i++) {
		 * System.out.println("PrmssnId>>>>>>>>>>"+roleMenuList.get(i). getPrmssnId());
		 * System.out.println("MenuId>>>>>>>>>>"+roleMenuList.get(i).getMenuId() ); }
		 */
		ModulePrmssnExample mdulePrmssnExample = new ModulePrmssnExample();
		ModulePrmssnExample.Criteria mdulePrmssCriteria = mdulePrmssnExample.createCriteria();
		mdulePrmssCriteria.andModuleIdEqualTo(moduleId);
		mdulePrmssCriteria.andStatusEqualTo("1");
		List<ModulePrmssn> modulePrmssnList = new ArrayList<ModulePrmssn>();
		modulePrmssnList = modulePrmssnMapper.selectPermissionAccess(mdulePrmssnExample);
		// Status List
		AdminParamExample statusList = new AdminParamExample();
		AdminParamExample.Criteria statusListCriteria = statusList.createCriteria();
		statusListCriteria.andParamTypeEqualTo("STATUS");
		List<AdminParam> StatusList = new ArrayList<AdminParam>();
		StatusList = adminParamMapper.selectByExample(statusList);
		// get menu items
		Map<String, Object> results = new HashMap<String, Object>();
		List<Menu> menuListLevel1 = new ArrayList<Menu>();
		List<Menu> menuListLevel2 = new ArrayList<Menu>();
		List<Menu> menuListLevel3 = new ArrayList<Menu>();
		List<Menu> menuListLevel4 = new ArrayList<Menu>();
		List<BigDecimal> idListFirstLevel = new ArrayList<BigDecimal>();
		List<BigDecimal> idListSecondLevel = new ArrayList<BigDecimal>();
		List<BigDecimal> idListThirdLevel = new ArrayList<BigDecimal>();
		MenuExample menuCountExample = new MenuExample();
		MenuExample.Criteria menuCountCriteria = menuCountExample.createCriteria();
		menuCountExample.setOrderByClause("Id");
		System.out.println("moduleId ----------- " + moduleId);
		menuCountCriteria.andModuleIdEqualTo(moduleId);
		menuCountCriteria.andStatusEqualTo("1");
		List<Menu> menuFirstLastIdList = new ArrayList<Menu>();
		menuFirstLastIdList = menuMapper.selectByExample(menuCountExample);
		long totalMenuCount = menuMapper.countByExample(menuCountExample);
		// select first and last id from menu table
		if (menuFirstLastIdList.size() > 0) {
			BigDecimal firstid = menuFirstLastIdList.get(0).getId();
			BigDecimal tmptoalMenuCount = new BigDecimal(totalMenuCount - 1);
			String covValue = String.valueOf(tmptoalMenuCount);
			BigDecimal covDecimalFirstId = new BigDecimal(covValue);
			BigDecimal lastId = menuFirstLastIdList.get(covDecimalFirstId.intValue()).getId();
			System.out.println("First Id  >>>>> " + firstid);
			System.out.println("last  Id  >>>>> " + lastId);
			MenuExample menuExample = new MenuExample();
			MenuExample.Criteria menuCriteria = menuExample.createCriteria();
			menuCriteria.andParentIdIsNull();
			menuCriteria.andModuleIdEqualTo(moduleId);

			menuListLevel1 = menuMapper.selectByExample(menuExample);
			for (Menu menuLevel1 : menuListLevel1) {
				idListFirstLevel.add(menuLevel1.getId());
			}
			MenuExample menu1Example = new MenuExample();
			MenuExample.Criteria menu1Criteria = menu1Example.createCriteria();
			menu1Criteria.andParentIdIn(idListFirstLevel);
			menuCriteria.andModuleIdEqualTo(moduleId);
			menuListLevel2 = menuMapper.selectByExample(menu1Example);
			for (Menu menuLevel2 : menuListLevel2) {
				idListSecondLevel.add(menuLevel2.getId());
			}
			System.out.println("idListSecondLevel Test 2>>" + idListSecondLevel);
			if (!idListSecondLevel.isEmpty()) {
				MenuExample menu2Example = new MenuExample();
				MenuExample.Criteria menu2Criteria = menu2Example.createCriteria();
				menu2Criteria.andParentIdIn(idListSecondLevel);
				menuCriteria.andModuleIdEqualTo(moduleId);

				menuListLevel3 = menuMapper.selectByExample(menu2Example);
			}
			for (Menu menuLevel3 : menuListLevel3) {
				idListThirdLevel.add(menuLevel3.getId());
			}
			if (!idListThirdLevel.isEmpty()) {
				MenuExample menu3Example = new MenuExample();
				MenuExample.Criteria menu3Criteria = menu3Example.createCriteria();
				menu3Criteria.andParentIdIn(idListThirdLevel);
				menuCriteria.andModuleIdEqualTo(moduleId);

				menuListLevel4 = menuMapper.selectByExample(menu3Example);
			}
			System.out.println("menuListLevel1" + menuListLevel1);
			System.out.println("menuListLevel2" + menuListLevel2);
			System.out.println("menuListLevel3" + menuListLevel3);
			System.out.println("menuListLevel4" + menuListLevel4);
			model.addAttribute("level1", menuListLevel1);
			model.addAttribute("level2", menuListLevel2);
			model.addAttribute("level3", menuListLevel3);
			model.addAttribute("level4", menuListLevel4);
			model.addAttribute("totalMenuCount", totalMenuCount);
			model.addAttribute("firstid", firstid);
			model.addAttribute("lastId", lastId);
			model.addAttribute("StatusList", StatusList);
			model.addAttribute("RoleModuleList", roleModuleList);
			model.addAttribute("modulePrmssnList", modulePrmssnList);
			model.addAttribute("roleMenuList", json);
			System.out.println("modulePrmssnList    >>>>>>>>>   :  " + modulePrmssnList);
		}
		return "uam/admin-system-group-profile-Update";
	}

	// update userRoleUpdateAction data - save
	@RequestMapping(value = "/userRoleUpdateAction", method = RequestMethod.POST)
	public String userRoleUpdateAction(HttpServletRequest req, @ModelAttribute("userGroup") UserGroup userGroup,
			UserGroupModule userGroupModule, BindingResult result, Model model) {
		HttpSession session = req.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String message = "Session Has Been Expired";
			model.addAttribute("error", message);
			return "redirect:/";
		}
		// Status List
		AdminParamExample statusList = new AdminParamExample();
		AdminParamExample.Criteria statusListCriteria = statusList.createCriteria();
		statusListCriteria.andParamTypeEqualTo("STATUS");
		List<AdminParam> StatusList = new ArrayList<AdminParam>();
		StatusList = adminParamMapper.selectByExample(statusList);
		// Module List - active only : to display in create user role form
		ModuleExample moduleList = new ModuleExample();
		ModuleExample.Criteria statusModuleCriteria = moduleList.createCriteria();
		statusModuleCriteria.andStatusEqualTo("1");
		moduleList.setOrderByClause("name");
		List<Module> ModuleList = new ArrayList<Module>();
		ModuleList = moduleMapper.selectByExample(moduleList);
		// Role List - active only : to display in create user role form
		RoleExample roleList = new RoleExample();
		RoleExample.Criteria statusRoleCriteria = roleList.createCriteria();
		statusRoleCriteria.andStatusEqualTo("1");
		roleList.setOrderByClause("name");
		List<Role> RoleList = new ArrayList<Role>();
		RoleList = roleMapper.selectByExample(roleList);
		// getting User RoleModule list All
		RoleModuleExample roleModuleExample = new RoleModuleExample();
		RoleModuleExample.Criteria roleModuleExampleCriteria = roleModuleExample.createCriteria();
		List<RoleModule> RoleModuleList = new ArrayList<RoleModule>();
		RoleModuleList = roleModuleMapper.getRoleModuleByCriteria(roleModuleExample);
		RoleModule roleModule = new RoleModule();

		String tmpModule = req.getParameter("module");
		String tmpRole = req.getParameter("role");
		String tmpStatus = req.getParameter("status");
		if (null != tmpModule && !tmpModule.isEmpty()) { // module_id
															// in
															// DSP_ADM_TBL_ROLE_MODULE
			roleModuleExampleCriteria.andModuleIdEqualTo((short) Integer.parseInt(req.getParameter("module")));
		}
		if (null != tmpRole && !tmpRole.isEmpty()) { // id
														// in
														// DSP_ADM_TBL_ROLE_MODULE
			roleModuleExampleCriteria.andRoleIdEqualTo((short) Integer.parseInt(req.getParameter("role")));
		}
		if (null != tmpStatus && !tmpStatus.isEmpty()) // status
														// in
														// DSP_ADM_TBL_ROLE_MODULE
		{
			roleModuleExampleCriteria.andStatusEqualTo(req.getParameter("status"));
		}
		RoleModuleList = roleModuleMapper.getRoleModuleByCriteria(roleModuleExample);
		System.out.println("RoleModuleList123" + RoleModuleList);
		model.addAttribute("RoleModuleList", RoleModuleList);
		model.addAttribute("StatusList", StatusList);
		model.addAttribute("ModuleList", ModuleList);
		model.addAttribute("RoleList", RoleList);
		String status = req.getParameter("status");
		System.out.println("status" + status);
		// 27022018 pramaiyan
		RoleMenu rolemenu = new RoleMenu();
		short id = Short.parseShort(req.getParameter("roleModuleid").trim()); // rolemodule
		// id from
		// roleModuletable
		short roleId = Short.parseShort(req.getParameter("roleId").trim()); // from
																			// admin-system-group-profile-update.jsp
		short moduleId = Short.parseShort(req.getParameter("moduleId").trim());// from
																				// admin-system-group-profile-update.jsp
		int firstIdreq = Integer.parseInt(req.getParameter("firstid").trim());// from
																				// admin-system-group-profile-update.jsp
		int lastIdreq = Integer.parseInt(req.getParameter("lastId").trim());// from
																			// admin-system-group-profile-update.jsp
		System.out.println("firstIdreq : " + firstIdreq);
		System.out.println("lastIdreq : " + lastIdreq);
		/*
		 * System.out.println("RoleModule_Id : "+id); System.out.println(
		 * "RoleId : "+roleId);
		 */
		System.out.println("Module Id : " + moduleId);

		short menuId = 0;
		String tmpAccessRight = "0";
		String allcheckBoxvalue = "0";
		for (int i = firstIdreq; i <= lastIdreq; i++) {
			System.out.println("---------------------------");
			RoleMenuExample roleMenuExample = new RoleMenuExample();
			System.out.println("menuId:::: " + i);
			RoleMenuExample.Criteria criteriaRoleMenuExample = roleMenuExample.createCriteria();
			criteriaRoleMenuExample.andRoleIdEqualTo(roleId);
			criteriaRoleMenuExample.andMenuIdEqualTo((short) i);
			criteriaRoleMenuExample.andModuleIdEqualTo(moduleId);
			// checking If MenuId Is exist or not in DSP_ADM_TBL_ROLE_MENU Table
			long menuidCount = roleMenuMapper.countByExample(roleMenuExample);
			System.out.println("accessRight>>>" + tmpAccessRight);

			// If MenuId Is not exist in DSP_ADM_TBL_ROLE_MENU Table
			if (menuidCount == 0) {
				System.out.println("Menu id Not Present" + menuidCount);
				if (null != req.getParameter("accessRight" + i)) {
					String checkBoxvalue = "";
					// Start reading selected checkbox value
					String tempAccessRighttemp[] = req.getParameterValues("accessRight" + i);
					if (tempAccessRighttemp != null && tempAccessRighttemp.length > 0) {
						for (int t = 0; t < tempAccessRighttemp.length; t++) {
							System.out.println("Access right" + tempAccessRighttemp[t]);
							checkBoxvalue = checkBoxvalue + tempAccessRighttemp[t].toString() + "$";
						}
						allcheckBoxvalue = checkBoxvalue.trim();
					}
					// End reading selected checkbox value

					String tempAccessRight = req.getParameter("accessRight" + i);
					// accessRight = Short.parseShort(tempAccessRight);
					tmpAccessRight = tempAccessRight;

				}
			}
			// If MenuId Is Present in DSP_ADM_TBL_ROLE_MENU Table
			else {

				menuId = (short) i;
				System.out.println(req.getParameter("accessRight" + i) + "  ::::  menu Id Present ::: " + i);
				if (null != req.getParameter("accessRight" + i)) {
					String checkBoxvalue = "";
					allcheckBoxvalue = "0";
					// System.out.println("menuId if :::" + menuId);
					// Start reading selected checkbox value
					String tempAccessRighttemp[] = req.getParameterValues("accessRight" + i);
					if (tempAccessRighttemp != null && tempAccessRighttemp.length > 0) {
						for (int t = 0; t < tempAccessRighttemp.length; t++) {
							System.out.println("Access right" + tempAccessRighttemp[t]);
							checkBoxvalue = checkBoxvalue + tempAccessRighttemp[t].toString() + "$";
						}
						allcheckBoxvalue = checkBoxvalue.trim();
					}
					// End
					String tempAccessRight = req.getParameter("accessRight" + i);
					// accessRight = Short.parseShort(tempAccessRight);
					tmpAccessRight = tempAccessRight;
				}
			}

			// If checkbox name is Null
			if (null == req.getParameter("accessRight" + i)) {
				tmpAccessRight = "0";
			}

			// If MenuId Is not exist in DSP_ADM_TBL_ROLE_MENU Table
			if (menuidCount == 0 && null != req.getParameter("accessRight" + i)) {
				rolemenu.setRoleId(roleId);
				rolemenu.setMenuId((short) i);
				rolemenu.setModuleId(moduleId);
				rolemenu.setPrmssnId(allcheckBoxvalue); // set permission id zero if uselect checkbox
				rolemenu.setCreateDate(new Date());
				short loginuserid = Short.parseShort(session.getAttribute("id").toString());
				rolemenu.setCreateBy(loginuserid);
				System.out.print("Calling Insert ");
				roleMenuMapper.insert(rolemenu);
			}

			else if (menuidCount > 0
					&& (null != req.getParameter("accessRight" + i) || null == req.getParameter("accessRight" + i))) {
				// System.out.println("accessRight test >>>>" +
				// req.getParameter("accessRight" + i) + " : " + accessRight);
				// Start reading selected checkbox value
				String tempAccessRighttemp[] = req.getParameterValues("accessRight" + i);
				allcheckBoxvalue = "0";
				if (tempAccessRighttemp != null && tempAccessRighttemp.length > 0) {
					String checkBoxvalue = "";
					for (int t = 0; t < tempAccessRighttemp.length; t++) {
						System.out.println("Access right" + tempAccessRighttemp[t]);
						checkBoxvalue = checkBoxvalue + tempAccessRighttemp[t].toString() + "$";
					}
					allcheckBoxvalue = checkBoxvalue.trim();
				}

				// End

				RoleMenu rolemenuUpdate = new RoleMenu();
				rolemenuUpdate.setRoleId(roleId);
				rolemenuUpdate.setMenuId((short) i);
				rolemenuUpdate.setModuleId(moduleId);
				rolemenuUpdate.setPrmssnId(allcheckBoxvalue);// set permission id zero if uselect checkbox
				rolemenuUpdate.setUpdateDate(new Date());
				// rolemenuUpdate.setUpdateBy(Short.parseShort(session.getAttribute("id").toString()));
				System.out.println("roleId >>> " + roleId);
				System.out.println("roleModuleCount" + menuidCount);
				short rolemenuId = roleMenuMapper.selectByExample(roleMenuExample).get(0).getId(); // getting Id value
																									// from
																									// DSP_ADM_TBL_ROLE_MENU
																									// for Update the
																									// specific record
				System.out.println("update  Permission in DSP_ADM_TBL_ROLE_MENU by Id ==== " + rolemenuId
						+ "  and Menu id ::::" + i);
				rolemenuUpdate.setId(rolemenuId);
				System.out.println("Calling updateByPrimaryKey ");
				roleMenuMapper.updateByPrimaryKey(rolemenuUpdate);
			}
		} // for loop end

		System.out.println("roleId >>> " + roleId);
		System.out.print("status" + status);
		roleModule.setRoleId(roleId);
		roleModule.setModuleId(moduleId);
		roleModule.setStatus(status);
		roleModule.setUpdateDate(new Date());
		roleModule.setUpdateBy(loginUser);
		System.out.println("val--------->" + loginUser);
		System.out.print("Calling Update Role Module ");
		int rs = roleModuleMapper.updateByMultipleCriteria(roleModule);
		if (rs == 1) {
			model.addAttribute("updatemessage", " Updated Data Successfully");
		} else {
			model.addAttribute("updatemessage", " Update Data Failed");
		}
		return "uam/admin-system-group-profile";
	}
	// for loop end
	// End By Pramaiyan
	// Start user profile tulasi code on 02/03/2017
	// create profile data - save

	@RequestMapping(value = "/createUserProfileAction", method = RequestMethod.POST)
	public String createUserProfileAction(HttpServletRequest req,
			@ModelAttribute("userProfile") UserProfile userProfile, BindingResult result, Model model)

	{

		HttpSession session = req.getSession();

		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String message = "Session Has Been Expired";
			model.addAttribute("error", message);
			return "redirect:/";
		}

		String pfNumber = req.getParameter("pfNumber");
		String managerPF = req.getParameter("managerPF");

		System.out.println("managerPF" + managerPF);

		String name = req.getParameter("name");
		// String groupID= req.getParameter("roleId");
		String[] roleid = req.getParameterValues("roleId");
		List list = Arrays.asList(roleid);
		String status = req.getParameter("status");
		String email = req.getParameter("email");
		UserRole userRole = new UserRole();
		Short userid = 0;
		if (session.getAttribute("id") != null) {
			userid = (Short) session.getAttribute("id");
		}

		userProfile.setPfNumber(pfNumber);
		userProfile.setName(name);
		userProfile.setCreateDate(new Date());
		userProfile.setCreateBy(loginUser);
		userProfile.setEmail(email);
		userProfile.setStatus(status);
		userProfile.setEmailLinkStatus("n");
		long v1 = 0;
		userProfile.setLogoutFlag(BigDecimal.valueOf(v1));
		int rs = userProfileMapper.insert(userProfile);

		UserProfileExample userProfileExample = new UserProfileExample();
		UserProfileExample.Criteria userProfileCriteria = userProfileExample.createCriteria();
		userProfileCriteria.andPfNumberEqualTo(pfNumber);
		userProfileExample.setOrderByClause("id");
		List<UserProfile> UserProfileList = new ArrayList<UserProfile>();
		UserProfileList = userProfileMapper.selectByExample(userProfileExample);
		Short insertlastid = 0;
		if (!UserProfileList.isEmpty()) {
			insertlastid = UserProfileList.get(0).getId();
		}

		System.out.println("insertlastid " + insertlastid);

		for (int i = 0; i < list.size(); i++) {
			String rId = String.valueOf(list.get(i));
			System.out.println("rId " + rId);
			userRole.setRoleModuleId(Short.valueOf(rId));
			userRole.setCreateBy(userid);
			userRole.setCreateDate(new Date());
			userRole.setStatus("1");
			userRole.setUserId(insertlastid);
			userRoleMapper.insert(userRole);

		}

		if (rs == 1) {
			model.addAttribute("savemessage", " Inserted Data Successfully");
		} else {
			model.addAttribute("savemessage", " Insert Data Failed");
		}

		String encryptionKey = "MZygpewJsCpRrfOr";
		String plainText = insertlastid.toString();
		AdvancedEncryptionStandard advancedEncryptionStandard = new AdvancedEncryptionStandard(encryptionKey);
		String cipherText = null;
		String decryptedCipherText = null;
		try {
			cipherText = advancedEncryptionStandard.encrypt(plainText);
			// decryptedCipherText =
			// advancedEncryptionStandard.decrypt(cipherText);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		StringBuffer url = req.getRequestURL();
		String uri = req.getRequestURI();
		String ctx = req.getContextPath();
		String base = url.substring(0, url.length() - uri.length() + ctx.length());

		String emailURL = base + "/" + "secure" + "/" + cipherText;
		System.out.println(emailURL);
		System.out.println("Email Servicee Starting");

		RegisterEmailVo registerEmailVo = new RegisterEmailVo();
		registerEmailVo.setUserName(pfNumber);
		registerEmailVo.setUserNric(pfNumber);
		registerEmailVo.setEmailLink(emailURL);
		registerEmailVo.setUserEmail(email);
		registerEmailVo.setTemplateName("UAMRegister");
		registerEmailVo = UserValidation_DAO.emailCallingForRegister(registerEmailVo);

		return userProfile(req, null, model);
	}

	@RequestMapping(value = "/userDetail", method = RequestMethod.GET)
	@ResponseBody
	public void userDetail(HttpServletResponse response, HttpServletRequest request) {
		System.out.println("user id>>>>>>" + request.getParameter("id"));
		UserProfile UserDetail = new UserProfile();
		UserDetail = userProfileMapper.selectByPrimaryKey((short) Integer.parseInt(request.getParameter("id")));
		UserRoleExample userRole = new UserRoleExample();
		UserRoleExample.Criteria Rolecriteria = userRole.createCriteria();
		Rolecriteria.andUserIdEqualTo(UserDetail.getId());
		List<UserRole> roleList = new ArrayList<UserRole>();
		List<Short> rolemoduleid = new ArrayList<Short>();
		roleList = userRoleMapper.selectByExample(userRole);
		if (!roleList.isEmpty()) {
			for (int i = 0; i < roleList.size(); i++) {
				rolemoduleid.add(roleList.get(i).getRoleModuleId());
			}
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// create Json Object
		JSONObject json = new JSONObject();
		// put some value pairs into the JSON object .
		try {
			json.put("pfNumber", UserDetail.getPfNumber());
			json.put("name", UserDetail.getName());
			json.put("mngPFNumber", UserDetail.getManagerPfNumber());
			json.put("remarks", UserDetail.getRemarks());
			json.put("email", UserDetail.getEmail());
			json.put("groupID", rolemoduleid);
			json.put("status", UserDetail.getStatus());
			json.put("id", UserDetail.getId());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// finally output the json string
		out.print(json.toString());
		System.out.println(json.toString());
	}

	// update profile data - save
	@RequestMapping(value = "/updateUserProfileAction", method = RequestMethod.POST)
	public String updateUserProfileAction(HttpServletRequest req,
			@ModelAttribute("userProfile") UserProfile userProfile, BindingResult result, Model model) {
		HttpSession session = req.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String message = "Session Has Been Expired";
			model.addAttribute("error", message);
			return "redirect:/";
		}
		String pfNumber = req.getParameter("pfNumber");
		String name = req.getParameter("name");
		String email = req.getParameter("email");
		String mngPFNumber = req.getParameter("mngPFNumber");
		String remarks = req.getParameter("remarks");

		Short usereditid = 0;
		if (req.getParameter("usereditid") != null) {
			usereditid = Short.valueOf(req.getParameter("usereditid"));
		}
		String[] roleid = req.getParameterValues("groupID");
		List list = Arrays.asList(roleid);
		String status = req.getParameter("status");
		UserRole userRole = new UserRole();
		Short userid = 0;
		if (session.getAttribute("id") != null) {
			userid = (Short) session.getAttribute("id");
		}
		userProfile.setPfNumber(pfNumber);
		userProfile.setName(name);
		userProfile.setEmail(email);
		userProfile.setManagerPfNumber(mngPFNumber);
		userProfile.setRemarks(remarks);

		userProfile.setStatus(status);
		userProfile.setUpdateBy(loginUser);
		userProfile.setUpdateDate(new Date());
		int rs = userProfileMapper.updateByPrimaryKeySelective(userProfile);
		UserRoleExample userRoleExample = new UserRoleExample();
		UserRoleExample.Criteria Rolecriteria = userRoleExample.createCriteria();
		Rolecriteria.andUserIdEqualTo(usereditid);
		int delroleList = userRoleMapper.deleteByExample(userRoleExample);
		System.out.println("delete record " + delroleList);
		for (int i = 0; i < list.size(); i++) {
			String rId = String.valueOf(list.get(i));
			System.out.println("rId " + rId);
			userRole.setRoleModuleId(Short.valueOf(rId));
			userRole.setCreateBy(userid);
			userRole.setCreateDate(new Date());
			userRole.setStatus("1");
			userRole.setUserId(usereditid);
			userRoleMapper.insert(userRole);
		}
		System.out.println("updated record " + rs);
		if (rs == 1) {
			model.addAttribute("updatemessage", " Updated Data Successfully");
		} else {
			model.addAttribute("updatemessage", " Update Data Failed");
		}
		return userProfile(req, null, model);
	}

	@RequestMapping(value = "/validateCreateUser", method = RequestMethod.POST)
	@ResponseBody
	public void validateCreateUser(HttpServletResponse response, HttpServletRequest request) {
		long existRecordCount = 0;
		if (!request.getParameter("pfNumber").trim().isEmpty()) {
			UserProfileExample userProfile = new UserProfileExample();
			UserProfileExample.Criteria criteria = userProfile.createCriteria();
			criteria.andPfNumberEqualTo(request.getParameter("pfNumber"));
			existRecordCount = userProfileMapper.countByExample(userProfile);
		} else {
			existRecordCount = -1;
		}
		// System.out.println(existRecordCount);
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// create Json Object
		JSONObject json = new JSONObject();
		// put some value pairs into the JSON object .
		try {
			json.put("count", existRecordCount);
			if (request.getParameter("pfNumber").trim().isEmpty()) {
				json.put("requiredPF", true);
			} else {
				json.put("requiredPF", false);
			}
			if (request.getParameter("name").trim().isEmpty()) {
				json.put("requiredName", true);
			} else {
				json.put("requiredName", false);
			}
			if (request.getParameter("roleId").trim().isEmpty()) {
				json.put("requiredRole", true);
			} else {
				json.put("requiredRole", false);
			}
			if (existRecordCount == 0) {
				json.put("recordFound", false);
			} else {
				json.put("recordFound", true);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// finally output the json string
		out.print(json.toString());
		System.out.println(json.toString());
	}

	@RequestMapping(value = "/validateUpdateUser", method = RequestMethod.POST)
	@ResponseBody
	public void validateUpdateUser(HttpServletResponse response, HttpServletRequest request) {
		long existRecordCount = 0;
		int id = Integer.parseInt(request.getParameter("id"));
		if (!request.getParameter("pfNumber").trim().isEmpty()) {
			UserProfileExample userProfile = new UserProfileExample();
			UserProfileExample.Criteria criteria = userProfile.createCriteria();
			criteria.andIdNotEqualTo((short) id);
			criteria.andPfNumberEqualTo(request.getParameter("pfNumber"));
			existRecordCount = userProfileMapper.countByExample(userProfile);
		} else {
			existRecordCount = -1;
		}
		// System.out.println(existRecordCount);
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// create Json Object
		JSONObject json = new JSONObject();
		// put some value pairs into the JSON object .
		try {
			json.put("count", existRecordCount);
			if (request.getParameter("pfNumber").trim().isEmpty()) {
				json.put("requiredPF", true);
			} else {
				json.put("requiredPF", false);
			}
			if (request.getParameter("name").trim().isEmpty()) {
				json.put("requiredName", true);
			} else {
				json.put("requiredName", false);
			}

			if (request.getParameter("email").trim().isEmpty()) {
				json.put("requiredEmail", true);
			} else {
				json.put("requiredEmail", false);
			}

			if (request.getParameter("mngPFNumber").trim().isEmpty()) {
				json.put("requiredmngPFNumber", true);
			} else {
				json.put("requiredmngPFNumber", false);
			}

			if (request.getParameter("remarks").trim().isEmpty()) {
				json.put("requiredRemarks", true);
			} else {
				json.put("requiredRemarks", false);
			}

			if (request.getParameter("groupID").trim().isEmpty()) {
				json.put("requiredRole", true);
			} else {
				json.put("requiredRole", false);
			}
			if (request.getParameter("status").trim().isEmpty()) {
				json.put("requiredStatus", true);
			} else {
				json.put("requiredStatus", false);
			}
			if (existRecordCount == 0) {
				json.put("recordFound", false);
			} else {
				json.put("recordFound", true);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// finally output the json string
		out.print(json.toString());
		System.out.println(json.toString());
	}

	///////////////////////////////////////// ****************
	///////////////////////////////////////// end****************************************/
	@RequestMapping(value = "/moduleList", method = RequestMethod.GET)
	public String moduleList(HttpServletRequest request, HttpServletResponse response, Model model) {
		// http://localhost:9999/DSPAdminModule/getDashboard
		return "/moduleList";
	}

	// *****************************************Start Menu management on
	// 24032018 by pramaiyan **************************************************
	@RequestMapping("/menuCreate")
	public String createMenu(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String message = "Session Has Been Expired";
			model.addAttribute("error", message);
			return "redirect:/";
		}
		// Module List - active only : to display in create menu
		ModuleExample moduleList = new ModuleExample();
		ModuleExample.Criteria statusModuleCriteria = moduleList.createCriteria();
		statusModuleCriteria.andStatusEqualTo("1");
		moduleList.setOrderByClause("name");
		List<Module> ModuleList = new ArrayList<Module>();
		ModuleList = moduleMapper.selectByExample(moduleList);
		model.addAttribute("moduleList", ModuleList);
		return "uam/admin-system-menu-management";
	}

	// create Menu Save Action on 24032018
	@RequestMapping("/createMenuAction")
	public String createMenuAction(@ModelAttribute Menu menu, HttpServletRequest request, HttpServletResponse response,
			Model model, BindingResult result) {
		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String message = "Session Has Been Expired";
			model.addAttribute("error", message);
			return "redirect:/";
		}
		String name = request.getParameter("name");
		String module = request.getParameter("module");
		String parentId = request.getParameter("parentId");
		String level = request.getParameter("level");
		String url = request.getParameter("url");
		int rs = 0;
		System.out.println("parentId" + parentId);
		if (null != name && null != module && null != level && null != url) {
			if (!name.isEmpty() && !module.isEmpty() && !level.isEmpty() && !url.isEmpty()) {
				{
					// Find Duplicate Menu Name exist or not in Menu table
					MenuExample menuNameCountExample = new MenuExample();
					MenuExample.Criteria menuCriteria = menuNameCountExample.createCriteria();
					menuCriteria.andModuleIdEqualTo(Short.parseShort(module));
					menuCriteria.andNameEqualTo(name);
					menuCriteria.andUrlEqualTo(url);
					Long menuNameCountList = menuMapper.countByExample(menuNameCountExample);
					System.out.println("MenuNameCountList : " + menuNameCountList);
					if (menuNameCountList == 0) {
						menu.setId(new BigDecimal(1));
						menu.setName(name.trim());
						menu.setModuleId(Short.parseShort(module));
						menu.setLevelId(Short.parseShort(level));
						if (!parentId.isEmpty()) {
							menu.setParentId(Short.parseShort(parentId));
						}
						menu.setUrl("/" + url.trim());
						menu.setStatus("1");
						menu.setCreateBy(Short.parseShort(session.getAttribute("id").toString()));
						menu.setCreateDate(new Date());
						// insert new Menu into menu table
						rs = menuMapper.insert(menu);
					}
				}
			}
		}
		/*
		 * //Find Duplicate Role Name Id exist or not in Role table RoleExample
		 * roleaNameCountExample = new RoleExample(); RoleExample.Criteria
		 * roleCountCriteria = roleaNameCountExample.createCriteria();
		 * roleCountCriteria.andNameEqualTo((rolenNameReq.toUpperCase())); Long
		 * roleNameCountList = roleMapper.countByExample(roleaNameCountExample);
		 * System.out.println("RoleNameCountList : " + roleNameCountList);
		 * if(roleNameCountList==0) { role.setName(rolenNameReq.toUpperCase().trim());
		 * role.setStatus("1");
		 * role.setCreateBy(Short.parseShort(session.getAttribute("id").toString ()));
		 * role.setCreateDate(new Date()); //insert Rolename into Role table
		 * roleMapper.insert(role); } else { // return "redirect:/userRole"; }
		 */
		if (rs == 1) {
			model.addAttribute("savemessage", " Inserted Data Successfully");
		} else {
			model.addAttribute("savemessage", " Insert Data Failed");
		}
		return createMenu(request, null, model);
	}

	@RequestMapping(value = "/validateMngPFNumber", method = RequestMethod.POST)
	@ResponseBody
	public String newcarChassisEngineUpdate(HttpServletRequest request, Model model, @RequestParam String managerPF) {
		List<UserProfile> userProfileList = new ArrayList<UserProfile>();
		String ManagerName = "";
		if (!managerPF.isEmpty()) {
			UserProfileExample userProfileExample = new UserProfileExample();
			UserProfileExample.Criteria criteria = userProfileExample.createCriteria();
			criteria.andPfNumberEqualTo(managerPF);
			userProfileList = userProfileMapper.selectByExample(userProfileExample);
			if (userProfileList.size() > 0) {
				System.out.println("managerPF" + userProfileList.get(0).getName());
				ManagerName = userProfileList.get(0).getName();
			}
		} else {
			System.out.println("managerPF Not  Found ");
		}
		return ManagerName;
	}

	@RequestMapping(value = "/validateMngPFNumberUpdate", method = RequestMethod.POST)
	@ResponseBody
	public String validateMngPFNumberUpdate(HttpServletRequest request, Model model, @RequestParam String mngPFNumber) {
		List<UserProfile> userProfileList = new ArrayList<UserProfile>();
		String ManagerName = "";
		if (!mngPFNumber.isEmpty()) {
			UserProfileExample userProfileExample = new UserProfileExample();
			UserProfileExample.Criteria criteria = userProfileExample.createCriteria();
			criteria.andPfNumberEqualTo(mngPFNumber);
			userProfileList = userProfileMapper.selectByExample(userProfileExample);
			if (userProfileList.size() > 0) {
				System.out.println("managerPF" + userProfileList.get(0).getName());
				ManagerName = userProfileList.get(0).getName();
			}
		} else {
			System.out.println("managerPF Not  Found ");
		}
		return ManagerName;
	}

	////////// ***********end
	////////// *****************************////////////////////////////

	////// Tulasi added on 03/04/2018 - Register AD//////////

	// -------------------------------------------Set Password First
	// Time--------------------------------------------------------------------------------
	@RequestMapping(value = { "/secure/{id}" }, method = RequestMethod.GET)
	public String secure(HttpServletRequest request, HttpServletResponse response, Model model,
			@PathVariable String id) {

		String redirectTO = null;
		String encryptionKey = "MZygpewJsCpRrfOr";
		AdvancedEncryptionStandard advancedEncryptionStandard = new AdvancedEncryptionStandard(encryptionKey);
		String cipherID = id;
		String decryptedCipherID = null;
		try {
			decryptedCipherID = advancedEncryptionStandard.decrypt(cipherID);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// System.out.println(cipherID);
		// System.out.println(decryptedCipherID);

		if (decryptedCipherID != null) {

			UserProfile userProfile = userProfileMapper.selectByPrimaryKey(Short.valueOf(decryptedCipherID));

			System.out.println("userId " + decryptedCipherID);
			if (userProfile == null) {
				redirectTO = "admin-setpassword-exp";
			} else {

				if ("n".equalsIgnoreCase(userProfile.getEmailLinkStatus())) {
					userProfile.setEmailLinkStatus("y");
					userProfileMapper.updateByPrimaryKeySelective(userProfile);
					model.addAttribute("userID", userProfile.getPfNumber());

					redirectTO = "admin-setpassword";
				} else {
					redirectTO = "admin-setpassword-exp";
				}
			}
		} else {
			redirectTO = "admin-setpassword-exp";
		}
		return redirectTO;

	}

	/////// 03/04/2018 - change password//////////
	@RequestMapping("/ChangePw")
	public String ChangePw(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String pfNumber = (String) session.getAttribute("pfNumber");
		if (pfNumber == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);

			return "admin-login";
		}

		model.addAttribute("userID", pfNumber);

		return "uam/changepassword";

	}

	@RequestMapping("/setPasswordDone")
	public String setPasswordDone(HttpServletRequest request, HttpServletResponse response, Model model) {

		String redirectTO = null;
		String pfnumber = "";
		String newpassword = "";
		String confpassword = "";

		pfnumber = request.getParameter("username"); // Agent id
		System.out.println("setPasswordDone usernam " + pfnumber);
		List<UserProfile> userProfileList_check = new ArrayList<UserProfile>();
		UserProfileExample userProfileExample = new UserProfileExample();
		UserProfileExample.Criteria user_criteria_check = userProfileExample.createCriteria();
		if (!pfnumber.isEmpty()) {
			user_criteria_check.andPfNumberEqualTo(pfnumber);
		}

		userProfileList_check = userProfileMapper.selectByExample(userProfileExample);

		if (userProfileList_check.size() != 1) {
			model.addAttribute("setpw_error", "User Id Not Exist");
			model.addAttribute("userID", pfnumber);
			return "admin-setpassword";
		}

		if (!userProfileList_check.get(0).getPfNumber().equalsIgnoreCase(pfnumber)) {
			model.addAttribute("setpw_error", "User Id Not Exist");
			model.addAttribute("userID", pfnumber);
			return "admin-setpassword";
		}
		newpassword = request.getParameter("newpassword").trim();
		confpassword = request.getParameter("confpassword").trim();

		if (!newpassword.equalsIgnoreCase(confpassword)) {
			model.addAttribute("setpw_error", "Passwords are not matching");
			model.addAttribute("userID", pfnumber);
			return "admin-setpassword";
		}
		if (!passCheck(newpassword)) {
			model.addAttribute("setpw_error",
					"The password must be at least 1 number, 1 special character, 1 upper and 1 lower case alphabets");
			model.addAttribute("userID", pfnumber);
			return "admin-setpassword";
		}
		if (!passCheck(confpassword)) {
			model.addAttribute("setpw_error",
					"The password must be at least 1 number, 1 special character, 1 upper and 1 lower case alphabets");
			model.addAttribute("userID", pfnumber);
			return "admin-setpassword";
		}

		System.out.println("calling Ad Creating User " + pfnumber + "newpassword " + newpassword);
		LDAPAttributesBean ldap = RegCPF.ADUserCreate(pfnumber, newpassword);
		System.out.println(ldap.getValidated() + "validateing USer ");
		System.out.println(ldap.getError_code() + " Error code ");
		if (ldap.getError_code().equals("D0000")) {
			if (ldap.getValidated().equals(1) && ldap.getError_code().equals("D0000")) {

				RegisterEmailVo registerEmailVo = new RegisterEmailVo();
				registerEmailVo.setUserName(request.getParameter("username"));
				registerEmailVo.setUserNric(request.getParameter("username"));
				registerEmailVo.setUserPswd(request.getParameter("newpassword"));
				UserProfileExample userExample = new UserProfileExample();
				UserProfileExample.Criteria agent_criteria = userExample.createCriteria();
				agent_criteria.andPfNumberEqualTo(request.getParameter("username"));

				List<UserProfile> userProfileList = new ArrayList<UserProfile>();
				userProfileList = userProfileMapper.selectByExample(userExample);
				String email = userProfileList.get(0).getEmail();
				registerEmailVo.setEmailLink("");
				registerEmailVo.setUserName(userProfileList.get(0).getPfNumber());
				registerEmailVo.setUserNric(pfnumber);
				registerEmailVo.setUserEmail(email);
				registerEmailVo.setTemplateName("ChangePswdSuccessTemplate");
				registerEmailVo = UserValidation_DAO.emailCallingForRegister(registerEmailVo);
				model.addAttribute("setpw_success", "Password Updated Successfully");
			} else {
				model.addAttribute("setpw_error", "Error in creating new password,Please try again later!");
				model.addAttribute("userID", pfnumber);
				return "admin-setpassword";
			}
		} else if (ldap.getError_code().equals("D0022")) {
			model.addAttribute("setpw_error", "User already Exist!  <a href='forgotpassword'>Forgot Password</a> ");
			model.addAttribute("userID", pfnumber);
			return "admin-setpassword";
		}
		return adminLogin(model);
	}

	@RequestMapping("/ChangePwDone")
	public String ChangePwDone(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String pfnumber = (String) session.getAttribute("pfnumber");
		System.out.println(pfnumber + "pfnumber USer changepw");
		if (pfnumber == null) {

			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "/";
		}
		String username = request.getParameter("username").trim();
		String curpassword = request.getParameter("curpassword").trim();
		String newpassword = request.getParameter("newpassword").trim();
		String confpassword = request.getParameter("confpassword").trim();
		if (!pfnumber.equalsIgnoreCase(username)) {
			model.addAttribute("changepw_error", "Wrong Login ID");
			return "changepassword";
		}

		if (!newpassword.equalsIgnoreCase(confpassword)) {
			model.addAttribute("changepw_error", "Passwords are not matching");
			return "changepassword";
		}
		LDAPAttributesBean ldap = RegCPF.ADChangePassword(username, curpassword, newpassword);
		System.out.println(ldap.getValidated() + "validateing USer changepw");
		System.out.println(ldap.getError_code() + " Error code ");
		if (ldap.getError_code().equals("D0000")) {
			if (ldap.getValidated().equals(1) && ldap.getError_code().equals("D0000")) {
				model.addAttribute("changepw_login", "Password Updated Successfully");
			} else {
				model.addAttribute("userID", pfnumber);
				model.addAttribute("changepw_error", ldap.getError_msg());

				return "changepassword";
			}
		} else if (ldap.getError_code().equals("D0018")) {
			model.addAttribute("userID", pfnumber);
			model.addAttribute("changepw_error", ldap.getError_msg());

			return "changepassword";
		} else {
			model.addAttribute("userID", pfnumber);
			model.addAttribute("changepw_error", "Error in Updating Password,Please try again later");
			return "changepassword";
		}

		return "uam/changepassword";
	}

	public boolean passCheck(String passwd) {
		boolean flag = false;
		String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#!$%^&+=])(?=\\S+$).{8,}";
		System.out.println(passwd.matches(pattern));
		if (passwd.matches(pattern)) {
			flag = true;
		}
		return flag;

	}

	@RequestMapping("/adminLogin")
	public String adminLogin(Model model) {

		return "uam/admin-login";

	}

	@RequestMapping("/forgotPassword")
	public String forgotPassword(Model model) {

		// model.addAttribute("agentProfile",new AgentProfile());

		return "uam/forgotpassword";

	}

	@RequestMapping(value = "/forgotPasswordDone", method = RequestMethod.POST)
	public String forgotPasswordDone(HttpServletRequest request, HttpServletResponse response, Model model) {

		/*
		 * HttpSession session = request.getSession(); String agentidno
		 * =(String)session.getAttribute("agentidno"); if(agentidno == null) { String
		 * sessionexpired = "Session Has Been Expired";
		 * model.addAttribute("sessionexpired", sessionexpired);
		 * model.addAttribute("agentProfile",new AgentProfile()); return "agent_login";
		 * }
		 */

		String Userid = request.getParameter("username");
		String email = request.getParameter("email").trim();
		List<UserProfile> userProfileList = new ArrayList<UserProfile>();
		UserProfileExample userProfileExample = new UserProfileExample();
		UserProfileExample.Criteria user_criteria = userProfileExample.createCriteria();
		if (!Userid.isEmpty()) {
			user_criteria.andPfNumberEqualTo(Userid);
		}
		if (!email.isEmpty()) {
			user_criteria.andEmailEqualTo(email);
		}
		userProfileList = userProfileMapper.selectByExample(userProfileExample);

		if (userProfileList.size() <= 0 || userProfileList.size() > 1) {
			model.addAttribute("forgotpw_success", "Invalid user name or email");
		} else {

			UserProfile userProfile = new UserProfile();
			userProfile = userProfileList.get(0);

			RegisterEmailVo registerEmailVo = new RegisterEmailVo();
			registerEmailVo.setUserName(userProfileList.get(0).getPfNumber());
			registerEmailVo.setUserNric(request.getParameter("username"));
			registerEmailVo.setUserPswd("");
			String emailUrl = genPswdLink(request.getParameter("username"), request);
			registerEmailVo.setEmailLink(emailUrl);
			registerEmailVo.setUserEmail(email);
			registerEmailVo.setTemplateName("PswdTemplate");
			registerEmailVo = UserValidation_DAO.emailCallingForRegister(registerEmailVo);
			userProfile.setEmailLinkStatus("n");
			userProfileMapper.updateByPrimaryKeySelective(userProfile);
			model.addAttribute("forgotpw_success", "Send email Successfully");
		}
		return "uam/forgotpassword";
		// return agentLogin(model);
		// return agentLogin(model);
	}

	public String genPswdLink(String userId, HttpServletRequest request) {
		String encryptionKey = "MZygpewJsCpRrfOr";
		String plainText = userId;
		AdvancedEncryptionStandard advancedEncryptionStandard = new AdvancedEncryptionStandard(encryptionKey);
		String cipherText = null;
		String decryptedCipherText = null;
		try {
			cipherText = advancedEncryptionStandard.encrypt(plainText);
			// decryptedCipherText =
			// advancedEncryptionStandard.decrypt(cipherText);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(plainText);
		System.out.println(cipherText);
		System.out.println(decryptedCipherText);

		StringBuffer url = request.getRequestURL();
		String uri = request.getRequestURI();
		String ctx = request.getContextPath();
		String base = url.substring(0, url.length() - uri.length() + ctx.length());
		System.out.println(url+"url");
		System.out.println(uri+"uri");
		System.out.println(ctx+"ctx");
		System.out.println(base+"base");
		String emailURL = base + "/uam/" + "setpw" + "/" + cipherText;
		System.out.println(emailURL + "In genpswdling method");

		return emailURL;
	}

	// -------------------------------------------Set Password First
	// Time--------------------------------------------------------------------------------
	@RequestMapping(value = { "/setpw/{id}" }, method = RequestMethod.GET)
	public String setpw(HttpServletRequest request, HttpServletResponse response, Model model,
			@PathVariable String id) {

		String redirectTO = null;
		String encryptionKey = "MZygpewJsCpRrfOr";
		AdvancedEncryptionStandard advancedEncryptionStandard = new AdvancedEncryptionStandard(encryptionKey);
		String cipherID = id;

		String decryptedCipherID = null;
		try {
			decryptedCipherID = advancedEncryptionStandard.decrypt(cipherID);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// System.out.println(cipherID);
		// System.out.println(decryptedCipherID);
		System.out.println("userId " + decryptedCipherID);
		if (decryptedCipherID != null) {
			List<UserProfile> userProfileList_check = new ArrayList<UserProfile>();
			UserProfileExample userProfileExample = new UserProfileExample();
			UserProfileExample.Criteria user_criteria_check = userProfileExample.createCriteria();
			if (!decryptedCipherID.isEmpty()) {
				user_criteria_check.andPfNumberEqualTo(decryptedCipherID);
			}

			userProfileList_check = userProfileMapper.selectByExample(userProfileExample);

			if (userProfileList_check.size() != 1) {
				// UserProfile userProfile =
				// userProfileMapper.selectByPrimaryKey(Short.valueOf(decryptedCipherID));

				System.out.println("userId " + decryptedCipherID);

				redirectTO = "uam/admin-setpassword-exp";
			} else {
				UserProfile userProfile = userProfileList_check.get(0);
				if ("n".equalsIgnoreCase(userProfile.getEmailLinkStatus())) {
					userProfile.setEmailLinkStatus("y");
					userProfileMapper.updateByPrimaryKeySelective(userProfile);
					model.addAttribute("userID", userProfile.getPfNumber());

					redirectTO = "uam/setpassword";
				} else {
					redirectTO = "uam/admin-setpassword-exp";
				}
			}
		} else {
			redirectTO = "uam/admin-setpassword-exp";
		}
		return redirectTO;

	}

	@RequestMapping("/setPwDone")
	public String setPwDone(HttpServletRequest request, HttpServletResponse response, Model model) {

		String redirectTO = null;
		String pfnumber = "";
		String newpassword = "";
		String confpassword = "";

		pfnumber = request.getParameter("username"); // Agent id
		System.out.println("setPasswordDone usernam " + pfnumber);
		List<UserProfile> userProfileList_check = new ArrayList<UserProfile>();
		UserProfileExample userProfileExample = new UserProfileExample();
		UserProfileExample.Criteria user_criteria_check = userProfileExample.createCriteria();
		if (!pfnumber.isEmpty()) {
			user_criteria_check.andPfNumberEqualTo(pfnumber);
		}

		userProfileList_check = userProfileMapper.selectByExample(userProfileExample);

		if (userProfileList_check.size() != 1) {
			model.addAttribute("setpw_error", "User Id Not Exist");
			model.addAttribute("userID", pfnumber);
			return "setpassword";
		}

		if (!userProfileList_check.get(0).getPfNumber().equalsIgnoreCase(pfnumber)) {
			model.addAttribute("setpw_error", "User Id Not Exist");
			model.addAttribute("userID", pfnumber);
			return "setpassword";
		}
		newpassword = request.getParameter("newpassword").trim();
		confpassword = request.getParameter("confpassword").trim();

		if (!newpassword.equalsIgnoreCase(confpassword)) {
			model.addAttribute("setpw_error", "Passwords are not matching");
			model.addAttribute("userID", pfnumber);
			return "setpassword";
		}
		if (!passCheck(newpassword)) {
			model.addAttribute("setpw_error",
					"The password must be at least 1 number, 1 special character, 1 upper and 1 lower case alphabets");
			model.addAttribute("userID", pfnumber);
			return "setpassword";
		}
		if (!passCheck(confpassword)) {
			model.addAttribute("setpw_error",
					"The password must be at least 1 number, 1 special character, 1 upper and 1 lower case alphabets");
			model.addAttribute("userID", pfnumber);
			return "setpassword";
		}

		System.out.println("calling Ad Creating User " + pfnumber + "newpassword " + newpassword);
		LDAPAttributesBean ldap = RegCPF.ADForgotpassword(pfnumber, newpassword);
		System.out.println(ldap.getValidated() + "validateing USer ");
		System.out.println(ldap.getError_code() + " Error code ");
		if (ldap.getError_code().equals("D0000")) {
			if (ldap.getValidated().equals(1) && ldap.getError_code().equals("D0000")) {

				RegisterEmailVo registerEmailVo = new RegisterEmailVo();
				registerEmailVo.setUserName(request.getParameter("username"));
				registerEmailVo.setUserNric(request.getParameter("username"));
				registerEmailVo.setUserPswd(request.getParameter("newpassword"));
				UserProfileExample userExample = new UserProfileExample();
				UserProfileExample.Criteria agent_criteria = userExample.createCriteria();
				agent_criteria.andPfNumberEqualTo(request.getParameter("username"));

				List<UserProfile> userProfileList = new ArrayList<UserProfile>();
				userProfileList = userProfileMapper.selectByExample(userExample);
				String email = userProfileList.get(0).getEmail();
				registerEmailVo.setEmailLink("");
				registerEmailVo.setUserName(userProfileList.get(0).getPfNumber());
				registerEmailVo.setUserNric(pfnumber);
				registerEmailVo.setUserEmail(email);
				registerEmailVo.setTemplateName("ChangePswdSuccessTemplate");
				registerEmailVo = UserValidation_DAO.emailCallingForRegister(registerEmailVo);
				model.addAttribute("setpw_success", "Password Updated Successfully");
			} else {
				model.addAttribute("setpw_error", "Error in creating new password,Please try again later!");
				model.addAttribute("userID", pfnumber);
				return "setpassword";
			}
		}
		return adminLogin(model);
	}

	////// ENd tulasi added on 03/04/2018 - change password //////////

	@RequestMapping(value = "/generateMenuExcel", method = RequestMethod.GET)
	public String generateExcel(HttpServletRequest request, HttpServletResponse response, Model model)
			throws ServletException, IOException {

		System.out.println("generateMenuExcel<TxnExcel-admin> => ");

		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
		ServletContext servletContext = request.getSession().getServletContext();

		List<RoleModule> menuList = new ArrayList<RoleModule>();
		List<String> statusList = new ArrayList<String>();

		RoleModuleExample roleModuleExample = new RoleModuleExample();
		RoleModuleExample.Criteria roleModuleExampleCriteria = roleModuleExample.createCriteria();

		menuList = roleModuleMapper.getRoleModuleMenu(roleModuleExample);

		/*
		 *
		 * Collections.sort(menuList, new Comparator<RoleModule>() {
		 *
		 * @Override public int compare(RoleModule o1, RoleModule o2) { return
		 * o1.getRecord_date().compareTo(o2.getRecord_date()); } });
		 *
		 * Collections.reverse(menuList);
		 *
		 * if (menuList.size() <= 0) { model.addAttribute("emptySearch",
		 * "No Record Found"); }
		 */
		model.addAttribute("reportResult", menuList);

		// start excel

		Random ran = new Random();
		int randomNo = ran.nextInt(6) + 5;

		DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHMMSS");
		Date date = new Date();

		String file = "EtiqaTransactionReport" + dateFormat.format(date) + randomNo + ".xlsx";

		String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;
		// String absoluteDiskPath =
		// servletContext.getRealPath(relativeWebPath);

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Etiqa Transaction Report");
		// header fields based on product
		String[] fields = null;

		fields = new String[] { "Id", "Role Name", "Module Name", "Menu Name", "Access",

		};

		Row row1 = sheet.createRow(0);

		for (int cellIndex = 0; cellIndex < fields.length; cellIndex++) {
			Cell cell = row1.createCell(cellIndex);
			cell.setCellValue(fields[cellIndex]);
		}

		String[] fields2 = null;

		int i = 1;

		for (RoleModule roleModule : menuList) {

			// String s="1$5$";
			String s = roleModule.getPrmssnid();
			String s2 = "";
			String s3 = "";
			String s4 = "";
			String s5 = "";
			String s6 = "";
			String s7 = "";
			String val = "";

			String s1 = s.replace("$", ",");
			String[] test = s1.split(",");

			int numberOfItems = test.length;
			for (int j = 0; j < numberOfItems; j++) {
				String name = test[j];

				if (name.equals("0")) {
					s2 = "NO ACCESS";
				}

				if (name.equals("1")) {
					s2 = "FULL";
				}

				if (name.equals("2")) {
					s3 = "ADD";
				}
				if (name.equals("3")) {
					s4 = "UPDATE";
				}
				if (name.equals("4")) {
					s5 = "DELETE";
				}
				if (name.equals("5")) {
					s6 = "VIEW";
				}

			}
			if (!s2.equalsIgnoreCase("NO ACCESS")) {

				String tmpVal = "";

				if (roleModule.getMenuName().charAt(0) == '-') {
					tmpVal = roleModule.getMenuName().replaceFirst("-", " ").trim();
					if (tmpVal.charAt(0) == '-') {
						tmpVal = tmpVal.replaceFirst("-", " ").trim();
					}
				} else {
					tmpVal = roleModule.getMenuName().trim();
				}

				val = s7 + " " + s2 + " " + s3 + " " + s4 + " " + s5 + " " + s6;

				fields2 = new String[] { Integer.toString(i), roleModule.getRoleName(), roleModule.getModuleName(),
						tmpVal, val

				};

				Row row2 = sheet.createRow(i);

				for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
					Cell cell = row2.createCell(cellIndex);
					cell.setCellValue(fields2[cellIndex]);
				}

				i++;

			}
		}

		// System.out.println("txnStatus "+txn.getStatus());

		try {
			FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
			workbook.write(outputStream);
			workbook.close();

			File fileToDownload = new File(relativeWebPath);

			FileInputStream inputStream = new FileInputStream(fileToDownload);
			response.setContentType(servletContext.getMimeType(relativeWebPath));
			response.setContentLength((int) fileToDownload.length());

			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
			response.setHeader(headerKey, headerValue);

			// get output stream of the response
			OutputStream outStream = response.getOutputStream();

			byte[] buffer = new byte[4096];
			int bytesRead = -1;

			// write bytes read from the input stream into the output stream
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}

			outStream.flush();
			outStream.close();
			inputStream.close();

			if (fileToDownload.exists()) {

				boolean success = new File(relativeWebPath).delete();
				if (success) {
					System.out.println("The file " + file + " has been successfully deleted");
				}

			}

		} catch (FileNotFoundException e) {
			// response.getOutputStream().flush();
			// response.getOutputStream().close();

			e.printStackTrace();
		} catch (IOException e) {
			// response.getOutputStream().flush();
			// response.getOutputStream().close();
			e.printStackTrace();
		}

		return "uam/admin-system-group-profile";

	}

	// start by pramaiyan on 03072018

	@RequestMapping("/userAccessControlReport")
	public String roleAuditProfile(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String message = "Session Has Been Expired";
			model.addAttribute("error", message);
			return "redirect:/";
		}

		/*
		 * RoleModuleExample roleModuleExample = new RoleModuleExample();
		 *
		 * try{ if (null != request.getParameter("fromdate") && null !=
		 * request.getParameter("todate")) { if
		 * (!request.getParameter("fromdate").isEmpty() &&
		 * !request.getParameter("todate").isEmpty()) {
		 *
		 * DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); String
		 * fromDate = request.getParameter("fromdate"); String toDate =
		 * request.getParameter("todate"); String str_fromDate = fromDate + " 00:00:00";
		 * String str_toDate = toDate + " 23:59:59"; Date date1 =
		 * formatter.parse(str_fromDate); Date date2 = formatter.parse(str_toDate);
		 *
		 * RoleModuleExample.Criteria roleModuleCriteria =
		 * roleModuleExample.createCriteria();
		 * roleModuleCriteria.andCreateDateBetween(date1,date2);
		 *
		 * } } }//try end catch(Exception e) {
		 * System.out.print("Exception In generateUserAccessControlReport"+e);
		 *
		 * }
		 */

		/*
		 * List<AuditRole> auditRoleList = new ArrayList<AuditRole>(); auditRoleList =
		 * auditRoleMapper.selectByExample(auditRoleExample);
		 * model.addAttribute("auditRoleList", auditRoleList);
		 */

		return "uam/user-access-control-report";
	}

	// *************************************************************Start
	// *******************************************************************
	@RequestMapping(value = "/generateUserAccessControlReportAction", method = RequestMethod.GET)
	public String generateUserAccessControlReport(HttpServletRequest request, HttpServletResponse response, Model model)
			throws ServletException, IOException {

		System.out.println("generateUserAccessControlReport<TxnExcel-admin> => ");

		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
		ServletContext servletContext = request.getSession().getServletContext();

		List<RoleModule> userAceessControlList = new ArrayList<RoleModule>();

		List<String> statusList = new ArrayList<String>();

		RoleModuleExample roleModuleExample = new RoleModuleExample();

		RoleModule rolemd = new RoleModule();
		try {
			if (null != request.getParameter("fromdate") && !request.getParameter("fromdate").isEmpty()) {

				DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
				String fromDate = request.getParameter("fromdate");
				String str_fromDate = fromDate;// + " 00:00:00";
				Date date1 = formatter.parse(str_fromDate);
				// rolemd.setCreateDate(date1);
				rolemd.setCreateFromDate(str_fromDate);
				System.out.println("Date :::::  Size " + date1);
			}

		} // try end
		catch (Exception e) {
			System.out.print("Exception In generateUserAccessControlReport" + e);

		}
		try {
			/*
			 * DateFormat formatter1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss"); String
			 * str_fromDate = "13-07-2018" + " 00:00:00"; Date date2 =
			 * formatter1.parse(str_fromDate);
			 * System.out.println("Date :::::  Size "+date2); rolemd.setCreateDate(date2);
			 */
			userAceessControlList = roleModuleMapper.getUserAccessControlReport(rolemd);
			model.addAttribute("userAccessReporttResult", userAceessControlList);
			System.out.println("userAceessControlList Size " + userAceessControlList.size());
			System.out.println("userAceessControlList Value  " + userAceessControlList);
		} catch (Exception e) {
			System.out.print("Exception In generateUserAccessControlReport" + e);

		}
		// declare the HashMap
		HashMap<Integer, String> mapRoleName = new HashMap<>();
		// put contents to our HashMap
		List<String> rolenameList = new ArrayList<String>();
		List<String> roleNameListForxls = new ArrayList<String>();
		List<String> staticList = new ArrayList<String>();
		staticList.add("No");
		// staticList.add("User Name");
		staticList.add("Main Menu");
		staticList.add("Menu Name");

		for (RoleModule roleModule : userAceessControlList) {
			rolenameList.add(roleModule.getRoleName());
			roleNameListForxls.add(roleModule.getRoleName());
		}
		List<String> listFinal = new ArrayList<String>();

		listFinal.addAll(staticList);
		listFinal.addAll(rolenameList);
		System.out.println("staticList ::::: " + staticList);
		System.out.println("RolenameList ::::: " + rolenameList);
		System.out.println("listFinal ::::: " + listFinal);

		// repeated elements will automatically filtered.
		Set<String> rolenNameWithoutDuplicates = new LinkedHashSet<String>(listFinal);

		System.out.println("rolenNameWithoutDuplicates ::::: " + rolenNameWithoutDuplicates);
		// now let's clear the ArrayList so that we can copy all elements from
		// LinkedHashSet primes.clear();
		listFinal.clear();
		// copying elements but without any duplicates
		listFinal.addAll(rolenNameWithoutDuplicates);

		// start excel
		if (userAceessControlList.size() > 0) {
			System.out.println("Calling Excel Download File Start");
			Random ran = new Random();
			int randomNo = ran.nextInt(6) + 5;

			DateFormat dateFormat = new SimpleDateFormat("DDMMYYYYHHMMSS");
			Date date = new Date();

			String file = "EtiqaUserAccessControlReport" + dateFormat.format(date) + randomNo + ".xlsx";

			String relativeWebPath = getClass().getClassLoader().getResource("").getPath() + file;

			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Etiqa User Access Control Report");

			Row row1 = sheet.createRow(0);
			// setting Tittle
			for (int cellIndex = 0; cellIndex < listFinal.size(); cellIndex++) {
				Cell cell = row1.createCell(cellIndex);
				cell.setCellValue(listFinal.get(cellIndex));
				if (cellIndex > 2) {

					String rolname = workbook.getSheetAt(0).getRow(0).getCell(cellIndex).toString();
					mapRoleName.put(cellIndex, rolname);

				}
			}

			String[] fields2 = null;

			int i = 1;

			for (RoleModule roleModule : userAceessControlList) {

				// String s="1$5$";
				String s = roleModule.getPrmssnid();
				String tmps1 = "";
				String s2 = "";
				String s3 = "";
				String s4 = "";
				String s5 = "";
				String s6 = "";
				String s7 = "";
				String tickMark = "";
				String permissionAccess = "";

				String s1 = s.replace("$", ",");
				String[] test = s1.split(",");

				int numberOfItems = test.length;
				// if access permission set more than one
				if (numberOfItems >= 2) {

					for (int j = 0; j < numberOfItems; j++) { // loop continiue till numberOfItems
						String name = test[j];

						if (name.equals("0")) {
							tmps1 = "NO ACCESS";
						}

						if (name.equals("1")) {
							s3 = "";
							s4 = "";
							s5 = "";
							s6 = "";
							s2 = "FULL";
							tickMark = "√";

						}

						if (name.equals("2")) {
							s2 = "";
							s4 = "";
							s5 = "";
							s6 = "";
							s3 = "ADD";
							tickMark = "√";
						}
						if (name.equals("3")) {
							s3 = "";
							s2 = "";
							s5 = "";
							s6 = "";
							s4 = "UPDATE";
							tickMark = "√";
						}
						if (name.equals("4")) {
							s3 = "";
							s4 = "";
							s2 = "";
							s6 = "";
							s5 = "DELETE";
							tickMark = "√";
						}
						if (name.equals("5")) {
							s3 = "";
							s4 = "";
							s5 = "";
							s2 = "";
							s6 = "VIEW";
							tickMark = "√";
						}

						if (!tmps1.equalsIgnoreCase("NO ACCESS") || tmps1.equalsIgnoreCase("")) {

							String parentMenu = "";
							if (null != roleModule.getParentmenu()) {
								if (roleModule.getParentmenu().charAt(0) == '-') {
									parentMenu = roleModule.getParentmenu().replaceFirst("-", " ").trim();
									if (parentMenu.charAt(0) == '-') {
										parentMenu = parentMenu.replaceFirst("-", " ").trim();
									}
								} else {
									parentMenu = roleModule.getParentmenu().trim();
								}
							}
							// write excel
							permissionAccess = s7 + s2 + s3 + s4 + s5 + s6;
							// fields2 = new String[] { Integer.toString(i),
							// roleModule.getUserName(),parentMenu,roleModule.getMenuName().concat("-"+permissionAccess),
							// tickMark}; //preparing data to write in excel file
							// fields2 = new String[] {
							// Integer.toString(i),parentMenu,roleModule.getMenuName().concat("-"+permissionAccess),
							// tickMark}; //preparing data to write in excel file
							fields2 = new String[] { Integer.toString(i), parentMenu,
									permissionAccess.concat("-" + roleModule.getMenuName()), tickMark }; // preparing
																											// data to
																											// write in
																											// excel
																											// file

							Row row2 = sheet.createRow(i);// create a new row
							CellStyle cellStyle = row2.getSheet().getWorkbook().createCellStyle();
							// CellStyle cellStyleWrap = row2.getSheet().getWorkbook().createCellStyle();
							cellStyle.setAlignment(CellStyle.ALIGN_CENTER);

							// cellStyleWrap.setWrapText(false);
							XSSFCellStyle style = workbook.createCellStyle();
							style.setBorderBottom(CellStyle.BORDER_THIN);
							style.setBorderTop(CellStyle.BORDER_THIN);
							style.setBorderRight(CellStyle.BORDER_THIN);
							style.setBorderLeft(CellStyle.BORDER_THIN);

							for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
								Cell cell = row2.createCell(cellIndex);
								if (cellIndex != 3) {
									cell.setCellValue(fields2[cellIndex]);
									cell.setCellStyle(style);

									// cell.setCellStyle(cellStyleWrap);

								}
								Iterator iter = mapRoleName.entrySet().iterator();
								while (iter.hasNext()) {
									Map.Entry entry = (Map.Entry) iter.next();

									int key = Integer.valueOf((int) entry.getKey());
									String value = entry.getValue().toString();
									String userRole = roleModule.getRoleName();
									// System.out.println("userRole"+userRole);

									if (userRole.toUpperCase().equals(value.toUpperCase())) { // here need to loop

										// create a groupHeaderrow row

										Cell cell1 = row2.createCell(key);
										cell1.setCellValue(fields2[3]);
										cell1.setCellStyle(cellStyle);
									}

								} // while end

							} // for end

							i++;
						}

					} // end for loop till numberOfItems
				} // if end for numberOfItems
				else {
					String name = test[0];

					if (name.equals("0")) {

						tmps1 = "NO ACCESS";
					}

					if (name.equals("1")) {
						s3 = "";
						s4 = "";
						s5 = "";
						s6 = "";
						s2 = "FULL";
						tickMark = "√";
					}

					if (name.equals("2")) {
						s2 = "";
						s4 = "";
						s5 = "";
						s6 = "";
						s3 = "ADD";
						tickMark = "√";
					}
					if (name.equals("3")) {
						s3 = "";
						s2 = "";
						s5 = "";
						s6 = "";
						s4 = "UPDATE";
						tickMark = "√";
					}
					if (name.equals("4")) {
						s3 = "";
						s4 = "";
						s2 = "";
						s6 = "";
						s5 = "DELETE";
						tickMark = "√";
					}
					if (name.equals("5")) {
						s3 = "";
						s4 = "";
						s5 = "";
						s2 = "";
						s6 = "VIEW";
						tickMark = "√";
					}
					if (!tmps1.equalsIgnoreCase("NO ACCESS") || tmps1.equalsIgnoreCase("")) {

						String parentMenu = "";
						if (null != roleModule.getParentmenu()) {
							if (roleModule.getParentmenu().charAt(0) == '-') {
								parentMenu = roleModule.getParentmenu().replaceFirst("-", " ").trim();
								if (parentMenu.charAt(0) == '-') {
									parentMenu = parentMenu.replaceFirst("-", " ").trim();
								}
							} else {
								parentMenu = roleModule.getParentmenu().trim();
							}
						}
						permissionAccess = s7 + s2 + s3 + s4 + s5 + s6;
						// fields2 = new String[] { Integer.toString(i),
						// roleModule.getUserName(),parentMenu,roleModule.getMenuName().concat("-"+permissionAccess),
						// tickMark}; //preparing data to write in excel file
						fields2 = new String[] { Integer.toString(i), parentMenu,
								permissionAccess.concat("-" + roleModule.getMenuName()), tickMark }; // preparing data
																										// to write in
																										// excel file

						Row row2 = sheet.createRow(i);

						// modified

						CellStyle cellStyle = row2.getSheet().getWorkbook().createCellStyle();
						// CellStyle cellStyleWrap = row2.getSheet().getWorkbook().createCellStyle();
						// cellStyleWrap.setWrapText(false);
						cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
						XSSFCellStyle style = workbook.createCellStyle();
						style.setBorderBottom(CellStyle.BORDER_THIN);
						style.setBorderTop(CellStyle.BORDER_THIN);
						style.setBorderRight(CellStyle.BORDER_THIN);
						style.setBorderLeft(CellStyle.BORDER_THIN);
						for (int cellIndex = 0; cellIndex < fields2.length; cellIndex++) {
							Iterator iter = mapRoleName.entrySet().iterator();
							Cell cell = row2.createCell(cellIndex);
							if (cellIndex != 3) {
								cell.setCellValue(fields2[cellIndex]);
								cell.setCellStyle(style);

								// cell.setCellStyle(cellStyleWrap);
							}
							while (iter.hasNext()) {
								Map.Entry entry = (Map.Entry) iter.next();

								int key = Integer.valueOf((int) entry.getKey());
								String value = entry.getValue().toString();
								String userRole = roleModule.getRoleName();
								// System.out.println("userRole"+userRole);

								if (userRole.toUpperCase().equals(value.toUpperCase())) { // here need to loop

									Cell cell1 = row2.createCell(key);
									cell1.setCellValue(fields2[3]);
									cell1.setCellStyle(cellStyle);
								}

							} // while end

						} // for end

						// End Modified
						i++;
					}
				} // else for numberOfItems end

			} // main for end

			// System.out.println("txnStatus "+txn.getStatus());

			try {
				System.out.println("Calling Excel Writing Function");
				FileOutputStream outputStream = new FileOutputStream(relativeWebPath);
				workbook.write(outputStream);
				workbook.close();

				File fileToDownload = new File(relativeWebPath);

				FileInputStream inputStream = new FileInputStream(fileToDownload);
				response.setContentType(servletContext.getMimeType(relativeWebPath));
				response.setContentLength((int) fileToDownload.length());

				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"", fileToDownload.getName());
				response.setHeader(headerKey, headerValue);

				// get output stream of the response
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				// write bytes read from the input stream into the output stream
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}

				outStream.flush();
				outStream.close();
				inputStream.close();

				if (fileToDownload.exists()) {

					boolean success = new File(relativeWebPath).delete();
					if (success) {
						System.out.println("The file " + file + " has been successfully deleted");
					}

				}

			} catch (FileNotFoundException e) {
				// response.getOutputStream().flush();
				// response.getOutputStream().close();

				e.printStackTrace();
			} catch (IOException e) {
				// response.getOutputStream().flush();
				// response.getOutputStream().close();
				e.printStackTrace();
			} finally {
				System.out.println("Calling Finally ");
			}
		} // if end for userAceessControlList
		return "uam/user-access-control-report";

	}

	// End by Pramaiyan
	// *************************************************************End
	// *******************************************************************

	/*****************************************************
	 * Private Methods
	 *******************************************/
	private void unauthorizedUser(String username) {
		String flag = "1";
		UserLog ulog = new UserLog();
		ulog.setLoginTime(new Date());
		ulog.setLogDate(new Date());
		ulog.setFullName(username);
		ulog.setLoginFlag(flag);
		userLogMapper.insertfalseLogin(ulog);
	}
}
