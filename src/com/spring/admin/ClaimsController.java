package com.spring.admin;

import java.io.File;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.etiqa.DAO.UserValidation_DAO;
import com.etiqa.admin.User;
import com.etiqa.utils.SecurityUtil;
import com.spring.VO.ClaimsForm;
import com.spring.VO.ClaimsFormExample;
import com.spring.VO.Menu;
import com.spring.mapper.ClaimsFormMapper;
import com.spring.service.MenuService;
import com.spring.utils.ServiceValidationUtils;

@Controller
@RequestMapping("claims")
public class ClaimsController {
	@Autowired
	MenuService menuService;
	ClaimsFormMapper claimsFormMapper;

	@Autowired
	public ClaimsController(ClaimsFormMapper claimsFormMapper

	) {
		this.claimsFormMapper = claimsFormMapper;

	}

	@RequestMapping("/claimLogin")
	public String adminLogin(HttpServletRequest request, Model model) {

		HttpSession session = request.getSession();

		// Added by prmaiyan start

		String userId = null;
		String role = null;
		String ModuleId = null;
		List<Menu> firstList = new ArrayList<Menu>();
		List<Menu> secondList = new ArrayList<Menu>();
		List<Menu> thirdList = new ArrayList<Menu>();
		List<Menu> fourthList = new ArrayList<Menu>();
		String uamReqParam = request.getParameter("id");
		session.setAttribute("uamReqParamSession", uamReqParam);

		System.out.println("Encript UAM_Req_Param Outer >>>> " + uamReqParam);
		if (uamReqParam != null) {
			System.out.println("Encript UAM_Req_Param  In >>>> " + uamReqParam);
			String decryptMsg = SecurityUtil.decriptString(uamReqParam);
			System.out.println("Encript UAM_Req_Param " + decryptMsg);
			// split the String for getting userId,roleId,ModuleId
			if (decryptMsg != null) {
				String[] items = decryptMsg.split("\\$");
				userId = items[0].toString();
				role = items[1].toString();
				ModuleId = items[2].toString();

				session.setAttribute("moduleId", Integer.parseInt(ModuleId));
				System.out.println("User Id :" + userId);
				System.out.println("Role Id :" + role);
				System.out.println("ModuleId :" + ModuleId);

			}

		} else {
			System.out.println("Encript UAM_Req_Param else  >>>> " + uamReqParam);
		}

		System.out.println("Original " + role);

		User muser = new User();

		muser = UserValidation_DAO.getvalidateByUserID(userId);

		session.setAttribute("FullName", muser.getFullName());
		session.setAttribute("logedUser", userId);
		int uId = muser.getID();
		session.setAttribute("user", String.valueOf(uId));
		System.out.println("Full Name  : " + muser.getFullName());

		String loginUser = (String) session.getAttribute("FullName");

		// get Menu List from Rest service start
		System.out.println("Calling DashBoard Controller Pramaiyan");
		Map<String, List<Menu>> map = getMenu(role, ModuleId);

		for (Entry<String, List<Menu>> entry : map.entrySet()) {
			String key = entry.getKey();
			List<Menu> values = entry.getValue();

			if (key == "1") {
				firstList.addAll(values);
			}
			if (key == "2") {
				secondList.addAll(values);
			}
			if (key == "3") {
				thirdList.addAll(values);
			}
			if (key == "4") {
				fourthList.addAll(values);
			}
		}

		session.setAttribute("level_1", firstList);
		session.setAttribute("level_2", secondList);
		session.setAttribute("level_3", thirdList);
		session.setAttribute("level_4", fourthList);

		// Added by prmaiyan END

		return "claim/pages/admin-claim-landing";

	}

	/*
	 * @RequestMapping("/adminLogout") public String adminLogout(HttpServletRequest
	 * request, Model model) {
	 *
	 * request.getSession().removeAttribute("user");
	 * request.getSession().removeAttribute("FullName");
	 * request.getSession().invalidate(); return "admin-login"; }
	 */

	// List All Cyber Agents
	@SuppressWarnings("unused")
	@RequestMapping(value = "/getKhairat", method = RequestMethod.GET)
	public String list(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();

		/*
		 * HttpSession session = request.getSession(); String loginUser = (String)
		 * session.getAttribute("user"); String userRole = (String)
		 * session.getAttribute("userRole"); session.setAttribute("loginUser",
		 * loginUser); session.setAttribute("userRole", userRole);
		 *
		 * System.out.println("ClaimsController<loginUser> => "+loginUser);
		 * System.out.println("ClaimsController<userRole> => "+userRole);
		 *
		 * if (loginUser == null || !"2".equalsIgnoreCase(userRole) ) {
		 * System.out.println("ClaimsController<getKhairat> => If block...!"); String
		 * sessionexpired = "Session Has Been Expired";
		 * model.addAttribute("sessionexpired", sessionexpired); return "admin-login"; }
		 */

		// Added by prmaiyan start
		String jobRole=request.getParameter("pid");
		System.out.println(jobRole+"jobRole");
		
		if(jobRole!=null && !jobRole.isEmpty()){
			jobRole = String.valueOf(jobRole.charAt(0));
			session.setAttribute("jobRolesession", jobRole);
		}else{
			 jobRole=(String)session.getAttribute( "jobRolesession" );
		}
		System.out.println(jobRole+"jobRole only in Khairat");
		String userId = null;
		String role = null;
		String ModuleId = null;
		List<Menu> firstList = new ArrayList<Menu>();
		List<Menu> secondList = new ArrayList<Menu>();
		List<Menu> thirdList = new ArrayList<Menu>();
		List<Menu> fourthList = new ArrayList<Menu>();
		String uamReqParam = request.getParameter("id");
		session.setAttribute("uamReqParamSession", uamReqParam);

		System.out.println("Encript UAM_Req_Param Outer >>>> " + uamReqParam);
		if (uamReqParam != null) {
			System.out.println("Encript UAM_Req_Param  In >>>> " + uamReqParam);
			String decryptMsg = SecurityUtil.decriptString(uamReqParam);
			System.out.println("Encript UAM_Req_Param " + decryptMsg);
			// split the String for getting userId,roleId,ModuleId
			if (decryptMsg != null) {
				String[] items = decryptMsg.split("\\$");
				userId = items[0].toString();
				role = items[1].toString();
				ModuleId = items[2].toString();

				System.out.println("User Id :" + userId);
				System.out.println("Role Id :" + role);
				System.out.println("ModuleId :" + ModuleId);
				session.setAttribute("userIdsession", userId);
				session.setAttribute("Rolesession", role);
				session.setAttribute("ModuleIdsession", ModuleId);

			}

		} else if(session!=null){
			userId = (String)session.getAttribute( "userIdsession" );
			role =(String)session.getAttribute( "Rolesession" );
			ModuleId =(String)session.getAttribute( "ModuleIdsession" );

		} else {
			System.out.println("Encript UAM_Req_Param else  >>>> " + uamReqParam);
		}

		System.out.println("Original " + role);

		User muser = new User();

		muser = UserValidation_DAO.getvalidateByUserID(userId);

		session.setAttribute("FullName", muser.getFullName());
		session.setAttribute("logedUser", userId);
		int uId = muser.getID();
		session.setAttribute("user", String.valueOf(uId));
		System.out.println("Full Name  : " + muser.getFullName());

		String loginUser = (String) session.getAttribute("FullName");

		// get Menu List from Rest service start
		System.out.println("Calling DashBoard Controller Pramaiyan");
		Map<String, List<Menu>> map = getMenu(role, ModuleId);

		for (Entry<String, List<Menu>> entry : map.entrySet()) {
			String key = entry.getKey();
			List<Menu> values = entry.getValue();

			if (key == "1") {
				firstList.addAll(values);
			}
			if (key == "2") {
				secondList.addAll(values);
			}
			if (key == "3") {
				thirdList.addAll(values);
			}
			if (key == "4") {
				fourthList.addAll(values);
			}
		}

		session.setAttribute("level_1", firstList);
		session.setAttribute("level_2", secondList);
		session.setAttribute("level_3", thirdList);
		session.setAttribute("level_4", fourthList);

		// Added by prmaiyan END
		List<ClaimsForm> claimsFormList = new ArrayList<ClaimsForm>();
		ClaimsFormExample claimsFormExample = new ClaimsFormExample();
		claimsFormExample.setOrderByClause("dcf.ID desc");
		claimsFormList = claimsFormMapper.selectByExample(claimsFormExample);
		model.addAttribute("claimsFormList", claimsFormList);
		return "claim/pages/admin-view-list-claims";
	}

	/*-------------------------------------------------------Start- Search Claims--------------------------------------------------------*/
	@RequestMapping(value = "/searchClaims", method = RequestMethod.POST)
	public String searchClaims(HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String nric = null;
		String dateFrom = null;
		String dateTo = null;
		String mobile = null;

		session.setAttribute("mobile", request.getParameter("mobile").trim());
		session.setAttribute("nric", request.getParameter("nric").trim());
		session.setAttribute("dateFrom", request.getParameter("dateFrom").trim());
		session.setAttribute("dateTo", request.getParameter("dateTo").trim());

		mobile = (String) session.getAttribute("mobile");
		nric = (String) session.getAttribute("nric");
		dateFrom = (String) session.getAttribute("dateFrom");
		dateTo = (String) session.getAttribute("dateTo");

		SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/yyyy");

		Calendar c = Calendar.getInstance();
		Calendar c1 = Calendar.getInstance();

		List<ClaimsForm> claimsFormList = new ArrayList<ClaimsForm>();

		ClaimsFormExample claimsFormExample = new ClaimsFormExample();
		ClaimsFormExample.Criteria claim_criteria = claimsFormExample.createCriteria();
		claimsFormExample.setOrderByClause("dcf.ID desc");
		if (!ServiceValidationUtils.isEmptyStringTrim(dateFrom) && !ServiceValidationUtils.isEmptyStringTrim(dateTo)) {
			Date datefrom = null;
			Date dateto = null;
			Date datefromsearch = null;
			Date datetosearch = null;
			try {
				datefrom = format2.parse(dateFrom);
				dateto = format2.parse(dateTo);
				c.setTime(datefrom);
				// c.add(Calendar.DATE, -1);
				String output = format2.format(c.getTime());
				datefromsearch = format2.parse(output);
				c1.setTime(dateto);
				c1.add(Calendar.DATE, 1);
				String output1 = format2.format(c1.getTime());
				datetosearch = format2.parse(output1);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				datefrom = null;
				dateto = null;
				e1.printStackTrace();
			}
			System.out.println(datefromsearch);
			System.out.println(datetosearch);
			if (datefromsearch != null && datetosearch != null) {
				claim_criteria.andCreateDateBetween(datefromsearch, datetosearch);
			}
		}

		if (!ServiceValidationUtils.isEmptyStringTrim(mobile)) {
			claim_criteria.andClaimtMobileEqualTo(mobile);
		}
		if (!ServiceValidationUtils.isEmptyStringTrim(nric)) {
			claim_criteria.andCustNricEqualTo(nric);
		}
		claimsFormList = claimsFormMapper.selectByExample(claimsFormExample);

		model.addAttribute("claimsFormList", claimsFormList);

		return "claim/pages/admin-view-list-claims";
	}
	/*-------------------------------------------------------End- Search Claims--------------------------------------------------------*/

	/**
	 * ------------------------------------------------------------------------------------------------------------------------------------
	 **/

	/**
	 * -----------------------------------------------------------------------------------------------------------------------------------
	 **/

	/*-------------------------------------------------------Start- Update Claims------------------------------------------------------------*/
	@RequestMapping(value = "/updateClaims", method = RequestMethod.POST)
	public String updateAgent(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam String id) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		System.out.println(id);
		ClaimsForm claimsForm = new ClaimsForm();
		claimsForm = claimsFormMapper.selectByPrimaryKey(Short.valueOf(id));

		Format formatter = new SimpleDateFormat("dd/MM/yyyy");
		String s = formatter.format(claimsForm.getCreateDate());

		System.out.println("Date Original  " + claimsForm.getCreateDate());
		System.out.println("DateFormated  " + s);

		model.addAttribute("claimsForm", claimsForm);

		model.addAttribute("s", s);

		return "claim/pages/admin-view-list-claims-edit";
	}

	@SuppressWarnings("null")
	@RequestMapping(value = "/updateClaimsDone", method = RequestMethod.POST)
	public String updateAgentDone(@ModelAttribute(value = "ClaimsForm") ClaimsForm claimsForm, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, Model model) {

		HttpSession session = request.getSession();
		String loginUser = (String) session.getAttribute("user");
		if (loginUser == null) {
			String sessionexpired = "Session Has Been Expired";
			model.addAttribute("sessionexpired", sessionexpired);
			return "admin-login";
		}

		String id = request.getParameter("id");
		claimsForm.setId(Short.valueOf(id));

		claimsFormMapper.updateByPrimaryKeySelective(claimsForm);
		model.addAttribute("claimsUpdatedMessage", "Claims Updated Successfully!");
		return listAfterUpdate(request, response, model);
	}

	@SuppressWarnings("unused")
	public String listAfterUpdate(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();

		
		// Added by prmaiyan start
		String jobRole=null;
			 jobRole=(String)session.getAttribute( "jobRolesession" );
		System.out.println(jobRole+"jobRole only in Khairat");
		
		String userId = null;
		String role = null;
		String ModuleId = null;
		List<Menu> firstList = new ArrayList<Menu>();
		List<Menu> secondList = new ArrayList<Menu>();
		List<Menu> thirdList = new ArrayList<Menu>();
		List<Menu> fourthList = new ArrayList<Menu>();
		String uamReqParam = (String)session.getAttribute("uamReqParamSession");
		session.setAttribute("uamReqParamSession", uamReqParam);

		System.out.println("Encript UAM_Req_Param Outer >>>> " + uamReqParam);
		if (uamReqParam != null) {
			System.out.println("Encript UAM_Req_Param  In >>>> " + uamReqParam);
			String decryptMsg = SecurityUtil.decriptString(uamReqParam);
			System.out.println("Encript UAM_Req_Param " + decryptMsg);
			// split the String for getting userId,roleId,ModuleId
			if (decryptMsg != null) {
				String[] items = decryptMsg.split("\\$");
				userId = items[0].toString();
				role = items[1].toString();
				ModuleId = items[2].toString();

				System.out.println("User Id :" + userId);
				System.out.println("Role Id :" + role);
				System.out.println("ModuleId :" + ModuleId);
				session.setAttribute("userIdsession", userId);
				session.setAttribute("Rolesession", role);
				session.setAttribute("ModuleIdsession", ModuleId);

			}

		} else if(session!=null){
			userId = (String)session.getAttribute( "userIdsession" );
			role =(String)session.getAttribute( "Rolesession" );
			ModuleId =(String)session.getAttribute( "ModuleIdsession" );

		} else {
			System.out.println("Encript UAM_Req_Param else  >>>> " + uamReqParam);
		}

		System.out.println("Original " + role);

		User muser = new User();

		muser = UserValidation_DAO.getvalidateByUserID(userId);

		session.setAttribute("FullName", muser.getFullName());
		session.setAttribute("logedUser", userId);
		int uId = muser.getID();
		session.setAttribute("user", String.valueOf(uId));
		System.out.println("Full Name  : " + muser.getFullName());

		String loginUser = (String) session.getAttribute("FullName");

		// get Menu List from Rest service start
		System.out.println("Calling DashBoard Controller Pramaiyan");
		Map<String, List<Menu>> map = getMenu(role, ModuleId);

		for (Entry<String, List<Menu>> entry : map.entrySet()) {
			String key = entry.getKey();
			List<Menu> values = entry.getValue();

			if (key == "1") {
				firstList.addAll(values);
			}
			if (key == "2") {
				secondList.addAll(values);
			}
			if (key == "3") {
				thirdList.addAll(values);
			}
			if (key == "4") {
				fourthList.addAll(values);
			}
		}

		session.setAttribute("level_1", firstList);
		session.setAttribute("level_2", secondList);
		session.setAttribute("level_3", thirdList);
		session.setAttribute("level_4", fourthList);

		// Added by prmaiyan END
		List<ClaimsForm> claimsFormList = new ArrayList<ClaimsForm>();
		ClaimsFormExample claimsFormExample = new ClaimsFormExample();
		claimsFormExample.setOrderByClause("dcf.ID desc");
		claimsFormList = claimsFormMapper.selectByExample(claimsFormExample);
		model.addAttribute("claimsFormList", claimsFormList);
		return "claim/pages/admin-view-list-claims";
	}
	@RequestMapping(value = "/DownloadFile", method = RequestMethod.GET)
	public void DownloadFile(HttpServletRequest request, HttpServletResponse response, Model model) {

		String id = request.getParameter("id");
		System.out.println("id: " + id);
		ClaimsForm claimsForm = new ClaimsForm();
		claimsForm = claimsFormMapper.selectByPrimaryKey(Short.valueOf(id));
		// file1.zip, file2.zip
		String fileName = claimsForm.getFilepath();
		File f = new File(fileName);
		System.out.println("File Name: " + fileName);
		try {
			byte[] fileData = null;
			if (claimsForm.getFiledoc() != null) {
				fileData = claimsForm.getFiledoc();

			}
			// abc.txt => text/plain
			// abc.zip => application/zip
			// abc.pdf => application/pdf
			String contentType = new MimetypesFileTypeMap().getContentType(f);
			System.out.println("Content Type: " + contentType);

			response.setHeader("Content-Type", contentType);
			response.setHeader("Content-Disposition", "inline; filename=\"" + claimsForm.getFilepath() + "\"");
			response.getOutputStream().write(fileData);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private Map<String, List<Menu>> getMenu(String roleid, String moduleId) {
		System.out.println("roleid >>>" + roleid);
		System.out.println("moduleId >>>> " + moduleId);
		Menu m = new Menu();
		RestTemplate rt = new RestTemplate();
		MultiValueMap<String, Object> mapInput = new LinkedMultiValueMap<String, Object>();
		Map<String, List<Menu>> resp = new HashMap<String, List<Menu>>();
		// String roleid="6";
		String delimiters = "$";
		// String moduleId="1";
		try {
			resp = menuService.getMenu(roleid + delimiters + moduleId);
			System.out.println(resp);
			m.setModuleId(Integer.parseInt(moduleId));
			System.out.println("resp  " + resp + "\n");
			System.out.println("Module Id  : " + m.getModuleId());
			System.out.println("-------------------------------");
			List<Menu> firstList = resp.get("first");
			List<Menu> secondList = resp.get("second");
			List<Menu> thirdList = resp.get("third");
			List<Menu> fourthList = resp.get("fourth");

			resp.put("1", firstList);
			resp.put("2", secondList);
			resp.put("3", thirdList);
			resp.put("4", fourthList);

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("no Connection");
		}
		return resp;
	}
}
