package com.spring.mapper;

import org.apache.ibatis.annotations.Param;

import com.spring.VO.DashBoard;
import com.spring.VO.DashBoard_BSummary;
import com.spring.VO.DashBoard_Entity;
import com.spring.VO.DashBoard_Renewal;

public interface DashBoardMapper {

	void selectState(@Param("record") DashBoard record);

	void selectQQ1(@Param("record") DashBoard record);

	void selectDashBoard1(@Param("record") DashBoard record);

	void select_B_Sum(@Param("record") DashBoard_BSummary record);

	void select_Ren_Ret1(@Param("record") DashBoard_Renewal record);

	void select_Entity(@Param("record") DashBoard_Entity record);

	void select_SF(@Param("record") DashBoard record);

	void select_A_VS_B(@Param("record") DashBoard record);

	void select_AgntPer(@Param("record") DashBoard record);

	void select_CSR(@Param("record") DashBoard record);

	void select_M_A_R(@Param("record") DashBoard record);

	void select_S_Lead(@Param("record") DashBoard record);

	void select__Aband(@Param("record") DashBoard record);

}
