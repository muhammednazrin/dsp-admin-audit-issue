package com.spring.mapper.roadtax;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.etiqa.common.DB.ConnectionFactory;

import oracle.jdbc.OracleTypes;

public class ConsignmentDAO {

	Connection connection = null;
	Statement statement = null;
	ResultSet rs = null;
	ResultSet rs1 = null;

	public ConsignmentRecord getallConsignmentRecord(String QQID) {
		ConsignmentRecord ConRe = new ConsignmentRecord();
		try {
			connection = ConnectionFactory.getConnection();
			CallableStatement cstmt = connection.prepareCall("{CALL DSP_ADM_SP_CONSIGNMENT_RECORD(?,?,?,?)}");
			// DSP_HOHH_SP_TOTALINFO_test
			cstmt.setString(1, QQID);
			cstmt.registerOutParameter(2, OracleTypes.CURSOR);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
			cstmt.execute();

			// String result = cstmt.getString(4);

			rs = (ResultSet) cstmt.getObject(2);
			rs1 = (ResultSet) cstmt.getObject(3);
			// rs = (ResultSet)cstmt.getObject("c_CusInfo");
			// rs1 = (ResultSet)cstmt.getObject("c_Routingcode");

			while (rs.next()) {
				System.out.println("customerName" + rs.getString("customerName"));
				ConRe.setCUSTOMER_NAME(rs.getString("customerName"));
				ConRe.setAddress1(rs.getString("address1"));
				ConRe.setAddress2(rs.getString("address2"));
				ConRe.setAddress3(rs.getString("address3"));
				ConRe.setPostcode(rs.getString("postcode"));
				ConRe.setMobile(rs.getString("mobile"));
				ConRe.setTrackingNo(rs.getString("trackingNo"));
			}

			while (rs1.next()) {
				ConRe.setRoutingCode(rs1.getString("Routing_Code"));
				System.out.println(rs1.getString("Routing_Code"));
			}

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);

		}
		return ConRe;
	}

}
