package com.spring.mapper.roadtax;

import java.io.IOException;
import java.sql.SQLException;

import net.sf.jasperreports.engine.JRException;

public class ConsignmentMain {

	public static void main(String[] args) throws ClassNotFoundException, JRException, IOException, SQLException {
		ConsignmentDAO dao = new ConsignmentDAO();
		ConsignmentRecord ConRe = dao.getallConsignmentRecord("15549");
		RoadTaxConsignmentJEXML jrxml = new RoadTaxConsignmentJEXML();
		jrxml.ConsignmentJEXML(ConRe);

	}

}
