package com.spring.mapper.roadtax;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.etiqa.common.DB.ConnectionFactory;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.ExporterInput;
import net.sf.jasperreports.export.OutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

public class RoadTaxConsignmentJEXML {

	public String ConsignmentJEXML(ConsignmentRecord FormVo)
			throws ClassNotFoundException, JRException, IOException, SQLException {
		// ConsignmentRecord FormVo=new ConsignmentRecord();
		// ConsignmentDAO dao=new ConsignmentDAO();
		// dao.getallConsignmentRecord("14985",FormVo);

		Map<String, Object> parameters = new HashMap<String, Object>();

		String fileName = FormVo.getTrackingNo() + ".pdf";
		String policyDocCode = "RCPTFORM";

		DateFormat dateFormat = new SimpleDateFormat("dd MMM YYYY ");
		Date date = new Date();
		String todaydate = dateFormat.format(date);
		System.out.println(todaydate);

		/*
		 * <parameter name="Account_No" class="java.lang.String"/> <parameter
		 * name="Entity_Name" class="java.lang.String"/> <parameter
		 * name="Sender_Address" class="java.lang.String"/> <parameter
		 * name="Sender_Area" class="java.lang.String"/> <parameter name="Sender_State"
		 * class="java.lang.String"/> <parameter name="Sender_Postcode"
		 * class="java.lang.String"/> <parameter name="Current_Date"
		 * class="java.lang.String"/> <parameter name="Receiver_Name"
		 * class="java.lang.String"/> <parameter name="Receiver_Address"
		 * class="java.lang.String"/> <parameter name="Receiver _Area"
		 * class="java.lang.String"/> <parameter name="Receiver _State"
		 * class="java.lang.String"/> <parameter name="Receiver _Postcode"
		 * class="java.lang.String"/> <parameter name="Receiver _Contact_Number"
		 * class="java.lang.String"/> <parameter name="Item" class="java.lang.String"/>
		 * <parameter name="Road_Tax_Value" class="java.lang.String"/> <parameter
		 * name="Consignment_Note_Number" class="java.lang.String"/> <parameter
		 * name="routing_code" class="java.lang.String"/>
		 */

		System.out.println(FormVo.getTrackingNo());
		System.out.println(FormVo.getCUSTOMER_NAME());
		System.out.println(FormVo.getAddress1());
		System.out.println(FormVo.getAddress2());
		System.out.println(FormVo.getAddress3());
		System.out.println(FormVo.getPostcode());
		System.out.println(FormVo.getMobile());
		System.out.println(FormVo.getRoutingCode());

		parameters.put("Account_No", "1234567890");
		parameters.put("Entity_Name", "Etiqa Insurance Berhad");
		parameters.put("Current_Date", todaydate);
		parameters.put("Receiver_Name", FormVo.getCUSTOMER_NAME());
		parameters.put("Receiver_Address", FormVo.getAddress1());
		parameters.put("Receiver_Area", FormVo.getAddress2());

		if (FormVo.getAddress3() != null) {
			parameters.put("Receiver_State", FormVo.getAddress3());
		} else {
			parameters.put("Receiver_State", "");
		}

		parameters.put("Receiver_Postcode", FormVo.getPostcode());

		parameters.put("Receiver_Contact_Number", FormVo.getMobile());
		parameters.put("Receiver_Contact_view", "*" + FormVo.getTrackingNo() + "*");
		parameters.put("Item", "");
		parameters.put("Road_Tax_Value", "");
		parameters.put("Consignment_Note_Number", FormVo.getTrackingNo());
		parameters.put("routing_code", "KUL-" + FormVo.getRoutingCode());

		try (InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("com/spring/mapper/roadtax/JrxmlTemplate/documents.properties")) {
			Properties prop = new Properties();
			prop.load(in);
			String jrxmlFile = prop.getProperty("Consignment");

			InputStream input = this.getClass().getClassLoader().getResourceAsStream(jrxmlFile);
			JasperReport jasperReport = JasperCompileManager.compileReport(input);
			JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
			// JasperExportManager.exportReportToPdfFile(print,
			// prop.getProperty("destinationPath")+fileName);
			Connection con = ConnectionFactory.getConnection();

			JRPdfExporter exporter = new JRPdfExporter();
			ExporterInput exporterInput = new SimpleExporterInput(print);
			exporter.setExporterInput(exporterInput);
			OutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(
					prop.getProperty("destinationPath") + fileName);
			exporter.setExporterOutput(exporterOutput);
			SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
			exporter.setConfiguration(configuration);
			exporter.exportReport();
			System.out.print("Pdf Gen Done!");

			System.out.println(prop.getProperty("destinationPath") + fileName + "Destination Path");
			System.out.println(prop.getProperty("sourcePath") + fileName + "Source Path");

			File f = new File(prop.getProperty("sourcePath") + fileName);
			FileInputStream fr = new FileInputStream(f);

			return fileName;

		} catch (JRException e) {
			e.printStackTrace();
			return "failtoStore";
		}

	}

}
