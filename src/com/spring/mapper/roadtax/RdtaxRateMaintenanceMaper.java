package com.spring.mapper.roadtax;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.spring.VO.roadtaxVO.RdtaxRateMaintenance;

public interface RdtaxRateMaintenanceMaper {

	// Fetch Rate maintenance report...............
	public List<RdtaxRateMaintenance> getRateMaintenanceReport(@Param("rigionid") String rigionid,
			@Param("carttype") String carttype);

	// Update Rate maintenance report...............
	public String updateRateMaintenanceForRegion(@Param("fromrange") String fromrange, @Param("torange") String torange,
			@Param("base_rate") String base_rate, @Param("progressive_rate") String progressive_rate,
			@Param("cal_id") String cal_id);

	// Update Rate maintenance report...............
	public Integer addRateMaintenanceForRegion(@Param("ccrangename") String ccrangename,
			@Param("regionidvalue") String regionidvalue, @Param("cartypeidvalue") String cartypeidvalue,
			@Param("fromrange") String fromrange, @Param("torange") String torange, @Param("baserate") String baserate,
			@Param("progressiverate") String progressiverate, @Param("status") String status);

	// Delete Rate maintenance record...............
	public Integer deleteRateMaintenanceReport(@Param("rowid") Integer rowid);

	// Fetch Rate maintenance report...............
	public List<RdtaxRateMaintenance> fetchRateMaintenance();

}
