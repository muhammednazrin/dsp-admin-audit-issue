package com.spring.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.spring.VO.SysParam;
import com.spring.VO.SysParamExample;

public interface SysParamMapper {
	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_PARAM
	 *
	 * @mbg.generated Sun May 20 17:10:23 SGT 2018
	 */
	long countByExample(SysParamExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_PARAM
	 *
	 * @mbg.generated Sun May 20 17:10:23 SGT 2018
	 */
	int deleteByExample(SysParamExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_PARAM
	 *
	 * @mbg.generated Sun May 20 17:10:23 SGT 2018
	 */
	int deleteByPrimaryKey(@Param("param") Object param, @Param("name") Object name);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_PARAM
	 *
	 * @mbg.generated Sun May 20 17:10:23 SGT 2018
	 */
	int insert(SysParam record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_PARAM
	 *
	 * @mbg.generated Sun May 20 17:10:23 SGT 2018
	 */
	int insertSelective(SysParam record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_PARAM
	 *
	 * @mbg.generated Sun May 20 17:10:23 SGT 2018
	 */
	List<SysParam> selectByExample(SysParamExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_PARAM
	 *
	 * @mbg.generated Sun May 20 17:10:23 SGT 2018
	 */
	SysParam selectByPrimaryKey(@Param("param") Object param, @Param("name") Object name);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_PARAM
	 *
	 * @mbg.generated Sun May 20 17:10:23 SGT 2018
	 */
	int updateByExampleSelective(@Param("record") SysParam record, @Param("example") SysParamExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_PARAM
	 *
	 * @mbg.generated Sun May 20 17:10:23 SGT 2018
	 */
	int updateByExample(@Param("record") SysParam record, @Param("example") SysParamExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_PARAM
	 *
	 * @mbg.generated Sun May 20 17:10:23 SGT 2018
	 */
	int updateByPrimaryKeySelective(SysParam record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_PARAM
	 *
	 * @mbg.generated Sun May 20 17:10:23 SGT 2018
	 */
	int updateByPrimaryKey(SysParam record);
}