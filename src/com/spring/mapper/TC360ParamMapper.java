package com.spring.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.spring.VO.TC360Param;
import com.spring.VO.TC360ParamExample;

public interface TC360ParamMapper {
	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_TC360_TBL_PARAM
	 *
	 * @mbggenerated Thu Nov 16 16:26:10 SGT 2017
	 */
	int countByExample(TC360ParamExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_TC360_TBL_PARAM
	 *
	 * @mbggenerated Thu Nov 16 16:26:10 SGT 2017
	 */
	int deleteByExample(TC360ParamExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_TC360_TBL_PARAM
	 *
	 * @mbggenerated Thu Nov 16 16:26:10 SGT 2017
	 */
	int deleteByPrimaryKey(Short id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_TC360_TBL_PARAM
	 *
	 * @mbggenerated Thu Nov 16 16:26:10 SGT 2017
	 */
	int insert(TC360Param record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_TC360_TBL_PARAM
	 *
	 * @mbggenerated Thu Nov 16 16:26:10 SGT 2017
	 */
	int insertSelective(TC360Param record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_TC360_TBL_PARAM
	 *
	 * @mbggenerated Thu Nov 16 16:26:10 SGT 2017
	 */
	List<TC360Param> selectByExample(TC360ParamExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_TC360_TBL_PARAM
	 *
	 * @mbggenerated Thu Nov 16 16:26:10 SGT 2017
	 */
	TC360Param selectByPrimaryKey(Short id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_TC360_TBL_PARAM
	 *
	 * @mbggenerated Thu Nov 16 16:26:10 SGT 2017
	 */
	int updateByExampleSelective(@Param("record") TC360Param record, @Param("example") TC360ParamExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_TC360_TBL_PARAM
	 *
	 * @mbggenerated Thu Nov 16 16:26:10 SGT 2017
	 */
	int updateByExample(@Param("record") TC360Param record, @Param("example") TC360ParamExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_TC360_TBL_PARAM
	 *
	 * @mbggenerated Thu Nov 16 16:26:10 SGT 2017
	 */
	int updateByPrimaryKeySelective(TC360Param record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_TC360_TBL_PARAM
	 *
	 * @mbggenerated Thu Nov 16 16:26:10 SGT 2017
	 */
	int updateByPrimaryKey(TC360Param record);
}