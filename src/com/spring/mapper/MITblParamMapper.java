package com.spring.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.spring.VO.MITblParam;
import com.spring.VO.MITblParamExample;

public interface MITblParamMapper {
	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_PARAMETERS
	 *
	 * @mbg.generated Tue Apr 11 17:48:14 SGT 2017
	 */
	long countByExample(MITblParamExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_PARAMETERS
	 *
	 * @mbg.generated Tue Apr 11 17:48:14 SGT 2017
	 */
	int deleteByExample(MITblParamExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_PARAMETERS
	 *
	 * @mbg.generated Tue Apr 11 17:48:14 SGT 2017
	 */
	int deleteByPrimaryKey(Short id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_PARAMETERS
	 *
	 * @mbg.generated Tue Apr 11 17:48:14 SGT 2017
	 */
	int insert(MITblParam record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_PARAMETERS
	 *
	 * @mbg.generated Tue Apr 11 17:48:14 SGT 2017
	 */
	int insertSelective(MITblParam record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_PARAMETERS
	 *
	 * @mbg.generated Tue Apr 11 17:48:14 SGT 2017
	 */
	List<MITblParam> selectByExample(MITblParamExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_PARAMETERS
	 *
	 * @mbg.generated Tue Apr 11 17:48:14 SGT 2017
	 */
	MITblParam selectByPrimaryKey(Short id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_PARAMETERS
	 *
	 * @mbg.generated Tue Apr 11 17:48:14 SGT 2017
	 */
	int updateByExampleSelective(@Param("record") MITblParam record, @Param("example") MITblParamExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_PARAMETERS
	 *
	 * @mbg.generated Tue Apr 11 17:48:14 SGT 2017
	 */
	int updateByExample(@Param("record") MITblParam record, @Param("example") MITblParamExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_PARAMETERS
	 *
	 * @mbg.generated Tue Apr 11 17:48:14 SGT 2017
	 */
	int updateByPrimaryKeySelective(MITblParam record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_MI_TBL_PARAMETERS
	 *
	 * @mbg.generated Tue Apr 11 17:48:14 SGT 2017
	 */
	int updateByPrimaryKey(MITblParam record);
}