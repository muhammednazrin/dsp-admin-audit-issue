package com.spring.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.spring.VO.BuddyPAOccupation;
import com.spring.VO.BuddyPAOccupationExample;

public interface BuddyPAOccupationMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_OCCUPATION
	 *
	 * @mbg.generated Mon May 28 20:57:33 SGT 2018
	 */
	long countByExample(BuddyPAOccupationExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_OCCUPATION
	 *
	 * @mbg.generated Mon May 28 20:57:33 SGT 2018
	 */
	int deleteByExample(BuddyPAOccupationExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_OCCUPATION
	 *
	 * @mbg.generated Mon May 28 20:57:33 SGT 2018
	 */
	int deleteByPrimaryKey(@Param("companyId") Object companyId, @Param("occupationCode") Object occupationCode);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_OCCUPATION
	 *
	 * @mbg.generated Mon May 28 20:57:33 SGT 2018
	 */
	int insert(BuddyPAOccupation record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_OCCUPATION
	 *
	 * @mbg.generated Mon May 28 20:57:33 SGT 2018
	 */
	int insertSelective(BuddyPAOccupation record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_OCCUPATION
	 *
	 * @mbg.generated Mon May 28 20:57:33 SGT 2018
	 */
	List<BuddyPAOccupation> selectByExampleWithBLOBs(BuddyPAOccupationExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_OCCUPATION
	 *
	 * @mbg.generated Mon May 28 20:57:33 SGT 2018
	 */
	List<BuddyPAOccupation> selectByExample(BuddyPAOccupationExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_OCCUPATION
	 *
	 * @mbg.generated Mon May 28 20:57:33 SGT 2018
	 */
	BuddyPAOccupation selectByPrimaryKey(@Param("companyId") Object companyId,
			@Param("occupationCode") Object occupationCode);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_OCCUPATION
	 *
	 * @mbg.generated Mon May 28 20:57:33 SGT 2018
	 */
	int updateByExampleSelective(@Param("record") BuddyPAOccupation record,
			@Param("example") BuddyPAOccupationExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_OCCUPATION
	 *
	 * @mbg.generated Mon May 28 20:57:33 SGT 2018
	 */
	int updateByExampleWithBLOBs(@Param("record") BuddyPAOccupation record,
			@Param("example") BuddyPAOccupationExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_OCCUPATION
	 *
	 * @mbg.generated Mon May 28 20:57:33 SGT 2018
	 */
	int updateByExample(@Param("record") BuddyPAOccupation record, @Param("example") BuddyPAOccupationExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_OCCUPATION
	 *
	 * @mbg.generated Mon May 28 20:57:33 SGT 2018
	 */
	int updateByPrimaryKeySelective(BuddyPAOccupation record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_OCCUPATION
	 *
	 * @mbg.generated Mon May 28 20:57:33 SGT 2018
	 */
	int updateByPrimaryKeyWithBLOBs(BuddyPAOccupation record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_SYS_TBL_OCCUPATION
	 *
	 * @mbg.generated Mon May 28 20:57:33 SGT 2018
	 */
	int updateByPrimaryKey(BuddyPAOccupation record);
}