package com.spring.mapper.report;

import java.util.List;

import com.spring.VO.report.TLReportExcelVO;
import com.spring.VO.report.TransSearchObject;

public interface TLReportExcelMapper {

	List<TLReportExcelVO> selectTLTransactionalReportAfterPayment(TransSearchObject record);

	List<TLReportExcelVO> selectTLTransactionalReportBeforePayment(TransSearchObject record);

	List<TLReportExcelVO> selectEPPIDSTransactionalReportBeforePayment(TransSearchObject record);

}
