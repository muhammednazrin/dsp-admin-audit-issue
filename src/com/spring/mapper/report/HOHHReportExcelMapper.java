package com.spring.mapper.report;

import java.util.List;

import com.spring.VO.report.HOHHReportExcelVO;
import com.spring.VO.report.TransSearchObject;

public interface HOHHReportExcelMapper {

	List<HOHHReportExcelVO> selectHOHHTransactionalReportAfterPayment(TransSearchObject record);

	List<HOHHReportExcelVO> selectHOHHTransactionalReportBeforePayment(TransSearchObject record);

}
