package com.spring.mapper.report;

import java.util.List;

import com.spring.VO.report.TransSearchObject;
import com.spring.VO.report.WTCReportExcelVO;

public interface WTCReportExcelMapper {

	List<WTCReportExcelVO> selectWTCTransactionalReportAfterPayment(TransSearchObject record);

	List<WTCReportExcelVO> selectWTCTransactionalReportBeforePayment(TransSearchObject record);

}
