package com.spring.mapper.report;

import java.util.List;
import com.spring.VO.report.PCCA01Report;
import com.spring.VO.report.TransSearchObject;

public interface PCCA01ReportMapper {
		
		List<PCCA01Report> selectPCCA01TransactionalReportAfterPayment(TransSearchObject record);

		List<PCCA01Report> selectPCCA01TransactionalReportBeforePayment(TransSearchObject record);

		//List<PCCA01Report> selectPTCA01TransactionalReportBeforePayment(TransSearchObject record);
	}
