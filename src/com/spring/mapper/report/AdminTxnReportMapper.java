package com.spring.mapper.report;

import java.util.List;

import com.spring.VO.report.AdminTxnReportVO;
import com.spring.VO.report.TransSearchObject;

public interface AdminTxnReportMapper {

	List<AdminTxnReportVO> selectMotorTransactionalReportAfterPayment(TransSearchObject record);

	List<AdminTxnReportVO> selectMotorTransactionalReportBeforePayment(TransSearchObject record);

}
