package com.spring.mapper.report;

import java.util.List;

import com.spring.VO.report.TLReportVO;
import com.spring.VO.report.TransSearchObject;

public interface TLReportMapper {

	List<TLReportVO> selectTLTransactionalReportAfterPayment(TransSearchObject record);

	List<TLReportVO> selectTLTransactionalReportBeforePayment(TransSearchObject record);

	List<TLReportVO> selectEPPIDSTransactionalReportBeforePayment(TransSearchObject record);

}
