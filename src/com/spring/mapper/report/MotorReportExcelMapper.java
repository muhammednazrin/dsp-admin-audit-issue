package com.spring.mapper.report;

import java.util.List;

import com.spring.VO.report.MotorReportExcelVO;
import com.spring.VO.report.TransSearchObject;

public interface MotorReportExcelMapper {

	List<MotorReportExcelVO> selectMotorTransactionalReportAfterPayment(TransSearchObject record);

	List<MotorReportExcelVO> selectMotorTransactionalReportBeforePayment(TransSearchObject record);

}
