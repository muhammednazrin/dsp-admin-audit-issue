package com.spring.mapper.report;

import com.spring.VO.report.MPReportExcel;

import com.spring.VO.report.TransSearchObject;

import java.math.BigDecimal;
import java.util.List;


public interface MPReportExcelMapper {
	
	List<MPReportExcel> selectMPTransactionalReportAfterPayment(TransSearchObject record);

	List<MPReportExcel> selectMPTransactionalReportBeforePayment(TransSearchObject record);

	List<MPReportExcel> selectMPTTransactionalReportBeforePayment(TransSearchObject record);
}