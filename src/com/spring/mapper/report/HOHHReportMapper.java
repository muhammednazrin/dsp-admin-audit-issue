package com.spring.mapper.report;

import java.util.List;

import com.spring.VO.report.HOHHReportVO;
import com.spring.VO.report.TransSearchObject;

public interface HOHHReportMapper {

	List<HOHHReportVO> selectHOHHTransactionalReportAfterPayment(TransSearchObject record);

	List<HOHHReportVO> selectHOHHTransactionalReportBeforePayment(TransSearchObject record);

}
