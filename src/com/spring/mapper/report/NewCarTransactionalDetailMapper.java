package com.spring.mapper.report;

import com.spring.VO.report.NewCarParamVO;
import com.spring.VO.report.NewCarTransactionalDetailVO;

public interface NewCarTransactionalDetailMapper {

	NewCarTransactionalDetailVO selectTransactionalDetails(NewCarParamVO newCarParamVO);

	public String updateCustomerInfo(NewCarTransactionalDetailVO newCarTransactionalDetailVO);

}
