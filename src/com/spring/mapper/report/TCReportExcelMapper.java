package com.spring.mapper.report;

import java.util.List;

import com.spring.VO.report.TCReportExcelVO;
import com.spring.VO.report.TransSearchObject;

public interface TCReportExcelMapper {

	List<TCReportExcelVO> selectTCTransactionalReportAfterPayment(TransSearchObject record);

	List<TCReportExcelVO> selectTCTransactionalReportBeforePayment(TransSearchObject record);

}
