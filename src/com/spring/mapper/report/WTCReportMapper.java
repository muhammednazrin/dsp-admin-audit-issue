package com.spring.mapper.report;

import java.util.List;

import com.spring.VO.report.TransSearchObject;
import com.spring.VO.report.WTCReportVO;

public interface WTCReportMapper {

	List<WTCReportVO> selectWTCTransactionalReportAfterPayment(TransSearchObject record);

	List<WTCReportVO> selectWTCTransactionalReportBeforePayment(TransSearchObject record);

}
