package com.spring.mapper.report;

import java.util.List;

import com.spring.VO.report.BuddyPAReportVO;
import com.spring.VO.report.TransSearchObject;

public interface BuddyPAReportMapper {

	List<BuddyPAReportVO> selectBuddyPATransactionalReportAfterPayment(TransSearchObject record);

	List<BuddyPAReportVO> selectBuddyPATransactionalReportBeforePayment(TransSearchObject record);

}
