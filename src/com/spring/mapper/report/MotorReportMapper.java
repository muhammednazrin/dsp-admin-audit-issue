package com.spring.mapper.report;

import java.util.List;

import com.spring.VO.report.MotorReportVO;
import com.spring.VO.report.TransSearchObject;

public interface MotorReportMapper {

	List<MotorReportVO> selectMotorTransactionalReportAfterPayment(TransSearchObject record);

	List<MotorReportVO> selectMotorTransactionalReportBeforePayment(TransSearchObject record);

}
