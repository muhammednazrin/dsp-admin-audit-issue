package com.spring.mapper.report;

import java.util.List;

import com.spring.VO.report.NewCarRegistrationReportVO;
import com.spring.VO.report.TransSearchObject;

public interface NewCarRegistrationMapper {

	List<NewCarRegistrationReportVO> selectNciTransactionalReportAfterPayment(TransSearchObject record);

	List<NewCarRegistrationReportVO> selectNciTransactionalReportBeforePayment(TransSearchObject record);

}
