package com.spring.mapper.report;

import java.util.List;

import com.spring.VO.report.ALLReportVO;
import com.spring.VO.report.TransSearchObject;

public interface ALLReportMapper {

	List<ALLReportVO> selectMotorTransactionalReportAfterPayment(TransSearchObject record);

	List<ALLReportVO> selectTLTransactionalReportAfterPayment(TransSearchObject record);

	List<ALLReportVO> selectWTCTransactionalReportAfterPayment(TransSearchObject record);

	List<ALLReportVO> selectHOHHTransactionalReportAfterPayment(TransSearchObject record);

	List<ALLReportVO> selectNciTransactionalReportAfterPayment(TransSearchObject record);

	List<ALLReportVO> selectBuddyPATransactionalReportAfterPayment(TransSearchObject record);

	List<ALLReportVO> selectTravelEzyTransactionalReportAfterPayment(TransSearchObject record);

	List<ALLReportVO> selectSECURETransactionalReportAfterPayment(TransSearchObject record);

	List<ALLReportVO> selectTCTransactionalReportAfterPayment(TransSearchObject record);
	
	List<ALLReportVO> selectPCCA01TransactionalReportAfterPayment(TransSearchObject record);
	
	List<ALLReportVO> selectMPTransactionalReportAfterPayment(TransSearchObject record);
}
