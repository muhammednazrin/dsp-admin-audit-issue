package com.spring.mapper.report;

import java.util.List;

import com.spring.VO.report.NewCarRegistrationReportExcelVO;
import com.spring.VO.report.TransSearchObject;

public interface NewCarRegistrationExcelMapper {

	List<NewCarRegistrationReportExcelVO> selectNciTransactionalReportAfterPayment(TransSearchObject record);

	List<NewCarRegistrationReportExcelVO> selectNciTransactionalReportBeforePayment(TransSearchObject record);

}
