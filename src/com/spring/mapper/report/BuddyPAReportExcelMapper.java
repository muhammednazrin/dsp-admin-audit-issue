package com.spring.mapper.report;

import java.util.List;

import com.spring.VO.report.BuddyPAReportExcelVO;
import com.spring.VO.report.TransSearchObject;

public interface BuddyPAReportExcelMapper {

	List<BuddyPAReportExcelVO> selectTransactionalReportAfterPayment(TransSearchObject record);

	List<BuddyPAReportExcelVO> selectTransactionalReportBeforePayment(TransSearchObject record);

}
