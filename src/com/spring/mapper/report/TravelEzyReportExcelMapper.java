package com.spring.mapper.report;

import java.util.List;

import com.spring.VO.report.TransSearchObject;
import com.spring.VO.report.TravelEzyReportExcelVO;

public interface TravelEzyReportExcelMapper {

	List<TravelEzyReportExcelVO> selectTravelEzyTransactionalReportAfterPayment(TransSearchObject record);

	List<TravelEzyReportExcelVO> selectTravelEzyTransactionalReportBeforePayment(TransSearchObject record);

}
