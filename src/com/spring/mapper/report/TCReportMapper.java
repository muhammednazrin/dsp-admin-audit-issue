package com.spring.mapper.report;

import java.util.List;

import com.spring.VO.report.TCReportVO;
import com.spring.VO.report.TransSearchObject;

public interface TCReportMapper {

	List<TCReportVO> selectTCTransactionalReportAfterPayment(TransSearchObject record);

	List<TCReportVO> selectTCTransactionalReportBeforePayment(TransSearchObject record);

}
