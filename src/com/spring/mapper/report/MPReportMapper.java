package com.spring.mapper.report;

import java.util.List;

import com.spring.VO.report.MPReport;
import com.spring.VO.report.TransSearchObject;


public interface MPReportMapper {

	List<MPReport> selectMPTransactionalReportAfterPayment(TransSearchObject record);

	List<MPReport> selectMPTransactionalReportBeforePayment(TransSearchObject record);

}