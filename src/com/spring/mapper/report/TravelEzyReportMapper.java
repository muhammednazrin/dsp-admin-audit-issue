package com.spring.mapper.report;

import java.util.List;

import com.spring.VO.report.TransSearchObject;
import com.spring.VO.report.TravelEzyReportVO;

public interface TravelEzyReportMapper {

	List<TravelEzyReportVO> selectTravelEzyTransactionalReportAfterPayment(TransSearchObject record);

	List<TravelEzyReportVO> selectTravelEzyTransactionalReportBeforePayment(TransSearchObject record);

}
