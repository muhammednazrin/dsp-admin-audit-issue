package com.spring.mapper.report;

import com.spring.VO.report.PCCA01ReportExcel;
import com.spring.VO.report.TransSearchObject;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PCCA01ReportExcelMapper {
  
		List<PCCA01ReportExcel> selectPCCA01TransactionalReportAfterPayment(TransSearchObject record);

		List<PCCA01ReportExcel> selectPCCA01TransactionalReportBeforePayment(TransSearchObject record);

		List<PCCA01ReportExcel> selectPTCA01TransactionalReportBeforePayment(TransSearchObject record);

	}
