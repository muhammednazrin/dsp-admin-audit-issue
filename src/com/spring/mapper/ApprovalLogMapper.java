package com.spring.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.spring.VO.ApprovalLog;
import com.spring.VO.ApprovalLogExample;

public interface ApprovalLogMapper {
	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_APPROVAL_LOG
	 *
	 * @mbg.generated Thu Mar 15 13:14:27 SGT 2018
	 */
	long countByExample(ApprovalLogExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_APPROVAL_LOG
	 *
	 * @mbg.generated Thu Mar 15 13:14:27 SGT 2018
	 */
	int deleteByExample(ApprovalLogExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_APPROVAL_LOG
	 *
	 * @mbg.generated Thu Mar 15 13:14:27 SGT 2018
	 */
	int deleteByPrimaryKey(Short id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_APPROVAL_LOG
	 *
	 * @mbg.generated Thu Mar 15 13:14:27 SGT 2018
	 */
	int insert(ApprovalLog record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_APPROVAL_LOG
	 *
	 * @mbg.generated Thu Mar 15 13:14:27 SGT 2018
	 */
	int insertSelective(ApprovalLog record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_APPROVAL_LOG
	 *
	 * @mbg.generated Thu Mar 15 13:14:27 SGT 2018
	 */
	List<ApprovalLog> selectByExampleWithBLOBs(ApprovalLogExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_APPROVAL_LOG
	 *
	 * @mbg.generated Thu Mar 15 13:14:27 SGT 2018
	 */
	List<ApprovalLog> selectByExample(ApprovalLogExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_APPROVAL_LOG
	 *
	 * @mbg.generated Thu Mar 15 13:14:27 SGT 2018
	 */
	ApprovalLog selectByPrimaryKey(Short id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_APPROVAL_LOG
	 *
	 * @mbg.generated Thu Mar 15 13:14:27 SGT 2018
	 */
	int updateByExampleSelective(@Param("record") ApprovalLog record, @Param("example") ApprovalLogExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_APPROVAL_LOG
	 *
	 * @mbg.generated Thu Mar 15 13:14:27 SGT 2018
	 */
	int updateByExampleWithBLOBs(@Param("record") ApprovalLog record, @Param("example") ApprovalLogExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_APPROVAL_LOG
	 *
	 * @mbg.generated Thu Mar 15 13:14:27 SGT 2018
	 */
	int updateByExample(@Param("record") ApprovalLog record, @Param("example") ApprovalLogExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_APPROVAL_LOG
	 *
	 * @mbg.generated Thu Mar 15 13:14:27 SGT 2018
	 */
	int updateByPrimaryKeySelective(ApprovalLog record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_APPROVAL_LOG
	 *
	 * @mbg.generated Thu Mar 15 13:14:27 SGT 2018
	 */
	int updateByPrimaryKeyWithBLOBs(ApprovalLog record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to
	 * the database table DSP_ADM_TBL_APPROVAL_LOG
	 *
	 * @mbg.generated Thu Mar 15 13:14:27 SGT 2018
	 */
	int updateByPrimaryKey(ApprovalLog record);

	int insert(byte[] record);
}