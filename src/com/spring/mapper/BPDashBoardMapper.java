package com.spring.mapper;

import org.apache.ibatis.annotations.Param;

import com.spring.VO.BPDashBoard;

public interface BPDashBoardMapper {

	void selectBPDashBoard(@Param("record1") BPDashBoard record1);

}
