package com.spring.constant;

import org.apache.log4j.Logger;

import test.QQConstantTest;

public class QQConstant {
	final static Logger logger = Logger.getLogger(QQConstant.class);

	private QQConstant() {

	}

	public static final String CANCERCARE_INSURANCE = "PCCA01";
	public static final String CANCERCARE_TAKAFUL = "PTCA01";
	public static final String MEDICALPASS_INSURANCE = "MP";
	public static final String MEDICALPASS_TAKAFUL = "MPT";

}
