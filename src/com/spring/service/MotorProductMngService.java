package com.spring.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.VO.CommonTaxType;
import com.spring.VO.CommonTaxTypeExample;
import com.spring.VO.DspMiTblOto;
import com.spring.VO.DspMiTblOtoExample;
import com.spring.VO.DspTblRangeRules;
import com.spring.VO.DspTblRangeRulesExample;
import com.spring.VO.MITblParam;
import com.spring.VO.MITblParamExample;
import com.spring.mapper.CommonTaxTypeMapper;
import com.spring.mapper.DspMiTblOtoMapper;
import com.spring.mapper.DspTblRangeRulesMapper;
import com.spring.mapper.MITblParamMapper;

@Service
public class MotorProductMngService {

	private static final Logger logger = Logger.getLogger(MotorProductMngService.class);
	CommonTaxTypeMapper commonTaxTypeMapper;
	MITblParamMapper miTblParamMapper;;
	DspMiTblOtoMapper dspMiTblOtoMapper;
	DspTblRangeRulesMapper dspTblRangeRulesMapper;

	@Autowired
	public MotorProductMngService(CommonTaxTypeMapper commonTaxTypeMapper, MITblParamMapper miTblParamMapper,
			DspMiTblOtoMapper dspMiTblOtoMapper, DspTblRangeRulesMapper dspTblRangeRulesMapper

	) {
		this.commonTaxTypeMapper = commonTaxTypeMapper;
		this.miTblParamMapper = miTblParamMapper;
		this.dspMiTblOtoMapper = dspMiTblOtoMapper;
		this.dspTblRangeRulesMapper = dspTblRangeRulesMapper;

	}

	public CommonTaxType getTaxDetails(String productCode, String taxType) {

		CommonTaxType taxTypeObj = new CommonTaxType();
		try {
			CommonTaxTypeExample commonTaxTypeExample = new CommonTaxTypeExample();
			CommonTaxTypeExample.Criteria criteria = commonTaxTypeExample.createCriteria();
			criteria.andProductCodeEqualTo(productCode);
			criteria.andTaxTypeEqualTo(taxType);
			List<CommonTaxType> list = commonTaxTypeMapper.selectByExample(commonTaxTypeExample);
			if (list.size() == 1) {
				taxTypeObj = list.get(0);
			}

		} catch (Exception e) {
			logger.info("Error in getting Motor Param");
		}
		return taxTypeObj;

	}

	public int updateTaxDetails(String productCode, String taxType, CommonTaxType taxTypeObj) {

		int result = 0;
		try {
			CommonTaxTypeExample commonTaxTypeExample = new CommonTaxTypeExample();
			CommonTaxTypeExample.Criteria criteria = commonTaxTypeExample.createCriteria();
			criteria.andProductCodeEqualTo(productCode);
			criteria.andTaxTypeEqualTo(taxType);
			result = commonTaxTypeMapper.updateByExampleSelective(taxTypeObj, commonTaxTypeExample);
			if (result > 0) {
				logger.info("Motor taxType " + taxType + "Updated");
			} else {
				logger.info("Motor taxType " + taxType + "Not Updated");
			}

		} catch (Exception e) {
			logger.info("Error in updating Motor taxType");
		}
		return result;

	}

	public MITblParam getMotorSingleParam(String paramName, Integer noOfParam) {
		MITblParam mITblParam = new MITblParam();
		try {
			List<MITblParam> list = new ArrayList<MITblParam>();
			MITblParamExample dspmiTblParamExample = new MITblParamExample();
			MITblParamExample.Criteria dspMiParam_criteria = dspmiTblParamExample.createCriteria();
			dspMiParam_criteria.andNameEqualTo(paramName);
			list = miTblParamMapper.selectByExample(dspmiTblParamExample);
			if (noOfParam == 1 && list.size() == 1) {
				mITblParam = list.get(0);
				logger.info("Motor Param " + paramName + "Retrieved");
			} else {
				logger.info("Motor Param " + paramName + "Not Exist");
			}

		} catch (Exception e) {
			logger.info("Error in getting Motor Param");
		}
		return mITblParam;

	}

	public List<MITblParam> getMotorMultipleParam(String paramName) {

		List<MITblParam> list = new ArrayList<MITblParam>();
		try {
			MITblParamExample dspmiTblParamExample = new MITblParamExample();
			MITblParamExample.Criteria dspMiParam_criteria = dspmiTblParamExample.createCriteria();
			dspMiParam_criteria.andNameEqualTo(paramName);
			list = miTblParamMapper.selectByExample(dspmiTblParamExample);
			if (list.size() > 0) {
				logger.info("Motor Param " + paramName + "Retrieved");
			} else {
				logger.info("Motor Param " + paramName + "Not Exist");
			}

		} catch (Exception e) {
			logger.info("Error in getting Motor Param");
		}
		return list;

	}

	public List<DspTblRangeRules> getMotorRangeRuleParam(String productCode, String ruleCode) {

		List<DspTblRangeRules> list = new ArrayList<DspTblRangeRules>();
		try {
			DspTblRangeRulesExample dspTblRangeRulesExample1 = new DspTblRangeRulesExample();
			DspTblRangeRulesExample.Criteria dspTlRangeRules_criteria1 = dspTblRangeRulesExample1.createCriteria();
			dspTlRangeRules_criteria1.andRuleCodeEqualTo(ruleCode);
			dspTlRangeRules_criteria1.andProductCodeEqualTo(productCode);
			list = dspTblRangeRulesMapper.selectByExample(dspTblRangeRulesExample1);
			if (list.size() <= 0) {
				logger.info("Motor Rule Range Param" + ruleCode + "Not Exist");
			}

		} catch (Exception e) {
			logger.info("Error in getting Motor Rule Range Param");
		}
		return list;

	}

	public DspMiTblOto getOTOStampDuty(String productCode, String Status) {

		DspMiTblOto dspMiTblOto = new DspMiTblOto();

		try {
			List<DspMiTblOto> list = new ArrayList<DspMiTblOto>();
			DspMiTblOtoExample OtoExample = new DspMiTblOtoExample();
			DspMiTblOtoExample.Criteria criteria = OtoExample.createCriteria();
			criteria.andStatusEqualTo("ACTIVE");
			criteria.andProductCodeEqualTo(productCode);
			list = dspMiTblOtoMapper.selectByExample(OtoExample);
			logger.info(list.get(0).getStampDuty());
			if (list.size() > 0) {
				if (list.size() == 1) {
					dspMiTblOto = list.get(0);
				}
				logger.info("OTO param Retrieved");
			}

		} catch (Exception e) {
			logger.info("Error in getting OTO Param");
		}
		return dspMiTblOto;

	}

	public int updateOTOStampDuty(String productCode, String Status, DspMiTblOto dspMiTblOto) {

		int result = 0;
		try {
			List<DspMiTblOto> list = new ArrayList<DspMiTblOto>();
			DspMiTblOtoExample OtoExample = new DspMiTblOtoExample();
			DspMiTblOtoExample.Criteria criteria = OtoExample.createCriteria();
			criteria.andStatusEqualTo("ACTIVE");
			criteria.andProductCodeEqualTo(productCode);
			result = dspMiTblOtoMapper.updateByExampleSelective(dspMiTblOto, OtoExample);
			if (result > 0) {
				logger.info("Motor OTO Updated");
			} else {
				logger.info("Motor OTO Not Updated");
			}

		} catch (Exception e) {
			logger.info("Error in updating Motor Param");
		}
		return result;

	}

	public int updateMotorParam(String paramName, MITblParam mITblParam) {

		int result = 0;
		try {
			MITblParamExample dspmiTblParamExample = new MITblParamExample();
			MITblParamExample.Criteria dspMiParam_criteria = dspmiTblParamExample.createCriteria();
			dspMiParam_criteria.andNameEqualTo(paramName);
			result = miTblParamMapper.updateByExampleSelective(mITblParam, dspmiTblParamExample);
			if (result > 0) {
				logger.info("Motor Param " + paramName + "Updated");
			} else {
				logger.info("Motor Param " + paramName + "Not Updated");
			}

		} catch (Exception e) {
			logger.info("Error in updating Motor Param");
		}
		return result;

	}

	public int updateMotorRangeRuleParam(String paramName, String productCode, DspTblRangeRules dspTblRangeRules) {

		int result = 0;
		try {
			DspTblRangeRulesExample dspTblRangeRulesExample1 = new DspTblRangeRulesExample();
			DspTblRangeRulesExample.Criteria dspTlRangeRules_criteria1 = dspTblRangeRulesExample1.createCriteria();
			dspTlRangeRules_criteria1.andRuleCodeEqualTo(paramName);
			dspTlRangeRules_criteria1.andProductCodeEqualTo(productCode);
			result = dspTblRangeRulesMapper.updateByExampleSelective(dspTblRangeRules, dspTblRangeRulesExample1);
			if (result > 0) {
				logger.info("Motor Param " + paramName + "Updated");
			} else {
				logger.info("Motor Param " + paramName + "Not Updated");
			}

		} catch (Exception e) {
			logger.info("Error in updating Motor Range Param");
		}
		return result;

	}
}
