package com.spring.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.uam.dao.MenuDao;
import com.spring.uam.vo.Menu;
import com.spring.uam.vo.RoleModule;

@Service
public class MenuService {
	MenuDao menuDao;

	@Autowired
	public MenuService(MenuDao menuDao

	) {
		this.menuDao = menuDao;

	}

	public List<RoleModule> getRoleListByUser(int userId, int moduleId) {
		ArrayList<RoleModule> RoleModuleList = new ArrayList<RoleModule>();
		try {
			RoleModuleList = menuDao.getRoleListByUser(userId, moduleId);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return RoleModuleList;
	}

	public Map getMenuList(String roles, int module_id) throws SQLException {
		Map levels = new HashMap();
		ArrayList<Menu> menu_first_all = new ArrayList<Menu>();
		ArrayList<Menu> menu_second_all = new ArrayList<Menu>();
		ArrayList<Menu> menu_third_all = new ArrayList<Menu>();
		ArrayList<Menu> menu_fourth_all = new ArrayList<Menu>();
		// String roles="1,10";
		String[] params = roles.split(",");
		for (int i = 0; i < params.length; i++) {
			// System.out.println(params[i]+"role id");
			ArrayList<Menu> menus = new ArrayList<Menu>();
			ArrayList<Menu> menu_first = new ArrayList<Menu>();
			ArrayList<Menu> menu_second = new ArrayList<Menu>();
			ArrayList<Menu> menu_third = new ArrayList<Menu>();
			ArrayList<Menu> menu_fourth = new ArrayList<Menu>();
			menus = menuDao.getMenuList(module_id, Integer.parseInt(params[i]));
			System.out.println(module_id + "," + params[i] + "menu size:" + menus.size());

			// System.out.println("size "+menus.size());
			for (Menu single : menus) {
				// System.out.println("check parmission 1 :" + single.getPermission_str());
				if (single.getLevelId() == 1) {
					// String permission_str =
					// SecurityUtil.enciptString(single.getPermission_str());
					// single.setPermission_str(permission_str);
					menu_first.add(single);
					// break;
				}
			}
			for (Menu single : menus) {
				// System.out.println("check parmission 2 :" + single.getPermission_str());
				if (single.getLevelId() == 2) {
					// String permission_str =
					// SecurityUtil.enciptString(single.getPermission_str());
					// single.setPermission_str(permission_str);
					menu_second.add(single);
					// break;
				}
			}
			for (Menu single : menus) {
				// System.out.println("check parmission 3 :" + single.getPermission_str());
				if (single.getLevelId() == 3) {
					// String permission_str =
					// SecurityUtil.enciptString(single.getPermission_str());
					// single.setPermission_str(permission_str);
					menu_third.add(single);
					// break;
				}
			}
			for (Menu single : menus) {
				// System.out.println("check parmission 4 :" + single.getPermission_str());
				if (single.getLevelId() == 4) {
					// String permission_str =
					// SecurityUtil.enciptString(single.getPermission_str());
					// single.setPermission_str(permission_str);
					menu_fourth.add(single);
					// break;
				}
			}
			menu_first_all.addAll(menu_first);
			menu_second_all.addAll(menu_second);
			menu_third_all.addAll(menu_third);
			menu_fourth_all.addAll(menu_fourth);
		}
		System.out.println(menu_first_all.size() + "first level size before merge");
		for (int i = 0; i < menu_first_all.size(); i++) {
			System.out.println(
					"Menu Name  and ID " + menu_first_all.get(i).getName() + "," + menu_first_all.get(i).getId());

		}
		Set<Menu> set_first = new HashSet<Menu>(menu_first_all);
		Set<Menu> set_second = new HashSet<Menu>(menu_second_all);
		Set<Menu> set_third = new HashSet<Menu>(menu_third_all);
		Set<Menu> set_fourth = new HashSet<Menu>(menu_fourth_all);
		Iterator<Menu> iterator = set_first.iterator();
		while (iterator.hasNext()) {
			Menu setElement = iterator.next();
			System.out.println(setElement.getName() + "," + setElement.getId());
		}
		// Set myOrderedSet = new LinkedHashSet(set_first);
		System.out.println(set_first.size() + "first level size after merge");
		ArrayList<Menu> menu_first_final = new ArrayList<Menu>(set_first);
		ArrayList<Menu> menu_second_final = new ArrayList<Menu>(set_second);
		ArrayList<Menu> menu_third_final = new ArrayList<Menu>(set_third);
		ArrayList<Menu> menu_fourth_final = new ArrayList<Menu>(set_fourth);
		// sorting menu based on Id
		/*
		 * Collections.sort(menu_first_final, (Menu a, Menu b) ->
		 * a.getId().compareTo(b.getId())); Collections.sort(menu_second_final, (Menu a,
		 * Menu b) -> a.getId().compareTo(b.getId()));
		 * Collections.sort(menu_third_final, (Menu a, Menu b) ->
		 * a.getId().compareTo(b.getId())); Collections.sort(menu_fourth_final, (Menu a,
		 * Menu b) -> a.getId().compareTo(b.getId()));
		 */
		levels.put("first", menu_first_final);
		levels.put("second", menu_second_final);
		levels.put("third", menu_third_final);
		levels.put("fourth", menu_fourth_final);
		return levels;
	}

	/////////////// ************* end ***************************/
	////////// ********** we service to get user
	/////////////// menu**********************************/
	// @RequestMapping(value = "/getMenu/{id}", method = RequestMethod.GET)
	// @ResponseBody
	// public @ResponseBody Map getMenu(@PathVariable("id") String id,
	// HttpServletResponse response,HttpServletRequest request) throws SQLException
	// {
	public Map getMenu(String id) throws SQLException {
		Map map = new HashMap();
		int moduleId = 0;
		String roles = "";
		System.out.println(id + "id");
		String params[] = id.split("\\$");
		if (params.length == 2) {
			moduleId = Integer.parseInt(params[1]);
			roles = params[0];
			System.out.println(roles + "," + moduleId);
			map = getMenuList(roles, moduleId);
		}
		return map;
	}

}
