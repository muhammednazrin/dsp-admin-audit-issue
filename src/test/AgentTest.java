package test;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.etiqa.model.agent.Agent;

public class AgentTest {
	final static Logger logger = Logger.getLogger(PCCA01ReportExcelTest.class);
	@Test
	public void Agentmodeltest() {
		
		Agent agent=new Agent();
		
		agent.setId(1);
		agent.setUserId("000011110");
		agent.setPassword("000011110");
		agent.setDateFrom("22-06-2019");
		agent.setDateTo("24-06-2019");
		agent.setDiscount("10");
		agent.setCommission("1");
        agent.setAgentType("Direct");
        agent.setAddress1("qqqq");
        agent.setAddress2("wwww");
        agent.setAddress3("rrrr");
        
        agent.setReferenceNo("CMO12131");
        agent.setAgentCode("Bangsar");
		
}
}