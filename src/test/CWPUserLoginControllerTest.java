
package test;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration

@ContextConfiguration(locations = { "file:WebContent/WEB-INF/spring/root-context.xml",
		"file:WebContent/WEB-INF/spring/appServlet/servlet-context.xml" })

@AutoConfigureMockMvc(secure = false)
public class CWPUserLoginControllerTest {
	
	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	private MockHttpServletRequest request;
	private MockHttpSession session;

	final static Logger logger = Logger.getLogger(CWPUserLoginControllerTest.class);

	@Before
	public void setup() {
		request = new MockHttpServletRequest();
		session = new MockHttpSession();
		session.setAttribute("Name", "Dhana");
		session.setAttribute("Password", "00011111");
		DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
		this.mockMvc = builder.build();
	}



	@Test
	public void CWPUserLoginControllerTest() throws Exception {
		ResultMatcher ok = MockMvcResultMatchers.status().isOk();
		ResultMatcher viewName = MockMvcResultMatchers.view().name("admin-phone");
		ResultMatcher forwardedURL = MockMvcResultMatchers.forwardedUrl("/admin-phone.jsp");

		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/cwp/cwpDashboard");
	

	}

}
