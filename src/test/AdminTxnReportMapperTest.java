package test;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.spring.VO.report.AdminTxnReportVO;
import com.spring.VO.report.PCCA01ReportExcel;

public class AdminTxnReportMapperTest {
	
	final static Logger logger = Logger.getLogger(PCCA01ReportExcelTest.class);
	@Test
	public void admintxnreportmappertest() {
	
		AdminTxnReportVO admintxnreportvo=new AdminTxnReportVO();
		
		admintxnreportvo.setTransaction_datetime("21-07-2019");
		admintxnreportvo.setPaymentTrxID("1234567890");
		admintxnreportvo.setPmnt_status("success");
		admintxnreportvo.setAmount("100.00");
		admintxnreportvo.setMotor_net_premium("100000.00");
		admintxnreportvo.setPassenger_pa_premium_payable("1009.00");
		admintxnreportvo.setGrosspremium_final("1800.00");
		admintxnreportvo.setDiscount("1");
		admintxnreportvo.setGst("3");
		admintxnreportvo.setInvoice_no("DCI0123544");
		admintxnreportvo.setPolicy_number("DCI01234567");
		admintxnreportvo.setCaps_dppa("1234");
		admintxnreportvo.setCustomer_name("Robert Willions");
		admintxnreportvo.setCustomer_nric_id("930277755044");
		admintxnreportvo.setRegistration_number("1234567891");
		admintxnreportvo.setPmnt_gateway_code("MPAY");
		admintxnreportvo.setTransaction_id("12341234");
		admintxnreportvo.setAuth_id("1234");
		admintxnreportvo.setBankrefno("9087654321");
		admintxnreportvo.setApprovalcode("12");
		admintxnreportvo.setFpx_fpxtxnid("123434");
		admintxnreportvo.setFpx_debitauthcode("145678");
		admintxnreportvo.setProduct_code("PCCA01");
		admintxnreportvo.setAgent_code("001123");
		
		
		asserNotNull(admintxnreportvo);

}
	private void asserNotNull(AdminTxnReportVO admintxnreportvo) {
		// TODO Auto-generated method stub
		
	}
}