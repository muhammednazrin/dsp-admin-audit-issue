package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.util.SystemOutLogger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.spring.VO.AgentProdMap;
import com.spring.VO.AgentProdMapExample;
import com.spring.VO.AgentProfile;
import com.spring.VO.AgentProfileExample;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
// @ContextConfiguration(classes = MyWebConfig.class)
@ContextConfiguration(locations = { "file:WebContent/WEB-INF/spring/root-context.xml",
		"file:WebContent/WEB-INF/spring/appServlet/servlet-context.xml" })

@AutoConfigureMockMvc(secure = false)
public class MyMvcControllerTest {

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	private MockHttpServletRequest request;
	private MockHttpSession session;

	final static Logger logger = Logger.getLogger(MyMvcControllerTest.class);

	@Before
	public void setup() {
		request = new MockHttpServletRequest();
		session = new MockHttpSession();
		session.setAttribute("Name", "Dhana");
		session.setAttribute("Password", "00011111");
		DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
		this.mockMvc = builder.build();
	}



	@Test
	public void testTranExcelController() throws Exception {
		ResultMatcher ok = MockMvcResultMatchers.status().isOk();
		ResultMatcher viewName = MockMvcResultMatchers.view().name("admin-login");
		ResultMatcher forwardedURL = MockMvcResultMatchers.forwardedUrl("/admin-login.jsp");

		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/generateExcel");
		// System.out.println(this.mockMvc.perform(builder).toString());
		this.mockMvc.perform(builder).andDo(MockMvcResultHandlers.print()).andExpect(ok).andExpect(viewName)
				.andExpect(forwardedURL);

	}

	@Test
	public void testTranExcelControllerr() {
		assertEquals(session.getAttribute("Name"), "Dhana");
		assertNotEquals(session.getAttribute("Name"), "Dhanal");

		assertEquals(session.getAttribute("Password"), "00011111");
		assertNotEquals(session.getAttribute("Password"), "000111111");
	}

	public List<AgentProdMap> agentProductSubmitDone(AgentProfile agentProfile) {

		logger.info(agentProfile.getId() + "Agent Details" + agentProfile.getAgentName());

		List<AgentProfile> agentProfileList = new ArrayList<AgentProfile>();
		AgentProfileExample example = new AgentProfileExample();

		List<AgentProdMap> agentProdMapList = new ArrayList<AgentProdMap>();
		AgentProdMapExample agentProdMapExample = new AgentProdMapExample();
		AgentProdMapExample.Criteria agentProdMap_criteria = agentProdMapExample.createCriteria();
		agentProdMap_criteria.andAgpIdEqualTo(Short.parseShort(agentProfile.getId().toString()));

		logger.info("   agenet profile data  1116 ::::::::::::::" + agentProfile.getAgentName());
		return agentProdMapList;

	}
}