
package test;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.spring.VO.report.MPReport;
import com.spring.admin.AgentController;

public class MPTReportMapperTest {

	final static Logger logger = Logger.getLogger(MPReportMapperTest.class);
	@Test
	public void mpreporttest() {
		
		
		
		MPReport mpreport=new MPReport();
		mpreport.setTransaction_datetime("10-07-2019");
		mpreport.setPaymentTrxID("1234567890");
		mpreport.setPmnt_status("success");
		mpreport.setAmount("100.00");
		mpreport.setPolicy_number("DCI0012345");
		mpreport.setCustomer_name("carolla caterian");
		mpreport.setCustomer_nric_id("930110755044");
		mpreport.setPmnt_gateway_code("mpay");
		mpreport.setTxn_id("1122334455");
		mpreport.setAuth_code("1111111");
		
		
		mpreport.setProduct_code("MPT");
		mpreport.setAgent_code("0111111");
		mpreport.setAgent_name("Anij");
		
		
		mpreport.setCoverage_term("1000.00");
		mpreport.setCoverage_amount("101.00");
		mpreport.setPremium_amount("190.00");
		mpreport.setPremium_mode("annual");
		
		
		mpreport.setQuotation_status("success");
		mpreport.setLast_page("lastpage");
		mpreport.setReason("bmi not valid");
		
		mpreport.setUWReason("Rejection List Data");
		
		
		
		asserNotNull(mpreport);
		//dspCiTblQQ.setDspQqId(new BigDecimal("10000"));
		
		
		//getter
		
		mpreport.getAgent_code();
		mpreport.getAgent_name();
		logger.info("$$$$$$mpreport.getAgent_code();$$$$$"+mpreport.getAgent_code());
		
	}

	private void asserNotNull(MPReport mpreport) {
		// TODO Auto-generated method stub
		
	}
}
