

package test;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.junit.Test;


import com.spring.VO.report.PCCA01Report;
import com.spring.VO.report.PCCA01ReportExcel;
import com.spring.admin.AgentController;

public class PTCA01ReportExcelVOTest {

	final static Logger logger = Logger.getLogger(PTCA01ReportExcelVOTest.class);
	@Test
	public void pccareportexceltest() {
		
		
		
		PCCA01ReportExcel pccareportexcel=new PCCA01ReportExcel();
		pccareportexcel.setTransaction_datetime("10-07-2019");
		pccareportexcel.setPaymentTrxID("1234567890");
		pccareportexcel.setPmnt_status("success");
		pccareportexcel.setAmount("100.00");
		pccareportexcel.setPolicy_number("DCT0012345");
		pccareportexcel.setCustomer_name("Willion Rose");
		pccareportexcel.setCustomer_nric_id("930110755044");
		pccareportexcel.setPmnt_gateway_code("mpay");
		pccareportexcel.setTxn_id("1122334455");
		pccareportexcel.setAuth_code("1111111");
		
		
		pccareportexcel.setProduct_code(QQConstantTest.CANCERCARE_INSURANCE);//(QQConstant.INSURANCE_PRODUCT_CODE
		pccareportexcel.setAgent_code("0111111");
		pccareportexcel.setAgent_name("Rose Ketty");
		
		
		pccareportexcel.setCoverage_term("1000.00");
		pccareportexcel.setCoverage_amount("101.00");
		pccareportexcel.setPremium_amount("190.00");
		pccareportexcel.setPremium_mode("annual");
		
		
		pccareportexcel.setQuotation_status("success");
		pccareportexcel.setLast_page("lastpage");
		pccareportexcel.setReason("bmi not valid");
		
		pccareportexcel.setUWReason("Rejection List Data");
		
		
		
		asserNotNull(pccareportexcel);
		//dspCiTblQQ.setDspQqId(new BigDecimal("10000"));
		
		
		//getter
		
		pccareportexcel.getAgent_code();
		pccareportexcel.getAgent_name();
		logger.info("$$$$$$pccareportexcel.getAgent_code();$$$$$"+pccareportexcel.getAgent_code());
		
	}

	private void asserNotNull(PCCA01ReportExcel pccareportexcel) {
		// TODO Auto-generated method stub
		
	}
}
