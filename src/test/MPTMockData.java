
package test;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;

import com.spring.VO.AgentProdMap;
import com.spring.VO.AgentProdMapExample;
import com.spring.VO.AgentProfile;
import com.spring.VO.AgentProfileExample;
import com.spring.mapper.AdminParamMapper;
import com.spring.mapper.AgentDocumentMapper;
import com.spring.mapper.AgentLinkMapper;
import com.spring.mapper.AgentProdMapMapper;
import com.spring.mapper.AgentProfileMapper;
import com.spring.mapper.ApprovalLogMapper;
import com.spring.mapper.ApprovalMapper;
import com.spring.mapper.CommonQQMapper;
import com.spring.mapper.ProductsMapper;
import com.spring.utils.AbsecSecurity;


@RunWith(MockitoJUnitRunner.class)
public class MPTMockData {
	@Autowired
	AgentProfileMapper agentProfileMapper;
	@Autowired
	ProductsMapper productsMapper;
	@Autowired
	AgentProdMapMapper agentProdMapMapper;
	@Autowired
	CommonQQMapper commonQQMapper;
	@Autowired
	AdminParamMapper adminParamMapper;
	@Autowired
	AgentDocumentMapper agentDocumentMapper;
	@Autowired
	AgentLinkMapper agentLinkMapper;
	@Autowired
	ApprovalLogMapper approvalLogMapper;
	@Autowired
	ApprovalMapper approvalMapper;	
	

private MockHttpServletRequest request;
private MockHttpSession session;
private String UUID;
private List<LinkMPT> agentProdMap;




	final static Logger logger = Logger.getLogger(MPMockData.class);

	@Before
	public void setupData() {
		UUID = AbsecSecurity.GetGUID();
		request = new MockHttpServletRequest();
		session = new MockHttpSession();
		session.setAttribute("Name", "Dhana");

		agentProdMap = new ArrayList<>();
		
		LinkMPT link1 = new LinkMPT("1", "MP", "EIB", "https://uat.etiqa.com.my:4442/getonline/medical-insurance-cyberagent?code=");
		agentProdMap.add(link1);
		link1 = new LinkMPT("1", "MPT", "ETB", "https://uat.etiqa.com.my:4442/getonline/medical-takaful-cyberagent?code=");
		agentProdMap.add(link1);
		
		
		 link1 = new LinkMPT("2", "MP", "EIB", "https://uat.etiqa.com.my:4442/getonline/medical-insurance-cyberagent?code=");
		agentProdMap.add(link1);
		
		 link1 = new LinkMPT("2", "MPT", "ETB", "https://uat.etiqa.com.my:4442/getonline/medical-takaful-cyberagent?code=");
		agentProdMap.add(link1);

	}
		
	@Test
	public void TestLinkMP() {
		String LinkMP = getLinkForAgentIdMP("1", "MP", "EIB");

		assertEquals(LinkMP, "https://uat.etiqa.com.my:4442/getonline/medical-insurance-cyberagent?code=" + UUID);

	}

	public String getLinkForAgentIdMP(String agentId, String productType, String productEntity) {
		String agentLink = null;

		for (LinkMPT l : agentProdMap) {
			if (l.getAgentId() == agentId && l.getProductType().trim() == productType
					&& l.getProductEntity().trim() == productEntity) {
				return l.getLinkMP() + UUID;
			}
		}

		return agentLink;
	}
	
	@Test
	public void agentProductSubmitDoneMPTest() {
		AgentProfile agentProfile = new AgentProfile();
		agentProfile.setId(new BigDecimal(2649));
		agentProfile.setAgentName("Pokemon");
		List<AgentProdMap> listOfAgentProdMap = agentProductSubmitDoneMP(agentProfile);
		for (AgentProdMap a : listOfAgentProdMap) {
			if (a.getProductCode().trim() == "MP") {
				assertEquals(a.getProdRefUrl(),
						"https://uat.etiqa.com.my:4442/getonline/medical-insurance-cyberagent?code="
								+ a.getAgentUuid());
			} else if (a.getProductCode().trim() == "MPT") {
				assertEquals(a.getProdRefUrl(),
						"https://uat.etiqa.com.my:4442/getonline/medical-takaful-cyberagent?code="
								+ a.getAgentUuid());
			}
		}

	}

	public List<AgentProdMap> agentProductSubmitDoneMP(AgentProfile agentProfile) {

		logger.info(agentProfile.getId() + "Agent Details" + agentProfile.getAgentName());

		List<AgentProfile> agentProfileList = new ArrayList<AgentProfile>();
		AgentProfileExample example = new AgentProfileExample();
		/// agentProfileList = agentProfileMapper.selectByExample(example);

		List<AgentProdMap> agentProdMapList = new ArrayList<AgentProdMap>();
		AgentProdMapExample agentProdMapExample = new AgentProdMapExample();
		AgentProdMapExample.Criteria agentProdMap_criteria = agentProdMapExample.createCriteria();
		agentProdMap_criteria.andAgpIdEqualTo(Short.parseShort(agentProfile.getId().toString()));
		// agentProdMap_criteria.andAgpIdEqualTo(Short.parseShort(agentProfile.getIdNo()));
		/// agentProdMapList = agentProdMapMapper.selectByExample(agentProdMapExample);

		logger.info("agenet profile data::::::::::::::" + agentProfile.getAgentName());
		return agentProdMapList;

	}


}


