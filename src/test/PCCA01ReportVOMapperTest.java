
package test;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.spring.VO.report.PCCA01Report;
import com.spring.admin.AgentController;

public class PCCA01ReportVOMapperTest {

	final static Logger logger = Logger.getLogger(PCCA01ReportVOMapperTest.class);
	@Test
	public void pccareporttest() {
		
		
		
		PCCA01Report pccareport=new PCCA01Report();
		pccareport.setTransaction_datetime("10-07-2019");
		pccareport.setPaymentTrxID("1234567890");
		pccareport.setPmnt_status("success");
		pccareport.setAmount("100.00");
		pccareport.setPolicy_number("DCI0012345");
		pccareport.setCustomer_name("Willion Rose");
		pccareport.setCustomer_nric_id("930110755044");
		pccareport.setPmnt_gateway_code("mpay");
		pccareport.setTxn_id("1122334455");
		pccareport.setAuth_code("1111111");
		
		
		pccareport.setProduct_code("PCCA01");
		pccareport.setAgent_code("0111111");
		pccareport.setAgent_name("Rose Ketty");
		
		
		pccareport.setCoverage_term("1000.00");
		pccareport.setCoverage_amount("101.00");
		pccareport.setPremium_amount("190.00");
		pccareport.setPremium_mode("annual");
		
		
		pccareport.setQuotation_status("success");
		pccareport.setLast_page("lastpage");
		pccareport.setReason("bmi not valid");
		
		pccareport.setUWReason("Rejection List Data");
		
		
		
		asserNotNull(pccareport);
		//dspCiTblQQ.setDspQqId(new BigDecimal("10000"));
		
		
		//getter
		
		pccareport.getAgent_code();
		pccareport.getAgent_name();
		logger.info("$$$$$$pccareport.getAgent_code();$$$$$"+pccareport.getAgent_code());
		
	}

	private void asserNotNull(PCCA01Report pccareport) {
		// TODO Auto-generated method stub
		
	}
}
