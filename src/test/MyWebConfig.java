package test;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.spring.admin.TranExcelController;
import com.spring.admin.TranReportController;
import com.spring.admin.UAMControllers.LoginController;
import com.spring.mapper.report.ALLReportMapper;
import com.spring.mapper.report.BuddyPAReportExcelMapper;
import com.spring.mapper.report.HOHHReportExcelMapper;
import com.spring.mapper.report.MPReportExcelMapper;
import com.spring.mapper.report.MotorReportExcelMapper;
import com.spring.mapper.report.PCCA01ReportExcelMapper;
import com.spring.mapper.report.TCReportExcelMapper;
import com.spring.mapper.report.TLReportExcelMapper;
import com.spring.mapper.report.TravelEzyReportExcelMapper;
import com.spring.mapper.report.WTCReportExcelMapper;

@Configuration
public class MyWebConfig {

@Autowired
	MotorReportExcelMapper motorReportExcelMapper;
	
	TLReportExcelMapper tlReportExcelMapper;
	
	WTCReportExcelMapper wtcReportExcelMapper;
	
	HOHHReportExcelMapper hohhReportExcelMapper;
	
	ALLReportMapper allReportMapper;

	BuddyPAReportExcelMapper buddyReportExcelMapper;
	
	TravelEzyReportExcelMapper travelEzyReportExcelMapper;
	
	TCReportExcelMapper tcReportExcelMapper;
	
	PCCA01ReportExcelMapper pcca01ReportExcelMapper;
	
	MPReportExcelMapper mpReportExcelMapper;

    @Bean
    public MyMvcController myMvcController() {
        return new MyMvcController();
    }
    
    
    @Bean
    public TranReportController tranReportController() {
        return new TranReportController();
    }
 
    @Autowired
    public TranExcelController tranExcelController;
    
    @Bean
    public LoginController logincontroller() {
    	
    	return new LoginController();
    }
    
    @Bean
    public TranExcelControllerTest3 tranexcelcintroller3()
    {
    	return new TranExcelControllerTest3();
    }
    
    
}