
package test;

public class LinkMPT {
	private String agentId;
	private String productType;
	private String productEntity;
	private String LinkMP;
	

	
	
	public LinkMPT(String agentId, String productType, String productEntity, String LinkMP) {
		super();
		this.agentId = agentId;
		this.productType = productType;
		this.productEntity = productEntity;
		this.LinkMP = LinkMP;
	}


	public String getAgentId() {
		return agentId;
	}


	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}


	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getProductEntity() {
		return productEntity;
	}
	public void setProductEntity(String productEntity) {
		this.productEntity = productEntity;
	}
	public String getLinkMP() {
		return LinkMP;
	}
	public void setLinkMP(String LinkMP) {
		this.LinkMP = LinkMP;
	}
	
	

}
