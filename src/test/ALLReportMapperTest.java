

package test;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.spring.VO.report.ALLReportVO;
import com.spring.admin.AgentController;

public class ALLReportMapperTest {

	final static Logger logger = Logger.getLogger(ALLReportMapperTest.class);
	@Test
	public void allreportvotest() {
		
		
		
		ALLReportVO allreportvo=new ALLReportVO();
		allreportvo.setTransaction_datetime("10-07-2019");
		allreportvo.setPaymentTrxID("1234567890");
		allreportvo.setPmnt_status("success");
		allreportvo.setAmount("100.00");
		allreportvo.setPolicy_number("DCI0012345");
		allreportvo.setCustomer_name("Willion Rose");
		allreportvo.setCustomer_nric_id("930110755044");
		allreportvo.setPmnt_gateway_code("mpay");
		allreportvo.setTxn_id("1122334455");
		allreportvo.setAuth_code("1111111");
		
		
		allreportvo.setProduct_code("PCCA01");
		allreportvo.setAgent_code("0111111");
		allreportvo.setAgent_name("Rose Ketty");
		
		
		allreportvo.setCoverage_term("1000.00");
		allreportvo.setCoverage_amount("101.00");
		allreportvo.setPremium_amount("190.00");
		allreportvo.setPremium_mode("annual");
		
		
		allreportvo.setQuotation_status("success");
		allreportvo.setLast_page("lastpage");
		allreportvo.setReason("bmi not valid");
		
		
		
		
		
		asserNotNull(allreportvo);
		//dspCiTblQQ.setDspQqId(new BigDecimal("10000"));
		
		
		//getter
		
		allreportvo.getAgent_code();
		allreportvo.getAgent_name();
		logger.info("$$$$$$allreportvo.getAgent_code();$$$$$"+allreportvo.getAgent_code());
		
	}

	private void asserNotNull(ALLReportVO allreportvo) {
		// TODO Auto-generated method stub
		
	}
}
