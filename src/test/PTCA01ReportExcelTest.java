
package test;

import java.io.File;
import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.spring.VO.report.PCCA01Report;
import com.spring.VO.report.PCCA01ReportExcel;
import com.spring.admin.AgentController;

public class PTCA01ReportExcelTest {

	final static Logger logger = Logger.getLogger(PTCA01ReportExcelTest.class);
	@Test
	public void pccareportexceltest() {
		
		
		
		PCCA01ReportExcel pccareportexcel=new PCCA01ReportExcel();
		pccareportexcel.setTransaction_datetime("10-07-2019");
		pccareportexcel.setPaymentTrxID("1234567890");
		pccareportexcel.setPmnt_status("success");
		pccareportexcel.setAmount("100.00");
		pccareportexcel.setPolicy_number("DCI0012345");
		pccareportexcel.setCustomer_name("Willion Rose");
		pccareportexcel.setCustomer_nric_id("930110755044");
		pccareportexcel.setPmnt_gateway_code("mpay");
		pccareportexcel.setTxn_id("1122334455");
		pccareportexcel.setAuth_code("1111111");
		
		
		pccareportexcel.setProduct_code("PTCA01");
		pccareportexcel.setAgent_code("0111111");
		pccareportexcel.setAgent_name("Rose Ketty");
		
		
		pccareportexcel.setCoverage_term("1000.00");
		pccareportexcel.setCoverage_amount("101.00");
		pccareportexcel.setPremium_amount("190.00");
		pccareportexcel.setPremium_mode("annual");
		
		
		pccareportexcel.setQuotation_status("success");
		pccareportexcel.setLast_page("lastpage");
		pccareportexcel.setReason("bmi not valid");
		
		pccareportexcel.setUWReason("Rejection List Data");
		
		
		
		asserNotNull(pccareportexcel);
		//dspCiTblQQ.setDspQqId(new BigDecimal("10000"));
		
		
		//getter
		
		pccareportexcel.getAgent_code();
		pccareportexcel.getAgent_name();
		logger.info("$$$$$$pccareportexcel.getAgent_code();$$$$$"+pccareportexcel.getAgent_code());
		
	}

	private void asserNotNull(PCCA01ReportExcel pccareportexcel) {
		// TODO Auto-generated method stub
		
	}

	public File getRecord_date() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getPmnt_gateway_code() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object getPmnt_status() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getLast_page() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getUWReason() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getQuotationCreateDateTime() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getTransaction_datetime() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getAmount() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getPremium_mode() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getDSPQQID() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getProduct_code() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getCustomer_name() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getCoverage_term() {
		// TODO Auto-generated method stub
		return null;
	}

	public int getCustomerAddress3() {
		// TODO Auto-generated method stub
		return 0;
	}

	public String getCustomerEmail() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getCustomerAddress1() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getCustomerPostcode() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getPolicy_number() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getCustomer_nric_id() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getTxn_id() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getAgent_code() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getCustomerAddress2() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getCustomerState() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getCoverage_amount() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getCustomerGender() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getCustomerMobileNo() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getDiscountCode() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getAgent_name() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getOperatorCode() {
		// TODO Auto-generated method stub
		return null;
	}
}
