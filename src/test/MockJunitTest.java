package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.spring.VO.AgentProdMap;
import com.spring.VO.AgentProdMapExample;
import com.spring.VO.AgentProfile;
import com.spring.VO.AgentProfileExample;
import com.spring.VO.Products;
import com.spring.VO.ProductsExample;
import com.spring.admin.AgentController;
import com.spring.mapper.AdminParamMapper;
import com.spring.mapper.AgentDocumentMapper;
import com.spring.mapper.AgentLinkMapper;
import com.spring.mapper.AgentProdMapMapper;
import com.spring.mapper.AgentProfileMapper;
import com.spring.mapper.ApprovalLogMapper;
import com.spring.mapper.ApprovalMapper;
import com.spring.mapper.CommonQQMapper;
import com.spring.mapper.ProductsMapper;
import com.spring.utils.AbsecSecurity;

@RunWith(MockitoJUnitRunner.class)
public class MockJunitTest {
	@Autowired
	AgentProfileMapper agentProfileMapper;
	@Autowired
	ProductsMapper productsMapper;
	@Autowired
	AgentProdMapMapper agentProdMapMapper;
	@Autowired
	CommonQQMapper commonQQMapper;
	@Autowired
	AdminParamMapper adminParamMapper;
	@Autowired
	AgentDocumentMapper agentDocumentMapper;
	@Autowired
	AgentLinkMapper agentLinkMapper;
	@Autowired
	ApprovalLogMapper approvalLogMapper;
	@Autowired
	ApprovalMapper approvalMapper;

	private MockHttpServletRequest request;
	private MockHttpSession session;
	private String UUID;
	private List<Link> agentProdMap;

	final static Logger logger = Logger.getLogger(MockJunitTest.class);

	@Before
	public void setupData() {
		UUID = AbsecSecurity.GetGUID();
		request = new MockHttpServletRequest();
		session = new MockHttpSession();
		session.setAttribute("Name", "Dhana");

		agentProdMap = new ArrayList<>();

		Link link1 = new Link("1", "PCCA01", "EIB",
				"https://uat.etiqa.com.my:4442/getonline/ecancercare-insurance-cyberagent?code=");
		agentProdMap.add(link1);
		link1 = new Link("1", "PTCA01", "ETB",
				"https://uat.etiqa.com.my:4442/getonline/ecancercare-takaful-cyberagent?code=");
		agentProdMap.add(link1);

		link1 = new Link("1", "MP", "EIB", "https://uat.etiqa.com.my:4442/getonline/medical-insurance-cyberagent?code=");
		agentProdMap.add(link1);
		link1 = new Link("1", "MPT", "ETB", "https://uat.etiqa.com.my:4442/getonline/medical-takaful-cyberagent?code=");
		agentProdMap.add(link1);

		link1 = new Link("2", "PCCA01", "EIB",
				"https://uat.etiqa.com.my:4442/getonline/ecancercare-insurance-cyberagent2?code=");
		agentProdMap.add(link1);
		link1 = new Link("2", "PTCA01", "ETB",
				"https://uat.etiqa.com.my:4442/getonline/ecancercare-takaful-cyberagent2?code=");
		agentProdMap.add(link1);

		link1 = new Link("2", "MP", "EIB", "https://uat.etiqa.com.my:4442/getonline/medical-insurance-cyberagent?code=");
		agentProdMap.add(link1);
		link1 = new Link("2", "MPT", "ETB", "https://uat.etiqa.com.my:4442/getonline/medical-takaful-cyberagent?code=");
		agentProdMap.add(link1);
		

	}

	@Test
	public void TestData() {
		assertEquals(session.getAttribute("Name"), "Dhana");
		assertNotEquals(session.getAttribute("Name"), "Dhanal");
	}

	@Test
	public void TestLink() {
		String link = getLinkForAgentId("1", "PCCA01", "EIB");

		assertEquals(link, "https://uat.etiqa.com.my:4442/getonline/ecancercare-insurance-cyberagent?code=" + UUID);

	}

	public String getLinkForAgentId(String agentId, String productType, String productEntity) {
		String agentLink = null;

		for (Link l : agentProdMap) {
			if (l.getAgentId() == agentId && l.getProductType().trim() == productType
					&& l.getProductEntity().trim() == productEntity) {
				return l.getLink() + UUID;
			}
		}

		return agentLink;
	}

	@Test
	public void agentProductSubmitDoneTest() {
		AgentProfile agentProfile = new AgentProfile();
		agentProfile.setId(new BigDecimal(2647));
		agentProfile.setAgentName("Dhana");
		List<AgentProdMap> listOfAgentProdMap = agentProductSubmitDone(agentProfile);
		for (AgentProdMap a : listOfAgentProdMap) {
			if (a.getProductCode().trim() == "PCCA01") {
				assertEquals(a.getProdRefUrl(),
						"https://uat.etiqa.com.my:4442/getonline/ecancercare-insurance-cyberagent?code="
								+ a.getAgentUuid());
			} else if (a.getProductCode().trim() == "PTCA01") {
				assertEquals(a.getProdRefUrl(),
						"https://uat.etiqa.com.my:4442/getonline/ecancercare-takaful-cyberagent?code="
								+ a.getAgentUuid());
			}
		}

	}

	public List<AgentProdMap> agentProductSubmitDone(AgentProfile agentProfile) {

		logger.info(agentProfile.getId() + "Agent Details" + agentProfile.getAgentName());

		List<AgentProfile> agentProfileList = new ArrayList<AgentProfile>();
		AgentProfileExample example = new AgentProfileExample();
		

		List<AgentProdMap> agentProdMapList = new ArrayList<AgentProdMap>();
		AgentProdMapExample agentProdMapExample = new AgentProdMapExample();
		AgentProdMapExample.Criteria agentProdMap_criteria = agentProdMapExample.createCriteria();
		agentProdMap_criteria.andAgpIdEqualTo(Short.parseShort(agentProfile.getId().toString()));
		

		logger.info("   agenet profile data  1116 ::::::::::::::" + agentProfile.getAgentName());
		return agentProdMapList;

	}




}
