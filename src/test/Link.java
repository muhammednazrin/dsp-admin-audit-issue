package test;

public class Link {
	private String agentId;
	private String productType;
	private String productEntity;
	private String link;
	

	
	
	public Link(String agentId, String productType, String productEntity, String link) {
		super();
		this.agentId = agentId;
		this.productType = productType;
		this.productEntity = productEntity;
		this.link = link;
	}


	public String getAgentId() {
		return agentId;
	}


	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}


	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getProductEntity() {
		return productEntity;
	}
	public void setProductEntity(String productEntity) {
		this.productEntity = productEntity;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	
	

}
