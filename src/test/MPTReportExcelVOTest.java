
package test;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.spring.VO.report.MPReportExcel;

import com.spring.admin.AgentController;

public class MPTReportExcelVOTest {

	final static Logger logger = Logger.getLogger(MPTReportExcelVOTest.class);

	@Test
	public void mpreportexceltest() {

		MPReportExcel mpreportexcel = new MPReportExcel();
		mpreportexcel.setTransaction_datetime("10-07-2019");
		mpreportexcel.setPaymentTrxID("1234567890");
		mpreportexcel.setPmnt_status("success");
		mpreportexcel.setAmount("100.00");
		mpreportexcel.setPolicy_number("MP0012345");
		mpreportexcel.setCustomer_name("carolla caterian");
		mpreportexcel.setCustomer_nric_id("930110755044");
		mpreportexcel.setPmnt_gateway_code("mpay");
		mpreportexcel.setTxn_id("1122334455");
		mpreportexcel.setAuth_code("1111111");

		mpreportexcel.setProduct_code("MP");
		mpreportexcel.setAgent_code("0111111");
		mpreportexcel.setAgent_name("Anij");

		mpreportexcel.setCoverage_term("1000.00");
		mpreportexcel.setCoverage_amount("101.00");
		mpreportexcel.setPremium_amount("190.00");
		mpreportexcel.setPremium_mode("annual");

		mpreportexcel.setQuotation_status("success");
		mpreportexcel.setLast_page("lastpage");
		mpreportexcel.setReason("bmi not valid");

		mpreportexcel.setUWReason("Rejection List Data");

		asserNotNull(mpreportexcel);
		// dspCiTblQQ.setDspQqId(new BigDecimal("10000"));

		// getter

		mpreportexcel.getAgent_code();
		mpreportexcel.getAgent_name();
		logger.info("$$$$$$mpreportexcel.getAgent_code();$$$$$" + mpreportexcel.getAgent_code());

	}

	private void asserNotNull(MPReportExcel mpreportexcel) {
		// TODO Auto-generated method stub

	}
}

