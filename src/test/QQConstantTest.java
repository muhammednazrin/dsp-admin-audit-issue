package test;

import org.apache.log4j.Logger;

public class QQConstantTest {
	final static Logger logger = Logger.getLogger(QQConstantTest.class);

	private QQConstantTest() {

	}

	public static final String CANCERCARE_INSURANCE = "PCCA01";
	public static final String CANCERCARE_TAKAFUL = "PTCA01";
	public static final String MEDICALPASS_INSURANCE = "MP";
	public static final String MEDICALPASS_TAKAFUL = "MPT";
	public static final String CANCERPRODUCTNAMEI="e-cancercare";
    public static final String MEDICALPRODUCTNAMET="Medical-Pass";
    public static final String PRODUCTENTITYI="EIB";
    public static final String PRODUCTENTITYT="ETB";
    public static final String STATUS="SUCCESS";
    

}
